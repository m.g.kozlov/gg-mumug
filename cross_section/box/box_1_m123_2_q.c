#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m123_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(Power(s1,9)*(s2 - t1)*
          (s*(Power(s2,2) + Power(t1,2) - 2*s2*(5 + t1 - 5*t2) - 
               10*t1*(-1 + t2) + 6*Power(-1 + t2,2)) - 
            2*Power(s,2)*(-3 + s2 - t1 + 3*t2) - 
            (s2 - t1)*(-1 + t2)*(-6 + 5*s2 - 5*t1 + 6*t2)) + 
         t2*Power(-1 + s + t2,3)*(-1 + s2 - t1 + t2)*
          (Power(s,4)*(-2 + t1 - t2) - 
            Power(s,3)*(1 - 2*Power(t1,2) + t2 + t1*(5 + 2*t2) + 
               s2*(2*t1 - 3*(2 + t2))) + 
            Power(s,2)*(10 + Power(t1,3) - 5*t2 - 2*Power(t2,2) - 
               Power(t1,2)*(10 + t2) + t1*(3 + 8*t2) + 
               Power(s2,2)*(t1 - 3*(2 + t2)) - 
               2*s2*(4 + Power(t1,2) + t2 - 2*t1*(4 + t2))) + 
            s*(-13 - 3*Power(t1,3) + 5*t2 + 8*Power(t2,2) + 
               Power(s2,3)*(2 + t2) + Power(t1,2)*(13 + t2) + 
               t1*(3 - 18*t2 + 4*Power(t2,2)) + 
               s2*(11*t2 - 4*t1*(6 + t2) + Power(t1,2)*(8 + t2)) + 
               Power(s2,2)*(11 + 3*t2 - t1*(7 + 2*t2))) - 
            2*(-3 + Power(s2,3) - Power(t1,3) + t2 + 7*Power(t2,2) - 
               4*Power(t2,3) + Power(t1,2)*(3 + t2 - Power(t2,2)) + 
               Power(s2,2)*(3 - 3*t1 + t2 - Power(t2,2)) + 
               t1*(1 - 6*t2 + 2*Power(t2,2)) + 
               s2*(-1 + 3*Power(t1,2) + 6*t2 - 2*Power(t2,2) + 
                  2*t1*(-3 - t2 + Power(t2,2))))) + 
         Power(s1,8)*(Power(s,3)*
             (-2 + 7*Power(s2,2) + 14*Power(t1,2) + t1*(8 - 21*t2) + 
               12*t2 - 6*Power(t2,2) + s2*(-18 - 17*t1 + 23*t2)) - 
            (s2 - t1)*(-1 + t2)*
             (22 + 10*Power(s2,3) - 10*Power(t1,3) + 
               Power(s2,2)*(-30*t1 + 7*(-4 + t2)) - 48*t2 + 
               26*Power(t2,2) + Power(t1,2)*(-32 + 11*t2) + 
               2*s2*(-2 + 15*Power(t1,2) - 7*t1*(-4 + t2) + 7*t2 - 
                  5*Power(t2,2)) + 2*t1*(7 - 13*t2 + 6*Power(t2,2))) + 
            Power(s,2)*(6 - 8*Power(s2,3) + 12*Power(t1,3) + 
               Power(s2,2)*(52 + 24*t1 - 50*t2) + 
               Power(t1,2)*(28 - 34*t2) - 26*t2 + 26*Power(t2,2) - 
               6*Power(t2,3) - 
               s2*(49 + 28*Power(t1,2) + t1*(88 - 92*t2) - 56*t2 + 
                  11*Power(t2,2)) + t1*(69 - 80*t2 + 15*Power(t2,2))) + 
            s*(2*Power(s2,4) + 2*Power(t1,4) + Power(t1,3)*(34 - 31*t2) + 
               4*Power(-1 + t2,2)*(-4 + 5*t2) + 
               Power(s2,3)*(-42 - 8*t1 + 39*t2) + 
               4*Power(t1,2)*(26 - 35*t2 + 9*Power(t2,2)) + 
               t1*(-1 - 5*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(77 + 12*Power(t1,2) - 102*t2 + 
                  25*Power(t2,2) - 9*t1*(-14 + 13*t2)) - 
               s2*(9 + 8*Power(t1,3) + Power(t1,2)*(118 - 109*t2) - 
                  22*t2 + 9*Power(t2,2) + 4*Power(t2,3) + 
                  3*t1*(59 - 78*t2 + 19*Power(t2,2))))) - 
         Power(s1,7)*(16 - 25*t1 - 185*Power(t1,2) + 5*Power(t1,3) + 
            20*Power(t1,4) + 5*Power(t1,5) + 5*Power(s2,5)*(-1 + t2) - 
            Power(s2,4)*(16 + 25*t1 - 4*t2)*(-1 + t2) - 68*t2 + 
            53*t1*t2 + 409*Power(t1,2)*t2 + 39*Power(t1,3)*t2 - 
            28*Power(t1,4)*t2 - 5*Power(t1,5)*t2 + 108*Power(t2,2) + 
            7*t1*Power(t2,2) - 253*Power(t1,2)*Power(t2,2) - 
            64*Power(t1,3)*Power(t2,2) + 8*Power(t1,4)*Power(t2,2) - 
            76*Power(t2,3) - 73*t1*Power(t2,3) + 
            19*Power(t1,2)*Power(t2,3) + 20*Power(t1,3)*Power(t2,3) + 
            20*Power(t2,4) + 38*t1*Power(t2,4) + 
            10*Power(t1,2)*Power(t2,4) + 
            Power(s2,3)*(-1 + t2)*
             (-45 + 50*Power(t1,2) + t1*(61 - 13*t2) + 89*t2 - 
               15*Power(t2,2)) + 
            Power(s,4)*(-20 + 9*Power(s2,2) + 40*Power(t1,2) + 64*t2 - 
               25*Power(t2,2) - 2*t1*(12 + 17*t2) + 
               s2*(-8 - 30*t1 + 28*t2)) + 
            Power(s2,2)*(-1 + t2)*
             (154 - 50*Power(t1,3) - 202*t2 + 42*Power(t2,2) + 
               6*Power(t2,3) + Power(t1,2)*(-98 + 26*t2) + 
               t1*(101 - 230*t2 + 42*Power(t2,2))) - 
            s2*(-1 + t2)*(35 - 25*Power(t1,4) - 50*t2 - 21*Power(t2,2) + 
               36*Power(t2,3) + Power(t1,3)*(-73 + 25*t2) + 
               Power(t1,2)*(51 - 185*t2 + 47*Power(t2,2)) + 
               t1*(335 - 418*t2 + 67*Power(t2,2) + 16*Power(t2,3))) + 
            Power(s,3)*(36 - 16*Power(s2,3) + 53*Power(t1,3) + 
               Power(s2,2)*(60 + 62*t1 - 77*t2) + 
               Power(t1,2)*(19 - 22*t2) - 88*t2 + 105*Power(t2,2) - 
               33*Power(t2,3) + t1*(102 - 141*t2 - 22*Power(t2,2)) + 
               s2*(-82 - 103*Power(t1,2) + 81*t2 + 18*Power(t2,2) + 
                  4*t1*(-25 + 37*t2))) + 
            Power(s,2)*(-35 + 8*Power(s2,4) + 20*Power(t1,4) + 
               Power(t1,3)*(50 - 15*t2) + 78*t2 - 103*Power(t2,2) + 
               68*Power(t2,3) - 8*Power(t2,4) + 
               Power(s2,3)*(-70 - 37*t1 + 74*t2) + 
               Power(s2,2)*(205 + 74*Power(t1,2) + t1*(220 - 217*t2) - 
                  251*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(155 - 274*t2 + 55*Power(t2,2)) - 
               t1*(-201 + 147*t2 + 28*Power(t2,2) + 10*Power(t2,3)) - 
               s2*(139 + 65*Power(t1,3) - 125*t2 + 18*Power(t2,2) - 
                  16*Power(t2,3) - 6*Power(t1,2)*(-32 + 25*t2) + 
                  t1*(379 - 503*t2 + 32*Power(t2,2)))) + 
            s*(-Power(s2,5) + Power(t1,5) + 
               5*Power(s2,4)*(6 + t1 - 6*t2) - 14*Power(t1,4)*(-1 + t2) - 
               Power(s2,3)*(121 + 10*Power(t1,2) - 118*t1*(-1 + t2) - 
                  135*t2 + 15*Power(t2,2)) + 
               Power(-1 + t2,2)*(-57 + 65*t2 + 20*Power(t2,2)) + 
               Power(t1,3)*(54 - 95*t2 + 42*Power(t2,2)) + 
               Power(t1,2)*(127 + 49*t2 - 225*Power(t2,2) + 
                  49*Power(t2,3)) + 
               t1*(-272 + 645*t2 - 504*Power(t2,2) + 129*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,2)*(36 + 10*Power(t1,3) - 
                  152*Power(t1,2)*(-1 + t2) + 142*t2 - 210*Power(t2,2) + 
                  32*Power(t2,3) + t1*(305 - 335*t2 + 33*Power(t2,2))) - 
               s2*(-212 + 5*Power(t1,4) - 78*Power(t1,3)*(-1 + t2) + 
                  529*t2 - 456*Power(t2,2) + 141*Power(t2,3) - 
                  2*Power(t2,4) + 
                  Power(t1,2)*(242 - 303*t2 + 64*Power(t2,2)) + 
                  2*t1*(69 + 115*t2 - 219*Power(t2,2) + 35*Power(t2,3))))) \
+ Power(s1,6)*(Power(s,5)*(-48 + 5*Power(s2,2) + 60*Power(t1,2) + 
               120*t2 - 35*Power(t2,2) - 4*t1*(15 + 11*t2) + 
               s2*(-28*t1 + 6*(4 + t2))) + 
            Power(s,4)*(26 - 12*Power(s2,3) + 110*Power(t1,3) + 
               Power(s2,2)*(-31 + 78*t1 - 30*t2) - 56*t2 + 
               163*Power(t2,2) - 56*Power(t2,3) + 
               4*Power(t1,2)*(9 + 5*t2) - 
               t1*(74 + 73*t2 + 119*Power(t2,2)) + 
               s2*(30 - 194*Power(t1,2) + 24*t2 + 40*Power(t2,2) + 
                  t1*(-13 + 131*t2))) + 
            Power(s,3)*(77 + 9*Power(s2,4) + 64*Power(t1,4) - 160*t2 + 
               145*Power(t2,2) + 33*Power(t2,3) - 23*Power(t2,4) + 
               Power(s2,3)*(20 - 70*t1 + 29*t2) + 
               Power(t1,3)*(30 + 113*t2) + 
               Power(t1,2)*(-182 + 135*t2 - 43*Power(t2,2)) + 
               2*Power(s2,2)*
                (61 + 102*Power(t1,2) + t1*(52 - 59*t2) - 164*t2 - 
                  Power(t2,2)) - 
               t1*(-501 + 539*t2 + 79*Power(t2,2) + 101*Power(t2,3)) + 
               s2*(-360 - 207*Power(t1,3) + 334*t2 + 29*Power(t2,2) + 
                  51*Power(t2,3) - 3*Power(t1,2)*(43 + 23*t2) + 
                  t1*(-43 + 264*t2 + 189*Power(t2,2)))) + 
            Power(s,2)*(-203 - 2*Power(s2,5) + 10*Power(t1,5) + 522*t2 - 
               607*Power(t2,2) + 298*Power(t2,3) - 8*Power(t2,4) - 
               2*Power(t2,5) + 2*Power(s2,4)*(-8 + 10*t1 + t2) + 
               18*Power(t1,4)*(-3 + 4*t2) - 
               Power(s2,3)*(117 - 2*t1 + 68*Power(t1,2) - 222*t2 + 
                  14*t1*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-178 + 233*t2 + 62*Power(t2,2)) + 
               Power(t1,2)*(376 - 591*t2 + 8*Power(t2,2) - 
                  14*Power(t2,3)) - 
               2*t1*(104 - 651*t2 + 521*Power(t2,2) - 11*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,2)*(321 + 94*Power(t1,3) + 89*t2 - 
                  539*Power(t2,2) + 36*Power(t2,3) + 
                  5*Power(t1,2)*(-11 + 31*t2) + 
                  t1*(6 - 62*t2 - 107*Power(t2,2))) + 
               s2*(205 - 54*Power(t1,4) + Power(t1,3)*(123 - 215*t2) - 
                  989*t2 + 782*Power(t2,2) - 65*Power(t2,3) + 
                  11*Power(t2,4) + 
                  Power(t1,2)*(296 - 363*t2 + 17*Power(t2,2)) + 
                  t1*(-631 + 195*t2 + 699*Power(t2,2) + 51*Power(t2,3)))) \
+ s*(Power(s2,5)*(6 - 7*t2) + Power(t1,5)*(-6 + 7*t2) + 
               2*Power(t1,4)*(8 - 35*t2 + 24*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (-78 + 209*t2 - 237*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(89 - 383*t2 + 258*Power(t2,2) + 
                  31*Power(t2,3)) + 
               Power(t1,2)*(-258 + 1170*t2 - 891*Power(t2,2) - 
                  38*Power(t2,3) + 17*Power(t2,4)) + 
               t1*(-484 + 515*t2 + 421*Power(t2,2) - 561*Power(t2,3) + 
                  107*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,4)*(49 - 72*t2 + 17*Power(t2,2) + 
                  t1*(-34 + 39*t2)) - 
               Power(s2,3)*(33 + 219*t2 - 288*Power(t2,2) + 
                  31*Power(t2,3) + 2*Power(t1,2)*(-46 + 51*t2) + 
                  t1*(26 - 84*t2 + 34*Power(t2,2))) + 
               Power(s2,2)*(-461 + 1177*t2 - 540*Power(t2,2) - 
                  205*Power(t2,3) + 29*Power(t2,4) + 
                  2*Power(t1,3)*(-56 + 61*t2) + 
                  2*Power(t1,2)*(-35 - 36*t2 + 53*Power(t2,2)) + 
                  t1*(137 + 188*t2 - 354*Power(t2,2) + 14*Power(t2,3))) \
+ s2*(399 + Power(t1,4)*(54 - 59*t2) - 492*t2 - 218*Power(t2,2) + 
                  428*Power(t2,3) - 117*Power(t2,4) + 
                  Power(t1,3)*(31 + 130*t2 - 137*Power(t2,2)) - 
                  Power(t1,2)*
                   (210 - 437*t2 + 187*Power(t2,2) + 25*Power(t2,3)) + 
                  t1*(743 - 2272*t2 + 1222*Power(t2,2) + 
                     340*Power(t2,3) - 33*Power(t2,4)))) + 
            (-1 + t2)*(-11*Power(s2,5)*(-2 + t2) + 
               Power(t1,5)*(-14 + 3*t2) + 
               2*Power(t1,4)*(-11 + 12*t2 + 8*Power(t2,2)) + 
               2*Power(-1 + t2,2)*(-17 + 12*t2 + 13*Power(t2,2)) + 
               Power(t1,3)*(39 - 105*t2 + 53*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(116 + 290*t2 - 463*Power(t2,2) + 
                  53*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(-223 + 559*t2 - 489*Power(t2,2) + 137*Power(t2,3) + 
                  16*Power(t2,4)) + 
               Power(s2,4)*(-6 + 24*t2 + t1*(-104 + 49*t2)) + 
               Power(s2,3)*(-130 + Power(t1,2)*(204 - 94*t2) + t2 + 
                  151*Power(t2,2) - 17*Power(t2,3) + 
                  2*t1*(49 - 88*t2 + 3*Power(t2,2))) + 
               Power(s2,2)*(40 + 332*t2 - 449*Power(t2,2) + 
                  75*Power(t2,3) + 2*Power(t2,4) + 
                  22*Power(t1,3)*(-9 + 4*t2) + 
                  Power(t1,2)*(-209 + 306*t2 + 11*Power(t2,2)) + 
                  t1*(289 - 8*t2 - 329*Power(t2,2) + 33*Power(t2,3))) - 
               s2*(5*Power(t1,4)*(-18 + 7*t2) + 
                  Power(t1,3)*(-139 + 178*t2 + 33*Power(t2,2)) + 
                  Power(t1,2)*
                   (201 - 117*t2 - 124*Power(t2,2) + 25*Power(t2,3)) + 
                  2*t1*(70 + 323*t2 - 456*Power(t2,2) + 60*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  2*(-98 + 259*t2 - 246*Power(t2,2) + 79*Power(t2,3) + 
                     6*Power(t2,4))))) - 
         s1*Power(-1 + s + t2,2)*
          (-30 + 223*t1 - 20*Power(t1,2) - 230*Power(t1,3) + 
            50*Power(t1,4) + 7*Power(t1,5) + 
            4*Power(s,6)*Power(t1 - t2,2) + 5*t2 - 668*t1*t2 + 
            568*Power(t1,2)*t2 + 522*Power(t1,3)*t2 - 
            141*Power(t1,4)*t2 - 14*Power(t1,5)*t2 + 205*Power(t2,2) + 
            231*t1*Power(t2,2) - 1396*Power(t1,2)*Power(t2,2) - 
            295*Power(t1,3)*Power(t2,2) + 141*Power(t1,4)*Power(t2,2) + 
            6*Power(t1,5)*Power(t2,2) - 211*Power(t2,3) + 
            968*t1*Power(t2,3) + 1133*Power(t1,2)*Power(t2,3) - 
            70*Power(t1,3)*Power(t2,3) - 44*Power(t1,4)*Power(t2,3) - 
            167*Power(t2,4) - 1070*t1*Power(t2,4) - 
            255*Power(t1,2)*Power(t2,4) + 64*Power(t1,3)*Power(t2,4) + 
            302*Power(t2,5) + 316*t1*Power(t2,5) - 
            26*Power(t1,2)*Power(t2,5) - 104*Power(t2,6) + 
            Power(s2,5)*(-16 + 42*t2 - 32*Power(t2,2) + 7*Power(t2,3)) - 
            Power(s,5)*(6 - 7*Power(t1,3) + 8*Power(s2,2)*(-1 + t2) + 
               4*t2 + 3*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,2)*(-11 + 6*t2) + 
               3*t1*(-6 + 4*t2 + 3*Power(t2,2)) + 
               s2*(6 + 3*Power(t1,2) + t1*(33 - 15*t2) - 35*t2 + 
                  12*Power(t2,2))) - 
            Power(s2,4)*(14 - 19*t2 - 12*Power(t2,2) + 10*Power(t2,3) + 
               Power(t2,4) + t1*
                (-68 + 165*t2 - 109*Power(t2,2) + 17*Power(t2,3))) - 
            Power(s2,2)*(-14 - 154*t2 + 461*Power(t2,2) - 
               365*Power(t2,3) + 38*Power(t2,4) + 30*Power(t2,5) + 
               Power(t1,3)*(-109 + 233*t2 - 117*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(323 - 925*t2 + 840*Power(t2,2) - 161*Power(t2,3) - 
                  50*Power(t2,4)) + 
               Power(t1,2)*(16 + 98*t2 - 297*Power(t2,2) + 
                  146*Power(t2,3) + Power(t2,4))) + 
            Power(s2,3)*(113 - 388*t2 + 467*Power(t2,2) - 
               211*Power(t2,3) + 28*Power(t2,4) + 
               Power(t1,2)*(-121 + 273*t2 - 155*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(48 - 35*t2 - 108*Power(t2,2) + 69*Power(t2,3) + 
                  2*Power(t2,4))) + 
            s2*(-67 + 332*t2 - 495*Power(t2,2) + 212*Power(t2,3) + 
               66*Power(t2,4) - 48*Power(t2,5) + 
               Power(t1,4)*(-47 + 97*t2 - 45*Power(t2,2)) + 
               Power(t1,3)*(-68 + 255*t2 - 342*Power(t2,2) + 
                  131*Power(t2,3)) + 
               Power(t1,2)*(458 - 1129*t2 + 766*Power(t2,2) + 
                  62*Power(t2,3) - 130*Power(t2,4)) + 
               t1*(-180 - 11*t2 + 884*Power(t2,2) - 945*Power(t2,3) + 
                  192*Power(t2,4) + 52*Power(t2,5))) + 
            Power(s,4)*(-4 + 2*Power(t1,4) + 24*Power(s2,3)*(-1 + t2) + 
               82*t2 - 64*Power(t2,2) - 8*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,3)*(-1 + 11*t2) + 
               Power(t1,2)*(52 + 33*t2 - 24*Power(t2,2)) + 
               t1*(-94 + 67*t2 - 40*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s2,2)*(-10 + 6*Power(t1,2) + 62*t2 + 
                  3*Power(t2,2) + 2*t1*(-51 + 13*t2)) + 
               s2*(33 + 4*Power(t1,3) - 76*t2 + 110*Power(t2,2) - 
                  23*Power(t2,3) + Power(t1,2)*(-85 + 3*t2) + 
                  2*t1*(-11 + t2 + 8*Power(t2,2)))) - 
            Power(s,3)*(-29 + Power(t1,5) + 24*Power(s2,4)*(-1 + t2) - 
               30*t2 - 131*Power(t2,2) + 126*Power(t2,3) + 
               13*Power(t2,4) - Power(t1,4)*(21 + 8*t2) + 
               Power(t1,3)*(-84 + 85*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(346 - 151*t2 - 121*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(91 - 204*t2 + 14*Power(t2,2) + 68*Power(t2,3) - 
                  8*Power(t2,4)) - 
               Power(s2,3)*(54 + 5*Power(t1,2) - 55*t2 + 
                  41*Power(t2,2) + 3*t1*(-35 + 13*t2)) + 
               Power(s2,2)*(164 + 11*Power(t1,3) - 516*t2 + 
                  267*Power(t2,2) - 14*Power(t2,3) + 
                  Power(t1,2)*(-193 + 22*t2) + 
                  t1*(282 - 143*t2 + 42*Power(t2,2))) + 
               s2*(-8 - 7*Power(t1,4) + 460*t2 - 195*Power(t2,2) - 
                  123*Power(t2,3) + 11*Power(t2,4) + 
                  Power(t1,3)*(133 + t2) + 
                  Power(t1,2)*(-126 + t2 - 18*Power(t2,2)) + 
                  t1*(-632 + 663*t2 - 88*Power(t2,2) + 13*Power(t2,3)))) \
+ s*(31 + 15*Power(t1,5)*(-1 + t2) + 398*t2 - 1126*Power(t2,2) + 
               622*Power(t2,3) + 327*Power(t2,4) - 252*Power(t2,5) + 
               Power(t1,4)*(-61 + 121*t2 - 66*Power(t2,2)) + 
               3*Power(s2,5)*(8 - 14*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(578 - 991*t2 + 409*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(t1,2)*(-74 - 951*t2 + 1936*Power(t2,2) - 
                  996*Power(t2,3) + 81*Power(t2,4)) - 
               t1*(667 - 1600*t2 + 321*Power(t2,2) + 1344*Power(t2,3) - 
                  788*Power(t2,4) + 52*Power(t2,5)) + 
               Power(s2,4)*(62 - 130*t2 + 84*Power(t2,2) - 
                  22*Power(t2,3) + t1*(-104 + 153*t2 - 37*Power(t2,2))) \
+ Power(s2,3)*(-228 + 679*t2 - 624*Power(t2,2) + 163*Power(t2,3) - 
                  Power(t2,4) + 
                  Power(t1,2)*(199 - 250*t2 + 33*Power(t2,2)) + 
                  t1*(-245 + 382*t2 - 148*Power(t2,2) + 35*Power(t2,3))) \
+ s2*(186 - 1095*t2 + 1691*Power(t2,2) - 863*Power(t2,3) + 
                  37*Power(t2,4) + 40*Power(t2,5) + 
                  Power(t1,4)*(93 - 100*t2 + 4*Power(t2,2)) - 
                  Power(t1,3)*
                   (41 + 40*t2 - 106*Power(t2,2) + Power(t2,3)) + 
                  t1*(697 - 538*t2 - 784*Power(t2,2) + 
                     651*Power(t2,3) - 18*Power(t2,4)) + 
                  Power(t1,2)*
                   (-1007 + 1937*t2 - 983*Power(t2,2) + 
                     17*Power(t2,3) - 3*Power(t2,4))) + 
               Power(s2,2)*(-75 - 4*t2 + 76*Power(t2,2) + 
                  96*Power(t2,3) - 97*Power(t2,4) + 
                  Power(t1,3)*(-197 + 224*t2 - 15*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (-95 + 111*t2 - 8*Power(t2,2) + 4*Power(t2,3)) + 
                  t1*(599 - 1459*t2 + 1048*Power(t2,2) - 
                     156*Power(t2,3) + 4*Power(t2,4)))) + 
            Power(s,2)*(-20 - Power(t1,5)*(-9 + t2) + 
               8*Power(s2,5)*(-1 + t2) - 479*t2 + 717*Power(t2,2) + 
               31*Power(t2,3) - 207*Power(t2,4) - 10*Power(t2,5) + 
               Power(t1,4)*(-22 - 3*t2 + 6*Power(t2,2)) - 
               Power(t1,3)*(444 - 556*t2 + 122*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(611 - 1205*t2 + 211*Power(t2,2) + 387*Power(t2,3) - 
                  84*Power(t2,4)) + 
               Power(t1,2)*(382 + 128*t2 - 619*Power(t2,2) + 
                  194*Power(t2,3) + 4*Power(t2,4)) + 
               Power(s2,4)*(-82 + 124*t2 - 45*Power(t2,2) - 
                  4*t1*(-9 + 5*t2)) + 
               Power(s2,3)*(91 - 280*t2 + 108*Power(t2,2) + 
                  16*Power(t2,3) + Power(t1,2)*(-83 + 25*t2) + 
                  t1*(342 - 338*t2 + 72*Power(t2,2))) - 
               Power(s2,2)*(-216 + 667*t2 - 697*Power(t2,2) + 
                  268*Power(t2,3) - 9*Power(t2,4) + 
                  Power(t1,3)*(-99 + 23*t2) + 
                  3*Power(t1,2)*(172 - 125*t2 + 9*Power(t2,2)) + 
                  t1*(114 - 470*t2 + 165*Power(t2,2) + 12*Power(t2,3))) + 
               s2*(-154 + 1286*t2 - 1579*Power(t2,2) + 377*Power(t2,3) + 
                  82*Power(t2,4) + Power(t1,4)*(-53 + 11*t2) - 
                  2*Power(t1,3)*(-139 + 79*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (529 - 856*t2 + 219*Power(t2,2) + 9*Power(t2,3)) - 
                  t1*(1112 - 1343*t2 + 252*Power(t2,2) + Power(t2,3) + 
                     14*Power(t2,4))))) + 
         Power(s1,5)*(Power(s,6)*
             (44 - Power(s2,2) - 50*Power(t1,2) + 
               2*s2*(7*t1 + 5*(-3 + t2)) - 96*t2 + 15*Power(t2,2) + 
               t1*(46 + 48*t2)) + 
            Power(s,4)*(-263 - 3*Power(s2,4) - 90*Power(t1,4) + 508*t2 - 
               514*Power(t2,2) + 55*Power(t2,3) + 10*Power(t2,4) + 
               Power(s2,3)*(-83 + 65*t1 + 7*t2) - 
               Power(t1,3)*(81 + 181*t2) + 
               6*Power(t1,2)*(73 - 127*t2 + 33*Power(t2,2)) + 
               t1*(-219 + 480*t2 + 353*Power(t2,2) + 143*Power(t2,3)) + 
               Power(s2,2)*(149 - 272*Power(t1,2) + 341*t2 - 
                  55*Power(t2,2) + t1*(-141 + 92*t2)) + 
               s2*(274 + 300*Power(t1,3) - 449*t2 - 138*Power(t2,2) + 
                  30*Power(t2,3) + 3*Power(t1,2)*(94 + 57*t2) + 
                  t1*(-411 + 143*t2 - 377*Power(t2,2)))) + 
            Power(s,5)*(66 + 3*Power(s2,3) - 117*Power(t1,3) + 
               Power(s2,2)*(76 - 51*t1 - 13*t2) - 96*t2 - 
               116*Power(t2,2) + 25*Power(t2,3) - 
               2*Power(t1,2)*(62 + 3*t2) + 
               t1*(214 + 43*t2 + 153*Power(t2,2)) + 
               s2*(195*Power(t1,2) + t1*(17 - 131*t2) + 
                  2*(-77 + t2 + 7*Power(t2,2)))) + 
            Power(s,3)*(236 + Power(s2,5) - 22*Power(t1,5) + 
               Power(t1,4)*(115 - 178*t2) + 
               Power(s2,4)*(62 - 33*t1 - 23*t2) - 664*t2 + 
               656*Power(t2,2) - 501*Power(t2,3) + 105*Power(t2,4) - 
               Power(t1,3)*(-583 + 950*t2 + Power(t2,2)) + 
               2*Power(t1,2)*
                (111 + 307*t2 - 323*Power(t2,2) + 99*Power(t2,3)) + 
               t1*(-742 - 124*t2 + 522*Power(t2,2) + 606*Power(t2,3) + 
                  33*Power(t2,4)) + 
               Power(s2,3)*(-90 + 153*Power(t1,2) - 257*t2 + 
                  13*Power(t2,2) + t1*(-7 + 101*t2)) + 
               Power(s2,2)*(-524 - 233*Power(t1,3) + 
                  Power(t1,2)*(56 - 479*t2) + 201*t2 + 
                  907*Power(t2,2) - 133*Power(t2,3) + 
                  t1*(915 - 886*t2 + 402*Power(t2,2))) + 
               s2*(430 + 134*Power(t1,4) + 660*t2 - 812*Power(t2,2) - 
                  237*Power(t2,3) + 40*Power(t2,4) + 
                  Power(t1,3)*(-226 + 579*t2) + 
                  Power(t1,2)*(-1483 + 2114*t2 - 320*Power(t2,2)) - 
                  t1*(-185 + 132*t2 + 903*Power(t2,2) + 237*Power(t2,3)))\
) + Power(s,2)*(48 + Power(t1,5)*(45 - 46*t2) + 281*t2 - 
               503*Power(t2,2) + 400*Power(t2,3) - 263*Power(t2,4) + 
               37*Power(t2,5) + Power(s2,5)*(-33 + 5*t1 + 27*t2) + 
               Power(s2,4)*(25 + 135*t1 - 26*Power(t1,2) + 45*t2 - 
                  160*t1*t2) + 
               Power(t1,4)*(173 - 62*t2 - 103*Power(t2,2)) + 
               Power(t1,3)*(-98 + 1260*t2 - 1352*Power(t2,2) + 
                  85*Power(t2,3)) + 
               Power(t1,2)*(-777 + 949*t2 - 147*Power(t2,2) + 
                  183*Power(t2,3) + 34*Power(t2,4)) + 
               t1*(1274 - 2895*t2 + 558*Power(t2,2) + 727*Power(t2,3) + 
                  229*Power(t2,4) - 5*Power(t2,5)) + 
               Power(s2,3)*(263 + 48*Power(t1,3) + 160*t2 - 
                  597*Power(t2,2) + 71*Power(t2,3) + 
                  10*Power(t1,2)*(-35 + 46*t2) - 
                  4*t1*(184 - 178*t2 + 29*Power(t2,2))) + 
               Power(s2,2)*(313 - 38*Power(t1,4) + 
                  Power(t1,3)*(472 - 594*t2) - 1663*t2 + 
                  612*Power(t2,2) + 850*Power(t2,3) - 118*Power(t2,4) + 
                  Power(t1,2)*(1559 - 1478*t2 - 27*Power(t2,2)) + 
                  t1*(-429 + 677*t2 - 220*Power(t2,2) + 233*Power(t2,3))\
) + s2*(-1125 + 11*Power(t1,5) + 1901*t2 + 340*Power(t2,2) - 
                  1008*Power(t2,3) - 10*Power(t2,4) + 14*Power(t2,5) + 
                  Power(t1,4)*(-269 + 313*t2) + 
                  Power(t1,3)*(-1021 + 783*t2 + 246*Power(t2,2)) + 
                  Power(t1,2)*
                   (355 - 2333*t2 + 2272*Power(t2,2) - 347*Power(t2,3)) \
+ t1*(477 + 577*t2 + 297*Power(t2,2) - 1614*Power(t2,3) + 27*Power(t2,4))\
)) - (-1 + t2)*(8*Power(s2,6)*(-1 + t2) + 
               2*Power(t1,5)*(-5 + 4*Power(t2,2)) + 
               Power(s2,5)*(16 - 45*t1*(-1 + t2) - 19*t2 + 
                  5*Power(t2,2)) + 
               Power(t1,4)*(125 - 212*t2 + 113*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (70 - 155*t2 + 135*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,3)*(129 - 371*t2 + 231*Power(t2,2) + 
                  32*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,2)*(-214 + 772*t2 - 352*Power(t2,2) - 
                  214*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(-169 - 144*t2 + 773*Power(t2,2) - 546*Power(t2,3) + 
                  82*Power(t2,4) + 4*Power(t2,5)) + 
               2*Power(s2,4)*
                (26 + 53*Power(t1,2)*(-1 + t2) - 5*t2 - 
                  13*Power(t2,2) + 4*Power(t2,3) - 
                  5*t1*(13 - 14*t2 + 2*Power(t2,2))) + 
               Power(s2,3)*(74 - 128*Power(t1,3)*(-1 + t2) - 190*t2 - 
                  32*Power(t2,2) + 137*Power(t2,3) - 14*Power(t2,4) + 
                  2*Power(t1,2)*(141 - 132*t2 + Power(t2,2)) + 
                  t1*(-283 + 353*t2 - 173*Power(t2,2) + 7*Power(t2,3))) \
+ s2*(143 - 19*Power(t1,5)*(-1 + t2) + 117*t2 - 666*Power(t2,2) + 
                  519*Power(t2,3) - 111*Power(t2,4) - 2*Power(t2,5) + 
                  Power(t1,4)*(90 - 41*t2 - 39*Power(t2,2)) + 
                  Power(t1,3)*
                   (-461 + 842*t2 - 501*Power(t2,2) + 24*Power(t2,3)) + 
                  t1*(562 - 1298*t2 + 23*Power(t2,2) + 
                     764*Power(t2,3) - 51*Power(t2,4)) + 
                  Power(t1,2)*
                   (-301 + 784*t2 - 763*Power(t2,2) + 223*Power(t2,3) - 
                     18*Power(t2,4))) + 
               Power(s2,2)*(-350 + 78*Power(t1,4)*(-1 + t2) + 613*t2 + 
                  168*Power(t2,2) - 481*Power(t2,3) + 50*Power(t2,4) + 
                  4*Power(t1,3)*(-62 + 46*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*
                   (567 - 973*t2 + 587*Power(t2,2) - 37*Power(t2,3)) + 
                  t1*(90 - 214*t2 + 569*Power(t2,2) - 397*Power(t2,3) + 
                     27*Power(t2,4)))) + 
            s*(-8*Power(s2,6)*(-1 + t2) + 
               Power(t1,5)*(-19 + 53*t2 - 32*Power(t2,2)) + 
               Power(s2,5)*(22 + 50*t1*(-1 + t2) - 45*t2 + 
                  21*Power(t2,2)) - 
               Power(t1,4)*(287 - 597*t2 + 297*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-261 + 259*t2 - 184*Power(t2,2) - 96*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,3)*(-242 + 407*t2 + 313*Power(t2,2) - 
                  491*Power(t2,3) + 18*Power(t2,4)) + 
               Power(t1,2)*(691 - 1734*t2 + 810*Power(t2,2) + 
                  90*Power(t2,3) + 153*Power(t2,4) - 10*Power(t2,5)) + 
               t1*(-410 + 2781*t2 - 3863*Power(t2,2) + 
                  1242*Power(t2,3) + 255*Power(t2,4) - 5*Power(t2,5)) + 
               Power(s2,3)*(-341 + 176*Power(t1,3)*(-1 + t2) + 318*t2 + 
                  592*Power(t2,2) - 650*Power(t2,3) + 76*Power(t2,4) + 
                  5*Power(t1,2)*(-5 - 60*t2 + 61*Power(t2,2)) + 
                  t1*(817 - 1769*t2 + 1111*Power(t2,2) - 159*Power(t2,3))\
) + Power(s2,4)*(-132*Power(t1,2)*(-1 + t2) + 
                  t1*(-2 + 119*t2 - 107*Power(t2,2)) + 
                  4*(-25 + 35*t2 - 13*Power(t2,2) + 3*Power(t2,3))) + 
               Power(s2,2)*(523 - 116*Power(t1,4)*(-1 + t2) + 130*t2 - 
                  2003*Power(t2,2) + 1274*Power(t2,3) + 
                  104*Power(t2,4) - 28*Power(t2,5) + 
                  Power(t1,3)*(-51 + 476*t2 - 405*Power(t2,2)) + 
                  7*Power(t1,2)*
                   (-242 + 551*t2 - 340*Power(t2,2) + 31*Power(t2,3)) + 
                  t1*(138 + 529*t2 - 1720*Power(t2,2) + 
                     1121*Power(t2,3) - 53*Power(t2,4))) + 
               s2*(468 + 30*Power(t1,5)*(-1 + t2) - 2420*t2 + 
                  3034*Power(t2,2) - 869*Power(t2,3) - 248*Power(t2,4) + 
                  35*Power(t2,5) + 
                  Power(t1,4)*(75 - 303*t2 + 218*Power(t2,2)) + 
                  Power(t1,3)*
                   (1264 - 2825*t2 + 1618*Power(t2,2) - 57*Power(t2,3)) + 
                  Power(t1,2)*
                   (430 - 1145*t2 + 650*Power(t2,2) + 83*Power(t2,3) - 
                     33*Power(t2,4)) + 
                  t1*(-1216 + 1397*t2 + 1408*Power(t2,2) - 
                     1171*Power(t2,3) - 450*Power(t2,4) + 32*Power(t2,5)))\
)) + Power(s1,2)*(-1 + s + t2)*
          (-34 - 31*t1 + 256*Power(t1,2) - 132*Power(t1,3) - 
            316*Power(t1,4) + 75*Power(t1,5) + 6*Power(t1,6) + 
            8*Power(s,7)*Power(t1 - t2,2) - 
            8*Power(s2,6)*(-2 + t2)*Power(-1 + t2,2) - 32*t2 + 
            480*t1*t2 - 439*Power(t1,2)*t2 + 995*Power(t1,3)*t2 + 
            755*Power(t1,4)*t2 - 211*Power(t1,5)*t2 - 12*Power(t1,6)*t2 + 
            215*Power(t2,2) - 1605*t1*Power(t2,2) - 
            646*Power(t1,2)*Power(t2,2) - 2074*Power(t1,3)*Power(t2,2) - 
            477*Power(t1,4)*Power(t2,2) + 197*Power(t1,5)*Power(t2,2) + 
            6*Power(t1,6)*Power(t2,2) + 93*Power(t2,3) + 
            2330*t1*Power(t2,3) + 2111*Power(t1,2)*Power(t2,3) + 
            1664*Power(t1,3)*Power(t2,3) - 56*Power(t1,4)*Power(t2,3) - 
            60*Power(t1,5)*Power(t2,3) - 691*Power(t2,4) - 
            1782*t1*Power(t2,4) - 1823*Power(t1,2)*Power(t2,4) - 
            418*Power(t1,3)*Power(t2,4) + 92*Power(t1,4)*Power(t2,4) + 
            591*Power(t2,5) + 780*t1*Power(t2,5) + 
            545*Power(t1,2)*Power(t2,5) - 34*Power(t1,3)*Power(t2,5) - 
            126*Power(t2,6) - 172*t1*Power(t2,6) - 
            4*Power(t1,2)*Power(t2,6) - 16*Power(t2,7) + 
            Power(s2,5)*(22 - 55*t2 + 56*Power(t2,2) - 33*Power(t2,3) + 
               9*Power(t2,4) + 3*t1*Power(-1 + t2,2)*(-28 + 9*t2)) + 
            Power(s,6)*(-26 + 20*Power(t1,3) + 
               Power(t1,2)*(52 - 19*t2) - 16*Power(s2,2)*(-1 + t2) + 
               14*t2 + 10*Power(t2,2) + 21*Power(t2,3) + 
               t1*(24 - 74*t2 - 22*Power(t2,2)) - 
               s2*(2 + 22*Power(t1,2) + t1*(69 - 59*t2) - 71*t2 + 
                  37*Power(t2,2))) + 
            Power(s2,4)*(-134 + 464*t2 - 618*Power(t2,2) + 
               382*Power(t2,3) - 107*Power(t2,4) + 11*Power(t2,5) - 
               3*Power(t1,2)*Power(-1 + t2,2)*(-63 + 11*t2) + 
               t1*(-111 + 261*t2 - 235*Power(t2,2) + 124*Power(t2,3) - 
                  34*Power(t2,4))) + 
            Power(s2,3)*(-28 - 63*t2 + 173*Power(t2,2) + 
               29*Power(t2,3) - 196*Power(t2,4) + 84*Power(t2,5) + 
               Power(t1,3)*Power(-1 + t2,2)*(-229 + 17*t2) + 
               Power(t1,2)*(133 - 252*t2 + 164*Power(t2,2) - 
                  96*Power(t2,3) + 41*Power(t2,4)) - 
               2*t1*(-273 + 798*t2 - 841*Power(t2,2) + 373*Power(t2,3) - 
                  72*Power(t2,4) + 11*Power(t2,5))) + 
            Power(s2,2)*(12 - 
               3*Power(t1,4)*(-51 + t2)*Power(-1 + t2,2) - 266*t2 + 
               1001*Power(t2,2) - 1511*Power(t2,3) + 1052*Power(t2,4) - 
               328*Power(t2,5) + 40*Power(t2,6) - 
               2*Power(t1,3)*
                (-27 + 135*t2 - 175*Power(t2,2) + 54*Power(t2,3) + 
                  8*Power(t2,4)) + 
               t1*(452 - 877*t2 + 402*Power(t2,2) - 124*Power(t2,3) + 
                  320*Power(t2,4) - 170*Power(t2,5)) + 
               Power(t1,2)*(-1051 + 2732*t2 - 2250*Power(t2,2) + 
                  465*Power(t2,3) + 81*Power(t2,4) + 11*Power(t2,5))) + 
            s2*(-51*Power(t1,5)*Power(-1 + t2,2) + 
               Power(t1,4)*(-173 + 527*t2 - 532*Power(t2,2) + 
                  173*Power(t2,3)) + 
               Power(t1,3)*(955 - 2355*t2 + 1663*Power(t2,2) - 
                  45*Power(t2,3) - 210*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (146 - 46*t2 - 215*Power(t2,2) - 6*Power(t2,3) + 
                  140*Power(t2,4)) + 
               Power(t1,2)*(-261 - 197*t2 + 1771*Power(t2,2) - 
                  1847*Power(t2,3) + 447*Power(t2,4) + 84*Power(t2,5)) + 
               t1*(-528 + 1681*t2 - 1736*Power(t2,2) + 283*Power(t2,3) + 
                  588*Power(t2,4) - 296*Power(t2,5) + 8*Power(t2,6))) + 
            Power(s,5)*(-10 + 16*Power(t1,4) + 
               56*Power(s2,3)*(-1 + t2) + 99*t2 - 133*Power(t2,2) + 
               61*Power(t2,3) + 18*Power(t2,4) + 
               9*Power(t1,3)*(3 + 2*t2) + 
               Power(t1,2)*(89 + 160*t2 - 66*Power(t2,2)) + 
               Power(s2,2)*(7 + 15*Power(t1,2) + t1*(268 - 126*t2) - 
                  186*t2 + 22*Power(t2,2)) + 
               t1*(-201 + 232*t2 - 308*Power(t2,2) + 14*Power(t2,3)) + 
               s2*(103 - 31*Power(t1,3) - 56*t2 + 242*Power(t2,2) - 
                  100*Power(t2,3) + Power(t1,2)*(-230 + 39*t2) + 
                  t1*(-128 + 53*t2 + 92*Power(t2,2)))) + 
            Power(s,4)*(155 + 4*Power(t1,5) - 72*Power(s2,4)*(-1 + t2) + 
               37*t2 + 296*Power(t2,2) - 417*Power(t2,3) + 
               99*Power(t2,4) + 5*Power(t2,5) + 
               Power(t1,4)*(-8 + 29*t2) + 
               Power(t1,3)*(67 + 77*t2 - 30*Power(t2,2)) + 
               Power(t1,2)*(-866 + 778*t2 + 155*Power(t2,2) - 
                  38*Power(t2,3)) + 
               t1*(-228 + 357*t2 + 12*Power(t2,2) - 443*Power(t2,3) + 
                  30*Power(t2,4)) + 
               Power(s2,3)*(114 + 4*Power(t1,2) - 5*t2 + 
                  92*Power(t2,2) + t1*(-373 + 166*t2)) - 
               Power(s2,2)*(384 + 4*Power(t1,3) - 970*t2 + 
                  884*Power(t2,2) - 140*Power(t2,3) + 
                  Power(t1,2)*(-493 + 76*t2) + 
                  3*t1*(149 - 173*t2 + 88*Power(t2,2))) - 
               s2*(96 + 4*Power(t1,4) + 968*t2 - 881*Power(t2,2) - 
                  172*Power(t2,3) + 89*Power(t2,4) + 
                  Power(t1,3)*(184 + 47*t2) - 
                  5*Power(t1,2)*(51 - 115*t2 + 35*Power(t2,2)) + 
                  t1*(-1604 + 2052*t2 - 831*Power(t2,2) + 35*Power(t2,3))\
)) + Power(s,3)*(-208 + 40*Power(s2,5)*(-1 + t2) - 637*t2 + 
               1655*Power(t2,2) - 161*Power(t2,3) - 530*Power(t2,4) + 
               64*Power(t2,5) + Power(t1,5)*(-1 + 8*t2) + 
               Power(t1,4)*(213 - 80*t2 + 10*Power(t2,2)) + 
               Power(t1,3)*(-821 + 344*t2 + 152*Power(t2,2) - 
                  34*Power(t2,3)) + 
               Power(t1,2)*(636 - 1413*t2 + 943*Power(t2,2) + 
                  58*Power(t2,3) + 6*Power(t2,4)) + 
               t1*(1459 - 2890*t2 + 1921*Power(t2,2) - 
                  330*Power(t2,3) - 313*Power(t2,4) + 10*Power(t2,5)) - 
               Power(s2,4)*(233 + 5*Power(t1,2) - 292*t2 + 
                  158*Power(t2,2) + 2*t1*(-109 + 55*t2)) + 
               Power(s2,3)*(371 + 15*Power(t1,3) - 1157*t2 + 
                  709*Power(t2,2) - 22*Power(t2,3) + 
                  Power(t1,2)*(-446 + 99*t2) + 
                  t1*(1150 - 1187*t2 + 394*Power(t2,2))) + 
               s2*(-314 + 5*Power(t1,5) + 1918*t2 - 4006*Power(t2,2) + 
                  2296*Power(t2,3) - 115*Power(t2,4) - 26*Power(t2,5) - 
                  Power(t1,4)*(128 + 17*t2) + 
                  Power(t1,3)*(124 - 261*t2 + 19*Power(t2,2)) + 
                  Power(t1,2)*
                   (2235 - 1327*t2 - 453*Power(t2,2) + 129*Power(t2,3)) \
+ t1*(-1858 + 3303*t2 - 2941*Power(t2,2) + 1323*Power(t2,3) - 
                     110*Power(t2,4))) - 
               Power(s2,2)*(15*Power(t1,4) + 
                  Power(t1,3)*(-397 + 20*t2) + 
                  Power(t1,2)*(1254 - 1236*t2 + 265*Power(t2,2)) + 
                  t1*(1817 - 2230*t2 + 467*Power(t2,2) + 
                     96*Power(t2,3)) - 
                  2*(263 - 217*t2 + 577*Power(t2,2) - 556*Power(t2,3) + 
                     75*Power(t2,4)))) - 
            s*(72 - 12*Power(t1,6)*(-1 + t2) - 508*t2 + 14*Power(t2,2) + 
               1867*Power(t2,3) - 1876*Power(t2,4) + 267*Power(t2,5) + 
               164*Power(t2,6) + 
               8*Power(s2,6)*(3 - 5*t2 + 2*Power(t2,2)) + 
               Power(t1,5)*(129 - 238*t2 + 110*Power(t2,2)) - 
               Power(t1,4)*(939 - 1671*t2 + 668*Power(t2,2) + 
                  67*Power(t2,3)) + 
               Power(t1,3)*(113 + 1206*t2 - 2743*Power(t2,2) + 
                  1530*Power(t2,3) - 113*Power(t2,4)) + 
               Power(t1,2)*(883 - 1823*t2 - 138*Power(t2,2) + 
                  2423*Power(t2,3) - 1434*Power(t2,4) + 86*Power(t2,5)) \
+ t1*(-542 + 2368*t2 - 3679*Power(t2,2) + 2642*Power(t2,3) - 
                  1123*Power(t2,4) + 326*Power(t2,5) + 8*Power(t2,6)) + 
               Power(s2,5)*(110 - 255*t2 + 205*Power(t2,2) - 
                  58*Power(t2,3) - 2*t1*(64 - 91*t2 + 27*Power(t2,2))) + 
               Power(s2,4)*(-229 + 719*t2 - 716*Power(t2,2) + 
                  231*Power(t2,3) - 8*Power(t2,4) + 
                  Power(t1,2)*(303 - 374*t2 + 71*Power(t2,2)) + 
                  t1*(-558 + 1142*t2 - 769*Power(t2,2) + 
                     178*Power(t2,3))) - 
               Power(s2,3)*(349 - 1046*t2 + 1663*Power(t2,2) - 
                  1501*Power(t2,3) + 569*Power(t2,4) - 38*Power(t2,5) + 
                  Power(t1,3)*(393 - 442*t2 + 49*Power(t2,2)) + 
                  Power(t1,2)*
                   (-916 + 1699*t2 - 964*Power(t2,2) + 173*Power(t2,3)) \
+ t1*(-900 + 2107*t2 - 1476*Power(t2,2) + 239*Power(t2,3) + 
                     18*Power(t2,4))) + 
               Power(s2,2)*(-23 - 1596*t2 + 4379*Power(t2,2) - 
                  3852*Power(t2,3) + 1113*Power(t2,4) - 
                  24*Power(t2,5) + 
                  Power(t1,4)*(281 - 302*t2 + 21*Power(t2,2)) + 
                  Power(t1,3)*
                   (-469 + 754*t2 - 331*Power(t2,2) + 44*Power(t2,3)) + 
                  Power(t1,2)*
                   (-2215 + 4234*t2 - 2013*Power(t2,2) - 
                     70*Power(t2,3) + 46*Power(t2,4)) + 
                  t1*(2386 - 5242*t2 + 4388*Power(t2,2) - 
                     2236*Power(t2,3) + 743*Power(t2,4) - 54*Power(t2,5)\
)) + s2*(481 - 480*t2 - 604*Power(t2,2) + 19*Power(t2,3) + 
                  1331*Power(t2,4) - 803*Power(t2,5) + 56*Power(t2,6) + 
                  Power(t1,5)*(-99 + 104*t2 - 5*Power(t2,2)) + 
                  Power(t1,4)*
                   (-128 + 296*t2 - 179*Power(t2,2) + 9*Power(t2,3)) + 
                  Power(t1,3)*
                   (2483 - 4517*t2 + 1921*Power(t2,2) + 
                     145*Power(t2,3) - 20*Power(t2,4)) + 
                  t1*(-1332 + 4394*t2 - 4595*Power(t2,2) + 
                     968*Power(t2,3) + 845*Power(t2,4) - 274*Power(t2,5)\
) + Power(t1,2)*(-2053 + 2623*t2 + 589*Power(t2,2) - 1232*Power(t2,3) + 
                     75*Power(t2,4) + 16*Power(t2,5)))) - 
            Power(s,2)*(-195 - 6*Power(t1,6) + 8*Power(s2,6)*(-1 + t2) - 
               119*t2 + 2467*Power(t2,2) - 3011*Power(t2,3) + 
               427*Power(t2,4) + 415*Power(t2,5) - 16*Power(t2,6) + 
               Power(s2,5)*(-138 + t1*(44 - 27*t2) + 212*t2 - 
                  89*Power(t2,2)) + 
               Power(t1,5)*(-41 + 51*t2 - 4*Power(t2,2)) + 
               Power(t1,4)*(872 - 930*t2 + 97*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,3)*(-948 + 322*t2 + 807*Power(t2,2) - 
                  249*Power(t2,3) + 6*Power(t2,4)) + 
               Power(t1,2)*(-727 + 1127*t2 + 609*Power(t2,2) - 
                  1101*Power(t2,3) + 71*Power(t2,4) - 5*Power(t2,5)) + 
               t1*(1565 - 4257*t2 + 3821*Power(t2,2) - 
                  1456*Power(t2,3) + 260*Power(t2,4) + 112*Power(t2,5)) + 
               Power(s2,4)*(-38 - 10*t2 - 96*Power(t2,2) + 
                  89*Power(t2,3) + Power(t1,2)*(-119 + 43*t2) + 
                  t1*(715 - 863*t2 + 254*Power(t2,2))) + 
               Power(s2,3)*(746 + Power(t1,3)*(179 - 47*t2) - 2298*t2 + 
                  2650*Power(t2,2) - 1143*Power(t2,3) + 96*Power(t2,4) + 
                  Power(t1,2)*(-1325 + 1314*t2 - 227*Power(t2,2)) + 
                  t1*(311 - 622*t2 + 719*Power(t2,2) - 268*Power(t2,3))) \
+ Power(s2,2)*(181 + 1895*t2 - 3105*Power(t2,2) + 640*Power(t2,3) + 
                  446*Power(t2,4) - 48*Power(t2,5) + 
                  11*Power(t1,4)*(-13 + 3*t2) + 
                  4*Power(t1,3)*(254 - 209*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*
                   (586 - 156*t2 - 732*Power(t2,2) + 231*Power(t2,3)) + 
                  t1*(-3918 + 7248*t2 - 4717*Power(t2,2) + 
                     1291*Power(t2,3) - 96*Power(t2,4))) + 
               s2*(-644 + Power(t1,5)*(53 - 10*t2) + 1085*t2 - 
                  2363*Power(t2,2) + 3772*Power(t2,3) - 
                  1999*Power(t2,4) + 172*Power(t2,5) + 
                  Power(t1,4)*(-227 + 122*t2 + 22*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1731 + 1718*t2 + 12*Power(t2,2) - 55*Power(t2,3)) + 
                  Power(t1,2)*
                   (4018 - 4954*t2 + 875*Power(t2,2) + 267*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(391 - 1818*t2 + 751*Power(t2,2) + 
                     1491*Power(t2,3) - 880*Power(t2,4) + 42*Power(t2,5)))\
)) + Power(s1,4)*(Power(s,7)*(-14 + 22*Power(t1,2) + 
               s2*(10 - 3*t1 - 5*t2) + 28*t2 + 5*Power(t2,2) - 
               t1*(12 + 31*t2)) + 
            Power(s,6)*(-96 + 62*Power(t1,3) + 
               Power(t1,2)*(144 - 26*t2) + 
               Power(s2,2)*(-19 + 14*t1 - 2*t2) + 138*t2 + 
               41*Power(t2,2) + 16*Power(t2,3) - 
               t1*(107 + 119*t2 + 76*Power(t2,2)) + 
               s2*(99 - 98*Power(t1,2) + 40*t2 - 57*Power(t2,2) + 
                  t1*(-75 + 121*t2))) + 
            Power(s,5)*(149 + 58*Power(t1,4) - 318*t2 + 
               334*Power(t2,2) + 22*Power(t2,3) + 17*Power(t2,4) + 
               Power(s2,3)*(-2 - 24*t1 + 36*t2) + 
               2*Power(t1,3)*(74 + 45*t2) - 
               2*Power(t1,2)*(65 - 304*t2 + 91*Power(t2,2)) + 
               Power(s2,2)*(-176 + 163*Power(t1,2) + 
                  t1*(326 - 215*t2) - 278*t2 + 96*Power(t2,2)) - 
               t1*(260 - 81*t2 + 634*Power(t2,2) + 43*Power(t2,3)) + 
               s2*(87 - 197*Power(t1,3) + 225*t2 + 201*Power(t2,2) - 
                  139*Power(t2,3) + Power(t1,2)*(-471 + 14*t2) + 
                  t1*(145 + 3*t2 + 266*Power(t2,2)))) + 
            Power(s,4)*(153 + 18*Power(t1,5) + 
               Power(s2,4)*(21 + 18*t1 - 46*t2) + 139*t2 - 
               29*Power(t2,2) + 86*Power(t2,3) + 17*Power(t2,4) + 
               6*Power(t2,5) + 2*Power(t1,4)*(-28 + 67*t2) + 
               Power(t1,3)*(-481 + 1007*t2 - 72*Power(t2,2)) + 
               Power(t1,2)*(-1241 + 1124*t2 + 308*Power(t2,2) - 
                  184*Power(t2,3)) + 
               t1*(695 - 657*t2 - 37*Power(t2,2) - 986*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,3)*(233 - 120*Power(t1,2) + 165*t2 + 
                  4*Power(t2,2) + t1*(-373 + 175*t2)) + 
               Power(s2,2)*(-43 + 204*Power(t1,3) + 141*t2 - 
                  1150*Power(t2,2) + 282*Power(t2,3) + 
                  3*Power(t1,2)*(165 + 37*t2) + 
                  t1*(-876 + 1142*t2 - 523*Power(t2,2))) + 
               s2*(-698 - 120*Power(t1,4) - 374*t2 + 1230*Power(t2,2) + 
                  98*Power(t2,3) - 127*Power(t2,4) - 
                  Power(t1,3)*(87 + 374*t2) + 
                  Power(t1,2)*(1250 - 2441*t2 + 498*Power(t2,2)) + 
                  t1*(1473 - 1941*t2 + 1718*Power(t2,2) + 73*Power(t2,3))\
)) - Power(s,3)*(456 + Power(t1,5)*(72 - 49*t2) + 
               Power(s2,5)*(10 + 5*t1 - 17*t2) - 179*t2 - 
               439*Power(t2,2) - 245*Power(t2,3) + 194*Power(t2,4) + 
               Power(t2,5) + Power(t1,4)*
                (148 - 233*t2 - 73*Power(t2,2)) + 
               Power(t1,3)*(626 + 717*t2 - 1511*Power(t2,2) + 
                  162*Power(t2,3)) + 
               2*Power(t1,2)*
                (-649 + 2060*t2 - 1662*Power(t2,2) + 333*Power(t2,3) + 
                  18*Power(t2,4)) - 
               t1*(294 + 1146*t2 + 144*Power(t2,2) - 1237*Power(t2,3) - 
                  525*Power(t2,4) + 16*Power(t2,5)) + 
               Power(s2,4)*(254 - 33*Power(t1,2) - 176*t2 + 
                  93*Power(t2,2) + 2*t1*(-70 + 17*t2)) + 
               Power(s2,3)*(-99 + 69*Power(t1,3) + 794*t2 - 
                  1174*Power(t2,2) + 172*Power(t2,3) + 
                  Power(t1,2)*(131 + 202*t2) + 
                  t1*(-1599 + 1935*t2 - 535*Power(t2,2))) + 
               Power(s2,2)*(-602 - 59*Power(t1,4) + 
                  Power(t1,3)*(190 - 487*t2) - 1014*t2 + 
                  695*Power(t2,2) + 1217*Power(t2,3) - 
                  268*Power(t2,4) + 
                  Power(t1,2)*(2568 - 3361*t2 + 493*Power(t2,2)) + 
                  t1*(1897 - 2018*t2 + 653*Power(t2,2) + 
                     225*Power(t2,3))) + 
               s2*(-506 + 18*Power(t1,5) + 1281*t2 + 1876*Power(t2,2) - 
                  2655*Power(t2,3) + 266*Power(t2,4) + 40*Power(t2,5) + 
                  Power(t1,4)*(-263 + 317*t2) + 
                  Power(t1,3)*(-1371 + 1835*t2 + 22*Power(t2,2)) - 
                  Power(t1,2)*
                   (2274 + 13*t2 - 2393*Power(t2,2) + 510*Power(t2,3)) + 
                  t1*(2018 - 3237*t2 + 3549*Power(t2,2) - 
                     3003*Power(t2,3) + 153*Power(t2,4)))) + 
            Power(s,2)*(488 - 18*Power(t1,6) - 1237*t2 + 
               443*Power(t2,2) + 163*Power(t2,3) + 308*Power(t2,4) - 
               156*Power(t2,5) - 9*Power(t2,6) + 
               Power(t1,5)*(159 - 82*t2 + 44*Power(t2,2)) + 
               Power(t1,4)*(10 - 869*t2 + 638*Power(t2,2) - 
                  24*Power(t2,3)) + 
               Power(t1,3)*(1381 - 2485*t2 + 719*Power(t2,2) + 
                  635*Power(t2,3) - 58*Power(t2,4)) + 
               Power(t1,2)*(492 + 884*t2 - 3501*Power(t2,2) + 
                  2565*Power(t2,3) - 606*Power(t2,4) + 14*Power(t2,5)) - 
               t1*(1476 - 842*t2 - 2382*Power(t2,2) + 181*Power(t2,3) + 
                  1395*Power(t2,4) + 72*Power(t2,5)) + 
               Power(s2,5)*(128 - 147*t2 + 45*Power(t2,2) - 
                  2*t1*(3 + 8*t2)) + 
               Power(s2,4)*(121 - 11*t2 - 97*Power(t2,2) - 
                  26*Power(t2,3) + Power(t1,2)*(-25 + 103*t2) + 
                  t1*(-857 + 961*t2 - 185*Power(t2,2))) + 
               Power(s2,3)*(-488 + 1155*t2 - 2143*Power(t2,2) + 
                  1620*Power(t2,3) - 212*Power(t2,4) - 
                  3*Power(t1,3)*(-43 + 71*t2) + 
                  Power(t1,2)*(1455 - 1541*t2 + 70*Power(t2,2)) + 
                  t1*(-471 + 1970*t2 - 1712*Power(t2,2) + 
                     397*Power(t2,3))) + 
               Power(s2,2)*(-869 - 870*t2 + 5009*Power(t2,2) - 
                  3192*Power(t2,3) - 98*Power(t2,4) + 84*Power(t2,5) + 
                  Power(t1,4)*(-177 + 181*t2) + 
                  Power(t1,3)*(-692 + 705*t2 + 279*Power(t2,2)) + 
                  Power(t1,2)*
                   (767 - 5106*t2 + 4410*Power(t2,2) - 629*Power(t2,3)) \
+ t1*(3828 - 8261*t2 + 7534*Power(t2,2) - 3094*Power(t2,3) + 
                     189*Power(t2,4))) + 
               s2*(653 + Power(t1,5)*(97 - 55*t2) + 1377*t2 - 
                  3323*Power(t2,2) - 403*Power(t2,3) + 
                  1801*Power(t2,4) - 205*Power(t2,5) + 
                  Power(t1,4)*(-193 + 104*t2 - 253*Power(t2,2)) + 
                  Power(t1,3)*
                   (-427 + 4016*t2 - 3239*Power(t2,2) + 282*Power(t2,3)) \
+ Power(t1,2)*(-4713 + 9319*t2 - 5495*Power(t2,2) + 497*Power(t2,3) + 
                     72*Power(t2,4)) + 
                  t1*(274 + 575*t2 - 2398*Power(t2,2) + 
                     322*Power(t2,3) + 1399*Power(t2,4) - 84*Power(t2,5))\
)) + (-1 + t2)*(16*Power(s2,6)*(-1 + t2) - 18*Power(t1,6)*(-1 + t2) + 
               Power(t1,5)*(-109 + 62*t2 + 57*Power(t2,2)) + 
               Power(t1,4)*(72 + 252*t2 - 361*Power(t2,2) + 
                  31*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (68 + 42*t2 - 51*Power(t2,2) + 53*Power(t2,3)) + 
               Power(t1,3)*(-127 + 412*t2 - 887*Power(t2,2) + 
                  643*Power(t2,3) - 16*Power(t2,4)) - 
               Power(t1,2)*(386 - 813*t2 + 284*Power(t2,2) + 
                  19*Power(t2,3) + 124*Power(t2,4)) + 
               t1*(280 - 1123*t2 + 1044*Power(t2,2) + 19*Power(t2,3) - 
                  178*Power(t2,4) - 42*Power(t2,5)) + 
               Power(s2,5)*(-44 + 60*t2 - 37*Power(t2,2) + 
                  11*Power(t2,3) - 6*t1*(-16 + 15*t2 + Power(t2,2))) + 
               Power(s2,4)*(150 - 388*t2 + 411*Power(t2,2) - 
                  201*Power(t2,3) + 22*Power(t2,4) + 
                  Power(t1,2)*(-201 + 164*t2 + 37*Power(t2,2)) + 
                  t1*(227 - 419*t2 + 305*Power(t2,2) - 63*Power(t2,3))) \
+ Power(s2,3)*(130 + 303*t2 - 799*Power(t2,2) + 384*Power(t2,3) - 
                  43*Power(t2,4) + 
                  Power(t1,3)*(169 - 94*t2 - 75*Power(t2,2)) + 
                  Power(t1,2)*
                   (-289 + 770*t2 - 695*Power(t2,2) + 114*Power(t2,3)) \
+ t1*(-840 + 1587*t2 - 1154*Power(t2,2) + 483*Power(t2,3) - 
                     52*Power(t2,4))) + 
               Power(s2,2)*(-56 - 342*t2 + 661*Power(t2,2) + 
                  41*Power(t2,3) - 325*Power(t2,4) + 21*Power(t2,5) + 
                  21*Power(t1,4)*(-1 - 2*t2 + 3*Power(t2,2)) - 
                  Power(t1,3)*
                   (36 + 461*t2 - 680*Power(t2,2) + 83*Power(t2,3)) + 
                  t1*(-381 - 83*t2 + 453*Power(t2,2) + 
                     166*Power(t2,3) - 80*Power(t2,4)) + 
                  Power(t1,2)*
                   (1260 - 1601*t2 + 510*Power(t2,2) - 
                     243*Power(t2,3) + 38*Power(t2,4))) - 
               s2*(320 - 1004*t2 + 661*Power(t2,2) + 302*Power(t2,3) - 
                  271*Power(t2,4) - 8*Power(t2,5) + 
                  Power(t1,5)*(45 - 64*t2 + 19*Power(t2,2)) + 
                  Power(t1,4)*
                   (-251 + 12*t2 + 310*Power(t2,2) - 21*Power(t2,3)) + 
                  Power(t1,2)*
                   (-398 + 711*t2 - 1326*Power(t2,2) + 
                     1222*Power(t2,3) - 134*Power(t2,4)) + 
                  2*Power(t1,3)*
                   (321 - 75*t2 - 297*Power(t2,2) + 35*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  t1*(-494 + 607*t2 + 124*Power(t2,2) + 
                     325*Power(t2,3) - 580*Power(t2,4) + 18*Power(t2,5)))\
) + s*(16*Power(s2,6)*(-1 + t2) - 36*Power(t1,6)*(-1 + t2) + 
               Power(t1,5)*(-232 + 170*t2 + 47*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,4)*(148 + 628*t2 - 1129*Power(t2,2) + 
                  380*Power(t2,3) - 21*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (108 - 660*t2 + 247*Power(t2,2) - 181*Power(t2,3) + 
                  50*Power(t2,4)) + 
               Power(t1,3)*(-701 + 2303*t2 - 3190*Power(t2,2) + 
                  1618*Power(t2,3) - 33*Power(t2,4) + 4*Power(t2,5)) - 
               Power(t1,2)*(1019 - 2160*t2 + 636*Power(t2,2) + 
                  804*Power(t2,3) - 395*Power(t2,4) + 96*Power(t2,5)) + 
               t1*(1194 - 2967*t2 + 528*Power(t2,2) + 2406*Power(t2,3) - 
                  704*Power(t2,4) - 451*Power(t2,5) - 6*Power(t2,6)) + 
               Power(s2,5)*(-144 + 292*t2 - 185*Power(t2,2) + 
                  39*Power(t2,3) + t1*(107 - 90*t2 - 17*Power(t2,2))) + 
               Power(s2,4)*(202 - 735*t2 + 971*Power(t2,2) - 
                  475*Power(t2,3) + 43*Power(t2,4) + 
                  Power(t1,2)*(-209 + 102*t2 + 107*Power(t2,2)) + 
                  t1*(836 - 1839*t2 + 1189*Power(t2,2) - 196*Power(t2,3))\
) + Power(s2,3)*(378 - 56*t2 - 246*Power(t2,2) - 575*Power(t2,3) + 
                  570*Power(t2,4) - 72*Power(t2,5) + 
                  Power(t1,3)*(109 + 110*t2 - 219*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1313 + 3286*t2 - 2219*Power(t2,2) + 
                     266*Power(t2,3)) + 
                  t1*(-1331 + 2505*t2 - 1592*Power(t2,2) + 
                     385*Power(t2,3) + 9*Power(t2,4))) + 
               s2*(-1025 + 1628*t2 + 1727*Power(t2,2) - 
                  3769*Power(t2,3) + 1158*Power(t2,4) + 
                  293*Power(t2,5) - 12*Power(t2,6) - 
                  4*Power(t1,5)*(31 - 45*t2 + 14*Power(t2,2)) + 
                  Power(t1,4)*
                   (391 + 144*t2 - 490*Power(t2,2) - 35*Power(t2,3)) + 
                  Power(t1,3)*
                   (-1062 - 775*t2 + 3247*Power(t2,2) - 
                     1553*Power(t2,3) + 119*Power(t2,4)) + 
                  Power(t1,2)*
                   (2426 - 6999*t2 + 9080*Power(t2,2) - 
                     5512*Power(t2,3) + 1054*Power(t2,4) - 
                     52*Power(t2,5)) + 
                  2*t1*(397 - 999*t2 + 1759*Power(t2,2) - 
                     2308*Power(t2,3) + 1142*Power(t2,4) + 9*Power(t2,5))\
) + Power(s2,2)*(401 - 820*t2 - 1523*Power(t2,2) + 4143*Power(t2,3) - 
                  2450*Power(t2,4) + 249*Power(t2,5) + 
                  Power(t1,4)*(97 - 282*t2 + 185*Power(t2,2)) + 
                  Power(t1,3)*
                   (462 - 2053*t2 + 1658*Power(t2,2) - 87*Power(t2,3)) - 
                  3*Power(t1,2)*
                   (-681 + 541*t2 + 499*Power(t2,2) - 421*Power(t2,3) + 
                     50*Power(t2,4)) + 
                  t1*(-2046 + 4607*t2 - 5639*Power(t2,2) + 
                     4666*Power(t2,3) - 1705*Power(t2,4) + 120*Power(t2,5)\
)))) + Power(s1,3)*(-4*Power(s,8)*Power(t1 - t2,2) + 
            Power(s,7)*(34 - 13*Power(t1,3) + 8*Power(s2,2)*(-1 + t2) - 
               48*t2 - 17*Power(t2,2) - 12*Power(t2,3) + 
               7*Power(t1,2)*(-9 + 2*t2) + 
               t1*(6 + 92*t2 + 11*Power(t2,2)) + 
               s2*(-14 + 19*Power(t1,2) + t1*(39 - 45*t2) - 29*t2 + 
                  26*Power(t2,2))) - 
            Power(s,6)*(-44 + 14*Power(t1,4) + 32*Power(s2,3)*(-1 + t2) + 
               18*t2 + 10*Power(t2,2) + 91*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,3)*(96 + 11*t2) + 
               Power(t1,2)*(109 + 116*t2 - 52*Power(t2,2)) + 
               t1*(-156 + 119*t2 - 375*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s2,2)*(-23 + 33*Power(t1,2) - 120*t2 + 
                  40*Power(t2,2) - 18*t1*(-10 + 7*t2)) + 
               s2*(129 - 47*Power(t1,3) + 78*t2 + 64*Power(t2,2) - 
                  78*Power(t2,3) + Power(t1,2)*(-251 + 60*t2) + 
                  t1*(-164 + 176*t2 + 65*Power(t2,2)))) + 
            Power(s,5)*(-301 - 5*Power(t1,5) + 48*Power(s2,4)*(-1 + t2) + 
               251*t2 - 244*Power(t2,2) + 207*Power(t2,3) - 
               153*Power(t2,4) - 4*Power(t2,5) - 
               Power(t1,4)*(17 + 32*t2) + 
               Power(t1,3)*(94 - 374*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(770 - 1407*t2 + 253*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(236 - 250*t2 + 450*Power(t2,2) + 471*Power(t2,3) - 
                  31*Power(t2,4)) + 
               Power(s2,3)*(-69 + 25*Power(t1,2) - 76*t2 - 
                  20*Power(t2,2) - 4*t1*(-76 + 47*t2)) - 
               Power(s2,2)*(-310 + 55*Power(t1,3) + 
                  Power(t1,2)*(424 - 126*t2) + 180*t2 - 
                  607*Power(t2,2) + 168*Power(t2,3) + 
                  t1*(82 + 314*t2 - 246*Power(t2,2))) + 
               s2*(35*Power(t1,4) + Power(t1,3)*(185 + 46*t2) + 
                  Power(t1,2)*(-12 + 863*t2 - 234*Power(t2,2)) + 
                  t1*(-1187 + 1862*t2 - 1335*Power(t2,2) + 
                     75*Power(t2,3)) + 
                  2*(89 + 156*t2 - 547*Power(t2,2) + 100*Power(t2,3) + 
                     39*Power(t2,4)))) - 
            Power(s,4)*(-345 + 32*Power(s2,5)*(-1 + t2) + 491*t2 + 
               642*Power(t2,2) - 271*Power(t2,3) - 241*Power(t2,4) + 
               101*Power(t2,5) + Power(t1,5)*(-34 + 15*t2) + 
               Power(s2,4)*(-157 + 7*Power(t1,2) + t1*(224 - 138*t2) + 
                  148*t2 - 100*Power(t2,2)) + 
               Power(t1,4)*(5 + 160*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(-691 + 378*t2 + 377*Power(t2,2) - 
                  56*Power(t2,3)) + 
               Power(t1,2)*(-347 - 2101*t2 + 3110*Power(t2,2) - 
                  677*Power(t2,3) + 16*Power(t2,4)) + 
               t1*(1366 - 2145*t2 + 2524*Power(t2,2) - 
                  2072*Power(t2,3) - 167*Power(t2,4) + 13*Power(t2,5)) + 
               Power(s2,3)*(436 - 21*Power(t1,3) - 825*t2 + 
                  923*Power(t2,2) - 132*Power(t2,3) + 
                  Power(t1,2)*(-357 + 128*t2) + 
                  t1*(639 - 1213*t2 + 458*Power(t2,2))) + 
               Power(s2,2)*(422 + 21*Power(t1,4) + 858*t2 - 
                  1034*Power(t2,2) - 575*Power(t2,3) + 184*Power(t2,4) + 
                  Power(t1,3)*(136 + 45*t2) + 
                  Power(t1,2)*(-808 + 2035*t2 - 498*Power(t2,2)) + 
                  t1*(-2933 + 3647*t2 - 1487*Power(t2,2) + 
                     18*Power(t2,3))) - 
               s2*(467 + 7*Power(t1,5) + 323*t2 + 1729*Power(t2,2) - 
                  2788*Power(t2,3) + 582*Power(t2,4) + 26*Power(t2,5) + 
                  Power(t1,4)*(-63 + 82*t2) + 
                  Power(t1,3)*(-321 + 1130*t2 - 128*Power(t2,2)) + 
                  Power(t1,2)*
                   (-3102 + 2861*t2 + 114*Power(t2,2) - 152*Power(t2,3)) \
+ t1*(-135 - 1075*t2 + 2405*Power(t2,2) - 2013*Power(t2,3) + 
                     165*Power(t2,4)))) + 
            (-1 + t2)*(6*Power(t1,6)*(4 - 7*t2 + 3*Power(t2,2)) + 
               8*Power(s2,6)*(-1 + 4*t2 - 4*Power(t2,2) + Power(t2,3)) - 
               Power(t1,5)*(70 - 119*t2 + 26*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,4)*(-221 + 482*t2 - 383*Power(t2,2) + 
                  126*Power(t2,3) + 8*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (84 - 166*t2 - 29*Power(t2,2) - 31*Power(t2,3) + 
                  42*Power(t2,4)) + 
               Power(t1,3)*(97 + 456*t2 - 1228*Power(t2,2) + 
                  918*Power(t2,3) - 256*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t1,2)*(-15 + 592*t2 - 2022*Power(t2,2) + 
                  2318*Power(t2,3) - 955*Power(t2,4) + 82*Power(t2,5)) + 
               t1*(-319 + 615*t2 + 3*Power(t2,2) - 229*Power(t2,3) - 
                  364*Power(t2,4) + 274*Power(t2,5) + 20*Power(t2,6)) + 
               Power(s2,5)*(37 - 95*t2 + 120*Power(t2,2) - 
                  65*Power(t2,3) + 10*Power(t2,4) - 
                  3*t1*(-19 + 70*t2 - 64*Power(t2,2) + 13*Power(t2,3))) + 
               Power(s2,4)*(50 - 161*t2 + 268*Power(t2,2) - 
                  209*Power(t2,3) + 64*Power(t2,4) + 
                  Power(t1,2)*
                   (-118 + 461*t2 - 412*Power(t2,2) + 69*Power(t2,3)) + 
                  t1*(-211 + 422*t2 - 412*Power(t2,2) + 
                     197*Power(t2,3) - 31*Power(t2,4))) + 
               Power(s2,3)*(-79 + 482*t2 - 1259*Power(t2,2) + 
                  1349*Power(t2,3) - 546*Power(t2,4) + 62*Power(t2,5) + 
                  Power(t1,3)*
                   (68 - 401*t2 + 386*Power(t2,2) - 53*Power(t2,3)) + 
                  t1*(-426 + 1332*t2 - 1650*Power(t2,2) + 
                     889*Power(t2,3) - 193*Power(t2,4)) + 
                  Power(t1,2)*
                   (501 - 866*t2 + 589*Power(t2,2) - 186*Power(t2,3) + 
                     32*Power(t2,4))) + 
               Power(s2,2)*(-216 + 7*t2 + 1038*Power(t2,2) - 
                  1254*Power(t2,3) + 422*Power(t2,4) + 3*Power(t2,5) + 
                  3*Power(t1,4)*
                   (18 + 19*t2 - 42*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,3)*
                   (-587 + 965*t2 - 448*Power(t2,2) + 11*Power(t2,3) - 
                     11*Power(t2,4)) + 
                  Power(t1,2)*
                   (425 - 1486*t2 + 1813*Power(t2,2) - 
                     836*Power(t2,3) + 156*Power(t2,4)) + 
                  t1*(775 - 2041*t2 + 2726*Power(t2,2) - 
                     2115*Power(t2,3) + 690*Power(t2,4) - 62*Power(t2,5))\
) + s2*(175 - 48*t2 - 610*Power(t2,2) + 292*Power(t2,3) + 
                  523*Power(t2,4) - 324*Power(t2,5) - 8*Power(t2,6) + 
                  Power(t1,5)*(-77 + 103*t2 - 26*Power(t2,2)) + 
                  Power(t1,4)*
                   (330 - 545*t2 + 177*Power(t2,2) + 73*Power(t2,3)) + 
                  Power(t1,3)*
                   (172 - 167*t2 - 48*Power(t2,2) + 30*Power(t2,3) - 
                     35*Power(t2,4)) + 
                  t1*(227 - 601*t2 + 999*Power(t2,2) - 
                     1189*Power(t2,3) + 766*Power(t2,4) - 202*Power(t2,5)\
) + Power(t1,2)*(-771 + 984*t2 + 2*Power(t2,2) - 365*Power(t2,3) + 
                     181*Power(t2,4) - 4*Power(t2,5)))) + 
            Power(s,3)*(-122 + 18*Power(t1,6) + 8*Power(s2,6)*(-1 + t2) + 
               1373*t2 - 213*Power(t2,2) - 2393*Power(t2,3) + 
               1139*Power(t2,4) + 106*Power(t2,5) - 22*Power(t2,6) + 
               Power(s2,5)*(-129 + t1*(61 - 39*t2) + 189*t2 - 
                  86*Power(t2,2)) - 
               3*Power(t1,5)*(58 - 20*t2 + 5*Power(t2,2)) + 
               Power(t1,4)*(254 + 153*t2 - 304*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,3)*(-741 + 2542*t2 - 1645*Power(t2,2) + 
                  13*Power(t2,3) + 13*Power(t2,4)) + 
               t1*(1073 - 3284*t2 + 3125*Power(t2,2) - 
                  3156*Power(t2,3) + 2365*Power(t2,4) - 39*Power(t2,5)) + 
               Power(t1,2)*(-2620 + 4061*t2 + 187*Power(t2,2) - 
                  2255*Power(t2,3) + 454*Power(t2,4) - 14*Power(t2,5)) + 
               Power(s2,3)*(716 - 1113*t2 + 1880*Power(t2,2) - 
                  1491*Power(t2,3) + 196*Power(t2,4) + 
                  Power(t1,3)*(59 + 10*t2) + 
                  Power(t1,2)*(-1159 + 2029*t2 - 502*Power(t2,2)) + 
                  t1*(-1479 + 780*t2 + 386*Power(t2,2) - 246*Power(t2,3))\
) + Power(s2,4)*(Power(t1,2)*(-121 + 48*t2) + 
                  t1*(758 - 1168*t2 + 383*Power(t2,2)) + 
                  2*(81 - 176*t2 + 139*Power(t2,2) + 6*Power(t2,3))) + 
               Power(s2,2)*(-111 + Power(t1,4)*(59 - 48*t2) + 2164*t2 - 
                  6300*Power(t2,2) + 4332*Power(t2,3) - 
                  297*Power(t2,4) - 64*Power(t2,5) + 
                  2*Power(t1,3)*(193 - 625*t2 + 92*Power(t2,2)) + 
                  Power(t1,2)*
                   (2502 + 7*t2 - 1977*Power(t2,2) + 408*Power(t2,3)) + 
                  t1*(-3866 + 9400*t2 - 8851*Power(t2,2) + 
                     3419*Power(t2,3) - 270*Power(t2,4))) + 
               s2*(-1096 - 785*t2 + 3839*Power(t2,2) + 144*Power(t2,3) - 
                  2355*Power(t2,4) + 453*Power(t2,5) + 
                  Power(t1,5)*(-68 + 21*t2) + 
                  2*Power(t1,4)*(159 + 70*t2 + 18*Power(t2,2)) - 
                  Power(t1,3)*
                   (1439 + 588*t2 - 1617*Power(t2,2) + 190*Power(t2,3)) + 
                  Power(t1,2)*
                   (3909 - 10709*t2 + 8117*Power(t2,2) - 
                     1562*Power(t2,3) + 63*Power(t2,4)) + 
                  t1*(3771 - 8242*t2 + 7746*Power(t2,2) - 
                     2214*Power(t2,3) - 840*Power(t2,4) + 70*Power(t2,5)))\
) + Power(s,2)*(-219 - 1113*t2 + 3057*Power(t2,2) - 507*Power(t2,3) - 
               2188*Power(t2,4) + 912*Power(t2,5) + 58*Power(t2,6) + 
               6*Power(t1,6)*(-10 + 9*t2) + 
               8*Power(s2,6)*(4 - 7*t2 + 3*Power(t2,2)) - 
               Power(t1,5)*(-374 + 343*t2 + 12*Power(t2,2) + 
                  5*Power(t2,3)) + 
               2*Power(t1,4)*(-3 - 102*t2 + 207*Power(t2,2) - 
                  94*Power(t2,3) + 5*Power(t2,4)) - 
               Power(t1,3)*(276 + 2008*t2 - 3993*Power(t2,2) + 
                  1950*Power(t2,3) - 145*Power(t2,4) + 5*Power(t2,5)) + 
               Power(t1,2)*(2254 - 7462*t2 + 8896*Power(t2,2) - 
                  3392*Power(t2,3) - 311*Power(t2,4) + 91*Power(t2,5)) + 
               t1*(441 + 1124*t2 - 2586*Power(t2,2) + 572*Power(t2,3) - 
                  615*Power(t2,4) + 1050*Power(t2,5) - 18*Power(t2,6)) + 
               Power(s2,5)*(83 - 239*t2 + 207*Power(t2,2) - 
                  66*Power(t2,3) + t1*(-219 + 353*t2 - 117*Power(t2,2))) \
+ Power(s2,4)*(-533 + 1502*t2 - 1813*Power(t2,2) + 872*Power(t2,3) - 
                  84*Power(t2,4) + 
                  Power(t1,2)*(461 - 723*t2 + 186*Power(t2,2)) + 
                  t1*(-427 + 1664*t2 - 1436*Power(t2,2) + 
                     321*Power(t2,3))) - 
               Power(s2,3)*(155 + 1042*t2 - 1779*Power(t2,2) + 
                  39*Power(t2,3) + 615*Power(t2,4) - 76*Power(t2,5) + 
                  Power(t1,3)*(329 - 557*t2 + 96*Power(t2,2)) + 
                  Power(t1,2)*
                   (-242 + 2819*t2 - 2769*Power(t2,2) + 488*Power(t2,3)) \
+ t1*(-3597 + 7493*t2 - 5817*Power(t2,2) + 1844*Power(t2,3) - 
                     130*Power(t2,4))) - 
               Power(s2,2)*(-566 + 707*t2 - 4055*Power(t2,2) + 
                  7787*Power(t2,3) - 4420*Power(t2,4) + 
                  539*Power(t2,5) + 
                  Power(t1,4)*(69 + 23*t2 + 18*Power(t2,2)) - 
                  Power(t1,3)*
                   (839 + 1259*t2 - 2070*Power(t2,2) + 272*Power(t2,3)) \
+ Power(t1,2)*(5248 - 9270*t2 + 4848*Power(t2,2) - 611*Power(t2,3) + 
                     9*Power(t2,4)) + 
                  t1*(-68 + 2259*t2 - 5570*Power(t2,2) + 
                     5275*Power(t2,3) - 1959*Power(t2,4) + 
                     132*Power(t2,5))) + 
               s2*(703 - 682*t2 - 4871*Power(t2,2) + 8336*Power(t2,3) - 
                  3014*Power(t2,4) - 546*Power(t2,5) + 106*Power(t2,6) + 
                  Power(t1,5)*(184 - 162*t2 + 21*Power(t2,2)) + 
                  Power(t1,4)*
                   (-1111 + 478*t2 + 542*Power(t2,2) - 34*Power(t2,3)) + 
                  Power(t1,3)*
                   (2190 - 3075*t2 + 430*Power(t2,2) + 549*Power(t2,3) - 
                     47*Power(t2,4)) + 
                  Power(t1,2)*
                   (252 + 5716*t2 - 11665*Power(t2,2) + 
                     7076*Power(t2,3) - 1273*Power(t2,4) + 60*Power(t2,5)\
) + t1*(-4008 + 11571*t2 - 16990*Power(t2,2) + 13964*Power(t2,3) - 
                     4754*Power(t2,4) + 133*Power(t2,5)))) + 
            s*(6*Power(t1,6)*(11 - 20*t2 + 9*Power(t2,2)) + 
               8*Power(s2,6)*(-4 + 12*t2 - 11*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(t1,5)*(287 - 519*t2 + 165*Power(t2,2) + 
                  68*Power(t2,3)) + 
               Power(t1,4)*(-415 + 1020*t2 - 960*Power(t2,2) + 
                  374*Power(t2,3) - 19*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (303 + 110*t2 - 906*Power(t2,2) - 196*Power(t2,3) + 
                  333*Power(t2,4) + 24*Power(t2,5)) + 
               Power(t1,3)*(432 + 788*t2 - 3511*Power(t2,2) + 
                  3288*Power(t2,3) - 1037*Power(t2,4) + 37*Power(t2,5)) + 
               Power(t1,2)*(-602 + 3633*t2 - 8413*Power(t2,2) + 
                  8403*Power(t2,3) - 3243*Power(t2,4) + 214*Power(t2,5) + 
                  8*Power(t2,6)) + 
               t1*(-865 + 1172*t2 + 1037*Power(t2,2) - 1013*Power(t2,3) - 
                  1182*Power(t2,4) + 649*Power(t2,5) + 202*Power(t2,6)) + 
               Power(s2,5)*(39 - 86*t2 + 75*Power(t2,2) - 
                  25*Power(t2,3) - 2*Power(t2,4) + 
                  t1*(215 - 621*t2 + 523*Power(t2,2) - 117*Power(t2,3))) + 
               Power(s2,4)*(330 - 1195*t2 + 1928*Power(t2,2) - 
                  1577*Power(t2,3) + 558*Power(t2,4) - 44*Power(t2,5) + 
                  Power(t1,2)*
                   (-451 + 1334*t2 - 1083*Power(t2,2) + 200*Power(t2,3)) \
+ t1*(-258 + 175*t2 + 297*Power(t2,2) - 264*Power(t2,3) + 45*Power(t2,4))) \
+ Power(s2,2)*(-586 - 107*t2 + 2034*Power(t2,2) - 393*Power(t2,3) - 
                  2130*Power(t2,4) + 1328*Power(t2,5) - 146*Power(t2,6) + 
                  Power(t1,4)*
                   (85 + 114*t2 - 223*Power(t2,2) + 24*Power(t2,3)) + 
                  Power(t1,3)*
                   (-1501 + 1924*t2 + 414*Power(t2,2) - 
                     934*Power(t2,3) + 87*Power(t2,4)) + 
                  Power(t1,2)*
                   (2928 - 8859*t2 + 9915*Power(t2,2) - 
                     5039*Power(t2,3) + 1133*Power(t2,4) - 78*Power(t2,5)\
) + t1*(1884 - 5530*t2 + 6841*Power(t2,2) - 4066*Power(t2,3) + 
                     763*Power(t2,4) + 99*Power(t2,5))) + 
               Power(s2,3)*(-161 + 1803*t2 - 4624*Power(t2,2) + 
                  4495*Power(t2,3) - 1633*Power(t2,4) + 123*Power(t2,5) + 
                  Power(t1,3)*
                   (317 - 1116*t2 + 937*Power(t2,2) - 138*Power(t2,3)) + 
                  Power(t1,2)*
                   (916 - 793*t2 - 885*Power(t2,2) + 879*Power(t2,3) - 
                     107*Power(t2,4)) + 
                  t1*(-2281 + 7174*t2 - 8965*Power(t2,2) + 
                     5480*Power(t2,3) - 1514*Power(t2,4) + 106*Power(t2,5)\
)) + s2*(66 + 762*t2 - 569*Power(t2,2) - 3534*Power(t2,3) + 
                  5376*Power(t2,4) - 2144*Power(t2,5) + 43*Power(t2,6) + 
                  Power(t1,5)*
                   (-200 + 313*t2 - 120*Power(t2,2) + 7*Power(t2,3)) + 
                  Power(t1,4)*
                   (1091 - 1739*t2 + 264*Power(t2,2) + 412*Power(t2,3) - 
                     23*Power(t2,4)) + 
                  Power(t1,2)*
                   (-2070 + 2488*t2 + 2143*Power(t2,2) - 
                     4356*Power(t2,3) + 2017*Power(t2,4) - 213*Power(t2,5)\
) + 2*Power(t1,3)*(-281 + 930*t2 - 959*Power(t2,2) + 381*Power(t2,3) - 
                     79*Power(t2,4) + 8*Power(t2,5)) + 
                  t1*(1607 - 5157*t2 + 8989*Power(t2,2) - 
                     10622*Power(t2,3) + 7138*Power(t2,4) - 
                     2035*Power(t2,5) + 80*Power(t2,6)))))))/
     (Power(-1 + s1,2)*s1*(-1 + t1)*(1 - s2 + t1 - t2)*
       (1 - s + s*s1 - s1*s2 + s1*t1 - t2)*(-1 + t2)*Power(-1 + s + t2,2)*
       (-1 + s - s1 + t2)*(s - s1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(28 - 27*s2 + 16*Power(s2,3) + 3*Power(s2,4) - 26*t1 + 4*s2*t1 - 
         41*Power(s2,2)*t1 - 13*Power(s2,3)*t1 - Power(s2,4)*t1 + 
         8*Power(t1,2) + 46*s2*Power(t1,2) + 26*Power(s2,2)*Power(t1,2) + 
         3*Power(s2,3)*Power(t1,2) - 20*Power(t1,3) - 28*s2*Power(t1,3) - 
         5*Power(s2,2)*Power(t1,3) + 12*Power(t1,4) + 5*s2*Power(t1,4) - 
         2*Power(t1,5) + 2*Power(s,6)*Power(-1 + s1,2)*Power(t1 - t2,2) - 
         118*t2 + 134*s2*t2 + 28*Power(s2,2)*t2 - 44*Power(s2,3)*t2 - 
         9*Power(s2,4)*t2 + Power(s2,5)*t2 + 102*t1*t2 - 98*s2*t1*t2 + 
         83*Power(s2,2)*t1*t2 + 31*Power(s2,3)*t1*t2 - 
         Power(s2,4)*t1*t2 + 14*Power(t1,2)*t2 - 68*s2*Power(t1,2)*t2 - 
         49*Power(s2,2)*Power(t1,2)*t2 + 2*Power(s2,3)*Power(t1,2)*t2 + 
         25*Power(t1,3)*t2 + 51*s2*Power(t1,3)*t2 - 
         4*Power(s2,2)*Power(t1,3)*t2 - 24*Power(t1,4)*t2 + 
         s2*Power(t1,4)*t2 + Power(t1,5)*t2 + 187*Power(t2,2) - 
         252*s2*Power(t2,2) - 50*Power(s2,2)*Power(t2,2) + 
         43*Power(s2,3)*Power(t2,2) + 12*Power(s2,4)*Power(t2,2) - 
         2*Power(s2,5)*Power(t2,2) - 167*t1*Power(t2,2) + 
         220*s2*t1*Power(t2,2) - 47*Power(s2,2)*t1*Power(t2,2) - 
         37*Power(s2,3)*t1*Power(t2,2) + Power(s2,4)*t1*Power(t2,2) - 
         66*Power(t1,2)*Power(t2,2) - 7*s2*Power(t1,2)*Power(t2,2) + 
         41*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         17*Power(t1,3)*Power(t2,2) - 31*s2*Power(t1,3)*Power(t2,2) - 
         Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         15*Power(t1,4)*Power(t2,2) - s2*Power(t1,4)*Power(t2,2) - 
         128*Power(t2,3) + 231*s2*Power(t2,3) - 
         6*Power(s2,2)*Power(t2,3) - 11*Power(s2,3)*Power(t2,3) - 
         6*Power(s2,4)*Power(t2,3) + 2*Power(s2,5)*Power(t2,3) + 
         145*t1*Power(t2,3) - 136*s2*t1*Power(t2,3) - 
         8*Power(s2,2)*t1*Power(t2,3) + 22*Power(s2,3)*t1*Power(t2,3) - 
         4*Power(s2,4)*t1*Power(t2,3) + 46*Power(t1,2)*Power(t2,3) + 
         49*s2*Power(t1,2)*Power(t2,3) - 
         24*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         34*Power(t1,3)*Power(t2,3) + 12*s2*Power(t1,3)*Power(t2,3) - 
         4*Power(t1,4)*Power(t2,3) + 22*Power(t2,4) - 
         116*s2*Power(t2,4) + 50*Power(s2,2)*Power(t2,4) - 
         6*Power(s2,3)*Power(t2,4) - Power(s2,4)*Power(t2,4) - 
         63*t1*Power(t2,4) - 16*s2*t1*Power(t2,4) + 
         15*Power(s2,2)*t1*Power(t2,4) + Power(s2,3)*t1*Power(t2,4) + 
         10*Power(t1,2)*Power(t2,4) - 20*s2*Power(t1,2)*Power(t2,4) + 
         12*Power(t1,3)*Power(t2,4) + 14*Power(t2,5) + 
         39*s2*Power(t2,5) - 22*Power(s2,2)*Power(t2,5) + 
         2*Power(s2,3)*Power(t2,5) + 5*t1*Power(t2,5) + 
         26*s2*t1*Power(t2,5) - 2*Power(s2,2)*t1*Power(t2,5) - 
         12*Power(t1,2)*Power(t2,5) - 5*Power(t2,6) - 9*s2*Power(t2,6) + 
         4*t1*Power(t2,6) + Power(s1,6)*Power(s2 - t1,3)*
          (5 + 4*Power(s2,2) + 5*Power(t1,2) + t1*(11 - 9*t2) - 9*t2 + 
            4*Power(t2,2) + s2*(-10 - 9*t1 + 8*t2)) + 
         Power(s1,5)*(s2 - t1)*
          (3*Power(s2,5) - 2*Power(s2,4)*(7 + 6*t1 - t2) + 
            Power(s2,3)*(-14 + 17*Power(t1,2) + 10*t2 - 5*Power(t2,2) + 
               t1*(49 + t2)) - 
            Power(s2,2)*(-45 + 9*Power(t1,3) + 75*t2 - 34*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,2)*(61 + 17*t2) + 
               t1*(-52 + 47*t2 - 22*Power(t2,2))) + 
            t1*(Power(t1,4) - 3*Power(-1 + t2,2)*(-5 + 4*t2) - 
               Power(t1,3)*(5 + 9*t2) + 
               Power(t1,2)*(26 - 31*t2 + 14*Power(t2,2)) + 
               t1*(51 - 89*t2 + 44*Power(t2,2) - 6*Power(t2,3))) + 
            s2*(3*Power(-1 + t2,2)*(-5 + 4*t2) + 
               Power(t1,3)*(31 + 23*t2) + 
               Power(t1,2)*(-64 + 68*t2 - 31*Power(t2,2)) + 
               t1*(-95 + 161*t2 - 75*Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s1,4)*(Power(s2,7) + Power(s2,6)*(-5 - 6*t1 + t2) + 
            Power(s2,5)*(-15 + 15*Power(t1,2) + t1*(21 - 2*t2) + 
               10*t2 - Power(t2,2)) - 
            Power(s2,4)*(-59 + 20*Power(t1,3) + 58*t2 + Power(t2,3) + 
               3*Power(t1,2)*(11 + t2) + 
               t1*(-73 + 61*t2 - 12*Power(t2,2))) + 
            Power(s2,3)*(30 + 15*Power(t1,4) - 52*t2 + 50*Power(t2,2) - 
               28*Power(t2,3) + Power(t1,3)*(23 + 11*t2) + 
               Power(t1,2)*(-132 + 127*t2 - 31*Power(t2,2)) + 
               t1*(-217 + 199*t2 + 7*Power(t2,2) + 11*Power(t2,3))) - 
            t1*(-1 + t2)*(2*Power(t1,4) + 
               3*Power(-1 + t2,2)*(-5 + 4*t2) + 
               Power(t1,3)*(32 + 31*t2) + 
               Power(t1,2)*(-55 + 68*t2 - 49*Power(t2,2)) + 
               t1*(-83 + 140*t2 - 75*Power(t2,2) + 18*Power(t2,3))) - 
            Power(s2,2)*(6*Power(t1,5) + 2*Power(t1,4)*(3 + 5*t2) + 
               Power(t1,3)*(-107 + 113*t2 - 30*Power(t2,2)) + 
               Power(-1 + t2,2)*(75 - 44*t2 + 13*Power(t2,2)) + 
               6*Power(t1,2)*
                (-48 + 37*t2 + 8*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(108 - 203*t2 + 187*Power(t2,2) - 89*Power(t2,3) - 
                  3*Power(t2,4))) + 
            s2*(Power(t1,6) + 3*Power(t1,5)*t2 + 
               3*Power(-1 + t2,3)*(-5 + 4*t2) + 
               Power(t1,4)*(-35 + 39*t2 - 10*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(157 - 99*t2 + 30*Power(t2,2)) + 
               2*Power(t1,3)*
                (-81 + 41*t2 + 36*Power(t2,2) + 4*Power(t2,3)) - 
               2*Power(t1,2)*(-67 + 139*t2 - 130*Power(t2,2) + 
                  57*Power(t2,3) + Power(t2,4)))) + 
         Power(s1,3)*(4*Power(t1,7) + Power(s2,7)*(-3 + t2) + 
            Power(-1 + t2,4)*(-5 + 4*t2) - 2*Power(t1,6)*(5 + 6*t2) + 
            Power(s2,6)*(-19 + t1*(21 - 5*t2) - 4*t2 + 2*Power(t2,2)) + 
            4*Power(t1,5)*(2 + 6*t2 + 3*Power(t2,2)) + 
            2*t1*Power(-1 + t2,3)*(29 - 18*t2 + 9*Power(t2,2)) - 
            Power(t1,2)*Power(-1 + t2,2)*(62 - 72*t2 + 61*Power(t2,2)) - 
            Power(t1,4)*(16 + 9*t2 + 4*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,3)*(81 - 103*t2 - 23*Power(t2,2) + 
               45*Power(t2,3)) + 
            Power(s2,5)*(15 - 54*t2 - 10*Power(t2,2) + Power(t2,3) + 
               2*Power(t1,2)*(-32 + 5*t2) + 
               t1*(107 + 27*t2 - 6*Power(t2,2))) + 
            Power(s2,4)*(54 - 10*Power(t1,3)*(-11 + t2) - 84*t2 + 
               14*Power(t2,2) - 17*Power(t2,3) + 
               6*Power(t1,2)*(-41 - 14*t2 + Power(t2,2)) + 
               t1*(-44 + 225*t2 + 54*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(-121 + 5*Power(t1,4)*(-23 + t2) + 210*t2 - 
               80*Power(t2,2) - 9*Power(t2,4) - 
               2*Power(t1,3)*(-148 - 71*t2 + Power(t2,2)) - 
               Power(t1,2)*(-30 + 363*t2 + 126*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-183 + 353*t2 - 109*Power(t2,2) + 69*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s2*(26*Power(t1,6) - 3*Power(t1,5)*(23 + 21*t2) + 
               5*Power(-1 + t2,3)*(11 - 6*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(29 + 123*t2 + 72*Power(t2,2)) + 
               Power(t1,3)*(46 - 214*t2 + 88*Power(t2,2) - 
                  52*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (102 - 99*t2 + 95*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(301 - 486*t2 + 136*Power(t2,2) + 
                  24*Power(t2,3) + 25*Power(t2,4))) + 
            Power(s2,2)*(-(Power(t1,5)*(-73 + t2)) - 
               Power(t1,4)*(197 + 132*t2) - 
               Power(-1 + t2,2)*
                (46 - 40*t2 + 42*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,3)*(20 + 291*t2 + 142*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(191 - 474*t2 + 187*Power(t2,2) - 
                  100*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(343 - 601*t2 + 251*Power(t2,2) - 29*Power(t2,3) + 
                  36*Power(t2,4)))) + 
         Power(s1,2)*(Power(s2,7)*t2 - 4*Power(t1,6)*(-4 + 3*t2) - 
            Power(s2,6)*(-9 + t1 + 11*t2 + 5*t1*t2 - 4*Power(t2,2)) - 
            Power(-1 + t2,4)*(15 - 8*t2 + 6*Power(t2,2)) + 
            t1*Power(-1 + t2,3)*(39 - 40*t2 + 31*Power(t2,2)) + 
            Power(t1,5)*(-33 - 11*t2 + 36*Power(t2,2)) - 
            Power(t1,2)*Power(-1 + t2,2)*
             (-123 + 33*t2 + 38*Power(t2,2)) + 
            Power(t1,4)*(15 + 63*t2 - 39*Power(t2,2) - 36*Power(t2,3)) + 
            Power(t1,3)*(-73 + 97*t2 - 57*Power(t2,2) + 
               21*Power(t2,3) + 12*Power(t2,4)) + 
            Power(s2,5)*(51 - 36*t2 - 21*Power(t2,2) + 5*Power(t2,3) + 
               5*Power(t1,2)*(1 + 2*t2) + 
               t1*(-55 + 57*t2 - 15*Power(t2,2))) - 
            Power(s2,4)*(15 - 133*t2 + 71*Power(t2,2) + 
               46*Power(t2,3) - 2*Power(t2,4) + 
               10*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(-146 + 131*t2 - 21*Power(t2,2)) + 
               t1*(237 - 149*t2 - 93*Power(t2,2) + 9*Power(t2,3))) + 
            s2*(Power(t1,6) + Power(t1,5)*(-83 + 62*t2) + 
               Power(t1,4)*(178 + 8*t2 - 155*Power(t2,2)) - 
               2*Power(-1 + t2,3)*
                (19 - 17*t2 + 12*Power(t2,2) + Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-287 + 145*t2 + 12*Power(t2,2) + 26*Power(t2,3)) + 
               Power(t1,3)*(-35 - 299*t2 + 159*Power(t2,2) + 
                  163*Power(t2,3)) + 
               Power(t1,2)*(-9 + 251*t2 - 272*Power(t2,2) + 
                  109*Power(t2,3) - 79*Power(t2,4))) + 
            Power(s2,3)*(-102 + 272*t2 - 184*Power(t2,2) + 
               53*Power(t2,3) - 38*Power(t2,4) - Power(t2,5) + 
               5*Power(t1,4)*(2 + t2) + 
               Power(t1,3)*(-214 + 173*t2 - 13*Power(t2,2)) + 
               Power(t1,2)*(433 - 204*t2 - 206*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(29 - 460*t2 + 262*Power(t2,2) + 152*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s2,2)*(-(Power(t1,5)*(5 + t2)) + 
               Power(t1,4)*(181 - 138*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-392 + 94*t2 + 253*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-149 + 74*t2 + 5*Power(t2,2) + 18*Power(t2,3)) + 
               Power(t1,2)*(6 + 563*t2 - 311*Power(t2,2) - 
                  233*Power(t2,3) - 7*Power(t2,4)) + 
               t1*(181 - 607*t2 + 491*Power(t2,2) - 165*Power(t2,3) + 
                  98*Power(t2,4) + 2*Power(t2,5)))) + 
         s1*(-Power(t1,6) + 2*Power(s2,6)*(-1 + t2)*t2 - 
            Power(-1 + t2,4)*(12 - 10*t2 + 5*Power(t2,2)) + 
            2*t1*Power(-1 + t2,3)*(-49 + 11*t2 + 10*Power(t2,2)) + 
            Power(t1,5)*(23 - 31*t2 + 12*Power(t2,2)) + 
            Power(t1,4)*(-44 + 19*t2 + 55*Power(t2,2) - 
               36*Power(t2,3)) - 
            Power(t1,2)*Power(-1 + t2,2)*
             (79 - 52*t2 + 43*Power(t2,2) + 12*Power(t2,3)) + 
            Power(t1,3)*(15 + 60*t2 - 116*Power(t2,2) + 5*Power(t2,3) + 
               36*Power(t2,4)) + 
            Power(s2,5)*(-9 + 19*t2 - 16*Power(t2,2) + 4*Power(t2,3) + 
               t1*(2 + 6*t2 - 7*Power(t2,2))) + 
            Power(s2,4)*(-49 + 84*t2 - 24*Power(t2,2) - 
               22*Power(t2,3) + 5*Power(t2,4) + 
               Power(t1,2)*(-8 - 6*t2 + 9*Power(t2,2)) + 
               t1*(47 - 83*t2 + 58*Power(t2,2) - 10*Power(t2,3))) + 
            Power(s2,3)*(5 - 108*t2 + 131*Power(t2,2) + 
               28*Power(t2,3) - 55*Power(t2,4) - Power(t2,5) + 
               Power(t1,3)*(13 + 2*t2 - 5*Power(t2,2)) + 
               4*Power(t1,2)*
                (-27 + 41*t2 - 23*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(177 - 259*t2 + 44*Power(t2,2) + 68*Power(t2,3) - 
                  6*Power(t2,4))) + 
            s2*(5*Power(t1,5) - 
               3*Power(t1,4)*(29 - 39*t2 + 16*Power(t2,2)) - 
               Power(-1 + t2,3)*
                (-101 + 33*t2 + 3*Power(t2,2) + 9*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-30 + 153*t2 - 5*Power(t2,2) + 46*Power(t2,3)) + 
               Power(t1,3)*(164 - 116*t2 - 135*Power(t2,2) + 
                  111*Power(t2,3)) - 
               Power(t1,2)*(11 + 280*t2 - 434*Power(t2,2) + 
                  23*Power(t2,3) + 120*Power(t2,4))) + 
            Power(s2,2)*(Power(t1,4)*(-11 + Power(t2,2)) - 
               2*Power(t1,3)*
                (-67 + 93*t2 - 43*Power(t2,2) + Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-87 + 158*t2 - 20*Power(t2,2) + 31*Power(t2,3)) + 
               Power(t1,2)*(-248 + 272*t2 + 60*Power(t2,2) - 
                  121*Power(t2,3) + Power(t2,4)) + 
               t1*(-10 + 335*t2 - 467*Power(t2,2) + 12*Power(t2,3) + 
                  126*Power(t2,4) + 4*Power(t2,5)))) + 
         Power(s,5)*(-1 + s1)*
          (-4 - 6*t1 + 10*Power(t1,2) - 7*Power(t1,3) + 2*t2 - 
            16*t1*t2 + 17*Power(t1,2)*t2 + 6*Power(t2,2) - 
            13*t1*Power(t2,2) + 3*Power(t2,3) + 
            2*Power(s1,2)*(-2 - 3*Power(t1,2) + 3*t2 - 3*Power(t2,2) + 
               s2*(2 + t1 + t2) + t1*(-5 + 6*t2)) + 
            2*s2*(2 + 2*Power(t1,2) + 3*t2 + 4*Power(t2,2) - 
               t1*(1 + 6*t2)) + 
            s1*(8 + Power(t1,3) + Power(t1,2)*(-4 + t2) - 8*t2 + 
               3*Power(t2,3) + t1*(16 + 4*t2 - 5*Power(t2,2)) - 
               4*s2*(Power(t1,2) - 3*t1*t2 + 2*(1 + t2 + Power(t2,2))))) \
+ Power(s,4)*(-4 - 17*t1 + 42*Power(t1,2) - 26*Power(t1,3) + 
            8*Power(t1,4) + 9*t2 - 30*t1*t2 + 52*Power(t1,2)*t2 - 
            18*Power(t1,3)*t2 - 4*Power(t2,2) - 42*t1*Power(t2,2) + 
            6*Power(t1,2)*Power(t2,2) + 16*Power(t2,3) + 
            10*t1*Power(t2,3) - 6*Power(t2,4) + 
            Power(s2,3)*(4 - t1 + t2) + 
            Power(s2,2)*(8 + 5*Power(t1,2) + 17*t2 + 13*Power(t2,2) - 
               t1*(15 + 16*t2)) + 
            Power(s1,4)*(-(Power(t1,2)*(-7 + t2)) + 
               s2*(2 + t1*(-5 + t2) - 5*t2 - Power(t2,2)) - 
               t2*(5 - 8*t2 + Power(t2,2)) + 
               t1*(11 - 13*t2 + 2*Power(t2,2))) + 
            s2*(-6 - 12*Power(t1,3) + 11*t2 + 40*Power(t2,2) + 
               11*Power(t2,3) + Power(t1,2)*(26 + 43*t2) - 
               t1*(9 + 74*t2 + 42*Power(t2,2))) + 
            Power(s1,2)*(8 + Power(t1,4) + Power(s2,3)*(12 + t1 - t2) - 
               16*t2 + 8*Power(t2,2) + 8*Power(t2,3) + 3*Power(t2,4) - 
               3*Power(t1,3)*(3 + 2*t2) + 
               3*Power(t1,2)*(13 - 2*t2 + 4*Power(t2,2)) + 
               t1*(10 - 21*t2 + 7*Power(t2,2) - 10*Power(t2,3)) - 
               Power(s2,2)*(-44 + Power(t1,2) - 51*t2 - 
                  11*Power(t2,2) + t1*(45 + 8*t2)) - 
               s2*(52 - 50*Power(t1,2) + Power(t1,3) - 50*t2 + 
                  3*Power(t2,2) + 10*Power(t2,3) + 
                  t1*(74 + 63*t2 - 11*Power(t2,2)))) + 
            Power(s1,3)*(-6 + 2*Power(t1,4) + Power(t1,3)*(15 - 6*t2) + 
               20*t2 - 8*Power(t2,2) - 9*Power(t2,3) + 
               Power(s2,3)*(-4 - t1 + t2) + 
               Power(s2,2)*(-18 + 4*Power(t1,2) + t1*(15 - 6*t2) - 
                  17*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(1 - 31*t2 + 6*Power(t2,2)) - 
               t1*(30 + 5*t2 - 25*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(20 - 5*Power(t1,3) - 12*t2 + 12*Power(t2,2) + 
                  Power(t2,3) + Power(t1,2)*(-23 + 11*t2) + 
                  t1*(40 + 15*t2 - 7*Power(t2,2)))) + 
            s1*(2 - 11*Power(t1,4) + Power(s2,3)*(-12 + t1 - t2) - 
               8*t2 - 6*Power(t2,2) - 12*Power(t2,3) + 3*Power(t2,4) + 
               6*Power(t1,3)*(3 + 5*t2) - 
               Power(t1,2)*(91 + 8*t2 + 24*Power(t2,2)) + 
               t1*(26 + 73*t2 + 2*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,2)*(34 + 10*Power(t1,2) + 51*t2 + 
                  28*Power(t2,2) - t1*(45 + 34*t2)) + 
               s2*(20*Power(t1,3) - Power(t1,2)*(49 + 60*t2) + 
                  t1*(48 + 113*t2 + 44*Power(t2,2)) - 
                  4*(-9 + 11*t2 + 11*Power(t2,2) + Power(t2,3))))) + 
         Power(s,3)*(-25 + 67*s2 - 19*Power(s2,2) - 27*Power(s2,3) - 
            4*Power(s2,4) + 30*t1 + 6*s2*t1 + 85*Power(s2,2)*t1 + 
            19*Power(s2,3)*t1 + Power(s2,4)*t1 - 93*Power(t1,2) - 
            102*s2*Power(t1,2) - 44*Power(s2,2)*Power(t1,2) - 
            3*Power(s2,3)*Power(t1,2) + 58*Power(t1,3) + 
            65*s2*Power(t1,3) + 5*Power(s2,2)*Power(t1,3) - 
            36*Power(t1,4) - 5*s2*Power(t1,4) + 2*Power(t1,5) + 72*t2 - 
            153*s2*t2 - 34*Power(s2,2)*t2 - 4*Power(s2,3)*t2 - 
            2*Power(s2,4)*t2 + 67*t1*t2 + 216*s2*t1*t2 + 
            62*Power(s2,2)*t1*t2 + 9*Power(s2,3)*t1*t2 - 
            34*Power(t1,2)*t2 - 169*s2*Power(t1,2)*t2 - 
            27*Power(s2,2)*Power(t1,2)*t2 + 85*Power(t1,3)*t2 + 
            19*s2*Power(t1,3)*t2 + Power(t1,4)*t2 - 34*Power(t2,2) - 
            44*s2*Power(t2,2) - 42*Power(s2,2)*Power(t2,2) - 
            8*Power(s2,3)*Power(t2,2) + 10*t1*Power(t2,2) + 
            79*s2*t1*Power(t2,2) + 40*Power(s2,2)*t1*Power(t2,2) - 
            55*Power(t1,2)*Power(t2,2) - 3*s2*Power(t1,2)*Power(t2,2) - 
            17*Power(t1,3)*Power(t2,2) - 42*Power(t2,3) + 
            33*s2*Power(t2,3) - 14*Power(s2,2)*Power(t2,3) - 
            25*t1*Power(t2,3) - 31*s2*t1*Power(t2,3) + 
            21*Power(t1,2)*Power(t2,3) + 31*Power(t2,4) + 
            20*s2*Power(t2,4) - 5*t1*Power(t2,4) - 2*Power(t2,5) + 
            Power(s1,5)*(Power(t1,2)*(-5 + t2) - 
               Power(s2,2)*(-1 + t2) + 2*Power(-1 + t2,2)*t2 + 
               t1*(-5 + 4*t2 - 3*Power(t2,2)) + 
               s2*(-5 + 4*t1 + 8*t2 + Power(t2,2))) - 
            Power(s1,4)*(Power(s2,4) + 8*Power(t1,4) + 
               Power(t1,3)*(52 - 17*t2) + Power(-1 + t2,3) + 
               Power(s2,3)*(-11 - 8*t1 + t2) + 
               Power(s2,2)*(24 + 21*Power(t1,2) + t1*(58 - 12*t2) - 
                  19*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(57 - 68*t2 + 10*Power(t2,2)) - 
               t1*(-26 + 51*t2 - 11*Power(t2,2) + Power(t2,3)) - 
               s2*(21 + 22*Power(t1,3) + Power(t1,2)*(99 - 28*t2) - 
                  8*t2 - 32*Power(t2,2) + 4*Power(t2,3) + 
                  t1*(57 - 59*t2 + 2*Power(t2,2)))) + 
            s1*(76 - 6*Power(t1,5) - 234*t2 + 204*Power(t2,2) - 
               30*Power(t2,3) - 17*Power(t2,4) + Power(t2,5) + 
               Power(s2,4)*(23 - 3*t1 + 4*t2) + 
               Power(t1,4)*(33 + 13*t2) + 
               Power(t1,3)*(-131 - 91*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(136 + 69*t2 + 138*Power(t2,2) - 
                  22*Power(t2,3)) + 
               t1*(-70 - 168*t2 + 116*Power(t2,2) - 63*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,3)*(88 + 14*Power(t1,2) + 43*t2 + 
                  29*Power(t2,2) - t1*(97 + 37*t2)) + 
               Power(s2,2)*(14 - 25*Power(t1,3) + 125*t2 + 
                  58*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t1,2)*(141 + 85*t2) - 
                  t1*(262 + 148*t2 + 79*Power(t2,2))) + 
               s2*(-173 + 20*Power(t1,4) + 391*t2 + Power(t2,2) - 
                  31*Power(t2,3) - 13*Power(t2,4) - 
                  5*Power(t1,3)*(20 + 13*t2) + 
                  Power(t1,2)*(303 + 187*t2 + 57*Power(t2,2)) + 
                  t1*(31 - 473*t2 - 88*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(12 + 2*Power(t1,5) + Power(t1,4)*(21 - 4*t2) + 
               Power(s2,4)*(17 + 3*t1 - 4*t2) - 14*t2 - 
               14*Power(t2,2) + 19*Power(t2,3) - 3*Power(t2,4) - 
               Power(t1,3)*(11 + 3*t2) + 
               Power(t1,2)*(-61 + 181*t2 - 36*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-15 + 148*t2 - 189*Power(t2,2) + 21*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,3)*(24 - 13*Power(t1,2) + 43*t2 - 
                  8*Power(t2,2) + t1*(-85 + 23*t2)) + 
               Power(s2,2)*(-15 + 19*Power(t1,3) + 
                  Power(t1,2)*(143 - 42*t2) + 98*t2 - 2*Power(t2,2) - 
                  4*Power(t2,3) + t1*(-40 - 128*t2 + 27*Power(t2,2))) + 
               s2*(-30 - 11*Power(t1,4) - 77*t2 + 119*Power(t2,2) + 
                  25*Power(t2,3) + 3*Power(t1,3)*(-32 + 9*t2) + 
                  Power(t1,2)*(14 + 103*t2 - 21*Power(t2,2)) + 
                  t1*(59 - 181*t2 - 48*Power(t2,2) + 5*Power(t2,3)))) - 
            Power(s1,2)*(64 - 4*Power(t1,5) + 
               Power(s2,4)*(35 + t1 - 2*t2) - 173*t2 + 141*Power(t2,2) - 
               36*Power(t2,3) + 5*Power(t2,4) - Power(t2,5) + 
               2*Power(t1,4)*(2 + 9*t2) + 
               Power(t1,3)*(-154 + 32*t2 - 25*Power(t2,2)) + 
               Power(t1,2)*(-98 + 337*t2 + Power(t2,2) + 
                  11*Power(t2,3)) + 
               t1*(-90 + 134*t2 - 127*Power(t2,2) - 42*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(96 - 2*Power(t1,2) + 77*t2 + 
                  9*Power(t2,2) - t1*(151 + t2)) - 
               Power(s2,2)*(43 + 3*Power(t1,3) - 219*t2 - 
                  12*Power(t2,2) + 7*Power(t2,3) - 
                  Power(t1,2)*(201 + 20*t2) + 
                  t1*(287 + 178*t2 + 14*Power(t2,2))) + 
               s2*(-120 + 8*Power(t1,4) + 149*t2 + 69*Power(t2,2) + 
                  15*Power(t2,3) + 11*Power(t2,4) - 
                  Power(t1,3)*(89 + 35*t2) + 
                  Power(t1,2)*(350 + 37*t2 + 57*Power(t2,2)) - 
                  t1*(-169 + 557*t2 + 3*Power(t2,2) + 41*Power(t2,3))))) \
+ Power(s,2)*(78 - 135*s2 + 15*Power(s2,2) + 58*Power(s2,3) + 
            11*Power(s2,4) - 52*t1 + 9*s2*t1 - 165*Power(s2,2)*t1 - 
            48*Power(s2,3)*t1 - 3*Power(s2,4)*t1 + 99*Power(t1,2) + 
            186*s2*Power(t1,2) + 99*Power(s2,2)*Power(t1,2) + 
            9*Power(s2,3)*Power(t1,2) - 85*Power(t1,3) - 
            122*s2*Power(t1,3) - 15*Power(s2,2)*Power(t1,3) + 
            60*Power(t1,4) + 15*s2*Power(t1,4) - 6*Power(t1,5) - 
            263*t2 + 436*s2*t2 + 58*Power(s2,2)*t2 - 37*Power(s2,3)*t2 - 
            7*Power(s2,4)*t2 + Power(s2,5)*t2 - 12*t1*t2 - 
            383*s2*t1*t2 + 18*Power(s2,2)*t1*t2 + 14*Power(s2,3)*t1*t2 - 
            Power(s2,4)*t1*t2 + 10*Power(t1,2)*t2 + 
            111*s2*Power(t1,2)*t2 + 14*Power(s2,2)*Power(t1,2)*t2 + 
            2*Power(s2,3)*Power(t1,2)*t2 - 74*Power(t1,3)*t2 - 
            4*Power(s2,2)*Power(t1,3)*t2 - 21*Power(t1,4)*t2 + 
            s2*Power(t1,4)*t2 + Power(t1,5)*t2 + 267*Power(t2,2) - 
            278*s2*Power(t2,2) + 29*Power(s2,2)*Power(t2,2) + 
            15*Power(s2,3)*Power(t2,2) - Power(s2,4)*Power(t2,2) + 
            18*t1*Power(t2,2) + 210*s2*t1*Power(t2,2) - 
            29*Power(s2,2)*t1*Power(t2,2) - 
            10*Power(s2,3)*t1*Power(t2,2) + 13*Power(t1,2)*Power(t2,2) - 
            103*s2*Power(t1,2)*Power(t2,2) + 
            3*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
            99*Power(t1,3)*Power(t2,2) + 10*s2*Power(t1,3)*Power(t2,2) - 
            2*Power(t1,4)*Power(t2,2) - 35*Power(t2,3) - 
            39*s2*Power(t2,3) - 56*Power(s2,2)*Power(t2,3) + 
            9*Power(s2,3)*Power(t2,3) + 38*t1*Power(t2,3) + 
            110*s2*t1*Power(t2,3) + 24*Power(s2,2)*t1*Power(t2,3) - 
            105*Power(t1,2)*Power(t2,3) - 
            25*s2*Power(t1,2)*Power(t2,3) - 2*Power(t1,3)*Power(t2,3) - 
            69*Power(t2,4) + 10*s2*Power(t2,4) - 
            23*Power(s2,2)*Power(t2,4) + 11*t1*Power(t2,4) + 
            8*s2*t1*Power(t2,4) + 6*Power(t1,2)*Power(t2,4) + 
            22*Power(t2,5) + 6*s2*Power(t2,5) - 3*t1*Power(t2,5) + 
            Power(s1,6)*(Power(s2,2)*(t1 + 3*t2) + 
               t1*(3*t1 + Power(t1,2) + t2 - Power(t2,2)) - 
               s2*(-2 + 2*Power(t1,2) + 5*t2 - 3*Power(t2,2) + 
                  3*t1*(1 + t2))) + 
            Power(s1,5)*(3*Power(s2,4) + 13*Power(t1,4) + 
               Power(t1,3)*(52 - 23*t2) + (-2 + t2)*Power(-1 + t2,2) + 
               Power(s2,3)*(-27 - 19*t1 + 4*t2) + 
               Power(s2,2)*(37 + 42*Power(t1,2) + t1*(99 - 24*t2) - 
                  43*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(48 - 67*t2 + 6*Power(t2,2)) + 
               t1*(15 - 27*t2 + 8*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-10 - 39*Power(t1,3) + 13*t2 + 5*Power(t2,2) - 
                  8*Power(t2,3) + Power(t1,2)*(-124 + 43*t2) + 
                  t1*(-81 + 102*t2 + 5*Power(t2,2)))) + 
            Power(s1,4)*(3*Power(s2,5) - 6*Power(t1,5) + 
               Power(s2,4)*(-32 - 22*t1 + 3*t2) + 
               Power(t1,4)*(-58 + 4*t2) + 
               Power(s2,3)*(63 + 56*Power(t1,2) + t1*(159 - 29*t2) - 
                  11*t2 - 6*Power(t2,2)) - 
               2*Power(-1 + t2,2)*(-5 + 4*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-46 - 20*t2 + 13*Power(t2,2)) + 
               Power(t1,2)*(77 - 169*t2 + 122*Power(t2,2) - 
                  14*Power(t2,3)) + 
               t1*(65 - 184*t2 + 156*Power(t2,2) - 40*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*(2 - 64*Power(t1,3) - 48*t2 + 
                  68*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t1,2)*(-284 + 57*t2) + 
                  t1*(-182 + 35*t2 + 2*Power(t2,2))) + 
               s2*(-43 + 33*Power(t1,4) + 110*t2 - 71*Power(t2,2) + 
                  4*Power(t2,3) - 5*Power(t1,3)*(-43 + 7*t2) + 
                  Power(t1,2)*(167 - 8*t2 - 7*Power(t2,2)) + 
                  t1*(-49 + 146*t2 - 138*Power(t2,2) + 9*Power(t2,3)))) \
- Power(s1,3)*(Power(s2,5)*(25 + 3*t1 - 6*t2) - 
               Power(t1,5)*(3 + 2*t2) + 
               Power(t1,4)*(95 - 27*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*(180 - 406*t2 + 24*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-6 + 40*t2 - 28*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(-7 + 82*t2 - 166*Power(t2,2) + 112*Power(t2,3) - 
                  21*Power(t2,4)) + 
               Power(t1,2)*(237 - 357*t2 + 201*Power(t2,2) + 
                  24*Power(t2,3) + 2*Power(t2,4)) + 
               Power(s2,4)*(36 - 14*Power(t1,2) + 47*t2 - 
                  12*Power(t2,2) + t1*(-134 + 33*t2)) + 
               Power(s2,3)*(-48 + 24*Power(t1,3) + 
                  Power(t1,2)*(255 - 61*t2) + 238*t2 + 
                  18*Power(t2,2) - 6*Power(t2,3) + 
                  t1*(-170 - 169*t2 + 39*Power(t2,2))) + 
               Power(s2,2)*(34 - 18*Power(t1,4) - 106*t2 + 
                  139*Power(t2,2) + 40*Power(t2,3) + 
                  Power(t1,3)*(-211 + 45*t2) + 
                  Power(t1,2)*(330 + 161*t2 - 30*Power(t2,2)) + 
                  t1*(292 - 869*t2 - 38*Power(t2,2) + 3*Power(t2,3))) + 
               s2*(21 + 5*Power(t1,5) + Power(t1,4)*(68 - 9*t2) - 
                  105*t2 + 152*Power(t2,2) - 71*Power(t2,3) + 
                  3*Power(t2,4) - 
                  3*Power(t1,3)*(97 + 4*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-443 + 1079*t2 - 31*Power(t2,2) + 13*Power(t2,3)) + 
                  t1*(-266 + 513*t2 - 459*Power(t2,2) + 4*Power(t2,3) - 
                     6*Power(t2,4)))) + 
            Power(s1,2)*(-(Power(s2,5)*(-33 + t1)) + Power(t1,6) - 
               Power(t1,5)*(27 + 2*t2) + 
               Power(t1,4)*(122 + 89*t2 - 4*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (-46 + 26*t2 + 7*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,3)*(-100 - 100*t2 - 169*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-12 + 552*t2 - 397*Power(t2,2) + 
                  108*Power(t2,3) - 9*Power(t2,4)) + 
               2*t1*(-161 + 424*t2 - 403*Power(t2,2) + 
                  136*Power(t2,3) + 3*Power(t2,4) + Power(t2,5)) + 
               Power(s2,4)*(139 + 6*Power(t1,2) + 33*t2 + 
                  11*Power(t2,2) - t1*(162 + 11*t2)) + 
               Power(s2,3)*(11 - 13*Power(t1,3) + 264*t2 + 
                  8*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t1,2)*(301 + 47*t2) - 
                  t1*(591 + 104*t2 + 56*Power(t2,2))) + 
               Power(s2,2)*(-213 + 13*Power(t1,4) + 599*t2 - 
                  153*Power(t2,2) - 6*Power(t2,3) + 15*Power(t2,4) - 
                  Power(t1,3)*(275 + 63*t2) + 
                  2*Power(t1,2)*(447 + 85*t2 + 48*Power(t2,2)) - 
                  t1*(-66 + 926*t2 + 20*Power(t2,2) + 61*Power(t2,3))) \
+ s2*(112 - 6*Power(t1,5) - 334*t2 + 411*Power(t2,2) - 
                  185*Power(t2,3) - Power(t2,4) - 3*Power(t2,5) + 
                  Power(t1,4)*(130 + 29*t2) - 
                  47*Power(t1,3)*(12 + 4*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (17 + 754*t2 + 215*Power(t2,2) + 20*Power(t2,3)) + 
                  t1*(228 - 1089*t2 + 430*Power(t2,2) - 
                     60*Power(t2,3) + 7*Power(t2,4)))) + 
            s1*(-Power(t1,6) + Power(s2,5)*(-11 + 2*t1 - 5*t2) + 
               Power(t1,5)*(30 + t2) + 
               Power(t1,4)*(-94 - 63*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(232 + 51*t2 - 5*Power(t2,2) - 
                  4*Power(t2,3)) + 
               2*Power(-1 + t2,2)*
                (-72 + 104*t2 - 24*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-92 - 275*t2 + 49*Power(t2,2) + 
                  137*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(247 - 314*t2 + 23*Power(t2,2) + 142*Power(t2,3) - 
                  101*Power(t2,4) + 3*Power(t2,5)) + 
               Power(s2,4)*(-91 - 8*Power(t1,2) + 10*t2 - 
                  16*Power(t2,2) + t1*(61 + 25*t2)) + 
               Power(s2,3)*(-129 + 13*Power(t1,3) - 10*t2 - 
                  9*Power(t2,2) - 6*Power(t2,3) - 
                  Power(t1,2)*(143 + 46*t2) + t1*(357 + 47*Power(t2,2))) \
+ Power(s2,2)*(157 - 11*Power(t1,4) - 563*t2 + 86*Power(t2,2) + 
                  114*Power(t2,3) + 22*Power(t2,4) + 
                  Power(t1,3)*(177 + 38*t2) - 
                  Power(t1,2)*(507 + 136*t2 + 27*Power(t2,2)) + 
                  t1*(361 + 218*t2 + 25*Power(t2,2) - 22*Power(t2,3))) + 
               s2*(119 + 5*Power(t1,5) - 443*t2 + 320*Power(t2,2) - 
                  21*Power(t2,3) + 28*Power(t2,4) - 3*Power(t2,5) - 
                  Power(t1,4)*(114 + 13*t2) + 
                  Power(t1,3)*(335 + 189*t2 - 8*Power(t2,2)) + 
                  Power(t1,2)*
                   (-467 - 237*t2 - 46*Power(t2,2) + 48*Power(t2,3)) - 
                  t1*(252 - 1290*t2 + 488*Power(t2,2) + 
                     153*Power(t2,3) + 29*Power(t2,4))))) + 
         s*(-83 + 108*s2 - 5*Power(s2,2) - 51*Power(s2,3) - 
            10*Power(s2,4) + 56*t1 - 10*s2*t1 + 136*Power(s2,2)*t1 + 
            43*Power(s2,3)*t1 + 3*Power(s2,4)*t1 - 49*Power(t1,2) - 
            152*s2*Power(t1,2) - 86*Power(s2,2)*Power(t1,2) - 
            9*Power(s2,3)*Power(t1,2) + 66*Power(t1,3) + 
            97*s2*Power(t1,3) + 15*Power(s2,2)*Power(t1,3) - 
            44*Power(t1,4) - 15*s2*Power(t1,4) + 6*Power(t1,5) + 307*t2 - 
            423*s2*t2 - 78*Power(s2,2)*t2 + 87*Power(s2,3)*t2 + 
            18*Power(s2,4)*t2 - 2*Power(s2,5)*t2 - 139*t1*t2 + 
            345*s2*t1*t2 - 155*Power(s2,2)*t1*t2 - 54*Power(s2,3)*t1*t2 + 
            2*Power(s2,4)*t1*t2 - 34*Power(t1,2)*t2 + 
            90*s2*Power(t1,2)*t2 + 62*Power(s2,2)*Power(t1,2)*t2 - 
            4*Power(s2,3)*Power(t1,2)*t2 - 20*Power(t1,3)*t2 - 
            70*s2*Power(t1,3)*t2 + 8*Power(s2,2)*Power(t1,3)*t2 + 
            44*Power(t1,4)*t2 - 2*s2*Power(t1,4)*t2 - 2*Power(t1,5)*t2 - 
            403*Power(t2,2) + 538*s2*Power(t2,2) + 
            51*Power(s2,2)*Power(t2,2) - 37*Power(s2,3)*Power(t2,2) - 
            15*Power(s2,4)*Power(t2,2) + 2*Power(s2,5)*Power(t2,2) + 
            164*t1*Power(t2,2) - 475*s2*t1*Power(t2,2) + 
            12*Power(s2,2)*t1*Power(t2,2) + 
            57*Power(s2,3)*t1*Power(t2,2) - Power(s2,4)*t1*Power(t2,2) + 
            110*Power(t1,2)*Power(t2,2) + 
            121*s2*Power(t1,2)*Power(t2,2) - 
            50*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
            3*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
            96*Power(t1,3)*Power(t2,2) + 19*s2*Power(t1,3)*Power(t2,2) + 
            Power(s2,2)*Power(t1,3)*Power(t2,2) - 
            11*Power(t1,4)*Power(t2,2) + s2*Power(t1,4)*Power(t2,2) + 
            190*Power(t2,3) - 249*s2*Power(t2,3) + 
            93*Power(s2,2)*Power(t2,3) - 7*Power(s2,3)*Power(t2,3) - 
            5*Power(s2,4)*Power(t2,3) - 125*t1*Power(t2,3) + 
            66*s2*t1*Power(t2,3) + 5*Power(s2,2)*t1*Power(t2,3) + 
            Power(s2,3)*t1*Power(t2,3) + 25*Power(t1,2)*Power(t2,3) - 
            48*s2*Power(t1,2)*Power(t2,3) + 
            5*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
            48*Power(t1,3)*Power(t2,3) - s2*Power(t1,3)*Power(t2,3) + 
            23*Power(t2,4) + 36*s2*Power(t2,4) - 
            55*Power(s2,2)*Power(t2,4) + 10*Power(s2,3)*Power(t2,4) + 
            34*t1*Power(t2,4) + 69*s2*t1*Power(t2,4) - 
            4*Power(s2,2)*t1*Power(t2,4) - 52*Power(t1,2)*Power(t2,4) - 
            5*s2*Power(t1,2)*Power(t2,4) - 41*Power(t2,5) - 
            10*s2*Power(t2,5) - 6*Power(s2,2)*Power(t2,5) + 
            10*t1*Power(t2,5) + 5*s2*t1*Power(t2,5) + 7*Power(t2,6) + 
            Power(s1,7)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) - 
            Power(s1,6)*(s2 - t1)*
             (4*Power(s2,3) - 7*Power(t1,3) + 
               Power(s2,2)*(-15*t1 + 11*(-1 + t2)) + 
               15*Power(t1,2)*(-1 + t2) - 2*Power(-1 + t2,2) + 
               t1*(-11 + 19*t2 - 8*Power(t2,2)) + 
               s2*(10 + 18*Power(t1,2) - 26*t1*(-1 + t2) - 17*t2 + 
                  7*Power(t2,2))) + 
            Power(s1,5)*(-6*Power(s2,5) + 9*Power(t1,5) + 
               Power(t1,4)*(52 - 6*t2) + 
               Power(s2,4)*(43 + 34*t1 - 5*t2) + Power(-1 + t2,3) + 
               2*t1*Power(-1 + t2,2)*(-9 + 8*t2) + 
               Power(t1,3)*(23 - 17*t2 - 14*Power(t2,2)) + 
               Power(t1,2)*(-33 + 70*t2 - 48*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(s2,3)*(-32 - 75*Power(t1,2) + 29*t2 + 
                  11*Power(t2,2) + 5*t1*(-37 + 5*t2)) + 
               Power(s2,2)*(-23 + 81*Power(t1,3) + 
                  Power(t1,2)*(293 - 41*t2) + 49*t2 - 36*Power(t2,2) + 
                  10*Power(t2,3) + t1*(94 - 89*t2 - 29*Power(t2,2))) + 
               s2*(-43*Power(t1,4) - Power(-1 + t2,2)*(-17 + 15*t2) + 
                  Power(t1,3)*(-203 + 27*t2) + 
                  Power(t1,2)*(-85 + 77*t2 + 32*Power(t2,2)) + 
                  t1*(52 - 107*t2 + 72*Power(t2,2) - 17*Power(t2,3)))) + 
            Power(s1,4)*(-3*Power(s2,6) + Power(t1,6) + 
               Power(s2,5)*(26 + 20*t1 - 3*t2) - 8*Power(-1 + t2,4) - 
               Power(t1,5)*(1 + 10*t2) - 
               Power(s2,4)*(18 + 50*Power(t1,2) + t1*(115 - 18*t2) + 
                  13*t2 - 4*Power(t2,2)) + 
               Power(t1,4)*(51 - 97*t2 + 19*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(15 - 24*t2 + 22*Power(t2,2)) + 
               Power(t1,3)*(223 - 297*t2 + 104*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(120 - 235*t2 + 101*Power(t2,2) + 
                  12*Power(t2,3) + 2*Power(t2,4)) + 
               Power(s2,2)*(91 - 32*Power(t1,4) - 168*t2 + 
                  52*Power(t2,2) + 25*Power(t2,3) - 
                  2*Power(t1,3)*(69 + t2) + 
                  Power(t1,2)*(98 - 308*t2 + 48*Power(t2,2)) + 
                  t1*(465 - 578*t2 + 188*Power(t2,2) - 21*Power(t2,3))) \
+ Power(s2,3)*(59*Power(t1,3) + Power(t1,2)*(189 - 25*t2) + 
                  t1*(6 + 120*t2 - 18*Power(t2,2)) + 
                  2*(-62 + 73*t2 - 22*Power(t2,2) + 2*Power(t2,3))) + 
               s2*(5*Power(t1,5) + Power(t1,4)*(39 + 22*t2) + 
                  Power(t1,3)*(-137 + 298*t2 - 53*Power(t2,2)) + 
                  Power(-1 + t2,2)*(13 - 17*t2 + 17*Power(t2,2)) + 
                  Power(t1,2)*
                   (-567 + 738*t2 - 257*Power(t2,2) + 32*Power(t2,3)) - 
                  t1*(221 - 437*t2 + 195*Power(t2,2) + 15*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            Power(s1,3)*(10*Power(t1,6) + Power(s2,6)*(15 + t1 - 4*t2) - 
               Power(t1,5)*(41 + 38*t2) + 
               Power(t1,3)*t2*(-239 + 199*t2 - 76*Power(t2,2)) + 
               Power(-1 + t2,3)*(1 - 2*t2 + 11*Power(t2,2)) + 
               Power(t1,4)*(8 + 82*t2 + 77*Power(t2,2)) - 
               4*t1*Power(-1 + t2,2)*
                (-27 + 34*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(326 - 805*t2 + 687*Power(t2,2) - 
                  239*Power(t2,3) + 31*Power(t2,4)) + 
               Power(s2,5)*(47 - 5*Power(t1,2) + 23*t2 - 
                  8*Power(t2,2) + 3*t1*(-29 + 7*t2)) + 
               Power(s2,4)*(-78 + 10*Power(t1,3) + 
                  Power(t1,2)*(204 - 40*t2) + 199*t2 + 24*Power(t2,2) - 
                  4*Power(t2,3) + t1*(-264 - 95*t2 + 25*Power(t2,2))) + 
               Power(s2,3)*(18 - 10*Power(t1,4) + 62*t2 - 
                  5*Power(t2,2) + 41*Power(t2,3) + 
                  Power(t1,3)*(-250 + 34*t2) + 
                  Power(t1,2)*(557 + 173*t2 - 21*Power(t2,2)) - 
                  t1*(-311 + 831*t2 + 69*Power(t2,2) + Power(t2,3))) + 
               s2*(-Power(t1,6) + Power(t1,5)*(-63 + t2) + 
                  Power(t1,4)*(252 + 128*t2 + 5*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (-70 + 85*t2 - 5*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(t1,3)*
                   (-142 + 603*t2 + 172*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-514 + 1246*t2 - 1055*Power(t2,2) + 
                     380*Power(t2,3) - 57*Power(t2,4)) + 
                  Power(t1,2)*
                   (31 + 467*t2 - 292*Power(t2,2) + 138*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(s2,2)*(201 + 5*Power(t1,5) - 469*t2 + 
                  374*Power(t2,2) - 121*Power(t2,3) + 15*Power(t2,4) - 
                  3*Power(t1,4)*(-57 + 4*t2) - 
                  Power(t1,3)*(551 + 191*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-383 + 1153*t2 + 140*Power(t2,2) + 14*Power(t2,3)) - 
                  t1*(60 + 255*t2 - 59*Power(t2,2) + 86*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            Power(s1,2)*(-15*Power(t1,6) + 8*Power(t1,5)*(8 + 3*t2) + 
               Power(t1,4)*(-104 - 73*t2 + 29*Power(t2,2)) + 
               Power(-1 + t2,3)*
                (-15 + 41*t2 - 16*Power(t2,2) + 2*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-234 + 329*t2 - 131*Power(t2,2) + 26*Power(t2,3)) - 
               2*Power(t1,3)*
                (-30 - 97*t2 + 23*Power(t2,2) + 56*Power(t2,3)) + 
               Power(t1,2)*(-246 + 209*t2 + 133*Power(t2,2) - 
                  194*Power(t2,3) + 98*Power(t2,4)) + 
               Power(s2,6)*(t1 - 2*(5 + t2)) + 
               Power(s2,5)*(-81 - 5*Power(t1,2) + 14*t2 - 
                  11*Power(t2,2) + t1*(61 + 15*t2)) + 
               Power(s2,4)*(10*Power(t1,3) - 
                  2*Power(t1,2)*(80 + 19*t2) + 
                  t1*(391 - 48*t2 + 51*Power(t2,2)) - 
                  2*(45 + 26*t2 - 11*Power(t2,2) + 7*Power(t2,3))) + 
               Power(s2,3)*(186 - 10*Power(t1,4) - 556*t2 + 
                  224*Power(t2,2) + 59*Power(t2,3) - 9*Power(t2,4) + 
                  Power(t1,3)*(229 + 44*t2) + 
                  Power(t1,2)*(-735 + 5*t2 - 72*Power(t2,2)) + 
                  t1*(386 + 184*t2 - 59*Power(t2,2) + 39*Power(t2,3))) + 
               Power(s2,2)*(-41 + 5*Power(t1,5) - 127*t2 + 
                  142*Power(t2,2) - 21*Power(t2,3) + 44*Power(t2,4) + 
                  3*Power(t2,5) - Power(t1,4)*(187 + 24*t2) + 
                  Power(t1,3)*(685 + 102*t2 + 35*Power(t2,2)) - 
                  Power(t1,2)*
                   (617 + 239*t2 - 22*Power(t2,2) + 12*Power(t2,3)) - 
                  t1*(477 - 1694*t2 + 804*Power(t2,2) + 
                     114*Power(t2,3) + 11*Power(t2,4))) - 
               s2*(Power(t1,6) - Power(t1,5)*(82 + 5*t2) + 
                  Power(t1,4)*(324 + 97*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (-425 - 180*t2 + 14*Power(t2,2) + 13*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-202 + 255*t2 - 88*Power(t2,2) + 25*Power(t2,3)) - 
                  Power(t1,2)*
                   (240 - 1355*t2 + 641*Power(t2,2) + 170*Power(t2,3) + 
                     16*Power(t2,4)) + 
                  t1*(-315 + 207*t2 + 67*Power(t2,2) - 61*Power(t2,3) + 
                     98*Power(t2,4) + 4*Power(t2,5)))) + 
            s1*(2*Power(t1,6) + 2*Power(s2,6)*t2 + 
               Power(t1,5)*(-47 + 26*t2) + 
               Power(t1,4)*(118 + 20*t2 - 82*Power(t2,2)) + 
               Power(-1 + t2,3)*
                (-82 + 85*t2 - 21*Power(t2,2) + 7*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (295 - 136*t2 - 25*Power(t2,2) + 48*Power(t2,3)) + 
               Power(t1,3)*(-118 - 88*t2 + 95*Power(t2,2) + 
                  60*Power(t2,3)) + 
               Power(t1,2)*(114 - 37*t2 - 49*Power(t2,2) - 
                  63*Power(t2,3) + 35*Power(t2,4)) + 
               Power(s2,5)*(20 - 16*t2 + Power(t2,2) - 2*t1*(2 + 3*t2)) + 
               Power(s2,4)*(117 - 97*t2 - 5*Power(t2,2) - 
                  5*Power(t2,3) + 2*Power(t1,2)*(8 + 3*t2) + 
                  t1*(-105 + 67*t2 + 3*Power(t2,2))) - 
               Power(s2,3)*(-51 - 107*t2 + 21*Power(t2,2) + 
                  69*Power(t2,3) + 17*Power(t2,4) + 
                  2*Power(t1,3)*(13 + t2) + 
                  3*Power(t1,2)*(-79 + 43*t2 + 4*Power(t2,2)) - 
                  t1*(-440 + 288*t2 + 37*Power(t2,2) + 29*Power(t2,3))) + 
               Power(s2,2)*(-242 + 22*Power(t1,4) + 783*t2 - 
                  626*Power(t2,2) + 58*Power(t2,3) + 24*Power(t2,4) + 
                  3*Power(t2,5) + 
                  Power(t1,3)*(-286 + 147*t2 + 11*Power(t2,2)) - 
                  Power(t1,2)*
                   (-630 + 219*t2 + 182*Power(t2,2) + 31*Power(t2,3)) + 
                  t1*(-132 - 522*t2 + 304*Power(t2,2) + 
                     172*Power(t2,3) + 25*Power(t2,4))) + 
               s2*(-10*Power(t1,5) + 
                  Power(t1,4)*(181 - 95*t2 - 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (-425 + 8*t2 + 232*Power(t2,2) + 7*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (130 + 31*t2 + 2*Power(t2,2) + 19*Power(t2,3)) + 
                  Power(t1,2)*
                   (203 + 480*t2 - 333*Power(t2,2) - 200*Power(t2,3) + 
                     3*Power(t2,4)) - 
                  t1*(-225 + 1062*t2 - 1045*Power(t2,2) + 
                     179*Power(t2,3) + 22*Power(t2,4) + 7*Power(t2,5))))))*
       B1(1 - s2 + t1 - t2,s1,1 - s + s1 - t2))/
     ((-1 + t1)*(1 - s2 + t1 - t2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,3)*(-1 + t2)*(s - s1 + t2)) - 
    (8*(-(Power(s1,9)*(s2 - t1)*
            (-3*Power(s,2)*(Power(s2,2) + Power(t1,2) - 
                 2*s2*(2 + t1 - 2*t2) - 4*t1*(-1 + t2) + 
                 2*Power(-1 + t2,2)) + 
              s*(s2 - t1)*(Power(s2,2) + Power(t1,2) + 
                 s2*(-2*t1 + 15*(-1 + t2)) - 15*t1*(-1 + t2) + 
                 14*Power(-1 + t2,2)) - 
              Power(s2 - t1,2)*(-1 + t2)*(-6 + 5*s2 - 5*t1 + 6*t2) + 
              Power(s,3)*(-6 + 4*s2 - 4*t1 + 6*t2))) - 
         t2*Power(-1 + s + t2,4)*(-1 + s2 - t1 + t2)*
          (Power(s,2)*(-2 + t1 - t2) - 
            2*(1 + s2 - t1 + t2 - Power(t2,2)) + 
            s*(3 - 3*t1 + t2 + s2*(2 + t2))) + 
         Power(s1,8)*(4*Power(s,4)*
             (1 + Power(s2,2) + 3*Power(t1,2) + t1*(4 - 3*t2) - 
               Power(t2,2) + s2*(-3 - 4*t1 + 2*t2)) + 
            Power(s,3)*(3*Power(t1,3) + 
               Power(s2,2)*(32 + 3*t1 - 15*t2) - 
               2*Power(-1 + t2,2)*(3 + t2) + Power(t1,2)*(14 + 3*t2) + 
               t1*(15 - 2*t2 - 13*Power(t2,2)) + 
               s2*(-23 - 6*Power(t1,2) + 18*t2 + 5*Power(t2,2) + 
                  2*t1*(-23 + 6*t2))) - 
            Power(s2 - t1,2)*(-1 + t2)*
             (-7*t1*Power(-1 + t2,2) - 2*Power(-1 + t2,2)*(11 + t2) + 
               2*Power(t1,2)*(5 + 3*t2) + Power(s2,2)*(9 + 7*t2) + 
               s2*(7*Power(-1 + t2,2) - t1*(19 + 13*t2))) + 
            s*(Power(s2,4)*(19 - 17*t2) - 
               Power(s2,3)*(53 + t1*(74 - 66*t2) - 54*t2 + 
                  Power(t2,2)) + 
               Power(s2,2)*(5*Power(-1 + t2,2)*(1 + 2*t2) - 
                  12*Power(t1,2)*(-9 + 8*t2) + 
                  6*t1*(27 - 28*t2 + Power(t2,2))) + 
               t1*(Power(t1,3)*(17 - 15*t2) + 
                  4*Power(-1 + t2,3)*(9 + t2) + 
                  3*t1*Power(-1 + t2,2)*(1 + 4*t2) + 
                  4*Power(t1,2)*(14 - 15*t2 + Power(t2,2))) + 
               s2*(-4*Power(-1 + t2,3)*(9 + t2) - 
                  2*t1*Power(-1 + t2,2)*(4 + 11*t2) + 
                  Power(t1,3)*(-70 + 62*t2) - 
                  3*Power(t1,2)*(55 - 58*t2 + 3*Power(t2,2)))) - 
            Power(s,2)*(4*Power(s2,4) + 3*Power(t1,4) + 
               Power(s2,3)*(32 - 15*t1 - 24*t2) - 
               2*Power(-1 + t2,3)*(5 + t2) + 
               t1*Power(-1 + t2,2)*(-11 + 3*t2) + 
               2*Power(t1,3)*(-13 + 9*t2) + 
               Power(t1,2)*(-65 + 84*t2 - 19*Power(t2,2)) + 
               Power(s2,2)*(-53 + 21*Power(t1,2) + 60*t2 - 
                  7*Power(t2,2) + t1*(-90 + 66*t2)) + 
               s2*(-13*Power(t1,3) + Power(t1,2)*(84 - 60*t2) + 
                  Power(-1 + t2,2)*(7 + t2) + 
                  2*t1*(59 - 72*t2 + 13*Power(t2,2))))) + 
         s1*Power(-1 + s + t2,3)*
          (5 + 5*t1 - 5*Power(t1,2) - 5*Power(t1,3) - 21*t2 + 14*t1*t2 + 
            35*Power(t1,2)*t2 - 15*Power(t2,2) - 89*t1*Power(t2,2) - 
            28*Power(t1,2)*Power(t2,2) - 
            Power(s2,3)*(-6 + t2)*Power(t2,2) + 87*Power(t2,3) + 
            84*t1*Power(t2,3) - 70*Power(t2,4) - 14*t1*Power(t2,4) + 
            14*Power(t2,5) + 2*Power(s,4)*(-1 + s2 - t1 + t2)*
             (-2 + t1 + t2) - 
            Power(s2,2)*(10 + (-51 + 11*t1)*t2 + 
               (54 + 5*t1)*Power(t2,2) - (16 + t1)*Power(t2,3) + 
               Power(t2,4)) - 
            Power(s,3)*(16 + 2*Power(s2,2)*(-2 + t1) - 3*Power(t1,3) - 
               54*t2 + 33*Power(t2,2) + 5*Power(t2,3) + 
               Power(t1,2)*(-9 + 19*t2) + 
               t1*(10 - 16*t2 - 21*Power(t2,2)) + 
               s2*(-12 + Power(t1,2) + t1*(8 - 14*t2) + 34*t2 + 
                  5*Power(t2,2))) + 
            s2*(5 - 45*t2 + 130*Power(t2,2) - 113*Power(t2,3) + 
               23*Power(t2,4) + Power(t1,2)*(5 + 11*t2 - Power(t2,2)) + 
               t1*(10 - 70*t2 + 64*Power(t2,2) - 8*Power(t2,3))) + 
            Power(s,2)*(24 - 11*Power(t1,3) - 97*t2 - 
               2*Power(s2,3)*t2 + 94*Power(t2,2) - 17*Power(t2,3) - 
               4*Power(t2,4) - 
               2*Power(t1,2)*(7 - 35*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(-16 + t1*(4 - 7*t2) + 43*t2 + 
                  9*Power(t2,2)) + 
               t1*(21 - 11*t2 - 46*Power(t2,2) + 12*Power(t2,3)) + 
               s2*(-8 + 16*t2 + 9*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t1,2)*(7 + 9*t2) - 
                  3*t1*(-5 + 29*t2 + 4*Power(t2,2)))) - 
            s*(16 - 13*Power(t1,3) - 71*t2 + 40*Power(t2,2) + 
               31*Power(t2,3) - 16*Power(t2,4) + 
               2*Power(s2,3)*t2*(-1 + 3*t2) + 
               Power(t1,2)*(-12 + 84*t2 - 31*Power(t2,2)) + 
               t1*(17 + 15*t2 - 117*Power(t2,2) + 47*Power(t2,3)) + 
               Power(s2,2)*(-22 + 93*t2 - 39*Power(t2,2) + 
                  Power(t2,3) + t1*(2 - 18*t2 - 5*Power(t2,2))) + 
               s2*(6 - 58*t2 + 137*Power(t2,2) - 42*Power(t2,3) - 
                  5*Power(t2,4) + 
                  Power(t1,2)*(11 + 20*t2 - Power(t2,2)) + 
                  t1*(19 - 140*t2 + 41*Power(t2,2) + 6*Power(t2,3))))) + 
         Power(s1,7)*(Power(s,5)*
             (-12 - 5*Power(s2,2) - 11*Power(t1,2) + 
               s2*(19 + 18*t1 - 11*t2) + 8*t2 + 6*Power(t2,2) + 
               t1*(-19 + 7*t2)) + 
            Power(s,4)*(18 + 8*Power(s2,3) + 7*Power(t1,3) - 
               2*Power(s2,2)*(10 + 12*t1 - 7*t2) - 73*t2 - 
               34*Power(t1,2)*t2 + 46*Power(t2,2) + 9*Power(t2,3) + 
               t1*(-7 - 42*t2 + 24*Power(t2,2)) + 
               s2*(4 + 9*Power(t1,2) + 54*t2 - 33*Power(t2,2) + 
                  2*t1*(11 + 9*t2))) + 
            Power(s,3)*(-Power(s2,4) + 10*Power(t1,4) + 
               Power(s2,3)*(-44 + 9*t2) + Power(t1,3)*(11 + 24*t2) + 
               Power(t1,2)*(-28 + 32*t2 - 53*Power(t2,2)) + 
               Power(-1 + t2,2)*(3 + 28*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-12 + 13*Power(t1,2) + t1*(142 - 37*t2) - 
                  94*t2 + 57*Power(t2,2)) + 
               t1*(-36 + 11*t2 + 7*Power(t2,2) + 18*Power(t2,3)) + 
               s2*(44 - 22*Power(t1,3) - 29*t2 + 5*Power(t2,2) - 
                  20*Power(t2,3) + Power(t1,2)*(-109 + 4*t2) + 
                  t1*(22 + 98*t2 - 22*Power(t2,2)))) + 
            (s2 - t1)*(-1 + t2)*
             (6*Power(s2,4)*(-1 + t2) + 2*Power(t1,4)*(-1 + t2) + 
               2*Power(-1 + t2,3)*(13 + 2*t2) + 
               Power(t1,3)*(-15 + 17*t2 - 15*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(-19 - 43*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(1 - 20*t1*(-1 + t2) + 12*Power(t2,2)) + 
               Power(t1,2)*(45 - 8*t2 - 54*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(s2,2)*(55 + 24*Power(t1,2)*(-1 + t2) - 37*t2 - 
                  26*Power(t2,2) + 8*Power(t2,3) + 
                  t1*(-10 + 3*t2 - 32*Power(t2,2))) + 
               s2*(-12*Power(t1,3)*(-1 + t2) + 
                  Power(-1 + t2,2)*(-19 - 43*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(24 - 20*t2 + 35*Power(t2,2)) + 
                  t1*(-102 + 51*t2 + 74*Power(t2,2) - 23*Power(t2,3)))) \
+ Power(s,2)*(-2*Power(s2,5) - 2*Power(t1,5) + 
               Power(s2,4)*(43 + 6*t1 - 16*t2) + 
               Power(t1,4)*(-5 + 37*t2) + 
               Power(-1 + t2,3)*(-37 - 23*t2 + 2*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(43 - 15*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(-81 + 48*t2 + 9*Power(t2,2)) - 
               Power(s2,3)*(-96 + 4*Power(t1,2) + t1*(145 - 32*t2) + 
                  39*t2 + 33*Power(t2,2)) + 
               Power(t1,2)*(-143 + 117*t2 + 81*Power(t2,2) - 
                  55*Power(t2,3)) + 
               Power(s2,2)*(-151 - 4*Power(t1,3) + 213*t2 - 
                  87*Power(t2,2) + 25*Power(t2,3) + 
                  3*Power(t1,2)*(52 + 7*t2) + 
                  12*t1*(-26 + 17*t2 + 3*Power(t2,2))) + 
               s2*(6*Power(t1,4) - Power(t1,3)*(49 + 74*t2) - 
                  Power(-1 + t2,2)*(46 - 15*t2 + 2*Power(t2,2)) - 
                  3*Power(t1,2)*(-99 + 71*t2 + 4*Power(t2,2)) + 
                  4*t1*(79 - 99*t2 + 18*Power(t2,2) + 2*Power(t2,3)))) + 
            s*(4*Power(s2,5)*(-1 + t2) - 4*Power(t1,5)*(-1 + t2) - 
               2*Power(-1 + t2,4)*(11 + 2*t2) + 
               Power(s2,4)*(-65 - 20*t1*(-1 + t2) + 69*t2 - 
                  3*Power(t2,2)) + 
               t1*Power(-1 + t2,3)*(-81 - 72*t2 + 4*Power(t2,2)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (-139 + t2 + 23*Power(t2,2)) + 
               Power(t1,4)*(-14 - 27*t2 + 42*Power(t2,2)) - 
               Power(t1,3)*(42 + 6*t2 - 73*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(s2,3)*(55 + 40*Power(t1,2)*(-1 + t2) - 71*t2 + 
                  42*Power(t2,2) - 26*Power(t2,3) - 
                  2*t1*(-115 + 111*t2 + 6*Power(t2,2))) + 
               Power(s2,2)*(-40*Power(t1,3)*(-1 + t2) - 
                  Power(-1 + t2,2)*(-145 + 19*t2 + 11*Power(t2,2)) + 
                  3*Power(t1,2)*(-93 + 70*t2 + 25*Power(t2,2)) + 
                  t1*(-143 + 109*t2 + 16*Power(t2,2) + 18*Power(t2,3))) \
+ s2*(20*Power(t1,4)*(-1 + t2) - 
                  Power(-1 + t2,3)*(-83 - 70*t2 + 4*Power(t2,2)) + 
                  2*t1*Power(-1 + t2,2)*
                   (-146 + 18*t2 + 13*Power(t2,2)) - 
                  2*Power(t1,3)*(-64 + 15*t2 + 51*Power(t2,2)) + 
                  Power(t1,2)*
                   (130 - 32*t2 - 131*Power(t2,2) + 33*Power(t2,3))))) - 
         Power(s1,2)*Power(-1 + s + t2,2)*
          (-32 + 59*t1 - 15*Power(t1,2) - 15*Power(t1,3) - 
            5*Power(t1,4) + 50*t2 - 154*t1*t2 + 138*Power(t1,2)*t2 + 
            90*Power(t1,3)*t2 + 4*Power(t1,4)*t2 + 137*Power(t2,2) + 
            65*t1*Power(t2,2) - 364*Power(t1,2)*Power(t2,2) - 
            92*Power(t1,3)*Power(t2,2) - 377*Power(t2,3) + 
            178*t1*Power(t2,3) + 327*Power(t1,2)*Power(t2,3) + 
            18*Power(t1,3)*Power(t2,3) + 289*Power(t2,4) - 
            230*t1*Power(t2,4) - 86*Power(t1,2)*Power(t2,4) - 
            53*Power(t2,5) + 82*t1*Power(t2,5) - 14*Power(t2,6) + 
            Power(s2,4)*t2*(6 - 12*t2 + 5*Power(t2,2)) + 
            Power(s2,3)*(32 - 2*(65 + t1)*t2 + 
               (146 + 11*t1)*Power(t2,2) - (53 + 5*t1)*Power(t2,3) + 
               4*Power(t2,4)) + 
            2*Power(s,5)*(8 - 3*Power(t1,2) - 2*t1*(-2 + t2) - 12*t2 + 
               5*Power(t2,2) + 4*s2*(-2 + t1 + t2)) - 
            Power(s2,2)*(3 - 40*t2 + 146*Power(t2,2) - 
               131*Power(t2,3) + 16*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,2)*(10 - 5*t2 + Power(t2,2)) + 
               t1*(69 - 307*t2 + 316*Power(t2,2) - 77*Power(t2,3) - 
                  4*Power(t2,4))) + 
            s2*(Power(t1,3)*(15 - 13*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(61 - 302*t2 + 313*Power(t2,2) - 
                  75*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-17 + 94*t2 - 127*Power(t2,2) + 11*Power(t2,3)) + 
               t1*(-19 - 53*t2 + 363*Power(t2,2) - 397*Power(t2,3) + 
                  106*Power(t2,4))) + 
            Power(s,4)*(-70 + Power(t1,3) + Power(t1,2)*(43 - 37*t2) + 
               175*t2 - 125*Power(t2,2) + 13*Power(t2,3) - 
               6*Power(s2,2)*(-5 + 2*t1 + 2*t2) + 
               t1*(-11 + 34*t2 + 23*Power(t2,2)) + 
               s2*(40 + 14*Power(t1,2) - 67*t2 - 2*Power(t2,2) + 
                  t1*(-73 + 36*t2))) + 
            Power(s,3)*(119 + Power(t1,4) + 2*Power(s2,3)*(-7 + 2*t1) - 
               9*Power(t1,3)*(-1 + t2) - 288*t2 + 340*Power(t2,2) - 
               164*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,2)*(-63 + 166*t2 - 29*Power(t2,2)) - 
               Power(s2,2)*(108 - 59*t1 + 2*Power(t1,2) - 222*t2 + 
                  46*t1*t2 + 10*Power(t2,2)) + 
               t1*(-24 + 87*t2 - 59*Power(t2,2) + 33*Power(t2,3)) + 
               s2*(5 - 3*Power(t1,3) + 72*Power(t1,2)*(-1 + t2) - 
                  175*t2 + 108*Power(t2,2) + 2*Power(t2,3) + 
                  t1*(198 - 340*t2 - 23*Power(t2,2)))) + 
            Power(s,2)*(-118 + Power(t1,4)*(-7 + t2) + 121*t2 + 
               4*Power(s2,4)*t2 + 64*Power(t2,2) + 21*Power(t2,3) - 
               89*Power(t2,4) + Power(t2,5) + 
               Power(t1,3)*(-30 + 87*t2 - 10*Power(t2,2)) + 
               2*Power(t1,2)*
                (16 - 52*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               t1*(109 - 370*t2 + 369*Power(t2,2) - 17*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,3)*(48 - 88*t2 - 7*Power(t2,2) + 
                  t1*(-8 + 7*t2)) - 
               Power(s2,2)*(-146 + 431*t2 - 262*Power(t2,2) + 
                  17*Power(t2,3) + 2*Power(t1,2)*(3 + 5*t2) + 
                  t1*(121 - 280*t2 + 19*Power(t2,2))) + 
               s2*(-82 + Power(t1,3)*(21 - 2*t2) + 705*t2 - 
                  1018*Power(t2,2) + 281*Power(t2,3) + 17*Power(t2,4) + 
                  Power(t1,2)*(139 - 348*t2 + 69*Power(t2,2)) - 
                  t1*(277 - 607*t2 + 162*Power(t2,2) + 68*Power(t2,3)))) \
+ s*(Power(t1,4)*(11 - 5*t2) + Power(s2,4)*t2*(-10 + 9*t2) + 
               Power(t1,3)*(35 - 167*t2 + 84*Power(t2,2)) + 
               Power(t1,2)*(23 - 141*t2 + 362*Power(t2,2) - 
                  175*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-81 - 160*t2 + 274*Power(t2,2) + 34*Power(t2,3)) + 
               2*t1*(-64 + 207*t2 - 206*Power(t2,2) - 2*Power(t2,3) + 
                  65*Power(t2,4)) - 
               Power(s2,3)*(66 - 211*t2 + 115*Power(t2,2) + 
                  3*Power(t2,3) + t1*(-4 + 5*t2 + 2*Power(t2,2))) + 
               Power(s2,2)*(-51 + 200*t2 - 133*Power(t2,2) + 
                  78*Power(t2,3) - 25*Power(t2,4) + 
                  Power(t1,2)*(18 + 5*t2 - 8*Power(t2,2)) + 
                  t1*(143 - 526*t2 + 262*Power(t2,2) + 19*Power(t2,3))) \
+ s2*(57 - 604*t2 + 1298*Power(t2,2) - 909*Power(t2,3) + 
                  153*Power(t2,4) + 5*Power(t2,5) + 
                  Power(t1,3)*(-33 + 15*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-142 + 569*t2 - 315*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(135 - 291*t2 - 128*Power(t2,2) + 163*Power(t2,3) - 
                     17*Power(t2,4))))) + 
         Power(s1,6)*(Power(s,6)*
             (6 + 2*Power(s2,2) + 3*Power(t1,2) - t2 - 9*Power(t2,2) + 
               s2*(-8 - 9*t1 + t2) + t1*(9 + 6*t2)) + 
            Power(s,5)*(29 - 6*Power(s2,3) - 11*Power(t1,3) - 
               9*Power(t2,2) - 25*Power(t2,3) + 
               Power(s2,2)*(36 + 19*t1 + 8*t2) + 
               Power(t1,2)*(-10 + 43*t2) + 
               t1*(14 + 39*t2 - 7*Power(t2,2)) + 
               s2*(-69 + Power(t1,2) + 5*t2 + 31*Power(t2,2) - 
                  2*t1*(13 + 31*t2))) + 
            Power(s,4)*(-90 + 6*Power(s2,4) - 10*Power(t1,4) + 250*t2 - 
               135*Power(t2,2) - 2*Power(t2,3) - 23*Power(t2,4) - 
               2*Power(t1,3)*(24 + 7*t2) - 
               Power(s2,3)*(24 + 11*t1 + 27*t2) - 
               Power(s2,2)*(54 + 17*Power(t1,2) + t1*(31 - 121*t2) - 
                  37*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(-20 - 85*t2 + 99*Power(t2,2)) + 
               t1*(-38 + 5*t2 + 149*Power(t2,2) - 52*Power(t2,3)) + 
               s2*(172 + 32*Power(t1,3) + Power(t1,2)*(99 - 76*t2) - 
                  325*t2 + 7*Power(t2,2) + 82*Power(t2,3) + 
                  t1*(95 + 28*t2 - 82*Power(t2,2)))) + 
            Power(s,3)*(-2*Power(s2,5) + 2*Power(t1,5) + 
               Power(s2,4)*(-16 + t1 + 26*t2) - 
               Power(t1,4)*(8 + 37*t2) + 
               Power(s2,3)*(224 + 13*Power(t1,2) + t1*(67 - 74*t2) - 
                  59*t2 - 31*Power(t2,2)) + 
               Power(t1,3)*(90 - 258*t2 + 31*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (-46 + 116*t2 + 9*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(38 + 9*t2 - 26*Power(t2,2) + 
                  70*Power(t2,3)) + 
               t1*(138 + 115*t2 - 497*Power(t2,2) + 303*Power(t2,3) - 
                  59*Power(t2,4)) - 
               Power(s2,2)*(59 + 19*Power(t1,3) - 
                  Power(t1,2)*(-62 + t2) - 258*t2 + 31*Power(t2,2) + 
                  77*Power(t2,3) + t1*(387 + 240*t2 - 222*Power(t2,2))) \
+ s2*(-258 + 5*Power(t1,4) + 349*t2 - 55*Power(t2,2) - 
                  111*Power(t2,3) + 75*Power(t2,4) + 
                  Power(t1,3)*(19 + 84*t2) + 
                  Power(t1,2)*(65 + 573*t2 - 230*Power(t2,2)) + 
                  2*t1*t2*(-101 - 5*t2 + 15*Power(t2,2)))) + 
            s*(Power(s2,5)*(6 + 4*t2 - 10*Power(t2,2)) + 
               2*Power(t1,5)*(-3 + 2*t2 + Power(t2,2)) - 
               Power(-1 + t2,4)*(-63 - 60*t2 + 4*Power(t2,2)) + 
               2*t1*Power(-1 + t2,3)*(-43 + 83*t2 + 25*Power(t2,2)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (280 + 342*t2 - 230*Power(t2,2) + 11*Power(t2,3)) - 
               Power(t1,4)*(58 - 147*t2 + 76*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(t1,3)*(109 - 290*t2 + 337*Power(t2,2) - 
                  182*Power(t2,3) + 26*Power(t2,4)) + 
               Power(s2,3)*(54 - 69*t2 - 59*Power(t2,2) + 
                  51*Power(t2,3) + 23*Power(t2,4) + 
                  Power(t1,2)*(159 - 190*t2 + 31*Power(t2,2)) + 
                  t1*(-287 + 204*t2 + 217*Power(t2,2) - 
                     118*Power(t2,3))) + 
               Power(s2,4)*(3*t1*(-19 + 14*t2 + 5*Power(t2,2)) + 
                  2*(59 - 74*t2 + 3*Power(t2,2) + 10*Power(t2,3))) + 
               s2*(3*Power(t1,4)*(25 - 34*t2 + 9*Power(t2,2)) - 
                  Power(-1 + t2,3)*(-87 + 187*t2 + 30*Power(t2,2)) + 
                  Power(t1,3)*
                   (57 - 362*t2 + 357*Power(t2,2) - 36*Power(t2,3)) + 
                  2*t1*Power(-1 + t2,2)*
                   (339 + 212*t2 - 170*Power(t2,2) + 22*Power(t2,3)) + 
                  Power(t1,2)*
                   (-110 + 369*t2 - 631*Power(t2,2) + 
                     421*Power(t2,3) - 49*Power(t2,4))) - 
               Power(s2,2)*(Power(t1,3)*
                   (177 - 242*t2 + 65*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (389 + 92*t2 - 103*Power(t2,2) + 25*Power(t2,3)) - 
                  Power(t1,2)*
                   (170 + 159*t2 - 504*Power(t2,2) + 151*Power(t2,3)) + 
                  t1*(64 - 34*t2 - 287*Power(t2,2) + 246*Power(t2,3) + 
                     11*Power(t2,4)))) + 
            (-1 + t2)*(4*Power(t1,5)*(-1 + t2) + 
               2*Power(-1 + t2,4)*(5 + t2) + 
               Power(t1,4)*(-40 + 87*t2 - 39*Power(t2,2)) - 
               4*Power(s2,5)*(-4 + 3*t2 + Power(t2,2)) - 
               t1*Power(-1 + t2,3)*(-39 - 65*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-80 - 8*t2 + 55*Power(t2,2)) - 
               Power(t1,3)*(-78 + 17*t2 + 59*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,4)*(18 + 8*t2 - 21*Power(t2,2) + 
                  3*Power(t2,3) + 7*t1*(-11 + 10*t2 + Power(t2,2))) + 
               Power(s2,3)*(-62 - 9*t2 + 72*Power(t2,2) - 
                  10*Power(t2,3) + 9*Power(t2,4) + 
                  Power(t1,2)*(145 - 154*t2 + 9*Power(t2,2)) - 
                  t1*(12 + 139*t2 - 152*Power(t2,2) + 33*Power(t2,3))) \
+ Power(s2,2)*(Power(t1,3)*(-127 + 150*t2 - 23*Power(t2,2)) + 
                  Power(-1 + t2,2)*(-101 + 33*t2 + 35*Power(t2,2)) + 
                  Power(t1,2)*
                   (-71 + 344*t2 - 283*Power(t2,2) + 58*Power(t2,3)) + 
                  t1*(188 + 49*t2 - 263*Power(t2,2) + 50*Power(t2,3) - 
                     24*Power(t2,4))) + 
               s2*(Power(-1 + t2,3)*(-39 - 65*t2 + 4*Power(t2,2)) + 
                  Power(t1,4)*(47 - 58*t2 + 11*Power(t2,2)) - 
                  t1*Power(-1 + t2,2)*(-183 + 29*t2 + 88*Power(t2,2)) + 
                  Power(t1,3)*
                   (105 - 300*t2 + 191*Power(t2,2) - 28*Power(t2,3)) + 
                  Power(t1,2)*
                   (-206 - 15*t2 + 238*Power(t2,2) - 30*Power(t2,3) + 
                     13*Power(t2,4)))) + 
            Power(s,2)*(Power(s2,5)*(12 - 8*t2) + 4*Power(t1,5)*t2 + 
               Power(t1,4)*(24 - 45*t2 - 44*Power(t2,2)) - 
               Power(-1 + t2,3)*(-44 - 95*t2 + 5*Power(t2,2)) - 
               2*t1*Power(-1 + t2,2)*
                (136 + 85*t2 - 99*Power(t2,2) + 10*Power(t2,3)) + 
               5*Power(t1,3)*
                (-22 + 95*t2 - 78*Power(t2,2) + 12*Power(t2,3)) + 
               3*Power(t1,2)*
                (43 + 69*t2 - 194*Power(t2,2) + 82*Power(t2,3)) + 
               Power(s2,4)*(-102 + 14*t2 + 37*Power(t2,2) + 
                  3*t1*(-7 + 3*t2)) + 
               Power(s2,3)*(-262 + 188*t2 + 35*Power(t2,2) + 
                  4*Power(t2,3) + Power(t1,2)*(-27 + 35*t2) + 
                  t1*(267 + 99*t2 - 148*Power(t2,2))) + 
               Power(s2,2)*(303 + Power(t1,3)*(69 - 61*t2) - 457*t2 + 
                  146*Power(t2,2) + 86*Power(t2,3) - 78*Power(t2,4) + 
                  3*Power(t1,2)*(-78 - 75*t2 + 37*Power(t2,2)) + 
                  t1*(507 - 6*t2 - 529*Power(t2,2) + 133*Power(t2,3))) + 
               s2*(3*Power(t1,4)*(-11 + 7*t2) + 
                  Power(t1,3)*(45 + 157*t2 + 44*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (313 + 21*t2 - 93*Power(t2,2) + 23*Power(t2,3)) - 
                  Power(t1,2)*
                   (117 + 711*t2 - 938*Power(t2,2) + 215*Power(t2,3)) + 
                  t1*(-435 + 234*t2 + 502*Power(t2,2) - 
                     404*Power(t2,3) + 103*Power(t2,4))))) + 
         Power(s1,3)*(-1 + s + t2)*
          (53 - 130*t1 + 134*Power(t1,2) - 71*Power(t1,3) - 
            22*Power(t1,4) - 2*Power(t1,5) - 190*t2 + 256*t1*t2 - 
            383*Power(t1,2)*t2 + 309*Power(t1,3)*t2 + 
            85*Power(t1,4)*t2 + 2*Power(t1,5)*t2 + 140*Power(t2,2) + 
            157*t1*Power(t2,2) + 347*Power(t1,2)*Power(t2,2) - 
            548*Power(t1,3)*Power(t2,2) - 92*Power(t1,4)*Power(t2,2) + 
            278*Power(t2,3) - 677*t1*Power(t2,3) + 
            57*Power(t1,2)*Power(t2,3) + 444*Power(t1,3)*Power(t2,3) + 
            28*Power(t1,4)*Power(t2,3) - 567*Power(t2,4) + 
            479*t1*Power(t2,4) - 293*Power(t1,2)*Power(t2,4) - 
            134*Power(t1,3)*Power(t2,4) + 370*Power(t2,5) - 
            55*t1*Power(t2,5) + 138*Power(t1,2)*Power(t2,5) - 
            82*Power(t2,6) - 30*t1*Power(t2,6) - 2*Power(t2,7) - 
            2*Power(s2,5)*t2*(2 - 3*t2 + Power(t2,2)) + 
            Power(s2,4)*(-34 + (107 + 9*t1)*t2 - 
               4*(28 + 3*t1)*Power(t2,2) + (43 + 3*t1)*Power(t2,3) - 
               5*Power(t2,4)) + 
            6*Power(s,6)*(4 - Power(t1,2) - 2*t1*(-1 + t2) - 6*t2 + 
               3*Power(t2,2) + 2*s2*(-2 + t1 + t2)) + 
            Power(s2,3)*(-31 + 117*t2 - 170*Power(t2,2) + 
               135*Power(t2,3) - 64*Power(t2,4) + 13*Power(t2,5) + 
               Power(t1,2)*(5 - 14*t2 + 9*Power(t2,2)) + 
               t1*(112 - 362*t2 + 363*Power(t2,2) - 111*Power(t2,3) + 
                  2*Power(t2,4))) + 
            s2*(3*Power(t1,4)*(3 - 4*t2 + Power(t2,2)) + 
               Power(t1,3)*(97 - 353*t2 + 374*Power(t2,2) - 
                  114*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (177 - 409*t2 + 494*Power(t2,2) + 18*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-92 - 140*t2 + 114*Power(t2,2) + 29*Power(t2,3)) + 
               Power(t1,2)*(-20 - 37*t2 + 333*Power(t2,2) - 
                  448*Power(t2,3) + 172*Power(t2,4))) + 
            Power(s2,2)*(-(Power(t1,3)*
                  (12 - 19*t2 + 6*Power(t2,2) + Power(t2,3))) + 
               Power(t1,2)*(-153 + 523*t2 - 533*Power(t2,2) + 
                  154*Power(t2,3) + 3*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (19 - 204*t2 + 410*Power(t2,2) - 92*Power(t2,3) + 
                  7*Power(t2,4)) + 
               t1*(119 - 379*t2 + 375*Power(t2,2) - 131*Power(t2,3) + 
                  31*Power(t2,4) - 15*Power(t2,5))) - 
            Power(s,5)*(120 + 3*Power(t1,3) - 222*t2 + 
               166*Power(t2,2) - 53*Power(t2,3) + 
               4*Power(s2,2)*(-16 + 6*t1 + 9*t2) + 
               Power(t1,2)*(-84 + 37*t2) + 
               t1*(-18 + 14*t2 + 13*Power(t2,2)) - 
               2*s2*(28 + 15*Power(t1,2) - 9*t2 - 2*Power(t2,2) + 
                  t1*(-81 + 35*t2))) + 
            Power(s,4)*(228 + Power(t1,3)*(50 - 14*t2) - 322*t2 + 
               329*Power(t2,2) - 259*Power(t2,3) + 56*Power(t2,4) + 
               2*Power(s2,3)*(-28 + 7*t1 + 15*t2) + 
               Power(t1,2)*(-106 + 275*t2 - 60*Power(t2,2)) + 
               t1*(-161 + 457*t2 - 210*Power(t2,2) + 18*Power(t2,3)) - 
               Power(s2,2)*(233 + 30*Power(t1,2) - 342*t2 + 
                  86*Power(t2,2) + 4*t1*(-61 + 27*t2)) + 
               s2*(71 + 16*Power(t1,3) - 561*t2 + 391*Power(t2,2) - 
                  75*Power(t2,3) + Power(t1,2)*(-257 + 105*t2) + 
                  t1*(394 - 580*t2 + 98*Power(t2,2)))) - 
            Power(s,3)*(282 - 13*Power(t1,4) + 118*t2 - 
               837*Power(t2,2) + 282*Power(t2,3) + 180*Power(t2,4) - 
               25*Power(t2,5) + 2*Power(s2,4)*(-8 + t1 + 2*t2) + 
               Power(t1,3)*(50 - 105*t2 + 19*Power(t2,2)) + 
               Power(t1,2)*(88 + 61*t2 - 289*Power(t2,2) + 
                  33*Power(t2,3)) + 
               t1*(-389 + 1142*t2 - 1157*Power(t2,2) + 
                  323*Power(t2,3) - 27*Power(t2,4)) - 
               Power(s2,3)*(190 + 3*Power(t1,2) - 352*t2 + 
                  89*Power(t2,2) + 10*t1*(-9 + 5*t2)) + 
               Power(s2,2)*(-355 + 430*t2 - 316*Power(t2,2) + 
                  63*Power(t2,3) + 2*Power(t1,2)*(-80 + 51*t2) + 
                  3*t1*(223 - 326*t2 + 51*Power(t2,2))) + 
               s2*(315 + Power(t1,4) + Power(t1,3)*(99 - 56*t2) - 
                  1762*t2 + 2366*Power(t2,2) - 912*Power(t2,3) + 
                  101*Power(t2,4) + 
                  Power(t1,2)*(-571 + 792*t2 - 102*Power(t2,2)) + 
                  t1*(394 - 516*t2 + 447*Power(t2,2) - 40*Power(t2,3)))) \
- Power(s,2)*(2*Power(t1,5) + Power(t1,4)*(45 - 64*t2) + 
               2*Power(s2,5)*t2 + 
               Power(s2,4)*(48 + t1*(-4 + t2) - 71*t2 + 
                  13*Power(t2,2)) + 
               2*Power(t1,3)*
                (29 - 58*t2 + 47*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(-433 + 821*t2 - 276*Power(t2,2) - 
                  247*Power(t2,3) + 4*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (290 + 809*t2 - 937*Power(t2,2) - 59*Power(t2,3) + 
                  4*Power(t2,4)) + 
               t1*(482 - 1047*t2 + 1121*Power(t2,2) - 
                  720*Power(t2,3) + 172*Power(t2,4) - 8*Power(t2,5)) + 
               Power(s2,3)*(298 + Power(t1,2)*(1 - 6*t2) - 795*t2 + 
                  574*Power(t2,2) - 101*Power(t2,3) + 
                  t1*(-193 + 307*t2 - 60*Power(t2,2))) + 
               Power(s2,2)*(165 + 371*t2 - 874*Power(t2,2) + 
                  202*Power(t2,3) + 5*Power(t2,4) + 
                  Power(t1,3)*(12 + t2) + 
                  Power(t1,2)*(320 - 528*t2 + 111*Power(t2,2)) + 
                  t1*(-982 + 2146*t2 - 1165*Power(t2,2) + 
                     93*Power(t2,3))) + 
               s2*(-347 + 1574*t2 - 3066*Power(t2,2) + 
                  2534*Power(t2,3) - 748*Power(t2,4) + 53*Power(t2,5) + 
                  Power(t1,4)*(-11 + 2*t2) - 
                  4*Power(t1,3)*(55 - 89*t2 + 16*Power(t2,2)) + 
                  Power(t1,2)*
                   (668 - 1328*t2 + 557*Power(t2,2) - 9*Power(t2,3)) + 
                  t1*(177 - 1161*t2 + 1364*Power(t2,2) - 
                     112*Power(t2,3) - 6*Power(t2,4)))) - 
            s*(2*Power(t1,5)*(-2 + t2) + 2*Power(s2,5)*t2*(-3 + 2*t2) + 
               Power(t1,4)*(-54 + 149*t2 - 79*Power(t2,2)) + 
               Power(-1 + t2,3)*
                (-214 - 361*t2 + 534*Power(t2,2) + 18*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-348 - 192*t2 + 185*Power(t2,2) + 63*Power(t2,3)) + 
               Power(t1,3)*(-157 + 482*t2 - 630*Power(t2,2) + 
                  283*Power(t2,3)) + 
               Power(t1,2)*(411 - 1079*t2 + 885*Power(t2,2) + 
                  70*Power(t2,3) - 287*Power(t2,4)) + 
               Power(s2,4)*(-66 + 159*t2 - 98*Power(t2,2) + 
                  14*Power(t2,3) + t1*(2 + 8*t2 - 4*Power(t2,2))) - 
               Power(s2,3)*(170 - 628*t2 + 723*Power(t2,2) - 
                  342*Power(t2,3) + 55*Power(t2,4) + 
                  Power(t1,2)*(-7 + 8*t2 + 3*Power(t2,2)) + 
                  t1*(-229 + 574*t2 - 328*Power(t2,2) + 26*Power(t2,3))) \
+ Power(s2,2)*(2*Power(t1,3)*(-12 + 9*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-343 + 904*t2 - 522*Power(t2,2) + 36*Power(t2,3)) + 
                  t2*(-761 + 2085*t2 - 1655*Power(t2,2) + 
                     346*Power(t2,3) - 15*Power(t2,4)) + 
                  t1*(577 - 1765*t2 + 1545*Power(t2,2) - 
                     462*Power(t2,3) + 39*Power(t2,4))) + 
               s2*(Power(t1,4)*(19 - 14*t2 + Power(t2,2)) + 
                  Power(t1,3)*
                   (234 - 638*t2 + 371*Power(t2,2) - 24*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (221 - 2*t2 + 290*Power(t2,2) - 228*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  Power(t1,2)*
                   (-269 + 710*t2 - 243*Power(t2,2) - 150*Power(t2,3) + 
                     18*Power(t2,4)) - 
                  t1*(424 - 2006*t2 + 3369*Power(t2,2) - 
                     1916*Power(t2,3) + 123*Power(t2,4) + 6*Power(t2,5)))\
)) - Power(s1,4)*(2*Power(s,7)*
             (8 - Power(t1,2) + t1*(4 - 6*t2) - 12*t2 + 7*Power(t2,2) + 
               4*s2*(-2 + t1 + t2)) + 
            Power(s,6)*(-100 + 3*Power(t1,3) + 
               Power(t1,2)*(72 - 34*t2) + 126*t2 - 75*Power(t2,2) + 
               52*Power(t2,3) - 4*Power(s2,2)*(-13 + 5*t1 + 9*t2) + 
               t1*(20 - 77*t2 - 21*Power(t2,2)) + 
               s2*(48 + 14*Power(t1,2) + 38*t2 - 17*Power(t2,2) + 
                  t1*(-128 + 83*t2))) + 
            Power(s,5)*(202 + 3*Power(t1,4) - Power(t1,3)*(-50 + t2) - 
               183*t2 - 17*Power(t2,2) - 42*Power(t2,3) + 
               72*Power(t2,4) + 4*Power(s2,3)*(-16 + 4*t1 + 13*t2) + 
               Power(t1,2)*(-43 + 262*t2 - 95*Power(t2,2)) + 
               t1*(-124 + 554*t2 - 430*Power(t2,2) + 21*Power(t2,3)) - 
               Power(s2,2)*(228 + 26*Power(t1,2) - 168*t2 + 
                  83*Power(t2,2) + 2*t1*(-154 + 81*t2)) + 
               s2*(110 + 7*Power(t1,3) - 503*t2 + 431*Power(t2,2) - 
                  135*Power(t2,3) + 3*Power(t1,2)*(-97 + 32*t2) + 
                  t1*(226 - 343*t2 + 192*Power(t2,2)))) + 
            Power(s,4)*(-252 + Power(s2,4)*(34 - 4*t1 - 28*t2) - 
               358*t2 + 1398*Power(t2,2) - 923*Power(t2,3) + 
               91*Power(t2,4) + 44*Power(t2,5) + 
               3*Power(t1,4)*(-5 + 3*t2) + 
               Power(t1,3)*(-17 + 213*t2 - 21*Power(t2,2)) + 
               Power(t1,2)*(-323 + 308*t2 + 310*Power(t2,2) - 
                  101*Power(t2,3)) + 
               t1*(288 - 949*t2 + 1392*Power(t2,2) - 759*Power(t2,3) + 
                  69*Power(t2,4)) + 
               Power(s2,3)*(240 + 14*Power(t1,2) - 367*t2 + 
                  173*Power(t2,2) + t1*(-219 + 113*t2)) + 
               Power(s2,2)*(458 - 16*Power(t1,3) + 
                  Power(t1,2)*(315 - 114*t2) + 30*t2 - 
                  192*Power(t2,2) + 9*Power(t2,3) + 
                  t1*(-864 + 1287*t2 - 409*Power(t2,2))) + 
               s2*(-560 + 6*Power(t1,4) + 1741*t2 - 1869*Power(t2,2) + 
                  870*Power(t2,3) - 223*Power(t2,4) + 
                  5*Power(t1,3)*(-23 + 4*t2) + 
                  Power(t1,2)*(660 - 1124*t2 + 229*Power(t2,2)) + 
                  t1*(1 - 827*t2 + 199*Power(t2,2) + 128*Power(t2,3)))) \
+ Power(s,3)*(6*Power(t1,5) + Power(s2,5)*(-6 + 4*t2) + 
               Power(s2,4)*(-104 + t1*(39 - 22*t2) + 206*t2 - 
                  99*Power(t2,2)) + 
               Power(t1,4)*(92 - 88*t2 + 9*Power(t2,2)) - 
               Power(t1,3)*(225 + 24*t2 - 362*Power(t2,2) + 
                  27*Power(t2,3)) + 
               Power(t1,2)*(974 - 2225*t2 + 1246*Power(t2,2) + 
                  111*Power(t2,3) - 43*Power(t2,4)) + 
               5*Power(-1 + t2,2)*
                (68 + 240*t2 - 269*Power(t2,2) + 31*Power(t2,3) + 
                  2*Power(t2,4)) + 
               t1*(-491 + 144*t2 + 111*Power(t2,2) + 791*Power(t2,3) - 
                  606*Power(t2,4) + 51*Power(t2,5)) + 
               Power(s2,3)*(-528 + 754*t2 - 538*Power(t2,2) + 
                  191*Power(t2,3) + Power(t1,2)*(-97 + 50*t2) + 
                  t1*(631 - 915*t2 + 280*Power(t2,2))) - 
               Power(s2,2)*(320 + 1057*t2 - 2326*Power(t2,2) + 
                  1051*Power(t2,3) - 165*Power(t2,4) + 
                  Power(t1,3)*(-107 + 50*t2) + 
                  4*Power(t1,2)*(215 - 270*t2 + 52*Power(t2,2)) + 
                  t1*(-1364 + 2356*t2 - 1757*Power(t2,2) + 
                     437*Power(t2,3))) + 
               s2*(905 - 1940*t2 + 2081*Power(t2,2) - 
                  1541*Power(t2,3) + 644*Power(t2,4) - 
                  149*Power(t2,5) + Power(t1,4)*(-49 + 18*t2) + 
                  Power(t1,3)*(241 - 283*t2 + 18*Power(t2,2)) + 
                  Power(t1,2)*
                   (-659 + 1700*t2 - 1585*Power(t2,2) + 
                     251*Power(t2,3)) + 
                  t1*(-849 + 4243*t2 - 4845*Power(t2,2) + 
                     1383*Power(t2,3) - 58*Power(t2,4)))) + 
            Power(s,2)*(4*Power(t1,5)*(-5 + 6*t2) + 
               4*Power(s2,5)*(4 - 6*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(-191 + 376*t2 - 207*Power(t2,2) + 
                  3*Power(t2,3)) + 
               2*Power(-1 + t2,3)*
                (195 + 301*t2 - 477*Power(t2,2) + 30*Power(t2,3)) + 
               Power(t1,3)*(541 - 788*t2 - 72*Power(t2,2) + 
                  371*Power(t2,3) - 10*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (661 + 1652*t2 - 1091*Power(t2,2) - 202*Power(t2,3) + 
                  12*Power(t2,4)) - 
               Power(t1,2)*(1167 - 3284*t2 + 3505*Power(t2,2) - 
                  1431*Power(t2,3) + 38*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,4)*(190 - 454*t2 + 387*Power(t2,2) - 
                  129*Power(t2,3) - 3*t1*(29 - 39*t2 + 14*Power(t2,2))) \
+ Power(s2,3)*(478 - 676*t2 + 198*Power(t2,2) - 97*Power(t2,3) + 
                  55*Power(t2,4) + 
                  Power(t1,2)*(203 - 261*t2 + 66*Power(t2,2)) + 
                  t1*(-1010 + 2132*t2 - 1407*Power(t2,2) + 
                     322*Power(t2,3))) + 
               Power(s2,2)*(15 + 1512*t2 - 4291*Power(t2,2) + 
                  3747*Power(t2,3) - 1134*Power(t2,4) + 
                  151*Power(t2,5) + 
                  Power(t1,3)*(-229 + 291*t2 - 54*Power(t2,2)) + 
                  Power(t1,2)*
                   (1271 - 2496*t2 + 1350*Power(t2,2) - 
                     200*Power(t2,3)) + 
                  t1*(-767 + 1281*t2 - 903*Power(t2,2) + 
                     692*Power(t2,3) - 177*Power(t2,4))) + 
               s2*(3*Power(t1,4)*(39 - 49*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*
                   (-260 + 442*t2 - 123*Power(t2,2) + 4*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (917 + 743*t2 - 629*Power(t2,2) - 35*Power(t2,3) + 
                     36*Power(t2,4)) + 
                  3*Power(t1,2)*
                   (-69 + 17*t2 + 301*Power(t2,2) - 334*Power(t2,3) + 
                     43*Power(t2,4)) + 
                  t1*(1305 - 5723*t2 + 9612*Power(t2,2) - 
                     6594*Power(t2,3) + 1499*Power(t2,4) - 
                     99*Power(t2,5)))) - 
            (-1 + t2)*(-4*Power(t1,5)*(2 - 5*t2 + 3*Power(t2,2)) + 
               3*Power(-1 + t2,4)*(-6 + 16*t2 + 7*Power(t2,2)) - 
               4*Power(s2,5)*
                (-3 + 4*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               t1*Power(-1 + t2,3)*
                (-154 - 141*t2 + 208*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-203 - 33*t2 - 38*Power(t2,2) + 18*Power(t2,3)) + 
               Power(t1,4)*(-61 + 170*t2 - 177*Power(t2,2) + 
                  76*Power(t2,3)) + 
               Power(t1,3)*(128 - 288*t2 + 156*Power(t2,2) + 
                  90*Power(t2,3) - 86*Power(t2,4)) + 
               Power(s2,4)*(51 - 133*t2 + 137*Power(t2,2) - 
                  62*Power(t2,3) + 15*Power(t2,4) + 
                  t1*(-53 + 72*t2 - 29*Power(t2,2) + 10*Power(t2,3))) + 
               Power(s2,3)*(9 + 133*t2 - 429*Power(t2,2) + 
                  389*Power(t2,3) - 118*Power(t2,4) + 16*Power(t2,5) + 
                  Power(t1,2)*
                   (99 - 150*t2 + 59*Power(t2,2) - 8*Power(t2,3)) + 
                  t1*(-221 + 590*t2 - 561*Power(t2,2) + 
                     197*Power(t2,3) - 37*Power(t2,4))) + 
               s2*(Power(t1,4)*(45 - 94*t2 + 49*Power(t2,2)) + 
                  Power(t1,3)*
                   (1 - 17*t2 + 82*Power(t2,2) - 98*Power(t2,3)) + 
                  Power(-1 + t2,3)*
                   (166 + 115*t2 - 207*Power(t2,2) + 9*Power(t2,3)) - 
                  t1*Power(-1 + t2,2)*
                   (-292 - 105*t2 - 177*Power(t2,2) + 62*Power(t2,3)) \
+ Power(t1,2)*(-262 + 816*t2 - 956*Power(t2,2) + 362*Power(t2,3) + 
                     40*Power(t2,4))) + 
               Power(s2,2)*(Power(t1,3)*
                   (-95 + 168*t2 - 75*Power(t2,2) + 2*Power(t2,3)) + 
                  2*Power(-1 + t2,2)*
                   (-41 - 55*t2 - 42*Power(t2,2) + 10*Power(t2,3)) + 
                  Power(t1,2)*
                   (230 - 610*t2 + 519*Power(t2,2) - 113*Power(t2,3) + 
                     22*Power(t2,4)) + 
                  t1*(128 - 675*t2 + 1255*Power(t2,2) - 
                     865*Power(t2,3) + 175*Power(t2,4) - 18*Power(t2,5))\
)) + s*(Power(t1,5)*(22 - 52*t2 + 30*Power(t2,2)) + 
               Power(t1,4)*(184 - 513*t2 + 537*Power(t2,2) - 
                  210*Power(t2,3)) + 
               2*Power(s2,5)*
                (-11 + 20*t2 - 15*Power(t2,2) + 6*Power(t2,3)) + 
               Power(-1 + t2,4)*
                (190 - 23*t2 - 278*Power(t2,2) + 7*Power(t2,3)) - 
               2*t1*Power(-1 + t2,3)*
                (-267 - 514*t2 + 457*Power(t2,2) + 19*Power(t2,3)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (-704 + 614*t2 - 534*Power(t2,2) + 47*Power(t2,3)) + 
               Power(t1,3)*(-450 + 1095*t2 - 662*Power(t2,2) - 
                  241*Power(t2,3) + 258*Power(t2,4)) + 
               Power(s2,4)*(-159 + 487*t2 - 549*Power(t2,2) + 
                  292*Power(t2,3) - 73*Power(t2,4) + 
                  t1*(105 - 188*t2 + 117*Power(t2,2) - 34*Power(t2,3))) \
+ Power(s2,3)*(-165 + 38*t2 + 709*Power(t2,2) - 823*Power(t2,3) + 
                  272*Power(t2,4) - 31*Power(t2,5) + 
                  Power(t1,2)*
                   (-219 + 412*t2 - 231*Power(t2,2) + 38*Power(t2,3)) + 
                  t1*(755 - 2237*t2 + 2259*Power(t2,2) - 
                     945*Power(t2,3) + 176*Power(t2,4))) + 
               Power(s2,2)*(Power(t1,3)*
                   (233 - 472*t2 + 261*Power(t2,2) - 22*Power(t2,3)) - 
                  6*Power(t1,2)*
                   (143 - 416*t2 + 378*Power(t2,2) - 120*Power(t2,3) + 
                     17*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (117 - 382*t2 + 1127*Power(t2,2) - 
                     327*Power(t2,3) + 42*Power(t2,4)) + 
                  t1*(-59 + 982*t2 - 2284*Power(t2,2) + 
                     1629*Power(t2,3) - 279*Power(t2,4) + 11*Power(t2,5)\
)) + s2*(Power(t1,4)*(-119 + 260*t2 - 147*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(-1 + t2,3)*
                   (614 + 773*t2 - 830*Power(t2,2) + 53*Power(t2,3)) + 
                  Power(t1,3)*
                   (78 - 233*t2 + 21*Power(t2,2) + 143*Power(t2,3) - 
                     Power(t2,4)) - 
                  t1*Power(-1 + t2,2)*
                   (879 - 1294*t2 + 2071*Power(t2,2) - 
                     532*Power(t2,3) + 30*Power(t2,4)) + 
                  Power(t1,2)*
                   (655 - 2034*t2 + 2103*Power(t2,2) - 459*Power(t2,3) - 
                     290*Power(t2,4) + 25*Power(t2,5))))) + 
         Power(s1,5)*(2*Power(s,7)*
             (2 + t1 - 3*t2 - 2*t1*t2 + 2*Power(t2,2) + 
               s2*(-2 + t1 + t2)) + 
            Power(s,6)*(-36 + 4*Power(t1,3) + Power(t1,2)*(19 - 16*t2) + 
               18*t2 + 17*Power(t2,2) + 12*Power(t2,3) - 
               6*Power(s2,2)*(-2 + t1 + 2*t2) - 2*t1*(3 + 28*t2) - 
               s2*(-24 + Power(t1,2) + t1*(20 - 34*t2) - 26*t2 + 
                  13*Power(t2,2))) + 
            Power(s,5)*(28 + 35*Power(t1,3) + 4*Power(t1,4) - 23*t2 - 
               112*Power(t2,2) + 109*Power(t2,3) + 12*Power(t2,4) + 
               2*Power(s2,3)*(-8 + 3*t1 + 12*t2) + 
               Power(t1,2)*(60 + 2*t2 - 40*Power(t2,2)) + 
               Power(s2,2)*(-102 + 2*Power(t1,2) + t1*(94 - 87*t2) - 
                  29*t2 - 3*Power(t2,2)) + 
               3*t1*(3 + 31*t2 - 62*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(110 - 12*Power(t1,3) - 106*t2 + 69*Power(t2,2) - 
                  51*Power(t2,3) + 5*Power(t1,2)*(-23 + 11*t2) + 
                  t1*(-15 + 113*t2 + 48*Power(t2,2)))) - 
            Power(s,4)*(-40 + 401*t2 - 646*Power(t2,2) + 
               450*Power(t2,3) - 161*Power(t2,4) - 4*Power(t2,5) - 
               3*Power(t1,4)*(-5 + 4*t2) + 
               2*Power(s2,4)*(-6 + t1 + 10*t2) + 
               Power(s2,3)*(-86 + Power(t1,2) + t1*(120 - 88*t2) + 
                  26*t2 - 47*Power(t2,2)) + 
               2*Power(t1,3)*(-23 - 91*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(203 - 545*t2 + 212*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-25 - 111*t2 + 61*Power(t2,2) + 156*Power(t2,3) - 
                  32*Power(t2,4)) - 
               Power(s2,2)*(250 + 8*Power(t1,3) + 
                  Power(t1,2)*(176 - 68*t2) + 105*t2 - 
                  251*Power(t2,2) + 63*Power(t2,3) + 
                  t1*(-127 + 215*t2 - 177*Power(t2,2))) + 
               s2*(448 + 5*Power(t1,4) - 714*t2 + 90*Power(t2,2) + 
                  72*Power(t2,3) + 55*Power(t2,4) + 
                  Power(t1,3)*(53 + 12*t2) - 
                  2*Power(t1,2)*(10 - 197*t2 + 76*Power(t2,2)) + 
                  t1*(-32 + 938*t2 - 673*Power(t2,2) + 40*Power(t2,3)))) \
+ (-1 + t2)*(-12*Power(t1,5)*Power(-1 + t2,2) + 
               Power(-1 + t2,4)*(-18 - 29*t2 + 2*Power(t2,2)) - 
               t1*Power(-1 + t2,3)*(-63 + 71*t2 + 59*Power(t2,2)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (-166 - 197*t2 + 131*Power(t2,2) + 2*Power(t2,3)) + 
               2*Power(s2,5)*
                (-11 + 13*t2 - 5*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,4)*(46 - 64*t2 - 15*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,3)*(-143 + 289*t2 - 275*Power(t2,2) + 
                  131*Power(t2,3) - 2*Power(t2,4)) + 
               Power(s2,4)*(-32 + 7*t2 + 38*Power(t2,2) - 
                  37*Power(t2,3) + 7*Power(t2,4) + 
                  t1*(116 - 161*t2 + 70*Power(t2,2) - 25*Power(t2,3))) + 
               Power(s2,3)*(Power(t1,2)*
                   (-205 + 298*t2 - 125*Power(t2,2) + 32*Power(t2,3)) + 
                  t1*(67 + 27*t2 - 115*Power(t2,2) + 107*Power(t2,3) - 
                     18*Power(t2,4)) + 
                  2*(5 - 7*t2 + 67*Power(t2,2) - 71*Power(t2,3) + 
                     6*Power(t2,4))) - 
               Power(s2,2)*(-2*Power(-1 + t2,2)*
                   (87 + 82*t2 - 65*Power(t2,2) + 11*Power(t2,3)) + 
                  Power(t1,3)*
                   (-138 + 193*t2 - 68*Power(t2,2) + 13*Power(t2,3)) + 
                  Power(t1,2)*
                   (3 + 102*t2 - 56*Power(t2,2) + 64*Power(t2,3) - 
                     11*Power(t2,4)) + 
                  2*t1*(88 - 198*t2 + 351*Power(t2,2) - 
                     274*Power(t2,3) + 33*Power(t2,4))) + 
               s2*(3*Power(t1,4)*(-5 + 2*t2 + 3*Power(t2,2)) + 
                  Power(-1 + t2,3)*(-73 + 92*t2 + 48*Power(t2,2)) + 
                  Power(t1,3)*
                   (-78 + 132*t2 + 36*Power(t2,2) - 22*Power(t2,3)) - 
                  t1*Power(-1 + t2,2)*
                   (331 + 381*t2 - 274*Power(t2,2) + 22*Power(t2,3)) + 
                  Power(t1,2)*
                   (314 - 691*t2 + 873*Power(t2,2) - 557*Power(t2,3) + 
                     61*Power(t2,4)))) + 
            Power(s,3)*(-18*Power(t1,5) + Power(s2,5)*(-4 + 6*t2) + 
               Power(s2,4)*(-23 + t1*(44 - 31*t2) + 59*t2 - 
                  53*Power(t2,2)) + 
               Power(t1,4)*(66 + 5*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(-269 + 199*t2 + 264*Power(t2,2) - 
                  32*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (20 + 438*t2 - 349*Power(t2,2) + 93*Power(t2,3)) + 
               Power(t1,2)*(385 - 1223*t2 + 1168*Power(t2,2) - 
                  394*Power(t2,3) + 8*Power(t2,4)) + 
               2*t1*(-109 - 402*t2 + 934*Power(t2,2) - 
                  444*Power(t2,3) + 15*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,3)*(-396 + 38*t2 + 163*Power(t2,2) - 
                  3*Power(t2,3) + Power(t1,2)*(-70 + 29*t2) + 
                  t1*(257 - 461*t2 + 210*Power(t2,2))) + 
               Power(s2,2)*(-80 - 513*t2 + 770*Power(t2,2) - 
                  320*Power(t2,3) + 87*Power(t2,4) + 
                  Power(t1,3)*(6 + 11*t2) + 
                  Power(t1,2)*(-411 + 738*t2 - 205*Power(t2,2)) + 
                  t1*(451 + 488*t2 - 312*Power(t2,2) - 69*Power(t2,3))) \
- s2*(-713 + 851*t2 + 294*Power(t2,2) - 718*Power(t2,3) + 
                  267*Power(t2,4) + 19*Power(t2,5) + 
                  3*Power(t1,4)*(-14 + 5*t2) + 
                  Power(t1,3)*(-111 + 341*t2 - 36*Power(t2,2)) + 
                  Power(t1,2)*
                   (-186 + 657*t2 + 167*Power(t2,2) - 116*Power(t2,3)) + 
                  t1*(331 - 2007*t2 + 2405*Power(t2,2) - 
                     939*Power(t2,3) + 98*Power(t2,4)))) + 
            Power(s,2)*(-48*Power(t1,5)*(-1 + t2) + 
               6*Power(s2,5)*(1 - 4*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(-88 + 45*t2 + 71*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*Power(-1 + t2,3)*
                (47 - 23*t2 - 93*Power(t2,2) + 9*Power(t2,3)) + 
               2*t1*Power(-1 + t2,2)*
                (265 + 617*t2 - 527*Power(t2,2) + 38*Power(t2,3)) + 
               Power(t1,3)*(566 - 1166*t2 + 497*Power(t2,2) + 
                  120*Power(t2,3) - 12*Power(t2,4)) + 
               Power(t1,2)*(-420 + 772*t2 - 839*Power(t2,2) + 
                  720*Power(t2,3) - 241*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,4)*(139 - 108*t2 + 38*Power(t2,2) - 
                  39*Power(t2,3) - 3*t1*(34 - 61*t2 + 27*Power(t2,2))) + 
               Power(s2,3)*(417 - 148*t2 - 575*Power(t2,2) + 
                  352*Power(t2,3) - 51*Power(t2,4) + 
                  3*Power(t1,2)*(68 - 99*t2 + 31*Power(t2,2)) + 
                  t1*(-597 + 750*t2 - 437*Power(t2,2) + 162*Power(t2,3))\
) - Power(s2,2)*(210 - 533*t2 + 741*Power(t2,2) - 426*Power(t2,3) + 
                  41*Power(t2,4) - 33*Power(t2,5) + 
                  3*Power(t1,3)*(26 - 31*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (-763 + 1263*t2 - 873*Power(t2,2) + 187*Power(t2,3)) \
+ t1*(166 + 1479*t2 - 2544*Power(t2,2) + 959*Power(t2,3) - 
                     75*Power(t2,4))) + 
               s2*(-3*Power(t1,4)*(26 - 31*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*
                   (-217 + 576*t2 - 545*Power(t2,2) + 60*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (803 + 534*t2 - 747*Power(t2,2) + 196*Power(t2,3)) + 
                  Power(t1,2)*
                   (-818 + 2783*t2 - 2430*Power(t2,2) + 
                     449*Power(t2,3) + Power(t2,4)) + 
                  t1*(597 - 1310*t2 + 1794*Power(t2,2) - 
                     1428*Power(t2,3) + 389*Power(t2,4) - 42*Power(t2,5))\
)) + s*(-42*Power(t1,5)*Power(-1 + t2,2) - 
               Power(-1 + t2,4)*(4 + 169*t2 + 27*Power(t2,2)) + 
               6*Power(s2,5)*
                (-4 + 7*t2 - 6*Power(t2,2) + 3*Power(t2,3)) + 
               3*t1*Power(-1 + t2,3)*
                (143 + 98*t2 - 161*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (-379 - 475*t2 + 176*Power(t2,2) + 44*Power(t2,3)) + 
               Power(t1,4)*(109 - 123*t2 - 52*Power(t2,2) + 
                  67*Power(t2,3)) + 
               Power(t1,3)*(-441 + 1280*t2 - 1317*Power(t2,2) + 
                  477*Power(t2,3) + Power(t2,4)) + 
               Power(s2,4)*(-130 + 193*t2 - 10*Power(t2,2) - 
                  53*Power(t2,3) + Power(t2,4) + 
                  t1*(176 - 333*t2 + 234*Power(t2,2) - 77*Power(t2,3))) + 
               Power(s2,3)*(-165 + 37*t2 + 643*Power(t2,2) - 
                  681*Power(t2,3) + 191*Power(t2,4) - 25*Power(t2,5) + 
                  Power(t1,2)*
                   (-338 + 627*t2 - 384*Power(t2,2) + 95*Power(t2,3)) + 
                  t1*(401 - 721*t2 + 271*Power(t2,2) + 29*Power(t2,3) + 
                     16*Power(t2,4))) + 
               s2*(Power(t1,4)*
                   (26 - 81*t2 + 60*Power(t2,2) - 5*Power(t2,3)) - 
                  2*t1*Power(-1 + t2,2)*
                   (352 + 431*t2 - 165*Power(t2,2) + 16*Power(t2,3)) - 
                  Power(-1 + t2,3)*
                   (505 + 102*t2 - 389*Power(t2,2) + 40*Power(t2,3)) + 
                  Power(t1,3)*
                   (-27 - 245*t2 + 523*Power(t2,2) - 279*Power(t2,3) + 
                     24*Power(t2,4)) + 
                  Power(t1,2)*
                   (790 - 2969*t2 + 4171*Power(t2,2) - 
                     2371*Power(t2,3) + 398*Power(t2,4) - 19*Power(t2,5))\
) + Power(s2,2)*(Power(t1,3)*(202 - 339*t2 + 168*Power(t2,2) - 
                     31*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (358 + 336*t2 - 151*Power(t2,2) + 91*Power(t2,3)) + 
                  Power(t1,2)*
                   (-353 + 896*t2 - 732*Power(t2,2) + 236*Power(t2,3) - 
                     41*Power(t2,4)) + 
                  t1*(-170 + 1600*t2 - 3429*Power(t2,2) + 
                     2543*Power(t2,3) - 592*Power(t2,4) + 48*Power(t2,5)))\
)))*R1q(s1))/(Power(-1 + s1,3)*s1*(-1 + t1)*(1 - s2 + t1 - t2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*(-1 + t2)*
       Power(-1 + s + t2,3)*(s - s1 + t2)) + 
    (8*(2*Power(s,12)*(-1 + s1)*Power(t1 - t2,2) + 
         Power(s,11)*(t1 - t2)*
          (14*t1 - 9*Power(t1,2) - 2*Power(s1,2)*(2 + 7*t1 - 9*t2) + 
            4*s2*(-1 + 2*t1 - 3*t2) - 6*t2 + 2*t1*t2 + 7*Power(t2,2) + 
            s1*(4 + 7*Power(t1,2) - 14*t2 - 9*Power(t2,2) + 
               2*t1*(1 + t2) + s2*(4 - 10*t1 + 14*t2))) + 
         Power(s,10)*(2 + 3*t1 - 59*Power(t1,2) + 51*Power(t1,3) - 
            19*Power(t1,4) - 19*t2 + 10*t1*t2 - 57*Power(t1,2)*t2 + 
            10*Power(t1,3)*t2 + 21*Power(t2,2) + 5*t1*Power(t2,2) + 
            30*Power(t1,2)*Power(t2,2) + Power(t2,3) - 
            14*t1*Power(t2,3) - 7*Power(t2,4) + 
            Power(s2,2)*(2 - 12*Power(t1,2) - 25*t2 - 30*Power(t2,2) + 
               t1*(7 + 40*t2)) - 
            2*Power(s1,3)*(2 + 2*Power(s2,2) - 16*Power(t1,2) + 11*t2 - 
               34*Power(t2,2) + s2*(-5 - 7*t1 + 4*t2) + t1*(-6 + 49*t2)) \
+ s2*(-6 + 31*Power(t1,3) + 14*t2 - 50*Power(t2,2) + 40*Power(t2,3) - 
               2*Power(t1,2)*(28 + 27*t2) + 
               t1*(24 + 134*t2 - 17*Power(t2,2))) + 
            Power(s1,2)*(12 - 52*Power(t1,3) + 
               Power(s2,2)*(12 - 5*t1 - 9*t2) - 11*t2 + 4*Power(t2,2) - 
               77*Power(t2,3) + Power(t1,2)*(34 + 39*t2) + 
               t1*(19 - 68*t2 + 90*Power(t2,2)) + 
               s2*(-30 + 75*Power(t1,2) + 50*t2 + 101*Power(t2,2) - 
                  8*t1*(4 + 19*t2))) + 
            s1*(-10 + 10*Power(t1,4) + 56*t2 - 107*Power(t2,2) + 
               69*Power(t2,3) + 16*Power(t2,4) + 
               Power(t1,3)*(30 + 8*t2) - 
               Power(t1,2)*(25 + 47*t2 + 30*Power(t2,2)) - 
               2*t1*(19 - 94*t2 + 26*Power(t2,2) + 2*Power(t2,3)) + 
               2*Power(s2,2)*
                (-5 + 10*Power(t1,2) + 19*t2 + 21*Power(t2,2) - 
                  3*t1*(1 + 10*t2)) + 
               s2*(26 - 30*Power(t1,3) - 64*t2 - 49*Power(t2,2) - 
                  63*Power(t2,3) + Power(t1,2)*(-9 + 29*t2) + 
                  t1*(2 + 6*t2 + 64*Power(t2,2))))) + 
         Power(s,9)*(-19 - 73*t1 + 99*Power(t1,2) - 162*Power(t1,3) + 
            128*Power(t1,4) - 23*Power(t1,5) + 127*t2 + 231*t1*t2 - 
            54*Power(t1,2)*t2 - 214*Power(t1,3)*t2 - 14*Power(t1,4)*t2 - 
            314*Power(t2,2) - 248*t1*Power(t2,2) + 
            44*Power(t1,2)*Power(t2,2) + 82*Power(t1,3)*Power(t2,2) + 
            220*Power(t2,3) + 78*t1*Power(t2,3) - 
            28*Power(t1,2)*Power(t2,3) - 36*Power(t2,4) - 
            19*t1*Power(t2,4) + 2*Power(t2,5) + 
            Power(s2,3)*(-4 + t1 + 8*Power(t1,2) + 54*t2 - 40*t1*t2 + 
               40*Power(t2,2)) + 
            2*Power(s1,4)*(12 + 13*Power(s2,2) - 3*Power(t1,2) + 
               25*t2 - 72*Power(t2,2) + s2*(-32 - 45*t1 + 25*t2) + 
               t1*(9 + 68*t2)) + 
            Power(s2,2)*(-27 - 39*Power(t1,3) + 78*t2 + 
               96*Power(t2,2) - 93*Power(t2,3) + 
               Power(t1,2)*(79 + 106*t2) + 
               t1*(-50 - 393*t2 + 10*Power(t2,2))) + 
            s2*(64 + 54*Power(t1,4) - 166*t2 + 283*Power(t2,2) - 
               144*Power(t2,3) + 34*Power(t2,4) - 
               52*Power(t1,3)*(4 + t2) + 
               Power(t1,2)*(243 + 526*t2 - 132*Power(t2,2)) + 
               t1*(-15 - 286*t2 + 70*Power(t2,2) + 96*Power(t2,3))) + 
            Power(s1,3)*(-72 + 13*Power(s2,3) + 145*Power(t1,3) + 
               100*t2 - 146*Power(t2,2) + 263*Power(t2,3) - 
               2*Power(t1,2)*(70 + 69*t2) + 
               Power(s2,2)*(-90 - 26*t1 + 77*t2) + 
               t1*(-189 + 416*t2 - 254*Power(t2,2)) + 
               s2*(177 - 177*Power(t1,2) - 144*t2 - 341*Power(t2,2) + 
                  2*t1*(71 + 192*t2))) + 
            Power(s1,2)*(33 - 74*Power(t1,4) + 
               Power(t1,3)*(5 - 131*t2) - 120*t2 + 181*Power(t2,2) - 
               66*Power(t2,3) - 133*Power(t2,4) + 
               Power(s2,3)*(-34 + 23*t1 + 48*t2) + 
               3*Power(t1,2)*(-44 - 39*t2 + 93*Power(t2,2)) + 
               t1*(64 - 303*t2 - 82*Power(t2,2) + 59*Power(t2,3)) + 
               Power(s2,2)*(63 - 181*Power(t1,2) - 269*t2 - 
                  272*Power(t2,2) + t1*(79 + 199*t2)) + 
               s2*(-62 + 232*Power(t1,3) + 3*Power(t1,2)*(-38 + t2) + 
                  68*t2 + 94*Power(t2,2) + 427*Power(t2,3) + 
                  t1*(59 + 624*t2 - 450*Power(t2,2)))) + 
            s1*(36 + 10*Power(t1,5) - 183*t2 + 484*Power(t2,2) - 
               439*Power(t2,3) + 142*Power(t2,4) + 14*Power(t2,5) + 
               Power(t1,4)*(79 + 10*t2) + 
               Power(t1,3)*(-99 + 81*t2 - 8*Power(t2,2)) - 
               Power(s2,3)*(-23 + 20*Power(t1,2) + t1*(12 - 80*t2) + 
                  118*t2 + 70*Power(t2,2)) + 
               Power(t1,2)*(254 + 471*t2 - 221*Power(t2,2) - 
                  40*Power(t2,3)) + 
               t1*(210 - 614*t2 + 555*Power(t2,2) - 81*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,2)*(34 + 50*Power(t1,3) + 
                  Power(t1,2)*(35 - 70*t2) + 120*t2 + 131*Power(t2,2) + 
                  189*Power(t2,3) + t1*(3 + 312*t2 - 153*Power(t2,2))) - 
               s2*(121 + 40*Power(t1,4) - 228*t2 + 22*Power(t2,2) + 
                  179*Power(t2,3) + 112*Power(t2,4) + 
                  2*Power(t1,3)*(51 + 10*t2) + 
                  Power(t1,2)*(-52 + 193*t2 - 156*Power(t2,2)) - 
                  2*t1*(-72 - 373*t2 + 9*Power(t2,2) + 8*Power(t2,3))))) \
- Power(s,8)*(-74 - 237*t1 - 39*Power(t1,2) - 408*Power(t1,3) + 
            386*Power(t1,4) - 189*Power(t1,5) + 15*Power(t1,6) + 
            264*t2 + 931*t1*t2 - 644*Power(t1,2)*t2 - 
            695*Power(t1,3)*t2 + 162*Power(t1,4)*t2 + 
            52*Power(t1,5)*t2 - 934*Power(t2,2) - 998*t1*Power(t2,2) + 
            1503*Power(t1,2)*Power(t2,2) + 693*Power(t1,3)*Power(t2,2) - 
            114*Power(t1,4)*Power(t2,2) + 1374*Power(t2,3) + 
            371*t1*Power(t2,3) - 745*Power(t1,2)*Power(t2,3) - 
            28*Power(t1,3)*Power(t2,3) - 629*Power(t2,4) + 
            44*t1*Power(t2,4) + 107*Power(t1,2)*Power(t2,4) + 
            35*Power(t2,5) - 24*t1*Power(t2,5) - 8*Power(t2,6) + 
            Power(s2,4)*(4 + 2*Power(t1,2) + t1*(5 - 20*t2) + 46*t2 + 
               30*Power(t2,2)) + 
            2*Power(s1,5)*(30 + 37*Power(s2,2) + 52*Power(t1,2) - 
               4*s2*(22 + 31*t1 - 17*t2) + 30*t2 - 95*Power(t2,2) + 
               t1*(68 + 22*t2)) - 
            Power(s2,3)*(133 + 21*Power(t1,3) - 420*t2 - 
               34*Power(t2,2) + 110*Power(t2,3) - 
               2*Power(t1,2)*(29 + 52*t2) + 
               t1*(11 + 529*t2 + 30*Power(t2,2))) + 
            Power(s2,2)*(103 + 51*Power(t1,4) - 412*t2 + 
               864*Power(t2,2) - 431*Power(t2,3) + 57*Power(t2,4) - 
               32*Power(t1,3)*(10 + 3*t2) + 
               Power(t1,2)*(292 + 1273*t2 - 228*Power(t2,2)) + 
               t1*(86 - 1783*t2 + 566*Power(t2,2) + 270*Power(t2,3))) + 
            s2*(155 - 47*Power(t1,5) + Power(t1,4)*(446 - 40*t2) - 
               162*t2 + 127*Power(t2,2) - 415*Power(t2,3) + 
               126*Power(t2,4) + 24*Power(t2,5) + 
               Power(t1,3)*(-671 - 952*t2 + 342*Power(t2,2)) + 
               Power(t1,2)*(611 + 1711*t2 - 1076*Power(t2,2) - 
                  160*Power(t2,3)) + 
               t1*(-214 + 675*t2 - 1193*Power(t2,2) + 520*Power(t2,3) - 
                  119*Power(t2,4))) + 
            Power(s1,4)*(-170 + 75*Power(s2,3) + 171*Power(t1,3) + 
               179*t2 - 320*Power(t2,2) + 481*Power(t2,3) - 
               Power(t1,2)*(298 + 269*t2) + 
               Power(s2,2)*(-318 - 244*t1 + 277*t2) + 
               t1*(-531 + 768*t2 - 284*Power(t2,2)) + 
               s2*(452 - 62*Power(t1,2) - 116*t2 - 689*Power(t2,2) + 
                  t1*(456 + 442*t2))) + 
            Power(s1,3)*(-46 + 3*Power(s2,4) - 234*Power(t1,4) + 
               Power(t1,3)*(91 - 535*t2) + 416*t2 - 540*Power(t2,2) + 
               388*Power(t2,3) - 418*Power(t2,4) + 
               3*Power(s2,3)*(-34 + 31*t1 + 90*t2) + 
               Power(t1,2)*(-598 - 745*t2 + 936*Power(t2,2)) + 
               t1*(-331 + 912*t2 - 1091*Power(t2,2) + 
                  197*Power(t2,3)) + 
               Power(s2,2)*(66 - 584*Power(t1,2) - 457*t2 - 
                  879*Power(t2,2) + t1*(65 + 88*t2)) + 
               s2*(159 + 722*Power(t1,3) - 775*t2 + 241*Power(t2,2) + 
                  1208*Power(t2,3) + Power(t1,2)*(-184 + 427*t2) + 
                  t1*(367 + 1954*t2 - 1084*Power(t2,2)))) + 
            Power(s1,2)*(247 + 51*Power(t1,5) - 1434*t2 + 
               2607*Power(t2,2) - 1598*Power(t2,3) + 257*Power(t2,4) + 
               117*Power(t2,5) + Power(s2,4)*(-6 + 40*t1 + 99*t2) + 
               Power(t1,4)*(131 + 311*t2) + 
               Power(t1,3)*(468 + 493*t2 + 48*Power(t2,2)) + 
               Power(t1,2)*(879 + 148*t2 + 782*Power(t2,2) - 
                  588*Power(t2,3)) + 
               t1*(1140 - 3407*t2 + 3270*Power(t2,2) - 
                  673*Power(t2,3) + 61*Power(t2,4)) - 
               Power(s2,3)*(229 + 238*Power(t1,2) + 346*t2 + 
                  501*Power(t2,2) + 3*t1*(24 + 37*t2)) + 
               Power(s2,2)*(750 + 407*Power(t1,3) - 1141*t2 + 
                  1031*Power(t2,2) + 1023*Power(t2,3) + 
                  Power(t1,2)*(30 + 651*t2) + 
                  t1*(1015 + 1875*t2 - 560*Power(t2,2))) - 
               s2*(816 + 260*Power(t1,4) - 2266*t2 + 1837*Power(t2,2) - 
                  263*Power(t2,3) + 735*Power(t2,4) + 
                  Power(t1,3)*(83 + 950*t2) + 
                  Power(t1,2)*(1315 + 1709*t2 - 685*Power(t2,2)) + 
                  t1*(1477 - 1131*t2 + 2622*Power(t2,2) - 
                     432*Power(t2,3)))) - 
            s1*(3 + 54*Power(t1,5) + 10*Power(t1,6) - 433*t2 + 
               430*Power(t2,2) + 885*Power(t2,3) - 926*Power(t2,4) + 
               159*Power(t2,5) + 6*Power(t2,6) + 
               Power(t1,4)*(-425 + 456*t2 - 10*Power(t2,2)) + 
               Power(s2,4)*(5 + 10*Power(t1,2) + t1*(28 - 60*t2) + 
                  160*t2 + 70*Power(t2,2)) + 
               Power(t1,3)*(226 + 845*t2 - 55*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(t1,2)*(315 + 901*t2 - 1883*Power(t2,2) + 
                  451*Power(t2,3) + 30*Power(t2,4)) + 
               t1*(47 - 214*t2 + 162*Power(t2,2) + 495*Power(t2,3) - 
                  163*Power(t2,4) + 16*Power(t2,5)) - 
               Power(s2,3)*(387 + 40*Power(t1,3) + 
                  Power(t1,2)*(43 - 90*t2) - 431*t2 + 362*Power(t2,2) + 
                  315*Power(t2,3) + t1*(23 + 888*t2 - 190*Power(t2,2))) \
+ Power(s2,2)*(645 + 56*Power(t1,3) + 60*Power(t1,4) - 1870*t2 + 
                  1235*Power(t2,2) + 65*Power(t2,3) + 336*Power(t2,4) + 
                  Power(t1,2)*(-181 + 1330*t2 - 330*Power(t2,2)) - 
                  3*t1*(-303 + 50*t2 - 404*Power(t2,2) + 4*Power(t2,3))) \
- s2*(188 + 40*Power(t1,5) - 1302*t2 + 2329*Power(t2,2) - 
                  1038*Power(t2,3) + 334*Power(t2,4) + 98*Power(t2,5) + 
                  5*Power(t1,4)*(19 + 6*t2) - 
                  2*Power(t1,3)*(312 - 529*t2 + 40*Power(t2,2)) + 
                  Power(t1,2)*
                   (813 + 1102*t2 + 637*Power(t2,2) - 184*Power(t2,3)) + 
                  t1*(923 - 4282*t2 + 4060*Power(t2,2) - 
                     360*Power(t2,3) + 96*Power(t2,4))))) + 
         Power(s,7)*(-78 + 153*t1 - 374*Power(t1,2) - 1005*Power(t1,3) + 
            566*Power(t1,4) - 717*Power(t1,5) + 128*Power(t1,6) - 
            4*Power(t1,7) - 229*t2 + 92*t1*t2 - 1578*Power(t1,2)*t2 + 
            160*Power(t1,3)*t2 + 1419*Power(t1,4)*t2 + 
            308*Power(t1,5)*t2 - 53*Power(t1,6)*t2 + 574*Power(t2,2) + 
            479*t1*Power(t2,2) + 5629*Power(t1,2)*Power(t2,2) + 
            443*Power(t1,3)*Power(t2,2) - 1487*Power(t1,4)*Power(t2,2) + 
            33*Power(t1,5)*Power(t2,2) + 1008*Power(t2,3) - 
            1194*t1*Power(t2,3) - 4627*Power(t1,2)*Power(t2,3) + 
            91*Power(t1,3)*Power(t2,3) + 189*Power(t1,4)*Power(t2,3) - 
            2249*Power(t2,4) + 498*t1*Power(t2,4) + 
            1101*Power(t1,2)*Power(t2,4) - 180*Power(t1,3)*Power(t2,4) + 
            920*Power(t2,5) - 179*t1*Power(t2,5) - 
            45*Power(t1,2)*Power(t2,5) + 38*Power(t2,6) + 
            55*t1*Power(t2,6) + 5*Power(t2,7) + 
            2*Power(s1,6)*(40 + 60*Power(s2,2) + 109*Power(t1,2) - 
               5*s2*(27 + 38*t1 - 21*t2) + 20*t2 - 81*Power(t2,2) - 
               9*t1*(-15 + 7*t2)) + 
            Power(s2,5)*(-(t1*(1 + 4*t2)) + 
               4*(4 + t2 + 3*Power(t2,2))) - 
            Power(s2,4)*(135 + 4*Power(t1,3) - 526*t2 + 
               34*Power(t2,2) + 65*Power(t2,3) - 
               3*Power(t1,2)*(11 + 17*t2) + 
               t1*(36 + 269*t2 + 55*Power(t2,2))) + 
            Power(s2,3)*(16*Power(t1,4) - Power(t1,3)*(221 + 76*t2) + 
               Power(t1,2)*(353 + 1106*t2 - 192*Power(t2,2)) + 
               t1*(498 - 3372*t2 + 988*Power(t2,2) + 400*Power(t2,3)) + 
               2*(-228 + 582*t2 - 61*Power(t2,2) - 178*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s2,2)*(-24*Power(t1,5) + Power(t1,4)*(475 - 38*t2) + 
               Power(t1,3)*(-1379 - 1113*t2 + 534*Power(t2,2)) - 
               Power(t1,2)*(17 - 6665*t2 + 3494*Power(t2,2) + 
                  360*Power(t2,3)) + 
               t1*(289 - 3674*t2 + 2134*Power(t2,2) + 
                  508*Power(t2,3) - 310*Power(t2,4)) + 
               2*(528 - 1669*t2 + 2676*Power(t2,2) - 1807*Power(t2,3) + 
                  304*Power(t2,4) + 51*Power(t2,5))) + 
            s2*(-265 + 16*Power(t1,6) + 1938*t2 - 5544*Power(t2,2) + 
               4808*Power(t2,3) - 586*Power(t2,4) - 176*Power(t2,5) - 
               56*Power(t2,6) + 6*Power(t1,5)*(-69 + 20*t2) + 
               Power(t1,4)*(1763 - 36*t2 - 332*Power(t2,2)) - 
               Power(t1,3)*(912 + 5238*t2 - 4027*Power(t2,2) + 
                  164*Power(t2,3)) + 
               Power(t1,2)*(1538 + 1110*t2 - 1027*Power(t2,2) - 
                  866*Power(t2,3) + 540*Power(t2,4)) - 
               t1*(1642 - 6726*t2 + 10326*Power(t2,2) - 
                  5552*Power(t2,3) + 471*Power(t2,4) + 124*Power(t2,5))) \
+ Power(s1,5)*(-194 + 189*Power(s2,3) + 7*Power(t1,3) + 66*t2 - 
               276*Power(t2,2) + 521*Power(t2,3) - 
               16*Power(t1,2)*(30 + 17*t2) + 
               Power(s2,2)*(-680 - 676*t1 + 565*t2) + 
               t1*(-775 + 616*t2 + 2*Power(t2,2)) + 
               s2*(667 + 435*Power(t1,2) + 92*t2 - 881*Power(t2,2) + 
                  2*t1*(511 + 26*t2))) + 
            Power(s1,4)*(-359 + 19*Power(s2,4) - 387*Power(t1,4) + 
               Power(t1,3)*(132 - 1097*t2) + 1818*t2 - 
               2068*Power(t2,2) + 1085*Power(t2,3) - 671*Power(t2,4) + 
               Power(s2,3)*(-182 + 105*t1 + 725*t2) + 
               Power(t1,2)*(-936 - 1387*t2 + 1626*Power(t2,2)) + 
               t1*(-1242 + 3738*t2 - 2730*Power(t2,2) + 
                  241*Power(t2,3)) - 
               2*Power(s2,2)*
                (26 + 432*Power(t1,2) - 55*t2 + 849*Power(t2,2) + 
                  t1*(61 + 376*t2)) + 
               s2*(752 + 1127*Power(t1,3) - 2716*t2 + 619*Power(t2,2) + 
                  1853*Power(t2,3) + Power(t1,2)*(27 + 1394*t2) + 
                  t1*(756 + 2244*t2 - 1108*Power(t2,2)))) + 
            Power(s1,3)*(614 - 40*Power(s2,5) + 155*Power(t1,5) - 
               3130*t2 + 4013*Power(t2,2) - 1123*Power(t2,3) - 
               437*Power(t2,4) + 347*Power(t2,5) + 
               45*Power(t1,4)*(3 + 32*t2) + 
               Power(s2,4)*(193 + 364*t1 + 467*t2) + 
               Power(t1,3)*(1711 + 1056*t2 + 341*Power(t2,2)) + 
               Power(t1,2)*(2005 - 3413*t2 + 5503*Power(t2,2) - 
                  2255*Power(t2,3)) + 
               t1*(2273 - 7230*t2 + 6676*Power(t2,2) - 
                  1281*Power(t2,3) + 68*Power(t2,4)) - 
               Power(s2,3)*(1281 + 1181*Power(t1,2) - 207*t2 + 
                  1663*Power(t2,2) + 4*t1*(245 + 393*t2)) + 
               Power(s2,2)*(2538 + 1585*Power(t1,3) - 5582*t2 + 
                  3520*Power(t2,2) + 2388*Power(t2,3) + 
                  Power(t1,2)*(972 + 4075*t2) + 
                  t1*(4057 + 3033*t2 - 138*Power(t2,2))) - 
               s2*(1888 + 883*Power(t1,4) - 6246*t2 + 
                  5626*Power(t2,2) - 1933*Power(t2,3) + 
                  1863*Power(t2,4) + 10*Power(t1,3)*(32 + 441*t2) + 
                  Power(t1,2)*(4553 + 3800*t2 - 906*Power(t2,2)) - 
                  2*t1*(-2357 + 4993*t2 - 5527*Power(t2,2) + 
                     805*Power(t2,3)))) + 
            Power(s1,2)*(351 - 17*Power(t1,6) - 1794*t2 + 
               6950*Power(t2,2) - 10348*Power(t2,3) + 
               5101*Power(t2,4) - 459*Power(t2,5) - 53*Power(t2,6) + 
               10*Power(s2,5)*(8 + 3*t1 + 9*t2) - 
               Power(t1,5)*(125 + 247*t2) + 
               Power(t1,4)*(118 - 873*t2 - 645*Power(t2,2)) + 
               Power(t1,3)*(753 + 509*t2 - 3071*Power(t2,2) + 
                  364*Power(t2,3)) + 
               Power(t1,2)*(2053 + 1287*t2 - 5418*Power(t2,2) - 
                  34*Power(t2,3) + 715*Power(t2,4)) + 
               t1*(2218 - 6946*t2 + 10030*Power(t2,2) - 
                  8510*Power(t2,3) + 2402*Power(t2,4) - 117*Power(t2,5)\
) - Power(s2,4)*(705 + 152*Power(t1,2) - 172*t2 + 682*Power(t2,2) + 
                  5*t1*(79 + 92*t2)) + 
               Power(s2,3)*(1697 + 293*Power(t1,3) - 3892*t2 + 
                  2523*Power(t2,2) + 1465*Power(t2,3) + 
                  Power(t1,2)*(475 + 1578*t2) + 
                  t1*(2273 + 1360*t2 + 650*Power(t2,2))) - 
               Power(s2,2)*(697 + 267*Power(t1,4) - 4239*t2 + 
                  4016*Power(t2,2) + 53*Power(t2,3) + 
                  1677*Power(t2,4) + Power(t1,3)*(210 + 2383*t2) + 
                  3*Power(t1,2)*(905 + 905*t2 + 356*Power(t2,2)) + 
                  t1*(3171 - 9687*t2 + 11341*Power(t2,2) - 
                     691*Power(t2,3))) + 
               s2*(-972 + 113*Power(t1,5) + 881*t2 - 1182*Power(t2,2) + 
                  2944*Power(t2,3) - 843*Power(t2,4) + 
                  653*Power(t2,5) + Power(t1,4)*(175 + 1422*t2) + 
                  Power(t1,3)*(1029 + 2056*t2 + 1745*Power(t2,2)) + 
                  Power(t1,2)*
                   (759 - 6218*t2 + 11334*Power(t2,2) - 
                     2035*Power(t2,3)) + 
                  t1*(81 - 8236*t2 + 10520*Power(t2,2) + 
                     902*Power(t2,3) - 26*Power(t2,4)))) + 
            s1*(-378 + 7*Power(t1,7) + 3093*t2 - 8860*Power(t2,2) + 
               8549*Power(t2,3) - 1181*Power(t2,4) - 1230*Power(t2,5) + 
               105*Power(t2,6) + Power(t2,7) + 
               Power(t1,6)*(-35 + 11*t2) + 
               Power(t1,5)*(-444 + 540*t2 - 63*Power(t2,2)) - 
               Power(s2,5)*(61 + 2*Power(t1,2) + t1*(12 - 24*t2) + 
                  80*t2 + 42*Power(t2,2)) + 
               Power(t1,4)*(945 - 1134*t2 + 794*Power(t2,2) + 
                  45*Power(t2,3)) + 
               3*Power(t1,3)*
                (-180 - 1566*t2 + 1579*Power(t2,2) - 103*Power(t2,3) + 
                  9*Power(t2,4)) - 
               Power(t1,2)*(1245 - 1903*t2 + 2228*Power(t2,2) - 
                  3154*Power(t2,3) + 788*Power(t2,4) + 33*Power(t2,5)) + 
               t1*(-2521 + 8457*t2 - 12000*Power(t2,2) + 
                  7834*Power(t2,3) - 955*Power(t2,4) - 
                  307*Power(t2,5) + 5*Power(t2,6)) + 
               Power(s2,4)*(606 + 15*Power(t1,3) - 1287*t2 + 
                  580*Power(t2,2) + 315*Power(t2,3) - 
                  Power(t1,2)*(21 + 65*t2) + 
                  t1*(133 + 872*t2 - 125*Power(t2,2))) + 
               Power(s2,3)*(93 - 40*Power(t1,4) + 1524*t2 - 
                  924*Power(t2,2) + 17*Power(t2,3) - 560*Power(t2,4) + 
                  10*Power(t1,3)*(17 + 4*t2) + 
                  Power(t1,2)*(5 - 2018*t2 + 360*Power(t2,2)) - 
                  2*t1*(941 - 2288*t2 + 1810*Power(t2,2) + 
                     20*Power(t2,3))) + 
               Power(s2,2)*(-2283 + 50*Power(t1,5) + 4724*t2 - 
                  4541*Power(t2,2) + 2903*Power(t2,3) - 
                  241*Power(t2,4) + 294*Power(t2,5) + 
                  6*Power(t1,4)*(-44 + 5*t2) - 
                  3*Power(t1,3)*(203 - 760*t2 + 80*Power(t2,2)) + 
                  Power(t1,2)*
                   (2535 - 5533*t2 + 5264*Power(t2,2) - 
                     320*Power(t2,3)) + 
                  t1*(-295 - 7278*t2 + 5284*Power(t2,2) + 
                     897*Power(t2,3) + 282*Power(t2,4))) + 
               s2*(1905 - 30*Power(t1,6) + Power(t1,5)*(162 - 40*t2) - 
                  6853*t2 + 14178*Power(t2,2) - 13648*Power(t2,3) + 
                  4131*Power(t2,4) - 393*Power(t2,5) - 42*Power(t2,6) + 
                  Power(t1,3)*(-2204 + 3378*t2 - 3018*Power(t2,2)) + 
                  2*Power(t1,4)*(488 - 797*t2 + 55*Power(t2,2)) + 
                  Power(t1,2)*
                   (1132 + 9576*t2 - 8555*Power(t2,2) - 
                     527*Power(t2,3) + 106*Power(t2,4)) - 
                  2*t1*(-2161 + 5772*t2 - 6994*Power(t2,2) + 
                     4797*Power(t2,3) - 717*Power(t2,4) + 52*Power(t2,5))\
))) + Power(s,6)*(-334 - 2237*t1 + 803*Power(t1,2) + 2421*Power(t1,3) - 
            135*Power(t1,4) + 1568*Power(t1,5) - 482*Power(t1,6) + 
            30*Power(t1,7) + 2487*t2 + 7881*t1*t2 + 
            1498*Power(t1,2)*t2 - 6570*Power(t1,3)*t2 - 
            4900*Power(t1,4)*t2 - 801*Power(t1,5)*t2 + 
            411*Power(t1,6)*t2 - 18*Power(t1,7)*t2 - 8472*Power(t2,2) - 
            14711*t1*Power(t2,2) - 8572*Power(t1,2)*Power(t2,2) + 
            8928*Power(t1,3)*Power(t2,2) + 
            6449*Power(t1,4)*Power(t2,2) - 528*Power(t1,5)*Power(t2,2) - 
            54*Power(t1,6)*Power(t2,2) + 12140*Power(t2,3) + 
            16578*t1*Power(t2,3) + 10377*Power(t1,2)*Power(t2,3) - 
            5634*Power(t1,3)*Power(t2,3) - 
            2012*Power(t1,4)*Power(t2,3) + 182*Power(t1,5)*Power(t2,3) - 
            6033*Power(t2,4) - 8996*t1*Power(t2,4) - 
            4648*Power(t1,2)*Power(t2,4) + 
            1489*Power(t1,3)*Power(t2,4) + 5*Power(t1,4)*Power(t2,4) - 
            817*Power(t2,5) + 1307*t1*Power(t2,5) + 
            490*Power(t1,2)*Power(t2,5) - 214*Power(t1,3)*Power(t2,5) + 
            953*Power(t2,6) + 45*t1*Power(t2,6) + 
            64*Power(t1,2)*Power(t2,6) + 75*Power(t2,7) + 
            34*t1*Power(t2,7) + Power(t2,8) - 
            2*Power(s1,7)*(30 + 60*Power(s2,2) + 108*Power(t1,2) + 
               t1*(138 - 99*t2) - 25*s2*(5 + 7*t1 - 4*t2) + 7*t2 - 
               44*Power(t2,2)) + 
            Power(s2,6)*(-14 + 2*t1 + 15*t2 - 2*Power(t2,2)) + 
            Power(s1,6)*(96 - 269*Power(s2,3) + 213*Power(t1,3) + 
               Power(s2,2)*(932 + 968*t1 - 727*t2) + 135*t2 + 
               68*Power(t2,2) - 343*Power(t2,3) + 
               Power(t1,2)*(618 + 61*t2) - 
               s2*(644 + 894*Power(t1,2) + t1*(1496 - 532*t2) + 
                  162*t2 - 697*Power(t2,2)) + 
               t1*(687 - 176*t2 - 296*Power(t2,2))) - 
            Power(s2,5)*(11 + 124*t2 + 30*Power(t2,2) - 
               12*Power(t2,3) + 2*Power(t1,2)*(6 + 5*t2) + 
               t1*(-87 + 27*t2 - 35*Power(t2,2))) + 
            Power(s2,4)*(837 - 2797*t2 + 1609*Power(t2,2) - 
               41*Power(t2,3) + 55*Power(t2,4) + 
               Power(t1,3)*(58 + 22*t2) + 
               Power(t1,2)*(-361 - 212*t2 + 78*Power(t2,2)) - 
               t1*(96 - 1763*t2 + 343*Power(t2,2) + 330*Power(t2,3))) + 
            Power(s2,3)*(4*Power(t1,4)*(-38 + 3*t2) + 
               Power(t1,3)*(1183 + 237*t2 - 370*Power(t2,2)) + 
               Power(t1,2)*(-570 - 5389*t2 + 3074*Power(t2,2) + 
                  400*Power(t2,3)) + 
               t1*(-3005 + 13550*t2 - 9716*Power(t2,2) + 
                  332*Power(t2,3) + 430*Power(t2,4)) - 
               2*(12 - 590*t2 + 2065*Power(t2,2) - 1673*Power(t2,3) + 
                  244*Power(t2,4) + 110*Power(t2,5))) + 
            Power(s2,2)*(-2777 + Power(t1,5)*(198 - 68*t2) + 7734*t2 - 
               11138*Power(t2,2) + 10904*Power(t2,3) - 
               5686*Power(t2,4) + 791*Power(t2,5) + 168*Power(t2,6) + 
               Power(t1,4)*(-2043 + 606*t2 + 322*Power(t2,2)) + 
               Power(t1,3)*(3040 + 5184*t2 - 5527*Power(t2,2) + 
                  324*Power(t2,3)) + 
               Power(t1,2)*(2874 - 22265*t2 + 19689*Power(t2,2) - 
                  1972*Power(t2,3) - 1090*Power(t2,4)) + 
               t1*(2525 - 5736*t2 + 9446*Power(t2,2) - 
                  6126*Power(t2,3) + 724*Power(t2,4) + 260*Power(t2,5))) \
+ s2*(2111 + 62*Power(t1,6)*(-2 + t2) - 8283*t2 + 21014*Power(t2,2) - 
               29344*Power(t2,3) + 17325*Power(t2,4) - 
               2286*Power(t2,5) - 406*Power(t2,6) - 32*Power(t2,7) + 
               Power(t1,5)*(1630 - 1030*t2 - 9*Power(t2,2)) - 
               Power(t1,4)*(3931 + 633*t2 - 3354*Power(t2,2) + 
                  588*Power(t2,3)) + 
               Power(t1,3)*(-571 + 16412*t2 - 18031*Power(t2,2) + 
                  3693*Power(t2,3) + 600*Power(t2,4)) + 
               Power(t1,2)*(-5342 + 13198*t2 - 17912*Power(t2,2) + 
                  11291*Power(t2,3) - 2670*Power(t2,4) + 
                  258*Power(t2,5)) + 
               t1*(3864 - 16988*t2 + 29329*Power(t2,2) - 
                  23514*Power(t2,3) + 7553*Power(t2,4) + 
                  39*Power(t2,5) - 291*Power(t2,6))) + 
            Power(s1,5)*(676 - 61*Power(s2,4) + 328*Power(t1,4) + 
               Power(s2,3)*(487 + 105*t1 - 1250*t2) - 2724*t2 + 
               2725*Power(t2,2) - 1181*Power(t2,3) + 600*Power(t2,4) + 
               Power(t1,3)*(-359 + 1468*t2) + 
               Power(t1,2)*(384 + 1621*t2 - 1717*Power(t2,2)) + 
               t1*(1695 - 5247*t2 + 3227*Power(t2,2) - 
                  49*Power(t2,3)) + 
               Power(s2,2)*(-121 + 461*Power(t1,2) - 866*t2 + 
                  1951*Power(t2,2) + t1*(-525 + 2278*t2)) - 
               s2*(1091 + 833*Power(t1,3) - 3825*t2 + 966*Power(t2,2) + 
                  1650*Power(t2,3) + Power(t1,2)*(-488 + 2651*t2) + 
                  t1*(190 + 1254*t2 - 416*Power(t2,2)))) + 
            Power(s1,4)*(-779 + 156*Power(s2,5) - 286*Power(t1,5) + 
               2737*t2 - 991*Power(t2,2) - 2393*Power(t2,3) + 
               1744*Power(t2,4) - 504*Power(t2,5) - 
               4*Power(t1,4)*(-33 + 778*t2) - 
               Power(s2,4)*(525 + 1026*t1 + 1136*t2) - 
               Power(t1,3)*(1890 + 1412*t2 + 717*Power(t2,2)) + 
               Power(t1,2)*(-2729 + 9003*t2 - 11844*Power(t2,2) + 
                  4172*Power(t2,3)) + 
               t1*(-2339 + 8138*t2 - 6900*Power(t2,2) + 
                  992*Power(t2,3) + 27*Power(t2,4)) + 
               Power(s2,3)*(2149 + 2654*Power(t1,2) - 1679*t2 + 
                  3024*Power(t2,2) + t1*(2225 + 4643*t2)) - 
               Power(s2,2)*(3788 + 3140*Power(t1,3) - 10447*t2 + 
                  6078*Power(t2,2) + 2996*Power(t2,3) + 
                  6*Power(t1,2)*(347 + 1668*t2) + 
                  t1*(5743 + 1854*t2 + 1876*Power(t2,2))) + 
               s2*(2381 + 1642*Power(t1,4) - 8810*t2 + 
                  8039*Power(t2,2) - 3446*Power(t2,3) + 
                  2376*Power(t2,4) + Power(t1,3)*(250 + 9613*t2) + 
                  Power(t1,2)*(5488 + 4591*t2 + 5*Power(t2,2)) + 
                  t1*(6950 - 20664*t2 + 19955*Power(t2,2) - 
                     2786*Power(t2,3)))) + 
            Power(s1,3)*(-1187 + 70*Power(s2,6) + 16*Power(t1,6) + 
               8605*t2 - 23369*Power(t2,2) + 24844*Power(t2,3) - 
               8883*Power(t2,4) + 50*Power(t2,5) + 152*Power(t2,6) - 
               Power(s2,5)*(457 + 496*t1 + 420*t2) + 
               Power(t1,5)*(247 + 1272*t2) + 
               Power(t1,4)*(893 - 897*t2 + 3005*Power(t2,2)) - 
               Power(t1,3)*(1393 + 7770*t2 - 12338*Power(t2,2) + 
                  1741*Power(t2,3)) + 
               Power(t1,2)*(-4563 + 512*t2 + 3760*Power(t2,2) + 
                  5532*Power(t2,3) - 2877*Power(t2,4)) + 
               t1*(-6164 + 20895*t2 - 32638*Power(t2,2) + 
                  25993*Power(t2,3) - 7414*Power(t2,4) + 
                  257*Power(t2,5)) + 
               3*Power(s2,4)*
                (676 + 451*Power(t1,2) - 653*t2 + 761*Power(t2,2) + 
                  t1*(608 + 948*t2)) - 
               Power(s2,3)*(2369 + 1799*Power(t1,3) - 9334*t2 + 
                  6664*Power(t2,2) + 3044*Power(t2,3) + 
                  3*Power(t1,2)*(740 + 2781*t2) + 
                  t1*(6135 - 2876*t2 + 5240*Power(t2,2))) + 
               Power(s2,2)*(-1581 + 1189*Power(t1,4) - 2312*t2 + 
                  2418*Power(t2,2) + 1761*Power(t2,3) + 
                  3217*Power(t2,4) + Power(t1,3)*(1043 + 11106*t2) + 
                  Power(t1,2)*(7545 - 3066*t2 + 8699*Power(t2,2)) + 
                  t1*(4157 - 28777*t2 + 32644*Power(t2,2) - 
                     2217*Power(t2,3))) - 
               s2*(-3386 + 333*Power(t1,5) + 10087*t2 - 
                  18992*Power(t2,2) + 17985*Power(t2,3) - 
                  5853*Power(t2,4) + 1566*Power(t2,5) + 
                  Power(t1,4)*(437 + 6459*t2) + 
                  Power(t1,3)*(4331 - 3046*t2 + 8747*Power(t2,2)) + 
                  Power(t1,2)*
                   (437 - 27217*t2 + 37729*Power(t2,2) - 
                     6397*Power(t2,3)) + 
                  t1*(-5053 - 2954*t2 + 5530*Power(t2,2) + 
                     9380*Power(t2,3) - 1356*Power(t2,4)))) + 
            Power(s1,2)*(427 + Power(t1,7) - 6348*t2 + 
               13764*Power(t2,2) - 2136*Power(t2,3) - 
               13260*Power(t2,4) + 7905*Power(t2,5) - 456*Power(t2,6) - 
               10*Power(t2,7) - Power(t1,6)*(11 + 92*t2) + 
               Power(t1,5)*(149 - 396*t2 - 609*Power(t2,2)) + 
               Power(t1,4)*(448 + 4824*t2 - 3779*Power(t2,2) - 
                  678*Power(t2,3)) + 
               Power(t1,3)*(1005 + 2958*t2 + 673*Power(t2,2) - 
                  6126*Power(t2,3) + 933*Power(t2,4)) + 
               Power(t1,2)*(2227 - 13041*t2 + 30150*Power(t2,2) - 
                  26044*Power(t2,3) + 4640*Power(t2,4) + 
                  528*Power(t2,5)) + 
               t1*(2469 - 11072*t2 + 21657*Power(t2,2) - 
                  13254*Power(t2,3) - 3859*Power(t2,4) + 
                  3188*Power(t2,5) - 73*Power(t2,6)) - 
               5*Power(s2,6)*(t1 + 3*(8 + t2)) + 
               Power(s2,5)*(646 + 7*Power(t1,2) - 712*t2 + 
                  613*Power(t2,2) + t1*(567 + 278*t2)) + 
               Power(s2,4)*(-442 + 23*Power(t1,3) + 3507*t2 - 
                  1670*Power(t2,2) - 1517*Power(t2,3) - 
                  3*Power(t1,2)*(323 + 409*t2) - 
                  8*t1*(292 - 245*t2 + 275*Power(t2,2))) + 
               Power(s2,3)*(-4258 - 62*Power(t1,4) + 6719*t2 - 
                  6889*Power(t2,2) + 3528*Power(t2,3) + 
                  2069*Power(t2,4) + Power(t1,3)*(716 + 2255*t2) + 
                  Power(t1,2)*(3784 - 3367*t2 + 4990*Power(t2,2)) + 
                  t1*(1317 - 17657*t2 + 14696*Power(t2,2) + 
                     814*Power(t2,3))) + 
               Power(s2,2)*(7123 + 53*Power(t1,5) - 22935*t2 + 
                  35993*Power(t2,2) - 24548*Power(t2,3) + 
                  3930*Power(t2,4) - 1478*Power(t2,5) - 
                  2*Power(t1,4)*(102 + 979*t2) + 
                  Power(t1,3)*(-2995 + 3306*t2 - 6441*Power(t2,2)) + 
                  Power(t1,2)*
                   (-634 + 28405*t2 - 25437*Power(t2,2) + 
                     490*Power(t2,3)) + 
                  t1*(9339 - 12502*t2 + 17916*Power(t2,2) - 
                     18100*Power(t2,3) + 598*Power(t2,4))) + 
               s2*(-2705 - 17*Power(t1,6) + 18111*t2 - 
                  44707*Power(t2,2) + 38784*Power(t2,3) - 
                  8139*Power(t2,4) - 460*Power(t2,5) + 
                  304*Power(t2,6) + 3*Power(t1,5)*(7 + 253*t2) + 
                  Power(t1,4)*(752 - 791*t2 + 3647*Power(t2,2)) + 
                  Power(t1,3)*
                   (-689 - 19079*t2 + 16190*Power(t2,2) + 
                     891*Power(t2,3)) + 
                  Power(t1,2)*
                   (-6280 + 3317*t2 - 12005*Power(t2,2) + 
                     20311*Power(t2,3) - 3190*Power(t2,4)) + 
                  t1*(-12469 + 47491*t2 - 79608*Power(t2,2) + 
                     56776*Power(t2,3) - 9175*Power(t2,4) + 
                     294*Power(t2,5)))) - 
            s1*(-1178 - 2*Power(t1,8) + Power(t1,7)*(54 - 20*t2) + 
               5281*t2 - 18157*Power(t2,2) + 34354*Power(t2,3) - 
               28152*Power(t2,4) + 6865*Power(t2,5) + 1005*Power(t2,6) - 
               40*Power(t2,7) + 
               Power(t1,6)*(-36 - 119*t2 + 46*Power(t2,2)) + 
               2*Power(t1,5)*
                (-807 + 1356*t2 - 654*Power(t2,2) + 28*Power(t2,3)) + 
               Power(t1,4)*(853 + 1545*t2 - 907*Power(t2,2) - 
                  557*Power(t2,3) - 130*Power(t2,4)) + 
               Power(t1,3)*(-552 - 11218*t2 + 20145*Power(t2,2) - 
                  9681*Power(t2,3) + 433*Power(t2,4) + 20*Power(t2,5)) + 
               Power(t1,2)*(-2897 - 685*t2 + 12995*Power(t2,2) - 
                  8140*Power(t2,3) - 2504*Power(t2,4) + 
                  1228*Power(t2,5) + 30*Power(t2,6)) + 
               t1*(-6023 + 21758*t2 - 33449*Power(t2,2) + 
                  32998*Power(t2,3) - 19725*Power(t2,4) + 
                  3699*Power(t2,5) + 309*Power(t2,6)) + 
               Power(s2,6)*(-73 + 26*t2 - 14*Power(t2,2) + 
                  t1*(6 + 4*t2)) + 
               Power(s2,5)*(279 + 2*Power(t1,3) - 1039*t2 + 
                  389*Power(t2,2) + 189*Power(t2,3) - 
                  Power(t1,2)*(64 + 25*t2) + 
                  t1*(319 + 162*t2 - 36*Power(t2,2))) - 
               Power(s2,4)*(-1884 + 10*Power(t1,4) + 3663*t2 - 
                  2977*Power(t2,2) + 375*Power(t2,3) + 
                  560*Power(t2,4) - 10*Power(t1,3)*(25 + 4*t2) + 
                  Power(t1,2)*(827 + 709*t2 - 210*Power(t2,2)) + 
                  t1*(1647 - 6394*t2 + 3820*Power(t2,2) + 
                     100*Power(t2,3))) + 
               Power(s2,3)*(-3988 + 20*Power(t1,5) + 
                  10*Power(t1,4)*(-48 + t2) + 14639*t2 - 
                  17856*Power(t2,2) + 8223*Power(t2,3) - 
                  1293*Power(t2,4) + 490*Power(t2,5) + 
                  Power(t1,3)*(1333 + 1014*t2 - 320*Power(t2,2)) + 
                  Power(t1,2)*
                   (3929 - 13553*t2 + 8608*Power(t2,2) - 
                     240*Power(t2,3)) + 
                  2*t1*(-2559 + 4757*t2 - 5576*Power(t2,2) + 
                     2512*Power(t2,3) + 230*Power(t2,4))) - 
               Power(s2,2)*(817 + 20*Power(t1,6) + 6090*t2 - 
                  20423*Power(t2,2) + 14040*Power(t2,3) - 
                  403*Power(t2,4) - 303*Power(t2,5) + 126*Power(t2,6) + 
                  10*Power(t1,5)*(-49 + 8*t2) + 
                  Power(t1,4)*(1132 + 798*t2 - 270*Power(t2,2)) + 
                  Power(t1,3)*
                   (5647 - 14792*t2 + 8620*Power(t2,2) - 
                     80*Power(t2,3)) + 
                  Power(t1,2)*
                   (-5249 + 5906*t2 - 11451*Power(t2,2) + 
                     8294*Power(t2,3) - 80*Power(t2,4)) + 
                  t1*(-9463 + 45363*t2 - 59258*Power(t2,2) + 
                     26949*Power(t2,3) - 2499*Power(t2,4) + 
                     288*Power(t2,5))) + 
               s2*(3911 + 10*Power(t1,7) - 7878*t2 + 10410*Power(t2,2) - 
                  24222*Power(t2,3) + 25387*Power(t2,4) - 
                  7450*Power(t2,5) + 314*Power(t2,6) + 7*Power(t2,7) + 
                  Power(t1,6)*(-256 + 71*t2) + 
                  Power(t1,5)*(416 + 424*t2 - 156*Power(t2,2)) + 
                  Power(t1,4)*
                   (4700 - 9306*t2 + 4751*Power(t2,2) + 15*Power(t2,3)) \
+ Power(t1,3)*(-2868 - 1490*t2 - 2369*Power(t2,2) + 4202*Power(t2,3) + 
                     150*Power(t2,4)) - 
                  Power(t1,2)*
                   (4013 - 39024*t2 + 58187*Power(t2,2) - 
                     26804*Power(t2,3) + 1479*Power(t2,4) + 
                     129*Power(t2,5)) + 
                  t1*(2646 + 3858*t2 - 18840*Power(t2,2) + 
                     5930*Power(t2,3) + 8677*Power(t2,4) - 
                     2412*Power(t2,5) + 32*Power(t2,6))))) + 
         Power(s,5)*(1388 + 5379*t1 - 1612*Power(t1,2) - 
            4576*Power(t1,3) - 875*Power(t1,4) - 2071*Power(t1,5) + 
            1023*Power(t1,6) - 96*Power(t1,7) + Power(s2,7)*(4 - 6*t2) - 
            6447*t2 - 22880*t1*t2 + 3009*Power(t1,2)*t2 + 
            20324*Power(t1,3)*t2 + 9424*Power(t1,4)*t2 + 
            879*Power(t1,5)*t2 - 1365*Power(t1,6)*t2 + 
            118*Power(t1,7)*t2 + 19134*Power(t2,2) + 
            45375*t1*Power(t2,2) - 1881*Power(t1,2)*Power(t2,2) - 
            37212*Power(t1,3)*Power(t2,2) - 
            14332*Power(t1,4)*Power(t2,2) + 
            2423*Power(t1,5)*Power(t2,2) + 401*Power(t1,6)*Power(t2,2) - 
            32*Power(t1,7)*Power(t2,2) - 37380*Power(t2,3) - 
            55010*t1*Power(t2,3) - 379*Power(t1,2)*Power(t2,3) + 
            30909*Power(t1,3)*Power(t2,3) + 
            6849*Power(t1,4)*Power(t2,3) - 
            1569*Power(t1,5)*Power(t2,3) + 14*Power(t1,6)*Power(t2,3) + 
            40280*Power(t2,4) + 41119*t1*Power(t2,4) + 
            2305*Power(t1,2)*Power(t2,4) - 
            10955*Power(t1,3)*Power(t2,4) - 
            635*Power(t1,4)*Power(t2,4) + 163*Power(t1,5)*Power(t2,4) - 
            20205*Power(t2,5) - 15556*t1*Power(t2,5) - 
            1442*Power(t1,2)*Power(t2,5) + 
            1271*Power(t1,3)*Power(t2,5) - 164*Power(t1,4)*Power(t2,5) + 
            2388*Power(t2,6) + 1264*t1*Power(t2,6) + 
            110*Power(t1,2)*Power(t2,6) - 50*Power(t1,3)*Power(t2,6) + 
            802*Power(t2,7) + 264*t1*Power(t2,7) + 
            62*Power(t1,2)*Power(t2,7) + 40*Power(t2,8) + 
            7*t1*Power(t2,8) + 
            2*Power(s1,8)*(12 + 37*Power(s2,2) + 59*Power(t1,2) + 
               t1*(79 - 66*t2) + t2 - 14*Power(t2,2) + 
               s2*(-70 - 97*t1 + 59*t2)) + 
            Power(s2,6)*(66 - 124*t2 + 76*Power(t2,2) + 5*Power(t2,3) + 
               t1*(-36 + 46*t2 - 8*Power(t2,2))) + 
            Power(s1,7)*(4 + 231*Power(s2,3) - 253*Power(t1,3) - 
               168*t2 + 38*Power(t2,2) + 133*Power(t2,3) + 
               6*Power(t1,2)*(-94 + 27*t2) + 
               Power(s2,2)*(-810 - 802*t1 + 607*t2) + 
               s2*(431 + 821*Power(t1,2) + t1*(1370 - 752*t2) - 
                  16*t2 - 303*Power(t2,2)) + 
               t1*(-403 + 16*t2 + 258*Power(t2,2))) - 
            Power(s2,5)*(242 - 1098*t2 + 777*Power(t2,2) - 
               88*Power(t2,3) + 78*Power(t2,4) + 
               6*Power(t1,2)*(-22 + 15*t2 + 2*Power(t2,2)) + 
               t1*(390 - 452*t2 + 394*Power(t2,2) - 144*Power(t2,3))) + 
            Power(s2,4)*(-2032 + 7269*t2 - 6654*Power(t2,2) + 
               1684*Power(t2,3) - 348*Power(t2,4) + 270*Power(t2,5) + 
               2*Power(t1,3)*(-172 + 69*t2 + 48*Power(t2,2)) - 
               5*Power(t1,2)*
                (-260 + 59*t2 + 38*Power(t2,2) + 44*Power(t2,3)) + 
               t1*(1466 - 8130*t2 + 6482*Power(t2,2) - 14*Power(t2,3) - 
                  335*Power(t2,4))) + 
            Power(s2,3)*(2801 - 13974*t2 + 27554*Power(t2,2) - 
               23992*Power(t2,3) + 8852*Power(t2,4) - 710*Power(t2,5) - 
               280*Power(t2,6) - 
               2*Power(t1,4)*(-318 + 181*t2 + 52*Power(t2,2)) + 
               Power(t1,3)*(-3243 + 778*t2 + 1773*Power(t2,2) - 
                  268*Power(t2,3)) + 
               Power(t1,2)*(-1449 + 18918*t2 - 19685*Power(t2,2) + 
                  2350*Power(t2,3) + 1100*Power(t2,4)) + 
               t1*(7112 - 30966*t2 + 33124*Power(t2,2) - 
                  10476*Power(t2,3) + 652*Power(t2,4) - 280*Power(t2,5))\
) - Power(s2,2)*(-3178 + 3709*t2 + 7872*Power(t2,2) - 
               11944*Power(t2,3) + 250*Power(t2,4) + 4326*Power(t2,5) - 
               1112*Power(t2,6) - 87*Power(t2,7) + 
               6*Power(t1,5)*(118 - 99*t2 + 4*Power(t2,2)) + 
               Power(t1,4)*(-4839 + 3224*t2 + 1379*Power(t2,2) - 
                  609*Power(t2,3)) + 
               Power(t1,3)*(2603 + 16959*t2 - 25455*Power(t2,2) + 
                  6579*Power(t2,3) + 720*Power(t2,4)) + 
               Power(t1,2)*(7897 - 45461*t2 + 54591*Power(t2,2) - 
                  18367*Power(t2,3) - 610*Power(t2,4) + 582*Power(t2,5)\
) + t1*(11942 - 48232*t2 + 80807*Power(t2,2) - 60488*Power(t2,3) + 
                  17294*Power(t2,4) + 98*Power(t2,5) - 630*Power(t2,6))) \
+ s2*(-4973 + 15712*t2 - 29108*Power(t2,2) + 48288*Power(t2,3) - 
               52244*Power(t2,4) + 26158*Power(t2,5) - 
               3365*Power(t2,6) - 424*Power(t2,7) - 6*Power(t2,8) + 
               Power(t1,6)*(412 - 438*t2 + 84*Power(t2,2)) - 
               Power(t1,5)*(3595 - 3778*t2 + 287*Power(t2,2) + 
                  284*Power(t2,3)) + 
               Power(t1,4)*(4899 + 4194*t2 - 13898*Power(t2,2) + 
                  5724*Power(t2,3) - 130*Power(t2,4)) + 
               Power(t1,3)*(3692 - 31188*t2 + 42453*Power(t2,2) - 
                  16424*Power(t2,3) - 279*Power(t2,4) + 756*Power(t2,5)\
) + Power(t1,2)*(13843 - 55996*t2 + 94637*Power(t2,2) - 
                  72732*Power(t2,3) + 22617*Power(t2,4) - 
                  1296*Power(t2,5) - 244*Power(t2,6)) - 
               4*t1*(762 - 2989*t2 + 4285*Power(t2,2) - 
                  3860*Power(t2,3) + 3078*Power(t2,4) - 
                  1356*Power(t2,5) + 120*Power(t2,6) + 44*Power(t2,7))) \
+ Power(s1,6)*(-629 + 109*Power(s2,4) - 91*Power(t1,4) + 
               Power(t1,3)*(852 - 1474*t2) + 2076*t2 - 
               1827*Power(t2,2) + 654*Power(t2,3) - 293*Power(t2,4) + 
               Power(s2,3)*(-995 - 401*t1 + 1485*t2) + 
               Power(t1,2)*(718 - 1741*t2 + 1263*Power(t2,2)) - 
               t1*(1096 - 3577*t2 + 2030*Power(t2,2) + 
                  125*Power(t2,3)) + 
               Power(s2,2)*(709 + 319*Power(t1,2) + 445*t2 - 
                  1206*Power(t2,2) - 4*t1*(-560 + 869*t2)) + 
               s2*(730 + 64*Power(t1,3) - 2716*t2 + 890*Power(t2,2) + 
                  821*Power(t2,3) + Power(t1,2)*(-2127 + 3508*t2) + 
                  t1*(-1341 + 1256*t2 - 148*Power(t2,2)))) + 
            Power(s1,5)*(588 - 222*Power(s2,5) + 276*Power(t1,5) - 
               697*t2 - 2816*Power(t2,2) + 4673*Power(t2,3) - 
               2092*Power(t2,4) + 390*Power(t2,5) + 
               Power(s2,4)*(96 + 1258*t1 + 1883*t2) + 
               Power(t1,4)*(-993 + 3995*t2) + 
               Power(t1,3)*(139 + 1786*t2 + 653*Power(t2,2)) + 
               Power(t1,2)*(2569 - 11671*t2 + 13810*Power(t2,2) - 
                  4492*Power(t2,3)) + 
               t1*(1894 - 6978*t2 + 5657*Power(t2,2) - 
                  699*Power(t2,3) - 108*Power(t2,4)) - 
               Power(s2,3)*(1078 + 2872*Power(t1,2) - 1829*t2 + 
                  3068*Power(t2,2) + t1*(262 + 7986*t2)) + 
               Power(s2,2)*(3249 + 3134*Power(t1,3) - 11086*t2 + 
                  6580*Power(t2,2) + 1955*Power(t2,3) + 
                  3*Power(t1,2)*(-411 + 4990*t2) + 
                  t1*(1811 + 1605*t2 + 3088*Power(t2,2))) - 
               s2*(2363 + 1574*Power(t1,4) - 8756*t2 + 
                  8048*Power(t2,2) - 3371*Power(t2,3) + 
                  1596*Power(t2,4) + 2*Power(t1,3)*(-1196 + 6431*t2) + 
                  Power(t1,2)*(825 + 5150*t2 + 812*Power(t2,2)) + 
                  t1*(6016 - 23000*t2 + 20890*Power(t2,2) - 
                     3082*Power(t2,3)))) + 
            Power(s1,4)*(1785 - 224*Power(s2,6) - 40*Power(t1,6) + 
               Power(t1,5)*(137 - 2751*t2) - 13221*t2 + 
               30357*Power(t2,2) - 26248*Power(t2,3) + 
               6434*Power(t2,4) + 1033*Power(t2,5) - 204*Power(t2,6) + 
               Power(s2,5)*(615 + 1372*t1 + 1080*t2) - 
               2*Power(t1,4)*(652 - 2139*t2 + 2820*Power(t2,2)) + 
               Power(t1,3)*(-1421 + 18833*t2 - 22288*Power(t2,2) + 
                  3533*Power(t2,3)) + 
               Power(t1,2)*(2218 + 2618*t2 + 1066*Power(t2,2) - 
                  12433*Power(t2,3) + 5104*Power(t2,4)) + 
               t1*(5616 - 23165*t2 + 43451*Power(t2,2) - 
                  36835*Power(t2,3) + 10877*Power(t2,4) - 
                  254*Power(t2,5)) - 
               Power(s2,4)*(1976 + 3357*Power(t1,2) - 3607*t2 + 
                  3823*Power(t2,2) + 6*t1*(321 + 1178*t2)) + 
               Power(s2,3)*(2164 + 4119*Power(t1,3) - 13293*t2 + 
                  10223*Power(t2,2) + 3312*Power(t2,3) + 
                  97*Power(t1,2)*(11 + 192*t2) + 
                  t1*(5302 - 8742*t2 + 11397*Power(t2,2))) + 
               Power(s2,2)*(1475 - 2575*Power(t1,4) + 
                  Power(t1,3)*(1321 - 23115*t2) + 2384*t2 + 
                  968*Power(t2,2) - 4097*Power(t2,3) - 
                  2879*Power(t2,4) + 
                  Power(t1,2)*(-6421 + 13139*t2 - 18870*Power(t2,2)) + 
                  t1*(-5702 + 46829*t2 - 50417*Power(t2,2) + 
                     3789*Power(t2,3))) + 
               s2*(-3023 + 705*Power(t1,5) + 14793*t2 - 
                  34054*Power(t2,2) + 31775*Power(t2,3) - 
                  10841*Power(t2,4) + 1806*Power(t2,5) + 
                  2*Power(t1,4)*(-607 + 6615*t2) + 
                  Power(t1,3)*(4399 - 12282*t2 + 16936*Power(t2,2)) + 
                  Power(t1,2)*
                   (5120 - 52786*t2 + 62519*Power(t2,2) - 
                     10397*Power(t2,3)) - 
                  2*t1*(1950 + 1839*t2 + 1888*Power(t2,2) - 
                     9126*Power(t2,3) + 1711*Power(t2,4)))) + 
            Power(s1,3)*(-223 - 51*Power(s2,7) - 34*Power(t1,7) + 
               3941*t2 + 2562*Power(t2,2) - 31574*Power(t2,3) + 
               40300*Power(t2,4) - 15502*Power(t2,5) + 
               511*Power(t2,6) + 28*Power(t2,7) + 
               Power(s2,6)*(339 + 330*t1 + 163*t2) + 
               Power(t1,6)*(324 + 451*t2) + 
               Power(t1,5)*(567 - 2924*t2 + 3099*Power(t2,2)) + 
               Power(t1,4)*(-1635 - 8924*t2 + 6696*Power(t2,2) + 
                  2133*Power(t2,3)) + 
               Power(t1,3)*(-3347 + 14276*t2 - 30207*Power(t2,2) + 
                  25303*Power(t2,3) - 4003*Power(t2,4)) - 
               Power(t1,2)*(2967 - 33973*t2 + 79738*Power(t2,2) - 
                  61192*Power(t2,3) + 8930*Power(t2,4) + 
                  1878*Power(t2,5)) + 
               t1*(1896 + 4775*t2 - 20294*Power(t2,2) + 
                  4511*Power(t2,3) + 18823*Power(t2,4) - 
                  9248*Power(t2,5) + 204*Power(t2,6)) - 
               Power(s2,5)*(979 + 830*Power(t1,2) - 2582*t2 + 
                  1933*Power(t2,2) + 6*t1*(237 + 308*t2)) + 
               Power(s2,4)*(-1582 + 1006*Power(t1,3) - 3588*t2 + 
                  2012*Power(t2,2) + 3258*Power(t2,3) + 
                  Power(t1,2)*(2356 + 6615*t2) + 
                  t1*(3955 - 12154*t2 + 9366*Power(t2,2))) - 
               Power(s2,3)*(-8545 + 539*Power(t1,4) + 21487*t2 - 
                  27184*Power(t2,2) + 13806*Power(t2,3) + 
                  2784*Power(t2,4) + 2*Power(t1,3)*(1125 + 5419*t2) + 
                  Power(t1,2)*(7794 - 27638*t2 + 22173*Power(t2,2)) + 
                  t1*(-3416 - 28026*t2 + 26304*Power(t2,2) + 
                     3452*Power(t2,3))) + 
               Power(s2,2)*(-7028 + 2*Power(t1,5) + 40088*t2 - 
                  80385*Power(t2,2) + 57525*Power(t2,3) - 
                  10742*Power(t2,4) + 2657*Power(t2,5) + 
                  3*Power(t1,4)*(583 + 2953*t2) + 
                  Power(t1,3)*(8206 - 32066*t2 + 27079*Power(t2,2)) + 
                  Power(t1,2)*
                   (-3289 - 53888*t2 + 50130*Power(t2,2) + 
                     1673*Power(t2,3)) + 
                  t1*(-19925 + 56707*t2 - 85917*Power(t2,2) + 
                     60488*Power(t2,3) - 4850*Power(t2,4))) + 
               s2*(523 + 116*Power(t1,6) - 23409*t2 + 
                  62099*Power(t2,2) - 43909*Power(t2,3) - 
                  650*Power(t2,4) + 5304*Power(t2,5) - 
                  718*Power(t2,6) - 2*Power(t1,5)*(548 + 1701*t2) + 
                  Power(t1,4)*(-3955 + 16924*t2 - 15438*Power(t2,2)) + 
                  Power(t1,3)*
                   (3090 + 38374*t2 - 32534*Power(t2,2) - 
                     3612*Power(t2,3)) + 
                  Power(t1,2)*
                   (14539 - 49316*t2 + 89114*Power(t2,2) - 
                     71853*Power(t2,3) + 11332*Power(t2,4)) + 
                  2*t1*(6520 - 41314*t2 + 84094*Power(t2,2) - 
                     61029*Power(t2,3) + 9795*Power(t2,4) + 
                     79*Power(t2,5)))) - 
            Power(s1,2)*(1699 - 5*Power(t1,8) - 15792*t2 + 
               58904*Power(t2,2) - 92233*Power(t2,3) + 
               56342*Power(t2,4) - 2974*Power(t2,5) - 
               6172*Power(t2,6) + 237*Power(t2,7) + 
               Power(t1,7)*(-1 + 16*t2) + 
               Power(s2,7)*(-78 + 5*t1 + 36*t2) + 
               6*Power(t1,6)*(65 - 43*t2 + 37*Power(t2,2)) + 
               Power(s2,6)*(198 - 43*Power(t1,2) + t1*(452 - 127*t2) - 
                  487*t2 + 276*Power(t2,2)) + 
               Power(t1,5)*(49 - 3530*t2 + 1470*Power(t2,2) + 
                  862*Power(t2,3)) + 
               Power(t1,4)*(10 + 9943*t2 - 17374*Power(t2,2) + 
                  8285*Power(t2,3) + 95*Power(t2,4)) + 
               Power(t1,3)*(-245 + 24454*t2 - 45845*Power(t2,2) + 
                  19452*Power(t2,3) + 3396*Power(t2,4) - 
                  1008*Power(t2,5)) + 
               Power(t1,2)*(6356 - 14456*t2 + 25338*Power(t2,2) - 
                  47992*Power(t2,3) + 40321*Power(t2,4) - 
                  8708*Power(t2,5) - 200*Power(t2,6)) + 
               t1*(11294 - 44888*t2 + 94178*Power(t2,2) - 
                  120163*Power(t2,3) + 75073*Power(t2,4) - 
                  13067*Power(t2,5) - 1901*Power(t2,6) + 18*Power(t2,7)\
) + Power(s2,5)*(1193 + 145*Power(t1,3) - 858*t2 + 1396*Power(t2,2) - 
                  1227*Power(t2,3) + 47*Power(t1,2)*(-25 + 3*t2) - 
                  2*t1*(605 - 1482*t2 + 905*Power(t2,2))) + 
               Power(s2,4)*(-6270 - 255*Power(t1,4) + 19397*t2 - 
                  19851*Power(t2,2) + 5806*Power(t2,3) + 
                  1599*Power(t2,4) + Power(t1,3)*(1739 + 2*t2) + 
                  Power(t1,2)*(3652 - 8248*t2 + 5345*Power(t2,2)) + 
                  t1*(-3216 - 3390*t2 + 66*Power(t2,2) + 
                     3445*Power(t2,3))) + 
               Power(s2,3)*(3310 + 255*Power(t1,5) - 25338*t2 + 
                  44990*Power(t2,2) - 23315*Power(t2,3) + 
                  2345*Power(t2,4) - 1707*Power(t2,5) - 
                  2*Power(t1,4)*(758 + 69*t2) + 
                  Power(t1,3)*(-6066 + 12088*t2 - 8157*Power(t2,2)) + 
                  Power(t1,2)*
                   (3406 + 15248*t2 - 5085*Power(t2,2) - 
                     6110*Power(t2,3)) + 
                  t1*(19156 - 73106*t2 + 87481*Power(t2,2) - 
                     37444*Power(t2,3) + 513*Power(t2,4))) + 
               Power(s2,2)*(8460 - 145*Power(t1,6) - 9966*t2 + 
                  3227*Power(t2,2) - 24142*Power(t2,3) + 
                  27949*Power(t2,4) - 5458*Power(t2,5) + 
                  711*Power(t2,6) + Power(t1,5)*(722 + 141*t2) + 
                  Power(t1,4)*(5416 - 9111*t2 + 6361*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1887 - 20424*t2 + 5858*Power(t2,2) + 
                     7655*Power(t2,3)) + 
                  Power(t1,2)*
                   (-19464 + 98538*t2 - 132747*Power(t2,2) + 
                     63613*Power(t2,3) - 4233*Power(t2,4)) + 
                  t1*(-11315 + 84880*t2 - 144020*Power(t2,2) + 
                     70096*Power(t2,3) + 159*Power(t2,4) - 
                     158*Power(t2,5))) + 
               s2*(-7624 + 43*Power(t1,7) + 28387*t2 - 
                  81677*Power(t2,2) + 139639*Power(t2,3) - 
                  104834*Power(t2,4) + 26185*Power(t2,5) - 
                  583*Power(t2,6) - 60*Power(t2,7) - 
                  Power(t1,6)*(143 + 71*t2) + 
                  Power(t1,5)*(-2380 + 3052*t2 - 2237*Power(t2,2)) + 
                  Power(t1,4)*
                   (455 + 12954*t2 - 3705*Power(t2,2) - 
                     4625*Power(t2,3)) + 
                  Power(t1,3)*
                   (6568 - 54772*t2 + 82491*Power(t2,2) - 
                     40260*Power(t2,3) + 2026*Power(t2,4)) + 
                  Power(t1,2)*
                   (7772 - 82804*t2 + 143686*Power(t2,2) - 
                     65611*Power(t2,3) - 5855*Power(t2,4) + 
                     2680*Power(t2,5)) + 
                  t1*(-16353 + 38378*t2 - 59176*Power(t2,2) + 
                     99174*Power(t2,3) - 79308*Power(t2,4) + 
                     16134*Power(t2,5) - 276*Power(t2,6)))) + 
            s1*(-1602 + 8*Power(t1,8)*(-2 + t2) + 1443*t2 + 
               2883*Power(t2,2) + 12050*Power(t2,3) - 
               42438*Power(t2,4) + 39217*Power(t2,5) - 
               11118*Power(t2,6) - 442*Power(t2,7) + 7*Power(t2,8) + 
               Power(s2,7)*(-38 + 4*t1 + 42*t2 - 2*Power(t2,2)) + 
               Power(t1,7)*(191 - 133*t2 + 10*Power(t2,2)) + 
               Power(t1,6)*(222 - 929*t2 + 637*Power(t2,2) - 
                  94*Power(t2,3)) + 
               Power(t1,5)*(-3648 + 7188*t2 - 4842*Power(t2,2) + 
                  1247*Power(t2,3) + 56*Power(t2,4)) + 
               Power(t1,4)*(-1524 + 14709*t2 - 19257*Power(t2,2) + 
                  6078*Power(t2,3) + 290*Power(t2,4) + 80*Power(t2,5)) - 
               Power(t1,3)*(1136 + 11100*t2 - 30200*Power(t2,2) + 
                  28957*Power(t2,3) - 11070*Power(t2,4) + 
                  694*Power(t2,5) + 50*Power(t2,6)) - 
               Power(t1,2)*(622 + 17999*t2 - 65357*Power(t2,2) + 
                  78350*Power(t2,3) - 33853*Power(t2,4) + 
                  1037*Power(t2,5) + 1194*Power(t2,6) + 10*Power(t2,7)) \
- t1*(4461 - 14161*t2 + 9067*Power(t2,2) - 153*Power(t2,3) + 
                  14602*Power(t2,4) - 19245*Power(t2,5) + 
                  5049*Power(t2,6) + 144*Power(t2,7)) - 
               Power(s2,6)*(102 + 49*t2 + 21*Power(t2,2) - 
                  63*Power(t2,3) + 4*Power(t1,2)*(7 + t2) - 
                  t1*(276 - 224*t2 + Power(t2,2))) + 
               Power(s2,5)*(1877 - 5596*t2 + 4510*Power(t2,2) - 
                  569*Power(t2,3) - 336*Power(t2,4) + 
                  12*Power(t1,3)*(8 + t2) + 
                  Power(t1,2)*(-986 + 563*t2 + 60*Power(t2,2)) - 
                  2*t1*(-118 - 819*t2 + 519*Power(t2,2) + 
                     48*Power(t2,3))) + 
               Power(s2,4)*(31 - 200*Power(t1,4) + 3783*t2 - 
                  6066*Power(t2,2) + 2951*Power(t2,3) - 
                  1100*Power(t2,4) + 490*Power(t2,5) - 
                  5*Power(t1,3)*(-427 + 197*t2 + 40*Power(t2,2)) - 
                  Power(t1,2)*
                   (458 + 5461*t2 - 3925*Power(t2,2) + 40*Power(t2,3)) \
+ t1*(-8974 + 27986*t2 - 25713*Power(t2,2) + 6761*Power(t2,3) + 
                     450*Power(t2,4))) + 
               Power(s2,3)*(-8583 + Power(t1,5)*(260 - 40*t2) + 
                  29366*t2 - 41469*Power(t2,2) + 32601*Power(t2,3) - 
                  13032*Power(t2,4) + 1857*Power(t2,5) - 
                  210*Power(t2,6) + 
                  10*Power(t1,4)*(-287 + 132*t2 + 25*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (378 + 3987*t2 - 2987*Power(t2,2) + 80*Power(t2,3)) \
+ Power(t1,2)*(18062 - 54126*t2 + 50895*Power(t2,2) - 
                     15538*Power(t2,3) - 140*Power(t2,4)) - 
                  2*t1*(-2278 + 14340*t2 - 16791*Power(t2,2) + 
                     4460*Power(t2,3) + 360*Power(t2,4) + 
                     220*Power(t2,5))) + 
               Power(s2,2)*(6164 - 37646*t2 + 100216*Power(t2,2) - 
                  121523*Power(t2,3) + 62238*Power(t2,4) - 
                  9448*Power(t2,5) + 20*Power(t2,6) + 21*Power(t2,7) + 
                  12*Power(t1,6)*(-17 + 5*t2) + 
                  Power(t1,5)*(2318 - 1202*t2 - 123*Power(t2,2)) - 
                  Power(t1,4)*
                   (350 + 6801*t2 - 5067*Power(t2,2) + 225*Power(t2,3)) \
+ Power(t1,3)*(-20358 + 53866*t2 - 47533*Power(t2,2) + 
                     14316*Power(t2,3) + 330*Power(t2,4)) + 
                  Power(t1,2)*
                   (-10261 + 59356*t2 - 66543*Power(t2,2) + 
                     14411*Power(t2,3) + 4540*Power(t2,4) - 
                     150*Power(t2,5)) + 
                  t1*(17967 - 83204*t2 + 138143*Power(t2,2) - 
                     114337*Power(t2,3) + 45324*Power(t2,4) - 
                     5709*Power(t2,5) + 87*Power(t2,6))) + 
               s2*(2583 + Power(t1,7)*(88 - 36*t2) + 8059*t2 - 
                  60561*Power(t2,2) + 87661*Power(t2,3) - 
                  30041*Power(t2,4) - 14729*Power(t2,5) + 
                  6948*Power(t2,6) - 156*Power(t2,7) + 
                  Power(t1,6)*(-1026 + 619*t2 + 4*Power(t2,2)) + 
                  4*Power(t1,5)*
                   (-76 + 907*t2 - 649*Power(t2,2) + 58*Power(t2,3)) + 
                  Power(t1,4)*
                   (13041 - 29318*t2 + 22683*Power(t2,2) - 
                     6217*Power(t2,3) - 360*Power(t2,4)) + 
                  2*Power(t1,3)*
                   (3599 - 24584*t2 + 29142*Power(t2,2) - 
                     7260*Power(t2,3) - 1505*Power(t2,4) + 
                     10*Power(t2,5)) + 
                  Power(t1,2)*
                   (-7086 + 59946*t2 - 118802*Power(t2,2) + 
                     104533*Power(t2,3) - 41121*Power(t2,4) + 
                     4256*Power(t2,5) + 140*Power(t2,6)) + 
                  2*t1*(-4800 + 33570*t2 - 84504*Power(t2,2) + 
                     91895*Power(t2,3) - 39325*Power(t2,4) + 
                     2156*Power(t2,5) + 988*Power(t2,6))))) - 
         (-1 + t2)*(Power(s1,10)*Power(s2 - t1,3)*
             (-6 + 5*s2 - 5*t1 + 6*t2) + 
            Power(s1,9)*Power(s2 - t1,2)*
             (20*Power(s2,3) - 20*Power(t1,3) + 
               2*Power(-1 + t2,2)*(11 + t2) + 
               Power(s2,2)*(-31 - 60*t1 + 10*t2) + 
               Power(t1,2)*(-32 + 11*t2) + 
               3*s2*(-3 + 20*Power(t1,2) - 7*t1*(-3 + t2) + 8*t2 - 
                  5*Power(t2,2)) + 3*t1*(3 - 8*t2 + 5*Power(t2,2))) + 
            Power(s1,8)*(s2 - t1)*
             (30*Power(s2,5) - 30*Power(t1,5) + 
               2*Power(-1 + t2,3)*(13 + 2*t2) - 
               2*Power(t1,4)*(5 + 7*t2) - 
               2*Power(s2,4)*(5 + 75*t1 + 7*t2) + 
               t1*Power(-1 + t2,2)*(1 + 83*t2) + 
               Power(t1,3)*(130 - 183*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(160 - 208*t2 + Power(t2,2) + 
                  47*Power(t2,3)) + 
               Power(s2,3)*(-150 + 300*Power(t1,2) + 211*t2 - 
                  32*Power(t2,2) + 8*t1*(5 + 7*t2)) + 
               Power(s2,2)*(176 - 300*Power(t1,3) - 255*t2 + 
                  47*Power(t2,2) + 32*Power(t2,3) - 
                  12*Power(t1,2)*(5 + 7*t2) + 
                  t1*(437 - 619*t2 + 95*Power(t2,2))) + 
               s2*(150*Power(t1,4) + 8*Power(t1,3)*(5 + 7*t2) - 
                  Power(-1 + t2,2)*(1 + 83*t2) + 
                  Power(t1,2)*(-417 + 591*t2 - 87*Power(t2,2)) - 
                  t1*(338 - 469*t2 + 54*Power(t2,2) + 77*Power(t2,3)))) \
+ Power(s1,7)*(20*Power(s2,7) - 20*Power(t1,7) + 
               Power(t1,6)*(60 - 46*t2) + 2*Power(-1 + t2,4)*(5 + t2) - 
               2*Power(s2,6)*(-25 + 70*t1 + 18*t2) + 
               t1*Power(-1 + t2,3)*(9 + 121*t2) + 
               Power(t1,5)*(263 - 216*t2 - 11*Power(t2,2)) - 
               3*Power(t1,2)*Power(-1 + t2,2)*
                (113 - 47*t2 - 77*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(183 + 15*t2 - 257*Power(t2,2) + 
                  54*Power(t2,3)) + 
               Power(t1,3)*(-263 + 905*t2 - 736*Power(t2,2) + 
                  15*Power(t2,3) + 79*Power(t2,4)) + 
               2*Power(s2,5)*
                (-139 + 210*Power(t1,2) + 115*t2 + 6*Power(t2,2) + 
                  t1*(-155 + 113*t2)) + 
               Power(s2,4)*(169 - 700*Power(t1,3) + 
                  Power(t1,2)*(800 - 590*t2) + 112*t2 - 
                  364*Power(t2,2) + 78*Power(t2,3) - 
                  2*t1*(-686 + 565*t2 + 31*Power(t2,2))) + 
               Power(s2,3)*(370 + 700*Power(t1,4) - 1251*t2 + 
                  1162*Power(t2,2) - 257*Power(t2,3) - 
                  24*Power(t2,4) + 20*Power(t1,3)*(-55 + 41*t2) + 
                  Power(t1,2)*(-2705 + 2214*t2 + 131*Power(t2,2)) - 
                  t1*(672 + 434*t2 - 1461*Power(t2,2) + 
                     335*Power(t2,3))) + 
               s2*(140*Power(t1,6) + 14*Power(t1,5)*(-25 + 19*t2) - 
                  Power(-1 + t2,3)*(9 + 121*t2) + 
                  Power(t1,4)*(-1321 + 1076*t2 + 65*Power(t2,2)) + 
                  t1*Power(-1 + t2,2)*
                   (713 - 351*t2 - 428*Power(t2,2) + 12*Power(t2,3)) - 
                  Power(t1,3)*
                   (699 + 243*t2 - 1250*Power(t2,2) + 288*Power(t2,3)) \
+ Power(t1,2)*(910 - 3111*t2 + 2700*Power(t2,2) - 325*Power(t2,3) - 
                     174*Power(t2,4))) + 
               Power(s2,2)*(-420*Power(t1,5) + 
                  Power(t1,4)*(850 - 640*t2) + 
                  Power(t1,3)*(2669 - 2174*t2 - 135*Power(t2,2)) - 
                  Power(-1 + t2,2)*
                   (372 - 206*t2 - 199*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (1019 + 550*t2 - 2090*Power(t2,2) + 491*Power(t2,3)) \
+ t1*(-1019 + 3465*t2 - 3138*Power(t2,2) + 575*Power(t2,3) + 
                     117*Power(t2,4)))) + 
            Power(s1,6)*(5*Power(s2,8) + 5*Power(t1,8) - 
               2*Power(s2,7)*(-15 + 20*t1 + 7*t2) + 
               Power(t1,7)*(-50 + 34*t2) - 
               Power(-1 + t2,4)*(4 + 53*t2) + 
               Power(t1,6)*(-13 - 36*t2 + 45*Power(t2,2)) + 
               t1*Power(-1 + t2,3)*
                (324 - 182*t2 - 321*Power(t2,2) + 12*Power(t2,3)) + 
               Power(t1,5)*(482 - 875*t2 + 318*Power(t2,2) + 
                  27*Power(t2,3)) + 
               Power(t1,4)*(1066 - 1974*t2 + 511*Power(t2,2) + 
                  493*Power(t2,3) - 121*Power(t2,4)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-250 + 1311*t2 - 503*Power(t2,2) - 299*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(t1,3)*(-718 + 813*t2 + 1211*Power(t2,2) - 
                  1675*Power(t2,3) + 320*Power(t2,4) + 49*Power(t2,5)) \
+ 2*Power(s2,6)*(-2 + 70*Power(t1,2) - 33*t2 + 33*Power(t2,2) + 
                  t1*(-115 + 59*t2)) + 
               Power(s2,5)*(-531 - 280*Power(t1,3) + 
                  Power(t1,2)*(750 - 414*t2) + 1046*t2 - 
                  497*Power(t2,2) + 30*Power(t2,3) + 
                  t1*(2 + 428*t2 - 406*Power(t2,2))) + 
               Power(s2,4)*(1298 + 350*Power(t1,4) - 2499*t2 + 
                  815*Power(t2,2) + 452*Power(t2,3) - 91*Power(t2,4) + 
                  10*Power(t1,3)*(-135 + 79*t2) + 
                  3*Power(t1,2)*(9 - 368*t2 + 339*Power(t2,2)) - 
                  3*t1*(-875 + 1720*t2 - 817*Power(t2,2) + 
                     52*Power(t2,3))) - 
               Power(s2,3)*(765 + 280*Power(t1,5) - 594*t2 - 
                  2072*Power(t2,2) + 2619*Power(t2,3) - 
                  736*Power(t2,4) + 18*Power(t2,5) + 
                  10*Power(t1,4)*(-145 + 89*t2) + 
                  40*Power(t1,3)*(1 - 36*t2 + 33*Power(t2,2)) + 
                  Power(t1,2)*
                   (5158 - 10026*t2 + 4622*Power(t2,2) - 
                     234*Power(t2,3)) + 
                  t1*(4911 - 9303*t2 + 2686*Power(t2,2) + 
                     2081*Power(t2,3) - 475*Power(t2,4))) + 
               Power(s2,2)*(140*Power(t1,6) + 
                  6*Power(t1,5)*(-155 + 99*t2) + 
                  Power(t1,4)*(-2 - 986*t2 + 928*Power(t2,2)) + 
                  Power(t1,3)*
                   (5047 - 9631*t2 + 4197*Power(t2,2) - 
                     93*Power(t2,3)) + 
                  Power(t1,2)*
                   (6975 - 13013*t2 + 3342*Power(t2,2) + 
                     3357*Power(t2,3) - 811*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-404 + 1699*t2 - 857*Power(t2,2) - 
                     179*Power(t2,3) + 4*Power(t2,4)) + 
                  t1*(2211 - 1804*t2 - 5738*Power(t2,2) + 
                     7250*Power(t2,3) - 1920*Power(t2,4) + Power(t2,5))\
) + s2*(-40*Power(t1,7) + Power(t1,6)*(330 - 218*t2) - 
                  6*Power(t1,5)*(-5 - 54*t2 + 55*Power(t2,2)) - 
                  Power(-1 + t2,3)*
                   (340 - 215*t2 - 304*Power(t2,2) + 12*Power(t2,3)) - 
                  Power(t1,4)*
                   (2465 - 4594*t2 + 1847*Power(t2,2) + 42*Power(t2,3)) \
- t1*Power(-1 + t2,2)*(-665 + 3038*t2 - 1383*Power(t2,2) - 
                     472*Power(t2,3) + 8*Power(t2,4)) + 
                  Power(t1,3)*
                   (-4428 + 8183*t2 - 1982*Power(t2,2) - 
                     2221*Power(t2,3) + 548*Power(t2,4)) + 
                  Power(t1,2)*
                   (-2161 + 2015*t2 + 4879*Power(t2,2) - 
                     6294*Power(t2,3) + 1491*Power(t2,4) + 
                     70*Power(t2,5)))) - 
            Power(-1 + t2,3)*
             (-60 + Power(t1,7)*(6 - 4*t2) - 49*t2 + 1064*Power(t2,2) - 
               2636*Power(t2,3) + 2922*Power(t2,4) - 1557*Power(t2,5) + 
               284*Power(t2,6) + 32*Power(t2,7) + 
               Power(t1,6)*(-14 - 17*t2 + 29*Power(t2,2)) - 
               2*Power(s2,7)*(2 - 5*t2 + 3*Power(t2,2) + Power(t2,3)) - 
               Power(t1,5)*(24 - 118*t2 + 57*Power(t2,2) + 
                  51*Power(t2,3)) + 
               Power(t1,4)*(8 + 85*t2 - 388*Power(t2,2) + 
                  292*Power(t2,3) + 39*Power(t2,4)) + 
               Power(t1,3)*(70 - 340*t2 + 404*Power(t2,2) + 
                  210*Power(t2,3) - 353*Power(t2,4) - 25*Power(t2,5)) + 
               Power(t1,2)*(66 - 323*t2 + 967*Power(t2,2) - 
                  1224*Power(t2,3) + 327*Power(t2,4) + 
                  181*Power(t2,5) + 20*Power(t2,6)) - 
               t1*(52 + 78*t2 - 1005*Power(t2,2) + 2287*Power(t2,3) - 
                  2105*Power(t2,4) + 615*Power(t2,5) + 80*Power(t2,6)) \
+ Power(s2,6)*(-2 - 10*t2 + 30*Power(t2,2) - 21*Power(t2,3) + 
                  Power(t2,4) + 
                  t1*(26 - 62*t2 + 44*Power(t2,2) + 6*Power(t2,3))) + 
               Power(s2,5)*(62 - 208*t2 + 183*Power(t2,2) + 
                  3*Power(t2,3) - 26*Power(t2,4) - 
                  2*Power(t1,2)*
                   (36 - 76*t2 + 59*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(6 + 88*t2 - 169*Power(t2,2) + 94*Power(t2,3) - 
                     7*Power(t2,4))) + 
               Power(s2,4)*(16 + 142*t2 - 636*Power(t2,2) + 
                  670*Power(t2,3) - 163*Power(t2,4) + 7*Power(t2,5) - 
                  2*Power(t1,3)*
                   (-57 + 96*t2 - 76*Power(t2,2) + 2*Power(t2,3)) + 
                  5*Power(t1,2)*
                   (-2 - 58*t2 + 84*Power(t2,2) - 33*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(-296 + 1026*t2 - 912*Power(t2,2) + 
                     51*Power(t2,3) + 61*Power(t2,4))) + 
               Power(s2,3)*(-216 + 811*t2 - 906*Power(t2,2) - 
                  140*Power(t2,3) + 603*Power(t2,4) - 
                  118*Power(t2,5) + 
                  2*Power(t1,4)*
                   (-58 + 69*t2 - 49*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(t1,2)*
                   (524 - 1887*t2 + 1608*Power(t2,2) - 
                     65*Power(t2,3) - 40*Power(t2,4)) + 
                  Power(t1,3)*
                   (28 + 459*t2 - 575*Power(t2,2) + 141*Power(t2,3) - 
                     13*Power(t2,4)) - 
                  t1*(28 + 569*t2 - 2301*Power(t2,2) + 
                     2262*Power(t2,3) - 447*Power(t2,4) + 
                     33*Power(t2,5))) + 
               Power(s2,2)*(110 - 1051*t2 + 3222*Power(t2,2) - 
                  4133*Power(t2,3) + 2080*Power(t2,4) - 
                  234*Power(t2,5) + 20*Power(t2,6) - 
                  2*Power(t1,5)*
                   (-39 + 31*t2 - 14*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,3)*
                   (-432 + 1646*t2 - 1269*Power(t2,2) - 
                     86*Power(t2,3) + Power(t2,4)) + 
                  Power(t1,4)*
                   (-54 - 367*t2 + 445*Power(t2,2) - 58*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  Power(t1,2)*
                   (8 + 818*t2 - 3097*Power(t2,2) + 
                     2805*Power(t2,3) - 363*Power(t2,4) + 
                     45*Power(t2,5)) + 
                  t1*(610 - 2360*t2 + 2843*Power(t2,2) - 
                     82*Power(t2,3) - 1244*Power(t2,4) + 
                     131*Power(t2,5))) + 
               s2*(94 + 355*t2 - 2791*Power(t2,2) + 5795*Power(t2,3) - 
                  5321*Power(t2,4) + 2030*Power(t2,5) - 
                  160*Power(t2,6) - 
                  2*Power(t1,6)*(16 - 10*t2 + Power(t2,2)) + 
                  Power(t1,5)*
                   (46 + 137*t2 - 180*Power(t2,2) + 9*Power(t2,3)) + 
                  Power(t1,4)*
                   (166 - 695*t2 + 447*Power(t2,2) + 148*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  Power(t1,2)*
                   (-452 + 1840*t2 - 2266*Power(t2,2) - 
                     39*Power(t2,3) + 1007*Power(t2,4) + 12*Power(t2,5)\
) - Power(t1,3)*(4 + 476*t2 - 1820*Power(t2,2) + 1505*Power(t2,3) - 
                     40*Power(t2,4) + 19*Power(t2,5)) + 
                  t1*(-266 + 1731*t2 - 4716*Power(t2,2) + 
                     5712*Power(t2,3) - 2530*Power(t2,4) + 
                     97*Power(t2,5) - 56*Power(t2,6)))) + 
            Power(s1,5)*(Power(t1,8)*(4 - 5*t2) + 
               Power(s2,8)*(-11 + 10*t2) - 
               2*Power(t1,7)*(22 - 51*t2 + 17*Power(t2,2)) + 
               Power(s2,7)*(21 + t1*(73 - 65*t2) - 76*t2 + 
                  31*Power(t2,2)) + 
               Power(t1,6)*(-49 + 53*t2 + 21*Power(t2,2) - 
                  45*Power(t2,3)) - 
               Power(-1 + t2,4)*
                (113 - 78*t2 - 137*Power(t2,2) + 6*Power(t2,3)) + 
               3*Power(t1,5)*
                (141 - 501*t2 + 506*Power(t2,2) - 153*Power(t2,3) + 
                  3*Power(t2,4)) - 
               t1*Power(-1 + t2,3)*
                (-133 + 1322*t2 - 820*Power(t2,2) - 361*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (1266 - 494*t2 - 2751*Power(t2,2) + 1344*Power(t2,3) + 
                  149*Power(t2,4)) + 
               Power(t1,4)*(1286 - 4507*t2 + 4586*Power(t2,2) - 
                  977*Power(t2,3) - 466*Power(t2,4) + 103*Power(t2,5)) \
+ Power(t1,3)*(2020 - 6385*t2 + 5306*Power(t2,2) + 1314*Power(t2,3) - 
                  2924*Power(t2,4) + 661*Power(t2,5) + 8*Power(t2,6)) + 
               Power(s2,6)*(125 - 316*t2 + 253*Power(t2,2) - 
                  82*Power(t2,3) + 7*Power(t1,2)*(-29 + 25*t2) + 
                  t1*(-195 + 608*t2 - 245*Power(t2,2))) + 
               Power(s2,5)*(-648 + 2501*t2 - 2894*Power(t2,2) + 
                  1165*Power(t2,3) - 112*Power(t2,4) - 
                  7*Power(t1,3)*(-43 + 35*t2) + 
                  18*Power(t1,2)*(39 - 111*t2 + 44*Power(t2,2)) + 
                  2*t1*(-331 + 866*t2 - 719*Power(t2,2) + 
                     244*Power(t2,3))) + 
               Power(s2,4)*(1969 - 7377*t2 + 8902*Power(t2,2) - 
                  3763*Power(t2,3) + 298*Power(t2,4) - 4*Power(t2,5) + 
                  35*Power(t1,4)*(-7 + 5*t2) - 
                  2*Power(t1,3)*(661 - 1767*t2 + 686*Power(t2,2)) - 
                  4*Power(t1,2)*
                   (-344 + 954*t2 - 841*Power(t2,2) + 306*Power(t2,3)) \
+ t1*(3277 - 12366*t2 + 14201*Power(t2,2) - 5804*Power(t2,3) + 
                     632*Power(t2,4))) + 
               Power(s2,3)*(-2908 + Power(t1,5)*(91 - 35*t2) + 
                  9622*t2 - 9439*Power(t2,2) + 730*Power(t2,3) + 
                  2754*Power(t2,4) - 778*Power(t2,5) + 
                  19*Power(t2,6) + 
                  Power(t1,4)*(1433 - 3656*t2 + 1383*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1357 + 4163*t2 - 4015*Power(t2,2) + 
                     1609*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (3187 - 11802*t2 + 13363*Power(t2,2) - 
                     5412*Power(t2,3) + 604*Power(t2,4)) - 
                  t1*(7294 - 27048*t2 + 31923*Power(t2,2) - 
                     12653*Power(t2,3) + 444*Power(t2,4) + 
                     140*Power(t2,5))) + 
               Power(s2,2)*(Power(t1,6)*(7 - 35*t2) - 
                  3*Power(t1,5)*(301 - 740*t2 + 271*Power(t2,2)) + 
                  Power(t1,4)*
                   (560 - 2173*t2 + 2446*Power(t2,2) - 
                     1133*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (1405 - 243*t2 - 3614*Power(t2,2) + 
                     1965*Power(t2,3) + Power(t2,4)) + 
                  Power(t1,3)*
                   (5932 - 21617*t2 + 23943*Power(t2,2) - 
                     9355*Power(t2,3) + 977*Power(t2,4)) + 
                  Power(t1,2)*
                   (9930 - 36285*t2 + 41364*Power(t2,2) - 
                     14660*Power(t2,3) - 617*Power(t2,4) + 
                     418*Power(t2,5)) + 
                  t1*(7784 - 25381*t2 + 23616*Power(t2,2) + 
                     596*Power(t2,3) - 8950*Power(t2,4) + 
                     2371*Power(t2,5) - 36*Power(t2,6))) + 
               s2*(Power(t1,7)*(-17 + 25*t2) + 
                  Power(t1,6)*(308 - 734*t2 + 258*Power(t2,2)) + 
                  Power(t1,5)*
                   (7 + 357*t2 - 631*Power(t2,2) + 387*Power(t2,3)) + 
                  Power(t1,4)*
                   (-2610 + 9381*t2 - 10042*Power(t2,2) + 
                     3629*Power(t2,3) - 298*Power(t2,4)) + 
                  Power(-1 + t2,3)*
                   (-198 + 1499*t2 - 995*Power(t2,2) - 
                     298*Power(t2,3) + 8*Power(t2,4)) - 
                  2*t1*Power(-1 + t2,2)*
                   (1327 - 328*t2 - 3241*Power(t2,2) + 
                     1684*Power(t2,3) + 72*Power(t2,4)) + 
                  Power(t1,3)*
                   (-5891 + 21121*t2 - 22929*Power(t2,2) + 
                     6747*Power(t2,3) + 1229*Power(t2,4) - 
                     377*Power(t2,5)) + 
                  Power(t1,2)*
                   (-6873 + 22027*t2 - 19247*Power(t2,2) - 
                     2874*Power(t2,3) + 9231*Power(t2,4) - 
                     2271*Power(t2,5) + 7*Power(t2,6)))) + 
            s1*Power(-1 + t2,2)*
             (-6*Power(t1,8)*(-1 + t2) + 
               4*Power(s2,8)*(2 - 5*t2 + 3*Power(t2,2)) + 
               Power(t1,7)*(4 - 51*t2 + 56*Power(t2,2)) - 
               Power(t1,6)*(99 - 224*t2 + 54*Power(t2,2) + 
                  109*Power(t2,3)) + 
               2*Power(t1,5)*
                (-12 + 252*t2 - 555*Power(t2,2) + 305*Power(t2,3) + 
                  41*Power(t2,4)) + 
               Power(t1,4)*(201 - 657*t2 + 66*Power(t2,2) + 
                  1234*Power(t2,3) - 829*Power(t2,4) - 63*Power(t2,5)) \
+ Power(-1 + t2,2)*(17 - 1039*t2 + 2767*Power(t2,2) - 
                  2458*Power(t2,3) + 557*Power(t2,4) + 192*Power(t2,5)) \
+ Power(t1,3)*(336 - 1953*t2 + 4202*Power(t2,2) - 3463*Power(t2,3) + 
                  470*Power(t2,4) + 369*Power(t2,5) + 56*Power(t2,6)) - 
               Power(t1,2)*(45 + 1128*t2 - 5874*Power(t2,2) + 
                  10162*Power(t2,3) - 7199*Power(t2,4) + 
                  1589*Power(t2,5) + 151*Power(t2,6)) + 
               t1*(-236 - 628*t2 + 6096*Power(t2,2) - 
                  13287*Power(t2,3) + 12638*Power(t2,4) - 
                  5089*Power(t2,5) + 386*Power(t2,6) + 120*Power(t2,7)) \
+ Power(s2,7)*(-6 + 42*t2 - 74*Power(t2,2) + 28*Power(t2,3) + 
                  Power(t2,4) + 
                  2*t1*(-30 + 72*t2 - 43*Power(t2,2) + Power(t2,3))) + 
               Power(s2,6)*(-152 + 454*t2 - 387*Power(t2,2) + 
                  3*Power(t2,3) + 44*Power(t2,4) + 
                  t1*(48 - 330*t2 + 536*Power(t2,2) - 
                     191*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (-98 + 222*t2 - 129*Power(t2,2) + 5*Power(t2,3))) + 
               Power(s2,5)*(126 - 904*t2 + 1934*Power(t2,2) - 
                  1521*Power(t2,3) + 296*Power(t2,4) + 7*Power(t2,5) + 
                  Power(t1,3)*
                   (-366 + 766*t2 - 420*Power(t2,2) + 20*Power(t2,3)) \
+ Power(t1,2)*(-144 + 1036*t2 - 1581*Power(t2,2) + 510*Power(t2,3) - 
                     10*Power(t2,4)) - 
                  2*t1*(-428 + 1281*t2 - 1090*Power(t2,2) + 
                     36*Power(t2,3) + 87*Power(t2,4))) + 
               Power(s2,4)*(614 - 1765*t2 + 869*Power(t2,2) + 
                  1682*Power(t2,3) - 1777*Power(t2,4) + 
                  329*Power(t2,5) - 
                  10*Power(t1,4)*
                   (-43 + 81*t2 - 40*Power(t2,2) + 2*Power(t2,3)) + 
                  5*Power(t1,3)*
                   (44 - 347*t2 + 500*Power(t2,2) - 138*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  Power(t1,2)*
                   (-1962 + 5815*t2 - 4799*Power(t2,2) + 
                     126*Power(t2,3) + 250*Power(t2,4)) + 
                  t1*(-680 + 4605*t2 - 9494*Power(t2,2) + 
                     7195*Power(t2,3) - 1340*Power(t2,4) + 
                     24*Power(t2,5))) + 
               Power(s2,3)*(-907 + 5255*t2 - 11414*Power(t2,2) + 
                  11071*Power(t2,3) - 4329*Power(t2,4) + 
                  306*Power(t2,5) + Power(t2,6) + 
                  2*Power(t1,5)*
                   (-164 + 270*t2 - 111*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,3)*
                   (2369 - 6837*t2 + 5241*Power(t2,2) + 
                     133*Power(t2,3) - 146*Power(t2,4)) - 
                  5*Power(t1,4)*
                   (38 - 342*t2 + 464*Power(t2,2) - 100*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  Power(t1,2)*
                   (1259 - 8671*t2 + 17603*Power(t2,2) - 
                     12768*Power(t2,3) + 2064*Power(t2,4) - 
                     107*Power(t2,5)) - 
                  t1*(2243 - 6719*t2 + 3904*Power(t2,2) + 
                     5210*Power(t2,3) - 5645*Power(t2,4) + 
                     815*Power(t2,5))) + 
               s2*(171 + 2389*t2 - 13404*Power(t2,2) + 
                  26206*Power(t2,3) - 24254*Power(t2,4) + 
                  10445*Power(t2,5) - 1497*Power(t2,6) - 
                  56*Power(t2,7) + 
                  Power(t1,7)*(-46 + 54*t2 - 8*Power(t2,2)) + 
                  Power(t1,6)*
                   (-28 + 340*t2 - 401*Power(t2,2) + 26*Power(t2,3)) + 
                  Power(t1,5)*
                   (603 - 1525*t2 + 743*Power(t2,2) + 
                     399*Power(t2,3) + 8*Power(t2,4)) + 
                  Power(t1,4)*
                   (325 - 3181*t2 + 6637*Power(t2,2) - 
                     4161*Power(t2,3) + 108*Power(t2,4) - 
                     38*Power(t2,5)) + 
                  Power(t1,3)*
                   (-1394 + 4478*t2 - 2407*Power(t2,2) - 
                     4067*Power(t2,3) + 3567*Power(t2,4) + 
                     15*Power(t2,5)) + 
                  t1*(-363 + 6793*t2 - 25732*Power(t2,2) + 
                     40500*Power(t2,3) - 29535*Power(t2,4) + 
                     8885*Power(t2,5) - 544*Power(t2,6)) + 
                  Power(t1,2)*
                   (-2180 + 11628*t2 - 23956*Power(t2,2) + 
                     21713*Power(t2,3) - 7246*Power(t2,4) + 
                     197*Power(t2,5) - 207*Power(t2,6))) + 
               Power(s2,2)*(129 - 4474*t2 + 17862*Power(t2,2) - 
                  28650*Power(t2,3) + 21523*Power(t2,4) - 
                  7031*Power(t2,5) + 639*Power(t2,6) - 
                  2*Power(t1,6)*
                   (-80 + 112*t2 - 33*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,5)*
                   (96 - 1012*t2 + 1284*Power(t2,2) - 
                     183*Power(t2,3) + 4*Power(t2,4)) + 
                  Power(t1,4)*
                   (-1615 + 4431*t2 - 2924*Power(t2,2) - 
                     480*Power(t2,3) + 18*Power(t2,4)) + 
                  Power(t1,3)*
                   (-1006 + 7647*t2 - 15570*Power(t2,2) + 
                     10645*Power(t2,3) - 1210*Power(t2,4) + 
                     114*Power(t2,5)) + 
                  Power(t1,2)*
                   (2822 - 8775*t2 + 5376*Power(t2,2) + 
                     6361*Power(t2,3) - 6606*Power(t2,4) + 
                     534*Power(t2,5)) + 
                  t1*(2774 - 15001*t2 + 31204*Power(t2,2) - 
                     29217*Power(t2,3) + 10942*Power(t2,4) - 
                     785*Power(t2,5) + 134*Power(t2,6)))) + 
            Power(s1,4)*(6*Power(s2,9)*(-1 + t2) - 
               2*Power(t1,9)*(-1 + t2) + 
               Power(s2,8)*(-42 - 50*t1*(-1 + t2) + 55*t2 - 
                  18*Power(t2,2)) + 
               Power(t1,8)*(-9 - 3*t2 + 7*Power(t2,2)) + 
               Power(t1,7)*(-70 + 243*t2 - 173*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(-1 + t2,4)*
                (-34 + 490*t2 - 387*Power(t2,2) - 141*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,6)*(51 + 197*t2 - 500*Power(t2,2) + 
                  197*Power(t2,3) + 29*Power(t2,4)) - 
               t1*Power(-1 + t2,3)*
                (1065 - 649*t2 - 2536*Power(t2,2) + 1797*Power(t2,3) + 
                  151*Power(t2,4)) + 
               Power(t1,5)*(451 - 2131*t2 + 2910*Power(t2,2) - 
                  1488*Power(t2,3) + 317*Power(t2,4) - 35*Power(t2,5)) \
- Power(t1,2)*Power(-1 + t2,2)*
                (-2125 + 5997*t2 - 2521*Power(t2,2) - 
                  3347*Power(t2,3) + 1580*Power(t2,4) + 20*Power(t2,5)) \
+ Power(t1,3)*(1474 - 8760*t2 + 16972*Power(t2,2) - 
                  12531*Power(t2,3) + 1306*Power(t2,4) + 
                  1990*Power(t2,5) - 451*Power(t2,6)) + 
               Power(t1,4)*(781 - 5221*t2 + 10335*Power(t2,2) - 
                  7860*Power(t2,3) + 1920*Power(t2,4) + 
                  64*Power(t2,5) - 28*Power(t2,6)) + 
               Power(s2,7)*(139 + 184*Power(t1,2)*(-1 + t2) - 461*t2 + 
                  390*Power(t2,2) - 84*Power(t2,3) + 
                  t1*(299 - 374*t2 + 115*Power(t2,2))) - 
               Power(s2,6)*(-278 + 392*Power(t1,3)*(-1 + t2) + 
                  376*t2 + 146*Power(t2,2) - 248*Power(t2,3) + 
                  30*Power(t2,4) + 
                  Power(t1,2)*(925 - 1094*t2 + 309*Power(t2,2)) + 
                  t1*(963 - 3184*t2 + 2686*Power(t2,2) - 
                     577*Power(t2,3))) + 
               Power(s2,5)*(-1164 + 532*Power(t1,4)*(-1 + t2) + 
                  4792*t2 - 6754*Power(t2,2) + 3966*Power(t2,3) - 
                  900*Power(t2,4) + 36*Power(t2,5) + 
                  Power(t1,3)*(1619 - 1782*t2 + 443*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (-929 + 3077*t2 - 2586*Power(t2,2) + 
                     550*Power(t2,3)) + 
                  t1*(-1411 + 1619*t2 + 1327*Power(t2,2) - 
                     1559*Power(t2,3) + 180*Power(t2,4))) + 
               Power(s2,4)*(1883 - 476*Power(t1,5)*(-1 + t2) - 
                  11510*t2 + 23354*Power(t2,2) - 20381*Power(t2,3) + 
                  7577*Power(t2,4) - 966*Power(t2,5) + 
                  34*Power(t2,6) - 
                  5*Power(t1,4)*(349 - 348*t2 + 69*Power(t2,2)) + 
                  Power(t1,3)*
                   (-4368 + 14547*t2 - 12145*Power(t2,2) + 
                     2526*Power(t2,3)) + 
                  Power(t1,2)*
                   (2964 - 2759*t2 - 3942*Power(t2,2) + 
                     3665*Power(t2,3) - 318*Power(t2,4)) + 
                  t1*(5728 - 23442*t2 + 32922*Power(t2,2) - 
                     19548*Power(t2,3) + 4788*Power(t2,4) - 
                     328*Power(t2,5))) + 
               Power(s2,3)*(-3134 + 280*Power(t1,6)*(-1 + t2) + 
                  18202*t2 - 37465*Power(t2,2) + 34109*Power(t2,3) - 
                  12741*Power(t2,4) + 868*Power(t2,5) + 
                  161*Power(t2,6) + 
                  Power(t1,5)*(1177 - 1010*t2 + 113*Power(t2,2)) + 
                  Power(t1,4)*
                   (3997 - 13423*t2 + 11070*Power(t2,2) - 
                     2204*Power(t2,3)) + 
                  Power(t1,3)*
                   (-3257 + 2126*t2 + 5824*Power(t2,2) - 
                     4318*Power(t2,3) + 145*Power(t2,4)) + 
                  Power(t1,2)*
                   (-10692 + 43961*t2 - 61683*Power(t2,2) + 
                     36801*Power(t2,3) - 9437*Power(t2,4) + 
                     810*Power(t2,5)) + 
                  t1*(-7250 + 42974*t2 - 85575*Power(t2,2) + 
                     73404*Power(t2,3) - 26703*Power(t2,4) + 
                     3252*Power(t2,5) - 66*Power(t2,6))) + 
               Power(s2,2)*(-104*Power(t1,7)*(-1 + t2) + 
                  Power(t1,6)*(-479 + 314*t2 + 25*Power(t2,2)) + 
                  3*Power(t1,5)*
                   (-709 + 2406*t2 - 1940*Power(t2,2) + 
                     355*Power(t2,3)) + 
                  Power(t1,4)*
                   (1927 - 374*t2 - 4900*Power(t2,2) + 
                     2834*Power(t2,3) + 123*Power(t2,4)) + 
                  Power(t1,3)*
                   (9307 - 38895*t2 + 54526*Power(t2,2) - 
                     32310*Power(t2,3) + 8427*Power(t2,4) - 
                     815*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (3238 - 9215*t2 + 5239*Power(t2,2) + 
                     2907*Power(t2,3) - 1813*Power(t2,4) + 
                     40*Power(t2,5)) + 
                  t1*(7954 - 46052*t2 + 93358*Power(t2,2) - 
                     81823*Power(t2,3) + 26974*Power(t2,4) + 
                     448*Power(t2,5) - 859*Power(t2,6)) + 
                  Power(t1,2)*
                   (9626 - 56588*t2 + 111258*Power(t2,2) - 
                     93265*Power(t2,3) + 32385*Power(t2,4) - 
                     3461*Power(t2,5) - 9*Power(t2,6))) + 
               s2*(22*Power(t1,8)*(-1 + t2) + 
                  Power(t1,7)*(105 - 34*t2 - 31*Power(t2,2)) + 
                  Power(t1,6)*
                   (605 - 2077*t2 + 1606*Power(t2,2) - 246*Power(t2,3)) \
+ Power(-1 + t2,3)*(1141 - 571*t2 - 2911*Power(t2,2) + 
                     2095*Power(t2,3) + 74*Power(t2,4)) - 
                  Power(t1,5)*
                   (552 + 433*t2 - 2337*Power(t2,2) + 
                     1067*Power(t2,3) + 129*Power(t2,4)) - 
                  t1*Power(-1 + t2,2)*
                   (5367 - 15187*t2 + 7617*Power(t2,2) + 
                     6451*Power(t2,3) - 3478*Power(t2,4) + 
                     22*Power(t2,5)) + 
                  Power(t1,4)*
                   (-3630 + 15715*t2 - 21921*Power(t2,2) + 
                     12579*Power(t2,3) - 3195*Power(t2,4) + 
                     332*Power(t2,5)) + 
                  Power(t1,3)*
                   (-5040 + 30345*t2 - 59372*Power(t2,2) + 
                     48102*Power(t2,3) - 15179*Power(t2,4) + 
                     1111*Power(t2,5) + 69*Power(t2,6)) + 
                  Power(t1,2)*
                   (-6276 + 36477*t2 - 72476*Power(t2,2) + 
                     59659*Power(t2,3) - 15055*Power(t2,4) - 
                     3515*Power(t2,5) + 1186*Power(t2,6)))) + 
            Power(s1,2)*(-1 + t2)*
             (2*Power(t1,9)*(-1 + t2) + 
               Power(t1,8)*(-12 + 38*t2 - 29*Power(t2,2)) + 
               2*Power(s2,9)*(2 - 5*t2 + 3*Power(t2,2)) + 
               Power(t1,7)*(65 - 76*t2 - 41*Power(t2,2) + 
                  65*Power(t2,3)) + 
               Power(t1,6)*(140 - 761*t2 + 997*Power(t2,2) - 
                  351*Power(t2,3) - 47*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (-354 + 1552*t2 - 1472*Power(t2,2) - 270*Power(t2,3) + 
                  605*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t1,5)*(-197 - 21*t2 + 1547*Power(t2,2) - 
                  1977*Power(t2,3) + 615*Power(t2,4) + 51*Power(t2,5)) \
- t1*Power(-1 + t2,2)*(209 - 3873*t2 + 9064*Power(t2,2) - 
                  6526*Power(t2,3) + 426*Power(t2,4) + 629*Power(t2,5)) \
+ Power(t1,4)*(-634 + 3370*t2 - 5523*Power(t2,2) + 2875*Power(t2,3) + 
                  123*Power(t2,4) - 166*Power(t2,5) - 52*Power(t2,6)) + 
               Power(t1,3)*(-289 + 3900*t2 - 12945*Power(t2,2) + 
                  17215*Power(t2,3) - 9502*Power(t2,4) + 
                  1595*Power(t2,5) + 27*Power(t2,6)) + 
               Power(t1,2)*(228 + 2883*t2 - 15561*Power(t2,2) + 
                  27910*Power(t2,3) - 22012*Power(t2,4) + 
                  6534*Power(t2,5) + 190*Power(t2,6) - 172*Power(t2,7)) \
+ Power(s2,8)*(-18 + 54*t2 - 56*Power(t2,2) + 17*Power(t2,3) + 
                  t1*(-34 + 82*t2 - 48*Power(t2,2))) + 
               Power(s2,7)*(-112 + 272*t2 - 193*Power(t2,2) + 
                  6*Power(t2,3) + 14*Power(t2,4) + 
                  8*Power(t1,2)*(16 - 37*t2 + 21*Power(t2,2)) + 
                  t1*(138 - 424*t2 + 437*Power(t2,2) - 127*Power(t2,3))\
) + Power(s2,6)*(342 - 1479*t2 + 2133*Power(t2,2) - 1199*Power(t2,3) + 
                  171*Power(t2,4) + 10*Power(t2,5) - 
                  56*Power(t1,3)*(5 - 11*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (-454 + 1424*t2 - 1451*Power(t2,2) + 
                     397*Power(t2,3)) + 
                  t1*(718 - 1682*t2 + 1117*Power(t2,2) + 
                     27*Power(t2,3) - 89*Power(t2,4))) + 
               Power(s2,4)*(-1819 + 8292*t2 - 13845*Power(t2,2) + 
                  9800*Power(t2,3) - 2342*Power(t2,4) - 
                  142*Power(t2,5) + 49*Power(t2,6) - 
                  28*Power(t1,5)*(13 - 25*t2 + 12*Power(t2,2)) + 
                  5*Power(t1,4)*
                   (-194 + 628*t2 - 611*Power(t2,2) + 135*Power(t2,3)) \
+ Power(t1,3)*(3021 - 6400*t2 + 3509*Power(t2,2) + 563*Power(t2,3) - 
                     238*Power(t2,4)) + 
                  t1*(-1983 + 2025*t2 + 8330*Power(t2,2) - 
                     15201*Power(t2,3) + 7928*Power(t2,4) - 
                     1009*Power(t2,5)) + 
                  Power(t1,2)*
                   (4671 - 20653*t2 + 29480*Power(t2,2) - 
                     16230*Power(t2,3) + 2427*Power(t2,4) - 
                     25*Power(t2,5))) + 
               Power(s2,5)*(463 - 561*t2 - 1629*Power(t2,2) + 
                  3235*Power(t2,3) - 1771*Power(t2,4) + 
                  245*Power(t2,5) + 
                  28*Power(t1,4)*(14 - 29*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*
                   (842 - 2688*t2 + 2689*Power(t2,2) - 
                     675*Power(t2,3)) + 
                  Power(t1,2)*
                   (-1974 + 4426*t2 - 2715*Power(t2,2) - 
                     222*Power(t2,3) + 212*Power(t2,4)) - 
                  t1*(2007 - 8738*t2 + 12541*Power(t2,2) - 
                     7018*Power(t2,3) + 1058*Power(t2,4) + 
                     18*Power(t2,5))) + 
               Power(s2,3)*(1364 - 12395*t2 + 36319*Power(t2,2) - 
                  48207*Power(t2,3) + 30819*Power(t2,4) - 
                  8703*Power(t2,5) + 802*Power(t2,6) + 
                  56*Power(t1,6)*(4 - 7*t2 + 3*Power(t2,2)) + 
                  Power(t1,5)*
                   (718 - 2344*t2 + 2191*Power(t2,2) - 
                     397*Power(t2,3)) + 
                  Power(t1,4)*
                   (-2784 + 5460*t2 - 2471*Power(t2,2) - 
                     782*Power(t2,3) + 122*Power(t2,4)) + 
                  Power(t1,3)*
                   (-5531 + 25082*t2 - 35521*Power(t2,2) + 
                     18923*Power(t2,3) - 2596*Power(t2,4) + 
                     83*Power(t2,5)) + 
                  Power(t1,2)*
                   (3428 - 3068*t2 - 15892*Power(t2,2) + 
                     27237*Power(t2,3) - 13300*Power(t2,4) + 
                     1415*Power(t2,5)) + 
                  t1*(7083 - 32203*t2 + 53542*Power(t2,2) - 
                     38006*Power(t2,3) + 9885*Power(t2,4) - 
                     282*Power(t2,5) + 9*Power(t2,6))) - 
               Power(s2,2)*(493 - 10912*t2 + 42714*Power(t2,2) - 
                  70977*Power(t2,3) + 57963*Power(t2,4) - 
                  22219*Power(t2,5) + 2957*Power(t2,6) - 
                  19*Power(t2,7) + 
                  8*Power(t1,7)*(11 - 17*t2 + 6*Power(t2,2)) + 
                  Power(t1,6)*
                   (338 - 1104*t2 + 977*Power(t2,2) - 127*Power(t2,3)) \
+ Power(t1,5)*(-1548 + 2722*t2 - 807*Power(t2,2) - 657*Power(t2,3) + 
                     17*Power(t2,4)) + 
                  Power(t1,4)*
                   (-3507 + 16523*t2 - 23098*Power(t2,2) + 
                     11584*Power(t2,3) - 1241*Power(t2,4) + 
                     69*Power(t2,5)) + 
                  Power(t1,3)*
                   (2956 - 2284*t2 - 14857*Power(t2,2) + 
                     23788*Power(t2,3) - 10515*Power(t2,4) + 
                     732*Power(t2,5)) + 
                  Power(t1,2)*
                   (9336 - 43025*t2 + 71660*Power(t2,2) - 
                     50458*Power(t2,3) + 13361*Power(t2,4) - 
                     1076*Power(t2,5) + 244*Power(t2,6)) + 
                  t1*(4200 - 33860*t2 + 94715*Power(t2,2) - 
                     122097*Power(t2,3) + 75708*Power(t2,4) - 
                     20509*Power(t2,5) + 1840*Power(t2,6))) + 
               s2*(Power(t1,8)*(20 - 26*t2 + 6*Power(t2,2)) + 
                  Power(t1,7)*
                   (94 - 304*t2 + 251*Power(t2,2) - 17*Power(t2,3)) - 
                  Power(t1,6)*
                   (482 - 722*t2 + 13*Power(t2,2) + 314*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  Power(t1,5)*
                   (-1122 + 5596*t2 - 7646*Power(t2,2) + 
                     3423*Power(t2,3) - 138*Power(t2,4) + 
                     19*Power(t2,5)) + 
                  Power(t1,4)*
                   (1245 - 659*t2 - 7213*Power(t2,2) + 
                     10494*Power(t2,3) - 3987*Power(t2,4) + 
                     30*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (623 - 6579*t2 + 14318*Power(t2,2) - 
                     10628*Power(t2,3) + 1663*Power(t2,4) + 
                     532*Power(t2,5)) + 
                  Power(t1,3)*
                   (4706 - 22484*t2 + 37486*Power(t2,2) - 
                     25127*Power(t2,3) + 5695*Power(t2,4) - 
                     486*Power(t2,5) + 238*Power(t2,6)) + 
                  Power(t1,2)*
                   (3126 - 25430*t2 + 71629*Power(t2,2) - 
                     91626*Power(t2,3) + 54861*Power(t2,4) - 
                     13611*Power(t2,5) + 1048*Power(t2,6)) + 
                  t1*(566 - 15206*t2 + 60971*Power(t2,2) - 
                     101578*Power(t2,3) + 81447*Power(t2,4) - 
                     29139*Power(t2,5) + 2766*Power(t2,6) + 
                     173*Power(t2,7)))) + 
            Power(s1,3)*(2*Power(t1,9)*(2 - 3*t2 + Power(t2,2)) - 
               2*Power(s2,9)*(5 - 11*t2 + 6*Power(t2,2)) - 
               Power(t1,8)*(2 + 30*t2 - 40*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(t1,7)*(-119 + 354*t2 - 269*Power(t2,2) + 
                  26*Power(t2,3) + 4*Power(t2,4)) + 
               Power(-1 + t2,4)*
                (342 - 265*t2 - 848*Power(t2,2) + 768*Power(t2,3) + 
                  51*Power(t2,4)) + 
               Power(t1,6)*(2 + 656*t2 - 1631*Power(t2,2) + 
                  1147*Power(t2,3) - 155*Power(t2,4) - 13*Power(t2,5)) + 
               t1*Power(-1 + t2,3)*
                (-1302 + 4751*t2 - 3386*Power(t2,2) - 
                  1844*Power(t2,3) + 1637*Power(t2,4) + 16*Power(t2,5)) \
+ Power(t1,2)*Power(-1 + t2,2)*
                (808 - 6792*t2 + 12424*Power(t2,2) - 6181*Power(t2,3) - 
                  1231*Power(t2,4) + 785*Power(t2,5)) + 
               Power(t1,5)*(474 - 1813*t2 + 1884*Power(t2,2) - 
                  114*Power(t2,3) - 400*Power(t2,4) - 51*Power(t2,5) + 
                  16*Power(t2,6)) + 
               Power(t1,4)*(726 - 5411*t2 + 13027*Power(t2,2) - 
                  13560*Power(t2,3) + 6068*Power(t2,4) - 
                  928*Power(t2,5) + 79*Power(t2,6)) + 
               Power(t1,3)*(295 - 5980*t2 + 21328*Power(t2,2) - 
                  30762*Power(t2,3) + 19826*Power(t2,4) - 
                  4426*Power(t2,5) - 393*Power(t2,6) + 112*Power(t2,7)) \
+ Power(s2,8)*(-16 + 14*t2 + 17*Power(t2,2) - 14*Power(t2,3) + 
                  14*t1*(6 - 13*t2 + 7*Power(t2,2))) + 
               Power(s2,7)*(242 - 814*t2 + 908*Power(t2,2) - 
                  364*Power(t2,3) + 32*Power(t2,4) - 
                  8*Power(t1,2)*(39 - 83*t2 + 44*Power(t2,2)) + 
                  t1*(108 - 46*t2 - 185*Power(t2,2) + 115*Power(t2,3))) \
+ Power(s2,6)*(-63 + 961*t2 - 2412*Power(t2,2) + 2167*Power(t2,3) - 
                  689*Power(t2,4) + 42*Power(t2,5) + 
                  56*Power(t1,3)*(12 - 25*t2 + 13*Power(t2,2)) - 
                  Power(t1,2)*
                   (324 + 18*t2 - 763*Power(t2,2) + 393*Power(t2,3)) + 
                  t1*(-1598 + 5375*t2 - 5943*Power(t2,2) + 
                     2359*Power(t2,3) - 221*Power(t2,4))) + 
               Power(s2,5)*(-1326 + 4875*t2 - 6102*Power(t2,2) + 
                  2601*Power(t2,3) + 168*Power(t2,4) - 
                  240*Power(t2,5) + 28*Power(t2,6) - 
                  28*Power(t1,4)*(33 - 67*t2 + 34*Power(t2,2)) + 
                  Power(t1,3)*
                   (562 + 320*t2 - 1675*Power(t2,2) + 737*Power(t2,3)) \
+ Power(t1,2)*(4461 - 14994*t2 + 16397*Power(t2,2) - 
                     6430*Power(t2,3) + 650*Power(t2,4)) - 
                  2*t1*(-270 + 3135*t2 - 7444*Power(t2,2) + 
                     6475*Power(t2,3) - 2023*Power(t2,4) + 
                     145*Power(t2,5))) + 
               Power(s2,4)*(2150 - 13562*t2 + 31652*Power(t2,2) - 
                  35102*Power(t2,3) + 19040*Power(t2,4) - 
                  4528*Power(t2,5) + 351*Power(t2,6) + 
                  28*Power(t1,5)*(30 - 59*t2 + 29*Power(t2,2)) - 
                  5*Power(t1,4)*
                   (122 + 140*t2 - 443*Power(t2,2) + 167*Power(t2,3)) + 
                  Power(t1,3)*
                   (-6823 + 22860*t2 - 24587*Power(t2,2) + 
                     9436*Power(t2,3) - 1026*Power(t2,4)) + 
                  2*Power(t1,2)*
                   (-727 + 7889*t2 - 18213*Power(t2,2) + 
                     15357*Power(t2,3) - 4606*Power(t2,4) + 
                     345*Power(t2,5)) + 
                  t1*(6221 - 23081*t2 + 29152*Power(t2,2) - 
                     12935*Power(t2,3) + 79*Power(t2,4) + 
                     600*Power(t2,5) - 56*Power(t2,6))) - 
               Power(s2,3)*(1855 - 18302*t2 + 57066*Power(t2,2) - 
                  82178*Power(t2,3) + 59591*Power(t2,4) - 
                  20595*Power(t2,5) + 2636*Power(t2,6) - 
                  73*Power(t2,7) + 
                  56*Power(t1,6)*(9 - 17*t2 + 8*Power(t2,2)) - 
                  Power(t1,5)*
                   (416 + 786*t2 - 1847*Power(t2,2) + 589*Power(t2,3)) \
+ Power(t1,4)*(-6172 + 20510*t2 - 21458*Power(t2,2) + 
                     7884*Power(t2,3) - 904*Power(t2,4)) + 
                  Power(t1,3)*
                   (-1750 + 19980*t2 - 45701*Power(t2,2) + 
                     37259*Power(t2,3) - 10405*Power(t2,4) + 
                     737*Power(t2,5)) + 
                  Power(t1,2)*
                   (11234 - 42239*t2 + 53918*Power(t2,2) - 
                     24826*Power(t2,3) + 1779*Power(t2,4) + 
                     67*Power(t2,5) + 27*Power(t2,6)) + 
                  t1*(8621 - 52451*t2 + 119564*Power(t2,2) - 
                     130312*Power(t2,3) + 69900*Power(t2,4) - 
                     16822*Power(t2,5) + 1504*Power(t2,6))) + 
               Power(s2,2)*(8*Power(t1,7)*
                   (24 - 43*t2 + 19*Power(t2,2)) - 
                  Power(t1,6)*
                   (168 + 514*t2 - 965*Power(t2,2) + 255*Power(t2,3)) + 
                  Power(t1,5)*
                   (-3300 + 10779*t2 - 10745*Power(t2,2) + 
                     3595*Power(t2,3) - 413*Power(t2,4)) + 
                  Power(t1,4)*
                   (-981 + 13557*t2 - 31247*Power(t2,2) + 
                     24488*Power(t2,3) - 6068*Power(t2,4) + 
                     341*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (2252 - 14672*t2 + 26467*Power(t2,2) - 
                     16604*Power(t2,3) + 1879*Power(t2,4) + 
                     491*Power(t2,5)) + 
                  Power(t1,3)*
                   (9583 - 36548*t2 + 46672*Power(t2,2) - 
                     21365*Power(t2,3) + 2249*Power(t2,4) - 
                     757*Power(t2,5) + 126*Power(t2,6)) + 
                  Power(t1,2)*
                   (11539 - 69840*t2 + 157878*Power(t2,2) - 
                     169883*Power(t2,3) + 89504*Power(t2,4) - 
                     21262*Power(t2,5) + 2070*Power(t2,6)) + 
                  t1*(4913 - 46864*t2 + 143770*Power(t2,2) - 
                     203746*Power(t2,3) + 144192*Power(t2,4) - 
                     47368*Power(t2,5) + 5117*Power(t2,6) - 
                     14*Power(t2,7))) - 
               s2*(14*Power(t1,8)*(3 - 5*t2 + 2*Power(t2,2)) - 
                  Power(t1,7)*
                   (34 + 188*t2 - 293*Power(t2,2) + 63*Power(t2,3)) + 
                  Power(t1,6)*
                   (-965 + 3050*t2 - 2781*Power(t2,2) + 
                     738*Power(t2,3) - 70*Power(t2,4)) - 
                  Power(-1 + t2,3)*
                   (1734 - 6104*t2 + 4613*Power(t2,2) + 
                     1632*Power(t2,3) - 1762*Power(t2,4) + 
                     15*Power(t2,5)) + 
                  Power(t1,5)*
                   (-206 + 4702*t2 - 11127*Power(t2,2) + 
                     8307*Power(t2,3) - 1673*Power(t2,4) + 
                     33*Power(t2,5)) + 
                  t1*Power(-1 + t2,2)*
                   (3184 - 21862*t2 + 39318*Power(t2,2) - 
                     22876*Power(t2,3) + 521*Power(t2,4) + 
                     1341*Power(t2,5)) + 
                  Power(t1,4)*
                   (3718 - 14328*t2 + 17688*Power(t2,2) - 
                     6987*Power(t2,3) + 317*Power(t2,4) - 
                     515*Power(t2,5) + 87*Power(t2,6)) + 
                  Power(t1,3)*
                   (5794 - 36362*t2 + 82993*Power(t2,2) - 
                     88233*Power(t2,3) + 44712*Power(t2,4) - 
                     9896*Power(t2,5) + 996*Power(t2,6)) + 
                  Power(t1,2)*
                   (3365 - 34608*t2 + 108161*Power(t2,2) - 
                     152411*Power(t2,3) + 104361*Power(t2,4) - 
                     31067*Power(t2,5) + 2013*Power(t2,6) + 
                     186*Power(t2,7))))) + 
         Power(s,4)*(-2406 - 6717*t1 + 2661*Power(t1,2) + 
            5852*Power(t1,3) + 1476*Power(t1,4) + 1593*Power(t1,5) - 
            1321*Power(t1,6) + 170*Power(t1,7) + 9413*t2 + 31549*t1*t2 - 
            12091*Power(t1,2)*t2 - 32597*Power(t1,3)*t2 - 
            10553*Power(t1,4)*t2 + 348*Power(t1,5)*t2 + 
            2425*Power(t1,6)*t2 - 320*Power(t1,7)*t2 - 
            19656*Power(t2,2) - 65027*t1*Power(t2,2) + 
            31200*Power(t1,2)*Power(t2,2) + 
            72241*Power(t1,3)*Power(t2,2) + 
            17146*Power(t1,4)*Power(t2,2) - 
            5903*Power(t1,5)*Power(t2,2) - 
            1165*Power(t1,6)*Power(t2,2) + 180*Power(t1,7)*Power(t2,2) + 
            37860*Power(t2,3) + 80206*t1*Power(t2,3) - 
            46229*Power(t1,2)*Power(t2,3) - 
            76084*Power(t1,3)*Power(t2,3) - 
            9612*Power(t1,4)*Power(t2,3) + 
            5207*Power(t1,5)*Power(t2,3) - 2*Power(t1,6)*Power(t2,3) - 
            28*Power(t1,7)*Power(t2,3) - 59165*Power(t2,4) - 
            69288*t1*Power(t2,4) + 33701*Power(t1,2)*Power(t2,4) + 
            38343*Power(t1,3)*Power(t2,4) + 
            1285*Power(t1,4)*Power(t2,4) - 
            1283*Power(t1,5)*Power(t2,4) + 61*Power(t1,6)*Power(t2,4) + 
            55922*Power(t2,5) + 42314*t1*Power(t2,5) - 
            9623*Power(t1,2)*Power(t2,5) - 
            8102*Power(t1,3)*Power(t2,5) + 398*Power(t1,4)*Power(t2,5) + 
            24*Power(t1,5)*Power(t2,5) - 26660*Power(t2,6) - 
            14356*t1*Power(t2,6) + 131*Power(t1,2)*Power(t2,6) + 
            281*Power(t1,3)*Power(t2,6) - 104*Power(t1,4)*Power(t2,6) + 
            4212*Power(t2,7) + 1147*t1*Power(t2,7) + 
            249*Power(t1,2)*Power(t2,7) + 32*Power(t1,3)*Power(t2,7) + 
            473*Power(t2,8) + 170*t1*Power(t2,8) + 
            15*Power(t1,2)*Power(t2,8) + 7*Power(t2,9) + 
            Power(s1,9)*(-4 - 26*Power(s2,2) - 48*t1 - 34*Power(t1,2) + 
               s2*(44 + 60*t1 - 40*t2) + 44*t1*t2 + 4*Power(t2,2)) - 
            2*Power(s2,7)*(10 - 25*t2 + 15*Power(t2,2) + Power(t2,3)) - 
            Power(s1,8)*(22 + 117*Power(s2,3) - 135*Power(t1,3) - 
               71*t2 + 22*Power(t2,2) + 27*Power(t2,3) + 
               Power(t1,2)*(-320 + 181*t2) + 
               Power(s2,2)*(-422 - 384*t1 + 319*t2) + 
               s2*(196 + 402*Power(t1,2) + t1*(744 - 502*t2) - 
                  128*t2 - 39*Power(t2,2)) + 
               t1*(-165 + 68*t2 + 68*Power(t2,2))) + 
            Power(s2,6)*(-126 + 320*t2 - 310*Power(t2,2) + 
               73*Power(t2,3) + 41*Power(t2,4) + 
               t1*(150 - 350*t2 + 240*Power(t2,2) - 26*Power(t2,3))) + 
            Power(s2,5)*(820 - 3668*t2 + 4653*Power(t2,2) - 
               2137*Power(t2,3) + 538*Power(t2,4) - 192*Power(t2,5) + 
               6*Power(t1,2)*
                (-80 + 150*t2 - 85*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(716 - 1250*t2 + 1275*Power(t2,2) - 868*Power(t2,3) + 
                  139*Power(t2,4))) + 
            Power(s2,4)*(2614 - 9922*t2 + 10446*Power(t2,2) - 
               2222*Power(t2,3) - 835*Power(t2,4) - 325*Power(t2,5) + 
               280*Power(t2,6) + 
               10*Power(t1,3)*
                (99 - 142*t2 + 42*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(-2190 + 1670*t2 + 90*Power(t2,2) + 
                  955*Power(t2,3) - 555*Power(t2,4)) + 
               5*t1*(-861 + 4336*t2 - 5748*Power(t2,2) + 
                  2413*Power(t2,3) - 186*Power(t2,4) + 32*Power(t2,5))) \
- Power(s2,3)*(6491 - 34167*t2 + 72658*Power(t2,2) - 
               75954*Power(t2,3) + 40788*Power(t2,4) - 
               11126*Power(t2,5) + 1146*Power(t2,6) + 130*Power(t2,7) + 
               10*Power(t1,4)*
                (146 - 193*t2 + 33*Power(t2,2) + 21*Power(t2,3)) - 
               5*Power(t1,3)*
                (971 - 627*t2 - 731*Power(t2,2) + 323*Power(t2,3) + 
                  72*Power(t2,4)) - 
               Power(t1,2)*(6542 - 44045*t2 + 65360*Power(t2,2) - 
                  29455*Power(t2,3) + 1090*Power(t2,4) + 
                  648*Power(t2,5)) + 
               t1*(8827 - 39008*t2 + 48869*Power(t2,2) - 
                  20168*Power(t2,3) + 2590*Power(t2,4) - 
                  1676*Power(t2,5) + 710*Power(t2,6))) + 
            Power(s2,2)*(-1003 - 13923*t2 + 71558*Power(t2,2) - 
               121694*Power(t2,3) + 90076*Power(t2,4) - 
               25554*Power(t2,5) - 382*Power(t2,6) + 921*Power(t2,7) + 
               15*Power(t2,8) + 
               6*Power(t1,5)*
                (235 - 345*t2 + 100*Power(t2,2) + 17*Power(t2,3)) + 
               5*Power(t1,4)*
                (-1339 + 1417*t2 + 465*Power(t2,2) - 598*Power(t2,3) + 
                  49*Power(t2,4)) - 
               Power(t1,3)*(1856 - 38070*t2 + 69015*Power(t2,2) - 
                  36470*Power(t2,3) + 2825*Power(t2,4) + 
                  984*Power(t2,5)) + 
               Power(t1,2)*(10000 - 52993*t2 + 72661*Power(t2,2) - 
                  32467*Power(t2,3) + 3055*Power(t2,4) - 
                  376*Power(t2,5) + 336*Power(t2,6)) + 
               t1*(22570 - 110645*t2 + 220339*Power(t2,2) - 
                  213642*Power(t2,3) + 101214*Power(t2,4) - 
                  20890*Power(t2,5) + 582*Power(t2,6) + 370*Power(t2,7))\
) + s2*(6521 - 16829*t2 + 4882*Power(t2,2) + 14180*Power(t2,3) + 
               4638*Power(t2,4) - 29938*Power(t2,5) + 
               19771*Power(t2,6) - 3037*Power(t2,7) - 186*Power(t2,8) + 
               2*Power(t1,6)*
                (-380 + 640*t2 - 285*Power(t2,2) + 18*Power(t2,3)) + 
               Power(t1,5)*(4761 - 7115*t2 + 1440*Power(t2,2) + 
                  1217*Power(t2,3) - 291*Power(t2,4)) + 
               Power(t1,4)*(-2794 - 12385*t2 + 33645*Power(t2,2) - 
                  22150*Power(t2,3) + 3410*Power(t2,4) + 
                  344*Power(t2,5)) + 
               Power(t1,3)*(-5263 + 34460*t2 - 51384*Power(t2,2) + 
                  24133*Power(t2,3) - 915*Power(t2,4) - 
                  1373*Power(t2,5) + 198*Power(t2,6)) - 
               Power(t1,2)*(21643 - 108417*t2 + 220552*Power(t2,2) - 
                  217097*Power(t2,3) + 102724*Power(t2,4) - 
                  19903*Power(t2,5) + 144*Power(t2,6) + 252*Power(t2,7)\
) + t1*(-2060 + 21116*t2 - 76213*Power(t2,2) + 119418*Power(t2,3) - 
                  82667*Power(t2,4) + 19344*Power(t2,5) + 
                  2079*Power(t2,6) - 1010*Power(t2,7) - 35*Power(t2,8))) \
+ Power(s1,7)*(318 - 107*Power(s2,4) - 62*Power(t1,4) + 
               Power(s2,3)*(1082 + 439*t1 - 1178*t2) - 872*t2 + 
               690*Power(t2,2) - 200*Power(t2,3) + 64*Power(t2,4) + 
               3*Power(t1,3)*(-331 + 367*t2) + 
               Power(t1,2)*(-1114 + 1597*t2 - 740*Power(t2,2)) + 
               t1*(293 - 1130*t2 + 661*Power(t2,2) + 87*Power(t2,3)) + 
               Power(s2,2)*(-948 - 608*Power(t1,2) + 519*t2 + 
                  229*Power(t2,2) + t1*(-2911 + 3146*t2)) + 
               s2*(-205 + 338*Power(t1,3) + 
                  Power(t1,2)*(2826 - 3073*t2) + 953*t2 - 
                  483*Power(t2,2) - 176*Power(t2,3) + 
                  t1*(1985 - 1982*t2 + 454*Power(t2,2)))) + 
            Power(s1,6)*(-313 + 128*Power(s2,5) - 103*Power(t1,5) + 
               Power(s2,4)*(945 - 646*t1 - 2114*t2) - 214*t2 + 
               2609*Power(t2,2) - 3156*Power(t2,3) + 1229*Power(t2,4) - 
               155*Power(t2,5) - 6*Power(t1,4)*(-294 + 557*t2) + 
               Power(t1,3)*(1608 - 2217*t2 - 116*Power(t2,2)) + 
               Power(t1,2)*(-1851 + 9038*t2 - 9902*Power(t2,2) + 
                  3040*Power(t2,3)) + 
               t1*(-1756 + 5935*t2 - 5168*Power(t2,2) + 
                  1001*Power(t2,3) + 101*Power(t2,4)) + 
               Power(s2,3)*(-915 + 1324*Power(t1,2) + 14*t2 + 
                  1579*Power(t2,2) + t1*(-3990 + 8847*t2)) - 
               Power(s2,2)*(1880 + 1325*Power(t1,3) - 7451*t2 + 
                  4973*Power(t2,2) + 363*Power(t2,3) + 
                  7*Power(t1,2)*(-1014 + 2131*t2) + 
                  t1*(-3839 + 4171*t2 + 1620*Power(t2,2))) + 
               s2*(2140 + 622*Power(t1,4) - 7106*t2 + 
                  6615*Power(t2,2) - 2253*Power(t2,3) + 
                  491*Power(t2,4) + 3*Power(t1,3)*(-1939 + 3842*t2) + 
                  Power(t1,2)*(-4567 + 6413*t2 + 153*Power(t2,2)) + 
                  t1*(3617 - 15975*t2 + 14340*Power(t2,2) - 
                     2542*Power(t2,3)))) + 
            Power(s1,5)*(-1567 + 252*Power(s2,6) + 42*Power(t1,6) + 
               Power(s2,5)*(219 - 1438*t1 - 1766*t2) + 10315*t2 - 
               20347*Power(t2,2) + 14519*Power(t2,3) - 
               1740*Power(t2,4) - 1320*Power(t2,5) + 140*Power(t2,6) + 
               Power(t1,5)*(-1130 + 3281*t2) + 
               Power(t1,4)*(819 - 5173*t2 + 5548*Power(t2,2)) + 
               Power(t1,3)*(5515 - 24140*t2 + 23432*Power(t2,2) - 
                  4070*Power(t2,3)) + 
               Power(t1,2)*(3591 - 10837*t2 + 445*Power(t2,2) + 
                  11730*Power(t2,3) - 4863*Power(t2,4)) + 
               t1*(-1197 + 11843*t2 - 31568*Power(t2,2) + 
                  30061*Power(t2,3) - 9279*Power(t2,4) + 
                  132*Power(t2,5)) + 
               Power(s2,4)*(398 + 3313*Power(t1,2) - 2916*t2 + 
                  3566*Power(t2,2) + 8*t1*(-283 + 1298*t2)) - 
               Power(s2,3)*(3322 + 3873*Power(t1,3) - 15374*t2 + 
                  11555*Power(t2,2) + 1556*Power(t2,3) + 
                  Power(t1,2)*(-7158 + 24490*t2) + 
                  t1*(212 - 8526*t2 + 12183*Power(t2,2))) + 
               Power(s2,2)*(3144 + 2341*Power(t1,4) - 9956*t2 + 
                  2835*Power(t2,2) + 3249*Power(t2,3) + 
                  966*Power(t2,4) + Power(t1,3)*(-9530 + 28173*t2) + 
                  Power(t1,2)*(388 - 14757*t2 + 20193*Power(t2,2)) + 
                  t1*(11556 - 55317*t2 + 51461*Power(t2,2) - 
                     4993*Power(t2,3))) - 
               s2*(-138 + 637*Power(t1,5) + 9108*t2 - 
                  28899*Power(t2,2) + 28926*Power(t2,3) - 
                  10069*Power(t2,4) + 1064*Power(t2,5) + 
                  3*Power(t1,4)*(-1849 + 5194*t2) + 
                  Power(t1,3)*(1393 - 14320*t2 + 17124*Power(t2,2)) + 
                  Power(t1,2)*
                   (13952 - 64657*t2 + 63803*Power(t2,2) - 
                     10713*Power(t2,3)) + 
                  t1*(6204 - 19714*t2 + 2970*Power(t2,2) + 
                     14868*Power(t2,3) - 4024*Power(t2,4)))) + 
            Power(s1,4)*(808 + 125*Power(s2,7) + 40*Power(t1,7) - 
               860*t2 - 17005*Power(t2,2) + 47300*Power(t2,3) - 
               43936*Power(t2,4) + 13810*Power(t2,5) - 83*Power(t2,6) - 
               34*Power(t2,7) - Power(s2,6)*(136 + 780*t1 + 565*t2) - 
               Power(t1,6)*(217 + 995*t2) + 
               Power(t1,5)*(-1539 + 7255*t2 - 5415*Power(t2,2)) - 
               Power(t1,4)*(2030 - 13557*t2 + 8215*Power(t2,2) + 
                  2730*Power(t2,3)) + 
               Power(t1,3)*(3414 - 25573*t2 + 52981*Power(t2,2) - 
                  39052*Power(t2,3) + 6655*Power(t2,4)) + 
               Power(t1,2)*(5844 - 51747*t2 + 107624*Power(t2,2) - 
                  74911*Power(t2,3) + 9631*Power(t2,4) + 
                  2859*Power(t2,5)) - 
               t1*(285 + 14786*t2 - 31336*Power(t2,2) + 
                  2095*Power(t2,3) + 26154*Power(t2,4) - 
                  12089*Power(t2,5) + 254*Power(t2,6)) + 
               Power(s2,5)*(581 + 1974*Power(t1,2) - 3851*t2 + 
                  2955*Power(t2,2) + t1*(5 + 4604*t2)) - 
               Power(s2,4)*(-385 + 2556*Power(t1,3) - 6345*t2 + 
                  3552*Power(t2,2) + 3495*Power(t2,3) + 
                  10*Power(t1,2)*(-142 + 1415*t2) + 
                  t1*(3494 - 21475*t2 + 15884*Power(t2,2))) + 
               Power(s2,3)*(-4182 + 1709*Power(t1,4) + 19067*t2 - 
                  35156*Power(t2,2) + 20777*Power(t2,3) + 
                  1260*Power(t2,4) + Power(t1,3)*(-2713 + 21471*t2) + 
                  Power(t1,2)*(9804 - 52103*t2 + 37626*Power(t2,2)) + 
                  t1*(4356 - 43590*t2 + 34771*Power(t2,2) + 
                     4679*Power(t2,3))) - 
               Power(s2,2)*(-4041 + 456*Power(t1,5) + 44184*t2 - 
                  97424*Power(t2,2) + 70377*Power(t2,3) - 
                  14368*Power(t2,4) + 2154*Power(t2,5) + 
                  Power(t1,4)*(-1609 + 17078*t2) + 
                  Power(t1,3)*(12989 - 62440*t2 + 44835*Power(t2,2)) + 
                  Power(t1,2)*
                   (12561 - 82035*t2 + 65135*Power(t2,2) + 
                     3252*Power(t2,3)) + 
                  t1*(-10393 + 58669*t2 - 119865*Power(t2,2) + 
                     86022*Power(t2,3) - 9421*Power(t2,4))) + 
               s2*(-2312 - 56*Power(t1,6) + 30266*t2 - 
                  61742*Power(t2,2) + 29731*Power(t2,3) + 
                  12849*Power(t2,4) - 9423*Power(t2,5) + 
                  780*Power(t2,6) + Power(t1,5)*(32 + 6713*t2) + 
                  Power(t1,4)*(7637 - 35216*t2 + 25553*Power(t2,2)) + 
                  Power(t1,3)*
                   (9850 - 58347*t2 + 42131*Power(t2,2) + 
                     4798*Power(t2,3)) + 
                  Power(t1,2)*
                   (-9491 + 65316*t2 - 138566*Power(t2,2) + 
                     105014*Power(t2,3) - 17452*Power(t2,4)) - 
                  t1*(10383 - 95872*t2 + 203601*Power(t2,2) - 
                     144582*Power(t2,3) + 23732*Power(t2,4) + 
                     1156*Power(t2,5)))) + 
            Power(s1,3)*(1262 + 17*Power(s2,8) - 20*Power(t1,8) - 
               21313*t2 + 74582*Power(t2,2) - 96504*Power(t2,3) + 
               37646*Power(t2,4) + 15116*Power(t2,5) - 
               11150*Power(t2,6) + 361*Power(t2,7) + 
               Power(s2,7)*(-125 - 105*t1 + 18*t2) + 
               Power(t1,7)*(252 + 19*t2) + 
               Power(t1,6)*(248 - 1731*t2 + 1240*Power(t2,2)) + 
               Power(t1,5)*(-2007 + 1641*t2 - 3754*Power(t2,2) + 
                  3183*Power(t2,3)) + 
               Power(t1,4)*(-2989 + 32615*t2 - 49973*Power(t2,2) + 
                  22313*Power(t2,3) - 695*Power(t2,4)) + 
               Power(t1,3)*(-1829 + 28355*t2 - 46720*Power(t2,2) + 
                  9178*Power(t2,3) + 14653*Power(t2,4) - 
                  3350*Power(t2,5)) + 
               Power(t1,2)*(6077 - 31663*t2 + 95888*Power(t2,2) - 
                  146774*Power(t2,3) + 96879*Power(t2,4) - 
                  19104*Power(t2,5) - 517*Power(t2,6)) + 
               t1*(5157 - 37779*t2 + 127520*Power(t2,2) - 
                  192770*Power(t2,3) + 120923*Power(t2,4) - 
                  18382*Power(t2,5) - 4520*Power(t2,6) + 56*Power(t2,7)\
) + Power(s2,6)*(34 + 243*Power(t1,2) - 1237*t2 + 781*Power(t2,2) + 
                  t1*(741 + 200*t2)) - 
               Power(s2,5)*(-2894 + 215*Power(t1,3) + 5431*t2 - 
                  5371*Power(t2,2) + 2788*Power(t2,3) + 
                  2*Power(t1,2)*(1092 + 617*t2) + 
                  2*t1*(629 - 4955*t2 + 3010*Power(t2,2))) + 
               Power(s2,4)*(-6958 - 85*Power(t1,4) + 32041*t2 - 
                  41876*Power(t2,2) + 15911*Power(t2,3) + 
                  1610*Power(t2,4) + Power(t1,3)*(4078 + 2595*t2) + 
                  Power(t1,2)*(5548 - 30739*t2 + 18625*Power(t2,2)) + 
                  t1*(-9523 + 15487*t2 - 17337*Power(t2,2) + 
                     10575*Power(t2,3))) + 
               Power(s2,3)*(-212 + 353*Power(t1,5) - 27624*t2 + 
                  59109*Power(t2,2) - 29137*Power(t2,3) - 
                  446*Power(t2,4) - 1940*Power(t2,5) - 
                  3*Power(t1,4)*(1639 + 890*t2) + 
                  Power(t1,3)*(-9684 + 46858*t2 - 28805*Power(t2,2)) + 
                  Power(t1,2)*
                   (14013 - 19872*t2 + 30292*Power(t2,2) - 
                     21355*Power(t2,3)) + 
                  t1*(26564 - 139040*t2 + 193987*Power(t2,2) - 
                     89472*Power(t2,3) + 4365*Power(t2,4))) + 
               Power(s2,2)*(6689 - 315*Power(t1,6) - 13639*t2 + 
                  45772*Power(t2,2) - 98901*Power(t2,3) + 
                  75195*Power(t2,4) - 15549*Power(t2,5) + 
                  1310*Power(t2,6) + Power(t1,5)*(3633 + 1402*t2) + 
                  Power(t1,4)*(7834 - 36685*t2 + 23150*Power(t2,2)) + 
                  Power(t1,3)*
                   (-13040 + 16648*t2 - 33811*Power(t2,2) + 
                     25320*Power(t2,3)) + 
                  Power(t1,2)*
                   (-36155 + 218334*t2 - 315865*Power(t2,2) + 
                     152796*Power(t2,3) - 12830*Power(t2,4)) + 
                  t1*(-6275 + 97434*t2 - 182967*Power(t2,2) + 
                     79273*Power(t2,3) + 16276*Power(t2,4) - 
                     2981*Power(t2,5))) + 
               s2*(-2381 + 127*Power(t1,7) + 32699*t2 - 
                  142773*Power(t2,2) + 238479*Power(t2,3) - 
                  163350*Power(t2,4) + 36303*Power(t2,5) + 
                  958*Power(t2,6) - 140*Power(t2,7) - 
                  2*Power(t1,6)*(739 + 165*t2) + 
                  Power(t1,5)*(-2722 + 13624*t2 - 8971*Power(t2,2)) + 
                  Power(t1,4)*
                   (7663 - 8473*t2 + 19239*Power(t2,2) - 
                     14935*Power(t2,3)) + 
                  Power(t1,3)*
                   (19538 - 143950*t2 + 213727*Power(t2,2) - 
                     101548*Power(t2,3) + 7550*Power(t2,4)) + 
                  Power(t1,2)*
                   (8766 - 100017*t2 + 172909*Power(t2,2) - 
                     60206*Power(t2,3) - 30500*Power(t2,4) + 
                     8251*Power(t2,5)) + 
                  t1*(-15115 + 56444*t2 - 159560*Power(t2,2) + 
                     259108*Power(t2,3) - 178082*Power(t2,4) + 
                     36068*Power(t2,5) - 526*Power(t2,6)))) + 
            Power(s1,2)*(1649 + 2*Power(t1,9) - 5228*t2 + 
               31915*Power(t2,2) - 108469*Power(t2,3) + 
               158820*Power(t2,4) - 101598*Power(t2,5) + 
               20758*Power(t2,6) + 2202*Power(t2,7) - 49*Power(t2,8) + 
               Power(t1,8)*(-23 + 8*t2) + 
               Power(s2,8)*(-26 + 2*t1 + 27*t2) + 
               Power(t1,7)*(-220 + 249*t2 - 51*Power(t2,2)) + 
               Power(t1,6)*(1356 - 1026*t2 + 385*Power(t2,2) - 
                  301*Power(t2,3)) + 
               Power(t1,5)*(2007 - 13121*t2 + 14243*Power(t2,2) - 
                  3497*Power(t2,3) - 618*Power(t2,4)) + 
               Power(t1,4)*(1437 - 834*t2 - 8371*Power(t2,2) + 
                  16614*Power(t2,3) - 8445*Power(t2,4) + 
                  444*Power(t2,5)) + 
               Power(t1,3)*(-5190 + 56196*t2 - 132939*Power(t2,2) + 
                  126396*Power(t2,3) - 47736*Power(t2,4) + 
                  3113*Power(t2,5) + 491*Power(t2,6)) + 
               Power(t1,2)*(-655 + 37140*t2 - 120066*Power(t2,2) + 
                  127336*Power(t2,3) - 31237*Power(t2,4) - 
                  19343*Power(t2,5) + 6574*Power(t2,6) + 25*Power(t2,7)\
) + t1*(11535 - 33366*t2 + 42639*Power(t2,2) - 74262*Power(t2,3) + 
                  119421*Power(t2,4) - 86457*Power(t2,5) + 
                  19922*Power(t2,6) + 433*Power(t2,7)) - 
               Power(s2,7)*(47 + 16*Power(t1,2) - 6*t2 + 
                  13*Power(t2,2) + t1*(-205 + 189*t2)) + 
               Power(s2,6)*(1184 + 56*Power(t1,3) - 2744*t2 + 
                  2309*Power(t2,2) - 677*Power(t2,3) + 
                  Power(t1,2)*(-747 + 615*t2) + 
                  t1*(81 + 457*t2 - 200*Power(t2,2))) + 
               Power(s2,5)*(-2604 - 112*Power(t1,4) + 
                  Power(t1,3)*(1593 - 1193*t2) + 11161*t2 - 
                  10893*Power(t2,2) + 1927*Power(t2,3) + 
                  963*Power(t2,4) + 
                  3*Power(t1,2)*(115 - 798*t2 + 397*Power(t2,2)) + 
                  t1*(-6259 + 13672*t2 - 11780*Power(t2,2) + 
                     3636*Power(t2,3))) + 
               Power(s2,4)*(-6071 + 140*Power(t1,5) + 16990*t2 - 
                  19856*Power(t2,2) + 14729*Power(t2,3) - 
                  3975*Power(t2,4) - 1030*Power(t2,5) + 
                  5*Power(t1,4)*(-423 + 293*t2) - 
                  5*Power(t1,3)*(294 - 1027*t2 + 511*Power(t2,2)) + 
                  Power(t1,2)*
                   (15437 - 30984*t2 + 27385*Power(t2,2) - 
                     9220*Power(t2,3)) + 
                  t1*(16010 - 68395*t2 + 71704*Power(t2,2) - 
                     21285*Power(t2,3) - 1225*Power(t2,4))) + 
               Power(s2,3)*(14262 - 112*Power(t1,6) + 
                  Power(t1,5)*(1751 - 1127*t2) - 70794*t2 + 
                  144745*Power(t2,2) - 141441*Power(t2,3) + 
                  60171*Power(t2,4) - 8030*Power(t2,5) + 
                  850*Power(t2,6) + 
                  5*Power(t1,4)*(491 - 1190*t2 + 553*Power(t2,2)) + 
                  Power(t1,3)*
                   (-21953 + 39386*t2 - 34950*Power(t2,2) + 
                     12915*Power(t2,3)) + 
                  Power(t1,2)*
                   (-33868 + 149170*t2 - 159062*Power(t2,2) + 
                     50495*Power(t2,3) + 500*Power(t2,4)) + 
                  t1*(11589 - 43700*t2 + 70827*Power(t2,2) - 
                     70955*Power(t2,3) + 30135*Power(t2,4) - 
                     1105*Power(t2,5))) + 
               Power(s2,2)*(-745 + 56*Power(t1,7) + 53678*t2 - 
                  194955*Power(t2,2) + 245573*Power(t2,3) - 
                  113117*Power(t2,4) + 7385*Power(t2,5) + 
                  2077*Power(t2,6) - 150*Power(t2,7) + 
                  Power(t1,6)*(-865 + 509*t2) - 
                  3*Power(t1,5)*(741 - 1333*t2 + 534*Power(t2,2)) + 
                  Power(t1,4)*
                   (18067 - 27814*t2 + 23705*Power(t2,2) - 
                     9630*Power(t2,3)) + 
                  Power(t1,3)*
                   (32129 - 150920*t2 + 160827*Power(t2,2) - 
                     48340*Power(t2,3) - 1795*Power(t2,4)) + 
                  3*Power(t1,2)*
                   (-1144 + 12182*t2 - 31354*Power(t2,2) + 
                     39189*Power(t2,3) - 18920*Power(t2,4) + 
                     1681*Power(t2,5)) + 
                  t1*(-39080 + 221882*t2 - 462778*Power(t2,2) + 
                     444612*Power(t2,3) - 186284*Power(t2,4) + 
                     22739*Power(t2,5) - 285*Power(t2,6))) - 
               s2*(7197 + 16*Power(t1,8) + 6469*t2 - 
                  62182*Power(t2,2) + 37452*Power(t2,3) + 
                  76694*Power(t2,4) - 91027*Power(t2,5) + 
                  25965*Power(t2,6) - 703*Power(t2,7) + 
                  Power(t1,7)*(-227 + 115*t2) + 
                  Power(t1,6)*(-1079 + 1502*t2 - 465*Power(t2,2)) + 
                  Power(t1,5)*
                   (7832 - 9510*t2 + 7054*Power(t2,2) - 
                     3277*Power(t2,3)) + 
                  Power(t1,4)*
                   (13674 - 72105*t2 + 76819*Power(t2,2) - 
                     20700*Power(t2,3) - 2175*Power(t2,4)) + 
                  Power(t1,3)*
                   (3523 + 9002*t2 - 51462*Power(t2,2) + 
                     77955*Power(t2,3) - 39045*Power(t2,4) + 
                     3352*Power(t2,5)) + 
                  Power(t1,2)*
                   (-29212 + 204798*t2 - 448467*Power(t2,2) + 
                     428787*Power(t2,3) - 173879*Power(t2,4) + 
                     17861*Power(t2,5) + 1012*Power(t2,6)) - 
                  t1*(3785 - 92181*t2 + 299061*Power(t2,2) - 
                     341242*Power(t2,3) + 120773*Power(t2,4) + 
                     20498*Power(t2,5) - 10304*Power(t2,6) + 
                     90*Power(t2,7)))) - 
            s1*(-924 - 5233*t2 + 45826*Power(t2,2) - 
               101512*Power(t2,3) + 87217*Power(t2,4) - 
               12499*Power(t2,5) - 21833*Power(t2,6) + 
               8882*Power(t2,7) + 76*Power(t2,8) + 
               4*Power(s2,8)*(-2 + 3*t2) - 
               2*Power(t1,8)*(25 - 28*t2 + 6*Power(t2,2)) + 
               Power(t1,7)*(348 - 371*t2 + 49*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,6)*(955 - 3242*t2 + 2961*Power(t2,2) - 
                  833*Power(t2,3) + 46*Power(t2,4)) - 
               Power(t1,5)*(4875 - 10708*t2 + 7643*Power(t2,2) - 
                  2633*Power(t2,3) + 517*Power(t2,4) + 84*Power(t2,5)) + 
               Power(t1,4)*(-5519 + 36468*t2 - 62949*Power(t2,2) + 
                  40253*Power(t2,3) - 8240*Power(t2,4) - 
                  249*Power(t2,5) + 10*Power(t2,6)) + 
               2*Power(t1,3)*
                (-2217 + 5564*t2 - 6585*Power(t2,2) + 755*Power(t2,3) + 
                  5789*Power(t2,4) - 3758*Power(t2,5) + 
                  444*Power(t2,6) + 10*Power(t2,7)) + 
               Power(t1,2)*(3296 - 35112*t2 + 112744*Power(t2,2) - 
                  170712*Power(t2,3) + 132799*Power(t2,4) - 
                  47919*Power(t2,5) + 4435*Power(t2,6) + 582*Power(t2,7)\
) + t1*(2895 - 20538*t2 + 79636*Power(t2,2) - 155180*Power(t2,3) + 
                  143652*Power(t2,4) - 50312*Power(t2,5) - 
                  3601*Power(t2,6) + 3384*Power(t2,7) + 24*Power(t2,8)) \
+ Power(s2,7)*(-134 + 266*t2 - 144*Power(t2,2) + 9*Power(t2,3) + 
                  2*t1*(38 - 50*t2 + Power(t2,2))) + 
               Power(s2,6)*(432 - 1713*t2 + 1495*Power(t2,2) - 
                  139*Power(t2,3) - 112*Power(t2,4) + 
                  Power(t1,2)*(-308 + 344*t2 + 6*Power(t2,2)) + 
                  t1*(912 - 1512*t2 + 708*Power(t2,2) - 44*Power(t2,3))) \
+ Power(s2,5)*(3389 - 11248*t2 + 12361*Power(t2,2) - 4928*Power(t2,3) - 
                  28*Power(t2,4) + 294*Power(t2,5) + 
                  Power(t1,3)*(730 - 696*t2 - 48*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2948 + 4016*t2 - 1429*Power(t2,2) + 
                     40*Power(t2,3)) + 
                  2*t1*(-1545 + 6346*t2 - 6188*Power(t2,2) + 
                     1404*Power(t2,3) + 132*Power(t2,4))) + 
               Power(s2,4)*(-7016 + 33259*t2 - 54839*Power(t2,2) + 
                  40085*Power(t2,3) - 13610*Power(t2,4) + 
                  2115*Power(t2,5) - 210*Power(t2,6) + 
                  10*Power(t1,4)*(-113 + 98*t2 + 8*Power(t2,2)) + 
                  5*Power(t1,3)*
                   (1140 - 1327*t2 + 313*Power(t2,2) + 24*Power(t2,3)) \
- 5*Power(t1,2)*(-1540 + 6660*t2 - 6768*Power(t2,2) + 
                     1777*Power(t2,3) + 58*Power(t2,4)) - 
                  t1*(15191 - 51295*t2 + 60502*Power(t2,2) - 
                     30555*Power(t2,3) + 4895*Power(t2,4) + 
                     400*Power(t2,5))) + 
               Power(s2,3)*(-3942 + 2491*t2 + 18116*Power(t2,2) - 
                  22860*Power(t2,3) + 3055*Power(t2,4) + 
                  4019*Power(t2,5) - 935*Power(t2,6) + 35*Power(t2,7) - 
                  2*Power(t1,5)*(-584 + 506*t2 + 15*Power(t2,2)) - 
                  5*Power(t1,4)*
                   (1374 - 1462*t2 + 218*Power(t2,2) + 63*Power(t2,3)) \
+ 5*Power(t1,3)*(-1967 + 8824*t2 - 9035*Power(t2,2) + 
                     2406*Power(t2,3) + 72*Power(t2,4)) + 
                  Power(t1,2)*
                   (29344 - 94760*t2 + 111743*Power(t2,2) - 
                     61695*Power(t2,3) + 13490*Power(t2,4) + 
                     30*Power(t2,5)) + 
                  2*t1*(16227 - 75230*t2 + 119568*Power(t2,2) - 
                     82239*Power(t2,3) + 24520*Power(t2,4) - 
                     2474*Power(t2,5) + 65*Power(t2,6))) + 
               Power(s2,2)*(10676 - 45193*t2 + 113972*Power(t2,2) - 
                  190892*Power(t2,3) + 179859*Power(t2,4) - 
                  80818*Power(t2,5) + 12681*Power(t2,6) - 
                  180*Power(t2,7) + 
                  Power(t1,6)*(-776 + 720*t2 - 42*Power(t2,2)) + 
                  Power(t1,5)*
                   (5024 - 5186*t2 + 574*Power(t2,2) + 296*Power(t2,3)) \
- 5*Power(t1,4)*(-1545 + 6757*t2 - 6764*Power(t2,2) + 
                     1755*Power(t2,3) + 60*Power(t2,4)) - 
                  Power(t1,3)*
                   (31546 - 91335*t2 + 99067*Power(t2,2) - 
                     54070*Power(t2,3) + 12700*Power(t2,4) + 
                     120*Power(t2,5)) + 
                  Power(t1,2)*
                   (-48237 + 233731*t2 - 370949*Power(t2,2) + 
                     243994*Power(t2,3) - 63155*Power(t2,4) + 
                     3040*Power(t2,5) + 250*Power(t2,6)) + 
                  t1*(43 + 4451*t2 - 20027*Power(t2,2) - 
                     560*Power(t2,3) + 38693*Power(t2,4) - 
                     27177*Power(t2,5) + 4623*Power(t2,6))) + 
               s2*(-2021 + 24562*t2 - 128893*Power(t2,2) + 
                  284352*Power(t2,3) - 288561*Power(t2,4) + 
                  124595*Power(t2,5) - 10836*Power(t2,6) - 
                  3193*Power(t2,7) + 35*Power(t2,8) + 
                  Power(t1,7)*(298 - 304*t2 + 44*Power(t2,2)) - 
                  Power(t1,6)*
                   (2032 - 2112*t2 + 233*Power(t2,2) + 126*Power(t2,3)) \
+ Power(t1,5)*(-3887 + 15228*t2 - 14565*Power(t2,2) + 
                     3794*Power(t2,3) + 32*Power(t2,4)) + 
                  Power(t1,4)*
                   (18879 - 47330*t2 + 43108*Power(t2,2) - 
                     20635*Power(t2,3) + 4650*Power(t2,4) + 
                     280*Power(t2,5)) + 
                  Power(t1,3)*
                   (28318 - 152998*t2 + 249601*Power(t2,2) - 
                     159854*Power(t2,3) + 35965*Power(t2,4) + 
                     42*Power(t2,5) - 180*Power(t2,6)) + 
                  Power(t1,2)*
                   (9107 - 22738*t2 + 25621*Power(t2,2) + 
                     10375*Power(t2,3) - 46866*Power(t2,4) + 
                     28900*Power(t2,5) - 4378*Power(t2,6) - 
                     50*Power(t2,7)) - 
                  2*t1*(8889 - 50660*t2 + 133034*Power(t2,2) - 
                     194837*Power(t2,3) + 156949*Power(t2,4) - 
                     60914*Power(t2,5) + 7273*Power(t2,6) + 
                     375*Power(t2,7))))) + 
         s*(Power(s1,10)*Power(s2 - t1,2)*
             (Power(s2,2) + Power(t1,2) + s2*(-2*t1 + 15*(-1 + t2)) - 
               15*t1*(-1 + t2) + 14*Power(-1 + t2,2)) + 
            Power(s1,9)*(s2 - t1)*
             (4*Power(s2,4) + 4*Power(t1,4) + 
               Power(t1,3)*(104 - 101*t2) + 
               4*Power(-1 + t2,3)*(9 + t2) + 
               t1*Power(-1 + t2,2)*(-3 + 32*t2) + 
               Power(s2,3)*(-106 - 16*t1 + 103*t2) + 
               Power(s2,2)*(135 + 24*Power(t1,2) + t1*(316 - 307*t2) - 
                  203*t2 + 68*Power(t2,2)) + 
               Power(t1,2)*(138 - 209*t2 + 71*Power(t2,2)) - 
               s2*(16*Power(t1,3) + Power(t1,2)*(314 - 305*t2) + 
                  Power(-1 + t2,2)*(-1 + 30*t2) + 
                  t1*(273 - 412*t2 + 139*Power(t2,2)))) + 
            Power(s1,8)*(6*Power(s2,6) + 6*Power(t1,6) + 
               Power(s2,5)*(-36*t1 + 238*(-1 + t2)) - 
               230*Power(t1,5)*(-1 + t2) + 
               2*Power(-1 + t2,4)*(11 + 2*t2) + 
               9*t1*Power(-1 + t2,3)*(5 + 16*t2) + 
               Power(t1,4)*(202 - 160*t2 - 41*Power(t2,2)) + 
               Power(s2,4)*(254 + 90*Power(t1,2) - 1182*t1*(-1 + t2) - 
                  256*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-513 + 322*t2 + 91*Power(t2,2)) + 
               Power(t1,3)*(-407 + 1171*t2 - 1004*Power(t2,2) + 
                  240*Power(t2,3)) + 
               Power(s2,3)*(382 - 120*Power(t1,3) + 
                  2348*Power(t1,2)*(-1 + t2) - 1055*t2 + 
                  847*Power(t2,2) - 174*Power(t2,3) + 
                  t1*(-985 + 970*t2 + 11*Power(t2,2))) + 
               s2*(-36*Power(t1,5) + 1158*Power(t1,4)*(-1 + t2) - 
                  Power(-1 + t2,3)*(47 + 142*t2) - 
                  2*t1*Power(-1 + t2,2)*
                   (-527 + 354*t2 + 73*Power(t2,2)) + 
                  Power(t1,3)*(-881 + 778*t2 + 99*Power(t2,2)) + 
                  Power(t1,2)*
                   (1205 - 3424*t2 + 2882*Power(t2,2) - 
                     663*Power(t2,3))) + 
               Power(s2,2)*(90*Power(t1,4) - 
                  2332*Power(t1,3)*(-1 + t2) - 
                  6*Power(t1,2)*(-235 + 222*t2 + 12*Power(t2,2)) + 
                  Power(-1 + t2,2)*(-533 + 370*t2 + 63*Power(t2,2)) + 
                  t1*(-1180 + 3308*t2 - 2725*Power(t2,2) + 
                     597*Power(t2,3)))) + 
            Power(s1,7)*(4*Power(s2,7) - 4*Power(t1,7) + 
               10*Power(t1,6)*(-22 + 23*t2) - 
               Power(-1 + t2,4)*(33 + 112*t2) + 
               Power(s2,6)*(-232 - 28*t1 + 242*t2) + 
               2*Power(s2,5)*
                (-67 + 42*Power(t1,2) + t1*(690 - 720*t2) + 195*t2 - 
                  134*Power(t2,2)) + 
               Power(t1,5)*(245 - 612*t2 + 379*Power(t2,2)) + 
               Power(t1,4)*(1491 - 3235*t2 + 1855*Power(t2,2) - 
                  106*Power(t2,3)) + 
               t1*Power(-1 + t2,3)*
                (706 - 635*t2 - 362*Power(t2,2) + 12*Power(t2,3)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (341 - 2150*t2 + 1174*Power(t2,2) + 117*Power(t2,3)) + 
               Power(t1,3)*(1279 - 2044*t2 - 446*Power(t2,2) + 
                  1785*Power(t2,3) - 574*Power(t2,4)) + 
               Power(s2,4)*(1635 - 140*Power(t1,3) - 3610*t2 + 
                  2147*Power(t2,2) - 167*Power(t2,3) + 
                  30*Power(t1,2)*(-114 + 119*t2) + 
                  10*t1*(79 - 219*t2 + 146*Power(t2,2))) + 
               Power(s2,3)*(-1661 + 140*Power(t1,4) + 
                  Power(t1,3)*(4520 - 4720*t2) + 3245*t2 - 
                  1082*Power(t2,2) - 804*Power(t2,3) + 
                  302*Power(t2,4) + 
                  Power(t1,2)*(-1829 + 4878*t2 - 3169*Power(t2,2)) + 
                  2*t1*(-3253 + 7241*t2 - 4400*Power(t2,2) + 
                     402*Power(t2,3))) + 
               Power(s2,2)*(-84*Power(t1,5) + 
                  30*Power(t1,4)*(-112 + 117*t2) + 
                  Power(t1,3)*(2069 - 5358*t2 + 3409*Power(t2,2)) - 
                  Power(-1 + t2,2)*
                   (320 - 2152*t2 + 1293*Power(t2,2) + 21*Power(t2,3)) \
- 5*Power(t1,2)*(-1918 + 4269*t2 - 2598*Power(t2,2) + 
                     241*Power(t2,3)) + 
                  t1*(4664 - 8703*t2 + 1847*Power(t2,2) + 
                     3390*Power(t2,3) - 1198*Power(t2,4))) + 
               s2*(28*Power(t1,6) + Power(t1,5)*(1332 - 1392*t2) + 
                  Power(t1,4)*(-1141 + 2892*t2 - 1811*Power(t2,2)) - 
                  Power(-1 + t2,3)*
                   (721 - 686*t2 - 326*Power(t2,2) + 12*Power(t2,3)) + 
                  2*t1*Power(-1 + t2,2)*
                   (330 - 2162*t2 + 1257*Power(t2,2) + 57*Power(t2,3)) \
+ 2*Power(t1,3)*(-3105 + 6854*t2 - 4096*Power(t2,2) + 
                     337*Power(t2,3)) + 
                  Power(t1,2)*
                   (-4271 + 7458*t2 - 253*Power(t2,2) - 
                     4415*Power(t2,3) + 1481*Power(t2,4)))) + 
            Power(s1,6)*(Power(s2,8) + Power(t1,8) + 
               Power(t1,7)*(87 - 95*t2) + 
               Power(s2,7)*(-95 - 8*t1 + 103*t2) + 
               Power(t1,6)*(-487 + 934*t2 - 443*Power(t2,2)) + 
               Power(s2,6)*(-414 + 28*Power(t1,2) + 
                  t1*(657 - 713*t2) + 748*t2 - 330*Power(t2,2)) + 
               Power(t1,5)*(-1023 + 1440*t2 + 37*Power(t2,2) - 
                  454*Power(t2,3)) - 
               Power(-1 + t2,4)*
                (324 - 384*t2 - 271*Power(t2,2) + 12*Power(t2,3)) - 
               2*t1*Power(-1 + t2,3)*
                (85 + 1414*t2 - 1266*Power(t2,2) - 173*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (3119 - 3100*t2 - 3795*Power(t2,2) + 
                  2702*Power(t2,3) + 37*Power(t2,4)) + 
               Power(t1,4)*(1125 - 5591*t2 + 8282*Power(t2,2) - 
                  4289*Power(t2,3) + 468*Power(t2,4)) + 
               Power(t1,3)*(3875 - 14191*t2 + 16784*Power(t2,2) - 
                  5602*Power(t2,3) - 1620*Power(t2,4) + 754*Power(t2,5)\
) + Power(s2,5)*(1331 - 56*Power(t1,3) - 2264*t2 + 671*Power(t2,2) + 
                  262*Power(t2,3) + 3*Power(t1,2)*(-649 + 705*t2) + 
                  t1*(2650 - 4860*t2 + 2186*Power(t2,2))) + 
               Power(s2,4)*(476 + 70*Power(t1,4) - 4193*t2 + 
                  7451*Power(t2,2) - 4161*Power(t2,3) + 
                  422*Power(t2,4) - 5*Power(t1,3)*(-641 + 697*t2) + 
                  Power(t1,2)*(-6971 + 12942*t2 - 5911*Power(t2,2)) - 
                  t1*(6373 - 10706*t2 + 2989*Power(t2,2) + 
                     1344*Power(t2,3))) + 
               Power(s2,3)*(-4448 - 56*Power(t1,5) + 17287*t2 - 
                  22630*Power(t2,2) + 10857*Power(t2,3) - 
                  871*Power(t2,4) - 195*Power(t2,5) + 
                  5*Power(t1,4)*(-633 + 689*t2) + 
                  40*Power(t1,3)*(241 - 452*t2 + 209*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (6033 - 9831*t2 + 2275*Power(t2,2) + 
                     1523*Power(t2,3)) + 
                  t1*(-2814 + 19358*t2 - 32813*Power(t2,2) + 
                     18608*Power(t2,3) - 2319*Power(t2,4))) + 
               Power(s2,2)*(28*Power(t1,6) - 
                  3*Power(t1,5)*(-625 + 681*t2) - 
                  4*Power(t1,4)*(1846 - 3492*t2 + 1631*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (5680 - 8851*t2 + 1390*Power(t2,2) + 
                     1781*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-3865 + 4626*t2 + 2739*Power(t2,2) - 
                     2546*Power(t2,3) + 83*Power(t2,4)) + 
                  Power(t1,2)*
                   (5388 - 31928*t2 + 51777*Power(t2,2) - 
                     29118*Power(t2,3) + 3851*Power(t2,4)) + 
                  t1*(12985 - 49845*t2 + 63990*Power(t2,2) - 
                     28814*Power(t2,3) + 530*Power(t2,4) + 
                     1154*Power(t2,5))) - 
               s2*(8*Power(t1,7) + Power(t1,6)*(617 - 673*t2) + 
                  Power(t1,5)*(-2966 + 5652*t2 - 2662*Power(t2,2)) - 
                  Power(t1,4)*
                   (5359 - 7922*t2 + 511*Power(t2,2) + 
                     2052*Power(t2,3)) - 
                  Power(-1 + t2,3)*
                   (154 + 2899*t2 - 2707*Power(t2,2) - 
                     226*Power(t2,3) + 8*Power(t2,4)) - 
                  t1*Power(-1 + t2,2)*
                   (-7065 + 7914*t2 + 6425*Power(t2,2) - 
                     5270*Power(t2,3) + 70*Power(t2,4)) + 
                  Power(t1,3)*
                   (4175 - 22354*t2 + 34697*Power(t2,2) - 
                     18960*Power(t2,3) + 2422*Power(t2,4)) + 
                  Power(t1,2)*
                   (12404 - 46680*t2 + 57948*Power(t2,2) - 
                     23305*Power(t2,3) - 2117*Power(t2,4) + 
                     1750*Power(t2,5)))) + 
            Power(s1,5)*(Power(t1,8)*(-4 + 5*t2) + 
               Power(s2,8)*(-6 + 7*t2) + 
               Power(s2,7)*(-111 + 46*t1 + 189*t2 - 54*t1*t2 - 
                  78*Power(t2,2)) + 
               Power(t1,7)*(153 - 329*t2 + 176*Power(t2,2)) + 
               Power(t1,6)*(-356 + 1377*t2 - 1514*Power(t2,2) + 
                  489*Power(t2,3)) + 
               Power(-1 + t2,4)*
                (198 + 1317*t2 - 1598*Power(t2,2) - 229*Power(t2,3) + 
                  8*Power(t2,4)) - 
               t1*Power(-1 + t2,3)*
                (3442 - 5429*t2 - 3869*Power(t2,2) + 
                  4918*Power(t2,3) + 70*Power(t2,4)) + 
               Power(t1,5)*(-2207 + 6496*t2 - 5943*Power(t2,2) + 
                  1326*Power(t2,3) + 316*Power(t2,4)) + 
               Power(t1,4)*(-1386 + 602*t2 + 7826*Power(t2,2) - 
                  12603*Power(t2,3) + 6358*Power(t2,4) - 
                  798*Power(t2,5)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (4390 - 17797*t2 + 16186*Power(t2,2) + 
                  1286*Power(t2,3) - 2861*Power(t2,4) + 7*Power(t2,5)) \
+ Power(t1,3)*(1974 - 17332*t2 + 44264*Power(t2,2) - 
                  47071*Power(t2,3) + 20242*Power(t2,4) - 
                  1672*Power(t2,5) - 405*Power(t2,6)) + 
               Power(s2,6)*(14*Power(t1,2)*(-11 + 13*t2) + 
                  t1*(894 - 1613*t2 + 719*Power(t2,2)) + 
                  2*(-208 + 709*t2 - 740*Power(t2,2) + 237*Power(t2,3))\
) - Power(s2,5)*(-2439 + 7664*t2 - 7875*Power(t2,2) + 
                  2614*Power(t2,3) + 24*Power(t2,4) + 
                  14*Power(t1,3)*(-21 + 25*t2) + 
                  3*Power(t1,2)*(984 - 1849*t2 + 865*Power(t2,2)) + 
                  t1*(-2830 + 9490*t2 - 9778*Power(t2,2) + 
                     3094*Power(t2,3))) + 
               Power(s2,4)*(-3755 + 8934*t2 - 2778*Power(t2,2) - 
                  6645*Power(t2,3) + 4685*Power(t2,4) - 
                  442*Power(t2,5) + 70*Power(t1,4)*(-5 + 6*t2) + 
                  3*Power(t1,3)*(1747 - 3389*t2 + 1642*Power(t2,2)) + 
                  Power(t1,2)*
                   (-7636 + 25577*t2 - 26306*Power(t2,2) + 
                     8305*Power(t2,3)) + 
                  t1*(-12773 + 40044*t2 - 41565*Power(t2,2) + 
                     14590*Power(t2,3) - 356*Power(t2,4))) + 
               Power(s2,3)*(108 + Power(t1,5)*(266 - 322*t2) + 
                  12423*t2 - 44072*Power(t2,2) + 55027*Power(t2,3) - 
                  28061*Power(t2,4) + 4727*Power(t2,5) - 
                  152*Power(t2,6) - 
                  7*Power(t1,4)*(777 - 1549*t2 + 772*Power(t2,2)) - 
                  4*Power(t1,3)*
                   (-2611 + 8837*t2 - 9141*Power(t2,2) + 
                     2895*Power(t2,3)) + 
                  Power(t1,2)*
                   (25828 - 80216*t2 + 82458*Power(t2,2) - 
                     28580*Power(t2,3) + 630*Power(t2,4)) + 
                  2*t1*(6277 - 13457*t2 - 474*Power(t2,2) + 
                     17552*Power(t2,3) - 11326*Power(t2,4) + 
                     1430*Power(t2,5))) - 
               s2*(Power(t1,7)*(-34 + 42*t2) + 
                  3*Power(t1,6)*(366 - 767*t2 + 401*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (-1355 + 4873*t2 - 5197*Power(t2,2) + 
                     1667*Power(t2,3)) + 
                  Power(-1 + t2,3)*
                   (-3819 + 6164*t2 + 3426*Power(t2,2) - 
                     4977*Power(t2,3) + 74*Power(t2,4)) + 
                  Power(t1,4)*
                   (-12013 + 36112*t2 - 34839*Power(t2,2) + 
                     9894*Power(t2,3) + 786*Power(t2,4)) + 
                  Power(t1,3)*
                   (-7576 + 9118*t2 + 24200*Power(t2,2) - 
                     48760*Power(t2,3) + 26674*Power(t2,4) - 
                     3660*Power(t2,5)) + 
                  2*t1*Power(-1 + t2,2)*
                   (4915 - 20711*t2 + 20454*Power(t2,2) - 
                     1110*Power(t2,3) - 2373*Power(t2,4) + 
                     36*Power(t2,5)) + 
                  Power(t1,2)*
                   (4356 - 49852*t2 + 138877*Power(t2,2) - 
                     156529*Power(t2,3) + 72873*Power(t2,4) - 
                     9026*Power(t2,5) - 699*Power(t2,6))) + 
               Power(s2,2)*(14*Power(t1,6)*(-9 + 11*t2) + 
                  Power(t1,5)*(3312 - 6771*t2 + 3459*Power(t2,2)) + 
                  4*Power(t1,4)*
                   (-1894 + 6553*t2 - 6859*Power(t2,2) + 
                     2185*Power(t2,3)) + 
                  4*Power(t1,3)*
                   (-6325 + 19363*t2 - 19416*Power(t2,2) + 
                     6293*Power(t2,3) + 55*Power(t2,4)) + 
                  Power(t1,2)*
                   (-14989 + 26496*t2 + 20100*Power(t2,2) - 
                     64616*Power(t2,3) + 38283*Power(t2,4) - 
                     5280*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (5344 - 23101*t2 + 23918*Power(t2,2) - 
                     3094*Power(t2,3) - 1913*Power(t2,4) + 
                     57*Power(t2,5)) + 
                  t1*(2171 - 44461*t2 + 137824*Power(t2,2) - 
                     163801*Power(t2,3) + 80531*Power(t2,4) - 
                     12159*Power(t2,5) - 105*Power(t2,6)))) - 
            Power(-1 + t2,3)*
             (439 - 135*t2 - 5206*Power(t2,2) + 13638*Power(t2,3) - 
               14791*Power(t2,4) + 7221*Power(t2,5) - 846*Power(t2,6) - 
               320*Power(t2,7) + 
               Power(t1,7)*(-40 + 38*t2 - 6*Power(t2,2)) + 
               Power(s2,7)*(20 - 50*t2 + 30*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,6)*(131 + 10*t2 - 152*Power(t2,2) + 
                  19*Power(t2,3)) + 
               Power(t1,5)*(102 - 815*t2 + 710*Power(t2,2) + 
                  70*Power(t2,3) - 11*Power(t2,4)) + 
               Power(t1,4)*(-91 - 217*t2 + 2002*Power(t2,2) - 
                  1917*Power(t2,3) + 62*Power(t2,4) + 17*Power(t2,5)) + 
               Power(t1,3)*(-632 + 3198*t2 - 4992*Power(t2,2) + 
                  1461*Power(t2,3) + 1038*Power(t2,4) + 
                  82*Power(t2,5) - 19*Power(t2,6)) - 
               Power(t1,2)*(527 - 2710*t2 + 7888*Power(t2,2) - 
                  10608*Power(t2,3) + 4865*Power(t2,4) + 
                  6*Power(t2,5) + 88*Power(t2,6)) + 
               t1*(522 - 565*t2 - 3980*Power(t2,2) + 
                  11593*Power(t2,3) - 11489*Power(t2,4) + 
                  3510*Power(t2,5) + 473*Power(t2,6) - 56*Power(t2,7)) \
+ Power(s2,6)*(18 + 22*t2 - 104*Power(t2,2) + 89*Power(t2,3) - 
                  17*Power(t2,4) - 
                  2*t1*(66 - 157*t2 + 111*Power(t2,2) + 8*Power(t2,3))) \
+ Power(s2,5)*(-374 + 1324*t2 - 1299*Power(t2,2) + 245*Power(t2,3) + 
                  42*Power(t2,4) + 6*Power(t2,5) - 
                  6*Power(t1,2)*
                   (-62 + 129*t2 - 97*Power(t2,2) + 2*Power(t2,3)) + 
                  4*t1*(-19 - 81*t2 + 156*Power(t2,2) - 
                     84*Power(t2,3) + 16*Power(t2,4))) + 
               Power(s2,4)*(-178 - 441*t2 + 3211*Power(t2,2) - 
                  3866*Power(t2,3) + 1274*Power(t2,4) - 
                  144*Power(t2,5) + 
                  Power(t1,3)*
                   (-608 + 994*t2 - 714*Power(t2,2) + 48*Power(t2,3)) \
+ Power(t1,2)*(186 + 1285*t2 - 1791*Power(t2,2) + 518*Power(t2,3) - 
                     78*Power(t2,4)) + 
                  t1*(1806 - 6690*t2 + 6640*Power(t2,2) - 
                     1484*Power(t2,3) + 43*Power(t2,4) - 35*Power(t2,5)\
)) + Power(s2,3)*(1529 - 6237*t2 + 8706*Power(t2,2) - 
                  3082*Power(t2,3) - 1448*Power(t2,4) + 
                  368*Power(t2,5) + 28*Power(t2,6) + 
                  Power(t1,4)*
                   (652 - 766*t2 + 426*Power(t2,2) - 32*Power(t2,3)) + 
                  Power(t1,3)*
                   (-413 - 2141*t2 + 2821*Power(t2,2) - 
                     447*Power(t2,3) + 20*Power(t2,4)) + 
                  Power(t1,2)*
                   (-3175 + 12549*t2 - 12257*Power(t2,2) + 
                     2517*Power(t2,3) - 254*Power(t2,4) + 
                     60*Power(t2,5)) + 
                  t1*(439 + 1903*t2 - 11468*Power(t2,2) + 
                     12774*Power(t2,3) - 3425*Power(t2,4) + 
                     353*Power(t2,5))) - 
               Power(s2,2)*(703 - 7272*t2 + 23183*Power(t2,2) - 
                  31559*Power(t2,3) + 18592*Power(t2,4) - 
                  3981*Power(t2,5) + 390*Power(t2,6) + 
                  6*Power(t1,5)*(78 - 69*t2 + 19*Power(t2,2)) - 
                  Power(t1,4)*
                   (613 + 1623*t2 - 2397*Power(t2,2) + 
                     258*Power(t2,3) + 23*Power(t2,4)) + 
                  Power(t1,3)*
                   (-2530 + 11139*t2 - 10500*Power(t2,2) + 
                     1492*Power(t2,3) - 200*Power(t2,4) + 
                     39*Power(t2,5)) + 
                  Power(t1,2)*
                   (350 + 2989*t2 - 15688*Power(t2,2) + 
                     16132*Power(t2,3) - 3202*Power(t2,4) + 
                     283*Power(t2,5)) + 
                  t1*(4470 - 18483*t2 + 26624*Power(t2,2) - 
                     11237*Power(t2,3) - 2002*Power(t2,4) + 
                     121*Power(t2,5) + 99*Power(t2,6))) + 
               s2*(-751 - 1659*t2 + 16945*Power(t2,2) - 
                  36459*Power(t2,3) + 34028*Power(t2,4) - 
                  13372*Power(t2,5) + 1220*Power(t2,6) + 
                  40*Power(t2,7) + 
                  2*Power(t1,6)*
                   (102 - 85*t2 + 9*Power(t2,2) + 2*Power(t2,3)) - 
                  Power(t1,5)*
                   (459 + 475*t2 - 999*Power(t2,2) + 101*Power(t2,3) + 
                     12*Power(t2,4)) + 
                  Power(t1,4)*
                   (-889 + 4771*t2 - 4294*Power(t2,2) + 
                     144*Power(t2,3) - 20*Power(t2,4) + 8*Power(t2,5)) \
+ Power(t1,3)*(180 + 1744*t2 - 9433*Power(t2,2) + 9141*Power(t2,3) - 
                     1113*Power(t2,4) + 57*Power(t2,5)) + 
                  Power(t1,2)*
                   (3484 - 15078*t2 + 22343*Power(t2,2) - 
                     9221*Power(t2,3) - 1700*Power(t2,4) - 
                     326*Power(t2,5) + 90*Power(t2,6)) + 
                  t1*(1815 - 12157*t2 + 33822*Power(t2,2) - 
                     43236*Power(t2,3) + 23133*Power(t2,4) - 
                     3759*Power(t2,5) + 494*Power(t2,6)))) + 
            Power(s1,4)*(-4*Power(s2,9)*(-1 + t2) + 
               4*Power(t1,9)*(-1 + t2) + 
               Power(t1,8)*(39 - 34*t2 - 6*Power(t2,2)) + 
               Power(s2,8)*(70 + 36*t1*(-1 + t2) - 128*t2 + 
                  57*Power(t2,2)) + 
               Power(t1,7)*(154 - 699*t2 + 711*Power(t2,2) - 
                  158*Power(t2,3)) + 
               Power(t1,6)*(-470 + 1527*t2 - 1712*Power(t2,2) + 
                  983*Power(t2,3) - 338*Power(t2,4)) + 
               Power(-1 + t2,4)*
                (1357 - 2862*t2 - 1220*Power(t2,2) + 
                  2790*Power(t2,3) + 33*Power(t2,4)) - 
               t1*Power(-1 + t2,3)*
                (2065 - 16278*t2 + 21876*Power(t2,2) - 
                  2636*Power(t2,3) - 4058*Power(t2,4) + 22*Power(t2,5)) \
+ Power(t1,5)*(-2395 + 11954*t2 - 19713*Power(t2,2) + 
                  13139*Power(t2,3) - 3030*Power(t2,4) + 45*Power(t2,5)\
) + Power(t1,2)*Power(-1 + t2,2)*
                (353 - 16143*t2 + 44587*Power(t2,2) - 
                  37159*Power(t2,3) + 6176*Power(t2,4) + 
                  1222*Power(t2,5)) + 
               Power(t1,4)*(-2475 + 11423*t2 - 17510*Power(t2,2) + 
                  7758*Power(t2,3) + 3904*Power(t2,4) - 
                  3571*Power(t2,5) + 474*Power(t2,6)) + 
               Power(t1,3)*(-592 - 4893*t2 + 33737*Power(t2,2) - 
                  69765*Power(t2,3) + 64597*Power(t2,4) - 
                  26650*Power(t2,5) + 3497*Power(t2,6) + 69*Power(t2,7)\
) + Power(s2,7)*(-193 - 144*Power(t1,2)*(-1 + t2) + 697*t2 - 
                  678*Power(t2,2) + 166*Power(t2,3) + 
                  t1*(-517 + 906*t2 - 381*Power(t2,2))) + 
               Power(s2,6)*(-1002 + 336*Power(t1,3)*(-1 + t2) + 
                  3355*t2 - 3879*Power(t2,2) + 1937*Power(t2,3) - 
                  421*Power(t2,4) + 
                  Power(t1,2)*(1683 - 2806*t2 + 1095*Power(t2,2)) + 
                  t1*(1559 - 5616*t2 + 5508*Power(t2,2) - 
                     1395*Power(t2,3))) + 
               Power(s2,5)*(3729 - 504*Power(t1,4)*(-1 + t2) - 
                  16951*t2 + 27923*Power(t2,2) - 20101*Power(t2,3) + 
                  5730*Power(t2,4) - 330*Power(t2,5) + 
                  Power(t1,3)*(-3149 + 4954*t2 - 1749*Power(t2,2)) + 
                  3*Power(t1,2)*
                   (-1668 + 6080*t2 - 6021*Power(t2,2) + 
                     1553*Power(t2,3)) + 
                  2*t1*(2823 - 9513*t2 + 11014*Power(t2,2) - 
                     5501*Power(t2,3) + 1207*Power(t2,4))) + 
               Power(s2,4)*(-5726 + 504*Power(t1,5)*(-1 + t2) + 
                  29242*t2 - 53558*Power(t2,2) + 42560*Power(t2,3) - 
                  13055*Power(t2,4) + 600*Power(t2,5) - 
                  60*Power(t2,6) + 
                  5*Power(t1,4)*(739 - 1086*t2 + 333*Power(t2,2)) + 
                  Power(t1,3)*
                   (8440 - 31439*t2 + 31443*Power(t2,2) - 
                     8164*Power(t2,3)) + 
                  Power(t1,2)*
                   (-13271 + 45253*t2 - 53068*Power(t2,2) + 
                     27121*Power(t2,3) - 6185*Power(t2,4)) + 
                  t1*(-20038 + 89753*t2 - 146553*Power(t2,2) + 
                     105855*Power(t2,3) - 31475*Power(t2,4) + 
                     2458*Power(t2,5))) + 
               Power(s2,3)*(6531 - 336*Power(t1,6)*(-1 + t2) - 
                  23669*t2 + 17524*Power(t2,2) + 28658*Power(t2,3) - 
                  52139*Power(t2,4) + 27392*Power(t2,5) - 
                  4433*Power(t2,6) + 136*Power(t2,7) + 
                  Power(t1,5)*(-2775 + 3758*t2 - 927*Power(t2,2)) + 
                  Power(t1,4)*
                   (-8145 + 31281*t2 - 31572*Power(t2,2) + 
                     8156*Power(t2,3)) + 
                  Power(t1,3)*
                   (16427 - 56680*t2 + 67538*Power(t2,2) - 
                     35704*Power(t2,3) + 8619*Power(t2,4)) + 
                  Power(t1,2)*
                   (40285 - 180214*t2 + 292884*Power(t2,2) - 
                     210500*Power(t2,3) + 62807*Power(t2,4) - 
                     5262*Power(t2,5)) + 
                  t1*(21980 - 108386*t2 + 193061*Power(t2,2) - 
                     147520*Power(t2,3) + 39671*Power(t2,4) + 
                     1912*Power(t2,5) - 730*Power(t2,6))) + 
               Power(s2,2)*(144*Power(t1,7)*(-1 + t2) + 
                  Power(t1,6)*(1297 - 1586*t2 + 261*Power(t2,2)) - 
                  3*Power(t1,5)*
                   (-1501 + 6002*t2 - 6102*Power(t2,2) + 
                     1545*Power(t2,3)) + 
                  Power(t1,4)*
                   (-11085 + 38457*t2 - 46301*Power(t2,2) + 
                     25351*Power(t2,3) - 6572*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-2412 - 13657*t2 + 53363*Power(t2,2) - 
                     51065*Power(t2,3) + 12717*Power(t2,4) + 
                     90*Power(t2,5)) + 
                  Power(t1,3)*
                   (-37767 + 170927*t2 - 277514*Power(t2,2) + 
                     196978*Power(t2,3) - 57139*Power(t2,4) + 
                     4515*Power(t2,5)) + 
                  Power(t1,2)*
                   (-29118 + 139443*t2 - 240109*Power(t2,2) + 
                     171246*Power(t2,3) - 33576*Power(t2,4) - 
                     10073*Power(t2,5) + 2205*Power(t2,6)) + 
                  t1*(-13511 + 41195*t2 + 3032*Power(t2,2) - 
                     135233*Power(t2,3) + 177623*Power(t2,4) - 
                     86449*Power(t2,5) + 13541*Power(t2,6) - 
                     198*Power(t2,7))) + 
               s2*(-36*Power(t1,8)*(-1 + t2) + 
                  Power(t1,7)*(-343 + 366*t2 - 15*Power(t2,2)) + 
                  Power(t1,6)*
                   (-1314 + 5542*t2 - 5655*Power(t2,2) + 
                     1371*Power(t2,3)) + 
                  Power(t1,5)*
                   (3755 - 12886*t2 + 15394*Power(t2,2) - 
                     8686*Power(t2,3) + 2483*Power(t2,4)) + 
                  Power(t1,4)*
                   (16186 - 75469*t2 + 122973*Power(t2,2) - 
                     85371*Power(t2,3) + 23107*Power(t2,4) - 
                     1426*Power(t2,5)) + 
                  Power(-1 + t2,3)*
                   (2590 - 19327*t2 + 26433*Power(t2,2) - 
                     5207*Power(t2,3) - 3578*Power(t2,4) + 
                     80*Power(t2,5)) - 
                  2*t1*Power(-1 + t2,2)*
                   (-991 - 15140*t2 + 49619*Power(t2,2) - 
                     44841*Power(t2,3) + 9740*Power(t2,4) + 
                     649*Power(t2,5)) + 
                  Power(t1,3)*
                   (15339 - 71722*t2 + 118116*Power(t2,2) - 
                     74044*Power(t2,3) + 3056*Power(t2,4) + 
                     11132*Power(t2,5) - 1889*Power(t2,6)) - 
                  Power(t1,2)*
                   (-7427 + 11646*t2 + 57002*Power(t2,2) - 
                     180195*Power(t2,3) + 193076*Power(t2,4) - 
                     86892*Power(t2,5) + 12772*Power(t2,6) + 
                     18*Power(t2,7)))) - 
            s1*Power(-1 + t2,2)*
             (8*Power(t1,8)*(4 - 5*t2 + Power(t2,2)) + 
               16*Power(s2,8)*(2 - 5*t2 + 3*Power(t2,2)) - 
               Power(t1,7)*(17 + 167*t2 - 237*Power(t2,2) + 
                  26*Power(t2,3)) + 
               2*Power(t1,6)*
                (-285 + 788*t2 - 512*Power(t2,2) - 55*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(t1,5)*(123 + 1907*t2 - 5481*Power(t2,2) + 
                  3911*Power(t2,3) - 240*Power(t2,4) - 34*Power(t2,5)) \
+ Power(t1,4)*(1446 - 6732*t2 + 8321*Power(t2,2) - 1353*Power(t2,3) - 
                  1653*Power(t2,4) - 211*Power(t2,5) + 38*Power(t2,6)) \
+ Power(-1 + t2,2)*(-22 - 3568*t2 + 7633*Power(t2,2) - 
                  2487*Power(t2,3) - 4137*Power(t2,4) + 
                  2673*Power(t2,5) + 56*Power(t2,6)) + 
               Power(t1,3)*(2349 - 13685*t2 + 31997*Power(t2,2) - 
                  33592*Power(t2,3) + 14315*Power(t2,4) - 
                  1578*Power(t2,5) + 245*Power(t2,6)) + 
               Power(t1,2)*(-294 - 4432*t2 + 25672*Power(t2,2) - 
                  46968*Power(t2,3) + 35355*Power(t2,4) - 
                  8749*Power(t2,5) - 797*Power(t2,6) + 207*Power(t2,7)) \
+ t1*(-1863 + 1961*t2 + 13691*Power(t2,2) - 35709*Power(t2,3) + 
                  30507*Power(t2,4) - 5944*Power(t2,5) - 
                  3835*Power(t2,6) + 1192*Power(t2,7)) + 
               Power(s2,7)*(2 + 80*t2 - 184*Power(t2,2) + 
                  69*Power(t2,3) + 6*Power(t2,4) + 
                  4*t1*(-61 + 146*t2 - 87*Power(t2,2) + 2*Power(t2,3))) \
+ Power(s2,6)*(-756 + 2502*t2 - 2578*Power(t2,2) + 653*Power(t2,3) + 
                  72*Power(t2,4) - 7*Power(t2,5) - 
                  4*Power(t1,2)*
                   (-203 + 457*t2 - 263*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(24 - 810*t2 + 1527*Power(t2,2) - 
                     535*Power(t2,3) - 17*Power(t2,4))) + 
               Power(s2,5)*(336 - 3229*t2 + 7992*Power(t2,2) - 
                  7265*Power(t2,3) + 2160*Power(t2,4) - 
                  180*Power(t2,5) + 
                  4*Power(t1,3)*
                   (-388 + 805*t2 - 432*Power(t2,2) + 15*Power(t2,3)) \
+ 4*t1*(1083 - 3620*t2 + 3771*Power(t2,2) - 1069*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(t1,2)*
                   (-78 + 2827*t2 - 4872*Power(t2,2) + 
                     1536*Power(t2,3) + 20*Power(t2,4))) - 
               Power(s2,4)*(-3782 + 13602*t2 - 15634*Power(t2,2) + 
                  3716*Power(t2,3) + 3179*Power(t2,4) - 
                  972*Power(t2,5) + 35*Power(t2,6) + 
                  40*Power(t1,4)*
                   (-47 + 88*t2 - 42*Power(t2,2) + Power(t2,3)) + 
                  5*Power(t1,3)*
                   (-3 + 995*t2 - 1627*Power(t2,2) + 
                     440*Power(t2,3) + 6*Power(t2,4)) + 
                  t1*(2139 - 17521*t2 + 40454*Power(t2,2) - 
                     34713*Power(t2,3) + 9551*Power(t2,4) - 
                     840*Power(t2,5)) - 
                  2*Power(t1,2)*
                   (-5029 + 16809*t2 - 17333*Power(t2,2) + 
                     5035*Power(t2,3) - 362*Power(t2,4) + 
                     25*Power(t2,5))) + 
               Power(s2,3)*(-4992 + 30944*t2 - 73579*Power(t2,2) + 
                  82693*Power(t2,3) - 44342*Power(t2,4) + 
                  10288*Power(t2,5) - 1063*Power(t2,6) - 
                  4*Power(t1,5)*(373 - 620*t2 + 247*Power(t2,2)) + 
                  5*Power(t1,4)*
                   (34 + 1006*t2 - 1584*Power(t2,2) + 
                     345*Power(t2,3) + 10*Power(t2,4)) + 
                  Power(t1,2)*
                   (4030 - 33904*t2 + 76729*Power(t2,2) - 
                     62646*Power(t2,3) + 15263*Power(t2,4) - 
                     1332*Power(t2,5)) - 
                  2*Power(t1,3)*
                   (-6156 + 20285*t2 - 20149*Power(t2,2) + 
                     5487*Power(t2,3) - 647*Power(t2,4) + 
                     40*Power(t2,5)) - 
                  2*t1*(7112 - 26562*t2 + 32273*Power(t2,2) - 
                     10666*Power(t2,3) - 3075*Power(t2,4) + 
                     582*Power(t2,5) + 48*Power(t2,6))) + 
               Power(s2,2)*(384 - 22246*t2 + 92349*Power(t2,2) - 
                  151892*Power(t2,3) + 117953*Power(t2,4) - 
                  41057*Power(t2,5) + 4506*Power(t2,6) - 
                  3*Power(t2,7) + 
                  4*Power(t1,6)*
                   (191 - 281*t2 + 87*Power(t2,2) + 3*Power(t2,3)) - 
                  Power(t1,5)*
                   (222 + 3040*t2 - 4629*Power(t2,2) + 
                     759*Power(t2,3) + 41*Power(t2,4)) + 
                  Power(t1,4)*
                   (-8568 + 27272*t2 - 25068*Power(t2,2) + 
                     5467*Power(t2,3) - 858*Power(t2,4) + 
                     45*Power(t2,5)) + 
                  Power(t1,3)*
                   (-2864 + 30068*t2 - 69545*Power(t2,2) + 
                     54124*Power(t2,3) - 10753*Power(t2,4) + 
                     830*Power(t2,5)) + 
                  Power(t1,2)*
                   (18356 - 71746*t2 + 90579*Power(t2,2) - 
                     33707*Power(t2,3) - 3710*Power(t2,4) - 
                     957*Power(t2,5) + 321*Power(t2,6)) + 
                  t1*(15982 - 90048*t2 + 201920*Power(t2,2) - 
                     217251*Power(t2,3) + 111253*Power(t2,4) - 
                     24422*Power(t2,5) + 2719*Power(t2,6))) + 
               s2*(1234 + 8831*t2 - 54489*Power(t2,2) + 
                  103027*Power(t2,3) - 86032*Power(t2,4) + 
                  27888*Power(t2,5) + 891*Power(t2,6) - 
                  1350*Power(t2,7) - 
                  4*Power(t1,7)*
                   (58 - 77*t2 + 18*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,6)*
                   (106 + 1055*t2 - 1552*Power(t2,2) + 
                     190*Power(t2,3) + 12*Power(t2,4)) + 
                  Power(t1,4)*
                   (514 - 12363*t2 + 30759*Power(t2,2) - 
                     22837*Power(t2,3) + 3121*Power(t2,4) - 
                     124*Power(t2,5)) + 
                  Power(t1,5)*
                   (3308 - 9918*t2 + 7954*Power(t2,2) - 
                     830*Power(t2,3) + 178*Power(t2,4) - 8*Power(t2,5)) \
+ Power(t1,2)*(-13222 + 72481*t2 - 160386*Power(t2,2) + 
                     169070*Power(t2,3) - 82373*Power(t2,4) + 
                     16276*Power(t2,5) - 1999*Power(t2,6)) - 
                  4*Power(t1,3)*
                   (2340 - 9739*t2 + 12497*Power(t2,2) - 
                     4361*Power(t2,3) - 598*Power(t2,4) - 
                     340*Power(t2,5) + 57*Power(t2,6)) - 
                  2*t1*(718 - 15941*t2 + 62615*Power(t2,2) - 
                     101354*Power(t2,3) + 76692*Power(t2,4) - 
                     24621*Power(t2,5) + 1751*Power(t2,6) + 
                     134*Power(t2,7)))) + 
            Power(s1,2)*(-1 + t2)*
             (2*Power(t1,9)*(4 - 5*t2 + Power(t2,2)) - 
               6*Power(s2,9)*(2 - 5*t2 + 3*Power(t2,2)) + 
               Power(t1,8)*(34 - 120*t2 + 99*Power(t2,2) - 
                  7*Power(t2,3)) + 
               Power(t1,7)*(-349 + 696*t2 - 319*Power(t2,2) - 
                  57*Power(t2,3) + 3*Power(t2,4)) - 
               Power(t1,6)*(382 - 2952*t2 + 4886*Power(t2,2) - 
                  2603*Power(t2,3) + 226*Power(t2,4) + 17*Power(t2,5)) \
+ Power(-1 + t2,3)*(-1010 + 980*t2 + 7609*Power(t2,2) - 
                  13486*Power(t2,3) + 5672*Power(t2,4) + 
                  504*Power(t2,5)) + 
               Power(t1,5)*(1326 - 3919*t2 + 1905*Power(t2,2) + 
                  1802*Power(t2,3) - 927*Power(t2,4) - 
                  242*Power(t2,5) + 19*Power(t2,6)) + 
               t1*Power(-1 + t2,2)*
                (-94 - 5013*t2 + 3772*Power(t2,2) + 
                  15975*Power(t2,3) - 21657*Power(t2,4) + 
                  6534*Power(t2,5) + 173*Power(t2,6)) + 
               Power(t1,4)*(3614 - 21768*t2 + 45307*Power(t2,2) - 
                  41144*Power(t2,3) + 16697*Power(t2,4) - 
                  2950*Power(t2,5) + 258*Power(t2,6)) + 
               Power(t1,3)*(1601 - 16560*t2 + 54694*Power(t2,2) - 
                  77326*Power(t2,3) + 48061*Power(t2,4) - 
                  10226*Power(t2,5) - 484*Power(t2,6) + 238*Power(t2,7)\
) + Power(t1,2)*(-2024 + 748*t2 + 17359*Power(t2,2) - 
                  31886*Power(t2,3) + 10751*Power(t2,4) + 
                  15307*Power(t2,5) - 12269*Power(t2,6) + 
                  2014*Power(t2,7)) + 
               2*Power(s2,8)*
                (17 - 49*t2 + 47*Power(t2,2) - 12*Power(t2,3) + 
                  t1*(52 - 125*t2 + 73*Power(t2,2))) + 
               Power(s2,7)*(466 - 1386*t2 + 1395*Power(t2,2) - 
                  502*Power(t2,3) + 53*Power(t2,4) - 
                  40*Power(t1,2)*(10 - 23*t2 + 13*Power(t2,2)) + 
                  t1*(-272 + 838*t2 - 821*Power(t2,2) + 
                     207*Power(t2,3))) + 
               Power(s2,6)*(-1068 + 4919*t2 - 7673*Power(t2,2) + 
                  4864*Power(t2,3) - 1029*Power(t2,4) + 
                  31*Power(t2,5) + 
                  56*Power(t1,3)*(16 - 35*t2 + 19*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (-304 + 990*t2 - 977*Power(t2,2) + 235*Power(t2,3)) \
+ t1*(-3060 + 8930*t2 - 8809*Power(t2,2) + 3101*Power(t2,3) - 
                     344*Power(t2,4))) + 
               Power(s2,5)*(-2892 + 8783*t2 - 7143*Power(t2,2) - 
                  663*Power(t2,3) + 2468*Power(t2,4) - 
                  577*Power(t2,5) + 60*Power(t2,6) - 
                  28*Power(t1,4)*(46 - 95*t2 + 49*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1704 + 5830*t2 - 5729*Power(t2,2) + 
                     1267*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (2879 - 8196*t2 + 7889*Power(t2,2) - 
                     2730*Power(t2,3) + 340*Power(t2,4)) + 
                  t1*(6566 - 30612*t2 + 47788*Power(t2,2) - 
                     30412*Power(t2,3) + 6778*Power(t2,4) - 
                     372*Power(t2,5))) + 
               Power(s2,4)*(8540 - 43546*t2 + 86350*Power(t2,2) - 
                  82634*Power(t2,3) + 39272*Power(t2,4) - 
                  8711*Power(t2,5) + 743*Power(t2,6) + 
                  28*Power(t1,5)*(44 - 85*t2 + 41*Power(t2,2)) - 
                  5*Power(t1,4)*
                   (-396 + 1414*t2 - 1367*Power(t2,2) + 
                     265*Power(t2,3)) + 
                  Power(t1,3)*
                   (-13617 + 37468*t2 - 34797*Power(t2,2) + 
                     11733*Power(t2,3) - 1697*Power(t2,4)) + 
                  Power(t1,2)*
                   (-15546 + 74622*t2 - 116951*Power(t2,2) + 
                     73868*Power(t2,3) - 16381*Power(t2,4) + 
                     1048*Power(t2,5)) + 
                  t1*(13021 - 40170*t2 + 33344*Power(t2,2) + 
                     1210*Power(t2,3) - 8940*Power(t2,4) + 
                     1445*Power(t2,5) - 90*Power(t2,6))) - 
               Power(s2,3)*(5617 - 52817*t2 + 160806*Power(t2,2) - 
                  224625*Power(t2,3) + 155236*Power(t2,4) - 
                  50309*Power(t2,5) + 6286*Power(t2,6) - 
                  196*Power(t2,7) + 
                  56*Power(t1,6)*(14 - 25*t2 + 11*Power(t2,2)) + 
                  Power(t1,5)*
                   (1504 - 5538*t2 + 5199*Power(t2,2) - 
                     829*Power(t2,3)) + 
                  Power(t1,4)*
                   (-12988 + 34082*t2 - 29873*Power(t2,2) + 
                     9482*Power(t2,3) - 1613*Power(t2,4)) + 
                  Power(t1,3)*
                   (-18304 + 92336*t2 - 145741*Power(t2,2) + 
                     90375*Power(t2,3) - 18991*Power(t2,4) + 
                     1205*Power(t2,5)) + 
                  Power(t1,2)*
                   (23306 - 73572*t2 + 63499*Power(t2,2) - 
                     3441*Power(t2,3) - 10399*Power(t2,4) + 
                     147*Power(t2,5) + 100*Power(t2,6)) + 
                  t1*(34757 - 174761*t2 + 341806*Power(t2,2) - 
                     323454*Power(t2,3) + 153760*Power(t2,4) - 
                     35512*Power(t2,5) + 3460*Power(t2,6))) + 
               Power(s2,2)*(1531 - 36087*t2 + 136456*Power(t2,2) - 
                  211477*Power(t2,3) + 150134*Power(t2,4) - 
                  38418*Power(t2,5) - 3964*Power(t2,6) + 
                  1825*Power(t2,7) + 
                  40*Power(t1,7)*(8 - 13*t2 + 5*Power(t2,2)) + 
                  Power(t1,6)*
                   (752 - 2798*t2 + 2521*Power(t2,2) - 
                     307*Power(t2,3)) - 
                  3*Power(t1,5)*
                   (2506 - 6158*t2 + 4889*Power(t2,2) - 
                     1325*Power(t2,3) + 270*Power(t2,4)) + 
                  Power(t1,4)*
                   (-11276 + 61611*t2 - 98302*Power(t2,2) + 
                     59077*Power(t2,3) - 11032*Power(t2,4) + 
                     582*Power(t2,5)) + 
                  Power(t1,3)*
                   (20443 - 65685*t2 + 57443*Power(t2,2) - 
                     6058*Power(t2,3) - 4777*Power(t2,4) - 
                     1975*Power(t2,5) + 249*Power(t2,6)) + 
                  3*Power(t1,2)*
                   (15833 - 80441*t2 + 157544*Power(t2,2) - 
                     148119*Power(t2,3) + 69763*Power(t2,4) - 
                     16213*Power(t2,5) + 1661*Power(t2,6)) + 
                  t1*(18083 - 144302*t2 + 413099*Power(t2,2) - 
                     557359*Power(t2,3) + 372307*Power(t2,4) - 
                     113982*Power(t2,5) + 12121*Power(t2,6) + 
                     27*Power(t2,7))) + 
               s2*(-2*Power(t1,8)*(38 - 55*t2 + 17*Power(t2,2)) + 
                  Power(t1,7)*
                   (-232 + 850*t2 - 731*Power(t2,2) + 65*Power(t2,3)) + 
                  Power(t1,6)*
                   (2453 - 5512*t2 + 3657*Power(t2,2) - 
                     578*Power(t2,3) + 162*Power(t2,4)) + 
                  Power(t1,5)*
                   (3402 - 21156*t2 + 34283*Power(t2,2) - 
                     19625*Power(t2,3) + 2899*Power(t2,4) - 
                     67*Power(t2,5)) + 
                  Power(t1,3)*
                   (-24896 + 131876*t2 - 262483*Power(t2,2) + 
                     244681*Power(t2,3) - 111498*Power(t2,4) + 
                     24788*Power(t2,5) - 2524*Power(t2,6)) + 
                  Power(t1,4)*
                   (-8592 + 27419*t2 - 22050*Power(t2,2) + 
                     268*Power(t2,3) + 1777*Power(t2,4) + 
                     1496*Power(t2,5) - 138*Power(t2,6)) + 
                  Power(-1 + t2,2)*
                   (-2046 + 15759*t2 - 19197*Power(t2,2) - 
                     10624*Power(t2,3) + 24986*Power(t2,4) - 
                     8606*Power(t2,5) + 38*Power(t2,6)) + 
                  t1*(-360 + 38796*t2 - 159040*Power(t2,2) + 
                     246536*Power(t2,3) - 160531*Power(t2,4) + 
                     21319*Power(t2,5) + 17417*Power(t2,6) - 
                     4137*Power(t2,7)) - 
                  Power(t1,2)*
                   (14141 - 108698*t2 + 308989*Power(t2,2) - 
                     412986*Power(t2,3) + 267291*Power(t2,4) - 
                     74599*Power(t2,5) + 5368*Power(t2,6) + 
                     488*Power(t2,7)))) + 
            Power(s1,3)*(-4*Power(t1,9)*(3 - 5*t2 + 2*Power(t2,2)) + 
               2*Power(s2,9)*(9 - 20*t2 + 11*Power(t2,2)) + 
               Power(t1,8)*(42 - 18*t2 - 44*Power(t2,2) + 
                  19*Power(t2,3)) + 
               Power(t1,7)*(385 - 1414*t2 + 1580*Power(t2,2) - 
                  595*Power(t2,3) + 48*Power(t2,4)) + 
               Power(-1 + t2,4)*
                (297 - 5931*t2 + 10405*Power(t2,2) - 2958*Power(t2,3) - 
                  1951*Power(t2,4) + 15*Power(t2,5)) + 
               Power(t1,6)*(-434 - 212*t2 + 2688*Power(t2,2) - 
                  2629*Power(t2,3) + 468*Power(t2,4) + 113*Power(t2,5)) \
- t1*Power(-1 + t2,3)*(-1829 - 4374*t2 + 30568*Power(t2,2) - 
                  36482*Power(t2,3) + 10239*Power(t2,4) + 
                  1321*Power(t2,5)) + 
               Power(t1,5)*(-2325 + 13205*t2 - 25342*Power(t2,2) + 
                  21565*Power(t2,3) - 8702*Power(t2,4) + 
                  1704*Power(t2,5) - 101*Power(t2,6)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (-122 + 3112*t2 - 23931*Power(t2,2) + 
                  48254*Power(t2,3) - 34678*Power(t2,4) + 
                  6629*Power(t2,5) + 186*Power(t2,6)) + 
               Power(t1,3)*(-107 + 6360*t2 - 21383*Power(t2,2) + 
                  20219*Power(t2,3) + 6858*Power(t2,4) - 
                  21648*Power(t2,5) + 11317*Power(t2,6) - 
                  1616*Power(t2,7)) + 
               Power(t1,4)*(-3403 + 23685*t2 - 59726*Power(t2,2) + 
                  69706*Power(t2,3) - 37846*Power(t2,4) + 
                  7555*Power(t2,5) + 115*Power(t2,6) - 87*Power(t2,7)) + 
               Power(s2,8)*(80 - 200*t2 + 160*Power(t2,2) - 
                  41*Power(t2,3) - 4*t1*(39 - 85*t2 + 46*Power(t2,2))) + 
               Power(s2,7)*(-624 + 2329*t2 - 2991*Power(t2,2) + 
                  1524*Power(t2,3) - 242*Power(t2,4) + 
                  40*Power(t1,2)*(15 - 32*t2 + 17*Power(t2,2)) + 
                  2*t1*(-289 + 667*t2 - 490*Power(t2,2) + 
                     116*Power(t2,3))) + 
               Power(s2,6)*(-440 + 509*t2 + 1172*Power(t2,2) - 
                  1974*Power(t2,3) + 736*Power(t2,4) - 9*Power(t2,5) - 
                  112*Power(t1,3)*(12 - 25*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*
                   (1870 - 3942*t2 + 2596*Power(t2,2) - 
                     552*Power(t2,3)) + 
                  t1*(4297 - 16175*t2 + 20810*Power(t2,2) - 
                     10618*Power(t2,3) + 1714*Power(t2,4))) + 
               Power(s2,5)*(5363 - 24639*t2 + 43588*Power(t2,2) - 
                  37209*Power(t2,3) + 15691*Power(t2,4) - 
                  2856*Power(t2,5) + 58*Power(t2,6) + 
                  28*Power(t1,4)*(69 - 140*t2 + 71*Power(t2,2)) + 
                  Power(t1,3)*
                   (-3522 + 6718*t2 - 3836*Power(t2,2) + 
                     696*Power(t2,3)) + 
                  Power(t1,2)*
                   (-12417 + 47170*t2 - 60771*Power(t2,2) + 
                     31017*Power(t2,3) - 5083*Power(t2,4)) + 
                  2*t1*(891 + 295*t2 - 6346*Power(t2,2) + 
                     7898*Power(t2,3) - 2871*Power(t2,4) + 
                     151*Power(t2,5))) - 
               Power(s2,4)*(7700 - 50272*t2 + 125052*Power(t2,2) - 
                  152092*Power(t2,3) + 94473*Power(t2,4) - 
                  28015*Power(t2,5) + 3295*Power(t2,6) - 
                  140*Power(t2,7) + 
                  56*Power(t1,5)*(33 - 65*t2 + 32*Power(t2,2)) + 
                  10*Power(t1,4)*
                   (-419 + 715*t2 - 334*Power(t2,2) + 45*Power(t2,3)) + 
                  Power(t1,3)*
                   (-19563 + 74924*t2 - 96384*Power(t2,2) + 
                     48963*Power(t2,3) - 8080*Power(t2,4)) + 
                  Power(t1,2)*
                   (3512 + 5864*t2 - 38104*Power(t2,2) + 
                     42465*Power(t2,3) - 14314*Power(t2,4) + 
                     667*Power(t2,5)) + 
                  t1*(26566 - 123551*t2 + 220444*Power(t2,2) - 
                     190349*Power(t2,3) + 82950*Power(t2,4) - 
                     17036*Power(t2,5) + 956*Power(t2,6))) + 
               Power(s2,2)*(-80*Power(t1,7)*
                   (6 - 11*t2 + 5*Power(t2,2)) + 
                  2*Power(t1,6)*
                   (749 - 937*t2 + 110*Power(t2,2) + 64*Power(t2,3)) + 
                  Power(t1,5)*
                   (9987 - 38383*t2 + 48090*Power(t2,2) - 
                     23232*Power(t2,3) + 3622*Power(t2,4)) + 
                  Power(t1,4)*
                   (-4182 - 5873*t2 + 38420*Power(t2,2) - 
                     38524*Power(t2,3) + 9450*Power(t2,4) + 
                     619*Power(t2,5)) + 
                  Power(t1,3)*
                   (-44297 + 217116*t2 - 398962*Power(t2,2) + 
                     350446*Power(t2,3) - 156388*Power(t2,4) + 
                     34764*Power(t2,5) - 2639*Power(t2,6)) + 
                  Power(-1 + t2,2)*
                   (-5943 + 23284*t2 - 11327*Power(t2,2) - 
                     35484*Power(t2,3) + 39638*Power(t2,4) - 
                     9837*Power(t2,5) + 219*Power(t2,6)) + 
                  t1*(-14660 + 120333*t2 - 336414*Power(t2,2) + 
                     427444*Power(t2,3) - 244456*Power(t2,4) + 
                     34720*Power(t2,5) + 16745*Power(t2,6) - 
                     3712*Power(t2,7)) - 
                  Power(t1,2)*
                   (45751 - 277469*t2 + 657752*Power(t2,2) - 
                     769972*Power(t2,3) + 459263*Power(t2,4) - 
                     127682*Power(t2,5) + 12282*Power(t2,6) + 
                     81*Power(t2,7))) + 
               Power(s2,3)*(6023 - 51521*t2 + 148187*Power(t2,2) - 
                  196154*Power(t2,3) + 123756*Power(t2,4) - 
                  29606*Power(t2,5) - 1377*Power(t2,6) + 
                  692*Power(t2,7) + 
                  56*Power(t1,6)*(21 - 40*t2 + 19*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (-1595 + 2385*t2 - 790*Power(t2,2) + 28*Power(t2,3)) \
+ Power(t1,4)*(-18182 + 69981*t2 - 89341*Power(t2,2) + 
                     44702*Power(t2,3) - 7300*Power(t2,4)) + 
                  2*Power(t1,3)*
                   (2339 + 4587*t2 - 26284*Power(t2,2) + 
                     27494*Power(t2,3) - 8159*Power(t2,4) + 
                     83*Power(t2,5)) + 
                  Power(t1,2)*
                   (50109 - 238004*t2 + 430300*Power(t2,2) - 
                     375546*Power(t2,3) + 166886*Power(t2,4) - 
                     36372*Power(t2,5) + 2587*Power(t2,6)) - 
                  2*t1*(-16330 + 100947*t2 - 242844*Power(t2,2) + 
                     288726*Power(t2,3) - 176145*Power(t2,4) + 
                     51166*Power(t2,5) - 5634*Power(t2,6) + 
                     112*Power(t2,7))) + 
               s2*(2*Power(t1,8)*(57 - 100*t2 + 43*Power(t2,2)) + 
                  Power(t1,7)*
                   (-390 + 362*t2 + 124*Power(t2,2) - 88*Power(t2,3)) + 
                  Power(t1,6)*
                   (-3009 + 11416*t2 - 13761*Power(t2,2) + 
                     6165*Power(t2,3) - 839*Power(t2,4)) - 
                  4*Power(t1,5)*
                   (-527 - 419*t2 + 3781*Power(t2,2) - 
                     3702*Power(t2,3) + 727*Power(t2,4) + 
                     131*Power(t2,5)) + 
                  Power(-1 + t2,3)*
                   (-2966 - 3827*t2 + 36204*Power(t2,2) - 
                     44898*Power(t2,3) + 14239*Power(t2,4) + 
                     691*Power(t2,5)) - 
                  2*t1*Power(-1 + t2,2)*
                   (-2930 + 10091*t2 + 6557*Power(t2,2) - 
                     42587*Power(t2,3) + 37904*Power(t2,4) - 
                     8499*Power(t2,5) + 14*Power(t2,6)) + 
                  Power(t1,4)*
                   (17716 - 91229*t2 + 170860*Power(t2,2) - 
                     149605*Power(t2,3) + 65463*Power(t2,4) - 
                     14276*Power(t2,5) + 1051*Power(t2,6)) + 
                  2*Power(t1,3)*
                   (12097 - 74766*t2 + 178421*Power(t2,2) - 
                     207159*Power(t2,3) + 119646*Power(t2,4) - 
                     30460*Power(t2,5) + 2097*Power(t2,6) + 
                     126*Power(t2,7)) + 
                  Power(t1,2)*
                   (8801 - 75328*t2 + 209348*Power(t2,2) - 
                     249885*Power(t2,3) + 111091*Power(t2,4) + 
                     18806*Power(t2,5) - 27625*Power(t2,6) + 
                     4792*Power(t2,7))))) + 
         Power(s,2)*(-3*Power(s1,10)*(s2 - t1)*
             (Power(s2,2) + Power(t1,2) - 2*s2*(2 + t1 - 2*t2) - 
               4*t1*(-1 + t2) + 2*Power(-1 + t2,2)) - 
            Power(s1,9)*(13*Power(s2,4) + 14*Power(t1,4) + 
               Power(t1,3)*(155 - 144*t2) + 
               2*Power(-1 + t2,3)*(5 + t2) + 
               t1*Power(-1 + t2,2)*(9 + 5*t2) + 
               Power(s2,3)*(-161 - 53*t1 + 150*t2) + 
               Power(s2,2)*(151 + 81*Power(t1,2) + t1*(477 - 444*t2) - 
                  244*t2 + 93*Power(t2,2)) + 
               Power(t1,2)*(163 - 268*t2 + 105*Power(t2,2)) + 
               s2*(-55*Power(t1,3) - Power(-1 + t2,2)*(5 + 9*t2) + 
                  Power(t1,2)*(-471 + 438*t2) - 
                  2*t1*(157 - 256*t2 + 99*Power(t2,2)))) + 
            Power(s1,8)*(-16*Power(s2,5) + 24*Power(t1,5) + 
               Power(t1,4)*(593 - 610*t2) + 
               Power(s2,4)*(561 + 88*t1 - 574*t2) + 
               Power(-1 + t2,3)*(23 + 47*t2) + 
               t1*Power(-1 + t2,2)*(-384 + 334*t2 + 25*Power(t2,2)) - 
               Power(s2,3)*(717 + 192*Power(t1,2) + 
                  t1*(2255 - 2311*t2) - 855*t2 + 170*Power(t2,2)) + 
               Power(t1,3)*(741 - 948*t2 + 239*Power(t2,2)) + 
               2*Power(t1,2)*
                (-85 + 408*t2 - 481*Power(t2,2) + 158*Power(t2,3)) + 
               2*Power(s2,2)*
                (-64 + 104*Power(t1,3) + 303*t2 - 334*Power(t2,2) + 
                  95*Power(t2,3) - 45*Power(t1,2)*(-38 + 39*t2) + 
                  3*t1*(369 - 456*t2 + 103*Power(t2,2))) - 
               s2*(112*Power(t1,4) + Power(t1,3)*(2319 - 2383*t2) + 
                  Power(-1 + t2,2)*(-389 + 334*t2 + 30*Power(t2,2)) + 
                  3*Power(t1,2)*(746 - 943*t2 + 229*Power(t2,2)) + 
                  4*t1*(-69 + 339*t2 - 391*Power(t2,2) + 
                     121*Power(t2,3)))) + 
            Power(s1,7)*(2*Power(s2,6) - 20*Power(t1,6) + 
               2*Power(s2,5)*(409 + 5*t1 - 463*t2) + 
               Power(t1,5)*(-883 + 1007*t2) + 
               Power(t1,4)*(-307 - 409*t2 + 764*Power(t2,2)) + 
               Power(t1,3)*(2455 - 7048*t2 + 5702*Power(t2,2) - 
                  1168*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (265 - 348*t2 - 115*Power(t2,2) + 6*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (378 + 1262*t2 - 1350*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(2581 - 6360*t2 + 3898*Power(t2,2) + 
                  627*Power(t2,3) - 746*Power(t2,4)) + 
               Power(s2,4)*(-678 - 80*Power(t1,2) + 294*t2 + 
                  413*Power(t2,2) + t1*(-4164 + 4720*t2)) + 
               Power(s2,3)*(-2047 + 180*Power(t1,3) + 
                  Power(t1,2)*(8485 - 9629*t2) + 5780*t2 - 
                  4206*Power(t2,2) + 532*Power(t2,3) + 
                  t1*(2563 - 1004*t2 - 1694*Power(t2,2))) + 
               Power(s2,2)*(2712 - 190*Power(t1,4) - 6924*t2 + 
                  5096*Power(t2,2) - 601*Power(t2,3) - 
                  283*Power(t2,4) + Power(t1,3)*(-8633 + 9809*t2) + 
                  3*Power(t1,2)*(-1123 + 219*t2 + 981*Power(t2,2)) + 
                  t1*(6543 - 18803*t2 + 14522*Power(t2,2) - 
                     2439*Power(t2,3))) + 
               s2*(98*Power(t1,5) + Power(t1,4)*(4377 - 4981*t2) + 
                  Power(t1,3)*(1791 + 462*t2 - 2426*Power(t2,2)) - 
                  Power(-1 + t2,2)*
                   (374 + 1227*t2 - 1332*Power(t2,2) + 28*Power(t2,3)) \
+ Power(t1,2)*(-6969 + 20125*t2 - 16072*Power(t2,2) + 
                     3093*Power(t2,3)) + 
                  2*t1*(-2670 + 6761*t2 - 4713*Power(t2,2) + 
                     156*Power(t2,3) + 466*Power(t2,4)))) + 
            Power(s1,6)*(17*Power(s2,7) + 11*Power(t1,7) + 
               Power(t1,6)*(527 - 701*t2) + 
               Power(s2,6)*(554 - 91*t1 - 676*t2) + 
               Power(t1,5)*(-1253 + 3335*t2 - 2016*Power(t2,2)) - 
               Power(t1,4)*(4780 - 10342*t2 + 5515*Power(t2,2) + 
                  182*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (409 + 913*t2 - 1537*Power(t2,2) - 43*Power(t2,3) + 
                  4*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-3309 + 7547*t2 - 546*Power(t2,2) - 
                  2711*Power(t2,3) + 69*Power(t2,4)) + 
               Power(t1,3)*(-2543 - 260*t2 + 11566*Power(t2,2) - 
                  11292*Power(t2,3) + 2559*Power(t2,4)) + 
               Power(t1,2)*(2673 - 17638*t2 + 32921*Power(t2,2) - 
                  22264*Power(t2,3) + 3501*Power(t2,4) + 
                  807*Power(t2,5)) + 
               Power(s2,5)*(513 + 189*Power(t1,2) - 1807*t2 + 
                  1228*Power(t2,2) + t1*(-3390 + 4174*t2)) - 
               Power(s2,4)*(4541 + 175*Power(t1,3) - 10309*t2 + 
                  5811*Power(t2,2) + 76*Power(t2,3) + 
                  Power(t1,2)*(-8571 + 10661*t2) + 
                  t1*(3341 - 10503*t2 + 6832*Power(t2,2))) + 
               Power(s2,3)*(4349 + 35*Power(t1,4) - 5833*t2 - 
                  3539*Power(t2,2) + 5842*Power(t2,3) - 
                  849*Power(t2,4) + 80*Power(t1,3)*(-143 + 180*t2) + 
                  2*Power(t1,2)*(4195 - 12214*t2 + 7689*Power(t2,2)) + 
                  t1*(19057 - 43831*t2 + 26290*Power(t2,2) - 
                     1024*Power(t2,3))) + 
               Power(s2,2)*(1890 + 63*Power(t1,5) + 
                  Power(t1,4)*(8484 - 10834*t2) - 15563*t2 + 
                  31353*Power(t2,2) - 23066*Power(t2,3) + 
                  5424*Power(t2,4) - 38*Power(t2,5) - 
                  2*Power(t1,3)*(5031 - 13955*t2 + 8594*Power(t2,2)) + 
                  Power(t1,2)*
                   (-29281 + 66987*t2 - 40452*Power(t2,2) + 
                     1984*Power(t2,3)) + 
                  2*t1*(-5855 + 6437*t2 + 8773*Power(t2,2) - 
                     11654*Power(t2,3) + 2344*Power(t2,4))) - 
               s2*(49*Power(t1,6) + Power(t1,5)*(3306 - 4298*t2) + 
                  Power(t1,4)*(-5753 + 15513*t2 - 9430*Power(t2,2)) + 
                  Power(t1,3)*
                   (-19545 + 43807*t2 - 25488*Power(t2,2) + 
                     702*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-3544 + 8071*t2 - 1190*Power(t2,2) - 
                     2399*Power(t2,3) + 112*Power(t2,4)) + 
                  Power(t1,2)*
                   (-9838 + 6485*t2 + 26065*Power(t2,2) - 
                     29118*Power(t2,3) + 6496*Power(t2,4)) + 
                  t1*(4382 - 32716*t2 + 64036*Power(t2,2) - 
                     45716*Power(t2,3) + 9376*Power(t2,4) + 
                     638*Power(t2,5)))) + 
            Power(s1,5)*(11*Power(s2,8) - 6*Power(t1,8) + 
               Power(s2,7)*(143 - 71*t1 - 184*t2) + 
               Power(t1,7)*(-46 + 163*t2) + 
               2*Power(t1,6)*(490 - 1297*t2 + 701*Power(t2,2)) + 
               Power(t1,5)*(880 + 362*t2 - 3391*Power(t2,2) + 
                  2169*Power(t2,3)) + 
               Power(t1,4)*(-6091 + 25902*t2 - 34368*Power(t2,2) + 
                  16017*Power(t2,3) - 1290*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (-1544 + 5138*t2 - 1081*Power(t2,2) - 
                  2610*Power(t2,3) + 75*Power(t2,4)) + 
               Power(t1,3)*(-9972 + 37209*t2 - 41488*Power(t2,2) + 
                  8041*Power(t2,3) + 8912*Power(t2,4) - 
                  2655*Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (-397 - 14012*t2 + 28912*Power(t2,2) - 
                  10644*Power(t2,3) - 2077*Power(t2,4) + 36*Power(t2,5)\
) + Power(t1,2)*(-5507 + 9932*t2 + 20376*Power(t2,2) - 
                  60126*Power(t2,3) + 45930*Power(t2,4) - 
                  10324*Power(t2,5) - 281*Power(t2,6)) + 
               Power(s2,6)*(708 + 189*Power(t1,2) - 1767*t2 + 
                  895*Power(t2,2) + 11*t1*(-89 + 122*t2)) - 
               Power(s2,5)*(1552 + 259*Power(t1,3) - 1820*t2 - 
                  1031*Power(t2,2) + 1367*Power(t2,3) + 
                  3*Power(t1,2)*(-930 + 1369*t2) + 
                  2*t1*(2593 - 6301*t2 + 3192*Power(t2,2))) + 
               Power(s2,4)*(-2700 + 175*Power(t1,4) + 17274*t2 - 
                  26922*Power(t2,2) + 13153*Power(t2,3) - 
                  624*Power(t2,4) + Power(t1,3)*(-4276 + 6851*t2) + 
                  2*Power(t1,2)*(7421 - 17894*t2 + 9123*Power(t2,2)) + 
                  t1*(7852 - 9300*t2 - 4737*Power(t2,2) + 
                     6477*Power(t2,3))) + 
               Power(s2,3)*(9923 - 21*Power(t1,5) + 
                  Power(t1,4)*(3779 - 6734*t2) - 43388*t2 + 
                  58750*Power(t2,2) - 25707*Power(t2,3) + 
                  111*Power(t2,4) + 264*Power(t2,5) + 
                  Power(t1,3)*
                   (-21470 + 52016*t2 - 26786*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (7365 - 7529*t2 - 6692*Power(t2,2) + 
                     7100*Power(t2,3)) + 
                  t1*(15305 - 82316*t2 + 123630*Power(t2,2) - 
                     63316*Power(t2,3) + 5984*Power(t2,4))) - 
               Power(s2,2)*(9084 + 49*Power(t1,6) + 
                  Power(t1,5)*(1899 - 3900*t2) - 25960*t2 + 
                  6523*Power(t2,2) + 38728*Power(t2,3) - 
                  38317*Power(t2,4) + 10374*Power(t2,5) - 
                  432*Power(t2,6) + 
                  Power(t1,4)*
                   (-16598 + 40835*t2 - 21297*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (6496 - 4567*t2 - 10036*Power(t2,2) + 
                     8303*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (9701 - 46776*t2 + 67514*Power(t2,2) - 
                     34570*Power(t2,3) + 3780*Power(t2,4)) + 
                  t1*(30855 - 129106*t2 + 168334*Power(t2,2) - 
                     66423*Power(t2,3) - 7493*Power(t2,4) + 
                     3692*Power(t2,5))) + 
               s2*(31*Power(t1,7) + Power(t1,6)*(488 - 1231*t2) - 
                  2*Power(t1,5)*(3236 - 8183*t2 + 4335*Power(t2,2)) + 
                  Power(t1,4)*
                   (-5442 + 1194*t2 + 13785*Power(t2,2) - 
                     9685*Power(t2,3)) + 
                  Power(t1,3)*
                   (22589 - 101188*t2 + 140202*Power(t2,2) - 
                     69564*Power(t2,3) + 7270*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (-539 - 14840*t2 + 30971*Power(t2,2) - 
                     12575*Power(t2,3) - 1256*Power(t2,4) + 
                     57*Power(t2,5)) + 
                  Power(t1,2)*
                   (31025 - 123222*t2 + 151076*Power(t2,2) - 
                     48209*Power(t2,3) - 17049*Power(t2,4) + 
                     6238*Power(t2,5)) - 
                  2*t1*(-7442 + 18819*t2 + 5178*Power(t2,2) - 
                     47984*Power(t2,3) + 41753*Power(t2,4) - 
                     10429*Power(t2,5) + 105*Power(t2,6)))) + 
            Power(-1 + t2,2)*
             (-1372 + 1960*t2 + 8015*Power(t2,2) - 23963*Power(t2,3) + 
               23657*Power(t2,4) - 7571*Power(t2,5) - 
               2200*Power(t2,6) + 1454*Power(t2,7) + 20*Power(t2,8) - 
               2*Power(t1,7)*
                (-57 + 71*t2 - 21*Power(t2,2) + Power(t2,3)) - 
               4*Power(s2,7)*
                (10 - 25*t2 + 15*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,6)*(-504 + 357*t2 + 232*Power(t2,2) - 
                  105*Power(t2,3) + 8*Power(t2,4)) - 
               Power(t1,5)*(38 - 2199*t2 + 2976*Power(t2,2) - 
                  755*Power(t2,3) + 14*Power(t2,4) + 10*Power(t2,5)) + 
               Power(t1,4)*(448 - 998*t2 - 2713*Power(t2,2) + 
                  4362*Power(t2,3) - 863*Power(t2,4) - 
                  24*Power(t2,5) + 4*Power(t2,6)) + 
               2*Power(t1,3)*
                (1175 - 6193*t2 + 11345*Power(t2,2) - 
                  7523*Power(t2,3) + 1321*Power(t2,4) - 
                  255*Power(t2,5) + 28*Power(t2,6)) + 
               Power(t1,2)*(1676 - 8775*t2 + 25204*Power(t2,2) - 
                  35600*Power(t2,3) + 20679*Power(t2,4) - 
                  3326*Power(t2,5) + 181*Power(t2,6) + 45*Power(t2,7)) \
+ t1*(-2178 + 5993*t2 - 330*Power(t2,2) - 15925*Power(t2,3) + 
                  18757*Power(t2,4) - 4489*Power(t2,5) - 
                  2203*Power(t2,6) + 363*Power(t2,7)) + 
               Power(s2,6)*(-66 + 55*t2 + 70*Power(t2,2) - 
                  127*Power(t2,3) + 56*Power(t2,4) + 
                  t1*(270 - 640*t2 + 450*Power(t2,2) + 4*Power(t2,3))) \
+ Power(s2,5)*(929 - 3494*t2 + 3765*Power(t2,2) - 1222*Power(t2,3) + 
                  166*Power(t2,4) - 60*Power(t2,5) + 
                  6*Power(t1,2)*
                   (-130 + 265*t2 - 190*Power(t2,2) + 13*Power(t2,3)) \
+ t1*(327 + 211*t2 - 594*Power(t2,2) + 263*Power(t2,3) - 
                     135*Power(t2,4))) + 
               Power(s2,4)*(809 - 703*t2 - 5212*Power(t2,2) + 
                  8308*Power(t2,3) - 3452*Power(t2,4) + 
                  451*Power(t2,5) + 15*Power(t2,6) - 
                  2*Power(t1,3)*
                   (-667 + 1051*t2 - 651*Power(t2,2) + 57*Power(t2,3)) \
+ Power(t1,2)*(-893 - 1842*t2 + 2769*Power(t2,2) - 230*Power(t2,3) + 
                     16*Power(t2,4)) + 
                  t1*(-4554 + 18211*t2 - 19929*Power(t2,2) + 
                     6521*Power(t2,3) - 899*Power(t2,4) + 
                     230*Power(t2,5))) + 
               Power(s2,3)*(-4554 + 20154*t2 - 33166*Power(t2,2) + 
                  21886*Power(t2,3) - 4826*Power(t2,4) + 
                  1026*Power(t2,5) - 316*Power(t2,6) + 
                  4*Power(t1,4)*
                   (-384 + 452*t2 - 177*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,3)*
                   (1881 + 3353*t2 - 5675*Power(t2,2) + 
                     487*Power(t2,3) + 194*Power(t2,4)) + 
                  Power(t1,2)*
                   (7882 - 34907*t2 + 38706*Power(t2,2) - 
                     11911*Power(t2,3) + 1342*Power(t2,4) - 
                     272*Power(t2,5)) + 
                  t1*(-2359 + 2124*t2 + 17433*Power(t2,2) - 
                     25974*Power(t2,3) + 8674*Power(t2,4) - 
                     692*Power(t2,5) - 70*Power(t2,6))) + 
               Power(s2,2)*(1659 - 20109*t2 + 67935*Power(t2,2) - 
                  97927*Power(t2,3) + 64152*Power(t2,4) - 
                  17366*Power(t2,5) + 1698*Power(t2,6) + 
                  42*Power(t2,7) + 
                  6*Power(t1,5)*
                   (199 - 202*t2 + 37*Power(t2,2) + 8*Power(t2,3)) - 
                  Power(t1,4)*
                   (2565 + 1942*t2 - 5301*Power(t2,2) + 
                     806*Power(t2,3) + 168*Power(t2,4)) + 
                  Power(t1,3)*
                   (-5856 + 31356*t2 - 35661*Power(t2,2) + 
                     9902*Power(t2,3) - 665*Power(t2,4) + 
                     84*Power(t2,5)) + 
                  Power(t1,2)*
                   (2366 - 1683*t2 - 24269*Power(t2,2) + 
                     33379*Power(t2,3) - 8823*Power(t2,4) + 
                     236*Power(t2,5) + 90*Power(t2,6)) + 
                  t1*(13846 - 60942*t2 + 100236*Power(t2,2) - 
                     66842*Power(t2,3) + 15757*Power(t2,4) - 
                     3360*Power(t2,5) + 693*Power(t2,6))) + 
               s2*(2635 + 1587*t2 - 39517*Power(t2,2) + 
                  89727*Power(t2,3) - 83753*Power(t2,4) + 
                  31758*Power(t2,5) - 1999*Power(t2,6) - 
                  426*Power(t2,7) - 
                  2*Power(t1,6)*
                   (278 - 299*t2 + 54*Power(t2,2) + 9*Power(t2,3)) + 
                  Power(t1,5)*
                   (1820 - 192*t2 - 2103*Power(t2,2) + 
                     518*Power(t2,3) + 29*Power(t2,4)) + 
                  Power(t1,4)*
                   (1637 - 13365*t2 + 16095*Power(t2,2) - 
                     4045*Power(t2,3) + 70*Power(t2,4) + 28*Power(t2,5)\
) + Power(t1,2)*(-11368 + 52062*t2 - 88086*Power(t2,2) + 
                     58925*Power(t2,3) - 13376*Power(t2,4) + 
                     2901*Power(t2,5) - 446*Power(t2,6)) + 
                  Power(t1,3)*
                   (-1264 + 1260*t2 + 14761*Power(t2,2) - 
                     20075*Power(t2,3) + 4464*Power(t2,4) + 
                     29*Power(t2,5) - 39*Power(t2,6)) - 
                  t1*(4848 - 33778*t2 + 96908*Power(t2,2) - 
                     130244*Power(t2,3) + 78601*Power(t2,4) - 
                     17753*Power(t2,5) + 1487*Power(t2,6) + 
                     99*Power(t2,7)))) + 
            Power(s1,4)*(2*Power(s2,9) + 2*Power(t1,9) + 
               Power(s2,8)*(-11 - 14*t1 + 10*t2) + 
               Power(t1,8)*(-60 + 19*t2) + 
               Power(t1,7)*(62 + 413*t2 - 293*Power(t2,2)) + 
               Power(s2,7)*(-22 + 40*Power(t1,2) + t1*(125 - 77*t2) - 
                  134*t2 + 74*Power(t2,2)) + 
               Power(t1,6)*(1247 - 4478*t2 + 4248*Power(t2,2) - 
                  1329*Power(t2,3)) + 
               Power(t1,5)*(2581 - 11001*t2 + 12034*Power(t2,2) - 
                  2335*Power(t2,3) - 1031*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (-1013 - 5269*t2 + 17033*Power(t2,2) - 
                  9004*Power(t2,3) - 1559*Power(t2,4) + 40*Power(t2,5)) \
+ t1*Power(-1 + t2,2)*(-5600 + 17123*t2 + 8385*Power(t2,2) - 
                  40748*Power(t2,3) + 18760*Power(t2,4) + 
                  439*Power(t2,5)) + 
               Power(t1,4)*(-2764 + 22108*t2 - 49264*Power(t2,2) + 
                  46460*Power(t2,3) - 18292*Power(t2,4) + 
                  1801*Power(t2,5)) + 
               Power(t1,3)*(-9307 + 64341*t2 - 146391*Power(t2,2) + 
                  143128*Power(t2,3) - 56412*Power(t2,4) + 
                  3503*Power(t2,5) + 1074*Power(t2,6)) + 
               Power(t1,2)*(-7702 + 54513*t2 - 119212*Power(t2,2) + 
                  94073*Power(t2,3) - 322*Power(t2,4) - 
                  30303*Power(t2,5) + 8944*Power(t2,6) + 9*Power(t2,7)) \
- Power(s2,6)*(-1331 + 56*Power(t1,3) + Power(t1,2)*(591 - 283*t2) + 
                  4640*t2 - 4311*Power(t2,2) + 1238*Power(t2,3) + 
                  t1*(193 - 1985*t2 + 1118*Power(t2,2))) + 
               Power(s2,5)*(-2950 + 28*Power(t1,4) + 
                  Power(t1,3)*(1525 - 629*t2) + 11453*t2 - 
                  13428*Power(t2,2) + 4133*Power(t2,3) + 
                  536*Power(t2,4) + 
                  3*Power(t1,2)*(387 - 2750*t2 + 1589*Power(t2,2)) + 
                  t1*(-8792 + 30192*t2 - 27906*Power(t2,2) + 
                     7998*Power(t2,3))) + 
               Power(s2,4)*(1156 + 28*Power(t1,5) + 4889*t2 - 
                  24072*Power(t2,2) + 30248*Power(t2,3) - 
                  12708*Power(t2,4) + 516*Power(t2,5) + 
                  5*Power(t1,4)*(-473 + 179*t2) + 
                  Power(t1,3)*(-2212 + 16243*t2 - 9661*Power(t2,2)) + 
                  Power(t1,2)*
                   (23591 - 80712*t2 + 74976*Power(t2,2) - 
                     21775*Power(t2,3)) + 
                  t1*(18787 - 69689*t2 + 81448*Power(t2,2) - 
                     28835*Power(t2,3) - 439*Power(t2,4))) + 
               Power(s2,3)*(2474 - 56*Power(t1,6) + 
                  Power(t1,5)*(2271 - 815*t2) - 46278*t2 + 
                  140094*Power(t2,2) - 162194*Power(t2,3) + 
                  76733*Power(t2,4) - 11365*Power(t2,5) + 
                  600*Power(t2,6) + 
                  2*Power(t1,4)*(934 - 8741*t2 + 5372*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-16289 + 55931*t2 - 52425*Power(t2,2) + 
                     15523*Power(t2,3)) + 
                  2*Power(t1,2)*
                   (-20620 + 75321*t2 - 86681*Power(t2,2) + 
                     30628*Power(t2,3) + 88*Power(t2,4)) - 
                  t1*(1831 + 34345*t2 - 121702*Power(t2,2) + 
                     143082*Power(t2,3) - 63460*Power(t2,4) + 
                     6040*Power(t2,5))) + 
               Power(s2,2)*(-8149 + 40*Power(t1,7) + 72837*t2 - 
                  185035*Power(t2,2) + 188100*Power(t2,3) - 
                  65555*Power(t2,4) - 7777*Power(t2,5) + 
                  5783*Power(t2,6) - 204*Power(t2,7) + 
                  Power(t1,6)*(-1325 + 457*t2) - 
                  3*Power(t1,5)*(195 - 3509*t2 + 2240*Power(t2,2)) + 
                  Power(t1,4)*
                   (24143 - 83634*t2 + 79089*Power(t2,2) - 
                     23898*Power(t2,3)) - 
                  2*Power(t1,3)*
                   (-20250 + 74515*t2 - 84063*Power(t2,2) + 
                     27437*Power(t2,3) + 1105*Power(t2,4)) + 
                  Power(t1,2)*
                   (-3233 + 79687*t2 - 227310*Power(t2,2) + 
                     247676*Power(t2,3) - 109015*Power(t2,4) + 
                     12429*Power(t2,5)) + 
                  t1*(-16280 + 166335*t2 - 446292*Power(t2,2) + 
                     490544*Power(t2,3) - 224074*Power(t2,4) + 
                     29353*Power(t2,5) + 222*Power(t2,6))) + 
               s2*(-14*Power(t1,8) + Power(t1,7)*(431 - 143*t2) + 
                  Power(t1,6)*(-79 - 3302*t2 + 2207*Power(t2,2)) + 
                  Power(t1,5)*
                   (-8942 + 31410*t2 - 29868*Power(t2,2) + 
                     9196*Power(t2,3)) + 
                  Power(t1,4)*
                   (-17678 + 67625*t2 - 74818*Power(t2,2) + 
                     20655*Power(t2,3) + 2968*Power(t2,4)) + 
                  Power(t1,3)*
                   (6672 - 72339*t2 + 178944*Power(t2,2) - 
                     181302*Power(t2,3) + 76555*Power(t2,4) - 
                     8706*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (7752 - 24081*t2 - 2342*Power(t2,2) + 
                     40262*Power(t2,3) - 20253*Power(t2,4) + 
                     303*Power(t2,5)) + 
                  Power(t1,2)*
                   (23537 - 186594*t2 + 456947*Power(t2,2) - 
                     475532*Power(t2,3) + 205367*Power(t2,4) - 
                     21553*Power(t2,5) - 1980*Power(t2,6)) + 
                  t1*(15856 - 128014*t2 + 307477*Power(t2,2) - 
                     287814*Power(t2,3) + 69982*Power(t2,4) + 
                     37166*Power(t2,5) - 14851*Power(t2,6) + 
                     198*Power(t2,7)))) - 
            s1*(-1 + t2)*(24*Power(s2,8)*(2 - 5*t2 + 3*Power(t2,2)) - 
               2*Power(t1,8)*
                (-35 + 53*t2 - 19*Power(t2,2) + Power(t2,3)) + 
               Power(t1,7)*(-146 - 58*t2 + 322*Power(t2,2) - 
                  99*Power(t2,3) + 8*Power(t2,4)) - 
               2*Power(t1,6)*
                (668 - 2159*t2 + 2028*Power(t2,2) - 504*Power(t2,3) + 
                  19*Power(t2,4) + 5*Power(t2,5)) + 
               Power(t1,5)*(1293 + 579*t2 - 8103*Power(t2,2) + 
                  7936*Power(t2,3) - 1402*Power(t2,4) - 
                  121*Power(t2,5) + 4*Power(t2,6)) + 
               Power(t1,4)*(4400 - 24949*t2 + 43932*Power(t2,2) - 
                  30436*Power(t2,3) + 8298*Power(t2,4) - 
                  1495*Power(t2,5) + 106*Power(t2,6)) + 
               Power(-1 + t2,2)*
                (-192 - 2419*t2 - 4125*Power(t2,2) + 
                  25663*Power(t2,3) - 28900*Power(t2,4) + 
                  9490*Power(t2,5) + 711*Power(t2,6)) + 
               Power(t1,3)*(6150 - 35432*t2 + 86553*Power(t2,2) - 
                  101362*Power(t2,3) + 54291*Power(t2,4) - 
                  10409*Power(t2,5) + 146*Power(t2,6) + 114*Power(t2,7)\
) + Power(t1,2)*(-1474 + 550*t2 + 19617*Power(t2,2) - 
                  43261*Power(t2,3) + 27096*Power(t2,4) + 
                  4070*Power(t2,5) - 8069*Power(t2,6) + 
                  1465*Power(t2,7)) + 
               t1*(-5589 + 19357*t2 - 28382*Power(t2,2) + 
                  38816*Power(t2,3) - 64305*Power(t2,4) + 
                  67936*Power(t2,5) - 32974*Power(t2,6) + 
                  5007*Power(t2,7) + 134*Power(t2,8)) + 
               Power(s2,7)*(80 - 130*t2 + 16*Power(t2,2) - 
                  7*Power(t2,3) + 14*Power(t2,4) + 
                  4*t1*(-94 + 224*t2 - 133*Power(t2,2) + 3*Power(t2,3))\
) - Power(s2,6)*(1473 - 5362*t2 + 6320*Power(t2,2) - 
                  2464*Power(t2,3) + 105*Power(t2,4) + 
                  42*Power(t2,5) + 
                  4*Power(t1,2)*
                   (-322 + 718*t2 - 407*Power(t2,2) + 11*Power(t2,3)) \
+ t1*(474 - 284*t2 - 658*Power(t2,2) + 215*Power(t2,3) + 
                     64*Power(t2,4))) + 
               Power(s2,5)*(-422 - 1525*t2 + 8350*Power(t2,2) - 
                  10198*Power(t2,3) + 4267*Power(t2,4) - 
                  679*Power(t2,5) + 21*Power(t2,6) + 
                  6*Power(t1,3)*
                   (-425 + 871*t2 - 453*Power(t2,2) + 7*Power(t2,3)) + 
                  Power(t1,2)*
                   (1436 + 183*t2 - 3357*Power(t2,2) + 
                     1024*Power(t2,3) + 147*Power(t2,4)) + 
                  t1*(8653 - 32051*t2 + 38563*Power(t2,2) - 
                     16341*Power(t2,3) + 1800*Power(t2,4) + 
                     60*Power(t2,5))) + 
               Power(s2,4)*(9597 - 40966*t2 + 64759*Power(t2,2) - 
                  45737*Power(t2,3) + 14576*Power(t2,4) - 
                  2613*Power(t2,5) + 240*Power(t2,6) + 
                  10*Power(t1,4)*
                   (323 - 601*t2 + 275*Power(t2,2) + 3*Power(t2,3)) - 
                  5*Power(t1,3)*
                   (550 + 206*t2 - 1370*Power(t2,2) + 
                     381*Power(t2,3) + 44*Power(t2,4)) + 
                  Power(t1,2)*
                   (-20399 + 76211*t2 - 91829*Power(t2,2) + 
                     39851*Power(t2,3) - 5594*Power(t2,4) + 
                     50*Power(t2,5)) + 
                  t1*(618 + 12964*t2 - 46478*Power(t2,2) + 
                     48963*Power(t2,3) - 17260*Power(t2,4) + 
                     2123*Power(t2,5))) - 
               Power(s2,3)*(9948 - 67847*t2 + 176783*Power(t2,2) - 
                  221326*Power(t2,3) + 138731*Power(t2,4) - 
                  41316*Power(t2,5) + 5148*Power(t2,6) - 
                  70*Power(t2,7) + 
                  4*Power(t1,5)*
                   (677 - 1145*t2 + 448*Power(t2,2) + 20*Power(t2,3)) \
- 5*Power(t1,4)*(664 + 232*t2 - 1510*Power(t2,2) + 385*Power(t2,3) + 
                     40*Power(t2,4)) + 
                  5*Power(t1,3)*
                   (-5069 + 18853*t2 - 22273*Power(t2,2) + 
                     9431*Power(t2,3) - 1414*Power(t2,4) + 
                     16*Power(t2,5)) + 
                  Power(t1,2)*
                   (1039 + 28078*t2 - 93050*Power(t2,2) + 
                     89724*Power(t2,3) - 26093*Power(t2,4) + 
                     2062*Power(t2,5) + 100*Power(t2,6)) + 
                  2*t1*(18711 - 82140*t2 + 132830*Power(t2,2) - 
                     97148*Power(t2,3) + 34171*Power(t2,4) - 
                     7522*Power(t2,5) + 810*Power(t2,6))) + 
               Power(s2,2)*(-1289 - 34031*t2 + 159688*Power(t2,2) - 
                  266898*Power(t2,3) + 200318*Power(t2,4) - 
                  60154*Power(t2,5) + 1145*Power(t2,6) + 
                  1215*Power(t2,7) + 
                  12*Power(t1,6)*
                   (123 - 193*t2 + 66*Power(t2,2) + 4*Power(t2,3)) - 
                  Power(t1,5)*
                   (2390 + 660*t2 - 4914*Power(t2,2) + 
                     1213*Power(t2,3) + 84*Power(t2,4)) - 
                  Power(t1,4)*
                   (18076 - 65609*t2 + 73951*Power(t2,2) - 
                     28809*Power(t2,3) + 4071*Power(t2,4) + 
                     30*Power(t2,5)) + 
                  Power(t1,3)*
                   (3205 + 23943*t2 - 88169*Power(t2,2) + 
                     81287*Power(t2,3) - 18876*Power(t2,4) + 
                     350*Power(t2,5) + 120*Power(t2,6)) + 
                  Power(t1,2)*
                   (49799 - 228597*t2 + 379014*Power(t2,2) - 
                     281348*Power(t2,3) + 101283*Power(t2,4) - 
                     23511*Power(t2,5) + 2496*Power(t2,6)) + 
                  t1*(34485 - 202750*t2 + 484476*Power(t2,2) - 
                     571487*Power(t2,3) + 337535*Power(t2,4) - 
                     91958*Power(t2,5) + 9708*Power(t2,6) + 
                     144*Power(t2,7))) + 
               s2*(3545 + 5819*t2 - 54820*Power(t2,2) + 
                  79329*Power(t2,3) - 9026*Power(t2,4) - 
                  59694*Power(t2,5) + 43302*Power(t2,6) - 
                  8458*Power(t2,7) + 3*Power(t2,8) - 
                  2*Power(t1,7)*
                   (239 - 361*t2 + 119*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(t1,6)*
                   (924 + 251*t2 - 1853*Power(t2,2) + 
                     490*Power(t2,3) - Power(t2,4)) + 
                  Power(t1,5)*
                   (7286 - 25184*t2 + 26228*Power(t2,2) - 
                     8636*Power(t2,3) + 938*Power(t2,4) + 
                     52*Power(t2,5)) + 
                  Power(t1,3)*
                   (-26374 + 130232*t2 - 222045*Power(t2,2) + 
                     163225*Power(t2,3) - 55815*Power(t2,4) + 
                     12575*Power(t2,5) - 1222*Power(t2,6)) + 
                  Power(t1,4)*
                   (-3655 - 7883*t2 + 41350*Power(t2,2) - 
                     38264*Power(t2,3) + 7178*Power(t2,4) + 
                     389*Power(t2,5) - 45*Power(t2,6)) + 
                  t1*(712 + 38952*t2 - 179528*Power(t2,2) + 
                     297518*Power(t2,3) - 211785*Power(t2,4) + 
                     48279*Power(t2,5) + 8857*Power(t2,6) - 
                     2993*Power(t2,7)) - 
                  Power(t1,2)*
                   (30512 - 170231*t2 + 395902*Power(t2,2) - 
                     455664*Power(t2,3) + 257174*Power(t2,4) - 
                     62887*Power(t2,5) + 5026*Power(t2,6) + 
                     321*Power(t2,7)))) + 
            Power(s1,3)*(-2*Power(t1,9)*(-6 + 5*t2) + 
               Power(s2,9)*(-6 + 8*t2) + 
               Power(s2,8)*(-115 + t1*(60 - 74*t2) + 225*t2 - 
                  107*Power(t2,2)) + 
               Power(t1,8)*(-118 + 101*t2 - 3*Power(t2,2)) + 
               Power(t1,7)*(-304 + 1249*t2 - 1077*Power(t2,2) + 
                  195*Power(t2,3)) + 
               Power(t1,6)*(1746 - 3714*t2 + 2701*Power(t2,2) - 
                  1442*Power(t2,3) + 573*Power(t2,4)) + 
               Power(t1,5)*(3752 - 23439*t2 + 39875*Power(t2,2) - 
                  25640*Power(t2,3) + 5601*Power(t2,4) + 27*Power(t2,5)\
) - Power(-1 + t2,3)*(-2139 + 11976*t2 - 6257*Power(t2,2) - 
                  15726*Power(t2,3) + 12020*Power(t2,4) + 
                  200*Power(t2,5)) + 
               Power(t1,4)*(2799 - 13614*t2 + 23013*Power(t2,2) - 
                  9626*Power(t2,3) - 8246*Power(t2,4) + 
                  6276*Power(t2,5) - 710*Power(t2,6)) + 
               t1*Power(-1 + t2,2)*
                (-94 + 30249*t2 - 81291*Power(t2,2) + 
                  54677*Power(t2,3) + 8521*Power(t2,4) - 
                  11135*Power(t2,5) + 14*Power(t2,6)) - 
               Power(t1,3)*(6114 - 47757*t2 + 138009*Power(t2,2) - 
                  198556*Power(t2,3) + 149963*Power(t2,4) - 
                  54219*Power(t2,5) + 6297*Power(t2,6) + 
                  126*Power(t2,7)) - 
               Power(t1,2)*(5820 - 62784*t2 + 221591*Power(t2,2) - 
                  362789*Power(t2,3) + 294179*Power(t2,4) - 
                  104070*Power(t2,5) + 5331*Power(t2,6) + 
                  2722*Power(t2,7)) + 
               Power(s2,7)*(370 - 1297*t2 + 1184*Power(t2,2) - 
                  268*Power(t2,3) + 8*Power(t1,2)*(-33 + 38*t2) + 
                  t1*(887 - 1592*t2 + 704*Power(t2,2))) + 
               Power(s2,6)*(1931 + Power(t1,3)*(672 - 728*t2) - 
                  6256*t2 + 7055*Power(t2,2) - 3581*Power(t2,3) + 
                  773*Power(t2,4) + 
                  Power(t1,2)*(-3085 + 5048*t2 - 2040*Power(t2,2)) + 
                  t1*(-2924 + 10402*t2 - 9654*Power(t2,2) + 
                     2305*Power(t2,3))) + 
               Power(s2,5)*(-7379 + 32601*t2 - 50959*Power(t2,2) + 
                  34194*Power(t2,3) - 8779*Power(t2,4) + 
                  178*Power(t2,5) + 28*Power(t1,4)*(-39 + 40*t2) + 
                  Power(t1,3)*(6263 - 9336*t2 + 3388*Power(t2,2)) + 
                  Power(t1,2)*
                   (9182 - 33510*t2 + 31575*Power(t2,2) - 
                     7790*Power(t2,3)) - 
                  2*t1*(5207 - 16543*t2 + 18322*Power(t2,2) - 
                     9288*Power(t2,3) + 2039*Power(t2,4))) + 
               Power(s2,4)*(5710 - 35043*t2 + 72614*Power(t2,2) - 
                  61912*Power(t2,3) + 19462*Power(t2,4) - 
                  1272*Power(t2,5) + 340*Power(t2,6) - 
                  28*Power(t1,5)*(-42 + 41*t2) - 
                  5*Power(t1,4)*(1607 - 2186*t2 + 698*Power(t2,2)) + 
                  Power(t1,3)*
                   (-15192 + 57209*t2 - 54517*Power(t2,2) + 
                     13665*Power(t2,3)) + 
                  Power(t1,2)*
                   (24805 - 76762*t2 + 84151*Power(t2,2) - 
                     43844*Power(t2,3) + 10190*Power(t2,4)) + 
                  t1*(39964 - 176796*t2 + 276373*Power(t2,2) - 
                     188066*Power(t2,3) + 52438*Power(t2,4) - 
                     3161*Power(t2,5))) + 
               Power(s2,3)*(-1246 - 1703*t2 + 45086*Power(t2,2) - 
                  121452*Power(t2,3) + 127907*Power(t2,4) - 
                  56101*Power(t2,5) + 7766*Power(t2,6) - 
                  280*Power(t2,7) + 56*Power(t1,6)*(-15 + 14*t2) + 
                  Power(t1,5)*(6605 - 8200*t2 + 2232*Power(t2,2)) + 
                  Power(t1,4)*
                   (14478 - 56381*t2 + 53958*Power(t2,2) - 
                     13480*Power(t2,3)) + 
                  Power(t1,3)*
                   (-32987 + 98044*t2 - 105840*Power(t2,2) + 
                     57328*Power(t2,3) - 14405*Power(t2,4)) + 
                  Power(t1,2)*
                   (-79835 + 360678*t2 - 567074*Power(t2,2) + 
                     386222*Power(t2,3) - 109338*Power(t2,4) + 
                     7779*Power(t2,5)) + 
                  t1*(-29015 + 149888*t2 - 283596*Power(t2,2) + 
                     225204*Power(t2,3) - 59502*Power(t2,4) - 
                     3492*Power(t2,5) + 924*Power(t2,6))) + 
               Power(s2,2)*(2274 + 35324*t2 - 213519*Power(t2,2) + 
                  436999*Power(t2,3) - 414151*Power(t2,4) + 
                  180584*Power(t2,5) - 27541*Power(t2,6) + 
                  30*Power(t2,7) - 8*Power(t1,7)*(-48 + 43*t2) + 
                  Power(t1,6)*(-3367 + 3792*t2 - 824*Power(t2,2)) + 
                  Power(t1,5)*
                   (-8012 + 32196*t2 - 30576*Power(t2,2) + 
                     7403*Power(t2,3)) + 
                  Power(t1,4)*
                   (25306 - 70580*t2 + 73229*Power(t2,2) - 
                     40965*Power(t2,3) + 11260*Power(t2,4)) + 
                  Power(t1,3)*
                   (73046 - 344811*t2 + 548740*Power(t2,2) - 
                     370662*Power(t2,3) + 102079*Power(t2,4) - 
                     6760*Power(t2,5)) + 
                  Power(t1,2)*
                   (43841 - 208087*t2 + 369666*Power(t2,2) - 
                     268622*Power(t2,3) + 47662*Power(t2,4) + 
                     18534*Power(t2,5) - 3621*Power(t2,6)) + 
                  t1*(-3326 + 54342*t2 - 243636*Power(t2,2) + 
                     469119*Power(t2,3) - 432381*Power(t2,4) + 
                     180703*Power(t2,5) - 25088*Power(t2,6) + 
                     336*Power(t2,7))) + 
               s2*(2*Power(t1,8)*(-51 + 44*t2) + 
                  Power(t1,7)*(965 - 968*t2 + 140*Power(t2,2)) + 
                  Power(t1,6)*
                   (2402 - 9868*t2 + 9107*Power(t2,2) - 
                     2030*Power(t2,3)) + 
                  Power(t1,5)*
                   (-10387 + 26182*t2 - 24652*Power(t2,2) + 
                     13928*Power(t2,3) - 4313*Power(t2,4)) + 
                  Power(t1,4)*
                   (-29548 + 151767*t2 - 246955*Power(t2,2) + 
                     163952*Power(t2,3) - 42001*Power(t2,4) + 
                     1937*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (-878 + 43663*t2 - 114839*Power(t2,2) + 
                     84753*Power(t2,3) - 1652*Power(t2,4) - 
                     10325*Power(t2,5) + 219*Power(t2,6)) + 
                  Power(t1,3)*
                   (-23335 + 106856*t2 - 181697*Power(t2,2) + 
                     114956*Power(t2,3) + 624*Power(t2,4) - 
                     20046*Power(t2,5) + 3067*Power(t2,6)) + 
                  Power(t1,2)*
                   (10672 - 101064*t2 + 339898*Power(t2,2) - 
                     552544*Power(t2,3) + 460245*Power(t2,4) - 
                     181395*Power(t2,5) + 24038*Power(t2,6) + 
                     81*Power(t2,7)) + 
                  t1*(4215 - 100630*t2 + 439176*Power(t2,2) - 
                     804520*Power(t2,3) + 713095*Power(t2,4) - 
                     287436*Power(t2,5) + 33188*Power(t2,6) + 
                     2912*Power(t2,7)))) - 
            Power(s1,2)*(-6*Power(t1,9)*(2 - 3*t2 + Power(t2,2)) + 
               6*Power(s2,9)*(2 - 5*t2 + 3*Power(t2,2)) + 
               Power(t1,8)*(-19 + 102*t2 - 102*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,7)*(718 - 1803*t2 + 1410*Power(t2,2) - 
                  322*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,6)*(-131 - 2935*t2 + 7094*Power(t2,2) - 
                  4872*Power(t2,3) + 723*Power(t2,4) + 99*Power(t2,5)) + 
               Power(t1,5)*(-3662 + 17616*t2 - 28251*Power(t2,2) + 
                  20445*Power(t2,3) - 7694*Power(t2,4) + 
                  1614*Power(t2,5) - 50*Power(t2,6)) + 
               Power(-1 + t2,3)*
                (941 + 9290*t2 - 37393*Power(t2,2) + 
                  37670*Power(t2,3) - 5695*Power(t2,4) - 
                  5252*Power(t2,5) + 19*Power(t2,6)) - 
               t1*Power(-1 + t2,2)*
                (-804 + 13464*t2 - 69478*Power(t2,2) + 
                  123889*Power(t2,3) - 84263*Power(t2,4) + 
                  14353*Power(t2,5) + 2297*Power(t2,6)) + 
               Power(t1,4)*(-7460 + 48233*t2 - 111745*Power(t2,2) + 
                  118248*Power(t2,3) - 58154*Power(t2,4) + 
                  10925*Power(t2,5) + 15*Power(t2,6) - 69*Power(t2,7)) - 
               Power(t1,3)*(988 - 7941*t2 + 27575*Power(t2,2) - 
                  33786*Power(t2,3) + 2251*Power(t2,4) + 
                  21315*Power(t2,5) - 12195*Power(t2,6) + 
                  1792*Power(t2,7)) + 
               Power(t1,2)*(6899 - 43733*t2 + 135769*Power(t2,2) - 
                  256400*Power(t2,3) + 299473*Power(t2,4) - 
                  204366*Power(t2,5) + 70738*Power(t2,6) - 
                  8136*Power(t2,7) - 244*Power(t2,8)) - 
               3*Power(s2,8)*
                (-4 + 15*t2 - 20*Power(t2,2) + 10*Power(t2,3) + 
                  t1*(36 - 86*t2 + 50*Power(t2,2))) + 
               Power(s2,7)*(-705 + 2396*t2 - 2823*Power(t2,2) + 
                  1326*Power(t2,3) - 207*Power(t2,4) + 
                  24*Power(t1,2)*(18 - 41*t2 + 23*Power(t2,2)) + 
                  t1*(-65 + 165*t2 - 222*Power(t2,2) + 146*Power(t2,3))) \
+ Power(s2,6)*(682 - 3846*t2 + 6608*Power(t2,2) - 4236*Power(t2,3) + 
                  692*Power(t2,4) + 78*Power(t2,5) - 
                  168*Power(t1,3)*(6 - 13*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*
                   (199 - 183*t2 + 210*Power(t2,2) - 310*Power(t2,3)) + 
                  t1*(4804 - 16135*t2 + 18864*Power(t2,2) - 
                     8866*Power(t2,3) + 1424*Power(t2,4))) + 
               Power(s2,5)*(7127 - 29998*t2 + 46679*Power(t2,2) - 
                  34093*Power(t2,3) + 12099*Power(t2,4) - 
                  1724*Power(t2,5) - 108*Power(t2,6) + 
                  84*Power(t1,4)*(18 - 37*t2 + 19*Power(t2,2)) + 
                  Power(t1,3)*
                   (-421 - 87*t2 + 282*Power(t2,2) + 394*Power(t2,3)) + 
                  Power(t1,2)*
                   (-14068 + 46398*t2 - 53535*Power(t2,2) + 
                     25102*Power(t2,3) - 4170*Power(t2,4)) + 
                  t1*(-5093 + 28578*t2 - 49304*Power(t2,2) + 
                     32966*Power(t2,3) - 7083*Power(t2,4) + 
                     68*Power(t2,5))) + 
               Power(s2,4)*(-13976 + 79560*t2 - 178964*Power(t2,2) + 
                  199463*Power(t2,3) - 114591*Power(t2,4) + 
                  31953*Power(t2,5) - 3602*Power(t2,6) + 
                  150*Power(t2,7) - 
                  84*Power(t1,5)*(18 - 35*t2 + 17*Power(t2,2)) - 
                  15*Power(t1,4)*
                   (-37 - 37*t2 + 66*Power(t2,2) + 22*Power(t2,3)) + 
                  5*Power(t1,3)*
                   (4610 - 14793*t2 + 16674*Power(t2,2) - 
                     7718*Power(t2,3) + 1318*Power(t2,4)) + 
                  Power(t1,2)*
                   (12461 - 74822*t2 + 131560*Power(t2,2) - 
                     88556*Power(t2,3) + 19445*Power(t2,4) - 
                     418*Power(t2,5)) + 
                  t1*(-33747 + 146024*t2 - 233238*Power(t2,2) + 
                     177745*Power(t2,3) - 69819*Power(t2,4) + 
                     13650*Power(t2,5) - 525*Power(t2,6))) + 
               Power(s2,3)*(4545 - 57988*t2 + 193055*Power(t2,2) - 
                  276460*Power(t2,3) + 184776*Power(t2,4) - 
                  49947*Power(t2,5) + 1496*Power(t2,6) + 
                  522*Power(t2,7) + 
                  168*Power(t1,6)*(6 - 11*t2 + 5*Power(t2,2)) + 
                  3*Power(t1,5)*
                   (-129 - 315*t2 + 450*Power(t2,2) + 50*Power(t2,3)) - 
                  5*Power(t1,4)*
                   (4585 - 14144*t2 + 15321*Power(t2,2) - 
                     6850*Power(t2,3) + 1179*Power(t2,4)) - 
                  Power(t1,3)*
                   (13514 - 94393*t2 + 172030*Power(t2,2) - 
                     115104*Power(t2,3) + 23480*Power(t2,4) + 
                     33*Power(t2,5)) + 
                  Power(t1,2)*
                   (62566 - 278949*t2 + 455946*Power(t2,2) - 
                     357842*Power(t2,3) + 148502*Power(t2,4) - 
                     32475*Power(t2,5) + 2072*Power(t2,6)) + 
                  t1*(61447 - 335817*t2 + 730738*Power(t2,2) - 
                     792538*Power(t2,3) + 444996*Power(t2,4) - 
                     121256*Power(t2,5) + 12638*Power(t2,6) - 
                     180*Power(t2,7))) + 
               Power(s2,2)*(789 + 13753*t2 - 32218*Power(t2,2) - 
                  41404*Power(t2,3) + 183971*Power(t2,4) - 
                  202406*Power(t2,5) + 91667*Power(t2,6) - 
                  14446*Power(t2,7) + 294*Power(t2,8) - 
                  24*Power(t1,7)*(18 - 31*t2 + 13*Power(t2,2)) + 
                  Power(t1,6)*
                   (85 + 915*t2 - 1098*Power(t2,2) + 14*Power(t2,3)) + 
                  Power(t1,5)*
                   (13900 - 40641*t2 + 41268*Power(t2,2) - 
                     17086*Power(t2,3) + 2832*Power(t2,4)) + 
                  Power(t1,4)*
                   (6476 - 62337*t2 + 120650*Power(t2,2) - 
                     80096*Power(t2,3) + 14220*Power(t2,4) + 
                     757*Power(t2,5)) + 
                  Power(t1,3)*
                   (-56061 + 257434*t2 - 427145*Power(t2,2) + 
                     339266*Power(t2,3) - 143637*Power(t2,4) + 
                     32510*Power(t2,5) - 2187*Power(t2,6)) + 
                  t1*(-17813 + 152807*t2 - 451112*Power(t2,2) + 
                     600572*Power(t2,3) - 360230*Power(t2,4) + 
                     64013*Power(t2,5) + 16092*Power(t2,6) - 
                     4326*Power(t2,7)) - 
                  Power(t1,2)*
                   (88488 - 482511*t2 + 1040523*Power(t2,2) - 
                     1109810*Power(t2,3) + 606153*Power(t2,4) - 
                     157050*Power(t2,5) + 14099*Power(t2,6) + 
                     150*Power(t2,7))) + 
               s2*(6*Power(t1,8)*(18 - 29*t2 + 11*Power(t2,2)) + 
                  Power(t1,7)*
                   (41 - 477*t2 + 510*Power(t2,2) - 50*Power(t2,3)) + 
                  Power(t1,6)*
                   (-4774 + 13030*t2 - 11949*Power(t2,2) + 
                     4186*Power(t2,3) - 584*Power(t2,4)) - 
                  Power(t1,5)*
                   (881 - 20969*t2 + 44578*Power(t2,2) - 
                     29690*Power(t2,3) + 4517*Power(t2,4) + 
                     551*Power(t2,5)) + 
                  Power(t1,4)*
                   (23777 - 112127*t2 + 186009*Power(t2,2) - 
                     145521*Power(t2,3) + 60549*Power(t2,4) - 
                     13575*Power(t2,5) + 798*Power(t2,6)) + 
                  Power(-1 + t2,2)*
                   (2677 + 4731*t2 - 78250*Power(t2,2) + 
                     163474*Power(t2,3) - 120353*Power(t2,4) + 
                     25935*Power(t2,5) + 1244*Power(t2,6)) + 
                  Power(t1,3)*
                   (48477 - 274487*t2 + 600494*Power(t2,2) - 
                     634983*Power(t2,3) + 333902*Power(t2,4) - 
                     78672*Power(t2,5) + 5048*Power(t2,6) + 
                     249*Power(t2,7)) + 
                  Power(t1,2)*
                   (14624 - 105084*t2 + 291307*Power(t2,2) - 
                     364583*Power(t2,3) + 181305*Power(t2,4) + 
                     6941*Power(t2,5) - 30234*Power(t2,6) + 
                     5721*Power(t2,7)) + 
                  t1*(-7901 + 33085*t2 - 114242*Power(t2,2) + 
                     314426*Power(t2,3) - 497744*Power(t2,4) + 
                     414671*Power(t2,5) - 165398*Power(t2,6) + 
                     23076*Power(t2,7) + 27*Power(t2,8))))) + 
         Power(s,3)*(2*Power(s1,10)*(s2 - t1)*(-3 + 2*s2 - 2*t1 + 3*t2) + 
            Power(s1,9)*(31*Power(s2,3) - 34*Power(t1,3) + 
               2*Power(-1 + t2,2)*(3 + t2) + Power(t1,2)*(-98 + 77*t2) + 
               Power(s2,2)*(-116 - 96*t1 + 95*t2) + 
               t1*(-45 + 56*t2 - 11*Power(t2,2)) + 
               s2*(53 + 99*Power(t1,2) + t1*(214 - 172*t2) - 72*t2 + 
                  19*Power(t2,2))) + 
            Power(s1,8)*(55*Power(s2,4) + 55*Power(t1,4) + 
               Power(t1,3)*(573 - 539*t2) + 
               Power(s2,3)*(-606 - 227*t1 + 575*t2) + 
               Power(-1 + t2,2)*(-85 + 40*t2 + Power(t2,2)) + 
               2*Power(s2,2)*
                (281 + 172*Power(t1,2) + t1*(871 - 823*t2) - 325*t2 + 
                  77*Power(t2,2)) + 
               Power(t1,2)*(642 - 922*t2 + 346*Power(t2,2)) + 
               t1*(2 + 96*t2 - 91*Power(t2,2) - 7*Power(t2,3)) - 
               s2*(-4 + 227*Power(t1,3) + Power(t1,2)*(1709 - 1610*t2) + 
                  128*t2 - 137*Power(t2,2) + 13*Power(t2,3) + 
                  2*t1*(593 - 768*t2 + 241*Power(t2,2)))) - 
            Power(s1,7)*(10*Power(s2,5) + 21*Power(t1,5) + 
               Power(s2,4)*(-22*t1 + 25*(47 - 59*t2)) + 
               Power(t1,4)*(1461 - 1834*t2) + 
               Power(t1,3)*(1661 - 1940*t2 + 279*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (122 + 294*t2 - 319*Power(t2,2) + 25*Power(t2,3)) + 
               Power(t1,2)*(-857 + 4001*t2 - 4301*Power(t2,2) + 
                  1297*Power(t2,3)) + 
               t1*(-1203 + 3666*t2 - 3376*Power(t2,2) + 
                  851*Power(t2,3) + 62*Power(t2,4)) + 
               Power(s2,3)*(-9*Power(t1,2) + t1*(-4800 + 6044*t2) + 
                  3*(-487 + 421*t2 + 65*Power(t2,2))) + 
               Power(s2,2)*(-722 + 65*Power(t1,3) + 
                  Power(t1,2)*(7568 - 9529*t2) + 3040*t2 - 
                  2488*Power(t2,2) + 310*Power(t2,3) + 
                  t1*(4783 - 5067*t2 + 290*Power(t2,2))) + 
               s2*(1312 - 65*Power(t1,4) - 3946*t2 + 3676*Power(t2,2) - 
                  1047*Power(t2,3) + 5*Power(t2,4) + 
                  Power(t1,3)*(-5404 + 6794*t2) + 
                  Power(t1,2)*(-4991 + 5760*t2 - 772*Power(t2,2)) - 
                  2*t1*(-733 + 3341*t2 - 3205*Power(t2,2) + 
                     737*Power(t2,3)))) + 
            Power(s1,6)*(-106*Power(s2,6) + 3*Power(t1,6) + 
               Power(t1,5)*(1513 - 2352*t2) + 
               Power(s2,5)*(-1057 + 558*t1 + 1724*t2) + 
               Power(t1,4)*(-92 + 2749*t2 - 2970*Power(t2,2)) + 
               Power(s2,4)*(705 - 1177*Power(t1,2) + 
                  t1*(5815 - 9278*t2) + 857*t2 - 1820*Power(t2,2)) + 
               Power(t1,3)*(-5575 + 17847*t2 - 15175*Power(t2,2) + 
                  2849*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-877 + 2904*t2 - 1216*Power(t2,2) - 570*Power(t2,3) + 
                  47*Power(t2,4)) + 
               Power(t1,2)*(-5173 + 12451*t2 - 4672*Power(t2,2) - 
                  5067*Power(t2,3) + 2608*Power(t2,4)) - 
               t1*(780 + 3141*t2 - 14010*Power(t2,2) + 
                  14864*Power(t2,3) - 4816*Power(t2,4) + 41*Power(t2,5)) \
+ Power(s2,3)*(3895 + 1240*Power(t1,3) - 12766*t2 + 9193*Power(t2,2) - 
                  235*Power(t2,3) + 2*Power(t1,2)*(-6391 + 10011*t2) + 
                  t1*(-2909 - 2892*t2 + 6828*Power(t2,2))) + 
               Power(s2,2)*(-5160 - 648*Power(t1,4) + 
                  Power(t1,3)*(13860 - 21458*t2) + 12989*t2 - 
                  7482*Power(t2,2) - 459*Power(t2,3) + 
                  259*Power(t2,4) + 
                  Power(t1,2)*(3453 + 6386*t2 - 11432*Power(t2,2)) + 
                  t1*(-13089 + 43715*t2 - 35483*Power(t2,2) + 
                     4629*Power(t2,3))) + 
               s2*(922 + 130*Power(t1,5) + 2863*t2 - 13736*Power(t2,2) + 
                  14838*Power(t2,3) - 5184*Power(t2,4) + 
                  297*Power(t2,5) + Power(t1,4)*(-7349 + 11342*t2) + 
                  Power(t1,3)*(-1157 - 7100*t2 + 9394*Power(t2,2)) + 
                  Power(t1,2)*
                   (14871 - 49094*t2 + 41755*Power(t2,2) - 
                     7337*Power(t2,3)) + 
                  t1*(10238 - 25584*t2 + 12968*Power(t2,2) + 
                     4710*Power(t2,3) - 2626*Power(t2,4)))) + 
            Power(s1,5)*(-95*Power(s2,7) - 21*Power(t1,7) + 
               Power(s2,6)*(-435 + 574*t1 + 881*t2) + 
               Power(t1,6)*(-383 + 1126*t2) + 
               Power(t1,5)*(2076 - 7150*t2 + 4639*Power(t2,2)) + 
               Power(t1,4)*(6073 - 15575*t2 + 8253*Power(t2,2) + 
                  1496*Power(t2,3)) + 
               Power(t1,3)*(484 + 14290*t2 - 38416*Power(t2,2) + 
                  29713*Power(t2,3) - 5646*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-1072 - 1482*t2 + 9662*Power(t2,2) - 
                  6463*Power(t2,3) - 102*Power(t2,4) + 19*Power(t2,5)) - 
               Power(t1,2)*(6205 - 43090*t2 + 81603*Power(t2,2) - 
                  53971*Power(t2,3) + 7080*Power(t2,4) + 
                  2166*Power(t2,5)) + 
               t1*(-4393 + 24929*t2 - 36911*Power(t2,2) + 
                  7730*Power(t2,3) + 16679*Power(t2,4) - 
                  8209*Power(t2,5) + 175*Power(t2,6)) - 
               Power(s2,5)*(554 + 1422*Power(t1,2) - 3478*t2 + 
                  2565*Power(t2,2) + 4*t1*(-763 + 1493*t2)) + 
               Power(s2,4)*(4089 + 1827*Power(t1,3) - 11983*t2 + 
                  6559*Power(t2,2) + 1654*Power(t2,3) + 
                  3*Power(t1,2)*(-2795 + 5456*t2) + 
                  t1*(4240 - 20490*t2 + 14277*Power(t2,2))) - 
               Power(s2,3)*(3207 + 1243*Power(t1,4) + 2362*t2 - 
                  20120*Power(t2,2) + 15643*Power(t2,3) - 
                  596*Power(t2,4) + 8*Power(t1,3)*(-1447 + 2901*t2) + 
                  10*Power(t1,2)*(1219 - 4952*t2 + 3314*Power(t2,2)) + 
                  4*t1*(5085 - 14713*t2 + 9498*Power(t2,2) + 
                     417*Power(t2,3))) + 
               Power(s2,2)*(-3279 + 372*Power(t1,5) + 34624*t2 - 
                  73179*Power(t2,2) + 52597*Power(t2,3) - 
                  11496*Power(t2,4) + 740*Power(t2,5) + 
                  Power(t1,4)*(-8413 + 17921*t2) + 
                  4*Power(t1,3)*(3988 - 14658*t2 + 9587*Power(t2,2)) + 
                  Power(t1,2)*
                   (34762 - 97410*t2 + 63768*Power(t2,2) + 
                     466*Power(t2,3)) + 
                  t1*(8158 + 14518*t2 - 75128*Power(t2,2) + 
                     62925*Power(t2,3) - 9056*Power(t2,4))) + 
               s2*(5555 + 8*Power(t1,6) + Power(t1,5)*(2988 - 7116*t2) - 
                  30521*t2 + 47164*Power(t2,2) - 17546*Power(t2,3) - 
                  11497*Power(t2,4) + 7269*Power(t2,5) - 
                  424*Power(t2,6) + 
                  Power(t1,4)*(-9524 + 33274*t2 - 21559*Power(t2,2)) - 
                  4*Power(t1,3)*
                   (6146 - 16529*t2 + 10147*Power(t2,2) + 
                     487*Power(t2,3)) + 
                  Power(t1,2)*
                   (-5366 - 26982*t2 + 94532*Power(t2,2) - 
                     77879*Power(t2,3) + 14349*Power(t2,4)) + 
                  2*t1*(4466 - 37781*t2 + 76181*Power(t2,2) - 
                     53035*Power(t2,3) + 9496*Power(t2,4) + 
                     666*Power(t2,5)))) - 
            Power(s1,4)*(29*Power(s2,8) - 18*Power(t1,8) + 
               Power(s2,7)*(19 - 189*t1 - 117*t2) + 
               3*Power(t1,7)*(72 + 37*t2) + 
               Power(t1,6)*(583 - 3181*t2 + 1975*Power(t2,2)) + 
               Power(t1,5)*(-1069 + 4106*t2 - 7006*Power(t2,2) + 
                  3991*Power(t2,3)) + 
               Power(t1,4)*(-8059 + 42767*t2 - 59623*Power(t2,2) + 
                  26789*Power(t2,3) - 1540*Power(t2,4)) + 
               Power(t1,3)*(-8361 + 39369*t2 - 47014*Power(t2,2) + 
                  2848*Power(t2,3) + 18012*Power(t2,4) - 
                  4336*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (1959 - 15597*t2 + 20102*Power(t2,2) + 
                  1909*Power(t2,3) - 8023*Power(t2,4) + 232*Power(t2,5)) \
- Power(t1,2)*(535 + 16376*t2 - 88117*Power(t2,2) + 
                  148216*Power(t2,3) - 97443*Power(t2,4) + 
                  19654*Power(t2,5) + 585*Power(t2,6)) + 
               t1*(-171 - 25766*t2 + 114471*Power(t2,2) - 
                  175325*Power(t2,3) + 105665*Power(t2,4) - 
                  14312*Power(t2,5) - 4628*Power(t2,6) + 66*Power(t2,7)) \
+ Power(s2,6)*(259 + 503*Power(t1,2) - 1897*t2 + 1128*Power(t2,2) + 
                  t1*(-167 + 1080*t2)) - 
               Power(s2,5)*(-985 + 677*Power(t1,3) + 3540*t2 - 
                  5092*Power(t2,2) + 2861*Power(t2,3) + 
                  12*Power(t1,2)*(-24 + 311*t2) + 
                  2*t1*(1626 - 7573*t2 + 4318*Power(t2,2))) + 
               Power(s2,4)*(-4796 + 425*Power(t1,4) + 30069*t2 - 
                  45247*Power(t2,2) + 19915*Power(t2,3) + 
                  285*Power(t2,4) + Power(t1,3)*(354 + 6579*t2) + 
                  Power(t1,2)*(11527 - 46167*t2 + 26065*Power(t2,2)) + 
                  t1*(-2059 + 11440*t2 - 20330*Power(t2,2) + 
                     12411*Power(t2,3))) + 
               Power(s2,3)*(5745 + Power(t1,5) - 43195*t2 + 
                  70326*Power(t2,2) - 30828*Power(t2,3) - 
                  1985*Power(t2,4) - 636*Power(t2,5) - 
                  Power(t1,4)*(1621 + 6501*t2) - 
                  14*Power(t1,3)*(1303 - 4984*t2 + 2831*Power(t2,2)) + 
                  Power(t1,2)*
                   (2058 - 20684*t2 + 42444*Power(t2,2) - 
                     26284*Power(t2,3)) + 
                  t1*(24935 - 142256*t2 + 212250*Power(t2,2) - 
                     103352*Power(t2,3) + 7424*Power(t2,4))) + 
               Power(s2,2)*(-5303 - 171*Power(t1,6) + 13131*t2 + 
                  31188*Power(t2,2) - 100082*Power(t2,3) + 
                  78399*Power(t2,4) - 18219*Power(t2,5) + 
                  1080*Power(t2,6) + 3*Power(t1,5)*(655 + 1202*t2) + 
                  Power(t1,4)*(14199 - 55331*t2 + 31952*Power(t2,2)) + 
                  Power(t1,3)*
                   (-2948 + 25314*t2 - 51272*Power(t2,2) + 
                     30770*Power(t2,3)) + 
                  Power(t1,2)*
                   (-44725 + 241172*t2 - 352546*Power(t2,2) + 
                     174460*Power(t2,3) - 16707*Power(t2,4)) + 
                  t1*(-22767 + 137290*t2 - 206914*Power(t2,2) + 
                     78358*Power(t2,3) + 20869*Power(t2,4) - 
                     5172*Power(t2,5))) + 
               s2*(1303 + 97*Power(t1,7) + 26346*t2 - 
                  128416*Power(t2,2) + 203303*Power(t2,3) - 
                  130267*Power(t2,4) + 25600*Power(t2,5) + 
                  2267*Power(t2,6) - 136*Power(t2,7) - 
                  2*Power(t1,6)*(527 + 513*t2) - 
                  2*Power(t1,5)*(2537 - 10827*t2 + 6425*Power(t2,2)) + 
                  Power(t1,4)*
                   (3033 - 16636*t2 + 31072*Power(t2,2) - 
                     18027*Power(t2,3)) + 
                  Power(t1,3)*
                   (32645 - 171752*t2 + 245166*Power(t2,2) - 
                     117812*Power(t2,3) + 10538*Power(t2,4)) + 
                  Power(t1,2)*
                   (25894 - 135184*t2 + 185370*Power(t2,2) - 
                     50598*Power(t2,3) - 37435*Power(t2,4) + 
                     10344*Power(t2,5)) + 
                  t1*(5348 + 3770*t2 - 117636*Power(t2,2) + 
                     245832*Power(t2,3) - 175506*Power(t2,4) + 
                     38238*Power(t2,5) - 434*Power(t2,6)))) - 
            Power(s1,3)*(2*Power(s2,9) + 4*Power(t1,9) + 
               7*Power(t1,8)*(-14 + 5*t2) + 
               Power(s2,8)*(-34 - 12*t1 + 35*t2) + 
               Power(s2,7)*(-188 + 24*Power(t1,2) + t1*(312 - 252*t2) + 
                  69*t2 - 11*Power(t2,2)) - 
               4*Power(t1,7)*(-45 - 63*t2 + 49*Power(t2,2)) + 
               Power(t1,6)*(1902 - 4612*t2 + 3823*Power(t2,2) - 
                  1278*Power(t2,3)) + 
               Power(t1,5)*(863 - 12496*t2 + 15935*Power(t2,2) - 
                  3381*Power(t2,3) - 1313*Power(t2,4)) + 
               Power(t1,4)*(-3240 + 28209*t2 - 59898*Power(t2,2) + 
                  54698*Power(t2,3) - 20667*Power(t2,4) + 
                  1652*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (1064 + 5064*t2 - 40392*Power(t2,2) + 
                  57616*Power(t2,3) - 20286*Power(t2,4) - 
                  3051*Power(t2,5) + 73*Power(t2,6)) + 
               Power(t1,3)*(-10730 + 84294*t2 - 195836*Power(t2,2) + 
                  190136*Power(t2,3) - 73311*Power(t2,4) + 
                  4252*Power(t2,5) + 1165*Power(t2,6)) + 
               Power(t1,2)*(-3848 + 50228*t2 - 130933*Power(t2,2) + 
                  109052*Power(t2,3) + 6981*Power(t2,4) - 
                  43503*Power(t2,5) + 11796*Power(t2,6) + 27*Power(t2,7)\
) + t1*(2695 + 1658*t2 + 8623*Power(t2,2) - 99757*Power(t2,3) + 
                  184078*Power(t2,4) - 126416*Power(t2,5) + 
                  28415*Power(t2,6) + 704*Power(t2,7)) + 
               Power(s2,6)*(1871 - 5857*t2 + 5162*Power(t2,2) - 
                  1428*Power(t2,3) + 12*Power(t1,2)*(-108 + 71*t2) + 
                  t1*(846 + 875*t2 - 682*Power(t2,2))) + 
               Power(s2,5)*(-1924 - 84*Power(t1,4) + 10858*t2 - 
                  13293*Power(t2,2) + 3323*Power(t2,3) + 
                  1143*Power(t2,4) - 4*Power(t1,3)*(-772 + 435*t2) + 
                  Power(t1,2)*(-1788 - 5454*t2 + 3761*Power(t2,2)) + 
                  2*t1*(-5634 + 17644*t2 - 15657*Power(t2,2) + 
                     4379*Power(t2,3))) + 
               Power(s2,4)*(-6162 + 168*Power(t1,5) + 26519*t2 - 
                  46156*Power(t2,2) + 40228*Power(t2,3) - 
                  13419*Power(t2,4) - 365*Power(t2,5) + 
                  10*Power(t1,4)*(-454 + 229*t2) + 
                  Power(t1,3)*(2632 + 11938*t2 - 8200*Power(t2,2)) + 
                  Power(t1,2)*
                   (29096 - 90418*t2 + 81351*Power(t2,2) - 
                     23530*Power(t2,3)) + 
                  t1*(16239 - 76190*t2 + 92083*Power(t2,2) - 
                     31387*Power(t2,3) - 1540*Power(t2,4))) + 
               Power(s2,3)*(7720 - 168*Power(t1,6) + 
                  Power(t1,5)*(4184 - 1940*t2) - 72907*t2 + 
                  195375*Power(t2,2) - 215584*Power(t2,3) + 
                  97959*Power(t2,4) - 13592*Power(t2,5) + 
                  1100*Power(t2,6) + 
                  Power(t1,4)*(-2988 - 13127*t2 + 9215*Power(t2,2)) + 
                  4*Power(t1,3)*
                   (-10073 + 30677*t2 - 27903*Power(t2,2) + 
                     8400*Power(t2,3)) + 
                  Power(t1,2)*
                   (-38142 + 175210*t2 - 208550*Power(t2,2) + 
                     72492*Power(t2,3) + 1125*Power(t2,4)) - 
                  4*t1*(-4449 + 25202*t2 - 49481*Power(t2,2) + 
                     46240*Power(t2,3) - 18142*Power(t2,4) + 
                     1301*Power(t2,5))) + 
               Power(s2,2)*(-959 + 96*Power(t1,7) + 68155*t2 - 
                  227234*Power(t2,2) + 260446*Power(t2,3) - 
                  98843*Power(t2,4) - 7693*Power(t2,5) + 
                  6208*Power(t2,6) - 280*Power(t2,7) + 
                  4*Power(t1,6)*(-588 + 253*t2) + 
                  Power(t1,5)*(2310 + 7719*t2 - 5594*Power(t2,2)) + 
                  Power(t1,4)*
                   (30915 - 90641*t2 + 82440*Power(t2,2) - 
                     25780*Power(t2,3)) + 
                  Power(t1,3)*
                   (36126 - 177778*t2 + 209958*Power(t2,2) - 
                     67496*Power(t2,3) - 3515*Power(t2,4)) + 
                  Power(t1,2)*
                   (-20714 + 153704*t2 - 325510*Power(t2,2) + 
                     311246*Power(t2,3) - 127374*Power(t2,4) + 
                     12835*Power(t2,5)) + 
                  t1*(-31150 + 251207*t2 - 624306*Power(t2,2) + 
                     659772*Power(t2,3) - 291659*Power(t2,4) + 
                     36028*Power(t2,5) - 64*Power(t2,6))) + 
               s2*(1778 - 30*Power(t1,8) + Power(t1,7)*(736 - 292*t2) - 
                  31378*t2 + 56933*Power(t2,2) + 40249*Power(t2,3) - 
                  169117*Power(t2,4) + 135992*Power(t2,5) - 
                  35189*Power(t2,6) + 732*Power(t2,7) + 
                  Power(t1,6)*(-1004 - 2272*t2 + 1707*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (-6112 + 16766*t2 - 14925*Power(t2,2) + 
                     4829*Power(t2,3)) + 
                  Power(t1,4)*
                   (-13162 + 80396*t2 - 96133*Power(t2,2) + 
                     26449*Power(t2,3) + 4100*Power(t2,4)) - 
                  2*Power(t1,3)*
                   (-6160 + 53812*t2 - 116820*Power(t2,2) + 
                     110606*Power(t2,3) - 44446*Power(t2,4) + 
                     4459*Power(t2,5)) + 
                  Power(t1,2)*
                   (34430 - 264762*t2 + 630189*Power(t2,2) - 
                     640176*Power(t2,3) + 269731*Power(t2,4) - 
                     27040*Power(t2,5) - 2241*Power(t2,6)) + 
                  2*t1*(2472 - 57966*t2 + 174799*Power(t2,2) - 
                     179673*Power(t2,3) + 42863*Power(t2,4) + 
                     27091*Power(t2,5) - 9498*Power(t2,6) + 
                     112*Power(t2,7)))) - 
            (-1 + t2)*(2366 - 6145*t2 + 1491*Power(t2,2) + 
               5590*Power(t2,3) + 3775*Power(t2,4) - 17512*Power(t2,5) + 
               14046*Power(t2,6) - 3457*Power(t2,7) - 154*Power(t2,8) + 
               4*Power(s2,7)*
                (10 - 25*t2 + 15*Power(t2,2) + 2*Power(t2,3)) + 
               4*Power(t1,7)*
                (-45 + 70*t2 - 30*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,6)*(1054 - 1385*t2 + 195*Power(t2,2) + 
                  183*Power(t2,3) - 39*Power(t2,4)) + 
               Power(t1,5)*(-579 - 2629*t2 + 5970*Power(t2,2) - 
                  3130*Power(t2,3) + 395*Power(t2,4) + 29*Power(t2,5)) + 
               Power(t1,4)*(-1120 + 5451*t2 - 3870*Power(t2,2) - 
                  1913*Power(t2,3) + 1489*Power(t2,4) - 
                  192*Power(t2,5) + 11*Power(t2,6)) - 
               Power(t1,3)*(4768 - 26036*t2 + 53267*Power(t2,2) - 
                  47298*Power(t2,3) + 17965*Power(t2,4) - 
                  2845*Power(t2,5) + 30*Power(t2,6) + 13*Power(t2,7)) - 
               Power(t1,2)*(2784 - 14257*t2 + 40176*Power(t2,2) - 
                  59113*Power(t2,3) + 39642*Power(t2,4) - 
                  9649*Power(t2,5) + 270*Power(t2,6) + 203*Power(t2,7)) \
+ t1*(4963 - 19321*t2 + 28011*Power(t2,2) - 17739*Power(t2,3) + 
                  8814*Power(t2,4) - 11049*Power(t2,5) + 
                  7281*Power(t2,6) - 919*Power(t2,7) - 33*Power(t2,8)) + 
               2*Power(s2,6)*
                (62 - 120*t2 + 80*Power(t2,2) + 19*Power(t2,3) - 
                  37*Power(t2,4) + 
                  2*t1*(-70 + 165*t2 - 115*Power(t2,2) + 6*Power(t2,3))) \
+ Power(s2,5)*(-4*Power(t1,2)*
                   (-210 + 415*t2 - 275*Power(t2,2) + 28*Power(t2,3)) + 
                  t1*(-669 + 635*t2 - 475*Power(t2,2) + 
                     417*Power(t2,3) + 44*Power(t2,4)) + 
                  2*(-600 + 2426*t2 - 2841*Power(t2,2) + 
                     1169*Power(t2,3) - 266*Power(t2,4) + 84*Power(t2,5)\
)) + Power(s2,4)*(-1927 + 5193*t2 - 39*Power(t2,2) - 6782*Power(t2,3) + 
                  3895*Power(t2,4) - 369*Power(t2,5) - 
                  115*Power(t2,6) + 
                  20*Power(t1,3)*
                   (-77 + 116*t2 - 56*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(t1,2)*
                   (1937 + 302*t2 - 1668*Power(t2,2) - 
                     808*Power(t2,3) + 357*Power(t2,4)) - 
                  5*t1*(-1204 + 5293*t2 - 6352*Power(t2,2) + 
                     2462*Power(t2,3) - 342*Power(t2,4) + 87*Power(t2,5)\
)) + Power(s2,3)*(7282 - 35002*t2 + 65818*Power(t2,2) - 
                  57390*Power(t2,3) + 24358*Power(t2,4) - 
                  6186*Power(t2,5) + 964*Power(t2,6) + 20*Power(t2,7) + 
                  20*Power(t1,4)*
                   (98 - 119*t2 + 29*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(t1,3)*
                   (4091 + 931*t2 - 6059*Power(t2,2) + 
                     641*Power(t2,3) + 556*Power(t2,4)) + 
                  Power(t1,2)*
                   (-10057 + 52037*t2 - 65793*Power(t2,2) + 
                     24947*Power(t2,3) - 1878*Power(t2,4) + 
                     184*Power(t2,5)) + 
                  2*t1*(3079 - 9761*t2 + 2773*Power(t2,2) + 
                     8079*Power(t2,3) - 3950*Power(t2,4) - 
                     132*Power(t2,5) + 200*Power(t2,6))) - 
               Power(s2,2)*(4*Power(t1,5)*
                   (420 - 505*t2 + 95*Power(t2,2) + 32*Power(t2,3)) + 
                  Power(t1,4)*
                   (-5481 + 1769*t2 + 5819*Power(t2,2) - 
                     2031*Power(t2,3) - 196*Power(t2,4)) - 
                  Power(t1,3)*
                   (6275 - 46716*t2 + 65004*Power(t2,2) - 
                     25446*Power(t2,3) + 1149*Power(t2,4) + 
                     294*Power(t2,5)) + 
                  Power(t1,2)*
                   (6759 - 25108*t2 + 8132*Power(t2,2) + 
                     20190*Power(t2,3) - 8887*Power(t2,4) - 
                     610*Power(t2,5) + 388*Power(t2,6)) + 
                  t1*(23299 - 108635*t2 + 198347*Power(t2,2) - 
                     165977*Power(t2,3) + 64162*Power(t2,4) - 
                     13068*Power(t2,5) + 1394*Power(t2,6) + 
                     70*Power(t2,7)) + 
                  4*(348 - 6729*t2 + 25244*Power(t2,2) - 
                     38798*Power(t2,3) + 27402*Power(t2,4) - 
                     8169*Power(t2,5) + 630*Power(t2,6) + 86*Power(t2,7)\
)) + s2*(-5275 + 5283*t2 + 37955*Power(t2,2) - 97829*Power(t2,3) + 
                  86528*Power(t2,4) - 24022*Power(t2,5) - 
                  4316*Power(t2,6) + 1640*Power(t2,7) + 28*Power(t2,8) + 
                  4*Power(t1,6)*
                   (210 - 285*t2 + 85*Power(t2,2) + 4*Power(t2,3)) + 
                  4*Power(t1,5)*
                   (-959 + 847*t2 + 387*Power(t2,2) - 305*Power(t2,3) + 
                     18*Power(t2,4)) - 
                  Power(t1,4)*
                   (459 - 18921*t2 + 31259*Power(t2,2) - 
                     13601*Power(t2,3) + 844*Power(t2,4) + 
                     240*Power(t2,5)) + 
                  Power(t1,3)*
                   (3648 - 16230*t2 + 6495*Power(t2,2) + 
                     12727*Power(t2,3) - 6371*Power(t2,4) + 
                     215*Power(t2,5) + 92*Power(t2,6)) + 
                  Power(t1,2)*
                   (20356 - 98050*t2 + 183747*Power(t2,2) - 
                     155267*Power(t2,3) + 58457*Power(t2,4) - 
                     10285*Power(t2,5) + 574*Power(t2,6) + 
                     60*Power(t2,7)) + 
                  t1*(5966 - 44664*t2 + 135250*Power(t2,2) - 
                     192098*Power(t2,3) + 126077*Power(t2,4) - 
                     32253*Power(t2,5) + 1263*Power(t2,6) + 
                     571*Power(t2,7)))) + 
            Power(s1,2)*(Power(s2,9)*(4 - 6*t2) + 
               Power(t1,9)*(-8 + 6*t2) + 
               Power(t1,8)*(21 + 2*t2 - 6*Power(t2,2)) + 
               Power(t1,7)*(652 - 1197*t2 + 565*Power(t2,2) - 
                  47*Power(t2,3)) + 
               Power(t1,6)*(-1311 + 218*t2 + 1664*Power(t2,2) - 
                  351*Power(t2,3) - 236*Power(t2,4)) - 
               Power(t1,5)*(4464 - 22978*t2 + 33195*Power(t2,2) - 
                  18728*Power(t2,3) + 3702*Power(t2,4) + 138*Power(t2,5)\
) + Power(-1 + t2,2)*(-811 - 13516*t2 + 34533*Power(t2,2) - 
                  6779*Power(t2,3) - 31009*Power(t2,4) + 
                  17643*Power(t2,5) + 221*Power(t2,6)) + 
               Power(t1,4)*(-6268 + 34626*t2 - 62102*Power(t2,2) + 
                  40131*Power(t2,3) - 3709*Power(t2,4) - 
                  3418*Power(t2,5) + 338*Power(t2,6)) + 
               Power(t1,3)*(4026 - 40327*t2 + 112185*Power(t2,2) - 
                  152444*Power(t2,3) + 112349*Power(t2,4) - 
                  40088*Power(t2,5) + 4510*Power(t2,6) + 83*Power(t2,7)) \
+ Power(t1,2)*(8825 - 78150*t2 + 252456*Power(t2,2) - 
                  393070*Power(t2,3) + 309394*Power(t2,4) - 
                  108309*Power(t2,5) + 6630*Power(t2,6) + 
                  2153*Power(t2,7)) + 
               t1*(-3366 - 11666*t2 + 113043*Power(t2,2) - 
                  244608*Power(t2,3) + 208489*Power(t2,4) - 
                  45662*Power(t2,5) - 26921*Power(t2,6) + 
                  10682*Power(t2,7) + 9*Power(t2,8)) + 
               2*Power(s2,8)*
                (27 - 57*t2 + 32*Power(t2,2) + t1*(-20 + 27*t2)) + 
               Power(s2,7)*(-382 + 1070*t2 - 821*Power(t2,2) + 
                  163*Power(t2,3) - 8*Power(t1,2)*(-22 + 27*t2) + 
                  t1*(-399 + 764*t2 - 410*Power(t2,2))) + 
               Power(s2,6)*(-910 + 2250*t2 - 2672*Power(t2,2) + 
                  1919*Power(t2,3) - 535*Power(t2,4) + 
                  56*Power(t1,3)*(-8 + 9*t2) + 
                  Power(t1,2)*(1361 - 2348*t2 + 1190*Power(t2,2)) + 
                  t1*(2838 - 7847*t2 + 6125*Power(t2,2) - 
                     1323*Power(t2,3))) + 
               Power(s2,5)*(7769 + Power(t1,4)*(728 - 756*t2) - 
                  31207*t2 + 42381*Power(t2,2) - 23988*Power(t2,3) + 
                  4647*Power(t2,4) + 307*Power(t2,5) + 
                  Power(t1,3)*(-2731 + 4268*t2 - 2034*Power(t2,2)) + 
                  Power(t1,2)*
                   (-8932 + 24082*t2 - 18815*Power(t2,2) + 
                     4277*Power(t2,3)) + 
                  t1*(3918 - 6834*t2 + 7570*Power(t2,2) - 
                     7294*Power(t2,3) + 2396*Power(t2,4))) + 
               Power(s2,4)*(-6313 + 38231*t2 - 78915*Power(t2,2) + 
                  67555*Power(t2,3) - 23169*Power(t2,4) + 
                  2805*Power(t2,5) - 515*Power(t2,6) + 
                  28*Power(t1,5)*(-28 + 27*t2) + 
                  5*Power(t1,4)*(685 - 976*t2 + 438*Power(t2,2)) - 
                  5*Power(t1,3)*
                   (-3128 + 8091*t2 - 6199*Power(t2,2) + 
                     1437*Power(t2,3)) + 
                  Power(t1,2)*
                   (-9078 + 9314*t2 - 7916*Power(t2,2) + 
                     13675*Power(t2,3) - 5555*Power(t2,4)) + 
                  t1*(-39164 + 162329*t2 - 226845*Power(t2,2) + 
                     136236*Power(t2,3) - 33040*Power(t2,4) + 
                     1055*Power(t2,5))) + 
               Power(s2,3)*(-8294 + 27558*t2 - 53985*Power(t2,2) + 
                  88897*Power(t2,3) - 87903*Power(t2,4) + 
                  38272*Power(t2,5) - 5018*Power(t2,6) + 
                  200*Power(t2,7) - 56*Power(t1,6)*(-10 + 9*t2) + 
                  Power(t1,5)*(-2669 + 3444*t2 - 1454*Power(t2,2)) + 
                  5*Power(t1,4)*
                   (-3326 + 8098*t2 - 5919*Power(t2,2) + 
                     1345*Power(t2,3)) + 
                  Power(t1,3)*
                   (14137 - 9656*t2 + 3384*Power(t2,2) - 
                     16100*Power(t2,3) + 7875*Power(t2,4)) + 
                  Power(t1,2)*
                   (75465 - 324128*t2 + 461494*Power(t2,2) - 
                     282948*Power(t2,3) + 72675*Power(t2,4) - 
                     3932*Power(t2,5)) + 
                  t1*(37655 - 185888*t2 + 340334*Power(t2,2) - 
                     266668*Power(t2,3) + 79180*Power(t2,4) - 
                     3088*Power(t2,5) - 160*Power(t2,6))) + 
               Power(s2,2)*(5281 - 57778*t2 + 242802*Power(t2,2) - 
                  465613*Power(t2,3) + 436418*Power(t2,4) - 
                  189221*Power(t2,5) + 28482*Power(t2,6) - 
                  442*Power(t2,7) + 8*Power(t1,7)*(-32 + 27*t2) + 
                  Power(t1,6)*(1219 - 1364*t2 + 530*Power(t2,2)) + 
                  Power(t1,5)*
                   (10822 - 24325*t2 + 16331*Power(t2,2) - 
                     3413*Power(t2,3)) + 
                  Power(t1,4)*
                   (-13473 + 7674*t2 + 1724*Power(t2,2) + 
                     10405*Power(t2,3) - 6230*Power(t2,4)) + 
                  Power(t1,3)*
                   (-68978 + 309075*t2 - 445172*Power(t2,2) + 
                     271868*Power(t2,3) - 68520*Power(t2,4) + 
                     3333*Power(t2,5)) + 
                  Power(t1,2)*
                   (-62797 + 292339*t2 - 505530*Power(t2,2) + 
                     367362*Power(t2,3) - 89052*Power(t2,4) - 
                     6539*Power(t2,5) + 2048*Power(t2,6)) + 
                  t1*(18608 - 99829*t2 + 251277*Power(t2,2) - 
                     381705*Power(t2,3) + 329505*Power(t2,4) - 
                     135320*Power(t2,5) + 18484*Power(t2,6) - 
                     180*Power(t2,7))) - 
               s2*(-3780 - 30215*t2 + 213277*Power(t2,2) - 
                  446328*Power(t2,3) + 404582*Power(t2,4) - 
                  139081*Power(t2,5) - 8665*Power(t2,6) + 
                  10406*Power(t2,7) - 196*Power(t2,8) + 
                  Power(t1,8)*(-68 + 54*t2) + 
                  Power(t1,7)*(281 - 228*t2 + 70*Power(t2,2)) + 
                  Power(t1,6)*
                   (4008 - 8182*t2 + 4785*Power(t2,2) - 803*Power(t2,3)) \
+ Power(t1,5)*(-6717 + 2966*t2 + 3754*Power(t2,2) + 2254*Power(t2,3) - 
                     2285*Power(t2,4)) + 
                  Power(t1,4)*
                   (-29372 + 139047*t2 - 201337*Power(t2,2) + 
                     119896*Power(t2,3) - 27940*Power(t2,4) + 
                     625*Power(t2,5)) + 
                  Power(t1,3)*
                   (-37723 + 179308*t2 - 306213*Power(t2,2) + 
                     208380*Power(t2,3) - 36750*Power(t2,4) - 
                     10240*Power(t2,5) + 1711*Power(t2,6)) + 
                  Power(t1,2)*
                   (13586 - 109256*t2 + 304190*Power(t2,2) - 
                     442077*Power(t2,3) + 354031*Power(t2,4) - 
                     137862*Power(t2,5) + 18149*Power(t2,6) + 
                     100*Power(t2,7)) + 
                  t1*(16949 - 147866*t2 + 511450*Power(t2,2) - 
                     863450*Power(t2,3) + 739245*Power(t2,4) - 
                     291634*Power(t2,5) + 32856*Power(t2,6) + 
                     2308*Power(t2,7)))) + 
            s1*(-16*Power(s2,8)*(2 - 5*t2 + 3*Power(t2,2)) + 
               8*Power(t1,8)*
                (-10 + 18*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               Power(t1,7)*(329 - 416*t2 - 8*Power(t2,2) + 
                  111*Power(t2,3) - 25*Power(t2,4)) + 
               Power(t1,6)*(1587 - 5902*t2 + 7021*Power(t2,2) - 
                  3071*Power(t2,3) + 392*Power(t2,4) + 11*Power(t2,5)) + 
               Power(t1,5)*(-3607 + 7491*t2 - 1842*Power(t2,2) - 
                  3276*Power(t2,3) + 968*Power(t2,4) + 183*Power(t2,5) + 
                  21*Power(t2,6)) + 
               Power(t1,3)*(-7611 + 40934*t2 - 99237*Power(t2,2) + 
                  119995*Power(t2,3) - 66495*Power(t2,4) + 
                  10444*Power(t2,5) + 2491*Power(t2,6) - 538*Power(t2,7)) \
+ Power(t1,4)*(-6906 + 44957*t2 - 93673*Power(t2,2) + 
                  84331*Power(t2,3) - 33832*Power(t2,4) + 
                  5155*Power(t2,5) + 31*Power(t2,6) - 15*Power(t2,7)) + 
               Power(-1 + t2,2)*
                (2 - 4433*t2 + 35040*Power(t2,2) - 72227*Power(t2,3) + 
                  54670*Power(t2,4) - 9565*Power(t2,5) - 
                  3644*Power(t2,6) + Power(t2,7)) + 
               t1*(7593 - 40077*t2 + 114973*Power(t2,2) - 
                  225684*Power(t2,3) + 286955*Power(t2,4) - 
                  211071*Power(t2,5) + 75216*Power(t2,6) - 
                  6816*Power(t2,7) - 1089*Power(t2,8)) + 
               Power(t1,2)*(3573 - 24094*t2 + 67030*Power(t2,2) - 
                  111908*Power(t2,3) + 125259*Power(t2,4) - 
                  87585*Power(t2,5) + 31753*Power(t2,6) - 
                  3919*Power(t2,7) - 107*Power(t2,8)) + 
               Power(s2,7)*(-168 + 450*t2 - 412*Power(t2,2) + 
                  155*Power(t2,3) - 16*Power(t2,4) - 
                  8*t1*(-33 + 78*t2 - 46*Power(t2,2) + Power(t2,3))) + 
               Power(s2,6)*(1330 - 5369*t2 + 7051*Power(t2,2) - 
                  3311*Power(t2,3) + 239*Power(t2,4) + 98*Power(t2,5) + 
                  8*Power(t1,2)*
                   (-119 + 261*t2 - 144*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(1092 - 2426*t2 + 1834*Power(t2,2) - 
                     649*Power(t2,3) + 86*Power(t2,4))) + 
               Power(s2,5)*(2484 - 8127*t2 + 8169*Power(t2,2) - 
                  1956*Power(t2,3) - 1183*Power(t2,4) + 
                  801*Power(t2,5) - 126*Power(t2,6) + 
                  8*Power(t1,3)*
                   (250 - 503*t2 + 249*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,2)*
                   (-3394 + 6196*t2 - 3432*Power(t2,2) + 
                     1009*Power(t2,3) - 190*Power(t2,4)) - 
                  2*t1*(4086 - 16941*t2 + 23023*Power(t2,2) - 
                     11893*Power(t2,3) + 1731*Power(t2,4) + 
                     108*Power(t2,5))) + 
               Power(s2,4)*(-12203 + 59944*t2 - 113557*Power(t2,2) + 
                  105169*Power(t2,3) - 50545*Power(t2,4) + 
                  12540*Power(t2,5) - 1335*Power(t2,6) + 
                  35*Power(t2,7) - 
                  40*Power(t1,4)*
                   (68 - 126*t2 + 55*Power(t2,2) + 3*Power(t2,3)) + 
                  5*Power(t1,3)*
                   (1269 - 1988*t2 + 724*Power(t2,2) - 
                     107*Power(t2,3) + 39*Power(t2,4)) + 
                  5*Power(t1,2)*
                   (3929 - 16612*t2 + 22867*Power(t2,2) - 
                     12161*Power(t2,3) + 2052*Power(t2,4) + 
                     39*Power(t2,5)) + 
                  t1*(-10102 + 31699*t2 - 31328*Power(t2,2) + 
                     11308*Power(t2,3) - 1805*Power(t2,4) - 
                     197*Power(t2,5) + 115*Power(t2,6))) + 
               Power(s2,3)*(6787 - 58916*t2 + 177156*Power(t2,2) - 
                  246239*Power(t2,3) + 168977*Power(t2,4) - 
                  54064*Power(t2,5) + 6276*Power(t2,6) + 
                  40*Power(t2,7) + 
                  24*Power(t1,5)*
                   (103 - 180*t2 + 72*Power(t2,2) + 5*Power(t2,3)) - 
                  5*Power(t1,4)*
                   (1480 - 2114*t2 + 484*Power(t2,2) + 79*Power(t2,3) + 
                     8*Power(t2,4)) - 
                  10*Power(t1,3)*
                   (2479 - 10548*t2 + 14426*Power(t2,2) - 
                     7588*Power(t2,3) + 1279*Power(t2,4) + 
                     28*Power(t2,5)) + 
                  Power(t1,2)*
                   (19079 - 54736*t2 + 49142*Power(t2,2) - 
                     20952*Power(t2,3) + 11735*Power(t2,4) - 
                     3848*Power(t2,5) + 200*Power(t2,6)) + 
                  t1*(50138 - 249612*t2 + 473544*Power(t2,2) - 
                     435728*Power(t2,3) + 206942*Power(t2,4) - 
                     50388*Power(t2,5) + 4912*Power(t2,6))) - 
               Power(s2,2)*(-6853 + 2060*t2 + 50853*Power(t2,2) - 
                  76971*Power(t2,3) + 906*Power(t2,4) + 
                  62621*Power(t2,5) - 39200*Power(t2,6) + 
                  6652*Power(t2,7) - 70*Power(t2,8) + 
                  8*Power(t1,6)*
                   (183 - 313*t2 + 126*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,5)*
                   (-5218 + 7090*t2 - 1002*Power(t2,2) - 
                     785*Power(t2,3) + 104*Power(t2,4)) - 
                  5*Power(t1,4)*
                   (3654 - 15273*t2 + 20293*Power(t2,2) - 
                     10201*Power(t2,3) + 1569*Power(t2,4) + 
                     72*Power(t2,5)) + 
                  Power(t1,3)*
                   (21395 - 54374*t2 + 38818*Power(t2,2) - 
                     12528*Power(t2,3) + 12355*Power(t2,4) - 
                     5266*Power(t2,5) + 220*Power(t2,6)) + 
                  Power(t1,2)*
                   (69401 - 359921*t2 + 693240*Power(t2,2) - 
                     634160*Power(t2,3) + 292141*Power(t2,4) - 
                     66519*Power(t2,5) + 5430*Power(t2,6) + 
                     100*Power(t2,7)) + 
                  t1*(30349 - 188014*t2 + 479012*Power(t2,2) - 
                     601373*Power(t2,3) + 372469*Power(t2,4) - 
                     97566*Power(t2,5) + 3614*Power(t2,6) + 
                     1560*Power(t2,7))) + 
               s2*(-4797 + 16229*t2 - 59906*Power(t2,2) + 
                  198594*Power(t2,3) - 349565*Power(t2,4) + 
                  310339*Power(t2,5) - 129913*Power(t2,6) + 
                  18462*Power(t2,7) + 557*Power(t2,8) - 
                  8*Power(t1,7)*
                   (-64 + 111*t2 - 49*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(t1,6)*
                   (-2022 + 2656*t2 - 184*Power(t2,2) - 
                     481*Power(t2,3) + 94*Power(t2,4)) - 
                  2*Power(t1,5)*
                   (3935 - 15667*t2 + 19783*Power(t2,2) - 
                     9263*Power(t2,3) + 1242*Power(t2,4) + 84*Power(t2,5)\
) + Power(t1,4)*(13541 - 30701*t2 + 14677*Power(t2,2) + 
                     2348*Power(t2,3) + 2640*Power(t2,4) - 
                     2205*Power(t2,5) + 10*Power(t2,6)) + 
                  2*Power(t1,3)*
                   (19186 - 107605*t2 + 213463*Power(t2,2) - 
                     193966*Power(t2,3) + 84788*Power(t2,4) - 
                     16913*Power(t2,5) + 911*Power(t2,6) + 40*Power(t2,7)\
) + Power(t1,2)*(31298 - 171956*t2 + 408215*Power(t2,2) - 
                     486899*Power(t2,3) + 279992*Power(t2,4) - 
                     58326*Power(t2,5) - 4269*Power(t2,6) + 
                     1996*Power(t2,7)) + 
                  2*t1*(-5289 + 17324*t2 - 25330*Power(t2,2) + 
                     45299*Power(t2,3) - 83634*Power(t2,4) + 
                     82769*Power(t2,5) - 36471*Power(t2,6) + 
                     5282*Power(t2,7) + 48*Power(t2,8))))))*
       R1q(1 - s + s1 - t2))/
     ((-1 + t1)*(1 - s2 + t1 - t2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*(-1 + t2)*
       Power(-1 + s + t2,3)*(-1 + s - s1 + t2)*(s - s1 + t2)*
       Power(-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2,2)) - 
    (8*(9 - 150*s2 + 216*Power(s2,2) - 4*Power(s2,3) - 79*Power(s2,4) + 
         2*Power(s2,5) + 6*Power(s2,6) + 114*t1 - 432*s2*t1 + 
         142*Power(s2,2)*t1 + 232*Power(s2,3)*t1 - 46*Power(s2,4)*t1 - 
         16*Power(s2,5)*t1 + 6*Power(s2,6)*t1 + 51*Power(t1,2) + 
         6*s2*Power(t1,2) - 264*Power(s2,2)*Power(t1,2) + 
         80*Power(s2,3)*Power(t1,2) - 3*Power(s2,4)*Power(t1,2) - 
         30*Power(s2,5)*Power(t1,2) - 144*Power(t1,3) + 
         160*s2*Power(t1,3) - 4*Power(s2,2)*Power(t1,3) + 
         56*Power(s2,3)*Power(t1,3) + 64*Power(s2,4)*Power(t1,3) - 
         49*Power(t1,4) - 58*s2*Power(t1,4) - 
         80*Power(s2,2)*Power(t1,4) - 76*Power(s2,3)*Power(t1,4) + 
         26*Power(t1,5) + 48*s2*Power(t1,5) + 
         54*Power(s2,2)*Power(t1,5) - 11*Power(t1,6) - 
         22*s2*Power(t1,6) + 4*Power(t1,7) + 
         2*Power(s,8)*(-1 + s1)*Power(t1 - t2,2) + 45*t2 + 770*s2*t2 - 
         1169*Power(s2,2)*t2 + 77*Power(s2,3)*t2 + 340*Power(s2,4)*t2 - 
         35*Power(s2,5)*t2 - 28*Power(s2,6)*t2 - 476*t1*t2 + 
         2268*s2*t1*t2 - 830*Power(s2,2)*t1*t2 - 1091*Power(s2,3)*t1*t2 + 
         328*Power(s2,4)*t1*t2 + 133*Power(s2,5)*t1*t2 - 
         12*Power(s2,6)*t1*t2 - 201*Power(t1,2)*t2 + 
         254*s2*Power(t1,2)*t2 + 1220*Power(s2,2)*Power(t1,2)*t2 - 
         641*Power(s2,3)*Power(t1,2)*t2 - 
         254*Power(s2,4)*Power(t1,2)*t2 + 50*Power(s2,5)*Power(t1,2)*t2 + 
         502*Power(t1,3)*t2 - 592*s2*Power(t1,3)*t2 + 
         382*Power(s2,2)*Power(t1,3)*t2 + 
         243*Power(s2,3)*Power(t1,3)*t2 - 82*Power(s2,4)*Power(t1,3)*t2 + 
         123*Power(t1,4)*t2 + 22*s2*Power(t1,4)*t2 - 
         115*Power(s2,2)*Power(t1,4)*t2 + 68*Power(s2,3)*Power(t1,4)*t2 - 
         56*Power(t1,5)*t2 + 20*s2*Power(t1,5)*t2 - 
         32*Power(s2,2)*Power(t1,5)*t2 + Power(t1,6)*t2 + 
         10*s2*Power(t1,6)*t2 - 2*Power(t1,7)*t2 - 50*Power(t2,2) - 
         2356*s2*Power(t2,2) + 2804*Power(s2,2)*Power(t2,2) + 
         42*Power(s2,3)*Power(t2,2) - 658*Power(s2,4)*Power(t2,2) + 
         22*Power(s2,5)*Power(t2,2) + 36*Power(s2,6)*Power(t2,2) + 
         1111*t1*Power(t2,2) - 4994*s2*t1*Power(t2,2) + 
         1073*Power(s2,2)*t1*Power(t2,2) + 
         2223*Power(s2,3)*t1*Power(t2,2) - 
         362*Power(s2,4)*t1*Power(t2,2) - 
         185*Power(s2,5)*t1*Power(t2,2) + 14*Power(s2,6)*t1*Power(t2,2) + 
         318*Power(t1,2)*Power(t2,2) - 436*s2*Power(t1,2)*Power(t2,2) - 
         2461*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         794*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         377*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         58*Power(s2,5)*Power(t1,2)*Power(t2,2) - 
         702*Power(t1,3)*Power(t2,2) + 1018*s2*Power(t1,3)*Power(t2,2) - 
         527*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         387*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         92*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         122*Power(t1,4)*Power(t2,2) + 10*s2*Power(t1,4)*Power(t2,2) + 
         209*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         68*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         63*Power(t1,5)*Power(t2,2) - 56*s2*Power(t1,5)*Power(t2,2) + 
         22*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         6*Power(t1,6)*Power(t2,2) - 2*s2*Power(t1,6)*Power(t2,2) - 
         406*Power(t2,3) + 4174*s2*Power(t2,3) - 
         3287*Power(s2,2)*Power(t2,3) - 510*Power(s2,3)*Power(t2,3) + 
         568*Power(s2,4)*Power(t2,3) + 42*Power(s2,5)*Power(t2,3) - 
         19*Power(s2,6)*Power(t2,3) - 2*Power(s2,7)*Power(t2,3) - 
         1505*t1*Power(t2,3) + 5269*s2*t1*Power(t2,3) + 
         150*Power(s2,2)*t1*Power(t2,3) - 
         1928*Power(s2,3)*t1*Power(t2,3) - 3*Power(s2,4)*t1*Power(t2,3) + 
         99*Power(s2,5)*t1*Power(t2,3) + 6*Power(s2,6)*t1*Power(t2,3) - 
         108*Power(t1,2)*Power(t2,3) - 85*s2*Power(t1,2)*Power(t2,3) + 
         2051*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         167*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         195*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         4*Power(s2,5)*Power(t1,2)*Power(t2,3) + 
         498*Power(t1,3)*Power(t2,3) - 709*s2*Power(t1,3)*Power(t2,3) + 
         142*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         181*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         4*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         18*Power(t1,4)*Power(t2,3) + 19*s2*Power(t1,4)*Power(t2,3) - 
         78*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         6*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         33*Power(t1,5)*Power(t2,3) + 12*s2*Power(t1,5)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,5)*Power(t2,3) + 885*Power(t2,4) - 
         3826*s2*Power(t2,4) + 1764*Power(s2,2)*Power(t2,4) + 
         549*Power(s2,3)*Power(t2,4) - 174*Power(s2,4)*Power(t2,4) - 
         31*Power(s2,5)*Power(t2,4) + Power(s2,6)*Power(t2,4) + 
         923*t1*Power(t2,4) - 2496*s2*t1*Power(t2,4) - 
         740*Power(s2,2)*t1*Power(t2,4) + 
         581*Power(s2,3)*t1*Power(t2,4) + 83*Power(s2,4)*t1*Power(t2,4) - 
         7*Power(s2,5)*t1*Power(t2,4) - 183*Power(t1,2)*Power(t2,4) + 
         265*s2*Power(t1,2)*Power(t2,4) - 
         567*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         66*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         15*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         123*Power(t1,3)*Power(t2,4) + 126*s2*Power(t1,3)*Power(t2,4) + 
         7*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         13*Power(s2,3)*Power(t1,3)*Power(t2,4) + 
         34*Power(t1,4)*Power(t2,4) + 7*s2*Power(t1,4)*Power(t2,4) + 
         4*Power(s2,2)*Power(t1,4)*Power(t2,4) - 599*Power(t2,5) + 
         1604*s2*Power(t2,5) - 348*Power(s2,2)*Power(t2,5) - 
         152*Power(s2,3)*Power(t2,5) + 7*Power(s2,4)*Power(t2,5) - 
         79*t1*Power(t2,5) + 441*s2*t1*Power(t2,5) + 
         199*Power(s2,2)*t1*Power(t2,5) - 33*Power(s2,3)*t1*Power(t2,5) + 
         103*Power(t1,2)*Power(t2,5) + 2*s2*Power(t1,2)*Power(t2,5) + 
         45*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
         33*Power(t1,3)*Power(t2,5) - 19*s2*Power(t1,3)*Power(t2,5) + 
         84*Power(t2,6) - 216*s2*Power(t2,6) + 
         20*Power(s2,2)*Power(t2,6) - 88*t1*Power(t2,6) - 
         56*s2*t1*Power(t2,6) + 20*Power(t1,2)*Power(t2,6) + 
         32*Power(t2,7) + Power(s1,7)*Power(s2 - t1,2)*
          (2 + 6*Power(s2,2) + 11*Power(t1,2) + t1*(6 - 7*t2) + 
            s2*(-8 - 15*t1 + 5*t2)) + 
         Power(s1,6)*(s2 - t1)*
          (26*Power(s2,4) + 11*Power(t1,4) + 4*(-1 + t2) + 
            6*Power(t1,3)*(-3 + 7*t2) + 
            Power(s2,3)*(-39 - 98*t1 + 10*t2) + 
            Power(t1,2)*(-96 + 135*t2 - 41*Power(t2,2)) + 
            t1*(-16 + 79*t2 - 59*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,2)*(-23 + 131*Power(t1,2) + 53*t2 - 
               14*Power(t2,2) + t1*(69 + t2)) - 
            s2*(-20 + 70*Power(t1,3) + 79*t2 - 55*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,2)*(12 + 53*t2) + 
               t1*(-113 + 180*t2 - 53*Power(t2,2)))) + 
         Power(s1,5)*(42*Power(s2,6) - 13*Power(t1,6) + 
            2*Power(-1 + t2,2) - Power(s2,5)*(35 + 210*t1 + 16*t2) + 
            Power(t1,5)*(-79 + 108*t2) + 
            Power(t1,4)*(-219 + 296*t2 - 63*Power(t2,2)) + 
            Power(t1,2)*(158 - 326*t2 + 325*Power(t2,2) - 
               129*Power(t2,3)) + 
            Power(t1,3)*(-119 + 11*t2 + 60*Power(t2,2) - 
               8*Power(t2,3)) + 
            t1*(14 - 133*t2 + 216*Power(t2,2) - 105*Power(t2,3) + 
               8*Power(t2,4)) + 
            Power(s2,4)*(-180 + 403*Power(t1,2) + 274*t2 - 
               62*Power(t2,2) + 3*t1*(15 + 62*t2)) + 
            Power(s2,3)*(198 - 356*Power(t1,3) + 
               Power(t1,2)*(163 - 587*t2) - 365*t2 + 178*Power(t2,2) - 
               19*Power(t2,3) + t1*(829 - 1201*t2 + 306*Power(t2,2))) + 
            Power(s2,2)*(29 + 120*Power(t1,4) - 48*t2 + 
               120*Power(t2,2) - 73*Power(t2,3) + 
               Power(t1,3)*(-400 + 788*t2) + 
               Power(t1,2)*(-1341 + 1880*t2 - 489*Power(t2,2)) + 
               t1*(-531 + 803*t2 - 348*Power(t2,2) + 36*Power(t2,3))) + 
            s2*(-16 + 14*Power(t1,5) + Power(t1,4)*(306 - 479*t2) + 
               135*t2 - 214*Power(t2,2) + 103*Power(t2,3) - 
               8*Power(t2,4) + 
               Power(t1,3)*(911 - 1249*t2 + 308*Power(t2,2)) + 
               Power(t1,2)*(450 - 443*t2 + 104*Power(t2,2) - 
                  7*Power(t2,3)) + 
               t1*(-183 + 364*t2 - 437*Power(t2,2) + 200*Power(t2,3)))) + 
         Power(s1,4)*(30*Power(s2,7) + 15*Power(t1,7) + 
            Power(t1,6)*(18 - 62*t2) + 
            Power(s2,6)*(29 - 150*t1 - 58*t2) + 
            Power(t1,5)*(-66 - 8*t2 + 3*Power(t2,2)) - 
            Power(-1 + t2,2)*
             (-4 + 53*t2 - 45*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,4)*(-495 + 1045*t2 - 509*Power(t2,2) + 
               28*Power(t2,3)) + 
            Power(t1,3)*(-777 + 2091*t2 - 1555*Power(t2,2) + 
               299*Power(t2,3)) + 
            Power(t1,2)*(-349 + 698*t2 - 682*Power(t2,2) + 
               257*Power(t2,3) + 20*Power(t2,4)) + 
            t1*(74 - 123*t2 + 215*Power(t2,2) - 301*Power(t2,3) + 
               135*Power(t2,4)) + 
            Power(s2,5)*(-359 + 271*Power(t1,2) + 406*t2 - 
               102*Power(t2,2) + t1*(-254 + 460*t2)) + 
            Power(s2,4)*(220 - 169*Power(t1,3) + 
               Power(t1,2)*(759 - 1343*t2) - 253*t2 + 32*Power(t2,2) - 
               34*Power(t2,3) + t1*(1542 - 1835*t2 + 532*Power(t2,2))) - 
            Power(s2,3)*(-409 + 84*Power(t1,4) + 
               Power(t1,3)*(1061 - 1911*t2) + 1139*t2 - 
               1130*Power(t2,2) + 334*Power(t2,3) + 
               Power(t1,2)*(2387 - 3042*t2 + 959*Power(t2,2)) + 
               t1*(182 + 365*t2 - 437*Power(t2,2) - 66*Power(t2,3))) + 
            Power(s2,2)*(-340 + 176*Power(t1,5) + 
               Power(t1,4)*(734 - 1405*t2) + 1068*t2 - 
               1381*Power(t2,2) + 637*Power(t2,3) - 40*Power(t2,4) + 
               Power(t1,3)*(1518 - 2211*t2 + 733*Power(t2,2)) + 
               Power(t1,2)*(-816 + 2611*t2 - 1542*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(-1712 + 4607*t2 - 4028*Power(t2,2) + 
                  1059*Power(t2,3))) - 
            s2*(13 + 89*Power(t1,6) + Power(t1,5)*(225 - 497*t2) + 
               71*t2 - 19*Power(t2,2) - 171*Power(t2,3) + 
               106*Power(t2,4) + 
               Power(t1,4)*(248 - 606*t2 + 207*Power(t2,2)) + 
               Power(t1,3)*(-1273 + 3038*t2 - 1582*Power(t2,2) + 
                  69*Power(t2,3)) + 
               Power(t1,2)*(-2088 + 5581*t2 - 4473*Power(t2,2) + 
                  1030*Power(t2,3)) + 
               t1*(-698 + 1807*t2 - 2120*Power(t2,2) + 921*Power(t2,3) - 
                  22*Power(t2,4)))) + 
         Power(s1,3)*(8*Power(s2,8) - 2*Power(t1,8) + 
            Power(s2,7)*(39 - 31*t1 - 53*t2) + 3*Power(t1,7)*(1 + t2) + 
            Power(s2,6)*(-162 - 249*t1 + 8*Power(t1,2) + 170*t2 + 
               411*t1*t2 - 74*Power(t2,2)) + 
            Power(t1,6)*(44 + 4*t2 + 19*Power(t2,2)) - 
            Power(t1,5)*(93 + 373*t2 - 170*Power(t2,2) + 
               16*Power(t2,3)) + 
            Power(t1,4)*(-233 + 352*t2 + 126*Power(t2,2) + 
               29*Power(t2,3)) - 
            Power(-1 + t2,2)*
             (-1 - 68*t2 - 6*Power(t2,2) + 47*Power(t2,3)) + 
            Power(t1,2)*(-1002 + 3846*t2 - 5014*Power(t2,2) + 
               2781*Power(t2,3) - 561*Power(t2,4)) + 
            Power(t1,3)*(-827 + 2881*t2 - 3057*Power(t2,2) + 
               1064*Power(t2,3) - 112*Power(t2,4)) - 
            t1*(275 - 973*t2 + 1799*Power(t2,2) - 1651*Power(t2,3) + 
               534*Power(t2,4) + 16*Power(t2,5)) + 
            Power(s2,5)*(-263 + 147*Power(t1,3) + 
               Power(t1,2)*(640 - 1254*t2) + 643*t2 - 328*Power(t2,2) - 
               28*Power(t2,3) + t1*(611 - 703*t2 + 412*Power(t2,2))) + 
            Power(s2,4)*(1061 - 330*Power(t1,4) - 2386*t2 + 
               2122*Power(t2,2) - 528*Power(t2,3) + 
               11*Power(t1,3)*(-77 + 179*t2) + 
               Power(t1,2)*(-712 + 994*t2 - 835*Power(t2,2)) + 
               t1*(1564 - 4061*t2 + 2031*Power(t2,2) + 56*Power(t2,3))) \
+ Power(s2,3)*(-493 + 327*Power(t1,5) + 1186*t2 - 1243*Power(t2,2) + 
               566*Power(t2,3) - 73*Power(t2,4) - 
               9*Power(t1,4)*(-67 + 189*t2) + 
               Power(t1,3)*(46 - 468*t2 + 754*Power(t2,2)) + 
               Power(t1,2)*(-3053 + 8800*t2 - 4384*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(-3580 + 8161*t2 - 7461*Power(t2,2) + 
                  1865*Power(t2,3))) - 
            Power(s2,2)*(433 + 164*Power(t1,6) + 
               Power(t1,5)*(211 - 777*t2) - 1686*t2 + 
               2700*Power(t2,2) - 2109*Power(t2,3) + 612*Power(t2,4) + 
               Power(t1,4)*(-454 + 80*t2 + 262*Power(t2,2)) + 
               Power(t1,3)*(-2373 + 8362*t2 - 4157*Power(t2,2) + 
                  126*Power(t2,3)) + 
               Power(t1,2)*(-3717 + 8737*t2 - 8589*Power(t2,2) + 
                  2072*Power(t2,3)) + 
               t1*(-284 - 406*t2 + 600*Power(t2,2) + 41*Power(t2,3) - 
                  14*Power(t2,4))) + 
            s2*(242 + 37*Power(t1,7) + Power(t1,6)*(22 - 152*t2) - 
               1041*t2 + 2168*Power(t2,2) - 2084*Power(t2,3) + 
               730*Power(t2,4) - 15*Power(t2,5) + 
               Power(t1,5)*(-281 + 83*t2 - 14*Power(t2,2)) + 
               Power(t1,4)*(-528 + 3353*t2 - 1646*Power(t2,2) + 
                  87*Power(t2,3)) + 
               Power(t1,3)*(-965 + 2610*t2 - 3376*Power(t2,2) + 
                  706*Power(t2,3)) + 
               Power(t1,2)*(1052 - 4544*t2 + 5009*Power(t2,2) - 
                  1658*Power(t2,3) + 186*Power(t2,4)) + 
               t1*(1485 - 5685*t2 + 7917*Power(t2,2) - 
                  5037*Power(t2,3) + 1220*Power(t2,4)))) - 
         s1*(-2*Power(t1,8) + Power(t1,7)*(3 + 8*t2) + 
            Power(t1,6)*(-37 + 72*t2 - 64*Power(t2,2)) + 
            Power(t1,5)*(71 - 160*t2 + 93*Power(t2,2) + 
               65*Power(t2,3)) + 
            Power(t1,4)*(291 - 706*t2 + 751*Power(t2,2) - 
               308*Power(t2,3) - 85*Power(t2,4)) + 
            Power(-1 + t2,2)*
             (51 - 180*t2 + 222*Power(t2,2) - 295*Power(t2,3) + 
               152*Power(t2,4)) + 
            Power(t1,3)*(105 + 152*t2 - 656*Power(t2,2) + 
               183*Power(t2,3) + 168*Power(t2,4) + 56*Power(t2,5)) - 
            Power(t1,2)*(111 - 164*t2 + 832*Power(t2,2) - 
               1319*Power(t2,3) + 329*Power(t2,4) + 211*Power(t2,5)) + 
            t1*(13 - 752*t2 + 2011*Power(t2,2) - 1740*Power(t2,3) + 
               542*Power(t2,4) - 194*Power(t2,5) + 120*Power(t2,6)) + 
            Power(s2,7)*(12 - 44*t2 + 30*Power(t2,2) + Power(t2,3) + 
               2*t1*(6 - 6*t2 + Power(t2,2))) - 
            Power(s2,6)*(4 + 50*t2 + 8*Power(t2,2) - 51*Power(t2,3) + 
               2*Power(t1,2)*(36 - 34*t2 + 5*Power(t2,2)) + 
               t1*(44 - 274*t2 + 213*Power(t2,2))) + 
            Power(s2,5)*(-197 + 653*t2 - 885*Power(t2,2) + 
               367*Power(t2,3) + 7*Power(t2,4) + 
               2*Power(t1,3)*(91 - 80*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(28 - 683*t2 + 584*Power(t2,2) - 
                  10*Power(t2,3)) - 
               t1*(81 - 556*t2 + 180*Power(t2,2) + 211*Power(t2,3))) + 
            Power(s2,3)*(637 - 2785*t2 + 4983*Power(t2,2) - 
               3641*Power(t2,3) + 797*Power(t2,4) + Power(t2,5) + 
               10*Power(t1,5)*(20 - 14*t2 + Power(t2,2)) - 
               Power(t1,4)*(184 + 630*t2 - 594*Power(t2,2) + 
                  15*Power(t2,3)) - 
               Power(t1,3)*(349 - 1889*t2 + 957*Power(t2,2) + 
                  183*Power(t2,3)) + 
               t1*(125 - 965*t2 - 482*Power(t2,2) + 2552*Power(t2,3) - 
                  1014*Power(t2,4)) + 
               Power(t1,2)*(-1197 + 4975*t2 - 7146*Power(t2,2) + 
                  2869*Power(t2,3) - 107*Power(t2,4))) + 
            Power(s2,4)*(94 - 168*t2 + 636*Power(t2,2) - 
               1009*Power(t2,3) + 394*Power(t2,4) - 
               10*Power(t1,4)*(25 - 20*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(91 + 880*t2 - 806*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,2)*(304 - 1599*t2 + 727*Power(t2,2) + 
                  313*Power(t2,3)) + 
               t1*(793 - 2995*t2 + 4227*Power(t2,2) - 
                  1760*Power(t2,3) + 24*Power(t2,4))) + 
            s2*(176 + Power(t1,7)*(22 - 8*t2) - 610*t2 + 
               1688*Power(t2,2) - 3031*Power(t2,3) + 2470*Power(t2,4) - 
               637*Power(t2,5) - 56*Power(t2,6) + 
               Power(t1,6)*(-40 - 59*t2 + 32*Power(t2,2)) + 
               Power(t1,5)*(74 + 15*t2 + 53*Power(t2,2) + 
                  14*Power(t2,3)) + 
               Power(t1,4)*(-344 + 1264*t2 - 1533*Power(t2,2) + 
                  320*Power(t2,3) - 38*Power(t2,4)) + 
               Power(t1,3)*(-222 - 106*t2 - 321*Power(t2,2) + 
                  827*Power(t2,3) + 46*Power(t2,4)) + 
               t1*(1182 - 4701*t2 + 10184*Power(t2,2) - 
                  11513*Power(t2,3) + 5472*Power(t2,4) - 
                  624*Power(t2,5)) + 
               Power(t1,2)*(1264 - 6419*t2 + 11185*Power(t2,2) - 
                  7201*Power(t2,3) + 1354*Power(t2,4) - 207*Power(t2,5))\
) + Power(s2,2)*(-769 + 3286*t2 - 7413*Power(t2,2) + 8797*Power(t2,3) - 
               4672*Power(t2,4) + 771*Power(t2,5) - 
               2*Power(t1,6)*(46 - 26*t2 + Power(t2,2)) + 
               Power(t1,5)*(134 + 254*t2 - 221*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,4)*(93 - 883*t2 + 429*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,3)*(874 - 3737*t2 + 5244*Power(t2,2) - 
                  1861*Power(t2,3) + 114*Power(t2,4)) + 
               Power(t1,2)*(-288 + 1945*t2 - 584*Power(t2,2) - 
                  2062*Power(t2,3) + 659*Power(t2,4)) + 
               t1*(-2000 + 9003*t2 - 15377*Power(t2,2) + 
                  10496*Power(t2,3) - 2232*Power(t2,4) + 134*Power(t2,5))\
)) + Power(s1,2)*(2*Power(t1,8) + Power(t1,7)*(9 - 29*t2) + 
            2*Power(s2,8)*(3 + 3*t1 - 8*t2) - 
            Power(s2,7)*(14 + 42*Power(t1,2) + t1*(28 - 129*t2) + 
               15*t2 + 20*Power(t2,2)) + 
            Power(t1,6)*(-11 + 72*t2 + 28*Power(t2,2)) - 
            Power(t1,5)*(112 - 195*t2 + 204*Power(t2,2) + 
               71*Power(t2,3)) + 
            Power(t1,3)*(-195 + 703*t2 + 60*Power(t2,2) - 
               355*Power(t2,3) - 155*Power(t2,4)) + 
            Power(-1 + t2,2)*
             (-61 + 206*t2 - 472*Power(t2,2) + 267*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(t1,4)*(-273 - 346*t2 + 725*Power(t2,2) - 
               43*Power(t2,3) + 52*Power(t2,4)) + 
            Power(t1,2)*(-433 + 2698*t2 - 4748*Power(t2,2) + 
               3236*Power(t2,3) - 941*Power(t2,4) + 172*Power(t2,5)) + 
            t1*(-478 + 2331*t2 - 4284*Power(t2,2) + 4064*Power(t2,3) - 
               2110*Power(t2,4) + 477*Power(t2,5)) + 
            Power(s2,6)*(-157 + 126*Power(t1,3) + 
               Power(t1,2)*(37 - 425*t2) + 366*t2 - 267*Power(t2,2) - 
               10*Power(t2,3) + t1*(-4 + 208*t2 + 117*Power(t2,2))) + 
            Power(s2,5)*(230 - 210*Power(t1,4) - 415*t2 + 
               592*Power(t2,2) - 316*Power(t2,3) + 
               Power(t1,3)*(23 + 750*t2) + 
               Power(t1,2)*(226 - 801*t2 - 261*Power(t2,2)) + 
               t1*(810 - 2235*t2 + 1621*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s2,4)*(655 + 210*Power(t1,5) - 2201*t2 + 
               2547*Power(t2,2) - 732*Power(t2,3) - 49*Power(t2,4) - 
               110*Power(t1,4)*(1 + 7*t2) + 
               Power(t1,3)*(-575 + 1395*t2 + 274*Power(t2,2)) + 
               Power(t1,2)*(-1613 + 5202*t2 - 3691*Power(t2,2) + 
                  25*Power(t2,3)) + 
               t1*(-478 + 720*t2 - 2118*Power(t2,2) + 1243*Power(t2,3))) \
+ Power(s2,3)*(-1363 - 126*Power(t1,6) + 4496*t2 - 6991*Power(t2,2) + 
               4823*Power(t2,3) - 989*Power(t2,4) + 
               Power(t1,5)*(114 + 461*t2) + 
               Power(t1,4)*(610 - 1205*t2 - 126*Power(t2,2)) + 
               Power(t1,2)*(71 + 450*t2 + 2648*Power(t2,2) - 
                  1622*Power(t2,3)) + 
               Power(t1,3)*(1560 - 5928*t2 + 3989*Power(t2,2) - 
                  83*Power(t2,3)) + 
               t1*(-2878 + 10256*t2 - 11446*Power(t2,2) + 
                  3282*Power(t2,3) - 9*Power(t2,4))) + 
            Power(s2,2)*(475 + 42*Power(t1,7) - 1572*t2 + 
               2915*Power(t2,2) - 2887*Power(t2,3) + 1072*Power(t2,4) - 
               19*Power(t2,5) - Power(t1,6)*(47 + 149*t2) + 
               Power(t1,5)*(-286 + 450*t2 + 9*Power(t2,2)) + 
               Power(t1,4)*(-747 + 3432*t2 - 2026*Power(t2,2) + 
                  69*Power(t2,3)) + 
               Power(t1,3)*(260 - 1205*t2 - 1514*Power(t2,2) + 
                  708*Power(t2,3)) + 
               Power(t1,2)*(3547 - 14393*t2 + 16208*Power(t2,2) - 
                  4561*Power(t2,3) + 244*Power(t2,4)) + 
               t1*(3316 - 10723*t2 + 16660*Power(t2,2) - 
                  11207*Power(t2,3) + 2060*Power(t2,4))) + 
            s2*(229 - 6*Power(t1,8) - 1103*t2 + 2239*Power(t2,2) - 
               2683*Power(t2,3) + 1816*Power(t2,4) - 498*Power(t2,5) + 
               Power(t1,7)*(3 + 20*t2) + 
               Power(t1,6)*(34 - 3*t2 + 7*Power(t2,2)) + 
               Power(t1,5)*(158 - 909*t2 + 346*Power(t2,2) - 
                  19*Power(t2,3)) + 
               Power(t1,4)*(29 + 255*t2 + 596*Power(t2,2) + 
                  58*Power(t2,3)) + 
               Power(t1,2)*(-1758 + 5527*t2 - 9726*Power(t2,2) + 
                  6724*Power(t2,3) - 907*Power(t2,4)) + 
               Power(t1,3)*(-1051 + 6684*t2 - 8034*Power(t2,2) + 
                  2054*Power(t2,3) - 238*Power(t2,4)) - 
               t1*(134 + 911*t2 - 1680*Power(t2,2) + 340*Power(t2,3) + 
                  90*Power(t2,4) + 173*Power(t2,5)))) + 
         Power(s,7)*(2 - 9*Power(t1,3) - 10*t2 + 5*s2*t2 + 
            5*Power(t2,2) + 12*s2*Power(t2,2) + Power(t2,3) + 
            Power(t1,2)*(4 + 8*s2 + 19*t2) + 
            Power(s1,2)*(2 - 12*Power(t1,2) - 6*t2 + 21*t1*t2 - 
               7*Power(t2,2) + s2*(t1 + t2)) - 
            t1*(-4 + 7*t2 + 11*Power(t2,2) + s2*(3 + 20*t2)) + 
            s1*(-4 + 7*Power(t1,3) + Power(t1,2)*(10 - 10*s2 - 13*t2) + 
               (16 - 6*s2)*t2 + (4 - 14*s2)*Power(t2,2) + Power(t2,3) + 
               t1*(-4 - 18*t2 + 5*Power(t2,2) + s2*(2 + 24*t2)))) - 
         Power(s,6)*(-8 - 48*t1 - 15*Power(t1,2) - 19*Power(t1,3) + 
            19*Power(t1,4) + 47*t2 + 99*t1*t2 + 55*Power(t1,2)*t2 - 
            46*Power(t1,3)*t2 - 32*Power(t2,2) - 46*t1*Power(t2,2) + 
            34*Power(t1,2)*Power(t2,2) - 2*Power(t2,3) - 
            6*t1*Power(t2,3) - Power(t2,4) + 
            Power(s2,2)*(-6 - 7*t1 + 12*Power(t1,2) + 36*t2 - 
               40*t1*t2 + 30*Power(t2,2)) + 
            Power(s1,3)*(6 + Power(s2,2) - 28*Power(t1,2) - 24*t2 - 
               8*Power(t2,2) + s2*(2 + t1 + t2) + t1*(4 + 45*t2)) + 
            s2*(21 - 31*Power(t1,3) - 87*t2 + 53*Power(t2,2) + 
               8*Power(t2,3) + Power(t1,2)*(28 + 86*t2) + 
               t1*(33 - 104*t2 - 63*Power(t2,2))) + 
            Power(s1,2)*(-26 + 36*Power(t1,3) + 131*t2 - 
               59*Power(t2,2) + 10*Power(t2,3) + 
               Power(s2,2)*(-10 + 7*t1 + 22*t2) - 
               Power(t1,2)*(2 + 31*t2) + 
               t1*(-84 + 89*t2 - 27*Power(t2,2)) + 
               s2*(25 - 57*Power(t1,2) - 107*t2 - 22*Power(t2,2) + 
                  t1*(41 + 64*t2))) + 
            s1*(28 - 10*Power(t1,4) - 158*t2 + 101*Power(t2,2) - 
               5*Power(t2,3) + Power(t1,3)*(-34 + 20*t2) + 
               Power(t1,2)*(51 + 13*t2 - 10*Power(t2,2)) + 
               t1*(132 - 241*t2 + 50*Power(t2,2)) + 
               Power(s2,2)*(15 - 20*Power(t1,2) - 62*t2 - 
                  42*Power(t2,2) + t1*(4 + 60*t2)) + 
               s2*(-48 + 30*Power(t1,3) + Power(t1,2)*(31 - 69*t2) + 
                  201*t2 - 21*Power(t2,2) + 7*Power(t2,3) + 
                  t1*(-83 + 28*t2 + 32*Power(t2,2))))) + 
         Power(s,5)*(-18 + 11*t1 + 74*Power(t1,2) + 12*Power(t1,3) + 
            64*Power(t1,4) - 23*Power(t1,5) + 79*t2 + 183*t1*t2 - 
            170*Power(t1,2)*t2 - 210*Power(t1,3)*t2 + 
            62*Power(t1,4)*t2 - 258*Power(t2,2) - 164*t1*Power(t2,2) + 
            165*Power(t1,2)*Power(t2,2) - 48*Power(t1,3)*Power(t2,2) + 
            144*Power(t2,3) - 6*t1*Power(t2,3) + 
            2*Power(t1,2)*Power(t2,3) + 17*Power(t2,4) + 
            7*t1*Power(t2,4) + 
            2*Power(s1,4)*(2 + 3*Power(s2,2) - 15*Power(t1,2) + 
               s2*(4 - 5*t1 - 3*t2) - 18*t2 - Power(t2,2) + 
               t1*(8 + 25*t2)) + 
            Power(s2,3)*(-24 + 8*Power(t1,2) + 94*t2 + 40*Power(t2,2) - 
               t1*(9 + 40*t2)) + 
            Power(s2,2)*(59 - 39*Power(t1,3) - 192*t2 + 
               140*Power(t2,2) + 27*Power(t2,3) + 
               Power(t1,2)*(73 + 154*t2) + 
               t1*(110 - 393*t2 - 150*Power(t2,2))) + 
            s2*(6 + 54*Power(t1,4) + 15*t2 + 79*Power(t2,2) - 
               51*Power(t2,3) - 6*Power(t2,4) - 
               16*Power(t1,3)*(8 + 11*t2) + 
               Power(t1,2)*(-81 + 488*t2 + 164*Power(t2,2)) - 
               t1*(197 - 314*t2 + 226*Power(t2,2) + 36*Power(t2,3))) + 
            Power(s1,3)*(-48 - 3*Power(s2,3) + 83*Power(t1,3) + 
               338*t2 - 220*Power(t2,2) + 28*Power(t2,3) - 
               2*Power(t1,2)*(42 + 5*t2) + 
               Power(s2,2)*(-22 + 35*t1 + 58*t2) - 
               2*t1*(144 - 212*t2 + 71*Power(t2,2)) + 
               s2*(66 - 141*Power(t1,2) - 316*t2 + 34*Power(t2,2) + 
                  2*t1*(73 + 31*t2))) + 
            Power(s1,2)*(72 - 27*Power(t1,4) + 
               Power(t1,3)*(25 - 74*t2) - 540*t2 + 308*Power(t2,2) + 
               30*Power(t2,3) + Power(s2,3)*(-36 + 14*t1 + 111*t2) + 
               Power(s2,2)*(155 - 95*Power(t1,2) + t1*(163 - 124*t2) - 
                  583*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(433 - 589*t2 + 149*Power(t2,2)) - 
               2*t1*(-297 + 572*t2 - 233*Power(t2,2) + 9*Power(t2,3)) + 
               s2*(-208 + 108*Power(t1,3) + 1002*t2 - 438*Power(t2,2) + 
                  60*Power(t2,3) + Power(t1,2)*(-142 + 131*t2) - 
                  2*t1*(344 - 561*t2 + 126*Power(t2,2)))) + 
            s1*(-6 + 10*Power(t1,5) + 143*t2 + 208*Power(t2,2) - 
               250*Power(t2,3) + 7*Power(t2,4) - 
               6*Power(t1,4)*(-7 + 5*t2) + 
               Power(t1,3)*(-101 + 65*t2 + 30*Power(t2,2)) - 
               Power(t1,2)*(372 - 665*t2 + 89*Power(t2,2) + 
                  10*Power(t2,3)) - 
               t1*(319 - 434*t2 + 27*Power(t2,2) + 85*Power(t2,3)) + 
               Power(s2,3)*(61 - 20*Power(t1,2) - 220*t2 - 
                  70*Power(t2,2) + t1*(8 + 80*t2)) + 
               Power(s2,2)*(-190 + 50*Power(t1,3) + 735*t2 - 
                  135*Power(t2,2) + 21*Power(t2,3) - 
                  Power(t1,2)*(1 + 150*t2) + 
                  t1*(-324 + 528*t2 + 87*Power(t2,2))) + 
               s2*(118 - 40*Power(t1,4) - 682*t2 + 301*Power(t2,2) + 
                  26*Power(t2,3) + Power(t1,3)*(-49 + 100*t2) + 
                  Power(t1,2)*(380 - 406*t2 - 60*Power(t2,2)) + 
                  t1*(738 - 1500*t2 + 299*Power(t2,2))))) + 
         Power(s,4)*(-27 - 323*t1 - 100*Power(t1,2) + 102*Power(t1,3) - 
            48*Power(t1,4) + 99*Power(t1,5) - 15*Power(t1,6) + 236*t2 + 
            845*t1*t2 + 706*Power(t1,2)*t2 + 39*Power(t1,3)*t2 - 
            302*Power(t1,4)*t2 + 40*Power(t1,5)*t2 - 342*Power(t2,2) - 
            646*t1*Power(t2,2) - 604*Power(t1,2)*Power(t2,2) + 
            191*Power(t1,3)*Power(t2,2) - 20*Power(t1,4)*Power(t2,2) - 
            126*Power(t2,3) + 115*t1*Power(t2,3) - 
            15*Power(t1,2)*Power(t2,3) - 20*Power(t1,3)*Power(t2,3) + 
            178*Power(t2,4) + 60*t1*Power(t2,4) + 
            15*Power(t1,2)*Power(t2,4) + 7*Power(t2,5) + 
            Power(s2,4)*(36 - 2*Power(t1,2) - 116*t2 - 30*Power(t2,2) + 
               5*t1*(3 + 4*t2)) - 
            2*Power(s1,5)*(-2 + 7*Power(s2,2) - 5*Power(t1,2) + 
               s2*(6 - 15*t1 - 7*t2) - 12*t2 + Power(t2,2) + 
               3*t1*(4 + 5*t2)) + 
            Power(s2,3)*(-37 + 21*Power(t1,3) + 34*t2 - 
               130*Power(t2,2) - 50*Power(t2,3) - 
               4*Power(t1,2)*(27 + 34*t2) + 
               t1*(-177 + 613*t2 + 190*Power(t2,2))) + 
            Power(s2,2)*(-185 - 51*Power(t1,4) + 648*t2 - 
               740*Power(t2,2) + 165*Power(t2,3) + 15*Power(t2,4) + 
               18*Power(t1,3)*(15 + 14*t2) + 
               Power(t1,2)*(172 - 1153*t2 - 316*Power(t2,2)) + 
               t1*(162 + 219*t2 + 284*Power(t2,2) + 90*Power(t2,3))) + 
            s2*(193 + 47*Power(t1,5) - 805*t2 + 1246*Power(t2,2) - 
               430*Power(t2,3) - 99*Power(t2,4) - 
               4*Power(t1,4)*(69 + 44*t2) + 
               Power(t1,3)*(17 + 958*t2 + 176*Power(t2,2)) - 
               Power(t1,2)*(247 + 233*t2 + 384*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(486 - 1786*t2 + 1399*Power(t2,2) - 59*Power(t2,3) - 
                  35*Power(t2,4))) + 
            2*Power(s1,4)*(14 + 3*Power(s2,3) - 56*Power(t1,3) + 
               Power(s2,2)*(2 - 31*t1 - 17*t2) + 
               Power(t1,2)*(85 - 6*t2) - 194*t2 + 148*Power(t2,2) - 
               17*Power(t2,3) + t1*(196 - 307*t2 + 107*Power(t2,2)) + 
               s2*(-20 + 96*Power(t1,2) + 181*t2 - 47*Power(t2,2) - 
                  t1*(105 + 32*t2))) + 
            Power(s1,2)*(-36 + 15*Power(t1,5) + 
               Power(s2,4)*(50 - 260*t2) + Power(t1,4)*(96 - 195*t2) + 
               111*t2 - 1446*Power(t2,2) + 1112*Power(t2,3) - 
               49*Power(t2,4) + 
               Power(t1,3)*(622 - 1263*t2 + 195*Power(t2,2)) + 
               Power(t1,2)*(1142 - 1729*t2 + 455*Power(t2,2) + 
                  25*Power(t2,3)) + 
               t1*(286 + 306*t2 - 1256*Power(t2,2) + 631*Power(t2,3)) + 
               Power(s2,3)*(-361 + 8*Power(t1,2) + 1367*t2 - 
                  160*Power(t2,2) + t1*(-327 + 845*t2)) - 
               Power(s2,2)*(-556 + Power(t1,3) + 2387*t2 - 
                  895*Power(t2,2) + 150*Power(t2,3) + 
                  Power(t1,2)*(-657 + 1259*t2) + 
                  t1*(-1856 + 4154*t2 - 855*Power(t2,2))) - 
               s2*(91 + 22*Power(t1,4) + Power(t1,3)*(476 - 869*t2) - 
                  1156*t2 - 116*Power(t2,2) + 436*Power(t2,3) + 
                  Power(t1,2)*(2161 - 4157*t2 + 857*Power(t2,2)) + 
                  t1*(2150 - 4324*t2 + 1571*Power(t2,2) - 
                     90*Power(t2,3)))) + 
            Power(s1,3)*(30*Power(s2,4) + 45*Power(t1,4) + 
               Power(s2,3)*(38 - 161*t1 - 275*t2) + 
               Power(t1,3)*(-242 + 383*t2) - 
               2*Power(t1,2)*(566 - 894*t2 + 277*Power(t2,2)) + 
               4*t1*(-245 + 520*t2 - 266*Power(t2,2) + 
                  14*Power(t2,3)) - 
               2*(37 - 403*t2 + 206*Power(t2,2) + 54*Power(t2,3)) + 
               Power(s2,2)*(-314 + 341*Power(t1,2) + 1354*t2 - 
                  290*Power(t2,2) + t1*(-366 + 613*t2)) + 
               s2*(-255*Power(t1,3) + Power(t1,2)*(556 - 787*t2) + 
                  20*t1*(81 - 154*t2 + 49*Power(t2,2)) + 
                  2*(149 - 961*t2 + 584*Power(t2,2) - 70*Power(t2,3)))) \
+ s1*(111 + 10*Power(t1,6) + Power(t1,5)*(11 - 40*t2) - 809*t2 + 
               1926*Power(t2,2) - 841*Power(t2,3) - 145*Power(t2,4) + 
               Power(t1,4)*(-152 + 133*t2 + 50*Power(t2,2)) + 
               Power(t1,3)*(-471 + 871*t2 + 17*Power(t2,2) - 
                  20*Power(t2,3)) - 
               Power(t1,2)*(129 + 626*t2 - 770*Power(t2,2) + 
                  217*Power(t2,3)) + 
               t1*(668 - 2609*t2 + 2741*Power(t2,2) - 824*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,4)*(10*Power(t1,2) - 4*t1*(8 + 15*t2) + 
                  5*(-21 + 76*t2 + 14*Power(t2,2))) + 
               Power(s2,3)*(303 - 40*Power(t1,3) - 1025*t2 + 
                  220*Power(t2,2) - 35*Power(t2,3) + 
                  Power(t1,2)*(161 + 170*t2) - 
                  2*t1*(-318 + 730*t2 + 65*Power(t2,2))) + 
               Power(s2,2)*(28 + 60*Power(t1,4) + 161*t2 + 
                  456*Power(t2,2) - 205*Power(t2,3) - 
                  5*Power(t1,3)*(43 + 40*t2) + 
                  t1*(-1438 + 2926*t2 - 483*Power(t2,2)) + 
                  2*Power(t1,2)*(-569 + 965*t2 + 75*Power(t2,2))) + 
               s2*(-389 - 40*Power(t1,5) + 1368*t2 - 2759*Power(t2,2) + 
                  1331*Power(t2,3) - 35*Power(t2,4) + 
                  5*Power(t1,4)*(15 + 26*t2) + 
                  Power(t1,3)*(759 - 983*t2 - 140*Power(t2,2)) + 
                  Power(t1,2)*
                   (1586 - 2721*t2 + 172*Power(t2,2) + 50*Power(t2,3)) + 
                  t1*(99 + 1090*t2 - 1640*Power(t2,2) + 551*Power(t2,3)))\
)) + Power(s,3)*(90 + 169*t1 - 434*Power(t1,2) - 254*Power(t1,3) + 
            34*Power(t1,4) - 159*Power(t1,5) + 62*Power(t1,6) - 
            4*Power(t1,7) - 399*t2 - 1415*t1*t2 - 25*Power(t1,2)*t2 + 
            798*Power(t1,3)*t2 + 429*Power(t1,4)*t2 - 
            155*Power(t1,5)*t2 + 7*Power(t1,6)*t2 + 1349*Power(t2,2) + 
            2834*t1*Power(t2,2) + 547*Power(t1,2)*Power(t2,2) - 
            787*Power(t1,3)*Power(t2,2) + 34*Power(t1,4)*Power(t2,2) + 
            11*Power(t1,5)*Power(t2,2) - 1675*Power(t2,3) - 
            1744*t1*Power(t2,3) - 151*Power(t1,2)*Power(t2,3) + 
            7*Power(t1,3)*Power(t2,3) - 27*Power(t1,4)*Power(t2,3) + 
            469*Power(t2,4) + 230*t1*Power(t2,4) + 
            49*Power(t1,2)*Power(t2,4) + 13*Power(t1,3)*Power(t2,4) + 
            108*Power(t2,5) + 33*t1*Power(t2,5) + 
            Power(s1,6)*(-6 + 16*Power(s2,2) + 8*Power(t1,2) + 
               s2*(8 - 35*t1 - 11*t2) - 6*t2 + Power(t2,2) + 
               t1*(16 + 9*t2)) + 
            Power(s2,4)*(-41 - 4*Power(t1,3) + 254*t2 + 5*Power(t2,2) + 
               55*Power(t2,3) + Power(t1,2)*(89 + 59*t2) + 
               t1*(92 - 416*t2 - 135*Power(t2,2))) + 
            Power(s2,5)*(-4*t1*(4 + t2) + 
               3*(-8 + 23*t2 + 4*Power(t2,2))) + 
            s2*(-247 + 16*Power(t1,6) + 831*t2 - 2170*Power(t2,2) + 
               2318*Power(t2,3) - 688*Power(t2,4) - 28*Power(t2,5) - 
               Power(t1,5)*(227 + 68*t2) + 
               Power(t1,4)*(455 + 707*t2 + 48*Power(t2,2)) + 
               Power(t1,3)*(106 - 1714*t2 - 115*Power(t2,2) + 
                  64*Power(t2,3)) + 
               t1*(621 + 18*t2 - 1140*Power(t2,2) + 624*Power(t2,3) - 
                  263*Power(t2,4)) + 
               Power(t1,2)*(1332 - 2974*t2 + 2295*Power(t2,2) + 
                  46*Power(t2,3) - 60*Power(t2,4))) + 
            Power(s2,3)*(16*Power(t1,4) - Power(t1,3)*(233 + 160*t2) + 
               Power(t1,2)*(25 + 1022*t2 + 304*Power(t2,2)) - 
               2*t1*(-153 + 736*t2 - 15*Power(t2,2) + 60*Power(t2,3)) - 
               2*(-158 + 520*t2 - 529*Power(t2,2) + 95*Power(t2,3) + 
                  10*Power(t2,4))) + 
            Power(s2,2)*(-94 - 24*Power(t1,5) + 434*t2 - 
               348*Power(t2,2) - 136*Power(t2,3) + 226*Power(t2,4) + 
               Power(t1,4)*(325 + 166*t2) - 
               Power(t1,3)*(389 + 1227*t2 + 240*Power(t2,2)) + 
               Power(t1,2)*(-405 + 2503*t2 + 46*Power(t2,2) + 
                  28*Power(t2,3)) + 
               t1*(-1401 + 3186*t2 - 2502*Power(t2,2) + 
                  114*Power(t2,3) + 70*Power(t2,4))) + 
            Power(s1,5)*(4 + 93*Power(t1,3) + 
               Power(s2,2)*(20 + 46*t1 - 26*t2) + 206*t2 - 
               176*Power(t2,2) + 19*Power(t2,3) - 
               Power(t1,2)*(134 + 19*t2) + 
               t1*(-228 + 374*t2 - 135*Power(t2,2)) - 
               2*s2*(13 + 75*Power(t1,2) + 85*t2 - 34*Power(t2,2) - 
                  t1*(67 + 50*t2))) + 
            Power(s1,4)*(40 - 84*Power(s2,4) - 84*Power(t1,4) + 
               Power(t1,3)*(341 - 504*t2) - 582*t2 + 281*Power(t2,2) + 
               101*Power(t2,3) + 4*Power(s2,3)*(20 + 99*t1 + 49*t2) + 
               Power(t1,2)*(1262 - 2035*t2 + 698*Power(t2,2)) + 
               t1*(676 - 1763*t2 + 1089*Power(t2,2) - 66*Power(t2,3)) - 
               2*Power(s2,2)*
                (-104 + 338*Power(t1,2) + 629*t2 - 198*Power(t2,2) + 
                  t1*(-85 + 284*t2)) + 
               s2*(-156 + 448*Power(t1,3) + 1719*t2 - 
                  1264*Power(t2,2) + 136*Power(t2,3) + 
                  8*Power(t1,2)*(-73 + 115*t2) + 
                  t1*(-1579 + 3220*t2 - 1174*Power(t2,2)))) - 
            Power(s1,3)*(-16 + 70*Power(s2,5) + 74*Power(t1,5) + 
               Power(t1,4)*(424 - 729*t2) + 
               Power(s2,4)*(1 - 314*t1 - 540*t2) + 431*t2 - 
               2178*Power(t2,2) + 1506*Power(t2,3) - 73*Power(t2,4) + 
               2*Power(t1,3)*(741 - 1469*t2 + 339*Power(t2,2)) + 
               Power(t1,2)*(1448 - 2150*t2 + 770*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(-296 + 1440*t2 - 2370*Power(t2,2) + 
                  1121*Power(t2,3)) + 
               Power(s2,3)*(-680 + 465*Power(t1,2) + 2576*t2 - 
                  660*Power(t2,2) + t1*(-473 + 2172*t2)) + 
               Power(s2,2)*(786 - 194*Power(t1,3) - 3618*t2 + 
                  1856*Power(t2,2) - 280*Power(t2,3) - 
                  7*Power(t1,2)*(-206 + 525*t2) + 
                  t1*(3544 - 8351*t2 + 2500*Power(t2,2))) + 
               s2*(1 - 101*Power(t1,4) + 1092*t2 + 618*Power(t2,2) - 
                  852*Power(t2,3) + 2*Power(t1,3)*(-697 + 1386*t2) + 
                  Power(t1,2)*(-4404 + 8844*t2 - 2497*Power(t2,2)) + 
                  t1*(-2636 + 5936*t2 - 2881*Power(t2,2) + 
                     224*Power(t2,3)))) + 
            Power(s1,2)*(-53 + 27*Power(t1,6) - 
               5*Power(s2,5)*(8 + 7*t1 - 67*t2) + 1396*t2 - 
               3153*Power(t2,2) + 931*Power(t2,3) + 484*Power(t2,4) - 
               2*Power(t1,5)*(-53 + 73*t2) + 
               Power(t1,4)*(377 - 1026*t2 + 66*Power(t2,2)) + 
               Power(s2,4)*(490 + 198*Power(t1,2) + 
                  t1*(441 - 1595*t2) - 1568*t2 + 295*Power(t2,2)) + 
               Power(t1,3)*(760 - 74*t2 - 663*Power(t2,2) + 
                  83*Power(t2,3)) + 
               Power(t1,2)*(-919 + 5120*t2 - 5369*Power(t2,2) + 
                  1384*Power(t2,3)) + 
               t1*(-1706 + 5794*t2 - 6775*Power(t2,2) + 
                  2598*Power(t2,3) + 9*Power(t2,4)) - 
               Power(s2,3)*(735 + 411*Power(t1,3) + 
                  Power(t1,2)*(1354 - 3106*t2) - 1826*t2 + 
                  380*Power(t2,2) - 200*Power(t2,3) + 
                  4*t1*(690 - 1603*t2 + 360*Power(t2,2))) + 
               Power(s2,2)*(-368 + 395*Power(t1,4) + 
                  Power(t1,3)*(1651 - 2913*t2) + 1741*t2 - 
                  4098*Power(t2,2) + 1444*Power(t2,3) + 
                  Power(t1,2)*(4523 - 9248*t2 + 1938*Power(t2,2)) + 
                  t1*(2885 - 3656*t2 + 296*Power(t2,2) - 
                     180*Power(t2,3))) + 
               s2*(684 - 174*Power(t1,5) - 3631*t2 + 7737*Power(t2,2) - 
                  4030*Power(t2,3) + 196*Power(t2,4) + 
                  Power(t1,4)*(-804 + 1213*t2) + 
                  Power(t1,3)*(-2630 + 5430*t2 - 859*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (1455 - 912*t2 - 455*Power(t2,2) + 50*Power(t2,3)) - 
                  2*t1*(-823 + 4123*t2 - 5117*Power(t2,2) + 
                     1568*Power(t2,3)))) - 
            s1*(102 - 7*Power(t1,7) + 62*t2 + 989*Power(t2,2) - 
               2935*Power(t2,3) + 1542*Power(t2,4) - Power(t2,5) + 
               Power(t1,6)*(22 + 29*t2) + 
               Power(t1,5)*(92 - 121*t2 - 37*Power(t2,2)) + 
               Power(t1,4)*(278 - 398*t2 - 67*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,3)*(-199 + 1892*t2 - 1394*Power(t2,2) + 
                  119*Power(t2,3)) + 
               Power(t1,2)*(-1530 + 3815*t2 - 2784*Power(t2,2) + 
                  379*Power(t2,3) + 107*Power(t2,4)) + 
               t1*(-742 + 849*t2 + 1345*Power(t2,2) - 
                  2100*Power(t2,3) + 752*Power(t2,4)) + 
               Power(s2,5)*(-99 + 2*Power(t1,2) + 350*t2 + 
                  42*Power(t2,2) - 2*t1*(29 + 12*t2)) + 
               Power(s2,4)*(216 - 15*Power(t1,3) - 391*t2 + 
                  90*Power(t2,2) - 35*Power(t2,3) + 
                  Power(t1,2)*(317 + 105*t2) + 
                  t1*(636 - 1750*t2 - 115*Power(t2,2))) + 
               Power(s2,3)*(520 + 40*Power(t1,4) - 2172*t2 + 
                  2172*Power(t2,2) - 460*Power(t2,3) - 
                  25*Power(t1,3)*(25 + 8*t2) + 
                  2*t1*(-471 + 544*t2 + 29*Power(t2,2)) + 
                  Power(t1,2)*(-1333 + 3156*t2 + 200*Power(t2,2))) + 
               Power(s2,2)*(-1047 - 50*Power(t1,5) + 4522*t2 - 
                  6018*Power(t2,2) + 2126*Power(t2,3) - 
                  70*Power(t2,4) + 7*Power(t1,4)*(79 + 30*t2) + 
                  Power(t1,3)*(1246 - 2583*t2 - 260*Power(t2,2)) + 
                  Power(t1,2)*
                   (1396 - 1139*t2 - 602*Power(t2,2) + 100*Power(t2,3)) \
+ 2*t1*(-1173 + 4221*t2 - 3413*Power(t2,2) + 677*Power(t2,3))) + 
               s2*(228 + 30*Power(t1,6) - 1996*t2 + 2658*Power(t2,2) + 
                  356*Power(t2,3) - 829*Power(t2,4) - 
                  Power(t1,5)*(209 + 120*t2) + 
                  2*Power(t1,4)*(-271 + 474*t2 + 85*Power(t2,2)) + 
                  Power(t1,2)*
                   (2024 - 8200*t2 + 6077*Power(t2,2) - 964*Power(t2,3)) \
+ Power(t1,3)*(-948 + 840*t2 + 521*Power(t2,2) - 80*Power(t2,3)) + 
                  t1*(3465 - 9736*t2 + 8840*Power(t2,2) - 
                     2292*Power(t2,3) - 96*Power(t2,4))))) - 
         Power(s,2)*(70 - 465*t1 - 862*Power(t1,2) + 46*Power(t1,3) + 
            134*Power(t1,4) - 137*Power(t1,5) + 90*Power(t1,6) - 
            12*Power(t1,7) - 126*t2 + 584*t1*t2 + 1417*Power(t1,2)*t2 + 
            456*Power(t1,3)*t2 + 18*Power(t1,4)*t2 - 
            170*Power(t1,5)*t2 + 11*Power(t1,6)*t2 + 2*Power(t1,7)*t2 + 
            490*Power(t2,2) + 1399*t1*Power(t2,2) - 
            728*Power(t1,2)*Power(t2,2) - 1032*Power(t1,3)*Power(t2,2) + 
            146*Power(t1,4)*Power(t2,2) + 45*Power(t1,5)*Power(t2,2) - 
            8*Power(t1,6)*Power(t2,2) - 1714*Power(t2,3) - 
            2816*t1*Power(t2,3) + 237*Power(t1,2)*Power(t2,3) + 
            308*Power(t1,3)*Power(t2,3) - 25*Power(t1,4)*Power(t2,3) + 
            10*Power(t1,5)*Power(t2,3) + 1952*Power(t2,4) + 
            1437*t1*Power(t2,4) + 49*Power(t1,2)*Power(t2,4) + 
            14*Power(t1,3)*Power(t2,4) - 4*Power(t1,4)*Power(t2,4) - 
            636*Power(t2,5) - 207*t1*Power(t2,5) - 
            45*Power(t1,2)*Power(t2,5) - 20*Power(t2,6) + 
            2*Power(s2,6)*(-3 - 3*t1 + 8*t2 + Power(t2,2)) + 
            Power(s1,7)*(-2 + 9*Power(s2,2) + 8*Power(t1,2) + 
               s2*(2 - 19*t1 - 3*t2) + t1*(4 + t2)) + 
            Power(s2,5)*(-50 + 217*t2 - 55*Power(t2,2) + 
               36*Power(t2,3) + 10*Power(t1,2)*(3 + t2) - 
               t1*(16 + 87*t2 + 51*Power(t2,2))) + 
            Power(s2,4)*(107 - 147*t2 + 336*Power(t2,2) - 
               60*Power(t2,3) - 15*Power(t2,4) - 
               2*Power(t1,3)*(36 + 19*t2) + 
               Power(t1,2)*(175 + 262*t2 + 146*Power(t2,2)) + 
               t1*(277 - 1266*t2 + 224*Power(t2,2) - 90*Power(t2,3))) + 
            Power(s2,3)*(4*Power(t1,4)*(27 + 13*t2) - 
               3*Power(t1,3)*(153 + 145*t2 + 48*Power(t2,2)) + 
               Power(t1,2)*(-330 + 2555*t2 - 342*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(-193 - 118*t2 - 566*Power(t2,2) - 72*Power(t2,3) + 
                  70*Power(t2,4)) + 
               2*(207 - 856*t2 + 1126*Power(t2,2) - 514*Power(t2,3) + 
                  127*Power(t2,4))) + 
            Power(s2,2)*(-717 + 2918*t2 - 4896*Power(t2,2) + 
               3354*Power(t2,3) - 668*Power(t2,4) - 42*Power(t2,5) - 
               2*Power(t1,5)*(51 + 14*t2) + 
               Power(t1,4)*(577 + 363*t2 + 36*Power(t2,2)) + 
               Power(t1,3)*(-108 - 2350*t2 + 277*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(208 + 673*t2 + 321*Power(t2,2) + 
                  242*Power(t2,3) - 90*Power(t2,4)) - 
               3*t1*(646 - 2112*t2 + 2322*Power(t2,2) - 
                  836*Power(t2,3) + 143*Power(t2,4))) + 
            s2*(182 - 1034*t2 + 1673*Power(t2,2) - 344*Power(t2,3) - 
               811*Power(t2,4) + 368*Power(t2,5) + 
               2*Power(t1,6)*(27 + t2) + 
               Power(t1,5)*(-361 - 130*t2 + 19*Power(t2,2)) + 
               Power(t1,4)*(348 + 1014*t2 - 149*Power(t2,2) - 
                  60*Power(t2,3)) + 
               Power(t1,3)*(-256 - 426*t2 - 237*Power(t2,2) - 
                  85*Power(t2,3) + 39*Power(t2,4)) + 
               Power(t1,2)*(1456 - 5050*t2 + 5780*Power(t2,2) - 
                  1835*Power(t2,3) + 164*Power(t2,4)) + 
               t1*(2209 - 5808*t2 + 6198*Power(t2,2) - 
                  3038*Power(t2,3) + 343*Power(t2,4) + 99*Power(t2,5))) \
+ Power(s1,6)*(6 + 6*Power(s2,3) + 44*Power(t1,3) + 
               Power(s2,2)*(14 + 11*t1 - 32*t2) + 41*t2 - 
               39*Power(t2,2) + 4*Power(t2,3) - 
               Power(t1,2)*(38 + 37*t2) + 
               t1*(-36 + 77*t2 - 31*Power(t2,2)) + 
               s2*(-33 - 63*Power(t1,2) - 19*t2 + 16*Power(t2,2) + 
                  t1*(29 + 80*t2))) + 
            Power(s1,5)*(18 - 84*Power(s2,4) - 102*Power(t1,4) + 
               Power(t1,3)*(166 - 248*t2) - 176*t2 + 83*Power(t2,2) + 
               29*Power(t2,3) + 2*Power(s2,3)*(69 + 196*t1 + 3*t2) + 
               Power(t1,2)*(627 - 1013*t2 + 380*Power(t2,2)) + 
               t1*(152 - 699*t2 + 534*Power(t2,2) - 36*Power(t2,3)) - 
               Power(s2,2)*(-17 + 656*Power(t1,2) + 476*t2 - 
                  192*Power(t2,2) + 4*t1*(37 + 19*t2)) + 
               s2*(-10 + 450*Power(t1,3) + 705*t2 - 599*Power(t2,2) + 
                  57*Power(t2,3) + Power(t1,2)*(-155 + 329*t2) + 
                  t1*(-649 + 1428*t2 - 576*Power(t2,2)))) - 
            Power(s1,4)*(28 + 156*Power(s2,5) + 52*Power(t1,5) + 
               Power(t1,4)*(489 - 801*t2) + 306*t2 - 1154*Power(t2,2) + 
               740*Power(t2,3) - 40*Power(t2,4) - 
               Power(s2,4)*(175 + 716*t1 + 324*t2) + 
               Power(t1,3)*(1551 - 2744*t2 + 716*Power(t2,2)) + 
               Power(t1,2)*(1011 - 1637*t2 + 744*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(-394 + 797*t2 - 1278*Power(t2,2) + 
                  727*Power(t2,3)) + 
               Power(s2,3)*(-536 + 1167*Power(t1,2) + 1980*t2 - 
                  604*Power(t2,2) + t1*(87 + 1588*t2)) + 
               Power(s2,2)*(610 - 758*Power(t1,3) + 
                  Power(t1,2)*(892 - 3147*t2) - 2738*t2 + 
                  1672*Power(t2,2) - 204*Power(t2,3) + 
                  t1*(2969 - 6825*t2 + 2238*Power(t2,2))) + 
               s2*(-30 + 99*Power(t1,4) + 761*t2 + 223*Power(t2,2) - 
                  536*Power(t2,3) + Power(t1,3)*(-1293 + 2684*t2) + 
                  Power(t1,2)*(-4022 + 7662*t2 - 2355*Power(t2,2)) + 
                  t1*(-1725 + 4438*t2 - 2558*Power(t2,2) + 
                     198*Power(t2,3)))) + 
            Power(s1,3)*(17 - 75*Power(s2,6) + 93*Power(t1,6) + 
               1023*t2 - 2028*Power(t2,2) + 417*Power(t2,3) + 
               381*Power(t2,4) - 71*Power(t1,5)*(-4 + 7*t2) + 
               Power(s2,5)*(-25 + 311*t1 + 535*t2) + 
               Power(t1,4)*(621 - 1659*t2 + 283*Power(t2,2)) + 
               Power(s2,4)*(995 - 339*Power(t1,2) + 
                  t1*(661 - 2893*t2) - 2404*t2 + 700*Power(t2,2)) + 
               Power(t1,3)*(98 + 1612*t2 - 1335*Power(t2,2) + 
                  126*Power(t2,3)) + 
               Power(t1,2)*(-1891 + 7604*t2 - 7541*Power(t2,2) + 
                  1980*Power(t2,3)) + 
               t1*(-1350 + 4581*t2 - 6431*Power(t2,2) + 
                  2957*Power(t2,3) - 14*Power(t2,4)) - 
               Power(s2,3)*(1111 + 192*Power(t1,3) + 
                  Power(t1,2)*(2292 - 6203*t2) - 2312*t2 + 
                  760*Power(t2,2) - 280*Power(t2,3) + 
                  t1*(4799 - 9861*t2 + 3040*Power(t2,2))) + 
               Power(s2,2)*(-434 + 625*Power(t1,4) + 
                  Power(t1,3)*(2985 - 6364*t2) + 2489*t2 - 
                  5072*Power(t2,2) + 1908*Power(t2,3) + 
                  Power(t1,2)*(7327 - 14294*t2 + 4167*Power(t2,2)) + 
                  t1*(2959 - 3231*t2 + 539*Power(t2,2) - 
                     336*Power(t2,3))) - 
               s2*(-485 + 423*Power(t1,5) + 
                  Power(t1,4)*(1613 - 3016*t2) + 3524*t2 - 
                  7721*Power(t2,2) + 4224*Power(t2,3) - 
                  219*Power(t2,4) + 
                  Power(t1,3)*(4144 - 8496*t2 + 2110*Power(t2,2)) + 
                  Power(t1,2)*
                   (1948 + 774*t2 - 1690*Power(t2,2) + 81*Power(t2,3)) \
+ 3*t1*(-885 + 3630*t2 - 4343*Power(t2,2) + 1369*Power(t2,3)))) - 
            Power(s1,2)*(51 + 9*Power(t1,7) + 
               Power(s2,6)*(30 + 49*t1 - 246*t2) + 
               Power(t1,6)*(43 - 32*t2) - 299*t2 + 2940*Power(t2,2) - 
               5167*Power(t2,3) + 2322*Power(t2,4) - 19*Power(t2,5) + 
               Power(t1,5)*(3 - 347*t2 - 34*Power(t2,2)) - 
               Power(s2,5)*(418 + 295*Power(t1,2) + 
                  t1*(376 - 1474*t2) - 853*t2 + 258*Power(t2,2)) + 
               Power(t1,4)*(79 + 1405*t2 - 932*Power(t2,2) + 
                  69*Power(t2,3)) + 
               Power(t1,3)*(-1517 + 5852*t2 - 5088*Power(t2,2) + 
                  816*Power(t2,3)) + 
               Power(t1,2)*(-1945 + 3986*t2 - 2430*Power(t2,2) + 
                  145*Power(t2,3) + 244*Power(t2,4)) + 
               t1*(141 - 2733*t2 + 6226*Power(t2,2) - 
                  5577*Power(t2,3) + 1825*Power(t2,4)) + 
               Power(s2,4)*(341 + 699*Power(t1,3) + 
                  Power(t1,2)*(1332 - 3539*t2) + 522*t2 - 
                  735*Power(t2,2) - 150*Power(t2,3) + 
                  3*t1*(723 - 1399*t2 + 435*Power(t2,2))) - 
               Power(s2,3)*(-1477 + 826*Power(t1,4) + 
                  Power(t1,3)*(2083 - 4265*t2) + 5907*t2 - 
                  7208*Power(t2,2) + 2016*Power(t2,3) + 
                  Power(t1,2)*(3880 - 7580*t2 + 2162*Power(t2,2)) + 
                  t1*(546 + 4662*t2 - 3878*Power(t2,2) - 
                     180*Power(t2,3))) + 
               Power(s2,2)*(-1723 + 499*Power(t1,5) + 
                  Power(t1,4)*(1567 - 2611*t2) + 6963*t2 - 
                  9733*Power(t2,2) + 3992*Power(t2,3) - 
                  294*Power(t2,4) + 
                  Power(t1,3)*(2928 - 6328*t2 + 1407*Power(t2,2)) + 
                  Power(t1,2)*
                   (-46 + 9695*t2 - 6876*Power(t2,2) + 
                     150*Power(t2,3)) + 
                  t1*(-6385 + 21581*t2 - 21752*Power(t2,2) + 
                     5622*Power(t2,3))) - 
               s2*(-122 + 135*Power(t1,6) + 
                  Power(t1,5)*(513 - 689*t2) + 1235*t2 - 
                  301*Power(t2,2) - 3261*Power(t2,3) + 
                  1957*Power(t2,4) + 
                  Power(t1,4)*(802 - 2439*t2 + 258*Power(t2,2)) + 
                  Power(t1,3)*
                   (-172 + 6960*t2 - 4665*Power(t2,2) + 
                     249*Power(t2,3)) + 
                  Power(t1,2)*
                   (-6471 + 21720*t2 - 19778*Power(t2,2) + 
                     4390*Power(t2,3)) + 
                  t1*(-4501 + 12001*t2 - 12114*Power(t2,2) + 
                     4024*Power(t2,3) + 27*Power(t2,4)))) + 
            s1*(-24 - 2*Power(t1,8) - 667*t2 + 3038*Power(t2,2) - 
               3005*Power(t2,3) - 43*Power(t2,4) + 619*Power(t2,5) + 
               Power(t1,7)*(19 + 8*t2) - 
               Power(t1,6)*(3 + 60*t2 + 10*Power(t2,2)) + 
               Power(t1,5)*(63 - 38*t2 - Power(t2,2) + 4*Power(t2,3)) - 
               Power(t1,4)*(276 - 1183*t2 + 671*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(t1,3)*(-1059 + 724*t2 + 603*Power(t2,2) - 
                  561*Power(t2,3) + 114*Power(t2,4)) + 
               Power(t1,2)*(681 - 4836*t2 + 7901*Power(t2,2) - 
                  4528*Power(t2,3) + 762*Power(t2,4)) + 
               t1*(1353 - 5858*t2 + 9088*Power(t2,2) - 
                  5760*Power(t2,3) + 1136*Power(t2,4) + 134*Power(t2,5)) \
+ 2*Power(s2,6)*(26 - 83*t2 - 7*Power(t2,2) + 2*t1*(11 + t2)) + 
               Power(s2,4)*(-771 - 10*Power(t1,4) + 2527*t2 - 
                  2285*Power(t2,2) + 485*Power(t2,3) + 
                  Power(t1,3)*(579 + 100*t2) + 
                  t1*(-318 + 2081*t2 - 832*Power(t2,2)) + 
                  Power(t1,2)*(376 - 2143*t2 - 150*Power(t2,2))) + 
               Power(s2,5)*(-23 + 2*Power(t1,3) - 306*t2 + 
                  99*Power(t2,2) + 21*Power(t2,3) - 
                  Power(t1,2)*(250 + 33*t2) + 
                  t1*(-257 + 972*t2 + 60*Power(t2,2))) + 
               Power(s2,3)*(931 + 20*Power(t1,5) - 2306*t2 + 
                  2258*Power(t2,2) - 892*Power(t2,3) + 70*Power(t2,4) - 
                  6*Power(t1,4)*(116 + 25*t2) + 
                  Power(t1,3)*(-103 + 2317*t2 + 240*Power(t2,2)) + 
                  Power(t1,2)*
                   (1134 - 4537*t2 + 1948*Power(t2,2) - 
                     100*Power(t2,3)) - 
                  2*t1*(-1954 + 5381*t2 - 4222*Power(t2,2) + 
                     803*Power(t2,3))) + 
               s2*(-691 + 10*Power(t1,7) + 4011*t2 - 
                  10416*Power(t2,2) + 10663*Power(t2,3) - 
                  3487*Power(t2,4) + 3*Power(t2,5) - 
                  Power(t1,6)*(150 + 49*t2) + 
                  Power(t1,5)*(92 + 403*t2 + 84*Power(t2,2)) + 
                  Power(t1,3)*
                   (2874 - 7930*t2 + 4991*Power(t2,2) - 
                     421*Power(t2,3)) + 
                  Power(t1,4)*
                   (303 - 1217*t2 + 583*Power(t2,2) - 45*Power(t2,3)) + 
                  t1*(-1812 + 10663*t2 - 18368*Power(t2,2) + 
                     11968*Power(t2,3) - 2518*Power(t2,4)) + 
                  Power(t1,2)*
                   (3302 - 2697*t2 - 1048*Power(t2,2) + 
                     1205*Power(t2,3) - 321*Power(t2,4))) + 
               Power(s2,2)*(526 - 20*Power(t1,6) - 3066*t2 + 
                  7244*Power(t2,2) - 6340*Power(t2,3) + 
                  1617*Power(t2,4) + 2*Power(t1,5)*(227 + 60*t2) - 
                  Power(t1,4)*(157 + 1323*t2 + 210*Power(t2,2)) + 
                  Power(t1,3)*
                   (-1159 + 4017*t2 - 1797*Power(t2,2) + 
                     120*Power(t2,3)) + 
                  Power(t1,2)*
                   (-5735 + 14982*t2 - 10479*Power(t2,2) + 
                     1590*Power(t2,3)) + 
                  t1*(-3185 + 4320*t2 - 1942*Power(t2,2) + 
                     352*Power(t2,3) + 144*Power(t2,4))))) + 
         s*(6 + 2*Power(s1,8)*Power(s2 - t1,2) - 488*t1 - 
            470*Power(t1,2) + 324*Power(t1,3) + 154*Power(t1,4) - 
            80*Power(t1,5) + 54*Power(t1,6) - 12*Power(t1,7) - 30*t2 + 
            1577*t1*t2 + 1155*Power(t1,2)*t2 - 738*Power(t1,3)*t2 - 
            280*Power(t1,4)*t2 + 5*Power(t1,5)*t2 + 3*Power(t1,6)*t2 + 
            4*Power(t1,7)*t2 - 252*Power(t2,2) - 1859*t1*Power(t2,2) - 
            1038*Power(t1,2)*Power(t2,2) + 372*Power(t1,3)*Power(t2,2) + 
            262*Power(t1,4)*Power(t2,2) - Power(t1,5)*Power(t2,2) - 
            12*Power(t1,6)*Power(t2,2) + 522*Power(t2,3) + 
            486*t1*Power(t2,3) + 461*Power(t1,2)*Power(t2,3) + 
            59*Power(t1,3)*Power(t2,3) - 59*Power(t1,4)*Power(t2,3) + 
            11*Power(t1,5)*Power(t2,3) + 82*Power(t2,4) + 
            677*t1*Power(t2,4) - 124*Power(t1,2)*Power(t2,4) - 
            67*Power(t1,3)*Power(t2,4) - 20*Power(t1,4)*Power(t2,4) - 
            576*Power(t2,5) - 449*t1*Power(t2,5) + 
            18*Power(t1,2)*Power(t2,5) + 19*Power(t1,3)*Power(t2,5) + 
            248*Power(t2,6) + 56*t1*Power(t2,6) + 
            Power(s1,7)*(s2 - t1)*
             (-8 + 3*Power(s2,2) - 9*Power(t1,2) + 
               2*s2*(1 + t1 - 4*t2) + 4*t2 + 14*t1*t2) + 
            Power(s2,6)*(-12 + 44*t2 - 22*Power(t2,2) + 13*Power(t2,3) - 
               4*t1*(3 - 3*t2 + 2*Power(t2,2))) + 
            Power(s2,5)*(-28 + 177*t2 - 129*Power(t2,2) + 
               33*Power(t2,3) - 6*Power(t2,4) + 
               4*Power(t1,2)*(15 - 10*t2 + 7*Power(t2,2)) - 
               4*t1*(-4 + 54*t2 - 19*Power(t2,2) + 9*Power(t2,3))) + 
            Power(s2,3)*(159 - 858*t2 + 1270*Power(t2,2) - 
               526*Power(t2,3) + 16*Power(t2,4) - 28*Power(t2,5) + 
               8*Power(t1,4)*(21 - 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(-303 - 518*t2 + 109*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(-553 + 1822*t2 - 2954*Power(t2,2) + 
                  1772*Power(t2,3) - 309*Power(t2,4)) + 
               Power(t1,2)*(-335 + 2258*t2 - 1727*Power(t2,2) + 
                  406*Power(t2,3) - 60*Power(t2,4))) + 
            Power(s2,4)*(193 - 613*t2 + 982*Power(t2,2) - 
               648*Power(t2,3) + 141*Power(t2,4) - 
               4*Power(t1,3)*(33 - 11*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(91 + 457*t2 - 111*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(216 - 1170*t2 + 886*Power(t2,2) - 220*Power(t2,3) + 
                  35*Power(t2,4))) + 
            s2*(401 - 1965*t2 + 4931*Power(t2,2) - 6211*Power(t2,3) + 
               3492*Power(t2,4) - 608*Power(t2,5) - 40*Power(t2,6) - 
               4*Power(t1,6)*(-15 + 2*t2 + Power(t2,2)) + 
               Power(t1,5)*(-229 - 82*t2 + 51*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,4)*(173 + 439*t2 - 340*Power(t2,2) + 
                  43*Power(t2,3) - 8*Power(t2,4)) + 
               Power(t1,3)*(-450 + 1044*t2 - 1351*Power(t2,2) + 
                  524*Power(t2,3) + 7*Power(t2,4)) + 
               t1*(1767 - 6730*t2 + 10880*Power(t2,2) - 
                  8668*Power(t2,3) + 3153*Power(t2,4) - 406*Power(t2,5)) \
+ Power(t1,2)*(454 - 2442*t2 + 4017*Power(t2,2) - 2404*Power(t2,3) + 
                  560*Power(t2,4) - 90*Power(t2,5))) + 
            Power(s2,2)*(-719 + 3245*t2 - 6588*Power(t2,2) + 
               6441*Power(t2,3) - 2789*Power(t2,4) + 412*Power(t2,5) + 
               4*Power(t1,5)*(-33 + t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(383 + 312*t2 - 91*Power(t2,2) - 
                  39*Power(t2,3)) + 
               Power(t1,3)*(54 - 1709*t2 + 1311*Power(t2,2) - 
                  273*Power(t2,3) + 39*Power(t2,4)) + 
               Power(t1,2)*(656 - 1973*t2 + 3061*Power(t2,2) - 
                  1589*Power(t2,3) + 181*Power(t2,4)) + 
               t1*(-946 + 4057*t2 - 5633*Power(t2,2) + 
                  2794*Power(t2,3) - 468*Power(t2,4) + 99*Power(t2,5))) + 
            Power(s1,6)*(-36*Power(s2,4) - 57*Power(t1,4) + 
               Power(s2,3)*(64 + 170*t1 - 31*t2) + 
               Power(t1,3)*(11 - 26*t2) - 
               Power(s2,2)*(19 + 293*Power(t1,2) + t1*(133 - 80*t2) + 
                  51*t2 - 29*Power(t2,2)) + 2*(3 - 4*t2 + Power(t2,2)) + 
               Power(t1,2)*(109 - 183*t2 + 77*Power(t2,2)) - 
               2*t1*(5 + 53*t2 - 52*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(8 + 216*Power(t1,3) + Power(t1,2)*(58 - 23*t2) + 
                  102*t2 - 98*Power(t2,2) + 8*Power(t2,3) + 
                  t1*(-78 + 214*t2 - 98*Power(t2,2)))) + 
            Power(s1,5)*(-112*Power(s2,5) + 8*Power(t1,5) + 
               Power(s2,4)*(165 + 526*t1 + 34*t2) + 
               Power(t1,4)*(-202 + 339*t2) + 
               Power(t1,3)*(-705 + 1093*t2 - 304*Power(t2,2)) + 
               t1*(79 + 6*t2 + 145*Power(t2,2) - 158*Power(t2,3)) + 
               Power(t1,2)*(-364 + 685*t2 - 365*Power(t2,2) + 
                  7*Power(t2,3)) + 
               2*(-5 - 41*t2 + 91*Power(t2,2) - 49*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s2,3)*(-179 + 919*Power(t1,2) + 604*t2 - 
                  188*Power(t2,2) + t1*(349 + 332*t2)) + 
               Power(s2,2)*(-216 + 716*Power(t1,3) + 887*t2 - 
                  601*Power(t2,2) + 57*Power(t2,3) + 
                  Power(t1,2)*(-13 + 935*t2) + 
                  t1*(-1100 + 2293*t2 - 747*Power(t2,2))) + 
               s2*(29 - 219*Power(t1,4) + Power(t1,3)*(399 - 976*t2) - 
                  234*t2 + 31*Power(t2,2) + 102*Power(t2,3) + 
                  Power(t1,2)*(1636 - 2798*t2 + 869*Power(t2,2)) - 
                  2*t1*(-285 + 794*t2 - 500*Power(t2,2) + 36*Power(t2,3))\
)) + Power(s1,4)*(-13 - 114*Power(s2,6) + 69*Power(t1,6) + 
               Power(t1,5)*(265 - 421*t2) + 285*t2 - 378*Power(t2,2) + 
               25*Power(t2,3) + 81*Power(t2,4) + 
               Power(s2,5)*(54 + 542*t1 + 226*t2) + 
               3*Power(t1,4)*(193 - 376*t2 + 79*Power(t2,2)) + 
               Power(s2,4)*(780 - 924*Power(t1,2) + 
                  t1*(191 - 1466*t2) - 1454*t2 + 406*Power(t2,2)) + 
               Power(t1,3)*(-92 + 1002*t2 - 729*Power(t2,2) + 
                  69*Power(t2,3)) + 
               Power(t1,2)*(-1179 + 3766*t2 - 3502*Power(t2,2) + 
                  986*Power(t2,3)) + 
               t1*(-389 + 1440*t2 - 2445*Power(t2,2) + 
                  1248*Power(t2,3) - 22*Power(t2,4)) + 
               Power(s2,3)*(-841 + 591*Power(t1,3) + 1730*t2 - 
                  736*Power(t2,2) + 136*Power(t2,3) + 
                  18*Power(t1,2)*(-69 + 199*t2) + 
                  t1*(-3495 + 6054*t2 - 1810*Power(t2,2))) + 
               Power(s2,2)*(-195 + 61*Power(t1,4) + 
                  Power(t1,3)*(1960 - 4091*t2) + 803*t2 - 
                  1682*Power(t2,2) + 769*Power(t2,3) + 
                  Power(t1,2)*(5268 - 8930*t2 + 2616*Power(t2,2)) + 
                  t1*(1889 - 2803*t2 + 1032*Power(t2,2) - 
                     198*Power(t2,3))) + 
               s2*(192 - 225*Power(t1,5) - 1305*t2 + 2660*Power(t2,2) - 
                  1459*Power(t2,3) + 80*Power(t2,4) + 
                  2*Power(t1,4)*(-614 + 1085*t2) + 
                  Power(t1,3)*(-3132 + 5458*t2 - 1449*Power(t2,2)) + 
                  Power(t1,2)*
                   (-953 + 34*t2 + 478*Power(t2,2) - 18*Power(t2,3)) - 
                  2*t1*(-708 + 2286*t2 - 2588*Power(t2,2) + 
                     893*Power(t2,3)))) + 
            Power(s1,3)*(-40 - 39*Power(s2,7) - 31*Power(t1,7) + 
               401*t2 - 1685*Power(t2,2) + 2174*Power(t2,3) - 
               865*Power(t2,4) + 15*Power(t2,5) + 
               17*Power(t1,6)*(-2 + 7*t2) + 
               Power(s2,6)*(-77 + 155*t1 + 266*t2) + 
               Power(t1,5)*(144 + 173*t2 + 10*Power(t2,2)) + 
               Power(s2,5)*(731 - 110*Power(t1,2) + 
                  t1*(661 - 1762*t2) - 1060*t2 + 362*Power(t2,2)) + 
               Power(t1,3)*(1753 - 5508*t2 + 4818*Power(t2,2) - 
                  878*Power(t2,3)) + 
               Power(t1,4)*(632 - 2559*t2 + 1341*Power(t2,2) - 
                  87*Power(t2,3)) + 
               t1*(-578 + 2187*t2 - 3258*Power(t2,2) + 
                  2997*Power(t2,3) - 1136*Power(t2,4)) + 
               Power(t1,2)*(1002 - 1885*t2 + 1042*Power(t2,2) - 
                  80*Power(t2,3) - 186*Power(t2,4)) - 
               Power(s2,4)*(355 + 361*Power(t1,3) + 
                  Power(t1,2)*(1962 - 4579*t2) + 365*t2 - 
                  476*Power(t2,2) - 140*Power(t2,3) + 
                  t1*(3250 - 4869*t2 + 1790*Power(t2,2))) + 
               Power(s2,2)*(1259 - 667*Power(t1,5) - 4913*t2 + 
                  7076*Power(t2,2) - 3284*Power(t2,3) + 
                  219*Power(t2,4) + Power(t1,4)*(-1855 + 3988*t2) + 
                  Power(t1,3)*(-3266 + 6270*t2 - 2186*Power(t2,2)) + 
                  t1*(5759 - 18411*t2 + 18792*Power(t2,2) - 
                     4851*Power(t2,3)) + 
                  Power(t1,2)*
                   (1786 - 11626*t2 + 6844*Power(t2,2) - 81*Power(t2,3))\
) + Power(s2,3)*(809*Power(t1,4) + Power(t1,3)*(2724 - 5944*t2) + 
                  Power(t1,2)*(5118 - 8232*t2 + 3059*Power(t2,2)) + 
                  t1*(25 + 4886*t2 - 3309*Power(t2,2) - 
                     224*Power(t2,3)) + 
                  2*(-744 + 2631*t2 - 3082*Power(t2,2) + 
                     846*Power(t2,3))) + 
               s2*(89 + 244*Power(t1,6) + Power(t1,5)*(543 - 1246*t2) + 
                  244*t2 + 96*Power(t2,2) - 1634*Power(t2,3) + 
                  993*Power(t2,4) + 
                  Power(t1,4)*(523 - 2020*t2 + 545*Power(t2,2)) + 
                  4*Power(t1,3)*
                   (-522 + 2416*t2 - 1338*Power(t2,2) + 63*Power(t2,3)) \
+ 4*Power(t1,2)*(-1517 + 4700*t2 - 4390*Power(t2,2) + 
                     1013*Power(t2,3)) + 
                  t1*(-2491 + 7174*t2 - 8423*Power(t2,2) + 
                     3518*Power(t2,3) - 28*Power(t2,4)))) + 
            Power(s1,2)*(-37 - 351*t2 + 1326*Power(t2,2) - 
               785*Power(t2,3) - 607*Power(t2,4) + 454*Power(t2,5) + 
               3*Power(t1,7)*(4 + t2) + 
               Power(s2,7)*(-20 - 28*t1 + 97*t2) - 
               Power(t1,6)*(15 + 107*t2 + 20*Power(t2,2)) + 
               Power(s2,6)*(171 + 181*Power(t1,2) + t1*(168 - 686*t2) - 
                  155*t2 + 113*Power(t2,2)) + 
               Power(t1,5)*(95 + 539*t2 - 193*Power(t2,2) + 
                  19*Power(t2,3)) - 
               Power(t1,4)*(91 - 957*t2 + 1093*Power(t2,2) + 
                  38*Power(t2,3)) + 
               Power(t1,3)*(390 - 4139*t2 + 4829*Power(t2,2) - 
                  1580*Power(t2,3) + 238*Power(t2,4)) + 
               Power(t1,2)*(1575 - 8247*t2 + 13401*Power(t2,2) - 
                  7708*Power(t2,3) + 1185*Power(t2,4)) + 
               t1*(935 - 3367*t2 + 5922*Power(t2,2) - 
                  5446*Power(t2,3) + 1651*Power(t2,4) + 173*Power(t2,5)) \
+ Power(s2,5)*(241 - 485*Power(t1,3) - 1198*t2 + 866*Power(t2,2) + 
                  60*Power(t2,3) + Power(t1,2)*(-532 + 1955*t2) + 
                  t1*(-657 + 698*t2 - 612*Power(t2,2))) + 
               Power(s2,3)*(1041 - 550*Power(t1,5) - 1479*t2 + 
                  707*Power(t2,2) - 342*Power(t2,3) + 196*Power(t2,4) + 
                  Power(t1,4)*(-780 + 2363*t2) + 
                  Power(t1,3)*(48 + 766*t2 - 1017*Power(t2,2)) + 
                  t1*(6429 - 15448*t2 + 14892*Power(t2,2) - 
                     4360*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (-2243 + 7868*t2 - 4601*Power(t2,2) + 50*Power(t2,3))\
) + Power(s2,4)*(-1607 + 690*Power(t1,4) + 
                  Power(t1,3)*(860 - 2897*t2) + 4049*t2 - 
                  4126*Power(t2,2) + 1294*Power(t2,3) + 
                  Power(t1,2)*(733 - 1099*t2 + 1193*Power(t2,2)) - 
                  t1*(1972 - 7373*t2 + 4690*Power(t2,2) + 
                     90*Power(t2,3))) + 
               s2*(-639 - 41*Power(t1,7) + 3516*t2 - 9157*Power(t2,2) + 
                  10032*Power(t2,3) - 3658*Power(t2,4) + 
                  38*Power(t2,5) + Power(t1,6)*(-108 + 169*t2) + 
                  Power(t1,5)*(257 + 256*t2 + 25*Power(t2,2)) + 
                  Power(t1,3)*
                   (3345 - 9098*t2 + 8560*Power(t2,2) - 
                     1524*Power(t2,3)) + 
                  Power(t1,4)*
                   (1075 - 5662*t2 + 2806*Power(t2,2) - 
                     138*Power(t2,3)) + 
                  t1*(-2933 + 14950*t2 - 26406*Power(t2,2) + 
                     18146*Power(t2,3) - 3885*Power(t2,4)) + 
                  Power(t1,2)*
                   (332 + 8157*t2 - 11490*Power(t2,2) + 
                     3964*Power(t2,3) - 488*Power(t2,4))) + 
               Power(s2,2)*(850 + 233*Power(t1,6) + 
                  Power(t1,5)*(400 - 1004*t2) - 4872*t2 + 
                  10921*Power(t2,2) - 9439*Power(t2,3) + 
                  2462*Power(t2,4) + 
                  Power(t1,4)*(-537 - 359*t2 + 318*Power(t2,2)) + 
                  Power(t1,3)*
                   (-3925 + 14684*t2 - 7991*Power(t2,2) + 
                     249*Power(t2,3)) + 
                  Power(t1,2)*
                   (-8076 + 19540*t2 - 18233*Power(t2,2) + 
                     4628*Power(t2,3)) + 
                  t1*(-1789 - 2395*t2 + 5677*Power(t2,2) - 
                     1856*Power(t2,3) + 27*Power(t2,4)))) + 
            s1*(56 - 4*Power(t1,8) - 351*t2 + 2109*Power(t2,2) - 
               4806*Power(t2,3) + 4221*Power(t2,4) - 1173*Power(t2,5) - 
               56*Power(t2,6) + 3*Power(t1,7)*(5 + 4*t2) + 
               2*Power(s2,7)*(6 + 6*t1 - 16*t2 - Power(t2,2)) - 
               Power(t1,6)*(32 + 17*t2 + 14*Power(t2,2)) + 
               Power(t1,5)*(193 - 325*t2 + 197*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(t1,4)*(412 - 169*t2 - 473*Power(t2,2) + 
                  141*Power(t2,3) - 38*Power(t2,4)) + 
               Power(t1,3)*(-607 + 1654*t2 - 1968*Power(t2,2) + 
                  939*Power(t2,3) + 10*Power(t2,4)) + 
               t1*(383 - 3589*t2 + 8963*Power(t2,2) - 9411*Power(t2,3) + 
                  4478*Power(t2,4) - 792*Power(t2,5)) - 
               Power(t1,2)*(448 + 815*t2 - 3138*Power(t2,2) + 
                  2911*Power(t2,3) - 1117*Power(t2,4) + 207*Power(t2,5)) \
+ Power(s2,6)*(48 - 266*t2 + 109*Power(t2,2) + 7*Power(t2,3) - 
                  4*Power(t1,2)*(18 + t2) + t1*t2*(188 + 17*t2)) + 
               Power(s2,5)*(-325 + 668*t2 - 777*Power(t2,2) + 
                  250*Power(t2,3) + 4*Power(t1,3)*(46 + 5*t2) + 
                  t1*(-392 + 1776*t2 - 753*Power(t2,2)) - 
                  10*Power(t1,2)*(22 + 43*t2 + 6*Power(t2,2))) + 
               Power(s2,4)*(-323 + 1718*t2 - 2094*Power(t2,2) + 
                  520*Power(t2,3) + 35*Power(t2,4) - 
                  20*Power(t1,4)*(13 + 2*t2) + 
                  Power(t1,3)*(655 + 492*t2 + 110*Power(t2,2)) + 
                  t1*(1155 - 2592*t2 + 3105*Power(t2,2) - 
                     929*Power(t2,3)) + 
                  Power(t1,2)*
                   (967 - 4303*t2 + 1847*Power(t2,2) - 50*Power(t2,3))) + 
               Power(s2,2)*(-1141 + 3675*t2 - 5838*Power(t2,2) + 
                  4391*Power(t2,3) - 1148*Power(t2,4) + 3*Power(t2,5) - 
                  4*Power(t1,6)*(28 + 5*t2) + 
                  Power(t1,5)*(538 + 124*t2 + 57*Power(t2,2)) + 
                  Power(t1,3)*
                   (1078 - 2453*t2 + 2880*Power(t2,2) - 485*Power(t2,3)) \
+ Power(t1,4)*(389 - 2742*t2 + 1110*Power(t2,2) - 45*Power(t2,3)) + 
                  t1*(-5720 + 18069*t2 - 24317*Power(t2,2) + 
                     14324*Power(t2,3) - 2780*Power(t2,4)) + 
                  Power(t1,2)*
                   (-3352 + 13627*t2 - 14892*Power(t2,2) + 
                     4453*Power(t2,3) - 321*Power(t2,4))) + 
               Power(s2,3)*(1709 - 6002*t2 + 9324*Power(t2,2) - 
                  6152*Power(t2,3) + 1327*Power(t2,4) + 
                  20*Power(t1,5)*(11 + 2*t2) - 
                  2*Power(t1,4)*(420 + 154*t2 + 55*Power(t2,2)) + 
                  Power(t1,3)*
                   (-997 + 4930*t2 - 2099*Power(t2,2) + 80*Power(t2,3)) \
+ Power(t1,2)*(-1525 + 3616*t2 - 4445*Power(t2,2) + 1156*Power(t2,3)) + 
                  t1*(2329 - 9336*t2 + 10184*Power(t2,2) - 
                     2876*Power(t2,3) + 96*Power(t2,4))) + 
               s2*(-36 + 854*t2 - 3771*Power(t2,2) + 6726*Power(t2,3) - 
                  5195*Power(t2,4) + 1390*Power(t2,5) + 
                  4*Power(t1,7)*(8 + t2) - 
                  2*Power(t1,6)*(80 + 23*t2 + 6*Power(t2,2)) + 
                  Power(t1,5)*
                   (17 + 622*t2 - 200*Power(t2,2) + 8*Power(t2,3)) - 
                  2*Power(t1,4)*
                   (288 - 543*t2 + 480*Power(t2,2) + 16*Power(t2,3)) + 
                  Power(t1,3)*
                   (934 - 5840*t2 + 7275*Power(t2,2) - 
                     2238*Power(t2,3) + 228*Power(t2,4)) + 
                  Power(t1,2)*
                   (4620 - 13774*t2 + 17059*Power(t2,2) - 
                     9136*Power(t2,3) + 1421*Power(t2,4)) + 
                  t1*(1969 - 3306*t2 + 1953*Power(t2,2) - 
                     276*Power(t2,3) - 424*Power(t2,4) + 268*Power(t2,5)))\
)))*R2q(1 - s2 + t1 - t2))/
     ((-1 + t1)*(1 - s2 + t1 - t2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*(-1 + t2)*(s - s1 + t2)*
       Power(-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2,2)) - 
    (8*(-288 + 789*s2 - 593*Power(s2,2) - 80*Power(s2,3) + 
         225*Power(s2,4) - 48*Power(s2,5) - 15*Power(s2,6) + 
         12*Power(s2,7) - Power(s2,8) - Power(s2,9) - 366*t1 + 
         181*s2*t1 + 743*Power(s2,2)*t1 - 626*Power(s2,3)*t1 + 
         107*Power(s2,4)*t1 + 12*Power(s2,5)*t1 - 75*Power(s2,6)*t1 + 
         18*Power(s2,7)*t1 + 7*Power(s2,8)*t1 - Power(s2,9)*t1 + 
         322*Power(t1,2) - 1144*s2*Power(t1,2) + 
         617*Power(s2,2)*Power(t1,2) + 28*Power(s2,3)*Power(t1,2) + 
         32*Power(s2,4)*Power(t1,2) + 212*Power(s2,5)*Power(t1,2) - 
         59*Power(s2,6)*Power(t1,2) - 16*Power(s2,7)*Power(t1,2) + 
         8*Power(s2,8)*Power(t1,2) + 472*Power(t1,3) - 
         208*s2*Power(t1,3) - 301*Power(s2,2)*Power(t1,3) + 
         34*Power(s2,3)*Power(t1,3) - 314*Power(s2,4)*Power(t1,3) + 
         36*Power(s2,5)*Power(t1,3) - 9*Power(s2,6)*Power(t1,3) - 
         30*Power(s2,7)*Power(t1,3) - 8*Power(t1,4) + 
         330*s2*Power(t1,4) - 189*Power(s2,2)*Power(t1,4) + 
         216*Power(s2,3)*Power(t1,4) + 145*Power(s2,4)*Power(t1,4) + 
         124*Power(s2,5)*Power(t1,4) + 70*Power(s2,6)*Power(t1,4) - 
         116*Power(t1,5) + 178*s2*Power(t1,5) - 
         11*Power(s2,2)*Power(t1,5) - 334*Power(s2,3)*Power(t1,5) - 
         289*Power(s2,4)*Power(t1,5) - 112*Power(s2,5)*Power(t1,5) - 
         52*Power(t1,6) - 64*s2*Power(t1,6) + 
         307*Power(s2,2)*Power(t1,6) + 348*Power(s2,3)*Power(t1,6) + 
         126*Power(s2,4)*Power(t1,6) + 24*Power(t1,7) - 
         136*s2*Power(t1,7) - 239*Power(s2,2)*Power(t1,7) - 
         98*Power(s2,3)*Power(t1,7) + 24*Power(t1,8) + 
         89*s2*Power(t1,8) + 50*Power(s2,2)*Power(t1,8) - 
         14*Power(t1,9) - 15*s2*Power(t1,9) + 2*Power(t1,10) + 
         Power(s,10)*(-1 + s1)*(2 - 2*s2 + 3*t1 - 3*t2)*
          Power(t1 - t2,2) + 1962*t2 - 4950*s2*t2 + 3664*Power(s2,2)*t2 + 
         170*Power(s2,3)*t2 - 1389*Power(s2,4)*t2 + 494*Power(s2,5)*t2 + 
         155*Power(s2,6)*t2 - 106*Power(s2,7)*t2 - 9*Power(s2,8)*t2 + 
         8*Power(s2,9)*t2 + Power(s2,10)*t2 + 1602*t1*t2 - 194*s2*t1*t2 - 
         3571*Power(s2,2)*t1*t2 + 3652*Power(s2,3)*t1*t2 - 
         1565*Power(s2,4)*t1*t2 - 440*Power(s2,5)*t1*t2 + 
         611*Power(s2,6)*t1*t2 - 20*Power(s2,7)*t1*t2 - 
         69*Power(s2,8)*t1*t2 - 6*Power(s2,9)*t1*t2 - 
         2708*Power(t1,2)*t2 + 5752*s2*Power(t1,2)*t2 - 
         2928*Power(s2,2)*Power(t1,2)*t2 + 
         1296*Power(s2,3)*Power(t1,2)*t2 + 
         465*Power(s2,4)*Power(t1,2)*t2 - 
         1472*Power(s2,5)*Power(t1,2)*t2 + 
         266*Power(s2,6)*Power(t1,2)*t2 + 
         272*Power(s2,7)*Power(t1,2)*t2 + 17*Power(s2,8)*Power(t1,2)*t2 - 
         2285*Power(t1,3)*t2 + 148*s2*Power(t1,3)*t2 + 
         830*Power(s2,2)*Power(t1,3)*t2 - 
         410*Power(s2,3)*Power(t1,3)*t2 + 
         1739*Power(s2,4)*Power(t1,3)*t2 - 
         712*Power(s2,5)*Power(t1,3)*t2 - 
         620*Power(s2,6)*Power(t1,3)*t2 - 34*Power(s2,7)*Power(t1,3)*t2 + 
         517*Power(t1,4)*t2 - 1758*s2*Power(t1,4)*t2 + 
         515*Power(s2,2)*Power(t1,4)*t2 - 
         746*Power(s2,3)*Power(t1,4)*t2 + 
         900*Power(s2,4)*Power(t1,4)*t2 + 
         864*Power(s2,5)*Power(t1,4)*t2 + 56*Power(s2,6)*Power(t1,4)*t2 + 
         703*Power(t1,5)*t2 - 390*s2*Power(t1,5)*t2 - 
         415*Power(s2,2)*Power(t1,5)*t2 - 
         596*Power(s2,3)*Power(t1,5)*t2 - 
         718*Power(s2,4)*Power(t1,5)*t2 - 70*Power(s2,5)*Power(t1,5)*t2 + 
         105*Power(t1,6)*t2 + 548*s2*Power(t1,6)*t2 + 
         190*Power(s2,2)*Power(t1,6)*t2 + 
         304*Power(s2,3)*Power(t1,6)*t2 + 56*Power(s2,4)*Power(t1,6)*t2 - 
         159*Power(t1,7)*t2 - 16*s2*Power(t1,7)*t2 - 
         12*Power(s2,2)*Power(t1,7)*t2 - 22*Power(s2,3)*Power(t1,7)*t2 - 
         3*Power(t1,8)*t2 - 40*s2*Power(t1,8)*t2 - 
         Power(s2,2)*Power(t1,8)*t2 + 11*Power(t1,9)*t2 + 
         4*s2*Power(t1,9)*t2 - Power(t1,10)*t2 - 5857*Power(t2,2) + 
         14163*s2*Power(t2,2) - 10990*Power(s2,2)*Power(t2,2) + 
         177*Power(s2,3)*Power(t2,2) + 4328*Power(s2,4)*Power(t2,2) - 
         1503*Power(s2,5)*Power(t2,2) - 638*Power(s2,6)*Power(t2,2) + 
         269*Power(s2,7)*Power(t2,2) + 63*Power(s2,8)*Power(t2,2) - 
         10*Power(s2,9)*Power(t2,2) - 2*Power(s2,10)*Power(t2,2) - 
         2540*t1*Power(t2,2) + 159*s2*t1*Power(t2,2) + 
         6819*Power(s2,2)*t1*Power(t2,2) - 
         11385*Power(s2,3)*t1*Power(t2,2) + 
         5450*Power(s2,4)*t1*Power(t2,2) + 
         2394*Power(s2,5)*t1*Power(t2,2) - 
         1633*Power(s2,6)*t1*Power(t2,2) - 
         311*Power(s2,7)*t1*Power(t2,2) + 76*Power(s2,8)*t1*Power(t2,2) + 
         11*Power(s2,9)*t1*Power(t2,2) + 7995*Power(t1,2)*Power(t2,2) - 
         10981*s2*Power(t1,2)*Power(t2,2) + 
         8455*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         6545*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         3451*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         4189*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         613*Power(s2,6)*Power(t1,2)*Power(t2,2) - 
         269*Power(s2,7)*Power(t1,2)*Power(t2,2) - 
         22*Power(s2,8)*Power(t1,2)*Power(t2,2) + 
         3781*Power(t1,3)*Power(t2,2) + 757*s2*Power(t1,3)*Power(t2,2) + 
         1465*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         2200*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         5631*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         581*Power(s2,5)*Power(t1,3)*Power(t2,2) + 
         577*Power(s2,6)*Power(t1,3)*Power(t2,2) + 
         14*Power(s2,7)*Power(t1,3)*Power(t2,2) - 
         2155*Power(t1,4)*Power(t2,2) + 2420*s2*Power(t1,4)*Power(t2,2) - 
         264*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         3879*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         185*Power(s2,4)*Power(t1,4)*Power(t2,2) - 
         809*Power(s2,5)*Power(t1,4)*Power(t2,2) + 
         14*Power(s2,6)*Power(t1,4)*Power(t2,2) - 
         1287*Power(t1,5)*Power(t2,2) - 410*s2*Power(t1,5)*Power(t2,2) - 
         895*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         167*Power(s2,3)*Power(t1,5)*Power(t2,2) + 
         745*Power(s2,4)*Power(t1,5)*Power(t2,2) - 
         28*Power(s2,5)*Power(t1,5)*Power(t2,2) + 
         169*Power(t1,6)*Power(t2,2) - 353*s2*Power(t1,6)*Power(t2,2) - 
         229*Power(s2,2)*Power(t1,6)*Power(t2,2) - 
         431*Power(s2,3)*Power(t1,6)*Power(t2,2) + 
         14*Power(s2,4)*Power(t1,6)*Power(t2,2) + 
         175*Power(t1,7)*Power(t2,2) + 117*s2*Power(t1,7)*Power(t2,2) + 
         139*Power(s2,2)*Power(t1,7)*Power(t2,2) + 
         2*Power(s2,3)*Power(t1,7)*Power(t2,2) - 
         24*Power(t1,8)*Power(t2,2) - 17*s2*Power(t1,8)*Power(t2,2) - 
         4*Power(s2,2)*Power(t1,8)*Power(t2,2) - 
         Power(t1,9)*Power(t2,2) + s2*Power(t1,9)*Power(t2,2) + 
         10016*Power(t2,3) - 24331*s2*Power(t2,3) + 
         19711*Power(s2,2)*Power(t2,3) + 13*Power(s2,3)*Power(t2,3) - 
         7751*Power(s2,4)*Power(t2,3) + 1761*Power(s2,5)*Power(t2,3) + 
         1189*Power(s2,6)*Power(t2,3) - 213*Power(s2,7)*Power(t2,3) - 
         83*Power(s2,8)*Power(t2,3) + 6*Power(s2,9)*Power(t2,3) + 
         2*Power(s2,10)*Power(t2,3) + 1493*t1*Power(t2,3) - 
         2736*s2*t1*Power(t2,3) - 8296*Power(s2,2)*t1*Power(t2,3) + 
         21295*Power(s2,3)*t1*Power(t2,3) - 
         7057*Power(s2,4)*t1*Power(t2,3) - 
         5170*Power(s2,5)*t1*Power(t2,3) + 
         1362*Power(s2,6)*t1*Power(t2,3) + 
         481*Power(s2,7)*t1*Power(t2,3) - 46*Power(s2,8)*t1*Power(t2,3) - 
         14*Power(s2,9)*t1*Power(t2,3) - 11043*Power(t1,2)*Power(t2,3) + 
         10461*s2*Power(t1,2)*Power(t2,3) - 
         17301*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         10326*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         9001*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         3697*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         1217*Power(s2,6)*Power(t1,2)*Power(t2,3) + 
         154*Power(s2,7)*Power(t1,2)*Power(t2,3) + 
         42*Power(s2,8)*Power(t1,2)*Power(t2,3) - 
         1820*Power(t1,3)*Power(t2,3) + 519*s2*Power(t1,3)*Power(t2,3) - 
         5828*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         7526*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         5394*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         1767*Power(s2,5)*Power(t1,3)*Power(t2,3) - 
         294*Power(s2,6)*Power(t1,3)*Power(t2,3) - 
         70*Power(s2,7)*Power(t1,3)*Power(t2,3) + 
         3238*Power(t1,4)*Power(t2,3) + 101*s2*Power(t1,4)*Power(t2,3) + 
         2437*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         4391*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         1605*Power(s2,4)*Power(t1,4)*Power(t2,3) + 
         350*Power(s2,5)*Power(t1,4)*Power(t2,3) + 
         70*Power(s2,6)*Power(t1,4)*Power(t2,3) + 
         697*Power(t1,5)*Power(t2,3) + 416*s2*Power(t1,5)*Power(t2,3) + 
         1822*Power(s2,2)*Power(t1,5)*Power(t2,3) + 
         903*Power(s2,3)*Power(t1,5)*Power(t2,3) - 
         266*Power(s2,4)*Power(t1,5)*Power(t2,3) - 
         42*Power(s2,5)*Power(t1,5)*Power(t2,3) - 
         347*Power(t1,6)*Power(t2,3) - 243*s2*Power(t1,6)*Power(t2,3) - 
         271*Power(s2,2)*Power(t1,6)*Power(t2,3) + 
         126*Power(s2,3)*Power(t1,6)*Power(t2,3) + 
         14*Power(s2,4)*Power(t1,6)*Power(t2,3) - 
         34*Power(t1,7)*Power(t2,3) + 17*s2*Power(t1,7)*Power(t2,3) - 
         34*Power(s2,2)*Power(t1,7)*Power(t2,3) - 
         2*Power(s2,3)*Power(t1,7)*Power(t2,3) + 
         8*Power(t1,8)*Power(t2,3) + 4*s2*Power(t1,8)*Power(t2,3) - 
         10708*Power(t2,4) + 27218*s2*Power(t2,4) - 
         21390*Power(s2,2)*Power(t2,4) - 1970*Power(s2,3)*Power(t2,4) + 
         7603*Power(s2,4)*Power(t2,4) - 523*Power(s2,5)*Power(t2,4) - 
         924*Power(s2,6)*Power(t2,4) + 18*Power(s2,7)*Power(t2,4) + 
         35*Power(s2,8)*Power(t2,4) + Power(s2,9)*Power(t2,4) + 
         125*t1*Power(t2,4) + 6889*s2*t1*Power(t2,4) + 
         9429*Power(s2,2)*t1*Power(t2,4) - 
         22086*Power(s2,3)*t1*Power(t2,4) + 
         2495*Power(s2,4)*t1*Power(t2,4) + 
         4351*Power(s2,5)*t1*Power(t2,4) - 
         169*Power(s2,6)*t1*Power(t2,4) - 
         210*Power(s2,7)*t1*Power(t2,4) - 4*Power(s2,8)*t1*Power(t2,4) + 
         7035*Power(t1,2)*Power(t2,4) - 6474*s2*Power(t1,2)*Power(t2,4) + 
         20553*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         4597*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         8407*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         570*Power(s2,5)*Power(t1,2)*Power(t2,4) + 
         547*Power(s2,6)*Power(t1,2)*Power(t2,4) + 
         3*Power(s2,7)*Power(t1,2)*Power(t2,4) - 
         1398*Power(t1,3)*Power(t2,4) - 4210*s2*Power(t1,3)*Power(t2,4) + 
         3952*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
         8238*Power(s2,3)*Power(t1,3)*Power(t2,4) - 
         956*Power(s2,4)*Power(t1,3)*Power(t2,4) - 
         810*Power(s2,5)*Power(t1,3)*Power(t2,4) + 
         10*Power(s2,6)*Power(t1,3)*Power(t2,4) - 
         1860*Power(t1,4)*Power(t2,4) - 1478*s2*Power(t1,4)*Power(t2,4) - 
         3922*Power(s2,2)*Power(t1,4)*Power(t2,4) + 
         874*Power(s2,3)*Power(t1,4)*Power(t2,4) + 
         745*Power(s2,4)*Power(t1,4)*Power(t2,4) - 
         25*Power(s2,5)*Power(t1,4)*Power(t2,4) + 
         151*Power(t1,5)*Power(t2,4) + 547*s2*Power(t1,5)*Power(t2,4) - 
         429*Power(s2,2)*Power(t1,5)*Power(t2,4) - 
         430*Power(s2,3)*Power(t1,5)*Power(t2,4) + 
         24*Power(s2,4)*Power(t1,5)*Power(t2,4) + 
         117*Power(t1,6)*Power(t2,4) + 98*s2*Power(t1,6)*Power(t2,4) + 
         145*Power(s2,2)*Power(t1,6)*Power(t2,4) - 
         11*Power(s2,3)*Power(t1,6)*Power(t2,4) - 
         6*Power(t1,7)*Power(t2,4) - 22*s2*Power(t1,7)*Power(t2,4) + 
         2*Power(s2,2)*Power(t1,7)*Power(t2,4) + 7262*Power(t2,5) - 
         19763*s2*Power(t2,5) + 13245*Power(s2,2)*Power(t2,5) + 
         3128*Power(s2,3)*Power(t2,5) - 3590*Power(s2,4)*Power(t2,5) - 
         317*Power(s2,5)*Power(t2,5) + 223*Power(s2,6)*Power(t2,5) + 
         20*Power(s2,7)*Power(t2,5) - 181*t1*Power(t2,5) - 
         6712*s2*t1*Power(t2,5) - 8578*Power(s2,2)*t1*Power(t2,5) + 
         11058*Power(s2,3)*t1*Power(t2,5) + 
         1115*Power(s2,4)*t1*Power(t2,5) - 
         1100*Power(s2,5)*t1*Power(t2,5) - 
         96*Power(s2,6)*t1*Power(t2,5) + 2*Power(s2,7)*t1*Power(t2,5) - 
         899*Power(t1,2)*Power(t2,5) + 4104*s2*Power(t1,2)*Power(t2,5) - 
         11798*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
         1368*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         2273*Power(s2,4)*Power(t1,2)*Power(t2,5) + 
         198*Power(s2,5)*Power(t1,2)*Power(t2,5) - 
         10*Power(s2,6)*Power(t1,2)*Power(t2,5) + 
         1692*Power(t1,3)*Power(t2,5) + 4216*s2*Power(t1,3)*Power(t2,5) + 
         508*Power(s2,2)*Power(t1,3)*Power(t2,5) - 
         2452*Power(s2,3)*Power(t1,3)*Power(t2,5) - 
         232*Power(s2,4)*Power(t1,3)*Power(t2,5) + 
         20*Power(s2,5)*Power(t1,3)*Power(t2,5) + 
         114*Power(t1,4)*Power(t2,5) + 213*s2*Power(t1,4)*Power(t2,5) + 
         1373*Power(s2,2)*Power(t1,4)*Power(t2,5) + 
         168*Power(s2,3)*Power(t1,4)*Power(t2,5) - 
         20*Power(s2,4)*Power(t1,4)*Power(t2,5) - 
         151*Power(t1,5)*Power(t2,5) - 320*s2*Power(t1,5)*Power(t2,5) - 
         72*Power(s2,2)*Power(t1,5)*Power(t2,5) + 
         10*Power(s2,3)*Power(t1,5)*Power(t2,5) + 
         3*Power(t1,6)*Power(t2,5) + 14*s2*Power(t1,6)*Power(t2,5) - 
         2*Power(s2,2)*Power(t1,6)*Power(t2,5) - 2973*Power(t2,6) + 
         8696*s2*Power(t2,6) - 3997*Power(s2,2)*Power(t2,6) - 
         1726*Power(s2,3)*Power(t2,6) + 537*Power(s2,4)*Power(t2,6) + 
         130*Power(s2,5)*Power(t2,6) + 5*Power(s2,6)*Power(t2,6) - 
         439*t1*Power(t2,6) + 2787*s2*t1*Power(t2,6) + 
         4142*Power(s2,2)*t1*Power(t2,6) - 
         1814*Power(s2,3)*t1*Power(t2,6) - 
         515*Power(s2,4)*t1*Power(t2,6) - 17*Power(s2,5)*t1*Power(t2,6) - 
         1158*Power(t1,2)*Power(t2,6) - 2222*s2*Power(t1,2)*Power(t2,6) + 
         2370*Power(s2,2)*Power(t1,2)*Power(t2,6) + 
         812*Power(s2,3)*Power(t1,2)*Power(t2,6) + 
         12*Power(s2,4)*Power(t1,2)*Power(t2,6) - 
         408*Power(t1,3)*Power(t2,6) - 1264*s2*Power(t1,3)*Power(t2,6) - 
         592*Power(s2,2)*Power(t1,3)*Power(t2,6) + 
         16*Power(s2,3)*Power(t1,3)*Power(t2,6) + 
         171*Power(t1,4)*Power(t2,6) + 158*s2*Power(t1,4)*Power(t2,6) - 
         25*Power(s2,2)*Power(t1,4)*Power(t2,6) + 
         7*Power(t1,5)*Power(t2,6) + 9*s2*Power(t1,5)*Power(t2,6) + 
         616*Power(t2,7) - 1952*s2*Power(t2,7) + 
         300*Power(s2,2)*Power(t2,7) + 278*Power(s2,3)*Power(t2,7) + 
         36*Power(s2,4)*Power(t2,7) + 2*Power(s2,5)*Power(t2,7) + 
         418*t1*Power(t2,7) - 358*s2*t1*Power(t2,7) - 
         668*Power(s2,2)*t1*Power(t2,7) - 90*Power(s2,3)*t1*Power(t2,7) - 
         10*Power(s2,4)*t1*Power(t2,7) + 498*Power(t1,2)*Power(t2,7) + 
         518*s2*Power(t1,2)*Power(t2,7) + 
         26*Power(s2,2)*Power(t1,2)*Power(t2,7) + 
         8*Power(s2,3)*Power(t1,2)*Power(t2,7) - 
         42*Power(t1,3)*Power(t2,7) + 46*s2*Power(t1,3)*Power(t2,7) + 
         6*Power(s2,2)*Power(t1,3)*Power(t2,7) - 
         18*Power(t1,4)*Power(t2,7) - 6*s2*Power(t1,4)*Power(t2,7) - 
         22*Power(t2,8) + 122*s2*Power(t2,8) + 
         50*Power(s2,2)*Power(t2,8) + 10*Power(s2,3)*Power(t2,8) - 
         120*t1*Power(t2,8) - 16*s2*t1*Power(t2,8) - 
         20*Power(s2,2)*t1*Power(t2,8) - 42*Power(t1,2)*Power(t2,8) - 
         14*s2*Power(t1,2)*Power(t2,8) + 8*Power(t1,3)*Power(t2,8) - 
         8*Power(t2,9) + 8*s2*Power(t2,9) + 8*t1*Power(t2,9) - 
         Power(s1,10)*Power(s2 - t1,3)*
          (5 + 4*Power(s2,2) + 5*Power(t1,2) + t1*(11 - 9*t2) - 9*t2 + 
            4*Power(t2,2) + s2*(-10 - 9*t1 + 8*t2)) - 
         Power(s1,9)*(s2 - t1)*
          (23*Power(s2,5) + Power(s2,4)*(-80 - 117*t1 + 42*t2) + 
            Power(s2,3)*(61 + 237*Power(t1,2) - 73*t2 + 
               15*Power(t2,2) - 4*t1*(-81 + 41*t2)) - 
            Power(s2,2)*(-15 + 239*Power(t1,3) + 
               Power(t1,2)*(490 - 238*t2) + 21*t2 - 10*Power(t2,2) + 
               4*Power(t2,3) + t1*(183 - 212*t2 + 38*Power(t2,2))) - 
            t1*(24*Power(t1,4) + Power(t1,3)*(82 - 36*t2) + 
               3*Power(-1 + t2,2)*(-5 + 4*t2) + 
               Power(t1,2)*(59 - 62*t2 + 6*Power(t2,2)) + 
               t1*(-21 + 35*t2 - 20*Power(t2,2) + 6*Power(t2,3))) + 
            s2*(120*Power(t1,4) + 3*Power(-1 + t2,2)*(-5 + 4*t2) - 
               8*Power(t1,3)*(-41 + 19*t2) + 
               Power(t1,2)*(181 - 201*t2 + 29*Power(t2,2)) + 
               t1*(-35 + 53*t2 - 27*Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s1,8)*(-56*Power(s2,7) + 
            Power(s2,6)*(201 + 380*t1 - 89*t2) - 
            Power(s2,5)*(99 + 1095*Power(t1,2) + t1*(1133 - 475*t2) - 
               42*t2 + 10*Power(t2,2)) + 
            Power(s2,4)*(-267 + 1735*Power(t1,3) + 
               Power(t1,2)*(2615 - 1024*t2) + 482*t2 - 
               252*Power(t2,2) + 23*Power(t2,3) + 
               t1*(388 + 7*t2 - 14*Power(t2,2))) - 
            Power(s2,3)*(-257 + 1630*Power(t1,4) + 564*t2 - 
               423*Power(t2,2) + 116*Power(t2,3) - 
               23*Power(t1,3)*(-137 + 49*t2) + 
               Power(t1,2)*(526 + 491*t2 - 163*Power(t2,2)) + 
               8*t1*(-142 + 266*t2 - 145*Power(t2,2) + 14*Power(t2,3))) \
+ t1*(35*Power(t1,6) + Power(t1,5)*(94 - 15*t2) + 
               3*Power(-1 + t2,3)*(-5 + 4*t2) + 
               Power(t1,4)*(-43 + 216*t2 - 60*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-7 + 15*t2 + 18*Power(t2,2)) + 
               Power(t1,3)*(-342 + 693*t2 - 405*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(t1,2)*(-280 + 601*t2 - 428*Power(t2,2) + 
                  107*Power(t2,3))) + 
            Power(s2,2)*(906*Power(t1,5) + 
               Power(t1,4)*(2078 - 652*t2) + 
               Power(t1,3)*(241 + 1009*t2 - 304*Power(t2,2)) + 
               Power(-1 + t2,2)*(-15 + 28*t2 + 13*Power(t2,2)) + 
               2*Power(t1,2)*
                (-906 + 1750*t2 - 983*Power(t2,2) + 97*Power(t2,3)) + 
               t1*(-795 + 1735*t2 - 1286*Power(t2,2) + 
                  349*Power(t2,3) - 3*Power(t2,4))) + 
            s2*(-275*Power(t1,6) - 3*Power(-1 + t2,3)*(-5 + 4*t2) + 
               2*Power(t1,5)*(-352 + 89*t2) - 
               t1*Power(-1 + t2,2)*(-23 + 45*t2 + 30*Power(t2,2)) + 
               3*Power(t1,4)*(13 - 261*t2 + 75*Power(t2,2)) + 
               Power(t1,3)*(1285 - 2547*t2 + 1463*Power(t2,2) - 
                  145*Power(t2,3)) + 
               Power(t1,2)*(817 - 1768*t2 + 1285*Power(t2,2) - 
                  336*Power(t2,3) + 2*Power(t2,4)))) + 
         Power(s1,7)*(-73*Power(s2,8) - 8*Power(t1,8) + 
            Power(t1,7)*(43 - 88*t2) + 
            Power(s2,7)*(215 + 533*t1 - 86*t2) - 
            Power(-1 + t2,4)*(-5 + 4*t2) - 
            2*t1*Power(-1 + t2,3)*(-16 + 18*t2 + 9*Power(t2,2)) + 
            Power(t1,6)*(458 - 649*t2 + 172*Power(t2,2)) - 
            Power(t1,2)*Power(-1 + t2,2)*
             (409 - 519*t2 + 287*Power(t2,2)) + 
            Power(t1,5)*(961 - 1541*t2 + 513*Power(t2,2) - 
               68*Power(t2,3)) + 
            Power(t1,3)*(-401 + 1664*t2 - 2247*Power(t2,2) + 
               1190*Power(t2,3) - 206*Power(t2,4)) + 
            Power(t1,4)*(505 - 510*t2 - 217*Power(t2,2) + 
               259*Power(t2,3) - 8*Power(t2,4)) + 
            Power(s2,6)*(212 - 1669*Power(t1,2) - 338*t2 + 
               49*Power(t2,2) + t1*(-1259 + 422*t2)) + 
            Power(s2,5)*(-1108 + 2913*Power(t1,3) + 
               Power(t1,2)*(3006 - 719*t2) + 1824*t2 - 
               751*Power(t2,2) + 64*Power(t2,3) + 
               t1*(-1567 + 2519*t2 - 473*Power(t2,2))) + 
            Power(s2,4)*(808 - 3075*Power(t1,4) - 1458*t2 + 
               816*Power(t2,2) - 139*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,3)*(-3691 + 288*t2) + 
               Power(t1,2)*(4629 - 7375*t2 + 1581*Power(t2,2)) + 
               t1*(5515 - 9212*t2 + 3823*Power(t2,2) - 377*Power(t2,3))) \
+ Power(s2,3)*(267 + 1983*Power(t1,5) - 1000*t2 + 1261*Power(t2,2) - 
               674*Power(t2,3) + 146*Power(t2,4) + 
               Power(t1,4)*(2359 + 608*t2) + 
               Power(t1,3)*(-7063 + 11040*t2 - 2567*Power(t2,2)) + 
               Power(t1,2)*(-10855 + 18218*t2 - 7455*Power(t2,2) + 
                  806*Power(t2,3)) + 
               t1*(-3028 + 5173*t2 - 2529*Power(t2,2) + 
                  283*Power(t2,3) - 15*Power(t2,4))) + 
            Power(s2,2)*(-743*Power(t1,6) - 
               Power(t1,5)*(621 + 886*t2) + 
               5*Power(t1,4)*(1181 - 1798*t2 + 442*Power(t2,2)) + 
               Power(t1,3)*(10558 - 17637*t2 + 6958*Power(t2,2) - 
                  805*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-365 + 461*t2 - 276*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(-918 + 3598*t2 - 4673*Power(t2,2) + 
                  2476*Power(t2,3) - 483*Power(t2,4)) + 
               Power(t1,2)*(4125 - 6439*t2 + 2336*Power(t2,2) + 
                  143*Power(t2,3) + 9*Power(t2,4))) + 
            s2*(139*Power(t1,7) + Power(t1,6)*(-52 + 461*t2) + 
               Power(t1,5)*(-2574 + 3793*t2 - 972*Power(t2,2)) + 
               Power(-1 + t2,3)*(-35 + 42*t2 + 15*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*
                (-774 + 981*t2 - 565*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,4)*(-5071 + 8348*t2 - 3088*Power(t2,2) + 
                  380*Power(t2,3)) + 
               2*Power(t1,3)*
                (-1205 + 1617*t2 - 203*Power(t2,2) - 273*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(t1,2)*(1056 - 4278*t2 + 5683*Power(t2,2) - 
                  3008*Power(t2,3) + 547*Power(t2,4)))) + 
         Power(s1,6)*(-52*Power(s2,9) - 21*Power(t1,9) + 
            Power(s2,8)*(78 + 389*t1 - 13*t2) + 
            Power(t1,8)*(-83 + 129*t2) + 
            Power(t1,7)*(-164 + 347*t2 - 156*Power(t2,2)) + 
            Power(-1 + t2,4)*(-15 + 16*t2 + 6*Power(t2,2)) + 
            t1*Power(-1 + t2,3)*(244 - 347*t2 + 277*Power(t2,2)) + 
            Power(t1,6)*(72 - 657*t2 + 406*Power(t2,2) + 
               24*Power(t2,3)) + 
            2*Power(t1,2)*Power(-1 + t2,2)*
             (-29 + 433*t2 - 428*Power(t2,2) + 189*Power(t2,3)) + 
            Power(t1,5)*(1507 - 4573*t2 + 4303*Power(t2,2) - 
               1046*Power(t2,3) + 24*Power(t2,4)) + 
            Power(t1,4)*(2866 - 8424*t2 + 8439*Power(t2,2) - 
               3258*Power(t2,3) + 366*Power(t2,4)) + 
            Power(t1,3)*(1696 - 4909*t2 + 4610*Power(t2,2) - 
               1290*Power(t2,3) - 147*Power(t2,4) + 40*Power(t2,5)) - 
            Power(s2,7)*(-588 + 1234*Power(t1,2) + 715*t2 - 
               138*Power(t2,2) + t1*(358 + 139*t2)) + 
            Power(s2,6)*(-1353 + 2126*Power(t1,3) + 2067*t2 - 
               799*Power(t2,2) + 107*Power(t2,3) + 
               Power(t1,2)*(397 + 1262*t2) - 
               2*t1*(2007 - 2569*t2 + 588*Power(t2,2))) + 
            Power(s2,5)*(-334 - 2074*Power(t1,4) + 
               Power(t1,3)*(738 - 3899*t2) + 1268*t2 - 
               1326*Power(t2,2) + 301*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,2)*(11409 - 15275*t2 + 3962*Power(t2,2)) + 
               t1*(7006 - 10388*t2 + 3962*Power(t2,2) - 684*Power(t2,3))\
) + Power(s2,4)*(2756 + 992*Power(t1,5) - 7757*t2 + 7611*Power(t2,2) - 
               3060*Power(t2,3) + 439*Power(t2,4) + 
               5*Power(t1,4)*(-515 + 1268*t2) + 
               Power(t1,3)*(-17420 + 24367*t2 - 7004*Power(t2,2)) + 
               Power(t1,2)*(-14335 + 19986*t2 - 7262*Power(t2,2) + 
                  1652*Power(t2,3)) + 
               t1*(2852 - 9821*t2 + 9965*Power(t2,2) - 
                  2413*Power(t2,3) - 36*Power(t2,4))) + 
            Power(s2,3)*(-1891 + 26*Power(t1,6) + 
               Power(t1,5)*(3142 - 6017*t2) + 5677*t2 - 
               6265*Power(t2,2) + 3122*Power(t2,3) - 649*Power(t2,4) + 
               6*Power(t2,5) + 
               Power(t1,4)*(15290 - 22453*t2 + 7086*Power(t2,2)) + 
               Power(t1,3)*(14427 - 17643*t2 + 5598*Power(t2,2) - 
                  1946*Power(t2,3)) + 
               t1*(-11331 + 32606*t2 - 32641*Power(t2,2) + 
                  13231*Power(t2,3) - 1821*Power(t2,4)) + 
               Power(t1,2)*(-8144 + 26684*t2 - 26519*Power(t2,2) + 
                  6605*Power(t2,3) + 16*Power(t2,4))) + 
            Power(s2,2)*(-286*Power(t1,7) + 
               3*Power(t1,6)*(-659 + 1118*t2) - 
               2*Power(t1,5)*(3789 - 5926*t2 + 2056*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (1 + 464*t2 - 424*Power(t2,2) + 289*Power(t2,3)) + 
               Power(t1,4)*(-7036 + 5888*t2 - 857*Power(t2,2) + 
                  1161*Power(t2,3)) + 
               Power(t1,3)*(10575 - 33550*t2 + 32750*Power(t2,2) - 
                  8221*Power(t2,3) + 68*Power(t2,4)) + 
               Power(t1,2)*(17273 - 50402*t2 + 50921*Power(t2,2) - 
                  20547*Power(t2,3) + 2689*Power(t2,4)) + 
               t1*(5593 - 16742*t2 + 17922*Power(t2,2) - 
                  8156*Power(t2,3) + 1390*Power(t2,4) - 7*Power(t2,5))) \
+ s2*(134*Power(t1,8) + Power(t1,7)*(638 - 1017*t2) + 
               Power(t1,6)*(1889 - 3261*t2 + 1262*Power(t2,2)) + 
               Power(t1,5)*(1219 + 747*t2 - 1048*Power(t2,2) - 
                  314*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-223 + 313*t2 - 266*Power(t2,2) + 2*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-44 + 1296*t2 - 1251*Power(t2,2) + 659*Power(t2,3)) + 
               Power(t1,3)*(-11564 + 33977*t2 - 34330*Power(t2,2) + 
                  13634*Power(t2,3) - 1673*Power(t2,4)) + 
               Power(t1,4)*(-6456 + 19992*t2 - 19173*Power(t2,2) + 
                  4774*Power(t2,3) - 80*Power(t2,4)) - 
               Power(t1,2)*(5391 - 15935*t2 + 16181*Power(t2,2) - 
                  6230*Power(t2,3) + 543*Power(t2,4) + 50*Power(t2,5)))) \
- Power(s1,5)*(17*Power(s2,10) - 16*Power(t1,10) + 
            12*Power(t1,9)*(-1 + 5*t2) - 
            2*Power(s2,9)*(-12 + 55*t1 + 25*t2) + 
            Power(t1,8)*(187 - 104*t2 - 42*Power(t2,2)) + 
            Power(-1 + t2,4)*(50 - 82*t2 + 91*Power(t2,2)) + 
            Power(t1,7)*(368 - 1361*t2 + 774*Power(t2,2) - 
               26*Power(t2,3)) + 
            2*t1*Power(-1 + t2,3)*
             (71 + 67*t2 - 100*Power(t2,2) + 149*Power(t2,3)) + 
            Power(t1,6)*(491 - 2314*t2 + 2661*Power(t2,2) - 
               846*Power(t2,3) + 24*Power(t2,4)) + 
            Power(t1,2)*Power(-1 + t2,2)*
             (-2146 + 4722*t2 - 2980*Power(t2,2) + 643*Power(t2,3) + 
               72*Power(t2,4)) + 
            Power(t1,5)*(171 + 814*t2 - 1720*Power(t2,2) + 
               770*Power(t2,3) + 80*Power(t2,4)) + 
            Power(t1,4)*(-2122 + 10258*t2 - 17448*Power(t2,2) + 
               12139*Power(t2,3) - 2940*Power(t2,4) + 120*Power(t2,5)) + 
            Power(t1,3)*(-3941 + 16575*t2 - 27442*Power(t2,2) + 
               21751*Power(t2,3) - 7839*Power(t2,4) + 896*Power(t2,5)) + 
            Power(s2,8)*(-433 + 240*Power(t1,2) + 535*t2 - 
               163*Power(t2,2) + 6*t1*(-51 + 101*t2)) + 
            Power(s2,7)*(277 - 12*Power(t1,3) - 319*t2 + 
               90*Power(t2,2) - 108*Power(t2,3) - 
               4*Power(t1,2)*(-369 + 728*t2) + 
               t1*(2872 - 3805*t2 + 1389*Power(t2,2))) - 
            Power(s2,6)*(-2339 + 966*Power(t1,4) + 
               Power(t1,3)*(3786 - 7606*t2) + 5316*t2 - 
               4079*Power(t2,2) + 954*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,2)*(7828 - 11317*t2 + 4911*Power(t2,2)) + 
               t1*(660 + 684*t2 - 780*Power(t2,2) - 719*Power(t2,3))) - 
            Power(s2,4)*(51 + 2520*Power(t1,6) + 
               Power(t1,5)*(5634 - 12310*t2) - 1845*t2 + 
               4591*Power(t2,2) - 3509*Power(t2,3) + 692*Power(t2,4) + 
               13*Power(t2,5) + 
               5*Power(t1,4)*(1586 - 3325*t2 + 2175*Power(t2,2)) - 
               Power(t1,3)*(7148 - 28237*t2 + 16394*Power(t2,2) + 
                  2476*Power(t2,3)) + 
               t1*(-17123 + 43374*t2 - 39403*Power(t2,2) + 
                  16196*Power(t2,3) - 3179*Power(t2,4)) + 
               2*Power(t1,2)*
                (-15829 + 40241*t2 - 34403*Power(t2,2) + 
                  8922*Power(t2,3) + 7*Power(t2,4))) + 
            Power(s2,5)*(-3933 + 2184*Power(t1,5) + 9994*t2 - 
               9012*Power(t2,2) + 3618*Power(t2,3) - 672*Power(t2,4) - 
               24*Power(t1,4)*(-243 + 505*t2) + 
               Power(t1,3)*(11028 - 18131*t2 + 9467*Power(t2,2)) - 
               Power(t1,2)*(1430 - 10024*t2 + 6305*Power(t2,2) + 
                  1883*Power(t2,3)) + 
               t1*(-13623 + 32808*t2 - 26724*Power(t2,2) + 
                  6626*Power(t2,3) + 46*Power(t2,4))) + 
            s2*(166*Power(t1,9) + Power(t1,8)*(216 - 686*t2) + 
               Power(t1,7)*(-972 + 203*t2 + 621*Power(t2,2)) + 
               Power(t1,6)*(-2772 + 9542*t2 - 5381*Power(t2,2) + 
                  17*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (135 + 47*t2 - 65*Power(t2,2) + 257*Power(t2,3)) + 
               Power(t1,4)*(-5711 + 10369*t2 - 6304*Power(t2,2) + 
                  2465*Power(t2,3) - 1284*Power(t2,4)) + 
               Power(t1,5)*(-6296 + 20896*t2 - 21091*Power(t2,2) + 
                  6228*Power(t2,3) - 124*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-4354 + 9499*t2 - 6517*Power(t2,2) + 
                  1930*Power(t2,3) + 64*Power(t2,4)) + 
               Power(t1,2)*(11760 - 48875*t2 + 79930*Power(t2,2) - 
                  63071*Power(t2,3) + 23256*Power(t2,4) - 
                  3000*Power(t2,5)) + 
               Power(t1,3)*(6232 - 31944*t2 + 56384*Power(t2,2) - 
                  40080*Power(t2,3) + 9698*Power(t2,4) - 318*Power(t2,5)\
)) + Power(s2,3)*(3735 + 1740*Power(t1,7) + 
               Power(t1,6)*(3396 - 7976*t2) - 14937*t2 + 
               23436*Power(t2,2) - 18037*Power(t2,3) + 
               6907*Power(t2,4) - 1104*Power(t2,5) + 
               Power(t1,5)*(1728 - 8315*t2 + 7563*Power(t2,2)) - 
               Power(t1,4)*(10787 - 37553*t2 + 21276*Power(t2,2) + 
                  1674*Power(t2,3)) + 
               Power(t1,2)*(-27816 + 68763*t2 - 61514*Power(t2,2) + 
                  25543*Power(t2,3) - 5466*Power(t2,4)) + 
               Power(t1,3)*(-37117 + 100072*t2 - 89525*Power(t2,2) + 
                  24162*Power(t2,3) - 138*Power(t2,4)) + 
               t1*(1969 - 14576*t2 + 29715*Power(t2,2) - 
                  21997*Power(t2,3) + 4858*Power(t2,4) + 3*Power(t2,5))) \
+ Power(s2,2)*(-723*Power(t1,8) + 6*Power(t1,7)*(-201 + 527*t2) + 
               Power(t1,6)*(1348 + 1675*t2 - 3049*Power(t2,2)) + 
               Power(t1,5)*(7856 - 26518*t2 + 14924*Power(t2,2) + 
                  479*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-2160 + 4620*t2 - 3357*Power(t2,2) + 
                  1206*Power(t2,3) + 2*Power(t2,4)) + 
               2*Power(t1,4)*
                (11274 - 32832*t2 + 30897*Power(t2,2) - 
                  8686*Power(t2,3) + 109*Power(t2,4)) + 
               Power(t1,3)*(20166 - 46566*t2 + 39147*Power(t2,2) - 
                  16200*Power(t2,3) + 4163*Power(t2,4)) + 
               Power(t1,2)*(-6028 + 34417*t2 - 64060*Power(t2,2) + 
                  46429*Power(t2,3) - 10924*Power(t2,4) + 
                  208*Power(t2,5)) + 
               t1*(-11523 + 47090*t2 - 75646*Power(t2,2) + 
                  59095*Power(t2,3) - 22201*Power(t2,4) + 
                  3185*Power(t2,5)))) + 
         Power(s1,4)*(-3*Power(t1,11) + Power(t1,10)*(6 + 7*t2) + 
            Power(s2,10)*(-17 - 22*t1 + 45*t2) + 
            Power(t1,9)*(113 - 82*t2 + 4*Power(t2,2)) + 
            Power(s2,9)*(75 + 205*Power(t1,2) + t1*(153 - 487*t2) - 
               104*t2 + 98*Power(t2,2)) - 
            Power(t1,8)*(218 + 417*t2 - 263*Power(t2,2) + 
               16*Power(t2,3)) + 
            Power(-1 + t2,4)*
             (54 - 44*t2 + 41*Power(t2,2) + 86*Power(t2,3)) + 
            Power(t1,6)*(197 + 3655*t2 - 6053*Power(t2,2) + 
               1887*Power(t2,3) - 164*Power(t2,4)) + 
            Power(t1,7)*(-629 + 1493*t2 + 90*Power(t2,2) - 
               133*Power(t2,3) + 8*Power(t2,4)) + 
            t1*Power(-1 + t2,3)*
             (-1261 + 2862*t2 - 2130*Power(t2,2) + 871*Power(t2,3) + 
               56*Power(t2,4)) + 
            Power(t1,2)*Power(-1 + t2,2)*
             (2727 - 9565*t2 + 12693*Power(t2,2) - 6791*Power(t2,3) + 
               1172*Power(t2,4)) + 
            Power(t1,4)*(-758 + 834*t2 + 493*Power(t2,2) - 
               954*Power(t2,3) + 424*Power(t2,4) - 20*Power(t2,5)) + 
            Power(t1,5)*(100 + 3308*t2 - 10120*Power(t2,2) + 
               8373*Power(t2,3) - 1814*Power(t2,4) + 112*Power(t2,5)) + 
            Power(t1,3)*(1166 - 9354*t2 + 24893*Power(t2,2) - 
               30291*Power(t2,3) + 17702*Power(t2,4) - 
               4372*Power(t2,5) + 256*Power(t2,6)) + 
            Power(s2,8)*(281 - 851*Power(t1,3) - 784*t2 + 
               466*Power(t2,2) + 61*Power(t2,3) + 
               Power(t1,2)*(-587 + 2272*t2) + 
               t1*(-274 + 503*t2 - 854*Power(t2,2))) + 
            Power(s2,7)*(-1397 + 2068*Power(t1,4) + 
               Power(t1,3)*(1247 - 6055*t2) + 3326*t2 - 
               3201*Power(t2,2) + 1038*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,2)*(-348 - 423*t2 + 3183*Power(t2,2)) - 
               2*t1*(1180 - 3678*t2 + 2231*Power(t2,2) + 
                  208*Power(t2,3))) + 
            Power(s2,5)*(5783 + 3430*Power(t1,6) - 18155*t2 + 
               23331*Power(t2,2) - 13499*Power(t2,3) + 
               2607*Power(t2,4) + 20*Power(t2,5) - 
               7*Power(t1,5)*(-163 + 1631*t2) + 
               Power(t1,4)*(-9328 + 7517*t2 + 8521*Power(t2,2)) - 
               Power(t1,3)*(13977 - 57257*t2 + 35913*Power(t2,2) + 
                  1649*Power(t2,3)) + 
               t1*(1705 - 9880*t2 + 12638*Power(t2,2) + 
                  913*Power(t2,3) - 3028*Power(t2,4)) + 
               Power(t1,2)*(-17630 + 45546*t2 - 56833*Power(t2,2) + 
                  20944*Power(t2,3) - 6*Power(t2,4))) - 
            Power(s2,6)*(36 + 3248*Power(t1,5) - 362*t2 + 
               548*Power(t2,2) + 762*Power(t2,3) - 589*Power(t2,4) - 
               21*Power(t1,4)*(-75 + 487*t2) + 
               Power(t1,3)*(-3755 + 2283*t2 + 6648*Power(t2,2)) - 
               2*Power(t1,2)*
                (3976 - 13974*t2 + 8627*Power(t2,2) + 577*Power(t2,3)) \
+ t1*(-7921 + 19621*t2 - 21166*Power(t2,2) + 7327*Power(t2,3) + 
                  27*Power(t2,4))) + 
            Power(s2,3)*(961 + 1156*Power(t1,8) - 866*t2 - 
               4298*Power(t2,2) + 8087*Power(t2,3) - 4612*Power(t2,4) + 
               762*Power(t2,5) - 34*Power(t2,6) - 
               3*Power(t1,7)*(41 + 1359*t2) + 
               Power(t1,6)*(-9496 + 8427*t2 + 3369*Power(t2,2)) - 
               2*Power(t1,5)*
                (3273 - 25707*t2 + 16548*Power(t2,2) + 173*Power(t2,3)) \
+ Power(t1,3)*(10099 - 71835*t2 + 95830*Power(t2,2) - 
                  20988*Power(t2,3) - 5006*Power(t2,4)) + 
               Power(t1,4)*(-7709 + 24554*t2 - 62083*Power(t2,2) + 
                  26366*Power(t2,3) - 248*Power(t2,4)) + 
               t1*(24400 - 83084*t2 + 115159*Power(t2,2) - 
                  82949*Power(t2,3) + 31624*Power(t2,4) - 
                  5226*Power(t2,5)) + 
               Power(t1,2)*(43424 - 154807*t2 + 225940*Power(t2,2) - 
                  143140*Power(t2,3) + 29553*Power(t2,4) - 
                  284*Power(t2,5))) + 
            Power(s2,4)*(-7192 - 2450*Power(t1,7) + 24439*t2 - 
               33453*Power(t2,2) + 23778*Power(t2,3) - 
               9129*Power(t2,4) + 1576*Power(t2,5) + 
               49*Power(t1,6)*(-7 + 173*t2) - 
               3*Power(t1,5)*(-4075 + 3547*t2 + 2286*Power(t2,2)) + 
               5*Power(t1,4)*
                (2707 - 13929*t2 + 8861*Power(t2,2) + 245*Power(t2,3)) \
+ Power(t1,3)*(18621 - 51001*t2 + 79782*Power(t2,2) - 
                  31399*Power(t2,3) + 142*Power(t2,4)) + 
               Power(t1,2)*(-6722 + 41280*t2 - 53375*Power(t2,2) + 
                  7081*Power(t2,3) + 5816*Power(t2,4)) - 
               t1*(26014 - 86573*t2 + 118251*Power(t2,2) - 
                  71659*Power(t2,3) + 14344*Power(t2,4) + 12*Power(t2,5)\
)) + s2*(53*Power(t1,10) - 2*Power(t1,9)*(25 + 86*t2) + 
               3*Power(t1,8)*(-365 + 301*t2 + 31*Power(t2,2)) + 
               Power(t1,7)*(723 + 5013*t2 - 3249*Power(t2,2) + 
                  91*Power(t2,3)) + 
               Power(t1,6)*(2600 - 5650*t2 - 4539*Power(t2,2) + 
                  2548*Power(t2,3) - 66*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (-1246 + 2733*t2 - 2131*Power(t2,2) + 
                  1016*Power(t2,3) + 26*Power(t2,4)) + 
               Power(t1,5)*(1232 - 24777*t2 + 36908*Power(t2,2) - 
                  10909*Power(t2,3) + 226*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (5448 - 18577*t2 + 23614*Power(t2,2) - 
                  12514*Power(t2,3) + 2501*Power(t2,4)) + 
               Power(t1,3)*(11491 - 35561*t2 + 46480*Power(t2,2) - 
                  32592*Power(t2,3) + 12046*Power(t2,4) - 
                  1940*Power(t2,5)) + 
               Power(t1,4)*(8545 - 42742*t2 + 79671*Power(t2,2) - 
                  57065*Power(t2,3) + 12314*Power(t2,4) - 
                  472*Power(t2,5)) + 
               Power(t1,2)*(-829 + 15290*t2 - 49546*Power(t2,2) + 
                  64903*Power(t2,3) - 38556*Power(t2,4) + 
                  9240*Power(t2,5) - 502*Power(t2,6))) + 
            Power(s2,2)*(-338*Power(t1,9) + 
               4*Power(t1,8)*(37 + 295*t2) + 
               Power(t1,7)*(4373 - 3817*t2 - 908*Power(t2,2)) - 
               2*Power(t1,6)*
                (-305 + 11123*t2 - 7216*Power(t2,2) + 52*Power(t2,3)) + 
               Power(t1,5)*(-1777 + 1353*t2 + 25618*Power(t2,2) - 
                  12037*Power(t2,3) + 189*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (2734 - 8996*t2 + 10822*Power(t2,2) - 
                  5625*Power(t2,3) + 1301*Power(t2,4)) + 
               Power(t1,4)*(-6475 + 61195*t2 - 85400*Power(t2,2) + 
                  22778*Power(t2,3) + 1567*Power(t2,4)) + 
               Power(t1,3)*(-31838 + 125823*t2 - 200571*Power(t2,2) + 
                  133672*Power(t2,3) - 28316*Power(t2,4) + 
                  636*Power(t2,5)) + 
               Power(t1,2)*(-27941 + 93372*t2 - 128679*Power(t2,2) + 
                  92717*Power(t2,3) - 34965*Power(t2,4) + 
                  5610*Power(t2,5)) + 
               t1*(-1332 - 4840*t2 + 28331*Power(t2,2) - 
                  41839*Power(t2,3) + 24816*Power(t2,4) - 
                  5376*Power(t2,5) + 240*Power(t2,6)))) + 
         Power(s1,3)*(Power(s2,12) - Power(t1,11) + 11*Power(t1,10)*t2 + 
            Power(s2,11)*(1 - 15*t1 + 14*t2) + 
            Power(t1,9)*(85 - 45*t2 - 23*Power(t2,2)) - 
            Power(t1,8)*(29 + 422*t2 - 251*Power(t2,2) + 
               21*Power(t2,3)) - 
            Power(-1 + t2,4)*
             (-291 + 628*t2 - 512*Power(t2,2) + 320*Power(t2,3) + 
               16*Power(t2,4)) + 
            Power(t1,7)*(-932 + 900*t2 + 1019*Power(t2,2) - 
               576*Power(t2,3) + 66*Power(t2,4)) - 
            2*t1*Power(-1 + t2,3)*
             (437 - 1815*t2 + 2614*Power(t2,2) - 1485*Power(t2,3) + 
               390*Power(t2,4)) - 
            Power(t1,6)*(133 - 3488*t2 + 3602*Power(t2,2) + 
               233*Power(t2,3) - 179*Power(t2,4) + 32*Power(t2,5)) - 
            Power(t1,2)*Power(-1 + t2,2)*
             (589 + 1903*t2 - 7321*Power(t2,2) + 7464*Power(t2,3) - 
               2808*Power(t2,4) + 288*Power(t2,5)) + 
            Power(t1,5)*(2671 - 1039*t2 - 10445*Power(t2,2) + 
               11276*Power(t2,3) - 2794*Power(t2,4) + 378*Power(t2,5)) + 
            Power(t1,4)*(1196 - 2056*t2 - 8725*Power(t2,2) + 
               20357*Power(t2,3) - 12655*Power(t2,4) + 
               2050*Power(t2,5) - 200*Power(t2,6)) + 
            Power(t1,3)*(-1961 + 4940*t2 - 5259*Power(t2,2) + 
               4596*Power(t2,3) - 3078*Power(t2,4) + 454*Power(t2,5) + 
               308*Power(t2,6)) + 
            Power(s2,10)*(-12 + 95*Power(t1,2) + 40*t2 + 
               27*Power(t2,2) - t1*(23 + 150*t2)) + 
            Power(s2,9)*(66 - 345*Power(t1,3) - 338*t2 + 
               321*Power(t2,2) + 16*Power(t2,3) + 
               Power(t1,2)*(166 + 713*t2) + 
               t1*(207 - 511*t2 - 239*Power(t2,2))) + 
            Power(s2,8)*(-75 + 810*Power(t1,4) + 56*t2 - 
               576*Power(t2,2) + 505*Power(t2,3) + 2*Power(t2,4) - 
               Power(t1,3)*(621 + 1984*t2) + 
               Power(t1,2)*(-1223 + 2651*t2 + 927*Power(t2,2)) - 
               t1*(423 - 2974*t2 + 2927*Power(t2,2) + 107*Power(t2,3))) \
+ Power(s2,6)*(2193 + 1470*Power(t1,6) - 7381*t2 + 11778*Power(t2,2) - 
               9588*Power(t2,3) + 2590*Power(t2,4) + 9*Power(t2,5) - 
               14*Power(t1,5)*(153 + 314*t2) + 
               7*Power(t1,4)*(-1005 + 1948*t2 + 415*Power(t2,2)) - 
               Power(t1,3)*(566 - 23401*t2 + 24962*Power(t2,2) + 
                  425*Power(t2,3)) + 
               t1*(5853 - 24544*t2 + 34461*Power(t2,2) - 
                  11290*Power(t2,3) - 1455*Power(t2,4)) + 
               Power(t1,2)*(2680 - 11469*t2 - 6858*Power(t2,2) + 
                  11391*Power(t2,3) - 23*Power(t2,4))) + 
            Power(s2,7)*(-924 - 1302*Power(t1,5) + 3458*t2 - 
               4629*Power(t2,2) + 1424*Power(t2,3) + 268*Power(t2,4) + 
               2*Power(t1,4)*(711 + 1792*t2) + 
               Power(t1,3)*(3773 - 7620*t2 - 2065*Power(t2,2)) + 
               Power(t1,2)*(973 - 11162*t2 + 11405*Power(t2,2) + 
                  296*Power(t2,3)) + 
               t1*(-183 + 1617*t2 + 3253*Power(t2,2) - 
                  3703*Power(t2,3) - 3*Power(t2,4))) + 
            Power(s2,4)*(-8889 + 645*Power(t1,8) + 35046*t2 - 
               61222*Power(t2,2) + 56355*Power(t2,3) - 
               25171*Power(t2,4) + 3872*Power(t2,5) - 24*Power(t2,6) - 
               2*Power(t1,7)*(753 + 1064*t2) + 
               7*Power(t1,6)*(-931 + 1758*t2 + 227*Power(t2,2)) + 
               Power(t1,5)*(4054 + 23863*t2 - 28966*Power(t2,2) - 
                  61*Power(t2,3)) + 
               Power(t1,3)*(16034 - 99392*t2 + 161089*Power(t2,2) - 
                  58212*Power(t2,3) - 2988*Power(t2,4)) - 
               5*Power(t1,4)*
                (-2292 + 9248*t2 - 463*Power(t2,2) - 
                  3742*Power(t2,3) + 28*Power(t2,4)) + 
               t1*(-6153 + 30928*t2 - 58945*Power(t2,2) + 
                  33831*Power(t2,3) + 4998*Power(t2,4) - 
                  4480*Power(t2,5)) + 
               Power(t1,2)*(20348 - 61750*t2 + 119479*Power(t2,2) - 
                  116165*Power(t2,3) + 32537*Power(t2,4) - 
                  204*Power(t2,5))) + 
            Power(s2,5)*(1583 - 1170*Power(t1,7) - 6057*t2 + 
               9941*Power(t2,2) - 4552*Power(t2,3) - 2137*Power(t2,4) + 
               1189*Power(t2,5) + 14*Power(t1,6)*(156 + 265*t2) - 
               7*Power(t1,5)*(-1201 + 2278*t2 + 381*Power(t2,2)) + 
               Power(t1,4)*(-1665 - 29960*t2 + 33733*Power(t2,2) + 
                  310*Power(t2,3)) + 
               Power(t1,3)*(-8001 + 31552*t2 + 5404*Power(t2,2) - 
                  19064*Power(t2,3) + 90*Power(t2,4)) + 
               Power(t1,2)*(-14254 + 69143*t2 - 103025*Power(t2,2) + 
                  35514*Power(t2,3) + 3061*Power(t2,4)) + 
               t1*(-10721 + 35332*t2 - 61172*Power(t2,2) + 
                  53605*Power(t2,3) - 14699*Power(t2,4) + 8*Power(t2,5))\
) + Power(s2,2)*(51*Power(t1,10) - Power(t1,9)*(187 + 174*t2) + 
               Power(t1,8)*(-893 + 1820*t2 + 120*Power(t2,2)) + 
               Power(t1,7)*(2322 + 2767*t2 - 4850*Power(t2,2) + 
                  49*Power(t2,3)) + 
               Power(t1,6)*(3468 - 18837*t2 + 5972*Power(t2,2) + 
                  3223*Power(t2,3) - 47*Power(t2,4)) + 
               Power(t1,5)*(-3107 - 27500*t2 + 67943*Power(t2,2) - 
                  27426*Power(t2,3) + 329*Power(t2,4)) + 
               Power(t1,3)*(1360 + 45857*t2 - 138152*Power(t2,2) + 
                  108119*Power(t2,3) - 14676*Power(t2,4) - 
                  2094*Power(t2,5)) + 
               Power(t1,4)*(7772 - 2845*t2 + 34925*Power(t2,2) - 
                  64174*Power(t2,3) + 19406*Power(t2,4) - 
                  509*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (1853 - 2449*t2 - 1052*Power(t2,2) + 
                  1989*Power(t2,3) - 326*Power(t2,4) + 100*Power(t2,5)) \
+ t1*(-20572 + 84132*t2 - 149407*Power(t2,2) + 150494*Power(t2,3) - 
                  90009*Power(t2,4) + 28888*Power(t2,5) - 
                  3526*Power(t2,6)) + 
               Power(t1,2)*(-29209 + 125609*t2 - 257392*Power(t2,2) + 
                  274321*Power(t2,3) - 135065*Power(t2,4) + 
                  22174*Power(t2,5) - 636*Power(t2,6))) + 
            Power(s2,3)*(8571 - 235*Power(t1,9) - 35760*t2 + 
               63749*Power(t2,2) - 63516*Power(t2,3) + 
               37774*Power(t2,4) - 12606*Power(t2,5) + 
               1788*Power(t2,6) + Power(t1,8)*(681 + 794*t2) + 
               Power(t1,7)*(3183 - 6116*t2 - 587*Power(t2,2)) - 
               Power(t1,6)*(4157 + 11354*t2 - 15547*Power(t2,2) + 
                  68*Power(t2,3)) + 
               Power(t1,5)*(-8825 + 39089*t2 - 7739*Power(t2,2) - 
                  10695*Power(t2,3) + 113*Power(t2,4)) + 
               Power(t1,4)*(-6316 + 75988*t2 - 141311*Power(t2,2) + 
                  53668*Power(t2,3) + 1062*Power(t2,4)) + 
               Power(t1,3)*(-18515 + 43580*t2 - 105583*Power(t2,2) + 
                  122978*Power(t2,3) - 35496*Power(t2,4) + 
                  506*Power(t2,5)) + 
               Power(t1,2)*(6719 - 57726*t2 + 132389*Power(t2,2) - 
                  90064*Power(t2,3) + 2718*Power(t2,4) + 
                  5578*Power(t2,5)) + 
               t1*(28438 - 115863*t2 + 214981*Power(t2,2) - 
                  210155*Power(t2,3) + 98081*Power(t2,4) - 
                  15550*Power(t2,5) + 200*Power(t2,6))) - 
            s2*(5*Power(t1,11) - Power(t1,10)*(26 + 17*t2) + 
               Power(t1,9)*(-110 + 271*t2 + 10*Power(t2,2)) + 
               Power(t1,8)*(689 + 146*t2 - 722*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,7)*(495 - 4654*t2 + 2022*Power(t2,2) + 
                  346*Power(t2,3) - 8*Power(t2,4)) + 
               Power(t1,6)*(-3646 - 1947*t2 + 15547*Power(t2,2) - 
                  6898*Power(t2,3) + 343*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (953 - 3700*t2 + 4837*Power(t2,2) - 2602*Power(t2,3) + 
                  794*Power(t2,4)) + 
               Power(t1,5)*(944 + 10424*t2 - 4175*Power(t2,2) - 
                  13577*Power(t2,3) + 4517*Power(t2,4) - 222*Power(t2,5)\
) - t1*Power(-1 + t2,2)*(2718 - 1595*t2 - 6943*Power(t2,2) + 
                  8618*Power(t2,3) - 2938*Power(t2,4) + 370*Power(t2,5)) \
+ Power(t1,4)*(6180 + 11963*t2 - 65212*Power(t2,2) + 
                  58610*Power(t2,3) - 11891*Power(t2,4) + 
                  571*Power(t2,5)) + 
               Power(t1,2)*(-13998 + 53502*t2 - 91319*Power(t2,2) + 
                  92002*Power(t2,3) - 55545*Power(t2,4) + 
                  16790*Power(t2,5) - 1432*Power(t2,6)) - 
               2*Power(t1,3)*(4232 - 21368*t2 + 56179*Power(t2,2) - 
                  70439*Power(t2,3) + 37405*Power(t2,4) - 
                  6273*Power(t2,5) + 330*Power(t2,6)))) + 
         Power(s1,2)*(-3*Power(t1,11) + Power(s2,12)*t2 + 
            Power(t1,10)*(6 + 9*t2) + 
            Power(t1,9)*(40 - 49*t2 - 17*Power(t2,2)) - 
            Power(s2,11)*(3 + t1 - 8*t2 + 10*t1*t2 - 2*Power(t2,2)) + 
            Power(t1,8)*(93 - 293*t2 + 177*Power(t2,2) + 
               28*Power(t2,3)) + 
            Power(t1,7)*(-518 + 428*t2 + 562*Power(t2,2) - 
               464*Power(t2,3) + 37*Power(t2,4)) + 
            Power(-1 + t2,4)*
             (105 - 590*t2 + 834*Power(t2,2) - 430*Power(t2,3) + 
               206*Power(t2,4)) - 
            Power(t1,6)*(1010 - 3709*t2 + 2618*Power(t2,2) + 
               730*Power(t2,3) - 774*Power(t2,4) + 102*Power(t2,5)) + 
            t1*Power(-1 + t2,3)*
             (1082 - 1351*t2 - 769*Power(t2,2) + 1714*Power(t2,3) - 
               710*Power(t2,4) + 168*Power(t2,5)) - 
            2*Power(t1,2)*Power(-1 + t2,2)*
             (1272 - 2119*t2 + 1649*Power(t2,2) - 1645*Power(t2,3) + 
               676*Power(t2,4) + 182*Power(t2,5)) + 
            Power(t1,4)*(3734 - 11245*t2 + 5175*Power(t2,2) + 
               10480*Power(t2,3) - 10589*Power(t2,4) + 
               2876*Power(t2,5) - 410*Power(t2,6)) + 
            Power(t1,5)*(1566 - 404*t2 - 5645*Power(t2,2) + 
               5042*Power(t2,3) - 518*Power(t2,4) - 169*Power(t2,5) + 
               48*Power(t2,6)) + 
            Power(t1,3)*(381 - 4076*t2 + 3486*Power(t2,2) + 
               11044*Power(t2,3) - 19201*Power(t2,4) + 
               9580*Power(t2,5) - 1382*Power(t2,6) + 168*Power(t2,7)) + 
            Power(s2,10)*(-3 - 37*t2 + 57*Power(t2,2) + Power(t2,3) + 
               5*Power(t1,2)*(2 + 9*t2) + 
               t1*(37 - 93*t2 - 16*Power(t2,2))) + 
            Power(s2,9)*(36 - 186*t2 + 72*Power(t2,2) + 
               89*Power(t2,3) - 15*Power(t1,3)*(3 + 8*t2) + 
               2*Power(t1,2)*(-97 + 237*t2 + 28*Power(t2,2)) - 
               4*t1*(-16 - 73*t2 + 131*Power(t2,2) + Power(t2,3))) + 
            Power(s2,7)*(-55 + 694*t2 - 211*Power(t2,2) - 
               1460*Power(t2,3) + 905*Power(t2,4) - 2*Power(t2,5) - 
               42*Power(t1,5)*(5 + 6*t2) + 
               2*Power(t1,4)*(-541 + 1344*t2 + 70*Power(t2,2)) + 
               Power(t1,3)*(1233 + 1825*t2 - 4998*Power(t2,2) + 
                  28*Power(t2,3)) + 
               t1*(399 - 4681*t2 + 10387*Power(t2,2) - 
                  5505*Power(t2,3) - 245*Power(t2,4)) + 
               Power(t1,2)*(1900 - 8100*t2 + 4555*Power(t2,2) + 
                  2103*Power(t2,3) - 12*Power(t2,4))) + 
            Power(s2,8)*(-96 + 696*t2 - 1403*Power(t2,2) + 
               706*Power(t2,3) + 47*Power(t2,4) + 
               30*Power(t1,4)*(4 + 7*t2) + 
               Power(t1,3)*(577 - 1404*t2 - 112*Power(t2,2)) + 
               Power(t1,2)*(-397 - 986*t2 + 2126*Power(t2,2)) + 
               t1*(-423 + 1919*t2 - 945*Power(t2,2) - 657*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,6)*(1326 - 6153*t2 + 12522*Power(t2,2) - 
               10218*Power(t2,3) + 2102*Power(t2,4) + 383*Power(t2,5) + 
               42*Power(t1,6)*(6 + 5*t2) - 
               14*Power(t1,5)*(-95 + 249*t2 + 8*Power(t2,2)) - 
               7*Power(t1,4)*
                (323 + 275*t2 - 1072*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(-4493 + 18668*t2 - 11534*Power(t2,2) - 
                  3805*Power(t2,3) + 30*Power(t2,4)) + 
               Power(t1,2)*(-276 + 13048*t2 - 32357*Power(t2,2) + 
                  18019*Power(t2,3) + 491*Power(t2,4)) + 
               t1*(855 - 7176*t2 + 5148*Power(t2,2) + 
                  7416*Power(t2,3) - 5446*Power(t2,4) + 27*Power(t2,5))) \
- Power(s2,5)*(1747 - 7094*t2 + 15122*Power(t2,2) - 20463*Power(t2,3) + 
               13715*Power(t2,4) - 3106*Power(t2,5) + 16*Power(t2,6) + 
               30*Power(t1,7)*(7 + 4*t2) - 
               28*Power(t1,6)*(-38 + 111*t2 + 2*Power(t2,2)) - 
               7*Power(t1,5)*
                (371 + 139*t2 - 1066*Power(t2,2) + 12*Power(t2,3)) + 
               Power(t1,4)*(-6210 + 25936*t2 - 17397*Power(t2,2) - 
                  4245*Power(t2,3) + 40*Power(t2,4)) + 
               Power(t1,3)*(1338 + 19092*t2 - 54908*Power(t2,2) + 
                  32258*Power(t2,3) + 425*Power(t2,4)) + 
               Power(t1,2)*(2894 - 24852*t2 + 23005*Power(t2,2) + 
                  14512*Power(t2,3) - 13439*Power(t2,4) + 
                  110*Power(t2,5)) + 
               t1*(6476 - 32428*t2 + 70475*Power(t2,2) - 
                  59979*Power(t2,3) + 13703*Power(t2,4) + 
                  1585*Power(t2,5))) + 
            Power(s2,4)*(-2933 + 13481*t2 - 28863*Power(t2,2) + 
               29166*Power(t2,3) - 9940*Power(t2,4) - 
               1938*Power(t2,5) + 1048*Power(t2,6) + 
               15*Power(t1,8)*(8 + 3*t2) - 
               2*Power(t1,7)*(-257 + 942*t2 + 8*Power(t2,2)) - 
               7*Power(t1,6)*
                (267 - 19*t2 - 702*Power(t2,2) + 8*Power(t2,3)) + 
               5*Power(t1,4)*
                (717 + 2971*t2 - 10904*Power(t2,2) + 
                  6852*Power(t2,3) + 9*Power(t2,4)) + 
               Power(t1,5)*(-5081 + 22266*t2 - 16322*Power(t2,2) - 
                  2979*Power(t2,3) + 30*Power(t2,4)) + 
               Power(t1,3)*(3608 - 41100*t2 + 45322*Power(t2,2) + 
                  12624*Power(t2,3) - 17359*Power(t2,4) + 
                  210*Power(t2,5)) + 
               Power(t1,2)*(11507 - 63602*t2 + 152152*Power(t2,2) - 
                  136844*Power(t2,3) + 34214*Power(t2,4) + 
                  2304*Power(t2,5)) + 
               t1*(7965 - 29204*t2 + 61555*Power(t2,2) - 
                  87669*Power(t2,3) + 60488*Power(t2,4) - 
                  13559*Power(t2,5) + 92*Power(t2,6))) - 
            Power(s2,3)*(-8072 + 38266*t2 - 82897*Power(t2,2) + 
               102524*Power(t2,3) - 72151*Power(t2,4) + 
               25484*Power(t2,5) - 3226*Power(t2,6) + 72*Power(t2,7) + 
               5*Power(t1,9)*(9 + 2*t2) + 
               Power(t1,8)*(107 - 744*t2 - 2*Power(t2,2)) + 
               Power(t1,7)*(-787 + 509*t2 + 2066*Power(t2,2) - 
                  20*Power(t2,3)) + 
               Power(t1,5)*(4029 + 4601*t2 - 31163*Power(t2,2) + 
                  21701*Power(t2,3) - 193*Power(t2,4)) + 
               Power(t1,6)*(-2248 + 11404*t2 - 9445*Power(t2,2) - 
                  1277*Power(t2,3) + 12*Power(t2,4)) + 
               Power(t1,4)*(667 - 35110*t2 + 46813*Power(t2,2) + 
                  2596*Power(t2,3) - 12291*Power(t2,4) + 
                  210*Power(t2,5)) + 
               Power(t1,3)*(7915 - 54059*t2 + 156208*Power(t2,2) - 
                  153568*Power(t2,3) + 42218*Power(t2,4) + 
                  1130*Power(t2,5)) + 
               Power(t1,2)*(14786 - 44050*t2 + 84635*Power(t2,2) - 
                  130805*Power(t2,3) + 96280*Power(t2,4) - 
                  21844*Power(t2,5) + 300*Power(t2,6)) + 
               t1*(-6522 + 34461*t2 - 87703*Power(t2,2) + 
                  100119*Power(t2,3) - 39727*Power(t2,4) - 
                  3142*Power(t2,5) + 2598*Power(t2,6))) + 
            s2*(-Power(t1,11) + 18*Power(t1,10)*(1 + t2) - 
               Power(t1,9)*(9 + 85*t2 + 54*Power(t2,2)) + 
               Power(t1,8)*(-122 - 198*t2 + 499*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,7)*(-744 + 1382*t2 + 582*Power(t2,2) - 
                  1224*Power(t2,3) + 29*Power(t2,4)) + 
               Power(t1,6)*(2048 + 1376*t2 - 6771*Power(t2,2) + 
                  2408*Power(t2,3) + 557*Power(t2,4) - 22*Power(t2,5)) \
- Power(-1 + t2,3)*(1409 - 2417*t2 + 622*Power(t2,2) + 
                  430*Power(t2,3) - 16*Power(t2,4) + 106*Power(t2,5)) + 
               Power(t1,5)*(2995 - 7891*t2 - 8005*Power(t2,2) + 
                  20405*Power(t2,3) - 8039*Power(t2,4) + 
                  459*Power(t2,5)) - 
               t1*Power(-1 + t2,2)*
                (-8960 + 23688*t2 - 29171*Power(t2,2) + 
                  22631*Power(t2,3) - 8858*Power(t2,4) + 
                  610*Power(t2,5)) + 
               Power(t1,4)*(-7229 + 7732*t2 + 4399*Power(t2,2) + 
                  6772*Power(t2,3) - 15413*Power(t2,4) + 
                  4382*Power(t2,5) - 260*Power(t2,6)) + 
               Power(t1,3)*(-6673 + 13922*t2 + 22754*Power(t2,2) - 
                  67302*Power(t2,3) + 44533*Power(t2,4) - 
                  7834*Power(t2,5) + 516*Power(t2,6)) + 
               Power(t1,2)*(8404 - 35796*t2 + 88397*Power(t2,2) - 
                  139780*Power(t2,3) + 121619*Power(t2,4) - 
                  49298*Power(t2,5) + 6846*Power(t2,6) - 392*Power(t2,7)\
)) + Power(s2,2)*(Power(t1,10)*(10 + t2) - Power(t1,9)*(23 + 173*t2) + 
               Power(t1,8)*(-148 + 310*t2 + 503*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,6)*(2406 - 1314*t2 - 8937*Power(t2,2) + 
                  7675*Power(t2,3) - 135*Power(t2,4)) + 
               Power(t1,7)*(-315 + 3020*t2 - 3150*Power(t2,2) - 
                  303*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,5)*(-2377 - 14184*t2 + 25768*Power(t2,2) - 
                  3416*Power(t2,3) - 4424*Power(t2,4) + 107*Power(t2,5)) \
- Power(t1,4)*(427 + 12550*t2 - 72632*Power(t2,2) + 86160*Power(t2,3) - 
                  26870*Power(t2,4) + 329*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (-6111 + 18100*t2 - 23484*Power(t2,2) + 
                  17337*Power(t2,3) - 6790*Power(t2,4) + 918*Power(t2,5)\
) + Power(t1,3)*(14231 - 29268*t2 + 39448*Power(t2,2) - 
                  75413*Power(t2,3) + 65438*Power(t2,4) - 
                  15604*Power(t2,5) + 436*Power(t2,6)) + 
               Power(t1,2)*(-650 + 18303*t2 - 86769*Power(t2,2) + 
                  127775*Power(t2,3) - 63731*Power(t2,4) + 
                  3754*Power(t2,5) + 1444*Power(t2,6)) + 
               t1*(-16820 + 77837*t2 - 173796*Power(t2,2) + 
                  229548*Power(t2,3) - 172836*Power(t2,4) + 
                  64173*Power(t2,5) - 8356*Power(t2,6) + 250*Power(t2,7))\
)) + s1*(Power(t1,11) + 4*Power(t1,10)*(-3 + t2) + 
            2*Power(s2,11)*(-1 + t2)*t2 + 
            Power(t1,9)*(27 + 16*t2 - 16*Power(t2,2)) + 
            Power(t1,8)*(47 - 216*t2 + 84*Power(t2,2) + 
               15*Power(t2,3)) - 
            Power(t1,7)*(23 + 134*t2 - 542*Power(t2,2) + 
               255*Power(t2,3) + 15*Power(t2,4)) - 
            Power(t1,6)*(483 - 1504*t2 + 1121*Power(t2,2) + 
               399*Power(t2,3) - 430*Power(t2,4) + 27*Power(t2,5)) - 
            Power(-1 + t2,4)*
             (390 - 710*t2 + 237*Power(t2,2) + 88*Power(t2,3) + 
               24*Power(t2,4) + 40*Power(t2,5)) + 
            2*t1*Power(-1 + t2,3)*
             (706 - 1458*t2 + 1267*Power(t2,2) - 910*Power(t2,3) + 
               320*Power(t2,4) + 70*Power(t2,5)) - 
            Power(t1,2)*Power(-1 + t2,2)*
             (701 + 466*t2 - 1302*Power(t2,2) - 1955*Power(t2,3) + 
               2434*Power(t2,4) - 448*Power(t2,5) + 64*Power(t2,6)) + 
            Power(t1,5)*(-331 + 2820*t2 - 5726*Power(t2,2) + 
               3770*Power(t2,3) + 2*Power(t2,4) - 572*Power(t2,5) + 
               70*Power(t2,6)) + 
            Power(t1,4)*(1667 - 4818*t2 + 2393*Power(t2,2) + 
               4107*Power(t2,3) - 4428*Power(t2,4) + 986*Power(t2,5) + 
               123*Power(t2,6) - 32*Power(t2,7)) + 
            Power(t1,3)*(1866 - 10174*t2 + 18190*Power(t2,2) - 
               11057*Power(t2,3) - 2461*Power(t2,4) + 
               5246*Power(t2,5) - 1822*Power(t2,6) + 212*Power(t2,7)) + 
            Power(s2,10)*(3 - 16*t2 + 8*Power(t2,2) + 4*Power(t2,3) + 
               t1*(2 + 16*t2 - 17*Power(t2,2))) + 
            Power(s2,9)*(3 + 32*t2 - 120*Power(t2,2) + 76*Power(t2,3) + 
               2*Power(t2,4) + 
               2*Power(t1,2)*(-9 - 28*t2 + 32*Power(t2,2)) - 
               t1*(29 - 162*t2 + 90*Power(t2,2) + 26*Power(t2,3))) - 
            Power(s2,8)*(36 - 252*t2 + 368*Power(t2,2) - 
               21*Power(t2,3) - 92*Power(t2,4) + Power(t2,5) + 
               Power(t1,3)*(-73 - 112*t2 + 140*Power(t2,2)) - 
               Power(t1,2)*(115 - 716*t2 + 423*Power(t2,2) + 
                  70*Power(t2,3)) + 
               t1*(59 + 122*t2 - 853*Power(t2,2) + 582*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(s2,7)*(62 - 558*t2 + 1720*Power(t2,2) - 
               2003*Power(t2,3) + 653*Power(t2,4) + 36*Power(t2,5) + 
               4*Power(t1,4)*(-44 - 35*t2 + 49*Power(t2,2)) - 
               Power(t1,3)*(233 - 1816*t2 + 1113*Power(t2,2) + 
                  98*Power(t2,3)) + 
               Power(t1,2)*(290 - 11*t2 - 2646*Power(t2,2) + 
                  1955*Power(t2,3)) + 
               t1*(313 - 2019*t2 + 3126*Power(t2,2) - 529*Power(t2,3) - 
                  548*Power(t2,4) + 7*Power(t2,5))) + 
            Power(s2,6)*(120 - 1140*t2 + 2192*Power(t2,2) - 
               530*Power(t2,3) - 1379*Power(t2,4) + 658*Power(t2,5) - 
               6*Power(t2,6) - 
               14*Power(t1,5)*(-20 - 8*t2 + 13*Power(t2,2)) + 
               7*Power(t1,4)*
                (31 - 416*t2 + 261*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(-642 + 852*t2 + 4667*Power(t2,2) - 
                  3764*Power(t2,3) + 35*Power(t2,4)) + 
               Power(t1,2)*(-1094 + 6612*t2 - 10859*Power(t2,2) + 
                  2637*Power(t2,3) + 1394*Power(t2,4) - 20*Power(t2,5)) \
- t1*(141 - 2634*t2 + 9578*Power(t2,2) - 12088*Power(t2,3) + 
                  4203*Power(t2,4) + 145*Power(t2,5))) + 
            Power(s2,5)*(-884 + 4818*t2 - 12687*Power(t2,2) + 
               16776*Power(t2,3) - 9913*Power(t2,4) + 
               1652*Power(t2,5) + 207*Power(t2,6) + 
               28*Power(t1,6)*(-11 - 2*t2 + 4*Power(t2,2)) - 
               7*Power(t1,5)*
                (-7 - 436*t2 + 279*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(688 - 2081*t2 - 5084*Power(t2,2) + 
                  4545*Power(t2,3) - 70*Power(t2,4)) + 
               Power(t1,3)*(1988 - 11379*t2 + 20201*Power(t2,2) - 
                  6000*Power(t2,3) - 1970*Power(t2,4) + 30*Power(t2,5)) \
+ Power(t1,2)*(-142 - 4899*t2 + 21829*Power(t2,2) - 
                  30236*Power(t2,3) + 11216*Power(t2,4) + 
                  192*Power(t2,5)) + 
               t1*(-615 + 6621*t2 - 13825*Power(t2,2) + 
                  5815*Power(t2,3) + 5643*Power(t2,4) - 
                  3142*Power(t2,5) + 24*Power(t2,6))) - 
            Power(s2,4)*(-650 + 2674*t2 - 6205*Power(t2,2) + 
               12323*Power(t2,3) - 15154*Power(t2,4) + 
               8711*Power(t2,5) - 1709*Power(t2,6) + 12*Power(t2,7) + 
               2*Power(t1,7)*(-119 - 8*t2 + 22*Power(t2,2)) + 
               7*Power(t1,6)*
                (53 + 296*t2 - 195*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,5)*(208 - 2518*t2 - 3435*Power(t2,2) + 
                  3526*Power(t2,3) - 63*Power(t2,4)) + 
               5*Power(t1,4)*
                (393 - 2148*t2 + 4354*Power(t2,2) - 1498*Power(t2,3) - 
                  336*Power(t2,4) + 5*Power(t2,5)) + 
               Power(t1,3)*(-715 - 4282*t2 + 25394*Power(t2,2) - 
                  39869*Power(t2,3) + 15909*Power(t2,4) + 
                  38*Power(t2,5)) + 
               Power(t1,2)*(-774 + 13428*t2 - 31782*Power(t2,2) + 
                  17829*Power(t2,3) + 8594*Power(t2,4) - 
                  6021*Power(t2,5) + 56*Power(t2,6)) + 
               t1*(-3303 + 18286*t2 - 50985*Power(t2,2) + 
                  70970*Power(t2,3) - 44003*Power(t2,4) + 
                  8246*Power(t2,5) + 632*Power(t2,6))) + 
            Power(s2,3)*(2178 - 11756*t2 + 30550*Power(t2,2) - 
               43913*Power(t2,3) + 32897*Power(t2,4) - 
               10180*Power(t2,5) - 166*Power(t2,6) + 390*Power(t2,7) + 
               2*Power(t1,8)*(-64 - t2 + 5*Power(t2,2)) + 
               Power(t1,7)*(445 + 856*t2 - 603*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(t1,6)*(306 + 1773*t2 + 1318*Power(t2,2) - 
                  1717*Power(t2,3) + 28*Power(t2,4)) + 
               Power(t1,5)*(921 - 4857*t2 + 13448*Power(t2,2) - 
                  5361*Power(t2,3) - 872*Power(t2,4) + 11*Power(t2,5)) \
- Power(t1,4)*(790 + 1288*t2 - 14646*Power(t2,2) + 28771*Power(t2,3) - 
                  12681*Power(t2,4) + 128*Power(t2,5)) + 
               Power(t1,3)*(651 + 10454*t2 - 33195*Power(t2,2) + 
                  24296*Power(t2,3) + 5284*Power(t2,4) - 
                  5764*Power(t2,5) + 84*Power(t2,6)) + 
               Power(t1,2)*(-4346 + 23299*t2 - 70990*Power(t2,2) + 
                  107885*Power(t2,3) - 72056*Power(t2,4) + 
                  15360*Power(t2,5) + 530*Power(t2,6)) + 
               t1*(-3425 + 13259*t2 - 25788*Power(t2,2) + 
                  43129*Power(t2,3) - 50112*Power(t2,4) + 
                  28329*Power(t2,5) - 5406*Power(t2,6) + 22*Power(t2,7))\
) + s2*(-10*Power(t1,10) + Power(t1,9)*(88 + 2*t2 - 17*Power(t2,2)) + 
               Power(t1,8)*(-163 - 167*t2 + 48*Power(t2,2) + 
                  59*Power(t2,3)) + 
               Power(t1,7)*(-166 + 783*t2 + 329*Power(t2,2) - 
                  382*Power(t2,3) - 34*Power(t2,4)) + 
               Power(t1,6)*(6 + 593*t2 - 1451*Power(t2,2) - 
                  894*Power(t2,3) + 994*Power(t2,4) - 28*Power(t2,5)) + 
               Power(-1 + t2,3)*
                (-2225 + 6275*t2 - 7643*Power(t2,2) + 
                  5293*Power(t2,3) - 1838*Power(t2,4) + 148*Power(t2,5)\
) + Power(t1,4)*(270 - 4459*t2 + 4371*Power(t2,2) + 8733*Power(t2,3) - 
                  13793*Power(t2,4) + 4968*Power(t2,5) - 
                  253*Power(t2,6)) + 
               Power(t1,5)*(1800 - 4107*t2 - 96*Power(t2,2) + 
                  4761*Power(t2,3) - 1351*Power(t2,4) - 
                  462*Power(t2,5) + 20*Power(t2,6)) + 
               t1*Power(-1 + t2,2)*
                (4620 - 13239*t2 + 21811*Power(t2,2) - 
                  23876*Power(t2,3) + 12158*Power(t2,4) - 
                  1626*Power(t2,5) + 72*Power(t2,6)) + 
               Power(t1,3)*(-5510 + 17705*t2 - 18141*Power(t2,2) + 
                  9670*Power(t2,3) - 10160*Power(t2,4) + 
                  8636*Power(t2,5) - 2310*Power(t2,6) + 118*Power(t2,7)\
) - Power(t1,2)*(1496 - 8887*t2 + 8615*Power(t2,2) + 
                  14850*Power(t2,3) - 29966*Power(t2,4) + 
                  16600*Power(t2,5) - 2846*Power(t2,6) + 138*Power(t2,7)\
)) - Power(s2,2)*(Power(t1,9)*(-46 + Power(t2,2)) + 
               Power(t1,8)*(272 + 176*t2 - 153*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(t1,7)*(370 + 736*t2 + 181*Power(t2,2) - 
                  480*Power(t2,3) + 5*Power(t2,4)) + 
               Power(t1,5)*(-313 + 630*t2 + 2314*Power(t2,2) - 
                  10202*Power(t2,3) + 5417*Power(t2,4) - 111*Power(t2,5)\
) + Power(t1,6)*(8 - 84*t2 + 4191*Power(t2,2) - 2109*Power(t2,3) - 
                  258*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,3)*(-1988 + 8192*t2 - 34047*Power(t2,2) + 
                  66194*Power(t2,3) - 51757*Power(t2,4) + 
                  13162*Power(t2,5) - 78*Power(t2,6)) + 
               Power(-1 + t2,2)*
                (3931 - 13862*t2 + 23573*Power(t2,2) - 
                  22420*Power(t2,3) + 9934*Power(t2,4) - 
                  1228*Power(t2,5) + 32*Power(t2,6)) + 
               Power(t1,4)*(2247 - 96*t2 - 14263*Power(t2,2) + 
                  16114*Power(t2,3) + 33*Power(t2,4) - 
                  2716*Power(t2,5) + 66*Power(t2,6)) + 
               Power(t1,2)*(-6618 + 23472*t2 - 35331*Power(t2,2) + 
                  44583*Power(t2,3) - 49546*Power(t2,4) + 
                  29240*Power(t2,5) - 5884*Power(t2,6) + 96*Power(t2,7)) \
+ t1*(2557 - 13062*t2 + 40049*Power(t2,2) - 69482*Power(t2,3) + 
                  59875*Power(t2,4) - 21125*Power(t2,5) + 
                  700*Power(t2,6) + 488*Power(t2,7)))) + 
         Power(s,9)*((-19 + 20*s1 - 2*Power(s1,2))*Power(t1,4) + 
            Power(t1,3)*(-19 + 33*s2 + s1*(52 - 39*s2 - 57*t2) + 
               55*t2 + Power(s1,2)*(-32 + 5*s2 + 6*t2)) + 
            Power(t1,2)*(-6 - 15*Power(s2,2) + s2*(18 - 90*t2) - 
               Power(s1,3)*(-1 + t2) + 41*t2 - 51*Power(t2,2) - 
               Power(s1,2)*(32 + 4*Power(s2,2) - 87*t2 + 
                  6*Power(t2,2) + s2*(-31 + 11*t2)) + 
               s1*(39 + 21*Power(s2,2) - 130*t2 + 51*Power(t2,2) + 
                  s2*(-53 + 104*t2))) - 
            t2*(5 + Power(s2,3) - 6*t2 - 3*Power(t2,2) - 
               2*Power(t2,3) + Power(s1,3)*(-1 + t2)*(-1 + s2 + t2) + 
               Power(s2,2)*(3 + 17*t2) + 
               s2*(-9 - 8*t2 + 24*Power(t2,2)) + 
               Power(s1,2)*(-1 + Power(s2,3) + 18*t2 - 
                  23*Power(t2,2) + Power(s2,2)*(-1 + 2*t2) + 
                  s2*(1 - 15*t2 + Power(t2,2))) - 
               s1*(5 + 2*Power(s2,3) + 12*t2 - 26*Power(t2,2) - 
                  3*Power(t2,3) + Power(s2,2)*(2 + 21*t2) + 
                  s2*(-9 - 26*t2 + 26*Power(t2,2)))) + 
            t1*(1 + Power(s2,3) - 25*Power(t2,2) + 13*Power(t2,3) + 
               Power(s1,3)*(-1 + t2)*(-1 + s2 + 2*t2) + 
               Power(s2,2)*(-1 + 32*t2) + 
               s2*(-1 - 26*t2 + 81*Power(t2,2)) + 
               Power(s1,2)*(-5 + Power(s2,3) + 50*t2 - 78*Power(t2,2) + 
                  2*Power(t2,3) + Power(s2,2)*(-5 + 6*t2) + 
                  s2*(9 - 46*t2 + 7*Power(t2,2))) - 
               s1*(-3 + 2*Power(s2,3) + 51*t2 - 104*Power(t2,2) + 
                  11*Power(t2,3) + 6*Power(s2,2)*(-1 + 7*t2) + 
                  s2*(7 - 79*t2 + 91*Power(t2,2))))) + 
         Power(s,8)*(-5 - 7*t1 + 26*Power(t1,2) + 44*Power(t1,3) - 
            18*Power(t1,4) - 52*Power(t1,5) + 11*t2 - 28*t1*t2 - 
            172*Power(t1,2)*t2 - 25*Power(t1,3)*t2 + 
            145*Power(t1,4)*t2 - 2*Power(t2,2) + 144*t1*Power(t2,2) + 
            131*Power(t1,2)*Power(t2,2) - 123*Power(t1,3)*Power(t2,2) - 
            16*Power(t2,3) - 115*t1*Power(t2,3) + 
            19*Power(t1,2)*Power(t2,3) + 27*Power(t2,4) + 
            11*t1*Power(t2,4) + Power(s2,4)*(-6*t1 + 7*t2) + 
            Power(s2,3)*(3 + 53*Power(t1,2) + t1*(3 - 114*t2) + 10*t2 + 
               61*Power(t2,2)) + 
            Power(s1,4)*(-1 + t2)*
             (-Power(s2,2) + 6*Power(t1,2) + t1*(6 - 13*t2) + 
               7*(-1 + t2)*t2 + s2*(1 - 5*t1 + 6*t2)) + 
            Power(s2,2)*(-13 - 140*Power(t1,3) - 32*t2 + 
               3*Power(t2,2) + 83*Power(t2,3) + 
               Power(t1,2)*(-26 + 372*t2) + 
               t1*(17 + 11*t2 - 315*Power(t2,2))) + 
            s2*(15 + 145*Power(t1,4) + Power(t1,3)*(41 - 410*t2) + 
               5*t2 - 56*Power(t2,2) + 2*Power(t2,3) - 16*Power(t2,4) + 
               Power(t1,2)*(-51 - 16*t2 + 369*Power(t2,2)) - 
               t1*(8 - 123*t2 + 27*Power(t2,2) + 88*Power(t2,3))) + 
            Power(s1,3)*(1 + Power(s2,4) + 12*Power(t1,4) + 
               Power(t1,3)*(148 - 50*t2) - 33*t2 + 119*Power(t2,2) - 
               104*Power(t2,3) + 2*Power(t2,4) + 
               Power(s2,3)*(-7 - 9*t1 + 14*t2) + 
               Power(s2,2)*(11 + 27*Power(t1,2) + t1*(64 - 67*t2) - 
                  56*t2 + 35*Power(t2,2)) + 
               Power(t1,2)*(190 - 419*t2 + 66*Power(t2,2)) + 
               t1*(55 - 319*t2 + 375*Power(t2,2) - 30*Power(t2,3)) - 
               s2*(6 + 31*Power(t1,3) + Power(t1,2)*(190 - 103*t2) - 
                  71*t2 + 125*Power(t2,2) - 24*Power(t2,3) + 
                  2*t1*(53 - 165*t2 + 48*Power(t2,2)))) + 
            s1*(9 + 57*Power(t1,5) + Power(s2,4)*(1 + 14*t1 - 16*t2) - 
               6*t2 + 22*Power(t2,2) - 22*Power(t2,3) - 
               47*Power(t2,4) - Power(t2,5) - 
               38*Power(t1,4)*(-5 + 4*t2) - 
               Power(s2,3)*(11 + 102*Power(t1,2) + t1*(41 - 198*t2) - 
                  16*t2 + 94*Power(t2,2)) + 
               Power(t1,3)*(172 - 414*t2 + 115*Power(t2,2)) + 
               Power(t1,2)*(65 - 232*t2 + 211*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(26 - 93*t2 + 82*Power(t2,2) + 60*Power(t2,3) - 
                  16*Power(t2,4)) + 
               Power(s2,2)*(33 + 219*Power(t1,3) + 
                  Power(t1,2)*(270 - 533*t2) + 3*t2 + 
                  160*Power(t2,2) - 96*Power(t2,3) + 
                  t1*(45 - 417*t2 + 410*Power(t2,2))) - 
               s2*(32 + 188*Power(t1,4) + Power(t1,3)*(420 - 503*t2) + 
                  t2 + 87*Power(t2,2) - 170*Power(t2,3) - 
                  26*Power(t2,4) + 
                  Power(t1,2)*(222 - 861*t2 + 416*Power(t2,2)) + 
                  t1*(40 - 300*t2 + 611*Power(t2,2) - 75*Power(t2,3)))) \
+ Power(s1,2)*(-5 - 12*Power(t1,5) + 25*t2 - 135*Power(t2,2) + 
               142*Power(t2,3) + 18*Power(t2,4) + 
               Power(s2,4)*(-2 - 8*t1 + 9*t2) + 
               Power(t1,4)*(-177 + 34*t2) - 
               6*Power(t1,3)*(60 - 78*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(15 + 38*Power(t1,2) + t1*(51 - 58*t2) - 
                  44*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(-289 + 816*t2 - 387*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-72 + 445*t2 - 598*Power(t2,2) + 78*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,2)*(32 + 64*Power(t1,3) + 
                  Power(t1,2)*(263 - 127*t2) - 98*t2 + 
                  178*Power(t2,2) - 9*Power(t2,3) + 
                  t1*(138 - 445*t2 + 72*Power(t2,2))) + 
               s2*(24 + 46*Power(t1,4) + Power(t1,3)*(391 - 112*t2) - 
                  82*t2 + 267*Power(t2,2) - 199*Power(t2,3) + 
                  Power(t1,2)*(480 - 913*t2 + 86*Power(t2,2)) + 
                  t1*(161 - 770*t2 + 721*Power(t2,2) - 20*Power(t2,3))))) \
+ Power(s,7)*(17 - 5*t1 - 70*Power(t1,2) - 2*Power(t1,3) + 
            186*Power(t1,4) + 80*Power(t1,5) - 80*Power(t1,6) + 
            3*Power(s2,5)*(5*t1 - 7*t2) - 39*t2 + 99*t1*t2 + 
            99*Power(t1,2)*t2 - 557*Power(t1,3)*t2 - 
            401*Power(t1,4)*t2 + 214*Power(t1,5)*t2 - 3*Power(t2,2) + 
            2*t1*Power(t2,2) + 462*Power(t1,2)*Power(t2,2) + 
            633*Power(t1,3)*Power(t2,2) - 161*Power(t1,4)*Power(t2,2) + 
            Power(t2,3) - 129*t1*Power(t2,3) - 
            377*Power(t1,2)*Power(t2,3) - Power(t1,3)*Power(t2,3) + 
            38*Power(t2,4) + 59*t1*Power(t2,4) + 
            29*Power(t1,2)*Power(t2,4) + 6*Power(t2,5) - 
            t1*Power(t2,5) - Power(s2,4)*
             (14 + 110*Power(t1,2) + 15*t2 + 119*Power(t2,2) - 
               t1*(5 + 236*t2)) + 
            Power(s2,3)*(50 + 320*Power(t1,3) + 41*t2 - 
               139*Power(t2,2) - 163*Power(t2,3) - 
               Power(t1,2)*(73 + 842*t2) + 
               t1*(-27 + 217*t2 + 682*Power(t2,2))) + 
            Power(s2,2)*(-33 - 450*Power(t1,4) + 15*t2 + 
               259*Power(t2,2) - 123*Power(t2,3) + 55*Power(t2,4) + 
               Power(t1,3)*(211 + 1274*t2) - 
               2*Power(t1,2)*(-115 + 381*t2 + 567*Power(t2,2)) + 
               t1*(-75 - 398*t2 + 718*Power(t2,2) + 255*Power(t2,3))) + 
            s2*(-21 + 305*Power(t1,5) + 23*t2 + 4*Power(t2,2) + 
               94*Power(t2,3) - 185*Power(t2,4) - 
               Power(t1,4)*(223 + 861*t2) + 
               Power(t1,3)*(-375 + 961*t2 + 732*Power(t2,2)) + 
               Power(t1,2)*(31 + 966*t2 - 1278*Power(t2,2) - 
                  101*Power(t2,3)) + 
               t1*(86 - 167*t2 - 825*Power(t2,2) + 725*Power(t2,3) - 
                  75*Power(t2,4))) + 
            Power(s1,5)*(Power(t1,3) + Power(t1,2)*(16 - 15*t2) - 
               20*Power(-1 + t2,2)*t2 + Power(s2,2)*(-7 + t1 + 8*t2) + 
               t1*(15 - 49*t2 + 34*Power(t2,2)) + 
               s2*(7 - 2*Power(t1,2) + 5*t2 - 12*Power(t2,2) + 
                  t1*(-9 + 7*t2))) + 
            Power(s1,4)*(-15 - 17*Power(t1,4) + 
               Power(s2,3)*(5 + 5*t1 - 38*t2) + 174*t2 - 
               411*Power(t2,2) + 280*Power(t2,3) - 8*Power(t2,4) + 
               5*Power(t1,3)*(-64 + 27*t2) + 
               Power(t1,2)*(-501 + 1049*t2 - 233*Power(t2,2)) - 
               Power(s2,2)*(22 + 27*Power(t1,2) + t1*(147 - 193*t2) - 
                  217*t2 + 140*Power(t2,2)) + 
               t1*(-205 + 981*t2 - 1014*Power(t2,2) + 
                  123*Power(t2,3)) + 
               s2*(31 + 39*Power(t1,3) + Power(t1,2)*(442 - 290*t2) - 
                  330*t2 + 464*Power(t2,2) - 110*Power(t2,3) + 
                  t1*(327 - 1004*t2 + 367*Power(t2,2)))) + 
            Power(s1,3)*(26 - 8*Power(s2,5) + 74*Power(t1,5) + 
               Power(t1,4)*(714 - 281*t2) + 
               Power(s2,4)*(49 + 78*t1 - 112*t2) - 225*t2 + 
               578*Power(t2,2) - 380*Power(t2,3) - 79*Power(t2,4) - 
               Power(s2,3)*(81 + 268*Power(t1,2) + t1*(442 - 591*t2) - 
                  522*t2 + 244*Power(t2,2)) + 
               Power(t1,3)*(1276 - 2027*t2 + 343*Power(t2,2)) + 
               Power(t1,2)*(944 - 2938*t2 + 1722*Power(t2,2) - 
                  139*Power(t2,3)) + 
               t1*(302 - 1709*t2 + 1959*Power(t2,2) - 
                  330*Power(t2,3) + 3*Power(t2,4)) + 
               Power(s2,2)*(98 + 408*Power(t1,3) + 
                  Power(t1,2)*(1442 - 1143*t2) - 870*t2 + 
                  1062*Power(t2,2) - 156*Power(t2,3) + 
                  t1*(742 - 2774*t2 + 855*Power(t2,2))) - 
               s2*(80 + 284*Power(t1,4) + Power(t1,3)*(1763 - 945*t2) - 
                  654*t2 + 1313*Power(t2,2) - 770*Power(t2,3) + 
                  16*Power(t2,4) + 
                  2*Power(t1,2)*(980 - 2195*t2 + 481*Power(t2,2)) + 
                  t1*(661 - 3663*t2 + 3282*Power(t2,2) - 
                     317*Power(t2,3)))) + 
            Power(s1,2)*(-4 - 30*Power(t1,6) + 
               2*Power(s2,5)*(7 + 14*t1 - 18*t2) + 8*t2 - 
               7*Power(t2,2) - 222*Power(t2,3) + 343*Power(t2,4) + 
               2*Power(t2,5) + 6*Power(t1,5)*(-70 + 13*t2) + 
               Power(t1,4)*(-975 + 1075*t2 - 54*Power(t2,2)) - 
               Power(s2,4)*(91 + 154*Power(t1,2) + t1*(226 - 248*t2) - 
                  272*t2 + 72*Power(t2,2)) - 
               Power(t1,3)*(776 - 1989*t2 + 770*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,2)*(-191 + 1027*t2 - 625*Power(t2,2) - 
                  3*Power(t2,3) + 12*Power(t2,4)) + 
               t1*(-64 + 337*t2 + 263*Power(t2,2) - 732*Power(t2,3) + 
                  116*Power(t2,4)) + 
               Power(s2,3)*(160 + 324*Power(t1,3) + 
                  Power(t1,2)*(1054 - 626*t2) - 712*t2 + 
                  769*Power(t2,2) - 36*Power(t2,3) + 
                  3*t1*(228 - 665*t2 + 108*Power(t2,2))) - 
               Power(s2,2)*(130 + 328*Power(t1,4) + 
                  Power(t1,3)*(1906 - 730*t2) - 628*t2 + 
                  1218*Power(t2,2) - 660*Power(t2,3) + 
                  Power(t1,2)*(2166 - 4409*t2 + 490*Power(t2,2)) + 
                  t1*(643 - 3904*t2 + 3014*Power(t2,2) - 
                     88*Power(t2,3))) + 
               s2*(45 + 160*Power(t1,5) + Power(t1,4)*(1484 - 394*t2) - 
                  135*t2 + 513*Power(t2,2) - 530*Power(t2,3) - 
                  173*Power(t2,4) + 
                  Power(t1,3)*(2548 - 3761*t2 + 292*Power(t2,2)) + 
                  Power(t1,2)*
                   (1351 - 5301*t2 + 2919*Power(t2,2) - 42*Power(t2,3)) \
+ t1*(214 - 2391*t2 + 2862*Power(t2,2) - 469*Power(t2,3) - 
                     16*Power(t2,4)))) + 
            s1*(-22 + 91*Power(t1,6) + Power(t1,5)*(309 - 224*t2) + 
               87*t2 - 157*Power(t2,2) + 281*Power(t2,3) - 
               244*Power(t2,4) - 25*Power(t2,5) + 
               Power(s2,5)*(-6 - 42*t1 + 56*t2) + 
               Power(t1,4)*(124 - 557*t2 + 133*Power(t2,2)) + 
               Power(s2,4)*(58 + 283*Power(t1,2) + t1*(118 - 546*t2) - 
                  120*t2 + 236*Power(t2,2)) + 
               Power(t1,3)*(-201 + 339*t2 + 45*Power(t2,2) + 
                  35*Power(t2,3)) + 
               Power(t1,2)*(-177 + 737*t2 - 1130*Power(t2,2) + 
                  320*Power(t2,3) - 28*Power(t2,4)) - 
               t1*(28 - 282*t2 + 1121*Power(t2,2) - 911*Power(t2,3) + 
                  92*Power(t2,4) + 7*Power(t2,5)) - 
               Power(s2,3)*(142 + 688*Power(t1,3) + 
                  Power(t1,2)*(637 - 1574*t2) - 133*t2 + 
                  407*Power(t2,2) - 192*Power(t2,3) + 
                  t1*(166 - 1124*t2 + 1061*Power(t2,2))) + 
               Power(s2,2)*(106 + 786*Power(t1,4) + 
                  Power(t1,3)*(1253 - 1958*t2) + 24*t2 - 
                  16*Power(t2,2) - 327*Power(t2,3) - 100*Power(t2,4) + 
                  Power(t1,2)*(333 - 2457*t2 + 1463*Power(t2,2)) + 
                  t1*(100 - 672*t2 + 1374*Power(t2,2) - 191*Power(t2,3))\
) + s2*(10 - 430*Power(t1,5) - 194*t2 + 333*Power(t2,2) - 
                  211*Power(t2,3) + 349*Power(t2,4) + 8*Power(t2,5) + 
                  61*Power(t1,4)*(-17 + 18*t2) + 
                  Power(t1,3)*(-349 + 2010*t2 - 771*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (-70 - 41*t2 + 278*Power(t2,2) + 8*Power(t2,3)) + 
                  t1*(20 - 177*t2 + 877*Power(t2,2) - 488*Power(t2,3) + 
                     119*Power(t2,4))))) + 
         Power(s,6)*(33 + 108*t1 + 66*Power(t1,2) - 257*Power(t1,3) - 
            373*Power(t1,4) + 178*Power(t1,5) + 236*Power(t1,6) - 
            75*Power(t1,7) - 5*Power(s2,6)*(4*t1 - 7*t2) - 138*t2 - 
            586*t1*t2 + Power(t1,2)*t2 + 892*Power(t1,3)*t2 - 
            443*Power(t1,4)*t2 - 898*Power(t1,5)*t2 + 
            190*Power(t1,6)*t2 + 190*Power(t2,2) + 508*t1*Power(t2,2) - 
            335*Power(t1,2)*Power(t2,2) + 174*Power(t1,3)*Power(t2,2) + 
            1226*Power(t1,4)*Power(t2,2) - 115*Power(t1,5)*Power(t2,2) - 
            142*Power(t2,3) + 320*t1*Power(t2,3) + 
            350*Power(t1,2)*Power(t2,3) - 697*Power(t1,3)*Power(t2,3) - 
            45*Power(t1,4)*Power(t2,3) - 28*Power(t2,4) - 
            368*t1*Power(t2,4) + 119*Power(t1,2)*Power(t2,4) + 
            50*Power(t1,3)*Power(t2,4) + 109*Power(t2,5) + 
            23*t1*Power(t2,5) - 5*Power(t1,2)*Power(t2,5) - 
            9*Power(t2,6) + Power(s2,5)*
             (25 + 140*Power(t1,2) + 32*t2 + 133*Power(t2,2) - 
               t1*(33 + 310*t2)) - 
            Power(s2,4)*(47 + 435*Power(t1,3) + 112*t2 - 
               503*Power(t2,2) - 203*Power(t2,3) - 
               50*Power(t1,2)*(6 + 23*t2) + 
               t1*(23 + 696*t2 + 879*Power(t2,2))) + 
            Power(s2,3)*(-124 + 740*Power(t1,4) + 302*t2 - 
               693*Power(t2,2) + 412*Power(t2,3) - 105*Power(t2,4) - 
               2*Power(t1,3)*(469 + 1065*t2) + 
               Power(t1,2)*(-147 + 2744*t2 + 1898*Power(t2,2)) + 
               t1*(328 + 581*t2 - 2571*Power(t2,2) - 424*Power(t2,3))) + 
            Power(s2,2)*(311 - 710*Power(t1,5) - 740*t2 + 
               120*Power(t2,2) - 50*Power(t2,3) + 470*Power(t2,4) + 
               7*Power(t1,4)*(192 + 295*t2) + 
               Power(t1,3)*(441 - 4426*t2 - 1806*Power(t2,2)) + 
               Power(t1,2)*(-858 - 1409*t2 + 4907*Power(t2,2) + 
                  236*Power(t2,3)) + 
               t1*(-137 + 508*t2 + 2104*Power(t2,2) - 
                  1987*Power(t2,3) + 215*Power(t2,4))) + 
            s2*(-197 + 360*Power(t1,6) + 604*t2 - 229*Power(t2,2) - 
               131*Power(t2,3) - 29*Power(t2,4) - 68*Power(t2,5) - 
               Power(t1,5)*(909 + 1000*t2) + 
               Power(t1,4)*(-474 + 3244*t2 + 769*Power(t2,2)) + 
               Power(t1,3)*(950 + 1383*t2 - 4065*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,2)*(478 - 1644*t2 - 1473*Power(t2,2) + 
                  2134*Power(t2,3) - 167*Power(t2,4)) + 
               t1*(-216 + 524*t2 - 79*Power(t2,2) - 163*Power(t2,3) - 
                  336*Power(t2,4) + 8*Power(t2,5))) + 
            Power(s1,6)*(Power(s2,3) - 6*Power(t1,3) + 
               Power(s2,2)*(19 - 8*t1 - 24*t2) + 
               30*Power(-1 + t2,2)*t2 + Power(t1,2)*(-26 + 21*t2) - 
               5*t1*(4 - 13*t2 + 9*Power(t2,2)) + 
               s2*(13*Power(t1,2) + t1*(7 + 3*t2) + 
                  5*(-4 + 3*t2 + Power(t2,2)))) + 
            Power(s1,5)*(52 - 17*Power(s2,4) - 27*Power(t1,4) + 
               Power(t1,3)*(284 - 122*t2) - 391*t2 + 748*Power(t2,2) - 
               436*Power(t2,3) + 12*Power(t2,4) + 
               Power(s2,3)*(91 + 83*t1 + 2*t2) + 
               Power(t1,2)*(622 - 1353*t2 + 373*Power(t2,2)) + 
               t1*(357 - 1561*t2 + 1535*Power(t2,2) - 
                  236*Power(t2,3)) - 
               Power(s2,2)*(76 + 142*Power(t1,2) + 252*t2 - 
                  235*Power(t2,2) + t1*(78 + 131*t2)) + 
               s2*(-44 + 103*Power(t1,3) + 587*t2 - 821*Power(t2,2) + 
                  228*Power(t2,3) + Power(t1,2)*(-282 + 251*t2) - 
                  2*t1*(163 - 675*t2 + 309*Power(t2,2)))) + 
            Power(s1,3)*(-57 + 28*Power(s2,6) + 193*Power(t1,6) + 
               Power(t1,5)*(1530 - 696*t2) - 
               2*Power(s2,5)*(73 + 147*t1 - 196*t2) + 279*t2 - 
               937*Power(t2,2) + 1784*Power(t2,3) - 1228*Power(t2,4) + 
               9*Power(t2,5) + 
               Power(t1,4)*(2778 - 4375*t2 + 790*Power(t2,2)) + 
               Power(s2,4)*(183 + 1136*Power(t1,2) + 
                  t1*(1422 - 2345*t2) - 1972*t2 + 812*Power(t2,2)) + 
               Power(t1,3)*(1580 - 6391*t2 + 3276*Power(t2,2) - 
                  264*Power(t2,3)) + 
               t1*(-58 + 187*t2 - 2649*Power(t2,2) + 
                  3043*Power(t2,3) - 573*Power(t2,4)) + 
               Power(t1,2)*(-26 - 2574*t2 + 1497*Power(t2,2) + 
                  133*Power(t2,3) - 23*Power(t2,4)) + 
               Power(s2,3)*(1 - 2117*Power(t1,3) + 3624*t2 - 
                  3538*Power(t2,2) + 504*Power(t2,3) + 
                  Power(t1,2)*(-5061 + 5464*t2) + 
                  t1*(-2038 + 10718*t2 - 3465*Power(t2,2))) + 
               Power(s2,2)*(-174 + 2055*Power(t1,4) + 
                  Power(t1,3)*(7970 - 6157*t2) - 2303*t2 + 
                  3969*Power(t2,2) - 1931*Power(t2,3) + 
                  56*Power(t2,4) + 
                  Power(t1,2)*(6581 - 20240*t2 + 5313*Power(t2,2)) + 
                  t1*(777 - 15388*t2 + 12239*Power(t2,2) - 
                     1379*Power(t2,3))) + 
               s2*(197 - 1001*Power(t1,5) - 96*t2 - 371*Power(t2,2) + 
                  28*Power(t2,3) + 742*Power(t2,4) + 
                  Power(t1,4)*(-5715 + 3342*t2) + 
                  Power(t1,3)*(-7504 + 15869*t2 - 3450*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2530 + 18270*t2 - 11761*Power(t2,2) + 
                     1130*Power(t2,3)) + 
                  t1*(199 + 6805*t2 - 7307*Power(t2,2) + 
                     1250*Power(t2,3) - 21*Power(t2,4)))) - 
            Power(s1,4)*(103 + 126*Power(t1,5) + 
               Power(s2,4)*(2 + 52*t1 - 294*t2) - 659*t2 + 
               1189*Power(t2,2) - 498*Power(t2,3) - 210*Power(t2,4) - 
               39*Power(t1,4)*(-34 + 19*t2) + 
               Power(t1,3)*(2271 - 4635*t2 + 1145*Power(t2,2)) + 
               Power(t1,2)*(1621 - 6166*t2 + 4407*Power(t2,2) - 
                  557*Power(t2,3)) + 
               t1*(679 - 3555*t2 + 3905*Power(t2,2) - 
                  881*Power(t2,3) + 27*Power(t2,4)) + 
               Power(s2,3)*(56 - 292*Power(t1,2) + 1557*t2 - 
                  812*Power(t2,2) + 10*t1*(-59 + 154*t2)) + 
               Power(s2,2)*(54 + 554*Power(t1,3) + 
                  Power(t1,2)*(2500 - 2959*t2) - 2543*t2 + 
                  3073*Power(t2,2) - 574*Power(t2,3) + 
                  t1*(1045 - 7234*t2 + 2783*Power(t2,2))) - 
               s2*(440*Power(t1,4) + Power(t1,3)*(3238 - 2454*t2) + 
                  Power(t1,2)*(3427 - 10452*t2 + 3126*Power(t2,2)) + 
                  t1*(1179 - 8745*t2 + 8260*Power(t2,2) - 
                     1154*Power(t2,3)) + 
                  2*(96 - 935*t2 + 1650*Power(t2,2) - 864*Power(t2,3) + 
                     28*Power(t2,4)))) + 
            Power(s1,2)*(-40*Power(t1,7) - 
               14*Power(s2,6)*(3 + 4*t1 - 6*t2) + 
               Power(t1,6)*(-559 + 90*t2) + 
               Power(t1,5)*(-1198 + 1365*t2 - 30*Power(t2,2)) + 
               Power(s2,5)*(225 + 564*t1 + 350*Power(t1,2) - 756*t2 - 
                  616*t1*t2 + 168*Power(t2,2)) - 
               Power(t1,4)*(529 - 1777*t2 + 705*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(t1,3)*(705 - 1057*t2 + 1815*Power(t2,2) - 
                  422*Power(t2,3) + 30*Power(t2,4)) + 
               Power(t1,2)*(860 - 2996*t2 + 6470*Power(t2,2) - 
                  3284*Power(t2,3) + 294*Power(t2,4)) + 
               t1*(527 - 2887*t2 + 6627*Power(t2,2) - 
                  4756*Power(t2,3) + 712*Power(t2,4) + 27*Power(t2,5)) \
+ 2*(123 - 510*t2 + 955*Power(t2,2) - 1078*Power(t2,3) + 
                  496*Power(t2,4) + 89*Power(t2,5)) - 
               Power(s2,4)*(252 + 880*Power(t1,3) + 
                  Power(t1,2)*(2537 - 1750*t2) - 1895*t2 + 
                  1659*Power(t2,2) - 84*Power(t2,3) + 
                  4*t1*(423 - 1277*t2 + 210*Power(t2,2))) + 
               Power(s2,3)*(1140*Power(t1,4) + 
                  Power(t1,3)*(5206 - 2484*t2) + 
                  Power(t1,2)*(5144 - 12299*t2 + 1526*Power(t2,2)) + 
                  t1*(1072 - 9559*t2 + 6787*Power(t2,2) - 
                     224*Power(t2,3)) - 
                  2*(87 + 469*t2 - 766*Power(t2,2) + 490*Power(t2,3))) \
+ Power(s2,2)*(759 - 800*Power(t1,5) - 2150*t2 + 2607*Power(t2,2) - 
                  829*Power(t2,3) + 707*Power(t2,4) + 
                  2*Power(t1,4)*(-2703 + 926*t2) + 
                  Power(t1,3)*(-7310 + 13663*t2 - 1234*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2079 + 14978*t2 - 8888*Power(t2,2) + 
                     126*Power(t2,3)) + 
                  t1*(937 + 3713*t2 - 2384*Power(t2,2) + 
                     519*Power(t2,3) + 56*Power(t2,4))) + 
               s2*(-780 + 286*Power(t1,6) + 
                  Power(t1,5)*(2774 - 676*t2) + 2924*t2 - 
                  4327*Power(t2,2) + 3690*Power(t2,3) - 
                  2193*Power(t2,4) - 14*Power(t2,5) + 
                  Power(t1,4)*(4831 - 7081*t2 + 410*Power(t2,2)) + 
                  Power(t1,3)*
                   (1788 - 9091*t2 + 4465*Power(t2,2) + 64*Power(t2,3)) \
+ t1*(-1379 + 3824*t2 - 9813*Power(t2,2) + 6321*Power(t2,3) - 
                     941*Power(t2,4)) - 
                  Power(t1,2)*
                   (1480 + 1236*t2 + 1499*Power(t2,2) - 
                     797*Power(t2,3) + 84*Power(t2,4)))) + 
            s1*(-177 + 90*Power(t1,7) + Power(t1,6)*(273 - 205*t2) + 
               Power(s2,6)*(15 + 70*t1 - 112*t2) + 616*t2 - 
               735*Power(t2,2) + 506*Power(t2,3) - 32*Power(t2,4) - 
               247*Power(t2,5) - 6*Power(t2,6) + 
               Power(t1,5)*(-231 - 355*t2 + 95*Power(t2,2)) - 
               Power(s2,5)*(119 + 480*Power(t1,2) + t1*(181 - 966*t2) - 
                  270*t2 + 364*Power(t2,2)) + 
               Power(t1,4)*(-447 + 1680*t2 - 361*Power(t2,2) + 
                  45*Power(t2,3)) + 
               Power(t1,3)*(94 + 1630*t2 - 3198*Power(t2,2) + 
                  622*Power(t2,3) - 5*Power(t2,4)) + 
               t1*(-256 + 1276*t2 - 2085*Power(t2,2) + 
                  702*Power(t2,3) + 299*Power(t2,4) - 61*Power(t2,5)) + 
               Power(t1,2)*(148 + 549*t2 - 3141*Power(t2,2) + 
                  1697*Power(t2,3) - 112*Power(t2,4) - 20*Power(t2,5)) + 
               Power(s2,4)*(179 + 1310*Power(t1,3) + 
                  Power(t1,2)*(796 - 2941*t2) - 80*t2 + 
                  294*Power(t2,2) - 210*Power(t2,3) + 
                  t1*(348 - 1661*t2 + 1701*Power(t2,2))) - 
               Power(s2,3)*(-215 + 1840*Power(t1,4) + 
                  Power(t1,3)*(1725 - 4352*t2) + 1241*t2 - 
                  1810*Power(t2,2) + 76*Power(t2,3) - 224*Power(t2,4) + 
                  Power(t1,2)*(74 - 3495*t2 + 2834*Power(t2,2)) + 
                  t1*(299 + 351*t2 + 493*Power(t2,2) - 161*Power(t2,3))) \
+ Power(s2,2)*(-776 + 1410*Power(t1,5) + Power(t1,4)*(1984 - 3390*t2) + 
                  2715*t2 - 3375*Power(t2,2) + 1765*Power(t2,3) - 
                  1015*Power(t2,4) - 28*Power(t2,5) + 
                  Power(t1,3)*(-651 - 3442*t2 + 2116*Power(t2,2)) + 
                  Power(t1,2)*
                   (-412 + 3029*t2 - 522*Power(t2,2) + 277*Power(t2,3)) \
+ t1*(-142 + 3037*t2 - 7840*Power(t2,2) + 2204*Power(t2,3) - 
                     385*Power(t2,4))) + 
               s2*(665 - 560*Power(t1,6) - 2150*t2 + 2223*Power(t2,2) - 
                  1637*Power(t2,3) + 1063*Power(t2,4) + 
                  186*Power(t2,5) + 14*Power(t1,5)*(-83 + 95*t2) + 
                  Power(t1,4)*(727 + 1693*t2 - 714*Power(t2,2)) + 
                  Power(t1,3)*
                   (979 - 4278*t2 + 1082*Power(t2,2) - 273*Power(t2,3)) \
+ Power(t1,2)*(-98 - 3683*t2 + 9162*Power(t2,2) - 2423*Power(t2,3) + 
                     168*Power(t2,4)) + 
                  t1*(464 - 3078*t2 + 7995*Power(t2,2) - 
                     4686*Power(t2,3) + 624*Power(t2,4) + 49*Power(t2,5))\
))) - Power(s,5)*(223 + 266*t1 - 513*Power(t1,2) - 1370*Power(t1,3) - 
            697*Power(t1,4) + 555*Power(t1,5) + 108*Power(t1,6) - 
            287*Power(t1,7) + 43*Power(t1,8) - 
            5*Power(s2,7)*(3*t1 - 7*t2) - 1016*t2 - 764*t1*t2 + 
            2606*Power(t1,2)*t2 + 2812*Power(t1,3)*t2 - 
            961*Power(t1,4)*t2 - 529*Power(t1,5)*t2 + 
            999*Power(t1,6)*t2 - 99*Power(t1,7)*t2 + 1674*Power(t2,2) + 
            1483*t1*Power(t2,2) - 1335*Power(t1,2)*Power(t2,2) + 
            757*Power(t1,3)*Power(t2,2) + 1426*Power(t1,4)*Power(t2,2) - 
            1250*Power(t1,5)*Power(t2,2) + 29*Power(t1,6)*Power(t2,2) - 
            1355*Power(t2,3) - 1899*t1*Power(t2,3) - 
            2288*Power(t1,2)*Power(t2,3) - 
            2319*Power(t1,3)*Power(t2,3) + 545*Power(t1,4)*Power(t2,3) + 
            77*Power(t1,5)*Power(t2,3) + 781*Power(t2,4) + 
            1226*t1*Power(t2,4) + 1638*Power(t1,2)*Power(t2,4) + 
            150*Power(t1,3)*Power(t2,4) - 60*Power(t1,4)*Power(t2,4) - 
            325*Power(t2,5) - 334*t1*Power(t2,5) - 
            206*Power(t1,2)*Power(t2,5) + 10*Power(t1,3)*Power(t2,5) + 
            10*Power(t2,6) + 47*t1*Power(t2,6) + 2*Power(t2,7) + 
            Power(s2,6)*(20 + 107*Power(t1,2) + 75*t2 + 
               77*Power(t2,2) - 3*t1*(19 + 88*t2)) + 
            5*Power(s1,7)*(Power(s2,3) - 3*Power(t1,3) + 
               Power(s2,2)*(5 - 5*t1 - 7*t2) + 5*Power(-1 + t2,2)*t2 + 
               Power(t1,2)*(-6 + 4*t2) + 
               t1*(-3 + 9*t2 - 6*Power(t2,2)) + 
               s2*(-6 + t1 + 7*Power(t1,2) + 9*t2 + 3*t1*t2 - 
                  3*Power(t2,2))) + 
            Power(s2,5)*(30 - 353*Power(t1,3) - 272*t2 + 
               841*Power(t2,2) + 175*Power(t2,3) + 
               Power(t1,2)*(454 + 974*t2) - 
               2*t1*(37 + 546*t2 + 327*Power(t2,2))) + 
            Power(s2,4)*(-342 + 685*Power(t1,4) + 671*t2 - 
               558*Power(t2,2) + 501*Power(t2,3) - 119*Power(t2,4) - 
               3*Power(t1,3)*(511 + 685*t2) + 
               Power(t1,2)*(310 + 4553*t2 + 1825*Power(t2,2)) + 
               t1*(190 + 601*t2 - 4469*Power(t2,2) - 479*Power(t2,3))) + 
            Power(s2,3)*(316 - 825*Power(t1,5) - 516*t2 - 
               1543*Power(t2,2) + 1189*Power(t2,3) + 555*Power(t2,4) + 
               Power(t1,4)*(2732 + 2535*t2) - 
               2*Power(t1,3)*(397 + 4428*t2 + 1175*Power(t2,2)) + 
               Power(t1,2)*(-1075 + 4*t2 + 9665*Power(t2,2) + 
                  372*Power(t2,3)) + 
               t1*(876 - 693*t2 + 1397*Power(t2,2) - 2601*Power(t2,3) + 
                  331*Power(t2,4))) + 
            Power(s2,2)*(401 + 605*Power(t1,6) - 1578*t2 + 
               3102*Power(t2,2) - 2282*Power(t2,3) + 641*Power(t2,4) - 
               230*Power(t2,5) - Power(t1,5)*(2683 + 1790*t2) + 
               Power(t1,4)*(962 + 9045*t2 + 1485*Power(t2,2)) + 
               Power(t1,3)*(2015 - 1252*t2 - 10537*Power(t2,2) + 
                  70*Power(t2,3)) + 
               Power(t1,2)*(-1647 - 1308*t2 + 372*Power(t2,2) + 
                  4184*Power(t2,3) - 397*Power(t2,4)) + 
               t1*(-1255 + 3815*t2 + 2311*Power(t2,2) - 
                  4488*Power(t2,3) - 535*Power(t2,4) + 27*Power(t2,5))) \
- s2*(648 + 247*Power(t1,7) - 2599*t2 + 3555*Power(t2,2) - 
               1898*Power(t2,3) + 871*Power(t2,4) - 610*Power(t2,5) + 
               40*Power(t2,6) - 2*Power(t1,6)*(687 + 332*t2) + 
               Power(t1,5)*(532 + 4724*t2 + 412*Power(t2,2)) + 
               Power(t1,4)*(1715 - 1448*t2 - 5750*Power(t2,2) + 
                  215*Power(t2,3)) + 
               Power(t1,3)*(-1810 - 2291*t2 + 2637*Power(t2,2) + 
                  2629*Power(t2,3) - 245*Power(t2,4)) + 
               t1*(-65 + 1579*t2 + 784*Power(t2,2) - 
                  4674*Power(t2,3) + 2597*Power(t2,4) - 247*Power(t2,5)\
) + Power(t1,2)*(-2341 + 6095*t2 + 1559*Power(t2,2) - 
                  5416*Power(t2,3) - 22*Power(t2,4) + 35*Power(t2,5))) + 
            Power(s1,6)*(78 - 47*Power(s2,4) - 110*Power(t1,4) + 
               Power(s2,3)*(276 + 236*t1 - 133*t2) - 449*t2 + 
               744*Power(t2,2) - 387*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,3)*(-62 + 82*t2) + 
               Power(t1,2)*(265 - 748*t2 + 264*Power(t2,2)) + 
               t1*(301 - 1284*t2 + 1272*Power(t2,2) - 
                  244*Power(t2,3)) + 
               Power(s2,2)*(-308 - 441*Power(t1,2) + 88*t2 + 
                  163*Power(t2,2) + t1*(-687 + 298*t2)) + 
               s2*(16 + 362*Power(t1,3) + Power(t1,2)*(479 - 247*t2) + 
                  429*t2 - 729*Power(t2,2) + 257*Power(t2,3) + 
                  t1*(170 + 550*t2 - 462*Power(t2,2)))) + 
            Power(s1,5)*(-225 - 102*Power(s2,5) + 35*Power(t1,5) + 
               1012*t2 - 1361*Power(t2,2) + 315*Power(t2,3) + 
               295*Power(t2,4) + 9*Power(s2,4)*(61 + 60*t1 + 20*t2) + 
               Power(t1,4)*(-857 + 752*t2) + 
               Power(t1,3)*(-1692 + 5441*t2 - 1724*Power(t2,2)) + 
               Power(t1,2)*(-1456 + 7438*t2 - 6361*Power(t2,2) + 
                  983*Power(t2,3)) + 
               t1*(-953 + 4391*t2 - 4896*Power(t2,2) + 
                  1449*Power(t2,3) - 46*Power(t2,4)) - 
               Power(s2,3)*(890 + 1043*Power(t1,2) + 1674*t2 - 
                  1158*Power(t2,2) + 2*t1*(791 + 578*t2)) + 
               Power(s2,2)*(245 + 909*Power(t1,3) + 3397*t2 - 
                  4523*Power(t2,2) + 948*Power(t2,3) + 
                  Power(t1,2)*(665 + 2524*t2) + 
                  t1*(1189 + 8130*t2 - 3969*Power(t2,2))) + 
               s2*(369 - 339*Power(t1,4) - 2824*t2 + 4671*Power(t2,2) - 
                  2341*Power(t2,3) + 72*Power(t2,4) - 
                  25*Power(t1,3)*(-49 + 92*t2) + 
                  Power(t1,2)*(1442 - 11982*t2 + 4535*Power(t2,2)) + 
                  t1*(841 - 10972*t2 + 11467*Power(t2,2) - 
                     1899*Power(t2,3)))) + 
            Power(s1,3)*(561 + 56*Power(s2,7) - 275*Power(t1,7) - 
               2702*t2 + 5418*Power(t2,2) - 5207*Power(t2,3) + 
               1433*Power(t2,4) + 617*Power(t2,5) + 
               Power(s2,6)*(-239 - 630*t1 + 784*t2) + 
               Power(t1,6)*(-1856 + 945*t2) - 
               3*Power(t1,5)*(940 - 1721*t2 + 325*Power(t2,2)) + 
               Power(s2,5)*(159 + 2700*Power(t1,2) + 
                  t1*(2627 - 5285*t2) - 3880*t2 + 1582*Power(t2,2)) + 
               Power(t1,4)*(-725 + 4789*t2 - 2438*Power(t2,2) + 
                  215*Power(t2,3)) + 
               Power(t1,3)*(944 - 2623*t2 + 8737*Power(t2,2) - 
                  2600*Power(t2,3) + 90*Power(t2,4)) + 
               Power(t1,2)*(768 - 7681*t2 + 21202*Power(t2,2) - 
                  13582*Power(t2,3) + 1723*Power(t2,4)) + 
               t1*(1091 - 8231*t2 + 18012*Power(t2,2) - 
                  13292*Power(t2,3) + 2882*Power(t2,4) + 8*Power(t2,5)) \
+ Power(s2,4)*(561 - 5895*Power(t1,3) + 6560*t2 - 5674*Power(t2,2) + 
                  966*Power(t2,3) + Power(t1,2)*(-10358 + 14423*t2) + 
                  t1*(-2993 + 22809*t2 - 7959*Power(t2,2))) + 
               Power(s2,3)*(-1505 + 7220*Power(t1,4) + 
                  Power(t1,3)*(19558 - 20344*t2) - 1057*t2 + 
                  818*Power(t2,2) - 1240*Power(t2,3) + 
                  112*Power(t2,4) + 
                  9*Power(t1,2)*(1271 - 5624*t2 + 1708*Power(t2,2)) - 
                  t1*(822 + 30276*t2 - 22163*Power(t2,2) + 
                     3297*Power(t2,3))) + 
               Power(s2,2)*(2367 - 5016*Power(t1,5) - 8367*t2 + 
                  13540*Power(t2,2) - 8493*Power(t2,3) + 
                  2793*Power(t2,4) + Power(t1,4)*(-19211 + 15584*t2) + 
                  Power(t1,3)*
                   (-17355 + 53488*t2 - 14170*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1065 + 45193*t2 - 29013*Power(t2,2) + 
                     3861*Power(t2,3)) + 
                  t1*(3288 + 5296*t2 + 5444*Power(t2,2) - 
                     2403*Power(t2,3) - 63*Power(t2,4))) + 
               s2*(-1984 + 1840*Power(t1,6) + 
                  Power(t1,5)*(9479 - 6107*t2) + 8618*t2 - 
                  14986*Power(t2,2) + 13723*Power(t2,3) - 
                  6255*Power(t2,4) + 54*Power(t2,5) + 
                  2*Power(t1,4)*(5785 - 13482*t2 + 3075*Power(t2,2)) + 
                  Power(t1,3)*
                   (2051 - 26266*t2 + 14962*Power(t2,2) - 
                     1745*Power(t2,3)) + 
                  t1*(-2637 + 14736*t2 - 37446*Power(t2,2) + 
                     25757*Power(t2,3) - 4320*Power(t2,4)) - 
                  Power(t1,2)*
                   (2811 + 885*t2 + 15578*Power(t2,2) - 
                     6054*Power(t2,3) + 138*Power(t2,4)))) + 
            Power(s1,4)*(-94 + 365*Power(t1,6) + 883*t2 - 
               2982*Power(t2,2) + 4271*Power(t2,3) - 2188*Power(t2,4) + 
               20*Power(t2,5) - 3*Power(t1,5)*(-886 + 615*t2) + 
               Power(s2,5)*(83 - 207*t1 + 910*t2) + 
               Power(s2,4)*(-600 + 1260*Power(t1,2) + 
                  t1*(1082 - 5467*t2) - 4809*t2 + 2268*Power(t2,2)) + 
               Power(t1,4)*(4366 - 9797*t2 + 2603*Power(t2,2)) + 
               Power(t1,3)*(2341 - 13750*t2 + 8498*Power(t2,2) - 
                  1117*Power(t2,3)) + 
               t1*(61 + 1013*t2 - 5613*Power(t2,2) + 
                  5714*Power(t2,3) - 1380*Power(t2,4)) + 
               Power(t1,2)*(321 - 6064*t2 + 4323*Power(t2,2) - 
                  6*Power(t2,3) - 6*Power(t2,4)) + 
               Power(s2,3)*(776 - 2903*Power(t1,3) + 8318*t2 - 
                  8429*Power(t2,2) + 1526*Power(t2,3) + 
                  3*Power(t1,2)*(-2209 + 4309*t2) + 
                  t1*(-470 + 24592*t2 - 9719*Power(t2,2))) + 
               Power(s2,2)*(-381 + 3219*Power(t1,4) - 5004*t2 + 
                  7867*Power(t2,2) - 3402*Power(t2,3) + 
                  168*Power(t2,4) - 14*Power(t1,3)*(-881 + 1067*t2) + 
                  Power(t1,2)*(7485 - 45004*t2 + 15318*Power(t2,2)) - 
                  t1*(322 + 32585*t2 - 27853*Power(t2,2) + 
                     4341*Power(t2,3))) - 
               s2*(-282 + 1734*Power(t1,5) + 
                  Power(t1,4)*(9530 - 8413*t2) + 423*t2 - 
                  823*Power(t2,2) + 1846*Power(t2,3) - 
                  1639*Power(t2,4) + 
                  Power(t1,3)*(10781 - 35018*t2 + 10470*Power(t2,2)) + 
                  Power(t1,2)*
                   (2956 - 38066*t2 + 27727*Power(t2,2) - 
                     3939*Power(t2,3)) + 
                  t1*(79 - 13222*t2 + 13701*Power(t2,2) - 
                     2634*Power(t2,3) + 162*Power(t2,4)))) + 
            Power(s1,2)*(-146 + 30*Power(t1,8) + 
               Power(t1,7)*(455 - 50*t2) - 
               14*Power(s2,7)*(5 + 5*t1 - 9*t2) + 337*t2 + 
               520*Power(t2,2) - 2249*Power(t2,3) + 3124*Power(t2,4) - 
               1660*Power(t2,5) - 16*Power(t2,6) - 
               5*Power(t1,6)*(-147 + 203*t2 + 6*Power(t2,2)) + 
               2*Power(s2,6)*
                (141 + 245*Power(t1,2) + t1*(432 - 490*t2) - 581*t2 + 
                  126*Power(t2,2)) + 
               Power(t1,5)*(-283 + 82*t2 + 110*Power(t2,2) + 
                  90*Power(t2,3)) + 
               Power(t1,3)*(659 + 6614*t2 - 15860*Power(t2,2) + 
                  5660*Power(t2,3) - 335*Power(t2,4)) + 
               Power(t1,4)*(-575 + 5346*t2 - 5605*Power(t2,2) + 
                  895*Power(t2,3) - 40*Power(t2,4)) + 
               Power(t1,2)*(495 + 5767*t2 - 14533*Power(t2,2) + 
                  6627*Power(t2,3) - 169*Power(t2,4) - 110*Power(t2,5)) \
- t1*(472 - 4012*t2 + 4561*Power(t2,2) + 2159*Power(t2,3) - 
                  3562*Power(t2,4) + 687*Power(t2,5)) - 
               Power(s2,5)*(40 + 1430*Power(t1,3) + 
                  Power(t1,2)*(3900 - 3080*t2) - 2118*t2 + 
                  1883*Power(t2,2) - 126*Power(t2,3) + 
                  t1*(2249 - 7877*t2 + 1386*Power(t2,2))) + 
               Power(s2,4)*(-1067 + 2250*Power(t1,4) + 
                  Power(t1,3)*(8815 - 5090*t2) + 2451*t2 - 
                  2453*Power(t2,2) - 266*Power(t2,3) + 
                  Power(t1,2)*(6950 - 20751*t2 + 2926*Power(t2,2)) + 
                  t1*(299 - 10386*t2 + 7976*Power(t2,2) - 
                     364*Power(t2,3))) + 
               Power(s2,2)*(-2743 + 1070*Power(t1,6) + 
                  Power(t1,5)*(7890 - 2456*t2) + 12393*t2 - 
                  17389*Power(t2,2) + 11403*Power(t2,3) - 
                  4915*Power(t2,4) - 42*Power(t2,5) + 
                  Power(t1,4)*(9293 - 19712*t2 + 1380*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-353 - 5781*t2 + 3931*Power(t2,2) + 
                     129*Power(t2,3)) + 
                  t1*(-3282 + 27723*t2 - 53799*Power(t2,2) + 
                     23659*Power(t2,3) - 3210*Power(t2,4)) + 
                  Power(t1,2)*
                   (-3979 + 19313*t2 - 30045*Power(t2,2) + 
                     6043*Power(t2,3) - 252*Power(t2,4))) + 
               Power(s2,3)*(2509 - 2050*Power(t1,5) - 11351*t2 + 
                  15875*Power(t2,2) - 6419*Power(t2,3) + 
                  1617*Power(t2,4) + 10*Power(t1,4)*(-1109 + 475*t2) + 
                  Power(t1,3)*(-10911 + 27626*t2 - 2952*Power(t2,2)) + 
                  Power(t1,2)*
                   (-157 + 17165*t2 - 12074*Power(t2,2) + 
                     210*Power(t2,3)) + 
                  t1*(3119 - 9957*t2 + 15646*Power(t2,2) - 
                     2367*Power(t2,3) + 112*Power(t2,4))) + 
               s2*(1266 - 290*Power(t1,7) - 4700*t2 + 
                  4799*Power(t2,2) - 2713*Power(t2,3) + 
                  650*Power(t2,4) + 1273*Power(t2,5) + 
                  4*Power(t1,6)*(-741 + 155*t2) + 
                  Power(t1,5)*(-4100 + 7137*t2 - 190*Power(t2,2)) + 
                  Power(t1,4)*
                   (887 + 2583*t2 - 1991*Power(t2,2) - 320*Power(t2,3)) \
+ Power(t1,3)*(2502 - 17153*t2 + 22457*Power(t2,2) - 
                     4305*Power(t2,3) + 180*Power(t2,4)) + 
                  Power(t1,2)*
                   (306 - 23281*t2 + 53041*Power(t2,2) - 
                     21997*Power(t2,3) + 1961*Power(t2,4)) + 
                  t1*(1714 - 18440*t2 + 35975*Power(t2,2) - 
                     19490*Power(t2,3) + 3025*Power(t2,4) + 
                     162*Power(t2,5)))) + 
            s1*(-402 - 58*Power(t1,8) + 
               10*Power(s2,7)*(2 + 7*t1 - 14*t2) + 1870*t2 - 
               3562*Power(t2,2) + 3595*Power(t2,3) - 2410*Power(t2,4) + 
               866*Power(t2,5) + 79*Power(t2,6) + 
               Power(t1,7)*(-112 + 129*t2) + 
               Power(t1,6)*(573 - 80*t2 - 69*Power(t2,2)) + 
               Power(t1,5)*(483 - 2849*t2 + 893*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,4)*(44 - 3162*t2 + 4877*Power(t2,2) - 
                  970*Power(t2,3) - 45*Power(t2,4)) + 
               Power(t1,3)*(818 - 4363*t2 + 5813*Power(t2,2) - 
                  1681*Power(t2,3) + 165*Power(t2,4) + 30*Power(t2,5)) + 
               Power(t1,2)*(899 - 5680*t2 + 4620*Power(t2,2) + 
                  2030*Power(t2,3) - 1301*Power(t2,4) + 80*Power(t2,5)) \
+ t1*(-181 - 341*t2 - 1950*Power(t2,2) + 5513*Power(t2,3) - 
                  3342*Power(t2,4) + 302*Power(t2,5) + 24*Power(t2,6)) - 
               Power(s2,6)*(112 + 507*Power(t1,2) - 248*t2 + 
                  350*Power(t2,2) - 6*t1*(-25 + 189*t2)) + 
               Power(s2,5)*(-40 + 1543*Power(t1,3) + 
                  Power(t1,2)*(435 - 3604*t2) + 796*t2 - 
                  595*Power(t2,2) - 84*Power(t2,3) + 
                  t1*(295 - 998*t2 + 1687*Power(t2,2))) + 
               Power(s2,4)*(951 - 2560*Power(t1,4) - 4277*t2 + 
                  5297*Power(t2,2) - 1079*Power(t2,3) + 
                  322*Power(t2,4) + Power(t1,3)*(-752 + 6005*t2) + 
                  Power(t1,2)*(523 + 1230*t2 - 3151*Power(t2,2)) + 
                  t1*(68 - 5265*t2 + 3950*Power(t2,2) - 231*Power(t2,3))\
) + Power(s2,3)*(-1637 + 2500*Power(t1,5) + 
                  Power(t1,4)*(958 - 5760*t2) + 6328*t2 - 
                  6356*Power(t2,2) + 2711*Power(t2,3) - 
                  1393*Power(t2,4) - 56*Power(t2,5) + 
                  2*Power(t1,3)*(-1396 - 51*t2 + 1450*Power(t2,2)) + 
                  Power(t1,2)*
                   (-351 + 13748*t2 - 8807*Power(t2,2) + 
                     990*Power(t2,3)) + 
                  t1*(-1410 + 13813*t2 - 22220*Power(t2,2) + 
                     5218*Power(t2,3) - 707*Power(t2,4))) + 
               Power(s2,2)*(523 - 1435*Power(t1,6) - 1631*t2 - 
                  1720*Power(t2,2) + 2090*Power(t2,3) + 
                  298*Power(t2,4) + 591*Power(t2,5) + 
                  14*Power(t1,5)*(-63 + 230*t2) - 
                  2*Power(t1,4)*(-1984 + 407*t2 + 707*Power(t2,2)) + 
                  Power(t1,3)*
                   (1141 - 17734*t2 + 9037*Power(t2,2) - 
                     938*Power(t2,3)) + 
                  Power(t1,2)*
                   (98 - 17609*t2 + 32085*Power(t2,2) - 
                     7395*Power(t2,3) + 420*Power(t2,4)) + 
                  t1*(2127 - 14816*t2 + 20189*Power(t2,2) - 
                     6578*Power(t2,3) + 1192*Power(t2,4) + 
                     147*Power(t2,5))) + 
               s2*(703 + 447*Power(t1,7) + Power(t1,6)*(483 - 984*t2) - 
                  3296*t2 + 7224*Power(t2,2) - 6356*Power(t2,3) + 
                  3087*Power(t2,4) - 1459*Power(t2,5) - 36*Power(t2,6) + 
                  Power(t1,5)*(-2455 + 516*t2 + 397*Power(t2,2)) + 
                  Power(t1,4)*
                   (-1301 + 11304*t2 - 4478*Power(t2,2) + 
                     250*Power(t2,3)) + 
                  Power(t1,3)*
                   (317 + 11235*t2 - 20039*Power(t2,2) + 
                     4226*Power(t2,3) + 10*Power(t2,4)) - 
                  Power(t1,2)*
                   (1351 - 12524*t2 + 18701*Power(t2,2) - 
                     5233*Power(t2,3) + 261*Power(t2,4) + 
                     120*Power(t2,5)) - 
                  t1*(747 - 6759*t2 + 3207*Power(t2,2) + 
                     6262*Power(t2,3) - 3216*Power(t2,4) + 
                     450*Power(t2,5))))) + 
         Power(s,4)*(193 - 117*t1 - 1770*Power(t1,2) - 
            2900*Power(t1,3) - 1037*Power(t1,4) + 561*Power(t1,5) - 
            440*Power(t1,6) - 402*Power(t1,7) + 182*Power(t1,8) - 
            14*Power(t1,9) - 931*t2 + 2966*t1*t2 + 
            11262*Power(t1,2)*t2 + 9626*Power(t1,3)*t2 + 
            1551*Power(t1,4)*t2 + 1443*Power(t1,5)*t2 + 
            1645*Power(t1,6)*t2 - 563*Power(t1,7)*t2 + 
            25*Power(t1,8)*t2 + 1939*Power(t2,2) - 7276*t1*Power(t2,2) - 
            18308*Power(t1,2)*Power(t2,2) - 
            10086*Power(t1,3)*Power(t2,2) - 
            3636*Power(t1,4)*Power(t2,2) - 
            3427*Power(t1,5)*Power(t2,2) + 495*Power(t1,6)*Power(t2,2) + 
            19*Power(t1,7)*Power(t2,2) - 2074*Power(t2,3) + 
            5302*t1*Power(t2,3) + 9316*Power(t1,2)*Power(t2,3) + 
            5124*Power(t1,3)*Power(t2,3) + 
            4252*Power(t1,4)*Power(t2,3) + 213*Power(t1,5)*Power(t2,3) - 
            67*Power(t1,6)*Power(t2,3) + 1357*Power(t2,4) + 
            192*t1*Power(t2,4) - 331*Power(t1,2)*Power(t2,4) - 
            2108*Power(t1,3)*Power(t2,4) - 615*Power(t1,4)*Power(t2,4) + 
            47*Power(t1,5)*Power(t2,4) - 864*Power(t2,5) - 
            1275*t1*Power(t2,5) - 107*Power(t1,2)*Power(t2,5) + 
            324*Power(t1,3)*Power(t2,5) - 10*Power(t1,4)*Power(t2,5) + 
            431*Power(t2,6) + 197*t1*Power(t2,6) - 
            26*Power(t1,2)*Power(t2,6) - 50*Power(t2,7) - 
            10*t1*Power(t2,7) + Power(s2,8)*(-6*t1 + 21*t2) + 
            Power(s2,7)*(5 + 45*Power(t1,2) + 98*t2 + 7*Power(t2,2) - 
               t1*(47 + 142*t2)) + 
            Power(s2,6)*(65 - 158*Power(t1,3) - 245*t2 + 
               727*Power(t2,2) + 119*Power(t2,3) + 
               20*Power(t1,2)*(18 + 25*t2) - 
               t1*(57 + 1013*t2 + 221*Power(t2,2))) + 
            Power(s2,5)*(-180 + 339*Power(t1,4) - 17*t2 + 
               726*Power(t2,2) + 112*Power(t2,3) - 77*Power(t2,4) - 
               Power(t1,3)*(1277 + 1130*t2) + 
               Power(t1,2)*(531 + 4228*t2 + 941*Power(t2,2)) - 
               t1*(181 - 77*t2 + 4170*Power(t2,2) + 438*Power(t2,3))) + 
            Power(s2,4)*(-524 - 480*Power(t1,5) + 2533*t2 - 
               4829*Power(t2,2) + 2923*Power(t2,3) + 310*Power(t2,4) + 
               10*Power(t1,4)*(263 + 165*t2) - 
               Power(t1,3)*(2056 + 9305*t2 + 1675*Power(t2,2)) + 
               Power(t1,2)*(128 + 2933*t2 + 10205*Power(t2,2) + 
                  525*Power(t2,3)) + 
               t1*(1232 + 357*t2 - 4108*Power(t2,2) - 
                  1055*Power(t2,3) + 285*Power(t2,4))) + 
            Power(s2,3)*(1713 + 451*Power(t1,6) - 6924*t2 + 
               7384*Power(t2,2) - 2909*Power(t2,3) + 962*Power(t2,4) - 
               340*Power(t2,5) - Power(t1,5)*(3305 + 1506*t2) + 
               Power(t1,4)*(3879 + 11770*t2 + 1465*Power(t2,2)) - 
               2*Power(t1,3)*
                (-246 + 4228*t2 + 6680*Power(t2,2) + 35*Power(t2,3)) + 
               Power(t1,2)*(-3281 - 2080*t2 + 10999*Power(t2,2) + 
                  2490*Power(t2,3) - 495*Power(t2,4)) + 
               t1*(249 - 3318*t2 + 13757*Power(t2,2) - 
                  12199*Power(t2,3) - 170*Power(t2,4) + 50*Power(t2,5))) \
- Power(s2,2)*(1286 + 270*Power(t1,7) - 5824*t2 + 5131*Power(t2,2) + 
               1080*Power(t2,3) - 942*Power(t2,4) - 743*Power(t2,5) + 
               65*Power(t2,6) - 28*Power(t1,6)*(89 + 29*t2) + 
               Power(t1,5)*(3861 + 8623*t2 + 587*Power(t2,2)) + 
               Power(t1,4)*(1409 - 10675*t2 - 9645*Power(t2,2) + 
                  345*Power(t2,3)) + 
               Power(t1,3)*(-4147 - 4600*t2 + 16005*Power(t2,2) + 
                  2050*Power(t2,3) - 490*Power(t2,4)) + 
               t1*(2976 - 14864*t2 + 19448*Power(t2,2) - 
                  11478*Power(t2,3) + 4289*Power(t2,4) - 
                  662*Power(t2,5)) + 
               Power(t1,2)*(337 - 1500*t2 + 17031*Power(t2,2) - 
                  19507*Power(t2,3) + 1025*Power(t2,4) + 100*Power(t2,5)\
)) + s2*(14 + 93*Power(t1,8) - 349*t2 - 839*Power(t2,2) + 
               2891*Power(t2,3) - 1970*Power(t2,4) + 445*Power(t2,5) - 
               184*Power(t2,6) + 10*Power(t2,7) - 
               115*Power(t1,7)*(9 + 2*t2) + 
               Power(t1,6)*(1961 + 3408*t2 + 51*Power(t2,2)) + 
               Power(t1,5)*(1345 - 6629*t2 - 3542*Power(t2,2) + 
                  276*Power(t2,3)) + 
               Power(t1,4)*(-2479 - 4303*t2 + 11815*Power(t2,2) + 
                  290*Power(t2,3) - 250*Power(t2,4)) + 
               Power(t1,2)*(4343 - 18100*t2 + 22566*Power(t2,2) - 
                  13759*Power(t2,3) + 5594*Power(t2,4) - 
                  802*Power(t2,5)) + 
               Power(t1,3)*(1649 - 2266*t2 + 11739*Power(t2,2) - 
                  14483*Power(t2,3) + 1500*Power(t2,4) + 60*Power(t2,5)\
) + t1*(1893 - 13779*t2 + 21264*Power(t2,2) - 8826*Power(t2,3) - 
                  417*Power(t2,4) - 286*Power(t2,5) + 171*Power(t2,6))) \
+ Power(s1,8)*(10*Power(s2,3) - 20*Power(t1,3) + 
               11*Power(-1 + t2,2)*t2 - 
               5*Power(s2,2)*(-3 + 8*t1 + 5*t2) + 
               2*Power(t1,2)*(-13 + 8*t2) + 
               t1*(-6 + 13*t2 - 7*Power(t2,2)) + 
               s2*(-25 + 50*Power(t1,2) + 49*t2 - 24*Power(t2,2) + 
                  t1*(11 + 9*t2))) + 
            Power(s1,7)*(57 - 53*Power(s2,4) - 138*Power(t1,4) + 
               Power(s2,3)*(339 + 273*t1 - 226*t2) - 267*t2 + 
               391*Power(t2,2) - 184*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,3)*(-328 + 262*t2) - 
               Power(s2,2)*(417 + 525*Power(t1,2) + 
                  t1*(988 - 651*t2) - 436*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(-166 + 107*t2 + 12*Power(t2,2)) + 
               t1*(89 - 445*t2 + 505*Power(t2,2) - 138*Power(t2,3)) + 
               s2*(94 + 443*Power(t1,3) + Power(t1,2)*(978 - 687*t2) + 
                  17*t2 - 283*Power(t2,2) + 164*Power(t2,3) + 
                  t1*(606 - 530*t2 - 40*Power(t2,2)))) + 
            Power(s1,6)*(-243 - 250*Power(s2,5) + 345*Power(t1,5) + 
               Power(s2,4)*(1310 + 1413*t1 - 305*t2) + 855*t2 - 
               903*Power(t2,2) + 90*Power(t2,3) + 208*Power(t2,4) + 
               Power(t1,4)*(638 + 21*t2) + 
               Power(t1,3)*(339 + 2763*t2 - 1248*Power(t2,2)) + 
               Power(t1,2)*(-479 + 4874*t2 - 5107*Power(t2,2) + 
                  918*Power(t2,3)) + 
               t1*(-794 + 3245*t2 - 3848*Power(t2,2) + 
                  1438*Power(t2,3) - 36*Power(t2,4)) - 
               Power(s2,3)*(1876 + 3094*Power(t1,2) + 117*t2 - 
                  740*Power(t2,2) - 31*t1*(-164 + 33*t2)) + 
               Power(s2,2)*(594 + 3294*Power(t1,3) + 
                  Power(t1,2)*(6894 - 1130*t2) + 2050*t2 - 
                  3496*Power(t2,2) + 835*Power(t2,3) + 
                  t1*(4779 + 2392*t2 - 2574*Power(t2,2))) - 
               s2*(-400 + 1708*Power(t1,4) + 
                  Power(t1,3)*(3758 - 391*t2) + 2398*t2 - 
                  3863*Power(t2,2) + 1931*Power(t2,3) - 
                  40*Power(t2,4) + 
                  Power(t1,2)*(3227 + 5050*t2 - 3072*Power(t2,2)) + 
                  t1*(279 + 6928*t2 - 8699*Power(t2,2) + 
                     1660*Power(t2,3)))) + 
            Power(s1,5)*(-25 - 255*Power(s2,6) + 140*Power(t1,6) + 
               Power(t1,5)*(1801 - 2044*t2) + 1016*t2 - 
               3598*Power(t2,2) + 4566*Power(t2,3) - 1993*Power(t2,4) + 
               13*Power(t2,5) + 2*Power(s2,5)*(701 + 710*t1 + 295*t2) + 
               Power(t1,4)*(3395 - 11750*t2 + 3732*Power(t2,2)) + 
               Power(t1,3)*(3229 - 18537*t2 + 12621*Power(t2,2) - 
                  1842*Power(t2,3)) + 
               t1*(840 - 433*t2 - 3886*Power(t2,2) + 
                  4972*Power(t2,3) - 1662*Power(t2,4)) + 
               Power(t1,2)*(2462 - 11296*t2 + 9028*Power(t2,2) - 
                  1063*Power(t2,3) + 14*Power(t2,4)) - 
               Power(s2,4)*(2536 + 2952*Power(t1,2) + 4811*t2 - 
                  2685*Power(t2,2) + 28*t1*(191 + 145*t2)) + 
               Power(s2,3)*(970 + 2746*Power(t1,3) + 10383*t2 - 
                  10772*Power(t2,2) + 2020*Power(t2,3) + 
                  2*Power(t1,2)*(2823 + 5399*t2) + 
                  t1*(6641 + 25595*t2 - 11765*Power(t2,2))) + 
               Power(s2,2)*(890 - 901*Power(t1,4) + 
                  Power(t1,3)*(945 - 13820*t2) - 8129*t2 + 
                  10633*Power(t2,2) - 4050*Power(t2,3) + 
                  180*Power(t2,4) + 
                  Power(t1,2)*(-1974 - 48902*t2 + 19321*Power(t2,2)) + 
                  t1*(29 - 40085*t2 + 35687*Power(t2,2) - 
                     5955*Power(t2,3))) - 
               s2*(386 + 198*Power(t1,5) + 
                  Power(t1,4)*(4446 - 8536*t2) - 861*t2 - 
                  309*Power(t2,2) + 2404*Power(t2,3) - 
                  1852*Power(t2,4) + 
                  Power(t1,3)*(5526 - 39868*t2 + 13973*Power(t2,2)) + 
                  Power(t1,2)*
                   (4320 - 48299*t2 + 37521*Power(t2,2) - 
                     5815*Power(t2,3)) + 
                  t1*(3402 - 20324*t2 + 19981*Power(t2,2) - 
                     4557*Power(t2,3) + 230*Power(t2,4)))) + 
            Power(s1,4)*(677 - 543*Power(t1,7) - 3718*t2 + 
               7492*Power(t2,2) - 6137*Power(t2,3) + 765*Power(t2,4) + 
               956*Power(t2,5) + Power(s2,6)*(200 - 430*t1 + 1540*t2) + 
               Power(t1,6)*(-3108 + 2467*t2) + 
               Power(t1,5)*(-4611 + 11235*t2 - 3163*Power(t2,2)) + 
               Power(t1,4)*(-2824 + 12577*t2 - 6585*Power(t2,2) + 
                  1097*Power(t2,3)) + 
               Power(t1,3)*(-2364 + 586*t2 + 13543*Power(t2,2) - 
                  5375*Power(t2,3) + 142*Power(t2,4)) + 
               Power(t1,2)*(-1301 - 11017*t2 + 35876*Power(t2,2) - 
                  25512*Power(t2,3) + 3880*Power(t2,4)) + 
               t1*(1302 - 12632*t2 + 27318*Power(t2,2) - 
                  21338*Power(t2,3) + 5747*Power(t2,4) - 12*Power(t2,5)\
) + Power(s2,5)*(-924 + 2865*Power(t1,2) - 7935*t2 + 
                  3640*Power(t2,2) - 2*t1*(-673 + 5320*t2)) + 
               Power(s2,4)*(645 - 7703*Power(t1,3) + 13534*t2 - 
                  11310*Power(t2,2) + 2380*Power(t2,3) + 
                  Power(t1,2)*(-11083 + 29980*t2) + 
                  t1*(-286 + 45946*t2 - 18785*Power(t2,2))) + 
               Power(s2,3)*(-70 + 10762*Power(t1,4) + 
                  Power(t1,3)*(26281 - 43967*t2) - 4230*t2 + 
                  932*Power(t2,2) - 890*Power(t2,3) + 
                  280*Power(t2,4) + 
                  Power(t1,2)*
                   (11911 - 102111*t2 + 37855*Power(t2,2)) + 
                  t1*(1557 - 61356*t2 + 44630*Power(t2,2) - 
                     8700*Power(t2,3))) + 
               s2*(-2291 + 3317*Power(t1,6) + 
                  Power(t1,5)*(15169 - 14701*t2) + 12523*t2 - 
                  24521*Power(t2,2) + 22885*Power(t2,3) - 
                  9186*Power(t2,4) + 100*Power(t2,5) + 
                  Power(t1,4)*(17789 - 56494*t2 + 17531*Power(t2,2)) + 
                  Power(t1,3)*
                   (8946 - 59343*t2 + 34703*Power(t2,2) - 
                     6117*Power(t2,3)) + 
                  t1*(80 + 22171*t2 - 62590*Power(t2,2) + 
                     45615*Power(t2,3) - 8548*Power(t2,4)) - 
                  2*Power(t1,2)*
                   (-2995 + 6196*t2 + 11665*Power(t2,2) - 
                     5850*Power(t2,3) + 15*Power(t2,4))) - 
               Power(s2,2)*(-1773 + 8268*Power(t1,5) + 
                  Power(t1,4)*(28805 - 35321*t2) + 12049*t2 - 
                  24505*Power(t2,2) + 17823*Power(t2,3) - 
                  5045*Power(t2,4) + 
                  Power(t1,3)*(23879 - 109359*t2 + 37078*Power(t2,2)) + 
                  Power(t1,2)*
                   (8324 - 94588*t2 + 61438*Power(t2,2) - 
                     11340*Power(t2,3)) + 
                  t1*(3594 - 16390*t2 - 8683*Power(t2,2) + 
                     5601*Power(t2,3) + 405*Power(t2,4)))) + 
            Power(s1,3)*(182 + 70*Power(s2,8) + 230*Power(t1,8) + 
               Power(t1,7)*(1329 - 746*t2) - 
               10*Power(s2,7)*(23 + 84*t1 - 98*t2) - 1479*t2 + 
               5649*Power(t2,2) - 10744*Power(t2,3) + 
               10243*Power(t2,4) - 3862*Power(t2,5) - 24*Power(t2,6) + 
               Power(t1,6)*(966 - 3184*t2 + 662*Power(t2,2)) + 
               Power(s2,6)*(29 + 3950*Power(t1,2) + 
                  t1*(2921 - 7371*t2) - 4296*t2 + 1946*Power(t2,2)) - 
               2*Power(t1,5)*
                (542 - 1532*t2 + 552*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,3)*(1835 + 20646*t2 - 47357*Power(t2,2) + 
                  21854*Power(t2,3) - 2426*Power(t2,4)) + 
               Power(t1,4)*(344 + 15475*t2 - 23900*Power(t2,2) + 
                  5589*Power(t2,3) - 140*Power(t2,4)) - 
               Power(t1,2)*(957 - 19654*t2 + 35678*Power(t2,2) - 
                  15493*Power(t2,3) + 137*Power(t2,4) + 204*Power(t2,5)\
) - t1*(1415 - 7357*t2 + 3179*Power(t2,2) + 11630*Power(t2,3) - 
                  11160*Power(t2,4) + 2628*Power(t2,5)) + 
               Power(s2,5)*(520 - 9830*Power(t1,3) + 4633*t2 - 
                  3972*Power(t2,2) + 1176*Power(t2,3) + 
                  2*Power(t1,2)*(-6451 + 11538*t2) + 
                  t1*(-2371 + 27643*t2 - 11277*Power(t2,2))) + 
               Power(s2,4)*(-1312 + 14400*Power(t1,4) + 
                  Power(t1,3)*(28327 - 38940*t2) + 8907*t2 - 
                  14899*Power(t2,2) + 3095*Power(t2,3) + 
                  140*Power(t2,4) + 
                  Power(t1,2)*(10455 - 70548*t2 + 26215*Power(t2,2)) - 
                  t1*(786 + 21841*t2 - 15850*Power(t2,2) + 
                     4795*Power(t2,3))) + 
               Power(s2,2)*(-3936 + 6790*Power(t1,6) + 
                  Power(t1,5)*(24047 - 21495*t2) + 22677*t2 - 
                  36138*Power(t2,2) + 26228*Power(t2,3) - 
                  10150*Power(t2,4) + 135*Power(t2,5) + 
                  Power(t1,4)*(15730 - 64492*t2 + 19605*Power(t2,2)) - 
                  Power(t1,3)*
                   (2092 + 15097*t2 - 10188*Power(t2,2) + 
                     4765*Power(t2,3)) + 
                  t1*(-3098 + 72763*t2 - 149625*Power(t2,2) + 
                     80376*Power(t2,3) - 13005*Power(t2,4)) + 
                  Power(t1,2)*
                   (1826 + 62751*t2 - 127484*Power(t2,2) + 
                     34277*Power(t2,3) - 345*Power(t2,4))) + 
               Power(s2,3)*(3542 - 12820*Power(t1,5) - 26944*t2 + 
                  46013*Power(t2,2) - 25228*Power(t2,3) + 
                  5600*Power(t2,4) + Power(t1,4)*(-34658 + 38120*t2) - 
                  5*Power(t1,3)*(3679 - 18386*t2 + 6221*Power(t2,2)) + 
                  Power(t1,2)*
                   (643 + 32576*t2 - 21477*Power(t2,2) + 
                     7220*Power(t2,3)) + 
                  t1*(917 - 35925*t2 + 73683*Power(t2,2) - 
                     19215*Power(t2,3) - 105*Power(t2,4))) + 
               s2*(1095 - 1950*Power(t1,7) - 3928*t2 + 
                  1189*Power(t2,2) + 4242*Power(t2,3) - 
                  5815*Power(t2,4) + 3657*Power(t2,5) + 
                  Power(t1,6)*(-8834 + 6376*t2) + 
                  Power(t1,5)*(-6414 + 22947*t2 - 6046*Power(t2,2)) + 
                  Power(t1,4)*
                   (2799 - 3335*t2 + 515*Power(t2,2) + 
                     1170*Power(t2,3)) + 
                  Power(t1,3)*
                   (-1775 - 51208*t2 + 92600*Power(t2,2) - 
                     23746*Power(t2,3) + 450*Power(t2,4)) + 
                  Power(t1,2)*
                   (-2072 - 66665*t2 + 150215*Power(t2,2) - 
                     76342*Power(t2,3) + 9953*Power(t2,4)) + 
                  t1*(4362 - 43357*t2 + 75965*Power(t2,2) - 
                     41591*Power(t2,3) + 7489*Power(t2,4) + 
                     40*Power(t2,5)))) + 
            Power(s1,2)*(-1376 - 12*Power(t1,9) - 
               14*Power(s2,8)*(5 + 4*t1 - 9*t2) + 6429*t2 - 
               12449*Power(t2,2) + 12864*Power(t2,3) - 
               7173*Power(t2,4) + 1038*Power(t2,5) + 688*Power(t2,6) + 
               Power(t1,8)*(-227 + 6*t2) + 
               Power(t1,7)*(-109 + 402*t2 + 54*Power(t2,2)) + 
               2*Power(s2,7)*
                (85 + 217*Power(t1,2) + t1*(415 - 518*t2) - 518*t2 + 
                  126*Power(t2,2)) - 
               3*Power(t1,6)*
                (-339 + 593*t2 - 129*Power(t2,2) + 26*Power(t2,3)) + 
               Power(t1,5)*(1224 - 8356*t2 + 7035*Power(t2,2) - 
                  862*Power(t2,3) + 30*Power(t2,4)) + 
               Power(t1,4)*(2591 - 13538*t2 + 16644*Power(t2,2) - 
                  4642*Power(t2,3) + 90*Power(t2,4)) + 
               Power(t1,3)*(6573 - 21646*t2 + 13910*Power(t2,2) + 
                  4672*Power(t2,3) - 2017*Power(t2,4) + 210*Power(t2,5)\
) + Power(t1,2)*(5237 - 16280*t2 - 1965*Power(t2,2) + 
                  26466*Power(t2,3) - 14220*Power(t2,4) + 
                  1420*Power(t2,5)) + 
               t1*(-554 + 4808*t2 - 20866*Power(t2,2) + 
                  30258*Power(t2,3) - 16000*Power(t2,4) + 
                  2341*Power(t2,5) + 92*Power(t2,6)) + 
               Power(s2,6)*(321 - 1440*Power(t1,3) + 351*t2 - 
                  875*Power(t2,2) + 126*Power(t2,3) + 
                  Power(t1,2)*(-3831 + 3556*t2) + 
                  t1*(-1393 + 7236*t2 - 1512*Power(t2,2))) + 
               Power(s2,5)*(-1442 + 2662*Power(t1,4) + 
                  Power(t1,3)*(9382 - 6656*t2) + 8488*t2 - 
                  9037*Power(t2,2) + 1302*Power(t2,3) + 
                  Power(t1,2)*(4250 - 20637*t2 + 3626*Power(t2,2)) + 
                  t1*(-2235 + 408*t2 + 2923*Power(t2,2) - 
                     392*Power(t2,3))) + 
               Power(s2,3)*(-1101 + 2046*Power(t1,6) + 
                  Power(t1,5)*(12050 - 4876*t2) + 10048*t2 - 
                  8973*Power(t2,2) + 3708*Power(t2,3) - 
                  3685*Power(t2,4) - 70*Power(t2,5) + 
                  Power(t1,4)*(5726 - 27330*t2 + 2640*Power(t2,2)) + 
                  Power(t1,3)*
                   (-12849 + 20457*t2 - 2800*Power(t2,2) + 
                     540*Power(t2,3)) + 
                  t1*(-7201 + 69909*t2 - 104289*Power(t2,2) + 
                     40561*Power(t2,3) - 5995*Power(t2,4)) + 
                  Power(t1,2)*
                   (-11118 + 87152*t2 - 99249*Power(t2,2) + 
                     18365*Power(t2,3) - 420*Power(t2,4))) + 
               Power(s2,4)*(2515 - 2980*Power(t1,5) - 19037*t2 + 
                  26348*Power(t2,2) - 11050*Power(t2,3) + 
                  2275*Power(t2,4) + 5*Power(t1,4)*(-2719 + 1476*t2) + 
                  Power(t1,3)*(-6579 + 31310*t2 - 4370*Power(t2,2)) + 
                  Power(t1,2)*
                   (7344 - 8212*t2 - 2125*Power(t2,2) + 
                     210*Power(t2,3)) + 
                  t1*(5981 - 43006*t2 + 49169*Power(t2,2) - 
                     8665*Power(t2,3) + 140*Power(t2,4))) + 
               Power(s2,2)*(-3301 - 824*Power(t1,7) + 14188*t2 - 
                  33705*Power(t2,2) + 31630*Power(t2,3) - 
                  12534*Power(t2,4) + 3695*Power(t2,5) + 
                  Power(t1,6)*(-6405 + 1796*t2) + 
                  Power(t1,5)*(-2879 + 13716*t2 - 572*Power(t2,2)) + 
                  Power(t1,4)*
                   (12018 - 21444*t2 + 5045*Power(t2,2) - 
                     850*Power(t2,3)) + 
                  Power(t1,3)*
                   (11285 - 87594*t2 + 94174*Power(t2,2) - 
                     16805*Power(t2,3) + 450*Power(t2,4)) + 
                  Power(t1,2)*
                   (9779 - 95616*t2 + 143369*Power(t2,2) - 
                     51055*Power(t2,3) + 5395*Power(t2,4)) + 
                  t1*(6470 - 41956*t2 + 36544*Power(t2,2) + 
                     1786*Power(t2,3) - 466*Power(t2,4) + 
                     405*Power(t2,5))) + 
               s2*(4299 + 170*Power(t1,8) + 
                  Power(t1,7)*(1866 - 296*t2) - 19712*t2 + 
                  38325*Power(t2,2) - 37111*Power(t2,3) + 
                  20771*Power(t2,4) - 6634*Power(t2,5) - 
                  80*Power(t2,6) + 
                  Power(t1,6)*(814 - 3661*t2 - 118*Power(t2,2)) + 
                  Power(t1,5)*
                   (-5616 + 10219*t2 - 2555*Power(t2,2) + 
                     444*Power(t2,3)) + 
                  Power(t1,3)*
                   (-7684 + 58282*t2 - 82072*Power(t2,2) + 
                     26186*Power(t2,3) - 1765*Power(t2,4)) + 
                  Power(t1,4)*
                   (-5930 + 43316*t2 - 42092*Power(t2,2) + 
                     6665*Power(t2,3) - 200*Power(t2,4)) + 
                  t1*(-1687 + 3240*t2 + 35706*Power(t2,2) - 
                     64279*Power(t2,3) + 30785*Power(t2,4) - 
                     4333*Power(t2,5)) - 
                  Power(t1,2)*
                   (11944 - 52925*t2 + 40429*Power(t2,2) + 
                     9864*Power(t2,3) - 5429*Power(t2,4) + 
                     550*Power(t2,5)))) + 
            s1*(553 + 25*Power(t1,9) + 
               Power(s2,8)*(15 + 42*t1 - 112*t2) - 2340*t2 + 
               3123*Power(t2,2) - 1062*Power(t2,3) - 1099*Power(t2,4) + 
               1571*Power(t2,5) - 741*Power(t2,6) - 12*Power(t2,7) - 
               2*Power(t1,8)*(14 + 31*t2) + 
               Power(t1,7)*(-497 + 396*t2 + 61*Power(t2,2)) - 
               Power(s2,7)*(33 + 326*Power(t1,2) + t1*(51 - 882*t2) - 
                  18*t2 + 196*Power(t2,2)) - 
               Power(t1,6)*(173 - 2188*t2 + 1091*Power(t2,2) + 
                  61*Power(t2,3)) + 
               Power(t1,5)*(-788 + 1545*t2 - 2682*Power(t2,2) + 
                  904*Power(t2,3) + 62*Power(t2,4)) + 
               Power(t1,3)*(-3778 + 3576*t2 + 11855*Power(t2,2) - 
                  15447*Power(t2,3) + 3879*Power(t2,4) - 90*Power(t2,5)) \
- Power(t1,4)*(3404 - 3468*t2 - 2076*Power(t2,2) + 2101*Power(t2,3) + 
                  35*Power(t2,4) + 25*Power(t2,5)) + 
               t1*(1202 - 8777*t2 + 19810*Power(t2,2) - 
                  16828*Power(t2,3) + 3850*Power(t2,4) + 
                  878*Power(t2,5) - 90*Power(t2,6)) - 
               Power(t1,2)*(784 + 5874*t2 - 24940*Power(t2,2) + 
                  26156*Power(t2,3) - 8530*Power(t2,4) + 
                  685*Power(t2,5) + 56*Power(t2,6)) + 
               Power(s2,6)*(-268 + 1099*Power(t1,3) + 1707*t2 - 
                  1484*Power(t2,2) + 84*Power(t2,3) - 
                  13*Power(t1,2)*(10 + 223*t2) + 
                  t1*(-103 + 661*t2 + 931*Power(t2,2))) + 
               Power(s2,5)*(1029 - 2100*Power(t1,4) - 4721*t2 + 
                  5661*Power(t2,2) - 1434*Power(t2,3) + 
                  308*Power(t2,4) + 9*Power(t1,3)*(102 + 583*t2) + 
                  Power(t1,2)*(1348 - 3801*t2 - 1756*Power(t2,2)) + 
                  t1*(1299 - 10410*t2 + 8721*Power(t2,2) - 
                     749*Power(t2,3))) + 
               Power(s2,4)*(-332 + 2485*Power(t1,5) + 390*t2 + 
                  2698*Power(t2,2) - 1795*Power(t2,3) - 
                  665*Power(t2,4) - 70*Power(t2,5) - 
                  5*Power(t1,4)*(381 + 1156*t2) + 
                  Power(t1,3)*(-4199 + 8630*t2 + 1695*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2532 + 26072*t2 - 20290*Power(t2,2) + 
                     1845*Power(t2,3)) + 
                  t1*(-4073 + 19579*t2 - 25259*Power(t2,2) + 
                     5910*Power(t2,3) - 805*Power(t2,4))) + 
               Power(s2,2)*(5894 + 861*Power(t1,7) - 25787*t2 + 
                  39642*Power(t2,2) - 25242*Power(t2,3) + 
                  8599*Power(t2,4) - 2708*Power(t2,5) - 
                  90*Power(t2,6) - Power(t1,6)*(1104 + 1787*t2) + 
                  Power(t1,5)*(-5641 + 7017*t2 + 481*Power(t2,2)) + 
                  Power(t1,4)*
                   (-1743 + 27105*t2 - 16635*Power(t2,2) + 
                     620*Power(t2,3)) + 
                  Power(t1,3)*
                   (-7017 + 25622*t2 - 37077*Power(t2,2) + 
                     8500*Power(t2,3) + 125*Power(t2,4)) + 
                  t1*(460 - 17549*t2 + 58757*Power(t2,2) - 
                     53408*Power(t2,3) + 13221*Power(t2,4) - 
                     1335*Power(t2,5)) + 
                  Power(t1,2)*
                   (-11980 + 20779*t2 + 5155*Power(t2,2) - 
                     15425*Power(t2,3) + 1470*Power(t2,4) - 
                     300*Power(t2,5))) + 
               Power(s2,3)*(-3529 - 1862*Power(t1,6) + 16368*t2 - 
                  28712*Power(t2,2) + 18449*Power(t2,3) - 
                  3905*Power(t2,4) + 1040*Power(t2,5) + 
                  Power(t1,5)*(1985 + 4032*t2) - 
                  7*Power(t1,4)*(-933 + 1480*t2 + 140*Power(t2,2)) + 
                  Power(t1,3)*
                   (2655 - 35014*t2 + 24475*Power(t2,2) - 
                     1820*Power(t2,3)) + 
                  Power(t1,2)*
                   (7166 - 31965*t2 + 43764*Power(t2,2) - 
                     9795*Power(t2,3) + 560*Power(t2,4)) + 
                  t1*(4694 - 9125*t2 - 6026*Power(t2,2) + 
                     10229*Power(t2,3) - 180*Power(t2,4) + 
                     245*Power(t2,5))) + 
               s2*(-3329 - 224*Power(t1,8) + 14417*t2 - 
                  20640*Power(t2,2) + 11248*Power(t2,3) - 
                  2793*Power(t2,4) + 550*Power(t2,5) + 523*Power(t2,6) + 
                  Power(t1,7)*(300 + 479*t2) + 
                  Power(t1,6)*(2594 - 2561*t2 - 236*Power(t2,2)) + 
                  Power(t1,5)*
                   (762 - 11648*t2 + 6304*Power(t2,2) + 81*Power(t2,3)) \
+ Power(t1,4)*(3683 - 10060*t2 + 15593*Power(t2,2) - 4085*Power(t2,3) - 
                     250*Power(t2,4)) + 
                  Power(t1,3)*
                   (11022 - 15512*t2 - 3903*Power(t2,2) + 
                     9092*Power(t2,3) - 590*Power(t2,4) + 
                     150*Power(t2,5)) + 
                  Power(t1,2)*
                   (6800 - 2364*t2 - 40967*Power(t2,2) + 
                     48538*Power(t2,3) - 12364*Power(t2,4) + 
                     512*Power(t2,5)) + 
                  t1*(-3440 + 25205*t2 - 57870*Power(t2,2) + 
                     49644*Power(t2,3) - 15307*Power(t2,4) + 
                     1404*Power(t2,5) + 120*Power(t2,6))))) + 
         Power(s,3)*(567 + 1487*t1 + 2144*Power(t1,2) + 
            2384*Power(t1,3) + 717*Power(t1,4) - 165*Power(t1,5) + 
            856*Power(t1,6) + 206*Power(t1,7) - 314*Power(t1,8) + 
            56*Power(t1,9) - 2*Power(t1,10) + Power(s2,9)*(t1 - 7*t2) - 
            3199*t2 - 10577*t1*t2 - 15769*Power(t1,2)*t2 - 
            11255*Power(t1,3)*t2 - 2806*Power(t1,4)*t2 - 
            2465*Power(t1,5)*t2 - 1315*Power(t1,6)*t2 + 
            1089*Power(t1,7)*t2 - 119*Power(t1,8)*t2 + 
            7209*Power(t2,2) + 27476*t1*Power(t2,2) + 
            37712*Power(t1,2)*Power(t2,2) + 
            21057*Power(t1,3)*Power(t2,2) + 
            7694*Power(t1,4)*Power(t2,2) + 
            3724*Power(t1,5)*Power(t2,2) - 
            1576*Power(t1,6)*Power(t2,2) - 81*Power(t1,7)*Power(t2,2) + 
            17*Power(t1,8)*Power(t2,2) - 8299*Power(t2,3) - 
            33659*t1*Power(t2,3) - 40409*Power(t1,2)*Power(t2,3) - 
            21089*Power(t1,3)*Power(t2,3) - 
            7907*Power(t1,4)*Power(t2,3) + 637*Power(t1,5)*Power(t2,3) + 
            445*Power(t1,6)*Power(t2,3) - 31*Power(t1,7)*Power(t2,3) + 
            5308*Power(t2,4) + 20305*t1*Power(t2,4) + 
            21049*Power(t1,2)*Power(t2,4) + 
            11018*Power(t1,3)*Power(t2,4) + 
            1648*Power(t1,4)*Power(t2,4) - 423*Power(t1,5)*Power(t2,4) + 
            21*Power(t1,6)*Power(t2,4) - 1922*Power(t2,5) - 
            5505*t1*Power(t2,5) - 5302*Power(t1,2)*Power(t2,5) - 
            2024*Power(t1,3)*Power(t2,5) + 86*Power(t1,4)*Power(t2,5) - 
            5*Power(t1,5)*Power(t2,5) + 274*Power(t2,6) + 
            540*t1*Power(t2,6) + 622*Power(t1,2)*Power(t2,6) + 
            44*Power(t1,3)*Power(t2,6) + 72*Power(t2,7) - 
            72*t1*Power(t2,7) - 8*Power(t1,2)*Power(t2,7) - 
            10*Power(t2,8) + Power(s2,8)*
             (2 - 8*Power(t1,2) - 65*t2 + 19*Power(t2,2) + 
               t1*(19 + 44*t2)) + 
            Power(s2,7)*(-22 + 30*Power(t1,3) + 27*t2 - 
               293*Power(t2,2) - 73*Power(t2,3) - 
               Power(t1,2)*(147 + 142*t2) + 
               t1*(27 + 545*t2 - 30*Power(t2,2))) + 
            Power(s1,9)*(-10*Power(s2,3) + 15*Power(t1,3) + 
               Power(t1,2)*(16 - 11*t2) - 2*Power(-1 + t2,2)*t2 + 
               Power(s2,2)*(-1 + 35*t1 + 6*t2) + 
               t1*(1 + t2 - 2*Power(t2,2)) + 
               s2*(11 - 40*Power(t1,2) + 5*t1*(-3 + t2) - 25*t2 + 
                  14*Power(t2,2))) + 
            Power(s2,6)*(-65 - 70*Power(t1,4) + 677*t2 - 
               1463*Power(t2,2) + 263*Power(t2,3) + 21*Power(t2,4) + 
               Power(t1,3)*(539 + 320*t2) - 
               2*Power(t1,2)*(183 + 1094*t2 + 82*Power(t2,2)) + 
               t1*(71 + 736*t2 + 1892*Power(t2,2) + 353*Power(t2,3))) + 
            Power(s2,5)*(563 + 112*Power(t1,5) - 2166*t2 + 
               3258*Power(t2,2) - 2126*Power(t2,3) - 127*Power(t2,4) - 
               Power(t1,4)*(1211 + 520*t2) + 
               Power(t1,3)*(1669 + 5159*t2 + 588*Power(t2,2)) - 
               Power(t1,2)*(387 + 4738*t2 + 5254*Power(t2,2) + 
                  629*Power(t2,3)) + 
               t1*(221 - 2815*t2 + 7859*Power(t2,2) - 985*Power(t2,3) - 
                  121*Power(t2,4))) + 
            Power(s2,4)*(-105 - 126*Power(t1,6) - 744*t2 + 
               3778*Power(t2,2) - 4167*Power(t2,3) + 662*Power(t2,4) + 
               210*Power(t2,5) + Power(t1,5)*(1785 + 572*t2) - 
               20*Power(t1,4)*(197 + 375*t2 + 38*Power(t2,2)) + 
               Power(t1,3)*(1484 + 12141*t2 + 7875*Power(t2,2) + 
                  415*Power(t2,3)) + 
               Power(t1,2)*(24 + 4527*t2 - 19218*Power(t2,2) + 
                  1795*Power(t2,3) + 335*Power(t2,4)) - 
               t1*(2602 - 7835*t2 + 11948*Power(t2,2) - 
                  10443*Power(t2,3) + 15*Power(t2,4) + 55*Power(t2,5))) \
+ Power(s2,3)*(-2796 + 98*Power(t1,7) + 13961*t2 - 24295*Power(t2,2) + 
               19159*Power(t2,3) - 6410*Power(t2,4) + 118*Power(t2,5) + 
               40*Power(t2,6) - Power(t1,6)*(1729 + 394*t2) + 
               Power(t1,5)*(5333 + 6735*t2 + 426*Power(t2,2)) + 
               Power(t1,4)*(-2716 - 16849*t2 - 6545*Power(t2,2) + 
                  125*Power(t2,3)) - 
               2*Power(t1,3)*
                (802 + 1073*t2 - 13353*Power(t2,2) + 
                  1275*Power(t2,3) + 255*Power(t2,4)) + 
               3*Power(t1,2)*
                (1749 - 3418*t2 + 5088*Power(t2,2) - 
                  6858*Power(t2,3) + 490*Power(t2,4) + 50*Power(t2,5)) \
- t1*(-2479 + 6181*t2 + 3186*Power(t2,2) - 10788*Power(t2,3) + 
                  1536*Power(t2,4) + 638*Power(t2,5))) + 
            Power(s2,2)*(4665 - 50*Power(t1,8) - 23429*t2 + 
               40989*Power(t2,2) - 30091*Power(t2,3) + 
               8963*Power(t2,4) - 1538*Power(t2,5) + 482*Power(t2,6) - 
               20*Power(t2,7) + Power(t1,7)*(1057 + 152*t2) - 
               2*Power(t1,6)*(2091 + 1800*t2 + 20*Power(t2,2)) + 
               Power(t1,5)*(2511 + 13474*t2 + 2698*Power(t2,2) - 
                  337*Power(t2,3)) + 
               Power(t1,4)*(3433 - 2729*t2 - 21455*Power(t2,2) + 
                  2785*Power(t2,3) + 415*Power(t2,4)) - 
               Power(t1,3)*(5105 - 3202*t2 + 3992*Power(t2,2) - 
                  18960*Power(t2,3) + 2810*Power(t2,4) + 
                  140*Power(t2,5)) + 
               2*Power(t1,2)*
                (-1921 + 5800*t2 + 1173*Power(t2,2) - 
                  7839*Power(t2,3) + 839*Power(t2,4) + 486*Power(t2,5)) \
+ t1*(3001 - 18758*t2 + 39674*Power(t2,2) - 41153*Power(t2,3) + 
                  19878*Power(t2,4) - 1832*Power(t2,5) - 214*Power(t2,6)\
)) + s2*(-2809 + 15*Power(t1,9) + 14945*t2 - 29266*Power(t2,2) + 
               25382*Power(t2,3) - 9315*Power(t2,4) + 
               1686*Power(t2,5) - 732*Power(t2,6) + 114*Power(t2,7) - 
               Power(t1,8)*(369 + 25*t2) + 
               Power(t1,7)*(1771 + 1033*t2 - 56*Power(t2,2)) + 
               Power(t1,6)*(-1147 - 5880*t2 - 292*Power(t2,2) + 
                  177*Power(t2,3)) + 
               Power(t1,5)*(-2865 + 3801*t2 + 9147*Power(t2,2) - 
                  1753*Power(t2,3) - 161*Power(t2,4)) + 
               Power(t1,4)*(2062 + 3848*t2 - 6306*Power(t2,2) - 
                  7340*Power(t2,3) + 1905*Power(t2,4) + 50*Power(t2,5)) \
- Power(t1,3)*(-751 + 1869*t2 + 10632*Power(t2,2) - 
                  16964*Power(t2,3) + 2452*Power(t2,4) + 
                  630*Power(t2,5)) + 
               Power(t1,2)*(-2769 + 16856*t2 - 37598*Power(t2,2) + 
                  43509*Power(t2,3) - 24226*Power(t2,4) + 
                  3654*Power(t2,5) + 66*Power(t2,6)) + 
               t1*(-4704 + 29339*t2 - 61805*Power(t2,2) + 
                  58101*Power(t2,3) - 26671*Power(t2,4) + 
                  6418*Power(t2,5) - 778*Power(t2,6) + 40*Power(t2,7))) \
+ Power(s1,8)*(-19 + 26*Power(s2,4) + 83*Power(t1,4) + 72*t2 - 
               91*Power(t2,2) + 38*Power(t2,3) - 
               7*Power(t1,3)*(-36 + 31*t2) + 
               Power(s2,3)*(-199 - 147*t1 + 164*t2) + 
               Power(t1,2)*(217 - 327*t2 + 95*Power(t2,2)) + 
               t1*(29 - 31*t2 - 38*Power(t2,2) + 39*Power(t2,3)) + 
               Power(s2,2)*(262 + 299*Power(t1,2) - 367*t2 + 
                  82*Power(t2,2) - 7*t1*(-89 + 73*t2)) - 
               s2*(85 + 261*Power(t1,3) + Power(t1,2)*(676 - 564*t2) - 
                  140*t2 - 2*Power(t2,2) + 56*Power(t2,3) + 
                  t1*(469 - 664*t2 + 157*Power(t2,2)))) + 
            Power(s1,7)*(124 + 262*Power(s2,5) - 428*Power(t1,5) - 
               373*t2 + 334*Power(t2,2) - 22*Power(t2,3) - 
               63*Power(t2,4) + Power(t1,4)*(-1360 + 549*t2) + 
               Power(s2,4)*(-1319 - 1502*t1 + 554*t2) + 
               Power(s2,3)*(1753 + 3370*Power(t1,2) + 
                  t1*(5516 - 2285*t2) - 1094*t2 - 106*Power(t2,2)) + 
               Power(t1,3)*(-1392 + 189*t2 + 331*Power(t2,2)) - 
               Power(t1,2)*(200 + 1404*t2 - 2094*Power(t2,2) + 
                  467*Power(t2,3)) + 
               t1*(352 - 1365*t2 + 1803*Power(t2,2) - 
                  810*Power(t2,3) + 15*Power(t2,4)) - 
               Power(s2,2)*(570 + 3710*Power(t1,3) + 
                  Power(t1,2)*(8450 - 3473*t2) + 262*t2 - 
                  1300*Power(t2,2) + 406*Power(t2,3) + 
                  t1*(5168 - 2732*t2 - 413*Power(t2,2))) + 
               s2*(-210 + 2008*Power(t1,4) + 
                  Power(t1,3)*(5613 - 2291*t2) + 1114*t2 - 
                  1819*Power(t2,2) + 928*Power(t2,3) - 8*Power(t2,4) - 
                  2*Power(t1,2)*(-2405 + 919*t2 + 315*Power(t2,2)) + 
                  t1*(823 + 1559*t2 - 3258*Power(t2,2) + 
                     791*Power(t2,3)))) + 
            Power(s1,6)*(-36 + 510*Power(s2,6) + 314*Power(t1,6) - 
               482*t2 + 1889*Power(t2,2) - 2260*Power(t2,3) + 
               895*Power(t2,4) - 6*Power(t2,5) + 
               Power(s2,5)*(-2542 - 3132*t1 + 370*t2) + 
               Power(t1,5)*(430 + 814*t2) + 
               Power(t1,4)*(-579 + 7219*t2 - 2646*Power(t2,2)) + 
               Power(t1,3)*(-2998 + 14431*t2 - 10486*Power(t2,2) + 
                  1534*Power(t2,3)) + 
               Power(t1,2)*(-3619 + 11633*t2 - 9765*Power(t2,2) + 
                  1919*Power(t2,3) - 16*Power(t2,4)) + 
               t1*(-1188 + 2287*t2 - 355*Power(t2,2) - 
                  1712*Power(t2,3) + 1014*Power(t2,4)) + 
               Power(s2,4)*(3551 + 7740*Power(t1,2) + 1444*t2 - 
                  1430*Power(t2,2) - 4*t1*(-2958 + 293*t2)) - 
               Power(s2,3)*(330 + 9842*Power(t1,3) + 
                  Power(t1,2)*(20596 - 436*t2) + 7156*t2 - 
                  7513*Power(t2,2) + 1370*Power(t2,3) + 
                  t1*(11902 + 10243*t2 - 6576*Power(t2,2))) + 
               Power(s2,2)*(-2036 + 6756*Power(t1,4) + 8042*t2 - 
                  9028*Power(t2,2) + 3170*Power(t2,3) - 
                  80*Power(t2,4) + 2*Power(t1,3)*(8147 + 989*t2) + 
                  Power(t1,2)*(12422 + 23601*t2 - 11594*Power(t2,2)) + 
                  t1*(-1383 + 28108*t2 - 25620*Power(t2,2) + 
                     4200*Power(t2,3))) - 
               s2*(-873 + 2346*Power(t1,5) + 2239*t2 - 
                  1491*Power(t2,2) - 892*Power(t2,3) + 
                  1063*Power(t2,4) + Power(t1,4)*(5418 + 2426*t2) + 
                  Power(t1,3)*(3492 + 22021*t2 - 9094*Power(t2,2)) + 
                  Power(t1,2)*
                   (-4753 + 35473*t2 - 28683*Power(t2,2) + 
                     4406*Power(t2,3)) + 
                  t1*(-5564 + 19515*t2 - 18488*Power(t2,2) + 
                     4901*Power(t2,3) - 144*Power(t2,4)))) + 
            Power(s1,5)*(-494 + 340*Power(s2,7) + 361*Power(t1,7) + 
               Power(t1,6)*(2381 - 2675*t2) + 2877*t2 - 
               5635*Power(t2,2) + 4109*Power(t2,3) - 214*Power(t2,4) - 
               643*Power(t2,5) - 
               Power(s2,6)*(1693 + 1975*t1 + 880*t2) + 
               Power(t1,5)*(5064 - 13243*t2 + 4114*Power(t2,2)) + 
               Power(t1,4)*(7352 - 20909*t2 + 10385*Power(t2,2) - 
                  1662*Power(t2,3)) + 
               Power(t1,2)*(3009 + 7496*t2 - 30204*Power(t2,2) + 
                  23224*Power(t2,3) - 4124*Power(t2,4)) + 
               Power(t1,3)*(7852 - 10967*t2 - 5349*Power(t2,2) + 
                  4463*Power(t2,3) - 138*Power(t2,4)) + 
               t1*(-1221 + 11181*t2 - 23701*Power(t2,2) + 
                  19545*Power(t2,3) - 5922*Power(t2,4) + 3*Power(t2,5)) \
+ Power(s2,5)*(1989 + 4288*Power(t1,2) + 7369*t2 - 3440*Power(t2,2) + 
                  t1*(7161 + 7055*t2)) - 
               Power(s2,4)*(3841*Power(t1,3) + 
                  Power(t1,2)*(8803 + 22447*t2) + 
                  t1*(3853 + 44575*t2 - 18540*Power(t2,2)) + 
                  2*(-1192 + 8761*t2 - 6439*Power(t2,2) + 
                     1230*Power(t2,3))) + 
               Power(s2,3)*(-4958 + 84*Power(t1,4) + 12317*t2 - 
                  6399*Power(t2,2) + 1510*Power(t2,3) - 
                  240*Power(t2,4) + Power(t1,3)*(-2008 + 36486*t2) + 
                  Power(t1,2)*
                   (-6263 + 103604*t2 - 39394*Power(t2,2)) + 
                  t1*(-15148 + 79770*t2 - 52829*Power(t2,2) + 
                     9550*Power(t2,3))) - 
               s2*(-1511 + 1656*Power(t1,6) + 
                  Power(t1,5)*(9665 - 14587*t2) + 10021*t2 - 
                  21089*Power(t2,2) + 19731*Power(t2,3) - 
                  7319*Power(t2,4) + 52*Power(t2,5) + 
                  Power(t1,4)*(18130 - 63047*t2 + 20862*Power(t2,2)) + 
                  Power(t1,3)*
                   (25527 - 86972*t2 + 47903*Power(t2,2) - 
                     8002*Power(t2,3)) + 
                  t1*(4257 + 13020*t2 - 48675*Power(t2,2) + 
                     38498*Power(t2,3) - 8165*Power(t2,4)) + 
                  Power(t1,2)*
                   (22100 - 40247*t2 - 274*Power(t2,2) + 
                     7582*Power(t2,3) + 56*Power(t2,4))) + 
               Power(s2,2)*(861 + 2399*Power(t1,5) + 
                  Power(t1,4)*(12627 - 32126*t2) + 6414*t2 - 
                  18812*Power(t2,2) + 15529*Power(t2,3) - 
                  4458*Power(t2,4) + 
                  Power(t1,3)*(21193 - 116202*t2 + 41042*Power(t2,2)) + 
                  Power(t1,2)*
                   (30939 - 128311*t2 + 77469*Power(t2,2) - 
                     13430*Power(t2,3)) + 
                  t1*(19159 - 41522*t2 + 11380*Power(t2,2) + 
                     1649*Power(t2,3) + 460*Power(t2,4)))) + 
            Power(s1,4)*(-266 - 447*Power(t1,8) + 2129*t2 - 
               7700*Power(t2,2) + 13759*Power(t2,3) - 
               11790*Power(t2,4) + 3834*Power(t2,5) + 34*Power(t2,6) + 
               Power(t1,7)*(-2044 + 1849*t2) + 
               Power(t1,6)*(-1623 + 6295*t2 - 2095*Power(t2,2)) + 
               Power(t1,5)*(-69 - 2845*t2 + 1462*Power(t2,2) + 
                  445*Power(t2,3)) + 
               Power(t1,4)*(-1252 - 24351*t2 + 40334*Power(t2,2) - 
                  10804*Power(t2,3) + 248*Power(t2,4)) + 
               Power(t1,3)*(2112 - 42152*t2 + 77867*Power(t2,2) - 
                  38563*Power(t2,3) + 4856*Power(t2,4)) + 
               Power(t1,2)*(6843 - 39452*t2 + 57791*Power(t2,2) - 
                  26407*Power(t2,3) + 1641*Power(t2,4) + 
                  284*Power(t2,5)) + 
               t1*(3156 - 11036*t2 + 5168*Power(t2,2) + 
                  12248*Power(t2,3) - 13279*Power(t2,4) + 
                  3873*Power(t2,5)) + 
               Power(s2,7)*(515*t1 - 7*(23 + 222*t2)) + 
               Power(s2,6)*(8 - 3755*Power(t1,2) + 7475*t2 - 
                  3556*Power(t2,2) + t1*(-1499 + 12215*t2)) + 
               Power(s2,5)*(2487 + 11497*Power(t1,3) + 
                  Power(t1,2)*(12570 - 40016*t2) - 10537*t2 + 
                  6954*Power(t2,2) - 2282*Power(t2,3) + 
                  t1*(4150 - 48468*t2 + 21493*Power(t2,2))) - 
               Power(s2,4)*(2923 + 19185*Power(t1,4) + 
                  Power(t1,3)*(34114 - 70843*t2) + 7599*t2 - 
                  20772*Power(t2,2) + 5700*Power(t2,3) + 
                  280*Power(t2,4) + 
                  Power(t1,2)*
                   (18895 - 125517*t2 + 52605*Power(t2,2)) + 
                  t1*(14908 - 50531*t2 + 29700*Power(t2,2) - 
                     10165*Power(t2,3))) - 
               Power(s2,2)*(-5859 + 10885*Power(t1,6) + 
                  Power(t1,5)*(34263 - 43877*t2) + 31082*t2 - 
                  49493*Power(t2,2) + 35403*Power(t2,3) - 
                  11823*Power(t2,4) + 200*Power(t2,5) + 
                  Power(t1,4)*
                   (28234 - 119817*t2 + 45496*Power(t2,2)) + 
                  Power(t1,3)*
                   (24635 - 55130*t2 + 26790*Power(t2,2) - 
                     13298*Power(t2,3)) + 
                  t1*(-6766 + 111655*t2 - 213718*Power(t2,2) + 
                     121007*Power(t2,3) - 20392*Power(t2,4)) + 
                  Power(t1,2)*
                   (21339 + 77268*t2 - 186964*Power(t2,2) + 
                     56080*Power(t2,3) - 60*Power(t2,4))) + 
               Power(s2,3)*(-3479 + 18845*Power(t1,5) + 
                  Power(t1,4)*(46371 - 73122*t2) + 35963*t2 - 
                  64521*Power(t2,2) + 38175*Power(t2,3) - 
                  7990*Power(t2,4) + 
                  4*Power(t1,3)*
                   (8332 - 41697*t2 + 16628*Power(t2,2)) + 
                  Power(t1,2)*
                   (29671 - 83710*t2 + 44768*Power(t2,2) - 
                     17110*Power(t2,3)) + 
                  t1*(14734 + 36755*t2 - 103664*Power(t2,2) + 
                     31124*Power(t2,3) + 540*Power(t2,4))) + 
               s2*(-1460 + 3415*Power(t1,7) + 4625*t2 - 
                  1194*Power(t2,2) - 7268*Power(t2,3) + 
                  9611*Power(t2,4) - 4444*Power(t2,5) - 
                  4*Power(t1,6)*(-3285 + 3523*t2) + 
                  Power(t1,5)*(11266 - 43848*t2 + 15747*Power(t2,2)) + 
                  Power(t1,4)*
                   (7454 - 8569*t2 + 3306*Power(t2,2) - 
                     4516*Power(t2,3)) + 
                  Power(t1,2)*
                   (-5577 + 118304*t2 - 227458*Power(t2,2) + 
                     121705*Power(t2,3) - 17456*Power(t2,4)) + 
                  Power(t1,3)*
                   (10780 + 72463*t2 - 144406*Power(t2,2) + 
                     41460*Power(t2,3) - 568*Power(t2,4)) + 
                  t1*(-12489 + 71307*t2 - 109064*Power(t2,2) + 
                     61311*Power(t2,3) - 12303*Power(t2,4) + 
                     48*Power(t2,5)))) + 
            Power(s1,3)*(1464 - 56*Power(s2,9) + 112*Power(t1,9) + 
               Power(s2,8)*(127 + 714*t1 - 784*t2) + 
               Power(t1,8)*(520 - 335*t2) - 7363*t2 + 
               15284*Power(t2,2) - 16088*Power(t2,3) + 
               7297*Power(t2,4) + 814*Power(t2,5) - 1408*Power(t2,6) + 
               Power(t1,7)*(-728 - 623*t2 + 221*Power(t2,2)) + 
               Power(t1,6)*(-2330 + 7838*t2 - 3214*Power(t2,2) + 
                  115*Power(t2,3)) + 
               Power(t1,4)*(-6508 + 26808*t2 - 32316*Power(t2,2) + 
                  11984*Power(t2,3) - 1419*Power(t2,4)) + 
               Power(t1,5)*(-1206 + 19175*t2 - 23663*Power(t2,2) + 
                  5242*Power(t2,3) - 113*Power(t2,4)) + 
               Power(t1,2)*(-9146 + 11956*t2 + 40938*Power(t2,2) - 
                  79163*Power(t2,3) + 40036*Power(t2,4) - 
                  4856*Power(t2,5)) - 
               Power(t1,3)*(15264 - 34829*t2 + 2791*Power(t2,2) + 
                  27034*Power(t2,3) - 8960*Power(t2,4) + 
                  506*Power(t2,5)) + 
               t1*(1246 - 15637*t2 + 50281*Power(t2,2) - 
                  63232*Power(t2,3) + 33759*Power(t2,4) - 
                  6276*Power(t2,5) - 200*Power(t2,6)) + 
               Power(s2,7)*(33 - 3652*Power(t1,2) + 2606*t2 - 
                  1540*Power(t2,2) + t1*(-1884 + 6517*t2)) + 
               Power(s2,6)*(106 + 10166*Power(t1,3) + 
                  Power(t1,2)*(9334 - 23033*t2) + 1146*t2 - 
                  430*Power(t2,2) - 924*Power(t2,3) + 
                  t1*(396 - 17980*t2 + 10101*Power(t2,2))) + 
               Power(s2,5)*(872 - 17232*Power(t1,4) - 19110*t2 + 
                  27687*Power(t2,2) - 7286*Power(t2,3) - 
                  112*Power(t2,4) + 5*Power(t1,3)*(-4675 + 9037*t2) - 
                  2*Power(t1,2)*(647 - 25323*t2 + 13713*Power(t2,2)) + 
                  t1*(705 - 13473*t2 + 7320*Power(t2,2) + 
                     4375*Power(t2,3))) + 
               Power(s2,4)*(-5209 + 18566*Power(t1,5) + 
                  Power(t1,4)*(33915 - 53645*t2) + 35745*t2 - 
                  53166*Power(t2,2) + 28722*Power(t2,3) - 
                  6545*Power(t2,4) + 
                  Power(t1,3)*(-172 - 75527*t2 + 39715*Power(t2,2)) - 
                  Power(t1,2)*
                   (7648 - 53262*t2 + 29272*Power(t2,2) + 
                     8005*Power(t2,3)) + 
                  t1*(-5046 + 101525*t2 - 147953*Power(t2,2) + 
                     39380*Power(t2,3) + 105*Power(t2,4))) + 
               Power(s2,3)*(4073 - 12724*Power(t1,6) - 13818*t2 + 
                  2526*Power(t2,2) + 4598*Power(t2,3) + 
                  2610*Power(t2,4) - 180*Power(t2,5) + 
                  Power(t1,5)*(-29762 + 39199*t2) + 
                  Power(t1,4)*(4553 + 64038*t2 - 32720*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (9992 - 49123*t2 + 25705*Power(t2,2) + 
                     3445*Power(t2,3)) + 
                  2*Power(t1,2)*
                   (6392 - 106060*t2 + 150859*Power(t2,2) - 
                     39614*Power(t2,3) + 230*Power(t2,4)) + 
                  t1*(22940 - 145651*t2 + 214022*Power(t2,2) - 
                     105726*Power(t2,3) + 20280*Power(t2,4))) + 
               Power(s2,2)*(4408 + 5322*Power(t1,7) + 
                  Power(t1,6)*(15544 - 16987*t2) - 28688*t2 + 
                  73500*Power(t2,2) - 77768*Power(t2,3) + 
                  37044*Power(t2,4) - 8458*Power(t2,5) + 
                  Power(t1,5)*(-6320 - 30438*t2 + 14955*Power(t2,2)) - 
                  2*Power(t1,4)*
                   (11352 - 46353*t2 + 22674*Power(t2,2) + 
                     1265*Power(t2,3)) + 
                  Power(t1,2)*
                   (-37588 + 211739*t2 - 299598*Power(t2,2) + 
                     136262*Power(t2,3) - 22582*Power(t2,4)) - 
                  2*Power(t1,3)*
                   (7562 - 107640*t2 + 146994*Power(t2,2) - 
                     37351*Power(t2,3) + 450*Power(t2,4)) - 
                  t1*(24642 - 72948*t2 + 22347*Power(t2,2) + 
                     37214*Power(t2,3) - 9524*Power(t2,4) + 
                     80*Power(t2,5))) + 
               s2*(-5838 - 1216*Power(t1,8) + 30206*t2 - 
                  63465*Power(t2,2) + 68198*Power(t2,3) - 
                  40588*Power(t2,4) + 11450*Power(t2,5) + 
                  96*Power(t2,6) + Power(t1,7)*(-4419 + 3883*t2) + 
                  Power(t1,6)*(3532 + 7278*t2 - 3306*Power(t2,2)) + 
                  Power(t1,5)*
                   (11887 - 43233*t2 + 19534*Power(t2,2) + 
                     79*Power(t2,3)) + 
                  Power(t1,4)*
                   (7720 - 104750*t2 + 136199*Power(t2,2) - 
                     32810*Power(t2,3) + 560*Power(t2,4)) + 
                  Power(t1,3)*
                   (26365 - 128641*t2 + 171058*Power(t2,2) - 
                     71242*Power(t2,3) + 10266*Power(t2,4)) + 
                  Power(t1,2)*
                   (35926 - 93958*t2 + 22644*Power(t2,2) + 
                     59066*Power(t2,3) - 20686*Power(t2,4) + 
                     816*Power(t2,5)) + 
                  t1*(5531 + 13299*t2 - 112488*Power(t2,2) + 
                     160183*Power(t2,3) - 78692*Power(t2,4) + 
                     12364*Power(t2,5)))) + 
            Power(s1,2)*(408 - 2*Power(t1,10) + 
               14*Power(s2,9)*(3 + 2*t1 - 6*t2) - 2840*t2 + 
               9641*Power(t2,2) - 17630*Power(t2,3) + 
               18447*Power(t2,4) - 10922*Power(t2,5) + 
               2824*Power(t2,6) + 72*Power(t2,7) - 
               2*Power(t1,9)*(31 + 3*t2) + 
               Power(t1,8)*(133 + 33*t2 + 30*Power(t2,2)) - 
               Power(s2,8)*(15 + 486*t1 + 238*Power(t1,2) - 504*t2 - 
                  728*t1*t2 + 168*Power(t2,2)) + 
               Power(t1,7)*(776 - 1597*t2 + 378*Power(t2,2) - 
                  34*Power(t2,3)) + 
               Power(t1,5)*(3862 - 3489*t2 + 1255*Power(t2,2) - 
                  256*Power(t2,3) - 162*Power(t2,4)) + 
               Power(t1,6)*(615 - 4345*t2 + 4009*Power(t2,2) - 
                  397*Power(t2,3) + 12*Power(t2,4)) + 
               Power(t1,4)*(9117 - 5289*t2 - 22945*Power(t2,2) + 
                  21984*Power(t2,3) - 3973*Power(t2,4) + 
                  210*Power(t2,5)) + 
               Power(t1,3)*(2810 + 13665*t2 - 59722*Power(t2,2) + 
                  60262*Power(t2,3) - 17994*Power(t2,4) + 
                  1384*Power(t2,5)) + 
               Power(t1,2)*(-6663 + 41553*t2 - 81459*Power(t2,2) + 
                  60597*Power(t2,3) - 12964*Power(t2,4) - 
                  1392*Power(t2,5) + 300*Power(t2,6)) + 
               t1*(-3778 + 21019*t2 - 33139*Power(t2,2) + 
                  9944*Power(t2,3) + 15234*Power(t2,4) - 
                  11090*Power(t2,5) + 1800*Power(t2,6)) + 
               Power(s2,7)*(884*Power(t1,3) + 
                  Power(t1,2)*(2298 - 2702*t2) + 
                  t1*(-46 - 3565*t2 + 1092*Power(t2,2)) - 
                  3*(136 - 464*t2 + 119*Power(t2,2) + 28*Power(t2,3))) \
+ Power(s2,6)*(1042 - 1876*Power(t1,4) - 8918*t2 + 10168*Power(t2,2) - 
                  2156*Power(t2,3) + Power(t1,3)*(-5996 + 5622*t2) + 
                  Power(t1,2)*(1010 + 10523*t2 - 2926*Power(t2,2)) + 
                  t1*(3873 - 12032*t2 + 3994*Power(t2,2) + 
                     280*Power(t2,3))) + 
               Power(s2,5)*(-1653 + 2492*Power(t1,5) + 
                  Power(t1,4)*(9642 - 7174*t2) + 9747*t2 - 
                  13321*Power(t2,2) + 7188*Power(t2,3) - 
                  2023*Power(t2,4) + 
                  Power(t1,3)*(-3968 - 16813*t2 + 4100*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (4779 - 13737*t2 + 4991*Power(t2,2) + 
                     42*Power(t2,3)) + 
                  t1*(-7072 + 52677*t2 - 59218*Power(t2,2) + 
                     12657*Power(t2,3) - 112*Power(t2,4))) + 
               Power(s2,4)*(-1851 - 2128*Power(t1,6) + 16428*t2 - 
                  38439*Power(t2,2) + 25184*Power(t2,3) - 
                  2555*Power(t2,4) + 70*Power(t2,5) + 
                  Power(t1,5)*(-9972 + 5734*t2) + 
                  Power(t1,4)*(7350 + 15715*t2 - 3070*Power(t2,2)) + 
                  Power(t1,3)*
                   (27554 - 73961*t2 + 27470*Power(t2,2) - 
                     650*Power(t2,3)) + 
                  Power(t1,2)*
                   (18547 - 124749*t2 + 137287*Power(t2,2) - 
                     28865*Power(t2,3) + 420*Power(t2,4)) + 
                  t1*(11666 - 47873*t2 + 58253*Power(t2,2) - 
                     26454*Power(t2,3) + 6640*Power(t2,4))) + 
               Power(s2,3)*(10204 + 1148*Power(t1,7) + 
                  Power(t1,6)*(6646 - 2778*t2) - 53144*t2 + 
                  101095*Power(t2,2) - 81508*Power(t2,3) + 
                  29480*Power(t2,4) - 5610*Power(t2,5) + 
                  Power(t1,5)*(-7470 - 8639*t2 + 988*Power(t2,2)) + 
                  Power(t1,4)*
                   (-30026 + 75654*t2 - 27535*Power(t2,2) + 
                     1200*Power(t2,3)) + 
                  Power(t1,2)*
                   (-28824 + 86406*t2 - 92990*Power(t2,2) + 
                     36004*Power(t2,3) - 7850*Power(t2,4)) - 
                  2*Power(t1,3)*
                   (12122 - 75855*t2 + 81121*Power(t2,2) - 
                     16305*Power(t2,3) + 300*Power(t2,4)) - 
                  t1*(6910 + 30917*t2 - 132158*Power(t2,2) + 
                     108596*Power(t2,3) - 17874*Power(t2,4) + 
                     540*Power(t2,5))) + 
               s2*(2899 + 56*Power(t1,9) + Power(t1,8)*(636 - 62*t2) - 
                  10727*t2 + 5343*Power(t2,2) + 15508*Power(t2,3) - 
                  20191*Power(t2,4) + 10290*Power(t2,5) - 
                  3112*Power(t2,6) - 
                  Power(t1,7)*(1220 + 423*t2 + 164*Power(t2,2)) + 
                  Power(t1,6)*
                   (-6029 + 13391*t2 - 4079*Power(t2,2) + 
                     290*Power(t2,3)) + 
                  Power(t1,4)*
                   (-18175 + 26879*t2 - 18957*Power(t2,2) + 
                     5172*Power(t2,3) - 315*Power(t2,4)) + 
                  Power(t1,5)*
                   (-5428 + 33261*t2 - 32668*Power(t2,2) + 
                     5021*Power(t2,3) - 120*Power(t2,4)) + 
                  Power(t1,2)*
                   (269 - 65977*t2 + 211047*Power(t2,2) - 
                     207372*Power(t2,3) + 68054*Power(t2,4) - 
                     6564*Power(t2,5)) + 
                  Power(t1,3)*
                   (-28556 + 10957*t2 + 102132*Power(t2,2) - 
                     100516*Power(t2,3) + 19394*Power(t2,4) - 
                     840*Power(t2,5)) + 
                  t1*(15708 - 84699*t2 + 153292*Power(t2,2) - 
                     112319*Power(t2,3) + 30908*Power(t2,4) - 
                     2242*Power(t2,5) - 368*Power(t2,6))) + 
               Power(s2,2)*(-364*Power(t1,8) + 
                  Power(t1,7)*(-2748 + 722*t2) + 
                  Power(t1,6)*(4226 + 2665*t2 + 118*Power(t2,2)) - 
                  3*Power(t1,5)*
                   (-6199 + 14686*t2 - 5034*Power(t2,2) + 
                     292*Power(t2,3)) + 
                  2*Power(t1,4)*
                   (8270 - 49818*t2 + 51332*Power(t2,2) - 
                     9435*Power(t2,3) + 200*Power(t2,4)) + 
                  Power(t1,3)*
                   (33124 - 71670*t2 + 65760*Power(t2,2) - 
                     21654*Power(t2,3) + 3710*Power(t2,4)) + 
                  Power(t1,2)*
                   (28200 + 8821*t2 - 172906*Power(t2,2) + 
                     161944*Power(t2,3) - 30740*Power(t2,4) + 
                     1100*Power(t2,5)) + 
                  t1*(-13143 + 105158*t2 - 252898*Power(t2,2) + 
                     229700*Power(t2,3) - 79658*Power(t2,4) + 
                     10462*Power(t2,5)) + 
                  2*(-5334 + 23931*t2 - 37405*Power(t2,2) + 
                     26410*Power(t2,3) - 11226*Power(t2,4) + 
                     3418*Power(t2,5) + 80*Power(t2,6)))) + 
            s1*(-1902 + 7*Power(t1,10) - 
               2*Power(s2,9)*(3 + 7*t1 - 28*t2) + 9871*t2 - 
               21409*Power(t2,2) + 24355*Power(t2,3) - 
               15400*Power(t2,4) + 5279*Power(t2,5) - 574*Power(t2,6) - 
               220*Power(t2,7) - 11*Power(t1,9)*(5 + 2*t2) + 
               Power(t1,8)*(-92 + 315*t2 + 35*Power(t2,2)) + 
               Power(t1,7)*(379 + 131*t2 - 609*Power(t2,2) - 
                  43*Power(t2,3)) + 
               Power(t1,6)*(536 - 2135*t2 + 1430*Power(t2,2) + 
                  300*Power(t2,3) + 34*Power(t2,4)) + 
               Power(t1,4)*(6598 - 21554*t2 + 26588*Power(t2,2) - 
                  14455*Power(t2,3) + 3696*Power(t2,4) - 25*Power(t2,5)) \
+ Power(t1,5)*(1409 - 6754*t2 + 9209*Power(t2,2) - 5041*Power(t2,3) + 
                  158*Power(t2,4) - 11*Power(t2,5)) + 
               Power(t1,2)*(8437 - 32561*t2 + 33060*Power(t2,2) + 
                  3590*Power(t2,3) - 19308*Power(t2,4) + 
                  7132*Power(t2,5) - 392*Power(t2,6)) + 
               Power(t1,3)*(12541 - 42851*t2 + 48221*Power(t2,2) - 
                  16869*Power(t2,3) - 1622*Power(t2,4) + 
                  290*Power(t2,5) - 84*Power(t2,6)) - 
               t1*(690 - 3752*t2 + 16749*Power(t2,2) - 
                  32907*Power(t2,3) + 28156*Power(t2,4) - 
                  9841*Power(t2,5) + 864*Power(t2,6) + 22*Power(t2,7)) + 
               Power(s2,8)*(-22 + 117*Power(t1,2) + 144*t2 + 
                  44*Power(t2,2) - 2*t1*(7 + 219*t2)) + 
               Power(s2,7)*(202 - 434*Power(t1,3) - 1335*t2 + 
                  1335*Power(t2,2) - 144*Power(t2,3) + 
                  Power(t1,2)*(337 + 1478*t2) + 
                  t1*(338 - 1548*t2 - 135*Power(t2,2))) + 
               Power(s2,6)*(-288 + 938*Power(t1,4) + 870*t2 - 
                  1450*Power(t2,2) + 575*Power(t2,3) - 
                  196*Power(t2,4) - Power(t1,3)*(1447 + 2828*t2) + 
                  Power(t1,2)*(-1519 + 6303*t2 + Power(t2,2)) + 
                  t1*(-1212 + 8196*t2 - 8134*Power(t2,2) + 
                     875*Power(t2,3))) + 
               Power(s2,5)*(-1334 - 1302*Power(t1,5) + 9200*t2 - 
                  14799*Power(t2,2) + 7607*Power(t2,3) - 
                  553*Power(t2,4) + 56*Power(t2,5) + 
                  Power(t1,4)*(3145 + 3402*t2) + 
                  Power(t1,3)*(3387 - 13650*t2 + 445*Power(t2,2)) + 
                  Power(t1,2)*
                   (2730 - 21027*t2 + 20904*Power(t2,2) - 
                     2012*Power(t2,3)) + 
                  t1*(1588 - 2799*t2 + 5127*Power(t2,2) - 
                     1820*Power(t2,3) + 581*Power(t2,4))) + 
               Power(s2,4)*(4736 + 1204*Power(t1,6) - 23309*t2 + 
                  36025*Power(t2,2) - 22091*Power(t2,3) + 
                  5880*Power(t2,4) - 1095*Power(t2,5) - 
                  Power(t1,5)*(4079 + 2716*t2) - 
                  5*Power(t1,4)*(888 - 3561*t2 + 119*Power(t2,2)) + 
                  Power(t1,3)*
                   (-2461 + 28979*t2 - 29585*Power(t2,2) + 
                     2135*Power(t2,3)) + 
                  Power(t1,2)*
                   (-3230 + 1305*t2 - 5832*Power(t2,2) + 
                     2490*Power(t2,3) - 420*Power(t2,4)) + 
                  t1*(3564 - 35790*t2 + 64133*Power(t2,2) - 
                     36431*Power(t2,3) + 3380*Power(t2,4) - 
                     245*Power(t2,5))) + 
               Power(s2,3)*(-3170 - 742*Power(t1,7) + 12349*t2 - 
                  8837*Power(t2,2) - 3824*Power(t2,3) + 
                  2534*Power(t2,4) + 1182*Power(t2,5) + 
                  120*Power(t2,6) + Power(t1,6)*(3299 + 1498*t2) + 
                  3*Power(t1,5)*(1224 - 4888*t2 + 57*Power(t2,2)) - 
                  Power(t1,4)*
                   (286 + 22841*t2 - 25255*Power(t2,2) + 
                     880*Power(t2,3)) - 
                  2*Power(t1,3)*
                   (-1253 - 3157*t2 + 98*Power(t2,2) + 
                     1300*Power(t2,3) + 150*Power(t2,4)) + 
                  Power(t1,2)*
                   (-4473 + 59374*t2 - 111416*Power(t2,2) + 
                     66466*Power(t2,3) - 6250*Power(t2,4) + 
                     400*Power(t2,5)) + 
                  t1*(-16238 + 80154*t2 - 128525*Power(t2,2) + 
                     80536*Power(t2,3) - 19154*Power(t2,4) + 
                     2060*Power(t2,5))) + 
               Power(s2,2)*(-4044 + 294*Power(t1,8) + 21648*t2 - 
                  54230*Power(t2,2) + 62433*Power(t2,3) - 
                  31816*Power(t2,4) + 7368*Power(t2,5) - 
                  1302*Power(t2,6) - Power(t1,7)*(1637 + 588*t2) + 
                  Power(t1,6)*(-1943 + 7593*t2 + 195*Power(t2,2)) + 
                  Power(t1,5)*
                   (2262 + 10014*t2 - 13320*Power(t2,2) - 
                     151*Power(t2,3)) + 
                  Power(t1,4)*
                   (302 - 11832*t2 + 6060*Power(t2,2) + 
                     2435*Power(t2,3) + 550*Power(t2,4)) + 
                  Power(t1,2)*
                   (25447 - 113423*t2 + 176475*Power(t2,2) - 
                     109494*Power(t2,3) + 25000*Power(t2,4) - 
                     1248*Power(t2,5)) + 
                  Power(t1,3)*
                   (4999 - 54932*t2 + 98838*Power(t2,2) - 
                     59108*Power(t2,3) + 4730*Power(t2,4) - 
                     300*Power(t2,5)) + 
                  t1*(17840 - 69568*t2 + 77226*Power(t2,2) - 
                     17551*Power(t2,3) - 9624*Power(t2,4) + 
                     742*Power(t2,5) - 240*Power(t2,6))) + 
               s2*(5828 - 68*Power(t1,9) - 29534*t2 + 
                  63753*Power(t2,2) - 68883*Power(t2,3) + 
                  37835*Power(t2,4) - 11410*Power(t2,5) + 
                  2344*Power(t2,6) + 48*Power(t2,7) + 
                  Power(t1,8)*(457 + 158*t2) + 
                  Power(t1,7)*(619 - 2298*t2 - 161*Power(t2,2)) + 
                  Power(t1,6)*
                   (-1614 - 2117*t2 + 4154*Power(t2,2) + 
                     220*Power(t2,3)) - 
                  Power(t1,5)*
                   (1414 - 8277*t2 + 5139*Power(t2,2) + 
                     1380*Power(t2,3) + 249*Power(t2,4)) + 
                  Power(t1,4)*
                   (-4165 + 28902*t2 - 45965*Power(t2,2) + 
                     26507*Power(t2,3) - 1465*Power(t2,4) + 
                     100*Power(t2,5)) + 
                  Power(t1,3)*
                   (-20543 + 78132*t2 - 110563*Power(t2,2) + 
                     65504*Power(t2,3) - 15422*Power(t2,4) + 
                     308*Power(t2,5)) + 
                  Power(t1,2)*
                   (-27354 + 100463*t2 - 117292*Power(t2,2) + 
                     39792*Power(t2,3) + 6784*Power(t2,4) - 
                     1422*Power(t2,5) + 224*Power(t2,6)) + 
                  t1*(-5122 + 17057*t2 + 6789*Power(t2,2) - 
                     54116*Power(t2,3) + 48931*Power(t2,4) - 
                     14456*Power(t2,5) + 902*Power(t2,6))))) + 
         Power(s,2)*(-1341 - 2394*t1 - 734*Power(t1,2) + 
            112*Power(t1,3) - 52*Power(t1,4) - 238*Power(t1,5) - 
            716*Power(t1,6) + 44*Power(t1,7) + 277*Power(t1,8) - 
            84*Power(t1,9) + 6*Power(t1,10) + 7976*t2 + 
            Power(s2,10)*t2 + 13418*t1*t2 + 5904*Power(t1,2)*t2 + 
            1192*Power(t1,3)*t2 + 1113*Power(t1,4)*t2 + 
            2484*Power(t1,5)*t2 + 913*Power(t1,6)*t2 - 
            1052*Power(t1,7)*t2 + 143*Power(t1,8)*t2 + 
            6*Power(t1,9)*t2 - Power(t1,10)*t2 - 20212*Power(t2,2) - 
            33503*t1*Power(t2,2) - 20093*Power(t1,2)*Power(t2,2) - 
            8138*Power(t1,3)*Power(t2,2) - 
            5490*Power(t1,4)*Power(t2,2) - 
            3213*Power(t1,5)*Power(t2,2) + 
            1617*Power(t1,6)*Power(t2,2) + 210*Power(t1,7)*Power(t2,2) - 
            78*Power(t1,8)*Power(t2,2) + 4*Power(t1,9)*Power(t2,2) + 
            27817*Power(t2,3) + 46668*t1*Power(t2,3) + 
            34524*Power(t1,2)*Power(t2,3) + 
            17233*Power(t1,3)*Power(t2,3) + 
            6783*Power(t1,4)*Power(t2,3) - 
            1048*Power(t1,5)*Power(t2,3) - 942*Power(t1,6)*Power(t2,3) + 
            123*Power(t1,7)*Power(t2,3) - 6*Power(t1,8)*Power(t2,3) - 
            22144*Power(t2,4) - 38300*t1*Power(t2,4) - 
            32069*Power(t1,2)*Power(t2,4) - 
            15084*Power(t1,3)*Power(t2,4) - 
            1410*Power(t1,4)*Power(t2,4) + 
            1350*Power(t1,5)*Power(t2,4) - 31*Power(t1,6)*Power(t2,4) + 
            4*Power(t1,7)*Power(t2,4) + 10273*Power(t2,5) + 
            18461*t1*Power(t2,5) + 15519*Power(t1,2)*Power(t2,5) + 
            4776*Power(t1,3)*Power(t2,5) - 553*Power(t1,4)*Power(t2,5) - 
            55*Power(t1,5)*Power(t2,5) - Power(t1,6)*Power(t2,5) - 
            2837*Power(t2,6) - 4890*t1*Power(t2,6) - 
            3174*Power(t1,2)*Power(t2,6) - 248*Power(t1,3)*Power(t2,6) + 
            23*Power(t1,4)*Power(t2,6) + 514*Power(t2,7) + 
            560*t1*Power(t2,7) + 144*Power(t1,2)*Power(t2,7) + 
            6*Power(t1,3)*Power(t2,7) - 46*Power(t2,8) - 
            20*t1*Power(t2,8) - 
            Power(s2,9)*(1 - 20*t2 + 11*Power(t2,2) + t1*(3 + 6*t2)) + 
            Power(s2,8)*(-5 + 60*t2 + 19*Power(t2,2) + 38*Power(t2,3) + 
               Power(t1,2)*(24 + 17*t2) + 
               t1*(-13 - 146*t2 + 48*Power(t2,2))) - 
            Power(s2,7)*(-42 + 310*t2 - 761*Power(t2,2) + 
               212*Power(t2,3) - 5*Power(t2,4) + 
               Power(t1,3)*(90 + 34*t2) + 
               Power(t1,2)*(-143 - 540*t2 + 56*Power(t2,2)) + 
               t1*(-46 + 683*t2 + 203*Power(t2,2) + 220*Power(t2,3))) + 
            Power(s2,6)*(-5 - 269*t2 + 580*Power(t2,2) - 
               46*Power(t2,3) + 150*Power(t2,4) + 
               14*Power(t1,4)*(15 + 4*t2) - 
               Power(t1,3)*(613 + 1288*t2 + 52*Power(t2,2)) + 
               Power(t1,2)*(14 + 3077*t2 + 747*Power(t2,2) + 
                  506*Power(t2,3)) + 
               t1*(-181 + 1092*t2 - 4308*Power(t2,2) + 
                  1075*Power(t2,3) + 5*Power(t2,4))) - 
            Power(s2,5)*(656 - 3670*t2 + 7414*Power(t2,2) - 
               6513*Power(t2,3) + 1711*Power(t2,4) - 4*Power(t2,5) + 
               14*Power(t1,5)*(24 + 5*t2) - 
               Power(t1,4)*(1529 + 2044*t2 + 186*Power(t2,2)) + 
               Power(t1,3)*(862 + 7383*t2 + 1277*Power(t2,2) + 
                  554*Power(t2,3)) + 
               Power(t1,2)*(-530 + 686*t2 - 10809*Power(t2,2) + 
                  2578*Power(t2,3) + 109*Power(t2,4)) + 
               t1*(79 - 1724*t2 + 3871*Power(t2,2) + 171*Power(t2,3) + 
                  484*Power(t2,4) - 36*Power(t2,5))) + 
            Power(s2,4)*(998 - 4360*t2 + 7403*Power(t2,2) - 
               6289*Power(t2,3) + 2878*Power(t2,4) - 347*Power(t2,5) + 
               5*Power(t2,6) + 14*Power(t1,6)*(27 + 4*t2) - 
               Power(t1,5)*(2419 + 2100*t2 + 164*Power(t2,2)) + 
               5*Power(t1,4)*
                (568 + 2093*t2 + 191*Power(t2,2) + 44*Power(t2,3)) + 
               Power(t1,3)*(-1106 - 3028*t2 - 15166*Power(t2,2) + 
                  3925*Power(t2,3) + 290*Power(t2,4)) + 
               Power(t1,2)*(-70 - 3891*t2 + 11473*Power(t2,2) + 
                  558*Power(t2,3) + 175*Power(t2,4) - 125*Power(t2,5)) \
+ t1*(2587 - 13408*t2 + 27519*Power(t2,2) - 26316*Power(t2,3) + 
                  7222*Power(t2,4) + 67*Power(t2,5))) + 
            Power(s2,3)*(1916 - 10842*t2 + 23887*Power(t2,2) - 
               25914*Power(t2,3) + 13823*Power(t2,4) - 
               2516*Power(t2,5) - 242*Power(t2,6) + 20*Power(t2,7) - 
               2*Power(t1,7)*(147 + 11*t2) + 
               Power(t1,6)*(2453 + 1316*t2 + 32*Power(t2,2)) + 
               Power(t1,5)*(-4350 - 9025*t2 + 51*Power(t2,2) + 
                  112*Power(t2,3)) + 
               Power(t1,4)*(1274 + 7982*t2 + 12099*Power(t2,2) - 
                  4000*Power(t2,3) - 345*Power(t2,4)) + 
               2*Power(t1,3)*
                (725 + 1553*t2 - 9305*Power(t2,2) + 377*Power(t2,3) + 
                  455*Power(t2,4) + 80*Power(t2,5)) - 
               Power(t1,2)*(4291 - 18522*t2 + 39072*Power(t2,2) - 
                  43782*Power(t2,3) + 13506*Power(t2,4) + 
                  164*Power(t2,5)) + 
               t1*(-4494 + 20347*t2 - 33267*Power(t2,2) + 
                  26764*Power(t2,3) - 12798*Power(t2,4) + 
                  2086*Power(t2,5) + 86*Power(t2,6))) + 
            s2*(4779 - 26782*t2 + 62699*Power(t2,2) - 
               76111*Power(t2,3) + 48985*Power(t2,4) - 
               15574*Power(t2,5) + 2352*Power(t2,6) - 378*Power(t2,7) + 
               30*Power(t2,8) + Power(t1,9)*(-45 + 4*t2) + 
               Power(t1,8)*(548 + 48*t2 - 23*Power(t2,2)) + 
               Power(t1,7)*(-1554 - 1277*t2 + 373*Power(t2,2) + 
                  54*Power(t2,3)) + 
               Power(t1,6)*(50 + 4790*t2 + 347*Power(t2,2) - 
                  890*Power(t2,3) - 55*Power(t2,4)) + 
               Power(t1,5)*(2485 - 2406*t2 - 8191*Power(t2,2) + 
                  3057*Power(t2,3) + 470*Power(t2,4) + 20*Power(t2,5)) \
+ Power(t1,4)*(-609 - 2252*t2 + 150*Power(t2,2) + 12785*Power(t2,3) - 
                  6895*Power(t2,4) + 92*Power(t2,5)) + 
               Power(t1,3)*(-2628 + 10557*t2 - 10001*Power(t2,2) + 
                  3814*Power(t2,3) - 6632*Power(t2,4) + 
                  3434*Power(t2,5) - 72*Power(t2,6)) + 
               Power(t1,2)*(-1712 + 5276*t2 + 763*Power(t2,2) - 
                  19562*Power(t2,3) + 23237*Power(t2,4) - 
                  7434*Power(t2,5) - 158*Power(t2,6) + 24*Power(t2,7)) \
+ t1*(4798 - 26566*t2 + 62795*Power(t2,2) - 78179*Power(t2,3) + 
                  54492*Power(t2,4) - 20738*Power(t2,5) + 
                  3302*Power(t2,6) + 54*Power(t2,7))) - 
            Power(s2,2)*(5727 + Power(t1,8)*(-150 + t2) - 30836*t2 + 
               67632*Power(t2,2) - 74306*Power(t2,3) + 
               41672*Power(t2,4) - 10748*Power(t2,5) + 
               796*Power(t2,6) + 42*Power(t2,7) + 
               Power(t1,7)*(1543 + 440*t2 - 36*Power(t2,2)) + 
               Power(t1,6)*(-3594 - 4623*t2 + 587*Power(t2,2) + 
                  150*Power(t2,3)) + 
               Power(t1,5)*(653 + 8788*t2 + 4752*Power(t2,2) - 
                  2557*Power(t2,3) - 205*Power(t2,4)) - 
               Power(t1,3)*(3207 - 9016*t2 + 22030*Power(t2,2) - 
                  35716*Power(t2,3) + 13540*Power(t2,4) + 
                  56*Power(t2,5)) + 
               Power(t1,4)*(3065 - 823*t2 - 17002*Power(t2,2) + 
                  3210*Power(t2,3) + 1190*Power(t2,4) + 90*Power(t2,5)) \
+ Power(t1,2)*(-6176 + 27657*t2 - 41355*Power(t2,2) + 
                  31072*Power(t2,3) - 17962*Power(t2,4) + 
                  4620*Power(t2,5) + 42*Power(t2,6)) + 
               t1*(267 - 4068*t2 + 15884*Power(t2,2) - 
                  27921*Power(t2,3) + 22455*Power(t2,4) - 
                  5812*Power(t2,5) - 450*Power(t2,6) + 60*Power(t2,7))) \
+ Power(s1,10)*(5*Power(s2,3) + Power(s2,2)*(-3 - 16*t1 + 2*t2) + 
               s2*(-2 + 17*Power(t1,2) + t1*(9 - 7*t2) + 5*t2 - 
                  3*Power(t2,2)) + 
               t1*(-6*Power(t1,2) + (-1 + t2)*t2 + t1*(-6 + 5*t2))) - 
            Power(s1,9)*(3*Power(s2,4) + 23*Power(t1,4) + 
               Power(t1,3)*(75 - 78*t2) + (-2 + t2)*Power(-1 + t2,2) + 
               Power(s2,3)*(-49 - 29*t1 + 54*t2) + 
               Power(s2,2)*(70 + 72*Power(t1,2) + t1*(166 - 179*t2) - 
                  120*t2 + 43*Power(t2,2)) + 
               Power(t1,2)*(72 - 130*t2 + 51*Power(t2,2)) + 
               t1*(21 - 49*t2 + 24*Power(t2,2) + 4*Power(t2,3)) - 
               s2*(28 + 69*Power(t1,3) + Power(t1,2)*(192 - 203*t2) - 
                  59*t2 + 23*Power(t2,2) + 8*Power(t2,3) + 
                  2*t1*(69 - 121*t2 + 45*Power(t2,2)))) + 
            Power(s1,8)*(-138*Power(s2,5) + 236*Power(t1,5) + 
               Power(t1,4)*(814 - 419*t2) + 
               Power(s2,4)*(644 + 794*t1 - 342*t2) + 
               Power(-1 + t2,2)*(-25 + 19*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(834 - 745*t2 + 67*Power(t2,2)) - 
               Power(s2,3)*(782 + 1792*Power(t1,2) + 
                  t1*(2788 - 1470*t2) - 797*t2 + 102*Power(t2,2)) + 
               Power(t1,2)*(183 - 17*t2 - 307*Power(t2,2) + 
                  119*Power(t2,3)) + 
               t1*(-71 + 295*t2 - 440*Power(t2,2) + 219*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,2)*(244 + 1990*Power(t1,3) + 
                  Power(t1,2)*(4462 - 2337*t2) - 235*t2 - 
                  133*Power(t2,2) + 102*Power(t2,3) + 
                  t1*(2461 - 2452*t2 + 321*Power(t2,2))) - 
               s2*(1090*Power(t1,4) - 4*Power(t1,3)*(-783 + 407*t2) + 
                  Power(t1,2)*(2515 - 2404*t2 + 288*Power(t2,2)) + 
                  6*(-8 + 43*t2 - 72*Power(t2,2) + 37*Power(t2,3)) + 
                  t1*(445 - 319*t2 - 360*Power(t2,2) + 190*Power(t2,3)))\
) + Power(s1,7)*(-418*Power(s2,6) - 397*Power(t1,6) + 
               2*Power(s2,5)*(968 + 1309*t1 - 328*t2) + 
               2*Power(t1,5)*(-621 + 86*t2) + 
               Power(t1,4)*(-840 - 1825*t2 + 876*Power(t2,2)) + 
               Power(t1,3)*(1398 - 5807*t2 + 4556*Power(t2,2) - 
                  660*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (17 + 121*t2 - 162*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(584 - 1529*t2 + 1209*Power(t2,2) + 17*Power(t2,3) - 
                  281*Power(t2,4)) + 
               Power(t1,2)*(2020 - 5672*t2 + 4973*Power(t2,2) - 
                  1311*Power(t2,3) + 9*Power(t2,4)) + 
               Power(s2,4)*(-2331 - 6694*Power(t1,2) + 722*t2 + 
                  234*Power(t2,2) + t1*(-9548 + 3099*t2)) + 
               Power(s2,3)*(-49 + 8951*Power(t1,3) + 
                  Power(t1,2)*(18255 - 5524*t2) + 2504*t2 - 
                  2702*Power(t2,2) + 484*Power(t2,3) + 
                  t1*(8610 - 1222*t2 - 1299*Power(t2,2))) + 
               Power(s2,2)*(1312 - 6599*Power(t1,4) - 4005*t2 + 
                  4143*Power(t2,2) - 1443*Power(t2,3) + 
                  12*Power(t2,4) + Power(t1,3)*(-16852 + 4547*t2) + 
                  Power(t1,2)*(-11025 - 1622*t2 + 2805*Power(t2,2)) + 
                  t1*(1107 - 10196*t2 + 9647*Power(t2,2) - 
                     1545*Power(t2,3))) + 
               s2*(-491 + 2539*Power(t1,5) + 
                  Power(t1,4)*(7451 - 1638*t2) + 1428*t2 - 
                  1295*Power(t2,2) + 86*Power(t2,3) + 272*Power(t2,4) + 
                  Power(t1,3)*(5586 + 3947*t2 - 2616*Power(t2,2)) + 
                  Power(t1,2)*
                   (-2472 + 13550*t2 - 11555*Power(t2,2) + 
                     1740*Power(t2,3)) + 
                  t1*(-3259 + 9459*t2 - 8923*Power(t2,2) + 
                     2730*Power(t2,3) - 45*Power(t2,4)))) + 
            Power(s1,6)*(-515*Power(s2,7) + 42*Power(t1,7) + 
               Power(s2,6)*(2241 + 3398*t1 - 250*t2) + 
               Power(t1,6)*(-519 + 1261*t2) + 
               Power(t1,5)*(-3138 + 8168*t2 - 2535*Power(t2,2)) - 
               Power(s2,5)*(1717 + 9261*Power(t1,2) + 
                  t1*(11553 - 313*t2) + 2975*t2 - 1405*Power(t2,2)) + 
               2*Power(-1 + t2,2)*
                (99 - 358*t2 + 227*Power(t2,2) + 81*Power(t2,3)) + 
               Power(t1,4)*(-7467 + 17427*t2 - 8453*Power(t2,2) + 
                  1164*Power(t2,3)) + 
               Power(t1,3)*(-7623 + 13623*t2 - 3983*Power(t2,2) - 
                  1144*Power(t2,3) + 68*Power(t2,4)) + 
               Power(t1,2)*(-2190 - 1387*t2 + 11198*Power(t2,2) - 
                  9738*Power(t2,3) + 2150*Power(t2,4)) + 
               t1*(717 - 5092*t2 + 10554*Power(t2,2) - 
                  9122*Power(t2,3) + 2950*Power(t2,4) - 7*Power(t2,5)) \
+ Power(s2,4)*(-3858 + 13406*Power(t1,3) + 12501*t2 - 
                  7953*Power(t2,2) + 1220*Power(t2,3) + 
                  Power(t1,2)*(23025 + 2687*t2) + 
                  t1*(4590 + 20323*t2 - 8229*Power(t2,2))) + 
               Power(s2,3)*(5935 - 10949*Power(t1,4) - 12914*t2 + 
                  8308*Power(t2,2) - 1794*Power(t2,3) + 
                  80*Power(t2,4) - 12*Power(t1,3)*(1804 + 725*t2) + 
                  Power(t1,2)*(80 - 51827*t2 + 18990*Power(t2,2)) + 
                  t1*(19240 - 57327*t2 + 34193*Power(t2,2) - 
                     5080*Power(t2,3))) + 
               Power(s2,2)*(-1624 + 4866*Power(t1,5) + 228*t2 + 
                  5257*Power(t2,2) - 5769*Power(t2,3) + 
                  1941*Power(t2,4) + Power(t1,4)*(8709 + 10598*t2) + 
                  Power(t1,3)*
                   (-10200 + 62753*t2 - 21448*Power(t2,2)) + 
                  Power(t1,2)*
                   (-34591 + 94948*t2 - 53178*Power(t2,2) + 
                     7710*Power(t2,3)) + 
                  t1*(-20205 + 42095*t2 - 23038*Power(t2,2) + 
                     3075*Power(t2,3) - 216*Power(t2,4))) + 
               s2*(-594 - 987*Power(t1,6) + 4111*t2 - 
                  8994*Power(t2,2) + 8464*Power(t2,3) - 
                  3005*Power(t2,4) + 18*Power(t2,5) - 
                  Power(t1,5)*(255 + 5909*t2) + 
                  Power(t1,4)*(10385 - 36442*t2 + 11817*Power(t2,2)) + 
                  Power(t1,3)*
                   (26676 - 67549*t2 + 35391*Power(t2,2) - 
                     5014*Power(t2,3)) + 
                  t1*(3996 + 485*t2 - 15411*Power(t2,2) + 
                     14713*Power(t2,3) - 3849*Power(t2,4)) + 
                  Power(t1,2)*
                   (21931 - 42896*t2 + 18763*Power(t2,2) - 
                     113*Power(t2,3) + 48*Power(t2,4)))) + 
            Power(s1,5)*(-255*Power(s2,8) + 319*Power(t1,8) + 
               3*Power(s2,7)*(313 + 513*t1 + 234*t2) - 
               2*Power(t1,7)*(-765 + 883*t2) + 
               Power(t1,6)*(2679 - 6700*t2 + 2309*Power(t2,2)) + 
               Power(t1,5)*(3718 - 3620*t2 + 692*Power(t2,2) - 
                  644*Power(t2,3)) + 
               Power(t1,3)*(-6352 + 41668*t2 - 64646*Power(t2,2) + 
                  32480*Power(t2,3) - 4247*Power(t2,4)) + 
               Power(t1,4)*(1684 + 16381*t2 - 29451*Power(t2,2) + 
                  8795*Power(t2,3) - 218*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-67 + 717*t2 - 2104*Power(t2,2) + 1683*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(t1,2)*(8870 - 38006*t2 + 51194*Power(t2,2) - 
                  25503*Power(t2,3) + 3302*Power(t2,4) + 
                  208*Power(t2,5)) - 
               t1*(2837 - 9367*t2 + 8098*Power(t2,2) + 
                  2314*Power(t2,3) - 6351*Power(t2,4) + 
                  2469*Power(t2,5)) - 
               Power(s2,6)*(-841 + 3410*Power(t1,2) + 6306*t2 - 
                  2517*Power(t2,2) + t1*(3856 + 6547*t2)) + 
               Power(s2,5)*(-6883 + 2571*Power(t1,3) + 14987*t2 - 
                  7757*Power(t2,2) + 1740*Power(t2,3) + 
                  Power(t1,2)*(3166 + 24343*t2) - 
                  3*t1*(3037 - 14078*t2 + 5388*Power(t2,2))) + 
               Power(s2,4)*(6322 + 2280*Power(t1,4) + 
                  Power(t1,3)*(8646 - 47708*t2) - 2967*t2 - 
                  8236*Power(t2,2) + 2900*Power(t2,3) + 
                  180*Power(t2,4) + 
                  Power(t1,2)*
                   (31266 - 112982*t2 + 42011*Power(t2,2)) + 
                  t1*(35615 - 73646*t2 + 36010*Power(t2,2) - 
                     8370*Power(t2,3))) + 
               Power(s2,2)*(-6540 + 5226*Power(t1,6) + 28261*t2 - 
                  41716*Power(t2,2) + 27819*Power(t2,3) - 
                  7967*Power(t2,4) + 78*Power(t2,5) - 
                  7*Power(t1,5)*(-2936 + 5005*t2) + 
                  3*Power(t1,4)*
                   (13854 - 38588*t2 + 13801*Power(t2,2)) + 
                  Power(t1,3)*
                   (62833 - 110276*t2 + 46718*Power(t2,2) - 
                     12954*Power(t2,3)) + 
                  t1*(-12438 + 94742*t2 - 156126*Power(t2,2) + 
                     86594*Power(t2,3) - 14523*Power(t2,4)) + 
                  Power(t1,2)*
                   (31272 + 23235*t2 - 106704*Power(t2,2) + 
                     36197*Power(t2,3) + 84*Power(t2,4))) - 
               Power(s2,3)*(-3748 + 6215*Power(t1,5) + 
                  Power(t1,4)*(21989 - 53772*t2) + 27671*t2 - 
                  45242*Power(t2,2) + 26204*Power(t2,3) - 
                  5212*Power(t2,4) + 
                  Power(t1,3)*
                   (50221 - 155302*t2 + 56528*Power(t2,2)) + 
                  Power(t1,2)*
                   (69214 - 133823*t2 + 61514*Power(t2,2) - 
                     15230*Power(t2,3)) + 
                  t1*(24478 - 1739*t2 - 50253*Power(t2,2) + 
                     17799*Power(t2,3) + 460*Power(t2,4))) + 
               s2*(1707 - 2055*Power(t1,7) - 5949*t2 + 
                  6166*Power(t2,2) + 365*Power(t2,3) - 
                  4679*Power(t2,4) + 2390*Power(t2,5) + 
                  Power(t1,6)*(-8988 + 12239*t2) - 
                  4*Power(t1,5)*(4254 - 11054*t2 + 3887*Power(t2,2)) + 
                  Power(t1,4)*
                   (-26069 + 38732*t2 - 14149*Power(t2,2) + 
                     4998*Power(t2,3)) + 
                  Power(t1,3)*
                   (-14800 - 38388*t2 + 94138*Power(t2,2) - 
                     30093*Power(t2,3) + 414*Power(t2,4)) + 
                  Power(t1,2)*
                   (15180 - 109282*t2 + 176358*Power(t2,2) - 
                     93449*Power(t2,3) + 13714*Power(t2,4)) + 
                  t1*(15342 - 66362*t2 + 93267*Power(t2,2) - 
                     53618*Power(t2,3) + 11510*Power(t2,4) - 
                     9*Power(t2,5)))) + 
            Power(s1,4)*(-200*Power(t1,9) + 
               Power(t1,8)*(-638 + 747*t2) + 
               Power(s2,8)*(10 - 360*t1 + 938*t2) + 
               Power(t1,7)*(1030 + 977*t2 - 691*Power(t2,2)) + 
               Power(s2,7)*(850 + 2862*Power(t1,2) + 
                  t1*(1338 - 8288*t2) - 3967*t2 + 2100*Power(t2,2)) + 
               Power(t1,6)*(2871 - 10762*t2 + 4999*Power(t2,2) - 
                  45*Power(t2,3)) + 
               Power(t1,5)*(3767 - 28085*t2 + 33506*Power(t2,2) - 
                  8317*Power(t2,3) + 189*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-678 + 2725*t2 - 3519*Power(t2,2) + 927*Power(t2,3) + 
                  979*Power(t2,4)) + 
               Power(t1,4)*(11572 - 39465*t2 + 44165*Power(t2,2) - 
                  17239*Power(t2,3) + 2378*Power(t2,4)) + 
               Power(t1,3)*(15394 - 30403*t2 - 3205*Power(t2,2) + 
                  29123*Power(t2,3) - 10846*Power(t2,4) + 
                  636*Power(t2,5)) + 
               Power(t1,2)*(4343 + 8541*t2 - 64018*Power(t2,2) + 
                  88087*Power(t2,3) - 42795*Power(t2,4) + 
                  5875*Power(t2,5)) + 
               t1*(-2641 + 21223*t2 - 54585*Power(t2,2) + 
                  62755*Power(t2,3) - 33953*Power(t2,4) + 
                  6961*Power(t2,5) + 240*Power(t2,6)) - 
               Power(s2,6)*(3060 + 9812*Power(t1,3) + 
                  Power(t1,2)*(9096 - 30975*t2) - 2176*t2 + 
                  549*Power(t2,2) - 1330*Power(t2,3) + 
                  t1*(6291 - 27690*t2 + 14557*Power(t2,2))) + 
               Power(s2,5)*(-753 + 18930*Power(t1,4) + 
                  Power(t1,3)*(25698 - 64132*t2) + 20332*t2 - 
                  30107*Power(t2,2) + 8460*Power(t2,3) + 
                  168*Power(t2,4) + 
                  Power(t1,2)*(17247 - 79174*t2 + 41868*Power(t2,2)) - 
                  t1*(-14228 + 2139*t2 + 2756*Power(t2,2) + 
                     6978*Power(t2,3))) + 
               Power(s2,3)*(-9944 + 16690*Power(t1,6) + 
                  Power(t1,5)*(35870 - 62448*t2) + 25323*t2 - 
                  14669*Power(t2,2) + 1426*Power(t2,3) - 
                  2233*Power(t2,4) + 200*Power(t2,5) + 
                  Power(t1,4)*
                   (11792 - 103667*t2 + 57332*Power(t2,2)) + 
                  Power(t1,3)*
                   (7992 + 84198*t2 - 52450*Power(t2,2) - 
                     14362*Power(t2,3)) + 
                  t1*(-49994 + 201631*t2 - 260141*Power(t2,2) + 
                     128938*Power(t2,3) - 23688*Power(t2,4)) + 
                  Power(t1,2)*
                   (-26585 + 262532*t2 - 356870*Power(t2,2) + 
                     97980*Power(t2,3) - 60*Power(t2,4))) - 
               Power(s2,4)*(-11442 + 22440*Power(t1,5) + 
                  Power(t1,4)*(39580 - 80455*t2) + 47945*t2 - 
                  63302*Power(t2,2) + 33517*Power(t2,3) - 
                  6940*Power(t2,4) + 
                  Power(t1,3)*
                   (22048 - 120113*t2 + 64593*Power(t2,2)) + 
                  Power(t1,2)*
                   (21877 + 27454*t2 - 23159*Power(t2,2) - 
                     14325*Power(t2,3)) + 
                  t1*(-8520 + 117449*t2 - 167166*Power(t2,2) + 
                     46641*Power(t2,3) + 405*Power(t2,4))) + 
               Power(s2,2)*(-1839 - 7572*Power(t1,7) + 22805*t2 - 
                  64083*Power(t2,2) + 71040*Power(t2,3) - 
                  35486*Power(t2,4) + 7596*Power(t2,5) + 
                  Power(t1,6)*(-19048 + 29093*t2) + 
                  Power(t1,5)*(957 + 50020*t2 - 28863*Power(t2,2)) + 
                  Power(t1,4)*
                   (10906 - 99120*t2 + 54191*Power(t2,2) + 
                     6966*Power(t2,3)) + 
                  Power(t1,3)*
                   (34389 - 282130*t2 + 366176*Power(t2,2) - 
                     98194*Power(t2,3) + 852*Power(t2,4)) + 
                  Power(t1,2)*
                   (78064 - 300608*t2 + 375327*Power(t2,2) - 
                     174618*Power(t2,3) + 29088*Power(t2,4)) + 
                  t1*(38260 - 94762*t2 + 44783*Power(t2,2) + 
                     19263*Power(t2,3) - 6979*Power(t2,4) - 
                     72*Power(t2,5))) + 
               s2*(3987 + 1902*Power(t1,8) + 
                  Power(t1,7)*(5446 - 7340*t2) - 23398*t2 + 
                  52569*Power(t2,2) - 59491*Power(t2,3) + 
                  35581*Power(t2,4) - 9146*Power(t2,5) - 
                  102*Power(t2,6) + 
                  Power(t1,6)*(-3537 - 11992*t2 + 7404*Power(t2,2)) - 
                  Power(t1,5)*
                   (11060 - 53101*t2 + 26594*Power(t2,2) + 
                     1236*Power(t2,3)) + 
                  Power(t1,3)*
                   (-51084 + 186387*t2 - 222653*Power(t2,2) + 
                     96436*Power(t2,3) - 14718*Power(t2,4)) + 
                  Power(t1,4)*
                   (-19338 + 144800*t2 - 179871*Power(t2,2) + 
                     46712*Power(t2,3) - 744*Power(t2,4)) - 
                  Power(t1,2)*
                   (43837 - 100138*t2 + 27077*Power(t2,2) + 
                     49944*Power(t2,3) - 20277*Power(t2,4) + 
                     852*Power(t2,5)) - 
                  t1*(3255 + 28423*t2 - 124403*Power(t2,2) + 
                     156997*Power(t2,3) - 77178*Power(t2,4) + 
                     12972*Power(t2,5)))) + 
            Power(s1,2)*(-2*Power(s2,10)*(7 + 4*t1 - 18*t2) - 
               Power(t1,10)*(5 + 2*t2) + 
               Power(t1,9)*(54 - 35*t2 + 6*Power(t2,2)) + 
               Power(t1,8)*(67 - 439*t2 + 145*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(t1,6)*(375 + 4798*t2 - 6420*Power(t2,2) + 
                  1836*Power(t2,3) - 154*Power(t2,4)) + 
               Power(t1,7)*(-703 + 487*t2 + 533*Power(t2,2) - 
                  58*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,5)*(-1219 + 9328*t2 - 21517*Power(t2,2) + 
                  15378*Power(t2,3) - 2684*Power(t2,4) + 
                  107*Power(t2,5)) + 
               Power(t1,4)*(-14373 + 37289*t2 - 35357*Power(t2,2) + 
                  15980*Power(t2,3) - 3682*Power(t2,4) + 
                  264*Power(t2,5)) + 
               2*Power(-1 + t2,2)*
                (837 - 2592*t2 + 2675*Power(t2,2) - 785*Power(t2,3) - 
                  342*Power(t2,4) + 490*Power(t2,5)) + 
               Power(t1,3)*(-19905 + 56971*t2 - 35285*Power(t2,2) - 
                  27186*Power(t2,3) + 31868*Power(t2,4) - 
                  7002*Power(t2,5) + 436*Power(t2,6)) + 
               2*Power(t1,2)*
                (-2749 + 4523*t2 + 16018*Power(t2,2) - 
                  45148*Power(t2,3) + 38774*Power(t2,4) - 
                  12347*Power(t2,5) + 977*Power(t2,6)) + 
               t1*(4013 - 25071*t2 + 66675*Power(t2,2) - 
                  89574*Power(t2,3) + 61170*Power(t2,4) - 
                  18615*Power(t2,5) + 1152*Power(t2,6) + 
                  250*Power(t2,7)) + 
               Power(s2,9)*(-39 + 74*Power(t1,2) - 92*t2 + 
                  72*Power(t2,2) - 4*t1*(-39 + 82*t2)) + 
               Power(s2,8)*(214 - 304*Power(t1,3) - 1283*t2 + 
                  671*Power(t2,2) + 36*Power(t2,3) + 
                  Power(t1,2)*(-739 + 1306*t2) + 
                  t1*(562 + 520*t2 - 504*Power(t2,2))) + 
               Power(s2,7)*(-404 + 728*Power(t1,4) + 
                  Power(t1,3)*(1986 - 2980*t2) + 3478*t2 - 
                  5042*Power(t2,2) + 1580*Power(t2,3) + 
                  Power(t1,2)*(-2992 - 929*t2 + 1490*Power(t2,2)) - 
                  t1*(2370 - 10949*t2 + 5651*Power(t2,2) + 
                     128*Power(t2,3))) + 
               Power(s2,6)*(191 - 1120*Power(t1,5) + 4008*t2 - 
                  5473*Power(t2,2) + 135*Power(t2,3) + 
                  1113*Power(t2,4) + 14*Power(t1,4)*(-241 + 306*t2) + 
                  Power(t1,3)*(8388 - 229*t2 - 2382*Power(t2,2)) + 
                  Power(t1,2)*
                   (9683 - 38908*t2 + 19786*Power(t2,2) + 
                     42*Power(t2,3)) + 
                  t1*(2397 - 21579*t2 + 31502*Power(t2,2) - 
                     9771*Power(t2,3) + 56*Power(t2,4))) + 
               Power(s2,5)*(4761 + 1148*Power(t1,6) + 
                  Power(t1,5)*(3794 - 4004*t2) - 28184*t2 + 
                  52249*Power(t2,2) - 34458*Power(t2,3) + 
                  6317*Power(t2,4) - 42*Power(t2,5) + 
                  Power(t1,4)*(-14054 + 3355*t2 + 2154*Power(t2,2)) + 
                  Power(t1,3)*
                   (-20422 + 75649*t2 - 37795*Power(t2,2) + 
                     456*Power(t2,3)) + 
                  Power(t1,2)*
                   (-4640 + 53740*t2 - 81207*Power(t2,2) + 
                     24943*Power(t2,3) - 252*Power(t2,4)) - 
                  t1*(471 + 27798*t2 - 39039*Power(t2,2) + 
                     6857*Power(t2,3) + 4371*Power(t2,4))) + 
               Power(s2,3)*(3138 + 344*Power(t1,8) + 
                  Power(t1,7)*(1414 - 876*t2) + 976*t2 - 
                  39658*Power(t2,2) + 63590*Power(t2,3) - 
                  30318*Power(t2,4) + 2696*Power(t2,5) - 
                  160*Power(t2,6) + 
                  Power(t1,6)*(-9672 + 4685*t2 + 54*Power(t2,2)) + 
                  Power(t1,5)*
                   (-17814 + 62423*t2 - 29561*Power(t2,2) + 
                     864*Power(t2,3)) + 
                  Power(t1,4)*
                   (4732 + 45422*t2 - 86224*Power(t2,2) + 
                     25570*Power(t2,3) - 400*Power(t2,4)) - 
                  2*Power(t1,3)*
                   (-1337 + 56690*t2 - 77192*Power(t2,2) + 
                     22434*Power(t2,3) + 1945*Power(t2,4)) + 
                  t1*(49212 - 173741*t2 + 245309*Power(t2,2) - 
                     177862*Power(t2,3) + 67506*Power(t2,4) - 
                     12258*Power(t2,5)) + 
                  Power(t1,2)*
                   (38568 - 227521*t2 + 441286*Power(t2,2) - 
                     302052*Power(t2,3) + 57956*Power(t2,4) - 
                     1100*Power(t2,5))) + 
               Power(s2,4)*(-11052 - 784*Power(t1,7) + 41809*t2 - 
                  61977*Power(t2,2) + 48582*Power(t2,3) - 
                  21616*Power(t2,4) + 4720*Power(t2,5) + 
                  56*Power(t1,6)*(-51 + 43*t2) + 
                  Power(t1,5)*(14744 - 5633*t2 - 982*Power(t2,2)) - 
                  5*Power(t1,4)*
                   (-4967 + 17606*t2 - 8600*Power(t2,2) + 
                     190*Power(t2,3)) + 
                  Power(t1,3)*
                   (1967 - 68243*t2 + 111181*Power(t2,2) - 
                     33720*Power(t2,3) + 450*Power(t2,4)) + 
                  Power(t1,2)*
                   (-711 + 78430*t2 - 109156*Power(t2,2) + 
                     27608*Power(t2,3) + 6380*Power(t2,4)) + 
                  t1*(-21549 + 128157*t2 - 244271*Power(t2,2) + 
                     165070*Power(t2,3) - 31256*Power(t2,4) + 
                     405*Power(t2,5))) + 
               s2*(-8448 + 10*Power(t1,10) + Power(t1,9)*(74 - 4*t2) + 
                  44054*t2 - 94825*Power(t2,2) + 106086*Power(t2,3) - 
                  66789*Power(t2,4) + 24646*Power(t2,5) - 
                  4508*Power(t2,6) - 216*Power(t2,7) + 
                  Power(t1,8)*(-763 + 469*t2 - 58*Power(t2,2)) + 
                  Power(t1,7)*
                   (-1378 + 5603*t2 - 2321*Power(t2,2) + 
                     88*Power(t2,3)) + 
                  Power(t1,6)*
                   (3720 + 256*t2 - 7751*Power(t2,2) + 
                     1891*Power(t2,3) - 36*Power(t2,4)) + 
                  Power(t1,5)*
                   (-147 - 33478*t2 + 44137*Power(t2,2) - 
                     13355*Power(t2,3) + 517*Power(t2,4)) + 
                  Power(t1,3)*
                   (56558 - 166367*t2 + 192555*Power(t2,2) - 
                     111996*Power(t2,3) + 32004*Power(t2,4) - 
                     3898*Power(t2,5)) + 
                  Power(t1,4)*
                   (12191 - 74415*t2 + 152525*Power(t2,2) - 
                     106042*Power(t2,3) + 19763*Power(t2,4) - 
                     630*Power(t2,5)) + 
                  t1*(-3067 + 39846*t2 - 155315*Power(t2,2) + 
                     247631*Power(t2,3) - 179665*Power(t2,4) + 
                     56576*Power(t2,5) - 6198*Power(t2,6)) + 
                  Power(t1,2)*
                   (45810 - 128556*t2 + 59969*Power(t2,2) + 
                     99087*Power(t2,3) - 95200*Power(t2,4) + 
                     20260*Power(t2,5) - 900*Power(t2,6))) + 
               Power(s2,2)*(9979 - 88*Power(t1,9) - 56210*t2 + 
                  136191*Power(t2,2) - 166229*Power(t2,3) + 
                  104335*Power(t2,4) - 33178*Power(t2,5) + 
                  5208*Power(t2,6) + 4*Power(t1,8)*(-109 + 40*t2) + 
                  Power(t1,7)*(3772 - 2111*t2 + 150*Power(t2,2)) + 
                  Power(t1,6)*
                   (7185 - 25964*t2 + 11726*Power(t2,2) - 
                     402*Power(t2,3)) + 
                  Power(t1,5)*
                   (-7069 - 13561*t2 + 37008*Power(t2,2) - 
                     10435*Power(t2,3) + 180*Power(t2,4)) + 
                  Power(t1,4)*
                   (-1911 + 87420*t2 - 116511*Power(t2,2) + 
                     35501*Power(t2,3) + 405*Power(t2,4)) + 
                  Power(t1,3)*
                   (-32752 + 192635*t2 - 380272*Power(t2,2) + 
                     262104*Power(t2,3) - 50096*Power(t2,4) + 
                     1260*Power(t2,5)) + 
                  Power(t1,2)*
                   (-80345 + 261010*t2 - 340530*Power(t2,2) + 
                     225296*Power(t2,3) - 74212*Power(t2,4) + 
                     11172*Power(t2,5)) + 
                  t1*(-28935 + 70325*t2 + 15360*Power(t2,2) - 
                     136131*Power(t2,3) + 94296*Power(t2,4) - 
                     16098*Power(t2,5) + 552*Power(t2,6)))) + 
            Power(s1,3)*(28*Power(s2,10) + 29*Power(t1,10) + 
               Power(t1,9)*(82 - 76*t2) + 
               Power(s2,9)*(-34 - 378*t1 + 392*t2) + 
               Power(t1,8)*(-738 + 287*t2 + 18*Power(t2,2)) + 
               Power(s2,8)*(-35 + 2088*Power(t1,2) + 
                  t1*(592 - 3571*t2) - 692*t2 + 764*Power(t2,2)) + 
               Power(t1,7)*(-764 + 4483*t2 - 2044*Power(t2,2) + 
                  76*Power(t2,3)) + 
               Power(t1,6)*(835 + 3766*t2 - 8507*Power(t2,2) + 
                  2179*Power(t2,3) - 47*Power(t2,4)) + 
               Power(t1,5)*(-4720 - 3481*t2 + 11355*Power(t2,2) - 
                  3177*Power(t2,3) + 5*Power(t2,4)) + 
               Power(t1,3)*(5174 - 44781*t2 + 109076*Power(t2,2) - 
                  100768*Power(t2,3) + 34346*Power(t2,4) - 
                  3270*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (913 - 3603*t2 + 7254*Power(t2,2) - 7569*Power(t2,3) + 
                  3528*Power(t2,4) + 100*Power(t2,5)) - 
               Power(t1,4)*(8823 + 10098*t2 - 63557*Power(t2,2) + 
                  55152*Power(t2,3) - 11502*Power(t2,4) + 
                  509*Power(t2,5)) + 
               t1*(4604 - 18105*t2 + 14069*Power(t2,2) + 
                  21425*Power(t2,3) - 39129*Power(t2,4) + 
                  20908*Power(t2,5) - 3772*Power(t2,6)) + 
               Power(t1,2)*(13986 - 61664*t2 + 95425*Power(t2,2) - 
                  57397*Power(t2,3) + 5939*Power(t2,4) + 
                  4304*Power(t2,5) - 636*Power(t2,6)) + 
               Power(s2,7)*(-13 - 6413*Power(t1,3) - 3678*t2 + 
                  2578*Power(t2,2) + 456*Power(t2,3) + 
                  Power(t1,2)*(-3253 + 14068*t2) + 
                  t1*(1028 + 4524*t2 - 5595*Power(t2,2))) + 
               Power(s2,6)*(-2330 + 12299*Power(t1,4) + 
                  Power(t1,3)*(9072 - 31445*t2) + 15149*t2 - 
                  20781*Power(t2,2) + 6595*Power(t2,3) + 
                  56*Power(t2,4) + 
                  Power(t1,2)*(-6719 - 11668*t2 + 17367*Power(t2,2)) - 
                  t1*(2925 - 31534*t2 + 20593*Power(t2,2) + 
                     2457*Power(t2,3))) + 
               Power(s2,4)*(1230 + 12943*Power(t1,6) + 
                  Power(t1,5)*(15554 - 39257*t2) - 26037*t2 + 
                  68143*Power(t2,2) - 51496*Power(t2,3) + 
                  8940*Power(t2,4) + 135*Power(t2,5) + 
                  15*Power(t1,4)*(-2229 - 463*t2 + 2012*Power(t2,2)) - 
                  Power(t1,3)*
                   (37126 - 196159*t2 + 114878*Power(t2,2) + 
                     5570*Power(t2,3)) + 
                  t1*(-35652 + 76894*t2 - 67249*Power(t2,2) + 
                     46847*Power(t2,3) - 17415*Power(t2,4)) + 
                  Power(t1,2)*
                   (-40508 + 222294*t2 - 297639*Power(t2,2) + 
                     90567*Power(t2,3) - 345*Power(t2,4))) + 
               Power(s2,5)*(6389 - 15477*Power(t1,5) - 16068*t2 + 
                  16211*Power(t2,2) - 12168*Power(t2,3) + 
                  4494*Power(t2,4) + Power(t1,4)*(-14987 + 43826*t2) + 
                  Power(t1,3)*(20138 + 14413*t2 - 29668*Power(t2,2)) + 
                  Power(t1,2)*
                   (16468 - 108364*t2 + 66801*Power(t2,2) + 
                     5274*Power(t2,3)) + 
                  t1*(16101 - 92699*t2 + 124667*Power(t2,2) - 
                     38700*Power(t2,3) - 63*Power(t2,4))) + 
               Power(s2,3)*(-15765 - 7063*Power(t1,7) + 76652*t2 - 
                  144282*Power(t2,2) + 126520*Power(t2,3) - 
                  53048*Power(t2,4) + 9602*Power(t2,5) + 
                  Power(t1,6)*(-10227 + 22336*t2) + 
                  Power(t1,5)*(32720 - 2578*t2 - 18339*Power(t2,2)) + 
                  Power(t1,4)*
                   (42779 - 202946*t2 + 113132*Power(t2,2) + 
                     2720*Power(t2,3)) + 
                  Power(t1,3)*
                   (47383 - 265944*t2 + 361232*Power(t2,2) - 
                     107630*Power(t2,3) + 900*Power(t2,4)) + 
                  Power(t1,2)*
                   (72571 - 126494*t2 + 87414*Power(t2,2) - 
                     62312*Power(t2,3) + 25258*Power(t2,4)) + 
                  t1*(8314 + 78246*t2 - 266999*Power(t2,2) + 
                     222640*Power(t2,3) - 44686*Power(t2,4) + 
                     80*Power(t2,5))) + 
               Power(s2,2)*(13104 + 2373*Power(t1,8) + 
                  Power(t1,7)*(4108 - 7603*t2) - 54417*t2 + 
                  82083*Power(t2,2) - 59609*Power(t2,3) + 
                  26382*Power(t2,4) - 7442*Power(t2,5) - 
                  144*Power(t2,6) + 
                  Power(t1,6)*(-18753 + 4666*t2 + 6215*Power(t2,2)) - 
                  Power(t1,5)*
                   (26113 - 119752*t2 + 63381*Power(t2,2) + 
                     201*Power(t2,3)) + 
                  Power(t1,3)*
                   (-68462 + 83097*t2 - 26570*Power(t2,2) + 
                     29578*Power(t2,3) - 16242*Power(t2,4)) + 
                  Power(t1,4)*
                   (-25161 + 163823*t2 - 232817*Power(t2,2) + 
                     67563*Power(t2,3) - 840*Power(t2,4)) + 
                  t1*(35881 - 197728*t2 + 407555*Power(t2,2) - 
                     370845*Power(t2,3) + 146880*Power(t2,4) - 
                     21324*Power(t2,5)) - 
                  Power(t1,2)*
                   (28285 + 92502*t2 - 398721*Power(t2,2) + 
                     348488*Power(t2,3) - 74320*Power(t2,4) + 
                     1224*Power(t2,5))) + 
               s2*(-1661 - 429*Power(t1,9) + 2950*t2 + 
                  12229*Power(t2,2) - 37296*Power(t2,3) + 
                  38714*Power(t2,4) - 19540*Power(t2,5) + 
                  4604*Power(t2,6) + Power(t1,8)*(-907 + 1330*t2) + 
                  Power(t1,7)*(5794 - 2007*t2 - 942*Power(t2,2)) + 
                  Power(t1,6)*
                   (7694 - 36940*t2 + 18385*Power(t2,2) - 
                     298*Power(t2,3)) + 
                  Power(t1,5)*
                   (3680 - 46389*t2 + 73845*Power(t2,2) - 
                     20574*Power(t2,3) + 339*Power(t2,4)) + 
                  Power(t1,4)*
                   (29874 - 13948*t2 - 21161*Power(t2,2) + 
                     1232*Power(t2,3) + 3900*Power(t2,4)) + 
                  Power(t1,3)*
                   (27564 + 50391*t2 - 263422*Power(t2,2) + 
                     232496*Power(t2,3) - 50076*Power(t2,4) + 
                     1518*Power(t2,5)) + 
                  Power(t1,2)*
                   (-25536 + 166992*t2 - 374359*Power(t2,2) + 
                     346990*Power(t2,3) - 129252*Power(t2,4) + 
                     15290*Power(t2,5)) + 
                  t1*(-27305 + 118461*t2 - 183749*Power(t2,2) + 
                     122406*Power(t2,3) - 33023*Power(t2,4) + 
                     2696*Power(t2,5) + 600*Power(t2,6)))) + 
            s1*(Power(t1,11) + Power(s2,10)*(1 + 2*t1 - 16*t2) - 
               Power(t1,10)*(21 + 4*t2) + 
               Power(t1,9)*(79 + 89*t2 + 8*Power(t2,2)) - 
               Power(t1,8)*(-99 + 444*t2 + 105*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,7)*(-254 - 238*t2 + 1290*Power(t2,2) - 
                  50*Power(t2,3) + 7*Power(t2,4)) + 
               Power(t1,6)*(-731 + 1220*t2 + 111*Power(t2,2) - 
                  1865*Power(t2,3) + 98*Power(t2,4) - 2*Power(t2,5)) + 
               Power(t1,5)*(203 + 2905*t2 - 6482*Power(t2,2) + 
                  4400*Power(t2,3) - 55*Power(t2,4) + 55*Power(t2,5)) + 
               Power(t1,3)*(-9621 + 40910*t2 - 74842*Power(t2,2) + 
                  69748*Power(t2,3) - 32199*Power(t2,4) + 
                  6666*Power(t2,5) - 368*Power(t2,6)) + 
               Power(t1,4)*(-890 + 9342*t2 - 26096*Power(t2,2) + 
                  28219*Power(t2,3) - 12836*Power(t2,4) + 
                  1459*Power(t2,5) - 66*Power(t2,6)) - 
               Power(-1 + t2,2)*
                (-1101 + 4514*t2 - 8616*Power(t2,2) + 
                  8017*Power(t2,3) - 3448*Power(t2,4) + 
                  880*Power(t2,5) + 32*Power(t2,6)) + 
               t1*(-3240 + 18598*t2 - 36406*Power(t2,2) + 
                  26680*Power(t2,3) + 1625*Power(t2,4) - 
                  11803*Power(t2,5) + 4918*Power(t2,6) - 372*Power(t2,7)\
) + Power(t1,2)*(-12390 + 58866*t2 - 107647*Power(t2,2) + 
                  92009*Power(t2,3) - 33120*Power(t2,4) + 
                  1322*Power(t2,5) + 978*Power(t2,6) - 96*Power(t2,7)) + 
               Power(s2,9)*(19 - 18*Power(t1,2) - 110*t2 + 
                  14*Power(t2,2) + t1*(17 + 126*t2)) + 
               Power(s2,8)*(-31 + 73*Power(t1,3) + 370*t2 - 
                  548*Power(t2,2) + 93*Power(t2,3) - 
                  62*Power(t1,2)*(3 + 7*t2) + 
                  t1*(-188 + 1021*t2 - 154*Power(t2,2))) + 
               Power(s2,7)*(-175 - 176*Power(t1,4) + 1375*t2 - 
                  1456*Power(t2,2) + 272*Power(t2,3) + 80*Power(t2,4) + 
                  Power(t1,3)*(763 + 858*t2) + 
                  Power(t1,2)*(682 - 4059*t2 + 622*Power(t2,2)) + 
                  t1*(29 - 1965*t2 + 3481*Power(t2,2) - 549*Power(t2,3))\
) + Power(s2,6)*(1095 + 280*Power(t1,5) - 6243*t2 + 10435*Power(t2,2) - 
                  6369*Power(t2,3) + 931*Power(t2,4) - 28*Power(t2,5) - 
                  77*Power(t1,4)*(23 + 14*t2) + 
                  Power(t1,3)*(-1129 + 9162*t2 - 1274*Power(t2,2)) + 
                  Power(t1,2)*
                   (442 + 3855*t2 - 9606*Power(t2,2) + 
                     1299*Power(t2,3)) + 
                  t1*(1642 - 9997*t2 + 10860*Power(t2,2) - 
                     2312*Power(t2,3) - 259*Power(t2,4))) + 
               Power(s2,4)*(-4942 + 238*Power(t1,7) + 27428*t2 - 
                  56783*Power(t2,2) + 51382*Power(t2,3) - 
                  19736*Power(t2,4) + 1817*Power(t2,5) - 
                  90*Power(t2,6) - 7*Power(t1,6)*(371 + 78*t2) + 
                  Power(t1,5)*(901 + 12270*t2 - 854*Power(t2,2)) + 
                  5*Power(t1,4)*
                   (564 - 684*t2 - 2977*Power(t2,2) + 149*Power(t2,3)) \
+ Power(t1,3)*(7866 - 44428*t2 + 53666*Power(t2,2) - 
                     10270*Power(t2,3) + 325*Power(t2,4)) + 
                  t1*(7690 - 15409*t2 + 11412*Power(t2,2) - 
                     4490*Power(t2,3) + 5811*Power(t2,4) - 
                     1755*Power(t2,5)) + 
                  Power(t1,2)*
                   (12044 - 66804*t2 + 109427*Power(t2,2) - 
                     71573*Power(t2,3) + 9000*Power(t2,4) - 
                     300*Power(t2,5))) + 
               Power(s2,5)*(-1186 - 308*Power(t1,6) + 1954*t2 - 
                  1595*Power(t2,2) + 1691*Power(t2,3) - 
                  2051*Power(t2,4) + 690*Power(t2,5) + 
                  35*Power(t1,5)*(75 + 26*t2) + 
                  Power(t1,4)*(586 - 13049*t2 + 1442*Power(t2,2)) - 
                  Power(t1,3)*
                   (1659 + 2306*t2 - 15100*Power(t2,2) + 
                     1505*Power(t2,3)) + 
                  Power(t1,2)*
                   (-5260 + 29165*t2 - 32964*Power(t2,2) + 
                     6975*Power(t2,3) + 168*Power(t2,4)) + 
                  t1*(-5865 + 32464*t2 - 53279*Power(t2,2) + 
                     33298*Power(t2,3) - 4568*Power(t2,4) + 
                     147*Power(t2,5))) + 
               Power(s2,2)*(-8720 + 46*Power(t1,9) + 37333*t2 - 
                  55283*Power(t2,2) + 30793*Power(t2,3) - 
                  2085*Power(t2,4) - 1288*Power(t2,5) - 
                  756*Power(t2,6) - 72*Power(t2,7) - 
                  2*Power(t1,8)*(369 + 49*t2) + 
                  Power(t1,7)*(1393 + 3154*t2 + 98*Power(t2,2)) - 
                  Power(t1,6)*
                   (-1726 + 6425*t2 + 3896*Power(t2,2) + 
                     271*Power(t2,3)) + 
                  Power(t1,5)*
                   (1186 - 17633*t2 + 29112*Power(t2,2) - 
                     3312*Power(t2,3) + 375*Power(t2,4)) + 
                  Power(t1,3)*
                   (14149 - 21214*t2 + 2474*Power(t2,2) + 
                     9362*Power(t2,3) + 4508*Power(t2,4) - 
                     384*Power(t2,5)) + 
                  Power(t1,4)*
                   (3444 - 30685*t2 + 58907*Power(t2,2) - 
                     49465*Power(t2,3) + 4715*Power(t2,4) - 
                     150*Power(t2,5)) + 
                  t1*(-31662 + 140971*t2 - 262830*Power(t2,2) + 
                     246996*Power(t2,3) - 117375*Power(t2,4) + 
                     26900*Power(t2,5) - 2166*Power(t2,6)) + 
                  Power(t1,2)*
                   (-8200 + 68589*t2 - 187434*Power(t2,2) + 
                     207471*Power(t2,3) - 96482*Power(t2,4) + 
                     10920*Power(t2,5) - 336*Power(t2,6))) + 
               Power(s2,3)*(12511 - 128*Power(t1,8) - 56523*t2 + 
                  103360*Power(t2,2) - 93584*Power(t2,3) + 
                  43678*Power(t2,4) - 11270*Power(t2,5) + 
                  1558*Power(t2,6) + Power(t1,7)*(1721 + 254*t2) + 
                  Power(t1,6)*(-1822 - 7701*t2 + 154*Power(t2,2)) + 
                  Power(t1,5)*
                   (-2809 + 7695*t2 + 9513*Power(t2,2) + 
                     97*Power(t2,3)) + 
                  Power(t1,4)*
                   (-5579 + 37877*t2 - 51264*Power(t2,2) + 
                     8050*Power(t2,3) - 600*Power(t2,4)) + 
                  Power(t1,3)*
                   (-11161 + 66838*t2 - 113068*Power(t2,2) + 
                     80486*Power(t2,3) - 9000*Power(t2,4) + 
                     300*Power(t2,5)) + 
                  Power(t1,2)*
                   (-16136 + 31265*t2 - 18406*Power(t2,2) + 
                     872*Power(t2,3) - 6896*Power(t2,4) + 
                     1472*Power(t2,5)) + 
                  t1*(10883 - 69873*t2 + 164561*Power(t2,2) - 
                     166317*Power(t2,3) + 71824*Power(t2,4) - 
                     7630*Power(t2,5) + 240*Power(t2,6))) + 
               s2*(327 - 10*Power(t1,10) + 1148*t2 - 17097*Power(t2,2) + 
                  44751*Power(t2,3) - 48449*Power(t2,4) + 
                  24376*Power(t2,5) - 5886*Power(t2,6) + 
                  830*Power(t2,7) + 2*Power(t1,9)*(93 + 14*t2) - 
                  Power(t1,8)*(521 + 777*t2 + 56*Power(t2,2)) + 
                  Power(t1,7)*
                   (-617 + 2640*t2 + 946*Power(t2,2) + 101*Power(t2,3)) \
+ Power(t1,6)*(574 + 3879*t2 - 9244*Power(t2,2) + 647*Power(t2,3) - 
                     96*Power(t2,4)) + 
                  Power(t1,5)*
                   (1174 + 3210*t2 - 12533*Power(t2,2) + 
                     15488*Power(t2,3) - 1176*Power(t2,4) + 
                     33*Power(t2,5)) - 
                  Power(t1,4)*
                   (4720 - 499*t2 - 12597*Power(t2,2) + 
                     11835*Power(t2,3) + 1317*Power(t2,4) + 
                     78*Power(t2,5)) + 
                  Power(t1,3)*
                   (3149 - 35486*t2 + 105752*Power(t2,2) - 
                     120755*Power(t2,3) + 57230*Power(t2,4) - 
                     6566*Power(t2,5) + 252*Power(t2,6)) + 
                  Power(t1,2)*
                   (28934 - 126157*t2 + 235794*Power(t2,2) - 
                     224697*Power(t2,3) + 107248*Power(t2,4) - 
                     23294*Power(t2,5) + 1314*Power(t2,6)) + 
                  t1*(20692 - 95640*t2 + 166313*Power(t2,2) - 
                     131396*Power(t2,3) + 41042*Power(t2,4) + 
                     153*Power(t2,5) - 1074*Power(t2,6) + 66*Power(t2,7))\
))) - s*(-1047 - 1559*t1 + 489*Power(t1,2) + 1201*Power(t1,3) + 
            93*Power(t1,4) - 309*Power(t1,5) - 307*Power(t1,6) + 
            83*Power(t1,7) + 126*Power(t1,8) - 56*Power(t1,9) + 
            6*Power(t1,10) + 6653*t2 + 7662*t1*t2 - 3939*Power(t1,2)*t2 - 
            5189*Power(t1,3)*t2 + 607*Power(t1,4)*t2 + 
            2009*Power(t1,5)*t2 + 541*Power(t1,6)*t2 - 
            595*Power(t1,7)*t2 + 44*Power(t1,8)*t2 + 17*Power(t1,9)*t2 - 
            2*Power(t1,10)*t2 - 2*Power(s2,10)*(-1 + t2)*t2 - 
            18406*Power(t2,2) - 16766*t1*Power(t2,2) + 
            8758*Power(t1,2)*Power(t2,2) + 6601*Power(t1,3)*Power(t2,2) - 
            3928*Power(t1,4)*Power(t2,2) - 3167*Power(t1,5)*Power(t2,2) + 
            666*Power(t1,6)*Power(t2,2) + 335*Power(t1,7)*Power(t2,2) - 
            82*Power(t1,8)*Power(t2,2) + 5*Power(t1,9)*Power(t2,2) + 
            28640*Power(t2,3) + 21996*t1*Power(t2,3) - 
            5268*Power(t1,2)*Power(t2,3) + 717*Power(t1,3)*Power(t2,3) + 
            5756*Power(t1,4)*Power(t2,3) + 552*Power(t1,5)*Power(t2,3) - 
            866*Power(t1,6)*Power(t2,3) + 71*Power(t1,7)*Power(t2,3) + 
            2*Power(t1,8)*Power(t2,3) - 26913*Power(t2,4) - 
            19614*t1*Power(t2,4) - 5820*Power(t1,2)*Power(t2,4) - 
            7178*Power(t1,3)*Power(t2,4) - 1963*Power(t1,4)*Power(t2,4) + 
            1098*Power(t1,5)*Power(t2,4) + 60*Power(t1,6)*Power(t2,4) - 
            14*Power(t1,7)*Power(t2,4) + 15229*Power(t2,5) + 
            12520*t1*Power(t2,5) + 9790*Power(t1,2)*Power(t2,5) + 
            4326*Power(t1,3)*Power(t2,5) - 753*Power(t1,4)*Power(t2,5) - 
            246*Power(t1,5)*Power(t2,5) + 6*Power(t1,6)*Power(t2,5) - 
            4924*Power(t2,6) - 5507*t1*Power(t2,6) - 
            4604*Power(t1,2)*Power(t2,6) - 458*Power(t1,3)*Power(t2,6) + 
            208*Power(t1,4)*Power(t2,6) + 9*Power(t1,5)*Power(t2,6) + 
            862*Power(t2,7) + 1364*t1*Power(t2,7) + 
            608*Power(t1,2)*Power(t2,7) - 20*Power(t1,3)*Power(t2,7) - 
            6*Power(t1,4)*Power(t2,7) - 102*Power(t2,8) - 
            96*t1*Power(t2,8) - 14*Power(t1,2)*Power(t2,8) + 
            8*Power(t2,9) + Power(s1,11)*Power(s2 - t1,2)*
             (-1 + s2 - t1 + t2) + 
            Power(s1,10)*(s2 - t1)*
             (Power(s2,3) + 2*Power(t1,3) + Power(t1,2)*(6 - 10*t2) + 
               Power(s2,2)*(2 - 6*t2) + 2*Power(-1 + t2,2) - 
               s2*(4 + 3*Power(t1,2) + t1*(8 - 16*t2) - 11*t2 + 
                  7*Power(t2,2)) + t1*(5 - 13*t2 + 8*Power(t2,2))) + 
            Power(s2,9)*(-2 + 21*t2 - 17*Power(t2,2) + 13*Power(t2,3) + 
               t1*(-3 - 12*t2 + 11*Power(t2,2))) + 
            Power(s2,8)*(-4 + 4*t2 + 88*Power(t2,2) - 38*Power(t2,3) + 
               5*Power(t2,4) + 
               Power(t1,2)*(24 + 34*t2 - 22*Power(t2,2)) + 
               t1*(7 - 171*t2 + 94*Power(t2,2) - 84*Power(t2,3))) + 
            Power(s2,7)*(38 - 317*t2 + 737*Power(t2,2) - 
               493*Power(t2,3) + 125*Power(t2,4) + 
               2*Power(t1,3)*(-45 - 34*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(25 + 670*t2 - 229*Power(t2,2) + 
                  226*Power(t2,3)) + 
               t1*(59 - 277*t2 - 455*Power(t2,2) + 213*Power(t2,3) - 
                  15*Power(t2,4))) + 
            Power(s2,5)*(-324 + 2265*t2 - 5542*Power(t2,2) + 
               6075*Power(t2,3) - 2671*Power(t2,4) + 236*Power(t2,5) + 
               16*Power(t2,6) - 
               28*Power(t1,5)*(12 + 5*t2 + Power(t2,2)) + 
               Power(t1,4)*(781 + 2388*t2 - 455*Power(t2,2) + 
                  240*Power(t2,3)) + 
               Power(t1,3)*(-81 - 3919*t2 - 863*Power(t2,2) + 
                  963*Power(t2,3) + 85*Power(t2,4)) + 
               t1*(47 - 689*t2 + 2946*Power(t2,2) - 6082*Power(t2,3) + 
                  3457*Power(t2,4) - 229*Power(t2,5)) + 
               Power(t1,2)*(593 - 3453*t2 + 10513*Power(t2,2) - 
                  6792*Power(t2,3) + 1264*Power(t2,4) - 55*Power(t2,5))) \
+ Power(s2,6)*(-43 + 281*t2 - 885*Power(t2,2) + 1376*Power(t2,3) - 
               701*Power(t2,4) + 62*Power(t2,5) + 
               14*Power(t1,4)*(15 + 8*t2 + Power(t2,2)) - 
               Power(t1,3)*(241 + 1588*t2 - 357*Power(t2,2) + 
                  320*Power(t2,3)) + 
               Power(t1,2)*(-158 + 1578*t2 + 938*Power(t2,2) - 
                  566*Power(t2,3) - 7*Power(t2,4)) + 
               t1*(-212 + 1627*t2 - 4217*Power(t2,2) + 
                  2750*Power(t2,3) - 621*Power(t2,4) + 13*Power(t2,5))) + 
            Power(s2,4)*(875 - 4781*t2 + 11652*Power(t2,2) - 
               15476*Power(t2,3) + 11160*Power(t2,4) - 
               3601*Power(t2,5) + 176*Power(t2,6) + 10*Power(t2,7) + 
               14*Power(t1,6)*(27 + 8*t2 + Power(t2,2)) - 
               Power(t1,5)*(1403 + 2246*t2 - 497*Power(t2,2) + 
                  68*Power(t2,3)) - 
               5*Power(t1,4)*
                (-198 - 1068*t2 - 2*Power(t2,2) + 224*Power(t2,3) + 
                  29*Power(t2,4)) + 
               Power(t1,3)*(-929 + 3287*t2 - 14287*Power(t2,2) + 
                  9599*Power(t2,3) - 1360*Power(t2,4) + 90*Power(t2,5)) \
+ Power(t1,2)*(-51 + 671*t2 - 3006*Power(t2,2) + 10678*Power(t2,3) - 
                  7302*Power(t2,4) + 410*Power(t2,5)) + 
               t1*(1100 - 7847*t2 + 20008*Power(t2,2) - 
                  23336*Power(t2,3) + 10620*Power(t2,4) - 
                  798*Power(t2,5) - 21*Power(t2,6))) + 
            Power(s2,3)*(366 - 2747*t2 + 7865*Power(t2,2) - 
               10325*Power(t2,3) + 5923*Power(t2,4) - 580*Power(t2,5) - 
               560*Power(t2,6) + 58*Power(t2,7) + 
               2*Power(t1,7)*(-147 - 22*t2 + Power(t2,2)) + 
               Power(t1,6)*(1523 + 1226*t2 - 399*Power(t2,2) - 
                  26*Power(t2,3)) + 
               Power(t1,5)*(-1831 - 4259*t2 + 827*Power(t2,2) + 
                  843*Power(t2,3) + 115*Power(t2,4)) + 
               Power(t1,4)*(696 - 63*t2 + 10563*Power(t2,2) - 
                  8221*Power(t2,3) + 845*Power(t2,4) - 70*Power(t2,5)) - 
               2*Power(t1,3)*
                (-304 + 615*t2 + 237*Power(t2,2) + 4218*Power(t2,3) - 
                  4057*Power(t2,4) + 241*Power(t2,5)) + 
               Power(t1,2)*(-1369 + 9392*t2 - 26131*Power(t2,2) + 
                  35076*Power(t2,3) - 17632*Power(t2,4) + 
                  1200*Power(t2,5) + 10*Power(t2,6)) - 
               t1*(2963 - 15997*t2 + 37477*Power(t2,2) - 
                  48593*Power(t2,3) + 35769*Power(t2,4) - 
                  12246*Power(t2,5) + 646*Power(t2,6) + 40*Power(t2,7))) \
- Power(s2,2)*(3077 - 17713*t2 + 45591*Power(t2,2) - 65030*Power(t2,3) + 
               52999*Power(t2,4) - 23238*Power(t2,5) + 
               4316*Power(t2,6) + 28*Power(t2,7) - 30*Power(t2,8) + 
               2*Power(t1,8)*(-75 + t2 + 2*Power(t2,2)) + 
               Power(t1,7)*(995 + 300*t2 - 199*Power(t2,2) - 
                  24*Power(t2,3)) + 
               Power(t1,6)*(-1606 - 1962*t2 + 890*Power(t2,2) + 
                  358*Power(t2,3) + 45*Power(t2,4)) + 
               Power(t1,5)*(62 + 2751*t2 + 3319*Power(t2,2) - 
                  4092*Power(t2,3) + 325*Power(t2,4) - 25*Power(t2,5)) - 
               Power(t1,4)*(-1343 + 2339*t2 + 3273*Power(t2,2) + 
                  1420*Power(t2,3) - 4585*Power(t2,4) + 346*Power(t2,5)) \
+ Power(t1,3)*(-425 + 2294*t2 - 11239*Power(t2,2) + 23892*Power(t2,3) - 
                  15186*Power(t2,4) + 1196*Power(t2,5) + 12*Power(t2,6)) \
- Power(t1,2)*(3592 - 18156*t2 + 38676*Power(t2,2) - 
                  48366*Power(t2,3) + 38727*Power(t2,4) - 
                  15686*Power(t2,5) + 1276*Power(t2,6) + 24*Power(t2,7)) \
+ t1*(-1560 + 7119*t2 - 11537*Power(t2,2) + 8214*Power(t2,3) - 
                  3515*Power(t2,4) + 2911*Power(t2,5) - 
                  1740*Power(t2,6) + 108*Power(t2,7))) + 
            s2*(Power(t1,9)*(-45 + 8*t2 + Power(t2,2)) - 
               Power(t1,8)*(-361 + 17*t2 + 52*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,7)*(-707 - 473*t2 + 427*Power(t2,2) + 
                  61*Power(t2,3) + 7*Power(t2,4)) + 
               Power(t1,5)*(1089 - 1913*t2 - 2520*Power(t2,2) + 
                  1910*Power(t2,3) + 957*Power(t2,4) - 113*Power(t2,5)) \
- Power(t1,6)*(207 - 2265*t2 + 325*Power(t2,2) + 1006*Power(t2,3) - 
                  86*Power(t2,4) + 3*Power(t2,5)) + 
               Power(t1,4)*(477 - 3525*t2 + 3593*Power(t2,2) + 
                  5525*Power(t2,3) - 6601*Power(t2,4) + 
                  804*Power(t2,5) - 2*Power(t2,6)) + 
               Power(-1 + t2,2)*
                (3218 - 12658*t2 + 21567*Power(t2,2) - 
                  18466*Power(t2,3) + 7152*Power(t2,4) - 
                  906*Power(t2,5) + 4*Power(t2,6)) + 
               Power(t1,3)*(-1597 + 6333*t2 - 8923*Power(t2,2) + 
                  9493*Power(t2,3) - 12155*Power(t2,4) + 
                  7794*Power(t2,5) - 1014*Power(t2,6) + 12*Power(t2,7)) \
+ Power(t1,2)*(-3145 + 15155*t2 - 26253*Power(t2,2) + 
                  18272*Power(t2,3) - 2940*Power(t2,4) - 
                  117*Power(t2,5) - 1142*Power(t2,6) + 170*Power(t2,7)) \
+ t1*(1964 - 9171*t2 + 22527*Power(t2,2) - 35896*Power(t2,3) + 
                  36243*Power(t2,4) - 21353*Power(t2,5) + 
                  6090*Power(t2,6) - 364*Power(t2,7) - 40*Power(t2,8))) + 
            Power(s1,9)*(-36*Power(s2,5) + 59*Power(t1,5) + 
               Power(t1,4)*(191 - 116*t2) + 
               3*Power(s2,4)*(47 + 68*t1 - 30*t2) + Power(-1 + t2,3) + 
               2*t1*Power(-1 + t2,2)*(-3 + 8*t2) + 
               2*Power(t1,3)*(86 - 112*t2 + 23*Power(t2,2)) - 
               Power(s2,3)*(142 + 618*t1 + 455*Power(t1,2) - 192*t2 - 
                  390*t1*t2 + 44*Power(t2,2)) + 
               Power(t1,2)*(31 - 62*t2 + 20*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(s2,2)*(35 + 501*Power(t1,3) + 
                  Power(t1,2)*(1004 - 626*t2) - 71*t2 + 
                  26*Power(t2,2) + 10*Power(t2,3) + 
                  t1*(463 - 622*t2 + 141*Power(t2,2))) - 
               s2*(273*Power(t1,4) + Power(t1,3)*(718 - 442*t2) + 
                  5*Power(-1 + t2,2)*(-1 + 3*t2) + 
                  Power(t1,2)*(493 - 654*t2 + 143*Power(t2,2)) + 
                  t1*(70 - 145*t2 + 58*Power(t2,2) + 17*Power(t2,3)))) - 
            Power(s1,8)*(158*Power(s2,6) + 169*Power(t1,6) + 
               Power(t1,5)*(584 - 205*t2) + 
               2*Power(-1 + t2,3)*(-1 + 4*t2) + 
               Power(s2,5)*(-661 - 987*t1 + 292*t2) + 
               Power(t1,4)*(475 - 51*t2 - 89*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-80 + 97*t2 + 22*Power(t2,2)) + 
               Power(s2,4)*(684 + 2538*Power(t1,2) + 
                  t1*(3340 - 1443*t2) - 521*t2 + 54*Power(t2,2)) + 
               Power(t1,3)*(-266 + 959*t2 - 834*Power(t2,2) + 
                  127*Power(t2,3)) + 
               Power(t1,2)*(-399 + 1051*t2 - 984*Power(t2,2) + 
                  334*Power(t2,3) - 2*Power(t2,4)) - 
               Power(s2,3)*(-50 + 3441*Power(t1,3) + 
                  Power(t1,2)*(6637 - 2781*t2) + 361*t2 - 
                  405*Power(t2,2) + 80*Power(t2,3) + 
                  t1*(2669 - 1826*t2 + 143*Power(t2,2))) + 
               Power(s2,2)*(-291 + 2595*Power(t1,4) + 
                  Power(t1,3)*(6482 - 2606*t2) + 805*t2 - 
                  814*Power(t2,2) + 300*Power(t2,3) + 
                  6*Power(t1,2)*(626 - 355*t2 + 5*Power(t2,2)) + 
                  t1*(-303 + 1531*t2 - 1533*Power(t2,2) + 
                     263*Power(t2,3))) + 
               s2*(-1032*Power(t1,5) + Power(t1,4)*(-3108 + 1181*t2) - 
                  Power(-1 + t2,2)*(-76 + 98*t2 + 17*Power(t2,2)) + 
                  2*Power(t1,3)*(-1123 + 438*t2 + 74*Power(t2,2)) + 
                  Power(t1,2)*
                   (522 - 2138*t2 + 1971*Power(t2,2) - 313*Power(t2,3)) \
+ 2*t1*(338 - 909*t2 + 884*Power(t2,2) - 316*Power(t2,3) + 3*Power(t2,4))\
)) + Power(s1,7)*(-287*Power(s2,7) + 130*Power(t1,7) + 
               Power(s2,6)*(1146 + 1947*t1 - 379*t2) + 
               Power(t1,6)*(236 + 183*t2) + 
               Power(t1,5)*(-849 + 2354*t2 - 699*Power(t2,2)) - 
               Power(s2,5)*(691 + 5553*Power(t1,2) + 
                  t1*(6284 - 1872*t2) + 319*t2 - 181*Power(t2,2)) + 
               Power(-1 + t2,3)*(-39 + 56*t2 + 11*Power(t2,2)) - 
               2*t1*Power(-1 + t2,2)*
                (-107 + 287*t2 - 264*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(-2979 + 6364*t2 - 3192*Power(t2,2) + 
                  374*Power(t2,3)) + 
               Power(t1,3)*(-2811 + 5723*t2 - 3217*Power(t2,2) + 
                  264*Power(t2,3) + 12*Power(t2,4)) + 
               Power(t1,2)*(-548 + 371*t2 + 1120*Power(t2,2) - 
                  1410*Power(t2,3) + 467*Power(t2,4)) + 
               Power(s2,4)*(-1851 + 8612*Power(t1,3) + 
                  Power(t1,2)*(13858 - 3477*t2) + 4217*t2 - 
                  2386*Power(t2,2) + 281*Power(t2,3) + 
                  t1*(2252 + 3454*t2 - 1369*Power(t2,2))) + 
               Power(s2,3)*(2356 - 7813*Power(t1,4) - 5129*t2 + 
                  3632*Power(t2,2) - 838*Power(t2,3) + 8*Power(t2,4) + 
                  2*Power(t1,3)*(-7783 + 1403*t2) + 
                  Power(t1,2)*(-1677 - 10934*t2 + 3768*Power(t2,2)) + 
                  t1*(8516 - 19314*t2 + 10717*Power(t2,2) - 
                     1269*Power(t2,3))) + 
               Power(s2,2)*(-520 + 4119*Power(t1,5) + 
                  Power(t1,4)*(9200 - 591*t2) + 860*t2 - 
                  54*Power(t2,2) - 641*Power(t2,3) + 355*Power(t2,4) + 
                  Power(t1,3)*(-1487 + 15136*t2 - 4852*Power(t2,2)) + 
                  Power(t1,2)*
                   (-14495 + 32425*t2 - 17525*Power(t2,2) + 
                     2079*Power(t2,3)) + 
                  t1*(-7669 + 16454*t2 - 11030*Power(t2,2) + 
                     2203*Power(t2,3) - 45*Power(t2,4))) - 
               s2*(1155*Power(t1,6) + Power(t1,5)*(2590 + 414*t2) + 
                  Power(t1,4)*(-2452 + 9691*t2 - 2971*Power(t2,2)) - 
                  Power(-1 + t2,2)*
                   (-160 + 467*t2 - 477*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,3)*
                   (-10809 + 23692*t2 - 12386*Power(t2,2) + 
                     1465*Power(t2,3)) + 
                  Power(t1,2)*
                   (-8128 + 17053*t2 - 10606*Power(t2,2) + 
                     1612*Power(t2,3) - 18*Power(t2,4)) + 
                  t1*(-1112 + 1421*t2 + 760*Power(t2,2) - 
                     1833*Power(t2,3) + 764*Power(t2,4)))) + 
            Power(s1,6)*(-259*Power(s2,8) + 64*Power(t1,8) + 
               Power(t1,7)*(493 - 700*t2) + 
               Power(s2,7)*(830 + 1824*t1 - 89*t2) + 
               Power(t1,6)*(1942 - 3431*t2 + 1074*Power(t2,2)) + 
               Power(t1,5)*(3650 - 5176*t2 + 1566*Power(t2,2) - 
                  358*Power(t2,3)) + 
               2*Power(-1 + t2,3)*
                (-5 + 72*t2 - 121*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (797 - 1100*t2 + 186*Power(t2,2) + 553*Power(t2,3)) + 
               Power(t1,3)*(-3357 + 16336*t2 - 23228*Power(t2,2) + 
                  11944*Power(t2,3) - 1655*Power(t2,4)) + 
               Power(t1,4)*(1559 + 3001*t2 - 8141*Power(t2,2) + 
                  2948*Power(t2,3) - 80*Power(t2,4)) - 
               Power(t1,2)*(3686 - 14097*t2 + 18817*Power(t2,2) - 
                  10529*Power(t2,3) + 2073*Power(t2,4) + 50*Power(t2,5)) \
- Power(s2,6)*(-834 + 5395*Power(t1,2) + 2436*t2 - 695*Power(t2,2) + 
                  t1*(4469 + 270*t2)) + 
               Power(s2,5)*(-5042 + 8616*Power(t1,3) + 9063*t2 - 
                  4066*Power(t2,2) + 565*Power(t2,3) + 
                  Power(t1,2)*(9215 + 3523*t2) + 
                  t1*(-6875 + 17125*t2 - 4986*Power(t2,2))) + 
               Power(s2,4)*(4064 - 7865*Power(t1,4) - 5514*t2 + 
                  1073*Power(t2,2) + 133*Power(t2,3) + 40*Power(t2,4) - 
                  Power(t1,3)*(8277 + 10312*t2) + 
                  Power(t1,2)*(21321 - 47803*t2 + 14166*Power(t2,2)) + 
                  t1*(25398 - 45318*t2 + 19962*Power(t2,2) - 
                     2980*Power(t2,3))) + 
               Power(s2,3)*(1817 + 3864*Power(t1,5) - 9624*t2 + 
                  14089*Power(t2,2) - 7847*Power(t2,3) + 
                  1525*Power(t2,4) + Power(t1,4)*(1288 + 14533*t2) + 
                  Power(t1,3)*(-32995 + 68450*t2 - 20606*Power(t2,2)) + 
                  Power(t1,2)*
                   (-49587 + 86589*t2 - 36864*Power(t2,2) + 
                     5874*Power(t2,3)) + 
                  t1*(-15075 + 17045*t2 + 2300*Power(t2,2) - 
                     2801*Power(t2,3) - 144*Power(t2,4))) + 
               Power(s2,2)*(-2903 - 689*Power(t1,6) + 
                  Power(t1,5)*(3253 - 11038*t2) + 11029*t2 - 
                  15580*Power(t2,2) + 10195*Power(t2,3) - 
                  2759*Power(t2,4) + 18*Power(t2,5) + 
                  Power(t1,4)*(27299 - 53242*t2 + 16257*Power(t2,2)) + 
                  Power(t1,3)*
                   (46798 - 78652*t2 + 31672*Power(t2,2) - 
                     5426*Power(t2,3)) + 
                  t1*(-6573 + 34661*t2 - 50765*Power(t2,2) + 
                     27453*Power(t2,3) - 4656*Power(t2,4)) + 
                  Power(t1,2)*
                   (19384 - 14101*t2 - 16493*Power(t2,2) + 
                     8411*Power(t2,3) + 48*Power(t2,4))) - 
               s2*(160*Power(t1,7) + Power(t1,6)*(2333 - 4353*t2) + 
                  Power(t1,5)*(11526 - 21337*t2 + 6600*Power(t2,2)) + 
                  Power(t1,4)*
                   (21217 - 33494*t2 + 12270*Power(t2,2) - 
                     2325*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (626 - 1055*t2 + 414*Power(t2,2) + 451*Power(t2,3)) + 
                  Power(t1,2)*
                   (-8165 + 41582*t2 - 60219*Power(t2,2) + 
                     31761*Power(t2,3) - 4839*Power(t2,4)) + 
                  Power(t1,3)*
                   (9932 + 431*t2 - 21261*Power(t2,2) + 
                     8691*Power(t2,3) - 136*Power(t2,4)) + 
                  t1*(-6556 + 25062*t2 - 34449*Power(t2,2) + 
                     20934*Power(t2,3) - 5005*Power(t2,4) + 
                     14*Power(t2,5)))) + 
            Power(s1,5)*(-102*Power(s2,9) - 121*Power(t1,9) + 
               Power(s2,8)*(159 + 638*t1 + 292*t2) + 
               Power(t1,8)*(-373 + 550*t2) - 
               Power(s2,7)*(-1548 + 242*t1 + 1415*Power(t1,2) + 
                  2856*t2 + 3126*t1*t2 - 990*Power(t2,2)) + 
               Power(t1,7)*(98 + 966*t2 - 584*Power(t2,2)) + 
               Power(t1,6)*(1265 - 5180*t2 + 2712*Power(t2,2) + 
                  31*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (197 - 585*t2 + 411*Power(t2,2) + 213*Power(t2,3)) + 
               2*t1*Power(-1 + t2,2)*
                (-880 + 3226*t2 - 3527*Power(t2,2) + 1552*Power(t2,3) + 
                  32*Power(t2,4)) + 
               Power(t1,5)*(4747 - 19480*t2 + 20193*Power(t2,2) - 
                  5128*Power(t2,3) + 124*Power(t2,4)) + 
               Power(t1,4)*(10142 - 31400*t2 + 32410*Power(t2,2) - 
                  12033*Power(t2,3) + 1570*Power(t2,4)) + 
               Power(t1,3)*(8028 - 18578*t2 + 7545*Power(t2,2) + 
                  7131*Power(t2,3) - 4487*Power(t2,4) + 318*Power(t2,5)) \
+ Power(t1,2)*(203 + 7946*t2 - 28579*Power(t2,2) + 34847*Power(t2,3) - 
                  17353*Power(t2,4) + 2936*Power(t2,5)) + 
               Power(s2,6)*(-3965 + 683*Power(t1,3) + 5337*t2 - 
                  1999*Power(t2,2) + 668*Power(t2,3) + 
                  2*Power(t1,2)*(-1199 + 6659*t2) + 
                  t1*(-10695 + 20230*t2 - 7393*Power(t2,2))) + 
               Power(s2,5)*(-1213 + 2741*Power(t1,4) + 9356*t2 - 
                  12004*Power(t2,2) + 3219*Power(t2,3) + 
                  72*Power(t2,4) - 22*Power(t1,3)*(-474 + 1385*t2) + 
                  7*Power(t1,2)*(4285 - 8424*t2 + 3241*Power(t2,2)) + 
                  t1*(19872 - 23866*t2 + 8156*Power(t2,2) - 
                     3831*Power(t2,3))) + 
               Power(s2,3)*(-8057 + 6227*Power(t1,6) + 
                  Power(t1,5)*(18566 - 34802*t2) + 22708*t2 - 
                  21396*Power(t2,2) + 8685*Power(t2,3) - 
                  1949*Power(t2,4) + 52*Power(t2,5) + 
                  Power(t1,4)*(35338 - 81592*t2 + 35148*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-16338 + 5237*t2 + 2479*Power(t2,2) + 
                     4635*Power(t2,3)) + 
                  t1*(-43890 + 141407*t2 - 156472*Power(t2,2) + 
                     69264*Power(t2,3) - 11199*Power(t2,4)) + 
                  Power(t1,2)*
                   (-29544 + 146224*t2 - 169803*Power(t2,2) + 
                     45396*Power(t2,3) + 56*Power(t2,4))) - 
               Power(s2,4)*(-10507 + 6233*Power(t1,5) + 
                  Power(t1,4)*(18830 - 41550*t2) + 33723*t2 - 
                  37312*Power(t2,2) + 17012*Power(t2,3) - 
                  2983*Power(t2,4) + 
                  Power(t1,3)*(43892 - 91658*t2 + 37202*Power(t2,2)) + 
                  Power(t1,2)*
                   (37841 - 35160*t2 + 8900*Power(t2,2) - 
                     8515*Power(t2,3)) + 
                  t1*(-10433 + 60262*t2 - 73272*Power(t2,2) + 
                     19668*Power(t2,3) + 230*Power(t2,4))) + 
               Power(s2,2)*(-328 - 3415*Power(t1,7) + 6804*t2 - 
                  19989*Power(t2,2) + 23132*Power(t2,3) - 
                  12470*Power(t2,4) + 2851*Power(t2,5) + 
                  2*Power(t1,6)*(-5199 + 8753*t2) + 
                  Power(t1,5)*(-14695 + 41034*t2 - 18997*Power(t2,2)) + 
                  Power(t1,4)*
                   (-10227 - 20933*t2 + 16699*Power(t2,2) + 
                     5010*Power(t2,3)) + 
                  Power(t1,3)*
                   (37388 - 168566*t2 + 187999*Power(t2,2) - 
                     49792*Power(t2,3) + 414*Power(t2,4)) + 
                  Power(t1,2)*
                   (66691 - 213793*t2 + 234081*Power(t2,2) - 
                     99767*Power(t2,3) + 15056*Power(t2,4)) + 
                  t1*(25556 - 69964*t2 + 58908*Power(t2,2) - 
                     15350*Power(t2,3) + 730*Power(t2,4) - 9*Power(t2,5)\
)) + s2*(997*Power(t1,8) + Power(t1,7)*(3088 - 4818*t2) + 
                  Power(t1,6)*(2303 - 10472*t2 + 5351*Power(t2,2)) - 
                  Power(t1,5)*
                   (1780 - 19956*t2 + 11710*Power(t2,2) + 
                     1123*Power(t2,3)) + 
                  Power(t1,3)*
                   (-43450 + 137509*t2 - 147331*Power(t2,2) + 
                     59548*Power(t2,3) - 8410*Power(t2,4)) + 
                  Power(t1,4)*
                   (-21811 + 92728*t2 - 99657*Power(t2,2) + 
                     25973*Power(t2,3) - 436*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (-1654 + 5488*t2 - 6231*Power(t2,2) + 
                     3199*Power(t2,3) + 4*Power(t2,4)) - 
                  Power(t1,2)*
                   (25553 - 65848*t2 + 44888*Power(t2,2) + 
                     797*Power(t2,3) - 5935*Power(t2,4) + 
                     416*Power(t2,5)) - 
                  t1*(120 + 13673*t2 - 46674*Power(t2,2) + 
                     56309*Power(t2,3) - 29082*Power(t2,4) + 
                     5654*Power(t2,5)))) - 
            Power(s1,4)*(-43*Power(t1,10) + 
               Power(s2,9)*(47 + 137*t1 - 314*t2) + 
               Power(t1,9)*(-50 + 139*t2) + 
               Power(t1,8)*(785 - 357*t2 - 77*Power(t2,2)) - 
               Power(s2,8)*(530 + 1182*Power(t1,2) + 
                  t1*(704 - 3085*t2) - 1067*t2 + 692*Power(t2,2)) + 
               Power(t1,7)*(710 - 4555*t2 + 2306*Power(t2,2) - 
                  85*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-387 + 1214*t2 - 1825*Power(t2,2) + 1334*Power(t2,3) + 
                  26*Power(t2,4)) + 
               Power(t1,6)*(-310 - 3875*t2 + 8234*Power(t2,2) - 
                  2426*Power(t2,3) + 66*Power(t2,4)) + 
               Power(t1,5)*(1874 + 5499*t2 - 11271*Power(t2,2) + 
                  3111*Power(t2,3) + 68*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-1517 + 1103*t2 + 5275*Power(t2,2) - 
                  6182*Power(t2,3) + 2403*Power(t2,4)) + 
               Power(t1,4)*(-753 + 24643*t2 - 60649*Power(t2,2) + 
                  46796*Power(t2,3) - 10142*Power(t2,4) + 
                  472*Power(t2,5)) + 
               Power(t1,3)*(-9813 + 50128*t2 - 94401*Power(t2,2) + 
                  78978*Power(t2,3) - 27572*Power(t2,4) + 
                  2698*Power(t2,5)) + 
               2*Power(t1,2)*
                (-4431 + 18936*t2 - 29393*Power(t2,2) + 
                  19096*Power(t2,3) - 3488*Power(t2,4) - 
                  971*Power(t2,5) + 251*Power(t2,6)) + 
               Power(s2,7)*(446 + 4481*Power(t1,3) + 
                  Power(t1,2)*(3633 - 12959*t2) + 1709*t2 - 
                  1367*Power(t2,2) - 434*Power(t2,3) + 
                  t1*(2955 - 7328*t2 + 5413*Power(t2,2))) + 
               Power(s2,6)*(4561 - 9779*Power(t1,4) - 15137*t2 + 
                  16582*Power(t2,2) - 4862*Power(t2,3) - 
                  56*Power(t2,4) + 4*Power(t1,3)*(-2424 + 7673*t2) - 
                  2*Power(t1,2)*(2678 - 10295*t2 + 8938*Power(t2,2)) + 
                  t1*(271 - 19213*t2 + 12895*Power(t2,2) + 
                     2619*Power(t2,3))) + 
               Power(s2,5)*(-9017 + 13503*Power(t1,5) + 
                  Power(t1,4)*(15385 - 45209*t2) + 21895*t2 - 
                  20726*Power(t2,2) + 11583*Power(t2,3) - 
                  3155*Power(t2,4) + 
                  Power(t1,3)*(220 - 30018*t2 + 32482*Power(t2,2)) - 
                  Power(t1,2)*
                   (9486 - 76424*t2 + 47063*Power(t2,2) + 
                     6327*Power(t2,3)) + 
                  t1*(-27776 + 93014*t2 - 103147*Power(t2,2) + 
                     30198*Power(t2,3) + 162*Power(t2,4))) + 
               Power(s2,4)*(-1002 - 12187*Power(t1,6) + 16495*t2 - 
                  39170*Power(t2,2) + 28862*Power(t2,3) - 
                  5199*Power(t2,4) - 100*Power(t2,5) + 
                  2*Power(t1,5)*(-7641 + 21422*t2) - 
                  5*Power(t1,4)*(-2583 - 4606*t2 + 7057*Power(t2,2)) + 
                  Power(t1,3)*
                   (27024 - 152301*t2 + 88948*Power(t2,2) + 
                     7713*Power(t2,3)) + 
                  Power(t1,2)*
                   (65345 - 224357*t2 + 254392*Power(t2,2) - 
                     74550*Power(t2,3) + 30*Power(t2,4)) + 
                  t1*(42579 - 96931*t2 + 87288*Power(t2,2) - 
                     48745*Power(t2,3) + 13492*Power(t2,4))) + 
               Power(s2,3)*(13082 + 7147*Power(t1,7) + 
                  Power(t1,6)*(9519 - 25945*t2) - 58926*t2 + 
                  102374*Power(t2,2) - 85103*Power(t2,3) + 
                  34239*Power(t2,4) - 5684*Power(t2,5) + 
                  Power(t1,5)*(-20485 - 7012*t2 + 23137*Power(t2,2)) - 
                  Power(t1,4)*
                   (34266 - 169409*t2 + 95217*Power(t2,2) + 
                     4772*Power(t2,3)) + 
                  Power(t1,2)*
                   (-74842 + 150855*t2 - 121839*Power(t2,2) + 
                     71344*Power(t2,3) - 21328*Power(t2,4)) + 
                  Power(t1,3)*
                   (-74679 + 270232*t2 - 318348*Power(t2,2) + 
                     93508*Power(t2,3) - 568*Power(t2,4)) + 
                  t1*(2574 - 70623*t2 + 177372*Power(t2,2) - 
                     137275*Power(t2,3) + 27879*Power(t2,4) + 
                     48*Power(t2,5))) + 
               Power(s2,2)*(-8670 - 2601*Power(t1,8) + 36730*t2 - 
                  59717*Power(t2,2) + 48404*Power(t2,3) - 
                  21399*Power(t2,4) + 4550*Power(t2,5) + 
                  102*Power(t2,6) + 4*Power(t1,7)*(-887 + 2386*t2) - 
                  2*Power(t1,6)*(-7445 + 901*t2 + 4339*Power(t2,2)) + 
                  Power(t1,5)*
                   (22083 - 106355*t2 + 57875*Power(t2,2) + 
                     1137*Power(t2,3)) + 
                  Power(t1,4)*
                   (41312 - 167639*t2 + 211432*Power(t2,2) - 
                     62274*Power(t2,3) + 744*Power(t2,4)) + 
                  Power(t1,3)*
                   (59889 - 92998*t2 + 53447*Power(t2,2) - 
                     39674*Power(t2,3) + 14868*Power(t2,4)) + 
                  Power(t1,2)*
                   (-3568 + 119398*t2 - 302668*Power(t2,2) + 
                     238216*Power(t2,3) - 51471*Power(t2,4) + 
                     852*Power(t2,5)) + 
                  t1*(-36625 + 172350*t2 - 310180*Power(t2,2) + 
                     260987*Power(t2,3) - 100803*Power(t2,4) + 
                     14325*Power(t2,5))) + 
               s2*(524*Power(t1,9) + Power(t1,8)*(696 - 1877*t2) + 
                  2*Power(t1,7)*(-2697 + 915*t2 + 788*Power(t2,2)) + 
                  Power(t1,6)*
                   (-6782 + 34882*t2 - 18377*Power(t2,2) + 
                     149*Power(t2,3)) + 
                  Power(t1,4)*
                   (-20483 + 11680*t2 + 13101*Power(t2,2) + 
                     2381*Power(t2,3) - 3945*Power(t2,4)) + 
                  Power(t1,5)*
                   (-8453 + 47762*t2 - 69145*Power(t2,2) + 
                     20406*Power(t2,3) - 378*Power(t2,4)) - 
                  2*Power(-1 + t2,2)*
                   (-348 - 229*t2 + 2235*Power(t2,2) - 
                     2257*Power(t2,3) + 1140*Power(t2,4)) + 
                  Power(t1,2)*
                   (33545 - 164479*t2 + 304000*Power(t2,2) - 
                     256569*Power(t2,3) + 94934*Power(t2,4) - 
                     11485*Power(t2,5)) + 
                  Power(t1,3)*
                   (2749 - 89913*t2 + 225115*Power(t2,2) - 
                     176599*Power(t2,3) + 38933*Power(t2,4) - 
                     1272*Power(t2,5)) + 
                  t1*(17926 - 76835*t2 + 123309*Power(t2,2) - 
                     91680*Power(t2,3) + 31211*Power(t2,4) - 
                     3451*Power(t2,5) - 480*Power(t2,6)))) + 
            Power(s1,3)*(8*Power(s2,11) - 3*Power(t1,11) + 
               Power(t1,10)*(3 + 6*t2) + 
               Power(s2,10)*(-1 - 114*t1 + 112*t2) + 
               Power(t1,9)*(173 - 136*t2 + 5*Power(t2,2)) + 
               Power(s2,9)*(-31 + 676*Power(t1,2) + t1*(22 - 1110*t2) + 
                  33*t2 + 217*Power(t2,2)) - 
               Power(t1,8)*(319 + 682*t2 - 468*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,6)*(1270 + 6782*t2 - 11990*Power(t2,2) + 
                  4042*Power(t2,3) - 293*Power(t2,4)) + 
               Power(t1,7)*(-1303 + 2575*t2 + 99*Power(t2,2) - 
                  264*Power(t2,3) + 8*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (-397 + 848*t2 - 371*Power(t2,2) - 348*Power(t2,3) + 
                  748*Power(t2,4)) + 
               Power(t1,4)*(-9282 + 17829*t2 - 9220*Power(t2,2) + 
                  1122*Power(t2,3) - 539*Power(t2,4) + 47*Power(t2,5)) + 
               2*t1*Power(-1 + t2,2)*
                (2103 - 7957*t2 + 10702*Power(t2,2) - 
                  5599*Power(t2,3) + 982*Power(t2,4) + 185*Power(t2,5)) \
+ Power(t1,5)*(581 + 5870*t2 - 22931*Power(t2,2) + 19816*Power(t2,3) - 
                  4238*Power(t2,4) + 222*Power(t2,5)) + 
               Power(t1,3)*(-8694 + 12353*t2 + 30485*Power(t2,2) - 
                  72782*Power(t2,3) + 48422*Power(t2,4) - 
                  10322*Power(t2,5) + 660*Power(t2,6)) + 
               Power(t1,2)*(2891 - 25240*t2 + 80436*Power(t2,2) - 
                  119372*Power(t2,3) + 86485*Power(t2,4) - 
                  27578*Power(t2,5) + 2378*Power(t2,6)) + 
               Power(s2,8)*(211 - 2267*Power(t1,3) - 1973*t2 + 
                  1572*Power(t2,2) + 129*Power(t2,3) + 
                  3*Power(t1,2)*(-35 + 1608*t2) + 
                  t1*(852 - 862*t2 - 1755*Power(t2,2))) + 
               Power(s2,7)*(-1479 + 4840*Power(t1,4) + 
                  Power(t1,3)*(200 - 12102*t2) + 4377*t2 - 
                  6612*Power(t2,2) + 2884*Power(t2,3) + 
                  16*Power(t2,4) + 
                  Power(t1,2)*(-5409 + 5428*t2 + 6128*Power(t2,2)) - 
                  t1*(2436 - 17426*t2 + 13181*Power(t2,2) + 
                     779*Power(t2,3))) + 
               Power(s2,6)*(586 - 6944*Power(t1,5) + 4539*t2 - 
                  8904*Power(t2,2) + 891*Power(t2,3) + 
                  1687*Power(t2,4) + 14*Power(t1,4)*(-7 + 1383*t2) + 
                  Power(t1,3)*(16471 - 16450*t2 - 12060*Power(t2,2)) + 
                  Power(t1,2)*
                   (9705 - 63549*t2 + 46431*Power(t2,2) + 
                     1915*Power(t2,3)) + 
                  t1*(8112 - 25682*t2 + 42858*Power(t2,2) - 
                     18915*Power(t2,3) - 21*Power(t2,4))) + 
               Power(s2,5)*(9029 + 6832*Power(t1,6) - 35758*t2 + 
                  60032*Power(t2,2) - 42197*Power(t2,3) + 
                  9017*Power(t2,4) + 54*Power(t2,5) - 
                  126*Power(t1,5)*(2 + 163*t2) + 
                  Power(t1,4)*(-29043 + 28826*t2 + 14610*Power(t2,2)) - 
                  Power(t1,3)*
                   (19051 - 126092*t2 + 89928*Power(t2,2) + 
                     2389*Power(t2,3)) + 
                  Power(t1,2)*
                   (-16085 + 57607*t2 - 113188*Power(t2,2) + 
                     51086*Power(t2,3) - 138*Power(t2,4)) - 
                  t1*(856 + 38599*t2 - 66406*Power(t2,2) + 
                     12507*Power(t2,3) + 7848*Power(t2,4))) + 
               Power(s2,3)*(2631 + 2032*Power(t1,8) + 5497*t2 - 
                  43876*Power(t2,2) + 64128*Power(t2,3) - 
                  32424*Power(t2,4) + 4018*Power(t2,5) - 
                  96*Power(t2,6) - 2*Power(t1,7)*(220 + 3297*t2) + 
                  Power(t1,6)*(-21707 + 20972*t2 + 5160*Power(t2,2)) - 
                  Power(t1,5)*
                   (10826 - 106738*t2 + 73917*Power(t2,2) + 
                     189*Power(t2,3)) + 
                  Power(t1,4)*
                   (4387 + 18667*t2 - 119904*Power(t2,2) + 
                     59052*Power(t2,3) - 560*Power(t2,4)) - 
                  2*Power(t1,3)*
                   (-2858 + 90610*t2 - 140710*Power(t2,2) + 
                     39011*Power(t2,3) + 5695*Power(t2,4)) + 
                  t1*(59916 - 184660*t2 + 240841*Power(t2,2) - 
                     183825*Power(t2,3) + 84346*Power(t2,4) - 
                     16068*Power(t2,5)) + 
                  Power(t1,2)*
                   (72971 - 308912*t2 + 552872*Power(t2,2) - 
                     399390*Power(t2,3) + 86034*Power(t2,4) - 
                     816*Power(t2,5))) + 
               Power(s2,4)*(-14984 - 4586*Power(t1,7) + 47798*t2 - 
                  64462*Power(t2,2) + 51495*Power(t2,3) - 
                  25389*Power(t2,4) + 5373*Power(t2,5) + 
                  14*Power(t1,6)*(37 + 1035*t2) + 
                  Power(t1,5)*(31731 - 31178*t2 - 11122*Power(t2,2)) + 
                  5*Power(t1,4)*
                   (4034 - 29805*t2 + 20894*Power(t2,2) + 
                     291*Power(t2,3)) + 
                  Power(t1,3)*
                   (11497 - 58573*t2 + 156511*Power(t2,2) - 
                     73138*Power(t2,3) + 450*Power(t2,4)) + 
                  Power(t1,2)*
                   (-2570 + 120214*t2 - 192942*Power(t2,2) + 
                     46704*Power(t2,3) + 13967*Power(t2,4)) + 
                  t1*(-42274 + 171640*t2 - 295931*Power(t2,2) + 
                     210732*Power(t2,3) - 45254*Power(t2,4) + 
                     40*Power(t2,5))) + 
               s2*(76*Power(t1,10) - 2*Power(t1,9)*(21 + 116*t2) + 
                  Power(t1,8)*(-1986 + 1765*t2 + 125*Power(t2,2)) + 
                  Power(t1,7)*
                   (905 + 9424*t2 - 6446*Power(t2,2) + 125*Power(t2,3)) \
+ Power(t1,6)*(6785 - 11651*t2 - 8272*Power(t2,2) + 5322*Power(t2,3) - 
                     94*Power(t2,4)) + 
                  Power(t1,5)*
                   (-2188 - 51997*t2 + 83118*Power(t2,2) - 
                     26655*Power(t2,3) + 334*Power(t2,4)) + 
                  Power(t1,3)*
                   (48731 - 124818*t2 + 129004*Power(t2,2) - 
                     80909*Power(t2,3) + 33654*Power(t2,4) - 
                     5364*Power(t2,5)) + 
                  Power(t1,4)*
                   (14348 - 84646*t2 + 186968*Power(t2,2) - 
                     144149*Power(t2,3) + 31053*Power(t2,4) - 
                     1018*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (5022 - 17039*t2 + 21233*Power(t2,2) - 
                     12524*Power(t2,3) + 3940*Power(t2,4) + 
                     200*Power(t2,5)) + 
                  t1*(-11246 + 73719*t2 - 198086*Power(t2,2) + 
                     266723*Power(t2,3) - 185464*Power(t2,4) + 
                     61652*Power(t2,5) - 7298*Power(t2,6)) + 
                  Power(t1,2)*
                   (22279 - 29687*t2 - 87590*Power(t2,2) + 
                     199262*Power(t2,3) - 129420*Power(t2,4) + 
                     26062*Power(t2,5) - 1272*Power(t2,6))) + 
               Power(s2,2)*(8655 - 550*Power(t1,9) - 49629*t2 + 
                  118692*Power(t2,2) - 146641*Power(t2,3) + 
                  97597*Power(t2,4) - 33658*Power(t2,5) + 
                  4984*Power(t2,6) + 3*Power(t1,8)*(65 + 594*t2) + 
                  Power(t1,7)*(8949 - 8398*t2 - 1308*Power(t2,2)) + 
                  Power(t1,6)*
                   (1641 - 44451*t2 + 30531*Power(t2,2) - 
                     251*Power(t2,3)) + 
                  Power(t1,5)*
                   (-11914 + 12680*t2 + 48508*Power(t2,2) - 
                     26027*Power(t2,3) + 339*Power(t2,4)) + 
                  Power(t1,4)*
                   (-1958 + 140281*t2 - 217108*Power(t2,2) + 
                     65547*Power(t2,3) + 3543*Power(t2,4)) + 
                  Power(t1,3)*
                   (-54655 + 251806*t2 - 481010*Power(t2,2) + 
                     355188*Power(t2,3) - 76612*Power(t2,4) + 
                     1518*Power(t2,5)) + 
                  Power(t1,2)*
                   (-84381 + 243851*t2 - 296163*Power(t2,2) + 
                     212117*Power(t2,3) - 92072*Power(t2,4) + 
                     16012*Power(t2,5)) + 
                  t1*(-16222 + 12110*t2 + 99894*Power(t2,2) - 
                     188817*Power(t2,3) + 111931*Power(t2,4) - 
                     19130*Power(t2,5) + 600*Power(t2,6)))) - 
            Power(s1,2)*(Power(t1,11) + Power(s2,11)*(2 + t1 - 9*t2) - 
               Power(t1,10)*(7 + 10*t2) + 
               4*Power(t1,9)*(-13 + 6*t2 + 5*Power(t2,2)) - 
               Power(s2,10)*(-20 + 10*Power(t1,2) + t1*(19 - 86*t2) + 
                  19*t2 + 18*Power(t2,2)) + 
               Power(t1,8)*(-67 + 455*t2 - 203*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,7)*(1106 - 502*t2 - 1608*Power(t2,2) + 
                  728*Power(t2,3) - 41*Power(t2,4)) + 
               Power(t1,5)*(-4193 + 1948*t2 + 14259*Power(t2,2) - 
                  14789*Power(t2,3) + 3810*Power(t2,4) - 311*Power(t2,5)\
) + Power(t1,6)*(412 - 4370*t2 + 3025*Power(t2,2) + 1305*Power(t2,3) - 
                  491*Power(t2,4) + 22*Power(t2,5)) + 
               2*Power(-1 + t2,3)*
                (597 - 1823*t2 + 2358*Power(t2,2) - 1561*Power(t2,3) + 
                  623*Power(t2,4) + 53*Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (603 + 1152*t2 - 12814*Power(t2,2) + 
                  19651*Power(t2,3) - 9598*Power(t2,4) + 
                  1152*Power(t2,5)) + 
               Power(t1,3)*(11303 - 39936*t2 + 60532*Power(t2,2) - 
                  51388*Power(t2,3) + 23917*Power(t2,4) - 
                  4418*Power(t2,5) + 110*Power(t2,6)) + 
               Power(t1,4)*(-141 - 3547*t2 + 26027*Power(t2,2) - 
                  42136*Power(t2,3) + 23298*Power(t2,4) - 
                  4132*Power(t2,5) + 260*Power(t2,6)) + 
               Power(t1,2)*(9765 - 38276*t2 + 49555*Power(t2,2) - 
                  12657*Power(t2,3) - 23355*Power(t2,4) + 
                  19806*Power(t2,5) - 5230*Power(t2,6) + 392*Power(t2,7)\
) + Power(s2,9)*(-40 + 45*Power(t1,3) + Power(t1,2)*(77 - 365*t2) + 
                  415*t2 - 326*Power(t2,2) - 9*Power(t2,3) + 
                  t1*(-272 + 267*t2 + 135*Power(t2,2))) - 
               Power(s2,8)*(19 + 120*Power(t1,4) + 
                  Power(t1,3)*(171 - 904*t2) + 20*t2 - 
                  823*Power(t2,2) + 585*Power(t2,3) + 
                  2*Power(t1,2)*(-725 + 733*t2 + 218*Power(t2,2)) - 
                  2*t1*(191 - 1814*t2 + 1409*Power(t2,2) + 
                     17*Power(t2,3))) + 
               Power(s2,7)*(605 + 210*Power(t1,5) + 
                  Power(t1,4)*(216 - 1442*t2) - 4505*t2 + 
                  6739*Power(t2,2) - 2181*Power(t2,3) - 
                  347*Power(t2,4) + 
                  Power(t1,3)*(-4207 + 4392*t2 + 784*Power(t2,2)) - 
                  Power(t1,2)*
                   (1433 - 13567*t2 + 10532*Power(t2,2) + 
                     6*Power(t2,3)) + 
                  t1*(1051 - 1735*t2 - 4742*Power(t2,2) + 
                     3941*Power(t2,3) - 16*Power(t2,4))) + 
               Power(s2,6)*(-2651 - 252*Power(t1,6) + 9405*t2 - 
                  17681*Power(t2,2) + 14961*Power(t2,3) - 
                  4047*Power(t2,4) + 14*Power(t2,5) + 
                  14*Power(t1,5)*(-9 + 110*t2) + 
                  Power(t1,4)*(7497 - 8134*t2 - 840*Power(t2,2)) + 
                  Power(t1,3)*
                   (2764 - 28430*t2 + 22290*Power(t2,2) - 
                     174*Power(t2,3)) + 
                  Power(t1,2)*
                   (-6025 + 11479*t2 + 10693*Power(t2,2) - 
                     11297*Power(t2,3) + 84*Power(t2,4)) + 
                  t1*(-4624 + 32567*t2 - 47639*Power(t2,2) + 
                     16325*Power(t2,3) + 1586*Power(t2,4))) + 
               Power(s2,5)*(104 + 210*Power(t1,7) + 5966*t2 - 
                  17944*Power(t2,2) + 10957*Power(t2,3) + 
                  2226*Power(t2,4) - 2093*Power(t2,5) - 
                  14*Power(t1,6)*(3 + 79*t2) + 
                  7*Power(t1,5)*(-1229 + 1402*t2 + 74*Power(t2,2)) + 
                  Power(t1,4)*
                   (-2843 + 36551*t2 - 29300*Power(t2,2) + 
                     400*Power(t2,3)) + 
                  Power(t1,2)*
                   (12704 - 93849*t2 + 137705*Power(t2,2) - 
                     49289*Power(t2,3) - 2749*Power(t2,4)) + 
                  Power(t1,3)*
                   (15092 - 31125*t2 - 10937*Power(t2,2) + 
                     17837*Power(t2,3) - 180*Power(t2,4)) + 
                  t1*(13803 - 48279*t2 + 93871*Power(t2,2) - 
                     80410*Power(t2,3) + 21607*Power(t2,4) - 
                     162*Power(t2,5))) + 
               Power(s2,4)*(12349 - 120*Power(t1,8) - 52704*t2 + 
                  104227*Power(t2,2) - 105425*Power(t2,3) + 
                  48838*Power(t2,4) - 7664*Power(t2,5) + 
                  80*Power(t2,6) + 2*Power(t1,7)*(69 + 260*t2) - 
                  7*Power(t1,6)*(-911 + 1116*t2 + 20*Power(t2,2)) + 
                  Power(t1,5)*
                   (1210 - 29480*t2 + 24586*Power(t2,2) - 
                     426*Power(t2,3)) + 
                  5*Power(t1,4)*
                   (-4108 + 9113*t2 + 530*Power(t2,2) - 
                     3343*Power(t2,3) + 40*Power(t2,4)) + 
                  Power(t1,3)*
                   (-15570 + 139324*t2 - 210818*Power(t2,2) + 
                     78254*Power(t2,3) + 2035*Power(t2,4)) + 
                  Power(t1,2)*
                   (-27571 + 91636*t2 - 191311*Power(t2,2) + 
                     171111*Power(t2,3) - 46253*Power(t2,4) + 
                     550*Power(t2,5)) + 
                  t1*(-3153 - 23708*t2 + 86277*Power(t2,2) - 
                     61159*Power(t2,3) - 1368*Power(t2,4) + 
                     7027*Power(t2,5))) + 
               Power(s2,3)*(-17845 + 45*Power(t1,9) + 71590*t2 - 
                  121085*Power(t2,2) + 114985*Power(t2,3) - 
                  67721*Power(t2,4) + 23788*Power(t2,5) - 
                  3832*Power(t2,6) - Power(t1,8)*(114 + 149*t2) + 
                  Power(t1,7)*(-2925 + 4016*t2 - 24*Power(t2,2)) + 
                  Power(t1,6)*
                   (433 + 14473*t2 - 12948*Power(t2,2) + 
                     246*Power(t2,3)) + 
                  Power(t1,4)*
                   (6545 - 112471*t2 + 183137*Power(t2,2) - 
                     70291*Power(t2,3) - 225*Power(t2,4)) + 
                  Power(t1,5)*
                   (16019 - 38745*t2 + 4792*Power(t2,2) + 
                     9255*Power(t2,3) - 120*Power(t2,4)) + 
                  Power(t1,2)*
                   (12007 + 38073*t2 - 172842*Power(t2,2) + 
                     137162*Power(t2,3) - 13836*Power(t2,4) - 
                     8332*Power(t2,5)) + 
                  Power(t1,3)*
                   (25993 - 74914*t2 + 184492*Power(t2,2) - 
                     181788*Power(t2,3) + 50078*Power(t2,4) - 
                     840*Power(t2,5)) + 
                  t1*(-39382 + 170135*t2 - 354140*Power(t2,2) + 
                     376907*Power(t2,3) - 181442*Power(t2,4) + 
                     29558*Power(t2,5) - 368*Power(t2,6))) + 
               Power(s2,2)*(6911 - 10*Power(t1,10) - 23522*t2 + 
                  18093*Power(t2,2) + 18805*Power(t2,3) - 
                  33303*Power(t2,4) + 14342*Power(t2,5) - 
                  1542*Power(t2,6) + 216*Power(t2,7) + 
                  Power(t1,9)*(49 + 22*t2) + 
                  Power(t1,8)*(727 - 1247*t2 + 26*Power(t2,2)) - 
                  2*Power(t1,7)*
                   (376 + 1931*t2 - 1991*Power(t2,2) + 37*Power(t2,3)) \
+ Power(t1,5)*(3400 + 46059*t2 - 88487*Power(t2,2) + 
                     35165*Power(t2,3) - 548*Power(t2,4)) + 
                  Power(t1,6)*
                   (-6901 + 18905*t2 - 4763*Power(t2,2) - 
                     2771*Power(t2,3) + 36*Power(t2,4)) + 
                  Power(t1,4)*
                   (-10818 + 16641*t2 - 79105*Power(t2,2) + 
                     98383*Power(t2,3) - 28081*Power(t2,4) + 
                     630*Power(t2,5)) + 
                  2*Power(t1,3)*
                   (-9582 - 13469*t2 + 86444*Power(t2,2) - 
                     74732*Power(t2,3) + 13341*Power(t2,4) + 
                     1822*Power(t2,5)) + 
                  Power(t1,2)*
                   (42259 - 189155*t2 + 427985*Power(t2,2) - 
                     484943*Power(t2,3) + 241656*Power(t2,4) - 
                     40712*Power(t2,5) + 900*Power(t2,6)) + 
                  t1*(49115 - 193551*t2 + 325801*Power(t2,2) - 
                     308875*Power(t2,3) + 175448*Power(t2,4) - 
                     54574*Power(t2,5) + 6996*Power(t2,6))) + 
               s2*(Power(t1,11) - Power(t1,10)*(11 + t2) + 
                  Power(t1,9)*(-57 + 199*t2 - 5*Power(t2,2)) + 
                  Power(t1,8)*
                   (331 + 370*t2 - 590*Power(t2,2) + 9*Power(t2,3)) + 
                  Power(t1,7)*
                   (1390 - 4779*t2 + 1687*Power(t2,2) + 
                     327*Power(t2,3) - 4*Power(t2,4)) + 
                  Power(t1,6)*
                   (-4166 - 6623*t2 + 20971*Power(t2,2) - 
                     8711*Power(t2,3) + 289*Power(t2,4)) + 
                  Power(t1,5)*
                   (832 + 9881*t2 + 6709*Power(t2,2) - 
                     23562*Power(t2,3) + 7187*Power(t2,4) - 
                     214*Power(t2,5)) + 
                  Power(t1,4)*
                   (14399 + 4659*t2 - 82638*Power(t2,2) + 
                     77293*Power(t2,3) - 17514*Power(t2,4) + 
                     65*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (-1758 + 10309*t2 - 24640*Power(t2,2) + 
                     24701*Power(t2,3) - 10364*Power(t2,4) + 
                     1898*Power(t2,5)) + 
                  Power(t1,2)*
                   (-42735 + 162796*t2 - 267257*Power(t2,2) + 
                     247655*Power(t2,3) - 133281*Power(t2,4) + 
                     35860*Power(t2,5) - 3398*Power(t2,6)) + 
                  Power(t1,3)*
                   (-15085 + 75271*t2 - 204099*Power(t2,2) + 
                     255597*Power(t2,3) - 132350*Power(t2,4) + 
                     22950*Power(t2,5) - 872*Power(t2,6)) + 
                  t1*(-17505 + 66804*t2 - 79866*Power(t2,2) + 
                     8722*Power(t2,3) + 47623*Power(t2,4) - 
                     31764*Power(t2,5) + 6486*Power(t2,6) - 
                     500*Power(t2,7)))) + 
            s1*(2*Power(t1,11) - 2*Power(s2,11)*t2 - 
               2*Power(t1,10)*(13 + 2*t2) + 
               Power(t1,9)*(78 + 72*t2 - 9*Power(t2,2)) + 
               Power(t1,8)*(92 - 451*t2 - 41*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,7)*(-196 + 10*t2 + 1107*Power(t2,2) - 
                  173*Power(t2,3) - 3*Power(t2,4)) - 
               Power(t1,6)*(1074 - 2806*t2 + 1185*Power(t2,2) + 
                  2020*Power(t2,3) - 645*Power(t2,4) + 34*Power(t2,5)) + 
               Power(-1 + t2,3)*
                (426 - 639*t2 - 955*Power(t2,2) + 2035*Power(t2,3) - 
                  832*Power(t2,4) + 260*Power(t2,5)) + 
               Power(t1,4)*(3854 - 9654*t2 + 677*Power(t2,2) + 
                  14169*Power(t2,3) - 11201*Power(t2,4) + 
                  1982*Power(t2,5) - 83*Power(t2,6)) + 
               Power(t1,5)*(160 + 2856*t2 - 6835*Power(t2,2) + 
                  3198*Power(t2,3) + 1708*Power(t2,4) - 
                  512*Power(t2,5) + 20*Power(t2,6)) + 
               2*t1*Power(-1 + t2,2)*
                (-2107 + 7084*t2 - 9017*Power(t2,2) + 4881*Power(t2,3) - 
                  600*Power(t2,4) - 375*Power(t2,5) + 36*Power(t2,6)) + 
               Power(t1,3)*(330 - 2718*t2 - 4783*Power(t2,2) + 
                  26993*Power(t2,3) - 32819*Power(t2,4) + 
                  15260*Power(t2,5) - 2344*Power(t2,6) + 118*Power(t2,7)) \
+ Power(t1,2)*(-6260 + 28202*t2 - 57547*Power(t2,2) + 
                  69676*Power(t2,3) - 51999*Power(t2,4) + 
                  21876*Power(t2,5) - 4144*Power(t2,6) + 196*Power(t2,7)) \
+ Power(s2,10)*(4 - 30*t2 + 11*Power(t2,2) + 4*t1*(1 + 4*t2)) - 
               Power(s2,9)*(-16 + 23*t2 + 71*Power(t2,2) - 
                  30*Power(t2,3) + 4*Power(t1,2)*(9 + 14*t2) + 
                  t1*(26 - 269*t2 + 94*Power(t2,2))) + 
               Power(s2,8)*(-77 + 620*t2 - 943*Power(t2,2) + 
                  309*Power(t2,3) + 19*Power(t2,4) + 
                  2*Power(t1,3)*(73 + 56*t2) + 
                  2*Power(t1,2)*(23 - 537*t2 + 173*Power(t2,2)) + 
                  t1*(-217 + 474*t2 + 402*Power(t2,2) - 184*Power(t2,3))) \
- Power(s2,7)*(-55 + 334*t2 - 1302*Power(t2,2) + 1715*Power(t2,3) - 
                  485*Power(t2,4) + 8*Power(t2,5) + 
                  4*Power(t1,4)*(88 + 35*t2) + 
                  Power(t1,3)*(-96 - 2510*t2 + 714*Power(t2,2)) + 
                  Power(t1,2)*
                   (-967 + 2644*t2 + 907*Power(t2,2) - 462*Power(t2,3)) \
+ t1*(-572 + 4385*t2 - 6920*Power(t2,2) + 2246*Power(t2,3) + 
                     65*Power(t2,4))) + 
               Power(s2,6)*(765 - 5111*t2 + 10040*Power(t2,2) - 
                  7400*Power(t2,3) + 1098*Power(t2,4) + 
                  241*Power(t2,5) + 112*Power(t1,5)*(5 + t2) + 
                  14*Power(t1,4)*(-44 - 271*t2 + 64*Power(t2,2)) + 
                  Power(t1,3)*
                   (-2063 + 7270*t2 + 967*Power(t2,2) - 588*Power(t2,3)) \
+ Power(t1,2)*(-1801 + 12951*t2 - 21731*Power(t2,2) + 
                     6917*Power(t2,3) + 28*Power(t2,4)) + 
                  t1*(289 + 194*t2 - 5361*Power(t2,2) + 
                     9370*Power(t2,3) - 2576*Power(t2,4) + 49*Power(t2,5)\
)) - Power(s2,5)*(2475 - 10876*t2 + 22795*Power(t2,2) - 
                  25000*Power(t2,3) + 13101*Power(t2,4) - 
                  2161*Power(t2,5) + 36*Power(t2,6) + 
                  56*Power(t1,6)*(11 + t2) + 
                  14*Power(t1,5)*(-98 - 276*t2 + 49*Power(t2,2)) + 
                  Power(t1,4)*
                   (-2207 + 11742*t2 + 321*Power(t2,2) - 
                     350*Power(t2,3)) + 
                  Power(t1,3)*
                   (-3028 + 20229*t2 - 37901*Power(t2,2) + 
                     11738*Power(t2,3) - 170*Power(t2,4)) + 
                  Power(t1,2)*
                   (1936 - 3498*t2 - 6881*Power(t2,2) + 
                     21431*Power(t2,3) - 5763*Power(t2,4) + 
                     120*Power(t2,5)) + 
                  t1*(3992 - 27411*t2 + 54416*Power(t2,2) - 
                     41690*Power(t2,3) + 7242*Power(t2,4) + 
                     786*Power(t2,5))) + 
               Power(s2,4)*(-561 + 5591*t2 - 16667*Power(t2,2) + 
                  17272*Power(t2,3) - 4194*Power(t2,4) - 
                  2566*Power(t2,5) + 907*Power(t2,6) + 
                  4*Power(t1,7)*(119 + 4*t2) + 
                  Power(t1,5)*(-761 + 11918*t2 - 349*Power(t2,2)) + 
                  14*Power(t1,6)*(-126 - 191*t2 + 21*Power(t2,2)) - 
                  5*Power(t1,4)*
                   (551 - 3391*t2 + 7946*Power(t2,2) - 
                     2383*Power(t2,3) + 65*Power(t2,4)) + 
                  Power(t1,3)*
                   (3558 - 9242*t2 + 1153*Power(t2,2) + 
                     26151*Power(t2,3) - 6995*Power(t2,4) + 
                     150*Power(t2,5)) + 
                  Power(t1,2)*
                   (7332 - 55706*t2 + 115613*Power(t2,2) - 
                     94196*Power(t2,3) + 18257*Power(t2,4) + 
                     848*Power(t2,5)) + 
                  t1*(10971 - 47158*t2 + 97328*Power(t2,2) - 
                     106142*Power(t2,3) + 55878*Power(t2,4) - 
                     8928*Power(t2,5) + 120*Power(t2,6))) - 
               Power(s2,3)*(-9669 + 46940*t2 - 103844*Power(t2,2) + 
                  125271*Power(t2,3) - 81095*Power(t2,4) + 
                  24942*Power(t2,5) - 2556*Power(t2,6) + 
                  48*Power(t2,7) + 2*Power(t1,8)*(128 + t2) + 
                  2*Power(t1,7)*(-712 - 617*t2 + 23*Power(t2,2)) + 
                  Power(t1,6)*
                   (803 + 7724*t2 - 463*Power(t2,2) + 126*Power(t2,3)) + 
                  Power(t1,5)*
                   (-1052 + 5795*t2 - 25138*Power(t2,2) + 
                     7314*Power(t2,3) - 251*Power(t2,4)) + 
                  Power(t1,4)*
                   (2577 - 9798*t2 + 11772*Power(t2,2) + 
                     17669*Power(t2,3) - 4895*Power(t2,4) + 
                     100*Power(t2,5)) + 
                  2*Power(t1,3)*
                   (2310 - 25478*t2 + 59779*Power(t2,2) - 
                     54234*Power(t2,3) + 11472*Power(t2,4) + 
                     102*Power(t2,5)) + 
                  Power(t1,2)*
                   (18401 - 73584*t2 + 148013*Power(t2,2) - 
                     164910*Power(t2,3) + 91272*Power(t2,4) - 
                     14834*Power(t2,5) + 224*Power(t2,6)) + 
                  t1*(3344 - 2713*t2 - 23896*Power(t2,2) + 
                     39154*Power(t2,3) - 13709*Power(t2,4) - 
                     5076*Power(t2,5) + 1986*Power(t2,6))) + 
               Power(s2,2)*(-11961 + 92*Power(t1,9) + 59533*t2 - 
                  127141*Power(t2,2) + 149624*Power(t2,3) - 
                  103206*Power(t2,4) + 41177*Power(t2,5) - 
                  9026*Power(t2,6) + 1000*Power(t2,7) - 
                  Power(t1,8)*(716 + 360*t2 + 11*Power(t2,2)) + 
                  Power(t1,7)*
                   (1043 + 3114*t2 - 243*Power(t2,2) + 68*Power(t2,3)) + 
                  Power(t1,6)*
                   (253 - 1595*t2 - 8995*Power(t2,2) + 
                     2591*Power(t2,3) - 90*Power(t2,4)) + 
                  Power(t1,5)*
                   (253 - 4818*t2 + 12661*Power(t2,2) + 
                     5876*Power(t2,3) - 1890*Power(t2,4) + 
                     33*Power(t2,5)) - 
                  Power(t1,4)*
                   (1687 + 16077*t2 - 58556*Power(t2,2) + 
                     66128*Power(t2,3) - 15240*Power(t2,4) + 
                     231*Power(t2,5)) + 
                  Power(t1,3)*
                   (13949 - 46342*t2 + 88387*Power(t2,2) - 
                     108196*Power(t2,3) + 69022*Power(t2,4) - 
                     12040*Power(t2,5) + 252*Power(t2,6)) + 
                  Power(t1,2)*
                   (12239 - 31469*t2 + 1323*Power(t2,2) + 
                     42615*Power(t2,3) - 26578*Power(t2,4) - 
                     1004*Power(t2,5) + 1452*Power(t2,6)) + 
                  t1*(-17977 + 83518*t2 - 192025*Power(t2,2) + 
                     252150*Power(t2,3) - 180340*Power(t2,4) + 
                     62063*Power(t2,5) - 7344*Power(t2,6) + 
                     66*Power(t2,7))) - 
               s2*(20*Power(t1,10) - 
                  Power(t1,9)*(206 + 59*t2 + 4*Power(t2,2)) + 
                  Power(t1,8)*
                   (467 + 715*t2 - 68*Power(t2,2) + 12*Power(t2,3)) + 
                  Power(t1,7)*
                   (364 - 1929*t2 - 1481*Power(t2,2) + 462*Power(t2,3) - 
                     12*Power(t2,4)) + 
                  Power(t1,5)*
                   (-3276 + 4279*t2 + 9050*Power(t2,2) - 
                     19586*Power(t2,3) + 5054*Power(t2,4) - 
                     166*Power(t2,5)) + 
                  Power(t1,6)*
                   (-554 - 894*t2 + 5971*Power(t2,2) + 409*Power(t2,3) - 
                     321*Power(t2,4) + 4*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (-4991 + 16115*t2 - 17599*Power(t2,2) + 
                     6271*Power(t2,3) - 92*Power(t2,4) + 
                     36*Power(t2,5) + 64*Power(t2,6)) + 
                  Power(t1,4)*
                   (4204 - 6184*t2 + 8072*Power(t2,2) - 
                     21230*Power(t2,3) + 22235*Power(t2,4) - 
                     4485*Power(t2,5) + 132*Power(t2,6)) + 
                  Power(t1,3)*
                   (12188 - 32819*t2 + 9229*Power(t2,2) + 
                     34902*Power(t2,3) - 28264*Power(t2,4) + 
                     3488*Power(t2,5) + 290*Power(t2,6)) + 
                  Power(t1,2)*
                   (-8010 + 34170*t2 - 93955*Power(t2,2) + 
                     155381*Power(t2,3) - 133321*Power(t2,4) + 
                     53030*Power(t2,5) - 7376*Power(t2,6) + 
                     192*Power(t2,7)) + 
                  2*t1*(-8967 + 42984*t2 - 90437*Power(t2,2) + 
                     107985*Power(t2,3) - 77199*Power(t2,4) + 
                     31475*Power(t2,5) - 6271*Power(t2,6) + 
                     430*Power(t2,7))))))*
       T2q(1 - s + s1 - t2,1 - s2 + t1 - t2))/
     ((-1 + t1)*Power(1 - s2 + t1 - t2,2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,3)*(-1 + t2)*(s - s1 + t2)*
       Power(-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2,2)) - 
    (8*(Power(s,8)*(-1 + s1)*Power(t1 - t2,2)*(-2 + 2*s2 - 3*t1 + 3*t2) + 
         Power(s,7)*((4 - 5*s1 + 2*Power(s1,2))*Power(t1,4) - 
            Power(t1,3)*(15 + 8*s2 + s1*(3 - 14*s2 - 3*t2) + t2 + 
               Power(s1,2)*(-17 + 5*s2 + 6*t2)) + 
            Power(t1,2)*(-10 + 5*Power(s2,2) + Power(s1,3)*(-1 + t2) + 
               57*t2 - 21*Power(t2,2) + s2*(8 + 19*t2) + 
               Power(s1,2)*(22 + 4*Power(s2,2) - 42*t2 + 
                  6*Power(t2,2) + s2*(-21 + 11*t2)) - 
               s1*(13 + 11*Power(s2,2) + 13*t2 - 21*Power(t2,2) + 
                  s2*(-17 + 33*t2))) + 
            t2*(5 + Power(s2,3) - 22*t2 + 27*Power(t2,2) - 
               11*Power(t2,3) + Power(s1,3)*(-1 + t2)*(-1 + s2 + t2) + 
               Power(s2,2)*(3 + 7*t2) + 
               3*s2*(-3 + 6*t2 + Power(t2,2)) + 
               Power(s1,2)*(-1 + Power(s2,3) + 8*t2 - 8*Power(t2,2) + 
                  Power(s2,2)*(-1 + 2*t2) + s2*(1 - 5*t2 + Power(t2,2))\
) - s1*(5 + 2*Power(s2,3) - 14*t2 + 19*Power(t2,2) - 12*Power(t2,3) + 
                  Power(s2,2)*(2 + 11*t2) + 
                  s2*(-9 + 10*t2 + 5*Power(t2,2)))) - 
            t1*(1 + Power(s2,3) - 32*t2 + 69*Power(t2,2) - 
               29*Power(t2,3) + Power(s1,3)*(-1 + t2)*(-1 + s2 + 2*t2) + 
               Power(s2,2)*(-1 + 12*t2) + 
               s2*(-1 + 26*t2 + 14*Power(t2,2)) + 
               s1*(3 - 2*Power(s2,3) + Power(s2,2)*(6 - 22*t2) + t2 - 
                  35*Power(t2,2) + 31*Power(t2,3) + 
                  s2*(-7 + 7*t2 - 24*Power(t2,2))) + 
               Power(s1,2)*(-5 + Power(s2,3) + 30*t2 - 33*Power(t2,2) + 
                  2*Power(t2,3) + Power(s2,2)*(-5 + 6*t2) + 
                  s2*(9 - 26*t2 + 7*Power(t2,2))))) + 
         Power(s,6)*(5 + 25*t1 + 43*Power(t1,2) + 35*Power(t1,3) - 
            28*Power(t1,4) + 2*Power(t1,5) + Power(s2,4)*(t1 - 2*t2) - 
            37*t2 - 162*t1*t2 - 165*Power(t1,2)*t2 + 39*Power(t1,3)*t2 + 
            7*Power(t1,4)*t2 + 123*Power(t2,2) + 285*t1*Power(t2,2) + 
            85*Power(t1,2)*Power(t2,2) - 18*Power(t1,3)*Power(t2,2) - 
            155*Power(t2,3) - 175*t1*Power(t2,3) - 
            8*Power(t1,2)*Power(t2,3) + 79*Power(t2,4) + 
            32*t1*Power(t2,4) - 15*Power(t2,5) - 
            Power(s1,4)*(-1 + t2)*
             (-Power(s2,2) + t1 + Power(t1,2) - 3*t1*t2 + 
               2*(-1 + t2)*t2 + s2*(1 + t2)) - 
            Power(s2,3)*(3 + 3*Power(t1,2) + 7*t2 + 3*Power(t2,2) - 
               6*t1*(1 + t2)) + 
            Power(s2,2)*(13 + 5*Power(t1,3) - 15*t2 - 38*Power(t2,2) + 
               13*Power(t2,3) - Power(t1,2)*(35 + 2*t2) + 
               t1*(-2 + 85*t2 - 16*Power(t2,2))) + 
            s2*(-15 - 5*Power(t1,4) + Power(t1,3)*(57 - 9*t2) + 60*t2 - 
               97*Power(t2,2) + 20*Power(t2,3) + 27*Power(t2,4) + 
               2*Power(t1,2)*(-8 - 77*t2 + 30*Power(t2,2)) + 
               t1*(-29 + 97*t2 + 77*Power(t2,2) - 73*Power(t2,3))) + 
            Power(s1,3)*(-1 - Power(s2,4) - 8*Power(t1,4) + 
               Power(s2,3)*(7 + 8*t1 - 5*t2) + 
               25*Power(t1,3)*(-2 + t2) + 6*t2 - 12*Power(t2,2) + 
               6*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(-73 + 107*t2 - 25*Power(t2,2)) - 
               Power(s2,2)*(11 + 21*Power(t1,2) + t1*(52 - 30*t2) - 
                  24*t2 + 12*Power(t2,2)) + 
               t1*(-32 + 87*t2 - 63*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(6 + 22*Power(t1,3) + Power(t1,2)*(95 - 50*t2) - 
                  21*t2 + 23*Power(t2,2) - 7*Power(t2,3) + 
                  t1*(72 - 117*t2 + 35*Power(t2,2)))) + 
            Power(s1,2)*(5 + 2*Power(t1,5) + 
               Power(s2,4)*(2 + 3*t1 - 4*t2) - 36*Power(t2,2) + 
               58*Power(t2,3) - 27*Power(t2,4) + 
               Power(t1,4)*(9 + 2*t2) + 
               Power(t1,3)*(-10 + 51*t2 - 18*Power(t2,2)) - 
               Power(s2,3)*(15 + 13*Power(t1,2) - 20*t1*(-1 + t2) - 
                  9*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(11 + 144*t2 - 156*Power(t2,2) + 
                  22*Power(t2,3)) + 
               t1*(47 + 20*t2 - 192*Power(t2,2) + 123*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,2)*(32 + 19*Power(t1,3) - 29*t2 + 
                  15*Power(t2,2) + 2*Power(t2,3) - 
                  6*Power(t1,2)*(-8 + 5*t2) + 
                  t1*(77 - 51*t2 + 9*Power(t2,2))) + 
               s2*(-24 - 11*Power(t1,4) + 18*t2 + 4*Power(t2,2) + 
                  9*Power(t2,3) + 3*Power(t2,4) + 
                  3*Power(t1,3)*(-13 + 4*t2) + 
                  Power(t1,2)*(-62 + 3*t2 + 12*Power(t2,2)) + 
                  t1*(-101 + 49*t2 + 27*Power(t2,2) - 16*Power(t2,3)))) \
+ s1*(-9 - 2*Power(t1,5) + Power(t1,4)*(22 - 13*t2) + 29*t2 - 
               63*Power(t2,2) + 77*Power(t2,3) - 53*Power(t2,4) + 
               19*Power(t2,5) + Power(s2,4)*(-1 - 4*t1 + 6*t2) + 
               Power(s2,3)*(11 + 17*Power(t1,2) + t1*(2 - 32*t2) + 
                  7*t2 + 13*Power(t2,2)) + 
               2*Power(t1,3)*(17 - 50*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(38 - 119*t2 + 81*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-37 + 23*t2 + 8*Power(t2,2) + 50*Power(t2,3) - 
                  38*Power(t2,4)) + 
               Power(s2,2)*(-33 - 24*Power(t1,3) + 7*t2 + 
                  41*Power(t2,2) - 22*Power(t2,3) + 
                  Power(t1,2)*(26 + 25*t2) + 
                  t1*(-11 - 88*t2 + 21*Power(t2,2))) + 
               s2*(32 + 13*Power(t1,4) - 45*t2 + 44*Power(t2,2) + 
                  Power(t2,3) - 38*Power(t2,4) + 
                  7*Power(t1,3)*(-7 + 2*t2) + 
                  Power(t1,2)*(-56 + 242*t2 - 105*Power(t2,2)) + 
                  t1*(46 + 37*t2 - 194*Power(t2,2) + 116*Power(t2,3))))) \
+ Power(s,5)*(-31 + 86*s2 - 60*Power(s2,2) + 3*Power(s2,3) + 
            3*Power(s2,4) - 111*t1 + 108*s2*t1 + 31*Power(s2,2)*t1 - 
            24*Power(s2,3)*t1 - 6*Power(s2,4)*t1 - 134*Power(t1,2) + 
            6*s2*Power(t1,2) + 114*Power(s2,2)*Power(t1,2) + 
            18*Power(s2,3)*Power(t1,2) - 54*Power(t1,3) - 
            177*s2*Power(t1,3) - 30*Power(s2,2)*Power(t1,3) + 
            84*Power(t1,4) + 30*s2*Power(t1,4) - 12*Power(t1,5) + 
            159*t2 - 265*s2*t2 + 68*Power(s2,2)*t2 + 29*Power(s2,3)*t2 + 
            7*Power(s2,4)*t2 + Power(s2,5)*t2 + 593*t1*t2 - 
            293*s2*t1*t2 - 293*Power(s2,2)*t1*t2 - 
            34*Power(s2,3)*t1*t2 + 2*Power(s2,4)*t1*t2 + 
            408*Power(t1,2)*t2 + 491*s2*Power(t1,2)*t2 + 
            5*Power(s2,2)*Power(t1,2)*t2 - 
            7*Power(s2,3)*Power(t1,2)*t2 - 152*Power(t1,3)*t2 + 
            70*s2*Power(t1,3)*t2 + 11*Power(s2,2)*Power(t1,3)*t2 - 
            48*Power(t1,4)*t2 - 14*s2*Power(t1,4)*t2 + 
            7*Power(t1,5)*t2 - 421*Power(t2,2) + 303*s2*Power(t2,2) + 
            134*Power(s2,2)*Power(t2,2) + 15*Power(s2,3)*Power(t2,2) - 
            7*Power(s2,4)*Power(t2,2) - 991*t1*Power(t2,2) - 
            112*s2*t1*Power(t2,2) + 116*Power(s2,2)*t1*Power(t2,2) + 
            22*Power(s2,3)*t1*Power(t2,2) - 
            225*Power(t1,2)*Power(t2,2) - 
            405*s2*Power(t1,2)*Power(t2,2) - 
            27*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
            166*Power(t1,3)*Power(t2,2) + 
            19*s2*Power(t1,3)*Power(t2,2) - 7*Power(t1,4)*Power(t2,2) + 
            581*Power(t2,3) - 134*s2*Power(t2,3) - 
            107*Power(s2,2)*Power(t2,3) - 12*Power(s2,3)*Power(t2,3) + 
            682*t1*Power(t2,3) + 338*s2*t1*Power(t2,3) + 
            24*Power(s2,2)*t1*Power(t2,3) - 30*Power(t1,2)*Power(t2,3) + 
            47*s2*Power(t1,2)*Power(t2,3) - 12*Power(t1,3)*Power(t2,3) - 
            389*Power(t2,4) - 33*s2*Power(t2,4) - 
            8*Power(s2,2)*Power(t2,4) - 186*t1*Power(t2,4) - 
            95*s2*t1*Power(t2,4) + 8*Power(t1,2)*Power(t2,4) + 
            110*Power(t2,5) + 43*s2*Power(t2,5) + 13*t1*Power(t2,5) - 
            9*Power(t2,6) + Power(s1,5)*
             (Power(t1,3) + Power(t1,2)*(3 - 2*t2) + 
               Power(s2,2)*(2 + t1 - t2) + 2*Power(-1 + t2,2)*t2 - 
               t1*(-2 + t2 + Power(t2,2)) + 
               s2*(-2 - 2*Power(t1,2) + t2 + Power(t2,2) + 
                  t1*(-5 + 3*t2))) + 
            Power(s1,4)*(3*Power(s2,4) + 13*Power(t1,4) + 
               Power(t1,3)*(55 - 31*t2) + 
               Power(s2,3)*(-17 - 19*t1 + 2*t2) - 
               Power(-1 + t2,2)*(-2 + 7*t2) + 
               2*Power(s2,2)*
                (16 + 21*Power(t1,2) + t1*(42 - 15*t2) - 23*t2 + 
                  5*Power(t2,2)) + 
               Power(t1,2)*(72 - 103*t2 + 27*Power(t2,2)) + 
               t1*(34 - 83*t2 + 58*Power(t2,2) - 9*Power(t2,3)) - 
               s2*(19 + 39*Power(t1,3) + Power(t1,2)*(122 - 59*t2) - 
                  55*t2 + 47*Power(t2,2) - 11*Power(t2,3) + 
                  t1*(102 - 145*t2 + 35*Power(t2,2)))) + 
            Power(s1,3)*(3*Power(s2,5) - 6*Power(t1,5) - 
               2*Power(s2,4)*(12 + 11*t1 - 8*t2) - 
               Power(t1,4)*(27 + 8*t2) + 
               Power(s2,3)*(57 + 56*Power(t1,2) + t1*(120 - 71*t2) - 
                  69*t2 + 27*Power(t2,2)) + 
               Power(t1,3)*(39 - 114*t2 + 56*Power(t2,2)) + 
               Power(t1,2)*(107 - 368*t2 + 322*Power(t2,2) - 
                  65*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-5 + 6*t2 - 15*Power(t2,2) + Power(t2,3)) + 
               t1*(44 - 259*t2 + 389*Power(t2,2) - 198*Power(t2,3) + 
                  24*Power(t2,4)) - 
               Power(s2,2)*(62 + 64*Power(t1,3) + 
                  Power(t1,2)*(199 - 90*t2) - 93*t2 + 37*Power(t2,2) - 
                  2*Power(t2,3) + t1*(136 - 129*t2 + 42*Power(t2,2))) + 
               s2*(17 + 33*Power(t1,4) + Power(t1,3)*(130 - 27*t2) - 
                  35*t2 + 27*Power(t2,2) + 4*Power(t2,3) - 
                  13*Power(t2,4) + 
                  Power(t1,2)*(42 + 50*t2 - 39*Power(t2,2)) + 
                  t1*(17 + 134*t2 - 189*Power(t2,2) + 46*Power(t2,3)))) \
+ Power(s1,2)*(Power(s2,4)*(31 + 14*Power(t1,2) + t1*(17 - 24*t2) - 
                  12*t2) + Power(s2,5)*(-4 - 3*t1 + 6*t2) + 
               Power(t1,5)*(-13 + 8*t2) + 
               Power(t1,4)*(-43 + 78*t2 - 12*Power(t2,2)) + 
               Power(t1,3)*(-5 + 40*t2 - 12*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (27 + 48*t2 - 83*Power(t2,2) + 36*Power(t2,3)) + 
               Power(t1,2)*(-54 - 86*t2 + 309*Power(t2,2) - 
                  218*Power(t2,3) + 28*Power(t2,4)) + 
               t1*(-136 + 197*t2 + 223*Power(t2,2) - 461*Power(t2,3) + 
                  189*Power(t2,4) - 12*Power(t2,5)) - 
               Power(s2,3)*(59 + 24*Power(t1,3) + 
                  Power(t1,2)*(10 - 22*t2) - 77*t2 + 15*Power(t2,3) + 
                  t1*(74 + 24*t2 - 27*Power(t2,2))) + 
               s2*(62 - 5*Power(t1,5) + Power(t1,4)*(38 - 24*t2) + 
                  13*t2 - 193*Power(t2,2) + 58*Power(t2,3) + 
                  57*Power(t2,4) + 3*Power(t2,5) + 
                  Power(t1,3)*(94 - 242*t2 + 69*Power(t2,2)) + 
                  Power(t1,2)*
                   (70 - 323*t2 + 253*Power(t2,2) - 43*Power(t2,3)) - 
                  2*t1*(-68 + 135*t2 - 141*Power(t2,2) + 
                     53*Power(t2,3))) + 
               Power(s2,2)*(3 + 18*Power(t1,4) - 77*t2 + 
                  42*Power(t2,2) + 17*Power(t2,3) - 6*Power(t2,4) + 
                  4*Power(t1,3)*(-7 + 3*t2) - 
                  4*Power(t1,2)*(2 - 50*t2 + 21*Power(t2,2)) + 
                  t1*(29 + 126*t2 - 198*Power(t2,2) + 60*Power(t2,3)))) \
+ s1*(-Power(t1,6) + Power(s2,5)*(1 + 2*t1 - 6*t2) - 
               2*Power(t1,5)*(-6 + t2) + 
               Power(t1,4)*(-60 + 41*t2 - 17*Power(t2,2)) + 
               Power(t1,3)*(-135 + 427*t2 - 298*Power(t2,2) + 
                  58*Power(t2,3)) + 
               Power(t1,2)*(-103 + 522*t2 - 754*Power(t2,2) + 
                  353*Power(t2,3) - 37*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (49 - 15*t2 + 38*Power(t2,2) - 42*Power(t2,3) + 
                  15*Power(t2,4)) - 
               2*t1*(-70 + 116*t2 + 53*Power(t2,2) - 125*Power(t2,3) + 
                  18*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,4)*(-15 - 8*Power(t1,2) - 16*t2 + 
                  7*Power(t2,2) + 2*t1*(8 + 7*t2)) + 
               Power(s2,3)*(24 + 13*Power(t1,3) - 57*t2 - 
                  44*Power(t2,2) + 54*Power(t2,3) + 
                  Power(t1,2)*(-83 + 17*t2) + 
                  t1*(15 + 158*t2 - 97*Power(t2,2))) + 
               Power(s2,2)*(73 - 11*Power(t1,4) + 37*t2 - 
                  253*Power(t2,2) + 109*Power(t2,3) + 15*Power(t2,4) - 
                  18*Power(t1,3)*(-7 + 3*t2) + 
                  Power(t1,2)*(-38 - 231*t2 + 153*Power(t2,2)) + 
                  t1*(-83 + 249*t2 + 35*Power(t2,2) - 103*Power(t2,3))) \
+ s2*(-136 + 5*Power(t1,5) + 152*t2 + 123*Power(t2,2) - 
                  143*Power(t2,3) + 71*Power(t2,4) - 67*Power(t2,5) + 
                  Power(t1,4)*(-72 + 31*t2) + 
                  Power(t1,3)*(98 + 48*t2 - 46*Power(t2,2)) + 
                  Power(t1,2)*
                   (217 - 744*t2 + 488*Power(t2,2) - 88*Power(t2,3)) + 
                  t1*(-75 - 136*t2 + 619*Power(t2,2) - 535*Power(t2,3) + 
                     165*Power(t2,4))))) + 
         (-1 + t2)*(2*Power(s1,6)*Power(s2 - t1,4)*
             Power(-1 + s2 - t1 + t2,2) + 
            Power(s1,5)*Power(s2 - t1,3)*
             (4*Power(s2,3) - 4*Power(t1,3) + 
               Power(-1 + t2,3)*(7 + 4*t2) - 
               t1*Power(-1 + t2,2)*(19 + 7*t2) - 
               2*Power(s2,2)*(7 + 6*t1 - 5*t2 - 2*Power(t2,2)) + 
               3*Power(t1,2)*(-5 + 4*t2 + Power(t2,2)) + 
               s2*(12*Power(t1,2) + 2*Power(-1 + t2,2)*(9 + 4*t2) + 
                  t1*(29 - 22*t2 - 7*Power(t2,2)))) + 
            Power(s1,4)*(s2 - t1)*
             (4*Power(s2,6)*(-1 + t2) + 
               Power(s2,5)*(13 - 20*t1*(-1 + t2) - 28*t2 + 
                  9*Power(t2,2)) + 
               2*Power(s2,4)*
                (-9 + 20*Power(t1,2)*(-1 + t2) + 26*t2 - 
                  18*Power(t2,2) + Power(t2,3) + 
                  t1*(-29 + 60*t2 - 16*Power(t2,2))) + 
               Power(s2,3)*(-40*Power(t1,3)*(-1 + t2) - 
                  Power(-1 + t2,2)*(-32 + 10*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(103 - 202*t2 + 39*Power(t2,2)) + 
                  t1*(66 - 183*t2 + 112*Power(t2,2) + 5*Power(t2,3))) + 
               Power(s2,2)*(20*Power(t1,4)*(-1 + t2) + 
                  Power(t1,3)*(-91 + 166*t2 - 15*Power(t2,2)) - 
                  4*Power(-1 + t2,3)*(-9 - 5*t2 + Power(t2,2)) + 
                  t1*Power(-1 + t2,2)*(-91 + 20*t2 + 26*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (30 - 79*t2 + 40*Power(t2,2) + 9*Power(t2,3))) + 
               t1*(-3*Power(-1 + t2,4)*(3 + 4*t2) - 
                  2*t1*Power(-1 + t2,3)*
                   (-18 - 11*t2 + 3*Power(t2,2)) + 
                  Power(t1,4)*(-7 + 10*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*Power(-1 + t2,2)*
                   (-25 - 4*t2 + 14*Power(t2,2)) - 
                  Power(t1,3)*
                   (12 - 27*t2 + 4*Power(t2,2) + 11*Power(t2,3))) + 
               s2*(-4*Power(t1,5)*(-1 + t2) + 
                  3*Power(-1 + t2,4)*(3 + 4*t2) + 
                  Power(t1,4)*(40 - 66*t2 - 4*Power(t2,2)) + 
                  t1*Power(-1 + t2,3)*(-73 - 40*t2 + 9*Power(t2,2)) - 
                  3*Power(t1,2)*Power(-1 + t2,2)*
                   (-28 + 2*t2 + 11*Power(t2,2)) + 
                  Power(t1,3)*
                   (54 - 133*t2 + 48*Power(t2,2) + 31*Power(t2,3)))) + 
            Power(s1,2)*Power(-1 + t2,2)*
             (3*Power(t1,6) + Power(t1,5)*(46 - 27*t2) + 
               Power(s2,7)*t2 + Power(-1 + t2,4)*(1 + 4*t2) - 
               Power(s2,6)*(-21 + t1 + 9*t2 + 5*t1*t2 - 
                  2*Power(t2,2)) + 
               t1*Power(-1 + t2,3)*(-23 - 4*t2 + 18*Power(t2,2)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (28 + 95*t2 + 19*Power(t2,2)) + 
               Power(t1,4)*(67 - 202*t2 + 47*Power(t2,2)) - 
               2*Power(t1,3)*
                (3 + 103*t2 - 117*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,5)*(-84 + 108*t2 - 21*Power(t2,2) + 
                  Power(t2,3) + 5*Power(t1,2)*(1 + 2*t2) - 
                  3*t1*(35 - 13*t2 + 2*Power(t2,2))) + 
               Power(s2,4)*(57 - 284*t2 + 161*Power(t2,2) - 
                  22*Power(t2,3) - 10*Power(t1,3)*(1 + t2) + 
                  Power(t1,2)*(214 - 69*t2 + 6*Power(t2,2)) + 
                  t1*(393 - 467*t2 + 76*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(t1,6) + Power(t1,5)*(-34 + 6*t2) - 
                  6*Power(t1,4)*(47 - 39*t2 + 4*Power(t2,2)) - 
                  3*Power(-1 + t2,3)*(-8 + 5*Power(t2,2)) + 
                  t1*Power(-1 + t2,2)*
                   (39 + 218*t2 + 23*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,3)*
                   (-287 + 962*t2 - 359*Power(t2,2) + 36*Power(t2,3)) \
+ Power(t1,2)*(42 + 653*t2 - 867*Power(t2,2) + 195*Power(t2,3) - 
                     23*Power(t2,4))) - 
               Power(s2,2)*(Power(t1,5)*(5 + t2) + 
                  6*Power(t1,4)*(-21 + 5*t2) + 
                  Power(-1 + t2,2)*
                   (16 + 112*t2 + 11*Power(t2,2) + 3*Power(t2,3)) - 
                  Power(t1,3)*
                   (651 - 638*t2 + 82*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(76 + 645*t2 - 963*Power(t2,2) + 
                     275*Power(t2,3) - 33*Power(t2,4)) + 
                  2*Power(t1,2)*
                   (-214 + 797*t2 - 363*Power(t2,2) + 43*Power(t2,3) + 
                     Power(t2,4))) + 
               Power(s2,3)*(41 + 194*t2 - 324*Power(t2,2) + 
                  98*Power(t2,3) - 9*Power(t2,4) + 
                  5*Power(t1,4)*(2 + t2) + 
                  Power(t1,3)*(-225 + 63*t2 - 2*Power(t2,2)) - 
                  Power(t1,2)*
                   (724 - 790*t2 + 113*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-265 + 1118*t2 - 575*Power(t2,2) + 
                     72*Power(t2,3) + 2*Power(t2,4)))) - 
            Power(s1,3)*(-1 + t2)*
             (Power(s2,7)*(-7 + 3*t2) + 
               Power(s2,6)*(36 - 45*t2 + 7*Power(t2,2) - 
                  6*t1*(-7 + 3*t2)) + 
               Power(s2,5)*(-45 + 149*t2 - 69*Power(t2,2) + 
                  5*Power(t2,3) + 15*Power(t1,2)*(-7 + 3*t2) - 
                  2*t1*(99 - 118*t2 + 18*Power(t2,2))) + 
               Power(s2,4)*(9 - 151*t2 + 169*Power(t2,2) - 
                  28*Power(t2,3) + Power(t2,4) - 
                  20*Power(t1,3)*(-7 + 3*t2) + 
                  Power(t1,2)*(448 - 503*t2 + 75*Power(t2,2)) + 
                  t1*(225 - 699*t2 + 300*Power(t2,2) - 26*Power(t2,3))) \
+ t1*(Power(t1,5)*(15 - 7*t2) + Power(-1 + t2,4)*(5 + 12*t2) + 
                  Power(t1,4)*(42 - 91*t2 + 9*Power(t2,2)) + 
                  t1*Power(-1 + t2,3)*(-38 - 21*t2 + 18*Power(t2,2)) - 
                  Power(t1,2)*Power(-1 + t2,2)*
                   (-3 + 36*t2 + 35*Power(t2,2)) + 
                  Power(t1,3)*
                   (15 - 122*t2 + 92*Power(t2,2) + 15*Power(t2,3))) + 
               Power(s2,3)*(15*Power(t1,4)*(-7 + 3*t2) + 
                  Power(t1,3)*(-531 + 550*t2 - 79*Power(t2,2)) + 
                  2*Power(-1 + t2,2)*(-11 + 34*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*
                   (-448 + 1297*t2 - 498*Power(t2,2) + 49*Power(t2,3)) \
+ t1*(-44 + 589*t2 - 629*Power(t2,2) + 95*Power(t2,3) - 11*Power(t2,4))\
) + s2*(Power(t1,6)*(-7 + 3*t2) - Power(-1 + t2,4)*(5 + 12*t2) + 
                  Power(t1,5)*(-115 + 86*t2 - 9*Power(t2,2)) - 
                  t1*Power(-1 + t2,3)*(-79 - 33*t2 + 30*Power(t2,2)) + 
                  Power(t1,2)*Power(-1 + t2,2)*
                   (-33 + 152*t2 + 83*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(t1,4)*
                   (-217 + 528*t2 - 123*Power(t2,2) + 12*Power(t2,3)) \
+ Power(t1,3)*(-55 + 527*t2 - 469*Power(t2,2) + 5*Power(t2,3) - 
                     8*Power(t2,4))) + 
               Power(s2,2)*(-6*Power(t1,5)*(-7 + 3*t2) + 
                  Power(-1 + t2,3)*(-40 - 14*t2 + 13*Power(t2,2)) + 
                  Power(t1,4)*(345 - 317*t2 + 42*Power(t2,2)) + 
                  Power(t1,3)*
                   (443 - 1184*t2 + 381*Power(t2,2) - 40*Power(t2,3)) - 
                  t1*Power(-1 + t2,2)*
                   (-53 + 187*t2 + 67*Power(t2,2) + 3*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (25 - 281*t2 + 279*Power(t2,2) - 29*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            Power(-1 + t2,2)*
             (Power(t1,5)*(-2 + t2) + 
               Power(t1,4)*(4 - 6*t2 + Power(t2,2)) + 
               Power(s2,5)*t2*(1 - 2*t2 + 2*Power(t2,2)) + 
               Power(-1 + t2,4)*(-10 - 22*t2 + 9*Power(t2,2)) + 
               2*Power(t1,2)*Power(-1 + t2,2)*
                (3 - 26*t2 + 11*Power(t2,2)) - 
               t1*Power(-1 + t2,3)*(-8 - 44*t2 + 19*Power(t2,2)) + 
               Power(t1,3)*(10 - 41*t2 + 45*Power(t2,2) - 
                  14*Power(t2,3)) + 
               Power(s2,4)*(7 - 17*t2 + 16*Power(t2,2) - 
                  8*Power(t2,3) + Power(t2,4) - 
                  t1*(1 + t2 - Power(t2,2) + 4*Power(t2,3))) + 
               s2*(Power(t1,4)*(5 + t2 - Power(t2,2)) + 
                  2*t1*Power(-1 + t2,2)*
                   (-14 + 75*t2 - 41*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,3)*
                   (-16 + 27*t2 - 11*Power(t2,2) + 4*Power(t2,3)) - 
                  Power(-1 + t2,3)*
                   (21 + 53*t2 - 48*Power(t2,2) + 7*Power(t2,3)) + 
                  Power(t1,2)*
                   (-54 + 188*t2 - 197*Power(t2,2) + 67*Power(t2,3) - 
                     4*Power(t2,4))) + 
               Power(s2,3)*(-24 + 78*t2 - 83*Power(t2,2) + 
                  35*Power(t2,3) - 6*Power(t2,4) + 
                  Power(t1,2)*
                   (3 + 2*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(-21 + 43*t2 - 29*Power(t2,2) + 10*Power(t2,3) + 
                     Power(t2,4))) - 
               Power(s2,2)*(Power(t1,3)*(5 + 4*t2 + Power(t2,2)) + 
                  2*Power(-1 + t2,2)*
                   (-3 + 35*t2 - 26*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (-26 + 47*t2 - 23*Power(t2,2) + 6*Power(t2,3) + 
                     2*Power(t2,4)) - 
                  t1*(69 - 227*t2 + 233*Power(t2,2) - 80*Power(t2,3) + 
                     3*Power(t2,4) + 2*Power(t2,5)))) - 
            s1*Power(-1 + t2,2)*
             (Power(t1,6) - 2*Power(s2,6)*(-1 + t2)*t2 + 
               Power(t1,5)*(-7 + 4*t2) + 
               Power(t1,4)*(-40 + 81*t2 - 34*Power(t2,2)) + 
               2*Power(-1 + t2,4)*(-3 + t2 + 3*Power(t2,2)) + 
               t1*Power(-1 + t2,3)*(-28 - 80*t2 + 11*Power(t2,2)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (-21 - 164*t2 + 45*Power(t2,2)) + 
               Power(t1,3)*(-37 + 211*t2 - 231*Power(t2,2) + 
                  57*Power(t2,3)) + 
               Power(s2,5)*(21 - 30*t2 + 14*Power(t2,2) - 
                  4*Power(t2,3) + t1*(-2 - 6*t2 + 7*Power(t2,2))) + 
               Power(s2,4)*(-76 + 173*t2 - 110*Power(t2,2) + 
                  22*Power(t2,3) - 2*Power(t2,4) + 
                  Power(t1,2)*(8 + 6*t2 - 9*Power(t2,2)) + 
                  t1*(-84 + 106*t2 - 35*Power(t2,2) + 6*Power(t2,3))) + 
               Power(s2,3)*(31 - 264*t2 + 386*Power(t2,2) - 
                  183*Power(t2,3) + 29*Power(t2,4) + Power(t2,5) + 
                  Power(t1,3)*(-13 - 2*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*(135 - 152*t2 + 35*Power(t2,2)) + 
                  t1*(288 - 632*t2 + 359*Power(t2,2) - 40*Power(t2,3) - 
                     3*Power(t2,4))) + 
               s2*(-5*Power(t1,5) + 
                  Power(t1,4)*(44 - 38*t2 + 7*Power(t2,2)) + 
                  Power(t1,3)*
                   (218 - 461*t2 + 234*Power(t2,2) - 19*Power(t2,3)) + 
                  Power(-1 + t2,3)*
                   (28 + 80*t2 - 13*Power(t2,2) + 2*Power(t2,3)) - 
                  t1*Power(-1 + t2,2)*
                   (68 + 371*t2 - 181*Power(t2,2) + 22*Power(t2,3)) + 
                  Power(t1,2)*
                   (147 - 812*t2 + 975*Power(t2,2) - 342*Power(t2,3) + 
                     32*Power(t2,4))) - 
               Power(s2,2)*(Power(t1,4)*(-11 + Power(t2,2)) + 
                  Power(t1,3)*
                   (109 - 110*t2 + 21*Power(t2,2) + 2*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (57 + 180*t2 - 112*Power(t2,2) + 15*Power(t2,3)) + 
                  Power(t1,2)*
                   (390 - 839*t2 + 449*Power(t2,2) - 37*Power(t2,3) - 
                     5*Power(t2,4)) + 
                  t1*(139 - 856*t2 + 1114*Power(t2,2) - 
                     454*Power(t2,3) + 55*Power(t2,4) + 2*Power(t2,5))))) \
+ s*(Power(s1,6)*Power(s2 - t1,2)*
             (-8*Power(s2,3) + 8*Power(t1,3) - 
               s2*(24*Power(t1,2) - 28*t1*(-1 + t2) + 
                  5*Power(-1 + t2,2)) + 
               2*Power(s2,2)*(7 + 12*t1 - 7*t2) - 
               14*Power(t1,2)*(-1 + t2) + 5*t1*Power(-1 + t2,2) + 
               Power(-1 + t2,3))*(-1 + t2) - 
            Power(s1,5)*(s2 - t1)*
             (-(Power(t1,2)*(-75 + t2)*Power(-1 + t2,3)) - 
               2*Power(-1 + t2,5) + 2*Power(s2,5)*t2 - 
               2*Power(t1,5)*t2 - 4*t1*Power(-1 + t2,4)*(5 + 2*t2) + 
               2*Power(t1,3)*Power(-1 + t2,2)*(-38 + 11*t2) + 
               Power(t1,4)*(-27 + 38*t2 - 11*Power(t2,2)) - 
               2*Power(s2,4)*(14 + 5*(-4 + t1)*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(-20*Power(t1,3)*t2 + 
                  Power(-1 + t2,3)*(73 + t2) + 
                  2*t1*Power(-1 + t2,2)*(-112 + 31*t2) - 
                  3*Power(t1,2)*(55 - 78*t2 + 23*Power(t2,2))) + 
               s2*(-148*t1*Power(-1 + t2,3) + 10*Power(t1,4)*t2 + 
                  7*Power(-1 + t2,4)*(3 + t2) - 
                  2*Power(t1,2)*Power(-1 + t2,2)*(-113 + 32*t2) + 
                  Power(t1,3)*(109 - 154*t2 + 45*Power(t2,2))) + 
               Power(s2,3)*(20*Power(t1,2)*t2 - 
                  2*Power(-1 + t2,2)*(-37 + 10*t2) + 
                  t1*(111 - 158*t2 + 47*Power(t2,2)))) + 
            Power(s1,4)*(-1 + t2)*
             (4*Power(s2,7) + Power(-1 + t2,5) - 
               Power(s2,6)*(17 + 24*t1 + t2) - 
               Power(t1,6)*(15 + 7*t2) + 
               2*t1*Power(-1 + t2,4)*(13 + 8*t2) + 
               Power(t1,3)*Power(-1 + t2,2)*
                (187 - 158*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*Power(-1 + t2,3)*
                (-154 + 31*t2 + 11*Power(t2,2)) + 
               Power(t1,5)*(23 - 59*t2 + 34*Power(t2,2)) + 
               Power(t1,4)*(128 - 301*t2 + 213*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(s2,5)*(-6 + 60*Power(t1,2) + 38*t2 - 
                  30*Power(t2,2) + t1*(99 + 13*t2)) - 
               Power(s2,4)*(-99 + 80*Power(t1,3) + 231*t2 - 
                  160*Power(t2,2) + 28*Power(t2,3) + 
                  Power(t1,2)*(241 + 49*t2) + 
                  t1*(-40 + 197*t2 - 147*Power(t2,2))) + 
               s2*(4*Power(t1,6) - 3*Power(-1 + t2,4)*(9 + 5*t2) + 
                  Power(t1,5)*(91 + 37*t2) + 
                  Power(t1,4)*(-91 + 260*t2 - 159*Power(t2,2)) - 
                  Power(t1,2)*Power(-1 + t2,2)*
                   (566 - 474*t2 + Power(t2,2)) - 
                  t1*Power(-1 + t2,3)*
                   (-309 + 68*t2 + 17*Power(t2,2)) + 
                  5*Power(t1,3)*
                   (-95 + 222*t2 - 155*Power(t2,2) + 28*Power(t2,3))) + 
               Power(s2,3)*(60*Power(t1,4) + 
                  Power(t1,3)*(314 + 86*t2) + 
                  Power(t1,2)*(-107 + 422*t2 - 295*Power(t2,2)) + 
                  Power(-1 + t2,2)*(-188 + 150*t2 + 7*Power(t2,2)) + 
                  t1*(-417 + 970*t2 - 669*Power(t2,2) + 
                     116*Power(t2,3))) - 
               Power(s2,2)*(24*Power(t1,5) + 
                  Power(t1,4)*(231 + 79*t2) + 
                  Power(t1,3)*(-141 + 464*t2 - 303*Power(t2,2)) + 
                  t1*Power(-1 + t2,2)*(-567 + 466*t2 + 8*Power(t2,2)) - 
                  Power(-1 + t2,3)*(-151 + 29*t2 + 10*Power(t2,2)) + 
                  Power(t1,2)*
                   (-665 + 1548*t2 - 1071*Power(t2,2) + 188*Power(t2,3))\
)) - Power(s1,3)*(Power(-1 + t2,6)*(11 + 8*t2) + 
               Power(s2,7)*(9 - 14*t2 + 5*Power(t2,2)) + 
               2*t1*Power(-1 + t2,5)*(-72 + 32*t2 + 11*Power(t2,2)) - 
               Power(t1,2)*Power(-1 + t2,4)*
                (-195 + 381*t2 - 69*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,6)*(22 - 33*t2 + 10*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,3)*Power(-1 + t2,3)*
                (-325 + 561*t2 - 241*Power(t2,2) + 12*Power(t2,3)) - 
               Power(t1,4)*Power(-1 + t2,2)*
                (-151 + 272*t2 - 209*Power(t2,2) + 21*Power(t2,3)) + 
               Power(t1,5)*(-54 + 105*t2 - 2*Power(t2,2) - 
                  63*Power(t2,3) + 14*Power(t2,4)) - 
               Power(s2,6)*(39 - 82*t2 + 45*Power(t2,2) + 
                  6*t1*(9 - 14*t2 + 5*Power(t2,2))) + 
               Power(s2,5)*(37 - 89*t2 + 15*Power(t2,2) + 
                  57*Power(t2,3) - 20*Power(t2,4) + 
                  15*Power(t1,2)*(9 - 14*t2 + 5*Power(t2,2)) + 
                  t1*(230 - 478*t2 + 266*Power(t2,2) - 6*Power(t2,3))) \
- Power(s2,4)*(20*Power(t1,3)*(9 - 14*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (556 - 1137*t2 + 636*Power(t2,2) - 25*Power(t2,3)) \
+ Power(-1 + t2,2)*(-56 + 155*t2 - 185*Power(t2,2) + 19*Power(t2,3)) + 
                  t1*(220 - 555*t2 + 236*Power(t2,2) + 
                     153*Power(t2,3) - 54*Power(t2,4))) + 
               Power(s2,3)*(15*Power(t1,4)*
                   (9 - 14*t2 + 5*Power(t2,2)) - 
                  Power(-1 + t2,3)*
                   (-122 + 330*t2 - 205*Power(t2,2) + 4*Power(t2,3)) - 
                  4*Power(t1,3)*
                   (-175 + 349*t2 - 193*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*Power(-1 + t2,2)*
                   (-296 + 648*t2 - 655*Power(t2,2) + 35*Power(t2,3)) \
+ Power(t1,2)*(494 - 1244*t2 + 632*Power(t2,2) + 172*Power(t2,3) - 
                     54*Power(t2,4))) + 
               s2*(Power(t1,6)*(9 - 14*t2 + 5*Power(t2,2)) - 
                  Power(-1 + t2,5)*(-139 + 64*t2 + 17*Power(t2,2)) + 
                  2*Power(t1,5)*
                   (83 - 147*t2 + 69*Power(t2,2) + Power(t2,3)) + 
                  t1*Power(-1 + t2,4)*
                   (-365 + 707*t2 - 110*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(t1,2)*Power(-1 + t2,3)*
                   (-745 + 1396*t2 - 656*Power(t2,2) + 26*Power(t2,3)) \
+ Power(t1,3)*Power(-1 + t2,2)*
                   (-494 + 906*t2 - 727*Power(t2,2) + 47*Power(t2,3)) \
+ Power(t1,4)*(273 - 611*t2 + 209*Power(t2,2) + 163*Power(t2,3) - 
                     34*Power(t2,4))) + 
               Power(s2,2)*(-6*Power(t1,5)*
                   (9 - 14*t2 + 5*Power(t2,2)) + 
                  Power(-1 + t2,4)*(167 - 316*t2 + 30*Power(t2,2)) + 
                  t1*Power(-1 + t2,3)*
                   (-536 + 1147*t2 - 602*Power(t2,2) + 12*Power(t2,3)) \
+ Power(t1,4)*(-479 + 916*t2 - 485*Power(t2,2) + 18*Power(t2,3)) - 
                  Power(t1,2)*Power(-1 + t2,2)*
                   (-583 + 1127*t2 - 988*Power(t2,2) + 42*Power(t2,3)) \
+ 2*Power(t1,3)*(-265 + 642*t2 - 309*Power(t2,2) - 88*Power(t2,3) + 
                     20*Power(t2,4)))) + 
            Power(-1 + t2,2)*
             (Power(t1,5)*(-12 + 11*t2 - 2*Power(t2,2)) + 
               Power(s2,5)*t2*(5 - 10*t2 + 8*Power(t2,2)) + 
               Power(t1,4)*(28 - 36*t2 + 5*Power(t2,3)) + 
               Power(-1 + t2,4)*
                (-53 - 64*t2 + 27*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (9 - 155*t2 + 91*Power(t2,2) + 6*Power(t2,3)) - 
               t1*Power(-1 + t2,3)*
                (-52 - 81*t2 + 49*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(40 - 185*t2 + 227*Power(t2,2) - 
                  78*Power(t2,3) - 4*Power(t2,4)) - 
               Power(s2,4)*(-31 + 75*t2 - 73*Power(t2,2) + 
                  32*Power(t2,3) + 
                  t1*(6 + 2*t2 - 6*Power(t2,2) + 13*Power(t2,3))) + 
               Power(s2,3)*(-109 + 356*t2 - 387*Power(t2,2) + 
                  161*Power(t2,3) - 21*Power(t2,4) + 
                  Power(t1,2)*
                   (18 + t2 + 8*Power(t2,2) + 3*Power(t2,3)) + 
                  2*t1*(-47 + 89*t2 - 61*Power(t2,2) + 
                     19*Power(t2,3) + 6*Power(t2,4))) + 
               Power(s2,2)*(Power(t1,3)*
                   (-30 - 5*t2 + 4*Power(t2,2) + Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-23 + 287*t2 - 278*Power(t2,2) + 63*Power(t2,3)) + 
                  Power(t1,2)*
                   (128 - 199*t2 + 91*Power(t2,2) - 31*Power(t2,3) - 
                     7*Power(t2,4)) + 
                  t1*(321 - 1068*t2 + 1130*Power(t2,2) - 
                     391*Power(t2,3) + 2*Power(t2,4) + 6*Power(t2,5))) \
+ s2*(Power(t1,4)*(30 - 10*t2 - 6*Power(t2,2) + Power(t2,3)) - 
                  Power(-1 + t2,3)*
                   (109 + 172*t2 - 238*Power(t2,2) + 31*Power(t2,3)) + 
                  Power(t1,3)*
                   (-93 + 132*t2 - 42*Power(t2,2) + 20*Power(t2,3) - 
                     5*Power(t2,4)) - 
                  t1*Power(-1 + t2,2)*
                   (124 - 597*t2 + 406*Power(t2,2) - 34*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  Power(t1,2)*
                   (-246 + 886*t2 - 986*Power(t2,2) + 362*Power(t2,3) - 
                     23*Power(t2,4) + 7*Power(t2,5)))) + 
            Power(s1,2)*(-1 + t2)*
             (3*Power(s2,7)*(-1 + t2)*t2 - 
               Power(t1,6)*(10 - 11*t2 + Power(t2,2)) + 
               Power(-1 + t2,5)*(-47 + 34*t2 + 11*Power(t2,2)) - 
               2*t1*Power(-1 + t2,4)*
                (-28 + 178*t2 - 67*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,5)*(105 - 169*t2 + 62*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*Power(-1 + t2,3)*
                (-349 + 530*t2 - 334*Power(t2,2) + 29*Power(t2,3)) - 
               Power(t1,3)*Power(-1 + t2,2)*
                (-286 + 238*t2 - 275*Power(t2,2) + 56*Power(t2,3)) + 
               Power(t1,4)*(-63 + 286*t2 - 187*Power(t2,2) - 
                  76*Power(t2,3) + 40*Power(t2,4)) + 
               Power(s2,6)*(-1 + t2)*
                (49 - 21*t2 + 2*Power(t2,2) - 2*t1*(2 + 7*t2)) + 
               Power(s2,5)*(175 - 394*t2 + 252*Power(t2,2) - 
                  22*Power(t2,3) - 5*Power(t2,4) + 
                  5*Power(t1,2)*(-4 - t2 + 5*Power(t2,2)) + 
                  t1*(248 - 337*t2 + 86*Power(t2,2) + 3*Power(t2,3))) + 
               s2*(-(Power(t1,6)*(4 - 5*t2 + Power(t2,2))) + 
                  Power(t1,5)*
                   (98 - 123*t2 + 24*Power(t2,2) + Power(t2,3)) + 
                  Power(-1 + t2,4)*
                   (-26 + 293*t2 - 103*Power(t2,2) + 6*Power(t2,3)) - 
                  t1*Power(-1 + t2,3)*
                   (-431 + 788*t2 - 644*Power(t2,2) + 39*Power(t2,3)) \
+ Power(t1,2)*Power(-1 + t2,2)*
                   (-731 + 338*t2 - 451*Power(t2,2) + 
                     39*Power(t2,3) + 4*Power(t2,4)) + 
                  Power(t1,4)*
                   (672 - 1280*t2 + 699*Power(t2,2) - 
                     66*Power(t2,3) + 5*Power(t2,4)) + 
                  Power(t1,3)*
                   (370 - 1783*t2 + 1853*Power(t2,2) - 
                     470*Power(t2,3) + 39*Power(t2,4) - 9*Power(t2,5))) \
- Power(s2,4)*(20*Power(t1,3)*(-2 + t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (518 - 681*t2 + 141*Power(t2,2) + 22*Power(t2,3)) + 
                  t1*(866 - 1889*t2 + 1167*Power(t2,2) - 
                     86*Power(t2,3) - 28*Power(t2,4)) + 
                  2*(23 - 201*t2 + 260*Power(t2,2) - 85*Power(t2,3) + 
                     Power(t2,4) + 2*Power(t2,5))) + 
               Power(s2,3)*(5*Power(t1,4)*(-8 + 7*t2 + Power(t2,2)) + 
                  2*Power(t1,3)*
                   (283 - 366*t2 + 69*Power(t2,2) + 14*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (-203 + 43*t2 - 135*Power(t2,2) + 28*Power(t2,3)) + 
                  Power(t1,2)*
                   (1669 - 3538*t2 + 2153*Power(t2,2) - 
                     188*Power(t2,3) - 36*Power(t2,4)) + 
                  t1*(328 - 1977*t2 + 2447*Power(t2,2) - 
                     894*Power(t2,3) + 91*Power(t2,4) + 5*Power(t2,5))) \
+ Power(s2,2)*(2*Power(t1,5)*(10 - 11*t2 + Power(t2,2)) + 
                  2*Power(-1 + t2,3)*
                   (-56 + 157*t2 - 166*Power(t2,2) + 3*Power(t2,3)) - 
                  Power(t1,4)*
                   (335 - 430*t2 + 83*Power(t2,2) + 12*Power(t2,3)) - 
                  t1*Power(-1 + t2,2)*
                   (-639 + 114*t2 - 278*Power(t2,2) - 4*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(t1,3)*
                   (-1545 + 3154*t2 - 1875*Power(t2,2) + 
                     198*Power(t2,3) + 8*Power(t2,4)) + 
                  Power(t1,2)*
                   (-589 + 3072*t2 - 3593*Power(t2,2) + 
                     1270*Power(t2,3) - 168*Power(t2,4) + 8*Power(t2,5))\
)) + s1*Power(-1 + t2,2)*(8*Power(s2,6)*(-1 + t2)*t2 + 
               Power(t1,6)*(-5 + 2*t2) + 
               Power(t1,5)*(32 - 19*t2 - 4*Power(t2,2)) + 
               Power(-1 + t2,4)*
                (19 + 107*t2 - 67*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(118 - 271*t2 + 129*Power(t2,2) + 
                  9*Power(t2,3)) - 
               t1*Power(-1 + t2,3)*
                (-173 + 67*t2 - 106*Power(t2,2) + 22*Power(t2,3)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-214 - 48*t2 - 50*Power(t2,2) + 45*Power(t2,3)) + 
               Power(t1,3)*(35 - 375*t2 + 434*Power(t2,2) - 
                  62*Power(t2,3) - 32*Power(t2,4)) + 
               Power(s2,5)*(-71 + 100*t2 - 43*Power(t2,2) + 
                  11*Power(t2,3) + t1*(10 + 20*t2 - 27*Power(t2,2))) + 
               Power(s2,4)*(249 - 567*t2 + 355*Power(t2,2) - 
                  48*Power(t2,3) - 4*Power(t2,4) + 
                  Power(t1,2)*(-40 - 8*t2 + 33*Power(t2,2)) + 
                  t1*(288 - 347*t2 + 84*Power(t2,2) - 4*Power(t2,3))) - 
               Power(s2,3)*(49 - 656*t2 + 1094*Power(t2,2) - 
                  583*Power(t2,3) + 84*Power(t2,4) + 12*Power(t2,5) + 
                  Power(t1,3)*(-65 + 18*t2 + 17*Power(t2,2)) + 
                  3*Power(t1,2)*
                   (161 - 173*t2 + 23*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(981 - 2191*t2 + 1263*Power(t2,2) - 
                     80*Power(t2,3) - 33*Power(t2,4))) + 
               Power(s2,2)*(Power(t1,4)*(-55 + 22*t2 + 3*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (209 - 208*t2 + 27*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,2)*
                   (1348 - 3034*t2 + 1746*Power(t2,2) - 
                     133*Power(t2,3) - 17*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-262 - 244*t2 + 255*Power(t2,2) - 19*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(331 - 2297*t2 + 3287*Power(t2,2) - 
                     1543*Power(t2,3) + 221*Power(t2,4) + Power(t2,5))) \
+ s2*(-5*Power(t1,5)*(-5 + 2*t2) + 
                  Power(t1,4)*
                   (-184 + 163*t2 - 22*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(-1 + t2,3)*
                   (-114 + 40*t2 - 129*Power(t2,2) + 13*Power(t2,3)) + 
                  Power(t1,3)*
                   (-734 + 1681*t2 - 967*Power(t2,2) + 92*Power(t2,3) - 
                     12*Power(t2,4)) - 
                  t1*Power(-1 + t2,2)*
                   (-435 - 439*t2 + 377*Power(t2,2) - 41*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  Power(t1,2)*
                   (-330 + 2069*t2 - 2709*Power(t2,2) + 
                     1080*Power(t2,3) - 122*Power(t2,4) + 12*Power(t2,5))\
))) + Power(s,2)*(3*Power(s1,6)*Power(s2 - t1,2)*(-1 + t2)*
             (4*Power(s2,2) + 4*Power(t1,2) - 7*t1*(-1 + t2) + 
               3*Power(-1 + t2,2) + s2*(-7 - 8*t1 + 7*t2)) + 
            Power(s1,5)*(2*Power(s2,5)*(-8 + 11*t2) + 
               Power(s2,4)*(-2 + 81*t1 - 12*t2 - 111*t1*t2 + 
                  14*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3)*(166 - 226*t2) + 
                  96*Power(t1,2)*(-1 + t2)*t2 - 
                  Power(-1 + t2,3)*(-79 + 12*t2) + 
                  4*t1*Power(-1 + t2,2)*(-49 + 19*t2)) + 
               s2*(Power(-1 + t2,4)*(28 + 3*t2) + 
                  2*t1*Power(-1 + t2,3)*(-85 + 18*t2) + 
                  6*Power(t1,4)*(-14 + 19*t2) - 
                  Power(t1,2)*Power(-1 + t2,2)*(-203 + 83*t2) + 
                  Power(t1,3)*(-4 + 72*t2 - 68*Power(t2,2))) + 
               Power(s2,3)*(-(Power(-1 + t2,2)*(-63 + 23*t2)) + 
                  4*Power(t1,2)*(-41 + 56*t2) + 
                  t1*(4 + 56*t2 - 60*Power(t2,2))) - 
               t1*(Power(-1 + t2,4)*(30 + t2) - 
                  10*Power(t1,2)*Power(-1 + t2,2)*(-7 + 3*t2) + 
                  Power(t1,4)*(-17 + 23*t2) + 
                  t1*Power(-1 + t2,3)*(-91 + 24*t2) - 
                  2*Power(t1,3)*(1 - 10*t2 + 9*Power(t2,2)))) + 
            Power(s1,4)*(Power(t1,6)*(7 - 5*t2) - 
               7*Power(s2,6)*(-1 + t2) + Power(-1 + t2,5)*(19 + t2) + 
               t1*Power(-1 + t2,4)*(-107 + 73*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*Power(-1 + t2,3)*
                (23 - 124*t2 + 33*Power(t2,2)) - 
               Power(t1,3)*Power(-1 + t2,2)*
                (-118 + 48*t2 + 41*Power(t2,2)) + 
               Power(t1,5)*(97 - 135*t2 + 42*Power(t2,2)) + 
               Power(t1,4)*(193 - 404*t2 + 244*Power(t2,2) - 
                  33*Power(t2,3)) + 
               Power(s2,5)*(-64 + 80*t2 - 20*Power(t2,2) + 
                  t1*(-41 + 39*t2)) + 
               Power(s2,4)*(152 + Power(t1,2)*(101 - 91*t2) - 266*t2 + 
                  91*Power(t2,2) + 23*Power(t2,3) + 
                  t1*(340 - 429*t2 + 109*Power(t2,2))) + 
               Power(s2,2)*(Power(t1,4)*(101 - 81*t2) + 
                  Power(-1 + t2,3)*(22 - 101*t2 + 11*Power(t2,2)) - 
                  t1*Power(-1 + t2,2)*
                   (-407 + 200*t2 + 120*Power(t2,2)) + 
                  Power(t1,3)*(799 - 1052*t2 + 293*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (-347 + 676*t2 - 341*Power(t2,2) + 12*Power(t2,3))) \
+ Power(s2,3)*(2*Power(t1,3)*(-67 + 57*t2) + 
                  Power(t1,2)*(-733 + 942*t2 - 249*Power(t2,2)) + 
                  Power(-1 + t2,2)*(-129 + 45*t2 + 55*Power(t2,2)) - 
                  t1*(652 - 1211*t2 + 526*Power(t2,2) + 33*Power(t2,3))\
) + s2*(Power(t1,5)*(-41 + 31*t2) + 
                  Power(t1,4)*(-439 + 594*t2 - 175*Power(t2,2)) - 
                  Power(-1 + t2,4)*(-95 + 57*t2 + 8*Power(t2,2)) - 
                  t1*Power(-1 + t2,3)*(29 - 193*t2 + 28*Power(t2,2)) + 
                  Power(t1,2)*Power(-1 + t2,2)*
                   (-396 + 203*t2 + 106*Power(t2,2)) + 
                  Power(t1,3)*
                   (-734 + 1487*t2 - 832*Power(t2,2) + 79*Power(t2,3)))) \
- Power(s1,3)*(Power(s2,7)*(-1 + t2) + 
               Power(s2,6)*(-11 - 6*t1*(-1 + t2) + 21*t2 - 
                  18*Power(t2,2)) + Power(t1,6)*(t2 - 7*Power(t2,2)) + 
               Power(-1 + t2,5)*(-37 + 45*t2 + 4*Power(t2,2)) - 
               t1*Power(-1 + t2,4)*
                (134 + 148*t2 - 119*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*Power(-1 + t2,3)*
                (239 - 271*t2 - 65*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,3)*Power(-1 + t2,2)*
                (-610 + 949*t2 - 319*Power(t2,2) + 15*Power(t2,3)) + 
               Power(t1,5)*(-52 + 177*t2 - 161*Power(t2,2) + 
                  36*Power(t2,3)) + 
               Power(t1,4)*(-454 + 1290*t2 - 1250*Power(t2,2) + 
                  463*Power(t2,3) - 49*Power(t2,4)) + 
               Power(s2,5)*(48 + 15*Power(t1,2)*(-1 + t2) - 187*t2 + 
                  170*Power(t2,2) - 31*Power(t2,3) + 
                  t1*(32 - 62*t2 + 76*Power(t2,2))) + 
               Power(s2,4)*(-155 - 20*Power(t1,3)*(-1 + t2) + 547*t2 - 
                  604*Power(t2,2) + 204*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t1,2)*(-12 + 27*t2 - 125*Power(t2,2)) + 
                  t1*(-204 + 765*t2 - 641*Power(t2,2) + 80*Power(t2,3))\
) + Power(s2,3)*(15*Power(t1,4)*(-1 + t2) + 
                  2*Power(t1,3)*(-23 + 40*t2 + 53*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (240 - 371*t2 + 70*Power(t2,2) + 26*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (-189 + 678*t2 - 535*Power(t2,2) + 46*Power(t2,3)) \
+ t1*(862 - 2701*t2 + 2714*Power(t2,2) - 841*Power(t2,3) - 
                     34*Power(t2,4))) - 
               Power(s2,2)*(6*Power(t1,5)*(-1 + t2) + 
                  Power(t1,4)*(-55 + 97*t2 + 58*Power(t2,2)) - 
                  Power(-1 + t2,3)*
                   (266 - 184*t2 - 177*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*Power(-1 + t2,2)*
                   (1044 - 1622*t2 + 459*Power(t2,2) + 14*Power(t2,3)) \
- 2*Power(t1,3)*(-194 + 671*t2 - 529*Power(t2,2) + 52*Power(t2,3)) + 
                  Power(t1,2)*
                   (1735 - 5139*t2 + 4998*Power(t2,2) - 
                     1621*Power(t2,3) + 27*Power(t2,4))) + 
               s2*(Power(t1,6)*(-1 + t2) + 
                  2*Power(t1,5)*(-9 + 15*t2 + 13*Power(t2,2)) - 
                  Power(-1 + t2,4)*(-128 - 107*t2 + 69*Power(t2,2)) + 
                  Power(t1,4)*
                   (218 - 741*t2 + 620*Power(t2,2) - 97*Power(t2,3)) + 
                  t1*Power(-1 + t2,3)*
                   (-544 + 556*t2 + 157*Power(t2,2) + 9*Power(t2,3)) - 
                  Power(t1,2)*Power(-1 + t2,2)*
                   (-1428 + 2242*t2 - 750*Power(t2,2) + 41*Power(t2,3)) \
+ Power(t1,3)*(1482 - 4275*t2 + 4138*Power(t2,2) - 1447*Power(t2,3) + 
                     102*Power(t2,4)))) + 
            (-1 + t2)*(Power(s2,5)*t2*(10 - 20*t2 + 13*Power(t2,2)) + 
               Power(t1,5)*(-30 + 40*t2 - 14*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(-1 + t2,4)*
                (-115 - 29*t2 - 8*Power(t2,2) + 27*Power(t2,3)) + 
               Power(t1,4)*(84 - 111*t2 - 4*Power(t2,2) + 
                  32*Power(t2,3) - 4*Power(t2,4)) + 
               t1*Power(-1 + t2,3)*
                (150 - 175*t2 + 57*Power(t2,2) - 46*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (54 + 57*t2 - 126*Power(t2,2) - 13*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,3)*(51 - 347*t2 + 521*Power(t2,2) - 
                  214*Power(t2,3) - 17*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,4)*(54 - 128*t2 + 123*Power(t2,2) - 
                  45*Power(t2,3) - 7*Power(t2,4) + 
                  t1*(-15 + 5*t2 + 11*Power(t2,2) - 16*Power(t2,3))) + 
               Power(s2,3)*(-189 + 635*t2 - 700*Power(t2,2) + 
                  269*Power(t2,3) - 18*Power(t2,4) + 3*Power(t2,5) - 
                  Power(t1,2)*
                   (-45 + 25*t2 - 11*Power(t2,2) + Power(t2,3)) + 
                  2*t1*(-84 + 139*t2 - 81*Power(t2,2) + 
                     18*Power(t2,3) + 14*Power(t2,4))) - 
               Power(s2,2)*(Power(t1,3)*
                   (75 - 35*t2 - 11*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-267 + 349*t2 - 95*Power(t2,2) + 31*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (-12 + 439*t2 - 593*Power(t2,2) + 
                     137*Power(t2,3) + 5*Power(t2,4)) + 
                  t1*(-580 + 2020*t2 - 2218*Power(t2,2) + 
                     735*Power(t2,3) + 49*Power(t2,4) - 6*Power(t2,5))) \
+ s2*(Power(t1,4)*(75 - 65*t2 + Power(t2,2) + 4*Power(t2,3)) - 
                  Power(-1 + t2,3)*
                   (242 + 145*t2 - 451*Power(t2,2) + 51*Power(t2,3)) + 
                  Power(t1,3)*
                   (-237 + 310*t2 - 52*Power(t2,2) + 8*Power(t2,3) - 
                     17*Power(t2,4)) - 
                  t1*Power(-1 + t2,2)*
                   (177 - 848*t2 + 784*Power(t2,2) - 74*Power(t2,3) + 
                     9*Power(t2,4)) + 
                  Power(t1,2)*
                   (-428 + 1713*t2 - 2103*Power(t2,2) + 
                     846*Power(t2,3) - 50*Power(t2,4) + 22*Power(t2,5)))\
) + s1*(-1 + t2)*(12*Power(s2,6)*(-1 + t2)*t2 - 
               Power(t1,6)*(10 - 8*t2 + Power(t2,2)) + 
               Power(-1 + t2,4)*
                (55 + 348*t2 - 192*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,5)*(60 - 41*t2 - 14*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,4)*(78 - 277*t2 + 159*Power(t2,2) + 
                  39*Power(t2,3) - 8*Power(t2,4)) + 
               2*t1*Power(-1 + t2,3)*
                (161 - 358*t2 + 256*Power(t2,2) - 46*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (615 - 1137*t2 + 700*Power(t2,2) - 211*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(t1,3)*(-228 + 520*t2 - 567*Power(t2,2) + 
                  427*Power(t2,3) - 162*Power(t2,4) + 10*Power(t2,5)) + 
               Power(s2,5)*(-86 + 112*t2 - 32*Power(t2,2) + 
                  3*Power(t2,3) + t1*(20 + 20*t2 - 37*Power(t2,2))) + 
               Power(s2,4)*(261 - 616*t2 + 366*Power(t2,2) + 
                  15*Power(t2,3) - 35*Power(t2,4) + 
                  Power(t1,2)*(-80 + 28*t2 + 37*Power(t2,2)) + 
                  t1*(364 - 387*t2 - 6*Power(t2,2) + 50*Power(t2,3))) + 
               Power(s2,3)*(126 + 141*t2 - 731*Power(t2,2) + 
                  577*Power(t2,3) - 86*Power(t2,4) - 27*Power(t2,5) - 
                  2*Power(t1,3)*(-65 + 46*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*
                   (-671 + 675*t2 + 39*Power(t2,2) - 97*Power(t2,3)) + 
                  t1*(-1134 + 2723*t2 - 1655*Power(t2,2) + 
                     10*Power(t2,3) + 92*Power(t2,4))) + 
               Power(s2,2)*(-2*Power(t1,4)*
                   (55 - 44*t2 + 4*Power(t2,2)) + 
                  6*Power(t1,3)*
                   (109 - 113*t2 + 9*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (1606 - 4082*t2 + 2734*Power(t2,2) - 
                     303*Power(t2,3) - 9*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-482 + 511*t2 - 100*Power(t2,2) + 
                     73*Power(t2,3) + 24*Power(t2,4)) - 
                  t1*(83 + 1085*t2 - 2589*Power(t2,2) + 
                     1841*Power(t2,3) - 463*Power(t2,4) + 
                     43*Power(t2,5))) + 
               s2*(5*Power(t1,5)*(10 - 8*t2 + Power(t2,2)) + 
                  Power(t1,4)*
                   (-321 + 319*t2 - 41*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,3)*
                   (-811 + 2252*t2 - 1604*Power(t2,2) + 
                     239*Power(t2,3) - 40*Power(t2,4)) - 
                  Power(-1 + t2,3)*
                   (116 - 671*t2 + 642*Power(t2,2) - 62*Power(t2,3) + 
                     3*Power(t2,4)) - 
                  t1*Power(-1 + t2,2)*
                   (-1026 + 1274*t2 - 271*Power(t2,2) + 
                     64*Power(t2,3) + 11*Power(t2,4)) + 
                  Power(t1,2)*
                   (153 + 537*t2 - 1423*Power(t2,2) + 875*Power(t2,3) - 
                     187*Power(t2,4) + 45*Power(t2,5)))) + 
            Power(s1,2)*(3*Power(s2,7)*(-1 + t2)*t2 - 
               3*Power(t1,6)*(4 - 5*t2 + Power(t2,2)) - 
               Power(-1 + t2,5)*
                (99 + 45*t2 - 83*Power(t2,2) + 3*Power(t2,3)) + 
               t1*Power(-1 + t2,4)*
                (133 - 655*t2 + 172*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,2)*Power(-1 + t2,3)*
                (708 - 1555*t2 + 783*Power(t2,2) - 79*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,5)*(-38 + 52*t2 + 15*Power(t2,2) - 
                  37*Power(t2,3) + 2*Power(t2,4)) + 
               Power(t1,3)*Power(-1 + t2,2)*
                (803 - 1656*t2 + 997*Power(t2,2) - 207*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(t1,4)*(241 - 759*t2 + 1020*Power(t2,2) - 
                  667*Power(t2,3) + 171*Power(t2,4) - 6*Power(t2,5)) - 
               3*Power(s2,6)*(-1 + t2)*
                (-11 + 3*t2 + 2*Power(t2,2) + t1*(2 + 4*t2)) + 
               Power(s2,5)*(61 - 138*t2 + 54*Power(t2,2) + 
                  44*Power(t2,3) - 15*Power(t2,4) + 
                  15*Power(t1,2)*(-2 + t2 + Power(t2,2)) + 
                  3*t1*(59 - 72*t2 - Power(t2,2) + 14*Power(t2,3))) - 
               Power(s2,4)*(-179 + 60*Power(t1,3)*(-1 + t2) + 537*t2 - 
                  663*Power(t2,2) + 365*Power(t2,3) - 60*Power(t2,4) + 
                  Power(t1,2)*
                   (404 - 507*t2 + 15*Power(t2,2) + 88*Power(t2,3)) + 
                  t1*(416 - 984*t2 + 576*Power(t2,2) + 67*Power(t2,3) - 
                     45*Power(t2,4))) + 
               Power(s2,3)*(-15*Power(t1,4)*(4 - 5*t2 + Power(t2,2)) + 
                  12*Power(t1,3)*
                   (41 - 55*t2 + 8*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (952 - 2302*t2 + 1575*Power(t2,2) - 
                     148*Power(t2,3) - 17*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-331 + 974*t2 - 670*Power(t2,2) + 78*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(-526 + 1311*t2 - 1302*Power(t2,2) + 
                     475*Power(t2,3) + 78*Power(t2,4) - 36*Power(t2,5))) \
- s2*(3*Power(t1,6)*(2 - 3*t2 + Power(t2,2)) + 
                  Power(t1,5)*
                   (-107 + 156*t2 - 51*Power(t2,2) + 2*Power(t2,3)) - 
                  Power(-1 + t2,4)*
                   (-80 + 444*t2 - 35*Power(t2,2) + 15*Power(t2,3)) + 
                  Power(t1,4)*
                   (-379 + 852*t2 - 555*Power(t2,2) + 76*Power(t2,3) - 
                     24*Power(t2,4)) + 
                  Power(t1,2)*Power(-1 + t2,2)*
                   (1867 - 3954*t2 + 2205*Power(t2,2) - 
                     290*Power(t2,3) + Power(t2,4)) - 
                  2*t1*Power(-1 + t2,3)*
                   (371 - 1206*t2 + 759*Power(t2,2) - 68*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  Power(t1,3)*
                   (643 - 1727*t2 + 1974*Power(t2,2) - 
                     1051*Power(t2,3) + 137*Power(t2,4) + 24*Power(t2,5)\
)) + Power(s2,2)*(6*Power(t1,5)*(5 - 7*t2 + 2*Power(t2,2)) - 
                  3*Power(t1,4)*
                   (109 - 156*t2 + 41*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(-1 + t2,3)*
                   (105 - 934*t2 + 672*Power(t2,2) + 16*Power(t2,3)) + 
                  Power(t1,3)*
                   (-938 + 2256*t2 - 1623*Power(t2,2) + 
                     284*Power(t2,3) - 39*Power(t2,4)) - 
                  t1*Power(-1 + t2,2)*
                   (-1363 + 3166*t2 - 1752*Power(t2,2) + 
                     99*Power(t2,3) + 21*Power(t2,4)) + 
                  Power(t1,2)*
                   (749 - 1742*t2 + 1593*Power(t2,2) - 494*Power(t2,3) - 
                     172*Power(t2,4) + 66*Power(t2,5))))) + 
         Power(s,4)*(86 - 214*s2 + 97*Power(s2,2) + 46*Power(s2,3) - 
            19*Power(s2,4) + 221*t1 - 120*s2*t1 - 205*Power(s2,2)*t1 + 
            77*Power(s2,3)*t1 + 15*Power(s2,4)*t1 + 223*Power(t1,2) + 
            112*s2*Power(t1,2) - 232*Power(s2,2)*Power(t1,2) - 
            45*Power(s2,3)*Power(t1,2) + 47*Power(t1,3) + 
            314*s2*Power(t1,3) + 75*Power(s2,2)*Power(t1,3) - 
            140*Power(t1,4) - 75*s2*Power(t1,4) + 30*Power(t1,5) - 
            409*t2 + 682*s2*t2 - 86*Power(s2,2)*t2 - 
            167*Power(s2,3)*t2 + 14*Power(s2,4)*t2 - 5*Power(s2,5)*t2 - 
            1237*t1*t2 + 316*s2*t1*t2 + 861*Power(s2,2)*t1*t2 + 
            18*Power(s2,3)*t1*t2 - 10*Power(s2,4)*t1*t2 - 
            745*Power(t1,2)*t2 - 1031*s2*Power(t1,2)*t2 + 
            77*Power(s2,2)*Power(t1,2)*t2 + 
            35*Power(s2,3)*Power(t1,2)*t2 + 272*Power(t1,3)*t2 - 
            249*s2*Power(t1,3)*t2 - 55*Power(s2,2)*Power(t1,3)*t2 + 
            140*Power(t1,4)*t2 + 70*s2*Power(t1,4)*t2 - 
            35*Power(t1,5)*t2 + 943*Power(t2,2) - 658*s2*Power(t2,2) - 
            407*Power(s2,2)*Power(t2,2) + 77*Power(s2,3)*Power(t2,2) + 
            7*Power(s2,4)*Power(t2,2) + 5*Power(s2,5)*Power(t2,2) + 
            2391*t1*Power(t2,2) + 210*s2*t1*Power(t2,2) - 
            647*Power(s2,2)*t1*Power(t2,2) - 
            85*Power(s2,3)*t1*Power(t2,2) - Power(s2,4)*t1*Power(t2,2) + 
            627*Power(t1,2)*Power(t2,2) + 
            1243*s2*Power(t1,2)*Power(t2,2) + 
            118*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
            6*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
            478*Power(t1,3)*Power(t2,2) - 
            54*s2*Power(t1,3)*Power(t2,2) + 
            4*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
            14*Power(t1,4)*Power(t2,2) - 11*s2*Power(t1,4)*Power(t2,2) + 
            9*Power(t1,5)*Power(t2,2) - 1327*Power(t2,3) + 
            116*s2*Power(t2,3) + 548*Power(s2,2)*Power(t2,3) + 
            54*Power(s2,3)*Power(t2,3) - 12*Power(s2,4)*Power(t2,3) - 
            2084*t1*Power(t2,3) - 730*s2*t1*Power(t2,3) - 
            63*Power(s2,2)*t1*Power(t2,3) + 
            29*Power(s2,3)*t1*Power(t2,3) - 18*Power(t1,2)*Power(t2,3) - 
            326*s2*Power(t1,2)*Power(t2,3) - 
            20*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
            140*Power(t1,3)*Power(t2,3) + 
            26*s2*Power(t1,3)*Power(t2,3) - 23*Power(t1,4)*Power(t2,3) + 
            1125*Power(t2,4) + 111*s2*Power(t2,4) - 
            120*Power(s2,2)*Power(t2,4) - 8*Power(s2,3)*Power(t2,4) + 
            840*t1*Power(t2,4) + 374*s2*t1*Power(t2,4) + 
            48*Power(s2,2)*t1*Power(t2,4) - 84*Power(t1,2)*Power(t2,4) + 
            8*s2*Power(t1,2)*Power(t2,4) + 17*Power(t1,3)*Power(t2,4) - 
            514*Power(t2,5) - 64*s2*Power(t2,5) - 
            32*Power(s2,2)*Power(t2,5) - 133*t1*Power(t2,5) - 
            50*s2*t1*Power(t2,5) - 3*Power(t1,2)*Power(t2,5) + 
            98*Power(t2,6) + 27*s2*Power(t2,6) + 2*t1*Power(t2,6) - 
            2*Power(t2,7) + Power(s1,6)*(s2 - t1)*
             (Power(s2,2) + Power(t1,2) - 3*t1*(-1 + t2) + 
               2*Power(-1 + t2,2) + s2*(-3 - 2*t1 + 3*t2)) + 
            Power(s1,5)*(-4*Power(s2,4) - 7*Power(t1,4) - 
               t1*(-2 + t2)*Power(-1 + t2,2) + 2*Power(-1 + t2,4) + 
               Power(s2,3)*(19*t1 + 3*t2) + Power(t1,3)*(-13 + 10*t2) + 
               Power(t1,2)*(-7 + 11*t2 - 4*Power(t2,2)) + 
               Power(s2,2)*(5 - 33*Power(t1,2) - 13*t2 + 
                  8*Power(t2,2) + t1*(-13 + 4*t2)) + 
               s2*(25*Power(t1,3) + Power(t1,2)*(26 - 17*t2) + 
                  Power(-1 + t2,2)*(-4 + 3*t2) + 
                  t1*(2 + 2*t2 - 4*Power(t2,2)))) + 
            Power(s1,4)*(-6*Power(s2,5) + 9*Power(t1,5) + 
               Power(s2,4)*(32 + 34*t1 - 6*t2) + 
               Power(-1 + t2,4)*(-5 + 4*t2) + 
               Power(t1,4)*(20 + 21*t2) + 
               Power(t1,3)*(-30 + 98*t2 - 69*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(76 - 73*t2 + 13*Power(t2,2)) - 
               Power(s2,3)*(39 + 75*Power(t1,2) + t1*(135 - 16*t2) - 
                  67*t2 + 27*Power(t2,2)) + 
               Power(t1,2)*(-97 + 245*t2 - 196*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(s2,2)*(-14 + 81*Power(t1,3) + 5*t2 + 
                  35*Power(t2,2) - 26*Power(t2,3) + 
                  Power(t1,2)*(194 + 7*t2) + 
                  3*t1*(26 - 32*t2 + 5*Power(t2,2))) - 
               s2*(43*Power(t1,4) + Power(t1,3)*(111 + 38*t2) + 
                  Power(t1,2)*(9 + 69*t2 - 81*Power(t2,2)) - 
                  Power(-1 + t2,2)*(32 - 21*t2 + 5*Power(t2,2)) + 
                  t1*(-99 + 214*t2 - 125*Power(t2,2) + 10*Power(t2,3)))) \
- Power(s1,3)*(3*Power(s2,6) - Power(t1,6) + 
               4*Power(t1,5)*(-4 + 5*t2) + 
               Power(s2,5)*(-23 - 20*t1 + 18*t2) + 
               Power(t1,4)*(-83 + 100*t2 - 15*Power(t2,2)) + 
               Power(s2,4)*(39 + 50*Power(t1,2) + t1*(81 - 62*t2) - 
                  64*t2 + 14*Power(t2,2)) + 
               Power(t1,3)*(35 - 80*t2 + 98*Power(t2,2) - 
                  46*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-18 - 6*t2 + 7*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-13 + 279*t2 - 198*Power(t2,2) + 22*Power(t2,3)) + 
               Power(t1,2)*(148 - 582*t2 + 799*Power(t2,2) - 
                  428*Power(t2,3) + 63*Power(t2,4)) + 
               Power(s2,3)*(-9 - 59*Power(t1,3) + 28*t2 + 
                  9*Power(t2,2) - 35*Power(t2,3) + 
                  Power(t1,2)*(-82 + 51*t2) + 
                  t1*(-18 + 20*t2 + 29*Power(t2,2))) + 
               Power(s2,2)*(-67 + 32*Power(t1,4) + 134*t2 - 
                  85*Power(t2,2) + 52*Power(t2,3) - 34*Power(t2,4) + 
                  Power(t1,3)*(-3 + 32*t2) + 
                  Power(t1,2)*(-181 + 286*t2 - 132*Power(t2,2)) + 
                  t1*(-35 + 143*t2 - 214*Power(t2,2) + 127*Power(t2,3))\
) + s2*(-5*Power(t1,5) + Power(t1,4)*(43 - 59*t2) + 
                  Power(t1,3)*(243 - 342*t2 + 104*Power(t2,2)) + 
                  Power(t1,2)*
                   (18 - 118*t2 + 134*Power(t2,2) - 55*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (29 - 18*t2 + 78*Power(t2,2) + Power(t2,3)) + 
                  t1*(37 + 51*t2 - 231*Power(t2,2) + 129*Power(t2,3) + 
                     14*Power(t2,4)))) + 
            Power(s1,2)*(-Power(t1,6) + Power(s2,6)*(2 + t1 - 4*t2) + 
               Power(t1,5)*(42 - 47*t2 + 12*Power(t2,2)) + 
               Power(t1,4)*(175 - 380*t2 + 220*Power(t2,2) - 
                  28*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (53 + 186*t2 - 166*Power(t2,2) + 26*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-93 - 9*t2 + 286*Power(t2,2) - 139*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(t1,3)*(179 - 583*t2 + 575*Power(t2,2) - 
                  192*Power(t2,3) + 12*Power(t2,4)) + 
               Power(t1,2)*(87 - 156*t2 + 75*Power(t2,2) + 
                  91*Power(t2,3) - 109*Power(t2,4) + 12*Power(t2,5)) + 
               Power(s2,5)*(-17 - 5*Power(t1,2) - 2*t2 + 
                  10*Power(t2,2) + 2*t1*(1 + 6*t2)) + 
               Power(s2,4)*(-10 + 10*Power(t1,3) + 
                  2*Power(t1,2)*(-19 + t2) - 26*t2 - 40*Power(t2,2) + 
                  20*Power(t2,3) + t1*(34 + 70*t2 - 65*Power(t2,2))) - 
               Power(s2,3)*(-157 + 10*Power(t1,4) + 246*t2 - 
                  147*Power(t2,2) + 44*Power(t2,3) + 5*Power(t2,4) + 
                  Power(t1,3)*(-83 + 38*t2) + 
                  Power(t1,2)*(35 + 158*t2 - 123*Power(t2,2)) + 
                  t1*(7 - 122*t2 - 116*Power(t2,2) + 50*Power(t2,3))) + 
               Power(s2,2)*(-167 + 5*Power(t1,5) + 562*t2 - 
                  586*Power(t2,2) + 253*Power(t2,3) - 52*Power(t2,4) - 
                  10*Power(t2,5) + Power(t1,4)*(-71 + 42*t2) + 
                  Power(t1,3)*(78 + 67*t2 - 79*Power(t2,2)) + 
                  Power(t1,2)*
                   (247 - 632*t2 + 196*Power(t2,2) - 18*Power(t2,3)) + 
                  t1*(-122 - 162*t2 + 422*Power(t2,2) - 
                     225*Power(t2,3) + 60*Power(t2,4))) - 
               s2*(Power(t1,6) + Power(t1,5)*(-23 + 14*t2) + 
                  Power(t1,4)*(102 - 70*t2 + Power(t2,2)) + 
                  Power(t1,3)*
                   (405 - 916*t2 + 492*Power(t2,2) - 76*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-38 - 356*t2 + 374*Power(t2,2) + 72*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(t1,2)*
                   (275 - 1194*t2 + 1387*Power(t2,2) - 
                     582*Power(t2,3) + 87*Power(t2,4)) + 
                  t1*(-58 + 10*t2 + 573*Power(t2,2) - 752*Power(t2,3) + 
                     253*Power(t2,4) - 26*Power(t2,5)))) + 
            s1*(Power(t1,6)*(5 - 3*t2) + 2*Power(s2,6)*t2 + 
               Power(s2,5)*(5 - 10*t1 + 19*t2 - 19*Power(t2,2)) + 
               Power(t1,5)*(-35 + 14*t2 + 6*Power(t2,2)) + 
               Power(t1,4)*(112 - 75*t2 + 45*Power(t2,2) - 
                  23*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-97 - 255*t2 + 580*Power(t2,2) - 148*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (106 + 138*t2 - 83*Power(t2,2) - 35*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(t1,3)*(382 - 1178*t2 + 1106*Power(t2,2) - 
                  416*Power(t2,3) + 57*Power(t2,4)) + 
               Power(t1,2)*(404 - 1845*t2 + 2843*Power(t2,2) - 
                  1917*Power(t2,3) + 563*Power(t2,4) - 48*Power(t2,5)) + 
               Power(s2,4)*(42 + Power(t1,2)*(40 - 18*t2) - 12*t2 + 
                  17*Power(t2,2) - 25*Power(t2,3) + 
                  t1*(-60 - 49*t2 + 80*Power(t2,2))) + 
               Power(s2,3)*(-212 + 463*t2 - 131*Power(t2,2) - 
                  126*Power(t2,3) + 55*Power(t2,4) + 
                  Power(t1,3)*(-65 + 37*t2) - 
                  9*Power(t1,2)*(-25 + 9*t2 + 8*Power(t2,2)) - 
                  t1*(8 + 355*t2 - 293*Power(t2,2) + 55*Power(t2,3))) + 
               Power(s2,2)*(82 + Power(t1,4)*(55 - 33*t2) - 727*t2 + 
                  1351*Power(t2,2) - 881*Power(t2,3) + 
                  105*Power(t2,4) + 70*Power(t2,5) + 
                  Power(t1,3)*(-325 + 247*t2 - 14*Power(t2,2)) + 
                  Power(t1,2)*
                   (-38 + 786*t2 - 702*Power(t2,2) + 197*Power(t2,3)) + 
                  t1*(551 - 1239*t2 + 236*Power(t2,2) + 
                     525*Power(t2,3) - 220*Power(t2,4))) + 
               s2*(5*Power(t1,5)*(-5 + 3*t2) + 
                  Power(t1,4)*(190 - 150*t2 + 19*Power(t2,2)) - 
                  Power(t1,3)*
                   (108 + 344*t2 - 347*Power(t2,2) + 94*Power(t2,3)) + 
                  Power(t1,2)*
                   (-716 + 2029*t2 - 1466*Power(t2,2) + 
                     282*Power(t2,3) + 18*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (-204 - 418*t2 + 689*Power(t2,2) - 35*Power(t2,3) + 
                     53*Power(t2,4)) + 
                  t1*(-376 + 1818*t2 - 2709*Power(t2,2) + 
                     1767*Power(t2,3) - 595*Power(t2,4) + 95*Power(t2,5))\
))) + Power(s,3)*(-132 + 297*s2 - 55*Power(s2,2) - 150*Power(s2,3) + 
            46*Power(s2,4) - 240*t1 - 41*s2*t1 + 503*Power(s2,2)*t1 - 
            153*Power(s2,3)*t1 - 20*Power(s2,4)*t1 - 181*Power(t1,2) - 
            344*s2*Power(t1,2) + 313*Power(s2,2)*Power(t1,2) + 
            60*Power(s2,3)*Power(t1,2) + 5*Power(t1,3) - 
            346*s2*Power(t1,3) - 100*Power(s2,2)*Power(t1,3) + 
            140*Power(t1,4) + 100*s2*Power(t1,4) - 40*Power(t1,5) + 
            622*t2 - 991*s2*t2 - 166*Power(s2,2)*t2 + 
            544*Power(s2,3)*t2 - 102*Power(s2,4)*t2 + 10*Power(s2,5)*t2 + 
            1378*t1*t2 + 478*s2*t1*t2 - 1965*Power(s2,2)*t1*t2 + 
            196*Power(s2,3)*t1*t2 + 20*Power(s2,4)*t1*t2 + 
            668*Power(t1,2)*t2 + 1788*s2*Power(t1,2)*t2 - 
            366*Power(s2,2)*Power(t1,2)*t2 - 
            70*Power(s2,3)*Power(t1,2)*t2 - 362*Power(t1,3)*t2 + 
            492*s2*Power(t1,3)*t2 + 110*Power(s2,2)*Power(t1,3)*t2 - 
            220*Power(t1,4)*t2 - 140*s2*Power(t1,4)*t2 + 
            70*Power(t1,5)*t2 - 1301*Power(t2,2) + 814*s2*Power(t2,2) + 
            1148*Power(s2,2)*Power(t2,2) - 605*Power(s2,3)*Power(t2,2) + 
            85*Power(s2,4)*Power(t2,2) - 20*Power(s2,5)*Power(t2,2) - 
            3053*t1*Power(t2,2) - 1594*s2*t1*Power(t2,2) + 
            2319*Power(s2,2)*t1*Power(t2,2) - 
            26*Power(s2,3)*t1*Power(t2,2) + 
            4*Power(s2,4)*t1*Power(t2,2) - 754*Power(t1,2)*Power(t2,2) - 
            2633*s2*Power(t1,2)*Power(t2,2) - 
            22*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
            24*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
            759*Power(t1,3)*Power(t2,2) - 68*s2*Power(t1,3)*Power(t2,2) - 
            16*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
            31*Power(t1,4)*Power(t2,2) + 44*s2*Power(t1,4)*Power(t2,2) - 
            36*Power(t1,5)*Power(t2,2) + 1688*Power(t2,3) + 
            572*s2*Power(t2,3) - 1749*Power(s2,2)*Power(t2,3) + 
            182*Power(s2,3)*Power(t2,3) - 17*Power(s2,4)*Power(t2,3) + 
            11*Power(s2,5)*Power(t2,3) + 3382*t1*Power(t2,3) + 
            2088*s2*t1*Power(t2,3) - 736*Power(s2,2)*t1*Power(t2,3) - 
            44*Power(s2,3)*t1*Power(t2,3) - 
            9*Power(s2,4)*t1*Power(t2,3) + 201*Power(t1,2)*Power(t2,3) + 
            1296*s2*Power(t1,2)*Power(t2,3) + 
            64*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
            4*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
            432*Power(t1,3)*Power(t2,3) - 68*s2*Power(t1,3)*Power(t2,3) - 
            4*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
            65*Power(t1,4)*Power(t2,3) + s2*Power(t1,4)*Power(t2,3) + 
            5*Power(t1,5)*Power(t2,3) - 1562*Power(t2,4) - 
            1151*s2*Power(t2,4) + 970*Power(s2,2)*Power(t2,4) + 
            26*Power(s2,3)*Power(t2,4) - 13*Power(s2,4)*Power(t2,4) - 
            2010*t1*Power(t2,4) - 1117*s2*t1*Power(t2,4) - 
            143*Power(s2,2)*t1*Power(t2,4) + 
            31*Power(s2,3)*t1*Power(t2,4) + 82*Power(t1,2)*Power(t2,4) - 
            122*s2*Power(t1,2)*Power(t2,4) + 
            5*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
            9*Power(t1,3)*Power(t2,4) - 6*s2*Power(t1,3)*Power(t2,4) - 
            17*Power(t1,4)*Power(t2,4) + 1010*Power(t2,5) + 
            509*s2*Power(t2,5) - 125*Power(s2,2)*Power(t2,5) + 
            3*Power(s2,3)*Power(t2,5) + 638*t1*Power(t2,5) + 
            202*s2*t1*Power(t2,5) + 22*Power(s2,2)*t1*Power(t2,5) - 
            5*Power(t1,2)*Power(t2,5) + 15*s2*Power(t1,2)*Power(t2,5) + 
            21*Power(t1,3)*Power(t2,5) - 389*Power(t2,6) - 
            56*s2*Power(t2,6) - 23*Power(s2,2)*Power(t2,6) - 
            97*t1*Power(t2,6) - 16*s2*t1*Power(t2,6) - 
            11*Power(t1,2)*Power(t2,6) + 64*Power(t2,7) + 
            6*s2*Power(t2,7) + 2*t1*Power(t2,7) - 
            Power(s1,6)*(s2 - t1)*(-1 + t2)*
             (5*Power(s2,2) + 5*Power(t1,2) - 7*t1*(-1 + t2) + 
               2*Power(-1 + t2,2) + s2*(-7 - 10*t1 + 7*t2)) + 
            Power(s1,5)*(4*Power(s2,5) - 5*Power(t1,5) + 
               Power(s2,4)*(14 - 21*t1 - 22*t2) - 6*Power(-1 + t2,4) + 
               t1*Power(-1 + t2,3)*(6 + t2) - 
               8*Power(t1,4)*(-2 + 3*t2) - 
               Power(t1,2)*Power(-1 + t2,2)*(-31 + 28*t2) + 
               8*Power(t1,3)*(6 - 13*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*(-27 + 44*Power(t1,2) + 62*t2 - 
                  35*Power(t2,2) + t1*(-58 + 90*t2)) + 
               Power(s2,2)*(-46*Power(t1,3) + 
                  Power(t1,2)*(90 - 138*t2) - 
                  3*Power(-1 + t2,2)*(-3 + 2*t2) + 
                  6*t1*(17 - 38*t2 + 21*Power(t2,2))) + 
               s2*(24*Power(t1,4) + Power(-1 + t2,3)*(-10 + 3*t2) + 
                  2*t1*Power(-1 + t2,2)*(-20 + 17*t2) + 
                  Power(t1,3)*(-62 + 94*t2) - 
                  3*Power(t1,2)*(41 - 90*t2 + 49*Power(t2,2)))) + 
            Power(s1,4)*(3*Power(s2,6) - Power(t1,6) + 
               Power(s2,5)*(-14 - 15*t1 + 6*t2) + 
               Power(t1,5)*(-11 + 28*t2) + 
               Power(t1,4)*(11 - 13*t2 + Power(t2,2)) + 
               Power(-1 + t2,4)*(5 + t2 + 2*Power(t2,2)) + 
               t1*Power(-1 + t2,3)*(-114 + 85*t2 + 3*Power(t2,2)) + 
               2*Power(t1,2)*Power(-1 + t2,2)*
                (72 - 113*t2 + 21*Power(t2,2)) + 
               Power(s2,4)*(28 + 29*Power(t1,2) - t1*(-50 + t2) - 
                  64*t2 + 35*Power(t2,2)) - 
               3*Power(t1,3)*
                (-32 + 92*t2 - 85*Power(t2,2) + 25*Power(t2,3)) - 
               Power(s2,3)*(55 + 26*Power(t1,3) - 134*t2 + 
                  94*Power(t2,2) - 15*Power(t2,3) + 
                  Power(t1,2)*(55 + 61*t2) + 
                  t1*(70 - 155*t2 + 81*Power(t2,2))) + 
               Power(s2,2)*(9*Power(t1,4) + Power(t1,3)*(5 + 129*t2) - 
                  Power(-1 + t2,2)*(-99 + 105*t2 + 34*Power(t2,2)) + 
                  Power(t1,2)*(67 - 131*t2 + 58*Power(t2,2)) + 
                  t1*(155 - 391*t2 + 290*Power(t2,2) - 54*Power(t2,3))) \
+ s2*(Power(t1,5) + Power(t1,4)*(25 - 101*t2) + 
                  Power(t1,3)*(-36 + 53*t2 - 13*Power(t2,2)) + 
                  t1*Power(-1 + t2,2)*(-221 + 287*t2 + 14*Power(t2,2)) - 
                  Power(-1 + t2,3)*(-76 + 35*t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*
                   (-196 + 533*t2 - 451*Power(t2,2) + 114*Power(t2,3)))) \
+ Power(s1,3)*(Power(s2,7) + Power(t1,6)*(-4 + 5*t2) + 
               Power(s2,6)*(-7 - 6*t1 + 8*t2) + 
               Power(t1,5)*(-56 + 105*t2 - 36*Power(t2,2)) + 
               Power(s2,5)*(-4 + 15*Power(t1,2) + t1*(20 - 26*t2) - 
                  11*t2 - 15*Power(t2,2)) - 
               Power(-1 + t2,4)*(-58 + 31*t2 + 17*Power(t2,2)) + 
               t1*Power(-1 + t2,3)*
                (-3 + 357*t2 - 189*Power(t2,2) + 10*Power(t2,3)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (-97 + 348*t2 - 294*Power(t2,2) + 34*Power(t2,3)) + 
               Power(t1,4)*(-308 + 550*t2 - 300*Power(t2,2) + 
                  43*Power(t2,3)) + 
               Power(t1,3)*(-224 + 504*t2 - 365*Power(t2,2) + 
                  73*Power(t2,3) + 12*Power(t2,4)) + 
               Power(s2,4)*(2 - 20*Power(t1,3) - 95*t2 + 
                  137*Power(t2,2) - 59*Power(t2,3) + 
                  5*Power(t1,2)*(-2 + 5*t2) + 
                  t1*(-6 + 55*t2 + 84*Power(t2,2))) + 
               Power(s2,3)*(11 + 15*Power(t1,4) + 209*t2 - 
                  437*Power(t2,2) + 244*Power(t2,3) - 27*Power(t2,4) - 
                  4*Power(t1,3)*(4 + t2) - 
                  2*Power(t1,2)*(-53 + 110*t2 + 59*Power(t2,2)) + 
                  t1*(264 - 193*t2 - 141*Power(t2,2) + 130*Power(t2,3))) \
+ Power(s2,2)*(-6*Power(t1,5) + Power(t1,4)*(13 + 2*t2) + 
                  Power(t1,3)*(-234 + 424*t2 + 8*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (290 - 435*t2 + 171*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (-65 - 189*t2 + 249*Power(t2,2) + 14*Power(t2,3)) + 
                  t1*(-266 + 238*t2 + 173*Power(t2,2) - 
                     119*Power(t2,3) - 26*Power(t2,4))) + 
               s2*(Power(t1,6) + Power(t1,5)*(4 - 10*t2) + 
                  Power(t1,4)*(194 - 353*t2 + 77*Power(t2,2)) + 
                  Power(t1,3)*
                   (912 - 1567*t2 + 817*Power(t2,2) - 102*Power(t2,3)) + 
                  Power(-1 + t2,3)*
                   (-4 - 183*t2 + 8*Power(t2,2) + 4*Power(t2,3)) - 
                  t1*Power(-1 + t2,2)*
                   (-71 - 284*t2 + 346*Power(t2,2) + 27*Power(t2,3)) + 
                  Power(t1,2)*
                   (495 - 1015*t2 + 725*Power(t2,2) - 262*Power(t2,3) + 
                     57*Power(t2,4)))) + 
            Power(s1,2)*(-3*Power(t1,6)*(-2 + t2) + Power(s2,7)*t2 + 
               Power(t1,5)*(-52 + 84*t2 - 63*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(-1 + t2,4)*
                (82 + 217*t2 - 178*Power(t2,2) + 12*Power(t2,3)) + 
               Power(t1,4)*(-380 + 963*t2 - 841*Power(t2,2) + 
                  282*Power(t2,3) - 22*Power(t2,4)) - 
               t1*Power(-1 + t2,3)*
                (-63 + 248*t2 + 75*Power(t2,2) - 60*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (453 - 1038*t2 + 595*Power(t2,2) - 47*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,3)*(-704 + 2434*t2 - 2904*Power(t2,2) + 
                  1477*Power(t2,3) - 321*Power(t2,4) + 18*Power(t2,5)) + 
               Power(s2,6)*(3 + 9*t2 - 10*Power(t2,2) - 2*t1*(2 + t2)) + 
               Power(s2,5)*(51 - 5*Power(t1,2)*(-4 + t2) - 63*t2 + 
                  39*Power(t2,2) - 5*Power(t2,3) + 
                  3*t1*(-11 - 9*t2 + 16*Power(t2,2))) + 
               Power(s2,4)*(-191 + 20*Power(t1,3)*(-2 + t2) + 530*t2 - 
                  367*Power(t2,2) + 10*Power(t2,3) + 20*Power(t2,4) + 
                  Power(t1,2)*(124 - 21*t2 - 72*Power(t2,2)) + 
                  t1*(-111 + 19*t2 + Power(t2,2) - 20*Power(t2,3))) + 
               Power(s2,3)*(86 - 712*t2 + 1043*Power(t2,2) - 
                  421*Power(t2,3) - 6*Power(t2,4) + 10*Power(t2,5) - 
                  5*Power(t1,4)*(-8 + 5*t2) + 
                  2*Power(t1,3)*(-105 + 69*t2 + 14*Power(t2,2)) + 
                  Power(t1,2)*
                   (51 + 313*t2 - 242*Power(t2,2) + 102*Power(t2,3)) + 
                  t1*(691 - 1653*t2 + 871*Power(t2,2) + 
                     178*Power(t2,3) - 95*Power(t2,4))) + 
               Power(s2,2)*(2*Power(t1,5)*(-10 + 7*t2) + 
                  9*Power(t1,4)*(19 - 17*t2 + 2*Power(t2,2)) - 
                  Power(t1,3)*
                   (25 + 347*t2 - 262*Power(t2,2) + 116*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-123 - 252*t2 + 250*Power(t2,2) + 86*Power(t2,3) + 
                     4*Power(t2,4)) - 
                  3*t1*(263 - 1137*t2 + 1391*Power(t2,2) - 
                     560*Power(t2,3) + 43*Power(t2,4)) + 
                  Power(t1,2)*
                   (-1201 + 2735*t2 - 1578*Power(t2,2) - 
                     32*Power(t2,3) + 88*Power(t2,4))) + 
               s2*(Power(t1,6)*(4 - 3*t2) + 
                  Power(t1,5)*(-61 + 57*t2 - 12*Power(t2,2)) + 
                  Power(t1,4)*
                   (86 - 6*t2 + 3*Power(t2,2) + 31*Power(t2,3)) + 
                  Power(-1 + t2,3)*
                   (-30 - 111*t2 + 307*Power(t2,2) + 36*Power(t2,3)) + 
                  Power(t1,3)*
                   (1081 - 2575*t2 + 1915*Power(t2,2) - 
                     438*Power(t2,3) + 9*Power(t2,4)) + 
                  t1*Power(-1 + t2,2)*
                   (266 - 1378*t2 + 1230*Power(t2,2) - 
                     211*Power(t2,3) + 23*Power(t2,4)) + 
                  Power(t1,2)*
                   (1466 - 5389*t2 + 6468*Power(t2,2) - 
                     3092*Power(t2,3) + 595*Power(t2,4) - 48*Power(t2,5))\
)) + s1*(8*Power(s2,6)*(-1 + t2)*t2 + 
               Power(t1,6)*(-10 + 12*t2 - 3*Power(t2,2)) + 
               Power(t1,5)*(60 - 55*t2 - 12*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,4)*(-69 - 2*t2 + 28*Power(t2,2) + 
                  64*Power(t2,3) - 22*Power(t2,4)) + 
               Power(-1 + t2,4)*
                (108 + 377*t2 - 221*Power(t2,2) - 13*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*Power(-1 + t2,3)*
                (180 - 887*t2 + 804*Power(t2,2) - 160*Power(t2,3) + 
                  7*Power(t2,4)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (746 - 2069*t2 + 1454*Power(t2,2) - 433*Power(t2,3) + 
                  28*Power(t2,4)) + 
               Power(t1,3)*(-501 + 1719*t2 - 2141*Power(t2,2) + 
                  1243*Power(t2,3) - 355*Power(t2,4) + 35*Power(t2,5)) + 
               Power(s2,5)*(-42 + 36*t2 + 22*Power(t2,2) - 
                  17*Power(t2,3) + t1*(20 - 19*Power(t2,2))) + 
               Power(s2,4)*(62 - 191*t2 + 89*Power(t2,2) + 
                  94*Power(t2,3) - 55*Power(t2,4) + 
                  Power(t1,2)*(-80 + 72*t2 + 3*Power(t2,2)) + 
                  t1*(208 - 151*t2 - 160*Power(t2,2) + 110*Power(t2,3))) \
+ Power(s2,3)*(321 - 793*t2 + 444*Power(t2,2) + 128*Power(t2,3) - 
                  100*Power(t2,4) + 
                  2*Power(t1,3)*(65 - 74*t2 + 14*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (241 - 241*t2 - 65*Power(t2,2) + 74*Power(t2,3)) + 
                  t1*(-452 + 1429*t2 - 1115*Power(t2,2) + 
                     72*Power(t2,3) + 70*Power(t2,4))) + 
               s2*(5*Power(t1,5)*(10 - 12*t2 + 3*Power(t2,2)) + 
                  Power(t1,4)*
                   (-312 + 398*t2 - 100*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,3)*
                   (-250 + 1311*t2 - 1413*Power(t2,2) + 
                     432*Power(t2,3) - 76*Power(t2,4)) + 
                  t1*Power(-1 + t2,2)*
                   (1057 - 2614*t2 + 1289*Power(t2,2) - 
                     291*Power(t2,3) + 11*Power(t2,4)) - 
                  Power(-1 + t2,3)*
                   (-70 - 949*t2 + 1023*Power(t2,2) - 80*Power(t2,3) + 
                     20*Power(t2,4)) + 
                  Power(t1,2)*
                   (872 - 2586*t2 + 2358*Power(t2,2) - 614*Power(t2,3) - 
                     99*Power(t2,4) + 69*Power(t2,5))) + 
               Power(s2,2)*(-2*Power(t1,4)*
                   (55 - 66*t2 + 16*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (284 - 355*t2 + 60*Power(t2,2) + 22*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (-399 + 1145*t2 - 723*Power(t2,2) + 186*Power(t2,3) + 
                     65*Power(t2,4)) + 
                  Power(t1,2)*
                   (709 - 2547*t2 + 2411*Power(t2,2) - 662*Power(t2,3) + 
                     83*Power(t2,4)) - 
                  t1*(726 - 1740*t2 + 641*Power(t2,2) + 957*Power(t2,3) - 
                     744*Power(t2,4) + 160*Power(t2,5))))))*
       T3q(1 - s + s1 - t2,s1))/
     ((-1 + t1)*Power(1 - s2 + t1 - t2,2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,3)*(-1 + t2)*
       Power(-1 + s + t2,2)*(s - s1 + t2)) + 
    (8*(-10 + 7*s2 + 12*Power(s2,2) - 16*Power(s2,3) + 7*Power(s2,4) - 
         28*t1 + 10*s2*t1 + 37*Power(s2,2)*t1 - 29*Power(s2,3)*t1 - 
         Power(s2,4)*t1 + 2*Power(t1,2) - 56*s2*Power(t1,2) + 
         52*Power(s2,2)*Power(t1,2) + 3*Power(s2,3)*Power(t1,2) + 
         30*Power(t1,3) - 38*s2*Power(t1,3) - 5*Power(s2,2)*Power(t1,3) + 
         8*Power(t1,4) + 5*s2*Power(t1,4) - 2*Power(t1,5) + 80*t2 - 
         66*s2*t2 - 66*Power(s2,2)*t2 + 92*Power(s2,3)*t2 - 
         29*Power(s2,4)*t2 + Power(s2,5)*t2 + 88*t1*t2 + 90*s2*t1*t2 - 
         235*Power(s2,2)*t1*t2 + 93*Power(s2,3)*t1*t2 - 
         Power(s2,4)*t1*t2 - 122*Power(t1,2)*t2 + 306*s2*Power(t1,2)*t2 - 
         143*Power(s2,2)*Power(t1,2)*t2 + 2*Power(s2,3)*Power(t1,2)*t2 - 
         141*Power(t1,3)*t2 + 97*s2*Power(t1,3)*t2 - 
         4*Power(s2,2)*Power(t1,3)*t2 - 18*Power(t1,4)*t2 + 
         s2*Power(t1,4)*t2 + Power(t1,5)*t2 - 207*Power(t2,2) + 
         134*s2*Power(t2,2) + 170*Power(s2,2)*Power(t2,2) - 
         183*Power(s2,3)*Power(t2,2) + 48*Power(s2,4)*Power(t2,2) - 
         2*Power(s2,5)*Power(t2,2) + 15*t1*Power(t2,2) - 
         444*s2*t1*Power(t2,2) + 453*Power(s2,2)*t1*Power(t2,2) - 
         119*Power(s2,3)*t1*Power(t2,2) + Power(s2,4)*t1*Power(t2,2) + 
         428*Power(t1,2)*Power(t2,2) - 525*s2*Power(t1,2)*Power(t2,2) + 
         143*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         217*Power(t1,3)*Power(t2,2) - 85*s2*Power(t1,3)*Power(t2,2) - 
         Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         13*Power(t1,4)*Power(t2,2) - s2*Power(t1,4)*Power(t2,2) + 
         204*Power(t2,3) - 31*s2*Power(t2,3) - 
         238*Power(s2,2)*Power(t2,3) + 169*Power(s2,3)*Power(t2,3) - 
         36*Power(s2,4)*Power(t2,3) + 2*Power(s2,5)*Power(t2,3) - 
         365*t1*Power(t2,3) + 696*s2*t1*Power(t2,3) - 
         376*Power(s2,2)*t1*Power(t2,3) + 72*Power(s2,3)*t1*Power(t2,3) - 
         4*Power(s2,4)*t1*Power(t2,3) - 574*Power(t1,2)*Power(t2,3) + 
         377*s2*Power(t1,2)*Power(t2,3) - 
         62*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         138*Power(t1,3)*Power(t2,3) + 30*s2*Power(t1,3)*Power(t2,3) - 
         4*Power(t1,4)*Power(t2,3) + 4*Power(t2,4) - 156*s2*Power(t2,4) + 
         172*Power(s2,2)*Power(t2,4) - 70*Power(s2,3)*Power(t2,4) + 
         9*Power(s2,4)*Power(t2,4) + 545*t1*Power(t2,4) - 
         470*s2*t1*Power(t2,4) + 127*Power(s2,2)*t1*Power(t2,4) - 
         13*Power(s2,3)*t1*Power(t2,4) + 342*Power(t1,2)*Power(t2,4) - 
         102*s2*Power(t1,2)*Power(t2,4) + 
         4*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         32*Power(t1,3)*Power(t2,4) - 156*Power(t2,5) + 
         157*s2*Power(t2,5) - 52*Power(s2,2)*Power(t2,5) + 
         8*Power(s2,3)*Power(t2,5) - 327*t1*Power(t2,5) + 
         118*s2*t1*Power(t2,5) - 6*Power(s2,2)*t1*Power(t2,5) - 
         76*Power(t1,2)*Power(t2,5) + 109*Power(t2,6) - 
         45*s2*Power(t2,6) + 2*Power(s2,2)*Power(t2,6) + 
         72*t1*Power(t2,6) - 24*Power(t2,7) + 
         Power(s,5)*Power(-1 + s1,4)*Power(t1 - t2,2)*
          (-2 + 2*s2 - 3*t1 + 3*t2) - 
         Power(s1,8)*Power(s2 - t1,3)*
          (5 + 4*Power(s2,2) + 5*Power(t1,2) + t1*(11 - 9*t2) - 9*t2 + 
            4*Power(t2,2) + s2*(-10 - 9*t1 + 8*t2)) - 
         Power(s1,7)*(s2 - t1)*
          (3*Power(s2,5) - 2*Power(s2,4)*(11 + 6*t1 - t2) + 
            Power(s2,3)*(16 + 17*Power(t1,2) - 12*t2 - 5*Power(t2,2) + 
               t1*(85 + t2)) - 
            Power(s2,2)*(-25 + 9*Power(t1,3) + 39*t2 - 18*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,2)*(121 + 17*t2) + 
               t1*(46 - 27*t2 - 22*Power(t2,2))) + 
            t1*(Power(t1,4) - 3*Power(-1 + t2,2)*(-5 + 4*t2) - 
               Power(t1,3)*(17 + 9*t2) + 
               Power(t1,2)*(-12 - t2 + 14*Power(t2,2)) + 
               t1*(31 - 53*t2 + 28*Power(t2,2) - 6*Power(t2,3))) + 
            s2*(3*Power(-1 + t2,2)*(-5 + 4*t2) + 
               Power(t1,3)*(75 + 23*t2) + 
               Power(t1,2)*(42 - 14*t2 - 31*Power(t2,2)) + 
               t1*(-55 + 89*t2 - 43*Power(t2,2) + 9*Power(t2,3)))) - 
         Power(s1,6)*(Power(s2,7) + Power(s2,6)*(-11 - 6*t1 + t2) + 
            Power(s2,5)*(31 + 15*Power(t1,2) + t1*(53 - 2*t2) + 4*t2 - 
               Power(t2,2)) - 
            Power(s2,4)*(-37 + 20*Power(t1,3) + 40*t2 - 
               18*Power(t2,2) + Power(t2,3) + Power(t1,2)*(85 + 3*t2) + 
               t1*(148 + 67*t2 - 12*Power(t2,2))) + 
            Power(s2,3)*(-79 + 15*Power(t1,4) + 117*t2 - 
               34*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,3)*(31 + 11*t2) + 
               Power(t1,2)*(264 + 239*t2 - 31*Power(t2,2)) + 
               t1*(-146 + 196*t2 - 117*Power(t2,2) + 11*Power(t2,3))) + 
            t1*(16*Power(t1,5) - 3*Power(-1 + t2,3)*(-5 + 4*t2) - 
               Power(t1,4)*(7 + 60*t2) - 
               t1*Power(-1 + t2,2)*(23 - 9*t2 + 18*Power(t2,2)) + 
               Power(t1,3)*(33 - 64*t2 + 45*Power(t2,2)) + 
               Power(t1,2)*(90 - 126*t2 + 19*Power(t2,2) + 
                  17*Power(t2,3))) + 
            s2*(Power(t1,6) + Power(t1,5)*(-56 + 3*t2) + 
               3*Power(-1 + t2,3)*(-5 + 4*t2) + 
               Power(t1,4)*(75 + 237*t2 - 10*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(37 - 3*t2 + 30*Power(t2,2)) + 
               Power(t1,3)*(-137 + 241*t2 - 168*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(t1,2)*(261 - 377*t2 + 84*Power(t2,2) + 
                  30*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s2,2)*(-6*Power(t1,5) + Power(t1,4)*(52 - 10*t2) - 
               Power(-1 + t2,2)*(15 + 4*t2 + 13*Power(t2,2)) + 
               Power(t1,3)*(-215 - 353*t2 + 30*Power(t2,2)) - 
               3*Power(t1,2)*
                (-71 + 111*t2 - 74*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(251 - 372*t2 + 105*Power(t2,2) + 13*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s1,5)*(4*Power(t1,7) + Power(t1,6)*(45 - 8*t2) - 
            Power(s2,7)*(-3 + t2) - Power(-1 + t2,4)*(-5 + 4*t2) + 
            Power(t1,5)*(21 - 93*t2 + 4*Power(t2,2)) - 
            2*t1*Power(-1 + t2,3)*(-1 + 6*t2 + 9*Power(t2,2)) - 
            2*Power(t1,4)*(-31 + 7*t2 + 24*Power(t2,2)) - 
            Power(t1,2)*Power(-1 + t2,2)*
             (129 - 48*t2 + 35*Power(t2,2)) + 
            Power(t1,3)*(56 + 32*t2 - 237*Power(t2,2) + 
               149*Power(t2,3)) + 
            Power(s2,6)*(t1*(-11 + 5*t2) - 2*(8 + 3*t2 + Power(t2,2))) + 
            Power(s2,5)*(3 + 54*t2 - 22*Power(t2,2) - Power(t2,3) - 
               2*Power(t1,2)*(1 + 5*t2) + 
               t1*(36 + 81*t2 + 6*Power(t2,2))) + 
            Power(s2,4)*(88 - 112*t2 + 35*Power(t2,2) - 
               11*Power(t2,3) + 2*Power(t1,3)*(31 + 5*t2) + 
               Power(t1,2)*(73 - 282*t2 - 6*Power(t2,2)) - 
               t1*(-35 + 392*t2 - 154*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(-22 + 7*t2 + 66*Power(t2,2) - 60*Power(t2,3) + 
               9*Power(t2,4) - Power(t1,4)*(113 + 5*t2) + 
               2*Power(t1,3)*(-160 + 214*t2 + Power(t2,2)) + 
               Power(t1,2)*(-144 + 941*t2 - 326*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-371 + 448*t2 - 120*Power(t2,2) + 45*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s2,2)*(Power(t1,5)*(89 + t2) - 
               6*Power(t1,4)*(-67 + 52*t2) + 
               Power(t1,3)*(192 - 1015*t2 + 282*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-101 + 16*t2 - 34*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(97 + 33*t2 - 396*Power(t2,2) + 290*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(t1,2)*(535 - 557*t2 + 66*Power(t2,2) - 
                  46*Power(t2,3) + 2*Power(t2,4))) + 
            s2*(-32*Power(t1,6) + 11*Power(t1,5)*(-20 + 9*t2) + 
               Power(t1,4)*(-107 + 505*t2 - 92*Power(t2,2)) + 
               Power(-1 + t2,3)*(-5 + 18*t2 + 15*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*
                (-232 + 69*t2 - 73*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-314 + 235*t2 + 67*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(-129 - 80*t2 + 579*Power(t2,2) - 
                  387*Power(t2,3) + 17*Power(t2,4)))) + 
         Power(s1,3)*(4*Power(t1,7) + Power(t1,6)*(71 - 80*t2) + 
            Power(s2,7)*(-7 + 5*t2) + 
            Power(s2,6)*(1 + t1*(53 - 23*t2) - 14*t2 - 16*Power(t2,2)) - 
            Power(-1 + t2,4)*(7 + 8*t2 + 27*Power(t2,2)) - 
            2*t1*Power(-1 + t2,3)*(34 - 13*t2 + 31*Power(t2,2)) + 
            Power(t1,5)*(-11 - 237*t2 + 256*Power(t2,2)) + 
            Power(t1,2)*Power(-1 + t2,2)*
             (11 - 364*t2 + 290*Power(t2,2)) + 
            Power(t1,4)*(-42 + 16*t2 + 313*Power(t2,2) - 
               288*Power(t2,3)) + 
            Power(t1,3)*(74 - 259*t2 + 425*Power(t2,2) - 
               348*Power(t2,3) + 108*Power(t2,4)) + 
            Power(s2,5)*(12 - 189*t2 + 206*Power(t2,2) - 
               51*Power(t2,3) + 2*Power(t1,2)*(-85 + 21*t2) + 
               t1*(77 + 13*t2 + 91*Power(t2,2))) - 
            Power(s2,4)*(57 - 424*t2 + 590*Power(t2,2) - 
               259*Power(t2,3) + 37*Power(t2,4) + 
               Power(t1,3)*(-294 + 38*t2) + 
               Power(t1,2)*(246 + 20*t2 + 177*Power(t2,2)) + 
               t1*(295 - 1074*t2 + 876*Power(t2,2) - 193*Power(t2,3))) + 
            s2*(-44*Power(t1,6) + Power(t1,5)*(-204 + 305*t2) + 
               Power(t1,4)*(284 + 260*t2 - 598*Power(t2,2)) + 
               Power(-1 + t2,3)*
                (32 - t2 + 72*Power(t2,2) + Power(t2,3)) + 
               2*t1*Power(-1 + t2,2)*
                (-28 + 333*t2 - 293*Power(t2,2) + 51*Power(t2,3)) + 
               Power(t1,3)*(218 - 674*t2 - 87*Power(t2,2) + 
                  547*Power(t2,3)) + 
               Power(t1,2)*(-110 + 514*t2 - 1074*Power(t2,2) + 
                  983*Power(t2,3) - 313*Power(t2,4))) + 
            Power(s2,3)*(50 - 126*t2 + 73*Power(t2,2) + 8*Power(t2,3) - 
               2*Power(t2,4) - 3*Power(t2,5) + 
               Power(t1,4)*(-291 + 17*t2) + 
               Power(t1,3)*(195 + 202*t2 + 145*Power(t2,2)) + 
               Power(t1,2)*(816 - 1795*t2 + 1048*Power(t2,2) - 
                  233*Power(t2,3)) + 
               t1*(276 - 1586*t2 + 1839*Power(t2,2) - 611*Power(t2,3) + 
                  86*Power(t2,4))) - 
            Power(s2,2)*(Power(t1,5)*(-161 + 3*t2) + 
               Power(t1,4)*(-106 + 406*t2 + 43*Power(t2,2)) + 
               Power(t1,3)*(806 - 887*t2 + 36*Power(t2,2) - 
                  91*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-19 + 230*t2 - 230*Power(t2,2) + 82*Power(t2,3)) + 
               Power(t1,2)*(395 - 1820*t2 + 1475*Power(t2,2) - 
                  93*Power(t2,3) + 49*Power(t2,4)) + 
               t1*(23 + 92*t2 - 518*Power(t2,2) + 601*Power(t2,3) - 
                  194*Power(t2,4) - 4*Power(t2,5)))) - 
         Power(s1,4)*(-16*Power(t1,7) + 5*Power(s2,7)*(-1 + t2) + 
            Power(t1,6)*(-2 + 68*t2) + 
            Power(t1,5)*(-73 + 111*t2 - 88*Power(t2,2)) - 
            Power(-1 + t2,4)*(-5 + 8*t2 + 6*Power(t2,2)) + 
            Power(s2,6)*(14 + 59*t1 - 74*t2 - 35*t1*t2 + 
               16*Power(t2,2)) + 
            2*Power(t1,2)*Power(-1 + t2,2)*
             (-68 - 5*t2 + 40*Power(t2,2)) - 
            t1*Power(-1 + t2,3)*(64 - 8*t2 + 65*Power(t2,2)) + 
            Power(t1,4)*(-89 + 376*t2 - 326*Power(t2,2) + 
               36*Power(t2,3)) + 
            Power(t1,3)*(-68 + 370*t2 - 510*Power(t2,2) + 
               208*Power(t2,3)) + 
            Power(s2,5)*(-52 + 130*t2 - 74*Power(t2,2) + 
               15*Power(t2,3) + Power(t1,2)*(-206 + 90*t2) + 
               t1*(-130 + 417*t2 - 81*Power(t2,2))) + 
            Power(s2,4)*(56 + Power(t1,3)*(318 - 110*t2) + 33*t2 - 
               139*Power(t2,2) + 45*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(359 - 824*t2 + 147*Power(t2,2)) + 
               t1*(152 - 418*t2 + 191*Power(t2,2) - 51*Power(t2,3))) + 
            Power(s2,3)*(64 - 268*t2 + 380*Power(t2,2) - 
               239*Power(t2,3) + 64*Power(t2,4) - Power(t2,5) + 
               Power(t1,4)*(-217 + 65*t2) + 
               Power(t1,3)*(-435 + 642*t2 - 115*Power(t2,2)) + 
               Power(t1,2)*(-37 + 289*t2 + 5*Power(t2,2) + 
                  57*Power(t2,3)) - 
               t1*(58 + 651*t2 - 1035*Power(t2,2) + 311*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s2*(44*Power(t1,6) - Power(t1,5)*(47 + 187*t2) + 
               Power(t1,4)*(257 - 379*t2 + 341*Power(t2,2)) + 
               Power(t1,3)*(239 - 1357*t2 + 1427*Power(t2,2) - 
                  297*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (-49 - 14*t2 - 60*Power(t2,2) + 2*Power(t2,3)) + 
               2*t1*Power(-1 + t2,2)*
                (102 + 8*t2 - 49*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(253 - 1197*t2 + 1648*Power(t2,2) - 
                  795*Power(t2,3) + 91*Power(t2,4))) + 
            Power(s2,2)*(Power(t1,5)*(23 - 15*t2) + 
               Power(t1,4)*(241 - 42*t2 + 33*Power(t2,2)) - 
               2*Power(-1 + t2,2)*
                (34 + t2 - 5*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,3)*(247 - 267*t2 + 375*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(t1,2)*(-148 + 1599*t2 - 1997*Power(t2,2) + 
                  527*Power(t2,3) + Power(t2,4)) + 
               t1*(-251 + 1104*t2 - 1534*Power(t2,2) + 840*Power(t2,3) - 
                  161*Power(t2,4) + 2*Power(t2,5)))) + 
         s1*(-Power(t1,6) + 2*Power(s2,6)*(-1 + t2)*t2 + 
            Power(t1,5)*(25 - 31*t2 + 12*Power(t2,2)) - 
            Power(-1 + t2,4)*(-16 + 20*t2 + 27*Power(t2,2)) + 
            Power(t1,4)*(104 - 351*t2 + 355*Power(t2,2) - 
               112*Power(t2,3)) + 
            2*t1*Power(-1 + t2,3)*
             (7 - 94*t2 + 19*Power(t2,2) + 48*Power(t2,3)) - 
            Power(t1,2)*Power(-1 + t2,2)*
             (103 - 106*t2 - 313*Power(t2,2) + 280*Power(t2,3)) + 
            Power(t1,3)*(13 - 364*t2 + 986*Power(t2,2) - 
               919*Power(t2,3) + 284*Power(t2,4)) + 
            Power(s2,5)*(-21 + 63*t2 - 68*Power(t2,2) + 
               22*Power(t2,3) + t1*(2 + 6*t2 - 7*Power(t2,2))) + 
            Power(s2,4)*(43 - 208*t2 + 292*Power(t2,2) - 
               146*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,2)*(-8 - 6*t2 + 9*Power(t2,2)) + 
               t1*(111 - 265*t2 + 236*Power(t2,2) - 60*Power(t2,3))) - 
            Power(s2,3)*(31 - 88*t2 + 149*Power(t2,2) - 
               130*Power(t2,3) + 23*Power(t2,4) + 15*Power(t2,5) + 
               Power(t1,3)*(-13 - 2*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(246 - 482*t2 + 338*Power(t2,2) - 
                  54*Power(t2,3)) + 
               t1*(115 - 669*t2 + 950*Power(t2,2) - 434*Power(t2,3) + 
                  22*Power(t2,4))) + 
            s2*(5*Power(t1,5) + 
               Power(t1,4)*(-137 + 203*t2 - 94*Power(t2,2)) - 
               Power(-1 + t2,3)*
                (11 - 161*t2 + 89*Power(t2,2) + 21*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (160 - 481*t2 - 13*Power(t2,2) + 262*Power(t2,3)) + 
               Power(t1,3)*(-252 + 1008*t2 - 1145*Power(t2,2) + 
                  405*Power(t2,3)) - 
               3*Power(t1,2)*
                (-35 - 82*t2 + 466*Power(t2,2) - 533*Power(t2,3) + 
                  184*Power(t2,4))) + 
            Power(s2,2)*(Power(t1,4)*(-11 + Power(t2,2)) - 
               4*Power(t1,3)*
                (-67 + 113*t2 - 63*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(220 - 1118*t2 + 1448*Power(t2,2) - 
                  581*Power(t2,3) + 7*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (19 - 240*t2 + 158*Power(t2,2) + 13*Power(t2,3) + 
                  14*Power(t2,4)) + 
               t1*(-88 + 41*t2 + 527*Power(t2,2) - 764*Power(t2,3) + 
                  262*Power(t2,4) + 22*Power(t2,5)))) + 
         Power(s1,2)*(Power(s2,7)*t2 - 6*Power(t1,6)*(-3 + 2*t2) - 
            Power(s2,6)*(-21 + t1 + 39*t2 + 5*t1*t2 - 20*Power(t2,2)) + 
            Power(-1 + t2,4)*(1 - 4*t2 + 47*Power(t2,2)) - 
            t1*Power(-1 + t2,3)*(13 - 150*t2 + 68*Power(t2,2)) + 
            Power(t1,5)*(127 - 288*t2 + 144*Power(t2,2)) + 
            Power(t1,4)*(5 - 403*t2 + 812*Power(t2,2) - 
               400*Power(t2,3)) - 
            Power(t1,2)*Power(-1 + t2,2)*
             (-21 - 195*t2 + 24*Power(t2,2) + 148*Power(t2,3)) + 
            Power(t1,3)*(-139 + 342*t2 + 174*Power(t2,2) - 
               793*Power(t2,3) + 416*Power(t2,4)) + 
            Power(s2,5)*(-33 + 135*t2 - 93*Power(t2,2) - 
               5*Power(t2,3) + 5*Power(t1,2)*(1 + 2*t2) - 
               15*t1*(9 - 13*t2 + 5*Power(t2,2))) + 
            Power(s2,4)*(18 + 94*t2 - 211*Power(t2,2) + 
               170*Power(t2,3) - 57*Power(t2,4) - 
               10*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(364 - 423*t2 + 105*Power(t2,2)) + 
               t1*(60 - 482*t2 + 352*Power(t2,2) + 37*Power(t2,3))) + 
            s2*(Power(t1,6) + Power(t1,5)*(-139 + 108*t2) + 
               Power(t1,4)*(-357 + 993*t2 - 564*Power(t2,2)) - 
               Power(-1 + t2,3)*
                (-25 + 125*t2 - 64*Power(t2,2) + 33*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-3 - 405*t2 + 188*Power(t2,2) + 132*Power(t2,3)) + 
               Power(t1,3)*(285 + 212*t2 - 1430*Power(t2,2) + 
                  877*Power(t2,3)) + 
               Power(t1,2)*(403 - 1639*t2 + 1473*Power(t2,2) + 
                  284*Power(t2,3) - 521*Power(t2,4))) + 
            Power(s2,3)*(34 - 500*t2 + 1041*Power(t2,2) - 
               782*Power(t2,3) + 246*Power(t2,4) - 39*Power(t2,5) + 
               5*Power(t1,4)*(2 + t2) + 
               Power(t1,3)*(-510 + 501*t2 - 65*Power(t2,2)) - 
               Power(t1,2)*(124 - 976*t2 + 701*Power(t2,2) + 
                  59*Power(t2,3)) + 
               t1*(254 - 793*t2 + 679*Power(t2,2) - 336*Power(t2,3) + 
                  140*Power(t2,4))) + 
            Power(s2,2)*(-(Power(t1,5)*(5 + t2)) + 
               3*Power(t1,4)*(127 - 110*t2 + 5*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-9 + 202*t2 - 175*Power(t2,2) + 26*Power(t2,3)) + 
               Power(t1,3)*(327 - 1334*t2 + 862*Power(t2,2) + 
                  27*Power(t2,3)) + 
               Power(t1,2)*(-562 + 890*t2 + 150*Power(t2,2) - 
                  311*Power(t2,3) - 83*Power(t2,4)) + 
               t1*(-298 + 1800*t2 - 2700*Power(t2,2) + 
                  1309*Power(t2,3) - 153*Power(t2,4) + 42*Power(t2,5)))) \
- Power(s,4)*Power(-1 + s1,3)*
          (-4 - 29*t1 - 36*Power(t1,2) + 4*Power(t1,4) - 
            Power(s1,3)*(t1 - t2)*(1 - s2 + t1 - t2)*(-1 + t2) + 41*t2 + 
            86*t1*t2 - 2*Power(t1,2)*t2 - 28*Power(t1,3)*t2 - 
            50*Power(t2,2) + 4*t1*Power(t2,2) + 
            60*Power(t1,2)*Power(t2,2) - 2*Power(t2,3) - 
            52*t1*Power(t2,3) + 16*Power(t2,4) - 
            Power(s2,3)*(-8 + t1 + 7*t2) + 
            Power(s2,2)*(-20 + 7*Power(t1,2) + 63*t2 - 5*Power(t2,2) - 
               t1*(35 + 2*t2)) + 
            s2*(16 - 10*Power(t1,3) - 97*t2 + 56*Power(t2,2) + 
               13*Power(t2,3) + Power(t1,2)*(30 + 33*t2) + 
               t1*(65 - 86*t2 - 36*Power(t2,2))) + 
            Power(s1,2)*(4 + 2*Power(t1,4) - 7*t2 - 4*Power(t2,2) + 
               8*Power(t2,3) + Power(s2,3)*(-t1 + t2) - 
               Power(t1,3)*(1 + 6*t2) + 
               Power(s2,2)*(4 + 4*Power(t1,2) + t1*(7 - 6*t2) - 3*t2 + 
                  2*Power(t2,2)) + 
               2*Power(t1,2)*(1 + 5*t2 + 3*Power(t2,2)) + 
               t1*(11 + 2*t2 - 17*Power(t2,2) - 2*Power(t2,3)) + 
               s2*(-8 - 5*Power(t1,3) + 9*t2 + Power(t2,2) + 
                  Power(t2,3) + Power(t1,2)*(-7 + 11*t2) + 
                  t1*(-17 + 6*t2 - 7*Power(t2,2)))) + 
            s1*(13*Power(t1,4) - 42*Power(t1,3)*(-1 + t2) + 
               2*Power(s2,3)*(-4 + t1 + 3*t2) + 
               Power(t1,2)*(55 - 130*t2 + 48*Power(t2,2)) + 
               t1*(17 - 129*t2 + 134*Power(t2,2) - 22*Power(t2,3)) + 
               t2*(-33 + 74*t2 - 46*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(16 + 11*Power(t1,2) - 60*t2 + 
                  25*Power(t2,2) - 4*t1*(-7 + 9*t2)) - 
               s2*(8 + 26*Power(t1,3) + Power(t1,2)*(67 - 79*t2) - 
                  87*t2 + 100*Power(t2,2) - 27*Power(t2,3) + 
                  t1*(47 - 167*t2 + 80*Power(t2,2))))) + 
         Power(s,3)*Power(-1 + s1,2)*
          (17 + 104*t1 + 117*Power(t1,2) - 10*Power(t1,3) - 
            20*Power(t1,4) + 2*Power(t1,5) - 170*t2 - 369*t1*t2 - 
            88*Power(t1,2)*t2 + 79*Power(t1,3)*t2 - 5*Power(t1,4)*t2 + 
            300*Power(t2,2) + 250*t1*Power(t2,2) - 
            111*Power(t1,2)*Power(t2,2) - 15*Power(t1,3)*Power(t2,2) - 
            152*Power(t2,3) + 65*t1*Power(t2,3) + 
            55*Power(t1,2)*Power(t2,3) - 13*Power(t2,4) - 
            55*t1*Power(t2,4) + 18*Power(t2,5) + 
            Power(s2,4)*(-8 + t1 + 6*t2) - 
            Power(s2,3)*(7 + 3*Power(t1,2) + 5*t1*(-7 + t2) + 8*t2 + 
               22*Power(t2,2)) - 
            Power(s1,5)*(-1 + t2)*
             (-Power(s2,2) + t1 + Power(t1,2) - 3*t1*t2 + 
               2*(-1 + t2)*t2 + s2*(1 + t2)) + 
            Power(s2,2)*(53 + 5*Power(t1,3) - 198*t2 + 
               142*Power(t2,2) - 42*Power(t2,3) - 
               Power(t1,2)*(76 + 5*t2) + 
               t1*(67 + 34*t2 + 42*Power(t2,2))) - 
            s2*(55 + 5*Power(t1,4) - 371*t2 + 426*Power(t2,2) - 
               121*Power(t2,3) + 6*Power(t2,4) - 
               3*Power(t1,3)*(23 + 3*t2) + 
               Power(t1,2)*(48 + 103*t2 + 9*Power(t2,2)) + 
               t1*(208 - 356*t2 + 87*Power(t2,2) - 11*Power(t2,3))) + 
            Power(s1,4)*(-1 + Power(s2,4) + 8*Power(t1,4) + 
               Power(t1,3)*(30 - 17*t2) + 11*t2 - 23*Power(t2,2) + 
               13*Power(t2,3) + Power(s2,3)*(-5 - 8*t1 + t2) + 
               Power(s2,2)*(8 + 21*Power(t1,2) + t1*(40 - 12*t2) - 
                  t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(35 - 34*t2 + 10*Power(t2,2)) - 
               t1*(-12 + 3*t2 + 9*Power(t2,2) + Power(t2,3)) - 
               s2*(3 + 22*Power(t1,3) + Power(t1,2)*(65 - 28*t2) + 
                  6*t2 - 14*Power(t2,2) + 4*Power(t2,3) + 
                  t1*(49 - 37*t2 + 2*Power(t2,2)))) + 
            s1*(-10 - 4*Power(t1,5) + Power(s2,4)*(39 - 5*t1 - 32*t2) + 
               128*t2 - 316*Power(t2,2) + 312*Power(t2,3) - 
               115*Power(t2,4) + Power(t2,5) + 
               Power(t1,4)*(11 + 43*t2) + 
               Power(t1,3)*(39 + 63*t2 - 106*Power(t2,2)) + 
               Power(t1,2)*(-44 + 133*t2 - 274*Power(t2,2) + 
                  100*Power(t2,3)) + 
               t1*(-34 + 236*t2 - 484*Power(t2,2) + 315*Power(t2,3) - 
                  34*Power(t2,4)) + 
               Power(s2,3)*(-62 + 26*Power(t1,2) + 267*t2 - 
                  15*Power(t2,2) + t1*(-209 + 47*t2)) + 
               Power(s2,2)*(4 - 41*Power(t1,3) - 231*t2 + 
                  94*Power(t2,2) + 75*Power(t2,3) + 
                  Power(t1,2)*(299 + 63*t2) + 
                  t1*(282 - 602*t2 - 97*Power(t2,2))) + 
               s2*(29 + 24*Power(t1,4) - 137*t2 + 259*Power(t2,2) - 
                  229*Power(t2,3) + 79*Power(t2,4) - 
                  Power(t1,3)*(140 + 121*t2) + 
                  Power(t1,2)*(-245 + 227*t2 + 249*Power(t2,2)) + 
                  t1*(-29 + 261*t2 + 142*Power(t2,2) - 231*Power(t2,3)))\
) + Power(s1,2)*(-8 - 14*Power(t1,5) + 83*t2 - 117*Power(t2,2) + 
               2*Power(t2,3) + 41*Power(t2,4) - Power(t2,5) + 
               Power(s2,4)*(-29 + 7*t1 + 22*t2) + 
               2*Power(t1,4)*(-59 + 24*t2) + 
               Power(t1,3)*(-204 + 340*t2 - 59*Power(t2,2)) + 
               Power(s2,3)*(44 + 12*Power(t1,2) + t1*(151 - 115*t2) - 
                  259*t2 + 77*Power(t2,2)) + 
               Power(t1,2)*(-134 + 495*t2 - 285*Power(t2,2) + 
                  29*Power(t2,3)) + 
               t1*(-76 + 366*t2 - 293*Power(t2,2) + 22*Power(t2,3) - 
                  3*Power(t2,4)) - 
               Power(s2,2)*(17 + 59*Power(t1,3) + 
                  Power(t1,2)*(367 - 242*t2) - 451*t2 + 
                  414*Power(t2,2) - 77*Power(t2,3) + 
                  t1*(263 - 904*t2 + 260*Power(t2,2))) + 
               s2*(10 + 54*Power(t1,4) + Power(t1,3)*(363 - 197*t2) - 
                  287*t2 + 421*Power(t2,2) - 171*Power(t2,3) + 
                  11*Power(t2,4) + 
                  Power(t1,2)*(440 - 1003*t2 + 243*Power(t2,2)) + 
                  t1*(171 - 1073*t2 + 811*Power(t2,2) - 111*Power(t2,3))\
)) + Power(s1,3)*(2 - 2*Power(t1,5) + Power(t1,3)*(31 - 129*t2) - 
               50*t2 + 104*Power(t2,2) - 59*Power(t2,3) + 
               3*Power(t2,4) + Power(t1,4)*(35 + 4*t2) + 
               Power(s2,4)*(-3 - 3*t1 + 4*t2) + 
               Power(s2,3)*(30 + 13*Power(t1,2) + t1*(31 - 23*t2) - 
                  t2 + 8*Power(t2,2)) - 
               Power(t1,2)*(23 + 163*t2 - 156*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-7 - 130*t2 + 191*Power(t2,2) - 65*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,2)*(47 + 19*Power(t1,3) + 
                  Power(t1,2)*(21 - 42*t2) + 22*t2 - 38*Power(t2,2) - 
                  4*Power(t2,3) + 9*t1*(14 + 4*t2 + 3*Power(t2,2))) + 
               s2*(18 + 11*Power(t1,4) + 59*t2 - 123*Power(t2,2) + 
                  55*Power(t2,3) - 3*Power(t1,3)*(14 + 9*t2) + 
                  Power(t1,2)*(62 + 167*t2 + 21*Power(t2,2)) + 
                  t1*(115 + 131*t2 - 180*Power(t2,2) - 5*Power(t2,3))))) \
- Power(s,2)*(-1 + s1)*(-24 + 59*s2 - 33*Power(s2,2) - 26*Power(s2,3) + 
            23*Power(s2,4) - 140*t1 + 229*s2*t1 + 9*Power(s2,2)*t1 - 
            96*Power(s2,3)*t1 - 3*Power(s2,4)*t1 - 115*Power(t1,2) - 
            60*s2*Power(t1,2) + 183*Power(s2,2)*Power(t1,2) + 
            9*Power(s2,3)*Power(t1,2) + 63*Power(t1,3) - 
            146*s2*Power(t1,3) - 15*Power(s2,2)*Power(t1,3) + 
            36*Power(t1,4) + 15*s2*Power(t1,4) - 6*Power(t1,5) + 
            277*t2 - 522*s2*t2 + 160*Power(s2,2)*t2 + 
            131*Power(s2,3)*t2 - 43*Power(s2,4)*t2 + Power(s2,5)*t2 + 
            540*t1*t2 - 383*s2*t1*t2 - 286*Power(s2,2)*t1*t2 + 
            104*Power(s2,3)*t1*t2 - Power(s2,4)*t1*t2 + 
            42*Power(t1,2)*t2 + 383*s2*Power(t1,2)*t2 - 
            124*Power(s2,2)*Power(t1,2)*t2 + 
            2*Power(s2,3)*Power(t1,2)*t2 - 198*Power(t1,3)*t2 + 
            66*s2*Power(t1,3)*t2 - 4*Power(s2,2)*Power(t1,3)*t2 - 
            3*Power(t1,4)*t2 + s2*Power(t1,4)*t2 + Power(t1,5)*t2 - 
            611*Power(t2,2) + 842*s2*Power(t2,2) - 
            167*Power(s2,2)*Power(t2,2) - 81*Power(s2,3)*Power(t2,2) + 
            23*Power(s2,4)*Power(t2,2) - 494*t1*Power(t2,2) + 
            36*s2*t1*Power(t2,2) + 171*Power(s2,2)*t1*Power(t2,2) - 
            44*Power(s2,3)*t1*Power(t2,2) + 
            263*Power(t1,2)*Power(t2,2) - 
            197*s2*Power(t1,2)*Power(t2,2) + 
            33*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
            89*Power(t1,3)*Power(t2,2) - 8*s2*Power(t1,3)*Power(t2,2) - 
            4*Power(t1,4)*Power(t2,2) + 461*Power(t2,3) - 
            433*s2*Power(t2,3) + 96*Power(s2,2)*Power(t2,3) + 
            3*Power(s2,3)*Power(t2,3) - 30*t1*Power(t2,3) + 
            42*s2*t1*Power(t2,3) + 6*Power(s2,2)*t1*Power(t2,3) - 
            183*Power(t1,2)*Power(t2,3) - 7*s2*Power(t1,2)*Power(t2,3) - 
            71*Power(t2,4) + 74*s2*Power(t2,4) - 
            35*Power(s2,2)*Power(t2,4) + 141*t1*Power(t2,4) + 
            34*s2*t1*Power(t2,4) + 14*Power(t1,2)*Power(t2,4) - 
            38*Power(t2,5) - 20*s2*Power(t2,5) - 17*t1*Power(t2,5) + 
            6*Power(t2,6) + Power(s1,7)*
             (Power(s2,2)*(-2 + t1 + 3*t2) + 
               t1*(t1 + Power(t1,2) + t2 - Power(t2,2)) + 
               s2*(2 + t1 - 2*Power(t1,2) - 5*t2 - 3*t1*t2 + 
                  3*Power(t2,2))) + 
            Power(s1,6)*(3*Power(s2,4) + 13*Power(t1,4) + 
               Power(t1,3)*(43 - 23*t2) + (-2 + t2)*Power(-1 + t2,2) + 
               Power(s2,3)*(-19 - 19*t1 + 4*t2) + 
               Power(s2,2)*(29 + 42*Power(t1,2) + t1*(74 - 24*t2) - 
                  26*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(45 - 55*t2 + 6*Power(t2,2)) + 
               t1*(11 - 26*t2 + 11*Power(t2,2) + 4*Power(t2,3)) - 
               s2*(39*Power(t1,3) + Power(t1,2)*(98 - 43*t2) + 
                  t1*(70 - 73*t2 - 5*Power(t2,2)) + 
                  4*(3 - 6*t2 + Power(t2,2) + 2*Power(t2,3)))) + 
            Power(s1,5)*(3*Power(s2,5) - 6*Power(t1,5) + 
               Power(s2,4)*(-23 - 22*t1 + 3*t2) + 
               Power(t1,4)*(-29 + 4*t2) + 
               Power(s2,3)*(70 + 56*Power(t1,2) + t1*(140 - 29*t2) + 
                  t2 - 6*Power(t2,2)) - 
               Power(-1 + t2,2)*(-14 + 11*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(-80 - 55*t2 + 13*Power(t2,2)) - 
               Power(t1,2)*(53 + 90*t2 - 164*Power(t2,2) + 
                  14*Power(t2,3)) + 
               t1*(30 - 97*t2 + 140*Power(t2,2) - 76*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*(-57 - 64*Power(t1,3) + 5*t2 + 
                  65*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t1,2)*(-244 + 57*t2) + 
                  t1*(-277 + 23*t2 + 2*Power(t2,2))) + 
               s2*(-1 + 33*Power(t1,4) + Power(t1,3)*(156 - 35*t2) + 
                  21*t2 - 72*Power(t2,2) + 52*Power(t2,3) + 
                  Power(t1,2)*(289 + 27*t2 - 7*Power(t2,2)) + 
                  t1*(120 + 54*t2 - 197*Power(t2,2) + 9*Power(t2,3)))) + 
            s1*(-Power(t1,6) + 2*Power(t1,5)*(15 + t2) + 
               Power(s2,5)*(-23 + 2*t1 + 16*t2) + 
               2*Power(t1,4)*(9 - 62*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-245 + 125*t2 + 250*Power(t2,2) - 
                  56*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-26 + 205*t2 - 137*Power(t2,2) + 68*Power(t2,3)) + 
               Power(t1,2)*(-175 + 603*t2 - 182*Power(t2,2) - 
                  316*Power(t2,3) + 55*Power(t2,4)) + 
               t1*(17 + 96*t2 - 89*Power(t2,2) - 234*Power(t2,3) + 
                  228*Power(t2,4) - 18*Power(t2,5)) - 
               Power(s2,4)*(40 + 8*Power(t1,2) - 19*t2 + 
                  81*Power(t2,2) + 2*t1*(-65 + 19*t2)) + 
               Power(s2,3)*(141 + 13*Power(t1,3) - 717*t2 + 
                  598*Power(t2,2) - 129*Power(t2,3) + 
                  Power(t1,2)*(-308 + 34*t2) + 
                  t1*(321 - 68*t2 + 195*Power(t2,2))) - 
               Power(s2,2)*(76 + 11*Power(t1,4) - 969*t2 + 
                  1351*Power(t2,2) - 430*Power(t2,3) - 
                  13*Power(t2,4) + 4*Power(t1,3)*(-87 + 4*t2) + 
                  2*Power(t1,2)*(238 + 53*t2 + 48*Power(t2,2)) - 
                  2*t1*(-399 + 1021*t2 - 533*Power(t2,2) + 
                     55*Power(t2,3))) + 
               s2*(-22 + 5*Power(t1,5) - 47*t2 + 226*Power(t2,2) - 
                  110*Power(t2,3) - 122*Power(t2,4) + 75*Power(t2,5) + 
                  Power(t1,4)*(-177 + 2*t2) + 
                  Power(t1,3)*(177 + 279*t2 - 36*Power(t2,2)) + 
                  Power(t1,2)*
                   (893 - 1386*t2 + 117*Power(t2,2) + 121*Power(t2,3)) \
+ t1*(327 - 1905*t2 + 1872*Power(t2,2) - 97*Power(t2,3) - 
                     167*Power(t2,4)))) + 
            Power(s1,4)*(Power(t1,5)*(-61 + 2*t2) + 
               Power(s2,5)*(-10 - 3*t1 + 6*t2) + 
               Power(t1,4)*(-135 + 205*t2 - 6*Power(t2,2)) + 
               Power(s2,4)*(74 + 58*t1 + 14*Power(t1,2) + 12*t2 - 
                  33*t1*t2 + 12*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (28 + 12*t2 - 64*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,3)*(-116 + 564*t2 - 221*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,2)*(-160 + 656*t2 - 547*Power(t2,2) + 
                  68*Power(t2,3) - 2*Power(t2,4)) + 
               4*t1*(-41 + 85*t2 - 59*Power(t2,2) + 12*Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(s2,3)*(127 + 24*Power(t1,3) + 
                  Power(t1,2)*(41 - 61*t2) + 117*t2 - 96*Power(t2,2) - 
                  6*Power(t2,3) + t1*(261 + 164*t2 + 39*Power(t2,2))) + 
               Power(s2,2)*(24 + 18*Power(t1,4) + 188*t2 - 
                  295*Power(t2,2) + 98*Power(t2,3) - 
                  Power(t1,3)*(113 + 45*t2) + 
                  2*Power(t1,2)*(69 + 265*t2 + 15*Power(t2,2)) + 
                  t1*(246 + 746*t2 - 472*Power(t2,2) - 3*Power(t2,3))) \
+ s2*(52 - 5*Power(t1,5) - 123*t2 + 147*Power(t2,2) - 73*Power(t2,3) - 
                  3*Power(t2,4) + Power(t1,4)*(167 + 9*t2) + 
                  Power(t1,3)*(184 - 583*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (4 - 1211*t2 + 612*Power(t2,2) - 13*Power(t2,3)) + 
                  t1*(159 - 909*t2 + 907*Power(t2,2) - 
                     193*Power(t2,3) + 6*Power(t2,4)))) + 
            Power(s1,3)*(5*Power(t1,6) - 18*Power(t1,5)*(-6 + t2) + 
               Power(s2,5)*(-36 + 8*t1 + 30*t2) + 
               Power(t1,4)*(245 - 212*t2 + 22*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (24 + 132*t2 - 191*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,3)*(-200 + 494*t2 + 29*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(181 - 333*t2 - 122*Power(t2,2) + 
                  264*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(129 + 176*t2 - 740*Power(t2,2) + 566*Power(t2,3) - 
                  133*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,4)*(35 - 2*Power(t1,2) - 418*t2 + 
                  101*Power(t2,2) - 8*t1*(-34 + 21*t2)) - 
               Power(s2,3)*(31 + 47*Power(t1,3) + 
                  Power(t1,2)*(770 - 366*t2) - 792*t2 + 
                  636*Power(t2,2) - 99*Power(t2,3) + 
                  t1*(485 - 1847*t2 + 391*Power(t2,2))) + 
               Power(s2,2)*(115 + 73*Power(t1,4) - 441*t2 + 
                  472*Power(t2,2) - 174*Power(t2,3) + 15*Power(t2,4) - 
                  122*Power(t1,3)*(-8 + 3*t2) + 
                  Power(t1,2)*(1156 - 2683*t2 + 486*Power(t2,2)) + 
                  t1*(446 - 2707*t2 + 1690*Power(t2,2) - 
                     208*Power(t2,3))) - 
               s2*(87 + 37*Power(t1,5) + Power(t1,4)*(550 - 156*t2) + 
                  67*t2 - 419*Power(t2,2) + 420*Power(t2,3) - 
                  158*Power(t2,4) + 3*Power(t2,5) + 
                  Power(t1,3)*(951 - 1466*t2 + 218*Power(t2,2)) + 
                  Power(t1,2)*
                   (610 - 2395*t2 + 1012*Power(t2,2) - 113*Power(t2,3)) \
+ t1*(410 - 1032*t2 + 523*Power(t2,2) + 62*Power(t2,3) + 11*Power(t2,4))\
)) - Power(s1,2)*(-2*Power(t1,6) + Power(t1,5)*(7 + 23*t2) + 
               Power(s2,5)*(-66 + 7*t1 + 53*t2) + 
               Power(t1,4)*(10 + 160*t2 - 60*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (-10 - 134*t2 + 163*Power(t2,2) + 49*Power(t2,3)) + 
               Power(t1,3)*(-260 + 471*t2 - 478*Power(t2,2) + 
                  62*Power(t2,3)) + 
               Power(t1,2)*(-316 + 1201*t2 - 1246*Power(t2,2) + 
                  399*Power(t2,3) - 26*Power(t2,4)) + 
               t1*(-117 + 1110*t2 - 1787*Power(t2,2) + 
                  830*Power(t2,3) - 39*Power(t2,4) + 3*Power(t2,5)) + 
               Power(s2,4)*(72 - 36*Power(t1,2) - 427*t2 + 
                  15*Power(t2,2) - 5*t1*(-87 + 32*t2)) + 
               Power(s2,3)*(8 + 68*Power(t1,3) + 94*t2 + 
                  131*Power(t2,2) - 147*Power(t2,3) + 
                  Power(t1,2)*(-894 + 85*t2) + 
                  t1*(-400 + 1370*t2 + 99*Power(t2,2))) + 
               Power(s2,2)*(-58*Power(t1,4) + 
                  Power(t1,3)*(754 + 121*t2) + 
                  Power(t1,2)*(559 - 1192*t2 - 375*Power(t2,2)) + 
                  t2*(858 - 1523*t2 + 822*Power(t2,2) - 
                     145*Power(t2,3)) + 
                  t1*(-299 + 274*t2 - 809*Power(t2,2) + 457*Power(t2,3))\
) + s2*(-9 + 21*Power(t1,5) - 719*t2 + 1721*Power(t2,2) - 
                  1370*Power(t2,3) + 383*Power(t2,4) - 6*Power(t2,5) - 
                  2*Power(t1,4)*(118 + 61*t2) + 
                  Power(t1,3)*(-241 + 89*t2 + 321*Power(t2,2)) + 
                  Power(t1,2)*
                   (576 - 883*t2 + 1169*Power(t2,2) - 366*Power(t2,3)) + 
                  t1*(356 - 2361*t2 + 3234*Power(t2,2) - 
                     1405*Power(t2,3) + 152*Power(t2,4))))) + 
         s*(19 - 24*s2 - 13*Power(s2,2) + 41*Power(s2,3) - 
            22*Power(s2,4) + 90*t1 - 94*s2*t1 - 78*Power(s2,2)*t1 + 
            91*Power(s2,3)*t1 + 3*Power(s2,4)*t1 + 33*Power(t1,2) + 
            132*s2*Power(t1,2) - 166*Power(s2,2)*Power(t1,2) - 
            9*Power(s2,3)*Power(t1,2) - 80*Power(t1,3) + 
            125*s2*Power(t1,3) + 15*Power(s2,2)*Power(t1,3) - 
            28*Power(t1,4) - 15*s2*Power(t1,4) + 6*Power(t1,5) - 219*t2 + 
            311*s2*t2 + 32*Power(s2,2)*t2 - 205*Power(s2,3)*t2 + 
            66*Power(s2,4)*t2 - 2*Power(s2,5)*t2 - 347*t1*t2 + 
            45*s2*t1*t2 + 481*Power(s2,2)*t1*t2 - 192*Power(s2,3)*t1*t2 + 
            2*Power(s2,4)*t1*t2 + 152*Power(t1,2)*t2 - 
            612*s2*Power(t1,2)*t2 + 272*Power(s2,2)*Power(t1,2)*t2 - 
            4*Power(s2,3)*Power(t1,2)*t2 + 286*Power(t1,3)*t2 - 
            172*s2*Power(t1,3)*t2 + 8*Power(s2,2)*Power(t1,3)*t2 + 
            26*Power(t1,4)*t2 - 2*s2*Power(t1,4)*t2 - 2*Power(t1,5)*t2 + 
            561*Power(t2,2) - 624*s2*Power(t2,2) - 
            133*Power(s2,2)*Power(t2,2) + 299*Power(s2,3)*Power(t2,2) - 
            75*Power(s2,4)*Power(t2,2) + 2*Power(s2,5)*Power(t2,2) + 
            244*t1*Power(t2,2) + 513*s2*t1*Power(t2,2) - 
            690*Power(s2,2)*t1*Power(t2,2) + 
            173*Power(s2,3)*t1*Power(t2,2) - Power(s2,4)*t1*Power(t2,2) - 
            628*Power(t1,2)*Power(t2,2) + 
            739*s2*Power(t1,2)*Power(t2,2) - 
            182*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
            3*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
            288*Power(t1,3)*Power(t2,2) + 91*s2*Power(t1,3)*Power(t2,2) + 
            Power(s2,2)*Power(t1,3)*Power(t2,2) - 
            7*Power(t1,4)*Power(t2,2) + s2*Power(t1,4)*Power(t2,2) - 
            506*Power(t2,3) + 343*s2*Power(t2,3) + 
            179*Power(s2,2)*Power(t2,3) - 151*Power(s2,3)*Power(t2,3) + 
            21*Power(s2,4)*Power(t2,3) + 361*t1*Power(t2,3) - 
            700*s2*t1*Power(t2,3) + 301*Power(s2,2)*t1*Power(t2,3) - 
            33*Power(s2,3)*t1*Power(t2,3) + 631*Power(t1,2)*Power(t2,3) - 
            262*s2*Power(t1,2)*Power(t2,3) + 
            19*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
            82*Power(t1,3)*Power(t2,3) - 7*s2*Power(t1,3)*Power(t2,3) + 
            57*Power(t2,4) + 54*s2*Power(t2,4) - 
            59*Power(s2,2)*Power(t2,4) + 16*Power(s2,3)*Power(t2,4) - 
            516*t1*Power(t2,4) + 225*s2*t1*Power(t2,4) - 
            14*Power(s2,2)*t1*Power(t2,4) - 188*Power(t1,2)*Power(t2,4) + 
            3*s2*Power(t1,2)*Power(t2,4) + 141*Power(t2,5) - 
            52*s2*Power(t2,5) - 6*Power(s2,2)*Power(t2,5) + 
            168*t1*Power(t2,5) + 11*s2*t1*Power(t2,5) - 53*Power(t2,6) - 
            8*s2*Power(t2,6) - 
            Power(s1,9)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) + 
            Power(s1,8)*(s2 - t1)*
             (4*Power(s2,3) - 7*Power(t1,3) - 2*Power(-1 + t2,2) + 
               Power(s2,2)*(-9 - 15*t1 + 11*t2) + 
               Power(t1,2)*(-13 + 15*t2) + 
               t1*(-7 + 15*t2 - 8*Power(t2,2)) + 
               s2*(6 + 18*Power(t1,2) + t1*(22 - 26*t2) - 13*t2 + 
                  7*Power(t2,2))) + 
            Power(s1,7)*(6*Power(s2,5) - 9*Power(t1,5) - 
               Power(-1 + t2,3) + Power(s2,4)*(-43 - 34*t1 + 5*t2) + 
               Power(t1,4)*(-62 + 6*t2) - 
               2*t1*Power(-1 + t2,2)*(-5 + 8*t2) + 
               Power(t1,3)*(-72 + 59*t2 + 14*Power(t2,2)) + 
               Power(t1,2)*(2 - 7*t2 + 16*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(s2,3)*(55 + 75*Power(t1,2) - 45*t2 - 
                  11*Power(t2,2) - 5*t1*(-39 + 5*t2)) + 
               Power(s2,2)*(-4 - 81*Power(t1,3) + 6*t2 + 
                  8*Power(t2,2) - 10*Power(t2,3) + 
                  Power(t1,2)*(-323 + 41*t2) + 
                  t1*(-189 + 163*t2 + 29*Power(t2,2))) + 
               s2*(43*Power(t1,4) + Power(t1,3)*(233 - 27*t2) + 
                  3*Power(-1 + t2,2)*(-3 + 5*t2) + 
                  Power(t1,2)*(206 - 177*t2 - 32*Power(t2,2)) + 
                  t1*(6 - 11*t2 - 12*Power(t2,2) + 17*Power(t2,3)))) + 
            Power(s1,6)*(3*Power(s2,6) - Power(t1,6) + 
               5*Power(t1,5)*(-3 + 2*t2) + 
               4*Power(-1 + t2,3)*(-1 + 2*t2) + 
               Power(s2,5)*(-32 - 20*t1 + 3*t2) + 
               Power(t1,4)*(56 + 157*t2 - 19*Power(t2,2)) + 
               Power(s2,4)*(114 + 50*Power(t1,2) + t1*(169 - 18*t2) + 
                  5*t2 - 4*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-37 + 40*t2 + 22*Power(t2,2)) + 
               2*Power(t1,3)*
                (-3 + 102*t2 - 110*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(-173 + 338*t2 - 219*Power(t2,2) + 
                  56*Power(t2,3) - 2*Power(t2,4)) - 
               Power(s2,3)*(47 + 59*Power(t1,3) + 
                  Power(t1,2)*(299 - 25*t2) + 15*t2 - 76*Power(t2,2) + 
                  4*Power(t2,3) + t1*(475 + 82*t2 - 18*Power(t2,2))) + 
               Power(s2,2)*(-91 + 32*Power(t1,4) + 161*t2 - 
                  109*Power(t2,2) + 39*Power(t2,3) + 
                  2*Power(t1,3)*(102 + t2) + 
                  Power(t1,2)*(659 + 316*t2 - 48*Power(t2,2)) + 
                  t1*(112 + 187*t2 - 350*Power(t2,2) + 21*Power(t2,3))) \
- s2*(5*Power(t1,5) + Power(t1,4)*(27 + 22*t2) + 
                  Power(t1,3)*(354 + 396*t2 - 53*Power(t2,2)) + 
                  Power(-1 + t2,2)*(-35 + 43*t2 + 17*Power(t2,2)) + 
                  Power(t1,2)*
                   (56 + 385*t2 - 503*Power(t2,2) + 32*Power(t2,3)) + 
                  t1*(-258 + 485*t2 - 322*Power(t2,2) + 
                     101*Power(t2,3) - 6*Power(t2,4)))) + 
            Power(s1,5)*(34*Power(t1,6) + Power(t1,5)*(158 - 98*t2) - 
               Power(s2,6)*(11 + t1 - 4*t2) + 
               Power(s2,5)*(75 + 5*Power(t1,2) + t1*(51 - 21*t2) + 
                  15*t2 + 8*Power(t2,2)) - 
               Power(-1 + t2,3)*(-22 + 30*t2 + 11*Power(t2,2)) + 
               Power(t1,4)*(134 - 528*t2 + 79*Power(t2,2)) + 
               2*t1*Power(-1 + t2,2)*
                (-25 + 45*t2 - 76*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(249 - 540*t2 + 295*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(343 - 365*t2 - 201*Power(t2,2) + 
                  246*Power(t2,3) - 23*Power(t2,4)) - 
               Power(s2,4)*(125 + 10*Power(t1,3) + 
                  Power(t1,2)*(38 - 40*t2) + 146*t2 - 82*Power(t2,2) - 
                  4*Power(t2,3) + t1*(240 + 201*t2 + 25*Power(t2,2))) + 
               Power(s2,3)*(-102 + 10*Power(t1,4) + 265*t2 - 
                  234*Power(t2,2) + 63*Power(t2,3) - 
                  2*Power(t1,3)*(58 + 17*t2) + 
                  3*Power(t1,2)*(24 + 219*t2 + 7*Power(t2,2)) + 
                  t1*(264 + 1080*t2 - 475*Power(t2,2) + Power(t2,3))) + 
               Power(s2,2)*(198 - 5*Power(t1,5) - 252*t2 + 
                  16*Power(t2,2) + 53*Power(t2,3) - 15*Power(t2,4) + 
                  3*Power(t1,4)*(77 + 4*t2) + 
                  Power(t1,3)*(434 - 869*t2 + Power(t2,2)) - 
                  2*Power(t1,2)*
                   (1 + 1142*t2 - 400*Power(t2,2) + 7*Power(t2,3)) + 
                  2*t1*(280 - 667*t2 + 488*Power(t2,2) - 
                     92*Power(t2,3) + 3*Power(t2,4))) + 
               s2*(Power(t1,6) - Power(t1,5)*(151 + t2) + 
                  Power(t1,4)*(-499 + 496*t2 - 5*Power(t2,2)) - 
                  Power(-1 + t2,2)*
                   (-1 + 8*t2 - 121*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,3)*
                   (-271 + 1878*t2 - 486*Power(t2,2) + 9*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (355 - 808*t2 + 520*Power(t2,2) - 57*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  t1*(-556 + 667*t2 + 125*Power(t2,2) - 
                     269*Power(t2,3) + 33*Power(t2,4)))) + 
            s1*(2*Power(t1,6) + 2*Power(s2,6)*t2 + 
               Power(t1,5)*(-53 + 26*t2) - 
               68*Power(t1,4)*(2 - 6*t2 + 3*Power(t2,2)) + 
               Power(-1 + t2,3)*
                (38 - 185*t2 - 27*Power(t2,2) + 23*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-15 - 384*t2 + 343*Power(t2,2) + 74*Power(t2,3)) + 
               Power(t1,3)*(190 + 56*t2 - 725*Power(t2,2) + 
                  442*Power(t2,3)) + 
               Power(t1,2)*(270 - 1041*t2 + 863*Power(t2,2) + 
                  271*Power(t2,3) - 363*Power(t2,4)) + 
               Power(s2,5)*(44 - 80*t2 + 41*Power(t2,2) - 
                  2*t1*(2 + 3*t2)) + 
               Power(s2,4)*(-35 + 191*t2 - 103*Power(t2,2) - 
                  25*Power(t2,3) + 2*Power(t1,2)*(8 + 3*t2) + 
                  t1*(-237 + 311*t2 - 115*Power(t2,2))) - 
               Power(s2,3)*(53 - 475*t2 + 691*Power(t2,2) - 
                  415*Power(t2,3) + 109*Power(t2,4) + 
                  2*Power(t1,3)*(13 + t2) - 
                  3*Power(t1,2)*(177 - 175*t2 + 38*Power(t2,2)) + 
                  t1*(30 + 562*t2 - 365*Power(t2,2) - 75*Power(t2,3))) + 
               Power(s2,2)*(72 + 22*Power(t1,4) - 1017*t2 + 
                  2058*Power(t2,2) - 1444*Power(t2,3) + 
                  376*Power(t2,4) - 45*Power(t2,5) + 
                  Power(t1,3)*(-580 + 463*t2 - 47*Power(t2,2)) + 
                  Power(t1,2)*
                   (4 + 1029*t2 - 690*Power(t2,2) - 55*Power(t2,3)) + 
                  t1*(616 - 1740*t2 + 1318*Power(t2,2) - 
                     452*Power(t2,3) + 147*Power(t2,4))) + 
               s2*(-10*Power(t1,5) + 
                  Power(t1,4)*(295 - 195*t2 + 7*Power(t2,2)) + 
                  Power(t1,3)*
                   (197 - 1066*t2 + 632*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,2)*
                   (-757 + 1194*t2 + 167*Power(t2,2) - 
                     482*Power(t2,3) - 11*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (4 + 227*t2 - 346*Power(t2,2) + 77*Power(t2,3) + 
                     20*Power(t2,4)) + 
                  t1*(-387 + 2360*t2 - 3491*Power(t2,2) + 
                     1521*Power(t2,3) + 18*Power(t2,4) - 21*Power(t2,5)))\
) + Power(s1,3)*(-14*Power(t1,6) + Power(s2,6)*(47 - 3*t1 - 38*t2) + 
               Power(t1,5)*(78 + 74*t2) + 
               Power(t1,4)*(-260 + 72*t2 + 9*Power(t2,2)) + 
               Power(-1 + t2,3)*
                (25 + 89*t2 - 204*Power(t2,2) + Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (195 + 292*t2 - 255*Power(t2,2) + 76*Power(t2,3)) - 
               Power(t1,3)*(436 - 1285*t2 + 584*Power(t2,2) + 
                  258*Power(t2,3)) + 
               2*Power(t1,2)*
                (-79 + 587*t2 - 757*Power(t2,2) + 117*Power(t2,3) + 
                  132*Power(t2,4)) + 
               Power(s2,5)*(-45 + 17*Power(t1,2) + 311*t2 - 
                  5*Power(t2,2) + t1*(-373 + 169*t2)) + 
               Power(s2,4)*(47 - 38*Power(t1,3) + 
                  Power(t1,2)*(1004 - 250*t2) + 104*t2 - 
                  299*Power(t2,2) + 121*Power(t2,3) + 
                  t1*(229 - 1284*t2 - 48*Power(t2,2))) + 
               Power(s2,3)*(-59 + 42*Power(t1,4) - 1115*t2 + 
                  1944*Power(t2,2) - 894*Power(t2,3) + 
                  117*Power(t2,4) + 2*Power(t1,3)*(-615 + 58*t2) + 
                  Power(t1,2)*(-442 + 1820*t2 + 213*Power(t2,2)) + 
                  t1*(463 - 1145*t2 + 1362*Power(t2,2) - 
                     420*Power(t2,3))) + 
               Power(s2,2)*(-63 - 23*Power(t1,5) + 876*t2 - 
                  1774*Power(t2,2) + 1277*Power(t2,3) - 
                  325*Power(t2,4) + 9*Power(t2,5) + 
                  Power(t1,4)*(691 + 32*t2) + 
                  Power(t1,3)*(455 - 958*t2 - 262*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1407 + 2178*t2 - 1834*Power(t2,2) + 
                     445*Power(t2,3)) + 
                  t1*(-502 + 4491*t2 - 5839*Power(t2,2) + 
                     2072*Power(t2,3) - 201*Power(t2,4))) + 
               s2*(5*Power(t1,6) - Power(t1,5)*(125 + 29*t2) + 
                  Power(t1,4)*(-275 + 37*t2 + 102*Power(t2,2)) + 
                  Power(t1,3)*
                   (1157 - 1209*t2 + 762*Power(t2,2) - 146*Power(t2,3)) \
+ Power(-1 + t2,2)*(84 + 212*t2 - 127*Power(t2,2) + 139*Power(t2,3)) + 
                  Power(t1,2)*
                   (1006 - 4681*t2 + 4485*Power(t2,2) - 
                     908*Power(t2,3) + 77*Power(t2,4)) + 
                  t1*(325 - 2396*t2 + 3702*Power(t2,2) - 
                     1717*Power(t2,3) + 95*Power(t2,4) - 9*Power(t2,5)))) \
+ Power(s1,4)*(-14*Power(t1,6) + Power(s2,6)*(-17 + 3*t1 + 18*t2) - 
               Power(t1,5)*(135 + 74*t2) + 
               Power(s2,5)*(5 - 5*Power(t1,2) + t1*(197 - 115*t2) - 
                  299*t2 + 61*Power(t2,2)) + 
               Power(t1,4)*(-17 + 98*t2 + 276*Power(t2,2)) + 
               Power(t1,3)*(5 - 501*t2 + 750*Power(t2,2) - 
                  268*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (31 - 13*t2 - 84*Power(t2,2) + 2*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (249 - 259*t2 + 195*Power(t2,2) + 10*Power(t2,3)) + 
               2*Power(t1,2)*
                (-23 - 466*t2 + 942*Power(t2,2) - 489*Power(t2,3) + 
                  36*Power(t2,4)) - 
               Power(s2,4)*(68 + 10*Power(t1,3) + 
                  Power(t1,2)*(676 - 274*t2) - 611*t2 + 
                  416*Power(t2,2) - 60*Power(t2,3) + 
                  t1*(368 - 1600*t2 + 277*Power(t2,2))) + 
               Power(s2,3)*(264 + 30*Power(t1,4) + 
                  Power(t1,3)*(1030 - 312*t2) - 268*t2 + 
                  24*Power(t2,2) - 15*Power(t2,3) + 9*Power(t2,4) + 
                  2*Power(t1,2)*(613 - 1458*t2 + 216*Power(t2,2)) + 
                  t1*(458 - 2556*t2 + 1351*Power(t2,2) - 
                     171*Power(t2,3))) - 
               Power(s2,2)*(132 + 25*Power(t1,5) + 
                  Power(t1,4)*(749 - 172*t2) + 281*t2 - 
                  952*Power(t2,2) + 726*Power(t2,3) - 190*Power(t2,4) + 
                  3*Power(t2,5) + 
                  Power(t1,3)*(1503 - 2154*t2 + 277*Power(t2,2)) + 
                  Power(t1,2)*
                   (681 - 3265*t2 + 1098*Power(t2,2) - 146*Power(t2,3)) \
+ t1*(704 - 294*t2 - 732*Power(t2,2) + 351*Power(t2,3) + 13*Power(t2,4))\
) + s2*(7*Power(t1,6) + Power(t1,5)*(229 - 37*t2) + 
                  Power(t1,4)*(775 - 465*t2 + 61*Power(t2,2)) - 
                  Power(-1 + t2,2)*
                   (83 - 110*t2 + 221*Power(t2,2) + Power(t2,3)) - 
                  Power(t1,3)*
                   (-308 + 1418*t2 + 113*Power(t2,2) + 35*Power(t2,3)) + 
                  Power(t1,2)*
                   (419 + 527*t2 - 1566*Power(t2,2) + 662*Power(t2,3)) + 
                  2*t1*(72 + 681*t2 - 1540*Power(t2,2) + 
                     941*Power(t2,3) - 156*Power(t2,4) + 2*Power(t2,5)))) \
+ Power(s1,2)*(-19*Power(t1,6) + Power(s2,6)*(-22 + t1 + 14*t2) + 
               2*Power(t1,5)*(-39 + 68*t2) + 
               Power(t1,4)*(234 + t2 - 314*Power(t2,2)) + 
               Power(-1 + t2,3)*
                (-30 - 57*t2 + 188*Power(t2,2) + 19*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (54 - 1012*t2 + 579*Power(t2,2) + 108*Power(t2,3)) + 
               Power(t1,3)*(88 - 546*t2 + 278*Power(t2,2) + 
                  226*Power(t2,3)) + 
               Power(t1,2)*(-291 + 841*t2 - 641*Power(t2,2) + 
                  31*Power(t2,3) + 60*Power(t2,4)) - 
               Power(s2,5)*(53 + 5*Power(t1,2) - 52*t2 + 
                  95*Power(t2,2) + t1*(-149 + 51*t2)) + 
               Power(s2,4)*(128 + 10*Power(t1,3) - 836*t2 + 
                  755*Power(t2,2) - 133*Power(t2,3) + 
                  Power(t1,2)*(-416 + 74*t2) + 
                  t1*(478 - 290*t2 + 322*Power(t2,2))) + 
               Power(s2,3)*(-89 - 10*Power(t1,4) + 
                  Power(t1,3)*(593 - 56*t2) + 897*t2 - 
                  1287*Power(t2,2) + 394*Power(t2,3) + 39*Power(t2,4) + 
                  Power(t1,2)*(-1034 + 367*t2 - 345*Power(t2,2)) + 
                  t1*(-947 + 3242*t2 - 2218*Power(t2,2) + 
                     260*Power(t2,3))) + 
               Power(s2,2)*(26 + 5*Power(t1,5) + 489*t2 - 
                  1145*Power(t2,2) + 920*Power(t2,3) - 383*Power(t2,4) + 
                  93*Power(t2,5) + Power(t1,4)*(-443 + 24*t2) + 
                  8*Power(t1,3)*(96 + 8*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*
                   (1763 - 3953*t2 + 1756*Power(t2,2) - 61*Power(t2,3)) \
+ t1*(151 - 2265*t2 + 2960*Power(t2,2) - 543*Power(t2,3) - 
                     165*Power(t2,4))) - 
               s2*(Power(t1,6) + Power(t1,5)*(-158 + 5*t2) + 
                  Power(t1,4)*(81 + 329*t2 - 14*Power(t2,2)) + 
                  Power(t1,3)*
                   (1178 - 1546*t2 - 21*Power(t2,2) + 66*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (6 + 776*t2 - 724*Power(t2,2) + 213*Power(t2,3)) + 
                  Power(t1,2)*
                   (142 - 1901*t2 + 1960*Power(t2,2) + 52*Power(t2,3) - 
                     115*Power(t2,4)) + 
                  t1*(-319 + 1692*t2 - 2512*Power(t2,2) + 
                     1497*Power(t2,3) - 415*Power(t2,4) + 57*Power(t2,5)))\
)))*T4q(s1))/(Power(-1 + s1,2)*(-1 + t1)*Power(1 - s2 + t1 - t2,2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,3)*(-1 + t2)*(s - s1 + t2)) - 
    (8*(-2 - 7*s2 + 8*Power(s2,2) - Power(s2,4) + 18*t1 - 12*s2*t1 + 
         7*Power(s2,2)*t1 + 3*Power(s2,3)*t1 - Power(s2,4)*t1 - 
         14*Power(t1,2) - 2*s2*Power(t1,2) + 2*Power(s2,2)*Power(t1,2) + 
         3*Power(s2,3)*Power(t1,2) - 4*Power(t1,3) - 8*s2*Power(t1,3) - 
         5*Power(s2,2)*Power(t1,3) + 4*Power(t1,4) + 5*s2*Power(t1,4) - 
         2*Power(t1,5) + 4*t2 + 32*s2*t2 - 40*Power(s2,2)*t2 - 
         4*Power(s2,3)*t2 + 3*Power(s2,4)*t2 + Power(s2,5)*t2 - 58*t1*t2 + 
         84*s2*t1*t2 - 31*Power(s2,2)*t1*t2 - 17*Power(s2,3)*t1*t2 - 
         Power(s2,4)*t1*t2 + 26*Power(t1,2)*t2 + 40*s2*Power(t1,2)*t2 + 
         13*Power(s2,2)*Power(t1,2)*t2 + 2*Power(s2,3)*Power(t1,2)*t2 - 
         7*Power(t1,3)*t2 + 7*s2*Power(t1,3)*t2 - 
         4*Power(s2,2)*Power(t1,3)*t2 - 6*Power(t1,4)*t2 + 
         s2*Power(t1,4)*t2 + Power(t1,5)*t2 + 9*Power(t2,2) - 
         92*s2*Power(t2,2) + 78*Power(s2,2)*Power(t2,2) + 
         17*Power(s2,3)*Power(t2,2) - 2*Power(s2,5)*Power(t2,2) + 
         79*t1*Power(t2,2) - 182*s2*t1*Power(t2,2) + 
         13*Power(s2,2)*t1*Power(t2,2) + 19*Power(s2,3)*t1*Power(t2,2) + 
         Power(s2,4)*t1*Power(t2,2) + 4*Power(t1,2)*Power(t2,2) - 
         51*s2*Power(t1,2)*Power(t2,2) - 
         25*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         19*Power(t1,3)*Power(t2,2) + 5*s2*Power(t1,3)*Power(t2,2) - 
         Power(s2,2)*Power(t1,3)*Power(t2,2) + Power(t1,4)*Power(t2,2) - 
         s2*Power(t1,4)*Power(t2,2) - 36*Power(t2,3) + 
         161*s2*Power(t2,3) - 76*Power(s2,2)*Power(t2,3) - 
         15*Power(s2,3)*Power(t2,3) - 4*Power(s2,4)*Power(t2,3) + 
         2*Power(s2,5)*Power(t2,3) - 69*t1*Power(t2,3) + 
         166*s2*t1*Power(t2,3) + 20*Power(s2,2)*t1*Power(t2,3) - 
         2*Power(s2,3)*t1*Power(t2,3) - 4*Power(s2,4)*t1*Power(t2,3) - 
         30*Power(t1,2)*Power(t2,3) + 11*s2*Power(t1,2)*Power(t2,3) + 
         6*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         8*Power(t1,3)*Power(t2,3) + 44*Power(t2,4) - 146*s2*Power(t2,4) + 
         38*Power(s2,2)*Power(t2,4) + 2*Power(s2,3)*Power(t2,4) + 
         Power(s2,4)*Power(t2,4) + 43*t1*Power(t2,4) - 
         62*s2*t1*Power(t2,4) - 11*Power(s2,2)*t1*Power(t2,4) + 
         Power(s2,3)*t1*Power(t2,4) + 14*Power(t1,2)*Power(t2,4) + 
         2*s2*Power(t1,2)*Power(t2,4) - 
         2*Power(s2,2)*Power(t1,2)*Power(t2,4) - 24*Power(t2,5) + 
         59*s2*Power(t2,5) - 8*Power(s2,2)*Power(t2,5) - 
         13*t1*Power(t2,5) + 6*s2*t1*Power(t2,5) + 
         2*Power(s2,2)*t1*Power(t2,5) + 5*Power(t2,6) - 7*s2*Power(t2,6) + 
         Power(s,5)*(-1 + s1)*Power(t1 - t2,2)*(-2 + 2*s2 - 3*t1 + 3*t2) + 
         Power(s1,5)*Power(s2 - t1,3)*
          (5 + 4*Power(s2,2) + 5*Power(t1,2) + t1*(11 - 9*t2) - 9*t2 + 
            4*Power(t2,2) + s2*(-10 - 9*t1 + 8*t2)) + 
         Power(s1,4)*(s2 - t1)*
          (3*Power(s2,5) - 2*Power(s2,4)*(5 + 6*t1 - t2) + 
            Power(s2,3)*(-16 + 17*Power(t1,2) + 8*t2 - 5*Power(t2,2) + 
               t1*(36 + t2)) - 
            Power(s2,2)*(-40 + 9*Power(t1,3) + 66*t2 - 30*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,2)*(46 + 17*t2) + 
               t1*(-57 + 40*t2 - 22*Power(t2,2))) + 
            t1*(Power(t1,4) - 3*Power(-1 + t2,2)*(-5 + 4*t2) - 
               Power(t1,3)*(4 + 9*t2) + 
               Power(t1,2)*(27 - 28*t2 + 14*Power(t2,2)) + 
               t1*(46 - 80*t2 + 40*Power(t2,2) - 6*Power(t2,3))) + 
            s2*(3*Power(-1 + t2,2)*(-5 + 4*t2) + 
               Power(t1,3)*(24 + 23*t2) + 
               Power(t1,2)*(-68 + 60*t2 - 31*Power(t2,2)) + 
               t1*(-85 + 143*t2 - 67*Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s1,3)*(Power(s2,7) + Power(s2,6)*(-6*t1 + t2) + 
            Power(s2,5)*(-17 + 15*Power(t1,2) + 20*t2 - Power(t2,2) - 
               2*t1*(2 + t2)) - 
            Power(s2,4)*(-37 + 20*Power(t1,3) + 
               3*Power(t1,2)*(-6 + t2) + 34*t2 - 17*Power(t2,2) + 
               Power(t2,3) + t1*(-77 + 106*t2 - 12*Power(t2,2))) + 
            Power(s2,3)*(26 + 15*Power(t1,4) - 30*t2 + 24*Power(t2,2) - 
               20*Power(t2,3) + Power(t1,3)*(-31 + 11*t2) + 
               Power(t1,2)*(-128 + 207*t2 - 31*Power(t2,2)) + 
               t1*(-160 + 133*t2 - 60*Power(t2,2) + 11*Power(t2,3))) + 
            t1*(Power(t1,5) - 3*Power(-1 + t2,3)*(-5 + 4*t2) - 
               Power(t1,4)*(2 + 7*t2) + 
               Power(t1,3)*(41 - 7*t2 - 15*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(68 - 45*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(-45 + 87*t2 - 81*Power(t2,2) + 
                  39*Power(t2,3))) + 
            s2*(Power(t1,6) + 3*Power(t1,5)*(-3 + t2) + 
               3*Power(-1 + t2,3)*(-5 + 4*t2) + 
               Power(t1,4)*(-21 + 69*t2 - 10*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(127 - 75*t2 + 30*Power(t2,2)) + 
               Power(t1,3)*(-167 + 76*t2 + 7*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(111 - 187*t2 + 165*Power(t2,2) - 
                  87*Power(t2,3) - 2*Power(t2,4))) + 
            Power(s2,2)*(-6*Power(t1,5) - 5*Power(t1,4)*(-5 + 2*t2) - 
               Power(-1 + t2,2)*(60 - 32*t2 + 13*Power(t2,2)) + 
               Power(t1,3)*(91 - 183*t2 + 30*Power(t2,2)) - 
               3*Power(t1,2)*(-83 + 56*t2 - 17*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-91 + 126*t2 - 102*Power(t2,2) + 64*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s1,2)*(3*Power(t1,6) + Power(t1,5)*(4 - 9*t2) + 
            Power(s2,7)*t2 + Power(-1 + t2,4)*(-5 + 4*t2) - 
            Power(s2,6)*(3 + t1 - 3*t2 + 5*t1*t2 - 2*Power(t2,2)) + 
            t1*Power(-1 + t2,3)*(43 - 24*t2 + 18*Power(t2,2)) + 
            Power(t1,4)*(-17 - 16*t2 + 29*Power(t2,2)) - 
            Power(t1,2)*Power(-1 + t2,2)*(30 - 15*t2 + 31*Power(t2,2)) - 
            2*Power(t1,3)*(-40 + 56*t2 - 21*Power(t2,2) + 
               5*Power(t2,3)) + 
            Power(s2,5)*(5*Power(t1,2)*(1 + 2*t2) + 
               t2*(-6 + 3*t2 + Power(t2,2)) - 
               3*t1*(-5 + 7*t2 + 2*Power(t2,2))) + 
            Power(s2,4)*(33 - 80*t2 + 53*Power(t2,2) - 10*Power(t2,3) - 
               10*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(-26 + 51*t2 + 6*Power(t2,2)) + 
               t1*(15 + t2 - 14*Power(t2,2) + Power(t2,3))) + 
            s2*(Power(t1,6) - 2*Power(t1,5)*(5 + 3*t2) - 
               6*Power(t1,4)*(5 - 9*t2 + Power(t2,2)) - 
               Power(-1 + t2,3)*(40 - 18*t2 + 15*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (47 + 41*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-23 + 230*t2 - 221*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,2)*(-228 + 383*t2 - 279*Power(t2,2) + 
                  147*Power(t2,3) - 23*Power(t2,4))) + 
            Power(s2,3)*(-51 + 94*t2 - 102*Power(t2,2) + 
               68*Power(t2,3) - 9*Power(t2,4) + 5*Power(t1,4)*(2 + t2) + 
               Power(t1,3)*(15 - 57*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-52 + 52*t2 + 13*Power(t2,2) - 
                  5*Power(t2,3)) + 
               t1*(-121 + 350*t2 - 257*Power(t2,2) + 42*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s2,2)*(-(Power(t1,5)*(5 + t2)) + 
               6*Power(t1,4)*(1 + 5*t2) + 
               Power(t1,3)*(63 - 92*t2 + 4*Power(t2,2) + 3*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (22 + 4*t2 + 17*Power(t2,2) + 3*Power(t2,3)) - 
               2*Power(t1,2)*(-64 + 242*t2 - 198*Power(t2,2) + 
                  31*Power(t2,3) + Power(t2,4)) + 
               t1*(200 - 369*t2 + 345*Power(t2,2) - 209*Power(t2,3) + 
                  33*Power(t2,4)))) + 
         s1*(-Power(t1,6) + Power(t1,5)*(7 - 4*t2) + 
            2*Power(s2,6)*(-1 + t2)*t2 + 
            t1*Power(-1 + t2,3)*(8 + 8*t2 + Power(t2,2)) - 
            2*Power(-1 + t2,4)*(5 - 2*t2 + 3*Power(t2,2)) + 
            Power(t1,4)*(-2 - 21*t2 + 16*Power(t2,2)) + 
            Power(t1,2)*Power(-1 + t2,2)*(61 - 24*t2 + 29*Power(t2,2)) + 
            Power(t1,3)*(-31 + 19*t2 + 47*Power(t2,2) - 35*Power(t2,3)) + 
            Power(s2,5)*(3 - 6*t2 - 2*Power(t2,2) + 4*Power(t2,3) + 
               t1*(2 + 6*t2 - 7*Power(t2,2))) + 
            Power(s2,4)*(Power(t1,2)*(-8 - 6*t2 + 9*Power(t2,2)) + 
               t2*(9 - 20*t2 + 2*Power(t2,2) + 2*Power(t2,3)) - 
               t1*(12 - 38*t2 + 13*Power(t2,2) + 6*Power(t2,3))) + 
            s2*(5*Power(t1,5) + 
               Power(t1,4)*(-20 + 2*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(-16 + 105*t2 - 62*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(-1 + t2,3)*
                (10 + 8*t2 - 3*Power(t2,2) + 2*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-104 + 87*t2 - 137*Power(t2,2) + 22*Power(t2,3)) + 
               Power(t1,2)*(-15 + 218*t2 - 393*Power(t2,2) + 
                  216*Power(t2,3) - 26*Power(t2,4))) + 
            Power(s2,3)*(-27 + 100*t2 - 130*Power(t2,2) + 
               75*Power(t2,3) - 17*Power(t2,4) - Power(t2,5) + 
               Power(t1,3)*(13 + 2*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(9 - 64*t2 + 37*Power(t2,2)) + 
               t1*(-18 + 32*t2 + 37*Power(t2,2) - 26*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s2,2)*(Power(t1,4)*(-11 + Power(t2,2)) + 
               Power(t1,3)*(13 + 34*t2 - 27*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-31 + 32*t2 - 82*Power(t2,2) + 15*Power(t2,3)) + 
               Power(t1,2)*(36 - 125*t2 + 29*Power(t2,2) + 
                  23*Power(t2,3) - 5*Power(t2,4)) + 
               t1*(71 - 328*t2 + 460*Power(t2,2) - 242*Power(t2,3) + 
                  37*Power(t2,4) + 2*Power(t2,5)))) + 
         Power(s,4)*(-4 - 13*t1 - 4*Power(t1,2) - 6*Power(t1,3) + 
            4*Power(t1,4) + 17*t2 + 28*t1*t2 + 26*Power(t1,2)*t2 - 
            10*Power(t1,3)*t2 - 24*Power(t2,2) - 34*t1*Power(t2,2) + 
            6*Power(t1,2)*Power(t2,2) + 14*Power(t2,3) + 
            2*t1*Power(t2,3) - 2*Power(t2,4) + Power(s2,3)*(-t1 + t2) + 
            Power(s2,2)*(t1 + 5*Power(t1,2) - 12*t1*t2 + t2*(3 + 7*t2)) + 
            s2*(4 - 8*Power(t1,3) - 11*t2 + 14*Power(t2,2) + 
               9*Power(t2,3) + Power(t1,2)*(2 + 25*t2) + 
               t1*(3 - 16*t2 - 26*Power(t2,2))) - 
            Power(s1,3)*(-4 + 11*t2 - 6*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(1 + t2) + t1*(-11 + 5*t2 - 2*Power(t2,2)) + 
               s2*(4 + t1 - t2 - t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(-12 + 2*Power(t1,4) + Power(t1,3)*(17 - 6*t2) + 
               35*t2 - 16*Power(t2,2) - 2*Power(t2,3) + 
               Power(s2,3)*(-t1 + t2) + 
               Power(t1,2)*(22 - 36*t2 + 6*Power(t2,2)) - 
               t1*(31 + 6*t2 - 21*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,2) + t1*(5 - 6*t2) + 
                  t2*(-1 + 2*t2)) + 
               s2*(12 - 5*Power(t1,3) - 5*t2 + Power(t2,2) + 
                  Power(t2,3) + Power(t1,2)*(-21 + 11*t2) + 
                  t1*(-3 + 20*t2 - 7*Power(t2,2)))) + 
            s1*(12 - 5*Power(t1,4) + 2*Power(s2,3)*(t1 - t2) + 
               12*Power(t1,3)*(-1 + t2) - 41*t2 + 32*Power(t2,2) - 
               10*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,2)*(-19 + 14*t2 - 6*Power(t2,2)) + 
               t1*(33 - 13*t2 + 8*Power(t2,2) - 4*Power(t2,3)) - 
               Power(s2,2)*(11*Power(t1,2) + t1*(6 - 22*t2) + 
                  t2*(2 + 11*t2)) + 
               s2*(-12 + 14*Power(t1,3) + Power(t1,2)*(23 - 39*t2) + 
                  15*t2 - 10*Power(t2,2) - 11*Power(t2,3) + 
                  t1*(1 - 13*t2 + 36*Power(t2,2))))) + 
         Power(s,3)*(9 + 10*t1 + 13*Power(t1,2) + 6*Power(t1,3) - 
            16*Power(t1,4) + 2*Power(t1,5) + Power(s2,4)*(t1 - 2*t2) - 
            46*t2 - 91*t1*t2 - 20*Power(t1,2)*t2 + 47*Power(t1,3)*t2 - 
            5*Power(t1,4)*t2 + 90*Power(t2,2) + 84*t1*Power(t2,2) - 
            29*Power(t1,2)*Power(t2,2) + 3*Power(t1,3)*Power(t2,2) - 
            70*Power(t2,3) - 19*t1*Power(t2,3) + 
            Power(t1,2)*Power(t2,3) + 17*Power(t2,4) - t1*Power(t2,4) + 
            Power(s2,3)*(1 - 3*Power(t1,2) - 8*t2 - 6*Power(t2,2) + 
               t1*(3 + 9*t2)) + 
            Power(s1,4)*(-(Power(s2,2)*(-1 + t2)) + 
               2*Power(-1 + t2,2)*t2 + Power(t1,2)*(1 + t2) + 
               t1*(-7 + 4*t2 - 3*Power(t2,2)) + 
               s2*(1 - 2*t1 + 4*t2 + Power(t2,2))) - 
            s2*(5 + 5*Power(t1,4) - 21*t2 + 2*Power(t2,2) + 
               3*Power(t2,3) - 6*Power(t2,4) - 
               3*Power(t1,3)*(11 + 5*t2) + 
               t1*t2*(16 - 77*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(-6 + 107*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(-3 + 5*Power(t1,3) + 28*t2 - 36*Power(t2,2) - 
               8*Power(t2,3) - Power(t1,2)*(20 + 17*t2) + 
               t1*(-11 + 58*t2 + 20*Power(t2,2))) + 
            Power(s1,3)*(7 - Power(s2,4) - 8*Power(t1,4) + 
               Power(s2,3)*(3 + 8*t1 - t2) - 27*t2 + 27*Power(t2,2) - 
               7*Power(t2,3) + Power(t1,3)*(-48 + 17*t2) + 
               Power(t1,2)*(-28 + 55*t2 - 10*Power(t2,2)) + 
               t1*(49 - 17*t2 + Power(t2,3)) + 
               Power(s2,2)*(11 - 21*Power(t1,2) - 10*t2 + 
                  4*Power(t2,2) + 2*t1*(-17 + 6*t2)) + 
               s2*(-28 + 22*Power(t1,3) + Power(t1,2)*(79 - 28*t2) + 
                  30*t2 - 39*Power(t2,2) + 4*Power(t2,3) + 
                  t1*(1 - 25*t2 + 2*Power(t2,2)))) + 
            Power(s1,2)*(-7 + 35*Power(t1,3) + 2*Power(t1,5) + 
               Power(t1,4)*(15 - 4*t2) + Power(s2,4)*(2 + 3*t1 - 4*t2) + 
               33*t2 - 45*Power(t2,2) + 22*Power(t2,3) - 3*Power(t2,4) - 
               Power(s2,3)*(3 + 13*Power(t1,2) - 23*t1*(-1 + t2) + 
                  8*Power(t2,2)) + 
               Power(t1,2)*(-31 + 84*t2 - 48*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-94 + 119*t2 - 141*Power(t2,2) + 36*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,2)*(19*Power(t1,3) + Power(t1,2)*(60 - 42*t2) + 
                  t1*(44 - 36*t2 + 27*Power(t2,2)) - 
                  2*(17 - 38*t2 + 9*Power(t2,2) + 2*Power(t2,3))) + 
               s2*(54 - 11*Power(t1,4) + 27*Power(t1,3)*(-2 + t2) - 
                  111*t2 + 142*Power(t2,2) - 3*Power(t2,3) + 
                  Power(t1,2)*(-83 + 45*t2 - 21*Power(t2,2)) + 
                  t1*(52 - 98*t2 + 12*Power(t2,2) + 5*Power(t2,3)))) + 
            s1*(-9 - 2*Power(t1,5) + 34*t2 - 58*Power(t2,2) + 
               46*Power(t2,3) - 14*Power(t2,4) + Power(t2,5) + 
               Power(t1,4)*(7 + 2*t2) + Power(s2,4)*(-1 - 4*t1 + 6*t2) + 
               Power(t1,3)*(13 - 58*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-1 + 17*Power(t1,2) + t1*(8 - 38*t2) + 
                  13*t2 + 19*Power(t2,2)) + 
               Power(t1,2)*(59 - 139*t2 + 81*Power(t2,2) - 
                  7*Power(t2,3)) + 
               t1*(46 - 39*t2 + 80*Power(t2,2) - 16*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(25 - 24*Power(t1,3) - 105*t2 + 
                  50*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t1,2)*(-7 + 58*t2) + 
                  t1*(13 - 46*t2 - 45*Power(t2,2))) + 
               s2*(-22 + 13*Power(t1,4) + 68*t2 - 117*Power(t2,2) + 
                  10*Power(t2,3) - 11*Power(t2,4) - 
                  7*Power(t1,3)*(1 + 4*t2) + 
                  Power(t1,2)*(-29 + 110*t2 + 6*Power(t2,2)) + 
                  t1*(-63 + 181*t2 - 113*Power(t2,2) + 20*Power(t2,3))))) \
+ Power(s,2)*(46*t1 - 35*Power(t1,2) - 9*Power(t1,3) + 24*Power(t1,4) - 
            6*Power(t1,5) + 25*t2 + Power(s2,5)*t2 + 44*t1*t2 + 
            42*Power(t1,2)*t2 - 54*Power(t1,3)*t2 + 9*Power(t1,4)*t2 + 
            Power(t1,5)*t2 - 93*Power(t2,2) - 146*t1*Power(t2,2) + 
            29*Power(t1,2)*Power(t2,2) + 11*Power(t1,3)*Power(t2,2) - 
            4*Power(t1,4)*Power(t2,2) + 125*Power(t2,3) + 
            72*t1*Power(t2,3) - 11*Power(t1,2)*Power(t2,3) + 
            6*Power(t1,3)*Power(t2,3) - 71*Power(t2,4) - 
            17*t1*Power(t2,4) - 4*Power(t1,2)*Power(t2,4) + 
            14*Power(t2,5) + t1*Power(t2,5) - 
            Power(s2,4)*(1 - 5*t2 + Power(t2,2) + t1*(3 + t2)) + 
            Power(s2,3)*(-2 + 11*t2 + 15*Power(t2,2) + 3*Power(t2,3) - 
               2*t1*t2*(17 + t2) + Power(t1,2)*(9 + 2*t2)) + 
            Power(s1,5)*(Power(s2,2)*(-2 + t1 + 3*t2) + 
               t1*(t1 + Power(t1,2) + t2 - Power(t2,2)) + 
               s2*(2 + t1 - 2*Power(t1,2) - 5*t2 - 3*t1*t2 + 
                  3*Power(t2,2))) + 
            Power(s2,2)*(15 - 86*t2 + 133*Power(t2,2) - 36*Power(t2,3) - 
               5*Power(t2,4) - Power(t1,3)*(15 + 4*t2) + 
               t1*(27 - 100*t2 - 27*Power(t2,2)) + 
               Power(t1,2)*(27 + 56*t2 + 9*Power(t2,2))) + 
            s2*(-19 + 18*t2 - 84*Power(t2,2) + 95*Power(t2,3) - 
               10*Power(t2,4) + Power(t1,4)*(15 + t2) - 
               2*Power(t1,3)*(25 + 18*t2 + Power(t2,2)) + 
               Power(t1,2)*(-14 + 149*t2 - 17*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-23 + 113*t2 - 180*Power(t2,2) + 48*Power(t2,3))) + 
            Power(s1,4)*(3*Power(s2,4) + 13*Power(t1,4) + 
               Power(t1,3)*(51 - 23*t2) + (-2 + t2)*Power(-1 + t2,2) + 
               Power(s2,3)*(-19 - 19*t1 + 4*t2) + 
               Power(s2,2)*(19 + 42*Power(t1,2) + t1*(82 - 24*t2) - 
                  32*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(29 - 55*t2 + 6*Power(t2,2)) + 
               t1*(11 - 24*t2 + 9*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-39*Power(t1,3) + Power(t1,2)*(-114 + 43*t2) + 
                  t1*(-44 + 79*t2 + 5*Power(t2,2)) + 
                  2*(-4 + 7*t2 + Power(t2,2) - 4*Power(t2,3)))) + 
            Power(s1,3)*(3*Power(s2,5) - 6*Power(t1,5) + 
               Power(s2,4)*(-11 - 22*t1 + 3*t2) + 
               Power(t1,4)*(-45 + 4*t2) + 
               Power(s2,3)*(2 + 56*Power(t1,2) + t1*(78 - 29*t2) + 
                  39*t2 - 6*Power(t2,2)) - 
               Power(-1 + t2,2)*(-10 + 9*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(-21 - 29*t2 + 13*Power(t2,2)) - 
               2*Power(t1,2)*
                (-95 + 109*t2 - 67*Power(t2,2) + 7*Power(t2,3)) + 
               t1*(52 - 182*t2 + 183*Power(t2,2) - 56*Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(s2,2)*(-55 + 64*Power(t1,3) + 
                  Power(t1,2)*(172 - 57*t2) + 62*t2 - 105*Power(t2,2) + 
                  6*Power(t2,3) + t1*(30 + 79*t2 - 2*Power(t2,2))) + 
               s2*(-31 + 33*Power(t1,4) + 114*t2 - 107*Power(t2,2) + 
                  24*Power(t2,3) - 5*Power(t1,3)*(-30 + 7*t2) + 
                  Power(t1,2)*(51 + 65*t2 - 7*Power(t2,2)) + 
                  t1*(-227 + 233*t2 - 199*Power(t2,2) + 9*Power(t2,3)))) \
+ s1*(-Power(t1,6) + Power(s2,5)*(1 + 2*t1 - 6*t2) + 
               Power(t1,5)*(6 + 4*t2) - 
               2*Power(t1,4)*(9 + 5*t2 + 4*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (22 - 61*t2 + 33*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-35 + 77*t2 - 46*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(19 + 255*t2 - 298*Power(t2,2) + 
                  94*Power(t2,3) - 7*Power(t2,4)) + 
               t1*(-173 + 358*t2 - 411*Power(t2,2) + 264*Power(t2,3) - 
                  40*Power(t2,4) + 2*Power(t2,5)) + 
               Power(s2,4)*(6 - 8*Power(t1,2) - 19*t2 - 
                  11*Power(t2,2) + t1*(4 + 26*t2)) + 
               Power(s2,3)*(-19 + 13*Power(t1,3) + 85*t2 - 
                  44*Power(t2,2) + 3*Power(t2,3) - 
                  2*Power(t1,2)*(16 + 17*t2) + 
                  t1*(-51 + 116*t2 + 11*Power(t2,2))) + 
               Power(s2,2)*(-16 - 11*Power(t1,4) + 161*t2 - 
                  143*Power(t2,2) + 46*Power(t2,3) + 15*Power(t2,4) + 
                  18*Power(t1,3)*(3 + t2) + 
                  4*Power(t1,2)*(19 - 51*t2 + 3*Power(t2,2)) - 
                  2*t1*(-21 + 96*t2 - 70*Power(t2,2) + 17*Power(t2,3))) \
+ s2*(78 + 5*Power(t1,5) - 233*t2 + 442*Power(t2,2) - 322*Power(t2,3) + 
                  38*Power(t2,4) - 3*Power(t2,5) - 
                  Power(t1,4)*(33 + 8*t2) + 
                  Power(t1,3)*(-13 + 117*t2 - 4*Power(t2,2)) + 
                  Power(t1,2)*
                   (7 + 28*t2 - 31*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-1 - 353*t2 + 318*Power(t2,2) - 91*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(s1,2)*(Power(t1,5)*(-7 + 2*t2) + 
               Power(s2,5)*(-4 - 3*t1 + 6*t2) + 
               Power(t1,4)*(-16 + 51*t2 - 6*Power(t2,2)) + 
               Power(s2,4)*(1 + 14*Power(t1,2) + t1*(26 - 33*t2) + 
                  6*t2 + 12*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (-12 + 67*t2 - 44*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,3)*(-65 + 205*t2 - 69*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,2)*(-265 + 227*t2 - 147*Power(t2,2) + 
                  10*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(49 - 78*t2 + 103*Power(t2,2) - 92*Power(t2,3) + 
                  18*Power(t2,4)) - 
               Power(s2,3)*(-46 + 24*Power(t1,3) + 
                  Power(t1,2)*(49 - 61*t2) + 145*t2 - 36*Power(t2,2) - 
                  6*Power(t2,3) + t1*(2 + 30*t2 + 39*Power(t2,2))) + 
               Power(s2,2)*(-83 + 18*Power(t1,4) + 
                  Power(t1,3)*(29 - 45*t2) + 54*t2 - 150*Power(t2,2) + 
                  2*Power(t2,3) + 
                  2*Power(t1,2)*(-10 + 52*t2 + 15*Power(t2,2)) - 
                  t1*(160 - 492*t2 + 138*Power(t2,2) + 3*Power(t2,3))) + 
               s2*(-14 - 5*Power(t1,5) + 49*t2 - 135*Power(t2,2) + 
                  103*Power(t2,3) - 3*Power(t2,4) + 
                  Power(t1,4)*(5 + 9*t2) + 
                  Power(t1,3)*(37 - 131*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (190 - 578*t2 + 190*Power(t2,2) - 13*Power(t2,3)) + 
                  t1*(337 - 300*t2 + 372*Power(t2,2) - 61*Power(t2,3) + 
                     6*Power(t2,4))))) + 
         s*(-5 + 30*s2 - 21*Power(s2,2) + Power(s2,3) + 2*Power(s2,4) - 
            64*t1 + 34*s2*t1 - 24*Power(s2,2)*t1 - 5*Power(s2,3)*t1 + 
            3*Power(s2,4)*t1 + 37*Power(t1,2) + 10*s2*Power(t1,2) - 
            14*Power(s2,2)*Power(t1,2) - 9*Power(s2,3)*Power(t1,2) + 
            10*Power(t1,3) + 33*s2*Power(t1,3) + 
            15*Power(s2,2)*Power(t1,3) - 16*Power(t1,4) - 
            15*s2*Power(t1,4) + 6*Power(t1,5) + 9*t2 - 63*s2*t2 + 
            86*Power(s2,2)*t2 + 3*Power(s2,3)*t2 - 6*Power(s2,4)*t2 - 
            2*Power(s2,5)*t2 + 83*t1*t2 - 151*s2*t1*t2 + 
            77*Power(s2,2)*t1*t2 + 42*Power(s2,3)*t1*t2 + 
            2*Power(s2,4)*t1*t2 - 74*Power(t1,2)*t2 - 
            100*s2*Power(t1,2)*t2 - 52*Power(s2,2)*Power(t1,2)*t2 - 
            4*Power(s2,3)*Power(t1,2)*t2 + 22*Power(t1,3)*t2 + 
            14*s2*Power(t1,3)*t2 + 8*Power(s2,2)*Power(t1,3)*t2 + 
            2*Power(t1,4)*t2 - 2*s2*Power(t1,4)*t2 - 2*Power(t1,5)*t2 + 
            7*Power(t2,2) + 150*s2*Power(t2,2) - 
            175*Power(s2,2)*Power(t2,2) - 13*Power(s2,3)*Power(t2,2) - 
            3*Power(s2,4)*Power(t2,2) + 2*Power(s2,5)*Power(t2,2) + 
            18*t1*Power(t2,2) + 293*s2*t1*Power(t2,2) - 
            30*Power(s2,2)*t1*Power(t2,2) - 7*Power(s2,3)*t1*Power(t2,2) - 
            Power(s2,4)*t1*Power(t2,2) + 2*Power(t1,2)*Power(t2,2) + 
            85*s2*Power(t1,2)*Power(t2,2) + 
            10*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
            3*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
            30*Power(t1,3)*Power(t2,2) - 5*s2*Power(t1,3)*Power(t2,2) + 
            Power(s2,2)*Power(t1,3)*Power(t2,2) + 
            5*Power(t1,4)*Power(t2,2) + s2*Power(t1,4)*Power(t2,2) - 
            30*Power(t2,3) - 247*s2*Power(t2,3) + 
            139*Power(s2,2)*Power(t2,3) + 9*Power(s2,3)*Power(t2,3) - 
            3*Power(s2,4)*Power(t2,3) - 37*t1*Power(t2,3) - 
            198*s2*t1*Power(t2,3) - 23*Power(s2,2)*t1*Power(t2,3) + 
            9*Power(s2,3)*t1*Power(t2,3) + 33*Power(t1,2)*Power(t2,3) - 
            2*s2*Power(t1,2)*Power(t2,3) - 
            Power(s2,2)*Power(t1,2)*Power(t2,3) - 
            2*Power(t1,3)*Power(t2,3) - 5*s2*Power(t1,3)*Power(t2,3) + 
            33*Power(t2,4) + 146*s2*Power(t2,4) - 
            29*Power(s2,2)*Power(t2,4) + 8*t1*Power(t2,4) + 
            25*s2*t1*Power(t2,4) + 2*Power(t1,2)*Power(t2,4) + 
            7*s2*Power(t1,2)*Power(t2,4) - 19*Power(t2,5) - 
            16*s2*Power(t2,5) - 8*t1*Power(t2,5) - 3*s2*t1*Power(t2,5) + 
            5*Power(t2,6) + Power(s1,6)*Power(s2 - t1,2)*
             (-1 + s2 - t1 + t2) - 
            Power(s1,5)*(s2 - t1)*
             (4*Power(s2,3) - 7*Power(t1,3) - 2*Power(-1 + t2,2) + 
               Power(s2,2)*(-12 - 15*t1 + 11*t2) + 
               Power(t1,2)*(-16 + 15*t2) - 
               2*t1*(5 - 9*t2 + 4*Power(t2,2)) + 
               s2*(9 + 18*Power(t1,2) + t1*(28 - 26*t2) - 16*t2 + 
                  7*Power(t2,2))) + 
            Power(s1,4)*(-6*Power(s2,5) + 9*Power(t1,5) + 
               Power(t1,4)*(47 - 6*t2) + 
               Power(s2,4)*(31 + 34*t1 - 5*t2) + Power(-1 + t2,3) + 
               16*t1*Power(-1 + t2,3) + 
               Power(t1,3)*(9 - 14*t2 - 14*Power(t2,2)) + 
               Power(t1,2)*(-29 + 58*t2 - 40*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(s2,3)*(-16 - 75*Power(t1,2) + 24*t2 + 
                  11*Power(t2,2) + t1*(-144 + 25*t2)) + 
               Power(s2,2)*(-20 + 81*Power(t1,3) + 
                  Power(t1,2)*(242 - 41*t2) + 39*t2 - 29*Power(t2,2) + 
                  10*Power(t2,3) + t1*(48 - 76*t2 - 29*Power(t2,2))) + 
               s2*(-43*Power(t1,4) - 15*Power(-1 + t2,3) + 
                  Power(t1,3)*(-176 + 27*t2) + 
                  Power(t1,2)*(-41 + 66*t2 + 32*Power(t2,2)) + 
                  t1*(45 - 85*t2 + 57*Power(t2,2) - 17*Power(t2,3)))) + 
            Power(s1,3)*(-3*Power(s2,6) + Power(t1,6) + 
               Power(t1,5)*(4 - 10*t2) + Power(s2,5)*(8 + 20*t1 - 3*t2) - 
               Power(-1 + t2,3)*(-7 + 8*t2) + 
               Power(t1,4)*(44 - 99*t2 + 19*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(5 - 8*t2 + 22*Power(t2,2)) + 
               Power(t1,3)*(264 - 295*t2 + 128*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(97 - 235*t2 + 147*Power(t2,2) - 
                  11*Power(t2,3) + 2*Power(t2,4)) + 
               Power(s2,4)*(13 - 50*Power(t1,2) - 50*t2 + 
                  4*Power(t2,2) + t1*(-37 + 18*t2)) + 
               Power(s2,3)*(-100 + 59*Power(t1,3) + 
                  Power(t1,2)*(58 - 25*t2) + 100*t2 - 89*Power(t2,2) + 
                  4*Power(t2,3) - 3*t1*(28 - 79*t2 + 6*Power(t2,2))) + 
               Power(s2,2)*(63 - 32*Power(t1,4) - 157*t2 + 
                  91*Power(t2,2) + 3*Power(t2,3) - 
                  Power(t1,3)*(33 + 2*t2) + 
                  Power(t1,2)*(178 - 433*t2 + 48*Power(t2,2)) + 
                  t1*(461 - 490*t2 + 305*Power(t2,2) - 21*Power(t2,3))) + 
               s2*(5*Power(t1,5) + 22*Power(t1,4)*t2 + 
                  Power(t1,3)*(-151 + 345*t2 - 53*Power(t2,2)) + 
                  Power(-1 + t2,2)*(4 - 2*t2 + 17*Power(t2,2)) + 
                  Power(t1,2)*
                   (-628 + 694*t2 - 353*Power(t2,2) + 32*Power(t2,3)) - 
                  2*t1*(83 - 207*t2 + 134*Power(t2,2) - 13*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            Power(s1,2)*(-Power(t1,6) + Power(s2,6)*(2 + t1 - 4*t2) + 
               Power(t1,5)*(9 - 2*t2) - 
               Power(s2,5)*(-7 + 5*Power(t1,2) + t1*(7 - 21*t2) + 8*t2 + 
                  8*Power(t2,2)) + 
               Power(-1 + t2,3)*(-4 + 6*t2 + 11*Power(t2,2)) + 
               Power(t1,4)*(48 - 29*t2 + 28*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*
                (-99 + 186*t2 - 62*Power(t2,2) + 4*Power(t2,3)) - 
               Power(t1,3)*(26 + 234*t2 - 209*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(t1,2)*(307 - 694*t2 + 628*Power(t2,2) - 
                  270*Power(t2,3) + 29*Power(t2,4)) + 
               Power(s2,4)*(-37 + 10*Power(t1,3) + 
                  Power(t1,2)*(4 - 40*t2) + 79*t2 - 22*Power(t2,2) - 
                  4*Power(t2,3) + t1*(-53 + 67*t2 + 25*Power(t2,2))) + 
               Power(s2,3)*(22 - 10*Power(t1,4) + 114*t2 - 
                  48*Power(t2,2) + 13*Power(t2,3) + 
                  Power(t1,3)*(11 + 34*t2) + 
                  Power(t1,2)*(115 - 164*t2 - 21*Power(t2,2)) - 
                  t1*(-121 + 334*t2 - 119*Power(t2,2) + Power(t2,3))) + 
               s2*(-Power(t1,6) + Power(t1,5)*(8 + t2) + 
                  Power(t1,4)*(12 - 50*t2 + 5*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (-62 + 131*t2 - 46*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(t1,3)*
                   (47 + 122*t2 - 21*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(-400 + 940*t2 - 995*Power(t2,2) + 
                     506*Power(t2,3) - 51*Power(t2,4)) + 
                  Power(t1,2)*
                   (73 + 545*t2 - 385*Power(t2,2) + 66*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(s2,2)*(102 + 5*Power(t1,5) - 266*t2 + 
                  373*Power(t2,2) - 224*Power(t2,3) + 15*Power(t2,4) - 
                  Power(t1,4)*(17 + 12*t2) - 
                  Power(t1,3)*(90 - 157*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-85 + 406*t2 - 146*Power(t2,2) + 14*Power(t2,3)) - 
                  t1*(75 + 405*t2 - 200*Power(t2,2) + 17*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            s1*(2*Power(t1,6) + 2*Power(s2,6)*t2 - 
               Power(t1,5)*(11 + 4*t2) + 
               Power(t1,4)*(20 + 18*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(76 - 94*t2 + Power(t2,2) - 20*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-19 + 65*t2 - 39*Power(t2,2) + 2*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (-83 + 151*t2 - 123*Power(t2,2) + 22*Power(t2,3)) + 
               Power(t1,2)*(-121 - 39*t2 + 251*Power(t2,2) - 
                  130*Power(t2,3) + 39*Power(t2,4)) - 
               Power(s2,5)*(4 - 10*t2 + Power(t2,2) + t1*(4 + 6*t2)) + 
               Power(s2,4)*(-5 + 5*t2 + 8*Power(t2,2) - 10*Power(t2,3) + 
                  2*Power(t1,2)*(8 + 3*t2) + 
                  t1*(12 - 55*t2 + 14*Power(t2,2))) + 
               Power(s2,3)*(50 - 162*t2 + 206*Power(t2,2) - 
                  48*Power(t2,3) - 9*Power(t2,4) - 
                  2*Power(t1,3)*(13 + t2) + 
                  Power(t1,2)*(6 + 87*t2 - 21*Power(t2,2)) + 
                  t1*(57 - 118*t2 + 2*Power(t2,2) + 24*Power(t2,3))) + 
               Power(s2,2)*(-59 + 22*Power(t1,4) - t2 - 51*Power(t2,2) + 
                  115*Power(t2,3) - 7*Power(t2,4) + 3*Power(t2,5) + 
                  Power(t1,3)*(-43 - 53*t2 + 4*Power(t2,2)) - 
                  Power(t1,2)*
                   (88 - 273*t2 + 66*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(-118 + 449*t2 - 590*Power(t2,2) + 153*Power(t2,3) - 
                     5*Power(t2,4))) - 
               s2*(10*Power(t1,5) - 
                  Power(t1,4)*(40 + 15*t2 + 4*Power(t2,2)) + 
                  Power(t1,3)*
                   (-16 + 178*t2 - 53*Power(t2,2) + 12*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-28 + 106*t2 - 130*Power(t2,2) + 19*Power(t2,3)) + 
                  Power(t1,2)*
                   (1 + 212*t2 - 398*Power(t2,2) + 86*Power(t2,3) - 
                     12*Power(t2,4)) + 
                  t1*(-189 + 25*t2 + 58*Power(t2,2) + 111*Power(t2,3) - 
                     9*Power(t2,4) + 4*Power(t2,5))))))*T5q(1 - s2 + t1 - t2)\
)/((-1 + t1)*(1 - s2 + t1 - t2)*Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,3)*
       (-1 + t2)*(s - s1 + t2)));
   return a;
};
