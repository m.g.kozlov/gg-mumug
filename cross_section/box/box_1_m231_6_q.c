#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m231_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-16*(-8*Power(s1,3)*t1*t2*
          (-t1 + 2*Power(t1,2) + t2 - 3*t1*t2 + Power(t2,2)) + 
         s1*(2*Power(t1,5)*(-3 + t2)*t2 - 
            Power(t2,2)*(1 + t2)*(s2*(-1 + t2) + 2*t2) - 
            Power(t1,4)*(1 + (4 + s - 2*s2)*t2 - 
               (21 + s - 2*s2)*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,3)*(2 + (3 + 17*s + 3*s2)*t2 - 
               (56 + 11*s + 4*s2)*Power(t2,2) + 
               (-39 - 6*s + s2)*Power(t2,3) + 2*Power(t2,4)) + 
            Power(t1,2)*(-1 - 2*(-9 + 4*s + 8*s2)*t2 + 
               (7 - 18*s + s2)*Power(t2,2) + 
               (96 + 17*s + 14*s2)*Power(t2,3) + 
               (32 + 9*s + s2)*Power(t2,4)) + 
            t1*t2*(-11 + (-6 + 8*s)*t2 + (5 + s)*Power(t2,2) - 
               (48 + 5*s)*Power(t2,3) - 4*(2 + s)*Power(t2,4) + 
               s2*(11 + 4*t2 - 15*Power(t2,2)))) + 
         Power(s1,2)*t1*(Power(t1,3)*t2*(-11 + 3*t2) - 
            Power(t1,2)*(1 + (8*s - 3*(13 + s2))*t2 + 
               (-51 - 8*s + 3*s2)*Power(t2,2) + Power(t2,3)) - 
            t1*(-1 + (17 - 4*s - 3*s2)*t2 + (82 - 8*s)*Power(t2,2) + 
               (52 + 12*s + 3*s2)*Power(t2,3) + 2*Power(t2,4)) + 
            t2*(5 - 4*(-3 + s)*t2 + 37*Power(t2,2) + 
               2*(9 + 2*s)*Power(t2,3) + 3*s2*(-2 + t2 + Power(t2,2)))) + 
         t2*(t2*Power(1 + t2,2)*(s2*(-1 + t2) + 2*t2) + 
            Power(t1,4)*(17 - 3*t2 + 2*Power(t2,2) - 
               s*(-3 + t2 + 2*Power(t2,2))) + 
            Power(t1,3)*(s2*(-7 + 5*t2 + 2*Power(t2,2)) - 
               4*(3 + 7*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               s*(-7 - 9*t2 + 12*Power(t2,2) + 4*Power(t2,3))) + 
            Power(t1,2)*(-11 + 28*t2 + 18*Power(t2,2) - 29*Power(t2,3) + 
               2*Power(t2,4) + 
               s2*(12 + t2 - 12*Power(t2,2) - Power(t2,3)) + 
               s*(4 + 6*t2 + 13*Power(t2,2) - 21*Power(t2,3) - 
                  2*Power(t2,4))) + 
            t1*(6 + (3 - 4*s)*t2 + (-26 + s)*Power(t2,2) - 
               (3 + 7*s)*Power(t2,3) + 2*(6 + 5*s)*Power(t2,4) - 
               s2*(5 + 5*t2 - 11*Power(t2,2) + Power(t2,4))))))/
     ((-1 + s1)*(-1 + t1)*t1*(-1 + s1 + t1 - t2)*Power(-1 + t2,2)*t2*
       Power(-t1 + t2,2)) + (8*
       (-8*Power(s1,3)*(-2 + t1)*t1 + 2*(-1 + s)*Power(t1,4)*(-1 + t2) + 
         t1*(3 + (-60 - 10*s + 9*s2)*t2 + 
            (28 + 11*s - 10*s2)*Power(t2,2) + (-3 - s + s2)*Power(t2,3)) \
- 2*Power(s1,2)*(t1*(-3 + 4*s*(-1 + t2) - t2) - Power(t1,3)*(-9 + t2) + 
            s2*(-1 - 3*t1 + Power(t1,2))*(-1 + t2) + 8*t2 - 
            Power(t1,2)*(19 + 2*s*(-1 + t2) + 5*t2)) + 
         Power(t1,3)*(-3 + (13 - 2*s2)*t2 + 2*(-1 + s2)*Power(t2,2) + 
            s*(1 + t2 - 2*Power(t2,2))) - 
         Power(t1,2)*(2 - (-28 + s2)*t2 + (10 + s2)*Power(t2,2) + 
            s*(-3 + t2 + 2*Power(t2,2))) + 
         t2*(-13 + 66*t2 - 17*Power(t2,2) + 4*Power(t2,3) - 
            s*(-4 + t2 + 3*Power(t2,2)) + s2*(10 - 17*t2 + 7*Power(t2,2))\
) + s1*(2*Power(t1,4)*(-5 + t2) + 
            Power(t1,3)*(33 + 6*s*(-1 + t2) - 2*s2*(-1 + t2) + 7*t2) + 
            (-1 + t2)*(-2 + (6 + 10*s - 9*s2)*t2) + 
            Power(t1,2)*(55 - 8*t2 + Power(t2,2) - 
               2*s*(-3 + 2*t2 + Power(t2,2)) + 
               s2*(-7 + 5*t2 + 2*Power(t2,2))) - 
            t1*(-10 + 2*s*Power(-1 + t2,2) + 101*t2 - 7*Power(t2,2) + 
               4*Power(t2,3) + s2*(13 - 20*t2 + 7*Power(t2,2)))))*
       B1(1 - s1 - t1 + t2,t1,t2))/((-1 + s1)*(s1*t1 - t2)*Power(-1 + t2,2)) \
+ (16*(-(Power(t2,2)*(1 + t2)*(s2*(-1 + t2) + 2*t2)) + 
         Power(t1,6)*(14 + 3*s*(-1 + t2) - t2 - Power(t2,2) + 
            s1*(-21 + 5*t2)) + 
         Power(t1,5)*(-17 - 16*Power(s1,2) - 3*s2 + 
            s*(-13 + 8*s1 - 8*t2)*(-1 + t2) - 28*t2 - s2*t2 + 
            3*Power(t2,2) + 4*s2*Power(t2,2) + 2*Power(t2,3) + 
            s1*(45 + 5*s2 + 58*t2 - 5*s2*t2 - 7*Power(t2,2))) - 
         Power(t1,4)*(-9 + s2 - t2 - 19*s2*t2 - 6*Power(t2,2) + 
            13*s2*Power(t2,2) + 11*Power(t2,3) + 5*s2*Power(t2,3) + 
            Power(t2,4) - 16*Power(s1,2)*(1 + 2*t2) + 
            s*(-1 + t2)*(-5 - 30*t2 - 7*Power(t2,2) + 8*s1*(1 + 2*t2)) + 
            s1*(15 + 120*t2 + 45*Power(t2,2) - 4*Power(t2,3) + 
               s2*(3 + 2*t2 - 5*Power(t2,2)))) + 
         Power(t1,3)*(-11 + 7*s2 + 15*t2 - 23*s2*t2 + 84*Power(t2,2) - 
            12*s2*Power(t2,2) + 15*Power(t2,3) + 27*s2*Power(t2,3) + 
            9*Power(t2,4) + s2*Power(t2,4) - 
            8*Power(s1,2)*(1 + 2*t2 + 3*Power(t2,2)) + 
            s*(-1 + t2)*(-3 - 25*Power(t2,2) - 2*Power(t2,3) + 
               4*s1*(1 + 2*t2 + 3*Power(t2,2))) + 
            s1*(13 + 14*t2 + 71*Power(t2,2) + 16*Power(t2,3) - 
               2*Power(t2,4) + 
               s2*(-7 + 12*t2 + 3*Power(t2,2) - 8*Power(t2,3)))) + 
         t1*t2*(1 - (-5 + s + 4*s1)*t2 - (-27 + s + 8*s1)*Power(t2,2) + 
            (23 + 2*s - 4*s1)*Power(t2,3) + 
            s2*(-4 - 4*t2 + Power(t2,2) + 7*Power(t2,3) + 
               s1*(1 + t2 - 2*Power(t2,3)))) + 
         Power(t1,2)*(5 - 4*t2 - 2*s*t2 - 49*Power(t2,2) + 
            9*s*Power(t2,2) - 79*Power(t2,3) - 15*s*Power(t2,3) - 
            13*Power(t2,4) + 8*s*Power(t2,4) + 
            8*Power(s1,2)*(t2 + Power(t2,3)) + 
            s2*(-3 + 9*t2 + 24*Power(t2,2) - 23*Power(t2,3) - 
               7*Power(t2,4)) + 
            s1*(-1 + t2)*(6 + (3 - 4*s)*t2 - 6*Power(t2,2) - 
               2*(1 + 2*s)*Power(t2,3) + 
               s2*(-5 + t2 + 10*Power(t2,2) + 2*Power(t2,3)))))*R1(t1))/
     ((-1 + s1)*Power(-1 + t1,2)*t1*Power(-1 + t2,2)*Power(-t1 + t2,3)) - 
    (16*(Power(s1,4)*(-4*Power(t1,2)*t2 + 4*Power(t2,3)) + 
         Power(s1,3)*(2*Power(t1,3)*(-7 + t2)*t2 + 
            Power(t1,2)*(1 - 2*(1 + s - s2)*t2 + 
               (9 + 2*s - 2*s2)*Power(t2,2)) - 
            t1*t2*(4 - 7*t2 - 26*Power(t2,2) + Power(t2,3) + 
               s2*(-1 + Power(t2,2))) - 
            Power(t2,2)*(3 + (5 - 2*s)*t2 + (15 + 2*s)*Power(t2,2) + 
               Power(t2,3) + s2*(-5 + 2*t2 + 3*Power(t2,2)))) + 
         Power(s1,2)*(4*Power(t1,4)*(-4 + t2)*t2 - 
            2*Power(t1,3)*(-1 + (1 + 2*s - 2*s2)*t2 + 
               (-9 - 2*s + 2*s2)*Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*t2*(-10 - 2*s2*(-1 + t2) + s*Power(-1 + t2,2) + 
               41*t2 + 42*Power(t2,2) + 3*Power(t2,3)) - 
            t1*t2*(-1 + (2 - 2*s - 11*s2)*t2 + 
               (47 - 4*s + 4*s2)*Power(t2,2) + 
               (6*s + 7*(6 + s2))*Power(t2,3) + 6*Power(t2,4)) + 
            Power(t2,2)*(5 - (2 + 3*s)*t2 + 2*(1 + s)*Power(t2,2) + 
               (10 + s)*Power(t2,3) + Power(t2,4) - 
               s2*(6 + t2 - 8*Power(t2,2) + Power(t2,3)))) + 
         s1*(2*Power(t1,5)*(-3 + t2)*t2 + 
            Power(t1,4)*(1 + (4 - 2*s + 2*s2)*t2 + 
               (7 + 2*s - 2*s2)*Power(t2,2)) + 
            Power(t1,2)*t2*(-2 + (7 + 12*s - s2)*t2 + 
               (-103 + 6*s - 6*s2)*Power(t2,2) + 
               (-57 - 18*s + 7*s2)*Power(t2,3) + 3*Power(t2,4)) + 
            Power(t1,3)*t2*(-6 + s2 + 45*t2 + 24*Power(t2,2) - 
               s2*Power(t2,2) - 3*Power(t2,3) + 
               s*(2 - 8*t2 + 6*Power(t2,2))) + 
            2*Power(t2,3)*(6 + (9 - 4*s)*t2 + (31 + 6*s)*Power(t2,2) - 
               2*s*Power(t2,3) + 
               2*s2*(-5 + 2*t2 + 2*Power(t2,2) + Power(t2,3))) - 
            2*t1*Power(t2,2)*(-13 + (13 + 3*s)*t2 + 
               (22 + 4*s)*Power(t2,2) - (19 + 7*s)*Power(t2,3) + 
               Power(t2,4) + s2*(8 + t2 - 16*Power(t2,2) + 7*Power(t2,3))\
)) + t2*(2*Power(t1,5)*(2 - t2 + Power(t2,2)) + 
            Power(t1,3)*(1 + (-22 + 6*s - 3*s2)*t2 + 
               (-13 + 8*s - 8*s2)*Power(t2,2) + 
               (-6 - 14*s + 11*s2)*Power(t2,3) + 8*Power(t2,4)) - 
            2*t1*t2*(2 + 2*(4 + 2*s + s2)*t2 + 
               (-55 - 6*s + 6*s2)*Power(t2,2) - 
               (43 + 6*s2)*Power(t2,3) + 2*(1 + s - s2)*Power(t2,4)) - 
            Power(t1,4)*(-1 + t2)*(s - 5*s*t2 + t2*(3 + 2*s2 + 7*t2)) + 
            4*Power(t2,2)*(-5 + (2 + 3*s)*t2 - 3*(5 + s)*Power(t2,2) - 
               13*Power(t2,3) + Power(t2,4) + s2*(6 + t2 - 7*Power(t2,2))\
) - Power(t1,2)*t2*(-13 + 4*t2 + 34*Power(t2,2) - 4*Power(t2,3) + 
               3*Power(t2,4) + 
               s*(4 + 7*t2 + 2*Power(t2,2) - 13*Power(t2,3)) + 
               s2*(6 - 19*t2 + 13*Power(t2,3)))))*R1(t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       Power(-1 + t2,2)*t2*Power(-t1 + t2,3)) - 
    (16*(12*Power(s1,4) - Power(s1,3)*
          (30 + s2 - 33*t1 + 6*s*(-1 + t2) + 20*t2 - s2*t2 + t1*t2 + 
            2*Power(t2,2)) + Power(t1,3)*
          (-4 - 3*s*(-1 + t2) - 3*t2 + 3*Power(t2,2)) + 
         Power(s1,2)*(22 - 64*t1 + 30*Power(t1,2) + 
            s2*(-1 + 2*t1 - 6*t2)*(-1 + t2) - 
            3*s*(-3 + 5*t1 - 2*t2)*(-1 + t2) - 39*t2 - 43*t1*t2 - 
            2*Power(t1,2)*t2 + 20*Power(t2,2) - t1*Power(t2,2) + 
            Power(t2,3)) + 4*(1 + (-11 + 4*s + 4*s2)*t2 - 
            (16 + 5*s + 3*s2)*Power(t2,2) + (-11 + s - s2)*Power(t2,3) + 
            Power(t2,4)) + Power(t1,2)*
          (16 + s2 + 9*t2 + 5*s2*t2 - 2*Power(t2,2) - 6*s2*Power(t2,2) - 
            3*Power(t2,3) + 3*s*(-3 + t2 + 2*Power(t2,2))) - 
         2*t1*(s2*(1 + 8*t2 - 7*Power(t2,2) - 2*Power(t2,3)) - 
            2*(-2 + 4*t2 + 9*Power(t2,2) + Power(t2,3)) + 
            s*(-1 - 2*t2 + Power(t2,2) + 2*Power(t2,3))) + 
         s1*(-12 + 38*t1 - 38*Power(t1,2) + 9*Power(t1,3) + 112*t2 - 
            18*t1*t2 - 26*Power(t1,2)*t2 - Power(t1,3)*t2 + 
            84*Power(t2,2) + 22*t1*Power(t2,2) + 
            4*Power(t1,2)*Power(t2,2) - 8*Power(t2,3) - 
            2*t1*Power(t2,3) - 
            2*s*(-1 + t2)*(1 + 6*Power(t1,2) - 3*t2 + 2*Power(t2,2) - 
               3*t1*(3 + 2*t2)) + 
            s2*(-1 + t2)*(2 + Power(t1,2) + 14*t2 + 4*Power(t2,2) - 
               2*t1*(1 + 6*t2))))*R2(1 - s1 - t1 + t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*Power(-1 + t2,2)) + 
    (8*(8*Power(s1,6)*t1 + 2*Power(t1,6)*(-1 + s + t2 - s*t2) + 
         Power(t1,4)*(-3 - 3*(-13 + 4*s + s2)*t2 + 
            (8 + 12*s + 3*s2)*Power(t2,2) - 4*Power(t2,3)) - 
         8*t2*(1 - 2*(6 + s - 2*s2)*t2 - (21 + s + s2)*Power(t2,2) + 
            (-10 + 3*s - 3*s2)*Power(t2,3) + 2*Power(t2,4)) + 
         2*Power(s1,5)*(-(Power(t1,2)*(-21 + t2)) + 
            s2*(1 + t1)*(-1 + t2) - 4*t2 - t1*(5 + 2*s*(-1 + t2) + 11*t2)\
) + Power(t1,5)*(5 + (-15 + 2*s2)*t2 - 2*(-1 + s2)*Power(t2,2) + 
            s*(-3 + t2 + 2*Power(t2,2))) + 
         Power(t1,3)*t2*(-19 + 60*t2 - 21*Power(t2,2) + 4*Power(t2,3) + 
            s*(22 - 7*t2 - 15*Power(t2,2)) + 
            s2*(-2 - 13*t2 + 15*Power(t2,2))) + 
         2*Power(t1,2)*t2*(-7 + (-76 + 11*s2)*t2 - 
            (41 + 9*s2)*Power(t2,2) - 2*(-2 + s2)*Power(t2,3) + 
            s*(2 + t2 - 5*Power(t2,2) + 2*Power(t2,3))) - 
         4*t1*t2*(-3 + 6*t2 + 5*Power(t2,2) - 18*Power(t2,3) + 
            2*Power(t2,4) + s*
             (1 + 13*t2 - 12*Power(t2,2) - 2*Power(t2,3)) + 
            s2*(-1 - 5*t2 + 4*Power(t2,2) + 2*Power(t2,3))) + 
         Power(s1,4)*(2 - 8*Power(t1,3)*(-11 + t2) + 
            (8 - 6*s + 7*s2)*t2 + (22 + 6*s - 7*s2)*Power(t2,2) + 
            Power(t1,2)*(-57 - 18*s*(-1 + t2) + 8*s2*(-1 + t2) - 
               83*t2 + 4*Power(t2,2)) - 
            t1*(18 + s2 + 87*t2 - 7*s2*t2 - 25*Power(t2,2) + 
               6*s2*Power(t2,2) - 6*s*(-1 + Power(t2,2)))) + 
         Power(s1,3)*(Power(t1,4)*(92 - 12*t2) + 
            Power(t1,3)*(-113 - 32*s*(-1 + t2) + 12*s2*(-1 + t2) - 
               115*t2 + 12*Power(t2,2)) + 
            t2*(13 + 50*t2 - 23*Power(t2,2) + 
               s*(8 + t2 - 9*Power(t2,2)) + 
               s2*(4 - 13*t2 + 9*Power(t2,2))) + 
            Power(t1,2)*(-37 - 218*t2 + 75*Power(t2,2) - 
               4*Power(t2,3) + s2*(9 + 11*t2 - 20*Power(t2,2)) + 
               s*(-21 + t2 + 20*Power(t2,2))) + 
            t1*(39 + (115 + 16*s2)*t2 + (210 - 20*s2)*Power(t2,2) + 
               4*(-5 + s2)*Power(t2,3) - 
               4*s*(1 + 7*t2 - 9*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,2)*(-8*Power(t1,5)*(-6 + t2) + 
            Power(t1,4)*(-97 - 28*s*(-1 + t2) + 8*s2*(-1 + t2) - 
               67*t2 + 12*Power(t2,2)) + 
            Power(t1,3)*(-15 - 222*t2 + 77*Power(t2,2) - 
               8*Power(t2,3) + s2*(13 + 11*t2 - 24*Power(t2,2)) + 
               3*s*(-9 + t2 + 8*Power(t2,2))) + 
            2*t2*(-21 - (26 + 9*s2)*t2 + (-65 + 11*s2)*Power(t2,2) - 
               2*(-4 + s2)*Power(t2,3) + 
               s*(2 + 5*t2 - 9*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,2)*(61 + (313 + 8*s2)*t2 + 
               (326 - 16*s2)*Power(t2,2) + 4*(-9 + 2*s2)*Power(t2,3) - 
               2*s*(4 + 25*t2 - 33*Power(t2,2) + 4*Power(t2,3))) + 
            t1*(-20 + 127*t2 + 172*Power(t2,2) - 155*Power(t2,3) + 
               12*Power(t2,4) + 
               s*(4 + 62*t2 - 37*Power(t2,2) - 29*Power(t2,3)) + 
               s2*(-4 - 34*t2 + 9*Power(t2,2) + 29*Power(t2,3)))) - 
         s1*(2*Power(t1,6)*(-5 + t2) + 
            Power(t1,5)*(33 + 12*s*(-1 + t2) - 2*s2*(-1 + t2) + 11*t2 - 
               4*Power(t2,2)) - 
            4*t1*(2 - 2*(20 + s - 4*s2)*t2 + 
               (-93 + 5*s - s2)*Power(t2,2) - 
               (71 + 5*s + 5*s2)*Power(t2,3) + 2*(5 + s - s2)*Power(t2,4)\
) - Power(t1,4)*(9 - 98*t2 + 29*Power(t2,2) - 4*Power(t2,3) + 
               s2*(5 + 7*t2 - 12*Power(t2,2)) + 
               3*s*(-5 + t2 + 4*Power(t2,2))) + 
            Power(t1,3)*(-21 + (-245 + 4*s2)*t2 - 146*Power(t2,2) - 
               4*(-5 + s2)*Power(t2,3) + 
               4*s*(1 + 10*t2 - 12*Power(t2,2) + Power(t2,3))) + 
            4*t2*(-5 + 20*t2 + 13*Power(t2,2) - 22*Power(t2,3) + 
               2*Power(t2,4) + 
               s*(1 + 11*t2 - 8*Power(t2,2) - 4*Power(t2,3)) + 
               s2*(-1 - 7*t2 + 4*Power(t2,2) + 4*Power(t2,3))) + 
            Power(t1,2)*(12 - 39*t2 - 150*Power(t2,2) + 137*Power(t2,3) - 
               16*Power(t2,4) + 
               s2*(4 + 40*t2 - 9*Power(t2,2) - 35*Power(t2,3)) + 
               s*(-4 - 76*t2 + 45*Power(t2,2) + 35*Power(t2,3)))))*
       T2(t2,1 - s1 - t1 + t2))/
     ((-1 + s1)*(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)) - 
    (8*(-8*Power(s1,4)*t1*(t1 - t2)*(1 + t2) + 
         2*Power(t1,6)*(-1 + s + t2 - s*t2) + 
         Power(t1,4)*(-3 + (16 + 9*s - 3*s2)*t2 + 
            3*(17 + s - s2)*Power(t2,2) + 6*(-2*s + s2)*Power(t2,3)) + 
         Power(t1,5)*(5 + (-9 + 2*s2)*t2 - 2*(2 + s2)*Power(t2,2) + 
            s*(-3 - 5*t2 + 8*Power(t2,2))) + 
         2*Power(t1,3)*t2*(2*t2*(-22 - 21*t2 + Power(t2,2)) - 
            s2*(1 - 6*t2 + 2*Power(t2,2) + 3*Power(t2,3)) + 
            s*(2 - 7*t2 + Power(t2,2) + 4*Power(t2,3))) - 
         2*Power(t1,2)*t2*(8 - 13*t2 - 67*Power(t2,2) - 33*Power(t2,3) + 
            Power(t2,4) + s2*
             (-2 + t2 + 7*Power(t2,2) - 5*Power(t2,3) - Power(t2,4)) + 
            s*(2 + 4*t2 - 9*Power(t2,2) + 2*Power(t2,3) + Power(t2,4))) + 
         t1*t2*(-(t2*(-8 + 44*t2 + 97*Power(t2,2) + 27*Power(t2,3))) + 
            s2*(4 + 4*t2 - 6*Power(t2,2) + 4*Power(t2,3) - 
               6*Power(t2,4)) + 
            s*(4 + 8*t2 - 15*Power(t2,3) + 3*Power(t2,4))) + 
         t2*(4 - 4*(-3 + s)*t2 - 4*(1 + s)*Power(t2,2) + 
            (17 + 4*s)*Power(t2,3) + 5*(6 + s)*Power(t2,4) - 
            (-5 + s)*Power(t2,5) + 
            s2*(-4 - 16*t2 + 4*Power(t2,2) + 14*Power(t2,3) + 
               Power(t2,4) + Power(t2,5))) + 
         4*Power(s1,3)*(2*Power(t1,4) - 2*Power(t2,2)*(1 + t2) + 
            Power(t1,3)*(-7 - 6*t2 + Power(t2,2)) + 
            t1*(-1 + (-2 + s)*t2 - 10*Power(t2,2) - 
               (7 + s)*Power(t2,3) + s2*(1 + t2 - 2*Power(t2,2))) - 
            Power(t1,2)*(-1 + s - 22*t2 - 12*Power(t2,2) - 
               s*Power(t2,2) + Power(t2,3) + s2*(-2 + t2 + Power(t2,2)))) \
- 2*Power(s1,2)*(Power(t1,5)*(-9 + t2) + 
            Power(t1,4)*(23 + s2 + 2*s*(-1 + t2) + 28*t2 - s2*t2 - 
               3*Power(t2,2)) + 
            Power(t1,3)*(-4 - 69*t2 - 38*Power(t2,2) + 3*Power(t2,3) + 
               s*(2 + 4*t2 - 6*Power(t2,2)) + 
               s2*(-5 + 2*t2 + 3*Power(t2,2))) + 
            Power(t1,2)*(-2 + 18*t2 + 79*Power(t2,2) + 34*Power(t2,3) - 
               Power(t2,4) + 
               s2*(2 + 3*t2 - 2*Power(t2,2) - 3*Power(t2,3)) + 
               2*s*(-2 - 2*t2 + Power(t2,2) + 3*Power(t2,3))) - 
            t2*(s2*(-2 - 2*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               2*(1 - (-4 + s)*t2 + 12*Power(t2,2) + 
                  (7 + s)*Power(t2,3))) + 
            t1*(-4 + 2*(-4 + s)*t2 + 2*(14 + s)*Power(t2,2) - 
               (13 + 2*s)*Power(t2,3) - (15 + 2*s)*Power(t2,4) + 
               s2*(4 + 14*t2 - 13*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4)))) - 
         s1*(2*Power(t1,6)*(-5 + t2) + 
            Power(t1,5)*(31 + 6*s*(-1 + t2) - 2*s2*(-1 + t2) + 39*t2 - 
               6*Power(t2,2)) - 
            Power(t1,4)*(14 + 80*t2 + 64*Power(t2,2) - 6*Power(t2,3) + 
               s2*(5 + 3*t2 - 8*Power(t2,2)) + 
               2*s*(-3 - 7*t2 + 10*Power(t2,2))) + 
            2*Power(t1,3)*(-7 + t2 + 34*Power(t2,2) + 29*Power(t2,3) - 
               Power(t2,4) + 2*s*
                (-1 - 3*t2 - 2*Power(t2,2) + 6*Power(t2,3)) - 
               s2*(-2 - 5*t2 + Power(t2,2) + 6*Power(t2,3))) + 
            2*Power(t1,2)*(t2*
                (5 + 45*t2 - 3*Power(t2,2) - 15*Power(t2,3)) + 
               2*s*(1 + t2 + Power(t2,2) - 3*Power(t2,4)) + 
               2*s2*(1 + 3*t2 - 8*Power(t2,2) + 2*Power(t2,3) + 
                  2*Power(t2,4))) + 
            t2*(s2*(-8 - 20*t2 + 16*Power(t2,2) + 9*Power(t2,3) + 
                  3*Power(t2,4)) + 
               2*(4 + (10 - 4*s)*t2 + (15 - 2*s)*Power(t2,2) + 
                  (38 + 5*s)*Power(t2,3) + (13 + s)*Power(t2,4))) + 
            t1*(4 + 4*(4 + s)*t2 + (-62 + 4*s)*Power(t2,2) - 
               2*(83 + 4*s)*Power(t2,3) - (39 + 2*s)*Power(t2,4) + 
               (7 + 2*s)*Power(t2,5) - 
               2*s2*(2 + 10*t2 - 2*Power(t2,2) - 15*Power(t2,3) + 
                  4*Power(t2,4) + Power(t2,5)))))*T3(t2,t1))/
     ((-1 + s1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)*
       Power(-t1 + t2,2)) - (8*
       (-16*Power(s1,4)*t1 + 2*Power(t1,5)*(-1 + s + t2 - s*t2) + 
         t1*(-3 + (21 - 26*s + s2)*t2 + (70 + 22*s - s2)*Power(t2,2) + 
            4*(2 + s)*Power(t2,3)) + 
         Power(t1,2)*(11 + s*(-3 + t2)*Power(-1 + t2,2) + 
            6*(-12 + s2)*t2 - (46 + 5*s2)*Power(t2,2) - 
            (5 + s2)*Power(t2,3)) + 
         Power(t1,3)*(-15 - 7*(-9 + s2)*t2 + (8 + 7*s2)*Power(t2,2) - 
            4*s*(-2 + t2 + Power(t2,2))) + 
         t2*(5 - 34*t2 - 35*Power(t2,2) + s*(20 + t2 - 21*Power(t2,2)) + 
            s2*(-2 + t2 + Power(t2,2))) + 
         Power(t1,4)*(9 + (-19 + 2*s2)*t2 - 2*(-1 + s2)*Power(t2,2) + 
            s*(-7 + 5*t2 + 2*Power(t2,2))) + 
         8*Power(s1,3)*(-5*Power(t1,2) + Power(t1,3) + 2*t2 + 
            t1*(s*(-1 + t2) + 4*(2 + t2))) + 
         2*Power(s1,2)*(-(Power(t1,4)*(-9 + t2)) + 
            s2*(-1 + 4*t1 - 4*Power(t1,2) + Power(t1,3))*(-1 + t2) + 
            2*Power(t1,2)*(25 + 5*s*(-1 + t2) + 9*t2) - 
            4*t2*(s*(-1 + t2) + 4*(2 + t2)) - 
            2*Power(t1,3)*(s*(-1 + t2) + 2*(9 + t2)) - 
            t1*(23 + 29*t2 + 8*Power(t2,2) + 
               4*s*(-4 + 3*t2 + Power(t2,2)))) + 
         s1*(-2 - 2*Power(t1,5)*(-5 + t2) - (-48 + 30*s + s2)*t2 + 
            (98 + 22*s + s2)*Power(t2,2) + 8*(2 + s)*Power(t2,3) - 
            Power(t1,4)*(51 + 6*s*(-1 + t2) - 2*s2*(-1 + t2) + 5*t2) + 
            Power(t1,3)*(72 + 17*t2 + 7*Power(t2,2) + 
               s2*(9 - 7*t2 - 2*Power(t2,2)) + 
               2*s*(-10 + 9*t2 + Power(t2,2))) + 
            Power(t1,2)*(-33 + 17*t2 - 4*s*(-9 + 7*t2 + 2*Power(t2,2)) + 
               3*s2*(-4 + t2 + 3*Power(t2,2))) + 
            t1*(4 - 75*t2 - 9*Power(t2,2) + 
               s2*(5 + 3*t2 - 8*Power(t2,2)) + 
               2*s*(-11 + 7*t2 + 4*Power(t2,2)))))*T4(t1))/
     ((-1 + s1)*(-1 + t1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)) \
+ (8*(8*Power(s1,4)*t1 + 2*Power(t1,4)*(-1 + s + t2 - s*t2) + 
         t1*(3 + (-29 + 4*s + s2)*t2 + (-61 - 5*s + 3*s2)*Power(t2,2) + 
            (-17 + s - 4*s2)*Power(t2,3)) + 
         2*Power(s1,3)*(-(Power(t1,2)*(-13 + t2)) + 
            s2*(-1 + t1)*(-1 + t2) - 4*t2 - 
            t1*(21 + 2*s*(-1 + t2) + 11*t2)) + 
         Power(t1,3)*(7 + (-15 + 2*s2)*t2 - 2*s2*Power(t2,2) + 
            s*(-5 + t2 + 4*Power(t2,2))) + 
         Power(t1,2)*(-8 + (41 - 5*s2)*t2 + (25 + 3*s2)*Power(t2,2) + 
            2*(-1 + s2)*Power(t2,3) + 
            s*(3 + 2*t2 - 3*Power(t2,2) - 2*Power(t2,3))) + 
         t2*(1 + 43*t2 + 15*Power(t2,2) + 5*Power(t2,3) - 
            s*(8 - 5*t2 - 4*Power(t2,2) + Power(t2,3)) + 
            s2*(2 - 5*t2 + 2*Power(t2,2) + Power(t2,3))) + 
         Power(s1,2)*(-2 - 4*Power(t1,3)*(-7 + t2) + 44*t2 - 2*s*t2 + 
            22*Power(t2,2) + 2*s*Power(t2,2) + 
            Power(t1,2)*(-63 - 10*s*(-1 + t2) - 43*t2 + 2*Power(t2,2)) + 
            s2*(-1 + t2)*(2 + 4*Power(t1,2) + 5*t2 - t1*(5 + 4*t2)) + 
            t1*(44 + 31*t2 + 21*Power(t2,2) + 
               2*s*(-7 + 4*t2 + 3*Power(t2,2)))) - 
         s1*(-2 + 2*Power(t1,4)*(-5 + t2) + (39 - 10*s - 5*s2)*t2 + 
            (64 + 9*s + s2)*Power(t2,2) + (19 + s + 4*s2)*Power(t2,3) + 
            Power(t1,3)*(43 + 8*s*(-1 + t2) - 2*s2*(-1 + t2) + 15*t2 - 
               2*Power(t2,2)) - 
            Power(t1,2)*(42 + 20*t2 + 18*Power(t2,2) + 
               5*s*(-3 + t2 + 2*Power(t2,2)) - 
               s2*(-7 + t2 + 6*Power(t2,2))) + 
            t1*(11 - 22*t2 - 36*Power(t2,2) + 7*Power(t2,3) + 
               s2*(5 + 4*t2 - 7*Power(t2,2) - 2*Power(t2,3)) + 
               2*s*(-5 + 2*t2 + 2*Power(t2,2) + Power(t2,3)))))*
       T5(1 - s1 - t1 + t2))/
     ((-1 + s1)*(-1 + s1 + t1 - t2)*(s1*t1 - t2)*Power(-1 + t2,2)));
   return a;
};
