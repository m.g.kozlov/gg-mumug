#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m321_5_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(Power(s2,2)*(1 - 2*Power(t1,3) + Power(t1,2)*(9 - s1 + t2) + 
            t1*(s1 - 5*(4 + t2))) + 
         s2*(-1 + 2*Power(t1,4) + Power(t1,3)*(-11 + s - 5*t2) + t2 + 
            Power(t1,2)*(-5 + 8*s1 + 31*t2 - 6*s1*t2 + 6*Power(t2,2) + 
               s*(5 + t2)) - t1*
             (-7 + 8*s1 - 29*t2 - 6*s1*t2 + 22*Power(t2,2) + s*(6 + t2))) \
+ t1*(7 - 13*t2 + 6*Power(t2,2) + Power(t1,3)*(2 + s1 + 4*t2) + 
            Power(t1,2)*(27 - 9*s1 - 30*t2 + 6*s1*t2 - 4*Power(t2,2)) + 
            t1*(-16 + 8*s1 - 13*t2 - 6*s1*t2 + 14*Power(t2,2)) - 
            s*(-1 + t1)*(Power(t1,2) - t1*(-8 + t2) - 
               2*(-2 + t2 + Power(t2,2))))))/
     ((-1 + s2)*(s2 - t1)*Power(-1 + t1,2)*t1*(-1 + s2 - t1 + t2)) - 
    (8*(-4*Power(t1,4) + 2*Power(t1,3)*(8 + s - 3*s1 + 3*t2) + 
         (-5 + 3*t2)*(1 + (-1 + s)*t2) - 
         Power(t1,2)*(41 + s1*(-21 + t2) - s*(-3 + t2) + 13*t2 + 
            2*Power(t2,2)) + t1*
          (34 + s1*(-15 + t2) + 19*t2 + 9*Power(t2,2) + 
            s*(1 + 4*t2 - 3*Power(t2,2))) - 
         Power(s2,2)*(-1 - (-1 + s)*Power(t1,3) + 21*t2 + 3*s*t2 + 
            9*Power(t2,2) - 6*Power(t2,3) + 
            s1*(-1 + t1)*(4 + 2*t1 + Power(t1,2) + 3*t2 - 
               2*Power(t2,2)) + 
            Power(t1,2)*(-7 + (-2 + s)*t2 - 2*Power(t2,2)) + 
            t1*(-5 + s + 5*t2 - 4*s*t2 + 9*Power(t2,2) + 2*Power(t2,3))) \
+ Power(s2,3)*(s1*(-1 + t1)*(1 + t1 + 2*t2) + 
            t2*(-5 + Power(t1,2) + 6*t2 - 2*t1*(4 + t2))) + 
         s2*(-4 - 22*t2 - 30*Power(t2,2) + s*Power(t2,2) - 
            6*Power(t2,3) + Power(t1,3)*(1 + s + 2*t2) - 
            Power(t1,2)*(2 + 9*s + 6*t2 + 4*Power(t2,2)) + 
            t1*(25 + 8*s + 42*t2 + 10*Power(t2,2) - s*Power(t2,2) + 
               2*Power(t2,3)) - 
            s1*(-1 + t1)*(5 + Power(t1,2) + 7*t2 + 2*Power(t2,2) - 
               t1*(10 + t2))))*B1(1 - s2 + t1 - t2,s2,t1))/
     ((-1 + s2)*Power(-1 + t1,2)*(-t1 + s2*t2)) + 
    (32*(-2*Power(s2,4) - t1*(1 + t1)*(s*(-1 + t1) + (-4 + t1)*t1) + 
         Power(s2,3)*(1 + s1 + s*(-1 + t1) + 3*t1 - s1*t1 - 5*t2 + 
            t1*t2) + s2*(3 + Power(t1,2)*(-10 + s1 - 4*t2) + 
            s*(-1 + t1)*(-1 + 2*t1 + Power(t1,2) - t2) + 
            2*t1*(-4 + t2) - 3*t2 + Power(t1,3)*(3 - s1 + t2)) + 
         Power(s2,2)*(-3 - (-5 + s)*t2 - 
            Power(t1,2)*(4 + s - 2*s1 + t2) + 
            t1*(11 + s - 2*s1 + 4*t2 + s*t2)))*R1q(s2))/
     (Power(-1 + s2,2)*Power(s2 - t1,2)*Power(-1 + t1,2)) + 
    (16*(Power(s2,4)*(1 - t1 + 7*Power(t1,2) + Power(t1,3)) - 
         Power(s2,3)*(2*Power(t1,4) - 2*t2 + 
            t1*(2 - (-6 + s1)*t2 + Power(t2,2)) - 
            Power(t1,2)*(4*s - s1*(3 + t2) + t2*(31 + t2)) + 
            Power(t1,3)*(4*s + 3*(4 - s1 + t2))) + 
         Power(s2,2)*(Power(t1,5) + Power(t2,2) + 
            Power(t1,4)*(14 + 5*s - 6*s1 + 3*t2) - 
            t1*(4 + t2 - (-8 + s + s1)*Power(t2,2) + Power(t2,3)) - 
            Power(t1,3)*(69 + 35*t2 + 7*Power(t2,2) - s1*(2 + 5*t2) + 
               s*(5 + 8*t2)) + 
            Power(t1,2)*(22 + (-23 + 8*s)*t2 - (-34 + s)*Power(t2,2) + 
               Power(t2,3) - s1*(-4 + 5*t2 + Power(t2,2)))) + 
         s2*t1*(-(Power(t1,4)*(11 + 3*s - 3*s1 + 2*t2)) + 
            Power(t2,2)*(1 + (-3 + s)*t2) + 
            t1*(-4 + (26 + 4*s)*t2 + (5*s - 2*(7 + s1))*Power(t2,2) - 
               (-10 + s)*Power(t2,3)) + 
            Power(t1,3)*(94 + s1*(5 - 7*t2) + 19*t2 + 5*Power(t2,2) + 
               s*(-1 + 5*t2)) - 
            Power(t1,2)*(-21 + 23*t2 + 28*Power(t2,2) + 3*Power(t2,3) + 
               s1*(8 - 7*t2 - 2*Power(t2,2)) + 
               s*(-4 + 9*t2 + 5*Power(t2,2)))) + 
         Power(t1,2)*(4*Power(t1,4) + 
            Power(t2,2)*(4 - 3*t2 + s*(4 + t2)) + 
            Power(t1,3)*(-40 + s*(8 - 3*t2) + 5*t2 - 3*Power(t2,2) + 
               s1*(-4 + 3*t2)) + 
            Power(t1,2)*(-4 - 2*t2 + 13*Power(t2,2) + s*(8 + 7*t2) - 
               s1*(-4 + 3*t2 + Power(t2,2))) - 
            t1*(16 - 9*t2 - (2 + s1)*Power(t2,2) + Power(t2,3) + 
               s*(16 + 4*t2 + 4*Power(t2,2) + Power(t2,3)))))*R1q(t1))/
     ((-1 + s2)*Power(s2 - t1,2)*Power(-1 + t1,2)*t1*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) + 
    (16*(6 + 4*Power(s2,4) + 4*Power(t1,4) - 11*t2 + 4*s*t2 - 4*s1*t2 + 
         25*Power(t2,2) - 8*s*Power(t2,2) + 3*s1*Power(t2,2) - 
         12*Power(t2,3) + 3*s*Power(t2,3) - 
         Power(s2,3)*(20 + s1 + 2*s*(-1 + t1) + 6*t1 - s1*t1 + 
            2*Power(t1,2) - 17*t2 + t1*t2) + 
         Power(t1,3)*(-38 - 3*s*(-2 + t2) + 5*t2 - 3*Power(t2,2) + 
            s1*(-2 + 3*t2)) + 
         Power(s2,2)*(20 + Power(t1,3) + s*(-1 + t1)*(5 + 4*t1 - 7*t2) + 
            Power(t1,2)*(14 - 5*s1 - 2*t2) - 49*t2 - 2*s1*t2 + 
            22*Power(t2,2) + t1*
             (-39 + 5*s1 - 17*t2 + 2*s1*t2 - 2*Power(t2,2))) - 
         Power(t1,2)*(26 - 7*t2 + Power(t2,2) - 3*Power(t2,3) + 
            s*(8 + 6*t2 - 5*Power(t2,2)) + 
            2*s1*(8 - 9*t2 + 3*Power(t2,2))) + 
         s2*(-13 + 107*t1 + 57*Power(t1,2) - 7*Power(t1,3) + 43*t2 - 
            40*t1*t2 + 23*Power(t1,2)*t2 - 2*Power(t1,3)*t2 - 
            41*Power(t2,2) - 14*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) + 9*Power(t2,3) - t1*Power(t2,3) + 
            s1*(-1 + t1)*(2 + 3*Power(t1,2) - 11*t1*(-1 + t2) - 3*t2 + 
               Power(t2,2)) - 
            s*(-1 + t1)*(2 + 3*Power(t1,2) + t1*(7 - 9*t2) - 13*t2 + 
               8*Power(t2,2))) + 
         t1*(-58 + 15*t2 + 23*Power(t2,2) - 3*Power(t2,3) + 
            s1*(18 - 17*t2 + 3*Power(t2,2)) + 
            s*(2 + 5*t2 + 3*Power(t2,2) - 3*Power(t2,3))))*
       R2q(1 - s2 + t1 - t2))/
     ((-1 + s2)*Power(-1 + t1,2)*(-1 + s2 - t1 + t2)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) + 
    (8*(4*Power(t1,5)*(5 + s - s1 + 2*t2) - 
         Power(t2,3)*(-5 + 3*t2)*(1 + (-1 + s)*t2) + 
         Power(s2,5)*(s1*(-1 + Power(t1,2)) + 
            (7 + 4*t1 + Power(t1,2) - 4*t2)*t2) + 
         t1*(12 + 4*(-9 + 2*s - 2*s1)*t2 + (62 - 40*s)*Power(t2,2) + 
            (-36 + 13*s + 11*s1)*Power(t2,3) + (1 - 5*s1)*Power(t2,4) + 
            3*(1 + s)*Power(t2,5)) - 
         4*Power(t1,4)*(s*(-1 + t2 + Power(t2,2)) - 
            s1*(-1 + t2 + Power(t2,2)) + t2*(10 + 4*t2 + Power(t2,2))) - 
         Power(s2,4)*(-1 - (-1 + s)*Power(t1,3) + 10*t2 + 3*s*t2 - 
            26*Power(t2,2) + 2*s*Power(t2,2) + 18*Power(t2,3) + 
            Power(t1,2)*(5 + 3*(5 + s)*t2 + Power(t2,2)) + 
            s1*(-1 + t1)*(5 + t1 + Power(t1,2) - 4*t2 - 7*t1*t2 + 
               2*Power(t2,2)) + 
            t1*(7 + s - 5*t2 - 6*s*t2 - 31*Power(t2,2) - 
               2*s*Power(t2,2) - 2*Power(t2,3))) + 
         2*Power(t1,3)*(24 - 50*t2 + 25*Power(t2,2) + 8*Power(t2,3) + 
            3*Power(t2,4) - s1*
             (18 - 26*t2 + 7*Power(t2,2) + 3*Power(t2,3)) + 
            s*(-22 - 2*t2 + 7*Power(t2,2) + 3*Power(t2,3))) + 
         Power(t1,2)*(-80 + 72*t2 - 16*Power(t2,2) + 27*Power(t2,3) - 
            19*Power(t2,4) - 2*Power(t2,5) + 
            s*(36 + 30*Power(t2,2) - 19*Power(t2,3) - 5*Power(t2,4)) + 
            s1*(44 - 48*t2 + 10*Power(t2,2) - 5*Power(t2,3) + 
               5*Power(t2,4))) + 
         Power(s2,3)*(-5 + (-9 + 11*s + 9*s1)*t2 - 
            (7 + 10*s + 8*s1)*Power(t2,2) + 
            (38 - 6*s + 6*s1)*Power(t2,3) - 30*Power(t2,4) + 
            Power(t1,3)*(16 + 13*t2 + 4*Power(t2,2) - 5*s1*(2 + t2) + 
               s*(2 + 5*t2)) - 
            Power(t1,2)*(1 + 90*t2 + 45*Power(t2,2) + 11*Power(t2,3) + 
               s1*(-11 + 3*t2 - 17*Power(t2,2)) + 
               s*(11 - 3*t2 + 11*Power(t2,2))) + 
            t1*(14 - 18*t2 + 8*Power(t2,2) + 69*Power(t2,3) + 
               6*Power(t2,4) - 
               s1*(1 + t2 + 9*Power(t2,2) + 6*Power(t2,3)) + 
               s*(9 - 19*t2 + 21*Power(t2,2) + 6*Power(t2,3)))) + 
         Power(s2,2)*(-4*Power(t1,4)*(3 + 2*s - 2*s1 + 3*t2) + 
            Power(t1,3)*(62 + 116*t2 + 35*Power(t2,2) + 8*Power(t2,3) + 
               s1*(2 - 22*t2 - 7*Power(t2,2)) + 
               s*(-2 + 6*t2 + 7*Power(t2,2))) - 
            t2*(-15 + 37*t2 - 23*Power(t2,2) - 26*Power(t2,3) + 
               22*Power(t2,4) + 
               s1*(-4 + t2 + 8*Power(t2,2) - 6*Power(t2,3)) + 
               s*(4 - 31*t2 + 14*Power(t2,2) + 6*Power(t2,3))) + 
            Power(t1,2)*(-16 + 61*t2 - 180*Power(t2,2) - 
               51*Power(t2,3) - 15*Power(t2,4) - 
               s*(-26 + 25*t2 + 3*Power(t2,2) + 13*Power(t2,3)) + 
               s1*(14 + t2 + 3*Power(t2,2) + 17*Power(t2,3))) + 
            t1*(14 - 28*t2 - 42*Power(t2,2) + 4*Power(t2,3) + 
               61*Power(t2,4) + 6*Power(t2,5) + 
               s1*(-24 + 17*t2 + 5*Power(t2,2) - 9*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s*(-16 + 23*t2 - 35*Power(t2,2) + 27*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         s2*(20*Power(t1,2) - 48*Power(t1,3) - 76*Power(t1,4) + 
            8*Power(t1,5) - 12*t2 + 96*t1*t2 - 20*Power(t1,2)*t2 + 
            108*Power(t1,3)*t2 - 44*Power(t1,4)*t2 + 21*Power(t2,2) - 
            74*t1*Power(t2,2) + 117*Power(t1,2)*Power(t2,2) + 
            88*Power(t1,3)*Power(t2,2) - 16*Power(t1,4)*Power(t2,2) - 
            39*Power(t2,3) - 34*t1*Power(t2,3) - 
            110*Power(t1,2)*Power(t2,3) + 31*Power(t1,3)*Power(t2,3) + 
            23*Power(t2,4) + 4*t1*Power(t2,4) - 
            23*Power(t1,2)*Power(t2,4) + 4*Power(t1,3)*Power(t2,4) + 
            7*Power(t2,5) + 19*t1*Power(t2,5) - 
            6*Power(t1,2)*Power(t2,5) - 6*Power(t2,6) + 
            2*t1*Power(t2,6) + 
            s1*(8*Power(t1,4)*(1 + t2) + 
               Power(t2,2)*(8 - 5*t2 - 3*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,3)*(16 - 16*t2 + 22*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(12 + 4*t2 - 19*Power(t2,2) + 
                  11*Power(t2,3) + 6*Power(t2,4)) - 
               t1*(4 + 28*t2 - 33*Power(t2,2) + 3*Power(t2,3) + 
                  3*Power(t2,4) + 2*Power(t2,5))) + 
            s*(-8*Power(t1,4)*(-1 + t2) - 
               Power(t2,2)*(8 - 25*t2 + 10*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(16 - 16*t2 + 14*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(t1,2)*(28 - 76*t2 + 29*Power(t2,2) + 
                  11*Power(t2,3) + 5*Power(t2,4)) + 
               t1*(4 - 52*t2 + 23*Power(t2,2) - 17*Power(t2,3) + 
                  15*Power(t2,4) + 2*Power(t2,5)))))*
       T2q(t1,1 - s2 + t1 - t2))/
     ((-1 + s2)*Power(-1 + t1,2)*(-1 + s2 - t1 + t2)*(-t1 + s2*t2)*
       (Power(s2,2) - 4*t1 + 2*s2*t2 + Power(t2,2))) - 
    (8*(Power(s2,4)*(s1*(-1 + t1)*(1 + t1 + 2*t2) + 
            t2*(11 + Power(t1,2) + 6*t2 - 2*t1*(4 + t2))) - 
         Power(s2,3)*(-1 + 16*t2 - 5*s*t2 - 17*Power(t2,2) - 
            6*Power(t2,3) + Power(t1,3)*(1 - s + 2*t2) + 
            Power(t1,2)*(-7 + (-17 + s)*t2 - 6*Power(t2,2)) + 
            t1*(11 + s + 51*t2 + 4*s*t2 + 19*Power(t2,2) + 
               2*Power(t2,3)) + 
            s1*(-1 + t1)*(5 + 3*Power(t1,2) - 3*t2 - 2*Power(t2,2) + 
               t1*(5 + 4*t2))) - 
         t1*(2*Power(t1,4)*(-1 + t2) + t1*(-1 + 2*s1 - 5*t2)*(-1 + t2) - 
            6*Power(-1 + t2,2) + 
            Power(t1,3)*(-27 + 3*s1*(-1 + t2) - 3*t2 - 2*Power(t2,2)) + 
            Power(t1,2)*(-38 - 5*s1*(-1 + t2) + 45*t2 + Power(t2,2)) - 
            s*(-1 + t1)*(Power(t1,2)*(-5 + t2) - 
               t1*(14 - 7*t2 + Power(t2,2)) + 2*(-2 + t2 + Power(t2,2)))) \
+ Power(s2,2)*(-5 + (3*s + 2*(7 + s1))*t2 + 
            (-19 + 5*s - 2*s1)*Power(t2,2) + 10*Power(t2,3) + 
            Power(t1,4)*(2 - 2*s + 3*s1 + t2) + 
            2*Power(t1,3)*(-6 + (-6 + s)*t2 - 3*Power(t2,2) + 
               s1*(2 + t2)) + 
            Power(t1,2)*(45 + 78*t2 + 22*Power(t2,2) + 2*Power(t2,3) + 
               s*(1 + 9*t2 - 4*Power(t2,2)) + 
               s1*(8 - 15*t2 - 2*Power(t2,2))) + 
            t1*(18 + 31*t2 - 69*Power(t2,2) - 4*Power(t2,3) - 
               s*(-1 + 14*t2 + Power(t2,2)) + 
               s1*(-15 + 11*t2 + 4*Power(t2,2)))) + 
         s2*((-1 + s - s1)*Power(t1,5) + 
            Power(t1,4)*(3 - 2*s1 - (-5 + s)*t2 + 2*Power(t2,2)) + 
            2*(-1 + t2)*t2*(3 - 3*t2 + s*(2 + t2)) - 
            Power(t1,3)*(59 + s1*(10 - 13*t2) + 47*t2 + 5*Power(t2,2) + 
               2*Power(t2,3) + s*(-3 + 2*t2 - 2*Power(t2,2))) - 
            2*t1*(-((-1 + t2)*(-2 + s1 + s1*t2 - 4*Power(t2,2))) + 
               s*(4 + t2 + 3*Power(t2,2))) + 
            Power(t1,2)*(-63 + 48*t2 + 35*Power(t2,2) + 4*Power(t2,3) + 
               s1*(15 - 13*t2 - 2*Power(t2,2)) + 
               s*(4 + 9*t2 + 2*Power(t2,2) - 2*Power(t2,3)))))*T3q(s2,t1))/
     ((-1 + s2)*(s2 - t1)*Power(-1 + t1,2)*(-1 + s2 - t1 + t2)*
       (-t1 + s2*t2)) - (8*(Power(t1,3)*(2 - 6*t2) - 
         (-5 + 3*t2)*(1 + (-1 + s)*t2) + 
         Power(t1,2)*(-51 + s*(5 - 3*t2) + s1*(5 - 3*t2) + 19*t2 + 
            6*Power(t2,2)) + t1*
          (-28 + 55*t2 - 21*Power(t2,2) + s1*(-5 + 3*t2) + 
            s*(-5 - 2*t2 + 3*Power(t2,2))) - 
         Power(s2,2)*(-1 - (-1 + s)*Power(t1,3) + 39*t2 - s*t2 - 
            17*Power(t2,2) - 6*Power(t2,3) + 
            s1*(-1 + t1)*(6 + 4*t1 + Power(t1,2) - 3*t2 - 
               2*Power(t2,2)) + 
            Power(t1,2)*(-3 + (-4 + s)*t2 - 2*Power(t2,2)) + 
            t1*(7 + s + 13*t2 + 11*Power(t2,2) + 2*Power(t2,3))) + 
         Power(s2,3)*(s1*(-1 + t1)*(1 + t1 + 2*t2) + 
            t2*(7 + Power(t1,2) + 6*t2 - 2*t1*(2 + t2))) + 
         s2*(-6 + 32*t2 - 6*s*t2 - 50*Power(t2,2) + 3*s*Power(t2,2) + 
            18*Power(t2,3) - Power(t1,3)*(1 + s + 2*t2) + 
            s1*(-1 + t1)*(5 + Power(t1,2) + t1*(6 - 5*t2) - 13*t2 + 
               6*Power(t2,2)) + 
            t1*(43 + 26*t2 - 22*Power(t2,2) - 6*Power(t2,3) + 
               s*(6 + 2*t2 - 3*Power(t2,2))) + 
            Power(t1,2)*(s*(-5 + 4*t2) + 8*(1 + t2 + Power(t2,2)))))*
       T4q(-1 + s2))/
     ((-1 + s2)*Power(-1 + t1,2)*(-1 + s2 - t1 + t2)*(-t1 + s2*t2)) + 
    (8*(2*Power(t1,4)*(1 + t2) + 
         (1 + (-1 + s)*t2)*(5 - 8*t2 + 3*Power(t2,2)) + 
         Power(t1,3)*(-13 + s - 13*t2 - 3*s*t2 - 4*Power(t2,2) + 
            s1*(-3 + 5*t2)) + Power(t1,2)*
          (-15 + 9*t2 + 14*Power(t2,2) + 2*Power(t2,3) + 
            s1*(-4 + 7*t2 - 5*Power(t2,2)) + s*(4 - 6*t2 + 6*Power(t2,2))) \
+ Power(s2,3)*(s1*(-1 + t1)*(1 + t1 + 2*t2) + 
            t2*(3 + Power(t1,2) + 6*t2 - 2*t1*(4 + t2))) - 
         Power(s2,2)*(-1 + 27*t2 - s*t2 - 24*Power(t2,2) - 
            12*Power(t2,3) + Power(t1,3)*(1 - s + t2) + 
            Power(t1,2)*(-7 + (-12 + s)*t2 - 5*Power(t2,2)) + 
            t1*(3 + s + 12*t2 + 29*Power(t2,2) + 4*Power(t2,3)) + 
            s1*(-1 + t1)*(6 + 2*Power(t1,2) - 4*t2 - 4*Power(t2,2) + 
               t1*(5 + t2))) + 
         t1*(s*(-5 + 4*t2 + 2*Power(t2,2) - 3*Power(t2,3)) + 
            (-1 + t2)*(s1*(-7 + 5*t2) - 3*(-9 + 4*t2 + Power(t2,2)))) + 
         s2*(-6 - (-1 + s)*Power(t1,4) + 37*t2 - 6*s*t2 - 38*Power(t2,2) + 
            8*s*Power(t2,2) + Power(t2,3) + 6*Power(t2,4) + 
            Power(t1,3)*(-2*Power(2 + t2,2) + s*(-1 + 2*t2)) + 
            s1*(-1 + t1)*(5 + Power(t1,3) - Power(t1,2)*(-4 + t2) - 
               6*t2 - Power(t2,2) + 2*Power(t2,3) + 
               t1*(11 - 13*t2 - 2*Power(t2,2))) + 
            Power(t1,2)*(7 + 40*t2 + 18*Power(t2,2) + 4*Power(t2,3) - 
               s*(4 - 8*t2 + Power(t2,2))) - 
            t1*(-30 + 21*t2 + 10*Power(t2,2) + 17*Power(t2,3) + 
               2*Power(t2,4) + s*(-6 + 4*t2 + 7*Power(t2,2)))))*
       T5q(1 - s2 + t1 - t2))/
     ((-1 + s2)*Power(-1 + t1,2)*(-1 + s2 - t1 + t2)*(-t1 + s2*t2)));
   return a;
};
