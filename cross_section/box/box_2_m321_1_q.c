#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m321_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(-2*Power(s2,10)*t1*t2*(s1 + t2) - 
         (-2 + s)*Power(t1,3)*
          (Power(s,2)*(2 + t1 - t2) + 2*(-t1 + Power(t1,2) - t2) + 
            s*(-3 + 3*t1 - s1*t1 + 3*t2))*
          (4*Power(t1,2) + (-1 + t2)*Power(t2,2) - 
            t1*(-4 + 4*t2 + Power(t2,2))) + 
         Power(s2,9)*(Power(s1,3)*t1 - 2*Power(s1,2)*t1*t2 + 
            s1*t1*((8 + s - 11*t2)*t2 + t1*(2 + 8*t2)) + 
            t2*(2*(1 + t2) + Power(t1,2)*(4 - 2*s + 6*t2) + 
               t1*(12 + s*(-14 + t2) - 8*t2 - 8*Power(t2,2)))) - 
         Power(s2,8)*(-4*t2*(-1 + t2 + 2*Power(t2,2)) + 
            Power(t1,3)*(2 - 2*s + 8*s1 + 12*t2 - 6*s*t2 + 12*s1*t2 + 
               6*Power(t2,2)) + 
            Power(t1,2)*(12 + 4*Power(s1,3) + 18*t2 - Power(s,2)*t2 - 
               47*Power(t2,2) - 13*Power(t2,3) - 
               2*Power(s1,2)*(1 + 7*t2) + 
               s1*(8 + 7*t2 - 29*Power(t2,2)) + 
               s*(-14 + s1 - 3*Power(s1,2) - 36*t2 + 14*s1*t2 + 
                  18*Power(t2,2))) + 
            t1*(2 - Power(s1,3)*(-2 + t2) + 
               (6 - 25*s - 14*Power(s,2))*t2 + 
               (-12 + 5*s + Power(s,2))*Power(t2,2) + 
               2*(-5 + s)*Power(t2,3) + 17*Power(t2,4) + 
               Power(s1,2)*(3 + (-23 + 5*s)*t2 + 15*Power(t2,2)) + 
               s1*t2*(53 - 2*Power(s,2) + s*(10 - 11*t2) - 53*t2 + 
                  21*Power(t2,2)))) + 
         Power(s2,7)*(2*t2*(-1 - 9*t2 - 2*Power(t2,2) + 6*Power(t2,3)) + 
            2*Power(t1,4)*(3 + 6*t2 + Power(t2,2) - 3*s*(1 + t2) + 
               s1*(6 + 4*t2)) - 
            t1*(-4 + (-23 + 24*s + 30*Power(s,2) - 14*Power(s,3))*t2 - 
               (53 + 15*s + 71*Power(s,2) + 2*Power(s,3))*Power(t2,2) + 
               (86 - 90*s - 6*Power(s,2))*Power(t2,3) - 85*Power(t2,4) + 
               18*Power(t2,5) + Power(s1,3)*(1 + 17*t2 + Power(t2,2)) + 
               Power(s1,2)*(-6 - (13 + 44*s)*t2 + 
                  (-106 + 11*s)*Power(t2,2) + 32*Power(t2,3)) + 
               s1*(-3 + (3 + 11*s + 13*Power(s,2))*t2 + 
                  2*(112 + 38*s + Power(s,2))*Power(t2,2) - 
                  (90 + 11*s)*Power(t2,3) + 25*Power(t2,4))) + 
            Power(t1,3)*(26 + 6*Power(s1,3) + 
               Power(s,2)*(-1 + 3*s1 - 11*t2) - 36*t2 - 61*Power(t2,2) - 
               9*Power(t2,3) - 2*Power(s1,2)*(7 + 13*t2) + 
               s1*(18 - 29*t2 - 27*Power(t2,2)) + 
               s*(-37 - 9*Power(s1,2) + 7*t2 + 22*Power(t2,2) + 
                  s1*(14 + 33*t2))) + 
            Power(t1,2)*(4 - 79*t2 + 2*Power(s,3)*t2 - 67*Power(t2,2) + 
               79*Power(t2,3) + 9*Power(t2,4) + Power(s1,3)*(7 + t2) + 
               Power(s,2)*(-14 + s1*(-2 + t2) - 5*t2 + 7*Power(t2,2)) + 
               Power(s1,2)*(-11 - 55*t2 + 54*Power(t2,2)) + 
               s1*(53 + 46*t2 - 65*Power(t2,2) + 44*Power(t2,3)) + 
               s*(-25 - 59*t2 + 93*Power(t2,2) - 36*Power(t2,3) + 
                  Power(s1,2)*(-4 + 6*t2) + 
                  s1*(4 + 63*t2 - 50*Power(t2,2))))) + 
         Power(s2,6)*(2*t2*(5 + 5*t2 - 12*Power(t2,2) - 8*Power(t2,3) + 
               4*Power(t2,4)) - 
            2*Power(t1,5)*(3 + 2*t2 - s*(3 + t2) + s1*(4 + t2)) + 
            t1*(1 + (-26 + 52*s + 29*Power(s,2) - 38*Power(s,3))*t2 + 
               (67 - 28*s - 152*Power(s,2) + 29*Power(s,3))*
                Power(t2,2) + 
               (217 - 140*s + 94*Power(s,2) + 4*Power(s,3))*
                Power(t2,3) + 
               (-215 + 131*s + 15*Power(s,2))*Power(t2,4) + 
               2*(54 + 5*s)*Power(t2,5) - 7*Power(t2,6) - 
               Power(s1,3)*(-2 + 28*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(3 + (16 - 39*s)*t2 + 
                  (12 + 101*s)*Power(t2,2) + (151 - 7*s)*Power(t2,3) - 
                  27*Power(t2,4)) - 
               s1*(6 + (-50 + 32*s - 20*Power(s,2))*t2 + 
                  3*(-5 - 3*s + 6*Power(s,2))*Power(t2,2) + 
                  2*(164 + 51*s + 5*Power(s,2))*Power(t2,3) + 
                  (-69 + 7*s)*Power(t2,4) + 21*Power(t2,5))) + 
            Power(t1,4)*(-3 + Power(s,3) - 4*Power(s1,3) + 69*t2 + 
               34*Power(t2,2) + 4*Power(t2,3) + 
               2*Power(s1,2)*(13 + 9*t2) + s1*t2*(53 + 11*t2) + 
               Power(s,2)*(11 - 6*s1 + 14*t2) + 
               s*(11 + 9*Power(s1,2) - 46*t2 - 7*Power(t2,2) - 
                  s1*(33 + 28*t2))) + 
            Power(t1,3)*(67 + 247*t2 - 129*Power(t2,2) - 
               51*Power(t2,3) - Power(s1,3)*(13 + 5*t2) + 
               Power(s,3)*(-2 + 8*t2) + 
               Power(s1,2)*(52 - 11*t2 - 67*Power(t2,2)) - 
               s1*(99 - 91*t2 + 17*Power(t2,2) + 25*Power(t2,3)) - 
               Power(s,2)*(-3 + 5*t2 + 16*Power(t2,2) + 
                  s1*(13 + 8*t2)) + 
               s*(64 - 161*t2 + 18*Power(t2,2) + 32*Power(t2,3) + 
                  3*Power(s1,2)*(7 + t2) + 
                  s1*(-56 - 44*t2 + 75*Power(t2,2)))) + 
            Power(t1,2)*(-27 - 86*t2 - 22*Power(t2,2) - 240*Power(t2,3) + 
               22*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*(-14 - 29*t2 + 7*Power(t2,2)) + 
               Power(s1,3)*(23 + 59*t2 + 14*Power(t2,2)) + 
               Power(s1,2)*(-37 - 263*t2 - 210*Power(t2,2) + 
                  82*Power(t2,3)) + 
               s1*(-9 + 460*t2 + 309*Power(t2,2) - 107*Power(t2,3) + 
                  41*Power(t2,4)) + 
               Power(s,2)*(30 - 128*t2 - 83*Power(t2,2) + 
                  15*Power(t2,3) + s1*(13 + 19*t2 + 10*Power(t2,2))) + 
               s*(27 + 3*t2 - 470*Power(t2,2) + 45*Power(t2,3) - 
                  27*Power(t2,4) + 
                  Power(s1,2)*(-41 - 86*t2 + 5*Power(t2,2)) + 
                  s1*(29 + 158*t2 + 229*Power(t2,2) - 54*Power(t2,3))))) + 
         Power(s2,5)*((2 - 2*s + 2*s1)*Power(t1,6) + 
            2*t2*Power(1 + t2,2)*
             (-4 + 13*t2 - 9*Power(t2,2) + Power(t2,3)) + 
            t1*(-8 + 2*(2 - 5*s - 12*Power(s,2) + 17*Power(s,3))*t2 + 
               (-77 + 145*s + 115*Power(s,2) - 84*Power(s,3))*
                Power(t2,2) + 
               (93 + 44*s - 228*Power(s,2) + 16*Power(s,3))*
                Power(t2,3) + 
               (268 - 234*s + 31*Power(s,2) + 2*Power(s,3))*
                Power(t2,4) + 
               2*(-93 + 21*s + 4*Power(s,2))*Power(t2,5) + 
               (41 + 7*s)*Power(t2,6) + 
               Power(s1,3)*t2*(4 + 3*t2 - 13*Power(t2,2)) - 
               Power(s1,2)*(6 + 8*t2 + (-4 + 78*s)*Power(t2,2) + 
                  (29 - 70*s)*Power(t2,3) + (-76 + s)*Power(t2,4) + 
                  8*Power(t2,5)) - 
               s1*(3 + (74 - 64*s + 9*Power(s,2))*t2 + 
                  (-92 + 56*s - 38*Power(s,2))*Power(t2,2) - 
                  (59 + 43*s + 3*Power(s,2))*Power(t2,3) + 
                  2*(100 + 8*s + 3*Power(s,2))*Power(t2,4) + 
                  8*(-5 + s)*Power(t2,5) + 8*Power(t2,6))) - 
            Power(t1,5)*(Power(s,3) - Power(s1,3) + 
               2*Power(s1,2)*(9 + 2*t2) + 
               Power(s,2)*(14 - 3*s1 + 4*t2) + 
               s1*(26 + 31*t2 + 2*Power(t2,2)) + 
               3*(7 + 13*t2 + 4*Power(t2,2)) + 
               s*(-24 + 3*Power(s1,2) - 23*t2 - 2*Power(t2,2) - 
                  4*s1*(7 + 2*t2))) + 
            Power(t1,4)*(Power(s1,3)*(13 + 3*t2) - 
               Power(s,3)*(13 + 4*t2) + 
               Power(s,2)*(4 + 32*s1 + 12*t2 + 5*s1*t2 + 
                  20*Power(t2,2)) + 
               Power(s1,2)*(-31 + 99*t2 + 32*Power(t2,2)) + 
               s1*(-5 - 138*t2 + 17*Power(t2,2) + 2*Power(t2,3)) + 
               2*(-95 - 18*t2 + 71*Power(t2,2) - 5*Power(t2,3) + 
                  Power(t2,4)) - 
               s*(-70 - 135*t2 + 82*Power(t2,2) + 
                  Power(s1,2)*(30 + 4*t2) + 
                  s1*(-76 + 69*t2 + 44*Power(t2,2)))) + 
            Power(t1,3)*(25 + 122*t2 + 539*Power(t2,2) + 
               116*Power(t2,3) - 19*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*(27 - 31*t2 + 15*Power(t2,2)) - 
               Power(s1,3)*(73 + 111*t2 + 26*Power(t2,2)) + 
               Power(s1,2)*(193 + 669*t2 + 76*Power(t2,2) - 
                  68*Power(t2,3)) - 
               s1*(218 + 1036*t2 + 116*Power(t2,2) + 37*Power(t2,3) + 
                  15*Power(t2,4)) - 
               Power(s,2)*(-69 - 184*t2 + 31*Power(t2,2) + 
                  3*Power(t2,3) + s1*(5 + 23*t2 + 21*Power(t2,2))) + 
               s*(-27 + 802*t2 + 179*Power(t2,2) - 30*Power(t2,3) + 
                  20*Power(t2,4) + 
                  2*Power(s1,2)*(47 + 50*t2 + 13*Power(t2,2)) + 
                  s1*(-136 - 631*t2 - 206*Power(t2,2) + 63*Power(t2,3)))) \
+ Power(t1,2)*(48 - 91*t2 - 596*Power(t2,2) + 63*Power(t2,3) - 
               170*Power(t2,4) - 63*Power(t2,5) + Power(t2,6) + 
               Power(s,3)*(38 - 31*t2 - 70*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s1,3)*(-8 + 68*t2 + 75*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(s1,2)*(-34 - 162*t2 - 485*Power(t2,2) - 
                  289*Power(t2,3) + 58*Power(t2,4)) + 
               s1*(-26 + 9*t2 + 874*Power(t2,2) + 656*Power(t2,3) - 
                  35*Power(t2,4) + 26*Power(t2,5)) + 
               Power(s,2)*(-29 + 238*t2 - 143*Power(t2,2) - 
                  191*Power(t2,3) + 13*Power(t2,4) + 
                  s1*(-20 + 3*t2 + 70*Power(t2,2) + 17*Power(t2,3))) + 
               s*(-61 - 2*t2 + 428*Power(t2,2) - 480*Power(t2,3) - 
                  109*Power(t2,4) - 8*Power(t2,5) + 
                  4*Power(s1,2)*
                   (12 - 40*t2 - 54*Power(t2,2) + Power(t2,3)) + 
                  s1*(26 + 99*t2 + 330*Power(t2,2) + 329*Power(t2,3) - 
                     14*Power(t2,4))))) + 
         s2*Power(t1,2)*(4*(25 + s - 27*s1)*Power(t1,5) + 
            2*Power(t1,4)*(4*Power(s,3) + 6*Power(s1,3) - 
               6*Power(s1,2)*(10 + t2) + 2*Power(s,2)*(11 - 6*s1 + t2) + 
               s*(40 + 2*Power(s1,2) + 4*s1*(-18 + t2) + 9*t2) - 
               6*(10 + 13*t2 + 2*Power(t2,2)) + 
               s1*(112 + 61*t2 + 13*Power(t2,2))) + 
            Power(t2,2)*(11 - 5*Power(s,3)*(-2 + t2) - 35*t2 + 
               25*Power(t2,2) - Power(t2,3) + 
               Power(s,2)*(1 + 7*t2 - 10*Power(t2,2) + 2*Power(t2,3)) - 
               s*(24 - 41*t2 + 6*Power(t2,2) + 11*Power(t2,3))) + 
            t1*(-40 + (140 - 6*s1)*t2 + 7*(-12 + s1)*Power(t2,2) - 
               (27 + 4*s1)*Power(t2,3) + (-32 + 11*s1)*Power(t2,4) + 
               (27 - 8*s1)*Power(t2,5) + 
               Power(s,3)*(-40 + 24*t2 + 5*Power(t2,2) + 
                  7*Power(t2,3) - Power(t2,4)) + 
               Power(s,2)*(-4 - 42*t2 - 2*(-48 + s1)*Power(t2,2) + 
                  (3 - 11*s1)*Power(t2,3) + 2*(1 + s1)*Power(t2,4)) + 
               s*(96 - 146*t2 + (-69 + 4*s1)*Power(t2,2) + 
                  3*(27 + 4*s1)*Power(t2,3) + (43 - 16*s1)*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(t1,3)*(60 - 180*t2 + 150*Power(t2,2) + 
               37*Power(t2,3) + 2*Power(s1,3)*(6 + t2 - 2*Power(t2,2)) - 
               Power(s,3)*(-40 + 8*t2 + Power(t2,2)) + 
               s1*(84 + 202*t2 - 133*Power(t2,2) - 31*Power(t2,3)) + 
               Power(s1,2)*(-80 - 88*t2 + 61*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s,2)*(-6*(12 - 3*t2 + 2*Power(t2,2)) + 
                  s1*(-20 + 24*t2 + 3*Power(t2,2))) + 
               s*(2*Power(s1,2)*(-66 + 17*t2 + Power(t2,2)) + 
                  s1*(264 + 64*t2 + 22*Power(t2,2) - 4*Power(t2,3)) - 
                  3*(60 + 42*t2 + 5*Power(t2,2) + Power(t2,3)))) + 
            Power(t1,2)*(-32 + 100*t2 + 111*Power(t2,2) - 
               63*Power(t2,3) - 28*Power(t2,4) - 
               2*Power(s1,3)*t2*(1 + t2) + 
               Power(s,3)*(-44 - 12*t2 - 11*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s1,2)*(12 - 4*t2 + 18*Power(t2,2) + 
                  22*Power(t2,3) - 8*Power(t2,4)) + 
               s1*(-24 + 18*t2 - 64*Power(t2,2) - 17*Power(t2,3) + 
                  19*Power(t2,4)) + 
               Power(s,2)*(s1*
                   (8 + 36*t2 + 2*Power(t2,2) - 5*Power(t2,3)) - 
                  4*(29 + 25*t2 - 4*Power(t2,2) + Power(t2,3))) - 
               s*(-256 + 90*t2 + 128*Power(t2,2) - 23*Power(t2,3) + 
                  2*Power(t2,4) + 
                  2*Power(s1,2)*t2*(-3 - 15*t2 + 5*Power(t2,2)) + 
                  s1*(40 + 36*t2 + 2*Power(t2,2) + 15*Power(t2,3) - 
                     4*Power(t2,4))))) + 
         Power(s2,2)*t1*(16*(-1 + s1)*Power(t1,6) + 
            Power(t2,2)*(-7 + 9*t2 + 5*Power(s,3)*(-2 + t2)*t2 + 
               19*Power(t2,2) - 25*Power(t2,3) + 4*Power(t2,4) + 
               5*Power(s,2)*t2*(1 - 3*t2 + 2*Power(t2,2)) + 
               s*(6 + 3*t2 - 20*Power(t2,2) + 5*Power(t2,3) + 
                  6*Power(t2,4))) + 
            t1*(20 + 4*(-4 + 3*s1)*t2 - 2*(89 + 11*s1)*Power(t2,2) + 
               (217 + 24*s1)*Power(t2,3) - (5 + 22*s1)*Power(t2,4) + 
               13*Power(t2,5) + (-27 + 8*s1)*Power(t2,6) + 
               Power(s,2)*t2*
                (-18 + 3*(18 + s1)*t2 + (-111 + 19*s1)*Power(t2,2) - 
                  29*Power(t2,3)) + 
               Power(s,3)*t2*
                (60 - 62*t2 - 13*Power(t2,2) + 3*Power(t2,3)) - 
               s*(24 + 72*t2 - (265 + 6*s1)*Power(t2,2) + 
                  (49 + 46*s1)*Power(t2,3) - 28*(-3 + s1)*Power(t2,4) + 
                  (41 - 12*s1)*Power(t2,5) + Power(t2,6))) - 
            2*Power(t1,5)*(142 + 2*Power(s,3) - 18*Power(s1,3) - 
               4*Power(s1,2)*(-19 + t2) + 100*t2 - Power(t2,2) + 
               s1*(-191 - 119*t2 + Power(t2,2)) + 
               s*(74 - 140*s1 + 26*Power(s1,2) + 13*t2 + Power(t2,2)) + 
               Power(s,2)*(-10*s1 + 4*(7 + t2))) + 
            Power(t1,4)*(164 + 802*t2 + 247*Power(t2,2) + 
               36*Power(t2,3) - Power(s,3)*(37 + 8*t2 + Power(t2,2)) - 
               Power(s1,3)*(62 + 30*t2 + 7*Power(t2,2)) + 
               Power(s1,2)*(376 + 442*t2 + 29*Power(t2,2) - 
                  4*Power(t2,3)) - 
               s1*(530 + 1108*t2 + 275*Power(t2,2) + 44*Power(t2,3)) + 
               Power(s,2)*(9 - 68*t2 + 13*Power(t2,2) + 
                  s1*(53 + 4*t2 + Power(t2,2))) + 
               s*(-148 + 66*t2 - 11*Power(t2,2) + 4*Power(t2,3) + 
                  Power(s1,2)*(114 + 26*t2 + 7*Power(t2,2)) + 
                  2*s1*(59 - 6*t2 - 38*Power(t2,2) + 2*Power(t2,3)))) + 
            Power(t1,3)*(Power(s,3)*
                (121 - 42*t2 + 18*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s1,3)*(-30 - 14*t2 + 15*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(s1,2)*(180 + 246*t2 - 5*Power(t2,2) - 
                  145*Power(t2,3) + 4*Power(t2,4)) - 
               2*(-32 - 56*t2 + 107*Power(t2,2) + 58*Power(t2,3) + 
                  13*Power(t2,4)) + 
               s1*(-154 - 494*t2 + 49*Power(t2,2) + 296*Power(t2,3) + 
                  31*Power(t2,4)) + 
               Power(s,2)*(82 + 474*t2 - 37*Power(t2,2) + 
                  10*Power(t2,3) - 
                  s1*(-1 + 109*t2 + 25*Power(t2,2) + Power(t2,3))) + 
               s*(-124 + 330*t2 + 595*Power(t2,2) - 36*Power(t2,3) + 
                  9*Power(t2,4) + 
                  Power(s1,2)*
                   (258 + 142*t2 - 99*Power(t2,2) - 4*Power(t2,3)) + 
                  s1*(-406 - 442*t2 - 217*Power(t2,2) + 48*Power(t2,3)))) \
+ Power(t1,2)*(116 - 250*t2 - 114*Power(t2,2) - 150*Power(t2,3) + 
               229*Power(t2,4) - 31*Power(t2,5) + 
               2*Power(s1,3)*t2*(2 + 3*t2 + Power(t2,2)) + 
               Power(s,3)*(130 + 67*t2 - 29*Power(t2,2) + 
                  3*Power(t2,3) - Power(t2,4)) + 
               s1*(86 - 72*t2 + 116*Power(t2,2) + 130*Power(t2,3) - 
                  53*Power(t2,4) - 3*Power(t2,5)) + 
               Power(s1,2)*(-24 + 2*t2 - 47*Power(t2,2) - 
                  50*Power(t2,3) - 9*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s,2)*(49 + 282*t2 + 145*Power(t2,2) - 
                  103*Power(t2,3) + 9*Power(t2,4) + 
                  s1*(-12 - 68*t2 - 27*Power(t2,2) + 34*Power(t2,3))) + 
               s*(-404 - 86*t2 + 235*Power(t2,2) + 195*Power(t2,3) - 
                  119*Power(t2,4) + 2*Power(t2,5) + 
                  Power(s1,2)*t2*
                   (-12 - 60*t2 - 19*Power(t2,2) + 13*Power(t2,3)) + 
                  s1*(24 + 188*t2 + 18*Power(t2,2) - 6*Power(t2,3) + 
                     35*Power(t2,4) - 4*Power(t2,5))))) + 
         Power(s2,4)*(2*t2*(1 - 5*t2 - 2*Power(t2,2) + 8*Power(t2,3) + 
               2*Power(t2,4) - 2*Power(t2,5)) + 
            2*Power(t1,6)*(7 + 2*Power(s,2) + 2*Power(s1,2) + 6*t2 + 
               s1*(10 + 3*t2) - s*(8 + 4*s1 + 3*t2)) + 
            t1*(9 + (7 - 47*s + 11*Power(s,2) - 10*Power(s,3))*t2 + 
               (-38 - 110*s - 49*Power(s,2) + 73*Power(s,3))*
                Power(t2,2) + 
               (-181 + 121*s + 163*Power(s,2) - 54*Power(s,3))*
                Power(t2,3) + 
               (55 + 94*s - 118*Power(s,2) + Power(s,3))*Power(t2,4) + 
               (129 - 109*s - 6*Power(s,2))*Power(t2,5) - 
               (57 + 8*s)*Power(t2,6) + 
               2*Power(s1,3)*Power(t2,2)*(1 + t2) + 
               Power(s1,2)*t2*
                (-6 - 11*t2 - 3*(2 + 13*s)*Power(t2,2) + 
                  (-25 + 13*s)*Power(t2,3) + 8*Power(t2,4)) + 
               s1*(6 + (44 - 12*s)*t2 - 
                  2*(30 - 50*s + 9*Power(s,2))*Power(t2,2) + 
                  4*(16 - 9*s + 4*Power(s,2))*Power(t2,3) + 
                  (49 + 11*s + 8*Power(s,2))*Power(t2,4) + 
                  (-51 + 20*s)*Power(t2,5) + 16*Power(t2,6))) + 
            Power(t1,5)*(103 + 9*Power(s,3) - 5*Power(s1,3) - 128*t2 + 
               2*Power(t2,2) - 4*Power(t2,3) + 
               3*s1*(37 + 28*t2 + 4*Power(t2,2)) - 
               Power(s,2)*(-1 + 17*s1 + 32*t2 + 4*Power(t2,2)) - 
               Power(s1,2)*(35 + 60*t2 + 4*Power(t2,2)) + 
               s*(-117 + 13*Power(s1,2) + 36*t2 - 6*Power(t2,2) + 
                  4*s1*t2*(17 + 2*t2))) - 
            Power(t1,4)*(14 + 616*t2 + 441*Power(t2,2) + 
               13*Power(t2,3) + 4*Power(t2,4) + 
               Power(s,3)*(-32 + 22*t2 + 7*Power(t2,2)) - 
               Power(s1,3)*(117 + 113*t2 + 14*Power(t2,2)) + 
               Power(s1,2)*(451 + 593*t2 - 54*Power(t2,2) - 
                  18*Power(t2,3)) + 
               s1*(-625 - 926*t2 - 199*Power(t2,2) - 51*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s,2)*(131 + 24*t2 + 43*Power(t2,2) - 
                  6*Power(t2,3) + s1*(5 - 41*t2 - 16*Power(t2,2))) + 
               s*(413 + 742*t2 - 86*Power(t2,2) + 46*Power(t2,3) - 
                  Power(t2,4) + 
                  Power(s1,2)*(117 + 118*t2 + 23*Power(t2,2)) + 
                  s1*(-445 - 860*t2 - 33*Power(t2,2) + 20*Power(t2,3)))) \
+ Power(t1,3)*(22 + 550*t2 + 848*Power(t2,2) + 294*Power(t2,3) + 
               209*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*(2 + 153*t2 - 43*Power(t2,2) + 
                  8*Power(t2,3)) - 
               Power(s1,3)*(28 + 166*t2 + 73*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(s1,2)*(195 + 694*t2 + 963*Power(t2,2) + 
                  121*Power(t2,3) - 35*Power(t2,4)) - 
               s1*(60 + 920*t2 + 2048*Power(t2,2) + 468*Power(t2,3) + 
                  57*Power(t2,4) + 5*Power(t2,5)) - 
               Power(s,2)*(98 + 55*t2 - 498*Power(t2,2) + 
                  31*Power(t2,3) - 2*Power(t2,4) + 
                  s1*(-21 + 54*t2 + 61*Power(t2,2) + 12*Power(t2,3))) + 
               s*(57 - 473*t2 + 407*Power(t2,2) + 503*Power(t2,3) - 
                  26*Power(t2,4) + 4*Power(t2,5) + 
                  Power(s1,2)*
                   (26 + 463*t2 + 136*Power(t2,2) + 13*Power(t2,3)) + 
                  s1*(-102 - 306*t2 - 731*Power(t2,2) - 
                     268*Power(t2,3) + 25*Power(t2,4)))) + 
            Power(t1,2)*(-22 + 219*t2 - 48*Power(t2,2) - 
               569*Power(t2,3) + Power(t2,4) + 108*Power(t2,5) - 
               29*Power(t2,6) + 
               Power(s1,3)*(-6 - 18*t2 + Power(t2,2) + 31*Power(t2,3)) + 
               Power(s,3)*(-34 + 201*t2 + 77*Power(t2,2) - 
                  39*Power(t2,3) + 3*Power(t2,4)) + 
               Power(s1,2)*(32 + 86*t2 + 109*Power(t2,2) - 
                  79*Power(t2,3) - 148*Power(t2,4) + 16*Power(t2,5)) + 
               s1*(92 - 118*t2 - 329*Power(t2,2) + 244*Power(t2,3) + 
                  295*Power(t2,4) - 32*Power(t2,5) + 8*Power(t2,6)) + 
               Power(s,2)*(24 - 103*t2 + 523*Power(t2,2) + 
                  216*Power(t2,3) - 83*Power(t2,4) + 4*Power(t2,5) + 
                  s1*(9 - 59*t2 - 127*Power(t2,2) + 9*Power(t2,3) + 
                     8*Power(t2,4))) + 
               s*(13 - 409*t2 - 151*Power(t2,2) + 690*Power(t2,3) + 
                  145*Power(t2,4) - 47*Power(t2,5) - Power(t2,6) + 
                  Power(s1,2)*
                   (-6 + 210*t2 + 61*Power(t2,2) - 144*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  s1*(-82 - 110*t2 - 157*Power(t2,2) - 142*Power(t2,3) + 
                     84*Power(t2,4) + 4*Power(t2,5))))) + 
         Power(s2,3)*(4*(-1 + s - s1)*Power(t1,7) + 
            2*Power(t2,2)*(1 - t2 - 2*Power(t2,2) + Power(t2,3) + 
               Power(t2,4)) - t1*
             (4 + (2 - 18*s + 6*s1)*t2 + 
               (-16*Power(s,2) + 20*Power(s,3) + 12*s*(1 + s1) - 
                  3*(16 + 5*s1))*Power(t2,2) + 
               (-30 - 44*Power(s,3) + s*(92 - 36*s1) + 20*s1 + 
                  Power(s,2)*(40 + 9*s1))*Power(t2,3) + 
               (123 + 8*Power(s,3) - 11*s1 + Power(s,2)*(-87 + 2*s1) + 
                  s*(-41 + 12*s1))*Power(t2,4) + 
               4*(3*Power(s,2) - 2*(2 + s1) + s*(-13 + 3*s1))*
                Power(t2,5) + (-19 + 5*s + 8*s1)*Power(t2,6)) + 
            2*Power(t1,6)*(14 + 12*t2 + Power(t2,2) + 
               2*Power(s,2)*(3 + t2) + 2*Power(s1,2)*(7 + t2) - 
               s1*(38 + 15*t2) + s*(7 + 3*t2 - 4*s1*(3 + t2))) + 
            Power(t1,5)*(232 + 605*t2 + 129*Power(t2,2) - Power(t2,3) + 
               Power(s1,2)*(441 + 166*t2 - 26*Power(t2,2)) + 
               Power(s,3)*(1 + 8*t2 + Power(t2,2)) - 
               Power(s1,3)*(102 + 44*t2 + Power(t2,2)) + 
               s1*(-675 - 585*t2 - 166*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s,2)*(2*(26 + 51*t2 + Power(t2,2)) - 
                  s1*(23 + 24*t2 + 3*Power(t2,2))) + 
               s*(515 + 119*t2 + 48*Power(t2,2) + Power(t2,3) + 
                  Power(s1,2)*(116 + 60*t2 + 3*Power(t2,2)) + 
                  s1*(-618 - 376*t2 + 20*Power(t2,2)))) + 
            Power(t1,4)*(-179 - 861*t2 - 914*Power(t2,2) - 
               246*Power(t2,3) - 16*Power(t2,4) + 
               Power(s,3)*(-89 + 68*t2 + Power(t2,2) - 2*Power(t2,3)) + 
               Power(s1,3)*(84 + 128*t2 + 33*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s1,2)*(-420 - 978*t2 - 519*Power(t2,2) + 
                  21*Power(t2,3) + 4*Power(t2,4)) + 
               s1*(398 + 1847*t2 + 1408*Power(t2,2) + 184*Power(t2,3) + 
                  23*Power(t2,4)) + 
               Power(s,2)*(122 - 328*t2 + 30*Power(t2,2) - 
                  16*Power(t2,3) + 
                  s1*(-30 + 27*t2 + 8*Power(t2,2) + 5*Power(t2,3))) + 
               s*(158 + 99*t2 - 550*Power(t2,2) + 35*Power(t2,3) - 
                  8*Power(t2,4) - 
                  2*Power(s1,2)*
                   (96 + 145*t2 + 14*Power(t2,2) + 5*Power(t2,3)) + 
                  s1*(102 + 243*t2 + 468*Power(t2,2) + 37*Power(t2,3) - 
                     4*Power(t2,4)))) + 
            Power(t1,3)*(-106 - 101*t2 + 123*Power(t2,2) + 
               579*Power(t2,3) - 167*Power(t2,4) + 22*Power(t2,5) + 
               Power(s1,3)*(24 + 26*t2 - 15*Power(t2,2) - 
                  23*Power(t2,3)) + 
               Power(s,3)*(-117 - 221*t2 + 50*Power(t2,2) - 
                  15*Power(t2,3) + Power(t2,4)) - 
               Power(s1,2)*(126 + 236*t2 + 169*Power(t2,2) - 
                  249*Power(t2,3) - 68*Power(t2,4) + 8*Power(t2,5)) - 
               s1*(19 - 484*t2 - 321*Power(t2,2) + 568*Power(t2,3) + 
                  126*Power(t2,4) + 8*Power(t2,5)) - 
               Power(s,2)*(18 + 353*t2 + 685*Power(t2,2) - 
                  122*Power(t2,3) + 8*Power(t2,4) + 
                  s1*(-6 - 153*t2 - 111*Power(t2,2) + 11*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s*(261 + 219*t2 - 582*Power(t2,2) - 695*Power(t2,3) + 
                  86*Power(t2,4) - 7*Power(t2,5) - 
                  Power(s1,2)*
                   (120 + 386*t2 - 114*Power(t2,2) - 78*Power(t2,3) + 
                     Power(t2,4)) + 
                  s1*(232 + 424*t2 + 408*Power(t2,2) + 55*Power(t2,3) - 
                     68*Power(t2,4) + 4*Power(t2,5)))) + 
            Power(t1,2)*(-23 - 89*t2 + 451*Power(t2,2) + 4*Power(t2,3) - 
               98*Power(t2,4) - 108*Power(t2,5) + 55*Power(t2,6) - 
               2*Power(s1,3)*t2*(1 + 3*t2 + 2*Power(t2,2)) + 
               Power(s,3)*(10 - 203*t2 + 38*Power(t2,2) + 
                  49*Power(t2,3) - 6*Power(t2,4)) + 
               Power(s1,2)*(12 + 8*t2 + 40*Power(t2,2) + 34*Power(t2,3) + 
                  42*Power(t2,4) - 16*Power(t2,5)) + 
               Power(s,2)*(-11 + 3*(5 + 12*s1)*t2 + 
                  (-359 + 44*s1)*Power(t2,2) + (64 - 45*s1)*Power(t2,3) + 
                  (109 - 8*s1)*Power(t2,4) - 6*Power(t2,5)) + 
               s1*(-68 + 10*t2 + 8*Power(t2,2) - 177*Power(t2,3) - 
                  15*Power(t2,4) + 54*Power(t2,5) - 16*Power(t2,6)) + 
               s*(56 + 481*t2 - 243*Power(t2,2) - 283*Power(t2,3) + 
                  57*Power(t2,4) + 107*Power(t2,5) + 2*Power(t2,6) + 
                  2*Power(s1,2)*t2*
                   (3 + 15*t2 + 34*Power(t2,2) - 13*Power(t2,3)) - 
                  s1*(-24 + 148*t2 + 118*Power(t2,2) - 57*Power(t2,3) + 
                     50*Power(t2,4) + 16*Power(t2,5)))))))/
     ((-1 + s1)*Power(-1 + s2,2)*s2*(-1 + t1)*t1*Power(-s2 + t1,2)*
       (-s + s1 - t2)*(1 - s2 + t1 - t2)*(t1 - s2*t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) - 
    (8*(Power(t1,6)*(-9 + s - s1 - 4*t2) - 
         Power(1 + (-1 + s)*t2,3)*(2 - 3*t2 + Power(t2,2)) + 
         Power(s2,8)*(Power(s1,3) + s1*Power(t2,2) + 2*Power(t2,3)) - 
         Power(t1,5)*(7 + Power(s,3) + Power(s1,2)*(-7 + t2) - 
            s1*(-3 + t2) - 29*t2 - 12*Power(t2,2) + 
            Power(s,2)*(-13 - 2*s1 + t2) + 
            s*(Power(s1,2) - 2*s1*(-10 + t2) + 2*(5 + t2))) - 
         Power(s2,7)*(Power(s1,3)*(4*t1 - 3*t2) + 
            Power(s1,2)*(3 + (-2 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            s1*t2*(-5*(-1 + t2)*t2 + s*(3 - 3*t1 + t2) + 
               t1*(2 + 3*t2)) + 
            Power(t2,2)*(1 + 13*t2 - 6*Power(t2,2) + t1*(6 + 4*t2) + 
               s*(3 - 4*t1 + 4*t2))) + 
         Power(t1,4)*(9 - Power(s1,3)*(-2 + t2) + 28*t2 - 
            37*Power(t2,2) - 12*Power(t2,3) + 2*Power(s,3)*(2 + t2) + 
            s1*(15 - 11*t2 + 2*Power(t2,2)) + 
            Power(s1,2)*(-1 - 8*t2 + 2*Power(t2,2)) + 
            Power(s,2)*(s1*(2 - 5*t2) + t2*(-19 + 2*t2)) + 
            s*(-16 + 4*Power(s1,2)*(-2 + t2) + 7*Power(t2,2) + 
               s1*(-37 + 33*t2 - 4*Power(t2,2)))) + 
         t1*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(1 + t2) + 
            Power(-1 + t2,2)*(2 - 8*t2 + Power(t2,2) + s1*(-5 + 4*t2)) - 
            s*(-1 + t2)*(-5 - 5*Power(t2,2) + Power(t2,3) + 
               s1*(6 - 8*t2 + 4*Power(t2,2)))) - 
         Power(t1,3)*(Power(s,3)*(1 + t2 + 3*Power(t2,2)) + 
            Power(s,2)*(-9 + 19*t2 - 11*Power(t2,2) + 2*Power(t2,3) + 
               s1*(10 + 12*t2 - 8*Power(t2,2))) + 
            (-1 + t2)*(5 - 36*t2 + 2*Power(s1,3)*t2 - 20*Power(t2,2) - 
               4*Power(t2,3) + s1*(16 + 37*t2 - 2*Power(t2,2)) + 
               Power(s1,2)*(-3 - 23*t2 + 3*Power(t2,2))) + 
            s*(9 - 93*t2 - 2*Power(t2,2) + 10*Power(t2,3) + 
               Power(s1,2)*(13 - 21*t2 + 2*Power(t2,2)) + 
               s1*(4 + 28*t2 + 21*Power(t2,2) - 6*Power(t2,3)))) + 
         Power(t1,2)*(Power(s,3)*t2*(2 - 7*t2 + 3*Power(t2,2)) + 
            Power(-1 + t2,2)*
             (2 + 13*t2 - 6*Power(t2,2) + Power(s1,2)*(-3 + 5*t2) + 
               s1*(2 - 22*t2 + 4*Power(t2,2))) + 
            Power(s,2)*(-2 + 15*t2 + 21*Power(t2,2) - 14*Power(t2,3) + 
               2*Power(t2,4) - 
               s1*(-4 + 22*t2 - 14*Power(t2,2) + Power(t2,3))) - 
            s*(-1 + t2)*(15 - 10*t2 - 6*Power(t2,2) - 2*Power(t2,3) + 
               Power(s1,2)*(4 - 8*t2 + 5*Power(t2,2)) + 
               s1*(-13 + 27*t2 - 20*Power(t2,2) + 4*Power(t2,3)))) + 
         Power(s2,6)*(Power(s1,3)*
             (-4 + 6*Power(t1,2) + 2*t2 + 3*Power(t2,2) - 
               t1*(2 + 11*t2)) + 
            Power(s1,2)*(Power(t1,2)*(1 - 9*s + 3*t2) + 
               t1*(10 - 4*s - 12*t2 + 9*s*t2 - 8*Power(t2,2)) + 
               t2*(-11 + 6*Power(t2,2) + s*(-4 + 3*t2))) + 
            s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 4*t2 - 4*Power(t2,2) - 
               11*Power(t2,3) + 9*Power(t2,4) + 
               t1*t2*(12 + t2 - 12*Power(t2,2)) + 
               Power(t1,2)*(1 + 6*t2 + 3*Power(t2,2)) + 
               s*(t2 - 5*Power(t2,2) - 5*Power(t2,3) - 
                  Power(t1,2)*(3 + 8*t2) + 
                  t1*(-3 + 9*t2 + 17*Power(t2,2)))) + 
            t2*(2*Power(t1,2)*(3 + 6*t2 + Power(t2,2)) + 
               Power(s,2)*(-3*t1 + 3*Power(t1,2) + t2 - 5*t1*t2 + 
                  2*Power(t2,2)) + 
               t1*(2 + 42*t2 + 7*Power(t2,2) - 7*Power(t2,3)) + 
               t2*(5 - 13*t2 - 41*Power(t2,2) + 6*Power(t2,3)) + 
               s*(3 + 3*t2 - 5*Power(t2,2) - 12*Power(t2,3) - 
                  Power(t1,2)*(8 + 7*t2) + t1*(3 + 9*t2 + 20*Power(t2,2))\
))) + Power(s2,5)*(-1 + 2*t2 - 2*s*t2 + 2*Power(t2,2) + 
            22*s*Power(t2,2) + 7*Power(s,2)*Power(t2,2) + 
            19*Power(t2,3) + 36*s*Power(t2,3) + 
            4*Power(s,2)*Power(t2,3) - 22*Power(t2,4) + 
            7*s*Power(t2,4) + 6*Power(s,2)*Power(t2,4) - 
            45*Power(t2,5) - 12*s*Power(t2,5) + 2*Power(t2,6) + 
            Power(t1,2)*(-1 + 
               (-45 + 2*s + 9*Power(s,2) - 2*Power(s,3))*t2 + 
               (-60 - 46*s + 17*Power(s,2))*Power(t2,2) + 
               (4 - 16*s)*Power(t2,3) + Power(t2,4)) + 
            Power(s1,3)*(-2 - 4*Power(t1,3) + 3*Power(t2,2) + 
               Power(t2,3) + Power(t1,2)*(7 + 15*t2) + 
               t1*(16 - 13*t2 - 10*Power(t2,2))) + 
            Power(t1,3)*(Power(s,3) - Power(s,2)*(3 + 4*t2) - 
               2*(1 + 6*t2 + 3*Power(t2,2)) + 
               s*(4 + 14*t2 + 3*Power(t2,2))) + 
            t1*t2*(-11 + 23*t2 + Power(s,3)*t2 + 168*Power(t2,2) + 
               37*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,2)*(5 - 16*t2 - 19*Power(t2,2)) + 
               s*(-13 - 17*t2 + 16*Power(t2,2) + 25*Power(t2,3))) + 
            Power(s1,2)*(12 - (17 + 6*s)*t2 - 4*(5 + 2*s)*Power(t2,2) + 
               (-8 + 3*s)*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,2)*(-8 + s*(3 - 27*t2) + 28*t2 + 
                  10*Power(t2,2)) + Power(t1,3)*(9*s - 3*(1 + t2)) + 
               t1*(8 + 35*t2 - 12*Power(t2,2) - 16*Power(t2,3) + 
                  2*s*(-4 + 2*t2 + 5*Power(t2,2)))) - 
            s1*(Power(t1,3)*(3 + 6*Power(s,2) + 6*t2 + Power(t2,2) - 
                  s*(8 + 7*t2)) + 
               Power(t1,2)*(7 + Power(s,2)*(5 - 14*t2) + 23*t2 - 
                  17*Power(t2,2) - 8*Power(t2,3) + 
                  2*s*(-5 + 19*t2 + 15*Power(t2,2))) + 
               t2*(-13 - 4*t2 + 2*Power(s,2)*t2 + 14*Power(t2,2) + 
                  8*Power(t2,3) - 7*Power(t2,4) + 
                  s*(-20 + 2*t2 + 5*Power(t2,2) + 9*Power(t2,3))) + 
               t1*(8 - 24*t2 - 45*Power(t2,2) + 6*Power(t2,3) + 
                  13*Power(t2,4) + Power(s,2)*t2*(7 + 4*t2) - 
                  s*(5 - 37*t2 + 11*Power(t2,2) + 37*Power(t2,3))))) + 
         Power(s2,4)*(Power(t1,4)*
             (4 - Power(s,3) + 6*t2 + Power(s,2)*(4 + t2) - 
               s*(7 + 6*t2)) + 
            Power(t1,3)*(16 + 75*t2 + 31*Power(t2,2) + 2*Power(t2,3) + 
               Power(s,3)*(-1 + 5*t2) + 
               Power(s,2)*(2 - 27*t2 - 10*Power(t2,2)) + 
               s*(-7 + 25*t2 + 38*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(3 + Power(t1,4) - 3*t2 + Power(t2,3) - 
               9*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(-22 + 29*t2 + 11*Power(t2,2)) - 
               t1*(-8 + 4*t2 + 9*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,2)*(6 - 4*t2 - 239*Power(t2,2) - 185*Power(t2,3) - 
               17*Power(t2,4) - Power(s,3)*t2*(2 + 7*t2) + 
               Power(s,2)*t2*(-12 + 41*t2 + 23*Power(t2,2)) + 
               s*(1 + 53*t2 + 53*Power(t2,2) - 57*Power(t2,3) - 
                  6*Power(t2,4))) - 
            t2*(5 + 4*t2 - 28*Power(t2,2) + 2*Power(s,3)*Power(t2,2) - 
               10*Power(t2,3) + 20*Power(t2,4) + 21*Power(t2,5) + 
               2*Power(s,2)*t2*
                (3 - 3*t2 + Power(t2,2) - 3*Power(t2,3)) + 
               s*(16 + t2 - 24*Power(t2,2) - 49*Power(t2,3) - 
                  9*Power(t2,4) + 4*Power(t2,5))) + 
            t1*(2 - 12*t2 - 61*Power(t2,2) + 37*Power(t2,3) + 
               206*Power(t2,4) + 34*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(5 + 3*t2) + 
               2*Power(s,2)*t2*
                (1 + 10*t2 - 8*Power(t2,2) - 10*Power(t2,3)) + 
               s*(-1 - 6*t2 - 113*Power(t2,2) - 86*Power(t2,3) + 
                  12*Power(t2,4) + 9*Power(t2,5))) + 
            Power(s1,2)*(6 + (-3 + 4*s)*t2 + 2*(-1 + s)*Power(t2,2) + 
               (2 - 8*s)*Power(t2,3) + (-12 + s)*Power(t2,4) + 
               2*Power(t2,5) + Power(t1,4)*(3 - 3*s + t2) + 
               Power(t1,2)*(-23 - 32*t2 + 33*Power(t2,2) + 
                  12*Power(t2,3) + s*(32 - 25*t2 - 27*Power(t2,2))) + 
               Power(t1,3)*(s*(9 + 23*t2) - 
                  4*(2 + 7*t2 + Power(t2,2))) + 
               t1*(-37 + 96*t2 + 72*Power(t2,2) - 6*Power(t2,3) - 
                  11*Power(t2,4) + 
                  s*(12 + 18*t2 + 7*Power(t2,2) + 6*Power(t2,3)))) + 
            s1*(-12 + (28 + 6*s)*t2 + (17 + 9*s)*Power(t2,2) - 
               (19 + 7*s)*Power(t2,3) + 3*(-4 + s)*Power(t2,4) + 
               (2 - 7*s)*Power(t2,5) + 2*Power(t2,6) + 
               Power(t1,4)*(3 + 3*Power(s,2) + 2*t2 - s*(7 + 2*t2)) + 
               Power(t1,3)*(17 + Power(s,2)*(1 - 19*t2) + 8*t2 - 
                  14*Power(t2,2) - Power(t2,3) + 
                  s*(3 + 59*t2 + 14*Power(t2,2))) + 
               Power(t1,2)*(-2 - 81*t2 - 46*Power(t2,2) + 
                  27*Power(t2,3) + 3*Power(t2,4) + 
                  Power(s,2)*(-2 + 6*t2 + 23*Power(t2,2)) + 
                  s*(18 + 95*t2 - 80*Power(t2,2) - 36*Power(t2,3))) - 
               t1*(10 + 45*t2 - 10*Power(t2,2) - 54*Power(t2,3) + 
                  15*Power(t2,4) + 4*Power(t2,5) + 
                  Power(s,2)*t2*(-12 + 17*t2 + 7*Power(t2,2)) + 
                  s*(-4 + 55*t2 + 47*Power(t2,2) - 8*Power(t2,3) - 
                     31*Power(t2,4))))) + 
         Power(s2,2)*(2 - 3*t2 + 19*s*t2 + 6*Power(t2,2) - 
            18*s*Power(t2,2) + 11*Power(s,2)*Power(t2,2) - 
            6*Power(t2,3) - 21*s*Power(t2,3) + 
            10*Power(s,2)*Power(t2,3) + 4*Power(s,3)*Power(t2,3) - 
            9*Power(t2,4) + 19*s*Power(t2,4) + Power(s,2)*Power(t2,4) + 
            2*Power(s,3)*Power(t2,4) + 15*Power(t2,5) - s*Power(t2,5) - 
            Power(s,2)*Power(t2,5) - 5*Power(t2,6) + 2*s*Power(t2,6) + 
            Power(s,2)*Power(t2,6) + 
            Power(t1,5)*(18 + 4*s - 7*Power(s,2) + Power(s,3) + 14*t2 - 
               s*t2) - Power(t1,4)*
             (23 + 148*t2 + 106*Power(t2,2) + 12*Power(t2,3) + 
               Power(s,3)*(-2 + 7*t2) - 
               3*Power(s,2)*(-7 + 9*t2 + Power(t2,2)) + 
               s*(-45 - 16*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*t1*(-5 - Power(t1,4) + 13*t2 - 5*Power(t2,2) - 
               3*Power(t2,3) + Power(t1,3)*(4 + 9*t2) + 
               Power(t1,2)*(7 - 9*t2 - 6*Power(t2,2)) + 
               t1*(15 - 14*t2 + 4*Power(t2,2) + Power(t2,3))) + 
            Power(t1,3)*(-31 - 73*t2 + 235*Power(t2,2) + 
               209*Power(t2,3) + 36*Power(t2,4) + 
               Power(s,3)*(2 + 11*Power(t2,2)) + 
               Power(s,2)*(4 + 58*t2 - 12*Power(t2,3)) + 
               s*(16 - 68*t2 - 146*Power(t2,2) + 15*Power(t2,3))) - 
            Power(t1,2)*(13 - 24*t2 - 115*Power(t2,2) - 
               17*Power(t2,3) + 179*Power(t2,4) + 36*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(4 + 5*t2) + 
               Power(s,2)*(-2 - 45*t2 + 112*Power(t2,2) + 
                  38*Power(t2,3) - 16*Power(t2,4)) + 
               s*(11 + 93*t2 - 175*Power(t2,2) - 92*Power(t2,3) + 
                  7*Power(t2,4))) + 
            t1*(-5 + 42*t2 - 54*Power(t2,2) - 
               6*Power(s,3)*Power(t2,2) - 98*Power(t2,3) + 
               62*Power(t2,4) + 41*Power(t2,5) + 12*Power(t2,6) + 
               Power(s,2)*t2*
                (5 + 73*t2 + 26*Power(t2,2) + 19*Power(t2,3) - 
                  8*Power(t2,4)) + 
               s*(6 + 5*t2 + 108*Power(t2,2) + 156*Power(t2,3) - 
                  41*Power(t2,4) - 6*Power(t2,5))) + 
            Power(s1,2)*(3*(-2 + s)*Power(t1,5) - 
               (-1 + t2)*(-6 + 11*t2 - 2*Power(t2,2) + 
                  (-3 + s)*Power(t2,3)) + 
               Power(t1,4)*(-1 + 13*t2 + 3*Power(t2,2) - 
                  s*(2 + 25*t2)) + 
               Power(t1,3)*(37 + 129*t2 - 12*Power(t2,2) - 
                  10*Power(t2,3) + 2*s*(11 - 5*t2 + 13*Power(t2,2))) - 
               Power(t1,2)*(-22 + 9*t2 + 38*Power(t2,2) + 
                  24*Power(t2,3) - 7*Power(t2,4) + 
                  s*(22 - 38*t2 + 23*Power(t2,2) + 8*Power(t2,3))) + 
               t1*(24 - 62*t2 + 2*Power(t2,2) + 15*Power(t2,3) + 
                  23*Power(t2,4) - 2*Power(t2,5) + 
                  s*(-8 - 13*t2 + 21*Power(t2,2) + 18*Power(t2,3)))) - 
            s1*(-9 + Power(t1,5)*(-4 - 13*s + 3*Power(s,2) - t2) + 
               (27 + 7*s)*t2 - (11 + 10*s)*Power(t2,2) + 
               3*(-7 + 3*Power(s,2))*Power(t2,3) + 
               (10 + 6*s - 5*Power(s,2))*Power(t2,4) + 
               (6 - 5*s + Power(s,2))*Power(t2,5) + 
               2*(-1 + s)*Power(t2,6) + 
               Power(t1,4)*(44 + Power(s,2)*(4 - 23*t2) + 27*t2 - 
                  2*Power(t2,2) + s*(-35 + 45*t2 + 6*Power(t2,2))) + 
               Power(t1,3)*(31 + 15*t2 - 33*Power(t2,2) + 
                  7*Power(t2,3) + 
                  Power(s,2)*(30 - 15*t2 + 31*Power(t2,2)) + 
                  s*(25 + 223*t2 - 27*Power(t2,2) - 22*Power(t2,3))) - 
               Power(t1,2)*(Power(s,2)*
                   (-5 - 59*t2 + 29*Power(t2,2) + 12*Power(t2,3)) + 
                  s*(12 - 119*t2 + 47*Power(t2,2) + 72*Power(t2,3) - 
                     24*Power(t2,4)) + 
                  2*(6 + 67*t2 - 43*Power(t2,2) - 25*Power(t2,3) + 
                     13*Power(t2,4))) + 
               t1*(-18 + 16*t2 - 70*Power(t2,2) + 19*Power(t2,3) + 
                  45*Power(t2,4) + 8*Power(t2,5) + 
                  Power(s,2)*t2*(17 + 38*t2 - 3*Power(t2,2)) + 
                  s*(7 - 91*t2 + 124*Power(t2,2) + 95*Power(t2,3) + 
                     16*Power(t2,4) - 10*Power(t2,5))))) - 
         s2*(3 - (-5 + s)*Power(t1,6) - 12*t2 - 2*s*t2 + 
            12*Power(t2,2) - 7*Power(s,2)*Power(t2,2) + 7*Power(t2,3) - 
            4*s*Power(t2,3) + 11*Power(s,2)*Power(t2,3) - 
            2*Power(s,3)*Power(t2,3) - 18*Power(t2,4) + 
            18*s*Power(t2,4) - 9*Power(s,2)*Power(t2,4) + 
            5*Power(s,3)*Power(t2,4) + 9*Power(t2,5) - 
            14*s*Power(t2,5) + 6*Power(s,2)*Power(t2,5) - 
            Power(s,3)*Power(t2,5) - Power(t2,6) + 2*s*Power(t2,6) - 
            Power(s,2)*Power(t2,6) + 
            Power(t1,5)*(-27 - 2*Power(s,3) - 55*t2 - 12*Power(t2,2) + 
               Power(s,2)*(3 + t2) + s*(11 + 3*t2)) + 
            Power(t1,4)*(-36 + 50*t2 + 129*Power(t2,2) + 
               36*Power(t2,3) + Power(s,3)*(6 + t2) + 
               Power(s,2)*(6 + 34*t2 - 7*Power(t2,2)) - 
               s*(-11 + 78*t2 + Power(t2,2))) + 
            Power(s1,3)*Power(t1,2)*
             (-3 + 3*Power(t1,3) + Power(t1,2)*(2 - 4*t2) + 11*t2 - 
               7*Power(t2,2) - Power(t2,3) + 
               t1*(9 - 7*t2 + 2*Power(t2,2))) + 
            Power(t1,3)*(Power(s,3)*(1 + t2 + 2*Power(t2,2)) + 
               t2*(58 + 65*t2 - 135*Power(t2,2) - 36*Power(t2,3)) + 
               s*(-40 + 19*t2 + 46*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s,2)*(17 - 37*t2 - 63*Power(t2,2) + 12*Power(t2,3))\
) + Power(t1,2)*(10 - t2 - 125*Power(t2,2) - 
               15*Power(s,3)*Power(t2,2) + 60*Power(t2,3) + 
               44*Power(t2,4) + 12*Power(t2,5) + 
               2*Power(s,2)*t2*
                (16 + 3*t2 + 13*Power(t2,2) - 4*Power(t2,3)) + 
               s*(3 + 36*t2 + 239*Power(t2,2) - 34*Power(t2,3) - 
                  16*Power(t2,4))) + 
            t1*(Power(s,3)*Power(t2,2)*(1 + 3*t2) - 
               Power(-1 + t2,2)*
                (-5 - 6*t2 - 18*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s,2)*t2*
                (-9 + 70*t2 - 14*Power(t2,2) - 6*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s*(4 + 48*t2 - 109*Power(t2,2) + 56*Power(t2,3) - 
                  3*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s1,2)*t1*(Power(t1,4)*(4 + t2) + 
               Power(t1,3)*(40 + 18*t2 - 7*Power(t2,2)) + 
               2*Power(-1 + t2,2)*(-5 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(9 - 16*t2 - 28*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(17 - 76*t2 + 27*Power(t2,2) + 37*Power(t2,3) - 
                  5*Power(t2,4)) + 
               s*(6 - 8*Power(t1,4) - 17*t2 + 16*Power(t2,2) - 
                  3*Power(t2,3) - 2*Power(t2,4) + 
                  Power(t1,3)*(-2 + 9*t2) + 
                  Power(t1,2)*(-17 + 12*t2 - 8*Power(t2,2)) + 
                  t1*(-24 + 18*t2 + 23*Power(t2,2) + Power(t2,3)))) + 
            s1*(Power(t1,6) - 
               (-1 + t2)*(-6 + (16 - 6*s)*t2 + 
                  (-13 + 11*s)*Power(t2,2) + 
                  (2 - 3*s + Power(s,2))*Power(t2,3) + 
                  Power(-1 + s,2)*Power(t2,4)) + 
               Power(t1,5)*(7*Power(s,2) - 2*(8 + t2) - s*(7 + 2*t2)) + 
               Power(t1,4)*(-15 + 4*t2 + Power(t2,2) - 
                  6*Power(s,2)*(1 + t2) + 
                  s*(-74 - 41*t2 + 14*Power(t2,2))) + 
               Power(t1,3)*(Power(s,2)*(-23 + 4*t2 + 3*Power(t2,2)) + 
                  2*(22 + 9*t2 - 27*Power(t2,2) + 8*Power(t2,3)) - 
                  s*(47 + 61*t2 - 108*Power(t2,2) + 20*Power(t2,3))) - 
               Power(t1,2)*(-13 - 51*t2 - 10*Power(t2,2) + 
                  72*Power(t2,3) + 2*Power(t2,4) + 
                  Power(s,2)*
                   (-3 + 77*t2 - 28*Power(t2,2) + Power(t2,3)) + 
                  s*(1 - 33*t2 + 166*Power(t2,2) + 21*Power(t2,3) - 
                     14*Power(t2,4))) + 
               t1*(Power(s,2)*t2*
                   (7 - 37*t2 + 22*Power(t2,2) - 2*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (15 - 35*t2 - 18*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(-13 + 21*t2 + 4*Power(t2,2) - 29*Power(t2,3) + 
                     23*Power(t2,4) - 6*Power(t2,5))))) + 
         Power(s2,3)*(4 - (2 - 3*s + Power(s,2))*Power(t1,5) - 13*t2 - 
            26*s*Power(t2,2) - 14*Power(s,2)*Power(t2,2) + 
            20*Power(t2,3) - 27*s*Power(t2,3) - 
            32*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) + 
            26*Power(t2,4) - 34*s*Power(t2,4) - 
            10*Power(s,2)*Power(t2,4) - 2*Power(s,3)*Power(t2,4) - 
            20*Power(t2,5) + 11*s*Power(t2,5) - 
            4*Power(s,2)*Power(t2,5) - 13*Power(t2,6) + 
            2*Power(s,2)*Power(t2,6) - 4*Power(t2,7) - 
            Power(t1,4)*(28 + 46*t2 + 12*Power(t2,2) + 
               Power(s,3)*(1 + 2*t2) - Power(s,2)*(7 + 17*t2) + 
               s*(-1 + 26*t2 + Power(t2,2))) + 
            Power(s1,3)*(Power(t1,4)*(5 + 2*t2) + 
               Power(t1,3)*(9 - 27*t2 - 4*Power(t2,2)) + 
               2*(1 - 2*t2 + Power(t2,3)) - 
               t1*(11 - 11*t2 + 2*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-11 + 9*t2 + 12*Power(t2,2) + 2*Power(t2,3))) \
+ Power(t1,3)*(-6 + 135*t2 + 263*Power(t2,2) + 80*Power(t2,3) + 
               4*Power(t2,4) + Power(s,3)*t2*(5 + 6*t2) + 
               Power(s,2)*(11 + 11*t2 - 50*Power(t2,2) - 
                  2*Power(t2,3)) + 
               s*(-22 - 105*t2 + 27*Power(t2,2) + 11*Power(t2,3))) + 
            Power(t1,2)*(4 + 77*t2 + 21*Power(t2,2) - 339*Power(t2,3) - 
               145*Power(t2,4) - 12*Power(t2,5) + 
               Power(s,3)*t2*(2 - 9*t2 - 6*Power(t2,2)) + 
               Power(s,2)*t2*
                (-46 - 33*t2 + 41*Power(t2,2) + 6*Power(t2,3)) + 
               s*(-4 + 43*t2 + 170*Power(t2,2) + 75*Power(t2,3) - 
                  23*Power(t2,4))) + 
            t1*(4 + 21*t2 - 58*Power(t2,2) - 72*Power(t2,3) + 
               39*Power(t2,4) + 102*Power(t2,5) + 12*Power(t2,6) + 
               Power(s,3)*Power(t2,3)*(7 + 2*t2) + 
               Power(s,2)*t2*
                (-14 + 11*t2 + 41*Power(t2,2) - 3*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s*(4 + 51*t2 - 16*Power(t2,2) - 168*Power(t2,3) - 
                  58*Power(t2,4) + 13*Power(t2,5))) - 
            Power(s1,2)*(9 + Power(t1,5) - (18 + 5*s)*t2 - 
               (1 + s)*Power(t2,2) + (3 + 8*s)*Power(t2,3) + 
               (1 + 4*s)*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,4)*(-5*(3 + 2*t2) + s*(11 + 6*t2)) + 
               2*Power(t1,3)*
                (-10 + 2*t2 + 12*Power(t2,2) + Power(t2,3) + 
                  s*(15 - 25*t2 - 7*Power(t2,2))) + 
               Power(t1,2)*(-20 + 185*t2 + 53*Power(t2,2) - 
                  28*Power(t2,3) - 4*Power(t2,4) + 
                  2*s*(17 - 2*t2 + 11*Power(t2,2) + 5*Power(t2,3))) - 
               t1*(-21 + 16*t2 - 8*Power(t2,2) + 41*Power(t2,3) + 
                  2*Power(t2,4) - 2*Power(t2,5) + 
                  s*(11 - 31*t2 + 16*Power(t2,2) + 9*Power(t2,3) + 
                     2*Power(t2,4)))) + 
            s1*(-6 + (-1 + 2*s)*Power(t1,5) + (6 - 23*s)*t2 + 
               (-7 + 10*s + 2*Power(s,2))*Power(t2,2) + 
               (-11 + 35*s + 16*Power(s,2))*Power(t2,3) + 
               (2 + 23*s + Power(s,2))*Power(t2,4) + 
               4*(3 + s)*Power(t2,5) - 2*(-2 + s)*Power(t2,6) + 
               Power(t1,4)*(-13 + 2*t2 + Power(t2,2) + 
                  Power(s,2)*(7 + 6*t2) - s*(23 + 27*t2)) + 
               Power(t1,3)*(35 + 90*t2 - 3*Power(t2,2) - 6*Power(t2,3) - 
                  4*Power(s,2)*(-4 + 7*t2 + 4*Power(t2,2)) + 
                  s*(-65 - 16*t2 + 80*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,2)*(29 + 53*t2 - 58*Power(t2,2) - 
                  15*Power(t2,3) + 11*Power(t2,4) + 
                  2*Power(s,2)*
                   (4 + t2 + 11*Power(t2,2) + 7*Power(t2,3)) + 
                  s*(-21 + 167*t2 + 108*Power(t2,2) - 73*Power(t2,3) - 
                     10*Power(t2,4))) + 
               t1*(26 - 125*t2 + 13*Power(t2,2) + 65*Power(t2,3) + 
                  11*Power(t2,4) - 14*Power(t2,5) - 
                  2*Power(s,2)*t2*
                   (-7 - 9*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
                  s*(-18 + 27*t2 + 7*Power(t2,2) - 26*Power(t2,3) - 
                     2*Power(t2,4) + 8*Power(t2,5))))))*
       B1(1 - s2 + t1 - t2,s2,t1))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*(1 - s2 + t1 - t2)*
       Power(t1 - s2*t2,3)) + (8*
       (2*Power(s2,11)*(Power(s1,3) + 4*s1*Power(t2,2) + 
            Power(t2,2)*(-6 + s + 3*t2)) + 
         (-2 + s)*Power(t1,4)*(1 + t1 - t2)*
          (Power(s,2)*(2 + t1 - t2) + 2*(-t1 + Power(t1,2) - t2) + 
            s*(-3 + 3*t1 - s1*t1 + 3*t2)) + 
         Power(s2,10)*(Power(s1,3)*(-6 - 10*t1 + 3*t2) + 
            Power(s1,2)*(-6 - 2*(-2 + t1)*t2 + 11*Power(t2,2) + 
               2*s*(3*t1 + t2)) - 
            s1*t2*(3*(16 - 7*t2)*t2 + 16*t1*(1 + 2*t2) + 
               s*(6 - 6*t1 + 6*t2)) + 
            t2*(2*t1*(12 + s*(-2 + t2) + 17*t2 - 9*Power(t2,2)) + 
               t2*(32 - 2*Power(s,2) - 2*s*(-2 + t2) - 56*t2 + 
                  13*Power(t2,2)))) + 
         Power(s2,9)*(Power(s1,3)*
             (2 + 20*Power(t1,2) + t1*(29 - 16*t2) - 2*t2 + Power(t2,2)) \
+ 2*Power(t1,2)*(-6 + s - 43*t2 - 2*s*t2 + 3*Power(s,2)*t2 - 
               17*Power(t2,2) - 8*s*Power(t2,2) + 9*Power(t2,3)) + 
            t1*t2*(-64 + 2*Power(s,2)*(-1 + t2) + 24*t2 + 
               132*Power(t2,2) - 23*Power(t2,3) + 
               s*(-14 - 60*t2 + 35*Power(t2,2))) + 
            t2*(-2*Power(s,2)*t2*(5 + 2*t2) + 
               t2*(-4 + 125*t2 - 85*Power(t2,2) + 7*Power(t2,3)) - 
               s*(-6 + 30*t2 + 4*Power(t2,2) + 11*Power(t2,3))) + 
            Power(s1,2)*(18 - (25 + 14*s)*t2 + 
               (-41 + 3*s)*Power(t2,2) + 19*Power(t2,3) + 
               Power(t1,2)*(2 - 24*s + 8*t2) + 
               t1*(26 - 36*t2 - 51*Power(t2,2) + s*(-26 + 7*t2))) + 
            s1*(6 + 2*Power(s,2)*t1*(3*t1 - t2) - 8*t2 + 
               78*Power(t2,2) - 80*Power(t2,3) + 21*Power(t2,4) + 
               t1*t2*(100 + 129*t2 - 66*Power(t2,2)) + 
               8*Power(t1,2)*(1 + 8*t2 + 6*Power(t2,2)) - 
               2*s*(Power(t1,2)*(3 + 11*t2) + 
                  t1*(3 - 7*t2 - 24*Power(t2,2)) + 
                  5*t2*(-2 - 3*t2 + Power(t2,2))))) - 
         s2*Power(t1,3)*(Power(s,3)*
             (8 - Power(t1,3) - 6*t2 - 5*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,2)*(14 + t2) + t1*(24 - 13*t2 - 3*Power(t2,2))) \
+ Power(s,2)*(-22 + Power(t1,3)*(27 + 5*s1 - 2*t2) + 19*t2 + 
               16*Power(t2,2) - 13*Power(t2,3) + 
               Power(t1,2)*(1 + 5*s1 - 10*t2 - 12*s1*t2) + 
               t1*(-56 + s1 + 48*t2 - 8*s1*t2 - 8*Power(t2,2) + 
                  7*s1*Power(t2,2) + 2*Power(t2,3))) + 
            2*s*(5 + 4*Power(t1,4) + 
               Power(t1,3)*(-15 - 2*Power(s1,2) + s1*(-4 + t2)) - 
               6*t2 - 8*Power(t2,2) + 9*Power(t2,3) + 
               t1*(-4 - 28*t2 + 29*Power(t2,2) - 3*Power(t2,3) + 
                  s1*(4 + 6*t2 - 10*Power(t2,2))) + 
               Power(t1,2)*(-36 + 2*Power(s1,2)*(-1 + t2) - 11*t2 - 
                  Power(t2,2) + s1*(1 + 15*t2 - Power(t2,2)))) + 
            2*(3 + (-13 + 3*s1)*Power(t1,4) - 2*t2 + 3*Power(t2,2) - 
               4*Power(t2,3) + 
               Power(t1,3)*(-28 + 3*Power(s1,2) + s1*(6 - 5*t2) + 
                  8*t2) + Power(t1,2)*
                (10 - 3*Power(s1,2)*(-1 + t2) + 40*t2 + 3*Power(t2,2) + 
                  s1*(-1 - 12*t2 + 2*Power(t2,2))) + 
               t1*(s1*(-4 - 3*t2 + 7*Power(t2,2)) + 
                  2*(12 + 7*t2 - 15*Power(t2,2) + Power(t2,3))))) + 
         Power(s2,2)*Power(t1,2)*
          (6 + (4 - 20*s1)*Power(t1,5) + t2 + 3*Power(t2,2) - 
            9*Power(t2,3) - Power(t2,4) + 
            2*Power(t1,4)*(-116 + 4*Power(s1,3) - 39*t2 - 
               Power(s1,2)*(11 + 2*t2) + 4*s1*(14 + 5*t2)) + 
            Power(t1,3)*(-164 + Power(s1,3)*(6 - 5*t2) + 103*t2 + 
               68*Power(t2,2) + s1*(84 - 27*t2 - 40*Power(t2,2)) + 
               Power(s1,2)*(24 - 11*t2 + 6*Power(t2,2))) + 
            Power(t1,2)*(182 + 225*t2 + 55*Power(t2,2) + 
               8*Power(t2,3) + Power(s1,2)*(32 - 33*t2 + Power(t2,2)) + 
               2*s1*(-20 - 19*t2 - 22*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s,3)*(-7*Power(t1,4) + Power(t1,3)*(17 + 5*t2) + 
               Power(t1,2)*(70 - 4*t2 - 5*Power(t2,2)) + 
               3*(-2 + 5*t2 - 6*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(27 + 10*t2 - 39*Power(t2,2) + 7*Power(t2,3))) + 
            t1*(80 + 133*t2 - 60*Power(t2,2) - 119*Power(t2,3) + 
               6*Power(t2,4) + 
               s1*(8 - 79*t2 + 46*Power(t2,2) + 25*Power(t2,3))) + 
            Power(s,2)*(-9 + Power(t1,4)*(61 + 15*s1 - 4*t2) - 41*t2 + 
               86*Power(t2,2) - 34*Power(t2,3) - 2*Power(t2,4) + 
               Power(t1,3)*(110 + 4*t2 - 6*Power(t2,2) - 
                  6*s1*(-4 + 3*t2)) + 
               Power(t1,2)*(-115 + 52*t2 - 51*Power(t2,2) + 
                  8*Power(t2,3) + s1*(28 - 41*t2 + 3*Power(t2,2))) + 
               t1*(-117 - 27*t2 + 115*Power(t2,2) - 32*Power(t2,3) + 
                  2*Power(t2,4) + s1*(9 - 21*t2 + 19*Power(t2,2)))) + 
            s*(16 + 6*Power(t1,5) + 10*t2 - 83*Power(t2,2) + 
               54*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,4)*(20 - 16*Power(s1,2) + 14*t2 + 
                  s1*(-70 + 8*t2)) + 
               Power(t1,3)*(-240 - 127*t2 + 4*Power(t2,2) + 
                  Power(s1,2)*(-54 + 25*t2) + 2*s1*(-5 + 31*t2)) + 
               Power(t1,2)*(-184 - 236*t2 + 121*Power(t2,2) - 
                  16*Power(t2,3) + 
                  Power(s1,2)*(-24 + 24*t2 - 5*Power(t2,2)) + 
                  s1*(22 + 104*t2 + 18*Power(t2,2) - 8*Power(t2,3))) - 
               t1*(-48 + 105*t2 + 50*Power(t2,2) - 87*Power(t2,3) + 
                  8*Power(t2,4) + 
                  2*s1*(11 - 48*t2 + 31*Power(t2,2) + 6*Power(t2,3))))) + 
         Power(s2,8)*(-2 + 4*t2 - 22*s*t2 - 45*Power(t2,2) + 
            34*s*Power(t2,2) + 20*Power(s,2)*Power(t2,2) - 
            6*Power(s,3)*Power(t2,2) - 96*Power(t2,3) + 
            4*s*Power(t2,3) - 23*Power(s,2)*Power(t2,3) + 
            163*Power(t2,4) - 6*s*Power(t2,4) - 
            2*Power(s,2)*Power(t2,4) - 45*Power(t2,5) - 
            7*s*Power(t2,5) + 
            Power(s1,3)*(6 - 20*Power(t1,3) - 3*t2 + 10*Power(t2,2) + 
               Power(t1,2)*(-54 + 34*t2) + 
               t1*(-10 + 13*t2 - 10*Power(t2,2))) + 
            2*Power(t1,3)*(23 + Power(s,3) + 61*t2 + 9*Power(t2,2) - 
               3*Power(t2,3) - Power(s,2)*(3 + 7*t2) + 
               s*(1 + 16*t2 + 9*Power(t2,2))) + 
            t1*t2*(6 - 335*t2 - 4*Power(s,3)*t2 - 110*Power(t2,2) + 
               131*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,2)*(52 + 40*t2 - 11*Power(t2,2)) + 
               s*(58 + 194*t2 - 105*Power(t2,2) + 40*Power(t2,3))) + 
            Power(t1,2)*(-4*Power(s,3)*t2 + 
               Power(s,2)*(-2 - 18*t2 + 21*Power(t2,2)) + 
               s*(10 + 148*t2 + 65*Power(t2,2) - 55*Power(t2,3)) + 
               2*(16 + 60*t2 - 109*Power(t2,2) - 61*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(s1,2)*(-6 + 4*Power(t1,3)*(-2 + 9*s - 3*t2) + 
               2*(8 + 11*s)*t2 - 42*s*Power(t2,2) + 
               (-69 + s)*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,2)*(-35 + s*(95 - 45*t2) + 118*t2 + 
                  87*Power(t2,2)) + 
               t1*(-71 + 188*t2 + 137*Power(t2,2) - 63*Power(t2,3) + 
                  s*(38 + 19*t2 + 6*Power(t2,2)))) - 
            s1*(18 - (41 + 4*s)*t2 - 
               (20 + 10*s + 21*Power(s,2))*Power(t2,2) - 
               2*(65 + 33*s)*Power(t2,3) + (42 + 4*s)*Power(t2,4) - 
               8*Power(t2,5) + 
               2*Power(t1,3)*
                (9*Power(s,2) - s*(11 + 15*t2) + 
                  16*(1 + 3*t2 + Power(t2,2))) + 
               Power(t1,2)*(52 + 337*t2 + 96*Power(t2,2) - 
                  73*Power(t2,3) - 7*Power(s,2)*(-4 + 3*t2) + 
                  4*s*(-10 + 13*t2 + 27*Power(t2,2))) + 
               t1*(22 + 128*t2 + 86*Power(t2,2) - 169*Power(t2,3) + 
                  50*Power(t2,4) + Power(s,2)*t2*(8 + 3*t2) - 
                  2*s*(14 - 84*t2 - 93*Power(t2,2) + 30*Power(t2,3))))) + 
         Power(s2,4)*(4 + 4*Power(t1,7) - 
            2*Power(t1,6)*(-5 + Power(s,3) + 19*s1 - 8*Power(s1,2) - 
               Power(s1,3) - Power(s,2)*(1 + 3*s1) + 
               s*(8*s1 + 3*Power(s1,2) + 2*(-9 + t2)) - 3*t2) - 12*t2 + 
            12*s*t2 + 11*Power(t2,2) - 8*s*Power(t2,2) + 
            3*Power(s,2)*Power(t2,2) - 6*Power(s,3)*Power(t2,2) + 
            9*Power(t2,3) - 19*s*Power(t2,3) - 
            9*Power(s,2)*Power(t2,3) + 3*Power(s,3)*Power(t2,3) - 
            19*Power(t2,4) + 10*s*Power(t2,4) + 
            6*Power(s,2)*Power(t2,4) + 7*Power(t2,5) + 5*s*Power(t2,5) - 
            Power(t1,5)*(250 + 245*t2 - 12*Power(t2,2) + 
               Power(s1,3)*(4 + 7*t2) + Power(s,3)*(-2 + 11*t2) + 
               Power(s1,2)*(-192 - 77*t2 + 10*Power(t2,2)) + 
               s1*(208 + 129*t2 + 40*Power(t2,2)) - 
               Power(s,2)*(90 + 86*t2 - 6*Power(t2,2) + 
                  3*s1*(1 + 7*t2)) + 
               s*(-254 - 89*t2 - 4*Power(t2,2) + 
                  Power(s1,2)*(2 + 3*t2) + 
                  s1*(326 + 188*t2 - 16*Power(t2,2)))) + 
            Power(t1,4)*(-631 - 1327*t2 - 205*Power(t2,2) - 
               2*Power(t2,3) + 
               Power(s1,3)*(98 + 45*t2 + 2*Power(t2,2)) + 
               Power(s,3)*(49 - 22*t2 + 12*Power(t2,2)) + 
               Power(s1,2)*(-77 - 285*t2 - 161*Power(t2,2) + 
                  8*Power(t2,3)) + 
               s1*(428 + 1151*t2 + 380*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s,2)*(137 + 424*t2 - 74*Power(t2,2) - 
                  4*Power(t2,3) + s1*(-44 + 44*t2 - 23*Power(t2,2))) + 
               s*(-73 - 521*t2 - 95*Power(t2,2) + 20*Power(t2,3) + 
                  Power(s1,2)*(-137 - 81*t2 + 11*Power(t2,2)) + 
                  2*s1*(-93 - 135*t2 + 100*Power(t2,2)))) - 
            t1*(-17 + 2*t2 - 244*Power(t2,2) + 169*Power(t2,3) + 
               51*Power(t2,4) + 19*Power(t2,5) + 
               Power(s,3)*t2*(48 - 7*t2 + Power(t2,2)) - 
               s1*(40 - 95*t2 + 24*Power(t2,2) - 3*Power(t2,3) + 
                  26*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s,2)*t2*
                (90 + 22*t2 - 31*Power(t2,2) + 14*Power(t2,3) + 
                  s1*(-2 - 23*t2 + 4*Power(t2,2))) + 
               s*(-10 - 247*t2 + 294*Power(t2,2) + 7*Power(t2,3) - 
                  32*Power(t2,4) + 6*Power(t2,5) + 
                  s1*(12 - 42*t2 + 50*Power(t2,2) - 20*Power(t2,3)))) + 
            Power(t1,2)*(71 + 649*t2 + 443*Power(t2,2) - 
               87*Power(t2,3) - 200*Power(t2,4) + 4*Power(t2,5) + 
               3*Power(s1,2)*
                (28 - 33*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               Power(s,3)*(-36 + 43*t2 - 47*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s1*(60 - 227*t2 - 80*Power(t2,2) - 59*Power(t2,3) + 
                  86*Power(t2,4)) + 
               Power(s,2)*(-89 - 460*t2 + 25*Power(t2,2) + 
                  2*Power(t2,3) + 12*Power(t2,4) + 
                  s1*(27 + 11*t2 + 90*Power(t2,2) - 24*Power(t2,3))) + 
               s*(143 - 81*t2 - 464*Power(t2,2) + 46*Power(t2,3) + 
                  32*Power(t2,4) - 6*Power(t2,5) - 
                  Power(s1,2)*
                   (40 - 40*t2 + 21*Power(t2,2) + 9*Power(t2,3)) + 
                  s1*(-56 + 318*t2 + 10*Power(t2,2) + 70*Power(t2,3) - 
                     36*Power(t2,4)))) + 
            Power(t1,3)*(75 + 175*t2 + 663*Power(t2,2) + 
               311*Power(t2,3) + 26*Power(t2,4) - 
               2*Power(s1,3)*(-23 + 13*t2 + 5*Power(t2,2)) + 
               Power(s,3)*(17 + 109*t2 - 23*Power(t2,2) + Power(t2,3)) - 
               2*s1*(-41 - 206*t2 + 101*Power(t2,2) + 95*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s1,2)*(105 - 99*t2 - 81*Power(t2,2) + 
                  17*Power(t2,3) + 8*Power(t2,4)) - 
               2*Power(s,2)*(62 + 34*t2 + 17*Power(t2,2) + 
                  33*Power(t2,3) - 5*Power(t2,4) + 
                  2*s1*(-8 - 3*t2 - Power(t2,2) + Power(t2,3))) + 
               2*s*(-81 - 571*t2 - 234*Power(t2,2) + 95*Power(t2,3) - 
                  7*Power(t2,4) + 
                  Power(s1,2)*
                   (-95 - 19*t2 + 20*Power(t2,2) + 5*Power(t2,3)) + 
                  s1*(65 + 185*t2 + 200*Power(t2,2) + 6*Power(t2,3) - 
                     8*Power(t2,4))))) + 
         Power(s2,3)*t1*(-10 + 2*(-3 + s1)*Power(t1,6) + 33*t2 - 
            62*s*t2 + 18*Power(s,2)*t2 + 12*Power(s,3)*t2 - 
            64*Power(t2,2) + 115*s*Power(t2,2) - 
            14*Power(s,2)*Power(t2,2) - 6*Power(s,3)*Power(t2,2) + 
            52*Power(t2,3) - 29*s*Power(t2,3) - 
            12*Power(s,2)*Power(t2,3) - 10*Power(t2,4) - 
            25*s*Power(t2,4) + 8*Power(s,2)*Power(t2,4) - Power(t2,5) + 
            s*Power(t2,5) + Power(t1,5)*
             (7*Power(s,3) + 
               2*s*(-43 + 3*Power(s1,2) + s1*(52 - 3*t2) - 4*t2) + 
               Power(s,2)*(-39 - 15*s1 + 2*t2) + 
               2*(40 + Power(s1,3) - 2*t2 + Power(s1,2)*(-25 + 2*t2) + 
                  s1*(43 + 23*t2))) + 
            Power(t1,4)*(601 + 5*Power(s,3)*(-1 + t2) + 516*t2 + 
               64*Power(t2,2) - Power(s1,3)*(45 + 8*t2) + 
               Power(s1,2)*(69 + 108*t2 + 4*Power(t2,2)) - 
               s1*(359 + 396*t2 + 54*Power(t2,2)) - 
               2*Power(s,2)*(102 + 43*t2 - 6*Power(t2,2) + 
                  s1*(11 + 2*t2)) + 
               s*(59 + 82*t2 - 18*Power(t2,2) + 
                  Power(s1,2)*(81 + 6*t2) + 
                  s1*(220 + 36*t2 - 18*Power(t2,2)))) - 
            Power(t1,3)*(-59 + 30*t2 + 450*Power(t2,2) + 
               74*Power(t2,3) + 
               Power(s1,3)*(26 - 18*t2 - 3*Power(t2,2)) + 
               Power(s,3)*(83 + 7*t2 + 7*Power(t2,2)) + 
               s1*(130 + 178*t2 - 211*Power(t2,2) - 42*Power(t2,3)) + 
               Power(s1,2)*(57 - 48*t2 - 18*Power(t2,2) + 
                  14*Power(t2,3)) + 
               Power(s,2)*(-2 + 186*t2 - 123*Power(t2,2) + 
                  6*Power(t2,3) + s1*(36 - 26*t2 - 15*Power(t2,2))) + 
               s*(-440 - 720*t2 + 23*Power(t2,2) - 2*Power(t2,3) + 
                  Power(s1,2)*(-158 + 34*t2 + 27*Power(t2,2)) + 
                  2*s1*(40 + 87*t2 + 74*Power(t2,2) - 9*Power(t2,3)))) - 
            t1*(29 + 208*t2 + 25*Power(t2,2) - 148*Power(t2,3) - 
               76*Power(t2,4) + 2*Power(t2,5) + 
               Power(s,3)*(-24 + 45*t2 - 59*Power(t2,2) + 
                  11*Power(t2,3)) + 
               s1*(44 - 142*t2 + 45*Power(t2,2) + 28*Power(t2,3) + 
                  25*Power(t2,4)) - 
               Power(s,2)*(49 + 239*t2 - 226*Power(t2,2) + 
                  49*Power(t2,3) + 2*Power(t2,4) + 
                  s1*(-7 + 10*t2 - 28*Power(t2,2) + 4*Power(t2,3))) + 
               s*(94 + 50*t2 - 291*Power(t2,2) + 106*Power(t2,3) + 
                  11*Power(t2,4) - 2*Power(t2,5) - 
                  2*s1*(20 - 56*t2 + 33*Power(t2,2) + 3*Power(t2,4)))) + 
            Power(t1,2)*(-243 - 581*t2 - 223*Power(t2,2) + 
               157*Power(t2,3) - 10*Power(t2,4) - 
               Power(s1,2)*(72 - 79*t2 + 6*Power(t2,2) + Power(t2,3)) - 
               Power(s,3)*(33 + 86*t2 - 63*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s1*(23 + 142*t2 + 99*Power(t2,2) - 36*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s,2)*(216 + 218*t2 - 119*Power(t2,2) + 
                  40*Power(t2,3) - 8*Power(t2,4) + 
                  s1*(-49 + 38*t2 - 26*Power(t2,2) + 4*Power(t2,3))) + 
               s*(9 + 500*t2 + 110*Power(t2,2) - 181*Power(t2,3) + 
                  22*Power(t2,4) + 
                  Power(s1,2)*
                   (48 - 48*t2 + 17*Power(t2,2) + 3*Power(t2,3)) + 
                  s1*(24 - 302*t2 - 34*Power(t2,2) + 22*Power(t2,3) + 
                     6*Power(t2,4))))) - 
         Power(s2,5)*(6 + 27*Power(t1,2) - 301*Power(t1,3) - 
            255*Power(t1,4) + 33*Power(t1,5) + 22*Power(t1,6) - 15*t2 + 
            169*t1*t2 + 273*Power(t1,2)*t2 - 1393*Power(t1,3)*t2 - 
            748*Power(t1,4)*t2 - 52*Power(t1,5)*t2 + 8*Power(t1,6)*t2 + 
            39*Power(t2,2) + 457*t1*Power(t2,2) + 
            563*Power(t1,2)*Power(t2,2) - 633*Power(t1,3)*Power(t2,2) - 
            158*Power(t1,4)*Power(t2,2) - 2*Power(t1,5)*Power(t2,2) + 
            36*Power(t2,3) + 49*t1*Power(t2,3) + 
            559*Power(t1,2)*Power(t2,3) + 181*Power(t1,3)*Power(t2,3) + 
            26*Power(t1,4)*Power(t2,3) - 59*Power(t2,4) - 
            175*t1*Power(t2,4) - 56*Power(t1,2)*Power(t2,4) + 
            6*Power(t1,3)*Power(t2,4) - 3*Power(t2,5) - 
            66*t1*Power(t2,5) + 
            Power(s1,3)*Power(t1,2)*
             (42 - 20*t2 - 12*Power(t2,2) + Power(t1,3)*(3 + 4*t2) - 
               Power(t1,2)*(4 + 14*t2 + 7*Power(t2,2)) + 
               2*t1*(55 + 42*t2 + 8*Power(t2,2))) + 
            Power(s,3)*(Power(t1,5)*(5 - 4*t2) + 
               Power(t2,2)*(-24 + 7*t2) + 
               t1*t2*(-72 - 13*t2 + 3*Power(t2,2)) + 
               Power(t1,4)*(25 - 34*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(7 + 19*t2 + 12*Power(t2,2)) - 
               Power(t1,2)*(24 + t2 - 15*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s,2)*(-2*Power(t1,6) + Power(t1,5)*(-21 + 16*t2) + 
               Power(t2,2)*(1 - 44*t2 + 14*Power(t2,2)) + 
               Power(t1,4)*(32 + 284*t2 + 15*Power(t2,2) - 
                  4*Power(t2,3)) + 
               2*t1*t2*(-72 - 97*t2 - 13*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,3)*(24 + 363*t2 + 26*Power(t2,2) - 
                  71*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,2)*(-51 - 230*t2 - 256*Power(t2,2) - 
                  28*Power(t2,3) + 22*Power(t2,4))) + 
            s*(6*Power(t1,6) + 
               Power(t1,5)*(125 + 42*t2 - 8*Power(t2,2)) + 
               Power(t1,4)*(382 + 254*t2 - 37*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,3)*(55 - 571*t2 - 617*Power(t2,2) + 
                  81*Power(t2,3) + 6*Power(t2,4)) + 
               t2*(40 + 21*t2 - 68*Power(t2,2) - 5*Power(t2,3) + 
                  8*Power(t2,4)) - 
               2*Power(t1,2)*
                (-30 + 214*t2 + 369*Power(t2,2) + 26*Power(t2,3) - 
                  36*Power(t2,4) + 3*Power(t2,5)) - 
               t1*(-22 - 285*t2 + 209*Power(t2,2) + 168*Power(t2,3) + 
                  26*Power(t2,4) + 12*Power(t2,5))) - 
            Power(s1,2)*t1*(-50 + 2*Power(t1,5) + 63*t2 - 
               10*Power(t2,2) - 3*Power(t2,3) - 
               Power(t1,4)*(55 + 34*t2) + 
               Power(t1,3)*(-259 - 368*t2 + 16*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-123 + 119*t2 + 75*Power(t2,2) + 33*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(t1,2)*(43 + 231*t2 + 459*Power(t2,2) + 
                  45*Power(t2,3) - 8*Power(t2,4)) + 
               s*(12 - 12*t2 + 11*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t1,4)*(1 + 12*t2) + 
                  Power(t1,3)*(-68 + 32*t2 - 15*Power(t2,2)) + 
                  t1*(102 + 68*t2 + 42*Power(t2,2) - 30*Power(t2,3)) + 
                  Power(t1,2)*
                   (85 + 185*t2 + 9*Power(t2,2) - Power(t2,3)))) + 
            s1*(12 + 4*(-2 + s)*Power(t1,6) + 2*(-13 + 6*s)*t2 + 
               (11 - 26*s + 7*Power(s,2))*Power(t2,2) + 
               (-6 + 8*s)*Power(t2,3) + (1 + 6*s)*Power(t2,4) + 
               8*Power(t2,5) + 
               Power(t1,5)*(-153 - 40*t2 + 6*Power(t2,2) + 
                  Power(s,2)*(-7 + 12*t2) - 2*s*(13 + 23*t2)) - 
               Power(t1,4)*(322 + 220*t2 + 41*Power(t2,2) + 
                  22*Power(t2,3) + 
                  Power(s,2)*(95 - 78*t2 + 12*Power(t2,2)) + 
                  s*(330 + 720*t2 + 12*Power(t2,2) - 10*Power(t2,3))) + 
               Power(t1,2)*(10*
                   (2 + 33*t2 + 5*Power(t2,2) - 32*Power(t2,3)) + 
                  3*Power(s,2)*
                   (5 + 14*t2 + 38*Power(t2,2) - 8*Power(t2,3)) + 
                  s*(80 + 368*t2 + 420*Power(t2,2) + 192*Power(t2,3) - 
                     66*Power(t2,4))) - 
               2*Power(t1,3)*
                (-93 - 686*t2 - 509*Power(t2,2) - 34*Power(t2,3) + 
                  2*Power(t2,4) + 
                  3*Power(s,2)*(15 - 11*t2 + 5*Power(t2,2)) + 
                  s*(-20 + 219*t2 - 209*Power(t2,2) - 53*Power(t2,3) + 
                     5*Power(t2,4))) + 
               t1*(63 - 132*t2 - 11*Power(t2,2) - 130*Power(t2,3) + 
                  76*Power(t2,4) + 24*Power(t2,5) + 
                  2*Power(s,2)*t2*(10 + 51*t2 - 12*Power(t2,2)) - 
                  2*s*(5 - 70*t2 + 36*Power(t2,2) - 63*Power(t2,3) + 
                     9*Power(t2,4))))) + 
         Power(s2,7)*(6 - 19*t2 + 10*s*t2 - 23*Power(t2,2) - 
            75*s*Power(t2,2) + 3*Power(s,2)*Power(t2,2) + 
            24*Power(s,3)*Power(t2,2) - 67*Power(t2,3) + 
            6*s*Power(t2,3) + 80*Power(s,2)*Power(t2,3) - 
            Power(s,3)*Power(t2,3) - 130*Power(t2,4) + 
            70*s*Power(t2,4) + 2*Power(s,2)*Power(t2,4) + 
            86*Power(t2,5) + 16*s*Power(t2,5) - 
            2*Power(t1,4)*(2*Power(s,3) - Power(s,2)*(7 + 5*t2) + 
               5*(7 + 9*t2 + Power(t2,2)) + 
               s*(8 + 18*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(-4 + 10*Power(t1,4) + 
               Power(t1,3)*(46 - 36*t2) + 2*t2 + Power(t2,2) + 
               4*Power(t1,2)*(4 - 5*t2 + 6*Power(t2,2)) - 
               t1*(29 + 12*t2 + 32*Power(t2,2))) + 
            Power(t1,3)*(Power(s,3)*(-8 + 11*t2) + 
               Power(s,2)*(34 + 16*t2 - 35*Power(t2,2)) + 
               s*(-86 - 265*t2 + 3*Power(t2,2) + 26*Power(t2,3)) - 
               4*(22 + 3*t2 - 70*Power(t2,2) - 11*Power(t2,3) + 
                  Power(t2,4))) - 
            Power(t1,2)*(2 - 303*t2 + 4*Power(s,3)*(-5 + t2)*t2 - 
               718*Power(t2,2) + 27*Power(t2,3) + 88*Power(t2,4) - 
               2*Power(t2,5) + 
               Power(s,2)*(12 + 131*t2 + 106*Power(t2,2) - 
                  31*Power(t2,3)) + 
               s*(52 + 420*t2 + 31*Power(t2,2) - 104*Power(t2,3) + 
                  29*Power(t2,4))) + 
            t1*(6 + 76*t2 + 489*Power(t2,2) - 179*Power(t2,3) - 
               138*Power(t2,4) + 31*Power(t2,5) + 
               Power(s,3)*t2*(12 + 17*t2 - 5*Power(t2,2)) + 
               2*Power(s,2)*t2*
                (-40 + 17*t2 + 43*Power(t2,2) - 8*Power(t2,3)) + 
               s*(-2 + 33*t2 - 50*Power(t2,2) + 248*Power(t2,3) - 
                  65*Power(t2,4) + 3*Power(t2,5))) - 
            Power(s1,2)*(18 + 4*Power(t1,4)*(-3 + 6*s - 2*t2) + 
               (-21 + 10*s)*t2 + (9 - 41*s)*Power(t2,2) + 
               5*(-5 + 2*s)*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,3)*(7 + s*(123 - 73*t2) + 178*t2 + 
                  65*Power(t2,2)) + 
               Power(t1,2)*(-63 + 503*t2 + 155*Power(t2,2) - 
                  75*Power(t2,3) + 2*s*(65 - 15*t2 + 18*Power(t2,2))) + 
               t1*(-20 + 63*t2 - 149*Power(t2,2) - 199*Power(t2,3) + 
                  24*Power(t2,4) + 
                  s*(10 + 3*t2 - 115*Power(t2,2) + 3*Power(t2,3)))) + 
            s1*(6 - 2*(13 + 28*s)*t2 + 
               (-29 + 8*s - 49*Power(s,2))*Power(t2,2) + 
               2*(9 - 33*s + 2*Power(s,2))*Power(t2,3) - 
               2*(-24 + s)*Power(t2,4) - 24*Power(t2,5) + 
               2*Power(t1,4)*
                (9*Power(s,2) - 3*s*(5 + 3*t2) + 
                  4*(6 + 8*t2 + Power(t2,2))) + 
               Power(t1,3)*(187 + Power(s,2)*(75 - 48*t2) + 414*t2 - 
                  9*Power(t2,2) - 32*Power(t2,3) + 
                  4*s*(-17 + 33*t2 + 24*Power(t2,2))) + 
               Power(t1,2)*(110 + 380*t2 - 101*Power(t2,2) - 
                  76*Power(t2,3) + 37*Power(t2,4) + 
                  2*Power(s,2)*(28 - 10*t2 + 9*Power(t2,2)) + 
                  s*(-66 + 602*t2 + 326*Power(t2,2) - 100*Power(t2,3))) \
+ t1*(55 - 204*t2 - 444*Power(t2,2) - 280*Power(t2,3) + 64*Power(t2,4) - 
                  8*Power(t2,5) + 
                  2*Power(s,2)*t2*(3 - 31*t2 + 2*Power(t2,2)) + 
                  2*s*(-26 + 39*t2 - 58*Power(t2,2) - 127*Power(t2,3) + 
                     15*Power(t2,4))))) - 
         Power(s2,6)*(2 - 12*t2 - 34*s*t2 - 68*Power(t2,2) - 
            94*s*Power(t2,2) + 13*Power(s,2)*Power(t2,2) + 
            36*Power(s,3)*Power(t2,2) - 91*Power(t2,3) + 
            65*s*Power(t2,3) + 88*Power(s,2)*Power(t2,3) - 
            5*Power(s,3)*Power(t2,3) + 25*Power(t2,4) + 
            68*s*Power(t2,4) - 8*Power(s,2)*Power(t2,4) + 
            58*Power(t2,5) + 6*s*Power(t2,5) - 
            2*Power(t1,5)*(27 + Power(s,3) + 19*t2 + 2*Power(t2,2) - 
               Power(s,2)*(5 + t2) + s*(9 + 6*t2)) + 
            Power(s1,3)*t1*(-20 + 2*Power(t1,4) + 
               Power(t1,3)*(14 - 19*t2) + 9*t2 + 6*Power(t2,2) + 
               2*Power(t1,2)*(5 - t2 + 11*Power(t2,2)) - 
               2*t1*(36 + 31*t2 + 18*Power(t2,2))) + 
            Power(t1,4)*(-85 + 102*t2 + 120*Power(t2,2) - 
               2*Power(t2,3) + Power(s,3)*(-15 + 11*t2) + 
               Power(s,2)*(55 - 20*t2 - 14*Power(t2,2)) + 
               s*(-165 - 177*t2 + 20*Power(t2,2) + 4*Power(t2,3))) + 
            Power(t1,3)*(93 + 778*t2 + 609*Power(t2,2) - 
               98*Power(t2,3) - 12*Power(t2,4) + 
               Power(s,3)*(-10 + 31*t2 - 6*Power(t2,2)) + 
               Power(s,2)*(1 - 257*t2 - 115*Power(t2,2) + 
                  26*Power(t2,3)) + 
               s*(-266 - 505*t2 + 138*Power(t2,2) + 11*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(t1,2)*(51 + 674*t2 + 756*Power(t2,2) - 
               278*Power(t2,3) - 71*Power(t2,4) + 4*Power(t2,5) + 
               Power(s,3)*(6 + 30*t2 - 25*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(-12 - 175*t2 + 56*Power(t2,2) + 
                  76*Power(t2,3) - 10*Power(t2,4)) + 
               s*(-35 + 67*t2 + 608*Power(t2,2) + 131*Power(t2,3) - 
                  77*Power(t2,4) + 2*Power(t2,5))) + 
            t1*(13 - 120*t2 - 185*Power(t2,2) - 509*Power(t2,3) + 
               12*Power(t2,4) + 74*Power(t2,5) + 
               3*Power(s,3)*t2*(16 + 9*t2 - 3*Power(t2,2)) + 
               2*Power(s,2)*t2*
                (21 + 117*t2 + 60*Power(t2,2) - 17*Power(t2,3)) + 
               s*(-14 - 27*t2 + 80*Power(t2,2) + 274*Power(t2,3) + 
                  8*Power(t2,4) + 10*Power(t2,5))) + 
            Power(s1,2)*(-12 + 16*t2 + (-3 + 2*s)*Power(t2,2) + 
               (-1 + 3*s)*Power(t2,3) + Power(t1,5)*(8 - 6*s + 2*t2) - 
               Power(t1,4)*(61 + s*(59 - 49*t2) + 126*t2 + 
                  18*Power(t2,2)) - 
               Power(t1,3)*(107 + 627*t2 + 53*Power(t2,2) - 
                  37*Power(t2,3) + 2*s*(78 - 32*t2 + 21*Power(t2,2))) + 
               t1*(-75 + 78*t2 + 9*Power(t2,2) + 61*Power(t2,3) - 
                  24*Power(t2,4) + 
                  s*(20 + 11*t2 + 96*Power(t2,2) - 30*Power(t2,3))) + 
               Power(t1,2)*(27 + 3*t2 + 451*Power(t2,2) + 
                  183*Power(t2,3) - 24*Power(t2,4) + 
                  s*(3 + 129*t2 + 93*Power(t2,2) - 3*Power(t2,3)))) + 
            s1*(-18 + (33 - 50*s)*t2 + 
               (-18 + 68*s - 35*Power(s,2))*Power(t2,2) + 
               (47 - 42*s + 4*Power(s,2))*Power(t2,3) + 
               (2 - 12*s)*Power(t2,4) - 24*Power(t2,5) + 
               2*Power(t1,5)*(3*Power(s,2) + 8*(2 + t2) - s*(9 + 2*t2)) + 
               Power(t1,4)*(252 + Power(s,2)*(60 - 41*t2) + 217*t2 - 
                  30*Power(t2,2) - 4*Power(t2,3) + 
                  2*s*(-12 + 67*t2 + 15*Power(t2,2))) + 
               Power(t1,3)*(274 + 381*t2 - 108*Power(t2,2) + 
                  35*Power(t2,3) + 8*Power(t2,4) + 
                  Power(s,2)*(139 - 85*t2 + 27*Power(t2,2)) + 
                  s*(70 + 980*t2 + 198*Power(t2,2) - 60*Power(t2,3))) + 
               t1*(10 - 139*t2 - 110*Power(t2,2) + 190*Power(t2,3) + 
                  36*Power(t2,4) - 24*Power(t2,5) + 
                  2*Power(s,2)*t2*(-11 - 72*t2 + 12*Power(t2,2)) + 
                  4*s*(-8 - 42*t2 - 40*Power(t2,2) - 57*Power(t2,3) + 
                     12*Power(t2,4))) + 
               Power(t1,2)*(26 - 740*t2 - 1116*Power(t2,2) - 
                  202*Power(t2,3) + 26*Power(t2,4) + 
                  Power(s,2)*(39 - 20*t2 - 34*Power(t2,2) + 
                     4*Power(t2,3)) + 
                  2*s*(-64 + 147*t2 - 171*Power(t2,2) - 147*Power(t2,3) + 
                     18*Power(t2,4))))))*R1q(s2))/
     ((-1 + s1)*Power(-1 + s2,3)*s2*(-1 + t1)*Power(-s2 + t1,3)*
       (-s + s1 - t2)*(1 - s2 + t1 - t2)*Power(t1 - s2*t2,2)) + 
    (8*(-4*Power(s2,11)*t1*(-1 + t2)*Power(t2,2) + 
         Power(s2,9)*(2*Power(s1,3)*Power(t1,2)*(1 + 5*t1 - 5*t2) - 
            12*Power(t2,3)*(1 + t2) + 
            Power(t1,3)*(4 + 8*(1 + 3*s)*t2 + 
               (-45 + 47*s)*Power(t2,2) - 18*Power(t2,3)) + 
            t1*t2*(4 + (13 + 12*s)*t2 + (30 + 33*s)*Power(t2,2) + 
               4*(25 + 6*s)*Power(t2,3) - 60*Power(t2,4)) - 
            2*Power(s1,2)*Power(t1,2)*
             (-3 + 2*t2 - t1*t2 + Power(t2,2) + s*(3*t1 + t2)) + 
            s1*t1*t2*(Power(t1,2)*(2 - 6*s - 5*t2) + 
               t1*(2 + 6*s + 46*t2 - 12*s*t2 + 10*Power(t2,2)) - 
               t2*(5 + 12*t2 + 24*Power(t2,2))) + 
            Power(t1,2)*t2*(12*Power(s,2)*t2 + 
               2*t2*(-49 + 19*t2 + 31*Power(t2,2)) - 
               s*(8 + 41*t2 + 87*Power(t2,2)))) - 
         Power(s2,10)*(2*Power(s1,3)*Power(t1,2) + 
            s1*t1*Power(t2,2)*(1 + t1 + 4*t2) + 
            t2*(2*t2*(1 + t2) + 
               Power(t1,2)*(8 + 2*(-1 + 6*s)*t2 - 13*Power(t2,2)) + 
               t1*t2*(-4*s*(1 + t2) + t2*(-29 + 24*t2)))) + 
         Power(s2,8)*(2*Power(t2,2)*
             (2 + 2*t2 - 15*Power(t2,2) - 15*Power(t2,3)) + 
            t1*Power(t2,2)*(49 + 94*t2 + 145*Power(t2,2) + 
               203*Power(t2,3) - 80*Power(t2,4) - 
               s1*(3 + 19*t2 + 43*Power(t2,2) + 60*Power(t2,3)) + 
               s*(-10 + 53*t2 + 104*Power(t2,2) + 60*Power(t2,3))) - 
            Power(t1,2)*(2 + 10*(2 + 3*s)*t2 + 
               (144 + 123*s - 36*Power(s,2))*Power(t2,2) + 
               (593 + 213*s - 72*Power(s,2))*Power(t2,3) + 
               (-121 + 252*s)*Power(t2,4) - 117*Power(t2,5) + 
               Power(s1,3)*(-4 - t2 + 20*Power(t2,2)) + 
               Power(s1,2)*(6 - 2*(19 + 5*s)*t2 + 
                  (1 + 12*s)*Power(t2,2) + 8*Power(t2,3)) + 
               s1*(6 + 2*(-9 + 4*s)*t2 + 
                  (-59 + 6*s - 2*Power(s,2))*Power(t2,2) + 
                  (-277 + 72*s)*Power(t2,3) - 61*Power(t2,4))) - 
            Power(t1,4)*(6 + 20*Power(s1,3) - 51*t2 - 98*Power(t2,2) - 
               16*Power(t2,3) + 6*Power(s,2)*(s1 + t2) + 
               Power(s1,2)*(2 + 8*t2) + 
               s1*(1 - 10*t2 - 30*Power(t2,2)) + 
               s*(12 - 24*Power(s1,2) + 94*t2 + 80*Power(t2,2) - 
                  2*s1*(3 + 11*t2))) + 
            Power(t1,3)*(-2*Power(s,2)*t2*(9 - s1 + 35*t2) + 
               Power(s1,3)*(4 + 51*t2) + 
               Power(s1,2)*(-26 + 22*t2 + Power(t2,2)) + 
               t2*(109 + 71*t2 - 267*Power(t2,2) - 72*Power(t2,3)) - 
               s1*(1 + 84*t2 + 164*Power(t2,2) + 38*Power(t2,3)) + 
               s*(4 + Power(s1,2)*(14 - 28*t2) + 76*t2 + 
                  403*Power(t2,2) + 242*Power(t2,3) + 
                  2*s1*(3 + 5*t2 + 23*Power(t2,2))))) + 
         Power(s2,7)*(-2*Power(t2,2)*
             (1 - 7*t2 - 8*Power(t2,2) + 20*Power(t2,3) + 
               20*Power(t2,4)) + 
            t1*t2*(-8 + (-25 - 18*s + 15*s1)*t2 + 
               (119 - 49*s - 4*s1)*Power(t2,2) + 
               (244 + 99*s - 28*s1)*Power(t2,3) + 
               (289 + 167*s - 72*s1)*Power(t2,4) + 
               4*(63 + 20*s - 20*s1)*Power(t2,5) - 60*Power(t2,6)) - 
            Power(t1,2)*(-2 + (66 - 30*s)*t2 + 
               (263 + 69*s + 28*Power(s,2))*Power(t2,2) + 
               (753 + 469*s - 168*Power(s,2) - 2*Power(s,3))*
                Power(t2,3) + 
               (1640 + 436*s - 182*Power(s,2))*Power(t2,4) + 
               (-147 + 377*s)*Power(t2,5) - 108*Power(t2,6) + 
               Power(s1,3)*t2*(-14 + 9*t2 + 20*Power(t2,2)) + 
               Power(s1,2)*(12 - 7*t2 - (103 + 23*s)*Power(t2,2) + 
                  (-25 + 28*s)*Power(t2,3) + 12*Power(t2,4)) + 
               s1*(-6 + (40 + 26*s)*t2 + 
                  (32 + 10*s - 14*Power(s,2))*Power(t2,2) + 
                  (-202 + 74*s - 6*Power(s,2))*Power(t2,3) + 
                  (-677 + 180*s)*Power(t2,4) - 124*Power(t2,5))) - 
            Power(t1,5)*(19 + 2*Power(s,3) - 20*Power(s1,3) + 142*t2 + 
               104*Power(t2,2) + 10*Power(t2,3) - 
               4*Power(s1,2)*(2 + 3*t2) - 
               2*Power(s,2)*(3 + 9*s1 + 7*t2) + 
               s1*(5 + 60*t2 + 50*Power(t2,2)) + 
               s*(-47 + 36*Power(s1,2) - 160*t2 - 76*Power(t2,2) + 
                  s1*(22 + 30*t2))) + 
            Power(t1,4)*(-40 - 160*t2 + 4*Power(s,3)*t2 + 
               389*Power(t2,2) + 402*Power(t2,3) + 56*Power(t2,4) - 
               Power(s1,3)*(51 + 104*t2) + 
               Power(s1,2)*(40 - 40*t2 + 23*Power(t2,2)) + 
               2*Power(s,2)*(6 + s1*(8 - 21*t2) + 71*t2 + 
                  59*Power(t2,2)) + 
               s1*(42 + 314*t2 + 343*Power(t2,2) + 110*Power(t2,3)) - 
               s*(39 + Power(s1,2)*(8 - 129*t2) + 567*t2 + 
                  1033*Power(t2,2) + 316*Power(t2,3) + 
                  s1*(46 + 74*t2 + 94*Power(t2,2)))) + 
            Power(t1,3)*(9 + 200*t2 + 1401*Power(t2,2) + 
               24*Power(s,3)*Power(t2,2) + 560*Power(t2,3) - 
               548*Power(t2,4) - 112*Power(t2,5) + 
               Power(s1,3)*(-29 + 34*t2 + 97*Power(t2,2)) - 
               Power(s1,2)*(20 + 163*t2 - 18*Power(t2,2) + 
                  5*Power(t2,3)) - 
               s1*(-17 + 118*t2 + 704*Power(t2,2) + 796*Power(t2,3) + 
                  101*Power(t2,4)) - 
               2*Power(s,2)*t2*
                (46 + 143*t2 + 154*Power(t2,2) + 2*s1*(-2 + 9*t2)) + 
               s*(12 + 161*t2 + 426*Power(t2,2) + 1452*Power(t2,3) + 
                  511*Power(t2,4) + 
                  Power(s1,2)*(-4 + 27*t2 - 15*Power(t2,2)) + 
                  2*s1*(-8 + 61*t2 + 156*Power(t2,2) + 133*Power(t2,3))))\
) + Power(s2,6)*(-6*Power(t2,3)*Power(1 + t2,2)*
             (1 - 5*t2 + 5*Power(t2,2)) + 
            t1*t2*(4 + 3*(-23 + 4*s - 2*s1)*t2 + 
               (-113 - 57*s + 41*s1)*Power(t2,2) + 
               (101 - 92*s + 6*s1)*Power(t2,3) + 
               2*(151 + 52*s - 10*s1)*Power(t2,4) + 
               (304 + 148*s - 63*s1)*Power(t2,5) + 
               (187 + 60*s - 60*s1)*Power(t2,6) - 24*Power(t2,7)) + 
            Power(t1,2)*(2 + (56 + 52*s)*t2 + 
               (-35 + 217*s - 108*Power(s,2))*Power(t2,2) + 
               (-647 - 175*s - 104*Power(s,2) + 6*Power(s,3))*
                Power(t2,3) + 
               (-1582 - 821*s + 335*Power(s,2) + 8*Power(s,3))*
                Power(t2,4) + 
               (-2332 - 422*s + 248*Power(s,2))*Power(t2,5) + 
               (67 - 308*s)*Power(t2,6) + 47*Power(t2,7) + 
               Power(s1,3)*Power(t2,2)*(16 - 13*t2 - 10*Power(t2,2)) + 
               Power(s1,2)*t2*
                (-42 + (-11 + 6*s)*t2 + (125 + 9*s)*Power(t2,2) + 
                  (41 - 32*s)*Power(t2,3) - 8*Power(t2,4)) + 
               s1*(12 + (-47 + 12*s)*t2 - 
                  2*(65 - 41*s + 6*Power(s,2))*Power(t2,2) + 
                  (-105 + 50*s + 43*Power(s,2))*Power(t2,3) + 
                  2*(160 - 69*s + 2*Power(s,2))*Power(t2,4) + 
                  (855 - 240*s)*Power(t2,5) + 121*Power(t2,6))) + 
            Power(t1,6)*(62 + 4*Power(s,3) - 10*Power(s1,3) + 160*t2 + 
               60*Power(t2,2) + 3*Power(t2,3) - 
               4*Power(s1,2)*(3 + 2*t2) - 
               2*Power(s,2)*(7 + 9*s1 + 5*t2) + 
               5*s1*(6 + 20*t2 + 7*Power(t2,2)) + 
               2*s*(12*Power(s1,2) + 3*s1*(5 + 3*t2) - 
                  4*(10 + 19*t2 + 5*Power(t2,2)))) - 
            Power(t1,5)*(-75 + 225*t2 + 804*Power(t2,2) + 
               309*Power(t2,3) + 31*Power(t2,4) + 
               2*Power(s,3)*(-2 + 9*t2) - 2*Power(s1,3)*(62 + 53*t2) + 
               Power(s1,2)*(21 - 6*t2 + 55*Power(t2,2)) + 
               Power(s,2)*(90 + (248 - 111*s1)*t2 + 93*Power(t2,2)) + 
               s1*(160 + 596*t2 + 545*Power(t2,2) + 143*Power(t2,3)) + 
               s*(-251 - 1370*t2 - 1264*Power(t2,2) - 238*Power(t2,3) + 
                  Power(s1,2)*(105 + 199*t2) - 
                  2*s1*(50 + 73*t2 + 63*Power(t2,2)))) + 
            Power(t1,4)*(-86 - 1431*t2 - 2221*Power(t2,2) + 
               617*Power(t2,3) + 635*Power(t2,4) + 73*Power(t2,5) - 
               4*Power(s,3)*t2*(13 + 11*t2) + 
               Power(s1,3)*(66 - 185*t2 - 182*Power(t2,2)) + 
               Power(s1,2)*(164 + 260*t2 + 8*Power(t2,2) + 
                  97*Power(t2,3)) + 
               s1*(11 + 749*t2 + 1972*Power(t2,2) + 1102*Power(t2,3) + 
                  167*Power(t2,4)) + 
               Power(s,2)*(38 + 432*t2 + 1075*Power(t2,2) + 
                  466*Power(t2,3) + s1*(-16 + 107*t2 + 42*Power(t2,2))) \
+ s*(-47 - 327*t2 - 2614*Power(t2,2) - 2737*Power(t2,3) - 
                  506*Power(t2,4) + 
                  3*Power(s1,2)*(-25 - 22*t2 + 50*Power(t2,2)) - 
                  2*s1*(22 + 378*t2 + 512*Power(t2,2) + 227*Power(t2,3))\
)) + Power(t1,3)*(19 + 310*t2 + 1441*Power(t2,2) + 4991*Power(t2,3) + 
               1611*Power(t2,4) - 501*Power(t2,5) - 85*Power(t2,6) + 
               2*Power(s,3)*Power(t2,2)*(-1 + 45*t2) + 
               Power(s1,3)*(-26 - 45*t2 + 70*Power(t2,2) + 
                  85*Power(t2,3)) - 
               Power(s1,2)*(-77 + 260*t2 + 402*Power(t2,2) + 
                  70*Power(t2,3) + Power(t2,4)) + 
               s1*(25 + 298*t2 + 16*Power(t2,2) - 1947*Power(t2,3) - 
                  1515*Power(t2,4) - 119*Power(t2,5)) - 
               Power(s,2)*t2*
                (-50 + 372*t2 + 986*Power(t2,2) + 555*Power(t2,3) + 
                  s1*(54 + 32*t2 + 135*Power(t2,2))) + 
               s*(-8 - 103*t2 + 363*Power(t2,2) + 733*Power(t2,3) + 
                  2221*Power(t2,4) + 560*Power(t2,5) + 
                  Power(s1,2)*
                   (-12 - 27*t2 + 52*Power(t2,2) + 75*Power(t2,3)) + 
                  2*s1*(7 + 30*t2 + 136*Power(t2,2) + 514*Power(t2,3) + 
                     263*Power(t2,4))))) + 
         Power(t1,4)*(8*Power(t1,6)*
             (2 + 2*Power(s,2) + 2*Power(s1,2) - t2 + s1*(10 + t2) - 
               s*(10 + 4*s1 + t2)) + 
            Power(t2,4)*(-15 + 37*t2 - 25*Power(t2,2) + 3*Power(t2,3) + 
               Power(s,3)*(-12 + 8*t2 + 3*Power(t2,2) - 
                  2*Power(t2,3)) - 
               s*(-16 + 31*t2 - 16*Power(t2,2) + Power(t2,3)) + 
               3*Power(s,2)*(4 - 5*t2 - Power(t2,2) + 2*Power(t2,3))) + 
            t1*Power(t2,2)*(114 + (-287 + 6*s1)*t2 - 
               3*(-49 + s1)*Power(t2,2) + 2*(-12 + 7*s1)*Power(t2,3) + 
               (71 - 21*s1)*Power(t2,4) + (-17 + 4*s1)*Power(t2,5) + 
               Power(s,3)*(96 - 64*t2 - 42*Power(t2,2) + 
                  15*Power(t2,3) + 5*Power(t2,4)) - 
               Power(s,2)*(96 - 120*t2 + 2*(-11 + s1)*Power(t2,2) + 
                  (62 - 17*s1)*Power(t2,3) + (5 + 8*s1)*Power(t2,4) + 
                  2*Power(t2,5)) + 
               s*(-128 + 236*t2 - 54*Power(t2,2) + 
                  (1 - 28*s1)*Power(t2,3) + (-58 + 28*s1)*Power(t2,4) + 
                  5*Power(t2,5))) - 
            Power(t1,2)*(192 + (-544 + 60*s1)*t2 - 
               2*(-10 + 6*s1 + 3*Power(s1,2))*Power(t2,2) + 
               (-32 + 55*s1 + Power(s1,2))*Power(t2,3) + 
               3*(185 - 51*s1 + 5*Power(s1,2))*Power(t2,4) + 
               (-65 + 27*s1 - 10*Power(s1,2))*Power(t2,5) + 
               (-16 + s1)*Power(t2,6) + 
               Power(s,3)*(192 - 128*t2 - 192*Power(t2,2) + 
                  30*Power(t2,3) + 35*Power(t2,4) + 7*Power(t2,5)) - 
               Power(s,2)*(192 - 240*t2 + 2*(-25 + 8*s1)*Power(t2,2) + 
                  (227 - 124*s1)*Power(t2,3) + 
                  3*(-5 + 16*s1)*Power(t2,4) + 
                  (35 + 17*s1)*Power(t2,5) + 4*Power(t2,6)) + 
               s*(-256 + 412*t2 - 4*(-76 + 3*s1)*Power(t2,2) + 
                  (27 - 206*s1 + 6*Power(s1,2))*Power(t2,3) + 
                  (-510 + 174*s1 - 11*Power(s1,2))*Power(t2,4) + 
                  2*(11 + 12*s1 + 5*Power(s1,2))*Power(t2,5) + 
                  (2 + 4*s1)*Power(t2,6))) + 
            Power(t1,5)*(608 + 64*Power(s,3) - 128*t2 - 6*Power(t2,2) - 
               Power(t2,3) - 
               16*Power(s,2)*(17 + 8*s1 + t2 + Power(t2,2)) - 
               16*Power(s1,2)*(-1 + 2*t2 + Power(t2,2)) + 
               s1*(-176 - 4*t2 - 48*Power(t2,2) + Power(t2,3)) + 
               s*(-80 + 64*Power(s1,2) + 44*t2 + 52*Power(t2,2) - 
                  Power(t2,3) + 16*s1*(18 + 3*t2 + 2*Power(t2,2)))) + 
            Power(t1,4)*(Power(s1,3)*t2*(-20 + 4*t2 + 3*Power(t2,2)) - 
               Power(s,3)*t2*(84 + 32*t2 + 3*Power(t2,2)) + 
               Power(s1,2)*(80 + 88*t2 - 46*Power(t2,2) + 
                  25*Power(t2,3)) + 
               2*(120 - 572*t2 - 54*Power(t2,2) + 32*Power(t2,3) + 
                  Power(t2,4)) + 
               s1*(-288 + 124*t2 + 128*Power(t2,2) + 13*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s,2)*(-464 + 200*t2 + 162*Power(t2,2) + 
                  21*Power(t2,3) + 
                  s1*(-96 + 196*t2 + 68*Power(t2,2) + 9*Power(t2,3))) - 
               s*(-704 - 252*t2 + 44*Power(t2,2) + 35*Power(t2,3) + 
                  8*Power(t2,4) + 
                  Power(s1,2)*
                   (-96 + 92*t2 + 40*Power(t2,2) + 9*Power(t2,3)) + 
                  2*s1*(-32 + 104*t2 + 66*Power(t2,2) + 23*Power(t2,3)))\
) + Power(t1,3)*(-672 + 64*t2 + 1012*Power(t2,2) + 304*Power(t2,3) - 
               75*Power(t2,4) - 8*Power(t2,5) + 
               Power(s1,3)*t2*
                (-12 + 12*t2 + 5*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s,3)*(-288 + 20*t2 + 48*Power(t2,2) + 
                  49*Power(t2,3) + 7*Power(t2,4)) + 
               Power(s1,2)*(48 - 56*t2 + 40*Power(t2,2) - 
                  56*Power(t2,3) + 9*Power(t2,4) - 2*Power(t2,5)) - 
               s1*(-96 + 4*t2 + 204*Power(t2,2) - 19*Power(t2,3) + 
                  23*Power(t2,4) + 3*Power(t2,5)) - 
               Power(s,2)*(-112 + 360*t2 - 256*Power(t2,2) + 
                  152*Power(t2,3) + 41*Power(t2,4) + 2*Power(t2,5) + 
                  s1*(32 - 188*t2 + 4*Power(t2,2) + 117*Power(t2,3) + 
                     18*Power(t2,4))) + 
               s*(960 + 156*t2 - 1288*Power(t2,2) - 61*Power(t2,3) + 
                  30*Power(t2,4) + 6*Power(t2,5) + 
                  Power(s1,2)*t2*
                   (60 - 104*t2 + 63*Power(t2,2) + 15*Power(t2,3)) + 
                  2*s1*(-96 - 128*t2 + 116*Power(t2,2) + 
                     68*Power(t2,3) + 17*Power(t2,4) + 2*Power(t2,5))))) \
+ Power(s2,4)*(-2*Power(t2,5)*
             (1 - t2 - 2*Power(t2,2) + Power(t2,3) + Power(t2,4)) + 
            t1*Power(t2,3)*(36 + (-43 + 24*s - 6*s1)*t2 + 
               (-75 - 35*s + 11*s1)*Power(t2,2) + 
               (-18 - 34*s + 5*s1)*Power(t2,3) + 
               (52 + 27*s - s1)*Power(t2,4) + 
               (55 + 16*s - 5*s1)*Power(t2,5) + 
               (13 + 4*s - 4*s1)*Power(t2,6)) - 
            Power(t1,2)*t2*(64 + (-89 + 6*s + 12*s1)*t2 - 
               (522 + 150*Power(s,2) + 43*s1 + 18*Power(s1,2) - 
                  9*s*(11 + 12*s1))*Power(t2,2) + 
               (-352 + 22*Power(s,3) + 240*s1 + 31*Power(s1,2) + 
                  Power(s,2)*(271 + 24*s1) - 
                  2*s*(327 + 95*s1 + 3*Power(s1,2)))*Power(t2,3) + 
               (62 - 27*Power(s,3) + Power(s,2)*(99 - 17*s1) + 
                  21*s1 - 9*Power(s1,2) + 
                  s*(383 - 62*s1 + 7*Power(s1,2)))*Power(t2,4) + 
               (944 - 8*Power(s,3) - 129*s1 - 4*Power(s1,2) + 
                  3*Power(s,2)*(-79 + 2*s1) + 
                  s*(349 + 56*s1 + 4*Power(s1,2)))*Power(t2,5) + 
               (680 - 80*Power(s,2) - 200*s1 + s*(-26 + 72*s1))*
                Power(t2,6) + (2 + 20*s - 11*s1)*Power(t2,7) + 
               Power(t2,8)) + 
            Power(t1,8)*(40 - 2*Power(s,2) - 2*Power(s1,2) + 
               2*s*(-20 + 2*s1 - 9*t2) + 21*t2 + s1*(35 + 18*t2)) + 
            Power(t1,3)*(-22 - 491*t2 - 1162*Power(t2,2) - 
               631*Power(t2,3) + 3430*Power(t2,4) + 5715*Power(t2,5) + 
               1238*Power(t2,6) - 105*Power(t2,7) - 7*Power(t2,8) + 
               Power(s1,3)*Power(t2,2)*
                (-22 + 9*t2 + 14*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s,3)*Power(t2,2)*
                (104 - 341*t2 - 8*Power(t2,2) + 93*Power(t2,3)) + 
               Power(s1,2)*t2*
                (24 + 131*t2 + 52*Power(t2,2) - 232*Power(t2,3) - 
                  64*Power(t2,4) + 4*Power(t2,5)) - 
               Power(s,2)*t2*
                (192 - 3*(183 + 2*s1)*t2 - 
                  (1256 + 137*s1)*Power(t2,2) + 
                  2*(599 + 65*s1)*Power(t2,3) + 
                  3*(404 + 29*s1)*Power(t2,4) + 263*Power(t2,5)) + 
               s1*(-84 - 222*t2 + 462*Power(t2,2) + 661*Power(t2,3) - 
                  213*Power(t2,4) - 1548*Power(t2,5) - 
                  684*Power(t2,6) + 13*Power(t2,7)) + 
               s*(12 + 8*(31 + 24*s1)*t2 + 
                  2*(-332 - 209*s1 + 6*Power(s1,2))*Power(t2,2) + 
                  (835 - 1328*s1 - 81*Power(s1,2))*Power(t2,3) + 
                  2*(611 + 135*s1 + 51*Power(s1,2))*Power(t2,4) + 
                  (-530 + 954*s1 + 69*Power(s1,2))*Power(t2,5) + 
                  (349 + 320*s1)*Power(t2,6) + 97*Power(t2,7))) - 
            Power(t1,7)*(216 + 649*t2 + 305*Power(t2,2) + 
               37*Power(t2,3) + Power(s,3)*(25 + 11*t2) - 
               Power(s1,3)*(72 + 11*t2) + 
               Power(s1,2)*(13 + 68*t2 + 14*Power(t2,2)) + 
               s1*(341 + 677*t2 + 288*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s,2)*(87 + 72*t2 + 14*Power(t2,2) - 
                  s1*(122 + 33*t2)) + 
               s*(-650 - 1048*t2 - 340*Power(t2,2) - 17*Power(t2,3) + 
                  Power(s1,2)*(169 + 33*t2) - 
                  14*s1*(5 + 10*t2 + 2*Power(t2,2)))) + 
            Power(t1,6)*(-990 - 1544*t2 + 1099*Power(t2,2) + 
               727*Power(t2,3) + 112*Power(t2,4) + 9*Power(t2,5) + 
               Power(s,3)*(-79 - 104*t2 + 10*Power(t2,2)) - 
               Power(s1,3)*(86 + 349*t2 + 70*Power(t2,2)) + 
               Power(s1,2)*(280 + 151*t2 + 423*Power(t2,2) + 
                  131*Power(t2,3)) + 
               s1*(637 + 2450*t2 + 2239*Power(t2,2) + 
                  739*Power(t2,3) + 66*Power(t2,4)) + 
               Power(s,2)*(596 + 1900*t2 + 1007*Power(t2,2) + 
                  143*Power(t2,3) + s1*(322 + 42*t2 - 90*Power(t2,2))) \
+ s*(-484 - 3841*t2 - 4342*Power(t2,2) - 1253*Power(t2,3) - 
                  84*Power(t2,4) + 
                  Power(s1,2)*(-245 + 411*t2 + 150*Power(t2,2)) - 
                  2*s1*(500 + 925*t2 + 639*Power(t2,2) + 
                     137*Power(t2,3)))) + 
            Power(t1,5)*(388 + 5000*t2 + 11377*Power(t2,2) + 
               3855*Power(t2,3) - 982*Power(t2,4) - 221*Power(t2,5) - 
               13*Power(t2,6) + 
               Power(s1,3)*(-266 + 94*t2 + 326*Power(t2,2) + 
                  86*Power(t2,3)) + 
               Power(s,3)*(14 + 309*t2 + 441*Power(t2,2) + 
                  146*Power(t2,3)) - 
               Power(s1,2)*(117 + 1092*t2 + 598*Power(t2,2) + 
                  320*Power(t2,3) + 153*Power(t2,4)) - 
               s1*(-583 + 269*t2 + 4819*Power(t2,2) + 
                  3769*Power(t2,3) + 828*Power(t2,4) + 98*Power(t2,5)) \
- Power(s,2)*(-21 + 754*t2 + 3437*Power(t2,2) + 2662*Power(t2,3) + 
                  365*Power(t2,4) + 
                  s1*(-4 + 401*t2 + 946*Power(t2,2) + 260*Power(t2,3))) \
+ 2*s*(-229 - 1042*t2 + 1130*Power(t2,2) + 2896*Power(t2,3) + 
                  964*Power(t2,4) + 92*Power(t2,5) + 
                  Power(s1,2)*
                   (38 + 223*t2 + 173*Power(t2,2) + 14*Power(t2,3)) + 
                  s1*(-105 + 890*t2 + 2240*Power(t2,2) + 
                     1306*Power(t2,3) + 233*Power(t2,4)))) + 
            Power(t1,4)*(176 + 485*t2 - 2356*Power(t2,2) - 
               11642*Power(t2,3) - 8429*Power(t2,4) - 496*Power(t2,5) + 
               261*Power(t2,6) + 11*Power(t2,7) + 
               Power(s,3)*t2*
                (262 + 165*t2 - 499*Power(t2,2) - 190*Power(t2,3)) + 
               Power(s1,3)*(28 + 36*t2 - 70*Power(t2,2) - 
                  95*Power(t2,3) - 34*Power(t2,4)) + 
               Power(s1,2)*(-386 + 177*t2 + 1218*Power(t2,2) + 
                  599*Power(t2,3) + 150*Power(t2,4) + 27*Power(t2,5)) + 
               s1*(-24 - 1484*t2 - 2104*Power(t2,2) + 
                  2059*Power(t2,3) + 4137*Power(t2,4) + 
                  800*Power(t2,5) + 36*Power(t2,6)) + 
               Power(s,2)*(-134 - 982*t2 + 255*Power(t2,2) + 
                  2621*Power(t2,3) + 2206*Power(t2,4) + 
                  511*Power(t2,5) + 
                  s1*(-12 - 144*t2 + 154*Power(t2,2) + 
                     571*Power(t2,3) + 392*Power(t2,4))) + 
               s*(102 + 265*t2 + 656*Power(t2,2) + 2440*Power(t2,3) - 
                  2020*Power(t2,4) - 1672*Power(t2,5) - 
                  164*Power(t2,6) + 
                  Power(s1,2)*
                   (84 + 270*t2 - 20*Power(t2,2) - 435*Power(t2,3) - 
                     198*Power(t2,4)) - 
                  2*s1*(-46 - 505*t2 - 94*Power(t2,2) + 
                     1143*Power(t2,3) + 1325*Power(t2,4) + 
                     228*Power(t2,5))))) + 
         Power(s2,5)*(-2*Power(t2,4)*
             (3 - 5*t2 - 8*Power(t2,2) + 6*Power(t2,3) + 6*Power(t2,4)) \
+ t1*Power(t2,2)*(34 + 3*(-33 + 10*s - 4*s1)*t2 + 
               (-157 - 67*s + 37*s1)*Power(t2,2) + 
               (15 - 82*s + 12*s1)*Power(t2,3) + 
               (187 + 68*s - 7*s1)*Power(t2,4) + 
               (178 + 71*s - 28*s1)*Power(t2,5) + 
               4*(19 + 6*s - 6*s1)*Power(t2,6) - 4*Power(t2,7)) + 
            Power(t1,2)*(-6 + (105 - 36*s + 54*s1)*t2 + 
               (350 + 84*Power(s,2) - 15*s1 - 12*Power(s1,2) - 
                  s*(7 + 72*s1))*Power(t2,2) + 
               (240 - 8*Power(s,3) - 266*s1 - 55*Power(s1,2) + 
                  6*Power(s1,3) - Power(s,2)*(281 + 30*s1) + 
                  2*s*(284 + 129*s1 + 6*Power(s1,2)))*Power(t2,3) + 
               (-544 + 21*Power(s,3) - 67*s1 + 63*Power(s1,2) - 
                  5*Power(s1,3) + 5*Power(s,2)*(-29 + 9*s1) + 
                  s*(-367 + 106*s1 - 11*Power(s1,2)))*Power(t2,4) + 
               (-1682 + 12*Power(s,3) + Power(s,2)*(369 - 4*s1) + 
                  282*s1 + 23*Power(s1,2) - 2*Power(s1,3) - 
                  2*s*(374 + 60*s1 + 9*Power(s1,2)))*Power(t2,5) + 
               (-1775 + 192*Power(s,2) + 583*s1 - 2*Power(s1,2) - 
                  20*s*(8 + 9*s1))*Power(t2,6) + 
               (1 - 129*s + 58*s1)*Power(t2,7) + 6*Power(t2,8)) + 
            Power(t1,7)*(-2*Power(s,3) + 2*Power(s1,3) + 
               2*Power(s1,2)*(4 + t2) + 2*Power(s,2)*(5 + 3*s1 + t2) - 
               3*(24 + 30*t2 + 5*Power(t2,2)) - 
               s1*(50 + 70*t2 + 9*Power(t2,2)) + 
               s*(76 - 6*Power(s1,2) + 80*t2 + 9*Power(t2,2) - 
                  2*s1*(9 + 2*t2))) + 
            Power(t1,6)*(41 + 690*t2 + 676*Power(t2,2) + 
               144*Power(t2,3) + 9*Power(t2,4) + 
               Power(s,3)*(6 + 25*t2) - 2*Power(s1,3)*(68 + 27*t2) + 
               Power(s1,2)*(1 + 62*t2 + 47*Power(t2,2)) + 
               s1*(291 + 776*t2 + 551*Power(t2,2) + 82*Power(t2,3)) + 
               Power(s,2)*(148 + 174*t2 + 47*Power(t2,2) - 
                  s1*(93 + 104*t2)) + 
               s*(-579 - 1598*t2 - 879*Power(t2,2) - 98*Power(t2,3) + 
                  Power(s1,2)*(223 + 133*t2) - 
                  2*s1*(50 + 92*t2 + 47*Power(t2,2)))) + 
            Power(t1,5)*(523 + 2590*t2 + 646*Power(t2,2) - 
               1309*Power(t2,3) - 361*Power(t2,4) - 33*Power(t2,5) + 
               Power(s,3)*(20 + 119*t2 + 27*Power(t2,2)) + 
               4*Power(s1,3)*(-8 + 92*t2 + 41*Power(t2,2)) - 
               Power(s1,2)*(330 + 193*t2 + 212*Power(t2,2) + 
                  181*Power(t2,3)) - 
               2*s1*(119 + 952*t2 + 1314*Power(t2,2) + 
                  538*Power(t2,3) + 81*Power(t2,4)) - 
               Power(s,2)*(176 + 1381*t2 + 1566*Power(t2,2) + 
                  333*Power(t2,3) + s1*(101 + 238*t2 - 49*Power(t2,2))) \
+ s*(54 + 1922*t2 + 4970*Power(t2,2) + 2448*Power(t2,3) + 
                  294*Power(t2,4) + 
                  Power(s1,2)*(261 - 145*t2 - 240*Power(t2,2)) + 
                  2*s1*(234 + 855*t2 + 738*Power(t2,2) + 
                     233*Power(t2,3)))) + 
            Power(t1,4)*(-109 - 1241*t2 - 7188*Power(t2,2) - 
               7058*Power(t2,3) - 95*Power(t2,4) + 515*Power(t2,5) + 
               43*Power(t2,6) - 
               Power(s,3)*t2*(18 + 279*t2 + 161*Power(t2,2)) + 
               Power(s1,3)*(126 + 32*t2 - 221*Power(t2,2) - 
                  132*Power(t2,3)) + 
               Power(s1,2)*(-83 + 854*t2 + 631*Power(t2,2) + 
                  152*Power(t2,3) + 93*Power(t2,4)) + 
               s1*(-247 - 698*t2 + 1884*Power(t2,2) + 
                  4292*Power(t2,3) + 1400*Power(t2,4) + 126*Power(t2,5)\
) + Power(s,2)*(-16 + 213*t2 + 1529*Power(t2,2) + 2347*Power(t2,3) + 
                  685*Power(t2,4) + 
                  2*s1*(11 + 50*t2 + 196*Power(t2,2) + 157*Power(t2,3))\
) - s*(-83 - 484*t2 - 534*Power(t2,2) + 4118*Power(t2,3) + 
                  3096*Power(t2,4) + 410*Power(t2,5) + 
                  Power(s1,2)*
                   (-40 + 88*t2 + 289*Power(t2,2) + 66*Power(t2,3)) + 
                  s1*(-70 + 348*t2 + 2018*Power(t2,2) + 
                     2544*Power(t2,3) + 650*Power(t2,4)))) + 
            Power(t1,3)*(-29 - 230*t2 + 347*Power(t2,2) + 
               3180*Power(t2,3) + 7676*Power(t2,4) + 2043*Power(t2,5) - 
               233*Power(t2,6) - 33*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(-126 - 11*t2 + 131*Power(t2,2)) + 
               Power(s1,3)*t2*
                (-32 - 7*t2 + 54*Power(t2,2) + 33*Power(t2,3)) + 
               Power(s1,2)*(78 + 181*t2 - 360*Power(t2,2) - 
                  460*Power(t2,3) - 118*Power(t2,4) + 7*Power(t2,5)) + 
               s1*(-52 + 398*t2 + 817*Power(t2,2) + 206*Power(t2,3) - 
                  2464*Power(t2,4) - 1432*Power(t2,5) - 52*Power(t2,6)) \
- Power(s,2)*t2*(-260 - 590*t2 + 853*Power(t2,2) + 1522*Power(t2,3) + 
                  521*Power(t2,4) + 
                  s1*(-36 + 19*t2 + 126*Power(t2,2) + 169*Power(t2,3))) \
+ s*(-28 - 330*t2 - 7*Power(t2,2) + 810*Power(t2,3) + 232*Power(t2,4) + 
                  1556*Power(t2,5) + 330*Power(t2,6) + 
                  Power(s1,2)*t2*
                   (-54 - 75*t2 + 110*Power(t2,2) + 125*Power(t2,3)) + 
                  2*s1*(6 - 123*t2 - 240*Power(t2,2) + 150*Power(t2,3) + 
                     700*Power(t2,4) + 272*Power(t2,5))))) + 
         s2*Power(t1,3)*(-24*(-1 + s - s1)*Power(t1,7) - 
            Power(t2,4)*(Power(s,3)*t2*(-22 + 21*t2 - 5*Power(t2,2)) + 
               Power(s,2)*(6 + 33*t2 - 78*Power(t2,2) + 
                  39*Power(t2,3)) + 
               2*(-11 + 15*t2 + 12*Power(t2,2) - 17*Power(t2,3) + 
                  Power(t2,4)) + 
               2*s*(5 - 10*t2 + 13*Power(t2,2) - 13*Power(t2,3) + 
                  5*Power(t2,4))) + 
            t1*Power(t2,2)*(-158 + (157 - 18*s1)*t2 + 
               (443 + 39*s1)*Power(t2,2) - 
               2*(137 + 50*s1)*Power(t2,3) + 
               (-101 + 59*s1)*Power(t2,4) + 
               7*(-17 + 4*s1)*Power(t2,5) - 8*(-4 + s1)*Power(t2,6) - 
               Power(s,3)*t2*
                (224 - 212*t2 - 22*Power(t2,2) + 32*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s,2)*(48 + 312*t2 + (-669 + 8*s1)*Power(t2,2) + 
                  (235 - 44*s1)*Power(t2,3) + 
                  (138 + 13*s1)*Power(t2,4) + (3 + 2*s1)*Power(t2,5) + 
                  2*Power(t2,6)) - 
               s*(-80 + 60*t2 + (27 + 44*s1)*Power(t2,2) - 
                  18*(-11 + 8*s1)*Power(t2,3) + 
                  (-109 + 72*s1)*Power(t2,4) + 
                  (-94 + 28*s1)*Power(t2,5) + 6*Power(t2,6))) + 
            Power(t1,2)*(208 + 20*(-8 + 9*s1)*t2 - 
               (1649 + 216*s1 + 18*Power(s1,2))*Power(t2,2) + 
               (53 + 598*s1 - 15*Power(s1,2))*Power(t2,3) + 
               19*(51 - 23*s1 + 4*Power(s1,2))*Power(t2,4) + 
               (1372 - 272*s1 - 39*Power(s1,2))*Power(t2,5) + 
               (-186 + 31*s1 - 4*Power(s1,2))*Power(t2,6) + 
               (-33 + 6*s1)*Power(t2,7) + 
               Power(s,3)*t2*
                (544 - 560*t2 - 422*Power(t2,2) + 212*Power(t2,3) + 
                  45*Power(t2,4) + 7*Power(t2,5)) - 
               Power(s,2)*(96 + 720*t2 + 
                  2*(-711 + 32*s1)*Power(t2,2) + 
                  (115 - 308*s1)*Power(t2,3) + 
                  (946 - 7*s1)*Power(t2,4) + 
                  (15 + 98*s1)*Power(t2,5) + 
                  (82 + 11*s1)*Power(t2,6) + 4*Power(t2,7)) + 
               s*(-160 - 188*t2 + (818 + 316*s1)*Power(t2,2) + 
                  2*(542 - 543*s1 + 9*Power(s1,2))*Power(t2,3) + 
                  (-455 + 280*s1 - 39*Power(s1,2))*Power(t2,4) + 
                  (-1128 + 366*s1 + 37*Power(s1,2))*Power(t2,5) + 
                  (77 + 56*s1 + 4*Power(s1,2))*Power(t2,6) + 
                  4*s1*Power(t2,7))) - 
            Power(t1,6)*(112 + 16*Power(s,3) - 16*Power(s1,3) + 20*t2 - 
               5*Power(t2,2) + 8*Power(s1,2)*(4 + 9*t2) + 
               Power(s,2)*(80 - 48*s1 + 72*t2) + 
               s1*(324 + 284*t2 + 23*Power(t2,2)) + 
               s*(-460 + 48*Power(s1,2) - 308*t2 - 23*Power(t2,2) - 
                  16*s1*(7 + 9*t2))) + 
            Power(t1,5)*(-1928 - 1708*t2 + 462*Power(t2,2) + 
               18*Power(t2,3) + 5*Power(t2,4) + 
               Power(s1,3)*(44 - 60*t2 + 7*Power(t2,2)) - 
               Power(s,3)*(116 + 140*t2 + 7*Power(t2,2)) + 
               Power(s1,2)*(-24 + 116*t2 + 135*Power(t2,2) + 
                  34*Power(t2,3)) + 
               s1*(540 + 672*t2 + 235*Power(t2,2) + 122*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s,2)*(664 + 948*t2 + 147*Power(t2,2) + 
                  34*Power(t2,3) + s1*(388 + 220*t2 + 21*Power(t2,2))) \
- s*(-284 + 288*t2 + 449*Power(t2,2) + 138*Power(t2,3) - 
                  2*Power(t2,4) + 
                  Power(s1,2)*(316 + 20*t2 + 21*Power(t2,2)) + 
                  2*s1*(440 + 516*t2 + 141*Power(t2,2) + 34*Power(t2,3))\
)) + Power(t1,4)*(-304 + 1836*t2 + 4480*Power(t2,2) + 53*Power(t2,3) - 
               181*Power(t2,4) - 5*Power(t2,5) + 
               Power(s1,3)*(-28 + 120*t2 - 17*Power(t2,2) + 
                  2*Power(t2,3) - 5*Power(t2,4)) + 
               Power(s,3)*(68 + 184*t2 + 239*Power(t2,2) + 
                  82*Power(t2,3) + 5*Power(t2,4)) - 
               2*Power(s1,2)*
                (44 + 366*t2 - 33*Power(t2,2) + 22*Power(t2,3) + 
                  21*Power(t2,4)) - 
               s1*(-796 - 848*t2 + 1307*Power(t2,2) + 
                  362*Power(t2,3) + 62*Power(t2,4) + 14*Power(t2,5)) - 
               Power(s,2)*(-600 - 868*t2 + 1206*Power(t2,2) + 
                  540*Power(t2,3) + 44*Power(t2,4) + 
                  s1*(-268 + 408*t2 + 551*Power(t2,2) + 
                     162*Power(t2,3) + 15*Power(t2,4))) + 
               s*(-1412 - 2912*t2 - 159*Power(t2,2) + 384*Power(t2,3) + 
                  132*Power(t2,4) + 16*Power(t2,5) + 
                  Power(s1,2)*
                   (-372 + 168*t2 + 329*Power(t2,2) + 78*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  2*s1*(-176 + 252*t2 + 542*Power(t2,2) + 
                     280*Power(t2,3) + 43*Power(t2,4)))) + 
            Power(t1,3)*(1152 + 1908*t2 - 2052*Power(t2,2) - 
               4041*Power(t2,3) - 774*Power(t2,4) + 233*Power(t2,5) + 
               17*Power(t2,6) + 
               Power(s1,3)*t2*
                (36 - 14*t2 - 42*Power(t2,2) + 13*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s,3)*(-192 - 980*t2 + 344*Power(t2,2) + 
                  188*Power(t2,3) + 93*Power(t2,4) + 11*Power(t2,5)) + 
               Power(s1,2)*(-144 + 144*t2 - 215*Power(t2,2) + 
                  281*Power(t2,3) + 16*Power(t2,4) + Power(t2,5) + 
                  2*Power(t2,6)) + 
               s1*(180 - 936*t2 + 719*Power(t2,2) + 404*Power(t2,3) + 
                  302*Power(t2,4) + 22*Power(t2,5) + 6*Power(t2,6)) + 
               Power(s,2)*(-192 - 768*t2 + 1533*Power(t2,2) - 
                  275*Power(t2,3) + 575*Power(t2,4) + 89*Power(t2,5) + 
                  2*Power(t2,6) + 
                  2*s1*(64 - 210*t2 - 171*Power(t2,2) + 
                     203*Power(t2,3) + 97*Power(t2,4) + 12*Power(t2,5))) \
- s*(1164 + 2648*t2 - 935*Power(t2,2) - 3770*Power(t2,3) + 
                  196*Power(t2,4) + 76*Power(t2,5) + 12*Power(t2,6) + 
                  3*Power(s1,2)*t2*
                   (60 - 100*t2 + 48*Power(t2,2) + 38*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  2*s1*(64 - 960*t2 + 27*Power(t2,2) + 507*Power(t2,3) + 
                     237*Power(t2,4) + 43*Power(t2,5) + 2*Power(t2,6))))) \
+ Power(s2,2)*Power(t1,2)*(-(Power(t1,7)*
               (110 + 8*Power(s,2) + 8*Power(s1,2) + 67*t2 + 
                 s1*(124 + 65*t2) - s*(144 + 16*s1 + 65*t2))) + 
            Power(t2,4)*(-19 + 30*t2 - 7*Power(t2,2) + 
               3*Power(s,3)*(-2 + t2)*Power(t2,2) + 25*Power(t2,3) - 
               30*Power(t2,4) + Power(t2,5) + 
               3*Power(s,2)*t2*
                (6 - 7*t2 - 3*Power(t2,2) + 4*Power(t2,3)) + 
               s*(6 - 37*t2 + 65*Power(t2,2) - 32*Power(t2,3) - 
                  7*Power(t2,4) + 5*Power(t2,5))) + 
            t1*Power(t2,2)*(134 + (-137 + 18*s1)*t2 - 
               (133 + 67*s1)*Power(t2,2) + 
               (-459 + 145*s1)*Power(t2,3) + 
               (409 - 34*s1)*Power(t2,4) + (193 - 53*s1)*Power(t2,5) + 
               (48 - 13*s1)*Power(t2,6) + (-15 + 4*s1)*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*
                (136 - 143*t2 + 18*Power(t2,2) + 5*Power(t2,3)) - 
               Power(s,2)*t2*
                (168 + (-11 + 6*s1)*t2 - 3*(162 + 11*s1)*Power(t2,2) + 
                  (322 + 6*s1)*Power(t2,3) + 72*Power(t2,4) + 
                  4*Power(t2,5)) + 
               s*(-48 + 220*t2 + 4*(-95 + 17*s1)*Power(t2,2) - 
                  4*(-41 + 39*s1)*Power(t2,3) + 
                  (227 + 36*s1)*Power(t2,4) + 
                  5*(-27 + 8*s1)*Power(t2,5) + 
                  (-37 + 12*s1)*Power(t2,6) + Power(t2,7))) + 
            Power(t1,2)*(-160 - 4*(-44 + 45*s1)*t2 + 
               (659 + 320*s1 + 18*Power(s1,2))*Power(t2,2) + 
               (2502 - 788*s1 + 51*Power(s1,2))*Power(t2,3) + 
               (-1149 + 25*s1 - 138*Power(s1,2))*Power(t2,4) + 
               (-2278 + 564*s1 + 57*Power(s1,2))*Power(t2,5) + 
               4*(-293 + 78*s1 + 3*Power(s1,2))*Power(t2,6) - 
               4*(-46 + 7*s1)*Power(t2,7) + (18 - 5*s1)*Power(t2,8) + 
               Power(s,3)*Power(t2,2)*
                (-520 + 742*t2 + 164*Power(t2,2) - 175*Power(t2,3) - 
                  18*Power(t2,4)) + 
               Power(s,2)*t2*
                (384 + (410 + 48*s1)*t2 - 
                  (2199 + 196*s1)*Power(t2,2) + 
                  (986 - 166*s1)*Power(t2,3) + 
                  3*(331 + 38*s1)*Power(t2,4) + 
                  (135 + 32*s1)*Power(t2,5) + 41*Power(t2,6)) + 
               s*(96 - 180*t2 + (594 - 508*s1)*Power(t2,2) - 
                  2*(501 - 491*s1 + 9*Power(s1,2))*Power(t2,3) + 
                  (-1349 + 536*s1 + 51*Power(s1,2))*Power(t2,4) + 
                  (1020 - 566*s1 - 51*Power(s1,2))*Power(t2,5) - 
                  2*(-349 + 158*s1 + 6*Power(s1,2))*Power(t2,6) - 
                  (61 + 32*s1)*Power(t2,7) + 2*Power(t2,8))) + 
            Power(t1,6)*(-64 + 448*t2 + 38*Power(t2,2) + 
               23*Power(t2,3) + Power(s,3)*(-28 + 27*t2) - 
               Power(s1,3)*(80 + 27*t2) + 
               Power(s1,2)*(18 + 227*t2 + 90*Power(t2,2)) + 
               s1*(640 + 1051*t2 + 394*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s,2)*(418 + 311*t2 + 90*Power(t2,2) - 
                  3*s1*(8 + 27*t2)) + 
               s*(-1012 - 1573*t2 - 460*Power(t2,2) - 22*Power(t2,3) + 
                  3*Power(s1,2)*(44 + 27*t2) - 
                  2*s1*(194 + 269*t2 + 90*Power(t2,2)))) + 
            Power(t1,5)*(2328 + 6424*t2 + 1391*Power(t2,2) - 
               675*Power(t2,3) - 31*Power(t2,4) - 7*Power(t2,5) + 
               Power(s1,3)*(-180 + 199*t2 + 44*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(s,3)*(72 + 379*t2 + 146*Power(t2,2) + 
                  3*Power(t2,3)) - 
               2*Power(s1,2)*
                (50 + 136*t2 + 176*Power(t2,2) + 97*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s1*(-356 - 2287*t2 - 1456*Power(t2,2) - 
                  544*Power(t2,3) - 100*Power(t2,4) + Power(t2,5)) + 
               s*(-984 + 721*t2 + 1964*Power(t2,2) + 938*Power(t2,3) + 
                  120*Power(t2,4) - Power(t2,5) + 
                  Power(s1,2)*
                   (560 + 229*t2 + 58*Power(t2,2) + 9*Power(t2,3)) + 
                  4*s1*(212 + 782*t2 + 416*Power(t2,2) + 
                     108*Power(t2,3) + 9*Power(t2,4))) - 
               Power(s,2)*(s1*
                   (340 + 807*t2 + 248*Power(t2,2) + 9*Power(t2,3)) + 
                  2*(126 + 1328*t2 + 746*Power(t2,2) + 
                     119*Power(t2,3) + 9*Power(t2,4)))) + 
            Power(t1,4)*(424 - 1616*t2 - 8839*Power(t2,2) - 
               6317*Power(t2,3) + 407*Power(t2,4) + 181*Power(t2,5) + 
               4*Power(t2,6) + 
               Power(s1,3)*(84 - 208*t2 - 14*Power(t2,2) - 
                  39*Power(t2,3) + 12*Power(t2,4)) - 
               Power(s,3)*(-196 + 418*t2 + 418*Power(t2,2) + 
                  341*Power(t2,3) + 50*Power(t2,4)) + 
               Power(s1,2)*(-294 + 1613*t2 + 471*Power(t2,2) + 
                  67*Power(t2,3) + 121*Power(t2,4) + 17*Power(t2,5)) + 
               s1*(-884 - 2673*t2 + 1206*Power(t2,2) + 
                  2761*Power(t2,3) + 416*Power(t2,4) + 
                  81*Power(t2,5) + 7*Power(t2,6)) + 
               Power(s,2)*(-510 - 759*t2 + 571*Power(t2,2) + 
                  2517*Power(t2,3) + 543*Power(t2,4) + 
                  23*Power(t2,5) + 
                  s1*(-292 + 204*t2 + 994*Power(t2,2) + 
                     697*Power(t2,3) + 112*Power(t2,4))) - 
               s*(-504 - 4835*t2 - 3842*Power(t2,2) + 
                  1433*Power(t2,3) + 812*Power(t2,4) + 
                  155*Power(t2,5) + 8*Power(t2,6) + 
                  Power(s1,2)*
                   (-540 - 102*t2 + 690*Power(t2,2) + 
                     317*Power(t2,3) + 74*Power(t2,4)) + 
                  2*s1*(-394 + 147*t2 + 957*Power(t2,2) + 
                     1243*Power(t2,3) + 298*Power(t2,4) + 20*Power(t2,5)\
))) + Power(t1,3)*(-562 - 3397*t2 - 583*Power(t2,2) + 6432*Power(t2,3) + 
               6305*Power(t2,4) + 523*Power(t2,5) - 246*Power(t2,6) - 
               10*Power(t2,7) - 
               2*Power(s1,3)*t2*
                (18 + 15*t2 - 45*Power(t2,2) + 5*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s,3)*(96 - 708*t2 - 972*Power(t2,2) + 
                  628*Power(t2,3) + 214*Power(t2,4) + 60*Power(t2,5)) - 
               Power(s1,2)*(-144 + 96*t2 - 417*Power(t2,2) + 
                  452*Power(t2,3) + 165*Power(t2,4) + 14*Power(t2,5) + 
                  4*Power(t2,6)) - 
               s1*(604 - 1258*t2 - 276*Power(t2,2) + 757*Power(t2,3) + 
                  1408*Power(t2,4) + 471*Power(t2,5) - 9*Power(t2,6) + 
                  3*Power(t2,7)) - 
               Power(s,2)*(64 - 1608*t2 - 225*Power(t2,2) + 
                  2408*Power(t2,3) + 660*Power(t2,4) + 
                  648*Power(t2,5) + 42*Power(t2,6) + 
                  s1*(96 - 148*t2 - 726*Power(t2,2) + 280*Power(t2,3) + 
                     462*Power(t2,4) + 119*Power(t2,5))) + 
               s*(340 + 2420*t2 + 3032*Power(t2,2) - 3425*Power(t2,3) - 
                  3309*Power(t2,4) + 577*Power(t2,5) + 68*Power(t2,6) + 
                  6*Power(t2,7) + 
                  Power(s1,2)*t2*
                   (180 - 276*t2 + 42*Power(t2,2) + 263*Power(t2,3) + 
                     63*Power(t2,4)) + 
                  2*s1*(256 - 896*t2 - 1211*Power(t2,2) + 
                     612*Power(t2,3) + 816*Power(t2,4) + 
                     293*Power(t2,5) + 20*Power(t2,6))))) + 
         Power(s2,3)*t1*(9*(-1 + s - s1)*Power(t1,8) + 
            (-1 + t2)*Power(t2,4)*
             (-6 - (1 + 6*s)*t2 + (5 + s)*Power(t2,2) + 
               (11 + 6*s)*Power(t2,3) + (7 + s)*Power(t2,4)) + 
            t1*Power(t2,2)*(-42 - 3*(11 + 4*s + 2*s1)*t2 + 
               (192 + 84*Power(s,2) + 37*s1 - s*(109 + 24*s1))*
                Power(t2,2) + 
               (102 - 20*Power(s,3) - 70*s1 - 
                  Power(s,2)*(119 + 6*s1) + s*(338 + 40*s1))*Power(t2,3) \
+ (103 + 15*Power(s,3) + Power(s,2)*(-39 + s1) + 8*s*(-23 + s1) - 9*s1)*
                Power(t2,4) + 
               (-265 + 2*Power(s,3) + Power(s,2)*(83 - 2*s1) + 22*s1 - 
                  3*s*(25 + 4*s1))*Power(t2,5) + 
               (-99 + 14*Power(s,2) + s*(33 - 12*s1) + 26*s1)*
                Power(t2,6) + (2 + s)*Power(t2,7)) + 
            Power(t1,2)*(48 + (80 - 84*s + 60*s1)*t2 - 
               (589 + 402*Power(s,2) + 104*s1 + 6*Power(s1,2) - 
                  2*s*(181 + 90*s1))*Power(t2,2) + 
               (-904 + 202*Power(s,3) + 214*s1 - 53*Power(s1,2) + 
                  Power(s,2)*(317 + 12*s1) + 
                  2*s*(-515 + 3*s1 + 3*Power(s1,2)))*Power(t2,3) + 
               (-337*Power(s,3) + 3*Power(s,2)*(364 + 45*s1) + 
                  s*(821 - 832*s1 - 29*Power(s1,2)) + 
                  6*(-212 + 77*s1 + 18*Power(s1,2)))*Power(t2,4) + 
               (1770 + 14*Power(s,3) - 256*s1 - 37*Power(s1,2) - 
                  2*Power(s,2)*(454 + 25*s1) + 
                  s*(787 + 162*s1 + 31*Power(s1,2)))*Power(t2,5) + 
               (1913 + 33*Power(s,3) - 464*s1 - 12*Power(s1,2) - 
                  15*Power(s,2)*(32 + s1) + 
                  4*s*(-126 + 80*s1 + 3*Power(s1,2)))*Power(t2,6) + 
               (357 - 63*Power(s,2) - 150*s1 + 4*s*(-24 + 25*s1))*
                Power(t2,7) + 2*(-31 + 6*s + 9*s1)*Power(t2,8) - 
               Power(t2,9)) + 
            Power(t1,7)*(226 + 15*Power(s,3) - 15*Power(s1,3) + 302*t2 + 
               71*Power(t2,2) + Power(s1,2)*(21 + 22*t2) + 
               Power(s,2)*(25 - 45*s1 + 22*t2) + 
               s1*(269 + 330*t2 + 58*Power(t2,2)) + 
               s*(-407 + 45*Power(s1,2) - 386*t2 - 58*Power(t2,2) - 
                  2*s1*(23 + 22*t2))) - 
            Power(t1,6)*(-712 + 249*t2 + 775*Power(t2,2) + 
               113*Power(t2,3) + 29*Power(t2,4) + 
               Power(s,3)*(-109 + 2*t2 + 11*Power(t2,2)) - 
               Power(s1,3)*(141 + 158*t2 + 11*Power(t2,2)) + 
               Power(s1,2)*(90 + 248*t2 + 326*Power(t2,2) + 
                  34*Power(t2,3)) + 
               s1*(817 + 1970*t2 + 1304*Power(t2,2) + 256*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s,2)*(818 + 1080*t2 + 374*Power(t2,2) + 
                  34*Power(t2,3) + s1*(229 - 162*t2 - 33*Power(t2,2))) + 
               s*(-1091 - 3412*t2 - 2072*Power(t2,2) - 316*Power(t2,3) - 
                  7*Power(t2,4) + 
                  3*Power(s1,2)*(7 + 106*t2 + 11*Power(t2,2)) - 
                  4*s1*(225 + 294*t2 + 175*Power(t2,2) + 17*Power(t2,3)))\
) + Power(t1,5)*(-1356 - 8327*t2 - 7863*Power(t2,2) + 261*Power(t2,3) + 
               519*Power(t2,4) + 32*Power(t2,5) + 3*Power(t2,6) - 
               Power(s1,3)*(-302 + 234*t2 + 217*Power(t2,2) + 
                  16*Power(t2,3)) - 
               Power(s,3)*(88 + 392*t2 + 413*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(s1,2)*(243 + 679*t2 + 499*Power(t2,2) + 
                  373*Power(t2,3) + 91*Power(t2,4)) + 
               s1*(-375 + 2328*t2 + 4223*Power(t2,2) + 
                  1662*Power(t2,3) + 411*Power(t2,4) + 26*Power(t2,5)) + 
               Power(s,2)*(-97 + 2035*t2 + 3921*Power(t2,2) + 
                  1181*Power(t2,3) + 107*Power(t2,4) + 
                  s1*(54 + 942*t2 + 799*Power(t2,2) + 84*Power(t2,3))) - 
               s*(-1217 - 962*t2 + 4249*Power(t2,2) + 3114*Power(t2,3) + 
                  717*Power(t2,4) + 34*Power(t2,5) + 
                  Power(s1,2)*
                   (412 + 540*t2 + 169*Power(t2,2) + 18*Power(t2,3)) + 
                  2*s1*(65 + 1805*t2 + 2069*Power(t2,2) + 
                     693*Power(t2,3) + 99*Power(t2,4)))) + 
            Power(t1,4)*(-419 + 75*t2 + 7948*Power(t2,2) + 
               13146*Power(t2,3) + 3663*Power(t2,4) - 580*Power(t2,5) - 
               75*Power(t2,6) - Power(t2,7) + 
               4*Power(s1,3)*
                (-21 + 26*t2 + 22*Power(t2,2) + 22*Power(t2,3) + 
                  Power(t2,4)) + 
               2*Power(s,3)*(-66 - 185*t2 + 344*Power(t2,2) + 
                  242*Power(t2,3) + 78*Power(t2,4)) - 
               Power(s1,2)*(-610 + 1285*t2 + 1338*Power(t2,2) + 
                  312*Power(t2,3) + 152*Power(t2,4) + 43*Power(t2,5)) + 
               s1*(440 + 2834*t2 + 1375*Power(t2,2) - 
                  4568*Power(t2,3) - 2326*Power(t2,4) - 
                  218*Power(t2,5) - 32*Power(t2,6)) - 
               Power(s,2)*(-418 - 863*t2 + 1158*Power(t2,2) + 
                  2780*Power(t2,3) + 1950*Power(t2,4) + 
                  177*Power(t2,5) + 
                  2*s1*(-66 - 58*t2 + 317*Power(t2,2) + 
                     516*Power(t2,3) + 162*Power(t2,4))) + 
               s*(-2 - 1556*t2 - 5603*Power(t2,2) - 898*Power(t2,3) + 
                  2682*Power(t2,4) + 636*Power(t2,5) + 58*Power(t2,6) + 
                  2*Power(s1,2)*
                   (-174 - 197*t2 + 245*Power(t2,2) + 282*Power(t2,3) + 
                     82*Power(t2,4)) + 
                  2*s1*(-302 - 389*t2 + 586*Power(t2,2) + 
                     1954*Power(t2,3) + 949*Power(t2,4) + 96*Power(t2,5))\
)) + Power(t1,3)*(158 + 1543*t2 + 3058*Power(t2,2) - 2661*Power(t2,3) - 
               8579*Power(t2,4) - 4491*Power(t2,5) + 46*Power(t2,6) + 
               95*Power(t2,7) + Power(t2,8) + 
               2*Power(s1,3)*t2*
                (6 + 27*t2 - 34*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s,3)*t2*(-196 + 816*t2 + 328*Power(t2,2) - 
                  417*Power(t2,3) - 94*Power(t2,4)) + 
               Power(s1,2)*(-48 - 16*t2 - 361*Power(t2,2) + 
                  230*Power(t2,3) + 309*Power(t2,4) + 56*Power(t2,5)) + 
               Power(s,2)*(96 + 12*(-18 + 7*s1)*t2 - 
                  (2383 + 386*s1)*Power(t2,2) - 
                  4*(-255 + 29*s1)*Power(t2,3) + 
                  (2405 + 371*s1)*Power(t2,4) + 
                  6*(154 + 31*s1)*Power(t2,5) + 207*Power(t2,6)) + 
               s1*(412 - 150*t2 - 1232*Power(t2,2) - 102*Power(t2,3) + 
                  1403*Power(t2,4) + 1738*Power(t2,5) + 
                  149*Power(t2,6) - 8*Power(t2,7)) - 
               s*(148 + 140*t2 + 2008*Power(t2,2) + 1666*Power(t2,3) - 
                  2736*Power(t2,4) - 691*Power(t2,5) + 429*Power(t2,6) + 
                  22*Power(t2,7) + 
                  Power(s1,2)*t2*
                   (60 - 68*t2 - 108*Power(t2,2) + 255*Power(t2,3) + 
                     99*Power(t2,4)) + 
                  2*s1*(96 + 32*t2 - 1367*Power(t2,2) - 362*Power(t2,3) + 
                     784*Power(t2,4) + 669*Power(t2,5) + 88*Power(t2,6))))\
))*R1q(t1))/((-1 + s1)*(-1 + t1)*t1*Power(-s2 + t1,3)*(-s + s1 - t2)*
       (1 - s2 + t1 - t2)*Power(t1 - s2*t2,2)*
       Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2),2)) - 
    (8*(-2*(-2 + t2)*Power(t2,3)*Power(1 + (-1 + s)*t2,3) - 
         t1*t2*(1 + (-1 + s)*t2)*
          (28 + (-68 + 56*s)*t2 + 
            (47 + 28*Power(s,2) - 10*s1 + 6*s*(-15 + 2*s1))*
             Power(t2,2) - 2*(9 + 5*Power(s,2) - 9*s1 + s*(-3 + 8*s1))*
             Power(t2,3) + (13 - 5*Power(s,2) - 8*s1 + 8*s*s1)*
             Power(t2,4) + 2*(-1 + s)*Power(t2,5)) - 
         2*Power(s2,8)*(Power(s1,3) + 2*s1*Power(t2,2) + 
            Power(t2,2)*(-2 + 3*s + t2)) + 
         8*Power(t1,6)*(2 + 2*Power(s,2) + 2*Power(s1,2) - t2 + 
            s1*(10 + t2) - s*(10 + 4*s1 + t2)) + 
         Power(t1,2)*(2*Power(s1,3)*(2 - 3*t2)*Power(t2,2) + 
            Power(s,3)*Power(t2,2)*
             (40 + 6*t2 - 35*Power(t2,2) - 7*Power(t2,3)) + 
            Power(s1,2)*t2*(-36 + 40*t2 + 29*Power(t2,2) - 
               17*Power(t2,3) + 4*Power(t2,4)) + 
            Power(s,2)*t2*(108 + 4*(-46 + 21*s1)*t2 - 
               (139 + 114*s1)*Power(t2,2) + (-9 + 42*s1)*Power(t2,3) + 
               (22 + 17*s1)*Power(t2,4) + 4*Power(t2,5)) + 
            s1*(72 - 124*t2 + 114*Power(t2,2) - 163*Power(t2,3) + 
               67*Power(t2,4) + 2*Power(t2,5) - 2*Power(t2,6)) + 
            2*(-56 + 80*t2 - 24*Power(t2,2) + 71*Power(t2,3) - 
               65*Power(t2,4) - 4*Power(t2,5) + 6*Power(t2,6)) + 
            s*(72 + 4*(-73 + 30*s1)*t2 + (142 - 320*s1)*Power(t2,2) + 
               (31 + 246*s1 + 2*Power(s1,2))*Power(t2,3) + 
               (158 - 74*s1 + 17*Power(s1,2))*Power(t2,4) - 
               (21 + 7*s1 + 10*Power(s1,2))*Power(t2,5) - 
               2*(1 + 2*s1)*Power(t2,6))) + 
         Power(t1,5)*(368 + 48*Power(s,3) + 16*Power(s1,3) - 116*t2 - 
            Power(t2,3) - 16*Power(s,2)*(15 + 5*s1 + t2 + Power(t2,2)) - 
            16*Power(s1,2)*(-7 + 2*t2 + Power(t2,2)) + 
            s1*(-248 + 36*t2 - 54*Power(t2,2) + Power(t2,3)) + 
            s*(8 + 16*Power(s1,2) - 4*t2 + 58*Power(t2,2) - 
               Power(t2,3) + 16*s1*(10 + 3*t2 + 2*Power(t2,2)))) - 
         Power(t1,4)*(64 + 280*t2 + 48*Power(t2,2) - 54*Power(t2,3) - 
            2*Power(t2,4) + Power(s1,3)*
             (-8 + 36*t2 + 6*Power(t2,2) - 3*Power(t2,3)) + 
            Power(s,3)*(24 + 76*t2 + 22*Power(t2,2) + 3*Power(t2,3)) - 
            Power(s1,2)*(176 + 84*t2 - 108*Power(t2,2) + 
               31*Power(t2,3)) + 
            s1*(120 + 60*t2 - 126*Power(t2,2) + Power(t2,3) - 
               7*Power(t2,4)) - 
            Power(s,2)*(-176 + 84*t2 + 124*Power(t2,2) + 
               27*Power(t2,3) + 
               s1*(-136 + 164*t2 + 38*Power(t2,2) + 9*Power(t2,3))) + 
            s*(-680 - 268*t2 + 62*Power(t2,2) + 19*Power(t2,3) + 
               8*Power(t2,4) + 
               Power(s1,2)*(-152 + 52*t2 + 10*Power(t2,2) + 
                  9*Power(t2,3)) + 
               2*s1*(96 + 60*t2 + 16*Power(t2,2) + 29*Power(t2,3)))) + 
         Power(t1,3)*(-208 - 176*t2 + 272*Power(t2,2) + 
            140*Power(t2,3) - 59*Power(t2,4) - 7*Power(t2,5) + 
            Power(s1,3)*(8 - 12*t2 + 10*Power(t2,2) + 15*Power(t2,3) - 
               4*Power(t2,4)) + 
            Power(s,3)*(8 - 20*t2 + 54*Power(t2,2) + 41*Power(t2,3) + 
               7*Power(t2,4)) + 
            s1*(88 + 236*t2 - 122*Power(t2,2) + 25*Power(t2,3) - 
               14*Power(t2,4) - 2*Power(t2,5)) - 
            2*Power(s1,2)*(-16 + 88*t2 - 6*Power(t2,2) + 
               14*Power(t2,3) - 7*Power(t2,4) + Power(t2,5)) + 
            Power(s,2)*(s1*(-72 + 92*t2 + 30*Power(t2,2) - 
                  91*Power(t2,3) - 18*Power(t2,4)) - 
               2*(-16 - 144*t2 - 74*Power(t2,2) + 34*Power(t2,3) + 
                  19*Power(t2,4) + Power(t2,5))) + 
            s*(88 - 124*t2 - 550*Power(t2,2) - 65*Power(t2,3) + 
               29*Power(t2,4) + 5*Power(t2,5) + 
               Power(s1,2)*(-72 + 100*t2 - 142*Power(t2,2) + 
                  35*Power(t2,3) + 15*Power(t2,4)) + 
               2*s1*(144 - 96*t2 + 56*Power(t2,2) + 20*Power(t2,3) + 
                  13*Power(t2,4) + 2*Power(t2,5)))) + 
         Power(s2,7)*(Power(s1,3)*(2 + 6*t1 - 11*t2) - 
            2*Power(s1,2)*(-3 - (-2 + t1)*t2 + 3*Power(t2,2) + 
               s*(3*t1 + t2)) + 
            s1*t2*((27 - 23*t2)*t2 + t1*(8 + t2) - s*(-6 + 6*t1 + t2)) - 
            t2*(t1*(8 + 8*t2 + Power(t2,2) - 12*s*(1 + t2)) + 
               t2*(10 - 6*Power(s,2) - 39*t2 + 12*Power(t2,2) + 
                  s*(2 + 33*t2)))) - 
         Power(s2,6)*(Power(s1,3)*
             (-4 + 6*Power(t1,2) - 2*t2 + 24*Power(t2,2) - 
               3*t1*(3 + 11*t2)) + 
            Power(t1,2)*(-4 - 22*t2 + 6*Power(s,2)*t2 - 
               19*Power(t2,2) - 3*Power(t2,3) + 
               s*(6 + 24*t2 + 17*Power(t2,2))) + 
            t1*t2*(-20 + 114*t2 + 8*Power(t2,2) + 4*Power(t2,3) + 
               Power(s,2)*(6 + 19*t2) - s*(10 + 162*t2 + 53*Power(t2,2))\
) + t2*(-4*Power(s,2)*t2*(1 + 9*t2) + 
               t2*(39 + 78*t2 - 128*Power(t2,2) + 28*Power(t2,3)) + 
               s*(6 - 11*t2 + 20*Power(t2,2) + 70*Power(t2,3))) + 
            Power(s1,2)*(6 - (41 + 10*s)*t2 + (11 + 9*s)*Power(t2,2) + 
               30*Power(t2,3) + Power(t1,2)*(2 - 12*s + 4*t2) + 
               t1*(14 - 22*t2 - 13*Power(t2,2) + 7*s*(-2 + 5*t2))) + 
            s1*(6 - 8*t2 - 18*Power(t2,2) - 137*Power(t2,3) + 
               58*Power(t2,4) + 2*Power(t1,2)*(2 + t2 - 6*Power(t2,2)) + 
               t1*t2*(58 - 23*t2 - 2*Power(t2,2)) + 
               Power(s,2)*(6*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               s*(-2*Power(t1,2)*(3 + 5*t2) + 
                  2*t1*(-3 + 8*Power(t2,2)) + 
                  t2*(8 - 20*t2 + 11*Power(t2,2))))) + 
         Power(s2,5)*(2 - 4*t2 + 10*s*t2 - 36*Power(t2,2) + 
            36*s*Power(t2,2) - Power(s,2)*Power(t2,2) - 
            6*Power(s,3)*Power(t2,2) - 141*Power(t2,3) + 
            79*s*Power(t2,3) + 15*Power(s,2)*Power(t2,3) - 
            Power(s,3)*Power(t2,3) - 196*Power(t2,4) - 
            64*s*Power(t2,4) + 86*Power(s,2)*Power(t2,4) + 
            202*Power(t2,5) - 70*s*Power(t2,5) - 32*Power(t2,6) + 
            Power(t1,3)*(-12 - 2*Power(s,3) - 35*t2 - 15*Power(t2,2) + 
               2*Power(s,2)*(3 + t2) + s*(12 + 34*t2 + 9*Power(t2,2))) + 
            Power(s1,3)*(2*Power(t1,3) - 3*Power(t1,2)*(13 + 11*t2) - 
               2*t2*(-8 + 6*t2 + 13*Power(t2,2)) + 
               t1*(-22 + 43*t2 + 72*Power(t2,2))) + 
            Power(t1,2)*(-10 + 111*t2 + 4*Power(s,3)*t2 + 
               112*Power(t2,2) + 35*Power(t2,3) + 9*Power(t2,4) + 
               Power(s,2)*(6 + 52*t2 + 5*Power(t2,2)) - 
               s*(8 + 235*t2 + 223*Power(t2,2) + 59*Power(t2,3))) + 
            t1*t2*(80 + 386*t2 + 7*Power(s,3)*t2 - 447*Power(t2,2) + 
               41*Power(t2,3) - 6*Power(t2,4) - 
               Power(s,2)*(28 + 117*t2 + 80*Power(t2,2)) + 
               s*(-20 + 7*t2 + 479*Power(t2,2) + 92*Power(t2,3))) + 
            Power(s1,2)*(Power(t1,3)*(4 - 6*s + 2*t2) - 
               2*(6 - 2*t2 - (57 + 20*s)*Power(t2,2) + 
                  (2 + 8*s)*Power(t2,3) + 30*Power(t2,4)) + 
               Power(t1,2)*(2 - 40*t2 + 5*Power(t2,2) + 
                  s*(23 + 70*t2)) + 
               t1*(-35 - 73*t2 + 111*Power(t2,2) + 32*Power(t2,3) + 
                  s*(-4 + 47*t2 - 76*Power(t2,2)))) + 
            s1*(6 - (49 + 26*s)*t2 + 
               2*(-4 - 27*s + Power(s,2))*Power(t2,2) - 
               2*(-25 - 8*s + Power(s,2))*Power(t2,3) + 
               (278 - 34*s)*Power(t2,4) - 82*Power(t2,5) + 
               Power(t1,3)*(1 + 6*Power(s,2) - 24*t2 - 9*Power(t2,2) - 
                  2*s*(5 + 2*t2)) + 
               Power(t1,2)*(31 + Power(s,2)*(16 - 41*t2) + 31*t2 + 
                  29*Power(t2,2) + 43*Power(t2,3) + 
                  s*(-23 + 40*t2 - 10*Power(t2,2))) + 
               t1*(10 - 56*t2 + Power(s,2)*(14 - 3*t2)*t2 - 
                  469*Power(t2,2) + 37*Power(t2,3) - 2*Power(t2,4) + 
                  4*s*(-4 + 15*t2 + 19*Power(t2,2))))) + 
         Power(s2,4)*(-2 + 19*t2 + 16*s*t2 + 73*Power(t2,2) + 
            61*s*Power(t2,2) - 42*Power(s,2)*Power(t2,2) + 
            4*Power(s,3)*Power(t2,2) - 61*Power(t2,3) + 
            138*s*Power(t2,3) - 4*Power(s,2)*Power(t2,3) - 
            26*Power(s,3)*Power(t2,3) - 210*Power(t2,4) + 
            186*s*Power(t2,4) + 20*Power(s,2)*Power(t2,4) - 
            4*Power(s,3)*Power(t2,4) - 218*Power(t2,5) - 
            96*s*Power(t2,5) + 104*Power(s,2)*Power(t2,5) + 
            168*Power(t2,6) - 30*s*Power(t2,6) - 18*Power(t2,7) + 
            Power(t1,4)*(17 - 2*Power(s,2) + 21*t2 - s*(17 + 18*t2)) - 
            Power(t1,3)*(36 + 152*t2 + 106*Power(t2,2) + 
               37*Power(t2,3) + Power(s,3)*(-4 + 11*t2) + 
               Power(s,2)*(39 + 14*Power(t2,2)) - 
               s*(106 + 291*t2 + 200*Power(t2,2) + 17*Power(t2,3))) + 
            Power(s1,3)*(Power(t1,3)*(43 + 11*t2) + 
               Power(t1,2)*(17 - 145*t2 - 61*Power(t2,2)) - 
               2*Power(t2,2)*(-12 + 14*t2 + 7*Power(t2,2)) + 
               t1*(-28 - 18*t2 + 67*Power(t2,2) + 78*Power(t2,3))) + 
            Power(t1,2)*(-41 - 542*t2 + Power(s,3)*(-18 + t2)*t2 + 
               452*Power(t2,2) + 140*Power(t2,3) + 17*Power(t2,4) + 
               9*Power(t2,5) + 
               Power(s,2)*(6 + 165*t2 + 313*Power(t2,2) + 
                  53*Power(t2,3)) + 
               s*(21 + 38*t2 - 958*Power(t2,2) - 441*Power(t2,3) - 
                  75*Power(t2,4))) + 
            t1*(-2 + 82*t2 + 450*Power(t2,2) + 1050*Power(t2,3) - 
               739*Power(t2,4) + 75*Power(t2,5) - 4*Power(t2,6) + 
               4*Power(s,3)*t2*(3 + 2*t2 + 7*Power(t2,2)) - 
               Power(s,2)*t2*
                (4 + 40*t2 + 389*Power(t2,2) + 130*Power(t2,3)) + 
               s*(2 - 137*t2 - 468*Power(t2,2) + 28*Power(t2,3) + 
                  545*Power(t2,4) + 78*Power(t2,5))) + 
            Power(s1,2)*(-2*Power(t1,4) + 
               2*t2*(-24 + 22*t2 + 6*(13 + 5*s)*Power(t2,2) - 
                  7*(-1 + s)*Power(t2,3) - 30*Power(t2,4)) + 
               Power(t1,3)*(21 + 4*t2 - 14*Power(t2,2) - 
                  s*(82 + 33*t2)) + 
               Power(t1,2)*(102 - 73*t2 - 135*Power(t2,2) + 
                  41*Power(t2,3) + s*(-86 + 89*t2 + 123*Power(t2,2))) + 
               t1*(56 - 265*t2 - 203*Power(t2,2) + 209*Power(t2,3) + 
                  38*Power(t2,4) - 
                  s*(12 + 58*t2 - 57*Power(t2,2) + 74*Power(t2,3)))) + 
            s1*(12 + 2*(-7 + 6*s)*t2 - 3*(55 + 16*s)*Power(t2,2) + 
               8*(-7 - 15*s + Power(s,2))*Power(t2,3) + 
               2*(22 - 8*s + Power(s,2))*Power(t2,4) + 
               (282 - 46*s)*Power(t2,5) - 68*Power(t2,6) + 
               2*Power(t1,4)*(6 + 2*s + 9*t2) + 
               Power(t1,3)*(-31 - 68*t2 - 148*Power(t2,2) - 
                  17*Power(t2,3) + Power(s,2)*(35 + 33*t2) + 
                  4*s*(-3 - t2 + 7*Power(t2,2))) + 
               Power(t1,2)*(20 + 555*t2 + 226*Power(t2,2) + 
                  73*Power(t2,3) + 57*Power(t2,4) + 
                  Power(s,2)*(-19 + 74*t2 - 63*Power(t2,2)) - 
                  s*(56 + 277*t2 + 26*Power(t2,2) + 94*Power(t2,3))) + 
               t1*(43 + 107*t2 - 43*Power(t2,2) - 1072*Power(t2,3) + 
                  85*Power(t2,4) - 8*Power(t2,5) + 
                  Power(s,2)*t2*(-30 + 35*t2 - 32*Power(t2,2)) + 
                  2*s*(7 + 42*t2 + 81*Power(t2,2) + 145*Power(t2,3) + 
                     20*Power(t2,4))))) + 
         Power(s2,3)*(-4 + 9*(-1 + s - s1)*Power(t1,5) + 
            (8 - 12*s + 48*s1)*t2 + 
            (61 + 24*Power(s,2) + 10*s1 - 60*Power(s1,2) + 
               2*s*(-31 + 6*s1))*Power(t2,2) + 
            (131 - 140*Power(s,2) + 16*Power(s,3) - 233*s1 + 
               52*Power(s1,2) + 16*Power(s1,3) + s*(127 + 4*s1))*
             Power(t2,3) + (-1 - 44*Power(s,3) - 52*s1 + 
               12*Power(s,2)*s1 + 104*Power(s1,2) - 22*Power(s1,3) + 
               4*s*(36 - 29*s1 + 10*Power(s1,2)))*Power(t2,4) + 
            (-166 - 6*Power(s,3) + 12*s1 + 16*Power(s1,2) - 
               3*Power(s1,3) + 2*Power(s,2)*(5 + 4*s1) - 
               2*s*(-97 + 17*s1 + 3*Power(s1,2)))*Power(t2,5) + 
            (-108 + 66*Power(s,2) + 143*s1 - 30*Power(s1,2) - 
               s*(74 + 29*s1))*Power(t2,6) - 
            (-71 + s + 31*s1)*Power(t2,7) - 4*Power(t2,8) + 
            Power(t1,4)*(60 + 15*Power(s,3) - 15*Power(s1,3) + 123*t2 + 
               71*Power(t2,2) + Power(s1,2)*(-9 + 22*t2) + 
               Power(s,2)*(-5 - 45*s1 + 22*t2) + 
               s*(-121 + 45*Power(s1,2) + s1*(14 - 44*t2) - 223*t2 - 
                  58*Power(t2,2)) + s1*(37 + 167*t2 + 58*Power(t2,2))) - 
            Power(t1,3)*(-234 + 73*t2 + 375*Power(t2,2) + 
               16*Power(t2,3) + 29*Power(t2,4) + 
               Power(s,3)*(-3 - 25*t2 + 11*Power(t2,2)) - 
               Power(s1,3)*(45 + 131*t2 + 11*Power(t2,2)) + 
               Power(s1,2)*(38 - 183*t2 + 70*Power(t2,2) + 
                  34*Power(t2,3)) + 
               s1*(217 + 359*t2 + 196*Power(t2,2) + 230*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s,2)*(78 + 449*t2 + 118*Power(t2,2) + 
                  34*Power(t2,3) + s1*(113 - 81*t2 - 33*Power(t2,2))) + 
               s*(31 - 783*t2 - 790*Power(t2,2) - 290*Power(t2,3) - 
                  7*Power(t2,4) + 
                  Power(s1,2)*(-65 + 237*t2 + 33*Power(t2,2)) - 
                  2*s1*(118 + 57*t2 + 94*Power(t2,2) + 34*Power(t2,3)))) \
+ Power(t1,2)*(-40 - 491*t2 - 2106*Power(t2,2) + 798*Power(t2,3) + 
               46*Power(t2,4) + 5*Power(t2,5) + 3*Power(t2,6) - 
               Power(s,3)*(6 + 29*t2 + 83*Power(t2,2) + 
                  21*Power(t2,3)) - 
               Power(s1,3)*(-78 + 19*t2 + 187*Power(t2,2) + 
                  45*Power(t2,3)) + 
               Power(s1,2)*(43 + 459*t2 - 249*Power(t2,2) - 
                  171*Power(t2,3) + 53*Power(t2,4)) + 
               s1*(-117 - 83*t2 + 1619*Power(t2,2) + 316*Power(t2,3) + 
                  55*Power(t2,4) + 33*Power(t2,5)) + 
               Power(s,2)*(11 - 13*t2 + 603*Power(t2,2) + 
                  541*Power(t2,3) + 69*Power(t2,4) + 
                  s1*(10 - 65*t2 + 169*Power(t2,2) - 3*Power(t2,3))) + 
               s*(89 + 787*t2 + 371*Power(t2,2) - 1208*Power(t2,3) - 
                  319*Power(t2,4) - 41*Power(t2,5) + 
                  Power(s1,2)*
                   (30 - 111*t2 + 101*Power(t2,2) + 69*Power(t2,3)) - 
                  2*s1*(-33 + 195*t2 + 344*Power(t2,2) + 
                     101*Power(t2,3) + 61*Power(t2,4)))) + 
            t1*(-17 - 175*t2 + 117*Power(t2,2) + 727*Power(t2,3) + 
               1030*Power(t2,4) - 577*Power(t2,5) + 35*Power(t2,6) - 
               Power(t2,7) + Power(s,3)*t2*
                (-8 + 58*t2 + 37*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s1,3)*t2*
                (-52 + 42*t2 + 41*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s1,2)*(84 + 8*t2 - 455*Power(t2,2) - 
                  261*Power(t2,3) + 181*Power(t2,4) + 22*Power(t2,5)) + 
               s1*(-46 + 419*t2 + 233*Power(t2,2) + 107*Power(t2,3) - 
                  1006*Power(t2,4) + 119*Power(t2,5) - 7*Power(t2,6)) + 
               Power(s,2)*t2*
                (128 + 67*t2 + 53*Power(t2,2) - 495*Power(t2,3) - 
                  100*Power(t2,4) - 
                  s1*(-12 + 22*t2 + 11*Power(t2,2) + 58*Power(t2,3))) + 
               s*(-10 - 127*t2 - 437*Power(t2,2) - 1139*Power(t2,3) + 
                  122*Power(t2,4) + 241*Power(t2,5) + 32*Power(t2,6) - 
                  Power(s1,2)*t2*
                   (48 + 146*t2 - 37*Power(t2,2) + 26*Power(t2,3)) + 
                  2*s1*(6 + 16*t2 + 131*Power(t2,2) + 103*Power(t2,3) + 
                     183*Power(t2,4) + 25*Power(t2,5))))) + 
         Power(s2,2)*(t2*(-16 + 4*(-13 + 6*s + 6*s1)*t2 + 
               (79 + 60*Power(s,2) + 62*s1 - 24*Power(s1,2) - 
                  2*s*(89 + 6*s1))*Power(t2,2) + 
               (41 - 172*Power(s,2) + 24*Power(s,3) - 137*s1 + 
                  18*Power(s1,2) + 4*Power(s1,3) + 3*s*(45 + 16*s1))*
                Power(t2,3) + 
               (39 - 36*Power(s,3) - 8*s1 + 27*Power(s1,2) - 
                  6*Power(s1,3) + 2*Power(s,2)*(7 + 4*s1) + 
                  2*s*(21 - 24*s1 + 5*Power(s1,2)))*Power(t2,4) + 
               (-71 - 4*Power(s,3) + 2*s1 + 7*Power(s,2)*s1 + 
                  5*Power(s1,2) - s*(-91 + 20*s1 + Power(s1,2)))*
                Power(t2,5) + 
               (-16 + 20*Power(s,2) + 29*s1 - 6*Power(s1,2) - 
                  7*s*(4 + s1))*Power(t2,6) + 
               2*(6 + s - 3*s1)*Power(t2,7)) - 
            Power(t1,5)*(48 + 8*Power(s,2) + 8*Power(s1,2) + 67*t2 + 
               s1*(62 + 65*t2) - s*(82 + 16*s1 + 65*t2)) + 
            t1*(12 + (-169 + 166*s - 72*Power(s,2))*t2 - 
               (459 + 271*s - 348*Power(s,2) + 32*Power(s,3))*
                Power(t2,2) + 
               (-115 - 367*s + 147*Power(s,2) + 90*Power(s,3))*
                Power(t2,3) + 
               (547 - 949*s + 109*Power(s,2) + 55*Power(s,3))*
                Power(t2,4) + 
               (372 + 142*s - 263*Power(s,2) + 28*Power(s,3))*
                Power(t2,5) + 
               (-203 + 25*s - 35*Power(s,2))*Power(t2,6) + 
               (1 + 5*s)*Power(t2,7) + 
               Power(s1,3)*Power(t2,2)*
                (-32 + 50*t2 + 8*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s1,2)*t2*
                (132 - 12*(13 + 2*s)*t2 - (227 + 134*s)*Power(t2,2) + 
                  3*(-45 + 7*s)*Power(t2,3) + (69 + 5*s)*Power(t2,4) + 
                  5*Power(t2,5)) + 
               s1*(-84 + (-86 + 36*s)*t2 + 
                  (677 + 100*s - 24*Power(s,2))*Power(t2,2) + 
                  (155 + 126*s + 62*Power(s,2))*Power(t2,3) + 
                  (75 + 152*s - 79*Power(s,2))*Power(t2,4) - 
                  2*(191 - 83*s + 21*Power(s,2))*Power(t2,5) + 
                  8*(8 + 3*s)*Power(t2,6) - 2*Power(t2,7))) + 
            Power(t1,4)*(-60 + 306*t2 + 21*Power(t2,2) + 
               23*Power(t2,3) + Power(s,3)*(-46 + 27*t2) - 
               Power(s1,3)*(62 + 27*t2) + 
               Power(s1,2)*(-68 - 3*t2 + 90*Power(t2,2)) + 
               Power(s,2)*(228 + s1*(30 - 81*t2) + 81*t2 + 
                  90*Power(t2,2)) + 
               s1*(154 + 193*t2 + 369*Power(t2,2) + 22*Power(t2,3)) + 
               s*(-234 - 637*t2 - 435*Power(t2,2) - 22*Power(t2,3) + 
                  Power(s1,2)*(78 + 81*t2) - 
                  2*s1*(56 + 39*t2 + 90*Power(t2,2)))) + 
            Power(t1,3)*(180 + 1860*t2 + 175*Power(t2,2) - 
               305*Power(t2,3) + 2*Power(t2,4) - 7*Power(t2,5) + 
               Power(s1,3)*(-98 + 133*t2 + 121*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(s,3)*(18 + 67*t2 + 69*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s1,2)*(-168 + 136*t2 + 293*Power(t2,2) - 
                  122*Power(t2,3) - 18*Power(t2,4)) + 
               s1*(106 - 1185*t2 - 767*Power(t2,2) - 62*Power(t2,3) - 
                  120*Power(t2,4) + Power(t2,5)) - 
               Power(s,2)*(-80 + 384*t2 + 907*Power(t2,2) + 
                  166*Power(t2,3) + 18*Power(t2,4) + 
                  s1*(-74 + 249*t2 + 17*Power(t2,2) + 9*Power(t2,3))) + 
               s*(-410 - 599*t2 + 1053*Power(t2,2) + 412*Power(t2,3) + 
                  140*Power(t2,4) - Power(t2,5) + 
                  Power(s1,2)*
                   (118 + 49*t2 - 173*Power(t2,2) + 9*Power(t2,3)) + 
                  s1*(56 + 720*t2 + 434*Power(t2,2) + 288*Power(t2,3) + 
                     36*Power(t2,4)))) - 
            Power(t1,2)*(-108 + 58*t2 + 861*Power(t2,2) + 
               1706*Power(t2,3) - 608*Power(t2,4) - 38*Power(t2,5) - 
               4*Power(t2,6) + 
               Power(s1,3)*(-40 + 6*t2 + 53*Power(t2,2) + 
                  95*Power(t2,3) + 9*Power(t2,4)) + 
               Power(s,3)*(-4 + 10*t2 + 81*Power(t2,2) + 
                  119*Power(t2,3) + 29*Power(t2,4)) + 
               Power(s,2)*(68 + (145 + 18*s1)*t2 + 
                  (257 + 35*s1)*Power(t2,2) - 
                  (745 + 197*s1)*Power(t2,3) - 
                  (355 + 49*s1)*Power(t2,4) - 29*Power(t2,5)) + 
               Power(s1,2)*(196 - 439*t2 - 759*Power(t2,2) + 
                  231*Power(t2,3) + 87*Power(t2,4) - 23*Power(t2,5)) + 
               s1*(146 + 377*t2 + 567*Power(t2,2) - 1215*Power(t2,3) - 
                  60*Power(t2,4) - 17*Power(t2,5) - 7*Power(t2,6)) + 
               s*(-18 - 541*t2 - 2349*Power(t2,2) - 307*Power(t2,3) + 
                  450*Power(t2,4) + 87*Power(t2,5) + 8*Power(t2,6) + 
                  Power(s1,2)*
                   (-84 - 178*t2 - 41*Power(t2,2) - 17*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  s1*(8 + 62*t2 + 630*Power(t2,2) + 652*Power(t2,3) + 
                     200*Power(t2,4) + 52*Power(t2,5))))) - 
         s2*(24*(-1 + s - s1)*Power(t1,6) - 
            Power(t2,2)*(1 + (-1 + s)*t2)*
             (16 + 4*(8*s - 3*(3 + s1))*t2 + 
               (17 - 62*s + 16*Power(s,2) + 20*s1)*Power(t2,2) - 
               2*(7*Power(s,2) + 3*(1 + s1) - s*(2 + s1))*Power(t2,3) - 
               (-11 + Power(s,2) - 2*s*(-1 + s1) + 2*s1)*Power(t2,4) + 
               2*(-1 + s)*Power(t2,5)) + 
            t1*(-28 + 4*(-29 + 15*s + 15*s1)*t2 + 
               (163 + 132*Power(s,2) + 70*s1 - 72*Power(s1,2) + 
                  s*(-398 + 36*s1))*Power(t2,2) + 
               (53 + 52*Power(s,3) - 209*s1 + 100*Power(s1,2) + 
                  8*Power(s1,3) + 4*Power(s,2)*(-89 + 12*s1) + 
                  s*(253 - 104*s1 - 12*Power(s1,2)))*Power(t2,3) + 
               (157 - 54*Power(s,3) - 45*s1 + 2*Power(s1,2) - 
                  12*Power(s1,3) - 5*Power(s,2)*(13 + 14*s1) + 
                  s*(43 + 78*s1 + 42*Power(s1,2)))*Power(t2,4) + 
               (-197 - 31*Power(s,3) + 27*s1 + 18*Power(s1,2) + 
                  Power(s,2)*(-39 + 55*s1) + 
                  s*(273 - 70*s1 - 8*Power(s1,2)))*Power(t2,5) - 
               (24 + 7*Power(s,3) - 37*s1 + 8*Power(s1,2) - 
                  Power(s,2)*(48 + 11*s1) + 
                  s*(55 + 14*s1 + 4*Power(s1,2)))*Power(t2,6) + 
               4*(6 + Power(s,2) - 2*s1 - s*s1)*Power(t2,7)) + 
            Power(t1,5)*(84 + 16*Power(s,3) - 16*Power(s1,3) - 
               8*Power(s,2)*(-2 + 6*s1 - 9*t2) + 32*t2 - 5*Power(t2,2) + 
               8*Power(s1,2)*(-4 + 9*t2) + 
               s1*(68 + 276*t2 + 23*Power(t2,2)) + 
               s*(-196 + 48*Power(s1,2) - 300*t2 - 23*Power(t2,2) - 
                  16*s1*(-1 + 9*t2))) + 
            Power(t1,4)*(608 + 804*t2 - 332*Power(t2,2) + 
               3*Power(t2,3) - 5*Power(t2,4) + 
               Power(s1,3)*(-36 + 116*t2 - 7*Power(t2,2)) + 
               Power(s,3)*(-12 + 84*t2 + 7*Power(t2,2)) + 
               Power(s1,2)*(60 + 272*t2 - 101*Power(t2,2) - 
                  34*Power(t2,3)) + 
               s1*(-348 - 696*t2 + 21*Power(t2,2) - 141*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s,2)*(36 + 736*t2 + 113*Power(t2,2) + 
                  34*Power(t2,3) + s1*(124 + 52*t2 + 21*Power(t2,2))) + 
               s*(-276 + 328*t2 + 167*Power(t2,2) + 157*Power(t2,3) - 
                  2*Power(t2,4) + 
                  Power(s1,2)*(172 - 148*t2 + 21*Power(t2,2)) + 
                  s1*(216 + 432*t2 + 214*Power(t2,2) + 68*Power(t2,3)))) \
+ Power(t1,3)*(8 - 412*t2 - 1174*Power(t2,2) + 151*Power(t2,3) + 
               109*Power(t2,4) + 5*Power(t2,5) + 
               Power(s1,3)*(52 - 80*t2 - 47*Power(t2,2) - 
                  29*Power(t2,3) + 5*Power(t2,4)) - 
               Power(s,3)*(-12 + 80*t2 + 129*Power(t2,2) + 
                  55*Power(t2,3) + 5*Power(t2,4)) + 
               Power(s1,2)*(-88 + 868*t2 - 98*Power(t2,2) - 
                  161*Power(t2,3) + 54*Power(t2,4)) + 
               s1*(-116 - 560*t2 + 431*Power(t2,2) + 237*Power(t2,3) + 
                  9*Power(t2,4) + 14*Power(t2,5)) + 
               Power(s,2)*(-40 - 348*t2 + 390*Power(t2,2) + 
                  391*Power(t2,3) + 56*Power(t2,4) + 
                  s1*(-68 - 96*t2 + 267*Power(t2,2) + 81*Power(t2,3) + 
                     15*Power(t2,4))) - 
               s*(-164 - 2112*t2 - 601*Power(t2,2) + 301*Power(t2,3) + 
                  74*Power(t2,4) + 16*Power(t2,5) + 
                  Power(s1,2)*
                   (-164 - 192*t2 + 91*Power(t2,2) - 3*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  2*s1*(-88 + 412*t2 + 166*Power(t2,2) + 
                     103*Power(t2,3) + 55*Power(t2,4)))) + 
            Power(t1,2)*(-72 - 564*t2 - 284*Power(t2,2) + 
               653*Power(t2,3) + 404*Power(t2,4) - 191*Power(t2,5) - 
               10*Power(t2,6) + 
               Power(s1,3)*t2*
                (-4 + 2*t2 + 33*Power(t2,2) + 14*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s,3)*t2*(-4 + 54*t2 + 103*Power(t2,2) + 
                  61*Power(t2,3) + 11*Power(t2,4)) + 
               s1*(-220 + 760*t2 + 159*Power(t2,2) + 37*Power(t2,3) - 
                  226*Power(t2,4) + 19*Power(t2,5) - 4*Power(t2,6)) + 
               Power(s1,2)*(108 - 204*t2 - 113*Power(t2,2) - 
                  233*Power(t2,3) + 53*Power(t2,4) + 13*Power(t2,5) - 
                  2*Power(t2,6)) - 
               Power(s,2)*(36 + 12*(-23 + 5*s1)*t2 - 
                  (351 + 166*s1)*Power(t2,2) + 
                  101*(-3 + s1)*Power(t2,3) + 
                  (259 + 103*s1)*Power(t2,4) + 
                  (79 + 24*s1)*Power(t2,5) + 2*Power(t2,6)) + 
               s*(92 - 104*t2 - 251*Power(t2,2) - 1365*Power(t2,3) + 
                  15*Power(t2,4) + 53*Power(t2,5) + 10*Power(t2,6) + 
                  Power(s1,2)*t2*
                   (-60 - 78*t2 - 67*Power(t2,2) + 28*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  s1*(120 + 248*t2 - 134*Power(t2,2) + 290*Power(t2,3) + 
                     129*Power(t2,4) + 62*Power(t2,5) + 4*Power(t2,6))))))*
       R2q(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*(1 - s2 + t1 - t2)*
       Power(t1 - s2*t2,2)*Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - 
         Power(t2,2),2)) - (8*(Power(t2,5)*Power(1 + (-1 + s)*t2,3)*
          (2 - 3*t2 + Power(t2,2)) + 
         Power(s2,12)*(Power(s1,3) + s1*Power(t2,2) + 2*Power(t2,3)) + 
         t1*(-1 + t2)*Power(t2,3)*(1 + (-1 + s)*t2)*
          (20 + 10*(-5 + 4*s)*t2 + 
            (38 + 20*Power(s,2) - 5*s1 + s*(-65 + 6*s1))*Power(t2,2) + 
            (-14 - 9*Power(s,2) + s*(12 - 8*s1) + 9*s1)*Power(t2,3) - 
            (-7 + s + 2*Power(s,2) + 4*s1 - 4*s*s1)*Power(t2,4) + 
            (-1 + s)*Power(t2,5)) + 
         Power(t1,2)*t2*(60 + 90*(-3 + 2*s)*t2 + 
            10*(46 + 18*Power(s,2) - 5*s1 + s*(-68 + 6*s1))*
             Power(t2,2) + 10*
             (-42 + 6*Power(s,3) + s*(81 - 25*s1) + 19*s1 + 
               Power(s,2)*(-49 + 6*s1))*Power(t2,3) + 
            (312 - 80*Power(s,3) - 262*s1 + 3*Power(s1,2) - 
               4*Power(s,2)*(-73 + 36*s1) + 
               s*(-375 + 401*s1 - 4*Power(s1,2)))*Power(t2,4) + 
            (-225 - 2*Power(s,3) + 168*s1 - 11*Power(s1,2) + 
               Power(s,2)*(23 + 130*s1) + 
               s*(185 - 292*s1 + 12*Power(s1,2)))*Power(t2,5) + 
            (98 + 25*Power(s,3) + Power(s,2)*(1 - 38*s1) - 64*s1 + 
               13*Power(s1,2) + s*(-120 + 85*s1 - 13*Power(s1,2)))*
             Power(t2,6) + (-19 + Power(s,3) + Power(s,2)*(2 - 7*s1) + 
               22*s1 - 5*Power(s1,2) + s*(-2 - 8*s1 + 5*Power(s1,2)))*
             Power(t2,7) + (4 - 4*Power(s,2) - 4*s1 + s*(2 + 4*s1))*
             Power(t2,8)) - 2*Power(t1,8)*
          (-32 + 8*Power(s,3) + 8*Power(s1,2) - 8*Power(s1,3) - 
            8*Power(s,2)*(1 + 3*s1) + 6*t2 - 3*Power(t2,2) + 
            s*(44 + 24*Power(s1,2) + 4*t2 - 3*Power(t2,2)) + 
            3*s1*(4 + Power(t2,2))) - 
         Power(s2,11)*(Power(s1,3)*(1 + 4*t1 - 8*t2) + 
            Power(s1,2)*(3 + (-2 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            s1*t2*(2*(3 - 5*t2)*t2 + s*(3 - 3*t1 + t2) + 
               t1*(2 + 3*t2)) + 
            Power(t2,2)*(1 + s*(3 - 4*t1) + 11*t2 - 16*Power(t2,2) + 
               t1*(6 + 4*t2))) + 
         2*Power(t1,7)*(-136 + 4*t2 - 26*Power(t2,2) - Power(t2,3) - 
            Power(s1,3)*(12 + 4*t2 + 5*Power(t2,2)) + 
            Power(s,3)*(-28 + 8*t2 + 5*Power(t2,2)) + 
            3*Power(s1,2)*(40 + 6*t2 + 3*Power(t2,2) + Power(t2,3)) + 
            s1*(4 - 100*t2 - 5*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s,2)*(104 + 2*t2 - 3*Power(t2,2) + 3*Power(t2,3) + 
               s1*(76 - 20*t2 - 15*Power(t2,2))) + 
            s*(4 + 96*t2 + 63*Power(t2,2) - 4*Power(t2,3) + 
               Power(s1,2)*(-36 + 16*t2 + 15*Power(t2,2)) - 
               2*s1*(176 + 2*t2 + 3*Power(t2,2) + 3*Power(t2,3)))) - 
         Power(t1,6)*(608 - 756*t2 - 334*Power(t2,2) - 48*Power(t2,3) - 
            16*Power(t2,4) + Power(t2,5) + 
            Power(s1,3)*(-56 + 2*Power(t2,2) - 6*Power(t2,3)) + 
            2*Power(s,3)*(-44 - 60*t2 - 15*Power(t2,2) + 
               6*Power(t2,3)) + 
            2*Power(s1,2)*(72 + 104*t2 - 18*Power(t2,2) + 
               28*Power(t2,3) + 9*Power(t2,4)) + 
            s1*(-800 + 488*t2 + 104*Power(t2,2) - 188*Power(t2,3) + 
               Power(t2,5)) + 
            2*Power(s,2)*(-72 + 72*t2 + 54*Power(t2,2) + 
               16*Power(t2,3) + 7*Power(t2,4) + 
               s1*(28 + 184*t2 + 47*Power(t2,2) - 15*Power(t2,3))) + 
            s*(-96 - 72*t2 + 148*Power(t2,2) + 238*Power(t2,3) + 
               34*Power(t2,4) - Power(t2,5) + 
               6*Power(s1,2)*
                (36 - 52*t2 - 11*Power(t2,2) + 4*Power(t2,3)) - 
               8*s1*(-136 + 52*t2 + 41*Power(t2,2) + 9*Power(t2,3) + 
                  4*Power(t2,4)))) + 
         Power(t1,5)*(384 - 16*t2 + 448*Power(t2,2) - 596*Power(t2,3) - 
            218*Power(t2,4) - 15*Power(t2,5) - Power(t2,6) + 
            2*Power(s1,3)*(12 - 4*t2 - 33*Power(t2,2) + 6*Power(t2,3) + 
               4*Power(t2,4) + Power(t2,5)) - 
            Power(s,3)*(-56 + 96*t2 + 242*Power(t2,2) + 
               42*Power(t2,3) + 6*Power(t2,4) + Power(t2,5)) + 
            s1*(64 + 56*t2 - 928*Power(t2,2) + 490*Power(t2,3) + 
               40*Power(t2,4) - 57*Power(t2,5) + Power(t2,6)) + 
            Power(s1,2)*(-48 - 168*t2 + 568*Power(t2,2) - 
               172*Power(t2,3) - 24*Power(t2,4) + 33*Power(t2,5) + 
               Power(t2,6)) + 
            Power(s,2)*(304 - 24*t2 - 168*Power(t2,2) - 
               128*Power(t2,3) + 106*Power(t2,4) + 29*Power(t2,5) + 
               Power(t2,6) + 
               2*s1*(-140 - 164*t2 + 289*Power(t2,2) + 96*Power(t2,3) + 
                  12*Power(t2,4) + 2*Power(t2,5))) + 
            s*(-2*s1*t2*(64 - 240*t2 - 102*Power(t2,2) + 
                  89*Power(t2,3) + 30*Power(t2,4) + Power(t2,5)) - 
               Power(s1,2)*(312 - 560*t2 + 110*Power(t2,2) + 
                  210*Power(t2,3) + 26*Power(t2,4) + 5*Power(t2,5)) + 
               2*(-352 + 340*t2 + 124*Power(t2,2) + 37*Power(t2,3) + 
                  73*Power(t2,4) + 41*Power(t2,5) + Power(t2,6)))) - 
         Power(t1,3)*(Power(s,3)*Power(t2,2)*
             (40 - 30*t2 - 90*Power(t2,2) + 109*Power(t2,3) + 
               17*Power(t2,4) + Power(t2,5)) + 
            Power(s,2)*t2*(108 + 2*(-199 + 90*s1)*t2 + 
               (54 - 460*s1)*Power(t2,2) + (206 + 464*s1)*Power(t2,3) + 
               (3 - 86*s1)*Power(t2,4) + (93 - 84*s1)*Power(t2,5) - 
               3*(9 + 2*s1)*Power(t2,6) - 6*Power(t2,7)) + 
            s*(72 + 72*(-7 + 3*s1)*t2 + (598 - 828*s1)*Power(t2,2) - 
               30*(5 - 40*s1 + Power(s1,2))*Power(t2,3) + 
               2*(269 - 419*s1 + 61*Power(s1,2))*Power(t2,4) + 
               (-439 + 308*s1 - 147*Power(s1,2))*Power(t2,5) + 
               (-133 - 100*s1 + 51*Power(s1,2))*Power(t2,6) + 
               (10 + 19*s1 + 8*Power(s1,2))*Power(t2,7) + 
               2*(-3 + 5*s1)*Power(t2,8)) - 
            (-1 + t2)*(-144 + 168*t2 - 108*Power(t2,2) + 
               378*Power(t2,3) - 294*Power(t2,4) + 57*Power(t2,5) - 
               50*Power(t2,6) - 4*Power(t2,7) + 
               2*Power(s1,3)*Power(t2,2)*
                (2 - 2*t2 - 2*Power(t2,2) + Power(t2,4)) + 
               Power(s1,2)*t2*
                (-36 + 22*t2 + 64*Power(t2,2) - 72*Power(t2,3) + 
                  45*Power(t2,4) - 9*Power(t2,5) + 3*Power(t2,6)) + 
               s1*(72 - 168*t2 + 334*Power(t2,2) - 358*Power(t2,3) + 
                  190*Power(t2,4) - 162*Power(t2,5) + 45*Power(t2,6) - 
                  2*Power(t2,7)))) + 
         Power(t1,4)*(288 + 28*t2 - 742*Power(t2,2) + 404*Power(t2,3) - 
            268*Power(t2,4) + 227*Power(t2,5) + 52*Power(t2,6) + 
            Power(t2,7) + Power(s1,3)*
             (-8 + 16*t2 - 30*Power(t2,2) + 6*Power(t2,3) + 
               36*Power(t2,4) - 14*Power(t2,5) - 3*Power(t2,6)) + 
            2*Power(s,3)*(-4 + 12*t2 - 63*Power(t2,2) + 
               100*Power(t2,3) + 46*Power(t2,4) + 5*Power(t2,5) + 
               Power(t2,6)) - 
            Power(s1,2)*(32 - 240*t2 + 308*Power(t2,2) - 
               248*Power(t2,3) + 254*Power(t2,4) - 101*Power(t2,5) + 
               12*Power(t2,6) + 4*Power(t2,7)) + 
            s1*(-8 - 376*t2 + 654*Power(t2,2) - 690*Power(t2,3) + 
               670*Power(t2,4) - 213*Power(t2,5) + Power(t2,6) + 
               6*Power(t2,7)) - 
            Power(s,2)*(32 + 176*t2 - 44*Power(t2,2) + 52*Power(t2,3) - 
               324*Power(t2,4) + 6*Power(t2,5) + 47*Power(t2,6) + 
               4*Power(t2,7) + 
               s1*(-72 + 288*t2 - 542*Power(t2,2) - 54*Power(t2,3) + 
                  320*Power(t2,4) + 58*Power(t2,5) + 7*Power(t2,6))) + 
            s*(-8 + 224*t2 + 826*Power(t2,2) - 674*Power(t2,3) - 
               452*Power(t2,4) - 18*Power(t2,5) - 48*Power(t2,6) - 
               9*Power(t2,7) + 
               2*Power(s1,2)*
                (36 - 100*t2 + 223*Power(t2,2) - 242*Power(t2,3) + 
                  60*Power(t2,4) + 37*Power(t2,5) + 4*Power(t2,6)) + 
               s1*(-384 + 704*t2 - 632*Power(t2,2) + 524*Power(t2,3) - 
                  354*Power(t2,4) - 59*Power(t2,5) + 75*Power(t2,6) + 
                  8*Power(t2,7)))) + 
         Power(s2,10)*(Power(s1,3)*
             (-3 + 6*Power(t1,2) - 4*t2 + 28*Power(t2,2) - 
               t1*(8 + 31*t2)) + 
            Power(s1,2)*(3 - (28 + 5*s)*t2 + 8*(1 + s)*Power(t2,2) + 
               16*Power(t2,3) + Power(t1,2)*(1 - 9*s + 3*t2) + 
               t1*(10 - 11*t2 - 13*Power(t2,2) + s*(-7 + 24*t2))) + 
            s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 4*t2 + 2*Power(t2,2) - 
               48*Power(t2,3) + 44*Power(t2,4) + 
               t1*t2*(14 - 16*t2 - 23*Power(t2,2)) + 
               Power(t1,2)*(1 + 6*t2 + 3*Power(t2,2)) - 
               s*(Power(t1,2)*(3 + 8*t2) + 
                  t1*(3 - 6*t2 - 32*Power(t2,2)) + 
                  t2*(-4 + 19*t2 + 6*Power(t2,2)))) + 
            t2*(2*Power(t1,2)*(3 + 6*t2 + Power(t2,2)) + 
               t1*(2 + 36*t2 - 47*Power(t2,2) - 27*Power(t2,3)) + 
               t2*(6 + t2 - 82*Power(t2,2) + 56*Power(t2,3)) + 
               Power(s,2)*(3*Power(t1,2) + t2 - 2*Power(t2,2) - 
                  t1*(3 + 5*t2)) + 
               s*(3 + 6*t2 - 20*Power(t2,2) - Power(t1,2)*(8 + 7*t2) + 
                  t1*(3 - 7*t2 + 24*Power(t2,2))))) - 
         Power(s2,9)*(1 - 2*t2 + 5*s*t2 + 4*Power(t2,2) - 
            31*s*Power(t2,2) - 6*Power(s,2)*Power(t2,2) - 
            53*Power(t2,3) - 49*s*Power(t2,3) - 
            11*Power(s,2)*Power(t2,3) - 45*Power(t2,4) + 
            52*s*Power(t2,4) + 16*Power(s,2)*Power(t2,4) + 
            264*Power(t2,5) - 112*Power(t2,6) + 
            Power(t1,2)*(1 + 
               (39 - 22*s - 6*Power(s,2) + 2*Power(s,3))*t2 + 
               (-42 + 71*s - 32*Power(s,2))*Power(t2,2) + 
               3*(-34 + 9*s)*Power(t2,3) - 11*Power(t2,4)) + 
            Power(t1,3)*(2 - Power(s,3) + 12*t2 + 6*Power(t2,2) + 
               Power(s,2)*(3 + 4*t2) - s*(4 + 14*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(-1 + 4*Power(t1,3) + 18*t2 + Power(t2,2) - 
               56*Power(t2,3) - Power(t1,2)*(41 + 45*t2) + 
               t1*(-22 + 58*t2 + 105*Power(t2,2))) + 
            t1*t2*(13 + 17*t2 - Power(s,3)*t2 - 363*Power(t2,2) + 
               159*Power(t2,3) + 78*Power(t2,4) + 
               2*Power(s,2)*(-4 + 7*t2 + 12*Power(t2,2)) + 
               s*(16 - 35*t2 + 59*Power(t2,2) - 57*Power(t2,3))) + 
            Power(s1,2)*(-9 + (-5 + s)*t2 + (109 + 34*s)*Power(t2,2) - 
               4*(1 + 7*s)*Power(t2,3) - 56*Power(t2,4) + 
               Power(t1,3)*(3 - 9*s + 3*t2) + 
               t1*(-28 + s - 76*t2 + 44*s*t2 + 81*Power(t2,2) - 
                  85*s*Power(t2,2) + 62*Power(t2,3)) + 
               Power(t1,2)*(9 - 40*t2 - 25*Power(t2,2) + 
                  18*s*(1 + 4*t2))) + 
            s1*(3 - 16*(2 + s)*t2 + 
               2*(9 - 8*s + Power(s,2))*Power(t2,2) + 
               (4 + 56*s)*Power(t2,3) + (175 + 12*s)*Power(t2,4) - 
               112*Power(t2,5) + 
               Power(t1,3)*(3 + 6*Power(s,2) + 6*t2 + Power(t2,2) - 
                  s*(8 + 7*t2)) + 
               Power(t1,2)*(8 + Power(s,2)*(8 - 29*t2) + 4*t2 - 
                  62*Power(t2,2) - 11*Power(t2,3) + 
                  s*(-13 + 75*t2 + 70*Power(t2,2))) + 
               t1*(8 - 10*t2 - 177*Power(t2,2) + 72*Power(t2,3) + 
                  75*Power(t2,4) + 3*Power(s,2)*t2*(2 + 3*t2) - 
                  4*s*(2 - 7*t2 + 7*Power(t2,2) + 32*Power(t2,3))))) + 
         Power(s2,8)*(1 - 12*t2 - 11*s*t2 + 10*Power(t2,2) - 
            36*s*Power(t2,2) - 12*Power(s,2)*Power(t2,2) + 
            11*Power(t2,3) + 95*s*Power(t2,3) + 
            32*Power(s,2)*Power(t2,3) + 2*Power(s,3)*Power(t2,3) + 
            200*Power(t2,4) + 155*s*Power(t2,4) + 
            48*Power(s,2)*Power(t2,4) + 196*Power(t2,5) - 
            60*s*Power(t2,5) - 56*Power(s,2)*Power(t2,5) - 
            478*Power(t2,6) + 140*Power(t2,7) + 
            Power(t1,4)*(4 - Power(s,3) + 6*t2 + Power(s,2)*(4 + t2) - 
               s*(7 + 6*t2)) + 
            Power(t1,3)*(14 - 7*t2 - 143*Power(t2,2) - 40*Power(t2,3) + 
               2*Power(s,3)*(-1 + 5*t2) + 
               Power(s,2)*(5 - 68*t2 - 30*Power(t2,2)) + 
               s*(-15 + 63*t2 + 103*Power(t2,2))) + 
            Power(s1,3)*(2 + Power(t1,4) + t2 - 45*Power(t2,2) + 
               22*Power(t2,3) + 70*Power(t2,4) - 
               Power(t1,3)*(65 + 29*t2) + 
               Power(t1,2)*(-7 + 255*t2 + 146*Power(t2,2)) + 
               t1*(28 + 84*t2 - 181*Power(t2,2) - 203*Power(t2,3))) + 
            Power(t1,2)*(7 + 34*t2 - 590*Power(t2,2) - 
               17*Power(s,3)*Power(t2,2) + 15*Power(t2,3) + 
               340*Power(t2,4) + 25*Power(t2,5) + 
               2*Power(s,2)*Power(t2,2)*(25 + 51*t2) + 
               s*(1 + t2 + 260*Power(t2,2) - 180*Power(t2,3) - 
                  24*Power(t2,4))) + 
            t1*(2 + t2 - 179*Power(t2,2) - 341*Power(t2,3) + 
               1337*Power(t2,4) - 303*Power(t2,5) - 125*Power(t2,6) + 
               4*Power(s,3)*Power(t2,2)*(1 + t2) + 
               Power(s,2)*t2*
                (-6 + 43*t2 + 16*Power(t2,2) - 33*Power(t2,3)) + 
               s*(-1 - 20*t2 - 183*Power(t2,2) + 128*Power(t2,3) - 
                  197*Power(t2,4) + 62*Power(t2,5))) + 
            Power(s1,2)*(-3 + (58 + 5*s)*t2 - 4*(7 + s)*Power(t2,2) - 
               2*(118 + 49*s)*Power(t2,3) + 8*(-4 + 7*s)*Power(t2,4) + 
               112*Power(t2,5) + Power(t1,4)*(3 - 3*s + t2) + 
               Power(t1,3)*(-15 - 70*t2 - 19*Power(t2,2) + 
                  s*(90 + 68*t2)) + 
               2*Power(t1,2)*
                (-57 + 6*t2 + 130*Power(t2,2) + 38*Power(t2,3) + 
                  s*(42 - 65*t2 - 126*Power(t2,2))) + 
               t1*(-59 + 270*t2 + 334*Power(t2,2) - 258*Power(t2,3) - 
                  153*Power(t2,4) + 
                  s*(13 + 46*t2 - 114*Power(t2,2) + 172*Power(t2,3)))) + 
            s1*(-9 + (2 - 10*s)*t2 + 
               (122 + 101*s + 2*Power(s,2))*Power(t2,2) - 
               3*(7 - 7*s + 6*Power(s,2))*Power(t2,3) - 
               (61 + 108*s)*Power(t2,4) - 380*Power(t2,5) + 
               182*Power(t2,6) + 
               Power(t1,4)*(3 + 3*Power(s,2) + 2*t2 - s*(7 + 2*t2)) + 
               Power(t1,3)*(10 - 49*t2 - 37*Power(t2,2) + 
                  6*Power(t2,3) - Power(s,2)*(23 + 49*t2) + 
                  s*(25 + 172*t2 + 49*Power(t2,2))) + 
               Power(t1,2)*(6 - 238*t2 - 151*Power(t2,2) + 
                  233*Power(t2,3) + 5*Power(t2,4) + 
                  Power(s,2)*(6 - 32*t2 + 123*Power(t2,2)) + 
                  s*(35 + 130*t2 - 428*Power(t2,2) - 214*Power(t2,3))) - 
               t1*(32 + 55*t2 - 51*Power(t2,2) - 835*Power(t2,3) + 
                  216*Power(t2,4) + 135*Power(t2,5) + 
                  Power(s,2)*t2*(-18 + 45*t2 + 29*Power(t2,2)) + 
                  s*(4 + 12*t2 + 142*Power(t2,2) - Power(t2,3) - 
                     256*Power(t2,4))))) + 
         Power(s2,7)*(3 - (2 - 3*s + Power(s,2))*Power(t1,5) - 3*t2 + 
            11*s*t2 - 41*Power(t2,2) - 80*s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) - 25*Power(t2,3) - 
            141*s*Power(t2,3) - 58*Power(s,2)*Power(t2,3) + 
            60*Power(t2,4) + 77*s*Power(t2,4) + 
            70*Power(s,2)*Power(t2,4) + 12*Power(s,3)*Power(t2,4) + 
            396*Power(t2,5) + 244*s*Power(t2,5) + 
            112*Power(s,2)*Power(t2,5) + 414*Power(t2,6) - 
            10*s*Power(t2,6) - 112*Power(s,2)*Power(t2,6) - 
            530*Power(t2,7) + 112*Power(t2,8) + 
            Power(t1,4)*(-4 + 88*t2 + 54*Power(t2,2) - 4*Power(t2,3) - 
               Power(s,3)*(10 + 7*t2) + 
               Power(s,2)*(33 + 76*t2 + 5*Power(t2,2)) + 
               s*(-16 - 123*t2 - 13*Power(t2,2) + 4*Power(t2,3))) + 
            Power(t1,3)*(-18 + 416*t2 + 353*Power(t2,2) - 
               561*Power(t2,3) - 114*Power(t2,4) + 
               Power(s,3)*(2 + 12*t2 + 41*Power(t2,2)) + 
               Power(s,2)*(10 + 47*t2 - 339*Power(t2,2) - 
                  64*Power(t2,3)) + 
               s*(-7 - 375*t2 + 117*Power(t2,2) + 125*Power(t2,3) - 
                  33*Power(t2,4))) + 
            Power(s1,3)*(Power(t1,4)*(44 + 7*t2) - 
               Power(t1,3)*(122 + 375*t2 + 89*Power(t2,2)) + 
               Power(t1,2)*(-148 + 56*t2 + 673*Power(t2,2) + 
                  263*Power(t2,3)) + 
               t1*(-15 + 123*t2 + 95*Power(t2,2) - 312*Power(t2,3) - 
                  245*Power(t2,4)) + 
               t2*(10 - 10*t2 - 60*Power(t2,2) + 55*Power(t2,3) + 
                  56*Power(t2,4))) + 
            Power(t1,2)*(-3 + 213*t2 + 831*Power(t2,2) - 
               2547*Power(t2,3) - 224*Power(t2,4) + 588*Power(t2,5) + 
               30*Power(t2,6) + 
               Power(s,3)*t2*(2 - 4*t2 - 49*Power(t2,2)) + 
               Power(s,2)*t2*
                (-90 - 129*t2 + 34*Power(t2,2) + 129*Power(t2,3)) + 
               s*(-5 + 123*t2 - 11*Power(t2,2) + 952*Power(t2,3) - 
                  146*Power(t2,4) + 40*Power(t2,5))) + 
            t1*(12 + 10*t2 - 39*Power(t2,2) - 926*Power(t2,3) - 
               1475*Power(t2,4) + 2503*Power(t2,5) - 355*Power(t2,6) - 
               120*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(-16 + 14*t2 + 3*Power(t2,2)) + 
               Power(s,2)*t2*
                (-8 - 78*t2 + 5*Power(t2,2) + 214*Power(t2,3) + 
                  26*Power(t2,4)) + 
               s*(5 + 80*t2 - 115*Power(t2,2) - 619*Power(t2,3) + 
                  209*Power(t2,4) - 341*Power(t2,5) + 15*Power(t2,6))) - 
            Power(s1,2)*(6 + Power(t1,5) - 2*t2 - 
               (142 + 25*s)*Power(t2,2) + (119 + 6*s)*Power(t2,3) + 
               2*(159 + 77*s)*Power(t2,4) + (80 - 70*s)*Power(t2,5) - 
               140*Power(t2,6) + 
               Power(t1,4)*(-42 - 54*t2 - 5*Power(t2,2) + 
                  7*s*(14 + 3*t2)) + 
               Power(t1,3)*(-113 + 310*t2 + 319*Power(t2,2) + 
                  32*Power(t2,3) - 3*s*(-16 + 191*t2 + 73*Power(t2,2))) \
+ Power(t1,2)*(50 + 876*t2 - 47*Power(t2,2) - 721*Power(t2,3) - 
                  106*Power(t2,4) + 
                  s*(24 - 316*t2 + 401*Power(t2,2) + 485*Power(t2,3))) \
+ t1*(88 + 163*t2 - 934*Power(t2,2) - 855*Power(t2,3) + 
                  469*Power(t2,4) + 218*Power(t2,5) + 
                  s*(2 - 57*t2 - 232*Power(t2,2) + 170*Power(t2,3) - 
                     213*Power(t2,4)))) + 
            s1*(3 + (-1 + 2*s)*Power(t1,5) - (62 + 13*s)*t2 + 
               (69 - 35*s)*Power(t2,2) + 
               (299 + 267*s + 18*Power(s,2))*Power(t2,3) + 
               (52 + 16*s - 67*Power(s,2))*Power(t2,4) - 
               2*(85 + 79*s)*Power(t2,5) + (-537 + 42*s)*Power(t2,6) + 
               196*Power(t2,7) + 
               Power(t1,4)*(10 + 39*t2 - 15*Power(t2,2) - 
                  4*Power(t2,3) + Power(s,2)*(64 + 21*t2) - 
                  2*s*(48 + 65*t2 + 5*Power(t2,2))) + 
               Power(t1,3)*(97 + 382*t2 - 150*Power(t2,2) + 
                  37*Power(t2,4) + 
                  Power(s,2)*(83 - 210*t2 - 171*Power(t2,2)) + 
                  s*(-188 + 335*t2 + 794*Power(t2,2) + 96*Power(t2,3))) \
+ Power(t1,2)*(103 - 21*t2 - 1554*Power(t2,2) - 514*Power(t2,3) + 
                  457*Power(t2,4) - 35*Power(t2,5) + 
                  Power(s,2)*
                   (2 + 80*t2 - 25*Power(t2,2) + 271*Power(t2,3)) + 
                  s*(-100 + 511*t2 + 801*Power(t2,2) - 
                     977*Power(t2,3) - 290*Power(t2,4))) - 
               t1*(-52 + 342*t2 + 285*Power(t2,2) - 170*Power(t2,3) - 
                  2085*Power(t2,4) + 426*Power(t2,5) + 145*Power(t2,6) + 
                  Power(s,2)*t2*
                   (4 - 155*t2 + 120*Power(t2,2) + 41*Power(t2,3)) + 
                  s*(14 + 133*t2 + 108*Power(t2,2) + 276*Power(t2,3) + 
                     232*Power(t2,4) - 262*Power(t2,5))))) + 
         Power(s2,6)*(-1 + 22*t2 + 8*s*t2 - 40*Power(t2,2) + 
            46*s*Power(t2,2) + 13*Power(s,2)*Power(t2,2) - 
            151*Power(t2,3) - 142*s*Power(t2,3) + 
            16*Power(s,2)*Power(t2,3) - 4*Power(s,3)*Power(t2,3) - 
            163*Power(t2,4) - 249*s*Power(t2,4) - 
            113*Power(s,2)*Power(t2,4) + 4*Power(s,3)*Power(t2,4) + 
            85*Power(t2,5) - 166*s*Power(t2,5) + 
            83*Power(s,2)*Power(t2,5) + 30*Power(s,3)*Power(t2,5) + 
            424*Power(t2,6) + 192*s*Power(t2,6) + 
            154*Power(s,2)*Power(t2,6) + 509*Power(t2,7) + 
            52*s*Power(t2,7) - 140*Power(s,2)*Power(t2,7) - 
            366*Power(t2,8) + 56*Power(t2,9) + 
            Power(t1,5)*(11*Power(s,3) - Power(s,2)*(46 + 15*t2) + 
               s*(47 + 26*t2 - 12*Power(t2,2)) + 
               4*(-5 - 8*t2 + 3*Power(t2,2))) + 
            Power(t1,4)*(-107 - 394*t2 + 420*Power(t2,2) + 
               204*Power(t2,3) - 2*Power(t2,4) - 
               5*Power(s,3)*(-4 + 17*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(-88 + 332*t2 + 259*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s*(171 + 110*t2 - 234*Power(t2,2) + 121*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(t1,3)*(-83 - 839*t2 + 2179*Power(t2,2) + 
               1651*Power(t2,3) - 1050*Power(t2,4) - 164*Power(t2,5) + 
               Power(s,3)*t2*(-4 + 93*t2 + 78*Power(t2,2)) + 
               Power(s,2)*(-6 + 288*t2 + 390*Power(t2,2) - 
                  532*Power(t2,3) - 34*Power(t2,4)) - 
               s*(-29 + 85*t2 + 1816*Power(t2,2) + 85*Power(t2,3) + 
                  180*Power(t2,4) + 72*Power(t2,5))) - 
            Power(s1,3)*(11*Power(t1,5) - 
               Power(t1,4)*(232 + 237*t2 + 20*Power(t2,2)) + 
               Power(t1,3)*(-209 + 645*t2 + 884*Power(t2,2) + 
                  142*Power(t2,3)) + 
               Power(t1,2)*(67 + 401*t2 - 294*Power(t2,2) - 
                  966*Power(t2,3) - 280*Power(t2,4)) + 
               Power(t2,2)*(-20 + 30*t2 + 45*Power(t2,2) - 
                  64*Power(t2,3) - 28*Power(t2,4)) + 
               t1*(20 + 15*t2 - 225*Power(t2,2) + 27*Power(t2,3) + 
                  315*Power(t2,4) + 189*Power(t2,5))) + 
            Power(t1,2)*(-30 + 33*t2 + 1603*Power(t2,2) + 
               4153*Power(t2,3) - 4980*Power(t2,4) - 437*Power(t2,5) + 
               572*Power(t2,6) + 20*Power(t2,7) + 
               Power(s,3)*t2*
                (10 + 6*t2 + 11*Power(t2,2) - 60*Power(t2,3)) + 
               Power(s,2)*(2 + 99*t2 - 293*Power(t2,2) - 
                  617*Power(t2,3) - 330*Power(t2,4) + 25*Power(t2,5)) + 
               s*(4 - 154*t2 + 511*Power(t2,2) - 392*Power(t2,3) + 
                  1590*Power(t2,4) + 75*Power(t2,5) + 105*Power(t2,6))) \
+ t1*(-15 + 130*t2 + 182*Power(t2,2) - 54*Power(t2,3) - 
               2053*Power(t2,4) - 3011*Power(t2,5) + 2645*Power(t2,6) - 
               261*Power(t2,7) - 69*Power(t2,8) - 
               2*Power(s,3)*Power(t2,2)*
                (1 + 43*t2 - 4*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,2)*t2*
                (13 + 39*t2 - 306*Power(t2,2) - 322*Power(t2,3) + 
                  556*Power(t2,4) + 135*Power(t2,5)) + 
               s*(1 + 113*t2 + 582*Power(t2,2) + 81*Power(t2,3) - 
                  886*Power(t2,4) + 154*Power(t2,5) - 325*Power(t2,6) - 
                  36*Power(t2,7))) + 
            Power(s1,2)*(t2*(-30 + 55*t2 + (193 + 50*s)*Power(t2,2) - 
                  5*(40 + s)*Power(t2,3) - (283 + 140*s)*Power(t2,4) + 
                  8*(-11 + 7*s)*Power(t2,5) + 112*Power(t2,6)) + 
               Power(t1,5)*(33*s - 5*(7 + 3*t2)) + 
               Power(t1,4)*(67 + 450*t2 + 148*Power(t2,2) + 
                  2*Power(t2,3) - s*(270 + 559*t2 + 60*Power(t2,2))) + 
               Power(t1,3)*(454 + 667*t2 - 1105*Power(t2,2) - 
                  552*Power(t2,3) + 4*Power(t2,4) + 
                  s*(-326 - 13*t2 + 1479*Power(t2,2) + 362*Power(t2,3))\
) - Power(t1,2)*(-386 + 917*t2 + 2830*Power(t2,2) + 18*Power(t2,3) - 
                  1044*Power(t2,4) - 55*Power(t2,5) + 
                  s*(122 + 274*t2 - 470*Power(t2,2) + 
                     635*Power(t2,3) + 535*Power(t2,4))) + 
               t1*(40 - 422*t2 - 84*Power(t2,2) + 1570*Power(t2,3) + 
                  1255*Power(t2,4) - 540*Power(t2,5) - 
                  181*Power(t2,6) + 
                  s*(-6 - 43*t2 + 50*Power(t2,2) + 456*Power(t2,3) - 
                     192*Power(t2,4) + 158*Power(t2,5)))) + 
            s1*(6 + (-7 + 6*s)*t2 - 2*(73 + 41*s)*Power(t2,2) + 
               (161 - 89*s + Power(s,2))*Power(t2,3) + 
               (470 + 371*s + 60*Power(s,2))*Power(t2,4) + 
               (204 + 47*s - 136*Power(s,2))*Power(t2,5) - 
               (215 + 178*s)*Power(t2,6) + (-506 + 84*s)*Power(t2,7) + 
               140*Power(t2,8) + 
               Power(t1,5)*(-13 - 33*Power(s,2) + 12*t2 + 
                  12*Power(t2,2) + s*(81 + 30*t2)) + 
               Power(t1,4)*(-200 - 131*t2 - 61*Power(t2,2) - 
                  155*Power(t2,3) - 12*Power(t2,4) + 
                  Power(s,2)*(18 + 407*t2 + 60*Power(t2,2)) - 
                  s*(-25 + 952*t2 + 407*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,3)*(-86 + 1342*t2 + 1931*Power(t2,2) - 
                  364*Power(t2,3) + 225*Power(t2,4) + 68*Power(t2,5) - 
                  Power(s,2)*
                   (77 - 283*t2 + 688*Power(t2,2) + 298*Power(t2,3)) + 
                  s*(-127 - 1891*t2 + 831*Power(t2,2) + 
                     1328*Power(t2,3) + 30*Power(t2,4))) + 
               Power(t1,2)*(109 + 890*t2 + 251*Power(t2,2) - 
                  4444*Power(t2,3) - 597*Power(t2,4) + 
                  532*Power(t2,5) - 75*Power(t2,6) + 
                  Power(s,2)*
                   (-7 - 213*t2 + 258*Power(t2,2) + 3*Power(t2,3) + 
                     315*Power(t2,4)) + 
                  s*(56 - 177*t2 + 1701*Power(t2,2) + 
                     2285*Power(t2,3) - 928*Power(t2,4) - 
                     130*Power(t2,5))) - 
               t1*(-92 - 54*t2 + 1254*Power(t2,2) + 922*Power(t2,3) - 
                  375*Power(t2,4) - 3105*Power(t2,5) + 542*Power(t2,6) + 
                  93*Power(t2,7) + 
                  Power(s,2)*t2*
                   (13 + 49*t2 - 523*Power(t2,2) + 131*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  s*(-7 - 34*t2 + 505*Power(t2,2) + 118*Power(t2,3) + 
                     53*Power(t2,4) + 621*Power(t2,5) - 88*Power(t2,6))))\
) + Power(s2,5)*(-2 + (4 - 6*s + 30*s1)*t2 + 
            (49 - 6*Power(s,2) - 80*s1 - 60*Power(s1,2) + 
               s*(61 + 30*s1))*Power(t2,2) + 
            (-6*Power(s,2) + 2*Power(s,3) + s*(138 - 113*s1) + 
               s1*(-237 + 102*s1 + 20*Power(s1,2)))*Power(t2,3) + 
            (-256 - 23*Power(s,3) + 162*s1 + 185*Power(s1,2) - 
               35*Power(s1,3) + Power(s,2)*(102 + 5*s1) + 
               10*s*(-6 - 20*s1 + 5*Power(s1,2)))*Power(t2,4) + 
            (-311 + 21*Power(s,3) + 407*s1 - 177*Power(s1,2) - 
               18*Power(s1,3) + 2*Power(s,2)*(-59 + 50*s1) - 
               5*s*(29 - 55*s1 + Power(s1,2)))*Power(t2,5) + 
            (90 + 40*Power(s,3) + Power(s,2)*(65 - 165*s1) + 281*s1 - 
               173*Power(s1,2) + 41*Power(s1,3) - 
               2*s*(231 - 57*s1 + 35*Power(s1,2)))*Power(t2,6) + 
            (126*Power(s,2) + s*(51 - 144*s1 + 28*Power(s1,2)) + 
               4*(54 - 32*s1 - 13*Power(s1,2) + 2*Power(s1,3)))*
             Power(t2,7) + (381 - 112*Power(s,2) - 313*s1 + 
               56*Power(s1,2) + 12*s*(5 + 7*s1))*Power(t2,8) + 
            8*(-19 + 8*s1)*Power(t2,9) + 16*Power(t2,10) + 
            Power(t1,6)*(7 + 10*Power(s,2) + 10*Power(s1,2) - 12*t2 - 
               3*s1*(1 + 4*t2) + s*(-13 - 20*s1 + 12*t2)) + 
            Power(t1,5)*(129 - 127*t2 - 178*Power(t2,2) + 
               16*Power(t2,3) + Power(s,3)*(31 + 55*t2) - 
               Power(s1,3)*(162 + 55*t2) - 
               Power(s1,2)*(195 + 236*t2 + 16*Power(t2,2)) + 
               s1*(123 + 112*t2 + 243*Power(t2,2) + 46*Power(t2,3)) - 
               Power(s,2)*(83 + 347*t2 + 16*Power(t2,2) + 
                  s1*(224 + 165*t2)) + 
               s*(-104 + 179*t2 - 145*Power(t2,2) - 46*Power(t2,3) + 
                  5*Power(s1,2)*(71 + 33*t2) + 
                  s1*(348 + 583*t2 + 32*Power(t2,2)))) + 
            Power(t1,4)*(303 - 729*t2 - 2233*Power(t2,2) + 
               812*Power(t2,3) + 354*Power(t2,4) + 8*Power(t2,5) - 
               Power(s,3)*(18 - 14*t2 + 229*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(s1,3)*(30 + 981*t2 + 481*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(s1,2)*(-521 + 617*t2 + 1074*Power(t2,2) + 
                  53*Power(t2,3) - 14*Power(t2,4)) - 
               s1*(403 + 2117*t2 + 505*Power(t2,2) + 563*Power(t2,3) + 
                  328*Power(t2,4) + 12*Power(t2,5)) + 
               Power(s,2)*(-90 - 891*t2 + 727*Power(t2,2) + 
                  221*Power(t2,3) - 14*Power(t2,4) + 
                  s1*(-274 + 363*t2 + 939*Power(t2,2) + 78*Power(t2,3))\
) + s*(-10 + 1591*t2 + 647*Power(t2,2) + 290*Power(t2,3) + 
                  330*Power(t2,4) + 12*Power(t2,5) - 
                  Power(s1,2)*
                   (-482 + 1358*t2 + 1191*Power(t2,2) + 78*Power(t2,3)) \
+ s1*(999 + 566*t2 - 2208*Power(t2,2) - 274*Power(t2,3) + 
                     28*Power(t2,4)))) + 
            Power(t1,3)*(15 - 1262*t2 - 5691*Power(t2,2) + 
               4277*Power(t2,3) + 2499*Power(t2,4) - 1040*Power(t2,5) - 
               118*Power(t2,6) + 
               Power(s1,3)*(360 + 295*t2 - 1307*Power(t2,2) - 
                  1066*Power(t2,3) - 118*Power(t2,4)) + 
               Power(s,3)*(-5 - 49*t2 - 79*Power(t2,2) + 
                  149*Power(t2,3) + 67*Power(t2,4)) + 
               Power(s1,2)*(-201 + 3208*t2 + 2126*Power(t2,2) - 
                  1727*Power(t2,3) - 343*Power(t2,4) + 61*Power(t2,5)) \
+ s1*(-500 - 941*t2 + 4448*Power(t2,2) + 3195*Power(t2,3) - 
                  701*Power(t2,4) + 406*Power(t2,5) + 57*Power(t2,6)) + 
               Power(s,2)*(-11 + 189*t2 + 1119*Power(t2,2) + 
                  1370*Power(t2,3) - 105*Power(t2,4) + 
                  44*Power(t2,5) + 
                  s1*(10 - 269*t2 + 450*Power(t2,2) - 
                     963*Power(t2,3) - 252*Power(t2,4))) + 
               s*(81 + 480*t2 + 792*Power(t2,2) - 3070*Power(t2,3) - 
                  315*Power(t2,4) - 480*Power(t2,5) - 63*Power(t2,6) + 
                  Power(s1,2)*
                   (299 - 733*t2 + 267*Power(t2,2) + 
                     1880*Power(t2,3) + 303*Power(t2,4)) + 
                  s1*(456 - 1831*t2 - 6106*Power(t2,2) + 
                     381*Power(t2,3) + 674*Power(t2,4) - 105*Power(t2,5)\
))) + Power(t1,2)*(-52 - 395*t2 - 348*Power(t2,2) + 4076*Power(t2,3) + 
               8762*Power(t2,4) - 4941*Power(t2,5) - 320*Power(t2,6) + 
               310*Power(t2,7) + 7*Power(t2,8) + 
               Power(s,3)*t2*
                (2 + 157*t2 + 41*Power(t2,2) + 91*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s1,3)*(80 - 235*t2 - 309*Power(t2,2) + 
                  485*Power(t2,3) + 798*Power(t2,4) + 171*Power(t2,5)) \
+ Power(s1,2)*(247 + 957*t2 - 2436*Power(t2,2) - 4389*Power(t2,3) + 
                  5*Power(t2,4) + 817*Power(t2,5) - 27*Power(t2,6)) + 
               s1*(-318 + 1539*t2 + 3035*Power(t2,2) + 
                  702*Power(t2,3) - 7016*Power(t2,4) - 
                  80*Power(t2,5) + 384*Power(t2,6) - 67*Power(t2,7)) + 
               Power(s,2)*(-2 + 109*t2 + 818*Power(t2,2) + 
                  297*Power(t2,3) - 1218*Power(t2,4) - 
                  882*Power(t2,5) - 102*Power(t2,6) + 
                  s1*(4 + 43*t2 - 1157*Power(t2,2) + 281*Power(t2,3) - 
                     114*Power(t2,4) + 163*Power(t2,5))) + 
               s*(-45 - 583*t2 - 951*Power(t2,2) + 805*Power(t2,3) - 
                  1108*Power(t2,4) + 1286*Power(t2,5) + 
                  197*Power(t2,6) + 93*Power(t2,7) - 
                  Power(s1,2)*
                   (-24 + 218*t2 + 653*Power(t2,2) - 427*Power(t2,3) + 
                     490*Power(t2,4) + 314*Power(t2,5)) + 
                  s1*(139 + 194*t2 - 572*Power(t2,2) + 
                     1511*Power(t2,3) + 3336*Power(t2,4) - 
                     27*Power(t2,5) + 102*Power(t2,6)))) - 
            t1*(32 - 25*t2 - 597*Power(t2,2) - 915*Power(t2,3) - 
               89*Power(t2,4) + 2089*Power(t2,5) + 3407*Power(t2,6) - 
               1601*Power(t2,7) + 117*Power(t2,8) + 22*Power(t2,9) + 
               Power(s,3)*Power(t2,2)*
                (-25 - t2 + 192*Power(t2,2) + 30*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s1,3)*t2*
                (60 - 40*t2 - 204*Power(t2,2) + 148*Power(t2,3) + 
                  178*Power(t2,4) + 91*Power(t2,5)) + 
               Power(s1,2)*(-60 + 30*t2 + 784*Power(t2,2) - 
                  254*Power(t2,3) - 1434*Power(t2,4) - 
                  1052*Power(t2,5) + 411*Power(t2,6) + 78*Power(t2,7)) + 
               s1*(35 - 469*t2 + 145*Power(t2,2) + 2105*Power(t2,3) + 
                  1698*Power(t2,4) - 398*Power(t2,5) - 
                  2865*Power(t2,6) + 436*Power(t2,7) + 33*Power(t2,8)) + 
               Power(s,2)*t2*
                (4 + 42*t2 - 293*Power(t2,2) + 578*Power(t2,3) + 
                  755*Power(t2,4) - 732*Power(t2,5) - 180*Power(t2,6) + 
                  s1*(-6 + 85*t2 + 301*Power(t2,2) - 936*Power(t2,3) + 
                     10*Power(t2,4) - 29*Power(t2,5))) + 
               s*(5 + 105*t2 - 252*Power(t2,2) - 993*Power(t2,3) - 
                  1205*Power(t2,4) + 442*Power(t2,5) - 27*Power(t2,6) + 
                  157*Power(t2,7) + 41*Power(t2,8) + 
                  Power(s1,2)*t2*
                   (30 + 85*t2 + 8*Power(t2,2) - 456*Power(t2,3) + 
                     202*Power(t2,4) - 59*Power(t2,5)) + 
                  s1*(-6 - 140*t2 - 304*Power(t2,2) + 903*Power(t2,3) + 
                     66*Power(t2,4) - 531*Power(t2,5) + 
                     794*Power(t2,6) + 88*Power(t2,7))))) + 
         Power(s2,4)*((4 - 4*s + 4*s1)*Power(t1,7) + 
            t2*(-10 + (35 - 30*s + 60*s1)*t2 + 
               (92 + 6*Power(s,2) - 40*s1 - 48*Power(s1,2) + 
                  s*(-13 + 24*s1))*Power(t2,2) + 
               (75 - 135*Power(s,2) + 10*Power(s,3) - 309*s1 + 
                  45*Power(s1,2) + 10*Power(s1,3) + s*(279 + 32*s1))*
                Power(t2,3) + 
               (-146 - 55*Power(s,3) + 120*s1 + 136*Power(s1,2) - 
                  19*Power(s1,3) + 2*Power(s,2)*(114 + 5*s1) + 
                  s*(18 - 280*s1 + 25*Power(s1,2)))*Power(t2,4) + 
               (-321 + 45*Power(s,3) + 148*s1 - 80*Power(s1,2) - 
                  3*Power(s1,3) + Power(s,2)*(-85 + 90*s1) + 
                  s*(99 + 91*s1 - 6*Power(s1,2)))*Power(t2,5) + 
               (119 + 30*Power(s,3) + Power(s,2)*(46 - 122*s1) + 
                  187*s1 - 74*Power(s1,2) + 14*Power(s1,3) + 
                  s*(-489 + 131*s1 - 14*Power(s1,2)))*Power(t2,6) + 
               (8 + 56*Power(s,2) - 19*s1 - 16*Power(s1,2) + 
                  Power(s1,3) + s*(-21 - 76*s1 + 8*Power(s1,2)))*
                Power(t2,7) + 
               2*(85 - 28*Power(s,2) - 60*s1 + 8*Power(s1,2) + 
                  2*s*(7 + 12*s1))*Power(t2,8) + 
               17*(-2 + s1)*Power(t2,9) + 2*Power(t2,10)) - 
            t1*(-10 + 5*(34 + 21*s)*t2 + 
               2*(29 + 265*s - 54*Power(s,2) + 6*Power(s,3))*
                Power(t2,2) - 
               (1009 + 46*s - 321*Power(s,2) + 87*Power(s,3))*
                Power(t2,3) + 
               (-1633 - 31*s - 578*Power(s,2) + 17*Power(s,3))*
                Power(t2,4) + 
               (78 - 2469*s + 640*Power(s,2) + 230*Power(s,3))*
                Power(t2,5) + 
               3*(260 - 69*s + 252*Power(s,2) + 20*Power(s,3))*
                Power(t2,6) + 
               (2211 + 24*s - 556*Power(s,2) + 24*Power(s,3))*
                Power(t2,7) + 
               (-523 + 19*s - 121*Power(s,2))*Power(t2,8) + 
               (29 + 18*s)*Power(t2,9) + 3*Power(t2,10) + 
               Power(s1,3)*Power(t2,2)*
                (60 - 94*t2 - 72*Power(t2,2) + 130*Power(t2,3) + 
                  43*Power(t2,4) + 25*Power(t2,5)) + 
               Power(s1,2)*t2*
                (-180 + (206 + 60*s)*t2 + 2*(375 + 64*s)*Power(t2,2) - 
                  (403 + 67*s)*Power(t2,3) - 
                  2*(384 + 119*s)*Power(t2,4) + 
                  2*(-242 + 87*s)*Power(t2,5) + 206*Power(t2,6) + 
                  7*Power(t2,7)) + 
               s1*(60 + 15*(-7 + 2*s)*t2 - 
                  (971 + 199*s + 30*Power(s,2))*Power(t2,2) + 
                  (357 - 684*s + 60*Power(s,2))*Power(t2,3) + 
                  (1537 + 765*s + 712*Power(s,2))*Power(t2,4) + 
                  (1669 + 280*s - 988*Power(s,2))*Power(t2,5) - 
                  (65 + 788*s + 109*Power(s,2))*Power(t2,6) + 
                  (-1621 + 577*s - 41*Power(s,2))*Power(t2,7) + 
                  4*(53 + 28*s)*Power(t2,8) + 5*Power(t2,9))) + 
            Power(t1,6)*(6 - 40*Power(s,3) + 40*Power(s1,3) + 75*t2 - 
               36*Power(t2,2) + 12*Power(s1,2)*(10 + 3*t2) + 
               4*Power(s,2)*(38 + 30*s1 + 9*t2) - 
               s1*(56 + 169*t2 + 66*Power(t2,2)) - 
               s*(46 + 120*Power(s1,2) - 59*t2 - 66*Power(t2,2) + 
                  8*s1*(34 + 9*t2))) + 
            Power(t1,5)*(24 + 1247*t2 - 141*Power(t2,2) - 
               382*Power(t2,3) - 36*Power(t2,4) - 
               2*Power(s1,3)*(143 + 283*t2 + 44*Power(t2,2)) + 
               Power(s,3)*(-72 + 199*t2 + 88*Power(t2,2)) + 
               Power(s1,2)*(-38 - 981*t2 - 223*Power(t2,2) + 
                  36*Power(t2,3)) + 
               s1*(792 + 1021*t2 + 769*Power(t2,2) + 654*Power(t2,3) + 
                  52*Power(t2,4)) + 
               Power(s,2)*(488 - 277*t2 - 509*Power(t2,2) + 
                  36*Power(t2,3) - 
                  2*s1*(-99 + 482*t2 + 132*Power(t2,2))) + 
               s*(Power(s1,2)*(160 + 1331*t2 + 264*Power(t2,2)) + 
                  s1*(-706 + 1540*t2 + 732*Power(t2,2) - 
                     72*Power(t2,3)) - 
                  2*(259 + 411*t2 + 115*Power(t2,2) + 308*Power(t2,3) + 
                     26*Power(t2,4)))) + 
            Power(t1,4)*(370 + 3897*t2 - 658*Power(t2,2) - 
               3329*Power(t2,3) + 866*Power(t2,4) + 266*Power(t2,5) + 
               6*Power(t2,6) + 
               Power(s,3)*(12 + 82*t2 - 52*Power(t2,2) - 
                  234*Power(t2,3) - 13*Power(t2,4)) + 
               Power(s1,3)*(-556 + 550*t2 + 1523*Power(t2,2) + 
                  440*Power(t2,3) + 13*Power(t2,4)) - 
               Power(s1,2)*(766 + 2955*t2 - 958*Power(t2,2) - 
                  764*Power(t2,3) + 199*Power(t2,4) + 19*Power(t2,5)) - 
               s1*(-590 + 2097*t2 + 4553*Power(t2,2) - 
                  208*Power(t2,3) + 827*Power(t2,4) + 
                  268*Power(t2,5) + 4*Power(t2,6)) + 
               Power(s,2)*(96 - 830*t2 - 2591*Power(t2,2) + 
                  56*Power(t2,3) - 108*Power(t2,4) - 19*Power(t2,5) + 
                  s1*(320 - 790*t2 + 879*Power(t2,2) + 
                     908*Power(t2,3) + 39*Power(t2,4))) + 
               s*(-476 - 1144*t2 + 3092*Power(t2,2) + 309*Power(t2,3) + 
                  757*Power(t2,4) + 282*Power(t2,5) + 4*Power(t2,6) + 
                  Power(s1,2)*
                   (344 + 778*t2 - 2350*Power(t2,2) - 
                     1114*Power(t2,3) - 39*Power(t2,4)) + 
                  s1*(34 + 6477*t2 + 2297*Power(t2,2) - 
                     1216*Power(t2,3) + 307*Power(t2,4) + 38*Power(t2,5)\
))) + Power(t1,3)*(210 + 663*t2 - 3747*Power(t2,2) - 
               13236*Power(t2,3) + 3778*Power(t2,4) + 
               1533*Power(t2,5) - 539*Power(t2,6) - 36*Power(t2,7) + 
               Power(s,3)*t2*
                (-69 - 127*t2 - 279*Power(t2,2) + 30*Power(t2,3) + 
                  14*Power(t2,4)) - 
               Power(s1,3)*(30 - 484*t2 + 90*Power(t2,2) + 
                  1272*Power(t2,3) + 663*Power(t2,4) + 41*Power(t2,5)) \
+ Power(s1,2)*(-982 + 1041*t2 + 6470*Power(t2,2) + 2706*Power(t2,3) - 
                  1380*Power(t2,4) + 88*Power(t2,5) + 61*Power(t2,6)) + 
               s1*(-164 - 3758*t2 - 3163*Power(t2,2) + 
                  7609*Power(t2,3) + 1874*Power(t2,4) - 
                  775*Power(t2,5) + 297*Power(t2,6) + 22*Power(t2,7)) + 
               Power(s,2)*(-24 - 1009*t2 - 297*Power(t2,2) + 
                  1499*Power(t2,3) + 2241*Power(t2,4) + 
                  454*Power(t2,5) + 62*Power(t2,6) + 
                  s1*(54 + 942*t2 - 260*Power(t2,2) + 
                     664*Power(t2,3) - 489*Power(t2,4) - 69*Power(t2,5)\
)) + s*(46 + 1339*t2 + 955*Power(t2,2) + 3234*Power(t2,3) - 
                  1893*Power(t2,4) - 135*Power(t2,5) - 
                  361*Power(t2,6) - 24*Power(t2,7) + 
                  Power(s1,2)*
                   (312 + 875*t2 - 657*Power(t2,2) + 300*Power(t2,3) + 
                     1122*Power(t2,4) + 96*Power(t2,5)) - 
                  s1*(274 - 1876*t2 + 2162*Power(t2,2) + 
                     8169*Power(t2,3) + 1011*Power(t2,4) + 
                     436*Power(t2,5) + 123*Power(t2,6)))) + 
            Power(t1,2)*(80 - 774*t2 - 1955*Power(t2,2) - 
               1374*Power(t2,3) + 3728*Power(t2,4) + 9282*Power(t2,5) - 
               2428*Power(t2,6) - 89*Power(t2,7) + 84*Power(t2,8) + 
               Power(t2,9) + Power(s,3)*t2*
                (-24 - 60*t2 + 445*Power(t2,2) + 129*Power(t2,3) + 
                  174*Power(t2,4) + 23*Power(t2,5)) + 
               Power(s1,3)*(60 + 60*t2 - 312*Power(t2,2) + 
                  52*Power(t2,3) + 349*Power(t2,4) + 363*Power(t2,5) + 
                  50*Power(t2,6)) + 
               Power(s1,2)*(-190 + 1019*t2 + 713*Power(t2,2) - 
                  2625*Power(t2,3) - 3379*Power(t2,4) + 
                  240*Power(t2,5) + 304*Power(t2,6) - 50*Power(t2,7)) + 
               s1*(-290 - 622*t2 + 3342*Power(t2,2) + 
                  4870*Power(t2,3) + 886*Power(t2,4) - 
                  6482*Power(t2,5) + 361*Power(t2,6) + 
                  169*Power(t2,7) - 29*Power(t2,8)) - 
               Power(s,2)*t2*
                (20 + 237*t2 - 1655*Power(t2,2) - 1744*Power(t2,3) + 
                  1306*Power(t2,4) + 962*Power(t2,5) + 
                  108*Power(t2,6) + 
                  s1*(-156 - 654*t2 + 2298*Power(t2,2) + 
                     57*Power(t2,3) + 314*Power(t2,4) + 11*Power(t2,5))) \
+ s*(-10 + 41*t2 - 1211*Power(t2,2) - 3094*Power(t2,3) + 
                  65*Power(t2,4) - 1243*Power(t2,5) + 390*Power(t2,6) + 
                  114*Power(t2,7) + 38*Power(t2,8) + 
                  Power(s1,2)*
                   (60 + 60*t2 - 6*Power(t2,2) - 693*Power(t2,3) + 
                     409*Power(t2,4) - 88*Power(t2,5) - 62*Power(t2,6)) \
+ s1*(-70 - 159*t2 + 420*Power(t2,2) - 709*Power(t2,3) - 
                     1071*Power(t2,4) + 2692*Power(t2,5) + 
                     664*Power(t2,6) + 150*Power(t2,7))))) - 
         Power(s2,2)*(-10*(-1 + s - s1)*Power(t1,8) + 
            Power(t2,3)*(-20 + (85 - 60*s + 30*s1)*t2 - 
               (122 + 60*Power(s,2) + 107*s1 + 6*Power(s1,2) - 
                  10*s*(25 + 3*s1))*Power(t2,2) + 
               (82 + 215*Power(s,2) - 20*Power(s,3) + 128*s1 + 
                  17*Power(s1,2) - 2*s*(152 + 49*s1))*Power(t2,3) + 
               (-71 + 50*Power(s,3) - 61*s1 - 13*Power(s1,2) - 
                  Power(s,2)*(192 + 5*s1) + 5*s*(26 + 17*s1))*
                Power(t2,4) + 
               (85 - 30*Power(s,3) + Power(s,2)*(43 - 8*s1) + 15*s1 - 
                  Power(s1,2) + s*(-95 + 3*s1 + Power(s1,2)))*
                Power(t2,5) - 
               (49 + 2*Power(s,3) + 2*s1 - 3*Power(s1,2) - 
                  3*Power(s,2)*(-5 + 4*s1) + 
                  s*(-83 + 21*s1 + Power(s1,2)))*Power(t2,6) + 
               (14 + 3*Power(s,2) - 5*s1 + s*(-4 + 3*s1))*Power(t2,7) + 
               2*(-2 + Power(s,2) + s1 - s*s1)*Power(t2,8)) + 
            t1*t2*(-60 + 12*(-11 + 3*s + 3*s1)*t2 + 
               2*(188 + 72*Power(s,2) + 155*s1 - 24*Power(s1,2) - 
                  s*(361 + 6*s1))*Power(t2,2) + 
               (-53 + 60*Power(s,3) - 704*s1 + 54*Power(s1,2) + 
                  12*Power(s1,3) + Power(s,2)*(-814 + 60*s1) + 
                  s*(1149 + 191*s1 - 30*Power(s1,2)))*Power(t2,3) + 
               (330 - 200*Power(s,3) + Power(s,2)*(807 - 175*s1) + 
                  296*s1 + 8*Power(s1,2) - 29*Power(s1,3) + 
                  s*(-375 - 194*s1 + 113*Power(s1,2)))*Power(t2,4) + 
               (-906 + 110*Power(s,3) - 68*s1 + 64*Power(s1,2) + 
                  13*Power(s1,3) + Power(s,2)*(-265 + 403*s1) + 
                  s*(1044 - 277*s1 - 102*Power(s1,2)))*Power(t2,5) + 
               (504 + 66*Power(s,3) + Power(s,2)*(224 - 243*s1) + 
                  74*s1 - 72*Power(s1,2) + 7*Power(s1,3) + 
                  s*(-1095 + 318*s1 + 4*Power(s1,2)))*Power(t2,6) + 
               (-191 + 16*Power(s,3) + Power(s,2)*(54 - 35*s1) + 
                  139*s1 - 13*Power(s1,2) - 3*Power(s1,3) + 
                  5*s*(-16 - 26*s1 + 5*Power(s1,2)))*Power(t2,7) + 
               (135 + 2*Power(s,3) - 89*s1 + 9*Power(s1,2) - 
                  Power(s,2)*(59 + 4*s1) + 
                  s*(5 + 57*s1 + 2*Power(s1,2)))*Power(t2,8) - 
               (3 + 6*Power(s,2) + s*(4 - 8*s1) - 6*s1 + 2*Power(s1,2))*
                Power(t2,9)) + 
            Power(t1,2)*(30 - 12*(48 - 17*s + 9*Power(s,2))*t2 - 
               6*(124 + 208*s - 101*Power(s,2) + 6*Power(s,3))*
                Power(t2,2) + 
               2*(151 + 57*s - 388*Power(s,2) + 84*Power(s,3))*
                Power(t2,3) + 
               2*(1409 - 1655*s + 554*Power(s,2) + 19*Power(s,3))*
                Power(t2,4) - 
               (1155 - 4056*s + 865*Power(s,2) + 380*Power(s,3))*
                Power(t2,5) + 
               (719 + 835*s - 903*Power(s,2) - 104*Power(s,3))*
                Power(t2,6) + 
               (-1339 + 140*s + 345*Power(s,2) - 55*Power(s,3))*
                Power(t2,7) + 
               (6 + 72*s + 134*Power(s,2) - 6*Power(s,3))*Power(t2,8) + 
               (-1 + s + 6*Power(s,2))*Power(t2,9) + 
               Power(s1,3)*Power(t2,2)*
                (-36 + 116*t2 - 7*Power(t2,2) - 105*Power(t2,3) + 
                  12*Power(t2,4) + 2*Power(t2,6)) + 
               Power(s1,2)*t2*
                (144 - 6*(49 + 6*s)*t2 - 10*(17 + 36*s)*Power(t2,2) + 
                  8*(-27 + 41*s)*Power(t2,3) + 
                  (555 + 232*s)*Power(t2,4) - 
                  4*(-53 + 58*s)*Power(t2,5) - 
                  (134 + 69*s)*Power(t2,6) - 
                  5*(-5 + 2*s)*Power(t2,7) + 4*Power(t2,8)) - 
               s1*(180 - 6*(-5 + 18*s)*t2 + 
                  12*(-133 - 8*s + 3*Power(s,2))*Power(t2,2) - 
                  2*(-294 + 89*s + 242*Power(s,2))*Power(t2,3) + 
                  (237 - 680*s + 1931*Power(s,2))*Power(t2,4) + 
                  (664 + 1433*s - 1207*Power(s,2))*Power(t2,5) + 
                  (1007 - 1369*s - 280*Power(s,2))*Power(t2,6) + 
                  (-924 + 377*s - 121*Power(s,2))*Power(t2,7) + 
                  (74 + 167*s - 14*Power(s,2))*Power(t2,8) + 
                  2*(2 + 5*s)*Power(t2,9))) - 
            2*Power(t1,7)*(30 + 23*Power(s,3) - 23*Power(s1,3) - 
               27*t2 - 22*Power(t2,2) - Power(s1,2)*(75 + 7*t2) - 
               Power(s,2)*(87 + 69*s1 + 7*t2) + 
               3*s1*(33 + 63*t2 + 10*Power(t2,2)) + 
               s*(-25 + 69*Power(s1,2) - 134*t2 - 30*Power(t2,2) + 
                  2*s1*(81 + 7*t2))) + 
            2*Power(t1,6)*(-307 + 10*t2 - 75*Power(t2,2) - 
               59*Power(t2,3) - 18*Power(t2,4) + 
               Power(s,3)*(-43 + 88*t2 + 36*Power(t2,2)) - 
               Power(s1,3)*(43 + 223*t2 + 36*Power(t2,2)) + 
               Power(s1,2)*(66 - 214*t2 + 121*Power(t2,2) + 
                  26*Power(t2,3)) + 
               s1*(454 + 412*t2 + 130*Power(t2,2) + 189*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s,2)*(386 + 246*t2 - 13*Power(t2,2) + 
                  26*Power(t2,3) - 3*s1*(-57 + 133*t2 + 36*Power(t2,2))\
) + s*(-168 + 19*t2 + 174*Power(t2,2) - 164*Power(t2,3) - 
                  15*Power(t2,4) + 
                  Power(s1,2)*(-85 + 534*t2 + 108*Power(t2,2)) - 
                  4*s1*(185 - 2*t2 + 27*Power(t2,2) + 13*Power(t2,3)))) \
+ Power(t1,5)*(232 + 4948*t2 + 1750*Power(t2,2) - 676*Power(t2,3) + 
               298*Power(t2,4) + 58*Power(t2,5) + 
               Power(s,3)*(10 + 194*t2 + 218*Power(t2,2) - 
                  98*Power(t2,3) - 5*Power(t2,4)) + 
               Power(s1,3)*(-278 + 312*t2 + 690*Power(t2,2) + 
                  204*Power(t2,3) + 5*Power(t2,4)) + 
               s1*(1232 + 114*t2 - 1252*Power(t2,2) + 
                  984*Power(t2,3) - 271*Power(t2,4) - 114*Power(t2,5)) \
- Power(s1,2)*(488 + 3968*t2 + 394*Power(t2,2) - 24*Power(t2,3) + 
                  293*Power(t2,4) + 19*Power(t2,5)) + 
               Power(s,2)*(408 + 60*t2 - 2106*Power(t2,2) - 
                  668*Power(t2,3) - 206*Power(t2,4) - 19*Power(t2,5) + 
                  s1*(598 - 780*t2 - 202*Power(t2,2) + 
                     400*Power(t2,3) + 15*Power(t2,4))) + 
               s*(-1016 - 3734*t2 + 232*Power(t2,2) - 
                  1240*Power(t2,3) - 25*Power(t2,4) + 110*Power(t2,5) - 
                  Power(s1,2)*
                   (490 - 610*t2 + 706*Power(t2,2) + 506*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  s1*(-864 + 6756*t2 + 3300*Power(t2,2) + 
                     576*Power(t2,3) + 499*Power(t2,4) + 38*Power(t2,5))\
)) + Power(t1,4)*(638 + 2652*t2 + 854*Power(t2,2) - 10756*Power(t2,3) - 
               857*Power(t2,4) + 170*Power(t2,5) - 106*Power(t2,6) + 
               Power(s1,3)*(-186 + 58*t2 + 88*Power(t2,2) - 
                  496*Power(t2,3) - 204*Power(t2,4) + Power(t2,5) + 
                  2*Power(t2,6)) - 
               Power(s,3)*(-38 + 228*t2 + 216*Power(t2,2) + 
                  452*Power(t2,3) + 178*Power(t2,4) + 19*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s1,2)*(-676 + 588*t2 + 3088*Power(t2,2) + 
                  1762*Power(t2,3) - 387*Power(t2,4) + 
                  270*Power(t2,5) + 68*Power(t2,6)) + 
               s1*(190 - 4818*t2 - 4352*Power(t2,2) + 
                  5822*Power(t2,3) + 52*Power(t2,4) - 
                  649*Power(t2,5) + 103*Power(t2,6) + 7*Power(t2,7)) + 
               Power(s,2)*(108 - 1936*t2 - 2032*Power(t2,2) - 
                  12*Power(t2,3) + 1602*Power(t2,4) + 
                  532*Power(t2,5) + 63*Power(t2,6) + 
                  s1*(-38 + 1794*t2 + 544*Power(t2,2) + 
                     936*Power(t2,3) + 318*Power(t2,4) + 
                     39*Power(t2,5) + 6*Power(t2,6))) - 
               s*(358 - 1734*t2 + 476*Power(t2,2) - 3338*Power(t2,3) - 
                  755*Power(t2,4) - 630*Power(t2,5) + 64*Power(t2,6) + 
                  7*Power(t2,7) + 
                  Power(s1,2)*
                   (-122 - 456*t2 + 752*Power(t2,2) + 
                     252*Power(t2,3) - 64*Power(t2,4) + 
                     21*Power(t2,5) + 6*Power(t2,6)) + 
                  s1*(648 - 2964*t2 - 4488*Power(t2,2) + 
                     4654*Power(t2,3) + 1683*Power(t2,4) + 
                     792*Power(t2,5) + 131*Power(t2,6)))) + 
            Power(t1,3)*(276 - 1022*t2 - 3350*Power(t2,2) - 
               708*Power(t2,3) - 911*Power(t2,4) + 5743*Power(t2,5) + 
               199*Power(t2,6) - 11*Power(t2,7) + 8*Power(t2,8) + 
               Power(s1,3)*(40 - 52*t2 - 150*Power(t2,2) + 
                  272*Power(t2,3) + 33*Power(t2,4) + 97*Power(t2,5) - 
                  4*Power(t2,6) - 4*Power(t2,7)) + 
               Power(s,3)*(4 + 2*t2 - 318*Power(t2,2) + 
                  678*Power(t2,3) + 236*Power(t2,4) + 272*Power(t2,5) + 
                  65*Power(t2,6) + 6*Power(t2,7)) + 
               s1*(-362 - 960*t2 + 1324*Power(t2,2) + 
                  3302*Power(t2,3) + 2952*Power(t2,4) - 
                  3834*Power(t2,5) + 225*Power(t2,6) + 
                  112*Power(t2,7) - 11*Power(t2,8)) + 
               Power(s1,2)*(-398 + 850*t2 + 942*Power(t2,2) - 
                  1756*Power(t2,3) - 1542*Power(t2,4) + 
                  349*Power(t2,5) + 19*Power(t2,6) - 78*Power(t2,7) - 
                  2*Power(t2,8)) - 
               Power(s,2)*(86 + (234 - 40*s1)*t2 + 
                  (1418 - 2366*s1)*Power(t2,2) + 
                  4*(-293 + 575*s1)*Power(t2,3) + 
                  (-2652 + 713*s1)*Power(t2,4) + 
                  (350 + 579*s1)*Power(t2,5) + 
                  2*(381 + 76*s1)*Power(t2,6) + 
                  2*(61 + 8*s1)*Power(t2,7) + 2*Power(t2,8)) + 
               s*(70 + 882*t2 + 2440*Power(t2,2) - 5130*Power(t2,3) - 
                  1323*Power(t2,4) - 1317*Power(t2,5) - 
                  394*Power(t2,6) - 89*Power(t2,7) + 10*Power(t2,8) + 
                  Power(s1,2)*
                   (180 + 154*t2 - 10*Power(t2,2) - 906*Power(t2,3) + 
                     666*Power(t2,4) + 267*Power(t2,5) + 
                     91*Power(t2,6) + 14*Power(t2,7)) + 
                  s1*(-108 + 8*t2 - 740*Power(t2,2) + 1776*Power(t2,3) - 
                     4141*Power(t2,4) + 1113*Power(t2,5) + 
                     851*Power(t2,6) + 200*Power(t2,7) + 4*Power(t2,8))))\
) + s2*(-((-1 + t2)*Power(t2,4)*(1 + (-1 + s)*t2)*
               (10 + (20*s - 6*(4 + s1))*t2 + 
                 (15 - 39*s + 10*Power(s,2) + 10*s1)*Power(t2,2) + 
                 (-9*Power(s,2) - 3*(2 + s1) + s*(7 + s1))*
                  Power(t2,3) + (6 + s*(-2 + s1) - s1)*Power(t2,4) + 
                 (-1 + s)*Power(t2,5))) + 
            t1*Power(t2,2)*(-60 - 20*(-13 + 9*s - 3*s1)*t2 - 
               5*(80 + 36*Power(s,2) + 39*s1 - 3*s*(49 + 2*s1))*
                Power(t2,2) + 
               (325 - 60*Power(s,3) - 30*Power(s,2)*(-20 + s1) + 
                  193*s1 - 10*Power(s1,2) + 
                  s*(-901 - 52*s1 + 6*Power(s1,2)))*Power(t2,3) + 
               (-295 + 125*Power(s,3) - 39*s1 + 32*Power(s1,2) + 
                  Power(s,2)*(-478 + 73*s1) + 
                  s*(442 - 64*s1 - 17*Power(s1,2)))*Power(t2,4) + 
               (293 - 53*Power(s,3) + Power(s,2)*(89 - 99*s1) - 
                  33*s1 - 32*Power(s1,2) + 
                  s*(-319 + 163*s1 + 16*Power(s1,2)))*Power(t2,5) + 
               (-149 - 16*Power(s,3) + 32*s1 + 8*Power(s1,2) + 
                  Power(s,2)*(-52 + 50*s1) + 
                  s*(233 - 82*s1 - 3*Power(s1,2)))*Power(t2,6) + 
               (-2*Power(s,3) + Power(s,2)*(7 + 4*s1) + 
                  s*(-8 + 11*s1 - 2*Power(s1,2)) + 
                  2*(17 - 12*s1 + Power(s1,2)))*Power(t2,7) + 
               (-8 + 6*Power(s,2) + 6*s1 - 2*s*(1 + 3*s1))*Power(t2,8)) \
+ Power(t1,2)*(-60 + 12*(-11 + 3*s + 3*s1)*t2 + 
               6*(61 + 24*Power(s,2) + 40*s1 - 18*Power(s1,2) + 
                  s*(-137 + 18*s1))*Power(t2,2) + 
               2*(-79 + 30*Power(s,3) - 242*s1 + 137*Power(s1,2) + 
                  6*Power(s1,3) + Power(s,2)*(-447 + 90*s1) + 
                  s*(632 - 202*s1 - 30*Power(s1,2)))*Power(t2,3) + 
               (738 - 180*Power(s,3) + Power(s,2)*(722 - 540*s1) + 
                  34*s1 - 269*Power(s1,2) - 24*Power(s1,3) + 
                  5*s*(-121 + 159*s1 + 36*Power(s1,2)))*Power(t2,4) + 
               (-1411 + 30*Power(s,3) + 225*s1 + 181*Power(s1,2) - 
                  3*Power(s1,3) + 3*Power(s,2)*(-73 + 273*s1) + 
                  s*(1655 - 990*s1 - 102*Power(s1,2)))*Power(t2,5) + 
               (780 + 147*Power(s,3) + Power(s,2)*(288 - 345*s1) - 
                  281*s1 - 54*Power(s1,2) + 23*Power(s1,3) + 
                  s*(-1391 + 628*s1 - 79*Power(s1,2)))*Power(t2,6) + 
               (-268 + 25*Power(s,3) + Power(s,2)*(139 - 83*s1) + 
                  338*s1 - 35*Power(s1,2) - 7*Power(s1,3) + 
                  s*(-179 - 267*s1 + 63*Power(s1,2)))*Power(t2,7) + 
               (141 + 7*Power(s,3) - 114*s1 + 16*Power(s1,2) - 
                  Power(s1,3) - 4*Power(s,2)*(19 + 4*s1) + 
                  s*(10 + 73*s1 + 10*Power(s1,2)))*Power(t2,8) + 
               (4 - 12*Power(s,2) + 6*s1 - 5*Power(s1,2) + 
                  2*s*(-5 + 9*s1))*Power(t2,9)) + 
            4*Power(t1,8)*(1 + 4*Power(s,2) + 4*Power(s1,2) + 3*t2 + 
               4*s*(3 - 2*s1 + t2) - 2*s1*(11 + 2*t2)) + 
            2*Power(t1,7)*(4*Power(s,3)*(1 + 7*t2) - 
               4*Power(s1,3)*(12 + 7*t2) + 
               Power(s1,2)*(-70 + 62*t2 + 7*Power(t2,2)) + 
               2*(-44 - 40*t2 + Power(t2,2) - 6*Power(t2,3)) + 
               s1*(184 + 18*t2 + 56*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s,2)*(122 - 26*t2 + 7*Power(t2,2) - 
                  28*s1*(2 + 3*t2)) + 
               s*(-12 + 186*t2 - 37*Power(t2,2) - 11*Power(t2,3) + 
                  4*Power(s1,2)*(25 + 21*t2) - 
                  2*s1*(34 + 18*t2 + 7*Power(t2,2)))) - 
            Power(t1,6)*(-972 - 1116*t2 + 66*Power(t2,2) - 
               164*Power(t2,3) - 21*Power(t2,4) - 
               4*Power(s1,3)*
                (-26 + 81*t2 + 16*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,3)*(64 - 228*t2 + 58*Power(t2,2) + 
                  20*Power(t2,3)) + 
               2*Power(s1,2)*
                (428 + 436*t2 - 17*Power(t2,2) + 76*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s1*(-280 + 92*t2 - 614*Power(t2,2) + 34*Power(t2,3) + 
                  61*Power(t2,4)) + 
               Power(s,2)*(-360 + 984*t2 + 206*Power(t2,2) + 
                  60*Power(t2,3) + 18*Power(t2,4) - 
                  4*s1*(22 - 105*t2 + 45*Power(t2,2) + 15*Power(t2,3))) \
+ s*(1208 + 44*t2 + 820*Power(t2,2) + 294*Power(t2,3) - 
                  55*Power(t2,4) + 
                  6*Power(s1,2)*
                   (-24 + 22*t2 + 31*Power(t2,2) + 10*Power(t2,3)) - 
                  4*s1*(364 + 608*t2 + 35*Power(t2,2) + 
                     53*Power(t2,3) + 9*Power(t2,4)))) + 
            Power(t1,5)*(816 + 1832*t2 - 4572*Power(t2,2) - 
               1074*Power(t2,3) - 67*Power(t2,4) - 57*Power(t2,5) + 
               2*Power(t2,6) - 
               Power(s,3)*(88 + 108*t2 + 264*Power(t2,2) + 
                  166*Power(t2,3) - 11*Power(t2,4) + Power(t2,5)) + 
               Power(s1,3)*(-176 + 100*t2 - 138*Power(t2,2) - 
                  114*Power(t2,3) + 2*Power(t2,4) + Power(t2,5)) + 
               Power(s1,2)*(-256 + 1416*t2 + 750*Power(t2,2) - 
                  44*Power(t2,3) + 163*Power(t2,4) + 64*Power(t2,5)) + 
               s1*(-936 - 3240*t2 + 2774*Power(t2,2) + 
                  26*Power(t2,3) - 537*Power(t2,4) + 36*Power(t2,5) + 
                  5*Power(t2,6)) + 
               Power(s,2)*(-272 - 1112*t2 + 26*Power(t2,2) + 
                  680*Power(t2,3) + 247*Power(t2,4) + 53*Power(t2,5) + 
                  s1*(640 + 540*t2 + 502*Power(t2,2) + 
                     394*Power(t2,3) - 20*Power(t2,4) + 3*Power(t2,5))) \
+ s*(8 - 360*t2 + 998*Power(t2,2) + 674*Power(t2,3) + 682*Power(t2,4) + 
                  39*Power(t2,5) - 5*Power(t2,6) - 
                  Power(s1,2)*
                   (-520 + 596*t2 + 244*Power(t2,2) + 
                     114*Power(t2,3) - 7*Power(t2,4) + 3*Power(t2,5)) - 
                  s1*(-592 - 3632*t2 + 2072*Power(t2,2) + 
                     1180*Power(t2,3) + 396*Power(t2,4) + 
                     117*Power(t2,5)))) + 
            Power(t1,4)*(-84 - 1924*t2 - 124*Power(t2,2) - 
               1422*Power(t2,3) + 3095*Power(t2,4) + 527*Power(t2,5) + 
               19*Power(t2,6) + 4*Power(t2,7) + 
               Power(s1,3)*(56 - 276*t2 + 356*Power(t2,2) - 
                  44*Power(t2,3) + 54*Power(t2,4) - 11*Power(t2,5) - 
                  7*Power(t2,6)) + 
               Power(s,3)*(16 - 212*t2 + 478*Power(t2,2) + 
                  276*Power(t2,3) + 194*Power(t2,4) + 58*Power(t2,5) + 
                  5*Power(t2,6)) + 
               s1*(-544 + 520*t2 + 578*Power(t2,2) + 
                  3102*Power(t2,3) - 2325*Power(t2,4) + 
                  65*Power(t2,5) + 133*Power(t2,6) - 7*Power(t2,7)) - 
               Power(s1,2)*(280 - 1208*t2 + 774*Power(t2,2) + 
                  1422*Power(t2,3) - 467*Power(t2,4) - 9*Power(t2,5) + 
                  84*Power(t2,6) + 3*Power(t2,7)) - 
               Power(s,2)*(56 + 1112*t2 - 62*Power(t2,2) - 
                  1686*Power(t2,3) - 114*Power(t2,4) + 
                  467*Power(t2,5) + 105*Power(t2,6) + 3*Power(t2,7) + 
                  s1*(40 - 1652*t2 + 824*Power(t2,2) + 
                     1012*Power(t2,3) + 418*Power(t2,4) + 
                     165*Power(t2,5) + 17*Power(t2,6))) + 
               s*(104 + 2392*t2 - 2782*Power(t2,2) - 946*Power(t2,3) - 
                  698*Power(t2,4) - 451*Power(t2,5) - 173*Power(t2,6) + 
                  2*Power(t2,7) + 
                  Power(s1,2)*
                   (288 + 244*t2 - 1322*Power(t2,2) + 
                     780*Power(t2,3) + 230*Power(t2,4) + 
                     118*Power(t2,5) + 19*Power(t2,6)) + 
                  s1*(336 - 1440*t2 + 2312*Power(t2,2) - 
                     3176*Power(t2,3) + 143*Power(t2,4) + 
                     638*Power(t2,5) + 188*Power(t2,6) + 6*Power(t2,7)))\
) - Power(t1,3)*(192 + 744*t2 + 92*Power(t2,2) - 2510*Power(t2,3) + 
               1237*Power(t2,4) - 836*Power(t2,5) + 951*Power(t2,6) + 
               89*Power(t2,7) + Power(t2,8) - 
               Power(s1,3)*t2*
                (4 - 10*t2 + 126*Power(t2,2) - 156*Power(t2,3) + 
                  11*Power(t2,4) + 7*Power(t2,5) + 6*Power(t2,6)) + 
               Power(s,3)*t2*
                (-4 - 68*t2 - 138*Power(t2,2) + 465*Power(t2,3) + 
                  101*Power(t2,4) + 67*Power(t2,5) + 9*Power(t2,6)) - 
               Power(s1,2)*(108 - 404*t2 + 340*Power(t2,2) - 
                  496*Power(t2,3) + 369*Power(t2,4) + 320*Power(t2,5) - 
                  194*Power(t2,6) + 33*Power(t2,7) + 8*Power(t2,8)) + 
               s1*(360 - 1348*t2 + 414*Power(t2,2) + 612*Power(t2,3) - 
                  716*Power(t2,4) + 1561*Power(t2,5) - 
                  756*Power(t2,6) + 39*Power(t2,7) + 10*Power(t2,8)) - 
               Power(s,2)*(-36 - 4*(-79 + 9*s1)*t2 + 
                  (-360 + 574*s1)*Power(t2,2) + 
                  (908 - 1926*s1)*Power(t2,3) + 
                  (-421 + 862*s1)*Power(t2,4) + 
                  11*(-93 + 37*s1)*Power(t2,5) + 
                  (197 + 158*s1)*Power(t2,6) + 
                  (136 + 27*s1)*Power(t2,7) + 10*Power(t2,8)) + 
               s*(-48 + 620*t2 - 300*Power(t2,2) + 3472*Power(t2,3) - 
                  3095*Power(t2,4) - 1004*Power(t2,5) - 
                  128*Power(t2,6) - 110*Power(t2,7) - 13*Power(t2,8) + 
                  Power(s1,2)*t2*
                   (36 + 200*t2 + 66*Power(t2,2) - 615*Power(t2,3) + 
                     305*Power(t2,4) + 105*Power(t2,5) + 24*Power(t2,6)) \
+ s1*(-216 - 120*t2 + 652*Power(t2,2) - 1604*Power(t2,3) + 
                     2104*Power(t2,4) - 1581*Power(t2,5) + 
                     170*Power(t2,6) + 193*Power(t2,7) + 18*Power(t2,8)))\
)) + Power(s2,3)*(-2*Power(t1,7)*
             (6 + s + 11*Power(s,2) - 22*s1 - 22*s*s1 + 11*Power(s1,2) - 
               16*t2 + 21*s*t2 - 21*s1*t2) + 
            Power(t2,2)*(-20 + 4*(-11 + 3*s + 3*s1)*t2 + 
               (127 + 48*Power(s,2) + 115*s1 - 6*Power(s1,2) - 
                  8*s*(28 + 3*s1))*Power(t2,2) + 
               (3 - 258*Power(s,2) + 20*Power(s,3) - 264*s1 - 
                  24*Power(s1,2) + 2*Power(s1,3) + s*(363 + 157*s1))*
                Power(t2,3) + 
               (33 - 70*Power(s,3) + 101*s1 + 64*Power(s1,2) - 
                  4*Power(s1,3) + 2*Power(s,2)*(137 + 5*s1) + 
                  s*(-64 - 215*s1 + 5*Power(s1,2)))*Power(t2,4) + 
               (-207 + 50*Power(s,3) - 9*s1 - 13*Power(s1,2) + 
                  Power(s,2)*(-62 + 42*s1) + 
                  s*(179 + s1 - 4*Power(s1,2)))*Power(t2,5) + 
               (110 + 12*Power(s,3) + Power(s,2)*(32 - 53*s1) + 
                  54*s1 - 21*Power(s1,2) + 2*Power(s1,3) + 
                  s*(-275 + 76*s1 + 2*Power(s1,2)))*Power(t2,6) + 
               (8*Power(s,2) + s*(-10 - 23*s1 + Power(s1,2)) - 
                  2*(20 - 7*s1 + Power(s1,2)))*Power(t2,7) + 
               (41 - 16*Power(s,2) - 25*s1 + 2*Power(s1,2) + 
                  5*s*(1 + 3*s1))*Power(t2,8) + (-3 + 2*s1)*Power(t2,9)) \
- t1*(-20 - 60*(-1 + s - 3*s1)*t2 + 
               2*(188 + 24*Power(s,2) - 35*s1 - 72*Power(s1,2) + 
                  s*(-47 + 6*s1))*Power(t2,2) + 
               2*(199 + 16*Power(s,3) - 575*s1 + 94*Power(s1,2) + 
                  16*Power(s1,3) + Power(s,2)*(-236 + 6*s1) + 
                  s*(487 + 16*s1 + 6*Power(s1,2)))*Power(t2,3) + 
               (-398 - 166*Power(s,3) + Power(s,2)*(708 - 128*s1) + 
                  440*s1 + 340*Power(s1,2) - 83*Power(s1,3) + 
                  s*(23 - 646*s1 + 180*Power(s1,2)))*Power(t2,4) + 
               (-1542 + 84*Power(s,3) + 292*s1 - 137*Power(s1,2) + 
                  15*Power(s1,3) + Power(s,2)*(-526 + 772*s1) + 
                  s*(1222 + 95*s1 - 167*Power(s1,2)))*Power(t2,5) + 
               (545 + 160*Power(s,3) + Power(s,2)*(468 - 635*s1) + 
                  757*s1 - 276*Power(s1,2) + 49*Power(s1,3) + 
                  s*(-2313 + 488*s1 - 52*Power(s1,2)))*Power(t2,6) + 
               (-174 + 46*Power(s,3) + Power(s,2)*(353 - 100*s1) + 
                  202*s1 - 111*Power(s1,2) - 4*Power(s1,3) + 
                  s*(-301 - 482*s1 + 94*Power(s1,2)))*Power(t2,7) + 
               (11*Power(s,3) - 3*Power(s,2)*(82 + 7*s1) + 
                  s*(13 + 244*s1 + 8*Power(s1,2)) + 
                  3*(263 - 177*s1 + 21*Power(s1,2) + Power(s1,3)))*
                Power(t2,8) - 
               (42*Power(s,2) + s*(13 - 49*s1) + 
                  7*(11 - 8*s1 + Power(s1,2)))*Power(t2,9) + 
               3*(1 + s)*Power(t2,10)) - 
            2*Power(t1,6)*(125 + 72*t2 - 106*Power(t2,2) - 
               30*Power(t2,3) + 9*Power(s,3)*(1 + 6*t2) - 
               2*Power(s1,3)*(53 + 27*t2) + 
               Power(s1,2)*(-169 - 158*t2 + 12*Power(t2,2)) + 
               3*s1*(79 + 99*t2 + 114*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s,2)*(31 - 248*t2 + 12*Power(t2,2) - 
                  2*s1*(62 + 81*t2)) + 
               s*(-172 - 65*t2 - 289*Power(t2,2) - 42*Power(t2,3) + 
                  Power(s1,2)*(221 + 162*t2) + 
                  s1*(170 + 406*t2 - 24*Power(t2,2)))) + 
            Power(t1,5)*(2*Power(s,3)*
                (22 - 69*t2 + 155*Power(t2,2) + 25*Power(t2,3)) - 
               2*Power(s1,3)*
                (-135 + 479*t2 + 316*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s1,2)*(982 + 120*t2 - 764*Power(t2,2) + 
                  272*Power(t2,3) + 57*Power(t2,4)) - 
               2*(538 + 639*t2 - 839*Power(t2,2) + 166*Power(t2,3) + 
                  139*Power(t2,4) + 12*Power(t2,5)) + 
               s1*(346 + 3122*t2 + 562*Power(t2,2) + 744*Power(t2,3) + 
                  477*Power(t2,4) + 18*Power(t2,5)) + 
               Power(s,2)*(26 + 2372*t2 + 272*Power(t2,2) + 
                  74*Power(t2,3) + 57*Power(t2,4) - 
                  2*s1*(-147 - 3*t2 + 626*Power(t2,2) + 75*Power(t2,3))) \
+ s*(610 - 1622*t2 - 22*Power(t2,2) - 318*Power(t2,3) - 
                  465*Power(t2,4) - 18*Power(t2,5) + 
                  2*Power(s1,2)*
                   (-424 + 545*t2 + 787*Power(t2,2) + 75*Power(t2,3)) - 
                  2*s1*(1008 + 1806*t2 - 412*Power(t2,2) + 
                     173*Power(t2,3) + 57*Power(t2,4)))) + 
            Power(t1,4)*(-(Power(s1,3)*
                  (168 + 300*t2 - 908*Power(t2,2) - 986*Power(t2,3) - 
                    158*Power(t2,4) + Power(t2,5))) + 
               Power(s,3)*(30 + 84*t2 + 376*Power(t2,2) + 
                  76*Power(t2,3) - 66*Power(t2,4) + Power(t2,5)) + 
               s1*(1158 + 3710*t2 - 3372*Power(t2,2) - 
                  2616*Power(t2,3) + 1054*Power(t2,4) - 
                  457*Power(t2,5) - 85*Power(t2,6)) - 
               Power(s1,2)*(-770 + 4446*t2 + 5018*Power(t2,2) - 
                  862*Power(t2,3) + 92*Power(t2,4) + 220*Power(t2,5) + 
                  7*Power(t2,6)) + 
               2*(-162 + 787*t2 + 5521*Power(t2,2) - 23*Power(t2,3) - 
                  841*Power(t2,4) + 242*Power(t2,5) + 34*Power(t2,6)) - 
               Power(s,2)*(-238 - 702*t2 + 920*Power(t2,2) + 
                  2914*Power(t2,3) + 803*Power(t2,4) + 
                  202*Power(t2,5) + 7*Power(t2,6) + 
                  s1*(196 - 396*t2 + 1148*Power(t2,2) - 
                     374*Power(t2,3) - 290*Power(t2,4) + 3*Power(t2,5))) \
+ s*(-306 - 2338*t2 - 4736*Power(t2,2) + 1200*Power(t2,3) - 
                  638*Power(t2,4) + 411*Power(t2,5) + 87*Power(t2,6) + 
                  Power(s1,2)*
                   (-914 + 204*t2 + 480*Power(t2,2) - 
                     1436*Power(t2,3) - 382*Power(t2,4) + 3*Power(t2,5)) \
+ 2*s1*(-568 + 224*t2 + 5101*Power(t2,2) + 1366*Power(t2,3) + 
                     374*Power(t2,4) + 211*Power(t2,5) + 7*Power(t2,6)))) \
+ Power(t1,3)*(188 + 1986*t2 + 2962*Power(t2,2) - 2418*Power(t2,3) - 
               13324*Power(t2,4) + 1076*Power(t2,5) + 339*Power(t2,6) - 
               127*Power(t2,7) - 2*Power(t2,8) + 
               Power(s1,3)*(-170 + 162*t2 + 92*Power(t2,2) - 
                  222*Power(t2,3) - 590*Power(t2,4) - 173*Power(t2,5) + 
                  3*Power(t2,6)) - 
               Power(s,3)*(-8 - 86*t2 + 506*Power(t2,2) + 
                  230*Power(t2,3) + 412*Power(t2,4) + 96*Power(t2,5) + 
                  13*Power(t2,6)) + 
               Power(s1,2)*(-128 - 1548*t2 + 1976*Power(t2,2) + 
                  4744*Power(t2,3) + 967*Power(t2,4) - 
                  512*Power(t2,5) + 201*Power(t2,6) + 22*Power(t2,7)) + 
               s1*(834 - 2144*t2 - 6556*Power(t2,2) - 
                  3386*Power(t2,3) + 7779*Power(t2,4) + 
                  50*Power(t2,5) - 436*Power(t2,6) + 96*Power(t2,7) + 
                  3*Power(t2,8)) + 
               Power(s,2)*(-20 + 120*t2 - 2512*Power(t2,2) - 
                  2462*Power(t2,3) + 838*Power(t2,4) + 
                  1851*Power(t2,5) + 413*Power(t2,6) + 24*Power(t2,7) + 
                  s1*(-42 - 634*t2 + 2784*Power(t2,2) + 
                     322*Power(t2,3) + 841*Power(t2,4) + 
                     98*Power(t2,5) + 29*Power(t2,6))) + 
               s*(8 + 76*t2 + 3556*Power(t2,2) + 228*Power(t2,3) + 
                  3327*Power(t2,4) + 11*Power(t2,5) + 113*Power(t2,6) - 
                  107*Power(t2,7) - 3*Power(t2,8) + 
                  Power(s1,2)*
                   (-100 + 66*t2 + 718*Power(t2,2) - 662*Power(t2,3) - 
                     102*Power(t2,4) + 171*Power(t2,5) - 19*Power(t2,6)) \
- s1*(332 - 244*t2 - 2416*Power(t2,2) - 2698*Power(t2,3) + 
                     4728*Power(t2,4) + 1539*Power(t2,5) + 
                     594*Power(t2,6) + 46*Power(t2,7)))) + 
            Power(t1,2)*(110 + 258*t2 - 1482*Power(t2,2) - 
               3366*Power(t2,3) - 445*Power(t2,4) + 587*Power(t2,5) + 
               5079*Power(t2,6) - 481*Power(t2,7) - 2*Power(t2,8) + 
               8*Power(t2,9) + 
               Power(s1,3)*t2*
                (60 - 72*t2 - 134*Power(t2,2) + 190*Power(t2,3) + 
                  90*Power(t2,4) + 73*Power(t2,5) + Power(t2,6)) + 
               Power(s,3)*t2*(12 - 100*t2 - 132*Power(t2,2) + 
                  556*Power(t2,3) + 172*Power(t2,4) + 146*Power(t2,5) + 
                  23*Power(t2,6)) + 
               Power(s1,2)*(-180 + 6*t2 + 1110*Power(t2,2) + 
                  78*Power(t2,3) - 1544*Power(t2,4) - 1240*Power(t2,5) + 
                  309*Power(t2,6) + 11*Power(t2,7) - 24*Power(t2,8)) + 
               s1*(140 - 1396*t2 - 116*Power(t2,2) + 2212*Power(t2,3) + 
                  3543*Power(t2,4) + 1153*Power(t2,5) - 
                  3400*Power(t2,6) + 280*Power(t2,7) + 41*Power(t2,8) - 
                  5*Power(t2,9)) - 
               Power(s,2)*t2*(218 - 224*t2 + 1202*Power(t2,2) - 
                  1514*Power(t2,3) - 2010*Power(t2,4) + 
                  849*Power(t2,5) + 522*Power(t2,6) + 43*Power(t2,7) + 
                  s1*(60 - 120*t2 - 1846*Power(t2,2) + 
                     2256*Power(t2,3) + 358*Power(t2,4) + 
                     301*Power(t2,5) + 47*Power(t2,6))) + 
               s*(50 + 600*t2 + 402*Power(t2,2) + 1208*Power(t2,3) - 
                  5053*Power(t2,4) - 1009*Power(t2,5) - 663*Power(t2,6) - 
                  82*Power(t2,7) + 20*Power(t2,8) + 6*Power(t2,9) + 
                  Power(s1,2)*t2*
                   (180 + 124*t2 - 124*Power(t2,2) - 456*Power(t2,3) + 
                     394*Power(t2,4) + 115*Power(t2,5) + 23*Power(t2,6)) \
+ s1*(-60 - 44*t2 - 662*Power(t2,2) + 380*Power(t2,3) + 676*Power(t2,4) - 
                     2467*Power(t2,5) + 1265*Power(t2,6) + 
                     533*Power(t2,7) + 66*Power(t2,8))))))*
       T2q(t1,1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*Power(1 - s2 + t1 - t2,2)*
       Power(t1 - s2*t2,3)*Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - 
         Power(t2,2),2)) + (8*(Power(s2,10)*
          (Power(s1,3) + s1*Power(t2,2) + 2*Power(t2,3)) - 
         Power(s2,9)*(Power(s1,3)*(1 + 7*t1 - 3*t2) + 
            Power(s1,2)*(3 + (-2 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            Power(t2,2)*(1 + 11*t2 - 6*Power(t2,2) + 
               s*(3 - 4*t1 + 4*t2) + 2*t1*(3 + 5*t2)) + 
            s1*t2*((6 - 5*t2)*t2 + s*(3 - 3*t1 + t2) + t1*(2 + 6*t2))) + 
         Power(s2,8)*(Power(s1,3)*
             (-3 + 21*Power(t1,2) + t1*(5 - 20*t2) + t2 + 3*Power(t2,2)) \
+ Power(s1,2)*(3 - (13 + 5*s)*t2 + (-2 + 3*s)*Power(t2,2) + 
               6*Power(t2,3) + Power(t1,2)*(1 - 18*s + 6*t2) + 
               t1*(19 - 17*t2 - 14*Power(t2,2) + s*(-7 + 6*t2))) + 
            s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 4*t2 + 2*Power(t2,2) - 
               14*Power(t2,3) + 9*Power(t2,4) + 
               t1*t2*(14 + 22*t2 - 27*Power(t2,2)) + 
               Power(t1,2)*(1 + 12*t2 + 15*Power(t2,2)) - 
               s*(Power(t1,2)*(3 + 17*t2) + 
                  t1*(3 - 15*t2 - 20*Power(t2,2)) + 
                  t2*(-4 + 4*t2 + 5*Power(t2,2)))) + 
            t2*(Power(s,2)*(-3*t1 + 3*Power(t1,2) + t2 - 5*t1*t2 + 
                  2*Power(t2,2)) + 
               Power(t1,2)*(6 + 30*t2 + 20*Power(t2,2)) + 
               t1*(2 + 39*t2 + 32*Power(t2,2) - 25*Power(t2,3)) + 
               t2*(6 + 10*t2 - 35*Power(t2,2) + 6*Power(t2,3)) + 
               s*(3 + 6*t2 - 9*Power(t2,2) - 12*Power(t2,3) - 
                  Power(t1,2)*(8 + 19*t2) + 
                  t1*(3 + 14*t2 + 32*Power(t2,2))))) + 
         Power(t1,3)*((1 + s1)*Power(t1,6) - Power(-1 + t2,4)*t2 + 
            t1*Power(-1 + t2,3)*(18 - 9*s1 + 8*t2 + Power(t2,2)) + 
            Power(t1,5)*(13 + 2*Power(s1,3) - 5*t2 + 
               Power(s1,2)*(1 + t2) - s1*(13 + t2)) + 
            Power(t1,4)*(27 - 30*t2 - 3*Power(s1,3)*t2 + 
               13*Power(t2,2) + s1*(-43 + 25*t2) + 
               Power(s1,2)*(21 + 2*t2 - 4*Power(t2,2))) - 
            Power(t1,2)*Power(-1 + t2,2)*
             (20 + 5*Power(s1,2)*(-1 + t2) + 39*t2 - 14*Power(t2,2) + 
               2*s1*(-5 - 13*t2 + 4*Power(t2,2))) + 
            Power(t1,3)*(-1 + t2)*
             (3 + 38*t2 + 2*Power(s1,3)*t2 - 24*Power(t2,2) + 
               Power(s1,2)*(-13 - 3*t2 + 3*Power(t2,2)) + 
               s1*(4 - 25*t2 + 8*Power(t2,2))) - 
            Power(s,3)*(4 + Power(t1,5) - 6*t2 + 2*Power(t2,2) - 
               2*Power(t2,3) + 3*Power(t2,4) - Power(t2,5) - 
               2*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(1 + 7*t2 + Power(t2,2)) - 
               Power(t1,2)*(-24 + 8*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               t1*(22 - 30*t2 + 9*Power(t2,2) - 3*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s,2)*(Power(t1,5)*(-1 + 4*s1 + t2) - 
               Power(-1 + t2,2)*
                (-2 + 4*t2 - 6*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,4)*(10 - 7*t2 + 4*Power(t2,2) + 
                  s1*(2 + 7*t2)) + 
               Power(t1,3)*(-15 + 11*t2 - 17*Power(t2,2) + 
                  6*Power(t2,3) + 2*s1*(-10 + 7*t2 + 3*Power(t2,2))) + 
               Power(t1,2)*(22 - 41*t2 + 9*Power(t2,2) + 
                  14*Power(t2,3) - 4*Power(t2,4) + 
                  s1*(-18 + 24*t2 - 7*Power(t2,3))) + 
               t1*(-1 + t2)*(-18 + 42*t2 - 21*Power(t2,2) + 
                  Power(t2,3) + Power(t2,4) + 
                  s1*(-2 + 4*t2 - 8*Power(t2,2) + 4*Power(t2,3)))) - 
            s*(Power(t1,6) - Power(-1 + t2,3)*
                (-4 - 8*t2 + 3*Power(t2,2)) + 
               Power(t1,5)*(5*Power(s1,2) + 2*s1*t2 - 2*(3 + t2)) + 
               Power(t1,4)*(-22 + 26*t2 - 8*Power(s1,2)*t2 + 
                  Power(t2,2) + s1*(23 + 3*t2 - 8*Power(t2,2))) + 
               t1*Power(-1 + t2,2)*
                (-37 - 44*t2 - 5*Power(t2,2) + 2*Power(t2,3) + 
                  s1*(16 - 7*t2 + 8*Power(t2,2))) - 
               Power(t1,2)*(-1 + t2)*
                (-47 - 74*t2 - 12*Power(t2,2) + 4*Power(t2,3) + 
                  Power(s1,2)*(2 - 8*t2 + 5*Power(t2,2)) + 
                  s1*(21 + 21*t2 - 10*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,3)*(-53 + 9*t2 - 24*Power(t2,2) + 
                  2*Power(t2,3) + 
                  Power(s1,2)*(-9 - 3*t2 + 8*Power(t2,2)) + 
                  s1*(52 - 6*t2 - 25*Power(t2,2) + 10*Power(t2,3))))) - 
         Power(s2,7)*(1 - 2*t2 + 5*s*t2 + 4*Power(t2,2) - 
            16*s*Power(t2,2) - 6*Power(s,2)*Power(t2,2) - 
            3*Power(t2,3) - 31*s*Power(t2,3) - 
            10*Power(s,2)*Power(t2,3) - 27*Power(t2,4) + s*Power(t2,4) - 
            6*Power(s,2)*Power(t2,4) + 39*Power(t2,5) + 
            12*s*Power(t2,5) - 2*Power(t2,6) + 
            Power(t1,2)*(1 + 
               (45 - s - 15*Power(s,2) + 2*Power(s,3))*t2 + 
               (147 + 63*s - 32*Power(s,2))*Power(t2,2) + 
               8*(2 + 11*s)*Power(t2,3) - 40*Power(t2,4)) + 
            Power(t1,3)*(2 - Power(s,3) + 30*t2 + 60*Power(t2,2) + 
               20*Power(t2,3) + Power(s,2)*(3 + 13*t2) - 
               2*s*(2 + 19*t2 + 18*Power(t2,2))) + 
            Power(s1,3)*(-1 + 35*Power(t1,3) + 
               Power(t1,2)*(8 - 57*t2) + 3*t2 - 4*Power(t2,2) - 
               Power(t2,3) + t1*(-21 + 11*t2 + 19*Power(t2,2))) + 
            t1*t2*(13 + 67*t2 - Power(s,3)*t2 - 71*Power(t2,2) - 
               115*Power(t2,3) + 23*Power(t2,4) + 
               Power(s,2)*(-8 + 14*t2 + 25*Power(t2,2)) + 
               s*(25 + 16*t2 - 35*Power(t2,2) - 59*Power(t2,3))) + 
            Power(s1,2)*(-9 + (10 + s)*t2 + (14 + 9*s)*Power(t2,2) + 
               (10 - 3*s)*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,3)*(6 - 45*s + 15*t2) + 
               t1*(11 - 85*t2 + 34*Power(t2,3) - 
                  s*(-1 + 16*t2 + Power(t2,2))) + 
               Power(t1,2)*(s*(-33 + 51*t2) - 
                  8*(-6 + 8*t2 + 5*Power(t2,2)))) + 
            s1*(3 - (17 + 16*s)*t2 + 
               2*(-1 + 2*s + Power(s,2))*Power(t2,2) + 
               (-8 + 6*s)*Power(t2,3) + (13 + 9*s)*Power(t2,4) - 
               7*Power(t2,5) + 
               Power(t1,3)*(6 + 15*Power(s,2) + 30*t2 + 
                  20*Power(t2,2) - s*(17 + 40*t2)) + 
               Power(t1,2)*(8 + Power(s,2)*(8 - 17*t2) + 71*t2 + 
                  16*Power(t2,2) - 59*Power(t2,3) + 
                  s*(-22 + 57*t2 + 84*Power(t2,2))) + 
               t1*(17 - 22*t2 - 29*Power(t2,2) - 50*Power(t2,3) + 
                  42*Power(t2,4) + 2*Power(s,2)*t2*(3 + 2*t2) - 
                  s*(8 - 55*t2 + 12*Power(t2,2) + 50*Power(t2,3))))) + 
         Power(s2,6)*(1 - 7*t2 - 11*s*t2 - 11*s*Power(t2,2) - 
            12*Power(s,2)*Power(t2,2) + 5*Power(t2,3) + 
            13*s*Power(t2,3) + 14*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) - 21*Power(t2,4) + 
            58*s*Power(t2,4) + 28*Power(s,2)*Power(t2,4) + 
            39*Power(t2,5) + 21*s*Power(t2,5) + 
            6*Power(s,2)*Power(t2,5) - 17*Power(t2,6) - 
            4*s*Power(t2,6) + 
            Power(t1,4)*(-4*Power(s,3) + Power(s,2)*(13 + 22*t2) - 
               s*(19 + 72*t2 + 34*Power(t2,2)) + 
               10*(1 + 6*t2 + 6*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(35*Power(t1,4) - Power(t1,3)*(1 + 90*t2) + 
               Power(t1,2)*(-62 + 47*t2 + 50*Power(t2,2)) + 
               2*(1 - 2*t2 + Power(t2,3)) - 
               t1*(5 - 13*t2 + 23*Power(t2,2) + 6*Power(t2,3))) + 
            Power(t1,3)*(17 + 174*t2 + 218*Power(t2,2) - 
               36*Power(t2,3) - 30*Power(t2,4) + 
               Power(s,3)*(-2 + 11*t2) + 
               Power(s,2)*(5 - 50*t2 - 76*Power(t2,2)) + 
               s*(-11 - 10*t2 + 170*Power(t2,2) + 113*Power(t2,3))) + 
            Power(t1,2)*(7 + 110*t2 + 94*Power(t2,2) - 
               10*Power(s,3)*Power(t2,2) - 340*Power(t2,3) - 
               119*Power(t2,4) + 31*Power(t2,5) + 
               6*Power(s,2)*t2*(-7 + 11*t2 + 14*Power(t2,2)) + 
               s*(1 + 76*t2 + 68*Power(t2,2) - 88*Power(t2,3) - 
                  115*Power(t2,4))) + 
            t1*(5 - 5*t2 + 18*Power(t2,2) - 95*Power(t2,3) + 
               41*Power(t2,4) + 118*Power(t2,5) - 12*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(4 + 3*t2) - 
               Power(s,2)*t2*
                (6 + 17*t2 + 45*Power(t2,2) + 38*Power(t2,3)) + 
               s*(-1 + 25*t2 - 127*Power(t2,2) - 117*Power(t2,3) + 
                  2*Power(t2,4) + 39*Power(t2,5))) + 
            Power(s1,2)*(-3 + (13 + 5*s)*t2 + (-8 + s)*Power(t2,2) + 
               (4 - 3*s)*Power(t2,3) + (-8 + s)*Power(t2,4) + 
               2*Power(t2,5) + Power(t1,4)*(15 - 60*s + 20*t2) + 
               Power(t1,3)*(55 - 135*t2 - 60*Power(t2,2) + 
                  s*(-57 + 130*t2)) + 
               Power(t1,2)*(1 - 230*t2 + 39*Power(t2,2) + 
                  78*Power(t2,3) + s*(17 - 28*t2 - 48*Power(t2,2))) + 
               t1*(-56 + 90*t2 + 98*Power(t2,2) + 32*Power(t2,3) - 
                  31*Power(t2,4) + 
                  s*(13 + 4*t2 + 38*Power(t2,2) - 5*Power(t2,3)))) + 
            s1*(-9 + (17 - 10*s)*t2 + 
               (7 + 21*s + 2*Power(s,2))*Power(t2,2) - 
               (23 + 15*s + 8*Power(s,2))*Power(t2,3) + 
               (17 - 20*s)*Power(t2,4) - (11 + 7*s)*Power(t2,5) + 
               2*Power(t2,6) + 
               5*Power(t1,4)*
                (3 + 6*Power(s,2) + 8*t2 + 3*Power(t2,2) - 
                  2*s*(4 + 5*t2)) + 
               Power(t1,3)*(44 + Power(s,2)*(31 - 64*t2) + 143*t2 - 
                  37*Power(t2,2) - 66*Power(t2,3) + 
                  s*(-53 + 163*t2 + 156*Power(t2,2))) + 
               Power(t1,2)*(39 - 88*t2 - 165*Power(t2,2) - 
                  43*Power(t2,3) + 75*Power(t2,4) + 
                  Power(s,2)*(6 + 16*t2 + 35*Power(t2,2)) - 
                  s*(19 - 254*t2 + 66*Power(t2,2) + 156*Power(t2,3))) - 
               t1*(-7 + 106*t2 + 51*Power(t2,2) - 2*Power(t2,3) - 
                  51*Power(t2,4) + 29*Power(t2,5) + 
                  Power(s,2)*t2*(-18 + 9*t2 + 5*Power(t2,2)) + 
                  s*(4 + 60*t2 + 15*Power(t2,2) - 23*Power(t2,3) - 
                     56*Power(t2,4))))) - 
         s2*Power(t1,2)*(3*(-1 + t2)*
             (-2 + (9 - 10*s - 2*Power(s,2) + 4*Power(s,3))*t2 + 
               (-15 + 21*s - 2*Power(s,3))*Power(t2,2) + 
               (11 - 12*s + 5*Power(s,2) - 2*Power(s,3))*Power(t2,3) + 
               (-3 + s - 3*Power(s,2) + Power(s,3))*Power(t2,4)) + 
            Power(t1,6)*(11 + Power(s,3) - 5*Power(s1,2) - 
               Power(s1,3) - 3*Power(s,2)*(2 + s1) + 
               s*(-2 + 11*s1 + 3*Power(s1,2) - t2) + 2*t2 + s1*(8 + t2)) \
+ Power(t1,5)*(66 - 13*t2 - 10*Power(t2,2) - Power(s,3)*(3 + 5*t2) + 
               Power(s1,3)*(8 + 7*t2) + 
               s1*(-69 - 34*t2 + Power(t2,2)) + 
               Power(s1,2)*(-6 + 11*t2 + 3*Power(t2,2)) + 
               Power(s,2)*(-23 + 18*t2 + 3*Power(t2,2) + 
                  s1*(10 + 17*t2)) - 
               s*(-47 - 22*t2 + 2*Power(t2,2) + 
                  Power(s1,2)*(15 + 19*t2) + 
                  s1*(-43 + 34*t2 + 6*Power(t2,2)))) + 
            Power(t1,4)*(70 - 17*t2 - 14*Power(t2,2) + 31*Power(t2,3) - 
               Power(s1,3)*(1 + 14*t2 + 6*Power(t2,2)) + 
               Power(s,3)*(4 + 4*t2 + 9*Power(t2,2)) + 
               Power(s1,2)*(81 + 88*t2 - 28*Power(t2,2) - 
                  8*Power(t2,3)) - 
               s1*(179 + 24*t2 - 85*Power(t2,2) + 8*Power(t2,3)) - 
               Power(s,2)*(40 - 35*t2 + 16*Power(t2,2) + 
                  10*Power(t2,3) + s1*(23 + t2 + 27*Power(t2,2))) + 
               s*(113 - 96*t2 - 87*Power(t2,2) + 11*Power(t2,3) + 
                  Power(s1,2)*(24 + 7*t2 + 24*Power(t2,2)) + 
                  s1*(-54 - 122*t2 + 53*Power(t2,2) + 18*Power(t2,3)))) \
+ t1*(-1 + t2)*(Power(s,3)*(8 + 66*t2 - 21*Power(t2,2) - 
                  10*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (9 + 58*t2 + 40*Power(t2,2) + Power(t2,3) - 
                  s1*(25 + 11*Power(t2,2))) - 
               s*(-1 + t2)*(-33 - 174*t2 - 136*Power(t2,2) + 
                  5*Power(t2,3) + 2*Power(t2,4) + 
                  s1*(28 - 9*t2 + 47*Power(t2,2) + 2*Power(t2,3))) + 
               Power(s,2)*(-12 - 42*t2 + 98*Power(t2,2) - 
                  26*Power(t2,3) + Power(t2,4) + Power(t2,5) + 
                  s1*(-2 + 10*t2 - 30*Power(t2,2) + 13*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(t1,3)*(-(Power(s,3)*
                  (5 + 29*t2 + 3*Power(t2,2) + 7*Power(t2,3))) + 
               Power(s,2)*(9 + 2*t2 - 64*Power(t2,2) + 2*Power(t2,3) + 
                  12*Power(t2,4) + 
                  s1*(-77 + 29*t2 + 26*Power(t2,2) + 16*Power(t2,3))) + 
               (-1 + t2)*(43 + 82*t2 + 39*Power(t2,2) - 
                  62*Power(t2,3) + 
                  Power(s1,3)*(-3 + 14*t2 + Power(t2,2)) + 
                  Power(s1,2)*
                   (-43 - 56*t2 + 16*Power(t2,2) + 5*Power(t2,3)) + 
                  s1*(11 - 69*t2 - 44*Power(t2,2) + 24*Power(t2,3))) + 
               s*(107 + 128*t2 + 113*Power(t2,2) + 62*Power(t2,3) - 
                  14*Power(t2,4) + 
                  Power(s1,2)*
                   (37 + 27*t2 - 30*Power(t2,2) - 10*Power(t2,3)) - 
                  s1*(120 + 181*t2 - 144*Power(t2,2) + 11*Power(t2,3) + 
                     18*Power(t2,4)))) + 
            Power(t1,2)*(Power(s,3)*
                (-28 - 70*t2 + 45*Power(t2,2) + 9*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (37 + 127*t2 + 99*Power(t2,2) - 38*Power(t2,3) + 
                  Power(s1,2)*(-23 + 21*t2 + 2*Power(t2,2)) + 
                  2*s1*(-13 - 50*t2 - 16*Power(t2,2) + 9*Power(t2,3))) + 
               s*(-1 + t2)*(-38 - 327*t2 - 302*Power(t2,2) + 
                  14*Power(t2,3) + 8*Power(t2,4) + 
                  Power(s1,2)*
                   (14 - 41*t2 + 20*Power(t2,2) + 2*Power(t2,3)) + 
                  s1*(24 + 102*t2 + 59*Power(t2,2) - 11*Power(t2,3) + 
                     6*Power(t2,4))) + 
               Power(s,2)*(s1*
                   (-42 + 3*t2 + 85*Power(t2,2) - 47*Power(t2,3) - 
                     4*Power(t2,4)) + 
                  2*(34 - t2 - 75*Power(t2,2) + 44*Power(t2,3) + 
                     Power(t2,4) - 3*Power(t2,5))))) - 
         Power(s2,5)*(-3 + 8*t2 - 11*s*t2 - 4*Power(t2,2) + 
            25*s*Power(t2,2) + 2*Power(s,2)*Power(t2,2) - 
            15*Power(t2,3) + 16*s*Power(t2,3) + 
            38*Power(s,2)*Power(t2,3) - 13*Power(t2,4) + 
            44*s*Power(t2,4) - 12*Power(s,2)*Power(t2,4) - 
            2*Power(s,3)*Power(t2,4) + 67*Power(t2,5) - 
            52*s*Power(t2,5) - 26*Power(s,2)*Power(t2,5) - 
            42*Power(t2,6) - 22*s*Power(t2,6) - 
            2*Power(s,2)*Power(t2,6) + 2*Power(t2,7) + 
            2*Power(t1,5)*(10 - 3*Power(s,3) + 30*t2 + 15*Power(t2,2) + 
               Power(t2,3) + Power(s,2)*(11 + 9*t2) - 
               2*s*(9 + 17*t2 + 4*Power(t2,2))) + 
            Power(s1,3)*t1*(11 + 21*Power(t1,4) - 25*t2 + 
               5*Power(t2,2) + 9*Power(t2,3) - 
               5*Power(t1,3)*(4 + 17*t2) + 
               7*Power(t1,2)*(-14 + 15*t2 + 10*Power(t2,2)) - 
               t1*(11 - 20*t2 + 58*Power(t2,2) + 14*Power(t2,3))) + 
            Power(t1,4)*(65 + 288*t2 + 147*Power(t2,2) - 
               49*Power(t2,3) - 10*Power(t2,4) + 
               Power(s,3)*(-6 + 23*t2) - 
               2*Power(s,2)*(-6 + 53*t2 + 43*Power(t2,2)) + 
               s*(-41 + 26*t2 + 228*Power(t2,2) + 71*Power(t2,3))) - 
            Power(t1,3)*(-53 - 276*t2 + 130*Power(t2,2) + 
               476*Power(t2,3) + 27*Power(t2,4) - 17*Power(t2,5) + 
               2*Power(s,3)*(1 + t2 + 15*Power(t2,2)) - 
               2*Power(s,2)*(-3 - 50*t2 + 80*Power(t2,2) + 
                  63*Power(t2,3)) + 
               s*(-6 - 188*t2 - 176*Power(t2,2) + 170*Power(t2,3) + 
                  107*Power(t2,4))) + 
            Power(t1,2)*(12 + 9*t2 - 44*Power(t2,2) - 139*Power(t2,3) + 
               294*Power(t2,4) + 92*Power(t2,5) - 14*Power(t2,6) + 
               Power(s,3)*t2*(-2 + 18*t2 + 13*Power(t2,2)) - 
               Power(s,2)*t2*
                (20 - 8*t2 + 113*Power(t2,2) + 82*Power(t2,3)) + 
               s*(2 + 51*t2 - 319*Power(t2,2) - 251*Power(t2,3) - 
                  7*Power(t2,4) + 59*Power(t2,5))) + 
            t1*(1 - 41*t2 + 6*Power(t2,2) - 43*Power(t2,3) + 
               84*Power(t2,4) + 38*Power(t2,5) - 49*Power(t2,6) + 
               4*Power(t2,7) - 
               4*Power(s,3)*Power(t2,2)*(-4 + 3*t2 + Power(t2,2)) + 
               Power(s,2)*t2*
                (8 - 12*t2 + 90*Power(t2,2) + 61*Power(t2,3) + 
                  18*Power(t2,4)) + 
               s*(-5 - 68*t2 - 34*Power(t2,2) + 267*Power(t2,3) + 
                  205*Power(t2,4) + 43*Power(t2,5) - 12*Power(t2,6))) + 
            Power(s1,2)*(-5*Power(t1,5)*(-4 + 9*s - 3*t2) + 
               (-1 + t2)*(-6 + 11*t2 - 2*Power(t2,2) - 
                  (5 + s)*Power(t2,3) + 2*Power(t2,4)) + 
               5*Power(t1,4)*
                (2 - 34*t2 - 10*Power(t2,2) + s*(-7 + 33*t2)) + 
               Power(t1,3)*(-56 - 323*t2 + 119*Power(t2,2) + 
                  92*Power(t2,3) + s*(63 - 71*t2 - 122*Power(t2,2))) + 
               Power(t1,2)*(-134 + 328*t2 + 258*Power(t2,2) + 
                  8*Power(t2,3) - 61*Power(t2,4) + 
                  s*(73 - 10*t2 + 77*Power(t2,2) + 13*Power(t2,3))) + 
               t1*(-11 + 67*t2 - 28*Power(t2,2) - 13*Power(t2,3) - 
                  25*Power(t2,4) + 10*Power(t2,5) + 
                  s*(2 + 33*t2 + 6*Power(t2,2) - 20*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            s1*(-3 + (17 + 13*s)*t2 - (14 + 15*s)*Power(t2,2) - 
               4*(3 + 5*s + 2*Power(s,2))*Power(t2,3) + 
               (23 - s + 7*Power(s,2))*Power(t2,4) + 
               21*(-1 + s)*Power(t2,5) + 2*(5 + s)*Power(t2,6) + 
               Power(t1,5)*(20 + 30*Power(s,2) + 30*t2 + 
                  6*Power(t2,2) - 5*s*(10 + 7*t2)) + 
               Power(t1,4)*(100 + Power(s,2)*(41 - 106*t2) + 143*t2 - 
                  78*Power(t2,2) - 39*Power(t2,3) + 
                  s*(-41 + 277*t2 + 149*Power(t2,2))) + 
               Power(t1,3)*(32 - 254*t2 - 311*Power(t2,2) + 
                  36*Power(t2,3) + 63*Power(t2,4) + 
                  Power(s,2)*(15 + 27*t2 + 97*Power(t2,2)) + 
                  s*(45 + 526*t2 - 236*Power(t2,2) - 222*Power(t2,3))) - 
               Power(t1,2)*(20 + 331*t2 + 177*Power(t2,2) - 
                  142*Power(t2,3) - 51*Power(t2,4) + 43*Power(t2,5) + 
                  Power(s,2)*
                   (2 - 64*t2 + 52*Power(t2,2) + 29*Power(t2,3)) + 
                  s*(-8 + 140*t2 + 145*Power(t2,2) - 88*Power(t2,3) - 
                     126*Power(t2,4))) + 
               t1*(-49 + 123*t2 - 45*Power(t2,2) - 22*Power(t2,3) + 
                  22*Power(t2,4) - 41*Power(t2,5) + 12*Power(t2,6) + 
                  Power(s,2)*t2*
                   (4 - 39*t2 + Power(t2,2) + 4*Power(t2,3)) - 
                  s*(-14 + 77*t2 - 63*Power(t2,2) + 104*Power(t2,3) + 
                     59*Power(t2,4) + 23*Power(t2,5))))) + 
         Power(s2,4)*((-1 + t2)*
             (1 - 2*(3 + 4*s)*t2 + 
               (4 + s - 13*Power(s,2))*Power(t2,2) + 
               (21 - 18*s - 11*Power(s,2) + 4*Power(s,3))*Power(t2,3) + 
               (-15 - 13*s + 22*Power(s,2))*Power(t2,4) + 
               (-25 + 32*s + 7*Power(s,2))*Power(t2,5) + 
               (20 + 6*s)*Power(t2,6)) + 
            Power(t1,6)*(20 - 4*Power(s,3) + 30*t2 + 6*Power(t2,2) + 
               Power(s,2)*(18 + 7*t2) - s*(34 + 32*t2 + 3*Power(t2,2))) \
+ Power(s1,3)*Power(t1,2)*(24 + 7*Power(t1,4) - 62*t2 + 
               22*Power(t2,2) + 16*Power(t2,3) - 
               Power(t1,3)*(29 + 48*t2) + 
               5*Power(t1,2)*(-17 + 27*t2 + 11*Power(t2,2)) - 
               t1*(14 - 7*t2 + 82*Power(t2,2) + 16*Power(t2,3))) + 
            Power(t1,5)*(111 + 242*t2 + 39*Power(t2,2) - 
               20*Power(t2,3) - Power(t2,4) + Power(s,3)*(-5 + 23*t2) - 
               Power(s,2)*t2*(123 + 47*t2) + 
               s*(-56 + 82*t2 + 147*Power(t2,2) + 19*Power(t2,3))) + 
            Power(t1,4)*(146 + 290*t2 - 364*Power(t2,2) - 
               268*Power(t2,3) + 18*Power(t2,4) + 3*Power(t2,5) - 
               Power(s,3)*(6 + 11*t2 + 40*Power(t2,2)) + 
               Power(s,2)*(-36 - 119*t2 + 218*Power(t2,2) + 
                  92*Power(t2,3)) + 
               s*(37 + 345*t2 + 164*Power(t2,2) - 181*Power(t2,3) - 
                  45*Power(t2,4))) + 
            Power(t1,3)*(23 + 34*t2 - 143*Power(t2,2) + 
               114*Power(t2,3) + 316*Power(t2,4) + 10*Power(t2,5) - 
               4*Power(t2,6) + 
               Power(s,3)*t2*(-8 + 33*t2 + 25*Power(t2,2)) - 
               Power(s,2)*(10 + 8*t2 - 56*Power(t2,2) + 
                  173*Power(t2,3) + 80*Power(t2,4)) + 
               s*(39 + 95*t2 - 479*Power(t2,2) - 390*Power(t2,3) + 
                  37*Power(t2,4) + 43*Power(t2,5))) + 
            Power(t1,2)*(-13 - 101*t2 - 45*Power(t2,2) + 
               64*Power(t2,3) + 196*Power(t2,4) - 74*Power(t2,5) - 
               29*Power(t2,6) + 2*Power(t2,7) + 
               Power(s,3)*t2*
                (10 + 12*t2 - 45*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s,2)*(2 + 99*t2 + 7*Power(t2,2) + 
                  102*Power(t2,3) + 47*Power(t2,4) + 28*Power(t2,5)) + 
               s*(-21 - 173*t2 + 299*Power(t2,2) + 595*Power(t2,3) + 
                  287*Power(t2,4) + 21*Power(t2,5) - 18*Power(t2,6))) + 
            t1*(2*Power(s,3)*Power(t2,2)*
                (-1 - 17*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*t2*
                (13 + 85*t2 + 54*Power(t2,2) - 130*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-14 + 16*t2 - 24*Power(t2,2) - 159*Power(t2,3) - 
                  50*Power(t2,4) + 6*Power(t2,5)) + 
               s*(1 - 55*t2 + 172*Power(t2,2) + 260*Power(t2,3) - 
                  194*Power(t2,4) - 162*Power(t2,5) - 26*Power(t2,6) + 
                  4*Power(t2,7))) + 
            Power(s1,2)*t1*(3*Power(t1,5)*(5 + 2*t2) + 
               Power(-1 + t2,3)*(-28 - t2 + 4*Power(t2,2)) - 
               Power(t1,4)*(47 + 127*t2 + 22*Power(t2,2)) + 
               2*Power(t1,3)*
                (-59 - 118*t2 + 78*Power(t2,2) + 29*Power(t2,3)) + 
               Power(t1,2)*(-138 + 626*t2 + 310*Power(t2,2) - 
                  76*Power(t2,3) - 57*Power(t2,4)) + 
               t1*(-9 + 156*t2 - 70*Power(t2,2) - 81*Power(t2,3) - 
                  12*Power(t2,4) + 16*Power(t2,5)) + 
               s*(-6 - 18*Power(t1,5) + 17*t2 - 10*Power(t2,2) - 
                  2*Power(t2,3) + Power(t2,4) + 
                  3*Power(t1,4)*(5 + 38*t2) + 
                  Power(t1,3)*(97 - 149*t2 - 133*Power(t2,2)) + 
                  Power(t1,2)*
                   (165 - 61*t2 + 117*Power(t2,2) + 39*Power(t2,3)) + 
                  t1*(14 + 95*t2 - 6*Power(t2,2) - 44*Power(t2,3) + 
                     Power(t2,4)))) + 
            s1*(-((-1 + t2)*(6 + 2*(-8 + 3*s)*t2 + 
                    (13 - 11*s)*Power(t2,2) + 
                    (2 - 7*s + Power(s,2))*Power(t2,3) + 
                    (-5 + 6*s + Power(s,2))*Power(t2,4) + 
                    (-4 + 6*s)*Power(t2,5) + 4*Power(t2,6))) + 
               Power(t1,6)*(15 + 15*Power(s,2) + 12*t2 + Power(t2,2) - 
                  s*(35 + 13*t2)) + 
               Power(t1,5)*(121 + Power(s,2)*(14 - 89*t2) + 72*t2 - 
                  56*Power(t2,2) - 11*Power(t2,3) + 
                  s*(31 + 261*t2 + 72*Power(t2,2))) + 
               Power(t1,4)*(-43 - 425*t2 - 262*Power(t2,2) + 
                  76*Power(t2,3) + 24*Power(t2,4) + 
                  Power(s,2)*(7 + 62*t2 + 121*Power(t2,2)) + 
                  s*(212 + 516*t2 - 378*Power(t2,2) - 155*Power(t2,3))) \
- Power(t1,3)*(120 + 636*t2 + 140*Power(t2,2) - 306*Power(t2,3) + 
                  13*Power(t2,4) + 27*Power(t2,5) + 
                  Power(s,2)*
                   (23 - 104*t2 + 109*Power(t2,2) + 57*Power(t2,3)) + 
                  s*(-61 + 325*t2 + 288*Power(t2,2) - 212*Power(t2,3) - 
                     130*Power(t2,4))) + 
               Power(t1,2)*(-94 + 276*t2 - 153*Power(t2,2) + 
                  60*Power(t2,3) - 82*Power(t2,4) - 25*Power(t2,5) + 
                  18*Power(t2,6) + 
                  Power(s,2)*
                   (-7 - 31*t2 - 91*Power(t2,2) + 57*Power(t2,3) + 
                     12*Power(t2,4)) - 
                  s*(-58 + 296*t2 - 36*Power(t2,2) + 159*Power(t2,3) + 
                     71*Power(t2,4) + 33*Power(t2,5))) + 
               t1*(Power(s,2)*t2*
                   (-13 - 9*t2 + 16*Power(t2,2) + 3*Power(t2,3) - 
                     2*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (7 - 61*t2 - 39*Power(t2,2) - 33*Power(t2,3) - 
                     18*Power(t2,4) + 4*Power(t2,5)) + 
                  s*(7 + 43*t2 - 109*Power(t2,2) - 72*Power(t2,3) + 
                     84*Power(t2,4) + 47*Power(t2,5))))) + 
         Power(s2,2)*t1*((2 + Power(s,2) + s1 + Power(s1,2) - 
               s*(3 + 2*s1))*Power(t1,7) - 
            3*(-1 + t2)*(2 + (-7 + 6*s)*t2 + 
               (7 - 11*s + 8*Power(s,2) - 4*Power(s,3))*Power(t2,2) + 
               (1 + 6*s - 15*Power(s,2) + 4*Power(s,3))*Power(t2,3) - 
               (5 + 3*s - 7*Power(s,2) + Power(s,3))*Power(t2,4) + 
               2*(1 + s)*Power(t2,5)) + 
            Power(t1,6)*(47 + 22*t2 - 9*Power(s1,2)*(3 + t2) + 
               Power(s,3)*(3 + 2*t2) - Power(s1,3)*(7 + 2*t2) + 
               s1*(34 + 3*t2 - Power(t2,2)) - 
               Power(s,2)*(21 + 16*t2 + s1*(13 + 6*t2)) + 
               s*(-8 + 17*t2 + Power(t2,2) + Power(s1,2)*(17 + 6*t2) + 
                  s1*(49 + 25*t2))) + 
            Power(t1,5)*(154 + 5*t2 - 80*Power(t2,2) - 2*Power(t2,3) + 
               Power(s1,3)*(4 + 41*t2 + 4*Power(t2,2)) - 
               Power(s,3)*(5 + 17*t2 + 6*Power(t2,2)) + 
               Power(s1,2)*(-47 + 14*t2 + 31*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s1*(-142 - 178*t2 - 7*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,2)*(-67 + 20*t2 + 51*Power(t2,2) + 
                  2*Power(t2,3) + s1*(3 + 66*t2 + 16*Power(t2,2))) - 
               s*(Power(s1,2)*(-3 + 90*t2 + 14*Power(t2,2)) + 
                  s1*(-183 + 36*t2 + 88*Power(t2,2) + 4*Power(t2,3)) + 
                  2*(-52 - 75*t2 + 11*Power(t2,2) + 5*Power(t2,3)))) + 
            3*t1*(-1 + t2)*(Power(s,3)*t2*
                (8 + 23*t2 - 14*Power(t2,2)) + 
               Power(s,2)*t2*
                (-16 - 18*t2 + 54*Power(t2,2) - 11*Power(t2,3) + 
                  Power(t2,4) + 
                  s1*(4 - 12*t2 + 3*Power(t2,2) + Power(t2,3))) - 
               Power(-1 + t2,2)*
                (1 - 4*t2 - 36*Power(t2,2) - 15*Power(t2,3) + 
                  s1*(11 - 4*t2 + 7*Power(t2,2) + 4*Power(t2,3))) - 
               s*(-1 + t2)*(-5 - 36*t2 - 86*Power(t2,2) - 
                  45*Power(t2,3) + 4*Power(t2,4) + 
                  s1*(6 - 9*t2 + 29*Power(t2,2) + 8*Power(t2,3)))) + 
            Power(t1,4)*(73 + 41*t2 - 24*Power(t2,2) + 
               101*Power(t2,3) + 19*Power(t2,4) - 
               Power(s1,3)*(5 + 25*t2 + 31*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s,3)*(4 - 8*t2 + 33*Power(t2,2) + 6*Power(t2,3)) + 
               s1*(-300 - 347*t2 + 239*Power(t2,2) + 44*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s1,2)*(97 + 370*t2 - 12*Power(t2,2) - 
                  52*Power(t2,3) - 4*Power(t2,4)) - 
               Power(s,2)*(60 - 63*t2 + 7*Power(t2,2) + 
                  62*Power(t2,3) + 6*Power(t2,4) + 
                  s1*(59 - 49*t2 + 94*Power(t2,2) + 14*Power(t2,3))) + 
               s*(186 - 36*t2 - 389*Power(t2,2) - 33*Power(t2,3) + 
                  23*Power(t2,4) + 
                  Power(s1,2)*
                   (106 - 43*t2 + 89*Power(t2,2) + 10*Power(t2,3)) + 
                  s1*(-6 - 396*t2 + 23*Power(t2,2) + 117*Power(t2,3) + 
                     10*Power(t2,4)))) + 
            Power(t1,3)*(-(Power(s,3)*
                  (-3 + 53*t2 + 5*Power(t2,2) + 27*Power(t2,3) + 
                    2*Power(t2,4))) + 
               Power(s,2)*(49 + 114*t2 - 162*Power(t2,2) - 
                  10*Power(t2,3) + 33*Power(t2,4) + 6*Power(t2,5) + 
                  s1*(-101 - 23*t2 + 27*Power(t2,2) + 63*Power(t2,3) + 
                     4*Power(t2,4))) + 
               (-1 + t2)*(71 + 186*t2 + 109*Power(t2,2) - 
                  60*Power(t2,3) - 51*Power(t2,4) + 
                  2*Power(s1,3)*(-7 + 19*t2 + 3*Power(t2,2)) + 
                  Power(s1,2)*
                   (-51 - 168*t2 - 4*Power(t2,2) + 26*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  s1*(33 - 121*t2 - 128*Power(t2,2) - 3*Power(t2,3) + 
                     24*Power(t2,4))) + 
               s*(32 + 267*t2 + 398*Power(t2,2) + 290*Power(t2,3) + 
                  23*Power(t2,4) - 20*Power(t2,5) + 
                  Power(s1,2)*
                   (57 + 89*t2 - 51*Power(t2,2) - 33*Power(t2,3) - 
                     2*Power(t2,4)) - 
                  s1*(54 + 552*t2 - 186*Power(t2,2) - 12*Power(t2,3) + 
                     49*Power(t2,4) + 8*Power(t2,5)))) + 
            Power(t1,2)*(Power(s,3)*t2*
                (-94 - 39*t2 + 57*Power(t2,2) + 8*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (22 + 127*t2 + 262*Power(t2,2) + 73*Power(t2,3) - 
                  34*Power(t2,4) + 
                  Power(s1,2)*(-47 + 44*t2 + 3*Power(t2,2)) + 
                  s1*(-25 - 149*t2 - 113*Power(t2,2) - 7*Power(t2,3) + 
                     14*Power(t2,4))) + 
               s*(-1 + t2)*(16 - 240*t2 - 822*Power(t2,2) - 
                  286*Power(t2,3) + 36*Power(t2,4) + 6*Power(t2,5) + 
                  Power(s1,2)*
                   (28 - 69*t2 + 24*Power(t2,2) + 7*Power(t2,3)) + 
                  s1*(-12 + 83*t2 + 239*Power(t2,2) + 49*Power(t2,3) - 
                     Power(t2,4) + 2*Power(t2,5))) - 
               Power(s,2)*(s1*
                   (28 + 85*t2 - 165*Power(t2,2) + 37*Power(t2,3) + 
                     25*Power(t2,4)) + 
                  2*(-21 - 96*t2 + 106*Power(t2,2) + 32*Power(t2,3) - 
                     27*Power(t2,4) + 5*Power(t2,5) + Power(t2,6))))) + 
         Power(s2,3)*((-1 + t2)*
             (2 + (-7 + 6*s)*t2 + 
               3*(3 - 5*s + 2*Power(s,2))*Power(t2,2) - 
               (7 - 16*s + 7*Power(s,2) + 2*Power(s,3))*Power(t2,3) + 
               (7 - 9*s - 3*Power(s,2) + Power(s,3))*Power(t2,4) + 
               (-6 + 4*Power(s,2))*Power(t2,5) + 2*(1 + s)*Power(t2,6)) + 
            Power(t1,7)*(Power(s,3) - Power(s1,3) - 2*s1*(3 + t2) - 
               Power(s1,2)*(6 + t2) - Power(s,2)*(7 + 3*s1 + t2) - 
               2*(5 + 3*t2) + 
               s*(16 + 3*Power(s1,2) + 6*t2 + s1*(13 + 2*t2))) - 
            Power(t1,6)*(99 + 105*t2 + Power(t2,2) - 2*Power(t2,3) - 
               5*Power(s1,3)*(4 + 3*t2) + Power(s,3)*(1 + 11*t2) - 
               4*Power(s1,2)*(14 + 13*t2 + Power(t2,2)) - 
               s1*(-84 - 17*t2 + 16*Power(t2,2) + Power(t2,3)) - 
               Power(s,2)*(22 + 71*t2 + 10*Power(t2,2) + 
                  s1*(14 + 37*t2)) + 
               s*(-33 + 68*t2 + 38*Power(t2,2) + Power(t2,3) + 
                  Power(s1,2)*(33 + 41*t2) + 
                  s1*(76 + 127*t2 + 14*Power(t2,2)))) + 
            Power(t1,5)*(-201 - 124*t2 + 271*Power(t2,2) + 
               53*Power(t2,3) - 6*Power(t2,4) + 
               Power(s1,3)*(33 - 101*t2 - 23*Power(t2,2)) + 
               Power(s,3)*(7 + 21*t2 + 25*Power(t2,2)) + 
               Power(s1,2)*(109 + 67*t2 - 102*Power(t2,2) - 
                  18*Power(t2,3)) + 
               s1*(136 + 386*t2 + 93*Power(t2,2) - 38*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s,2)*(75 + 50*t2 - 159*Power(t2,2) - 
                  29*Power(t2,3) + s1*(5 - 94*t2 - 71*Power(t2,2))) + 
               s*(-95 - 341*t2 - 26*Power(t2,2) + 83*Power(t2,3) + 
                  6*Power(t2,4) + 
                  Power(s1,2)*(-63 + 166*t2 + 69*Power(t2,2)) + 
                  s1*(-296 - 189*t2 + 282*Power(t2,2) + 48*Power(t2,3)))) \
+ t1*(-1 + t2)*(Power(s,3)*Power(t2,2)*(-25 - 8*t2 + 8*Power(t2,2)) + 
               Power(s,2)*t2*
                (4 + 86*t2 - 58*Power(t2,2) - 59*Power(t2,3) + 
                  7*Power(t2,4) + 
                  s1*(-6 + 14*t2 + Power(t2,2) - Power(t2,3))) + 
               Power(-1 + t2,2)*
                (-1 + 18*t2 - 28*Power(t2,2) - 83*Power(t2,3) - 
                  14*Power(t2,4) + 
                  s1*(23 - 16*t2 + 9*Power(t2,2) + 16*Power(t2,3) + 
                     4*Power(t2,4))) + 
               s*(-1 + t2)*(-5 - 34*t2 - 82*Power(t2,2) - 
                  171*Power(t2,3) - 46*Power(t2,4) + 2*Power(t2,5) + 
                  s1*(6 - 31*t2 + 53*Power(t2,2) + 34*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            Power(t1,4)*(-45 - 50*t2 + 82*Power(t2,2) - 
               231*Power(t2,3) - 109*Power(t2,4) + 3*Power(t2,5) + 
               Power(s1,3)*(11 + 17*t2 + 68*Power(t2,2) + 
                  9*Power(t2,3)) - 
               Power(s,3)*(2 - 16*t2 + 43*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(s1,2)*(14 - 662*t2 - 148*Power(t2,2) + 
                  106*Power(t2,3) + 25*Power(t2,4)) + 
               2*s1*(130 + 346*t2 - 71*Power(t2,2) - 113*Power(t2,3) + 
                  20*Power(t2,4) + 3*Power(t2,5)) + 
               Power(s,2)*(40 - 41*t2 - 42*Power(t2,2) + 
                  147*Power(t2,3) + 36*Power(t2,4) + 
                  s1*(59 - 101*t2 + 135*Power(t2,2) + 47*Power(t2,3))) - 
               s*(131 + 98*t2 - 574*Power(t2,2) - 269*Power(t2,3) + 
                  68*Power(t2,4) + 11*Power(t2,5) + 
                  Power(s1,2)*
                   (187 - 90*t2 + 134*Power(t2,2) + 34*Power(t2,3)) + 
                  s1*(74 - 506*t2 - 184*Power(t2,2) + 240*Power(t2,3) + 
                     61*Power(t2,4)))) + 
            Power(t1,3)*(Power(s,3)*
                (-5 + 13*t2 + 19*Power(t2,2) + 43*Power(t2,3) + 
                  6*Power(t2,4)) - 
               Power(s,2)*(29 + 204*t2 - 88*Power(t2,2) + 
                  45*Power(t2,4) + 20*Power(t2,5) + 
                  s1*(-51 - 73*t2 - 43*Power(t2,2) + 95*Power(t2,3) + 
                     12*Power(t2,4))) - 
               (-1 + t2)*(46 + 210*t2 + 207*Power(t2,2) - 
                  19*Power(t2,3) - 93*Power(t2,4) - 11*Power(t2,5) + 
                  2*Power(s1,3)*(-13 + 26*t2 + 7*Power(t2,2)) + 
                  2*Power(s1,2)*
                   (-10 - 108*t2 - 34*Power(t2,2) + 17*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  s1*(80 - 193*t2 - 91*Power(t2,2) - 101*Power(t2,3) + 
                     37*Power(t2,4) + 8*Power(t2,5))) + 
               s*(36 - 2*t2 - 661*Power(t2,2) - 527*Power(t2,3) - 
                  207*Power(t2,4) + 33*Power(t2,5) + 8*Power(t2,6) + 
                  Power(s1,2)*
                   (-41 - 132*t2 + 40*Power(t2,2) + 50*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  s1*(-58 + 594*t2 - 61*Power(t2,2) + 57*Power(t2,3) + 
                     63*Power(t2,4) + 25*Power(t2,5)))) - 
            Power(t1,2)*(Power(s,3)*t2*
                (-2 - 97*t2 + 19*Power(t2,2) + 28*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-22 + 5*t2 - 236*Power(t2,2) - 199*Power(t2,3) - 
                  8*Power(t2,4) + 10*Power(t2,5) + 
                  Power(s1,2)*
                   (51 - 50*t2 - 3*Power(t2,2) + 2*Power(t2,3)) + 
                  s1*(5 + 125*t2 + 101*Power(t2,2) + 59*Power(t2,3) - 
                     6*Power(t2,4) - 4*Power(t2,5))) + 
               Power(s,2)*(-(s1*
                     (4 + 77*t2 - 71*Power(t2,2) - 27*Power(t2,3) + 
                       25*Power(t2,4) + 2*Power(t2,5))) + 
                  2*(1 + 70*t2 + 35*Power(t2,2) - 71*Power(t2,3) - 
                     40*Power(t2,4) + 7*Power(t2,5) - 2*Power(t2,6))) + 
               s*(-1 + t2)*(Power(s1,2)*
                   (22 - 47*t2 + 8*Power(t2,2) + 7*Power(t2,3)) + 
                  s1*(-22 - 35*t2 + 227*Power(t2,2) + 173*Power(t2,3) + 
                     17*Power(t2,4)) + 
                  2*(1 + 39*t2 - 343*Power(t2,2) - 292*Power(t2,3) - 
                     61*Power(t2,4) + 10*Power(t2,5) + Power(t2,6))))))*
       T3q(s2,t1))/
     ((-1 + s1)*(-1 + t1)*Power(-s2 + t1,2)*(-s + s1 - t2)*
       Power(1 - s2 + t1 - t2,2)*Power(t1 - s2*t2,3)) + 
    (8*((33 - 5*s - 19*s1)*Power(t1,6) + 
         Power(1 + (-1 + s)*t2,3)*(2 - 3*t2 + Power(t2,2)) + 
         Power(s2,10)*(Power(s1,3) + s1*Power(t2,2) + 2*Power(t2,3)) - 
         Power(t1,5)*(-19 + Power(s,3) + s1 - 2*Power(s1,3) + 77*t2 - 
            43*s1*t2 - Power(s,2)*(3 + t2) + Power(s1,2)*(5 + 3*t2) + 
            s*(-36 + 46*s1 + Power(s1,2) - 10*t2 - 2*s1*t2)) - 
         Power(s2,9)*(Power(s1,3)*(4 + 4*t1 - 3*t2) + 
            Power(s1,2)*(3 + (-2 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            s1*t2*((9 - 5*t2)*t2 + s*(3 - 3*t1 + t2) + t1*(2 + 3*t2)) + 
            Power(t2,2)*(1 + 17*t2 - 6*Power(t2,2) + t1*(6 + 4*t2) + 
               s*(3 - 4*t1 + 4*t2))) + 
         Power(t1,4)*(-45 + Power(s1,3)*(8 - 5*t2) - 36*t2 + 
            71*Power(t2,2) + 2*Power(s,3)*(1 + t2) + 
            s1*(59 + 27*t2 - 42*Power(t2,2)) + 
            Power(s,2)*(8 + s1*(-16 + t2) - 3*t2 - 4*Power(t2,2)) + 
            Power(s1,2)*(-21 - 8*t2 + 8*Power(t2,2)) + 
            s*(50 - 62*t2 - 7*Power(t2,2) + 2*Power(s1,2)*(-9 + 7*t2) + 
               s1*(-45 + 57*t2))) - 
         t1*(-1 + t2)*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(-1 + 2*t2) + 
            (-1 + t2)*(-2 - 6*t2 + Power(t2,2) + s1*(-5 + 4*t2)) + 
            s*(5 + 8*t2 + Power(t2,2) - Power(t2,3) + 
               s1*(-6 + 8*t2 - 4*Power(t2,2)))) + 
         Power(t1,3)*(-(Power(s,3)*(-1 + 7*t2 + Power(t2,2))) + 
            Power(s,2)*(19 - 9*t2 - 9*Power(t2,2) + 6*Power(t2,3) + 
               2*s1*(-13 + 13*t2 + Power(t2,2))) + 
            (-1 + t2)*(13 + 2*Power(s1,3)*(-2 + t2) + 22*t2 - 
               32*Power(t2,2) + 
               Power(s1,2)*(1 + 21*t2 - 5*Power(t2,2)) + 
               s1*(-20 - 51*t2 + 22*Power(t2,2))) + 
            s*(-25 + 3*t2 + 34*Power(t2,2) + 2*Power(t2,3) + 
               Power(s1,2)*(-27 + 41*t2 - 18*Power(t2,2)) + 
               s1*(32 - 18*t2 + 5*Power(t2,2) - 6*Power(t2,3)))) + 
         Power(t1,2)*(Power(s,3)*t2*(-2 + 5*t2 + Power(t2,2)) - 
            Power(-1 + t2,2)*
             (-2 + t2 - 4*Power(t2,2) + Power(s1,2)*(-3 + 5*t2) + 
               2*s1*(-4 - 7*t2 + 2*Power(t2,2))) + 
            Power(s,2)*(2 - 7*t2 + Power(t2,2) + 12*Power(t2,3) - 
               4*Power(t2,4) + 
               s1*(-4 + 10*t2 + 2*Power(t2,2) - 7*Power(t2,3))) + 
            s*(-1 + t2)*(5 - 30*t2 - 20*Power(t2,2) + 2*Power(t2,3) + 
               Power(s1,2)*(4 - 8*t2 + 5*Power(t2,2)) + 
               s1*(-1 + t2 - 4*Power(t2,2) + 4*Power(t2,3)))) + 
         Power(s2,8)*(Power(s1,3)*
             (3 + 6*Power(t1,2) + t1*(14 - 11*t2) - 8*t2 + 
               3*Power(t2,2)) + 
            Power(s1,2)*(12 - (19 + 8*s)*t2 + (-8 + 3*s)*Power(t2,2) + 
               6*Power(t2,3) + Power(t1,2)*(1 - 9*s + 3*t2) + 
               t1*(10 - 8*t2 - 8*Power(t2,2) + s*(-16 + 9*t2))) + 
            s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 4*t2 + 23*Power(t2,2) - 
               37*Power(t2,3) + 9*Power(t2,4) + 
               t1*t2*(20 + 13*t2 - 12*Power(t2,2)) + 
               Power(t1,2)*(1 + 6*t2 + 3*Power(t2,2)) - 
               s*(Power(t1,2)*(3 + 8*t2) + 
                  t1*(3 + 3*t2 - 17*Power(t2,2)) + 
                  t2*(-13 + t2 + 5*Power(t2,2)))) + 
            t2*(2*Power(t1,2)*(3 + 6*t2 + Power(t2,2)) + 
               Power(s,2)*(-3*t1 + 3*Power(t1,2) + t2 - 5*t1*t2 + 
                  2*Power(t2,2)) + 
               t1*(2 + 54*t2 + 11*Power(t2,2) - 7*Power(t2,3)) + 
               t2*(9 + 49*t2 - 45*Power(t2,2) + 6*Power(t2,3)) + 
               s*(3 + 15*t2 + 11*Power(t2,2) - 12*Power(t2,3) - 
                  Power(t1,2)*(8 + 7*t2) + t1*(3 - 7*t2 + 20*Power(t2,2))\
))) - Power(s2,7)*(1 - 2*t2 + 14*s*t2 + 25*Power(t2,2) + 
            11*s*Power(t2,2) - 3*Power(s,2)*Power(t2,2) + 
            58*Power(t2,3) - 22*s*Power(t2,3) + 
            4*Power(s,2)*Power(t2,3) - 128*Power(t2,4) - 
            45*s*Power(t2,4) - 6*Power(s,2)*Power(t2,4) + 
            39*Power(t2,5) + 12*s*Power(t2,5) - 2*Power(t2,6) + 
            Power(t1,2)*(1 + 
               (57 - 34*s + 3*Power(s,2) + 2*Power(s,3))*t2 + 
               (72 + 18*s - 17*Power(s,2))*Power(t2,2) + 
               8*(-1 + 2*s)*Power(t2,3) - Power(t2,4)) + 
            Power(t1,3)*(2 - Power(s,3) + 12*t2 + 6*Power(t2,2) + 
               Power(s,2)*(3 + 4*t2) - s*(4 + 14*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(-6 + 4*Power(t1,3) + 
               Power(t1,2)*(17 - 15*t2) - 3*t2 + 5*Power(t2,2) - 
               Power(t2,3) + t1*(6 - 25*t2 + 10*Power(t2,2))) + 
            t1*t2*(19 + 175*t2 - Power(s,3)*t2 - 64*Power(t2,2) - 
               25*Power(t2,3) + 3*Power(t2,4) + 
               Power(s,2)*(-17 - 4*t2 + 19*Power(t2,2)) + 
               s*(25 + 25*t2 + 56*Power(t2,2) - 25*Power(t2,3))) + 
            s1*(12 + (-29 + 5*s)*t2 + 
               (23 - 5*s + 2*Power(s,2))*Power(t2,2) - 
               (83 + 15*s)*Power(t2,3) + (62 + 9*s)*Power(t2,4) - 
               7*Power(t2,5) + 
               Power(t1,3)*(3 + 6*Power(s,2) + 6*t2 + Power(t2,2) - 
                  s*(8 + 7*t2)) + 
               Power(t1,2)*(11 + Power(s,2)*(17 - 14*t2) + 47*t2 - 
                  5*Power(t2,2) - 8*Power(t2,3) + 
                  s*(-22 + 6*t2 + 30*Power(t2,2))) + 
               t1*(8 + 38*t2 - 38*Power(t2,2) - 54*Power(t2,3) + 
                  13*Power(t2,4) + Power(s,2)*t2*(3 + 4*t2) + 
                  s*(-17 + 52*t2 + 51*Power(t2,2) - 37*Power(t2,3)))) + 
            Power(s1,2)*(9 - (35 + 17*s)*t2 + 2*(1 + 9*s)*Power(t2,2) + 
               (28 - 3*s)*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,3)*(3 - 9*s + 3*t2) + 
               Power(t1,2)*(3*s*(-13 + 9*t2) - 
                  2*(-6 + 8*t2 + 5*Power(t2,2))) + 
               t1*(s*(-29 + 26*t2 - 10*Power(t2,2)) + 
                  2*(16 - 38*t2 - 9*Power(t2,2) + 8*Power(t2,3))))) + 
         s2*((-15 + 7*s)*Power(t1,6) - 
            (-1 + t2)*(-5 + (15 - 26*s)*t2 + 
               (-23 + 46*s - 31*Power(s,2))*Power(t2,2) + 
               (22 - 36*s + 28*Power(s,2) - 10*Power(s,3))*
                Power(t2,3) + 
               (-10 + 18*s - 11*Power(s,2) + 3*Power(s,3))*
                Power(t2,4) + Power(-1 + s,2)*Power(t2,5)) + 
            Power(t1,5)*(-95 + 2*Power(s,3) + Power(s,2)*(5 - 3*t2) - 
               71*t2 - s*(59 + t2)) + 
            Power(s1,3)*Power(t1,2)*
             (-3 + 7*Power(t1,3) - t2 + 5*Power(t2,2) - Power(t2,3) - 
               8*Power(t1,2)*(1 + t2) + t1*(-19 + t2 + 6*Power(t2,2))) + 
            Power(t1,4)*(14 + 92*t2 + 185*Power(t2,2) - 
               Power(s,3)*(6 + t2) + s*(-201 + 10*t2 - 9*Power(t2,2)) + 
               Power(s,2)*(6 - 34*t2 + 9*Power(t2,2))) + 
            Power(t1,3)*(34 + 218*t2 - 49*Power(t2,2) - 
               163*Power(t2,3) + 
               Power(s,3)*(-3 + 23*t2 - 6*Power(t2,2)) + 
               s*(8 - 41*t2 + 106*Power(t2,2) + Power(t2,3)) - 
               Power(s,2)*(55 + 55*t2 - 69*Power(t2,2) + 8*Power(t2,3))) \
+ Power(t1,2)*(4 + 41*t2 - 9*Power(t2,2) - 112*Power(t2,3) + 
               76*Power(t2,4) + 
               Power(s,3)*t2*(8 - 15*t2 + 4*Power(t2,2)) - 
               2*Power(s,2)*(4 + 21*t2 - 37*Power(t2,2) + 
                  22*Power(t2,3)) - 
               s*(-21 + 4*t2 + 29*Power(t2,2) + 26*Power(t2,3) + 
                  4*Power(t2,4))) + 
            t1*(Power(s,3)*Power(t2,2)*(5 - 15*t2 + 4*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (1 + 22*t2 - 24*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s,2)*t2*
                (-25 + 8*t2 + 14*Power(t2,2) - 8*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s*(-16 + 22*t2 - 85*Power(t2,2) + 86*Power(t2,3) - 
                  11*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s1,2)*t1*(Power(t1,4)*(-70 + t2) + 
               2*Power(-1 + t2,2)*(-5 + 6*t2 + Power(t2,2)) + 
               Power(t1,3)*(-56 + 116*t2 + 9*Power(t2,2)) - 
               Power(t1,2)*(1 - 116*t2 + 2*Power(t2,2) + 
                  29*Power(t2,3)) + 
               t1*(-15 + 36*t2 + 43*Power(t2,2) - 83*Power(t2,3) + 
                  19*Power(t2,4)) + 
               s*(6 - 4*Power(t1,4) + Power(t1,3)*(-12 + t2) - 17*t2 + 
                  16*Power(t2,2) - 3*Power(t2,3) - 2*Power(t2,4) + 
                  Power(t1,2)*(17 + 34*t2 - 20*Power(t2,2)) + 
                  t1*(4 + 52*t2 - 69*Power(t2,2) + 25*Power(t2,3)))) - 
            s1*(7*Power(t1,6) + 
               Power(t1,5)*(-118 - 69*s + 5*Power(s,2) - 86*t2 - 
                  2*s*t2) + (-1 + t2)*
                (-6 + (16 - 6*s)*t2 + (-13 + 11*s)*Power(t2,2) + 
                  (2 - 3*s + Power(s,2))*Power(t2,3) + 
                  Power(-1 + s,2)*Power(t2,4)) - 
               Power(t1,4)*(61 - 180*t2 - 151*Power(t2,2) + 
                  Power(s,2)*(38 + 8*t2) + 
                  s*(182 + 51*t2 - 22*Power(t2,2))) + 
               Power(t1,3)*(22 + 344*t2 - 70*Power(t2,2) - 
                  120*Power(t2,3) + 
                  Power(s,2)*(-95 + 44*t2 + 11*Power(t2,2)) + 
                  s*(23 - 103*t2 + 222*Power(t2,2) - 32*Power(t2,3))) + 
               t1*(-(Power(-1 + t2,2)*
                     (-17 - t2 - 24*Power(t2,2) + 6*Power(t2,3))) + 
                  s*Power(-1 + t2,2)*
                   (-11 + 57*t2 - 23*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(s,2)*t2*
                   (-31 + 81*t2 - 56*Power(t2,2) + 8*Power(t2,3))) + 
               Power(t1,2)*(23 + 69*t2 - 6*Power(t2,2) - 
                  140*Power(t2,3) + 54*Power(t2,4) - 
                  Power(s,2)*
                   (19 + t2 - 50*Power(t2,2) + 17*Power(t2,3)) + 
                  s*(19 + 61*t2 + 18*Power(t2,2) - 65*Power(t2,3) + 
                     6*Power(t2,4))))) + 
         Power(s2,6)*(4 - 13*t2 + 13*s*t2 + 31*Power(t2,2) - 
            38*s*Power(t2,2) - 27*Power(s,2)*Power(t2,2) + 
            39*Power(t2,3) - 61*s*Power(t2,3) + 
            10*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) - 
            163*Power(t2,4) - 29*s*Power(t2,4) - 
            20*Power(s,2)*Power(t2,4) + 106*Power(t2,5) + 
            43*s*Power(t2,5) + 6*Power(s,2)*Power(t2,5) - 
            11*Power(t2,6) - 4*s*Power(t2,6) + 
            Power(t1,4)*(4 - Power(s,3) + 6*t2 + Power(s,2)*(4 + t2) - 
               s*(7 + 6*t2)) + 
            Power(t1,3)*(20 + 5*Power(s,3)*(-1 + t2) + 87*t2 + 
               19*Power(t2,2) - 2*Power(t2,3) + 
               Power(s,2)*(14 - 11*t2 - 10*Power(t2,2)) + 
               s*(-23 - 31*t2 + 26*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(-9 + Power(t1,4) + Power(t1,3)*(7 - 9*t2) + 
               5*t2 - 3*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(-2 - 25*t2 + 11*Power(t2,2)) - 
               t1*(28 + 5*t2 - 19*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,2)*(10 + 206*t2 + Power(s,3)*(6 - 7*t2)*t2 + 
               109*Power(t2,2) - 79*Power(t2,3) + 3*Power(t2,4) + 
               Power(s,2)*t2*(-27 - 21*t2 + 23*Power(t2,2)) + 
               s*(1 - 11*t2 + 164*Power(t2,2) + Power(t2,3) - 
                  6*Power(t2,4))) + 
            t1*(2 + 46*t2 + 255*Power(t2,2) - 330*Power(t2,3) + 
               11*Power(t2,4) + 10*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(1 + 3*t2) + 
               Power(s,2)*t2*
                (-39 + 43*t2 + 48*Power(t2,2) - 20*Power(t2,3)) + 
               s*(-1 + 67*t2 - 2*Power(t2,2) - 16*Power(t2,3) - 
                  64*Power(t2,4) + 9*Power(t2,5))) + 
            Power(s1,2)*(-18 + (2 - 8*s)*t2 + (26 + 37*s)*Power(t2,2) + 
               (68 - 10*s)*Power(t2,3) + (-30 + s)*Power(t2,4) + 
               2*Power(t2,5) + Power(t1,4)*(3 - 3*s + t2) + 
               Power(t1,2)*(16 - 119*t2 - 3*Power(t2,2) + 
                  12*Power(t2,3) + s*(-49 + 71*t2 - 27*Power(t2,2))) + 
               Power(t1,3)*(s*(-27 + 23*t2) - 
                  4*(-1 + 4*t2 + Power(t2,2))) + 
               t1*(7 - 110*t2 + 50*Power(t2,2) + 66*Power(t2,3) - 
                  11*Power(t2,4) + 
                  s*(-8 + 25*t2 - 19*Power(t2,2) + 6*Power(t2,3)))) + 
            s1*(9 - (46 + 43*s)*t2 + 
               (13 + 22*s + 8*Power(s,2))*Power(t2,2) - 
               2*(58 + 23*s + 2*Power(s,2))*Power(t2,3) + 
               (125 + 33*s)*Power(t2,4) - (50 + 7*s)*Power(t2,5) + 
               2*Power(t2,6) + 
               Power(t1,4)*(3 + 3*Power(s,2) + 2*t2 - s*(7 + 2*t2)) + 
               Power(t1,3)*(29 + Power(s,2)*(25 - 19*t2) + 32*t2 - 
                  10*Power(t2,2) - Power(t2,3) + 
                  s*(-29 + 31*t2 + 14*Power(t2,2))) + 
               Power(t1,2)*(33 + 35*t2 - 129*Power(t2,2) - 
                  11*Power(t2,3) + 3*Power(t2,4) + 
                  Power(s,2)*(39 - 44*t2 + 23*Power(t2,2)) + 
                  s*(-43 + 179*t2 + 30*Power(t2,2) - 36*Power(t2,3))) + 
               t1*(22 - 41*t2 - 189*Power(t2,2) + 18*Power(t2,3) + 
                  55*Power(t2,4) - 4*Power(t2,5) + 
                  Power(s,2)*t2*(33 - 3*t2 - 7*Power(t2,2)) + 
                  s*(-37 + 132*t2 + 6*Power(t2,2) - 126*Power(t2,3) + 
                     31*Power(t2,4))))) + 
         Power(s2,4)*(-6 + 8*t2 - 53*s*t2 - 18*Power(t2,2) + 
            17*s*Power(t2,2) - 23*Power(s,2)*Power(t2,2) - 
            14*Power(t2,3) + 43*s*Power(t2,3) + 
            20*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) - 
            33*Power(t2,4) - 54*s*Power(t2,4) - 
            33*Power(s,2)*Power(t2,4) + 2*Power(s,3)*Power(t2,4) + 
            65*Power(t2,5) + 44*s*Power(t2,5) + 
            25*Power(s,2)*Power(t2,5) - 12*Power(t2,6) - 
            14*s*Power(t2,6) - 5*Power(s,2)*Power(t2,6) + 
            Power(t1,5)*(-3*Power(s,2) + Power(s,3) + 2*(7 + t2) - 
               s*(8 + t2)) + Power(t1,4)*
             (105 + Power(s,3)*(-3 + t2) + 82*t2 + 2*Power(t2,2) + 
               3*Power(s,2)*(-5 - 10*t2 + Power(t2,2)) + 
               s*(-16 + 60*t2 + Power(t2,2))) + 
            Power(t1,3)*(143 + 232*t2 - 162*Power(t2,2) - 
               9*Power(t2,3) + 
               Power(s,3)*(-13 + 7*t2 - 9*Power(t2,2)) + 
               s*(23 + 297*t2 + 2*Power(t2,2) - 26*Power(t2,3)) - 
               Power(s,2)*(2 + 97*t2 - 86*Power(t2,2) + 4*Power(t2,3))) \
- Power(s1,3)*(-5 + Power(t1,5) - Power(t1,4)*(-7 + t2) + 9*t2 + 
               4*Power(t2,2) - 25*Power(t2,3) + 14*Power(t2,4) + 
               Power(t1,3)*(36 - 28*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(55 - 25*t2 + 41*Power(t2,2) + 
                  17*Power(t2,3)) + 
               t1*(3 + 19*t2 - 34*Power(t2,2) - 12*Power(t2,3) - 
                  10*Power(t2,4))) + 
            Power(t1,2)*(21 + 274*t2 - 366*Power(t2,2) + 
               104*Power(t2,3) + 94*Power(t2,4) + 
               Power(s,3)*t2*(-6 - 3*t2 + 11*Power(t2,2)) + 
               Power(s,2)*(2 + 69*t2 + 191*Power(t2,2) - 
                  45*Power(t2,3) - 6*Power(t2,4)) + 
               s*(12 + 156*t2 + 242*Power(t2,2) - 131*Power(t2,3) + 
                  39*Power(t2,4))) - 
            t1*(5 + 24*t2 - 57*Power(t2,2) + 483*Power(t2,3) - 
               238*Power(t2,4) + 77*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-21 + 3*t2 + 4*Power(t2,2)) + 
               Power(s,2)*t2*
                (-11 - 93*t2 + 46*Power(t2,2) + 33*Power(t2,3) - 
                  12*Power(t2,4)) + 
               s*(17 + 81*t2 + 67*Power(t2,2) + 44*Power(t2,3) - 
                  33*Power(t2,4) - Power(t2,5))) + 
            Power(s1,2)*((-2 + 3*s)*Power(t1,5) - 
               Power(t1,4)*(36 + s*(-15 + t2) + 16*t2 - 
                  3*Power(t2,2)) + 
               Power(t1,3)*(-63 + 19*t2 + 98*Power(t2,2) - 
                  2*Power(t2,3) + s*(69 - 41*t2 - 18*Power(t2,2))) + 
               Power(t1,2)*(-87 + 251*t2 + 451*Power(t2,2) - 
                  28*Power(t2,3) - 11*Power(t2,4) + 
                  s*(140 - 19*t2 + 54*Power(t2,2) + 36*Power(t2,3))) + 
               t1*(-81 + 186*t2 + 128*Power(t2,2) - 305*Power(t2,3) - 
                  102*Power(t2,4) + 18*Power(t2,5) + 
                  s*(40 + 73*t2 - 86*Power(t2,2) - 18*Power(t2,3) - 
                     22*Power(t2,4))) + 
               t2*(-2 + 29*t2 - 25*Power(t2,2) - 93*Power(t2,3) + 
                  78*Power(t2,4) - 8*Power(t2,5) + 
                  s*(16 + 12*t2 - 89*Power(t2,2) + 40*Power(t2,3) + 
                     2*Power(t2,4)))) + 
            s1*(-27 - (-63 + s)*t2 + 
               (6 + 5*s + 8*Power(s,2))*Power(t2,2) + 
               (-46 + 57*s + 11*Power(s,2))*Power(t2,3) + 
               (170 + 79*s + 3*Power(s,2))*Power(t2,4) - 
               (130 + 76*s + 3*Power(s,2))*Power(t2,5) + 
               2*(4 + 5*s)*Power(t2,6) + 
               Power(t1,5)*(8 + 5*s - 3*Power(s,2) + t2) - 
               Power(t1,4)*(-17 + 39*t2 + 2*Power(t2,2) + 
                  Power(s,2)*(5 + t2) + s*(-68 - 41*t2 + 6*Power(t2,2))) \
+ Power(t1,3)*(-42 - 327*t2 - 23*Power(t2,2) + 26*Power(t2,3) + 
                  Power(s,2)*(1 + 2*t2 + 21*Power(t2,2)) + 
                  s*(146 + 128*t2 - 169*Power(t2,2) + 6*Power(t2,3))) - 
               Power(t1,2)*(44 + 511*t2 + 284*Power(t2,2) - 
                  11*Power(t2,3) + 81*Power(t2,4) + 
                  Power(s,2)*
                   (-13 + 21*t2 + 16*Power(t2,2) + 30*Power(t2,3)) + 
                  s*(-78 + 26*t2 + 735*Power(t2,2) - 50*Power(t2,3) - 
                     16*Power(t2,4))) + 
               t1*(-62 + 65*t2 - 284*Power(t2,2) + 703*Power(t2,3) - 
                  20*Power(t2,4) + 48*Power(t2,5) + 
                  Power(s,2)*t2*
                   (59 - 101*t2 + 16*Power(t2,2) + 16*Power(t2,3)) + 
                  s*(29 - 130*t2 - 67*Power(t2,2) + 125*Power(t2,3) + 
                     149*Power(t2,4) - 26*Power(t2,5))))) + 
         Power(s2,5)*(-3 - (2 - 3*s + Power(s,2))*Power(t1,5) + 19*t2 + 
            26*s*t2 - 14*Power(t2,2) + 50*s*Power(t2,2) + 
            51*Power(s,2)*Power(t2,2) - 11*Power(t2,3) + 
            21*s*Power(t2,3) - 28*Power(s,2)*Power(t2,3) + 
            6*Power(s,3)*Power(t2,3) + 140*Power(t2,4) + 
            15*s*Power(t2,4) + 36*Power(s,2)*Power(t2,4) - 
            2*Power(s,3)*Power(t2,4) - 101*Power(t2,5) - 
            45*s*Power(t2,5) - 20*Power(s,2)*Power(t2,5) + 
            29*Power(t2,6) + 12*s*Power(t2,6) + 
            2*Power(s,2)*Power(t2,6) - 
            Power(t1,4)*(32 + Power(s,2)*(9 - 13*t2) + 34*t2 + 
               Power(s,3)*(-3 + 2*t2) + s*(-29 + 2*t2 + Power(t2,2))) + 
            Power(t1,3)*(-80 - 233*t2 + 13*Power(t2,2) + 
               4*Power(t2,3) + 
               Power(s,3)*(11 - 13*t2 + 6*Power(t2,2)) - 
               Power(s,2)*(18 - 79*t2 + 12*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s*(34 - 83*t2 - 86*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s1,3)*(Power(t1,4)*(1 + 2*t2) + 
               Power(t1,3)*(11 + 7*t2 - 4*Power(t2,2)) + 
               t2*(2 + 9*t2 - 17*Power(t2,2) - 2*Power(t2,3)) + 
               2*Power(t1,2)*
                (25 - 7*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               t1*(35 - 12*t2 + 4*Power(t2,2) + 20*Power(t2,3))) - 
            Power(t1,2)*(27 + 336*t2 - 125*Power(t2,2) - 
               179*Power(t2,3) + 22*Power(t2,4) + 
               Power(s,3)*t2*(4 - 15*t2 + 6*Power(t2,2)) + 
               Power(s,2)*t2*
                (-47 + 80*t2 + 31*Power(t2,2) - 6*Power(t2,3)) + 
               s*(8 + 97*t2 + 220*Power(t2,2) - 137*Power(t2,3) - 
                  Power(t2,4))) + 
            t1*(-4 - 24*t2 - 213*Power(t2,2) + 488*Power(t2,3) - 
               242*Power(t2,4) - 11*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-13 - 3*t2 + 2*Power(t2,2)) + 
               Power(s,2)*t2*
                (37 - 115*t2 - 26*Power(t2,2) + 51*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s*(8 - 46*t2 + 113*Power(t2,2) + 22*Power(t2,3) - 
                  7*Power(t2,4) - 19*Power(t2,5))) - 
            Power(s1,2)*(-27 + Power(t1,5) + 13*(3 + s)*t2 + 
               (29 + 33*s)*Power(t2,2) + (27 - 53*s)*Power(t2,3) - 
               (125 + 2*s)*Power(t2,4) + 20*Power(t2,5) + 
               Power(t1,4)*(-3 - 6*t2 + s*(-1 + 6*t2)) + 
               Power(t1,3)*(-29 - 79*t2 + 10*Power(t2,2) + 
                  2*Power(t2,3) + s*(-9 + 36*t2 - 14*Power(t2,2))) + 
               Power(t1,2)*(-34 - 113*t2 + 147*Power(t2,2) + 
                  32*Power(t2,3) - 4*Power(t2,4) + 
                  s*(31 + 35*t2 - 58*Power(t2,2) + 10*Power(t2,3))) + 
               t1*(-73 + 59*t2 + 222*Power(t2,2) + 111*Power(t2,3) - 
                  52*Power(t2,4) + 2*Power(t2,5) + 
                  s*(37 + 27*t2 + 12*Power(t2,2) + 35*Power(t2,3) - 
                     2*Power(t2,4)))) + 
            s1*(18 + (-1 + 2*s)*Power(t1,5) + (-13 + 61*s)*t2 - 
               (3 + 56*s + 12*Power(s,2))*Power(t2,2) + 
               (75 + 26*s + 4*Power(s,2))*Power(t2,3) - 
               (234 + 112*s + 5*Power(s,2))*Power(t2,4) + 
               (71 + 30*s)*Power(t2,5) - 2*(8 + s)*Power(t2,6) + 
               Power(t1,4)*(-25 + s*(5 - 19*t2) - 6*t2 + Power(t2,2) + 
                  Power(s,2)*(-5 + 6*t2)) + 
               Power(t1,3)*(-48 + 52*t2 + 64*Power(t2,2) - 
                  2*Power(t2,3) - 
                  2*Power(s,2)*(18 - 21*t2 + 8*Power(t2,2)) + 
                  s*(-15 - 183*t2 + 28*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,2)*(-20 + 214*t2 + 249*Power(t2,2) - 
                  109*Power(t2,3) - 5*Power(t2,4) + 
                  Power(s,2)*
                   (-43 + 38*t2 - 50*Power(t2,2) + 14*Power(t2,3)) + 
                  s*(7 - 347*t2 + 226*Power(t2,2) + 55*Power(t2,3) - 
                     10*Power(t2,4))) + 
               t1*(4 + 109*t2 + 368*Power(t2,2) - 177*Power(t2,3) + 
                  54*Power(t2,4) + 22*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-75 + 66*t2 + 18*Power(t2,2) - 4*Power(t2,3)) + 
                  s*(25 - 52*t2 + 166*Power(t2,2) + 213*Power(t2,3) - 
                     96*Power(t2,4) + 8*Power(t2,5))))) + 
         Power(s2,3)*(9 + (-1 + s)*Power(t1,6) - 29*t2 + 14*s*t2 + 
            26*Power(t2,2) - 16*s*Power(t2,2) - 
            39*Power(s,2)*Power(t2,2) + 35*Power(t2,3) - 
            81*s*Power(t2,3) + 41*Power(s,2)*Power(t2,3) - 
            12*Power(s,3)*Power(t2,3) - 64*Power(t2,4) + 
            96*s*Power(t2,4) - 7*Power(s,2)*Power(t2,4) + 
            9*Power(s,3)*Power(t2,4) + 13*Power(t2,5) - 
            37*s*Power(t2,5) - 4*Power(s,2)*Power(t2,5) - 
            Power(s,3)*Power(t2,5) + 10*Power(t2,6) + 10*s*Power(t2,6) + 
            3*Power(s,2)*Power(t2,6) - 
            Power(t1,5)*(47 + 2*Power(s,3) + Power(s,2)*(-16 + t2) + 
               13*t2 - s*t2) + 
            Power(t1,4)*(-156 - 64*t2 + 19*Power(t2,2) + 
               Power(s,3)*(-1 + 7*t2) + 
               Power(s,2)*(63 - 3*t2 - 5*Power(t2,2)) + 
               2*s*(-41 - 41*t2 + 9*Power(t2,2))) - 
            Power(t1,3)*(122 + 73*t2 - 4*Power(t2,2) + 
               117*Power(t2,3) + 
               Power(s,2)*t2*(52 + 85*t2 - 18*Power(t2,2)) + 
               Power(s,3)*(-7 - 14*t2 + 8*Power(t2,2)) + 
               s*(118 + 510*t2 - 21*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s1,3)*(-2 + Power(t1,5) + Power(t1,4)*(5 - 12*t2) + 
               4*t2 - 6*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,3)*(34 + 8*t2 + 40*Power(t2,2)) - 
               Power(t1,2)*(-14 + 26*t2 + 17*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(-13 + 35*t2 - 64*Power(t2,2) + 28*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(t1,2)*(12 - 42*t2 + 576*Power(t2,2) - 
               61*Power(t2,3) + 101*Power(t2,4) + 
               2*Power(s,3)*t2*(7 - 8*t2 + Power(t2,2)) + 
               s*(7 - 33*t2 - 121*Power(t2,2) + 189*Power(t2,3) - 
                  37*Power(t2,4)) - 
               Power(s,2)*(8 + 230*t2 + 67*Power(t2,2) - 
                  111*Power(t2,3) + 14*Power(t2,4))) + 
            t1*(17 - t2 + 97*Power(t2,2) + 161*Power(t2,3) - 
               235*Power(t2,4) + Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-9 - 6*t2 + 2*Power(t2,2)) - 
               Power(s,2)*t2*
                (61 - 19*t2 - 63*Power(t2,2) + 35*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(8 + 161*t2 - 159*Power(t2,2) + 136*Power(t2,3) - 
                  91*Power(t2,4) + 15*Power(t2,5))) - 
            Power(s1,2)*(15 + (-38 + 5*s)*t2 + (31 + s)*Power(t2,2) - 
               (5 + 44*s)*Power(t2,3) + (-31 + 44*s)*Power(t2,4) + 
               (36 - 10*s)*Power(t2,5) - 8*Power(t2,6) + 
               Power(t1,5)*(-11 + 4*s + t2) + 
               Power(t1,4)*(-23 + s*(23 - 31*t2) + 56*t2 + 
                  5*Power(t2,2)) + 
               Power(t1,3)*(7 + 460*t2 + 174*Power(t2,2) - 
                  27*Power(t2,3) + s*(139 - 30*t2 + 70*Power(t2,2))) - 
               Power(t1,2)*(53 - 323*t2 + 170*Power(t2,2) + 
                  389*Power(t2,3) - 35*Power(t2,4) + 
                  s*(-136 + 70*t2 + 7*Power(t2,2) + 45*Power(t2,3))) + 
               t1*(-5 + 48*t2 - 10*Power(t2,2) - 307*Power(t2,3) + 
                  196*Power(t2,4) - 6*Power(t2,5) + 
                  s*(1 + 106*t2 - 250*Power(t2,2) + 70*Power(t2,3) + 
                     12*Power(t2,4)))) + 
            s1*(-Power(t1,6) + 
               Power(t1,5)*(-1 + 5*Power(s,2) - 2*t2 + s*(-27 + 2*t2)) + 
               Power(t1,4)*(98 + Power(s,2)*(19 - 26*t2) + 112*t2 - 
                  26*Power(t2,2) + s*(-123 + 44*t2 + 10*Power(t2,2))) + 
               Power(t1,3)*(187 + 618*t2 + 161*Power(t2,2) + 
                  130*Power(t2,3) + 
                  Power(s,2)*(79 - 46*t2 + 39*Power(t2,2)) + 
                  s*(-130 + 569*t2 + 282*Power(t2,2) - 44*Power(t2,3))) \
+ Power(t1,2)*(58 + 240*t2 - 588*Power(t2,2) - 381*Power(t2,3) - 
                  109*Power(t2,4) + 
                  Power(s,2)*
                   (25 + 55*t2 + 44*Power(t2,2) - 19*Power(t2,3)) + 
                  2*s*(-57 + 195*t2 + 131*Power(t2,2) - 
                     228*Power(t2,3) + 20*Power(t2,4))) + 
               t1*(57 - 214*t2 + 114*Power(t2,2) - 506*Power(t2,3) + 
                  349*Power(t2,4) + 24*Power(t2,5) + 
                  Power(s,2)*t2*(15 - 17*t2 - 26*Power(t2,2)) + 
                  s*(-53 + 116*t2 - 167*Power(t2,2) - 253*Power(t2,3) + 
                     121*Power(t2,4) + 2*Power(t2,5))) - 
               t2*(2 + 52*t2 - 76*Power(t2,2) + 53*Power(t2,3) - 
                  47*Power(t2,4) + 16*Power(t2,5) - 
                  Power(s,2)*t2*
                   (-2 - 17*t2 + 9*Power(t2,2) + Power(t2,3)) + 
                  s*(47 - 75*t2 + 67*Power(t2,2) - 36*Power(t2,4) + 
                     10*Power(t2,5))))) + 
         Power(s2,2)*((7 - 3*s)*Power(t1,6) + 
            Power(t1,5)*(s*(16 - 9*t2) + 15*(5 + t2) + 
               Power(s,2)*(-20 + 3*t2)) + 
            Power(t1,4)*(122 + Power(s,3)*(6 - 7*t2) + 122*t2 + 
               83*Power(t2,2) + 
               Power(s,2)*(-57 + 56*t2 - 3*Power(t2,2)) + 
               s*(227 + 142*t2 - 2*Power(t2,2))) + 
            Power(s1,3)*t1*(5 + 3*Power(t1,4) - 13*t2 + 17*Power(t2,2) - 
               9*Power(t2,3) - 2*Power(t1,3)*(6 + 13*t2) + 
               Power(t1,2)*(3 + 19*t2 + 22*Power(t2,2)) + 
               t1*(7 + 26*t2 - 10*Power(t2,2) - 5*Power(t2,3))) + 
            Power(t1,3)*(20 - 258*t2 - 185*Power(t2,2) - 
               161*Power(t2,3) + 
               Power(s,3)*(1 - 29*t2 + 18*Power(t2,2)) + 
               Power(s,2)*(45 + 149*t2 - 39*Power(t2,2) - 
                  10*Power(t2,3)) + 
               s*(97 + 351*t2 - 226*Power(t2,2) + 23*Power(t2,3))) - 
            Power(t1,2)*(21 + 87*t2 + 311*Power(t2,2) - 
               256*Power(t2,3) - 103*Power(t2,4) + 
               Power(s,3)*t2*(14 - 21*t2 + 12*Power(t2,2)) + 
               Power(s,2)*(-12 - 190*t2 + 115*Power(t2,2) + 
                  26*Power(t2,3) - 18*Power(t2,4)) + 
               s*(28 + 72*t2 + Power(t2,2) + 12*Power(t2,3) - 
                  5*Power(t2,4))) + 
            t2*(Power(s,3)*Power(t2,2)*(18 - 19*t2 + 3*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (2 + 18*t2 - 22*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s,2)*t2*
                (59 - 85*t2 + 45*Power(t2,2) - 16*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(31 - 65*t2 + 104*Power(t2,2) - 100*Power(t2,3) + 
                  36*Power(t2,4) - 6*Power(t2,5))) - 
            t1*(11 - 40*t2 + 123*Power(t2,2) - 107*Power(t2,3) - 
               41*Power(t2,4) + 54*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(5 - 21*t2 + 2*Power(t2,2)) + 
               Power(s,2)*t2*
                (-59 + 54*t2 + 22*Power(t2,2) - 45*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s*(-13 + 101*t2 - 236*Power(t2,2) + 142*Power(t2,3) - 
                  44*Power(t2,4) + 8*Power(t2,5))) + 
            Power(s1,2)*(Power(t1,5)*(7 - 6*s + 3*t2) + 
               (-1 + t2)*(-6 + 11*t2 - 2*Power(t2,2) + 
                  (-3 + s)*Power(t2,3)) + 
               Power(t1,4)*(120 + 221*t2 - 15*Power(t2,2) + 
                  s*(40 + 21*t2)) + 
               Power(t1,3)*(42 + 141*t2 - 390*Power(t2,2) + 
                  11*Power(t2,3) + s*(89 - 51*t2 - 8*Power(t2,2))) + 
               Power(t1,2)*(7 + 34*t2 - 321*Power(t2,2) + 
                  131*Power(t2,3) + 23*Power(t2,4) + 
                  s*(46 - 124*t2 - 10*Power(t2,2) + 19*Power(t2,3))) + 
               t1*(28 - 68*t2 + 56*Power(t2,2) - 93*Power(t2,3) + 
                  99*Power(t2,4) - 22*Power(t2,5) + 
                  s*(-16 + 69*t2 - 159*Power(t2,2) + 120*Power(t2,3) - 
                     26*Power(t2,4)))) + 
            s1*(15 + 3*Power(t1,6) + (-49 + 31*s)*t2 + 
               (73 - 66*s)*Power(t2,2) + 
               (-55 + 34*s + 5*Power(s,2))*Power(t2,3) + 
               (10 + 8*s - 7*Power(s,2))*Power(t2,4) + 
               (8 - 9*s + 3*Power(s,2))*Power(t2,5) + 
               2*(-1 + s)*Power(t2,6) + 
               Power(t1,5)*(-39 + 3*Power(s,2) + s*(21 - 6*t2) + 16*t2) + 
               Power(t1,4)*(-249 - 300*t2 - 140*Power(t2,2) + 
                  2*Power(s,2)*(-17 + 6*t2) + 
                  2*s*(-40 - 146*t2 + 9*Power(t2,2))) + 
               Power(t1,3)*(-121 + 88*t2 + 508*Power(t2,2) + 
                  185*Power(t2,3) + 
                  Power(s,2)*(-132 + 39*t2 - 35*Power(t2,2)) + 
                  s*(11 - 637*t2 + 302*Power(t2,2) + 8*Power(t2,3))) + 
               Power(t1,2)*(Power(s,2)*
                   (-35 - 53*t2 + 47*Power(t2,2) + 25*Power(t2,3)) + 
                  s*(71 - 119*t2 + 260*Power(t2,2) + 90*Power(t2,3) - 
                     44*Power(t2,4)) - 
                  2*(1 - 67*t2 - 273*Power(t2,2) + 152*Power(t2,3) + 
                     55*Power(t2,4))) + 
               t1*(-1 + 87*t2 - 45*Power(t2,2) + 28*Power(t2,3) - 
                  117*Power(t2,4) + 48*Power(t2,5) - 
                  Power(s,2)*t2*
                   (53 - 126*t2 + 45*Power(t2,2) + 8*Power(t2,3)) + 
                  s*(17 + 40*t2 - 12*Power(t2,2) + 84*Power(t2,3) - 
                     112*Power(t2,4) + 22*Power(t2,5))))))*T4q(-1 + s2))/
     ((-1 + s1)*Power(-1 + s2,2)*(-1 + t1)*(-s + s1 - t2)*
       Power(1 - s2 + t1 - t2,2)*Power(t1 - s2*t2,3)) + 
    (8*((-1 + s - s1)*Power(t1,6) - 
         Power(1 + (-1 + s)*t2,3)*(2 - 3*t2 + Power(t2,2)) + 
         Power(s2,7)*(Power(s1,3) + s1*Power(t2,2) + 2*Power(t2,3)) + 
         Power(t1,5)*(-13 + Power(s,3) - 2*Power(s1,3) + 5*t2 - 
            Power(s1,2)*(1 + t2) + s1*(9 + t2) - 
            Power(s,2)*(-1 + 4*s1 + t2) + 
            s*(5*Power(s1,2) + 2*s1*t2 - 2*(3 + t2))) - 
         Power(s2,6)*(Power(s1,3)*(1 + 4*t1 - 3*t2) + 
            Power(s1,2)*(3 + (-2 + t1)*t2 - 2*Power(t2,2) - 
               s*(3*t1 + t2)) + 
            s1*t2*((6 - 5*t2)*t2 + s*(3 - 3*t1 + t2) + t1*(2 + 3*t2)) + 
            Power(t2,2)*(1 + 11*t2 - 6*Power(t2,2) + t1*(6 + 4*t2) + 
               s*(3 - 4*t1 + 4*t2))) + 
         Power(t1,4)*(-7 + 24*t2 + 3*Power(s1,3)*t2 - 11*Power(t2,2) - 
            2*Power(s,3)*(1 + t2) + s1*(29 - 13*t2 - 2*Power(t2,2)) + 
            Power(s1,2)*(-19 - 4*t2 + 4*Power(t2,2)) + 
            Power(s,2)*(6 - 5*t2 + 4*Power(t2,2) + s1*(4 + 7*t2)) + 
            s*(-18 + 22*t2 + 3*Power(t2,2) - 2*Power(s1,2)*(1 + 4*t2) + 
               s1*(19 + 3*t2 - 8*Power(t2,2)))) + 
         t1*(-1 + t2)*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(-1 + 2*t2) + 
            (-1 + t2)*(-2 - 6*t2 + Power(t2,2) + s1*(-5 + 4*t2)) + 
            s*(5 + 8*t2 + Power(t2,2) - Power(t2,3) + 
               s1*(-6 + 8*t2 - 4*Power(t2,2)))) - 
         Power(t1,2)*(Power(s,3)*t2*(-2 + 5*t2 + Power(t2,2)) - 
            Power(-1 + t2,2)*(-2 + t2 - 4*Power(t2,2) + 
               Power(s1,2)*(-3 + 5*t2) + 2*s1*(-4 - 7*t2 + 2*Power(t2,2))\
) + Power(s,2)*(2 - 7*t2 + Power(t2,2) + 12*Power(t2,3) - 
               4*Power(t2,4) + 
               s1*(-4 + 10*t2 + 2*Power(t2,2) - 7*Power(t2,3))) + 
            s*(-1 + t2)*(5 - 30*t2 - 20*Power(t2,2) + 2*Power(t2,3) + 
               Power(s1,2)*(4 - 8*t2 + 5*Power(t2,2)) + 
               s1*(-1 + t2 - 4*Power(t2,2) + 4*Power(t2,3)))) + 
         Power(t1,3)*(Power(s,3)*(-1 + 7*t2 + Power(t2,2)) - 
            (-1 + t2)*(27 + 16*t2 + 2*Power(s1,3)*t2 - 12*Power(t2,2) + 
               s1*(-24 - 13*t2 + 2*Power(t2,2)) + 
               Power(s1,2)*(-7 - 5*t2 + 3*Power(t2,2))) - 
            Power(s,2)*(15 + 5*t2 - 13*Power(t2,2) + 6*Power(t2,3) + 
               2*s1*(-11 + 7*t2 + 3*Power(t2,2))) + 
            s*(-9 - 11*t2 - 24*Power(t2,2) - 2*Power(t2,3) + 
               Power(s1,2)*(-13 - t2 + 8*Power(t2,2)) + 
               s1*(40 - 2*t2 - 19*Power(t2,2) + 10*Power(t2,3)))) + 
         Power(s2,5)*(Power(s1,3)*
             (-3 + 6*Power(t1,2) + t1*(2 - 11*t2) + t2 + 3*Power(t2,2)) + 
            Power(s1,2)*(3 - (13 + 5*s)*t2 + (-2 + 3*s)*Power(t2,2) + 
               6*Power(t2,3) + Power(t1,2)*(1 - 9*s + 3*t2) + 
               t1*(10 - 11*t2 - 8*Power(t2,2) + s*(-7 + 9*t2))) + 
            s1*(3 + Power(s,2)*t1*(3*t1 - t2) - 4*t2 + 2*Power(t2,2) - 
               14*Power(t2,3) + 9*Power(t2,4) + 
               2*t1*t2*(7 + 2*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(1 + 6*t2 + 3*Power(t2,2)) - 
               s*(Power(t1,2)*(3 + 8*t2) + 
                  t1*(3 - 6*t2 - 17*Power(t2,2)) + 
                  t2*(-4 + 4*t2 + 5*Power(t2,2)))) + 
            t2*(2*Power(t1,2)*(3 + 6*t2 + Power(t2,2)) + 
               Power(s,2)*(-3*t1 + 3*Power(t1,2) + t2 - 5*t1*t2 + 
                  2*Power(t2,2)) + 
               t2*(6 + 10*t2 - 27*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(-2 - 36*t2 + Power(t2,2) + 7*Power(t2,3)) + 
               s*(3 + 6*t2 - Power(t2,2) - 12*Power(t2,3) - 
                  Power(t1,2)*(8 + 7*t2) + t1*(3 + 5*t2 + 20*Power(t2,2)))\
)) + Power(s2,4)*(-1 + 2*t2 - 5*s*t2 - 4*Power(t2,2) + 16*s*Power(t2,2) + 
            6*Power(s,2)*Power(t2,2) + 3*Power(t2,3) + 35*s*Power(t2,3) + 
            2*Power(s,2)*Power(t2,3) + 27*Power(t2,4) + 7*s*Power(t2,4) + 
            6*Power(s,2)*Power(t2,4) - 23*Power(t2,5) - 
            12*s*Power(t2,5) + 2*Power(t2,6) + 
            Power(t1,2)*(-1 + 
               (-39 + 10*s + 6*Power(s,2) - 2*Power(s,3))*t2 + 
               (-36 - 39*s + 17*Power(s,2))*Power(t2,2) - 
               2*(-7 + 8*s)*Power(t2,3) + Power(t2,4)) + 
            Power(t1,3)*(Power(s,3) - Power(s,2)*(3 + 4*t2) - 
               2*(1 + 6*t2 + 3*Power(t2,2)) + 
               s*(4 + 14*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(1 - 4*Power(t1,3) - 3*t2 + 4*Power(t2,2) + 
               Power(t2,3) + Power(t1,2)*(1 + 15*t2) - 
               2*t1*(-6 + 4*t2 + 5*Power(t2,2))) + 
            t1*t2*(-13 - 49*t2 + Power(s,3)*t2 + 73*Power(t2,2) + 
               4*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,2)*(8 - 11*t2 - 19*Power(t2,2)) + 
               s*(-16 - 22*t2 + 4*Power(t2,2) + 25*Power(t2,3))) - 
            s1*(3 - (17 + 16*s)*t2 + 
               2*(-1 + 2*s + Power(s,2))*Power(t2,2) + 
               (-4 + 6*s)*Power(t2,3) + (13 + 9*s)*Power(t2,4) - 
               7*Power(t2,5) + 
               Power(t1,3)*(3 + 6*Power(s,2) + 6*t2 + Power(t2,2) - 
                  s*(8 + 7*t2)) + 
               Power(t1,2)*(8 + Power(s,2)*(8 - 14*t2) + 29*t2 - 
                  14*Power(t2,2) - 8*Power(t2,3) + 
                  s*(-13 + 30*t2 + 30*Power(t2,2))) + 
               t1*(8 - 10*t2 - 35*Power(t2,2) - 2*Power(t2,3) + 
                  13*Power(t2,4) + 2*Power(s,2)*t2*(3 + 2*t2) + 
                  s*(-8 + 43*t2 - 37*Power(t2,3)))) + 
            Power(s1,2)*(9 - (10 + s)*t2 - (14 + 9*s)*Power(t2,2) + 
               (-10 + 3*s)*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,3)*(9*s - 3*(1 + t2)) + 
               Power(t1,2)*(-9 + 25*t2 + 10*Power(t2,2) - 
                  3*s*(-4 + 9*t2)) + 
               t1*(s*(-1 + t2 + 10*Power(t2,2)) - 
                  2*(1 - 23*t2 + 3*Power(t2,2) + 8*Power(t2,3))))) + 
         Power(s2,3)*(1 - 7*t2 - 11*s*t2 - 11*s*Power(t2,2) - 
            12*Power(s,2)*Power(t2,2) - 11*Power(t2,3) + 
            25*s*Power(t2,3) + 6*Power(s,2)*Power(t2,3) - 
            2*Power(s,3)*Power(t2,3) + 9*Power(t2,4) + 38*s*Power(t2,4) + 
            13*Power(t2,5) + 9*s*Power(t2,5) + 6*Power(s,2)*Power(t2,5) - 
            5*Power(t2,6) - 4*s*Power(t2,6) + 
            Power(t1,4)*(4 - Power(s,3) + 6*t2 + Power(s,2)*(4 + t2) - 
               s*(7 + 6*t2)) + 
            Power(t1,3)*(14 + 51*t2 + Power(t2,2) - 2*Power(t2,3) + 
               Power(s,3)*(-2 + 5*t2) + 
               Power(s,2)*(5 - 23*t2 - 10*Power(t2,2)) + 
               s*(-11 + 11*t2 + 35*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(Power(t1,4) - Power(t1,3)*(5 + 9*t2) + 
               Power(t1,2)*(-17 + 20*t2 + 11*Power(t2,2)) + 
               2*(1 - 2*t2 + Power(t2,3)) - 
               t1*(2 - 4*t2 + 11*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,2)*(7 + 71*t2 - 35*Power(t2,2) - 
               7*Power(s,3)*Power(t2,2) - 47*Power(t2,3) + 
               6*Power(t2,4) + 
               Power(s,2)*t2*(-18 + 30*t2 + 23*Power(t2,2)) + 
               s*(1 + 43*t2 + 68*Power(t2,2) - 47*Power(t2,3) - 
                  6*Power(t2,4))) + 
            t1*(2 + t2 + 6*Power(t2,2) - 92*Power(t2,3) + 
               58*Power(t2,4) + Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(4 + 3*t2) - 
               Power(s,2)*t2*
                (6 - 25*t2 + 11*Power(t2,2) + 20*Power(t2,3)) + 
               s*(-1 + 10*t2 - 91*Power(t2,2) - 66*Power(t2,3) + 
                  9*Power(t2,4) + 9*Power(t2,5))) + 
            Power(s1,2)*(-3 + (13 + 5*s)*t2 + (-8 + s)*Power(t2,2) - 
               s*Power(t2,3) + (-4 + s)*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,4)*(3 - 3*s + t2) + 
               Power(t1,3)*(-5 + (-25 + 23*s)*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-14 - 53*t2 + 27*Power(t2,2) + 
                  12*Power(t2,3) + s*(14 - 10*t2 - 27*Power(t2,2))) + 
               t1*(-29 + 60*t2 + 56*Power(t2,2) - 11*Power(t2,4) + 
                  s*(13 + t2 + 11*Power(t2,2) + 6*Power(t2,3)))) + 
            s1*(-9 + (17 - 10*s)*t2 + 
               (7 + 21*s + 2*Power(s,2))*Power(t2,2) + 
               (3 - 23*s)*Power(t2,3) - (13 + 10*s)*Power(t2,4) - 
               7*(1 + s)*Power(t2,5) + 2*Power(t2,6) + 
               Power(t1,4)*(3 + 3*Power(s,2) + 2*t2 - s*(7 + 2*t2)) + 
               Power(t1,3)*(20 + Power(s,2)*(7 - 19*t2) + 14*t2 - 
                  13*Power(t2,2) - Power(t2,3) + 
                  s*(-5 + 52*t2 + 14*Power(t2,2))) + 
               Power(t1,2)*(6 - 46*t2 - 48*Power(t2,2) + 
                  21*Power(t2,3) + 3*Power(t2,4) + 
                  Power(s,2)*(6 - 2*t2 + 23*Power(t2,2)) + 
                  s*(5 + 113*t2 - 60*Power(t2,2) - 36*Power(t2,3))) - 
               t1*(2 + 55*t2 + 33*Power(t2,2) - 42*Power(t2,3) + 
                  4*Power(t2,4) + 4*Power(t2,5) + 
                  Power(s,2)*t2*(-18 + 15*t2 + 7*Power(t2,2)) + 
                  s*(4 + 12*t2 + 27*Power(t2,2) - 7*Power(t2,3) - 
                     31*Power(t2,4))))) + 
         s2*(Power(t1,5)*(8 + s - 6*Power(s,2) + Power(s,3) + 2*t2 - 
               s*t2) + (-1 + t2)*
             (1 - 2*(3 + 4*s)*t2 + (4 + s - 13*Power(s,2))*Power(t2,2) + 
               (7 + Power(s,2) - 4*Power(s,3))*Power(t2,3) + 
               (-7 + 9*s - 2*Power(s,2))*Power(t2,4) + 
               Power(-1 + s,2)*Power(t2,5)) + 
            Power(t1,4)*(33 + 4*t2 - 5*Power(s,3)*t2 - 10*Power(t2,2) + 
               s*(29 + 18*t2 - 2*Power(t2,2)) + 
               3*Power(s,2)*(-6 + 5*t2 + Power(t2,2))) - 
            Power(s1,3)*Power(t1,2)*
             (-3 + Power(t1,3) + 11*t2 - 7*Power(t2,2) - Power(t2,3) - 
               Power(t1,2)*(2 + 7*t2) + t1*(1 + 5*t2 + 6*Power(t2,2))) + 
            Power(t1,3)*(7 - 21*t2 - 37*Power(t2,2) + 27*Power(t2,3) + 
               Power(s,3)*t2*(-2 + 9*t2) + 
               s*(23 - 20*t2 - 70*Power(t2,2) + 5*Power(t2,3)) - 
               2*Power(s,2)*(1 - 14*t2 + 5*Power(t2,2) + 5*Power(t2,3))) \
+ Power(t1,2)*(-10 - 68*t2 + 45*Power(t2,2) + 61*Power(t2,3) - 
               28*Power(t2,4) - Power(s,3)*t2*(2 + 7*Power(t2,2)) + 
               s*(-6 + t2 + 83*Power(t2,2) + 62*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s,2)*(2 + 51*t2 - 35*Power(t2,2) - 4*Power(t2,3) + 
                  12*Power(t2,4))) + 
            t1*(2*Power(s,3)*Power(t2,2)*(-1 + 3*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-5 + 10*t2 - 3*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s,2)*t2*
                (13 - 29*t2 + 22*Power(t2,2) + 8*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s*(1 - 22*t2 + 109*Power(t2,2) - 68*Power(t2,3) - 
                  22*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s1,2)*t1*(-5*Power(t1,4) - 
               2*Power(-1 + t2,2)*(-5 + 6*t2 + Power(t2,2)) + 
               Power(t1,3)*(-9 + 8*t2 + 3*Power(t2,2)) - 
               2*Power(t1,2)*
                (-9 - 38*t2 + 5*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(6 + 21*t2 - 28*Power(t2,2) - 4*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s*(-6 + 3*Power(t1,4) + 17*t2 - 19*Power(t1,3)*t2 - 
                  16*Power(t2,2) + 3*Power(t2,3) + 2*Power(t2,4) + 
                  Power(t1,2)*(24 - 11*t2 + 24*Power(t2,2)) - 
                  2*t1*(-4 - 16*t2 + 6*Power(t2,2) + 5*Power(t2,3)))) + 
            s1*(Power(t1,5)*(5 + 11*s - 3*Power(s,2) + t2) + 
               (-1 + t2)*(-6 + (16 - 6*s)*t2 + 
                  (-13 + 11*s)*Power(t2,2) + 
                  (2 - 3*s + Power(s,2))*Power(t2,3) + 
                  Power(-1 + s,2)*Power(t2,4)) + 
               Power(t1,4)*(-24 - 21*t2 + Power(t2,2) + 
                  Power(s,2)*(-2 + 17*t2) + 
                  s*(41 - 28*t2 - 6*Power(t2,2))) + 
               t1*(-2*Power(-1 + t2,2)*
                   (-1 - 14*t2 - 6*Power(t2,2) + 3*Power(t2,3)) - 
                  Power(s,2)*t2*
                   (13 - 39*t2 + 20*Power(t2,2) + 4*Power(t2,3)) + 
                  s*Power(-1 + t2,2)*
                   (7 + 18*t2 + Power(t2,2) + 6*Power(t2,3))) + 
               Power(t1,3)*(-44 - 61*t2 + 51*Power(t2,2) - 
                  2*Power(t2,3) + 
                  Power(s,2)*(-17 + 14*t2 - 27*Power(t2,2)) + 
                  s*(13 - 99*t2 + 29*Power(t2,2) + 18*Power(t2,3))) + 
               Power(t1,2)*(-1 + 87*t2 - 60*Power(t2,2) - 
                  32*Power(t2,3) + 6*Power(t2,4) + 
                  Power(s,2)*(-7 - 19*t2 + 8*Power(t2,2) + 
                     16*Power(t2,3)) + 
                  s*(16 - 149*t2 + 63*Power(t2,2) + Power(t2,3) - 
                     18*Power(t2,4))))) + 
         Power(s2,2)*(3 - (2 - 3*s + Power(s,2))*Power(t1,5) - 8*t2 + 
            11*s*t2 + 4*Power(t2,2) - 25*s*Power(t2,2) - 
            2*Power(s,2)*Power(t2,2) + 5*Power(t2,3) - 20*s*Power(t2,3) - 
            2*Power(s,2)*Power(t2,3) - 9*Power(t2,4) + 28*s*Power(t2,4) - 
            2*Power(s,3)*Power(t2,4) + 9*Power(t2,5) + 6*s*Power(t2,5) - 
            2*Power(s,2)*Power(t2,5) - 4*Power(t2,6) + 
            2*Power(s,2)*Power(t2,6) - 
            Power(t1,4)*(2*Power(s,3)*t2 + 4*(5 + 4*t2) - 
               Power(s,2)*(3 + 16*t2) + s*(-8 + 20*t2 + Power(t2,2))) + 
            Power(s1,3)*t1*(-5 + 13*t2 - 5*Power(t2,2) - 3*Power(t2,3) + 
               2*Power(t1,3)*(2 + t2) - 
               4*Power(t1,2)*(-2 + 5*t2 + Power(t2,2)) + 
               t1*(2 + t2 + 13*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,3)*(2*Power(s,3)*(1 + t2 + 3*Power(t2,2)) - 
               2*Power(s,2)*(-3 - 11*t2 + 21*Power(t2,2) + Power(t2,3)) + 
               2*(-16 - 22*t2 + 23*Power(t2,2) + Power(t2,3)) + 
               s*(-11 - 92*t2 + 10*Power(t2,2) + 10*Power(t2,3))) - 
            Power(t1,2)*(3 + 12*t2 - 92*Power(t2,2) + 22*Power(t2,3) + 
               19*Power(t2,4) + 
               2*Power(s,3)*t2*(-1 + 3*t2 + 3*Power(t2,2)) + 
               Power(s,2)*t2*(22 + 35*t2 - 38*Power(t2,2) - 
                  6*Power(t2,3)) + 
               s*(5 - 6*t2 - 112*Power(t2,2) - 48*Power(t2,3) + 
                  17*Power(t2,4))) + 
            t1*(2 + 20*t2 + 42*Power(t2,2) - 38*Power(t2,3) - 
               47*Power(t2,4) + 21*Power(t2,5) + 
               2*Power(s,3)*Power(t2,2)*(-2 + 3*t2 + Power(t2,2)) - 
               Power(s,2)*t2*(8 - 10*Power(t2,2) + 9*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s*(5 + 35*t2 - 35*Power(t2,2) - 104*Power(t2,3) - 
                  47*Power(t2,4) + 8*Power(t2,5))) - 
            Power(s1,2)*(Power(t1,5) + 
               (-3 + 2*s)*Power(t1,4)*(4 + 3*t2) + 
               (-1 + t2)*(-6 + 11*t2 - 2*Power(t2,2) + 
                  (-3 + s)*Power(t2,3)) + 
               Power(t1,3)*(-23 - 13*t2 + 22*Power(t2,2) + 
                  2*Power(t2,3) + s*(18 - 33*t2 - 14*Power(t2,2))) + 
               Power(t1,2)*(-20 + 118*t2 + 42*Power(t2,2) - 
                  22*Power(t2,3) - 4*Power(t2,4) + 
                  s*(34 - 16*t2 + 23*Power(t2,2) + 10*Power(t2,3))) + 
               t1*(-2 + 28*t2 - 16*Power(t2,2) - 15*Power(t2,3) + 
                  3*Power(t2,4) + 2*Power(t2,5) + 
                  s*(2 + 18*t2 + 9*Power(t2,2) - 9*Power(t2,3) - 
                     2*Power(t2,4)))) + 
            s1*(3 + (-1 + 2*s)*Power(t1,5) - (17 + 13*s)*t2 + 
               (14 + 15*s)*Power(t2,2) + 
               (10 + 8*s - 8*Power(s,2))*Power(t2,3) + 
               (-7 - 11*s + 7*Power(s,2))*Power(t2,4) + 
               (-5 + 3*s)*Power(t2,5) - 2*(-1 + s)*Power(t2,6) + 
               Power(t1,4)*(-16 + Power(t2,2) + Power(s,2)*(4 + 6*t2) - 
                  s*(16 + 25*t2)) + 
               Power(t1,3)*(13 + 64*t2 + 4*Power(t2,2) - 5*Power(t2,3) + 
                  Power(s,2)*(3 - 15*t2 - 16*Power(t2,2)) + 
                  s*(-54 - 48*t2 + 70*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,2)*(23 + 103*t2 - 12*Power(t2,2) - 
                  38*Power(t2,3) + 8*Power(t2,4) + 
                  Power(s,2)*(2 - 10*t2 + 19*Power(t2,2) + 
                     14*Power(t2,3)) + 
                  s*(-20 + 56*t2 + 70*Power(t2,2) - 63*Power(t2,3) - 
                     10*Power(t2,4))) + 
               t1*(22 - 72*t2 - 12*Power(t2,2) + 41*Power(t2,3) + 
                  27*Power(t2,4) - 6*Power(t2,5) - 
                  Power(s,2)*t2*
                   (4 - 21*t2 + 15*Power(t2,2) + 4*Power(t2,3)) + 
                  s*(-14 + 47*t2 + 24*Power(t2,2) + 9*Power(t2,3) + 
                     13*Power(t2,4) + 8*Power(t2,5))))))*
       T5q(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*(1 - s2 + t1 - t2)*
       Power(t1 - s2*t2,3)));
   return a;
};
