#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m231_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(Power(s1,3)*t1*Power(-1 + t2,2)*
          (Power(s2,4)*(3*t1*Power(t2,2) + Power(t1,2)*(1 + t2) + 
               Power(t2,2)*(5 + 2*t2)) + 
            Power(s2,3)*(-2*Power(t1,3)*(1 + t2) + 
               2*Power(t2,2)*(-5 + 12*t2 + 2*Power(t2,2)) + 
               t1*t2*(-5 - 23*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-3 + t2 + 8*Power(t2,2))) + 
            s2*(8*Power(t1,4)*(1 + t2) + 2*Power(t2,4)*(-5 + 7*t2) + 
               t1*Power(t2,2)*
                (60 - 83*t2 - 25*Power(t2,2) + 2*Power(t2,3)) - 
               2*Power(t1,3)*
                (-6 - 6*t2 + 15*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*t2*
                (24 + 115*t2 - 15*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s2,2)*(Power(t1,4)*(1 + t2) + 
               Power(t2,3)*(-20 + 33*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(1 + 13*t2 + 7*Power(t2,2)) + 
               t1*t2*(10 - 42*t2 - 56*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(2 + 9*t2 - 13*Power(t2,2) + 15*Power(t2,3))) \
- t1*(4*Power(t1,4) + 2*Power(t2,3)*(-5 + 7*t2) + 
               Power(t1,3)*(12 - 16*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               t1*t2*(40 - 50*t2 - 23*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(8 + 60*t2 - 9*Power(t2,2) + 11*Power(t2,3) - 
                  Power(t2,4)))) - 
         Power(s1,2)*t1*(-1 + t2)*
          (Power(s2,5)*t2*(Power(t1,2)*(-1 + 3*t2) - 
               t1*(1 + t2 + 2*Power(t2,2)) + 
               t2*(-5 + 2*t2 + 5*Power(t2,2))) + 
            Power(s2,4)*(-(Power(t1,3)*(-1 + t2 + 6*Power(t2,2))) + 
               t1*t2*(2 + 6*t2 - (15 + 8*s)*Power(t2,2) - 
                  7*Power(t2,3)) + 
               Power(t1,2)*(1 + (3 + 2*s)*t2 + (3 + 2*s)*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t2,2)*(-15 - 2*(11 + s)*t2 + 
                  (25 + 6*s)*Power(t2,2) + 16*Power(t2,3))) + 
            Power(s2,3)*(Power(t1,4)*(-2 + 5*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-2 + s + 2*t2 - 4*s*t2 - 11*Power(t2,2) - 
                  5*s*Power(t2,2) - 21*Power(t2,3)) - 
               t1*t2*(-25 - (77 + 19*s)*t2 + (22 + 26*s)*Power(t2,2) + 
                  (100 + 17*s)*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t2,2)*(35 - 3*(32 + 3*s)*t2 + 
                  (-34 + 7*s)*Power(t2,2) + 10*(8 + s)*Power(t2,3) + 
                  17*Power(t2,4)) + 
               Power(t1,2)*t2*
                (3 - 10*t2 + 44*Power(t2,2) + 11*Power(t2,3) + 
                  s*(-11 + 23*t2 + 12*Power(t2,2)))) + 
            s2*(-2*Power(t1,5)*(-4 + 7*t2 + 11*Power(t2,2)) - 
               (-1 + t2)*Power(t2,4)*
                (29 - 3*(11 + 3*s)*t2 + 2*(-18 + s)*Power(t2,2)) + 
               t1*Power(t2,2)*
                (-222 + (465 + 36*s)*t2 + (57 - 23*s)*Power(t2,2) - 
                  5*(49 + 4*s)*Power(t2,3) - (59 + s)*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(t1,2)*t2*
                (-102 - 4*(88 + 17*s)*t2 + (157 + 85*s)*Power(t2,2) + 
                  (284 + 31*s)*Power(t2,3) + (13 + 8*s)*Power(t2,4) + 
                  6*Power(t2,5)) + 
               Power(t1,4)*(8 + 6*t2 - 18*Power(t2,2) + 
                  59*Power(t2,3) + 7*Power(t2,4) + 
                  4*s*(-1 + 5*t2 + 6*Power(t2,2))) - 
               Power(t1,3)*t2*
                (-18 - 140*t2 + 172*Power(t2,2) + 9*Power(t2,3) + 
                  17*Power(t2,4) + 
                  s*(-36 + 73*t2 + 46*Power(t2,2) + 5*Power(t2,3)))) - 
            Power(s2,2)*(Power(t1,5)*(-1 + 3*t2) + 
               Power(t1,4)*(3 + s + 6*t2 - 2*s*t2 - 39*Power(t2,2) - 
                  3*s*Power(t2,2) - 10*Power(t2,3)) - 
               Power(t2,3)*(64 - (143 + 18*s)*t2 + 
                  20*(-1 + s)*Power(t2,2) + (93 + 2*s)*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(t1,2)*(1 + (73 + 2*s)*t2 + 
                  (25 + 29*s)*Power(t2,2) - 2*(29 + 41*s)*Power(t2,3) - 
                  3*(26 + 7*s)*Power(t2,4) - 11*Power(t2,5)) + 
               t1*t2*(41 - (200 + 3*s)*t2 - (153 + 47*s)*Power(t2,2) + 
                  2*(87 + 40*s)*Power(t2,3) + 
                  (149 + 10*s)*Power(t2,4) - 5*Power(t2,5)) + 
               Power(t1,3)*(5 - 12*t2 + 45*Power(t2,2) + 
                  10*Power(t2,3) + 32*Power(t2,4) + 
                  s*(1 - t2 + 24*Power(t2,2) + 16*Power(t2,3)))) + 
            t1*(4*Power(t1,5)*(-1 + 3*t2) + 
               (-1 + t2)*Power(t2,3)*
                (29 - 3*(11 + 3*s)*t2 + 2*(-18 + s)*Power(t2,2)) - 
               t1*t2*(-164 + (337 + 12*s)*t2 + (31 + 6*s)*Power(t2,2) - 
                  (164 + 15*s)*Power(t2,3) - (41 + 10*s)*Power(t2,4) + 
                  (1 + 3*s)*Power(t2,5)) + 
               Power(t1,2)*(4 + 8*(31 + s)*t2 + 9*(-9 + s)*Power(t2,2) - 
                  (158 + 21*s)*Power(t2,3) - 2*(3 + 13*s)*Power(t2,4) + 
                  (-9 + 6*s)*Power(t2,5)) - 
               Power(t1,4)*(4 - 20*t2 + 27*Power(t2,2) + 7*Power(t2,3) + 
                  4*s*(-1 + 5*t2)) + 
               Power(t1,3)*(4 - 96*t2 + 89*Power(t2,2) - 2*Power(t2,3) + 
                  17*Power(t2,4) + 
                  s*(4 - 16*t2 + 37*Power(t2,2) + 14*Power(t2,3) - 
                     3*Power(t2,4))))) + 
         s1*t1*(-8*Power(s2,6)*(-1 + t2)*Power(t2,2)*
             (t1 + Power(t1,2) - 3*t1*t2 + t2*(-1 + 2*t2)) - 
            Power(s2,5)*t2*(8*Power(t1,3)*(1 + t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(10 + 21*(-1 + s)*t2 - 
                  (56 + 9*s)*Power(t2,2) + 67*Power(t2,3)) + 
               t2*(-18 + (7 - 8*s)*t2 + (62 + 37*s)*Power(t2,2) - 
                  (93 + 17*s)*Power(t2,3) + 42*Power(t2,4)) + 
               t1*t2*(7 - 37*t2 + 117*Power(t2,2) - 87*Power(t2,3) + 
                  s*(8 - 58*t2 + 26*Power(t2,2)))) + 
            Power(s2,4)*(-8*Power(t1,4)*t2*(-2 + t2 + Power(t2,2)) + 
               Power(t1,3)*(2 + 2*(7 + 10*s)*t2 + 
                  (-111 + 25*s)*Power(t2,2) + (32 - 9*s)*Power(t2,3) + 
                  63*Power(t2,4)) + 
               Power(t2,2)*(7 + (80 + 9*s)*t2 + 
                  (-69 - 3*s + 13*Power(s,2))*Power(t2,2) - 
                  (148 + 77*s + Power(s,2))*Power(t2,3) + 
                  (164 + 47*s)*Power(t2,4) - 34*Power(t2,5)) + 
               t1*t2*(-19 - 3*(11 + 6*s)*t2 + 
                  (85 + 42*s - 26*Power(s,2))*Power(t2,2) + 
                  (-13 + 147*s + 2*Power(s,2))*Power(t2,3) - 
                  3*(42 + 29*s)*Power(t2,4) + 106*Power(t2,5)) + 
               Power(t1,2)*t2*
                (17 - 84*t2 - Power(s,2)*(-13 + t2)*t2 + 
                  202*Power(t2,2) + 4*Power(t2,3) - 139*Power(t2,4) + 
                  s*(9 - 59*t2 - 95*Power(t2,2) + 49*Power(t2,3)))) + 
            Power(s2,3)*(8*Power(t1,5)*(-1 + t2)*t2 + 
               Power(t1,4)*(-3 + s + 30*t2 - 33*s*t2 + 
                  101*Power(t2,2) - 4*s*Power(t2,2) - 108*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(t1,2)*(1 + (41 + 3*s)*t2 + 
                  (76 + 85*s - 19*Power(s,2))*Power(t2,2) + 
                  (-279 - 490*s + 78*Power(s,2))*Power(t2,3) + 
                  (332 + 17*s + 13*Power(s,2))*Power(t2,4) + 
                  (-54 + 97*s)*Power(t2,5) - 117*Power(t2,6)) + 
               Power(t2,2)*(-46 + (115 - 7*s)*t2 + 
                  (93 + 74*s - 39*Power(s,2))*Power(t2,2) + 
                  (-184 - 125*s + 70*Power(s,2))*Power(t2,3) + 
                  (-143 + 7*s - 7*Power(s,2))*Power(t2,4) + 
                  3*(57 + 13*s)*Power(t2,5) - 6*Power(t2,6)) + 
               t1*t2*(-3 + (-202 + 5*s)*t2 + 
                  (26 - 161*s + 68*Power(s,2))*Power(t2,2) + 
                  (425 + 448*s - 144*Power(s,2))*Power(t2,3) + 
                  (-128 - 45*s + 4*Power(s,2))*Power(t2,4) - 
                  (167 + 103*s)*Power(t2,5) + 49*Power(t2,6)) - 
               Power(t1,3)*(2 - 30*t2 + 28*Power(t2,2) + 
                  322*Power(t2,3) - 234*Power(t2,4) - 88*Power(t2,5) + 
                  2*Power(s,2)*t2*(5 + 2*t2 + 5*Power(t2,2)) + 
                  s*(1 - t2 - 200*Power(t2,2) - 25*Power(t2,3) + 
                     33*Power(t2,4)))) + 
            Power(s2,2)*(Power(t1,4)*
                (-7 + (-53 - 75*s + 2*Power(s,2))*t2 + 
                  (223 - 200*s + 7*Power(s,2))*Power(t2,2) + 
                  (55 + 27*s + 3*Power(s,2))*Power(t2,3) + 
                  4*(-51 + 2*s)*Power(t2,4) - 14*Power(t2,5)) + 
               Power(t1,2)*(-1 + (117 + 10*s)*t2 + 
                  (221 + 146*s - 60*Power(s,2))*Power(t2,2) + 
                  (-301 - 290*s + 114*Power(s,2))*Power(t2,3) + 
                  (-414 - 439*s + 136*Power(s,2))*Power(t2,4) + 
                  (345 + 194*s + 26*Power(s,2))*Power(t2,5) + 
                  (76 + 91*s)*Power(t2,6) - 43*Power(t2,7)) + 
               Power(t2,2)*(6 + 2*(-55 + 6*s)*t2 + 
                  (231 - 34*s)*Power(t2,2) + 
                  (8 + 125*s - 78*Power(s,2))*Power(t2,3) + 
                  (-193 - 205*s + 101*Power(s,2))*Power(t2,4) + 
                  (-46 + 97*s - 11*Power(s,2))*Power(t2,5) + 
                  (102 + 5*s)*Power(t2,6) + 2*Power(t2,7)) + 
               t1*t2*(37 - (108 + 23*s)*t2 + 
                  (-404 - 30*s + 30*Power(s,2))*Power(t2,2) + 
                  (406 - 65*s + 61*Power(s,2))*Power(t2,3) + 
                  (485 + 432*s - 216*Power(s,2))*Power(t2,4) + 
                  (-258 - 229*s + 5*Power(s,2))*Power(t2,5) - 
                  (166 + 49*s)*Power(t2,6) + 8*Power(t2,7)) + 
               Power(t1,5)*(1 - 66*t2 + 13*Power(t2,2) + 
                  52*Power(t2,3) + s*(-1 + 13*t2)) + 
               Power(t1,3)*(-1 - 107*t2 + 143*Power(t2,2) - 
                  29*Power(t2,3) - 211*Power(t2,4) + 158*Power(t2,5) + 
                  47*Power(t2,6) - 
                  Power(s,2)*t2*
                   (-30 + 99*t2 + 28*Power(t2,2) + 23*Power(t2,3)) + 
                  s*(1 - 82*t2 + 306*Power(t2,2) + 399*Power(t2,3) - 
                     89*Power(t2,4) - 55*Power(t2,5)))) - 
            t1*(Power(t1,4)*(4 + (64 + 4*s - 4*Power(s,2))*t2 + 
                  (-139 - 143*s + 52*Power(s,2))*Power(t2,2) + 
                  (104 + 23*s)*Power(t2,3) + (-31 + 8*s)*Power(t2,4) - 
                  2*Power(t2,5)) - 
               (-1 + t2)*Power(t2,2)*
                (24 + (-77 + 24*s)*t2 + 
                  (63 - 33*s + 6*Power(s,2))*Power(t2,2) + 
                  (54 + 39*s - 42*Power(s,2))*Power(t2,3) - 
                  (41 + 28*s + Power(s,2))*Power(t2,4) + 
                  (-23 - 2*s + 3*Power(s,2))*Power(t2,5)) + 
               Power(t1,2)*(-4 + 16*(11 + s)*t2 + 
                  (59 + 231*s - 258*Power(s,2))*Power(t2,2) + 
                  (-379 - 468*s + 192*Power(s,2))*Power(t2,3) + 
                  (-67 + 136*s + 117*Power(s,2))*Power(t2,4) + 
                  3*(59 + 11*s + 10*Power(s,2))*Power(t2,5) + 
                  (44 + 40*s - 9*Power(s,2))*Power(t2,6) - 6*Power(t2,7)\
) + t1*t2*(148 - (329 + 44*s)*t2 + 
                  5*(-17 - 6*s + 24*Power(s,2))*Power(t2,2) + 
                  (369 + 124*s - 26*Power(s,2))*Power(t2,3) + 
                  (46 + 13*s - 94*Power(s,2))*Power(t2,4) - 
                  (100 + 44*s + 21*Power(s,2))*Power(t2,5) + 
                  (-51 - 19*s + 9*Power(s,2))*Power(t2,6) + 
                  2*Power(t2,7)) + 
               4*Power(t1,5)*
                (1 - 3*t2 - Power(t2,2) + 3*Power(t2,3) + 
                  s*(-1 + 13*t2)) + 
               Power(t1,3)*(-4 - 184*t2 + 197*Power(t2,2) + 
                  165*Power(t2,3) - 183*Power(t2,4) + 3*Power(t2,5) + 
                  6*Power(t2,6) + 
                  Power(s,2)*t2*
                   (132 - 114*t2 - 116*Power(t2,2) - 13*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  s*(4 - 144*t2 + 272*Power(t2,2) + 9*Power(t2,3) - 
                     38*Power(t2,4) - 31*Power(t2,5)))) + 
            s2*(-32*Power(t1,6)*(-1 + t2)*t2 - 
               (-1 + t2)*Power(t2,3)*
                (18 + 2*(-31 + 6*s)*t2 + (57 - 15*s)*Power(t2,2) + 
                  (42 + 45*s - 39*Power(s,2))*Power(t2,3) + 
                  (-29 - 46*s + 5*Power(s,2))*Power(t2,4) + 
                  (-26 + 4*s)*Power(t2,5)) + 
               Power(t1,2)*t2*
                (16 + (469 + 20*s - 12*Power(s,2))*t2 + 
                  (-181 + 451*s - 386*Power(s,2))*Power(t2,2) + 
                  (-694 - 979*s + 463*Power(s,2))*Power(t2,3) + 
                  (11 + 332*s + 100*Power(s,2))*Power(t2,4) + 
                  (349 + 106*s + 3*Power(s,2))*Power(t2,5) + 
                  (36 + 34*s)*Power(t2,6) - 6*Power(t2,7)) + 
               t1*Power(t2,2)*
                (246 + (-569 - 10*s + 6*Power(s,2))*t2 + 
                  5*(-12 - 43*s + 42*Power(s,2))*Power(t2,2) + 
                  (550 + 377*s - 160*Power(s,2))*Power(t2,3) + 
                  (74 - 48*s - 86*Power(s,2))*Power(t2,4) + 
                  (-187 - 97*s + 6*Power(s,2))*Power(t2,5) - 
                  7*(8 + s)*Power(t2,6) + 2*Power(t2,7)) + 
               Power(t1,3)*(-4 + 2*(-88 - 13*s + 3*Power(s,2))*t2 + 
                  (-154 - 225*s + 142*Power(s,2))*Power(t2,2) + 
                  (566 + 757*s - 290*Power(s,2))*Power(t2,3) - 
                  2*(-12 + 53*s + 59*Power(s,2))*Power(t2,4) - 
                  (308 + 129*s + 4*Power(s,2))*Power(t2,5) + 
                  (46 - 31*s)*Power(t2,6) + 6*Power(t2,7)) + 
               2*Power(t1,5)*(6 + 6*t2 - 81*Power(t2,2) + 
                  56*Power(t2,3) + 13*Power(t2,4) + 
                  s*(-2 + 61*t2 + 29*Power(t2,2) - 4*Power(t2,3))) + 
               Power(t1,4)*(8 + 60*t2 - 71*Power(t2,2) + 20*Power(t2,3) + 
                  63*Power(t2,4) - 78*Power(t2,5) - 2*Power(t2,6) + 
                  2*Power(s,2)*t2*(17 + 13*t2 + 30*Power(t2,2)) + 
                  s*(4 + 20*t2 - 337*Power(t2,2) - 145*Power(t2,3) + 
                     78*Power(t2,4) + 8*Power(t2,5))))) + 
         t2*(8*Power(s2,6)*t1*(-1 + t2)*t2*
             (t1 + Power(t1,2) - 3*t1*t2 + t2*(-1 + 2*t2)) + 
            Power(s2,5)*(-2*Power(-1 + t2,2)*Power(t2,2)*(1 + t2) + 
               8*Power(t1,4)*(1 + t2 - 2*Power(t2,2)) + 
               Power(t1,3)*(8 + (-6 + 4*s)*t2 + 
                  (-88 + 13*s)*Power(t2,2) + (92 - 5*s)*Power(t2,3) - 
                  6*Power(t2,4)) - 
               t1*t2*(10 + (6 + 8*s)*t2 - 4*(19 + 5*s)*Power(t2,2) + 
                  (121 - 5*s)*Power(t2,3) + (-66 + 5*s)*Power(t2,4) + 
                  5*Power(t2,5)) + 
               Power(t1,2)*t2*
                (14 - 74*t2 + 185*Power(t2,2) - 136*Power(t2,3) + 
                  11*Power(t2,4) + 
                  2*s*(4 - 12*t2 - 9*Power(t2,2) + 5*Power(t2,3)))) + 
            Power(s2,3)*(-8*Power(t1,6)*(-1 + t2) - 
               4*Power(-1 + t2,2)*Power(t2,2)*
                (-1 - t2 + 3*Power(t2,2) + 3*Power(t2,3)) + 
               t1*t2*(8 + 4*(4 + s)*t2 + 
                  (-96 - 65*s + 39*Power(s,2) + 2*Power(s,3))*
                   Power(t2,2) + 
                  (2 + 43*s + 3*Power(s,2) - 13*Power(s,3))*
                   Power(t2,3) - 
                  (-192 - 129*s + 101*Power(s,2) + Power(s,3))*
                   Power(t2,4) + 
                  (-149 - 106*s + 11*Power(s,2))*Power(t2,5) + 
                  (36 + 7*s)*Power(t2,6) - 9*Power(t2,7)) + 
               Power(t1,2)*(-6 + 94*t2 + 
                  (79 + 165*s - 74*Power(s,2) - 4*Power(s,3))*
                   Power(t2,2) + 
                  (-387 - 218*s + 22*Power(s,2) + 31*Power(s,3))*
                   Power(t2,3) + 
                  (233 - 307*s + 198*Power(s,2) + 9*Power(s,3))*
                   Power(t2,4) + 
                  (64 + 244*s - 26*Power(s,2))*Power(t2,5) - 
                  4*(25 + 7*s)*Power(t2,6) + 23*Power(t2,7)) + 
               Power(t1,5)*(Power(s,2)*(5 + 16*t2 + 3*Power(t2,2)) + 
                  s*(15 + 5*t2 + 12*Power(t2,2) + 4*Power(t2,3)) + 
                  2*(-11 - 59*t2 + 62*Power(t2,2) + 8*Power(t2,3))) + 
               Power(t1,4)*(-4 - 101*t2 + 578*Power(t2,2) - 
                  417*Power(t2,3) - 60*Power(t2,4) + 4*Power(t2,5) + 
                  Power(s,3)*t2*(5 + 7*t2) - 
                  2*Power(s,2)*
                   (-2 - 9*t2 + 18*Power(t2,2) + 5*Power(t2,3)) + 
                  s*(11 - 82*t2 - 115*Power(t2,2) + 16*Power(t2,3) - 
                     22*Power(t2,4))) - 
               Power(t1,3)*(16 + 137*t2 - 480*Power(t2,2) + 
                  743*Power(t2,3) - 320*Power(t2,4) - 114*Power(t2,5) + 
                  18*Power(t2,6) + 
                  Power(s,3)*t2*(-2 + 23*t2 + 15*Power(t2,2)) + 
                  Power(s,2)*t2*
                   (-31 + 48*t2 + 77*Power(t2,2) - 22*Power(t2,3)) + 
                  s*(4 + 111*t2 - 242*Power(t2,2) - 288*Power(t2,3) + 
                     166*Power(t2,4) - 39*Power(t2,5)))) + 
            Power(s2,4)*(-8*Power(-1 + t2,2)*Power(t2,3)*(1 + t2) + 
               8*Power(t1,5)*(-2 + t2 + Power(t2,2)) - 
               t1*t2*(-8 + 4*(13 + s)*t2 + 
                  (10 + 19*s + 4*Power(s,2))*Power(t2,2) + 
                  (-192 - 73*s + 26*Power(s,2))*Power(t2,3) + 
                  (218 + 21*s - 6*Power(s,2))*Power(t2,4) + 
                  (-92 + 5*s)*Power(t2,5) + 12*Power(t2,6)) + 
               Power(t1,2)*(10 + 2*(11 + 6*s)*t2 + 
                  2*(-19 + 5*s + 4*Power(s,2))*Power(t2,2) + 
                  (-68 - 138*s + 47*Power(s,2))*Power(t2,3) + 
                  (278 + 29*s - 19*Power(s,2))*Power(t2,4) + 
                  (-232 + 3*s)*Power(t2,5) + 28*Power(t2,6)) - 
               Power(t1,4)*(26 - 134*t2 + 42*Power(t2,2) + 
                  68*Power(t2,3) - 2*Power(t2,4) + 
                  Power(s,2)*t2*(5 + 7*t2) + 
                  s*(4 + 28*t2 - 3*Power(t2,2) + 7*Power(t2,3))) + 
               Power(t1,3)*(4*Power(s,2)*t2*(-1 - 4*t2 + 5*Power(t2,2)) + 
                  s*(-8 + 13*t2 + 93*Power(t2,2) - 11*Power(t2,3) + 
                     9*Power(t2,4)) - 
                  2*(11 - 51*t2 + 101*Power(t2,2) + 36*Power(t2,3) - 
                     106*Power(t2,4) + 9*Power(t2,5)))) + 
            Power(s2,2)*(-2*Power(-1 + t2,2)*Power(t2,2)*
                (1 - 3*t2 - 4*Power(t2,2) + 4*Power(t2,3) + 
                  4*Power(t2,4)) + 
               t1*t2*(-1 + (19 + 3*s)*t2 + 
                  (20 - 8*s + Power(s,2) + 2*Power(s,3))*Power(t2,2) + 
                  (-102 - 96*s + 73*Power(s,2) + 3*Power(s,3))*
                   Power(t2,3) + 
                  (17 + 103*s + 25*Power(s,2) - 28*Power(s,3))*
                   Power(t2,4) - 
                  (-79 - 118*s + 127*Power(s,2) + Power(s,3))*
                   Power(t2,5) + 
                  (-10 - 129*s + 4*Power(s,2))*Power(t2,6) + 
                  (-20 + 9*s)*Power(t2,7) - 2*Power(t2,8)) + 
               Power(t1,2)*(-9 - (33 + 10*s)*t2 + 
                  (196 + 105*s - 44*Power(s,2) - 6*Power(s,3))*
                   Power(t2,2) + 
                  (52 + 143*s - 118*Power(s,2) + Power(s,3))*
                   Power(t2,3) + 
                  (-465 - 431*s + 138*Power(s,2) + 75*Power(s,3))*
                   Power(t2,4) + 
                  (281 - 91*s + 247*Power(s,2) + 14*Power(s,3))*
                   Power(t2,5) + 
                  (-70 + 273*s + 5*Power(s,2))*Power(t2,6) + 
                  (42 - 25*s)*Power(t2,7) + 6*Power(t2,8)) - 
               Power(t1,6)*(-68 + 18*t2 + 50*Power(t2,2) + 
                  3*Power(s,2)*(3 + t2) + s*(3 + 5*t2 + 4*Power(t2,2))) + 
               Power(t1,5)*(-(Power(s,3)*(4 + 7*t2 + Power(t2,2))) + 
                  2*Power(s,2)*
                   (-1 + 16*t2 + 32*Power(t2,2) + Power(t2,3)) + 
                  2*(51 - 163*t2 - 9*Power(t2,2) + 119*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  s*(7 + 178*t2 - 17*Power(t2,2) + 66*Power(t2,3) + 
                     6*Power(t2,4))) + 
               Power(t1,4)*(99 - 191*t2 + 113*Power(t2,2) + 
                  345*Power(t2,3) - 358*Power(t2,4) - 10*Power(t2,5) + 
                  2*Power(t2,6) + 
                  Power(s,3)*
                   (-2 + 15*t2 + 33*Power(t2,2) + 14*Power(t2,3)) + 
                  Power(s,2)*
                   (-42 + 32*t2 + 151*Power(t2,2) - 126*Power(t2,3) + 
                     9*Power(t2,4)) - 
                  s*(-81 + 63*t2 + 564*Power(t2,2) - 190*Power(t2,3) + 
                     105*Power(t2,4) + 19*Power(t2,5))) - 
               Power(t1,3)*(44 + 123*t2 - 73*Power(t2,2) - 
                  421*Power(t2,3) + 545*Power(t2,4) - 240*Power(t2,5) + 
                  16*Power(t2,6) + 6*Power(t2,7) + 
                  Power(s,3)*t2*
                   (-6 + 15*t2 + 73*Power(t2,2) + 26*Power(t2,3)) + 
                  Power(s,2)*t2*
                   (-85 - 15*t2 + 337*Power(t2,2) + 55*Power(t2,3) + 
                     20*Power(t2,4)) + 
                  s*(-7 + 178*t2 - 9*Power(t2,2) - 717*Power(t2,3) + 
                     195*Power(t2,4) + 101*Power(t2,5) - 29*Power(t2,6)))) \
+ t1*(Power(t2,2)*(-1 + Power(t2,2))*
                (-4 + (13 - 6*s)*t2 + 
                  (-13 + 15*s - 6*Power(s,2))*Power(t2,2) + 
                  (3 - 12*s + 9*Power(s,2) - 2*Power(s,3))*Power(t2,3) + 
                  (1 + 3*s - 3*Power(s,2) + Power(s,3))*Power(t2,4)) + 
               Power(t1,2)*(32 + 6*(-10 + s + 4*Power(s,2))*t2 + 
                  (-2 - 295*s + 174*Power(s,2) + 24*Power(s,3))*
                   Power(t2,2) - 
                  (5 - 286*s + Power(s,2) + 28*Power(s,3))*Power(t2,3) + 
                  (27 + 195*s - 89*Power(s,2) - 73*Power(s,3))*
                   Power(t2,4) - 
                  (-45 + 84*s + 111*Power(s,2) + 13*Power(s,3))*
                   Power(t2,5) + 
                  (-5 - 112*s - 21*Power(s,2) + 6*Power(s,3))*
                   Power(t2,6) + 4*(-8 + s)*Power(t2,7)) - 
               t1*t2*(-12 + (11 + 4*s + 12*Power(s,2))*t2 + 
                  (35 - 32*s + 4*Power(s,2) + 6*Power(s,3))*
                   Power(t2,2) + 
                  (-54 - 7*s - 2*Power(s,2) + 8*Power(s,3))*
                   Power(t2,3) + 
                  (8 + 55*s + 24*Power(s,2) - 23*Power(s,3))*
                   Power(t2,4) + 
                  (9 + 21*s - 28*Power(s,2) - 7*Power(s,3))*
                   Power(t2,5) + 
                  (17 - 43*s - 10*Power(s,2) + 4*Power(s,3))*
                   Power(t2,6) + 2*(-7 + s)*Power(t2,7)) + 
               4*Power(t1,6)*(3*Power(s,2)*(3 + t2) + 
                  3*(-1 + Power(t2,2)) + s*(2 + 7*t2 + 3*Power(t2,2))) + 
               Power(t1,5)*(12*Power(s,3)*(1 + 3*t2) + 
                  Power(s,2)*(4 - 12*t2 - 5*Power(t2,2) + Power(t2,3)) - 
                  2*t2*(-20 + 14*t2 + 5*Power(t2,2) + Power(t2,3)) + 
                  s*(64 - 182*t2 + 43*Power(t2,2) - 31*Power(t2,3) - 
                     2*Power(t2,4))) + 
               Power(t1,4)*(Power(s,3)*
                   (12 - 62*t2 - 102*Power(t2,2) - 5*Power(t2,3) + 
                     Power(t2,4)) - 
                  2*Power(s,2)*
                   (-92 + 16*t2 + 115*Power(t2,2) + 27*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  4*(11 - 21*t2 + 34*Power(t2,2) - 40*Power(t2,3) + 
                     16*Power(t2,4)) + 
                  s*(-240 + 166*t2 + 465*Power(t2,2) - 332*Power(t2,3) + 
                     9*Power(t2,4) + 4*Power(t2,5))) + 
               Power(t1,3)*(24 + 36*t2 - 83*Power(t2,2) + 
                  91*Power(t2,3) - 171*Power(t2,4) + 83*Power(t2,5) + 
                  20*Power(t2,6) + 
                  Power(s,3)*t2*
                   (-30 + 84*t2 + 117*Power(t2,2) + 13*Power(t2,3) - 
                     4*Power(t2,4)) + 
                  Power(s,2)*(-12 - 360*t2 + 36*Power(t2,2) + 
                     322*Power(t2,3) + 121*Power(t2,4) + 25*Power(t2,5)) \
+ s*(-8 + 518*t2 - 529*Power(t2,2) - 443*Power(t2,3) + 378*Power(t2,4) + 
                     76*Power(t2,5) - 4*Power(t2,6)))) + 
            s2*(32*Power(t1,7)*(-1 + t2) - 
               2*Power(-1 + t2,3)*Power(t2,3)*
                (-1 + 2*Power(t2,2) + Power(t2,3)) - 
               2*Power(t1,6)*(Power(s,2)*(8 + 45*t2 + 7*Power(t2,2)) + 
                  2*(11 - 56*t2 + 40*Power(t2,2) + 5*Power(t2,3)) + 
                  s*(31 + 8*t2 + 36*Power(t2,2) + 9*Power(t2,3))) + 
               Power(t1,5)*(-100 + 286*t2 - 468*Power(t2,2) + 
                  244*Power(t2,3) + 38*Power(t2,4) + 
                  Power(s,3)*(2 - 30*t2 - 44*Power(t2,2)) - 
                  Power(s,2)*(28 + 56*t2 - 113*Power(t2,2) + 
                     4*Power(t2,3) + Power(t2,4)) + 
                  s*(-46 + 188*t2 + 155*Power(t2,2) + 23*Power(t2,3) + 
                     50*Power(t2,4) + 2*Power(t2,5))) + 
               t1*Power(t2,2)*
                (Power(s,3)*Power(t2,2)*
                   (4 - 17*Power(t2,2) + Power(t2,3)) - 
                  Power(s,2)*t2*
                   (-6 + 8*t2 - 31*Power(t2,2) - 27*Power(t2,3) + 
                     55*Power(t2,4) + Power(t2,5)) + 
                  s*Power(-1 + t2,2)*
                   (-6 - 8*t2 - 12*Power(t2,2) - 47*Power(t2,3) - 
                     45*Power(t2,4) + 2*Power(t2,5)) - 
                  2*Power(-1 + t2,2)*
                   (9 + 8*t2 - 14*Power(t2,2) - 3*Power(t2,3) + 
                     5*Power(t2,4) + 7*Power(t2,5))) + 
               Power(t1,4)*(52 + 168*t2 - 520*Power(t2,2) + 
                  525*Power(t2,3) - 288*Power(t2,4) + 57*Power(t2,5) + 
                  6*Power(t2,6) + 
                  Power(s,3)*(-2 - 14*t2 + 128*Power(t2,2) + 
                     113*Power(t2,3) + 3*Power(t2,4)) + 
                  2*Power(s,2)*
                   (4 - 105*t2 + 75*Power(t2,2) + 163*Power(t2,3) + 
                     37*Power(t2,4) + 6*Power(t2,5)) + 
                  s*(26 + 376*t2 - 549*Power(t2,2) - 512*Power(t2,3) + 
                     461*Power(t2,4) - 38*Power(t2,5) - 4*Power(t2,6))) - 
               Power(t1,3)*(-28 + 134*t2 + 100*Power(t2,2) - 
                  367*Power(t2,3) + 224*Power(t2,4) - 205*Power(t2,5) + 
                  116*Power(t2,6) + 26*Power(t2,7) + 
                  Power(s,3)*t2*
                   (-4 - 26*t2 + 166*Power(t2,2) + 111*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  Power(s,2)*t2*
                   (10 - 496*t2 + 37*Power(t2,2) + 604*Power(t2,3) + 
                     135*Power(t2,4) + 22*Power(t2,5)) + 
                  s*(6 + 8*t2 + 704*Power(t2,2) - 807*Power(t2,3) - 
                     652*Power(t2,4) + 658*Power(t2,5) + 51*Power(t2,6) - 
                     4*Power(t2,7))) + 
               Power(t1,2)*t2*(Power(s,3)*t2*
                   (-2 - 18*t2 + 68*Power(t2,2) + 59*Power(t2,3) + 
                     Power(t2,4)) + 
                  2*Power(s,2)*t2*
                   (-2 - 125*t2 - 36*Power(t2,2) + 114*Power(t2,3) + 
                     67*Power(t2,4) + 6*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (-56 - 50*t2 + 50*Power(t2,2) + 117*Power(t2,3) + 
                     93*Power(t2,4) + 34*Power(t2,5)) + 
                  s*(12 - 22*t2 + 376*Power(t2,2) - 353*Power(t2,3) - 
                     316*Power(t2,4) + 201*Power(t2,5) + 106*Power(t2,6) - 
                     4*Power(t2,7)))))))/
     ((-1 + s1)*(-1 + t1)*t1*(-s + s1 - t2)*Power(t1 - t2,2)*
       (1 - s2 + t1 - t2)*Power(-1 + t2,2)*t2*(t1 - s2*t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) + 
    (8*(Power(1 + t2,3)*Power(1 + (-1 + s)*t2,3)*
          (2 - 3*t2 + Power(t2,2)) - 
         Power(s2,7)*Power(t2,2)*((-3 + t2)*t2 + s1*(1 + t2)) - 
         Power(t1,6)*(-9 - 2*t2 + Power(t2,2) + Power(s,2)*(1 + t2) + 
            Power(s1,2)*(1 + t2) + s1*(-2 + t2 + Power(t2,2)) - 
            s*(2*s1*(1 + t2) + t2*(3 + t2))) - 
         Power(t1,5)*(9 + 40*t2 + 6*Power(t2,2) - 5*Power(t2,3) + 
            Power(s,3)*t2*(3 + t2) - 
            Power(s1,3)*(-1 + 2*t2 + Power(t2,2)) + 
            Power(s1,2)*(6 + 9*t2 - 5*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s,2)*(11 + s1 - 5*t2 - 8*s1*t2 - 12*Power(t2,2) - 
               3*s1*Power(t2,2) - 2*Power(t2,3)) - 
            2*s1*(4 - t2 + Power(t2,3)) + 
            s*(-12 - 9*t2 + 20*Power(t2,2) + 7*Power(t2,3) + 
               Power(s1,2)*(-2 + 7*t2 + 3*Power(t2,2)) + 
               s1*(-19 - 6*t2 + 17*Power(t2,2) + 4*Power(t2,3)))) - 
         t1*(1 + t2)*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(5 + 6*t2 - 14*Power(t2,2) - 2*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(-1 + t2,2)*
             (-4 - 16*t2 - Power(t2,2) + 2*Power(t2,3) + 
               s1*(-1 - 5*t2 - Power(t2,2) + 3*Power(t2,3))) - 
            s*(-1 + t2)*(-1 - 13*t2 - 39*Power(t2,2) + 9*Power(t2,4) + 
               s1*(6 + 8*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(t1,4)*(-43 + 23*t2 + 62*Power(t2,2) + 11*Power(t2,3) - 
            7*Power(t2,4) + Power(s1,2)*
             (-31 + 33*t2 + 24*Power(t2,2) - 5*Power(t2,3) - 
               3*Power(t2,4)) - 
            Power(s1,3)*(-4 + 6*t2 - 2*Power(t2,2) + Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,3)*(2 - 2*t2 + 4*Power(t2,2) + 5*Power(t2,3) + 
               Power(t2,4)) - 
            s1*(-32 + 40*t2 - 8*Power(t2,2) + Power(t2,3) + 
               5*Power(t2,4)) - 
            Power(s,2)*(s1*(8 - 2*t2 + 2*Power(t2,2) + 
                  11*Power(t2,3) + 3*Power(t2,4)) + 
               2*(8 - 28*t2 - 12*Power(t2,2) + 17*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s*(32 - 68*t2 - 74*Power(t2,2) + 42*Power(t2,3) + 
               20*Power(t2,4) + 
               Power(s1,2)*(2 + 6*t2 - 4*Power(t2,2) + 7*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(37 - 50*t2 - 67*Power(t2,2) + 27*Power(t2,3) + 
                  13*Power(t2,4)))) - 
         Power(t1,3)*(Power(s,3)*t2*
             (9 - 7*t2 - 9*Power(t2,2) + 9*Power(t2,3) + 4*Power(t2,4)) \
- (-1 + t2)*(-37 - 43*t2 - 37*Power(t2,2) - 2*Power(t2,3) + 
               5*Power(t2,4) + Power(s1,3)*(1 + 2*t2 + Power(t2,4)) + 
               4*Power(s1,2)*
                (-3 - 2*t2 - 7*Power(t2,2) + Power(t2,3) + Power(t2,4)) \
+ s1*(61 - 17*t2 - 24*Power(t2,2) + 4*Power(t2,3) + 6*Power(t2,4))) - 
            Power(s,2)*(17 + 71*t2 - 100*Power(t2,2) - 99*Power(t2,3) + 
               40*Power(t2,4) + 21*Power(t2,5) + 
               s1*(-13 + 17*t2 + 22*Power(t2,2) - 30*Power(t2,3) + 
                  9*Power(t2,4) + 9*Power(t2,5))) + 
            s*(65 + 118*t2 - 124*Power(t2,2) - 162*Power(t2,3) + 
               31*Power(t2,4) + 28*Power(t2,5) + 
               Power(s1,2)*(10 - 23*t2 + 30*Power(t2,2) - 
                  16*Power(t2,3) - Power(t2,4) + 6*Power(t2,5)) + 
               s1*(-83 + 118*t2 + 33*Power(t2,2) - 123*Power(t2,3) + 
                  9*Power(t2,4) + 20*Power(t2,5)))) + 
         Power(t1,2)*(Power(s,3)*t2*
             (3 + 15*t2 - 11*Power(t2,2) - 22*Power(t2,3) + 
               7*Power(t2,4) + 6*Power(t2,5)) - 
            Power(-1 + t2,2)*
             (t2*(-20 - 11*t2 + 2*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(2 + t2 - 3*Power(t2,2) + Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(-2 - 30*t2 - 41*Power(t2,2) + 4*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s*(-1 + t2)*(12 - 4*t2 - 165*Power(t2,2) - 
               129*Power(t2,3) + 22*Power(t2,4) + 22*Power(t2,5) + 
               Power(s1,2)*(-4 - t2 + 4*Power(t2,2) - 4*Power(t2,3) - 
                  2*Power(t2,4) + 3*Power(t2,5)) + 
               s1*(7 + 72*t2 + 27*Power(t2,2) - 76*Power(t2,3) + 
                  3*Power(t2,4) + 17*Power(t2,5))) - 
            Power(s,2)*(1 + 31*t2 + 94*Power(t2,2) - 66*Power(t2,3) - 
               125*Power(t2,4) + 18*Power(t2,5) + 23*Power(t2,6) + 
               s1*(-4 - 24*t2 + 22*Power(t2,2) + 34*Power(t2,3) - 
                  32*Power(t2,4) - 3*Power(t2,5) + 9*Power(t2,6)))) + 
         Power(s2,6)*t2*(t2*(1 + 2*(6 + s)*t2 + 2*(7 + s)*Power(t2,2) - 
               5*Power(t2,3) - t1*(9 + s - 3*t2)*(1 + t2)) + 
            s1*(t1*(2 + 6*t2 + 4*Power(t2,2)) + 
               t2*(s*(1 + t2) - t2*(5 + t2)))) + 
         Power(s2,5)*(Power(s1,3)*(-1 + t2) + 
            Power(s1,2)*t2*(-1 + s + t1 + t2 - s*t2 + t1*t2 + 
               4*Power(t2,2) + 2*t1*Power(t2,2)) - 
            s1*(Power(t1,2)*(1 + 9*t2 + 14*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(t2,2)*(8 - 13*Power(t2,2) + 3*Power(t2,3) + 
                  s*(-3 + 4*t2 + Power(t2,2))) + 
               t1*t2*(-6*t2*(2 + 2*t2 + Power(t2,2)) + 
                  s*(2 + 7*t2 + 5*Power(t2,2)))) - 
            t2*(t1*(2 + (39 + 5*s - Power(s,2))*t2 + 
                  (72 + 13*s - Power(s,2))*Power(t2,2) + 
                  12*(1 + s)*Power(t2,3) - 11*Power(t2,4)) - 
               Power(t1,2)*(9 + 24*t2 - 3*Power(t2,3) + 
                  s*(2 + 5*t2 + 3*Power(t2,2))) + 
               t2*(Power(s,2)*t2*(1 + t2) + 
                  t2*(1 - 34*t2 - 22*Power(t2,2) + 9*Power(t2,3)) - 
                  s*(-1 - 4*t2 + Power(t2,2) + 12*Power(t2,3))))) + 
         Power(s2,4)*(-(Power(t1,3)*(1 + t2)*
               (3 + s + 23*t2 + 6*s*t2 - 5*Power(t2,2) + 
                 3*s*Power(t2,2) - Power(t2,3))) + 
            Power(s1,3)*(3 - 5*t2 + 2*Power(t2,2) + 
               2*(-1 + t1)*Power(t2,3)) + 
            Power(t1,2)*(1 + (42 + 4*s - 2*Power(s,2))*t2 + 
               (134 + 24*s - 6*Power(s,2))*Power(t2,2) + 
               (97 + 41*s - 4*Power(s,2))*Power(t2,3) + 
               (-21 + 19*s)*Power(t2,4) - 7*Power(t2,5)) + 
            t1*t2*(Power(s,2)*t2*(1 + 4*t2 + 13*Power(t2,2)) + 
               s*(2 + 17*t2 + 20*Power(t2,2) - 47*Power(t2,3) - 
                  38*Power(t2,4)) + 
               t2*(6 - 127*t2 - 134*Power(t2,2) + 8*Power(t2,3) + 
                  13*Power(t2,4))) + 
            Power(t2,2)*(-8 - 3*t2 + 9*Power(t2,2) + 40*Power(t2,3) + 
               15*Power(t2,4) - 7*Power(t2,5) + 
               Power(s,2)*(-2 + 4*t2 - 8*Power(t2,3)) + 
               s*(8 - 16*t2 - 21*Power(t2,2) + 6*Power(t2,3) + 
                  23*Power(t2,4))) - 
            Power(s1,2)*(-3 - 4*t2 + (1 - 5*s)*Power(t2,2) - 
               s*Power(t2,3) - 12*Power(t2,4) + 
               Power(t1,2)*(1 + 3*t2 + 8*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(-1 + 3*t2 + 19*Power(t2,2) + 8*Power(t2,3) - 
                  5*Power(t2,4) + s*(4 - 2*t2 + 4*Power(t2,3)))) + 
            s1*(4*Power(t1,3)*(1 + 4*t2 + 4*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*t2*
                (9 + 23*t2 + 18*Power(t2,2) + 10*Power(t2,3)) + 
               t1*t2*(-18 - 11*t2 + 41*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t2,4)) + 
               t2*(2 - 4*t2 + 15*Power(t2,2) - 2*Power(t2,3) - 
                  22*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s,2)*t2*
                (2*t1*(1 + Power(t2,2)) + t2*(-1 - 4*t2 + Power(t2,2))) \
+ s*(t2 - 4*Power(t2,2) - 4*Power(t2,3) + Power(t2,4) - 2*Power(t2,5) + 
                  Power(t1,2)*
                   (1 + 13*t2 + 19*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(t2 + 4*Power(t2,2) - 4*Power(t2,3) - 
                     15*Power(t2,4))))) + 
         Power(s2,3)*(-15*Power(t1,3) + 9*Power(t1,4) - t2 + 17*t1*t2 - 
            9*Power(t1,2)*t2 - 108*Power(t1,3)*t2 + 24*Power(t1,4)*t2 + 
            3*Power(t2,2) + 19*t1*Power(t2,2) + 
            170*Power(t1,2)*Power(t2,2) - 167*Power(t1,3)*Power(t2,2) - 
            43*Power(t2,3) + 27*t1*Power(t2,3) + 
            281*Power(t1,2)*Power(t2,3) - 15*Power(t1,3)*Power(t2,3) - 
            3*Power(t1,4)*Power(t2,3) - 8*Power(t2,4) - 
            164*t1*Power(t2,4) + 76*Power(t1,2)*Power(t2,4) + 
            20*Power(t1,3)*Power(t2,4) + 24*Power(t2,5) - 
            105*t1*Power(t2,5) - 38*Power(t1,2)*Power(t2,5) + 
            Power(t1,3)*Power(t2,5) + 23*Power(t2,6) + 
            17*t1*Power(t2,6) - 4*Power(t1,2)*Power(t2,6) + 
            4*Power(t2,7) + 5*t1*Power(t2,7) - 2*Power(t2,8) + 
            Power(s,3)*t2*(Power(t2,2)*(2 + t2 + Power(t2,2)) + 
               Power(t1,2)*(1 + t2 + 2*Power(t2,2)) - 
               t1*t2*(3 + 2*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(Power(t1,2)*
                (3 - 5*Power(t2,2) - 6*Power(t2,3)) - 
               2*(1 - t2 - 2*Power(t2,2) + Power(t2,3) + Power(t2,4)) + 
               t1*(1 - 3*t2 + 7*Power(t2,3) + 3*Power(t2,4))) + 
            Power(s,2)*(Power(t2,3)*
                (4 + 13*t2 - 4*Power(t2,2) - 17*Power(t2,3)) + 
               Power(t1,3)*(1 + 10*t2 + 12*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(t1,2)*t2*
                (3 - 5*t2 + 35*Power(t2,2) + 27*Power(t2,3)) + 
               t1*t2*(5 - 7*t2 - 25*Power(t2,2) + 24*Power(t2,3) + 
                  39*Power(t2,4))) + 
            s*(Power(t1,4)*(3 + 9*t2 + 7*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*(1 + 17*t2 + 45*Power(t2,2) + 
                  55*Power(t2,3) + 10*Power(t2,4)) + 
               t1*t2*(-11 + 67*t2 + 99*Power(t2,2) + 43*Power(t2,3) - 
                  108*Power(t2,4) - 42*Power(t2,5)) + 
               Power(t1,2)*(-1 - 24*t2 - 84*Power(t2,2) + 
                  37*Power(t2,3) + 135*Power(t2,4) + 33*Power(t2,5)) + 
               t2*(-2 - 26*t2 + 18*Power(t2,2) - 29*Power(t2,3) - 
                  42*Power(t2,4) + 19*Power(t2,5) + 18*Power(t2,6))) + 
            Power(s1,2)*(-9 - (-10 + s)*t2 - (13 + 9*s)*Power(t2,2) + 
               (-1 + 16*s)*Power(t2,3) + 6*Power(t2,4) + 
               7*Power(t2,5) + 
               2*Power(t1,3)*
                (1 + 4*t2 + 8*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(2 + 32*t2 + 36*Power(t2,2) - 
                  2*Power(t2,3) - 12*Power(t2,4) + 
                  s*(-1 + t2 + 12*Power(t2,2) + 14*Power(t2,3))) - 
               t1*(7 + 4*t2 - 8*Power(t2,2) + 54*Power(t2,3) + 
                  21*Power(t2,4) - 6*Power(t2,5) + 
                  s*(-12 + 22*t2 - 3*Power(t2,2) + 18*Power(t2,3) + 
                     7*Power(t2,4)))) - 
            s1*(3 + (11 + 12*s)*t2 - 
               (4 + 19*s + Power(s,2))*Power(t2,2) + 
               (-33 + 36*s + 8*Power(s,2))*Power(t2,3) + 
               (1 - 3*s + 8*Power(s,2))*Power(t2,4) + 
               (8 - 3*s - 3*Power(s,2))*Power(t2,5) + 
               (16 + 3*s)*Power(t2,6) - 2*Power(t2,7) + 
               Power(t1,4)*(6 + 14*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(s*
                   (7 + 23*t2 + 31*Power(t2,2) + 11*Power(t2,3)) - 
                  2*(1 + 7*t2 + 8*Power(t2,2) + 11*Power(t2,3) + 
                     3*Power(t2,4))) - 
               t1*(-2 + 10*t2 - 38*Power(t2,2) - 36*Power(t2,3) + 
                  81*Power(t2,4) + 10*Power(t2,5) - Power(t2,6) + 
                  3*Power(s,2)*t2*
                   (1 + 6*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
                  s*(5 + 39*t2 - 20*Power(t2,2) + 39*Power(t2,3) + 
                     25*Power(t2,4) - 24*Power(t2,5))) + 
               Power(t1,2)*(Power(s,2)*
                   (5 + t2 + 8*Power(t2,2) + 10*Power(t2,3)) + 
                  s*(-2 + 29*t2 + 7*Power(t2,2) - 24*Power(t2,3) - 
                     36*Power(t2,4)) + 
                  2*(-5 - 12*t2 + 26*Power(t2,2) + 6*Power(t2,3) + 
                     4*Power(t2,4) + 3*Power(t2,5))))) + 
         Power(s2,2)*(1 + 6*t2 + 12*s*t2 - 3*Power(t2,2) + 
            27*s*Power(t2,2) + 13*Power(s,2)*Power(t2,2) - 
            4*Power(t2,3) + 45*s*Power(t2,3) + 
            15*Power(s,2)*Power(t2,3) - 17*Power(t2,4) + 
            30*s*Power(t2,4) + 6*Power(s,2)*Power(t2,4) + 
            20*Power(t2,5) - 94*s*Power(t2,5) + 
            16*Power(s,2)*Power(t2,5) + 3*Power(s,3)*Power(t2,5) - 
            Power(t2,6) - 42*s*Power(t2,6) - 12*Power(s,2)*Power(t2,6) + 
            3*Power(s,3)*Power(t2,6) - 2*Power(t2,7) + 
            17*s*Power(t2,7) - 14*Power(s,2)*Power(t2,7) + 
            5*s*Power(t2,8) - 
            Power(t1,5)*(1 + t2)*(9 - 3*t2 + s*(3 + 2*t2)) + 
            Power(t1,4)*(32 + 116*t2 + 58*Power(t2,2) - 
               17*Power(t2,3) - 3*Power(t2,4) - 
               Power(s,2)*(5 + 11*t2 + 12*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s*(4 + 15*t2 + 53*Power(t2,2) + 23*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,3)*t1*(1 - t2 - 6*Power(t2,2) + 5*Power(t2,4) + 
               Power(t2,5) + 
               Power(t1,2)*(-3 - 2*t2 + 11*Power(t2,2) + 
                  6*Power(t2,3)) - 
               t1*(7 - 15*t2 + 4*Power(t2,2) + 9*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(t1,3)*(4 - 95*t2 - 257*Power(t2,2) - 
               180*Power(t2,3) + 31*Power(t2,4) + 13*Power(t2,5) - 
               2*Power(s,3)*(1 + t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(3 - 29*t2 + 19*Power(t2,2) + 
                  72*Power(t2,3) + 17*Power(t2,4)) + 
               s*(11 + 104*t2 + 35*Power(t2,2) - 147*Power(t2,3) - 
                  88*Power(t2,4) - 7*Power(t2,5))) + 
            Power(t1,2)*(-9 - 30*t2 - 126*Power(t2,2) + 
               233*Power(t2,3) + 228*Power(t2,4) - 3*Power(t2,5) - 
               17*Power(t2,6) + 
               Power(s,3)*t2*
                (7 + 4*t2 + 12*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s,2)*t2*
                (33 + 84*t2 + 9*Power(t2,2) - 118*Power(t2,3) - 
                  42*Power(t2,4)) + 
               s*(3 - 77*t2 - 117*Power(t2,2) - 190*Power(t2,3) + 
                  86*Power(t2,4) + 135*Power(t2,5) + 16*Power(t2,6))) - 
            t1*(-1 + 7*t2 - 129*Power(t2,2) - 22*Power(t2,3) + 
               63*Power(t2,4) + 78*Power(t2,5) + 11*Power(t2,6) - 
               7*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*
                (5 + 2*t2 + 9*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s,2)*t2*
                (16 + 54*t2 + 53*Power(t2,2) + 30*Power(t2,3) - 
                  70*Power(t2,4) - 41*Power(t2,5)) + 
               s*(1 - 21*t2 + 118*Power(t2,2) - 24*Power(t2,3) - 
                  248*Power(t2,4) - 58*Power(t2,5) + 85*Power(t2,6) + 
                  15*Power(t2,7))) - 
            Power(s1,2)*(2*Power(t1,4)*
                (1 + 6*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               (-1 + Power(t2,2))*
                (6 - 7*t2 - 5*Power(t2,2) + (5 + 2*s)*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,3)*(17 + 48*t2 + 26*Power(t2,2) - 
                  18*Power(t2,3) - 9*Power(t2,4) + 
                  s*(-8 + 3*t2 + 31*Power(t2,2) + 16*Power(t2,3))) + 
               Power(t1,2)*(4 + 34*t2 - 65*Power(t2,2) - 
                  99*Power(t2,3) + 9*Power(t2,4) + 9*Power(t2,5) - 
                  s*(14 - 28*t2 + 29*Power(t2,2) + 22*Power(t2,3) + 
                     23*Power(t2,4))) + 
               t1*(-2 - 32*t2 - 27*Power(t2,2) + 32*Power(t2,3) + 
                  32*Power(t2,4) - 3*Power(t2,6) + 
                  s*(14 - 37*t2 + 38*Power(t2,2) - 18*Power(t2,3) + 
                     17*Power(t2,4) + 4*Power(t2,5)))) + 
            s1*(9 + (-5 + 17*s)*t2 + (11 - 10*s)*Power(t2,2) + 
               (-27 - 32*s + Power(s,2))*Power(t2,3) + 
               (-3 - 20*s + 2*Power(s,2))*Power(t2,4) + 
               (11 + 40*s - 8*Power(s,2))*Power(t2,5) + 
               (7 + 6*s + 3*Power(s,2))*Power(t2,6) - 
               (3 + s)*Power(t2,7) + 
               2*Power(t1,5)*(2 + 3*t2 + Power(t2,2)) + 
               Power(t1,4)*(-2 - 2*t2 - 12*Power(t2,2) - 
                  13*Power(t2,3) - Power(t2,4) + 
                  s*(9 + 25*t2 + 24*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,3)*(-13 + 37*t2 + 7*Power(t2,2) + 
                  9*Power(t2,3) + 14*Power(t2,4) + 2*Power(t2,5) + 
                  2*Power(s,2)*
                   (-1 + 4*t2 + 13*Power(t2,2) + 7*Power(t2,3)) + 
                  s*(21 + 36*t2 + 34*Power(t2,2) - 79*Power(t2,3) - 
                     26*Power(t2,4))) - 
               Power(t1,2)*(6 - 19*t2 - 110*Power(t2,2) + 
                  118*Power(t2,3) + 33*Power(t2,4) + 7*Power(t2,5) + 
                  Power(t2,6) + 
                  Power(s,2)*
                   (-13 + 23*t2 + 23*Power(t2,2) + 22*Power(t2,3) + 
                     27*Power(t2,4)) + 
                  s*(35 - 59*t2 + 137*Power(t2,2) + 129*Power(t2,3) - 
                     56*Power(t2,4) - 42*Power(t2,5))) + 
               t1*(14 + 4*t2 - 82*Power(t2,2) + 50*Power(t2,3) - 
                  25*Power(t2,4) + 34*Power(t2,5) + 5*Power(t2,6) + 
                  Power(s,2)*t2*
                   (-16 + 21*t2 + 16*Power(t2,2) + 7*Power(t2,3) + 
                     10*Power(t2,4)) + 
                  s*(-12 - 62*t2 + 145*Power(t2,2) + 16*Power(t2,3) + 
                     17*Power(t2,4) - 7*Power(t2,5) - 19*Power(t2,6))))) \
+ s2*(Power(t1,6)*(3 + s - t2 + s*t2) - 
            (1 + t2)*(3 + (-3 + 16*s)*t2 + 
               (3 - 6*s + 17*Power(s,2))*Power(t2,2) + 
               2*(-11 - s + Power(s,2) + 2*Power(s,3))*Power(t2,3) - 
               (-32 + 50*s - 4*Power(s,2) + Power(s,3))*Power(t2,4) + 
               (-12 + 45*s - 33*Power(s,2) + Power(s,3))*Power(t2,5) + 
               (-2 + 2*s + 6*Power(s,2) - 3*Power(s,3))*Power(t2,6) + 
               (1 - 5*s + 4*Power(s,2))*Power(t2,7)) + 
            Power(t1,5)*(-29 - 42*t2 + 2*Power(t2,2) + 3*Power(t2,3) + 
               Power(s,2)*(3 + 8*t2 + 3*Power(t2,2)) - 
               s*(-1 + 17*t2 + 16*Power(t2,2) + 2*Power(t2,3))) + 
            Power(t1,4)*(18 + 97*t2 + 145*Power(t2,2) - 14*Power(t2,4) + 
               Power(s,3)*(-1 + 4*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(21 + 14*t2 - 50*Power(t2,2) - 
                  29*Power(t2,3) - 2*Power(t2,4)) + 
               s*(-41 - 49*t2 + 41*Power(t2,2) + 75*Power(t2,3) + 
                  14*Power(t2,4))) - 
            Power(s1,3)*Power(t1,2)*
             (Power(t1,2)*(-2 + t2 + 7*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(1 + t2 - 5*Power(t2,3) - 5*Power(t2,4)) + 
               2*(-1 - Power(t2,3) + Power(t2,4) + Power(t2,5))) + 
            Power(t1,3)*(14 + 133*t2 - 132*Power(t2,2) - 
               200*Power(t2,3) - 18*Power(t2,4) + 19*Power(t2,5) + 
               Power(s,3)*(2 + 7*t2 - 9*Power(t2,2) - 27*Power(t2,3) - 
                  9*Power(t2,4)) + 
               s*(26 + 7*t2 + 209*Power(t2,2) + 73*Power(t2,3) - 
                  135*Power(t2,4) - 36*Power(t2,5)) + 
               Power(s,2)*(-27 - 49*t2 - 87*Power(t2,2) + 
                  58*Power(t2,3) + 79*Power(t2,4) + 10*Power(t2,5))) - 
            t1*(7 + 13*Power(t2,2) - 49*Power(t2,3) + 19*Power(t2,4) + 
               12*Power(t2,5) + Power(t2,6) - 3*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*
                (-13 - 11*t2 + 4*Power(t2,2) + 13*Power(t2,3) + 
                  11*Power(t2,4)) - 
               Power(s,2)*t2*
                (15 + 34*t2 + 25*Power(t2,2) - 118*Power(t2,3) - 
                  68*Power(t2,4) + 50*Power(t2,5) + 14*Power(t2,6)) + 
               s*t2*(18 + 97*t2 + 182*Power(t2,2) - 166*Power(t2,3) - 
                  184*Power(t2,4) + 29*Power(t2,5) + 24*Power(t2,6))) + 
            Power(t1,2)*(4 - 123*t2 - 20*Power(t2,2) + 45*Power(t2,3) + 
               90*Power(t2,4) + 14*Power(t2,5) - 10*Power(t2,6) + 
               Power(s,3)*t2*
                (-11 - 14*t2 + 9*Power(t2,2) + 29*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s,2)*(1 + 21*t2 + 2*Power(t2,2) + 
                  158*Power(t2,3) + 25*Power(t2,4) - 93*Power(t2,5) - 
                  18*Power(t2,6)) + 
               s*(5 + 174*t2 + 123*Power(t2,2) - 339*Power(t2,3) - 
                  239*Power(t2,4) + 101*Power(t2,5) + 43*Power(t2,6))) + 
            Power(s1,2)*t1*(Power(t1,4)*(2 + 7*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(20 + 29*t2 + 3*Power(t2,2) - 
                  14*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,2)*(35 + 17*t2 - 120*Power(t2,2) - 
                  24*Power(t2,3) + 17*Power(t2,4) + 3*Power(t2,5)) + 
               t1*(5 - 74*t2 + 12*Power(t2,2) + 41*Power(t2,3) + 
                  32*Power(t2,4) - 15*Power(t2,5) - Power(t2,6)) + 
               2*(-1 - t2 + 6*Power(t2,2) - 7*Power(t2,4) + 
                  Power(t2,5) + 2*Power(t2,6)) + 
               s*(6 - 7*t2 - 17*Power(t2,2) + 14*Power(t2,3) + 
                  12*Power(t2,4) - 7*Power(t2,5) - Power(t2,6) + 
                  Power(t1,3)*
                   (-5 + 6*t2 + 23*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(t1,2)*
                   (12 + 4*t2 - 15*Power(t2,2) + 28*Power(t2,3) + 
                     19*Power(t2,4)) + 
                  t1*(-22 + 77*t2 - 59*Power(t2,2) + 2*Power(t2,3) + 
                     6*Power(t2,4) + 14*Power(t2,5)))) + 
            s1*(-(Power(t1,6)*(1 + t2)) + 
               (-1 + t2)*Power(1 + t2,2)*
                (6 + 2*(-7 + 3*s)*t2 + (8 - 13*s)*Power(t2,2) + 
                  (2 + 6*s - 5*Power(s,2))*Power(t2,3) + 
                  (-2 + s + Power(s,2))*Power(t2,4)) - 
               Power(t1,5)*(2 + 2*t2 - 8*Power(t2,2) - 2*Power(t2,3) + 
                  s*(5 + 15*t2 + 6*Power(t2,2))) - 
               Power(t1,4)*(13 + 9*t2 + 8*Power(t2,3) + 4*Power(t2,4) + 
                  Power(s,2)*
                   (-4 + 9*t2 + 25*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(26 + 62*t2 - 37*Power(t2,2) - 43*Power(t2,3) - 
                     4*Power(t2,4))) + 
               Power(t1,3)*(4 - 104*t2 + 99*Power(t2,2) + 
                  10*Power(t2,3) + 9*Power(t2,4) + 6*Power(t2,5) + 
                  Power(s,2)*
                   (21 - 12*t2 - 10*Power(t2,2) + 50*Power(t2,3) + 
                     23*Power(t2,4)) + 
                  s*(-53 + 60*t2 + 174*Power(t2,2) + 35*Power(t2,3) - 
                     75*Power(t2,4) - 13*Power(t2,5))) + 
               Power(t1,2)*(1 + 101*t2 - 136*Power(t2,2) + 
                  35*Power(t2,3) + 13*Power(t2,4) - 8*Power(t2,5) - 
                  6*Power(t2,6) - 
                  Power(s,2)*
                   (12 + 18*t2 - 38*Power(t2,2) - 4*Power(t2,3) + 
                     25*Power(t2,4) + 27*Power(t2,5)) + 
                  s*(49 - 120*t2 + 81*Power(t2,2) - 59*Power(t2,3) - 
                     95*Power(t2,4) + 52*Power(t2,5) + 14*Power(t2,6))) + 
               t1*(Power(-1 + t2,2)*
                   (-7 - 51*t2 - 58*Power(t2,2) - 27*Power(t2,3) + 
                     13*Power(t2,4) + 2*Power(t2,5)) + 
                  Power(s,2)*t2*
                   (17 - 13*t2 - 31*Power(t2,2) + 18*Power(t2,3) + 
                     4*Power(t2,4) + 9*Power(t2,5)) + 
                  s*(13 + 5*t2 + 7*Power(t2,2) + 11*Power(t2,3) - 
                     53*Power(t2,4) + 37*Power(t2,5) - 15*Power(t2,6) - 
                     5*Power(t2,7))))))*B1(1 - s2 + t1 - t2,t2,t1))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*(1 - s2 + t1 - t2)*
       Power(t1 - s2*t2,3)) - (8*
       (2*Power(s2,9)*t1*(t1 - t2)*Power(t2,2)*
          ((-2 + s1 - t2)*t2 + t1*(-2 + 3*s1 + t2)) + 
         Power(s2,8)*t2*(2*Power(t2,3)*(1 + t2) - 
            Power(t1,4)*(-8 + 2*(-6 + s)*t2 + 3*Power(t2,2) + 
               s1*(12 + 25*t2)) + 
            Power(t1,3)*t2*(14 - 2*Power(s1,2) - 32*t2 + 
               17*Power(t2,2) + s*(4 - 8*s1 + 2*t2) + s1*(-10 + 57*t2)) \
+ Power(t1,2)*t2*(-6 - 13*t2 + 2*(7 + s)*Power(t2,2) - 26*Power(t2,3) - 
               2*Power(s1,2)*(3 + 2*t2) + 
               s1*(15 + 4*(7 + 2*s)*t2 - 26*Power(t2,2))) + 
            t1*Power(t2,2)*(-8 - (17 + 4*s)*t2 - 
               2*(-6 + s)*Power(t2,2) + 12*Power(t2,3) + 
               s1*(3 + 6*t2 - 12*Power(t2,2)))) + 
         Power(s2,7)*(12*Power(t2,5)*(1 + t2) - 
            Power(t1,5)*(4 + (30 - 4*s)*t2 + (21 - 11*s)*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(t1,2)*Power(t2,2)*
             (21 + 6*(3 + s)*t2 - 47*Power(t2,2) + 
               (97 + 8*s)*Power(t2,3) - 68*Power(t2,4)) + 
            t1*Power(t2,3)*(-3 - (59 + s)*t2 - 
               (109 + 24*s)*Power(t2,2) - 12*(1 + s)*Power(t2,3) + 
               30*Power(t2,4)) + 
            Power(t1,3)*t2*(12 + (19 + 3*s)*t2 + 
               (37 + 44*s)*Power(t2,2) + (-128 + 31*s)*Power(t2,3) + 
               48*Power(t2,4)) + 
            Power(s1,3)*Power(t1,2)*
             (5*t1*t2*(1 + t2) - Power(t1,2)*(2 + t2) + 
               Power(t2,2)*(3 + 2*t2)) + 
            Power(s1,2)*Power(t1,2)*t2*
             (2*Power(t1,3) + Power(t1,2)*(2 + 2*s - 5*t2) + 
               t1*(12 + 29*t2 + 11*Power(t2,2)) - 
               t2*(18 + 61*t2 + 2*s*t2 + 32*Power(t2,2))) - 
            Power(t1,4)*t2*(28 - 89*t2 - 88*Power(t2,2) + 
               8*Power(t2,3) + s*(8 + 24*t2 + 38*Power(t2,2))) + 
            s1*t1*(Power(t1,4)*(6 + 50*t2 + 25*Power(t2,2)) + 
               t1*Power(t2,2)*
                (-2 + (86 + 8*s)*t2 + 3*(61 + 20*s)*Power(t2,2) - 
                  70*Power(t2,3)) + 
               Power(t2,3)*(2 + 23*t2 + 40*Power(t2,2) - 
                  30*Power(t2,3)) + 
               Power(t1,2)*t2*
                (-30 - 3*(27 + 8*s)*t2 - (83 + 88*s)*Power(t2,2) + 
                  204*Power(t2,3)) + 
               Power(t1,3)*t2*
                (32 - 118*t2 - 153*Power(t2,2) + 4*s*(4 + 7*t2)))) + 
         Power(s2,5)*(2*Power(t2,4)*
             (1 - 7*t2 - 8*Power(t2,2) + 20*Power(t2,3) + 
               20*Power(t2,4)) + 
            t1*Power(t2,3)*(3 + (71 + 11*s)*t2 + 
               (-45 + 8*s)*Power(t2,2) - 2*(175 + 9*s)*Power(t2,3) - 
               2*(217 + 40*s)*Power(t2,4) - 20*(9 + 2*s)*Power(t2,5) + 
               30*Power(t2,6) + 
               s1*(6 - 51*t2 - 20*Power(t2,2) + 102*Power(t2,3) + 
                  160*Power(t2,4) - 30*Power(t2,5))) - 
            Power(t1,2)*Power(t2,2)*
             (25 + (9 + 26*s)*t2 + (-631 + 2*Power(s,2))*Power(t2,2) + 
               (-1140 - 260*s - 11*Power(s,2) + 2*Power(s,3))*
                Power(t2,3) - (731 + 5*s + 40*Power(s,2))*Power(t2,4) + 
               (-437 + 8*s)*Power(t2,5) + 68*Power(t2,6) + 
               Power(s1,3)*t2*(20 - 27*t2 - 12*Power(t2,2)) + 
               Power(s1,2)*(18 + (-233 + 6*s)*t2 + 
                  (123 - 17*s)*Power(t2,2) + (427 + 4*s)*Power(t2,3) + 
                  160*Power(t2,4)) + 
               s1*(-7 + 4*(40 + 7*s)*t2 + 
                  (145 + 42*s + 7*Power(s,2))*Power(t2,2) + 
                  2*(-53 - 62*s + 9*Power(s,2))*Power(t2,3) - 
                  (719 + 280*s)*Power(t2,4) + 64*Power(t2,5))) + 
            Power(t1,3)*t2*(-3 + (-257 + 10*s)*t2 + 
               (-253 - 26*s + 6*Power(s,2))*Power(t2,2) + 
               (49 + 81*s - 41*Power(s,2) + 4*Power(s,3))*Power(t2,3) + 
               (-1281 + 588*s - 143*Power(s,2))*Power(t2,4) + 
               (-456 + 307*s)*Power(t2,5) + 23*Power(t2,6) + 
               Power(s1,3)*(22 - 143*t2 + 14*Power(t2,2) + 
                  93*Power(t2,3)) + 
               Power(s1,2)*(-57 + (194 + 33*s)*t2 + 
                  (832 - 50*s)*Power(t2,2) + 
                  (738 - 87*s)*Power(t2,3) + 231*Power(t2,4)) + 
               s1*(79 + (37 + 66*s)*t2 + 
                  (-1263 - 24*s + 31*Power(s,2))*Power(t2,2) + 
                  (-2398 - 926*s + 69*Power(s,2))*Power(t2,3) - 
                  2*(409 + 294*s)*Power(t2,4) + 285*Power(t2,5))) - 
            Power(t1,4)*(-5 - 2*(-57 + s)*t2 + 
               (283 + 80*s + 6*Power(s,2))*Power(t2,2) + 
               (432 + 794*s - 57*Power(s,2))*Power(t2,3) + 
               (-1593 + 1174*s - 187*Power(s,2))*Power(t2,4) + 
               (-455 + 506*s)*Power(t2,5) - 57*Power(t2,6) + 
               Power(s1,3)*(-8 - 22*t2 + 175*Power(t2,2) + 
                  94*Power(t2,3)) + 
               Power(s1,2)*(27 + (147 + 44*s)*t2 - 
                  6*(-61 + 19*s)*Power(t2,2) + 
                  (422 - 186*s)*Power(t2,3) + 169*Power(t2,4)) + 
               s1*(2 + (-383 + 40*s)*t2 + 
                  (-1317 - 244*s + 51*Power(s,2))*Power(t2,2) + 
                  (-2223 - 1348*s + 99*Power(s,2))*Power(t2,3) + 
                  (604 - 352*s)*Power(t2,4) + 296*Power(t2,5))) + 
            Power(t1,7)*(-27 - 50*t2 + 2*Power(s,2)*t2 - 
               21*Power(t2,2) + 2*Power(s1,2)*(2 + t2) + 
               s1*(25 - 10*t2 - 11*Power(t2,2)) + 
               s*(11 + 32*t2 + 11*Power(t2,2) - 4*s1*(1 + t2))) + 
            Power(t1,6)*(13 + 362*t2 + 2*Power(s,3)*t2 + 
               484*Power(t2,2) + 142*Power(t2,3) + 15*Power(t2,4) - 
               Power(s1,3)*(7 + 11*t2) - 
               3*Power(s1,2)*(1 + 20*t2 + 3*Power(t2,2)) + 
               s1*t2*(-443 - 313*t2 + 54*Power(t2,2)) + 
               Power(s,2)*(-5*s1*(2 + 3*t2) + t2*(8 + 17*t2)) + 
               s*(-20 - 146*t2 - 335*Power(t2,2) - 98*Power(t2,3) + 
                  Power(s1,2)*(17 + 24*t2) + 
                  s1*(38 + 52*t2 - 8*Power(t2,2)))) + 
            Power(t1,5)*(5 + 337*t2 - 386*Power(t2,2) - 
               4*Power(s,3)*Power(t2,2) - 1347*Power(t2,3) - 
               353*Power(t2,4) - 57*Power(t2,5) + 
               15*Power(s1,3)*(1 + 3*t2 + 4*Power(t2,2)) + 
               Power(s1,2)*(-10 + 20*t2 + 131*Power(t2,2) + 
                  81*Power(t2,3)) + 
               s1*(-35 - 537*t2 - 35*Power(t2,2) + 1154*Power(t2,3) + 
                  38*Power(t2,4)) + 
               Power(s,2)*t2*
                (2 - 35*t2 - 103*Power(t2,2) + s1*(37 + 63*t2)) + 
               s*(3 + 118*t2 + 606*Power(t2,2) + 964*Power(t2,3) + 
                  334*Power(t2,4) + 
                  Power(s1,2)*(17 - 98*t2 - 119*Power(t2,2)) - 
                  2*s1*(-1 + 108*t2 + 297*Power(t2,2) + 16*Power(t2,3))))\
) + Power(s2,6)*(2*Power(t2,4)*
             (-2 - 2*t2 + 15*Power(t2,2) + 15*Power(t2,3)) + 
            Power(t1,6)*(16 + 51*t2 + 28*Power(t2,2) + 5*Power(t2,3) - 
               2*Power(s1,2)*(1 + 2*t2) + 
               5*s1*(-5 - 10*t2 + Power(t2,2)) - 
               2*s*(1 + (11 - 2*s1)*t2 + 8*Power(t2,2))) + 
            Power(t1,2)*Power(t2,2)*
             (2 + (200 + 6*s)*t2 + 5*(71 + 16*s)*Power(t2,2) + 
               (131 + s + 8*Power(s,2))*Power(t2,3) + 
               (281 + 8*s)*Power(t2,4) - 92*Power(t2,5) + 
               Power(s1,3)*(-6 + 15*t2 + 8*Power(t2,2)) + 
               Power(s1,2)*(51 + (-81 + s)*t2 - 
                  3*(77 + 2*s)*Power(t2,2) - 100*Power(t2,3)) + 
               s1*(-35 + 2*(-21 + s)*t2 + 
                  (173 + 56*s - 4*Power(s,2))*Power(t2,2) + 
                  (499 + 180*s)*Power(t2,3) - 96*Power(t2,4))) + 
            t1*Power(t2,3)*(14 - 24*t2 - (197 + 7*s)*Power(t2,2) - 
               4*(74 + 15*s)*Power(t2,3) - 10*(10 + 3*s)*Power(t2,4) + 
               40*Power(t2,5) + 
               s1*(-11 + 2*t2 + 68*Power(t2,2) + 110*Power(t2,3) - 
                  40*Power(t2,4))) + 
            Power(t1,3)*t2*(-18 + (63 - 4*s)*t2 + 
               (148 + 11*s)*Power(t2,2) + 
               (-259 + 214*s - 24*Power(s,2))*Power(t2,3) + 
               (-311 + 144*s)*Power(t2,4) + 57*Power(t2,5) + 
               3*Power(s1,3)*(-7 + 6*t2 + 11*Power(t2,2)) + 
               Power(s1,2)*(21 - 6*(-31 + s)*t2 + 
                  (242 - 15*s)*Power(t2,2) + 99*Power(t2,3)) + 
               s1*(-5 - 10*(35 + 2*s)*t2 + 
                  2*(-377 - 137*s + 6*Power(s,2))*Power(t2,2) - 
                  14*(25 + 24*s)*Power(t2,3) + 349*Power(t2,4))) - 
            Power(t1,4)*(6 + 3*(5 + 2*s)*t2 + 
               4*(64 + 31*s)*Power(t2,2) - 
               3*(170 - 87*s + 8*Power(s,2))*Power(t2,3) + 
               (-279 + 208*s)*Power(t2,4) - 13*Power(t2,5) + 
               Power(s1,3)*(-3 + 32*t2 + 18*Power(t2,2)) + 
               s1*(-15 - 2*(45 + 13*s)*t2 + 
                  (-447 - 284*s + 12*Power(s,2))*Power(t2,2) - 
                  8*(-56 + 23*s)*Power(t2,3) + 335*Power(t2,4)) + 
               Power(s1,2)*t2*
                (27 + 47*t2 + 65*Power(t2,2) - s*(13 + 32*t2))) + 
            Power(t1,5)*(14 - 66*t2 + 4*Power(s,2)*(s1 - 2*t2)*t2 - 
               347*Power(t2,2) - 141*Power(t2,3) - 23*Power(t2,4) + 
               Power(s1,3)*(5 + 7*t2) + 2*Power(s1,2)*t2*(4 + 17*t2) + 
               s1*(-18 + 61*t2 + 455*Power(t2,2) + 81*Power(t2,3)) + 
               s*(-(Power(s1,2)*(8 + 11*t2)) - 
                  2*s1*(4 + 33*t2 + 16*Power(t2,2)) + 
                  2*(2 + 21*t2 + 64*Power(t2,2) + 51*Power(t2,3))))) + 
         Power(s2,4)*(Power(t1,8)*
             (24 - 2*Power(s,2) + 5*s1 - 2*Power(s1,2) + 
               2*s*(-8 + 2*s1 - 11*t2) + 27*t2 + 22*s1*t2) + 
            6*Power(t2,5)*Power(1 + t2,2)*(1 - 5*t2 + 5*Power(t2,2)) + 
            t1*Power(t2,3)*(-6 + (49 - 6*s + 24*s1)*t2 + 
               (169 + 29*s - 83*s1)*Power(t2,2) + 
               (-29 + 24*s - 52*s1)*Power(t2,3) + 
               (-342 - 22*s + 83*s1)*Power(t2,4) + 
               (-361 - 60*s + 130*s1)*Power(t2,5) - 
               6*(26 + 5*s + 2*s1)*Power(t2,6) + 12*Power(t2,7)) - 
            Power(t1,3)*t2*(-3 + 2*(-1 + 5*s)*t2 + 
               (911 + 123*s + 79*Power(s,2))*Power(t2,2) + 
               2*(691 + 385*s + 6*Power(s,2) - 7*Power(s,3))*
                Power(t2,3) + 
               (1254 - 8*s + 252*Power(s,2) - 33*Power(s,3))*
                Power(t2,4) + 
               (2275 - 862*s + 321*Power(s,2))*Power(t2,5) + 
               (402 - 337*s)*Power(t2,6) + 6*Power(t2,7) + 
               Power(s1,3)*t2*
                (-136 + 337*t2 + 8*Power(t2,2) - 131*Power(t2,3)) - 
               Power(s1,2)*(6 - (605 + 6*s)*t2 + 
                  (296 + 135*s)*Power(t2,2) - 
                  2*(-788 + 65*s)*Power(t2,3) + 
                  (1048 - 169*s)*Power(t2,4) + 239*Power(t2,5)) + 
               s1*(26 + (-666 + 56*s)*t2 + 
                  (-371 - 436*s + 29*Power(s,2))*Power(t2,2) - 
                  6*(-297 + 27*s + 17*Power(s,2))*Power(t2,3) + 
                  (3424 + 1344*s - 125*Power(s,2))*Power(t2,4) + 
                  2*(509 + 249*s)*Power(t2,5) - 72*Power(t2,6))) + 
            Power(t1,4)*(4 + (157 + 9*s)*t2 + 
               (-230 + 106*s + 87*Power(s,2))*Power(t2,2) - 
               2*(537 - 136*s - 81*Power(s,2) + 4*Power(s,3))*
                Power(t2,3) + 
               (935 - 1814*s + 578*Power(s,2) - 34*Power(s,3))*
                Power(t2,4) + 
               (2671 - 2272*s + 501*Power(s,2))*Power(t2,5) + 
               (431 - 610*s)*Power(t2,6) + 58*Power(t2,7) + 
               Power(s1,3)*(-12 + 212*t2 + 164*Power(t2,2) - 
                  417*Power(t2,3) - 190*Power(t2,4)) - 
               Power(s1,2)*(-36 + (329 - 8*s)*t2 + 
                  2*(570 + 83*s)*Power(t2,2) + 
                  (1353 - 371*s)*Power(t2,3) + 
                  (1018 - 392*s)*Power(t2,4) + 145*Power(t2,5)) + 
               s1*(-22 + (-58 + 40*s)*t2 + 
                  3*(690 - 102*s + 17*Power(s,2))*Power(t2,2) + 
                  (4875 + 984*s - 255*Power(s,2))*Power(t2,3) + 
                  (4646 + 2466*s - 198*Power(s,2))*Power(t2,4) + 
                  2*(-32 + 77*s)*Power(t2,5) - 43*Power(t2,6))) + 
            Power(t1,2)*Power(t2,2)*
             (11 + (-228 + 13*s)*t2 + 
               (-132 - 32*s + 25*Power(s,2))*Power(t2,2) + 
               (968 + 149*s - 21*Power(s,2) - 5*Power(s,3))*
                Power(t2,3) + 
               (1702 + 412*s + 39*Power(s,2) - 10*Power(s,3))*
                Power(t2,4) + 
               (1174 + 3*s + 80*Power(s,2))*Power(t2,5) + 
               (385 - 22*s)*Power(t2,6) - 26*Power(t2,7) + 
               Power(s1,3)*Power(t2,2)*(-22 + 21*t2 + 8*Power(t2,2)) - 
               Power(s1,2)*t2*
                (84 + (-433 + 24*s)*t2 + (47 - 45*s)*Power(t2,2) + 
                  (415 - 4*s)*Power(t2,3) + 140*Power(t2,4)) + 
               s1*(6 + 3*(33 + 8*s)*t2 + 
                  2*(-133 - 77*s + 3*Power(s,2))*Power(t2,2) - 
                  (311 + 182*s + 11*Power(s,2))*Power(t2,3) + 
                  (-135 + 114*s - 32*Power(s,2))*Power(t2,4) + 
                  3*(191 + 80*s)*Power(t2,5) - 10*Power(t2,6))) + 
            Power(t1,7)*(-120 - 513*t2 - 317*Power(t2,2) - 
               63*Power(t2,3) - Power(s,3)*(4 + 5*t2) + 
               Power(s1,3)*(7 + 5*t2) + 
               Power(s1,2)*(26 + 16*t2 - 18*Power(t2,2)) + 
               s1*(141 + 379*t2 - 128*Power(t2,2) - 23*Power(t2,3)) + 
               Power(s,2)*(15*s1*(1 + t2) - 2*t2*(25 + 9*t2)) + 
               s*(56 + 368*t2 + 292*Power(t2,2) + 23*Power(t2,3) - 
                  3*Power(s1,2)*(6 + 5*t2) + 
                  s1*(-20 + 34*t2 + 36*Power(t2,2)))) + 
            Power(t1,6)*(-134 - 120*t2 + 1771*Power(t2,2) + 
               1311*Power(t2,3) + 274*Power(t2,4) + 15*Power(t2,5) + 
               Power(s,3)*t2*(13 + 12*t2) - 
               Power(s1,3)*(35 + 93*t2 + 50*Power(t2,2)) + 
               Power(s1,2)*(16 - 34*t2 - 259*Power(t2,2) + 
                  67*Power(t2,3)) + 
               s1*(199 + 582*t2 - 1423*Power(t2,2) - 289*Power(t2,3) + 
                  102*Power(t2,4)) + 
               Power(s,2)*(4 + 75*t2 + 297*Power(t2,2) + 
                  135*Power(t2,3) + s1*(11 - 114*t2 - 74*Power(t2,2))) \
+ s*(-40 - 541*t2 - 1450*Power(t2,2) - 1289*Power(t2,3) - 
                  168*Power(t2,4) + 
                  2*Power(s1,2)*(24 + 97*t2 + 56*Power(t2,2)) + 
                  s1*(8 + 594*t2 + 136*Power(t2,2) - 202*Power(t2,3)))) \
+ Power(t1,5)*(50 + 248*t2 + 1625*Power(t2,2) - 1633*Power(t2,3) - 
               2410*Power(t2,4) - 463*Power(t2,5) - 53*Power(t2,6) + 
               2*Power(s,3)*Power(t2,2)*(-5 + 2*t2) + 
               2*Power(s1,3)*
                (-21 + 106*t2 + 107*Power(t2,2) + 78*Power(t2,3)) + 
               Power(s1,2)*(58 + 113*t2 + 468*Power(t2,2) + 
                  540*Power(t2,3) - 9*Power(t2,4)) - 
               s1*(113 + 983*t2 + 3657*Power(t2,2) + 821*Power(t2,3) - 
                  930*Power(t2,4) + 92*Power(t2,5)) - 
               Power(s,2)*t2*
                (37 + 204*t2 + 610*Power(t2,2) + 377*Power(t2,3) + 
                  s1*(39 - 263*t2 - 164*Power(t2,2))) + 
               s*(-6 + 60*t2 + 810*Power(t2,2) + 2514*Power(t2,3) + 
                  2486*Power(t2,4) + 470*Power(t2,5) - 
                  Power(s1,2)*
                   (2 - 7*t2 + 462*Power(t2,2) + 324*Power(t2,3)) + 
                  2*s1*(-4 + 8*t2 - 769*Power(t2,2) - 705*Power(t2,3) + 
                     135*Power(t2,4))))) + 
         Power(s2,3)*(11*(-1 + s - s1)*Power(t1,9) + 
            2*Power(t2,6)*(3 - 5*t2 - 8*Power(t2,2) + 6*Power(t2,3) + 
               6*Power(t2,4)) + 
            t1*Power(t2,4)*(-36 + (73 - 12*s + 30*s1)*t2 + 
               (189 + 25*s - 57*s1)*Power(t2,2) + 
               (-8 + 24*s - 46*s1)*Power(t2,3) + 
               (-179 - 13*s + 35*s1)*Power(t2,4) + 
               (-161 - 24*s + 56*s1)*Power(t2,5) - 
               2*(34 + 6*s + s1)*Power(t2,6) + 2*Power(t2,7)) + 
            Power(t1,2)*Power(t2,3)*
             (15 - 486*t2 - 252*Power(t2,2) + 759*Power(t2,3) + 
               1322*Power(t2,4) + 862*Power(t2,5) + 182*Power(t2,6) - 
               4*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(6 - 13*t2 - 20*Power(t2,2)) + 
               2*Power(s1,3)*Power(t2,2)*(-4 + 3*t2 + Power(t2,2)) + 
               Power(s1,2)*t2*
                (-150 + 359*t2 + 41*Power(t2,2) - 204*Power(t2,3) - 
                  64*Power(t2,4)) + 
               Power(s,2)*t2*
                (-18 + t2 + 12*s1*t2 + (-79 + 9*s1)*Power(t2,2) + 
                  (57 - 28*s1)*Power(t2,3) + 80*Power(t2,4)) + 
               s*(-6 + (-13 + 108*s1)*t2 - 
                  2*(29 + 87*s1 + 15*Power(s1,2))*Power(t2,2) + 
                  (369 - 242*s1 + 43*Power(s1,2))*Power(t2,3) + 
                  (346 + 38*s1 + 6*Power(s1,2))*Power(t2,4) + 
                  (-11 + 108*s1)*Power(t2,5) - 16*Power(t2,6)) + 
               s1*(-12 + 189*t2 - 148*Power(t2,2) - 337*Power(t2,3) - 
                  252*Power(t2,4) + 238*Power(t2,5) + 10*Power(t2,6))) + 
            Power(t1,8)*(178 + 3*Power(s,3) - 3*Power(s1,3) + 300*t2 + 
               113*Power(t2,2) + Power(s1,2)*(-7 + 34*t2) + 
               Power(s,2)*(33 - 9*s1 + 34*t2) + 
               s1*(-147 + 94*t2 + 78*Power(t2,2)) + 
               s*(-135 + 9*Power(s1,2) - 290*t2 - 78*Power(t2,2) - 
                  2*s1*(13 + 34*t2))) + 
            Power(t1,5)*(-47 + 317*t2 + 1680*Power(t2,2) + 
               1868*Power(t2,3) - 3752*Power(t2,4) - 1990*Power(t2,5) - 
               302*Power(t2,6) - 17*Power(t2,7) + 
               2*Power(s,3)*Power(t2,2)*(45 + 44*t2 + 43*Power(t2,2)) + 
               2*Power(s1,3)*
                (-32 - 211*t2 + 314*Power(t2,2) + 242*Power(t2,3) + 
                  73*Power(t2,4)) + 
               Power(s1,2)*(216 + 865*t2 + 870*Power(t2,2) + 
                  1674*Power(t2,3) + 608*Power(t2,4) - 143*Power(t2,5)) \
- s1*(-4 + 1228*t2 + 4881*Power(t2,2) + 8520*Power(t2,3) + 
                  2171*Power(t2,4) + 158*Power(t2,5) + 99*Power(t2,6)) \
+ s*(6 + (-222 + 258*s1 + 308*Power(s1,2))*t2 + 
                  (-779 + 324*s1 - 280*Power(s1,2))*Power(t2,2) - 
                  12*(-136 + 335*s1 + 86*Power(s1,2))*Power(t2,3) + 
                  (4909 - 852*s1 - 260*Power(s1,2))*Power(t2,4) + 
                  562*(5 + s1)*Power(t2,5) + 291*Power(t2,6)) + 
               Power(s,2)*t2*
                (-3 - 10*t2 - 626*Power(t2,2) - 1816*Power(t2,3) - 
                  533*Power(t2,4) + 
                  2*s1*(9 + 21*t2 + 282*Power(t2,2) + 14*Power(t2,3)))) \
+ Power(t1,4)*t2*(57 + 790*t2 + 519*Power(t2,2) - 255*Power(t2,3) + 
               3287*Power(t2,4) + 2286*Power(t2,5) + 237*Power(t2,6) + 
               19*Power(t2,7) - 
               Power(s,3)*Power(t2,2)*(68 + 95*t2 + 132*Power(t2,2)) + 
               Power(s1,3)*(-224 + 742*t2 + 328*Power(t2,2) - 
                  499*Power(t2,3) - 161*Power(t2,4)) - 
               Power(s1,2)*(-600 + 747*t2 + 2502*Power(t2,2) + 
                  2147*Power(t2,3) + 960*Power(t2,4) + 16*Power(t2,5)) \
+ s1*(-684 - 844*t2 + 3784*Power(t2,2) + 6839*Power(t2,3) + 
                  4636*Power(t2,4) + 466*Power(t2,5) + 77*Power(t2,6)) \
- s*(18 + 2*(-122 + 157*s1 + 98*Power(s1,2))*t2 + 
                  2*(-591 + 634*s1 + 58*Power(s1,2))*Power(t2,2) - 
                  (1415 + 2046*s1 + 571*Power(s1,2))*Power(t2,3) - 
                  2*(-1116 + 928*s1 + 157*Power(s1,2))*Power(t2,4) + 
                  2*(1043 + 95*s1)*Power(t2,5) + 356*Power(t2,6)) + 
               Power(s,2)*t2*
                (-33 - 116*t2 + 181*Power(t2,2) + 1416*Power(t2,3) + 
                  601*Power(t2,4) - 
                  3*s1*(6 - 36*t2 + 145*Power(t2,2) + 22*Power(t2,3)))) \
+ Power(t1,7)*(150 - 855*t2 - 1895*Power(t2,2) - 573*Power(t2,3) - 
               55*Power(t2,4) + Power(s,3)*(5 + 2*t2 - 3*Power(t2,2)) + 
               Power(s1,3)*(49 + 82*t2 + 3*Power(t2,2)) - 
               2*Power(s1,2)*
                (10 - 125*t2 + 57*Power(t2,2) + 23*Power(t2,3)) + 
               s1*(-303 + 728*t2 + 752*Power(t2,2) - 344*Power(t2,3) - 
                  13*Power(t2,4)) + 
               s*(165 + 822*t2 + 1840*Power(t2,2) + 628*Power(t2,3) + 
                  13*Power(t2,4) - 
                  9*Power(s1,2)*(13 + 18*t2 + Power(t2,2)) + 
                  4*s1*(-49 - 48*t2 + 114*Power(t2,2) + 23*Power(t2,3))\
) + Power(s,2)*(s1*(63 + 78*t2 + 9*Power(t2,2)) - 
                  2*(32 + 113*t2 + 171*Power(t2,2) + 23*Power(t2,3)))) + 
            Power(t1,3)*Power(t2,2)*
             (203 + 230*t2 - 1373*Power(t2,2) - 2142*Power(t2,3) - 
               2419*Power(t2,4) - 1942*Power(t2,5) - 204*Power(t2,6) - 
               5*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(9 + 54*t2 + 85*Power(t2,2)) + 
               Power(s1,3)*t2*
                (202 - 341*t2 - 11*Power(t2,2) + 90*Power(t2,3)) + 
               Power(s1,2)*(168 - 1271*t2 + 22*Power(t2,2) + 
                  1448*Power(t2,3) + 721*Power(t2,4) + 114*Power(t2,5)) \
- s1*(196 - 1496*t2 - 719*Power(t2,2) + 910*Power(t2,3) + 
                  2341*Power(t2,4) + 639*Power(t2,5) + 35*Power(t2,6)) \
+ s*(18 + 2*(-7 - 21*s1 + 6*Power(s1,2))*t2 + 
                  (-419 + 920*s1 + 137*Power(s1,2))*Power(t2,2) - 
                  2*(931 - 114*s1 + 63*Power(s1,2))*Power(t2,3) - 
                  9*(17 + 100*s1 + 15*Power(s1,2))*Power(t2,4) + 
                  (666 - 174*s1)*Power(t2,5) + 186*Power(t2,6)) + 
               Power(s,2)*t2*
                (47 + 58*t2 + 140*Power(t2,2) - 506*Power(t2,3) - 
                  345*Power(t2,4) + 
                  s1*(6 - 81*t2 + 110*Power(t2,2) + 75*Power(t2,3)))) - 
            Power(t1,6)*(78 + 1467*t2 + 411*Power(t2,2) - 
               3653*Power(t2,3) - 1369*Power(t2,4) - 210*Power(t2,5) - 
               5*Power(t2,6) + 
               Power(s,3)*t2*(42 + 39*t2 + 16*Power(t2,2)) + 
               Power(s1,3)*(30 + 188*t2 + 341*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(s1,2)*(-67 + 109*t2 + 497*Power(t2,2) + 
                  163*Power(t2,3) - 155*Power(t2,4)) - 
               s1*(249 + 2362*t2 + 2795*Power(t2,2) - 
                  1012*Power(t2,3) + 327*Power(t2,4) + 62*Power(t2,5)) + 
               Power(s,2)*(-7 - 131*t2 - 577*Power(t2,2) - 
                  1157*Power(t2,3) - 243*Power(t2,4) + 
                  s1*(6 + 144*t2 + 317*Power(t2,2) + 18*Power(t2,3))) + 
               s*(-17 + 116*t2 + 2265*Power(t2,2) + 4418*Power(t2,3) + 
                  1905*Power(t2,4) + 106*Power(t2,5) - 
                  Power(s1,2)*
                   (-124 + 406*t2 + 697*Power(t2,2) + 84*Power(t2,3)) + 
                  2*s1*(5 - 197*t2 - 1103*Power(t2,2) + 
                     265*Power(t2,3) + 199*Power(t2,4))))) + 
         s2*t1*(-32*(-1 + s - s1)*Power(t1,9) - 
            4*Power(t2,6)*(1 - t2 - 2*Power(t2,2) + Power(t2,3) + 
               Power(t2,4)) + 
            t1*Power(t2,4)*(64 + (-71 + 54*s - 36*s1)*t2 + 
               (-113 + 42*Power(s,2) + 56*s1 - s*(103 + 12*s1))*
                Power(t2,2) + 
               (25 - 49*Power(s,2) + 14*Power(s,3) + 26*s*(-2 + s1) + 
                  12*s1)*Power(t2,3) + 
               (21 + Power(s,3) + s*(107 - 12*s1) - 16*s1 + 
                  Power(s,2)*(-67 + 10*s1))*Power(t2,4) - 
               2*(-30 + 5*Power(s,3) + Power(s,2)*(-12 + s1) + 8*s1 + 
                  s*s1)*Power(t2,5) + 
               (26 - 6*s + 8*Power(s,2))*Power(t2,6)) + 
            Power(t1,2)*Power(t2,3)*
             (Power(s,3)*Power(t2,2)*
                (-32 - 45*t2 + 34*Power(t2,2) + 51*Power(t2,3)) + 
               8*Power(s1,2)*t2*
                (24 - 34*t2 - 3*Power(t2,2) + 11*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s,2)*t2*
                (24 + (-17 + 54*s1)*t2 + (-502 + 27*s1)*Power(t2,2) - 
                  (202 + 27*s1)*Power(t2,3) + 
                  7*(27 + 4*s1)*Power(t2,4) + 36*Power(t2,5)) - 
               t2*(-569 - 113*t2 + 214*Power(t2,2) + 401*Power(t2,3) + 
                  353*Power(t2,4) + 86*Power(t2,5)) + 
               s1*(-84 - 136*t2 + 69*Power(t2,2) + 116*Power(t2,3) + 
                  187*Power(t2,4) - 92*Power(t2,5) - 12*Power(t2,6)) + 
               s*(60 - 2*(-65 + 96*s1)*t2 + 
                  6*(79 + 31*s1 + 6*Power(s1,2))*Power(t2,2) + 
                  (-543 + 214*s1 - 54*Power(s1,2))*Power(t2,3) + 
                  4*(-136 - 33*s1 + 2*Power(s1,2))*Power(t2,4) + 
                  (101 + 10*s1 + 2*Power(s1,2))*Power(t2,5) + 
                  2*(23 + 5*s1)*Power(t2,6))) - 
            Power(t1,8)*(280 + 388*t2 + 136*Power(s1,2)*t2 + 
               25*Power(t2,2) + 8*Power(s,2)*(12 + 17*t2) + 
               s1*(-284 + 140*t2 + 45*Power(t2,2)) - 
               s*(268 + 548*t2 + 45*Power(t2,2) + 16*s1*(6 + 17*t2))) + 
            Power(t1,7)*(-480 - 148*t2 + 1398*Power(t2,2) + 
               196*Power(t2,3) + 11*Power(t2,4) + 
               Power(s1,3)*(-84 - 140*t2 + 27*Power(t2,2)) - 
               Power(s,3)*(20 + 60*t2 + 27*Power(t2,2)) + 
               Power(s1,2)*(8 - 412*t2 + 355*Power(t2,2) + 
                  70*Power(t2,3)) + 
               s1*(708 + 704*t2 + 267*Power(t2,2) + 206*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s,2)*(136 + 516*t2 + 731*Power(t2,2) + 
                  70*Power(t2,3) + s1*(-92 - 20*t2 + 81*Power(t2,2))) - 
               s*(220 + 880*t2 + 2001*Power(t2,2) + 402*Power(t2,3) + 
                  2*Power(t2,4) + 
                  Power(s1,2)*(-196 - 220*t2 + 81*Power(t2,2)) + 
                  2*s1*(-208 - 220*t2 + 543*Power(t2,2) + 
                     70*Power(t2,3)))) + 
            Power(t1,4)*t2*(-240 - 733*t2 + 123*Power(t2,2) - 
               221*Power(t2,3) - 2089*Power(t2,4) - 1186*Power(t2,5) - 
               105*Power(t2,6) + 
               2*Power(s,3)*t2*
                (-18 + 52*t2 + 47*Power(t2,2) + 184*Power(t2,3) + 
                  53*Power(t2,4)) + 
               Power(s1,3)*(544 - 708*t2 - 370*Power(t2,2) + 
                  309*Power(t2,3) + 119*Power(t2,4) - 18*Power(t2,5)) + 
               Power(s1,2)*(-1488 + 424*t2 + 1147*Power(t2,2) + 
                  1100*Power(t2,3) + 498*Power(t2,4) - 
                  91*Power(t2,5) - 8*Power(t2,6)) - 
               s1*(-1364 - 1980*t2 + 2340*Power(t2,2) + 
                  2032*Power(t2,3) + 1903*Power(t2,4) + 
                  552*Power(t2,5) + 82*Power(t2,6)) + 
               s*(180 + 2*(-163 + 208*s1 + 74*Power(s1,2))*t2 + 
                  2*(-1561 + 1515*s1 + 58*Power(s1,2))*Power(t2,2) - 
                  (4444 + 1990*s1 + 401*Power(s1,2))*Power(t2,3) + 
                  (698 - 868*s1 - 238*Power(s1,2))*Power(t2,4) + 
                  (1317 + 550*s1 + 111*Power(s1,2))*Power(t2,5) + 
                  2*(97 + 27*s1)*Power(t2,6)) + 
               Power(s,2)*t2*
                (96 + 1247*t2 + 1858*Power(t2,2) - 1312*Power(t2,3) - 
                  877*Power(t2,4) - 56*Power(t2,5) + 
                  s1*(180 - 394*t2 + 446*Power(t2,2) - 
                     145*Power(t2,3) - 199*Power(t2,4)))) + 
            Power(t1,3)*Power(t2,2)*
             (-448 - 517*t2 + 749*Power(t2,2) + 715*Power(t2,3) + 
               1134*Power(t2,4) + 913*Power(t2,5) + 116*Power(t2,6) + 
               Power(s,3)*t2*
                (12 + 36*t2 + 32*Power(t2,2) - 185*Power(t2,3) - 
                  104*Power(t2,4)) + 
               2*Power(s1,3)*t2*
                (-98 + 131*t2 - 9*Power(t2,2) - 26*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s1,2)*(-384 + 936*t2 + 346*Power(t2,2) - 
                  619*Power(t2,3) - 331*Power(t2,4) - 4*Power(t2,5) + 
                  2*Power(t2,6)) + 
               s1*(780 - 1548*t2 - 522*Power(t2,2) + 125*Power(t2,3) + 
                  560*Power(t2,4) + 337*Power(t2,5) + 52*Power(t2,6)) - 
               s*(180 + (326 - 64*s1 - 84*Power(s1,2))*t2 + 
                  6*(-62 + 237*s1 + 24*Power(s1,2))*Power(t2,2) - 
                  4*(697 - 11*s1 + 25*Power(s1,2))*Power(t2,3) - 
                  (693 + 662*s1 + 107*Power(s1,2))*Power(t2,4) + 
                  2*(277 + 74*s1 + 21*Power(s1,2))*Power(t2,5) + 
                  2*(68 + 19*s1)*Power(t2,6)) + 
               Power(s,2)*t2*
                (16 - 635*t2 - 1536*Power(t2,2) + 149*Power(t2,3) + 
                  567*Power(t2,4) + 64*Power(t2,5) + 
                  s1*(-60 + 270*t2 - 88*Power(t2,2) - 66*Power(t2,3) + 
                     129*Power(t2,4)))) + 
            Power(t1,5)*(144 + 140*t2 - 2106*Power(t2,2) - 
               997*Power(t2,3) + 2690*Power(t2,4) + 945*Power(t2,5) + 
               87*Power(t2,6) - 
               Power(s,3)*t2*
                (-36 + 208*t2 + 234*Power(t2,2) + 349*Power(t2,3) + 
                  54*Power(t2,4)) + 
               Power(s1,3)*(128 + 980*t2 - 418*Power(t2,2) - 
                  392*Power(t2,3) - 104*Power(t2,4) + 25*Power(t2,5)) + 
               Power(s1,2)*(-432 - 1984*t2 - 279*Power(t2,2) - 
                  1539*Power(t2,3) - 248*Power(t2,4) + 
                  211*Power(t2,5) + 10*Power(t2,6)) + 
               s1*(52 + 1016*t2 + 3839*Power(t2,2) + 
                  4234*Power(t2,3) + 1925*Power(t2,4) + 
                  611*Power(t2,5) + 58*Power(t2,6)) - 
               s*(60 + 4*(-146 + 32*s1 + 105*Power(s1,2))*t2 + 
                  (-3639 + 2126*s1 - 204*Power(s1,2))*Power(t2,2) - 
                  2*(1300 + 1883*s1 + 471*Power(s1,2))*Power(t2,3) + 
                  (2835 + 204*s1 - 42*Power(s1,2))*Power(t2,4) + 
                  2*(844 + 413*s1 + 52*Power(s1,2))*Power(t2,5) + 
                  34*(4 + s1)*Power(t2,6)) + 
               Power(s,2)*t2*
                (-144 - 731*t2 - 547*Power(t2,2) + 2163*Power(t2,3) + 
                  759*Power(t2,4) + 24*Power(t2,5) + 
                  s1*(-180 + 102*t2 - 540*Power(t2,2) + 
                     411*Power(t2,3) + 133*Power(t2,4)))) + 
            Power(t1,6)*(136 + 1788*t2 + 1178*Power(t2,2) - 
               2405*Power(t2,3) - 546*Power(t2,4) - 49*Power(t2,5) + 
               Power(s1,3)*(20 + 184*t2 + 379*Power(t2,2) - 
                  2*Power(t2,3) - 11*Power(t2,4)) + 
               Power(s,3)*(-12 + 120*t2 + 199*Power(t2,2) + 
                  158*Power(t2,3) + 11*Power(t2,4)) - 
               2*Power(s1,2)*
                (12 - 218*t2 - 534*Power(t2,2) + 95*Power(t2,3) + 
                  101*Power(t2,4) + 2*Power(t2,5)) - 
               s1*(436 + 2848*t2 + 3221*Power(t2,2) + 924*Power(t2,3) + 
                  449*Power(t2,4) + 18*Power(t2,5)) - 
               Power(s,2)*(-56 + 76*t2 + 648*Power(t2,2) + 
                  1730*Power(t2,3) + 354*Power(t2,4) + 4*Power(t2,5) + 
                  s1*(-60 - 168*t2 - 229*Power(t2,2) + 
                     318*Power(t2,3) + 33*Power(t2,4))) + 
               s*(-116 - 1040*t2 + 263*Power(t2,2) + 3366*Power(t2,3) + 
                  1181*Power(t2,4) + 40*Power(t2,5) + 
                  Power(s1,2)*
                   (188 - 408*t2 - 807*Power(t2,2) + 162*Power(t2,3) + 
                     33*Power(t2,4)) + 
                  4*s1*(-40 - 18*t2 - 627*Power(t2,2) + 
                     342*Power(t2,3) + 139*Power(t2,4) + 2*Power(t2,5))))\
) + Power(t1,2)*(-2*Power(t2,5)*(1 + t2)*
             (-3 + (9 - 6*s)*t2 - 
               3*(3 - 5*s + 2*Power(s,2))*Power(t2,2) - 
               Power(-2 + s,2)*(-1 + 2*s)*Power(t2,3) + 
               Power(-1 + s,3)*Power(t2,4)) + 
            t1*Power(t2,3)*(-48 + (70 - 84*s + 12*s1)*t2 - 
               2*(-28 + 39*Power(s,2) + 6*s*(-9 + s1) + 6*s1)*
                Power(t2,2) + 
               (-26*Power(s,3) + Power(s,2)*(13 - 12*s1) - 
                  2*(31 + 5*s1) + 2*s*(61 + 5*s1))*Power(t2,3) + 
               (16 - 29*Power(s,3) + Power(s,2)*(127 - 4*s1) - 
                  14*s1 + 2*s*(-67 + 12*s1))*Power(t2,4) + 
               2*(-9 + 2*Power(s,3) + 15*s1 + Power(s,2)*(6 + 7*s1) - 
                  s*(19 + 17*s1))*Power(t2,5) + 
               2*(5*Power(s,3) - 3*(3 + s1) - Power(s,2)*(16 + 3*s1) + 
                  s*(13 + 6*s1))*Power(t2,6)) + 
            16*Power(t1,8)*(6 + 3*Power(s,2) + 3*Power(s1,2) + s1*t2 - 
               s*(8 + 6*s1 + t2)) + 
            Power(t1,3)*t2*(192 + (218 + 212*s - 144*Power(s,2))*t2 + 
               (-415 + 472*s + 62*Power(s,2) - 84*Power(s,3))*
                Power(t2,2) - 
               (65 + 1420*s - 1311*Power(s,2) + 266*Power(s,3))*
                Power(t2,3) - 
               (224 + 777*s - 463*Power(s,2) + 32*Power(s,3))*
                Power(t2,4) + 
               (-359 + 324*s - 412*Power(s,2) + 124*Power(s,3))*
                Power(t2,5) + 
               (-65 + 101*s - 88*Power(s,2) + 20*Power(s,3))*
                Power(t2,6) - 
               2*Power(s1,3)*t2*
                (-48 + 66*t2 - 7*Power(t2,2) - 10*Power(t2,3) - 
                  2*Power(t2,4) + Power(t2,5)) + 
               Power(s1,2)*(96 - 32*(7 + 3*s)*t2 + 
                  6*(-27 + 22*s)*Power(t2,2) + 
                  (109 + 4*s)*Power(t2,3) + 
                  (193 - 101*s)*Power(t2,4) + 20*Power(t2,5) + 
                  2*(-7 + 9*s)*Power(t2,6)) + 
               s1*(-416 + (900 - 320*s)*t2 + 
                  (6 + 1084*s - 348*Power(s,2))*Power(t2,2) + 
                  (-273 + 222*s + 76*Power(s,2))*Power(t2,3) + 
                  (38 - 788*s + 261*Power(s,2))*Power(t2,4) + 
                  (-173 + 110*s - 105*Power(s,2))*Power(t2,5) + 
                  (-40 + 74*s - 36*Power(s,2))*Power(t2,6))) + 
            Power(t1,4)*(16 + 4*(56 - 51*s + 36*Power(s,2))*t2 + 
               (-4 + 628*s - 570*Power(s,2) + 84*Power(s,3))*
                Power(t2,2) + 
               (-238 + 2441*s - 1865*Power(s,2) + 302*Power(s,3))*
                Power(t2,3) + 
               (477 + 184*s + 117*Power(s,2) - 86*Power(s,3))*
                Power(t2,4) + 
               (473 - 628*s + 586*Power(s,2) - 136*Power(s,3))*
                Power(t2,5) - 
               2*(-26 + 57*s - 31*Power(s,2) + 5*Power(s,3))*
                Power(t2,6) + 
               Power(s1,3)*(-192 + 192*t2 + 196*Power(t2,2) - 
                  88*Power(t2,3) - 79*Power(t2,4) + 6*Power(t2,5) + 
                  4*Power(t2,6)) + 
               Power(s1,2)*(576 + 32*(-13 + 4*s)*t2 - 
                  146*(1 + 2*s)*Power(t2,2) + 
                  (-157 + 54*s)*Power(t2,3) + 
                  (-383 + 322*s)*Power(t2,4) + 
                  (69 - 93*s)*Power(t2,5) - 2*(-7 + 9*s)*Power(t2,6)) + 
               s1*(-352 + (-924 + 64*s)*t2 + 
                  4*(284 - 509*s + 135*Power(s,2))*Power(t2,2) + 
                  (389 + 950*s - 412*Power(s,2))*Power(t2,3) + 
                  (457 + 950*s - 245*Power(s,2))*Power(t2,4) + 
                  (279 - 400*s + 223*Power(s,2))*Power(t2,5) + 
                  3*(17 - 22*s + 8*Power(s,2))*Power(t2,6))) + 
            Power(t1,2)*Power(t2,2)*
             (32 + (-326 + 60*s + 48*Power(s,2))*t2 + 
               (43 - 544*s + 194*Power(s,2) + 28*Power(s,3))*
                Power(t2,2) + 
               (133 + 163*s - 399*Power(s,2) + 126*Power(s,3))*
                Power(t2,3) + 
               (43 + 568*s - 429*Power(s,2) + 66*Power(s,3))*
                Power(t2,4) + 
               (141 - 55*s + 125*Power(s,2) - 51*Power(s,3))*
                Power(t2,5) + 
               (50 - 60*s + 72*Power(s,2) - 20*Power(s,3))*
                Power(t2,6) - 
               2*Power(s1,2)*t2*
                (48 + (-67 + 6*s)*t2 - (4 + 11*s)*Power(t2,2) + 
                  4*(3 + 2*s)*Power(t2,3) + (14 - 8*s)*Power(t2,4) + 
                  3*(-1 + s)*Power(t2,5)) + 
               s1*(96 + 4*(-13 + 48*s)*t2 + 
                  4*(-16 - 47*s + 21*Power(s,2))*Power(t2,2) + 
                  (92 - 254*s + 40*Power(s,2))*Power(t2,3) + 
                  (-126 + 194*s - 75*Power(s,2))*Power(t2,4) + 
                  (18 + 54*s - 8*Power(s,2))*Power(t2,5) + 
                  (20 - 46*s + 24*Power(s,2))*Power(t2,6))) + 
            Power(t1,7)*(288 - 16*Power(s1,3)*(-4 + t2) - 392*t2 + 
               16*Power(s,3)*t2 - 46*Power(t2,2) - 3*Power(t2,3) - 
               8*Power(s1,2)*(-18 + 12*t2 + 5*Power(t2,2)) - 
               s1*(352 + 68*t2 + 72*Power(t2,2) + Power(t2,3)) - 
               8*Power(s,2)*(-2 + 32*t2 + 5*Power(t2,2) + 
                  s1*(-8 + 6*t2)) + 
               s*(-64 + 524*t2 + 148*Power(t2,2) + Power(t2,3) + 
                  16*Power(s1,2)*(-8 + 3*t2) + 
                  16*s1*(-20 + 22*t2 + 5*Power(t2,2)))) + 
            Power(t1,6)*(Power(s,3)*t2*(44 - 80*t2 - 15*Power(t2,2)) + 
               Power(s1,3)*t2*(-116 - 28*t2 + 15*Power(t2,2)) + 
               4*(-76 - 168*t2 + 169*Power(t2,2) + 45*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s1,2)*(80 - 456*t2 + 2*Power(t2,2) + 
                  109*Power(t2,3) + 4*Power(t2,4)) + 
               s1*(432 + 836*t2 + 332*Power(t2,2) + 185*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s,2)*(-176 - 360*t2 + 602*Power(t2,2) + 
                  205*Power(t2,3) + 4*Power(t2,4) + 
                  s1*(96 - 316*t2 + 132*Power(t2,2) + 45*Power(t2,3))) \
- s*(-464 - 612*t2 + 896*Power(t2,2) + 455*Power(t2,3) + 
                  20*Power(t2,4) + 
                  Power(s1,2)*
                   (96 - 388*t2 + 24*Power(t2,2) + 45*Power(t2,3)) + 
                  2*s1*(240 - 656*t2 + 182*Power(t2,2) + 
                     157*Power(t2,3) + 4*Power(t2,4)))) + 
            Power(t1,5)*(-96 + 552*t2 + 628*Power(t2,2) - 
               702*Power(t2,3) - 365*Power(t2,4) - 34*Power(t2,5) + 
               Power(s,3)*t2*
                (-28 - 180*t2 + 141*Power(t2,2) + 72*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s1,3)*(-288 + 68*t2 + 72*Power(t2,2) + 
                  109*Power(t2,3) - 25*Power(t2,4) - 2*Power(t2,5)) - 
               2*Power(s1,2)*
                (-392 + 172*t2 - 260*Power(t2,2) - 121*Power(t2,3) + 
                  65*Power(t2,4) + 5*Power(t2,5)) - 
               s1*(80 + 1108*t2 + 808*Power(t2,2) + 607*Power(t2,3) + 
                  283*Power(t2,4) + 33*Power(t2,5)) - 
               Power(s,2)*(48 + 4*(-142 + 93*s1)*t2 - 
                  8*(159 + 70*s1)*Power(t2,2) + 
                  3*(222 + 7*s1)*Power(t2,3) + 
                  (464 + 169*s1)*Power(t2,4) + 6*(4 + s1)*Power(t2,5)) + 
               s*(16 - 1140*t2 - 1836*Power(t2,2) + 665*Power(t2,3) + 
                  702*Power(t2,4) + 72*Power(t2,5) + 
                  Power(s1,2)*
                   (-32 + 268*t2 - 340*Power(t2,2) - 229*Power(t2,3) + 
                     122*Power(t2,4) + 6*Power(t2,5)) + 
                  2*s1*(32 + 816*t2 - 960*Power(t2,2) - 
                     136*Power(t2,3) + 252*Power(t2,4) + 17*Power(t2,5)))\
)) - Power(s2,2)*(-2*Power(t2,7)*
             (1 - t2 - 2*Power(t2,2) + Power(t2,3) + Power(t2,4)) + 
            t1*Power(t2,5)*(34 + (-31 + 6*s - 12*s1)*t2 + 
               (-85 - 7*s + 14*s1)*Power(t2,2) + 
               (7 - 8*s + 14*s1)*Power(t2,3) + 
               3*(15 + s - 2*s1)*Power(t2,4) + 
               2*(15 + 2*s - 5*s1)*Power(t2,5) + 2*(6 + s)*Power(t2,6)) + 
            Power(t1,2)*Power(t2,3)*
             (-42 + 3*(29 + 4*s + 2*s1)*t2 + 
               (460 - 12*Power(s,2) + s*(143 - 72*s1) - 185*s1 + 
                  84*Power(s1,2))*Power(t2,2) + 
               (148 - 16*Power(s,3) + Power(s,2)*(67 - 6*s1) - 5*s1 - 
                  108*Power(s1,2) + 2*s*(49 + 11*s1 + 6*Power(s1,2)))*
                Power(t2,3) + 
               (-270 + 9*Power(s,3) - 23*Power(s,2)*(-5 + s1) + 
                  147*s1 - 28*Power(s1,2) + 
                  s*(-303 + 116*s1 - 14*Power(s1,2)))*Power(t2,4) + 
               (-487 + 20*Power(s,3) + 129*s1 + 40*Power(s1,2) + 
                  Power(s,2)*(-47 + 12*s1) - 
                  2*s*(63 - s1 + Power(s1,2)))*Power(t2,5) - 
               4*(70 + 10*Power(s,2) + 10*s1 - 3*Power(s1,2) + 
                  s*(-4 + 5*s1))*Power(t2,6) + 
               4*(-9 + s - s1)*Power(t2,7)) + 
            Power(t1,9)*(102 + 16*Power(s,2) + 16*Power(s1,2) + 97*t2 + 
               s1*(20 + 87*t2) - s*(96 + 32*s1 + 87*t2)) + 
            Power(t1,3)*Power(t2,2)*
             (70 - (633 + 56*s + 6*Power(s,2))*t2 + 
               (-560 - 172*s + 29*Power(s,2) + 22*Power(s,3))*
                Power(t2,2) + 
               (851 + 951*s - 448*Power(s,2) + 7*Power(s,3))*
                Power(t2,3) + 
               (1607 + 1684*s - 348*Power(s,2) - 70*Power(s,3))*
                Power(t2,4) + 
               (1596 + 13*s + 452*Power(s,2) - 97*Power(s,3))*
                Power(t2,5) + 
               (752 - 264*s + 179*Power(s,2))*Power(t2,6) + 
               (47 - 41*s)*Power(t2,7) - 
               2*Power(s1,3)*Power(t2,2)*
                (52 - 63*t2 - Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*t2*
                (-402 + (1011 - 6*s)*t2 + (189 + 19*s)*Power(t2,2) + 
                  (-618 + 32*s)*Power(t2,3) + 
                  (-214 + 36*s)*Power(t2,4) - 20*Power(t2,5)) + 
               s1*(48 + 10*(47 + 18*s)*t2 - 
                  2*(502 + 143*s + 6*Power(s,2))*Power(t2,2) + 
                  (-680 - 868*s + 75*Power(s,2))*Power(t2,3) - 
                  2*(75 - 18*s + 26*Power(s,2))*Power(t2,4) + 
                  (728 + 234*s + 15*Power(s,2))*Power(t2,5) - 
                  2*(-87 + s)*Power(t2,6) + 18*Power(t2,7))) + 
            Power(t1,5)*(26 + (461 + 72*s - 18*Power(s,2))*t2 + 
               (41 + 1086*s - 93*Power(s,2) + 30*Power(s,3))*
                Power(t2,2) - 
               (2432 - 3879*s + 254*Power(s,2) + 88*Power(s,3))*
                Power(t2,3) - 
               2*(-558 + 271*s - 796*Power(s,2) + 163*Power(s,3))*
                Power(t2,4) + 
               (3804 - 3919*s + 2062*Power(s,2) - 164*Power(s,3))*
                Power(t2,5) + 
               (811 - 1342*s + 307*Power(s,2))*Power(t2,6) - 
               8*(-9 + 8*s)*Power(t2,7) - 
               Power(s1,3)*(96 - 560*t2 - 972*Power(t2,2) + 
                  688*Power(t2,3) + 441*Power(t2,4) + 27*Power(t2,5)) + 
               Power(s1,2)*(288 + 2*(-569 + 32*s)*t2 - 
                  3*(871 + 242*s)*Power(t2,2) + 
                  2*(-851 + 317*s)*Power(t2,3) + 
                  2*(-876 + 473*s)*Power(t2,4) - 
                  (82 + 49*s)*Power(t2,5) + 93*Power(t2,6)) + 
               s1*(-176 + (-618 + 284*s)*t2 + 
                  2*(1607 - 673*s + 138*Power(s,2))*Power(t2,2) + 
                  (6915 + 188*s - 490*Power(s,2))*Power(t2,3) + 
                  (6920 + 3444*s - 346*Power(s,2))*Power(t2,4) + 
                  2*(928 - 285*s + 120*Power(s,2))*Power(t2,5) + 
                  (405 - 346*s)*Power(t2,6) + 25*Power(t2,7))) - 
            Power(t1,4)*t2*(-42 + (101 - 40*s - 18*Power(s,2))*t2 + 
               (1254 + 668*s - 13*Power(s,2) + 54*Power(s,3))*
                Power(t2,2) + 
               (785 + 3874*s - 766*Power(s,2) - 70*Power(s,3))*
                Power(t2,3) + 
               (1864 + 2313*s + 133*Power(s,2) - 221*Power(s,3))*
                Power(t2,4) + 
               (3139 - 1478*s + 1390*Power(s,2) - 182*Power(s,3))*
                Power(t2,5) + 
               (943 - 887*s + 327*Power(s,2))*Power(t2,6) + 
               (58 - 80*s)*Power(t2,7) + 
               Power(s1,3)*t2*
                (-520 + 816*t2 + 165*Power(t2,2) - 279*Power(t2,3) - 
                  44*Power(t2,4)) + 
               Power(s1,2)*(-48 + 2*(875 + 24*s)*t2 - 
                  (139 + 386*s)*Power(t2,2) + 
                  2*(-972 + 77*s)*Power(t2,3) + 
                  4*(-351 + 98*s)*Power(t2,4) + 
                  (-325 + 42*s)*Power(t2,5) + 22*Power(t2,6)) + 
               s1*(208 + (-2354 + 412*s)*t2 + 
                  2*(-813 - 633*s + 34*Power(s,2))*Power(t2,2) + 
                  (2269 - 1816*s - 20*Power(s,2))*Power(t2,3) + 
                  (3429 + 1760*s - 289*Power(s,2))*Power(t2,4) + 
                  (2035 + 368*s + 150*Power(s,2))*Power(t2,5) + 
                  (320 - 186*s)*Power(t2,6) + 31*Power(t2,7))) - 
            Power(t1,8)*(104 + 1204*t2 + 648*Power(t2,2) + 
               65*Power(t2,3) + Power(s,3)*(4 + 7*t2) - 
               Power(s1,3)*(32 + 7*t2) + 
               Power(s1,2)*(-78 + 47*t2 + 134*Power(t2,2)) + 
               s1*(-124 - 785*t2 + 382*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s,2)*(26 + 303*t2 + 134*Power(t2,2) - 
                  s1*(40 + 21*t2)) + 
               s*(-128 - 1153*t2 - 880*Power(t2,2) - 42*Power(t2,3) + 
                  Power(s1,2)*(68 + 21*t2) - 
                  2*s1*(-50 + 175*t2 + 134*Power(t2,2)))) + 
            Power(t1,7)*(-424 - 1310*t2 + 1895*Power(t2,2) + 
               1969*Power(t2,3) + 307*Power(t2,4) + 13*Power(t2,5) + 
               Power(s1,3)*(-48 - 239*t2 - 146*Power(t2,2) + 
                  11*Power(t2,3)) - 
               Power(s,3)*(12 - 17*t2 + 44*Power(t2,2) + 
                  11*Power(t2,3)) + 
               2*Power(s1,2)*
                (42 - 59*t2 - 226*Power(t2,2) + 207*Power(t2,3) + 
                  15*Power(t2,4)) + 
               s1*(520 + 2421*t2 - 26*Power(t2,2) + 434*Power(t2,3) + 
                  196*Power(t2,4) + Power(t2,5)) + 
               Power(s,2)*(-28 + 254*t2 + 1300*Power(t2,2) + 
                  718*Power(t2,3) + 30*Power(t2,4) + 
                  s1*(104 - 329*t2 - 58*Power(t2,2) + 33*Power(t2,3))) - 
               s*(-116 + 995*t2 + 3382*Power(t2,2) + 2912*Power(t2,3) + 
                  360*Power(t2,4) + Power(t2,5) + 
                  Power(s1,2)*
                   (-4 - 551*t2 - 248*Power(t2,2) + 33*Power(t2,3)) + 
                  4*s1*(54 - 400*t2 + 29*Power(t2,2) + 283*Power(t2,3) + 
                     15*Power(t2,4)))) + 
            Power(t1,6)*(80 + 906*t2 + 3319*Power(t2,2) - 
               1123*Power(t2,3) - 3265*Power(t2,4) - 610*Power(t2,5) - 
               50*Power(t2,6) + 
               7*Power(s,3)*t2*
                (2 + 2*t2 + 31*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s1,3)*(-192 + 344*t2 + 418*Power(t2,2) + 
                  413*Power(t2,3) - 10*Power(t2,4)) + 
               Power(s1,2)*(446 + 71*t2 + 1313*Power(t2,2) + 
                  1135*Power(t2,3) - 331*Power(t2,4) - 93*Power(t2,5)) - 
               Power(s,2)*(-6 + (-91 + 300*s1)*t2 + 
                  (359 - 690*s1)*Power(t2,2) + 
                  (2239 - 169*s1)*Power(t2,3) + 
                  (1661 + 150*s1)*Power(t2,4) + 149*Power(t2,5)) - 
               s1*(184 + 2419*t2 + 7204*Power(t2,2) + 4323*Power(t2,3) + 
                  793*Power(t2,4) + 363*Power(t2,5) + 9*Power(t2,6)) + 
               s*(-68 - 511*t2 - 180*Power(t2,2) + 3807*Power(t2,3) + 
                  4670*Power(t2,4) + 1017*Power(t2,5) + 20*Power(t2,6) + 
                  Power(s1,2)*
                   (-16 + 342*t2 - 994*Power(t2,2) - 799*Power(t2,3) + 
                     90*Power(t2,4)) + 
                  2*s1*(-26 + 327*t2 - 1329*Power(t2,2) - 
                     1019*Power(t2,3) + 783*Power(t2,4) + 121*Power(t2,5))\
))))*R1(t1))/((-1 + s1)*(-1 + t1)*t1*(-s + s1 - t2)*Power(t1 - t2,3)*
       (1 - s2 + t1 - t2)*Power(t1 - s2*t2,2)*
       Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2),2)) - 
    (8*(Power(s1,3)*Power(-1 + t2,2)*
          (-(Power(s2,2)*(s2*(-3 + t2) + 6*Power(-1 + t2,2))*
               Power(t2,4)) + Power(t1,6)*(1 + 3*t2 - 2*Power(t2,2)) - 
            s2*t1*Power(t2,3)*
             (-12*Power(-1 + t2,2) + Power(s2,2)*t2*(1 + 5*t2) + 
               s2*(6 + 5*t2 - 9*Power(t2,2) + 4*Power(t2,3))) + 
            Power(t1,5)*(3 - 8*t2 - 2*Power(t2,2) - Power(t2,3) + 
               2*Power(t2,4) + 
               s2*(-2 - 5*t2 - 3*Power(t2,2) + 4*Power(t2,3))) + 
            Power(t1,4)*(2 - 20*t2 + 28*Power(t2,2) - 7*Power(t2,3) + 
               7*Power(t2,4) - 4*Power(t2,5) + 
               Power(s2,2)*(1 + 5*t2 + 4*Power(t2,2) - 4*Power(t2,3)) + 
               s2*(-3 + 7*t2 + 13*Power(t2,2) + 12*Power(t2,3) - 
                  11*Power(t2,4))) + 
            Power(t1,2)*Power(t2,2)*
             (-6*Power(-1 + t2,2) + 
               Power(s2,3)*(6 + t2 - Power(t2,2)) + 
               Power(s2,2)*(-18 + 23*t2 + 17*Power(t2,2) - 
                  4*Power(t2,3)) + 
               s2*(15 - 15*t2 - 2*Power(t2,2) + 12*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(t1,3)*t2*(-8 + Power(s2,3)*(-3 + t2) + 11*t2 - 
               3*Power(t2,2) - 4*Power(t2,4) + 2*Power(t2,5) + 
               Power(s2,2)*(5 - 29*t2 + 6*Power(t2,3)) + 
               s2*(6 + 22*t2 - 48*Power(t2,2) - 9*Power(t2,3) + 
                  11*Power(t2,4)))) + 
         Power(s1,2)*(-1 + t2)*
          (Power(t1,7)*(-1 - 3*t2 + 2*Power(t2,2)) + 
            Power(t1,6)*(-1 - 6*t2 + 20*Power(t2,2) - 3*Power(t2,3) - 
               4*Power(t2,4) + 
               s2*(2 + 12*Power(t2,2) - 6*Power(t2,3)) + 
               s*(1 + 6*t2 - 9*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s2,2)*Power(t2,4)*
             (Power(s2,2)*(3 - 4*t2 - 2*Power(t2,2) + 5*Power(t2,3)) + 
               (-1 + t2)*(21 - (36 + 7*s)*t2 + 
                  (-19 + 21*s)*Power(t2,2) + 32*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(9 - 2*t2 + (-22 + 4*s)*Power(t2,2) + 8*Power(t2,3) + 
                  7*Power(t2,4))) - 
            s2*t1*Power(t2,3)*
             (Power(s2,3)*(-4 + 7*t2 - 5*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(-6 + (3 - 4*s)*t2 + 
                  5*(-1 + 4*s)*Power(t2,2) + (13 - 4*s)*Power(t2,3) + 
                  3*Power(t2,4)) + 
               2*(-1 + t2)*(33 - (64 + s)*t2 + 
                  (-3 + 8*s)*Power(t2,2) + 6*(6 + s)*Power(t2,3) + 
                  (-2 + s)*Power(t2,4)) + 
               s2*(58 + (-65 + 23*s)*t2 - 79*s*Power(t2,2) + 
                  (-58 + 65*s)*Power(t2,3) + (68 + 3*s)*Power(t2,4) - 
                  3*Power(t2,5))) - 
            Power(t1,2)*Power(t2,2)*
             (Power(s2,4)*(1 - 6*t2 + 12*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(s2,3)*(11 + (-5 + 4*s)*t2 + 
                  (1 - 20*s)*Power(t2,2) + (-31 + 4*s)*Power(t2,3)) + 
               Power(s2,2)*(-7 + (45 - 28*s)*t2 + 
                  (-59 + 62*s)*Power(t2,2) + (71 - 52*s)*Power(t2,3) - 
                  2*(40 + 9*s)*Power(t2,4) + 18*Power(t2,5)) - 
               (-1 + t2)*(33 - (62 + 7*s)*t2 + 
                  (1 + 13*s)*Power(t2,2) + 2*(8 + 9*s)*Power(t2,3) - 
                  2*(-9 + 8*s)*Power(t2,4) + 6*(-1 + s)*Power(t2,5)) + 
               s2*(-29 + (-51 + 10*s)*t2 + (80 + 21*s)*Power(t2,2) - 
                  3*(-50 + 7*s)*Power(t2,3) - (171 + s)*Power(t2,4) + 
                  (19 - 21*s)*Power(t2,5) + 2*Power(t2,6))) + 
            Power(t1,3)*t2*(-10 - 3*(4 + 3*s)*t2 + 
               (6 + 40*s)*Power(t2,2) + 8*(6 + s)*Power(t2,3) + 
               (4 - 82*s)*Power(t2,4) + (-50 + 57*s)*Power(t2,5) - 
               2*(-7 + 9*s)*Power(t2,6) - 
               Power(s2,4)*t2*(1 - 3*t2 + 4*Power(t2,2)) - 
               2*Power(s2,3)*
                (-1 + 4*t2 + (-9 + 2*s)*Power(t2,2) + 16*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,2)*(1 + (14 - 19*s)*t2 + 
                  (12 + 7*s)*Power(t2,2) + (-22 + 3*s)*Power(t2,3) - 
                  (71 + 27*s)*Power(t2,4) + 30*Power(t2,5)) + 
               s2*(7 + (-2 + 21*s)*t2 - (95 + 17*s)*Power(t2,2) + 
                  (255 - 29*s)*Power(t2,3) + 
                  (-208 + 37*s)*Power(t2,4) + (27 - 48*s)*Power(t2,5) + 
                  8*Power(t2,6))) + 
            Power(t1,5)*(1 - 17*t2 + 54*Power(t2,2) - 48*Power(t2,3) - 
               6*Power(t2,4) + 10*Power(t2,5) + 
               Power(s2,2)*(-1 + t2 - 7*Power(t2,2) - 9*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s2*(2 - 4*t2 + 22*Power(t2,2) - 72*Power(t2,3) + 
                  24*Power(t2,4) + 4*Power(t2,5)) + 
               s*(1 + 6*t2 - 18*Power(t2,2) + 4*Power(t2,3) + 
                  Power(t2,4) - 6*Power(t2,5) + 
                  s2*(-1 - 13*t2 + 5*Power(t2,2) + 9*Power(t2,3) - 
                     12*Power(t2,4)))) + 
            Power(t1,4)*(1 + (-24 + s)*t2 + (99 - 27*s)*Power(t2,2) + 
               (-140 + 9*s)*Power(t2,3) + (47 + 53*s)*Power(t2,4) + 
               (33 - 42*s)*Power(t2,5) + 2*(-7 + 9*s)*Power(t2,6) + 
               2*Power(s2,3)*t2*(1 - 3*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(-1 - Power(t2,2) + 3*Power(t2,3) + 
                  56*Power(t2,4) - 21*Power(t2,5) + 
                  s*t2*(7 + 4*t2 - 11*Power(t2,2) + 12*Power(t2,3))) + 
               s2*t2*(5 - 11*t2 - 67*Power(t2,2) + 137*Power(t2,3) - 
                  30*Power(t2,4) - 10*Power(t2,5) + 
                  s*(-8 + 33*t2 + 7*Power(t2,2) - 37*Power(t2,3) + 
                     41*Power(t2,4))))) + 
         s1*(-(Power(t1,7)*((-1 + t2)*
                  (1 + 6*(-1 + s2)*t2 + (9 + 2*s2)*Power(t2,2)) + 
                 s*(1 + 7*t2 - 3*Power(t2,2) + 7*Power(t2,3)))) + 
            s2*Power(t2,4)*(2*Power(s2,4)*t2*
                (-3 + 8*t2 - 11*Power(t2,2) + 6*Power(t2,3)) + 
               2*Power(-1 + t2,2)*
                (6 + 2*(-7 + 3*s)*t2 + (8 - 13*s)*Power(t2,2) + 
                  (2 + 6*s - 5*Power(s,2))*Power(t2,3) + 
                  (-2 + s + Power(s,2))*Power(t2,4)) + 
               Power(s2,3)*(15 + (-26 + 4*s)*t2 - 
                  (22 + 7*s)*Power(t2,2) + 26*(3 + s)*Power(t2,3) - 
                  (65 + 11*s)*Power(t2,4) + 20*Power(t2,5)) + 
               s2*(-1 + t2)*(43 + (-87 + 7*s)*t2 + 
                  (-34 + 17*s + 2*Power(s,2))*Power(t2,2) + 
                  (104 + 32*s - 39*Power(s,2))*Power(t2,3) + 
                  (29 - 53*s + 3*Power(s,2))*Power(t2,4) - 
                  3*(19 + s)*Power(t2,5) + 2*Power(t2,6)) + 
               Power(s2,2)*(7 + (27 + 5*s)*t2 - 
                  (84 + 4*s + 3*Power(s,2))*Power(t2,2) + 
                  (2 - 7*s - 10*Power(s,2))*Power(t2,3) + 
                  (115 + 20*s + Power(s,2))*Power(t2,4) - 
                  7*(11 + 2*s)*Power(t2,5) + 10*Power(t2,6))) + 
            Power(t1,2)*Power(t2,2)*
             (2*Power(s2,5)*t2*
                (1 - 6*t2 - 3*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,3)*(-5 - (-5 + s)*t2 + 
                  (-58 + 13*s - 9*Power(s,2))*Power(t2,2) + 
                  (187 + 231*s - 30*Power(s,2))*Power(t2,3) + 
                  (-254 - 125*s + 3*Power(s,2))*Power(t2,4) + 
                  (88 + 26*s)*Power(t2,5) + 37*Power(t2,6)) - 
               (-1 + t2)*(-43 + (124 - 21*s)*t2 + 
                  (-52 + 57*s - 40*Power(s,2))*Power(t2,2) + 
                  (-84 - 72*s + 62*Power(s,2))*Power(t2,3) + 
                  (57 - 79*s + 59*Power(s,2))*Power(t2,4) + 
                  (-16 + 161*s - 71*Power(s,2))*Power(t2,5) + 
                  2*(7 - 23*s + 12*Power(s,2))*Power(t2,6)) + 
               Power(s2,2)*(15 + (5 - 15*s)*t2 - 
                  (23 + 89*s + 21*Power(s,2))*Power(t2,2) + 
                  (-116 + 115*s + 42*Power(s,2))*Power(t2,3) + 
                  (148 + 411*s - 93*Power(s,2))*Power(t2,4) - 
                  2*(-7 + 212*s + 18*Power(s,2))*Power(t2,5) + 
                  (-52 + 74*s)*Power(t2,6) + 9*Power(t2,7)) + 
               s2*(24 + (33 + 83*s)*t2 + 
                  (13 - 340*s + 40*Power(s,2))*Power(t2,2) + 
                  (-284 - 89*s + 68*Power(s,2))*Power(t2,3) + 
                  (62 + 926*s - 129*Power(s,2))*Power(t2,4) + 
                  5*(73 - 112*s + 6*Power(s,2))*Power(t2,5) - 
                  (211 + 58*s + 45*Power(s,2))*Power(t2,6) + 
                  (-2 + 38*s)*Power(t2,7)) + 
               Power(s2,4)*t2*
                (-19 + 56*t2 - 120*Power(t2,2) + 40*Power(t2,3) + 
                  43*Power(t2,4) + 
                  s*(-4 + 27*t2 + 30*Power(t2,2) - 17*Power(t2,3)))) + 
            Power(t1,3)*t2*(2 + 7*(-12 + s)*t2 + 
               (145 - 72*s + 48*Power(s,2))*Power(t2,2) + 
               (-17 + 390*s - 190*Power(s,2))*Power(t2,3) + 
               (-16 - 465*s + 85*Power(s,2))*Power(t2,4) + 
               (-74 - 75*s + 156*Power(s,2))*Power(t2,5) + 
               (29 + 289*s - 123*Power(s,2))*Power(t2,6) + 
               (15 - 74*s + 36*Power(s,2))*Power(t2,7) + 
               2*Power(s2,5)*Power(t2,2)*(1 + 2*t2 - 3*Power(t2,2)) - 
               Power(s2,4)*t2*
                (2 + (-2 + 5*s)*t2 + 14*(-6 + s)*Power(t2,2) + 
                  (66 - 7*s)*Power(t2,3) + 18*Power(t2,4)) + 
               Power(s2,2)*(1 + (2 + 11*s)*t2 + 
                  (103 + 67*s + 17*Power(s,2))*Power(t2,2) + 
                  (-302 - 327*s + 40*Power(s,2))*Power(t2,3) + 
                  (312 - 161*s + 9*Power(s,2))*Power(t2,4) + 
                  (-47 + 296*s + 42*Power(s,2))*Power(t2,5) - 
                  2*(32 + 51*s)*Power(t2,6) - 5*Power(t2,7)) - 
               s2*(3 + t2 + 39*s*t2 + 
                  (110 - 225*s + 48*Power(s,2))*Power(t2,2) + 
                  (-291 - 249*s + 38*Power(s,2))*Power(t2,3) + 
                  (102 + 1271*s - 185*Power(s,2))*Power(t2,4) + 
                  4*(47 - 205*s + 16*Power(s,2))*Power(t2,5) - 
                  (111 + 22*s + 73*Power(s,2))*Power(t2,6) + 
                  (-2 + 54*s)*Power(t2,7)) + 
               Power(s2,3)*t2*
                (24 - 51*t2 + 21*Power(t2,2) + 197*Power(t2,3) - 
                  181*Power(t2,4) - 10*Power(t2,5) + 
                  Power(s,2)*t2*(3 + 10*t2 - Power(t2,2)) + 
                  s*(7 - 37*t2 - 173*Power(t2,2) + 85*Power(t2,3) - 
                     26*Power(t2,4)))) + 
            t1*Power(t2,3)*(2*Power(s2,5)*t2*
                (2 - 3*t2 + 12*Power(t2,2) - 11*Power(t2,3)) + 
               Power(s2,4)*(3 + 5*t2 - 5*(4 + 3*s)*Power(t2,2) + 
                  (10 - 42*s)*Power(t2,3) + 7*(7 + 3*s)*Power(t2,4) - 
                  47*Power(t2,5)) + 
               Power(s2,3)*(-32 + (30 - 11*s)*t2 + 
                  (114 + 17*s + 9*Power(s,2))*Power(t2,2) + 
                  (-171 - 95*s + 30*Power(s,2))*Power(t2,3) + 
                  (23 + 31*s - 3*Power(s,2))*Power(t2,4) + 
                  (69 + 10*s)*Power(t2,5) - 33*Power(t2,6)) - 
               s2*(-1 + t2)*(56 - (109 + 56*s)*t2 - 
                  2*(39 - 82*s + 6*Power(s,2))*Power(t2,2) + 
                  (152 + 32*s - 23*Power(s,2))*Power(t2,3) - 
                  2*(-43 + 71*s + 13*Power(s,2))*Power(t2,4) - 
                  (107 + 8*s + 7*Power(s,2))*Power(t2,5) + 
                  10*s*Power(t2,6)) + 
               Power(s2,2)*(-9 + 13*(-6 + s)*t2 + 
                  (70 + 56*s + 11*Power(s,2))*Power(t2,2) + 
                  (269 + 30*s - 96*Power(s,2))*Power(t2,3) + 
                  (-281 - 337*s + 115*Power(s,2))*Power(t2,4) + 
                  (-88 + 253*s + 6*Power(s,2))*Power(t2,5) + 
                  (124 - 15*s)*Power(t2,6) - 7*Power(t2,7)) + 
               2*Power(-1 + t2,2)*t2*
                (Power(-1 + t2,2)*(-1 - 4*t2 + 3*Power(t2,2)) + 
                  Power(s,2)*t2*
                   (6 + 2*t2 - 7*Power(t2,2) + 3*Power(t2,3)) + 
                  s*(6 - 5*t2 - 12*Power(t2,2) + 17*Power(t2,3) - 
                     6*Power(t2,4)))) + 
            Power(t1,6)*(1 + (-26 - 24*s + 4*Power(s,2))*t2 + 
               (75 + 95*s - 16*Power(s,2))*Power(t2,2) + 
               (-87 - 54*s + 6*Power(s,2))*Power(t2,3) + 
               (36 + 11*s - 6*Power(s,2))*Power(t2,4) + 
               (1 + 8*s)*Power(t2,5) + 
               4*Power(s2,2)*t2*
                (-2 - 3*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               s2*(-3 + 29*t2 - 73*Power(t2,2) + 31*Power(t2,3) + 
                  16*Power(t2,4) + 
                  s*(1 + 14*t2 + 29*Power(t2,2) - 14*Power(t2,3) + 
                     18*Power(t2,4)))) + 
            Power(t1,4)*(-1 + (16 + s)*t2 + 
               (11 + 46*s - 24*Power(s,2))*Power(t2,2) + 
               (-82 - 451*s + 158*Power(s,2))*Power(t2,3) + 
               (26 + 744*s - 137*Power(s,2))*Power(t2,4) + 
               (61 - 202*s - 68*Power(s,2))*Power(t2,5) + 
               (-20 - 192*s + 59*Power(s,2))*Power(t2,6) + 
               (-11 + 66*s - 24*Power(s,2))*Power(t2,7) + 
               4*Power(s2,4)*Power(t2,2)*
                (-1 - 4*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-2 + (29 + 11*s)*t2 + (-119 + 44*s)*Power(t2,2) - 
                  (3 + 11*s)*Power(t2,3) + (97 + 4*s)*Power(t2,4) - 
                  2*Power(t2,5)) + 
               s2*(1 + (-5 + s)*t2 + 
                  (-36 - 83*s + 24*Power(s,2))*Power(t2,2) + 
                  (81 - 45*s - 34*Power(s,2))*Power(t2,3) + 
                  (-30 + 721*s - 81*Power(s,2))*Power(t2,4) + 
                  (-74 - 550*s + 32*Power(s,2))*Power(t2,5) + 
                  (61 + 66*s - 49*Power(s,2))*Power(t2,6) + 
                  (2 + 34*s)*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-7 - 45*t2 + 212*Power(t2,2) - 375*Power(t2,3) + 
                  162*Power(t2,4) + 52*Power(t2,5) + Power(t2,6) - 
                  Power(s,2)*t2*
                   (5 + 27*t2 - 11*Power(t2,2) + 15*Power(t2,3)) + 
                  s*(-2 - 17*t2 + 240*Power(t2,2) + 20*Power(t2,3) - 
                     82*Power(t2,4) + 57*Power(t2,5)))) + 
            Power(t1,5)*(Power(s,2)*t2*
                (4 - 54*t2 + 81*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4) + 6*Power(t2,5) + 
                  s2*(-4 + 25*t2 + 6*Power(t2,2) - 3*Power(t2,3) + 
                     12*Power(t2,4))) - 
               (-1 + t2)*(-1 - 6*t2 + 40*Power(t2,2) - 49*Power(t2,3) + 
                  31*Power(t2,4) - 3*Power(t2,5) + 
                  2*Power(s2,3)*t2*
                   (1 + 11*t2 + 11*Power(t2,2) + Power(t2,3)) + 
                  Power(s2,2)*
                   (2 - 14*t2 - 3*Power(t2,2) + 130*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  s2*(-2 + 43*t2 - 68*Power(t2,2) + 17*Power(t2,3) + 
                     80*Power(t2,4) + 2*Power(t2,5))) - 
               s*(-1 + 18*t2 - 207*Power(t2,2) + 432*Power(t2,3) - 
                  192*Power(t2,4) - 48*Power(t2,5) + 34*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (7 + 43*t2 + 18*Power(t2,2) - 7*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  s2*(1 - 27*t2 + 73*Power(t2,2) + 183*Power(t2,3) - 
                     162*Power(t2,4) + 68*Power(t2,5) + 8*Power(t2,6))))) \
+ t2*(Power(t1,7)*(-2*s*(3 + s2*Power(-1 + t2,2) - 7*t2 - 
                  2*Power(t2,2)) + 
               Power(s,2)*(5 + 2*t2 + 5*Power(t2,2)) + 
               (-1 + t2)*(-5 + 13*t2 - 2*Power(t2,2) + s2*(2 + 6*t2))) + 
            Power(t1,6)*(2*Power(s,3)*t2*(4 + t2 + Power(t2,2)) + 
               (-1 + t2)*(-2*Power(s2,2)*(1 + 8*t2 + 7*Power(t2,2)) + 
                  t2*(22 - 53*t2 + 13*Power(t2,2)) + 
                  s2*(13 - 18*t2 - 45*Power(t2,2) + 2*Power(t2,3))) + 
               s*(1 + 25*t2 - 58*Power(t2,2) - 15*Power(t2,3) + 
                  11*Power(t2,4) + 
                  2*Power(s2,2)*Power(-1 + t2,2)*(1 + 2*t2) + 
                  s2*(6 + t2 - 62*Power(t2,2) + 7*Power(t2,3))) - 
               Power(s,2)*(-5 + 33*t2 - 33*Power(t2,2) + 
                  13*Power(t2,3) + 4*Power(t2,4) + 
                  s2*(5 + 10*t2 + 9*Power(t2,2) + 12*Power(t2,3)))) + 
            t1*Power(t2,2)*(2*Power(s2,5)*t2*
                (-4 + 10*t2 - 15*Power(t2,2) + 4*Power(t2,3) + 
                  5*Power(t2,4)) - 
               (-1 + t2)*(10 + 7*(-7 + 4*s)*t2 + 
                  (74 - 121*s + 38*Power(s,2))*Power(t2,2) + 
                  (-22 + 113*s - 93*Power(s,2) + 20*Power(s,3))*
                   Power(t2,3) + 
                  (-38 + 53*s + 4*Power(s,2) - 9*Power(s,3))*
                   Power(t2,4) + 
                  (31 - 101*s + 83*Power(s,2) - 19*Power(s,3))*
                   Power(t2,5) + 
                  2*(-3 + 14*s - 16*Power(s,2) + 5*Power(s,3))*
                   Power(t2,6)) + 
               Power(s2,3)*(25 + t2 + 18*s*t2 - 
                  9*(10 + s)*Power(t2,2) + 
                  (33 - 63*s - 7*Power(s,2))*Power(t2,3) + 
                  (63 + 99*s - 98*Power(s,2))*Power(t2,4) + 
                  (-39 + 77*s + 45*Power(s,2))*Power(t2,5) + 
                  (2 - 74*s)*Power(t2,6) + 5*Power(t2,7)) - 
               Power(s2,2)*(12 - 6*(17 + s)*t2 + 
                  (67 - 78*s + 3*Power(s,2))*Power(t2,2) + 
                  (129 + 284*s - 163*Power(s,2) + 6*Power(s,3))*
                   Power(t2,3) + 
                  (-5 - 101*s + 49*Power(s,2) + 32*Power(s,3))*
                   Power(t2,4) + 
                  (-151 - 306*s + 259*Power(s,2) + 10*Power(s,3))*
                   Power(t2,5) + 
                  (-6 + 175*s - 76*Power(s,2))*Power(t2,6) + 
                  8*(7 + 4*s)*Power(t2,7)) + 
               s2*(-29 + (44 - 39*s)*t2 + 
                  (139 - 4*s - 55*Power(s,2))*Power(t2,2) + 
                  (-262 + 200*s + 78*Power(s,2) - 9*Power(s,3))*
                   Power(t2,3) - 
                  (27 + 286*s - 230*Power(s,2) + 12*Power(s,3))*
                   Power(t2,4) + 
                  (212 + 131*s - 258*Power(s,2) + 13*Power(s,3))*
                   Power(t2,5) - 
                  (51 - 50*s + 31*Power(s,2) + 16*Power(s,3))*
                   Power(t2,6) + 
                  (-26 - 52*s + 36*Power(s,2))*Power(t2,7)) + 
               Power(s2,4)*t2*
                (1 - 13*t2 + 33*Power(t2,2) - 66*Power(t2,3) + 
                  30*Power(t2,4) + 15*Power(t2,5) + 
                  s*(8 - 22*t2 + 13*Power(t2,2) + 80*Power(t2,3) - 
                     43*Power(t2,4)))) + 
            Power(t2,3)*(2*Power(-1 + t2,2)*Power(1 + (-1 + s)*t2,3)*
                (-2 - t2 + Power(t2,2)) - 
               2*Power(s2,5)*t2*
                (-4 + 11*t2 - 12*Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,4)*(12 + (-11 + 8*s)*t2 - 
                  (43 + 22*s)*Power(t2,2) + (79 + 17*s)*Power(t2,3) + 
                  (-46 + 24*s)*Power(t2,4) + (4 - 15*s)*Power(t2,5) + 
                  5*Power(t2,6)) + 
               Power(s2,3)*(5 - (44 + 3*s)*t2 + 
                  (52 - 14*s)*Power(t2,2) + 
                  (52 + 51*s + 4*Power(s,2))*Power(t2,3) + 
                  2*(-51 - 18*s + 16*Power(s,2))*Power(t2,4) + 
                  (29 - 20*s - 12*Power(s,2))*Power(t2,5) + 
                  (9 + 22*s)*Power(t2,6) - Power(t2,7)) + 
               Power(s2,2)*(14 + 2*(-18 + s)*t2 + 
                  (-12 - 23*s + Power(s,2))*Power(t2,2) + 
                  (55 + 69*s - 56*Power(s,2) + Power(s,3))*
                   Power(t2,3) + 
                  (40 - 20*s - Power(s,2) + 10*Power(s,3))*
                   Power(t2,4) + 
                  (-84 - 78*s + 76*Power(s,2) + Power(s,3))*
                   Power(t2,5) + 
                  (6 + 43*s - 20*Power(s,2))*Power(t2,6) + 
                  (17 + 7*s)*Power(t2,7)) - 
               s2*(-1 + t2)*t2*
                (Power(-1 + t2,3)*(-15 - 3*t2 + 2*Power(t2,2)) - 
                  Power(s,3)*Power(t2,2)*(-2 + t2 + 3*Power(t2,2)) - 
                  s*Power(-1 + t2,2)*
                   (-14 - 47*t2 - 3*Power(t2,2) + 10*Power(t2,3)) + 
                  Power(s,2)*t2*
                   (16 + 15*t2 - 34*Power(t2,2) - 5*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(t1,2)*t2*(7 + (26 - 17*s)*t2 + 
               (-175 + 207*s - 42*Power(s,2))*Power(t2,2) + 
               (217 - 431*s + 206*Power(s,2) - 42*Power(s,3))*
                Power(t2,3) + 
               (27 + 234*s - 301*Power(s,2) + 72*Power(s,3))*
                Power(t2,4) + 
               (-170 + 145*s - 10*Power(s,2) + 16*Power(s,3))*
                Power(t2,5) + 
               (61 - 201*s + 219*Power(s,2) - 54*Power(s,3))*
                Power(t2,6) + 
               (7 + 63*s - 72*Power(s,2) + 20*Power(s,3))*Power(t2,7) + 
               2*Power(s2,5)*Power(t2,2)*
                (1 + 6*t2 - 3*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,4)*t2*
                (18 + 2*(-22 + s)*t2 + 3*(17 + s)*Power(t2,2) + 
                  (54 - 84*s)*Power(t2,3) + (-61 + 43*s)*Power(t2,4) - 
                  18*Power(t2,5)) + 
               Power(s2,2)*(-14 - (37 + 14*s)*t2 + 
                  (28 - 91*s + 3*Power(s,2))*Power(t2,2) + 
                  (110 + 343*s - 163*Power(s,2) + 12*Power(s,3))*
                   Power(t2,3) + 
                  s*(-103 + 150*s + 36*Power(s,2))*Power(t2,4) + 
                  (-73 - 539*s + 305*Power(s,2) + 24*Power(s,3))*
                   Power(t2,5) + 
                  (-78 + 278*s - 115*Power(s,2))*Power(t2,6) + 
                  (64 + 54*s)*Power(t2,7)) + 
               s2*(10 + (-57 + 25*s)*t2 + 
                  (-84 - 19*s + 69*Power(s,2))*Power(t2,2) + 
                  (170 + 11*s - 222*Power(s,2) + 20*Power(s,3))*
                   Power(t2,3) + 
                  (309 + 214*s - 394*Power(s,2) + 62*Power(s,3))*
                   Power(t2,4) - 
                  2*(231 + 211*s - 341*Power(s,2) + 10*Power(s,3))*
                   Power(t2,5) + 
                  (9 + 73*s + Power(s,2) + 34*Power(s,3))*Power(t2,6) + 
                  (105 + 118*s - 64*Power(s,2))*Power(t2,7)) - 
               Power(s2,3)*t2*
                (29 - 71*t2 + 69*Power(t2,2) - 92*Power(t2,3) + 
                  5*Power(t2,4) + 51*Power(t2,5) + 9*Power(t2,6) + 
                  3*Power(s,2)*Power(t2,2)*
                   (1 - 34*t2 + 21*Power(t2,2)) + 
                  s*(15 - 28*t2 - 9*Power(t2,2) + 165*Power(t2,3) + 
                     94*Power(t2,4) - 93*Power(t2,5)))) - 
            Power(t1,5)*(Power(s,3)*t2*
                (-6 + 45*t2 + 4*Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4) + 
                  s2*(5 + 8*t2 + 7*Power(t2,2) + 4*Power(t2,3))) - 
               (-1 + t2)*(4 - 59*t2 + 53*Power(t2,2) + 64*Power(t2,3) - 
                  44*Power(t2,4) + 
                  2*Power(s2,3)*t2*(5 + 14*t2 + 5*Power(t2,2)) + 
                  s2*(3 - 26*t2 - 6*Power(t2,2) + 144*Power(t2,3) - 
                     19*Power(t2,4)) + 
                  Power(s2,2)*
                   (-6 - 13*t2 + 88*Power(t2,2) + 61*Power(t2,3) + 
                     2*Power(t2,4))) - 
               Power(s,2)*t2*
                (-8 + 151*t2 - 194*Power(t2,2) - 9*Power(t2,3) + 
                  24*Power(t2,4) + 
                  Power(s2,2)*
                   (10 + 3*t2 + 16*Power(t2,2) + 7*Power(t2,3)) + 
                  s2*(9 + 28*t2 - 55*Power(t2,2) + 50*Power(t2,3) + 
                     4*Power(t2,4))) + 
               s*(-6 + 42*t2 + 19*Power(t2,2) - 128*Power(t2,3) - 
                  11*Power(t2,4) + 48*Power(t2,5) + 
                  2*Power(s2,3)*t2*(2 - 3*t2 + Power(t2,3)) + 
                  Power(s2,2)*
                   (2 + 13*t2 - 48*Power(t2,2) - 65*Power(t2,3) + 
                     26*Power(t2,4)) + 
                  s2*(5 - 7*t2 + 59*Power(t2,2) - 251*Power(t2,3) + 
                     28*Power(t2,4) + 22*Power(t2,5)))) + 
            Power(t1,3)*(-1 + (-9 + 2*s)*t2 + 
               (109 - 141*s + 18*Power(s,2))*Power(t2,2) + 
               (-93 + 300*s - 137*Power(s,2) + 46*Power(s,3))*
                Power(t2,3) - 
               (222 + 231*s - 459*Power(s,2) + 110*Power(s,3))*
                Power(t2,4) + 
               (290 + 34*s - 243*Power(s,2) - 10*Power(s,3))*
                Power(t2,5) + 
               (-26 + 128*s - 209*Power(s,2) + 46*Power(s,3))*
                Power(t2,6) - 
               4*(12 + 23*s - 22*Power(s,2) + 5*Power(s,3))*
                Power(t2,7) + 
               2*Power(s2,5)*Power(t2,3)*(-3 + 2*t2 + Power(t2,2)) + 
               Power(s2,3)*t2*
                (-12 + (25 - 9*s)*t2 + 
                  (17 - s + 11*Power(s,2))*Power(t2,2) + 
                  (-196 + 171*s - 38*Power(s,2))*Power(t2,3) + 
                  (92 + 37*s + 39*Power(s,2))*Power(t2,4) + 
                  (67 - 54*s)*Power(t2,5) + 7*Power(t2,6)) + 
               s2*(1 - (-25 + s)*t2 + 
                  (44 + 61*s - 37*Power(s,2))*Power(t2,2) + 
                  (13 - 255*s + 213*Power(s,2) - 26*Power(s,3))*
                   Power(t2,3) - 
                  (417 + 53*s - 302*Power(s,2) + 84*Power(s,3))*
                   Power(t2,4) + 
                  (325 + 622*s - 779*Power(s,2) + 2*Power(s,3))*
                   Power(t2,5) + 
                  (148 - 184*s + 65*Power(s,2) - 36*Power(s,3))*
                   Power(t2,6) + 
                  (-139 - 142*s + 56*Power(s,2))*Power(t2,7)) + 
               Power(s2,4)*Power(t2,2)*
                (2 - 43*t2 - 12*Power(t2,2) + 43*Power(t2,3) + 
                  10*Power(t2,4) - 
                  s*(2 + t2 - 32*Power(t2,2) + 17*Power(t2,3))) - 
               Power(s2,2)*t2*
                (-23 + 17*t2 + 2*Power(t2,2) + 62*Power(t2,3) + 
                  83*Power(t2,4) - 167*Power(t2,5) + 26*Power(t2,6) + 
                  2*Power(s,3)*Power(t2,2)*(5 + 8*t2 + 11*Power(t2,2)) + 
                  Power(s,2)*
                   (t2 - 71*Power(t2,2) + 146*Power(t2,3) + 
                     121*Power(t2,4) - 89*Power(t2,5)) + 
                  s*(-6 - 26*t2 + 107*Power(t2,2) - 27*Power(t2,3) - 
                     525*Power(t2,4) + 221*Power(t2,5) + 40*Power(t2,6)))\
) + Power(t1,4)*(Power(s,3)*Power(t2,2)*
                (Power(s2,2)*(3 + 2*t2 + 7*Power(t2,2)) + 
                  s2*(18 + 45*t2 + 14*Power(t2,2) + 19*Power(t2,3)) + 
                  2*(-13 + 49*t2 + 2*Power(t2,2) - 7*Power(t2,3) + 
                     5*Power(t2,4))) - 
               Power(s,2)*t2*(2 - 35*t2 + 366*Power(t2,2) - 
                  357*Power(t2,3) - 98*Power(t2,4) + 62*Power(t2,5) + 
                  Power(s2,3)*t2*(5 - 2*t2 + 9*Power(t2,2)) + 
                  Power(s2,2)*t2*
                   (25 - 43*t2 + 17*Power(t2,2) + 37*Power(t2,3)) + 
                  s2*(-7 + 72*t2 + 107*Power(t2,2) - 390*Power(t2,3) + 
                     86*Power(t2,4) + 24*Power(t2,5))) - 
               (-1 + t2)*(2*Power(s2,4)*Power(t2,2)*
                   (7 + 8*t2 + Power(t2,2)) + 
                  Power(s2,3)*t2*
                   (-10 + 27*t2 + 110*Power(t2,2) + 39*Power(t2,3) + 
                     2*Power(t2,4)) - 
                  t2*(25 + 102*t2 - 206*Power(t2,2) + 3*Power(t2,3) + 
                     70*Power(t2,4)) + 
                  Power(s2,2)*
                   (2 + 4*t2 - 63*Power(t2,2) + 113*Power(t2,3) + 
                     147*Power(t2,4) + Power(t2,5)) - 
                  s2*(3 + 18*t2 + 67*Power(t2,2) - 87*Power(t2,3) - 
                     160*Power(t2,4) + 79*Power(t2,5))) + 
               s*(-1 + 35*t2 - 20*Power(t2,2) + 
                  2*Power(s2,4)*Power(-1 + t2,2)*Power(t2,2) + 
                  69*Power(t2,3) - 157*Power(t2,4) - 26*Power(t2,5) + 
                  88*Power(t2,6) + 
                  Power(s2,3)*t2*
                   (4 + 8*t2 - 75*Power(t2,2) + 15*Power(t2,4)) + 
                  Power(s2,2)*t2*
                   (12 - 10*t2 - 53*Power(t2,2) - 273*Power(t2,3) + 
                     97*Power(t2,4) + 11*Power(t2,5)) + 
                  s2*(1 - 38*t2 + 127*Power(t2,2) + 77*Power(t2,3) - 
                     516*Power(t2,4) + 117*Power(t2,5) + 88*Power(t2,6))))\
))*R1(t2))/((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*Power(t1 - t2,3)*
       (1 - s2 + t1 - t2)*Power(-1 + t2,3)*t2*Power(t1 - s2*t2,2)) + 
    (8*(2*Power(s2,8)*Power(t2,2)*(6 - 3*s - 5*s1 + t2) - 
         2*Power(t2,3)*Power(1 + (-1 + s)*t2,3)*(-2 - t2 + Power(t2,2)) + 
         t1*t2*(1 + (-1 + s)*t2)*
          (-28 + (44 - 56*s)*t2 + 
            (25 - 28*Power(s,2) + s*(34 - 12*s1) + 2*s1)*Power(t2,2) + 
            (-50 - 22*Power(s,2) + s*(90 - 4*s1) + 6*s1)*Power(t2,3) + 
            (7 + 9*Power(s,2) - 14*s1 + 2*s*(-6 + 7*s1))*Power(t2,4) + 
            2*(1 + 3*Power(s,2) + 3*s1 - s*(7 + 3*s1))*Power(t2,5)) + 
         16*Power(t1,6)*(6 + 3*Power(s,2) + 3*Power(s1,2) + s1*t2 - 
            s*(8 + 6*s1 + t2)) + 
         Power(t1,2)*(2*Power(s1,3)*(2 - 3*t2)*Power(t2,2) + 
            Power(s,3)*Power(t2,2)*
             (40 + 78*t2 + 17*Power(t2,2) - 39*Power(t2,3) - 
               6*Power(t2,4)) + 
            Power(s1,2)*t2*(-36 + 48*t2 + 5*Power(t2,2) + 
               15*Power(t2,3) - 18*Power(t2,4) + 6*Power(t2,5)) + 
            Power(s,2)*t2*(108 + 4*(-4 + 21*s1)*t2 + 
               (-399 + 30*s1)*Power(t2,2) - (189 + 86*s1)*Power(t2,3) + 
               (126 + 23*s1)*Power(t2,4) + 2*(13 + 6*s1)*Power(t2,5)) + 
            s1*(72 - 92*t2 - 70*Power(t2,2) + 149*Power(t2,3) - 
               145*Power(t2,4) + 50*Power(t2,5) + 2*Power(t2,6)) + 
            2*(-40 + 126*Power(t2,2) - 46*Power(t2,3) - 
               32*Power(t2,4) - 5*Power(t2,5) + 5*Power(t2,6)) - 
            s*(-72 - 4*(-41 + 30*s1)*t2 + 2*(161 + 72*s1)*Power(t2,2) + 
               (-367 + 190*s1 - 10*Power(s1,2))*Power(t2,3) + 
               (-252 - 258*s1 + 19*Power(s1,2))*Power(t2,4) + 
               (93 + 35*s1 - 16*Power(s1,2))*Power(t2,5) + 
               (24 + 22*s1 + 6*Power(s1,2))*Power(t2,6))) + 
         Power(t1,5)*(288 - 16*Power(s1,3)*(-3 + t2) - 156*t2 - 
            44*Power(t2,2) - 3*Power(t2,3) + 16*Power(s,3)*(1 + t2) - 
            8*Power(s1,2)*(-2 + 5*Power(t2,2)) - 
            s1*(-8 + 12*t2 + 34*Power(t2,2) + Power(t2,3)) - 
            8*Power(s,2)*(s1*(-2 + 6*t2) + 5*(2 + 4*t2 + Power(t2,2))) + 
            s*(-152 + 236*t2 + 110*Power(t2,2) + Power(t2,3) + 
               16*Power(s1,2)*(-5 + 3*t2) + 
               16*s1*(-6 + 10*t2 + 5*Power(t2,2)))) + 
         Power(t1,4)*(Power(s,3)*
             (8 + 36*t2 - 62*Power(t2,2) - 15*Power(t2,3)) + 
            Power(s1,3)*(-24 + 12*t2 - 46*Power(t2,2) + 
               15*Power(t2,3)) + 
            4*(4 - 96*t2 + Power(t2,2) + 16*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s1,2)*(336 - 140*t2 + 12*Power(t2,2) + 
               23*Power(t2,3) + 4*Power(t2,4)) + 
            s1*(-264 + 76*t2 - 18*Power(t2,2) + 51*Power(t2,3) + 
               7*Power(t2,4)) + 
            Power(s,2)*(-464 - 268*t2 + 308*Power(t2,2) + 
               119*Power(t2,3) + 4*Power(t2,4) + 
               s1*(152 - 172*t2 + 78*Power(t2,2) + 45*Power(t2,3))) - 
            s*(-696 - 420*t2 + 230*Power(t2,2) + 175*Power(t2,3) + 
               18*Power(t2,4) + 
               Power(s1,2)*(136 - 124*t2 - 30*Power(t2,2) + 
                  45*Power(t2,3)) + 
               2*s1*(96 - 276*t2 + 40*Power(t2,2) + 71*Power(t2,3) + 
                  4*Power(t2,4)))) + 
         Power(t1,3)*(-320 - 8*t2 + 180*Power(t2,2) + 174*Power(t2,3) - 
            59*Power(t2,4) - 7*Power(t2,5) - 
            2*Power(s1,2)*t2*
             (40 + 78*t2 - 58*Power(t2,2) + 17*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s1,3)*(8 - 12*t2 + 18*Power(t2,2) + 3*Power(t2,3) + 
               4*Power(t2,4) - 2*Power(t2,5)) + 
            Power(s,3)*(8 - 52*t2 - 98*Power(t2,2) + 45*Power(t2,3) + 
               43*Power(t2,4) + 2*Power(t2,5)) - 
            s1*(-312 + 276*t2 - 314*Power(t2,2) + 105*Power(t2,3) + 
               10*Power(t2,4) + 12*Power(t2,5)) - 
            Power(s,2)*(2*t2*
                (-184 - 358*t2 + 54*Power(t2,2) + 91*Power(t2,3) + 
                  8*Power(t2,4)) + 
               s1*(72 + 164*t2 - 118*Power(t2,2) - 65*Power(t2,3) + 
                  82*Power(t2,4) + 6*Power(t2,5))) + 
            s*(280 - 188*t2 - 866*Power(t2,2) - 7*Power(t2,3) + 
               169*Power(t2,4) + 29*Power(t2,5) + 
               Power(s1,2)*(-72 + 68*t2 + 74*Power(t2,2) - 
                  113*Power(t2,3) + 35*Power(t2,4) + 6*Power(t2,5)) + 
               2*s1*(16 + 248*t2 - 208*Power(t2,2) - 104*Power(t2,3) + 
                  63*Power(t2,4) + 9*Power(t2,5)))) + 
         Power(s2,7)*(-(Power(s1,3)*(2 + t2)) - 
            2*Power(s1,2)*t2*(1 - s - t1 + t2) + 
            s1*t2*((19 + 7*s - 49*t2)*t2 + t1*(20 + 11*t2)) + 
            t2*(t1*(-24 - 36*t2 - 5*Power(t2,2) + 4*s*(3 + 5*t2)) + 
               t2*(-12 + 6*Power(s,2) + 65*t2 + 8*Power(t2,2) - 
                  s*(16 + 41*t2)))) + 
         Power(s2,6)*(Power(s1,3)*
             (4 - 14*t2 - 4*Power(t2,2) + t1*(5 + 7*t2)) + 
            Power(t2,2)*(-23 - 70*t2 + 146*Power(t2,2) + 
               12*Power(t2,3) + 6*Power(s,2)*(1 + 7*t2) - 
               11*s*(-1 + 8*t2 + 10*Power(t2,2))) + 
            Power(t1,2)*(12 + 66*t2 + 39*Power(t2,2) + 5*Power(t2,3) - 
               s*(6 + 40*t2 + 21*Power(t2,2))) + 
            t1*t2*(-3*Power(s,2)*(4 + 9*t2) + 
               s*(32 + 180*t2 + 101*Power(t2,2)) - 
               2*(-12 + 101*t2 + 76*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s1,2)*(6 + (15 + 2*s)*t2 + (-9 + 7*s)*Power(t2,2) - 
               10*Power(t2,3) - 2*Power(t1,2)*(1 + 2*t2) + 
               t1*(2 + 6*t2 + 9*Power(t2,2) - s*(8 + 11*t2))) + 
            s1*(2*Power(t1,2)*(-5 + (-11 + 2*s)*t2 + 5*Power(t2,2)) + 
               t1*t2*(-42 + 4*Power(s,2) + 12*s*(-2 + t2) + 127*t2 + 
                  40*Power(t2,2)) + 
               t2*(4 + 18*t2 - Power(s,2)*t2 + 101*Power(t2,2) - 
                  98*Power(t2,3) + s*(2 + 4*t2 + 33*Power(t2,2))))) - 
         Power(s2,5)*(Power(t1,3)*
             (32 + 63*t2 - 2*Power(s,2)*t2 + 21*Power(t2,2) - 
               s*(20 + 42*t2 + 11*Power(t2,2))) + 
            Power(s1,3)*(Power(t1,2)*(7 + 11*t2) + 
               2*t2*(-8 + 18*t2 + 3*Power(t2,2)) - 
               t1*(10 + 31*t2 + 28*Power(t2,2))) - 
            Power(t1,2)*(-12 + 209*t2 + 2*Power(s,3)*t2 + 
               458*Power(t2,2) + 129*Power(t2,3) + 15*Power(t2,4) + 
               Power(s,2)*(6 + 46*t2 + 35*Power(t2,2)) - 
               s*(16 + 241*t2 + 391*Power(t2,2) + 81*Power(t2,3))) + 
            t1*t2*(-48 - 332*t2 - 9*Power(s,3)*t2 + 519*Power(t2,2) + 
               231*Power(t2,3) + 30*Power(t2,4) + 
               Power(s,2)*(10 + 149*t2 + 128*Power(t2,2)) - 
               s*(-12 + 223*t2 + 637*Power(t2,2) + 204*Power(t2,3))) + 
            t2*(2 + 52*t2 + 101*Power(t2,2) + 150*Power(t2,3) - 
               174*Power(t2,4) - 8*Power(t2,5) + 
               3*Power(s,3)*t2*(2 + t2) + 
               Power(s,2)*t2*(15 - 27*t2 - 116*Power(t2,2)) + 
               s*(4 - 78*t2 - 75*Power(t2,2) + 202*Power(t2,3) + 
                  150*Power(t2,4))) - 
            Power(s1,2)*(2*Power(t1,3)*(2 + t2) + 
               2*(-6 + 22*t2 + (35 + 4*s)*Power(t2,2) + 
                  4*(-2 + s)*Power(t2,3) - 10*Power(t2,4)) + 
               Power(t1,2)*(-4 - 18*t2 + 9*Power(t2,2) + 
                  s*(17 + 24*t2)) + 
               t1*(-27 - 21*t2 + 15*Power(t2,2) + 16*Power(t2,3) + 
                  s*(16 - 55*t2 - 42*Power(t2,2)))) + 
            s1*(6 + (27 + 22*s)*t2 + 
               (24 + 24*s - 10*Power(s,2))*Power(t2,2) + 
               2*(-29 - 7*s + 3*Power(s,2))*Power(t2,3) - 
               2*(107 + 31*s)*Power(t2,4) + 102*Power(t2,5) + 
               Power(t1,3)*(-11 + 20*t2 + 11*Power(t2,2) + 
                  4*s*(1 + t2)) + 
               Power(t1,2)*(-23 + 99*t2 + 127*Power(t2,2) - 
                  37*Power(t2,3) + 5*Power(s,2)*(2 + 3*t2) + 
                  s*(-17 + 28*t2 + 44*Power(t2,2))) - 
               t1*(-4 - 40*t2 - 325*Power(t2,2) + 253*Power(t2,3) + 
                  50*Power(t2,4) + Power(s,2)*t2*(8 + 5*t2) + 
                  2*s*(5 + 30*t2 - 52*Power(t2,2) + 29*Power(t2,3))))) + 
         Power(s2,4)*(2 + 13*t2 + 20*s*t2 + 95*Power(t2,2) - 
            5*s*Power(t2,2) - 38*Power(s,2)*Power(t2,2) + 
            4*Power(s,3)*Power(t2,2) - 121*Power(t2,3) + 
            282*s*Power(t2,3) - 82*Power(s,2)*Power(t2,3) - 
            22*Power(s,3)*Power(t2,3) - 174*Power(t2,4) + 
            166*s*Power(t2,4) + 54*Power(s,2)*Power(t2,4) - 
            14*Power(s,3)*Power(t2,4) - 148*Power(t2,5) - 
            248*s*Power(t2,5) + 164*Power(s,2)*Power(t2,5) + 
            116*Power(t2,6) - 110*s*Power(t2,6) + 2*Power(t2,7) + 
            Power(t1,4)*(29 - 2*Power(s,2) + 27*t2 - s*(21 + 22*t2)) - 
            Power(t1,3)*(72 + 492*t2 + 314*Power(t2,2) + 
               63*Power(t2,3) + Power(s,3)*(4 + 5*t2) + 
               Power(s,2)*(19 + 82*t2 + 18*Power(t2,2)) - 
               s*(102 + 483*t2 + 272*Power(t2,2) + 23*Power(t2,3))) + 
            Power(s1,3)*(Power(t1,3)*(7 + 5*t2) - 
               4*Power(t2,2)*(-6 + 11*t2 + Power(t2,2)) - 
               Power(t1,2)*(35 + 61*t2 + 29*Power(t2,2)) + 
               t1*(-28 + 54*t2 + 55*Power(t2,2) + 42*Power(t2,3))) + 
            Power(t1,2)*(-25 - 458*t2 + 578*Power(t2,2) + 
               842*Power(t2,3) + 159*Power(t2,4) + 15*Power(t2,5) - 
               Power(s,3)*t2*(14 + 9*t2) + 
               Power(s,2)*(10 + 233*t2 + 403*Power(t2,2) + 
                  121*Power(t2,3)) - 
               s*(-1 + 178*t2 + 1132*Power(t2,2) + 925*Power(t2,3) + 
                  117*Power(t2,4))) + 
            t1*(2 + 106*t2 + 298*Power(t2,2) + 832*Power(t2,3) - 
               627*Power(t2,4) - 157*Power(t2,5) - 20*Power(t2,6) + 
               Power(s,2)*Power(t2,2)*(22 - 511*t2 - 242*Power(t2,2)) + 
               2*Power(s,3)*t2*(6 + 4*t2 + 21*Power(t2,2)) + 
               s*(-2 - 213*t2 - 460*Power(t2,2) + 534*Power(t2,3) + 
                  941*Power(t2,4) + 206*Power(t2,5))) + 
            Power(s1,2)*(-2*Power(t1,4) + 
               2*t2*(-24 + 46*t2 + 6*(10 + s)*Power(t2,2) + 
                  (-7 + s)*Power(t2,3) - 10*Power(t2,4)) - 
               Power(t1,3)*(-9 + 16*t2 + 18*Power(t2,2) + 
                  3*s*(6 + 5*t2)) + 
               Power(t1,2)*(30 + 37*t2 - 45*Power(t2,2) + 
                  53*Power(t2,3) + s*(42 + 103*t2 + 49*Power(t2,2))) - 
               t1*(32 + 241*t2 + 59*Power(t2,2) - 5*Power(t2,3) - 
                  14*Power(t2,4) + 
                  s*(12 - 70*t2 + 79*Power(t2,2) + 58*Power(t2,3)))) + 
            s1*(12 + 2*(-23 + 6*s)*t2 - (125 + 32*s)*Power(t2,2) + 
               (-92 - 96*s + 40*Power(s,2))*Power(t2,3) + 
               (70 + 16*s - 14*Power(s,2))*Power(t2,4) + 
               (226 + 58*s)*Power(t2,5) - 58*Power(t2,6) + 
               2*Power(t1,4)*(5 + 2*s + 11*t2) + 
               Power(t1,3)*(21 + 130*t2 - 108*Power(t2,2) - 
                  23*Power(t2,3) + 15*Power(s,2)*(1 + t2) + 
                  2*s*(8 + 49*t2 + 18*Power(t2,2))) - 
               Power(t1,2)*(-22 - 347*t2 + 238*Power(t2,2) + 
                  179*Power(t2,3) - 51*Power(t2,4) + 
                  Power(s,2)*(-17 + 28*t2 + 11*Power(t2,2)) + 
                  s*(76 - 55*t2 + 184*Power(t2,2) + 174*Power(t2,3))) + 
               t1*(39 + 105*t2 - 11*Power(t2,2) - 792*Power(t2,3) + 
                  221*Power(t2,4) + 20*Power(t2,5) + 
                  Power(s,2)*t2*(-42 + 21*t2 - 26*Power(t2,2)) + 
                  2*s*(-7 + 43*t2 + 14*Power(t2,2) - 73*Power(t2,3) + 
                     56*Power(t2,4))))) + 
         Power(s2,3)*(-4 + 11*(-1 + s - s1)*Power(t1,5) + 
            (16 - 12*s + 48*s1)*t2 + 
            (41 + 24*Power(s,2) - 22*s1 - 60*Power(s1,2) + 
               2*s*(-23 + 6*s1))*Power(t2,2) + 
            (207 - 116*Power(s,2) + 16*Power(s,3) - 185*s1 + 
               76*Power(s1,2) + 16*Power(s1,3) + s*(-25 + 28*s1))*
             Power(t2,3) - (107 + 28*Power(s,3) + 
               Power(s,2)*(168 - 60*s1) + 100*s1 - 90*Power(s1,2) + 
               26*Power(s1,3) - 4*s*(101 - 31*s1 + 2*Power(s1,2)))*
             Power(t2,4) - (140 + 26*Power(s,3) - 42*s1 + 
               6*Power(s1,2) + Power(s1,3) + 
               2*Power(s,2)*(-33 + 8*s1) + 
               2*s*(-76 - 2*s1 + Power(s1,2)))*Power(t2,5) + 
            (-64 + 126*Power(s,2) + 119*s1 - 10*Power(s1,2) + 
               s*(-172 + 27*s1))*Power(t2,6) + 
            (41 - 41*s - 17*s1)*Power(t2,7) + 
            Power(t1,4)*(178 + 3*Power(s,3) - 3*Power(s1,3) + 321*t2 + 
               113*Power(t2,2) + Power(s1,2)*(7 + 34*t2) + 
               Power(s,2)*(47 - 9*s1 + 34*t2) + 
               s1*(-43 + 105*t2 + 78*Power(t2,2)) + 
               s*(-193 + 9*Power(s1,2) - 301*t2 - 78*Power(t2,2) - 
                  2*s1*(27 + 34*t2))) + 
            Power(t1,3)*(196 - 181*t2 - 1275*Power(t2,2) - 
               356*Power(t2,3) - 55*Power(t2,4) + 
               Power(s,3)*(15 + 29*t2 - 3*Power(t2,2)) + 
               Power(s1,3)*(41 + 55*t2 + 3*Power(t2,2)) - 
               Power(s1,2)*(30 - 19*t2 + 122*Power(t2,2) + 
                  46*Power(t2,3)) + 
               s1*(-123 + 105*t2 + 304*Power(t2,2) - 170*Power(t2,3) - 
                  13*Power(t2,4)) - 
               Power(s,2)*(126 + 389*t2 + 350*Power(t2,2) + 
                  46*Power(t2,3) + s1*(-35 + 3*t2 - 9*Power(t2,2))) + 
               s*(43 + 783*t2 + 1590*Power(t2,2) + 454*Power(t2,3) + 
                  13*Power(t2,4) - 
                  Power(s1,2)*(91 + 81*t2 + 9*Power(t2,2)) + 
                  s1*(4 + 202*t2 + 472*Power(t2,2) + 92*Power(t2,3)))) - 
            Power(t1,2)*(54 + 293*t2 + 1636*Power(t2,2) - 
               654*Power(t2,3) - 560*Power(t2,4) - 87*Power(t2,5) - 
               5*Power(t2,6) + 
               Power(s1,3)*(-6 + 103*t2 + 119*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(s,3)*(6 + 33*t2 + 95*Power(t2,2) + 
                  45*Power(t2,3)) - 
               Power(s1,2)*(195 + 227*t2 + 71*Power(t2,2) - 
                  35*Power(t2,3) + 65*Power(t2,4)) + 
               s1*(81 + 193*t2 - 1067*Power(t2,2) + 96*Power(t2,3) + 
                  49*Power(t2,4) - 31*Power(t2,5)) - 
               Power(s,2)*(-9 - 197*t2 + 759*Power(t2,2) + 
                  893*Power(t2,3) + 153*Power(t2,4) + 
                  s1*(2 + 67*t2 + 17*Power(t2,2) + 69*Power(t2,3))) + 
               s*(-141 - 725*t2 + 341*Power(t2,2) + 1884*Power(t2,3) + 
                  887*Power(t2,4) + 75*Power(t2,5) + 
                  Power(s1,2)*
                   (114 - 101*t2 - 197*Power(t2,2) + 3*Power(t2,3)) + 
                  s1*(50 + 214*t2 - 216*Power(t2,2) + 394*Power(t2,3) + 
                     218*Power(t2,4)))) + 
            t1*(-17 - 215*t2 + 301*Power(t2,2) + 591*Power(t2,3) + 
               788*Power(t2,4) - 401*Power(t2,5) - 49*Power(t2,6) - 
               5*Power(t2,7) + 
               Power(s1,3)*t2*
                (-52 + 90*t2 + 37*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s,3)*t2*
                (-8 + 50*t2 + 41*Power(t2,2) + 78*Power(t2,3)) + 
               Power(s1,2)*(84 - 136*t2 - 447*Power(t2,2) - 
                  37*Power(t2,3) - 15*Power(t2,4) + 6*Power(t2,5)) + 
               s1*(34 + 383*t2 + 249*Power(t2,2) + 139*Power(t2,3) - 
                  828*Power(t2,4) + 91*Power(t2,5) - 5*Power(t2,6)) + 
               Power(s,2)*t2*
                (128 + 243*t2 + 161*Power(t2,2) - 747*Power(t2,3) - 
                  228*Power(t2,4) + 
                  s1*(12 - 134*t2 + 37*Power(t2,2) - 74*Power(t2,3))) + 
               s*(-2 + 13*t2 - 809*Power(t2,2) - 1155*Power(t2,3) + 
                  640*Power(t2,4) + 661*Power(t2,5) + 104*Power(t2,6) - 
                  Power(s1,2)*t2*
                   (48 - 62*t2 + 11*Power(t2,2) + 32*Power(t2,3)) + 
                  2*s1*(6 - 32*t2 + 161*Power(t2,2) - 113*Power(t2,3) - 
                     29*Power(t2,4) + 54*Power(t2,5))))) + 
         Power(s2,2)*(-(t2*(16 - 4*(-11 + 6*s + 6*s1)*t2 + 
                 (-35 - 60*Power(s,2) - 46*s1 + 24*Power(s1,2) + 
                    2*s*(73 + 6*s1))*Power(t2,2) - 
                 (153 - 124*Power(s,2) + 24*Power(s,3) - 103*s1 + 
                    22*Power(s1,2) + 4*Power(s1,3) + s*(-57 + 64*s1))*
                  Power(t2,3) + 
                 (67 + 12*Power(s,3) + Power(s,2)*(162 - 40*s1) + 
                    40*s1 - 25*Power(s1,2) + 6*Power(s1,3) - 
                    2*s*(146 - 33*s1 + Power(s1,2)))*Power(t2,4) + 
                 (45 + 24*Power(s,3) + 9*Power(s,2)*(-6 + s1) - 
                    16*s1 + Power(s1,2) + s*(-45 + 4*s1 + Power(s1,2))\
)*Power(t2,5) + (6 - 50*Power(s,2) + s*(64 - 5*s1) - 25*s1 + 
                    2*Power(s1,2))*Power(t2,6) + 
                 2*(-3 + 3*s + s1)*Power(t2,7))) - 
            Power(t1,5)*(116 + 16*Power(s,2) + 16*Power(s1,2) + 97*t2 + 
               s1*(34 + 87*t2) - s*(110 + 32*s1 + 87*t2)) + 
            t1*(-12 + (-137 + 134*s - 72*Power(s,2))*t2 + 
               (-675 + 165*s + 292*Power(s,2) - 32*Power(s,3))*
                Power(t2,2) + 
               (213 - 1107*s + 635*Power(s,2) + 42*Power(s,3))*
                Power(t2,3) + 
               (447 - 961*s + 201*Power(s,2) + 67*Power(s,3))*
                Power(t2,4) + 
               (282 + 426*s - 517*Power(s,2) + 72*Power(s,3))*
                Power(t2,5) + 
               (-131 + 219*s - 107*Power(s,2))*Power(t2,6) + 
               7*(-1 + 3*s)*Power(t2,7) + 
               Power(s1,3)*Power(t2,2)*
                (-32 + 58*t2 + 8*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s1,2)*t2*
                (132 - 4*(53 + 6*s)*t2 - (251 + 22*s)*Power(t2,2) + 
                  (9 + 35*s)*Power(t2,3) - 3*(5 + s)*Power(t2,4) + 
                  Power(t2,5)) + 
               s1*(-84 + (2 + 36*s)*t2 + 
                  (601 - 36*s - 24*Power(s,2))*Power(t2,2) + 
                  (139 + 394*s - 146*Power(s,2))*Power(t2,3) + 
                  3*(53 - 110*s + 19*Power(s,2))*Power(t2,4) + 
                  (-376 + 38*s - 76*Power(s,2))*Power(t2,5) + 
                  4*(4 + 13*s)*Power(t2,6) - 4*Power(t2,7))) + 
            Power(t1,4)*(-24 + 888*t2 + 451*Power(t2,2) + 
               65*Power(t2,3) + Power(s,3)*(-6 + 7*t2) - 
               Power(s1,3)*(22 + 7*t2) + 
               Power(s1,2)*(4 + 101*t2 + 134*Power(t2,2)) + 
               s1*(-22 - 267*t2 + 187*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s,2)*(108 + 357*t2 + 134*Power(t2,2) - 
                  s1*(10 + 21*t2)) + 
               s*(-178 - 1225*t2 - 685*Power(t2,2) - 42*Power(t2,3) + 
                  Power(s1,2)*(38 + 21*t2) - 
                  2*s1*(32 + 229*t2 + 134*Power(t2,2)))) + 
            Power(t1,3)*(96 + 1378*t2 + 163*Power(t2,2) - 
               837*Power(t2,3) - 158*Power(t2,4) - 13*Power(t2,5) + 
               Power(s1,3)*(54 + 129*t2 + 69*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(s,3)*(10 + 47*t2 + 121*Power(t2,2) + 
                  11*Power(t2,3)) - 
               Power(s1,2)*(176 + 72*t2 - 33*Power(t2,2) + 
                  150*Power(t2,3) + 30*Power(t2,4)) - 
               s1*(-134 + 583*t2 + 87*Power(t2,2) - 38*Power(t2,3) + 
                  96*Power(t2,4) + Power(t2,5)) - 
               Power(s,2)*(-136 + 568*t2 + 1231*Power(t2,2) + 
                  454*Power(t2,3) + 30*Power(t2,4) + 
                  s1*(142 - 91*t2 + 173*Power(t2,2) + 33*Power(t2,3))) \
+ s*(-334 - 105*t2 + 1429*Power(t2,2) + 1380*Power(t2,3) + 
                  260*Power(t2,4) + Power(t2,5) + 
                  Power(s1,2)*
                   (30 - 267*t2 - 17*Power(t2,2) + 33*Power(t2,3)) + 
                  s1*(232 - 216*t2 + 466*Power(t2,2) + 
                     604*Power(t2,3) + 60*Power(t2,4)))) + 
            Power(t1,2)*(120 - 224*t2 - 655*Power(t2,2) - 
               1500*Power(t2,3) + 452*Power(t2,4) + 156*Power(t2,5) + 
               18*Power(t2,6) + 
               Power(s1,3)*(40 - 54*t2 - 81*Power(t2,2) - 
                  83*Power(t2,3) + Power(t2,4)) - 
               Power(s,3)*(-4 + 2*t2 + 53*Power(t2,2) + 
                  187*Power(t2,3) + 61*Power(t2,4)) + 
               Power(s1,2)*(-28 + 591*t2 + 319*Power(t2,2) + 
                  17*Power(t2,3) + 9*Power(t2,4) + 27*Power(t2,5)) + 
               Power(s,2)*(-60 + 13*(-17 + 6*s1)*t2 + 
                  (-753 + 41*s1)*Power(t2,2) + 
                  (1005 + 101*s1)*Power(t2,3) + 
                  (787 + 123*s1)*Power(t2,4) + 83*Power(t2,5)) + 
               s1*(-282 - 329*t2 - 765*Power(t2,2) + 1079*Power(t2,3) + 
                  14*Power(t2,4) + 41*Power(t2,5) + 7*Power(t2,6)) - 
               s*(14 - 869*t2 - 2459*Power(t2,2) + 229*Power(t2,3) + 
                  1230*Power(t2,4) + 363*Power(t2,5) + 18*Power(t2,6) + 
                  Power(s1,2)*
                   (-84 + 166*t2 + 35*Power(t2,2) - 169*Power(t2,3) + 
                     63*Power(t2,4)) + 
                  2*s1*(-84 + 109*t2 - 185*Power(t2,2) - 
                     114*Power(t2,3) + 185*Power(t2,4) + 55*Power(t2,5)))\
)) + s2*(-32*(-1 + s - s1)*Power(t1,6) + 
            Power(t2,2)*(1 + (-1 + s)*t2)*
             (16 + 4*(-7 + 8*s - 3*s1)*t2 + 
               (-17 - 38*s + 16*Power(s,2) + 16*s1)*Power(t2,2) + 
               2*(17 + Power(s,2) + 5*s*(-6 + s1))*Power(t2,3) - 
               (3 + 11*Power(s,2) + 2*s*(-8 + s1) + 4*s1)*Power(t2,4) + 
               (-2 + 8*s)*Power(t2,5)) + 
            t1*(28 - 20*(-5 + 3*s + 3*s1)*t2 - 
               (35 + 132*Power(s,2) + 38*s1 - 72*Power(s1,2) + 
                  s*(-310 + 36*s1))*Power(t2,2) + 
               (-381 - 52*Power(s,3) + Power(s,2)*(212 - 48*s1) + 
                  197*s1 - 100*Power(s1,2) - 8*Power(s1,3) + 
                  s*(295 + 8*s1 + 12*Power(s1,2)))*Power(t2,3) - 
               (-147 + 18*Power(s,3) + 37*s1 + 18*Power(s1,2) - 
                  12*Power(s1,3) + Power(s,2)*(-513 + 58*s1) + 
                  s*(647 - 172*s1 + 30*Power(s1,2)))*Power(t2,4) + 
               (109 + 43*Power(s,3) + 69*s1 + 10*Power(s1,2) + 
                  Power(s,2)*(57 + 47*s1) + 
                  s*(-249 - 170*s1 + 14*Power(s1,2)))*Power(t2,5) + 
               (16 + 33*Power(s,3) - 63*s1 - 4*Power(s1,2) - 
                  Power(s,2)*(164 + 35*s1) + 
                  s*(145 + 42*s1 + 2*Power(s1,2)))*Power(t2,6) - 
               2*(8 + 10*Power(s,2) - 5*s*(3 + s1))*Power(t2,7)) - 
            Power(t1,5)*(236 + 320*t2 + 25*Power(t2,2) + 
               8*Power(s1,2)*(4 + 17*t2) + 8*Power(s,2)*(16 + 17*t2) + 
               s1*(-92 + 68*t2 + 45*Power(t2,2)) - 
               s*(356 + 476*t2 + 45*Power(t2,2) + 16*s1*(10 + 17*t2))) + 
            Power(t1,4)*(-424 - 652*t2 + 582*Power(t2,2) + 
               135*Power(t2,3) + 11*Power(t2,4) + 
               Power(s1,3)*(-76 - 84*t2 + 27*Power(t2,2)) - 
               Power(s,3)*(36 + 116*t2 + 27*Power(t2,2)) + 
               Power(s1,2)*(4 - 16*t2 + 85*Power(t2,2) + 
                  70*Power(t2,3)) + 
               s1*(100 + 56*t2 + 3*Power(t2,2) + 99*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s,2)*(228 + 672*t2 + 461*Power(t2,2) + 
                  70*Power(t2,3) + s1*(-52 + 148*t2 + 81*Power(t2,2))) - 
               s*(-108 + 184*t2 + 935*Power(t2,2) + 295*Power(t2,3) + 
                  2*Power(t2,4) + 
                  Power(s1,2)*(-164 - 52*t2 + 81*Power(t2,2)) + 
                  2*s1*(-36 + 56*t2 + 273*Power(t2,2) + 70*Power(t2,3)))) \
+ Power(t1,3)*(48 + 220*t2 + 1244*Power(t2,2) - 171*Power(t2,3) - 
               173*Power(t2,4) - 21*Power(t2,5) + 
               Power(s1,3)*(-20 + 80*t2 + 67*Power(t2,2) + 
                  25*Power(t2,3) - 11*Power(t2,4)) + 
               Power(s,3)*(-12 + 80*t2 + 133*Power(t2,2) + 
                  131*Power(t2,3) + 11*Power(t2,4)) - 
               Power(s1,2)*(264 + 532*t2 - 122*Power(t2,2) + 
                  27*Power(t2,3) + 50*Power(t2,4) + 4*Power(t2,5)) - 
               s1*(-196 - 832*t2 + 565*Power(t2,2) + 29*Power(t2,3) + 
                  87*Power(t2,4) + 14*Power(t2,5)) - 
               Power(s,2)*(-120 - 972*t2 + 214*Power(t2,2) + 
                  867*Power(t2,3) + 202*Power(t2,4) + 4*Power(t2,5) + 
                  s1*(-100 + 192*t2 - 49*Power(t2,2) + 
                     237*Power(t2,3) + 33*Power(t2,4))) + 
               s*(-372 - 2160*t2 - 571*Power(t2,2) + 909*Power(t2,3) + 
                  434*Power(t2,4) + 36*Power(t2,5) + 
                  Power(s1,2)*
                   (92 + 96*t2 - 249*Power(t2,2) + 81*Power(t2,3) + 
                     33*Power(t2,4)) + 
                  2*s1*(-64 - 20*t2 - 342*Power(t2,2) + 
                     171*Power(t2,3) + 126*Power(t2,4) + 4*Power(t2,5)))) \
+ Power(t1,2)*(104 + 780*t2 - 102*Power(t2,2) - 483*Power(t2,3) - 
               392*Power(t2,4) + 149*Power(t2,5) + 14*Power(t2,6) + 
               Power(s1,3)*t2*
                (4 - 10*t2 - 29*Power(t2,2) - 18*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s,3)*t2*(-4 + 6*t2 + 19*Power(t2,2) + 
                  145*Power(t2,3) + 33*Power(t2,4)) + 
               Power(s1,2)*(-108 + 220*t2 + 217*Power(t2,2) + 
                  81*Power(t2,3) - 31*Power(t2,4) + 21*Power(t2,5) + 
                  2*Power(t2,6)) + 
               s1*(92 - 728*t2 + 165*Power(t2,2) - 479*Power(t2,3) + 
                  362*Power(t2,4) + 11*Power(t2,5) + 16*Power(t2,6)) + 
               Power(s,2)*(36 + 20*(-13 + 3*s1)*t2 + 
                  (-859 + 178*s1)*Power(t2,2) - 
                  (887 + 111*s1)*Power(t2,3) + 
                  (535 + 89*s1)*Power(t2,4) + 
                  (277 + 70*s1)*Power(t2,5) + 16*Power(t2,6)) - 
               s*(124 + 360*t2 - 927*Power(t2,2) - 1627*Power(t2,3) + 
                  247*Power(t2,4) + 347*Power(t2,5) + 50*Power(t2,6) + 
                  Power(s1,2)*t2*
                   (-60 + 18*t2 + 65*Power(t2,2) - 74*Power(t2,3) + 
                     41*Power(t2,4)) + 
                  s1*(120 + 24*t2 + 598*Power(t2,2) - 718*Power(t2,3) - 
                     47*Power(t2,4) + 154*Power(t2,5) + 18*Power(t2,6)))))\
)*R2(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*(1 - s2 + t1 - t2)*
       Power(t1 - s2*t2,2)*Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - 
         Power(t2,2),2)) - (8*(16*(-1 + s - s1)*Power(t1,9) - 
         Power(s2,12)*Power(t2,2)*(s1 + t2) - 
         Power(t2,5)*Power(1 + t2,2)*Power(1 + (-1 + s)*t2,3)*
          (2 - 3*t2 + Power(t2,2)) + 
         t1*Power(t2,3)*(1 + (-1 + s)*t2)*
          (20 + 10*(-3 + 4*s)*t2 + 
            (-38 + 20*Power(s,2) - s1 + 3*s*(-7 + 2*s1))*Power(t2,2) + 
            (62 + 15*Power(s,2) + 2*s*(-50 + s1) - 3*s1)*Power(t2,3) + 
            (19 - 28*Power(s,2) - 13*s*(-2 + s1) + 8*s1)*Power(t2,4) + 
            (-40 - 22*Power(s,2) + s*(75 + s1))*Power(t2,5) + 
            (5 + 8*Power(s,2) - 7*s1 + s*(-11 + 7*s1))*Power(t2,6) + 
            (2 + 4*Power(s,2) + 3*s1 - 3*s*(3 + s1))*Power(t2,7)) + 
         Power(t1,2)*t2*(-60 - 30*(-5 + 6*s)*t2 - 
            10*(-8 + 18*Power(s,2) - s1 + s*(-28 + 6*s1))*Power(t2,2) - 
            10*(37 + 6*Power(s,3) - 2*s1 - s*(54 + 5*s1) + 
               Power(s,2)*(-5 + 6*s1))*Power(t2,3) - 
            (6 + 80*Power(s,3) + 114*s1 - 2*Power(s1,2) + 
               3*Power(s,2)*(-197 + 8*s1) + 
               s*(610 - 199*s1 + 4*Power(s1,2)))*Power(t2,4) + 
            (404 + 67*Power(s,3) + 54*s1 - 5*Power(s1,2) + 
               2*Power(s,2)*(69 + 61*s1) + 
               s*(-760 - 184*s1 + 7*Power(s1,2)))*Power(t2,5) + 
            (-171 + 148*Power(s,3) + 133*s1 + 2*Power(s1,2) + 
               4*Power(s,2)*(-155 + 6*s1) + 
               s*(587 - 165*s1 - 2*Power(s1,2)))*Power(t2,6) + 
            (-43 + 7*Power(s,3) - 127*s1 + 6*Power(s1,2) - 
               4*Power(s,2)*(40 + 21*s1) + 
               s*(253 + 206*s1 - 6*Power(s1,2)))*Power(t2,7) + 
            (13 - 41*Power(s,3) + 19*s1 - 8*Power(s1,2) + 
               Power(s,2)*(125 + 18*s1) + 
               s*(-88 - 29*s1 + 8*Power(s1,2)))*Power(t2,8) + 
            (3 - 6*Power(s,3) + 5*s1 + 3*Power(s1,2) + 
               Power(s,2)*(23 + 9*s1) - s*(22 + 17*s1 + 3*Power(s1,2)))*
             Power(t2,9)) - 2*Power(t1,8)*
          (56 + 8*Power(s,3) - 8*Power(s1,3) - 94*t2 - 11*Power(t2,2) - 
            8*Power(s1,2)*(5 + 4*t2) - 8*Power(s,2)*(11 + 3*s1 + 4*t2) + 
            s1*(84 - 16*t2 - 17*Power(t2,2)) + 
            s*(20 + 24*Power(s1,2) + 100*t2 + 17*Power(t2,2) + 
               64*s1*(2 + t2))) + 
         Power(s2,11)*t2*(t2*
             (1 + 7*t2 - 2*s*t2 - 6*Power(t2,2) + t1*(3 - s + 3*t2)) + 
            s1*((s - 10*t2)*t2 + t1*(2 + 4*t2))) + 
         2*Power(t1,7)*(232 + 324*t2 - 116*Power(t2,2) - 
            85*Power(t2,3) - 5*Power(t2,4) + 
            Power(s1,3)*(28 - 4*t2 - 23*Power(t2,2)) + 
            Power(s,3)*(12 + 48*t2 + 23*Power(t2,2)) + 
            Power(s1,2)*(56 + 22*t2 - 77*Power(t2,2) - 37*Power(t2,3)) + 
            s1*(-180 + 204*t2 + Power(t2,2) - 35*Power(t2,3) - 
               5*Power(t2,4)) - 
            Power(s,2)*(-184 + 202*t2 + 253*Power(t2,2) + 
               37*Power(t2,3) + s1*(-36 + 100*t2 + 69*Power(t2,2))) + 
            s*(-356 - 112*t2 + 377*Power(t2,2) + 120*Power(t2,3) + 
               5*Power(t2,4) + 
               Power(s1,2)*(-76 + 56*t2 + 69*Power(t2,2)) + 
               s1*(-272 + 36*t2 + 330*Power(t2,2) + 74*Power(t2,3)))) + 
         Power(t1,6)*(512 - 500*t2 - 1134*Power(t2,2) - 
            228*Power(t2,3) + 242*Power(t2,4) + 53*Power(t2,5) + 
            Power(t2,6) + 2*Power(s1,3)*
             (-44 + 32*t2 - 43*Power(t2,2) + 9*Power(t2,3) + 
               20*Power(t2,4)) - 
            2*Power(s,3)*(28 - 52*t2 + 43*Power(t2,2) + 
               106*Power(t2,3) + 20*Power(t2,4)) + 
            Power(s1,2)*(560 + 16*t2 - 564*Power(t2,2) + 
               96*Power(t2,3) + 128*Power(t2,4) + 21*Power(t2,5)) + 
            s1*(-288 + 312*t2 - 336*Power(t2,2) - 92*Power(t2,3) + 
               138*Power(t2,4) + 22*Power(t2,5) + Power(t2,6)) + 
            Power(s,2)*(208 - 1888*t2 - 588*Power(t2,2) + 
               1164*Power(t2,3) + 406*Power(t2,4) + 21*Power(t2,5) + 
               2*s1*(108 - 72*t2 - 85*Power(t2,2) + 221*Power(t2,3) + 
                  60*Power(t2,4))) - 
            s*(128 - 2936*t2 - 1916*Power(t2,2) + 1034*Power(t2,3) + 
               726*Power(t2,4) + 74*Power(t2,5) + Power(t2,6) + 
               2*Power(s1,2)*
                (-28 + 44*t2 - 171*Power(t2,2) + 124*Power(t2,3) + 
                  60*Power(t2,4)) + 
               2*s1*(672 - 328*t2 - 952*Power(t2,2) + 382*Power(t2,3) + 
                  267*Power(t2,4) + 21*Power(t2,5)))) + 
         Power(t1,5)*(-656 + 144*t2 - 536*Power(t2,2) + 
            892*Power(t2,3) + 502*Power(t2,4) - 89*Power(t2,5) - 
            73*Power(t2,6) - 5*Power(t2,7) + 
            Power(s1,3)*(-56 + 88*t2 + 10*Power(t2,2) - 
               44*Power(t2,3) + 72*Power(t2,4) - 31*Power(t2,5) - 
               11*Power(t2,6)) + 
            Power(s,3)*(-24 + 176*t2 - 278*Power(t2,2) - 
               270*Power(t2,3) + 286*Power(t2,4) + 162*Power(t2,5) + 
               11*Power(t2,6)) + 
            Power(s1,2)*(144 + 216*t2 - 1240*Power(t2,2) + 
               628*Power(t2,3) + 254*Power(t2,4) - 136*Power(t2,5) - 
               33*Power(t2,6) - 2*Power(t2,7)) - 
            2*s1*(-472 + 852*t2 - 264*Power(t2,2) - 267*Power(t2,3) - 
               100*Power(t2,4) + 70*Power(t2,5) + 26*Power(t2,6) + 
               Power(t2,7)) - 
            Power(s,2)*(368 + 1208*t2 - 2464*Power(t2,2) - 
               2980*Power(t2,3) + 764*Power(t2,4) + 871*Power(t2,5) + 
               110*Power(t2,6) + 2*Power(t2,7) + 
               s1*(-312 + 520*t2 + 490*Power(t2,2) - 848*Power(t2,3) + 
                  160*Power(t2,4) + 355*Power(t2,5) + 33*Power(t2,6))) + 
            s*(1520 + 1784*t2 - 2904*Power(t2,2) - 4562*Power(t2,3) - 
               112*Power(t2,4) + 1026*Power(t2,5) + 215*Power(t2,6) + 
               7*Power(t2,7) + 
               Power(s1,2)*(280 - 640*t2 + 598*Power(t2,2) - 
                  294*Power(t2,3) - 198*Power(t2,4) + 224*Power(t2,5) + 
                  33*Power(t2,6)) + 
               s1*(-1888 + 2976*t2 + 1960*Power(t2,2) - 
                  3000*Power(t2,3) - 558*Power(t2,4) + 733*Power(t2,5) + 
                  143*Power(t2,6) + 4*Power(t2,7)))) + 
         Power(t1,3)*(Power(s,3)*Power(t2,2)*
             (40 + 170*t2 + 30*Power(t2,2) - 360*Power(t2,3) - 
               209*Power(t2,4) + 122*Power(t2,5) + 65*Power(t2,6) + 
               4*Power(t2,7)) + 
            Power(s,2)*t2*(108 + 90*(1 + 2*s1)*t2 + 
               2*(-477 + 50*s1)*Power(t2,2) - 
               8*(139 + 39*s1)*Power(t2,3) + 
               (1355 - 299*s1)*Power(t2,4) + 
               2*(705 + 163*s1)*Power(t2,5) + 
               (-250 + 48*s1)*Power(t2,6) - 
               3*(83 + 30*s1)*Power(t2,7) - 3*(7 + 3*s1)*Power(t2,8)) + 
            s*(72 + 24*(-5 + 9*s1)*t2 - 2*(493 + 118*s1)*Power(t2,2) + 
               (458 - 700*s1 + 42*Power(s1,2))*Power(t2,3) + 
               (2846 + 302*s1 - 54*Power(s1,2))*Power(t2,4) + 
               (-807 + 1253*s1 - 10*Power(s1,2))*Power(t2,5) + 
               (-1775 - 783*s1 + 77*Power(s1,2))*Power(t2,6) - 
               (19 + 220*s1 + 83*Power(s1,2))*Power(t2,7) + 
               (221 + 159*s1 + 23*Power(s1,2))*Power(t2,8) + 
               (28 + 20*s1 + 6*Power(s1,2))*Power(t2,9)) - 
            (-1 + t2)*(-48 - 264*t2 + 216*Power(t2,2) + 
               738*Power(t2,3) - 472*Power(t2,4) - 219*Power(t2,5) - 
               4*Power(t2,6) + 29*Power(t2,7) + 5*Power(t2,8) + 
               Power(s1,3)*Power(t2,2)*
                (4 - 4*t2 - 4*Power(t2,2) + Power(t2,3) + Power(t2,4) - 
                  Power(t2,5) + Power(t2,6)) + 
               2*Power(s1,2)*t2*
                (-18 + 15*t2 + 2*Power(t2,2) + 17*Power(t2,3) + 
                  11*Power(t2,4) - 36*Power(t2,5) + 15*Power(t2,6) + 
                  2*Power(t2,7)) + 
               s1*(72 - 72*t2 - 154*Power(t2,2) + 254*Power(t2,3) + 
                  316*Power(t2,4) - 543*Power(t2,5) + 12*Power(t2,6) + 
                  48*Power(t2,7) + 6*Power(t2,8)))) - 
         Power(t1,4)*(144 + 716*t2 - 1506*Power(t2,2) + 
            156*Power(t2,3) + 128*Power(t2,4) + 409*Power(t2,5) + 
            12*Power(t2,6) - 46*Power(t2,7) - 7*Power(t2,8) - 
            Power(s1,2)*t2*(-48 - 124*t2 + 16*Power(t2,2) + 
               540*Power(t2,3) - 435*Power(t2,4) + 26*Power(t2,5) + 
               42*Power(t2,6) + 3*Power(t2,7)) + 
            s1*(-328 + 440*t2 + 914*Power(t2,2) - 2218*Power(t2,3) + 
               954*Power(t2,4) + 412*Power(t2,5) - 68*Power(t2,6) - 
               58*Power(t2,7) - 5*Power(t2,8)) - 
            Power(s1,3)*(8 - 16*t2 + 38*Power(t2,2) - 30*Power(t2,3) - 
               12*Power(t2,4) + 18*Power(t2,5) - 20*Power(t2,6) + 
               10*Power(t2,7) + Power(t2,8)) + 
            Power(s,3)*(-8 + 56*t2 + 186*Power(t2,2) - 
               168*Power(t2,3) - 556*Power(t2,4) + 30*Power(t2,5) + 
               232*Power(t2,6) + 44*Power(t2,7) + Power(t2,8)) - 
            Power(s,2)*(2*t2*
                (160 + 886*t2 - 164*Power(t2,2) - 1766*Power(t2,3) - 
                  396*Power(t2,4) + 425*Power(t2,5) + 
                  117*Power(t2,6) + 5*Power(t2,7)) + 
               s1*(-72 - 288*t2 + 122*Power(t2,2) + 914*Power(t2,3) - 
                  344*Power(t2,4) - 482*Power(t2,5) + 270*Power(t2,6) + 
                  98*Power(t2,7) + 3*Power(t2,8))) + 
            s*(-296 - 432*t2 + 3878*Power(t2,2) + 886*Power(t2,3) - 
               4118*Power(t2,4) - 1740*Power(t2,5) + 616*Power(t2,6) + 
               300*Power(t2,7) + 20*Power(t2,8) + 
               Power(s1,2)*(72 - 40*t2 + 38*Power(t2,2) - 
                  196*Power(t2,3) + 320*Power(t2,4) - 274*Power(t2,5) + 
                  18*Power(t2,6) + 64*Power(t2,7) + 3*Power(t2,8)) + 
               s1*(64 - 1008*t2 - 896*Power(t2,2) + 3608*Power(t2,3) - 
                  548*Power(t2,4) - 1663*Power(t2,5) + 335*Power(t2,6) + 
                  214*Power(t2,7) + 13*Power(t2,8)))) - 
         Power(s2,10)*(Power(s1,3) - 
            Power(s1,2)*t2*(-1 + s + t1 + 2*t1*t2) + 
            s1*(Power(t1,2)*(1 + 8*t2 + 6*Power(t2,2)) + 
               Power(t2,2)*(-8 + s*(3 - 8*t2) - 6*t2 + 
                  44*Power(t2,2)) + 
               t1*t2*(s*(2 + 5*t2) - 2*t2*(16 + 15*t2))) + 
            t2*(Power(t1,2)*(3 - 2*s + 9*t2 - 3*s*t2 + 3*Power(t2,2)) - 
               t1*(-2 + (-25 + 7*s + Power(s,2))*t2 + 
                  (2 + 3*s)*Power(t2,2) + 14*Power(t2,3)) + 
               t2*(-3*Power(s,2)*t2 + 4*Power(t2,2)*(-13 + 3*t2) + 
                  s*(1 + 3*t2 + 20*Power(t2,2))))) + 
         Power(s2,9)*(-(Power(t1,2)*
               (-1 + (-29 + 8*s + 2*Power(s,2))*t2 + 
                 4*(-9 + 2*s + Power(s,2))*Power(t2,2) + 
                 (31 + 2*s)*Power(t2,3) + 10*Power(t2,4))) + 
            Power(s1,3)*(3 - 9*t2 - 2*Power(t2,2) + 
               2*t1*(1 + t2 + Power(t2,2))) + 
            Power(t1,3)*(1 + 9*t2 + 9*Power(t2,2) + Power(t2,3) - 
               s*(1 + 6*t2 + 3*Power(t2,2))) + 
            t1*t2*(-(Power(s,2)*t2*(11 + 8*t2)) + 
               2*t2*(-1 - 111*t2 - 43*Power(t2,2) + 8*Power(t2,3)) + 
               s*(2 + 15*t2 + 104*Power(t2,2) + 64*Power(t2,3))) - 
            Power(t2,2)*(8 + 17*t2 + 45*Power(t2,2) - 165*Power(t2,3) + 
               Power(s,2)*(2 + 2*t2 - 27*Power(t2,2)) + 
               s*(-8 + 3*t2 + 30*Power(t2,2) + 89*Power(t2,3))) - 
            Power(s1,2)*(-3 - 7*t2 + (1 - 12*s)*Power(t2,2) + 
               Power(t1,2)*(1 + 6*t2 + 6*Power(t2,2)) + 
               t1*(-1 - 9*Power(t2,3) + 4*s*(1 + t2 + Power(t2,2)))) + 
            s1*(Power(s,2)*(2*t1 - t2)*t2*(1 + t2) + 
               4*Power(t1,3)*(1 + 3*t2 + Power(t2,2)) - 
               2*Power(t1,2)*t2*(17 + 50*t2 + 14*Power(t2,2)) + 
               t1*t2*(-18 - 39*t2 + 172*Power(t2,2) + 95*Power(t2,3)) + 
               t2*(2 - 2*t2 + 59*Power(t2,2) + 51*Power(t2,3) - 
                  112*Power(t2,4)) + 
               s*(Power(t1,2)*(1 + 12*t2 + 11*Power(t2,2)) + 
                  t1*(t2 - 15*Power(t2,2) - 22*Power(t2,3)) + 
                  t2*(1 - 3*t2 - 20*Power(t2,2) + 29*Power(t2,3))))) + 
         Power(s2,8)*(-11*Power(t1,3) - 3*Power(t1,4) - t2 + 17*t1*t2 + 
            4*Power(t1,2)*t2 - 54*Power(t1,3)*t2 - 9*Power(t1,4)*t2 + 
            2*Power(t2,2) + 70*t1*Power(t2,2) + 
            364*Power(t1,2)*Power(t2,2) + 5*Power(t1,3)*Power(t2,2) - 
            3*Power(t1,4)*Power(t2,2) - 67*Power(t2,3) + 
            271*t1*Power(t2,3) + 501*Power(t1,2)*Power(t2,3) + 
            30*Power(t1,3)*Power(t2,3) - 96*Power(t2,4) - 
            722*t1*Power(t2,4) + 68*Power(t1,2)*Power(t2,4) + 
            2*Power(t1,3)*Power(t2,4) - 211*Power(t2,5) - 
            395*t1*Power(t2,5) - Power(t1,2)*Power(t2,5) + 
            290*Power(t2,6) - 30*t1*Power(t2,6) + 42*Power(t2,7) + 
            Power(s,3)*t2*(3*t1*(-1 + t2)*t2 - Power(t2,2)*(2 + t2) + 
               Power(t1,2)*(1 + 2*t2)) + 
            Power(s,2)*(Power(t1,3)*(1 + 9*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*t2*(9 + 28*t2 + 9*Power(t2,2)) + 
               Power(t2,3)*(-10 - 9*t2 + 109*Power(t2,2)) + 
               t1*t2*(5 + 14*t2 - 128*Power(t2,2) - 96*Power(t2,3))) - 
            Power(s1,3)*(2 - 19*t2 + 30*Power(t2,2) + 12*Power(t2,3) + 
               Power(t1,2)*(1 + 7*t2 + 6*Power(t2,2)) - 
               t1*(7 + 16*t2 + 16*Power(t2,2) + 11*Power(t2,3))) + 
            s*(Power(t1,4)*(3 + 6*t2 + Power(t2,2)) + 
               Power(t1,3)*(3 + 7*t2 + Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(1 + 23*t2 + 223*Power(t2,2) + 
                  294*Power(t2,3) + 90*Power(t2,4)) + 
               t1*t2*(-11 + 30*t2 + 124*Power(t2,2) + 
                  563*Power(t2,3) + 280*Power(t2,4)) - 
               t2*(2 + 28*t2 - 78*Power(t2,2) - 41*Power(t2,3) + 
                  124*Power(t2,4) + 231*Power(t2,5))) + 
            Power(s1,2)*(-9 - (-22 + s)*t2 + (41 - 8*s)*Power(t2,2) + 
               (17 + 53*s)*Power(t2,3) + Power(t2,4) + 
               2*Power(t1,3)*(2 + 6*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(t2 - 30*Power(t2,2) - 14*Power(t2,3) + 
                  s*(7 + 16*t2 + 14*Power(t2,2))) - 
               t1*(13 + 12*t2 + 13*Power(t2,2) + 26*Power(t2,3) - 
                  9*Power(t2,4) + 
                  s*(-12 + 50*t2 + 35*Power(t2,2) + 21*Power(t2,3)))) + 
            s1*(-3 - 2*(7 + 6*s)*t2 + 
               (4 + 14*s + Power(s,2))*Power(t2,2) - 
               (15 + 42*s + 2*Power(s,2))*Power(t2,3) + 
               (171 - 63*s - 8*Power(s,2))*Power(t2,4) + 
               3*(61 + 21*s)*Power(t2,5) - 182*Power(t2,6) - 
               Power(t1,4)*(6 + 8*t2 + Power(t2,2)) + 
               Power(t1,3)*(-(s*(7 + 24*t2 + 11*Power(t2,2))) + 
                  2*(6 + 55*t2 + 51*Power(t2,2) + 3*Power(t2,3))) + 
               Power(t1,2)*(10 + 64*t2 - 229*Power(t2,2) - 
                  400*Power(t2,3) - 40*Power(t2,4) - 
                  5*Power(s,2)*(1 + 2*t2 + 2*Power(t2,2)) + 
                  s*(2 + 2*t2 + 74*Power(t2,2) + 15*Power(t2,3))) + 
               t1*(-2 + 4*t2 - 222*Power(t2,2) - 347*Power(t2,3) + 
                  471*Power(t2,4) + 164*Power(t2,5) + 
                  Power(s,2)*t2*(3 + 25*t2 + 8*Power(t2,2)) + 
                  s*(5 + 42*t2 + 47*Power(t2,2) - 42*Power(t2,3) - 
                     13*Power(t2,4))))) + 
         Power(s2,7)*(1 + 7*t2 + 12*s*t2 - 3*Power(t2,2) + 
            25*s*Power(t2,2) + 13*Power(s,2)*Power(t2,2) + 
            56*Power(t2,3) - 108*s*Power(t2,3) - 
            12*Power(s,2)*Power(t2,3) - 212*Power(t2,4) + 
            348*s*Power(t2,4) - 36*Power(s,2)*Power(t2,4) - 
            14*Power(s,3)*Power(t2,4) - 218*Power(t2,5) + 
            259*s*Power(t2,5) - 9*Power(s,2)*Power(t2,5) - 
            8*Power(s,3)*Power(t2,5) - 448*Power(t2,6) - 
            287*s*Power(t2,6) + 259*Power(s,2)*Power(t2,6) + 
            305*Power(t2,7) - 385*s*Power(t2,7) + 84*Power(t2,8) + 
            Power(t1,5)*(3 - 3*s + 3*t2 - 2*s*t2) - 
            Power(t1,4)*(-22 - 27*t2 + 29*Power(t2,2) + 8*Power(t2,3) + 
               Power(s,2)*(5 + 10*t2 + 2*Power(t2,2)) + 
               2*s*(1 - 2*t2 + Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(Power(t1,3)*(1 + 9*t2 + 6*Power(t2,2)) - 
               10*t2*(1 - 5*t2 + 5*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(25 + 25*t2 + 55*Power(t2,2) + 
                  23*Power(t2,3)) + 
               t1*(-29 + 53*t2 + 66*Power(t2,2) + 46*Power(t2,3) + 
                  24*Power(t2,4))) - 
            Power(t1,3)*(2 + 270*t2 + 885*Power(t2,2) + 
               482*Power(t2,3) + 46*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*(2 + 6*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(1 + 39*t2 + 24*Power(t2,2) + 
                  8*Power(t2,3)) - 
               s*(11 + 216*t2 + 520*Power(t2,2) + 394*Power(t2,3) + 
                  61*Power(t2,4))) + 
            Power(t1,2)*(-9 - 91*t2 - 560*Power(t2,2) + 
               1121*Power(t2,3) + 1870*Power(t2,4) + 505*Power(t2,5) + 
               40*Power(t2,6) - Power(s,3)*t2*(-7 + Power(t2,2)) + 
               Power(s,2)*t2*
                (11 + 238*t2 + 434*Power(t2,2) + 147*Power(t2,3)) - 
               s*(-3 + 47*t2 + 194*Power(t2,2) + 1380*Power(t2,3) + 
                  1517*Power(t2,4) + 331*Power(t2,5))) + 
            t1*(1 - 4*t2 + 246*Power(t2,2) + 487*Power(t2,3) + 
               1455*Power(t2,4) - 1149*Power(t2,5) - 832*Power(t2,6) - 
               110*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(7 - 4*t2 + 25*Power(t2,2)) - 
               Power(s,2)*t2*
                (16 + 11*t2 - 115*Power(t2,2) + 590*Power(t2,3) + 
                  366*Power(t2,4)) + 
               s*(-1 + 24*t2 - 307*Power(t2,2) - 260*Power(t2,3) + 
                  389*Power(t2,4) + 1614*Power(t2,5) + 630*Power(t2,6))) \
- Power(s1,2)*(-6 + 58*t2 + (-67 + 5*s)*Power(t2,2) + 
               14*(-7 + 3*s)*Power(t2,3) - (69 + 122*s)*Power(t2,4) - 
               6*Power(t2,5) + 2*Power(t1,4)*(3 + 5*t2 + Power(t2,2)) + 
               Power(t1,3)*(1 - 37*t2 - 49*Power(t2,2) - Power(t2,3) + 
                  s*(6 + 27*t2 + 16*Power(t2,2))) - 
               Power(t1,2)*(10 + 30*t2 + 141*Power(t2,2) + 
                  4*Power(t2,3) + 29*Power(t2,4) + 
                  s*(38 + 83*t2 + 121*Power(t2,2) + 47*Power(t2,3))) + 
               t1*(16 + 139*t2 + 178*Power(t2,2) + 62*Power(t2,3) + 
                  92*Power(t2,4) + 23*Power(t2,5) + 
                  s*(14 - 99*t2 + 243*Power(t2,2) + 100*Power(t2,3) + 
                     41*Power(t2,4)))) + 
            s1*(9 + 17*(-1 + s)*t2 - 5*(17 + 15*s)*Power(t2,2) + 
               (-66 + 78*s + 4*Power(s,2))*Power(t2,3) + 
               2*(-28 - 100*s + 7*Power(s,2))*Power(t2,4) + 
               (240 - 127*s - 28*Power(s,2))*Power(t2,5) + 
               (363 + 91*s)*Power(t2,6) - 196*Power(t2,7) + 
               2*Power(t1,5)*(2 + t2) + 
               Power(t1,4)*(s*(13 + 20*t2 + 4*Power(t2,2)) + 
                  2*(-20 - 60*t2 - 16*Power(t2,2) + Power(t2,3))) + 
               Power(t1,3)*(-31 + 118*t2 + 617*Power(t2,2) + 
                  192*Power(t2,3) - 26*Power(t2,4) + 
                  2*Power(s,2)*(4 + 12*t2 + 7*Power(t2,2)) + 
                  s*(5 - 74*t2 - 63*Power(t2,2) + 7*Power(t2,3))) - 
               Power(t1,2)*(2 - 265*t2 - 845*Power(t2,2) + 
                  719*Power(t2,3) + 774*Power(t2,4) - 11*Power(t2,5) + 
                  Power(s,2)*
                   (-13 + 63*t2 + 69*Power(t2,2) + 23*Power(t2,3)) + 
                  s*(45 + 44*t2 + 176*Power(t2,2) - 6*Power(t2,3) + 
                     132*Power(t2,4))) + 
               t1*(20 + 14*t2 + 106*Power(t2,2) - 676*Power(t2,3) - 
                  1235*Power(t2,4) + 750*Power(t2,5) + 165*Power(t2,6) + 
                  2*Power(s,2)*t2*(-8 + 2*t2 + 47*Power(t2,2)) + 
                  s*(-12 - 27*t2 + 387*Power(t2,2) + 219*Power(t2,3) - 
                     67*Power(t2,4) + 109*Power(t2,5))))) + 
         Power(s2,6)*(-3 + (-1 + s)*Power(t1,6) + 4*t2 - 16*s*t2 + 
            46*Power(t2,2) + 54*s*Power(t2,2) - 
            17*Power(s,2)*Power(t2,2) + 78*Power(t2,3) + 
            21*s*Power(t2,3) + 41*Power(s,2)*Power(t2,3) + 
            4*Power(s,3)*Power(t2,3) + 280*Power(t2,4) - 
            240*s*Power(t2,4) - 86*Power(s,2)*Power(t2,4) + 
            3*Power(s,3)*Power(t2,4) - 385*Power(t2,5) + 
            871*s*Power(t2,5) - 127*Power(s,2)*Power(t2,5) - 
            41*Power(s,3)*Power(t2,5) - 253*Power(t2,6) + 
            647*s*Power(t2,6) + 26*Power(s,2)*Power(t2,6) - 
            28*Power(s,3)*Power(t2,6) - 523*Power(t2,7) - 
            420*s*Power(t2,7) + 399*Power(s,2)*Power(t2,7) + 
            192*Power(t2,8) - 427*s*Power(t2,8) + 84*Power(t2,9) + 
            t1*(-9 + (-4 - 25*s + 15*Power(s,2))*t2 + 
               (-218 + 283*s + 33*Power(s,2) + 13*Power(s,3))*
                Power(t2,2) + 
               (796 - 1595*s + 54*Power(s,2) + 49*Power(s,3))*
                Power(t2,3) + 
               (1111 - 1955*s + 405*Power(s,2) + 43*Power(s,3))*
                Power(t2,4) + 
               (3210 + 635*s - 1491*Power(s,2) + 91*Power(s,3))*
                Power(t2,5) + 
               (-944 + 2799*s - 754*Power(s,2))*Power(t2,6) + 
               (-1007 + 854*s)*Power(t2,7) - 142*Power(t2,8)) + 
            Power(t1,5)*(-15 + 8*t2 + 12*Power(t2,2) + 
               Power(s,2)*(5 + 3*t2) + s*(-3 - 5*t2 + 2*Power(t2,2))) + 
            Power(t1,4)*(76 + 671*t2 + 892*Power(t2,2) + 
               224*Power(t2,3) + 15*Power(t2,4) + 
               Power(s,3)*(3 + 7*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(19 + 22*t2 + 23*Power(t2,2) + 
                  4*Power(t2,3)) - 
               s*(77 + 412*t2 + 646*Power(t2,2) + 250*Power(t2,3) + 
                  15*Power(t2,4))) + 
            Power(s1,3)*(-(Power(t1,4)*(2 + 5*t2 + 2*Power(t2,2))) - 
               5*Power(t2,2)*
                (4 - 14*t2 + 9*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,3)*(17 + 67*t2 + 65*Power(t2,2) + 
                  13*Power(t2,3)) - 
               Power(t1,2)*(-2 + 147*t2 + 104*Power(t2,2) + 
                  146*Power(t2,3) + 23*Power(t2,4)) + 
               5*t1*(4 - 25*t2 + 22*Power(t2,2) + 32*Power(t2,3) + 
                  12*Power(t2,4) + 5*Power(t2,5))) - 
            Power(t1,3)*(-38 - 487*t2 + 674*Power(t2,2) + 
               3447*Power(t2,3) + 2087*Power(t2,4) + 313*Power(t2,5) + 
               12*Power(t2,6) + 
               Power(s,3)*(-2 + 7*t2 + 4*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s,2)*(23 + 169*t2 + 817*Power(t2,2) + 
                  614*Power(t2,3) + 109*Power(t2,4)) - 
               s*(20 + 128*t2 + 1717*Power(t2,2) + 3175*Power(t2,3) + 
                  1693*Power(t2,4) + 161*Power(t2,5))) + 
            Power(t1,2)*(2 - 290*t2 - 992*Power(t2,2) - 
               3831*Power(t2,3) + 1054*Power(t2,4) + 3395*Power(t2,5) + 
               1052*Power(t2,6) + 75*Power(t2,7) - 
               Power(s,3)*t2*
                (23 - 12*t2 + 73*Power(t2,2) + 50*Power(t2,3)) + 
               Power(s,2)*(1 - 6*t2 - 199*Power(t2,2) + 
                  1185*Power(t2,3) + 1859*Power(t2,4) + 478*Power(t2,5)\
) + s*(7 + 382*t2 + 570*Power(t2,2) - 48*Power(t2,3) - 
                  4143*Power(t2,4) - 3664*Power(t2,5) - 585*Power(t2,6))\
) + Power(s1,2)*(Power(t1,5)*(4 + 3*t2) + 
               t2*(30 - 155*t2 + (94 - 10*s)*Power(t2,2) + 
                  (112 - 90*s)*Power(t2,3) + 
                  5*(25 + 33*s)*Power(t2,4) + 15*Power(t2,5)) + 
               Power(t1,4)*(-16 - 59*t2 - 8*Power(t2,2) + 
                  4*Power(t2,3) + s*(7 + 17*t2 + 6*Power(t2,2))) - 
               Power(t1,3)*(5 + 195*t2 + 145*Power(t2,2) + 
                  121*Power(t2,3) + 68*Power(t2,4) + 
                  s*(84 + 158*t2 + 152*Power(t2,2) + 29*Power(t2,3))) + 
               Power(t1,2)*(131 + 329*t2 + 253*Power(t2,2) + 
                  463*Power(t2,3) + 242*Power(t2,4) + 
                  146*Power(t2,5) + 
                  s*(-130 + 345*t2 + 280*Power(t2,2) + 
                     301*Power(t2,3) + 11*Power(t2,4))) - 
               t1*(-88 + 90*t2 + 372*Power(t2,2) + 541*Power(t2,3) + 
                  129*Power(t2,4) + 144*Power(t2,5) + 75*Power(t2,6) + 
                  s*(-6 + 73*t2 - 403*Power(t2,2) + 635*Power(t2,3) + 
                     109*Power(t2,4) + 29*Power(t2,5)))) - 
            s1*(6 + Power(t1,6) + (-59 + 6*s)*t2 + 
               (42 - 98*s)*Power(t2,2) + 
               (141 + 141*s + 5*Power(s,2))*Power(t2,3) + 
               (256 - 249*s - 6*Power(s,2))*Power(t2,4) + 
               (113 + 481*s - 70*Power(s,2))*Power(t2,5) + 
               (-142 + 181*s + 56*Power(s,2))*Power(t2,6) - 
               (435 + 91*s)*Power(t2,7) + 140*Power(t2,8) + 
               Power(t1,5)*(-46 + 9*s - 46*t2 + 6*s*t2 + 
                  2*Power(t2,2)) + 
               Power(t1,4)*(17 + 418*t2 + 340*Power(t2,2) - 
                  86*Power(t2,3) - 15*Power(t2,4) + 
                  Power(s,2)*(8 + 19*t2 + 6*Power(t2,2)) + 
                  s*(-22 - 91*t2 + 15*Power(t2,2) + 8*Power(t2,3))) - 
               Power(t1,3)*(-102 - 861*t2 + 375*Power(t2,2) + 
                  1558*Power(t2,3) - 32*Power(t2,4) - 86*Power(t2,5) + 
                  Power(s,2)*
                   (51 + 105*t2 + 91*Power(t2,2) + 19*Power(t2,3)) + 
                  s*(5 + 324*t2 + 228*Power(t2,2) + 535*Power(t2,3) + 
                     177*Power(t2,4))) + 
               Power(t1,2)*(15 + 166*t2 - 929*Power(t2,2) - 
                  3232*Power(t2,3) + 1194*Power(t2,4) + 
                  798*Power(t2,5) - 95*Power(t2,6) + 
                  Power(s,2)*
                   (12 - 79*t2 + 232*Power(t2,2) + 115*Power(t2,3) - 
                     62*Power(t2,4)) + 
                  s*(-13 + 754*t2 + 354*Power(t2,2) + 512*Power(t2,3) + 
                     680*Power(t2,4) + 514*Power(t2,5))) + 
               t1*(-11 - 248*t2 - 389*Power(t2,2) - 377*Power(t2,3) + 
                  656*Power(t2,4) + 2320*Power(t2,5) - 715*Power(t2,6) - 
                  94*Power(t2,7) + 
                  Power(s,2)*t2*
                   (-17 + 102*t2 + 52*Power(t2,2) - 174*Power(t2,3) + 
                     59*Power(t2,4)) - 
                  s*(13 + 52*t2 - 447*Power(t2,2) + 1349*Power(t2,3) + 
                     452*Power(t2,4) - 80*Power(t2,5) + 325*Power(t2,6)))\
)) + Power(s2,5)*(2 + (-20 + 6*s - 30*s1)*t2 + 
            (5 + 6*Power(s,2) + 160*s1 + 60*Power(s1,2) - 
               s*(89 + 30*s1))*Power(t2,2) - 
            (16 - 59*s + 10*Power(s,2) + 2*Power(s,3) + 8*s1 - 
               141*s*s1 + 190*Power(s1,2) + 20*Power(s1,3))*Power(t2,3) \
+ (178 + 19*Power(s,3) + Power(s,2)*(72 - 25*s1) - 14*s1 + 
               53*Power(s1,2) + 55*Power(s1,3) - 
               2*s*(108 + 39*s1 + 5*Power(s1,2)))*Power(t2,4) + 
            (609 + 18*Power(s,3) - 386*s1 + 43*Power(s1,2) - 
               21*Power(s1,3) + Power(s,2)*(-244 + 5*s1) + 
               s*(-444 + 475*s1 - 100*Power(s1,2)))*Power(t2,5) - 
            (484 + 64*Power(s,3) + Power(s,2)*(311 - 140*s1) + 
               124*s1 - 129*Power(s1,2) + 30*Power(s1,3) + 
               s*(-1332 + 665*s1 - 136*Power(s1,2)))*Power(t2,6) - 
            (158 + 56*Power(s,3) + 27*s1 - 20*Power(s1,2) + 
               Power(s,2)*(-93 + 70*s1) + s*(-866 + 183*s1))*Power(t2,7) \
+ (-340 + 413*Power(s,2) + 321*s1 + 7*s*(-59 + 9*s1))*Power(t2,8) + 
            (67 - 315*s - 64*s1)*Power(t2,9) + 48*Power(t2,10) - 
            Power(t1,6)*(-1 + Power(s,2) + Power(s1,2) + 8*t2 + 
               2*s1*(10 + t2) - 2*s*(2 + s1 + t2)) + 
            Power(t1,5)*(Power(s1,3)*(1 + t2) - Power(s,3)*(2 + t2) + 
               Power(s1,2)*(24 + 9*t2 - 11*Power(t2,2)) + 
               s1*(106 + 266*t2 - 90*Power(t2,2) - 56*Power(t2,3)) - 
               3*(63 + 230*t2 + 130*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s,2)*(-7 - 15*t2 - 11*Power(t2,2) + 
                  s1*(5 + 3*t2)) + 
               s*(122 + 468*t2 + 387*Power(t2,2) + 56*Power(t2,3) - 
                  Power(s1,2)*(4 + 3*t2) + 
                  s1*(-43 + 6*t2 + 22*Power(t2,2)))) + 
            Power(t1,4)*(-153 + 50*t2 + 3077*Power(t2,2) + 
               3879*Power(t2,3) + 1010*Power(t2,4) + 64*Power(t2,5) + 
               Power(s,3)*(14 + 11*t2 + Power(t2,2) + Power(t2,3)) - 
               Power(s1,3)*(10 + 58*t2 + 19*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s1,2)*(77 + 226*t2 + 215*Power(t2,2) + 
                  249*Power(t2,3) + 30*Power(t2,4)) + 
               s1*(312 + 89*t2 - 1648*Power(t2,2) - 33*Power(t2,3) + 
                  394*Power(t2,4) + 24*Power(t2,5)) + 
               Power(s,2)*(26 + 705*t2 + 981*Power(t2,2) + 
                  429*Power(t2,3) + 30*Power(t2,4) - 
                  s1*(74 + 118*t2 + 21*Power(t2,2) + 3*Power(t2,3))) + 
               s*(-28 - 1111*t2 - 3284*Power(t2,2) - 3437*Power(t2,3) - 
                  782*Power(t2,4) - 24*Power(t2,5) + 
                  Power(s1,2)*
                   (58 + 165*t2 + 39*Power(t2,2) + 3*Power(t2,3)) - 
                  s1*(123 + 433*t2 + 838*Power(t2,2) + 
                     678*Power(t2,3) + 60*Power(t2,4)))) + 
            Power(t1,3)*(111 + 911*t2 + 4942*Power(t2,2) + 
               1513*Power(t2,3) - 5785*Power(t2,4) - 3704*Power(t2,5) - 
               511*Power(t2,6) - 13*Power(t2,7) + 
               Power(s1,3)*(109 + 101*t2 + 272*Power(t2,2) + 
                  96*Power(t2,3) - 14*Power(t2,4)) + 
               Power(s,3)*(4 - 11*t2 + 97*Power(t2,2) + 
                  173*Power(t2,3) + 41*Power(t2,4)) - 
               Power(s1,2)*(192 + 275*t2 + 812*Power(t2,2) + 
                  774*Power(t2,3) + 684*Power(t2,4) + 165*Power(t2,5)) \
+ s1*(63 - 523*t2 - 4208*Power(t2,2) + 725*Power(t2,3) + 
                  1939*Power(t2,4) - 454*Power(t2,5) - 104*Power(t2,6)) \
- Power(s,2)*(-15 + 37*t2 + 966*Power(t2,2) + 3776*Power(t2,3) + 
                  2252*Power(t2,4) + 266*Power(t2,5) + 
                  s1*(147 - 305*t2 - 267*Power(t2,2) + 
                     171*Power(t2,3) + 96*Power(t2,4))) + 
               s*(-147 - 487*t2 - 1116*Power(t2,2) + 5125*Power(t2,3) + 
                  8356*Power(t2,4) + 3134*Power(t2,5) + 
                  199*Power(t2,6) + 
                  Power(s1,2)*
                   (-86 - 407*t2 - 579*Power(t2,2) - 98*Power(t2,3) + 
                     69*Power(t2,4)) + 
                  s1*(481 + 350*t2 + 1197*Power(t2,2) + 
                     1654*Power(t2,3) + 2380*Power(t2,4) + 
                     431*Power(t2,5)))) + 
            Power(t1,2)*(6 + 266*t2 - 1100*Power(t2,2) - 
               2148*Power(t2,3) - 9150*Power(t2,4) - 530*Power(t2,5) + 
               3484*Power(t2,6) + 1099*Power(t2,7) + 62*Power(t2,8) - 
               3*Power(s,3)*t2*
                (-1 + 35*t2 + 30*Power(t2,2) + 121*Power(t2,3) + 
                  57*Power(t2,4)) + 
               2*Power(s1,3)*
                (40 - 15*t2 - 190*Power(t2,2) - 86*Power(t2,3) - 
                  87*Power(t2,4) + 10*Power(t2,5)) + 
               Power(s1,2)*(-52 + 614*t2 + 1227*Power(t2,2) + 
                  575*Power(t2,3) + 465*Power(t2,4) + 
                  555*Power(t2,5) + 225*Power(t2,6)) + 
               s1*(-196 - 649*t2 - 1011*Power(t2,2) + 
                  249*Power(t2,3) + 5854*Power(t2,4) - 
                  999*Power(t2,5) - 394*Power(t2,6) + 114*Power(t2,7)) \
+ Power(s,2)*(-1 + 19*t2 - 100*Power(t2,2) - 1410*Power(t2,3) + 
                  2877*Power(t2,4) + 3964*Power(t2,5) + 
                  780*Power(t2,6) + 
                  2*s1*(2 + 51*t2 + 116*Power(t2,2) - 
                     197*Power(t2,3) + 44*Power(t2,4) + 157*Power(t2,5)\
)) - s*(Power(s1,2)*(-144 + 819*t2 - 1207*Power(t2,2) - 
                     358*Power(t2,3) - 314*Power(t2,4) + 
                     163*Power(t2,5)) + 
                  t2*(149 - 2976*t2 - 5021*Power(t2,2) - 
                     1683*Power(t2,3) + 7240*Power(t2,4) + 
                     5017*Power(t2,5) + 592*Power(t2,6)) + 
                  s1*(-101 - 777*t2 + 2960*Power(t2,2) + 
                     666*Power(t2,3) + 207*Power(t2,4) + 
                     1825*Power(t2,5) + 835*Power(t2,6)))) + 
            t1*(-2 - 125*t2 - 314*Power(t2,2) - 1303*Power(t2,3) + 
               1422*Power(t2,4) + 1121*Power(t2,5) + 3652*Power(t2,6) - 
               359*Power(t2,7) - 738*Power(t2,8) - 96*Power(t2,9) + 
               Power(s,3)*Power(t2,2)*
                (-29 + 15*t2 + 130*Power(t2,2) + 178*Power(t2,3) + 
                  189*Power(t2,4)) + 
               2*Power(s1,3)*t2*
                (30 - 100*t2 + 42*Power(t2,2) + 115*Power(t2,3) + 
                  15*Power(t2,4) + 5*Power(t2,5)) - 
               Power(s1,2)*(60 - 390*t2 + 132*Power(t2,2) + 
                  454*Power(t2,3) + 705*Power(t2,4) + 154*Power(t2,5) + 
                  124*Power(t2,6) + 93*Power(t2,7)) + 
               s1*(-89 + 17*t2 + 529*Power(t2,2) + 1183*Power(t2,3) + 
                  456*Power(t2,4) + 400*Power(t2,5) - 
                  2515*Power(t2,6) + 382*Power(t2,7) + 25*Power(t2,8)) - 
               Power(s,2)*t2*
                (4 + 2*t2 - 275*Power(t2,2) - 659*Power(t2,3) - 
                  811*Power(t2,4) + 2328*Power(t2,5) + 
                  954*Power(t2,6) + 
                  s1*(6 - 113*t2 + 167*Power(t2,2) + 238*Power(t2,3) - 
                     202*Power(t2,4) + 158*Power(t2,5))) + 
               s*(1 - 93*t2 - 10*Power(t2,2) + 1285*Power(t2,3) - 
                  3967*Power(t2,4) - 4990*Power(t2,5) + 
                  671*Power(t2,6) + 3092*Power(t2,7) + 728*Power(t2,8) + 
                  Power(s1,2)*t2*
                   (30 - 175*t2 + 772*Power(t2,2) - 988*Power(t2,3) + 
                     10*Power(t2,4) + 15*Power(t2,5)) + 
                  s1*(-6 - 100*t2 + 206*Power(t2,2) - 1387*Power(t2,3) + 
                     2628*Power(t2,4) + 481*Power(t2,5) - 
                     61*Power(t2,6) + 445*Power(t2,7))))) + 
         Power(s2,4)*((2 - 2*s + 2*s1)*Power(t1,7) + 
            t2*(10 + 5*(-11 + 6*s - 12*s1)*t2 - 
               (20 + 31*s + 6*Power(s,2) - 104*s1 + 24*s*s1 - 
                  48*Power(s1,2))*Power(t2,2) + 
               (-185 + 37*s + 91*Power(s,2) - 10*Power(s,3) + 65*s1 - 
                  89*Power(s1,2) - 10*Power(s1,3))*Power(t2,3) + 
               (119 + 35*Power(s,3) + Power(s,2)*(138 - 50*s1) + 
                  188*s1 - 2*Power(s1,2) + 23*Power(s1,3) + 
                  s*(-570 + 42*s1 - 5*Power(s1,2)))*Power(t2,4) + 
               (701 + 45*Power(s,3) - 310*s1 - 31*Power(s1,2) - 
                  4*Power(s1,3) + Power(s,2)*(-362 + 5*s1) + 
                  s*(-578 + 536*s1 - 60*Power(s1,2)))*Power(t2,5) - 
               (443 + 55*Power(s,3) - 154*Power(s,2)*(-3 + s1) + 
                  73*s1 - 79*Power(s1,2) + 12*Power(s1,3) + 
                  s*(-1305 + 548*s1 - 67*Power(s1,2)))*Power(t2,6) - 
               (53 + 70*Power(s,3) + 81*s1 - 15*Power(s1,2) + 
                  2*Power(s,2)*(-69 + 28*s1) + s*(-652 + 125*s1))*
                Power(t2,7) + 
               (-105 - 280*s + 287*Power(s,2) + 141*s1 + 29*s*s1)*
                Power(t2,8) + (10 - 149*s - 17*s1)*Power(t2,9) + 
               15*Power(t2,10)) + 
            t1*(30 + 5*(4 + 33*s)*t2 + 
               (-10 - 99*s - 84*Power(s,2) + 12*Power(s,3))*
                Power(t2,2) - 
               (645 - 748*s + 191*Power(s,2) + 83*Power(s,3))*
                Power(t2,3) + 
               (-2973 + 3102*s + 648*Power(s,2) - 72*Power(s,3))*
                Power(t2,4) + 
               (1840 - 5671*s + 1888*Power(s,2) + 148*Power(s,3))*
                Power(t2,5) + 
               (561 - 6477*s + 989*Power(s,2) + 315*Power(s,3))*
                Power(t2,6) + 
               (2185 + 625*s - 2351*Power(s,2) + 245*Power(s,3))*
                Power(t2,7) + 
               (-38 + 2189*s - 768*Power(s,2))*Power(t2,8) + 
               (-321 + 384*s)*Power(t2,9) - 34*Power(t2,10) + 
               Power(s1,3)*Power(t2,2)*
                (60 - 166*t2 + 17*Power(t2,2) + 192*Power(t2,3) - 
                  8*Power(t2,4) - 3*Power(t2,5)) - 
               Power(s1,2)*t2*
                (180 - 10*(55 + 6*s)*t2 + 2*(23 + 64*s)*Power(t2,2) + 
                  (275 - 712*s)*Power(t2,3) + 
                  (436 + 936*s)*Power(t2,4) + 
                  (111 - 131*s)*Power(t2,5) + 
                  (70 - 41*s)*Power(t2,6) + 61*Power(t2,7)) + 
               s1*(60 + 15*(-27 + 2*s)*t2 - 
                  (166 + 203*s + 30*Power(s,2))*Power(t2,2) + 
                  (195 + 82*s + 180*Power(s,2))*Power(t2,3) + 
                  (1432 - 2115*s - 67*Power(s,2))*Power(t2,4) + 
                  (25 + 3158*s - 456*Power(s,2))*Power(t2,5) + 
                  2*(690 + 89*s + 96*Power(s,2))*Power(t2,6) + 
                  (-1602 + 20*s - 213*Power(s,2))*Power(t2,7) + 
                  (77 + 353*s)*Power(t2,8))) + 
            Power(t1,6)*(196 + 297*t2 + 13*Power(s,2)*t2 + 
               39*Power(t2,2) + Power(s1,2)*(-2 + 13*t2) + 
               s1*(-78 + 26*t2 + 73*Power(t2,2)) - 
               s*(126 + 268*t2 + 73*Power(t2,2) + s1*(-2 + 26*t2))) + 
            Power(t1,5)*(60 - 1319*t2 - 3727*Power(t2,2) - 
               1735*Power(t2,3) - 148*Power(t2,4) + 
               Power(s1,3)*(6 - 11*t2 - 5*Power(t2,2)) + 
               Power(s,3)*(-8 - 2*t2 + 5*Power(t2,2)) - 
               Power(s1,2)*(100 + 166*t2 + 335*Power(t2,2) + 
                  106*Power(t2,3)) - 
               2*s1*(52 - 453*t2 - 81*Power(t2,2) + 364*Power(t2,3) + 
                  56*Power(t2,4)) - 
               Power(s,2)*(226 + 737*t2 + 638*Power(t2,2) + 
                  106*Power(t2,3) + s1*(-26 + 7*t2 + 15*Power(t2,2))) + 
               s*(300 + 1714*t2 + 3447*Power(t2,2) + 1541*Power(t2,3) + 
                  112*Power(t2,4) + 
                  Power(s1,2)*(-24 + 20*t2 + 15*Power(t2,2)) + 
                  s1*(218 + 589*t2 + 973*Power(t2,2) + 212*Power(t2,3)))\
) + Power(t1,4)*(-310 - 3159*t2 - 3740*Power(t2,2) + 4306*Power(t2,3) + 
               6506*Power(t2,4) + 1475*Power(t2,5) + 69*Power(t2,6) + 
               Power(s1,3)*(-92 - 194*t2 - 178*Power(t2,2) + 
                  66*Power(t2,3) + 13*Power(t2,4)) - 
               Power(s,3)*(36 + 54*t2 + 204*Power(t2,2) + 
                  158*Power(t2,3) + 13*Power(t2,4)) + 
               Power(s1,2)*(44 + 557*t2 + 1134*Power(t2,2) + 
                  1328*Power(t2,3) + 681*Power(t2,4) + 48*Power(t2,5)) \
+ s1*(102 + 2756*t2 + 328*Power(t2,2) - 3034*Power(t2,3) + 
                  735*Power(t2,4) + 492*Power(t2,5) + 17*Power(t2,6)) + 
               Power(s,2)*(s1*
                   (-120 - 230*t2 + 64*Power(t2,2) + 
                     382*Power(t2,3) + 39*Power(t2,4)) + 
                  2*(80 + 78*t2 + 1938*Power(t2,2) + 
                     2131*Power(t2,3) + 579*Power(t2,4) + 
                     24*Power(t2,5))) - 
               s*(-130 - 1466*t2 + 3148*Power(t2,2) + 
                  9426*Power(t2,3) + 6785*Power(t2,4) + 
                  992*Power(t2,5) + 17*Power(t2,6) + 
                  Power(s1,2)*
                   (-320 - 418*t2 - 318*Power(t2,2) + 
                     290*Power(t2,3) + 39*Power(t2,4)) + 
                  s1*(152 + 725*t2 + 2071*Power(t2,2) + 
                     4452*Power(t2,3) + 1839*Power(t2,4) + 
                     96*Power(t2,5)))) + 
            Power(t1,3)*(-100 + 663*t2 + 2123*Power(t2,2) + 
               13531*Power(t2,3) + 5895*Power(t2,4) - 
               5035*Power(t2,5) - 3321*Power(t2,6) - 376*Power(t2,7) - 
               6*Power(t2,8) + 
               Power(s,3)*t2*
                (156 + 33*t2 + 590*Power(t2,2) + 663*Power(t2,3) + 
                  118*Power(t2,4)) + 
               Power(s1,3)*(-90 + 465*t2 + 236*Power(t2,2) + 
                  412*Power(t2,3) - 30*Power(t2,4) - 67*Power(t2,5)) - 
               2*Power(s1,2)*
                (194 + 639*t2 + 498*Power(t2,2) + 268*Power(t2,3) + 
                  611*Power(t2,4) + 524*Power(t2,5) + 85*Power(t2,6)) + 
               s1*(350 + 1047*t2 + 943*Power(t2,2) - 
                  7736*Power(t2,3) + 668*Power(t2,4) + 
                  1002*Power(t2,5) - 534*Power(t2,6) - 62*Power(t2,7)) \
+ Power(s,2)*(26 + 381*t2 + 1784*Power(t2,2) - 1828*Power(t2,3) - 
                  8056*Power(t2,4) - 3738*Power(t2,5) - 
                  309*Power(t2,6) + 
                  s1*(122 - 615*t2 + 666*Power(t2,2) + 
                     102*Power(t2,3) - 1122*Power(t2,4) - 
                     303*Power(t2,5))) + 
               s*(-62 - 2627*t2 - 5839*Power(t2,2) - 7285*Power(t2,3) + 
                  8214*Power(t2,4) + 11677*Power(t2,5) + 
                  3079*Power(t2,6) + 133*Power(t2,7) + 
                  Power(s1,2)*
                   (464 - 862*t2 - 713*Power(t2,2) - 841*Power(t2,3) + 
                     489*Power(t2,4) + 252*Power(t2,5)) + 
                  s1*(-546 + 2965*t2 + 543*Power(t2,2) + 
                     36*Power(t2,3) + 3372*Power(t2,4) + 
                     3902*Power(t2,5) + 479*Power(t2,6)))) + 
            Power(t1,2)*(90 + 
               (432 - 178*s - 185*Power(s,2) + 24*Power(s,3))*t2 - 
               (-2394 + 2698*s + 296*Power(s,2) + 7*Power(s,3))*
                Power(t2,2) - 
               (1905 - 7221*s + 1256*Power(s,2) + 190*Power(s,3))*
                Power(t2,3) - 
               (1441 - 13588*s + 3838*Power(s,2) + 349*Power(s,3))*
                Power(t2,4) + 
               (-10139 + 3951*s + 4188*Power(s,2) - 798*Power(s,3))*
                Power(t2,5) - 
               (1475 + 7821*s - 4877*Power(s,2) + 280*Power(s,3))*
                Power(t2,6) + 
               (2117 - 4094*s + 741*Power(s,2))*Power(t2,7) - 
               88*(-7 + 4*s)*Power(t2,8) + 25*Power(t2,9) + 
               Power(s1,3)*(-60 + 180*t2 + 38*Power(t2,2) - 
                  556*Power(t2,3) - 129*Power(t2,4) - 91*Power(t2,5) + 
                  60*Power(t2,6)) + 
               Power(s1,2)*(-250 - 178*t2 + 785*Power(t2,2) + 
                  1628*Power(t2,3) + 644*Power(t2,4) - 25*Power(t2,5) + 
                  597*Power(t2,6) + 174*Power(t2,7) + 
                  s*(-60 + 540*t2 - 1931*Power(t2,2) + 
                     2256*Power(t2,3) + 57*Power(t2,4) + 
                     114*Power(t2,5) - 315*Power(t2,6))) + 
               s1*(100 - 810*t2 - 2104*Power(t2,2) - 749*Power(t2,3) - 
                  2987*Power(t2,4) + 5559*Power(t2,5) - 
                  242*Power(t2,6) - 4*Power(t2,7) + 62*Power(t2,8) + 
                  Power(s,2)*t2*
                   (-180 + 328*t2 + 456*Power(t2,2) - 409*Power(t2,3) + 
                     490*Power(t2,4) + 535*Power(t2,5)) - 
                  s*(130 - 147*t2 - 3122*Power(t2,2) + 
                     5997*Power(t2,3) + 240*Power(t2,4) - 
                     745*Power(t2,5) + 2315*Power(t2,6) + 749*Power(t2,7)\
)))) + Power(s2,2)*((6 - 6*s + 6*s1)*Power(t1,8) + 
            Power(t2,3)*(-20 + (65 - 60*s + 30*s1)*t2 + 
               (4 - 60*Power(s,2) - 79*s1 - 6*Power(s1,2) + 
                  10*s*(17 + 3*s1))*Power(t2,2) + 
               (-152 + 115*Power(s,2) - 20*Power(s,3) + 
                  s*(108 - 82*s1) + 12*s1 + 13*Power(s1,2))*Power(t2,3) \
+ (38 + 10*Power(s,3) + Power(s,2)*(177 - 25*s1) + 15*s*(-25 + s1) + 
                  95*s1 - 2*Power(s1,2))*Power(t2,4) + 
               (45*Power(s,3) + 2*Power(s,2)*(-77 + 2*s1) + 
                  s*(-170 + 113*s1 - 2*Power(s1,2)) - 
                  2*(-87 + 27*s1 + 5*Power(s1,2)))*Power(t2,5) + 
               (-117 + Power(s,3) - 7*s1 + 4*Power(s1,2) + 
                  Power(s,2)*(-211 + 34*s1) + 
                  s*(345 - 65*s1 + 2*Power(s1,2)))*Power(t2,6) + 
               (1 - 28*Power(s,3) + Power(s,2)*(66 - 8*s1) - 
                  12*s*(-2 + s1) + Power(s1,2))*Power(t2,7) + 
               (7 + 34*Power(s,2) + s*(-37 + s1) + 3*s1)*Power(t2,8) - 
               5*s*Power(t2,9)) + 
            t1*t2*(-60 + 12*(-7 + 3*s + 3*s1)*t2 + 
               2*(-14 + 72*Power(s,2) + 79*s1 - 24*Power(s1,2) - 
                  s*(245 + 6*s1))*Power(t2,2) + 
               (815 + 60*Power(s,3) - 71*s1 + 78*Power(s1,2) + 
                  12*Power(s1,3) + 10*Power(s,2)*(-47 + 6*s1) + 
                  s*(-588 + 187*s1 - 30*Power(s1,2)))*Power(t2,3) - 
               (-140 + 40*Power(s,3) + Power(s,2)*(997 - 85*s1) + 
                  466*s1 + 12*Power(s1,2) + 25*Power(s1,3) + 
                  s*(-2005 + 188*s1 - 85*Power(s1,2)))*Power(t2,4) + 
               (-1806 - 225*Power(s,3) + Power(s,2)*(515 - 50*s1) + 
                  297*s1 - 16*Power(s1,2) + 2*Power(s1,3) + 
                  s*(2559 - 737*s1 + 49*Power(s1,2)))*Power(t2,5) + 
               (906 - 95*Power(s,3) - 259*s1 + 21*Power(s1,2) + 
                  16*Power(s1,3) - 4*Power(s,2)*(-489 + 58*s1) - 
                  5*s*(549 - 177*s1 + 31*Power(s1,2)))*Power(t2,6) + 
               (121 + 181*Power(s,3) + 448*s1 - 3*Power(s1,2) - 
                  4*Power(s1,3) + Power(s,2)*(239 + 114*s1) + 
                  s*(-1621 - 244*s1 + 45*Power(s1,2)))*Power(t2,7) + 
               (18 + 105*Power(s,3) - 128*s1 - 17*Power(s1,2) - 
                  Power(s1,3) - 5*Power(s,2)*(125 + 17*s1) + 
                  s*(349 + 98*s1 + 9*Power(s1,2)))*Power(t2,8) - 
               (111*Power(s,2) - 2*s*(117 + 22*s1) + 
                  3*(5 + 5*s1 + Power(s1,2)))*Power(t2,9) + 
               (-7 + 15*s)*Power(t2,10)) + 
            Power(t1,2)*(-90 - 12*(25 - s + 9*Power(s,2))*t2 - 
               6*(182 - 54*s - 73*Power(s,2) + 6*Power(s,3))*
                Power(t2,2) + 
               2*(-243 - 1242*s + 617*Power(s,2) + 36*Power(s,3))*
                Power(t2,3) + 
               (5716 - 9373*s + 245*Power(s,2) + 312*Power(s,3))*
                Power(t2,4) + 
               (-2362 + 6258*s - 4946*Power(s,2) + 309*Power(s,3))*
                Power(t2,5) - 
               (348 - 10778*s + 4239*Power(s,2) + 294*Power(s,3))*
                Power(t2,6) + 
               (-1211 + 940*s + 2301*Power(s,2) - 673*Power(s,3))*
                Power(t2,7) - 
               (22 + 2055*s - 1533*Power(s,2) + 146*Power(s,3))*
                Power(t2,8) + 
               2*(71 - 239*s + 66*Power(s,2))*Power(t2,9) + 
               (17 - 16*s)*Power(t2,10) + 
               Power(s1,3)*Power(t2,2)*
                (-36 + 100*t2 + 60*Power(t2,2) - 157*Power(t2,3) - 
                  6*Power(t2,4) + 4*Power(t2,5) + 17*Power(t2,6)) + 
               Power(s1,2)*t2*
                (144 - 6*(77 + 6*s)*t2 - 8*(7 + 15*s)*Power(t2,2) + 
                  (361 - 654*s)*Power(t2,3) + 
                  (-3 + 1157*s)*Power(t2,4) - 
                  3*(-55 + 86*s)*Power(t2,5) + 
                  25*(-7 + s)*Power(t2,6) - 
                  3*(-44 + 41*s)*Power(t2,7) + 14*Power(t2,8)) + 
               s1*(-180 + 6*(47 + 18*s)*t2 + 
                  (504 + 48*s - 36*Power(s,2))*Power(t2,2) + 
                  (124 + 550*s - 124*Power(s,2))*Power(t2,3) + 
                  (-1031 + 2409*s + 6*Power(s,2))*Power(t2,4) + 
                  (2406 - 4852*s + 653*Power(s,2))*Power(t2,5) + 
                  (-3213 + 1578*s - 470*Power(s,2))*Power(t2,6) + 
                  (626 + 370*s + 401*Power(s,2))*Power(t2,7) + 
                  (181 - 726*s + 252*Power(s,2))*Power(t2,8) - 
                  14*(-3 + 8*s)*Power(t2,9) + Power(t2,10))) + 
            2*Power(t1,7)*(5*Power(s,3) - 5*Power(s1,3) - 
               Power(s1,2)*(17 + 39*t2) - 
               Power(s,2)*(41 + 15*s1 + 39*t2) + 
               s1*(25 - 171*t2 - 96*Power(t2,2)) - 
               5*(36 + 87*t2 + 16*Power(t2,2)) + 
               s*(161 + 15*Power(s1,2) + 406*t2 + 96*Power(t2,2) + 
                  s1*(58 + 78*t2))) + 
            Power(t1,6)*(-694 - 968*t2 + 2710*Power(t2,2) + 
               2026*Power(t2,3) + 169*Power(t2,4) + 
               Power(s,3)*(2 - 64*t2 - 72*Power(t2,2)) + 
               Power(s1,3)*(-30 + 58*t2 + 72*Power(t2,2)) + 
               2*Power(s1,2)*
                (114 + 390*t2 + 519*Power(t2,2) + 143*Power(t2,3)) + 
               s1*(308 - 1608*t2 - 496*Power(t2,2) + 780*Power(t2,3) + 
                  143*Power(t2,4)) + 
               2*Power(s,2)*(150 + 1016*t2 + 947*Power(t2,2) + 
                  143*Power(t2,3) + 3*s1*(-11 + 31*t2 + 36*Power(t2,2))\
) - s*(24 + 1586*t2 + 4288*Power(t2,2) + 1974*Power(t2,3) + 
                  143*Power(t2,4) + 
                  2*Power(s1,2)*(-47 + 90*t2 + 108*Power(t2,2)) + 
                  4*s1*(88 + 539*t2 + 733*Power(t2,2) + 143*Power(t2,3))\
)) + Power(t1,5)*(288 + 5068*t2 + 8006*Power(t2,2) + 1612*Power(t2,3) - 
               4037*Power(t2,4) - 1132*Power(t2,5) - 48*Power(t2,6) + 
               Power(s1,3)*(242 + 264*t2 + 218*Power(t2,2) - 
                  310*Power(t2,3) - 88*Power(t2,4)) + 
               2*Power(s,3)*(33 + 69*t2 + 345*Power(t2,2) + 
                  316*Power(t2,3) + 44*Power(t2,4)) - 
               Power(s1,2)*(152 - 168*t2 + 6*Power(t2,2) + 
                  1542*Power(t2,3) + 1032*Power(t2,4) + 127*Power(t2,5)\
) - 2*s1*(-160 + 1295*t2 + 402*Power(t2,2) - 1163*Power(t2,3) + 
                  328*Power(t2,4) + 204*Power(t2,5) + 11*Power(t2,6)) - 
               Power(s,2)*(240 - 1704*t2 + 3518*Power(t2,2) + 
                  6716*Power(t2,3) + 2205*Power(t2,4) + 
                  127*Power(t2,5) + 
                  2*s1*(-55 - 122*t2 + 353*Power(t2,2) + 
                     787*Power(t2,3) + 132*Power(t2,4))) + 
               s*(-408 - 5966*t2 - 1840*Power(t2,2) + 
                  7656*Power(t2,3) + 6689*Power(t2,4) + 
                  1053*Power(t2,5) + 22*Power(t2,6) + 
                  2*Power(s1,2)*
                   (-289 - 251*t2 - 101*Power(t2,2) + 
                     626*Power(t2,3) + 132*Power(t2,4)) + 
                  s1*(568 - 1392*t2 - 772*Power(t2,2) + 
                     6222*Power(t2,3) + 3237*Power(t2,4) + 
                     254*Power(t2,5)))) + 
            Power(t1,4)*(586 + 196*t2 + 2582*Power(t2,2) - 
               10880*Power(t2,3) - 7732*Power(t2,4) + 
               1045*Power(t2,5) + 1818*Power(t2,6) + 226*Power(t2,7) + 
               3*Power(t2,8) + 
               Power(s1,3)*(126 - 478*t2 - 216*Power(t2,2) - 
                  376*Power(t2,3) + 52*Power(t2,4) + 229*Power(t2,5) + 
                  20*Power(t2,6)) - 
               Power(s,3)*(-30 + 356*t2 - 88*Power(t2,2) + 
                  908*Power(t2,3) + 1523*Power(t2,4) + 
                  481*Power(t2,5) + 20*Power(t2,6)) + 
               Power(s1,2)*(348 + 1764*t2 + 1700*Power(t2,2) - 
                  1794*Power(t2,3) + 128*Power(t2,4) + 
                  1093*Power(t2,5) + 304*Power(t2,6) + 12*Power(t2,7)) \
+ s1*(-902 - 570*t2 - 4760*Power(t2,2) + 3884*Power(t2,3) - 
                  359*Power(t2,4) + 102*Power(t2,5) + 
                  466*Power(t2,6) + 58*Power(t2,7) + Power(t2,8)) + 
               Power(s,2)*(68 - 676*t2 - 5192*Power(t2,2) - 
                  3548*Power(t2,3) + 7887*Power(t2,4) + 
                  5598*Power(t2,5) + 733*Power(t2,6) + 
                  12*Power(t2,7) + 
                  s1*(-446 + 1322*t2 - 752*Power(t2,2) - 
                     480*Power(t2,3) + 2350*Power(t2,4) + 
                     1191*Power(t2,5) + 60*Power(t2,6))) - 
               s*(610 - 3774*t2 - 11276*Power(t2,2) - 
                  18462*Power(t2,3) + 1467*Power(t2,4) + 
                  9738*Power(t2,5) + 2924*Power(t2,6) + 
                  168*Power(t2,7) + Power(t2,8) + 
                  Power(s1,2)*
                   (542 - 824*t2 - 544*Power(t2,2) - 
                     1148*Power(t2,3) + 879*Power(t2,4) + 
                     939*Power(t2,5) + 60*Power(t2,6)) + 
                  s1*(-1936 + 4944*t2 + 260*Power(t2,2) - 
                     5750*Power(t2,3) + 832*Power(t2,4) + 
                     5129*Power(t2,5) + 1037*Power(t2,6) + 
                     24*Power(t2,7)))) + 
            Power(t1,3)*(-120 - 698*t2 - 5786*Power(t2,2) + 
               2048*Power(t2,3) - 1036*Power(t2,4) + 6437*Power(t2,5) + 
               1798*Power(t2,6) - 807*Power(t2,7) - 313*Power(t2,8) - 
               13*Power(t2,9) + 
               Power(s1,3)*(40 - 68*t2 - 318*Power(t2,2) + 
                  506*Power(t2,3) + 127*Power(t2,4) + 79*Power(t2,5) - 
                  93*Power(t2,6) - 41*Power(t2,7)) + 
               Power(s,3)*(4 + 10*t2 - 150*Power(t2,2) - 
                  92*Power(t2,3) + 90*Power(t2,4) + 1307*Power(t2,5) + 
                  884*Power(t2,6) + 89*Power(t2,7)) - 
               Power(s1,2)*(-90 - 326*t2 + 254*Power(t2,2) + 
                  1038*Power(t2,3) + 1313*Power(t2,4) - 
                  1505*Power(t2,5) + 511*Power(t2,6) + 
                  255*Power(t2,7) + 24*Power(t2,8)) - 
               Power(s,2)*(78 - 2*(27 + 100*s1)*t2 + 
                  10*(31 + s1)*Power(t2,2) + 
                  (-2944 + 718*s1)*Power(t2,3) - 
                  (9877 + 657*s1)*Power(t2,4) + 
                  (637 + 267*s1)*Power(t2,5) + 
                  (6011 + 1479*s1)*Power(t2,6) + 
                  (1580 + 219*s1)*Power(t2,7) + 67*Power(t2,8)) - 
               s1*(562 - 668*t2 - 2080*Power(t2,2) + 4264*Power(t2,3) - 
                  6868*Power(t2,4) + 1519*Power(t2,5) + 
                  345*Power(t2,6) + 324*Power(t2,7) + 64*Power(t2,8) + 
                  2*Power(t2,9)) + 
               s*(22 + 842*t2 + 9564*Power(t2,2) - 5058*Power(t2,3) - 
                  21486*Power(t2,4) - 11120*Power(t2,5) + 
                  5217*Power(t2,6) + 3693*Power(t2,7) + 
                  427*Power(t2,8) + 7*Power(t2,9) + 
                  Power(s1,2)*
                   (180 - 574*t2 + 2366*Power(t2,2) - 
                     2784*Power(t2,3) + 260*Power(t2,4) - 
                     450*Power(t2,5) + 688*Power(t2,6) + 171*Power(t2,7)\
) + s1*(484 - 108*t2 - 5204*Power(t2,2) + 9942*Power(t2,3) - 
                     1061*Power(t2,4) - 4532*Power(t2,5) + 
                     2368*Power(t2,6) + 1423*Power(t2,7) + 91*Power(t2,8)\
)))) - Power(s2,3)*(Power(t2,2)*
             (-20 + 4*(-7 + 3*s + 3*s1)*t2 + 
               (19 + 48*Power(s,2) + 71*s1 - 6*Power(s1,2) - 
                  8*s*(20 + 3*s1))*Power(t2,2) + 
               (235 - 170*Power(s,2) + 20*Power(s,3) - 55*s1 - 
                  8*Power(s1,2) + 2*Power(s1,3) + 43*s*(-2 + 3*s1))*
                Power(t2,3) + 
               (-39 - 30*Power(s,3) - 211*s1 + 11*Power(s1,2) - 
                  4*Power(s1,3) + Power(s,2)*(-209 + 50*s1) + 
                  s*(631 - 57*s1 + Power(s1,2)))*Power(t2,4) + 
               (-458 - 60*Power(s,3) + Power(s,2)*(308 - 6*s1) + 
                  156*s1 + 36*Power(s1,2) + 
                  2*s*(219 - 172*s1 + 9*Power(s1,2)))*Power(t2,5) + 
               (282 + 22*Power(s,3) + Power(s,2)*(410 - 98*s1) + 
                  24*s1 - 27*Power(s1,2) + 2*Power(s1,3) - 
                  2*s*(418 - 131*s1 + 9*Power(s1,2)))*Power(t2,6) + 
               (10 + 56*Power(s,3) + 34*s1 - 6*Power(s1,2) + 
                  Power(s,2)*(-121 + 28*s1) + s*(-251 + 53*s1))*
                Power(t2,7) + 
               (1 - 129*Power(s,2) + s*(129 - 8*s1) - 33*s1)*
                Power(t2,8) + (41*s + 2*s1)*Power(t2,9) - 2*Power(t2,10)\
) + t1*(20 + 20*(-7 + 3*s - 9*s1)*t2 - 
               2*(70 + 24*Power(s,2) - 151*s1 - 72*Power(s1,2) + 
                  s*(29 + 6*s1))*Power(t2,2) - 
               2*(367 + 16*Power(s,3) - 149*s1 + 166*Power(s1,2) + 
                  16*Power(s1,3) + 2*Power(s,2)*(-82 + 3*s1) + 
                  s*(-91 - 8*s1 + 6*Power(s1,2)))*Power(t2,3) + 
               (131 + 94*Power(s,3) + Power(s,2)*(684 - 128*s1) + 
                  464*s1 + 87*Power(s1,3) + 
                  s*(-2003 + 184*s1 - 60*Power(s1,2)))*Power(t2,4) + 
               (3224 + 204*Power(s,3) - 860*s1 + 71*Power(s1,2) + 
                  Power(s1,3) - 2*Power(s,2)*(397 + 4*s1) + 
                  s*(-3962 + 1797*s1 - 301*Power(s1,2)))*Power(t2,5) + 
               (-1662 - 27*Power(s,3) + 362*s1 + 98*Power(s1,2) - 
                  86*Power(s1,3) + 3*Power(s,2)*(-875 + 152*s1) + 
                  s*(4987 - 2279*s1 + 523*Power(s1,2)))*Power(t2,6) - 
               (223 + 312*Power(s,3) + 1172*s1 - 42*Power(s1,2) - 
                  14*Power(s1,3) + 5*Power(s,2)*(141 + 34*s1) + 
                  5*s*(-912 - 35*s1 + 24*Power(s1,2)))*Power(t2,7) + 
               (-591 - 203*Power(s,3) + 589*s1 + 36*Power(s1,2) + 
                  4*Power(s1,3) + 2*Power(s,2)*(769 + 86*s1) - 
                  s*(559 + 103*s1 + 29*Power(s1,2)))*Power(t2,8) + 
               (11 + 386*Power(s,2) + 24*s1 + 21*Power(s1,2) - 
                  s*(958 + 167*s1))*Power(t2,9) + 
               (75 - 115*s + s1)*Power(t2,10) + 5*Power(t2,11)) + 
            2*Power(t1,7)*(42 + 3*Power(s,2) - 2*s1 + 3*Power(s1,2) + 
               12*t2 + 19*s1*t2 - s*(35 + 6*s1 + 19*t2)) + 
            2*Power(t1,6)*(-107 - 910*t2 - 840*Power(t2,2) - 
               98*Power(t2,3) - 2*Power(s1,3)*(3 + 5*t2) + 
               Power(s,3)*(3 + 10*t2) - 
               Power(s1,2)*(23 + 94*t2 + 69*Power(t2,2)) + 
               s1*(103 + 79*t2 - 345*Power(t2,2) - 104*Power(t2,3)) - 
               Power(s,2)*(113 + 200*t2 + 69*Power(t2,2) + 
                  6*s1*(2 + 5*t2)) + 
               s*(184 + 847*t2 + 778*Power(t2,2) + 104*Power(t2,3) + 
                  15*Power(s1,2)*(1 + 2*t2) + 
                  2*s1*(40 + 147*t2 + 69*Power(t2,2)))) + 
            Power(t1,5)*(-804 - 2726*t2 + 542*Power(t2,2) + 
               6062*Power(t2,3) + 2297*Power(t2,4) + 151*Power(t2,5) + 
               2*Power(s1,3)*
                (-21 - 83*t2 + 49*Power(t2,2) + 25*Power(t2,3)) - 
               2*Power(s,3)*(-6 + 57*t2 + 102*Power(t2,2) + 
                  25*Power(t2,3)) + 
               2*Power(s1,2)*
                (43 + 396*t2 + 693*Power(t2,2) + 579*Power(t2,3) + 
                  94*Power(t2,4)) + 
               s1*(722 + 738*t2 - 2984*Power(t2,2) + 268*Power(t2,3) + 
                  904*Power(t2,4) + 80*Power(t2,5)) + 
               2*Power(s,2)*(-65 + 954*t2 + 2024*Power(t2,2) + 
                  1027*Power(t2,3) + 94*Power(t2,4) + 
                  s1*(-105 - 57*t2 + 253*Power(t2,2) + 75*Power(t2,3))) \
- s*(-570 + 810*t2 + 5474*Power(t2,2) + 7496*Power(t2,3) + 
                  1977*Power(t2,4) + 80*Power(t2,5) + 
                  2*Power(s1,2)*
                   (-96 - 197*t2 + 200*Power(t2,2) + 75*Power(t2,3)) + 
                  2*s1*(-14 + 670*t2 + 2123*Power(t2,2) + 
                     1606*Power(t2,3) + 188*Power(t2,4)))) + 
            Power(t1,4)*(148 + 1158*t2 + 11206*Power(t2,2) + 
               10112*Power(t2,3) - 2368*Power(t2,4) - 
               5041*Power(t2,5) - 921*Power(t2,6) - 28*Power(t2,7) + 
               Power(s1,3)*(200 + 276*t2 + 452*Power(t2,2) + 
                  76*Power(t2,3) - 234*Power(t2,4) - 26*Power(t2,5)) + 
               Power(s,3)*(6 - 44*t2 + 496*Power(t2,2) + 
                  986*Power(t2,3) + 440*Power(t2,4) + 26*Power(t2,5)) - 
               2*Power(s1,2)*
                (335 + 351*t2 + 93*Power(t2,2) + 488*Power(t2,3) + 
                  945*Power(t2,4) + 342*Power(t2,5) + 17*Power(t2,6)) - 
               2*s1*(-159 - 563*t2 + 2955*Power(t2,2) + 
                  8*Power(t2,3) - 967*Power(t2,4) + 456*Power(t2,5) + 
                  131*Power(t2,6) + 3*Power(t2,7)) - 
               Power(s,2)*(-338 - 662*t2 - 988*Power(t2,2) + 
                  7798*Power(t2,3) + 7123*Power(t2,4) + 
                  1328*Power(t2,5) + 34*Power(t2,6) + 
                  2*s1*(242 - 390*t2 - 126*Power(t2,2) + 
                     718*Power(t2,3) + 557*Power(t2,4) + 39*Power(t2,5)\
)) + s*(-898 - 2958*t2 - 9820*Power(t2,2) + 3068*Power(t2,3) + 
                  13534*Power(t2,4) + 6412*Power(t2,5) + 
                  606*Power(t2,6) + 6*Power(t2,7) + 
                  2*Power(s1,2)*
                   (27 - 506*t2 - 468*Power(t2,2) + 187*Power(t2,3) + 
                     454*Power(t2,4) + 39*Power(t2,5)) + 
                  s1*(1340 + 584*t2 - 858*Power(t2,2) + 
                     2010*Power(t2,3) + 7143*Power(t2,4) + 
                     2012*Power(t2,5) + 68*Power(t2,6)))) + 
            Power(t1,3)*(204 + 1970*t2 - 858*Power(t2,2) + 
               492*Power(t2,3) - 14406*Power(t2,4) - 5762*Power(t2,5) + 
               2551*Power(t2,6) + 1520*Power(t2,7) + 124*Power(t2,8) + 
               Power(t2,9) - 
               2*Power(s,3)*(-4 - 63*t2 + 136*Power(t2,2) + 
                  111*Power(t2,3) + 636*Power(t2,4) + 
                  533*Power(t2,5) + 71*Power(t2,6)) + 
               Power(s1,3)*(30 + 138*t2 - 678*Power(t2,2) - 
                  230*Power(t2,3) - 279*Power(t2,4) + 
                  149*Power(t2,5) + 78*Power(t2,6)) + 
               Power(s1,2)*(-396 + 836*t2 + 2074*Power(t2,2) + 
                  1358*Power(t2,3) - 1085*Power(t2,4) + 
                  987*Power(t2,5) + 737*Power(t2,6) + 89*Power(t2,7)) + 
               s1*(-470 - 1932*t2 - 818*Power(t2,2) - 
                  5434*Power(t2,3) + 6201*Power(t2,4) - 
                  18*Power(t2,5) + 121*Power(t2,6) + 272*Power(t2,7) + 
                  18*Power(t2,8)) + 
               Power(s,2)*(-20 + 24*t2 - 1044*Power(t2,2) - 
                  6630*Power(t2,3) + 1455*Power(t2,4) + 
                  9335*Power(t2,5) + 3314*Power(t2,6) + 
                  196*Power(t2,7) + 
                  2*s1*(15 - 33*t2 + 453*Power(t2,2) - 
                     331*Power(t2,3) + 150*Power(t2,4) + 
                     940*Power(t2,5) + 181*Power(t2,6))) - 
               s*(236 + 2356*t2 - 6800*Power(t2,2) - 
                  17558*Power(t2,3) - 14091*Power(t2,4) + 
                  8184*Power(t2,5) + 9042*Power(t2,6) + 
                  1642*Power(t2,7) + 47*Power(t2,8) + 
                  Power(s1,2)*
                   (-460 + 1926*t2 - 2300*Power(t2,2) - 
                     322*Power(t2,3) - 664*Power(t2,4) + 
                     963*Power(t2,5) + 298*Power(t2,6)) + 
                  s1*(-224 - 3476*t2 + 7302*Power(t2,2) - 
                     180*Power(t2,3) - 3717*Power(t2,4) + 
                     3622*Power(t2,5) + 3237*Power(t2,6) + 
                     285*Power(t2,7)))) + 
            Power(t1,2)*(50 - 130*t2 - 906*Power(t2,2) - 
               5788*Power(t2,3) + 2671*Power(t2,4) + 265*Power(t2,5) + 
               5400*Power(t2,6) + 735*Power(t2,7) - 752*Power(t2,8) - 
               171*Power(t2,9) - 4*Power(t2,10) + 
               Power(s1,3)*t2*
                (60 - 168*t2 - 132*Power(t2,2) + 445*Power(t2,3) + 
                  41*Power(t2,4) + 11*Power(t2,5) - 49*Power(t2,6)) + 
               Power(s,3)*t2*
                (12 - 116*t2 - 134*Power(t2,2) + 52*Power(t2,3) + 
                  485*Power(t2,4) + 966*Power(t2,5) + 263*Power(t2,6)) - 
               Power(s1,2)*(180 - 510*t2 - 200*Power(t2,2) + 
                  528*Power(t2,3) + 806*Power(t2,4) + 438*Power(t2,5) - 
                  309*Power(t2,6) + 362*Power(t2,7) + 71*Power(t2,8)) - 
               s1*(260 + 556*t2 - 516*Power(t2,2) - 2264*Power(t2,3) + 
                  1638*Power(t2,4) - 4975*Power(t2,5) + 
                  2703*Power(t2,6) + 225*Power(t2,7) + 94*Power(t2,8) + 
                  15*Power(t2,9)) - 
               Power(s,2)*t2*
                (274 + 174*t2 - 326*Power(t2,2) - 3794*Power(t2,3) - 
                  5507*Power(t2,4) + 3896*Power(t2,5) + 
                  3594*Power(t2,6) + 419*Power(t2,7) + 
                  s1*(60 - 360*t2 + 124*Power(t2,2) + 693*Power(t2,3) - 
                     427*Power(t2,4) + 635*Power(t2,5) + 485*Power(t2,6)\
)) + s*(10 - 24*t2 + 980*Power(t2,2) + 7772*Power(t2,3) - 
                  8773*Power(t2,4) - 17213*Power(t2,5) - 
                  3456*Power(t2,6) + 5234*Power(t2,7) + 
                  1943*Power(t2,8) + 115*Power(t2,9) + 
                  Power(s1,2)*t2*
                   (180 - 484*t2 + 1846*Power(t2,2) - 
                     2298*Power(t2,3) + 281*Power(t2,4) + 
                     3*Power(t2,5) + 271*Power(t2,6)) + 
                  s1*(-60 + 212*t2 - 122*Power(t2,2) - 
                     4258*Power(t2,3) + 7265*Power(t2,4) - 
                     912*Power(t2,5) - 982*Power(t2,6) + 
                     1684*Power(t2,7) + 390*Power(t2,8))))) + 
         s2*(Power(t2,4)*(1 + (-1 + s)*t2)*
             (-10 + (-20*s + 6*(3 + s1))*t2 + 
               (19 + 23*s - 10*Power(s,2) - 8*s1)*Power(t2,2) - 
               (37 + Power(s,2) + 5*s*(-11 + s1) + 6*s1)*Power(t2,3) + 
               (-11 + 18*Power(s,2) + s*(-30 + s1) + 10*s1)*
                Power(t2,4) + 
               (26 + 4*Power(s,2) + 5*s*(-9 + s1))*Power(t2,5) - 
               (8*Power(s,2) + s*(-13 + s1) + 2*(2 + s1))*Power(t2,6) + 
               (-1 + 4*s)*Power(t2,7)) + 
            t1*Power(t2,2)*(60 + 60*(-3 + 3*s - s1)*t2 + 
               5*(-10 + 36*Power(s,2) + 27*s1 - 3*s*(29 + 2*s1))*
                Power(t2,2) + 
               (439 + 60*Power(s,3) + 30*Power(s,2)*(-8 + s1) - 
                  11*s1 + 2*Power(s1,2) + 
                  s*(-465 + 92*s1 - 6*Power(s1,2)))*Power(t2,3) + 
               (-36 + 15*Power(s,3) - 113*s1 + 
                  Power(s,2)*(-630 + 43*s1) + 
                  s*(940 - 82*s1 + 13*Power(s1,2)))*Power(t2,4) + 
               (-517 - 123*Power(s,3) + Power(s,2)*(175 - 57*s1) + 
                  51*s1 - 12*Power(s1,2) + 
                  s*(741 - 61*s1 + 4*Power(s1,2)))*Power(t2,5) + 
               (274 - 84*Power(s,3) + Power(s,2)*(735 - 46*s1) - 
                  60*s1 + 12*Power(s1,2) + 
                  s*(-887 + 146*s1 - 18*Power(s1,2)))*Power(t2,6) + 
               (29 + 58*Power(s,3) + 82*s1 + 2*Power(s1,2) + 
                  Power(s,2)*(1 + 44*s1) + 
                  2*s*(-104 - 55*s1 + 3*Power(s1,2)))*Power(t2,7) + 
               (31*Power(s,3) - 3*Power(s,2)*(47 + 8*s1) + 
                  s*(110 + 40*s1 + Power(s1,2)) - 
                  2*(8 + 11*s1 + 2*Power(s1,2)))*Power(t2,8) + 
               (-3 - 14*Power(s,2) - 2*s1 + s*(24 + 5*s1))*Power(t2,9)) + 
            Power(t1,2)*(60 - 12*(-7 + 3*s + 3*s1)*t2 - 
               6*(-33 + 24*Power(s,2) + 8*s1 - 18*Power(s1,2) + 
                  s*(-85 + 18*s1))*Power(t2,2) - 
               2*(495 + 30*Power(s,3) + 2*s1 + 109*Power(s1,2) + 
                  6*Power(s1,3) + 15*Power(s,2)*(-13 + 6*s1) - 
                  2*s*(312 + 37*s1 + 15*Power(s1,2)))*Power(t2,3) + 
               (-60*Power(s,3) + Power(s,2)*(1667 - 60*s1) + 
                  s*(-1964 + 465*s1 - 156*Power(s1,2)) + 
                  4*(-159 + 24*s1 + 7*Power(s1,2) + 6*Power(s1,3)))*
                Power(t2,4) + 
               (2478 + 235*Power(s,3) - 191*s1 + 154*Power(s1,2) - 
                  2*Power(s1,3) + Power(s,2)*(515 + 218*s1) + 
                  s*(-4655 + 181*s1 - 43*Power(s1,2)))*Power(t2,5) + 
               (-892 + 401*Power(s,3) + 959*s1 - 87*Power(s1,2) - 
                  10*Power(s1,3) + Power(s,2)*(-2934 + 274*s1) + 
                  s*(2800 - 1498*s1 + 213*Power(s1,2)))*Power(t2,6) - 
               (296 + 56*Power(s,3) + 927*s1 - 25*Power(s1,2) + 
                  2*Power(s1,3) + 2*Power(s,2)*(765 + 158*s1) + 
                  s*(-3001 - 990*s1 + 80*Power(s1,2)))*Power(t2,7) + 
               (-55 - 255*Power(s,3) + 98*s1 - 40*Power(s1,2) + 
                  Power(s,2)*(797 + 130*s1) + 
                  s*(-213 - 24*s1 + 32*Power(s1,2)))*Power(t2,8) + 
               (39 - 45*Power(s,3) + 47*s1 + 29*Power(s1,2) + 
                  2*Power(s1,3) + 6*Power(s,2)*(55 + 12*s1) - 
                  s*(402 + 173*s1 + 29*Power(s1,2)))*Power(t2,9) + 
               (10 + 18*Power(s,2) + 6*s1 + Power(s1,2) - 
                  s*(43 + 14*s1))*Power(t2,10)) + 
            4*Power(t1,8)*(47 + 4*Power(s,2) + 4*Power(s1,2) + 19*t2 + 
               2*s1*(9 + 11*t2) - 2*s*(4*s1 + 11*(2 + t2))) + 
            2*Power(t1,7)*(4*Power(s,3)*(1 + 7*t2) - 
               4*Power(s1,3)*(2 + 7*t2) - 
               7*Power(s1,2)*(14 + 34*t2 + 15*Power(t2,2)) + 
               s1*(176 + 278*t2 - 148*Power(t2,2) - 57*Power(t2,3)) - 
               2*(-96 + 70*t2 + 239*Power(t2,2) + 24*Power(t2,3)) - 
               Power(s,2)*(242 + 454*t2 + 105*Power(t2,2) + 
                  4*s1*(4 + 21*t2)) + 
               s*(108 + 534*t2 + 495*Power(t2,2) + 57*Power(t2,3) + 
                  4*Power(s1,2)*(5 + 21*t2) + 
                  s1*(260 + 692*t2 + 210*Power(t2,2)))) + 
            Power(t1,6)*(-1004 - 3084*t2 - 2154*Power(t2,2) + 
               1592*Power(t2,3) + 695*Power(t2,4) + 36*Power(t2,5) - 
               2*Power(s,3)*t2*(162 + 223*t2 + 54*Power(t2,2)) + 
               4*Power(s1,3)*
                (-30 - 57*t2 + 44*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s1,2)*(-232 - 328*t2 + 446*Power(t2,2) + 
                  672*Power(t2,3) + 167*Power(t2,4)) + 
               2*s1*(252 + 486*t2 - 751*Power(t2,2) + 81*Power(t2,3) + 
                  139*Power(t2,4) + 13*Power(t2,5)) + 
               Power(s,2)*(-536 + 264*t2 + 2970*Power(t2,2) + 
                  1692*Power(t2,3) + 167*Power(t2,4) + 
                  12*s1*(-26 + 11*t2 + 89*Power(t2,2) + 27*Power(t2,3))) \
- 2*s*(-700 - 1110*t2 + 706*Power(t2,2) + 1759*Power(t2,3) + 
                  410*Power(t2,4) + 13*Power(t2,5) + 
                  Power(s1,2)*
                   (-184 - 210*t2 + 399*Power(t2,2) + 162*Power(t2,3)) + 
                  s1*(-416 - 688*t2 + 1124*Power(t2,2) + 
                     1182*Power(t2,3) + 167*Power(t2,4)))) + 
            Power(t1,5)*(-176 - 2024*t2 + 3980*Power(t2,2) + 
               4746*Power(t2,3) + 271*Power(t2,4) - 1060*Power(t2,5) - 
               180*Power(t2,6) - 3*Power(t2,7) + 
               Power(s1,3)*(96 + 108*t2 + 194*Power(t2,2) + 
                  138*Power(t2,3) - 199*Power(t2,4) - 55*Power(t2,5)) + 
               Power(s,3)*(8 - 100*t2 + 312*Power(t2,2) + 
                  958*Power(t2,3) + 566*Power(t2,4) + 55*Power(t2,5)) - 
               Power(s1,2)*(1008 + 1272*t2 - 962*Power(t2,2) - 
                  844*Power(t2,3) + 626*Power(t2,4) + 343*Power(t2,5) + 
                  33*Power(t2,6)) - 
               2*s1*(-76 - 1076*t2 + 743*Power(t2,2) - 
                  331*Power(t2,3) - 45*Power(t2,4) + 197*Power(t2,5) + 
                  31*Power(t2,6) + Power(t2,7)) - 
               Power(s,2)*(-608 - 1544*t2 - 4506*Power(t2,2) + 
                  1992*Power(t2,3) + 4189*Power(t2,4) + 
                  943*Power(t2,5) + 33*Power(t2,6) + 
                  s1*(560 - 596*t2 - 610*Power(t2,2) + 
                     1090*Power(t2,3) + 1331*Power(t2,4) + 
                     165*Power(t2,5))) + 
               s*(-1192 - 2904*t2 - 11470*Power(t2,2) - 
                  3462*Power(t2,3) + 5096*Power(t2,4) + 
                  2360*Power(t2,5) + 195*Power(t2,6) + 2*Power(t2,7) + 
                  Power(s1,2)*
                   (328 - 540*t2 - 780*Power(t2,2) - 6*Power(t2,3) + 
                     964*Power(t2,4) + 165*Power(t2,5)) + 
                  s1*(1552 + 1232*t2 - 3500*Power(t2,2) - 
                     2644*Power(t2,3) + 3405*Power(t2,4) + 
                     1286*Power(t2,5) + 66*Power(t2,6)))) - 
            Power(t1,4)*(-308 - 3020*t2 + 908*Power(t2,2) - 
               1438*Power(t2,3) + 3769*Power(t2,4) + 1594*Power(t2,5) - 
               417*Power(t2,6) - 249*Power(t2,7) - 14*Power(t2,8) + 
               Power(s1,3)*(24 - 212*t2 + 228*Power(t2,2) + 
                  84*Power(t2,3) + 82*Power(t2,4) + 14*Power(t2,5) - 
                  85*Power(t2,6) - 7*Power(t2,7)) + 
               Power(s,3)*(16 - 276*t2 - 58*Power(t2,2) - 
                  300*Power(t2,3) + 550*Power(t2,4) + 981*Power(t2,5) + 
                  237*Power(t2,6) + 7*Power(t2,7)) + 
               s1*(288 + 1536*t2 - 4294*Power(t2,2) + 
                  3894*Power(t2,3) + 362*Power(t2,4) + 93*Power(t2,5) - 
                  370*Power(t2,6) - 101*Power(t2,7) - 4*Power(t2,8)) - 
               Power(s1,2)*(-328 + 24*t2 + 518*Power(t2,2) + 
                  1966*Power(t2,3) - 1799*Power(t2,4) + 
                  46*Power(t2,5) + 305*Power(t2,6) + 57*Power(t2,7) + 
                  2*Power(t2,8)) - 
               Power(s,2)*(104 + 456*t2 + 22*Power(t2,2) - 
                  7934*Power(t2,3) - 4046*Power(t2,4) + 
                  4009*Power(t2,5) + 2017*Power(t2,6) + 
                  177*Power(t2,7) + 2*Power(t2,8) + 
                  s1*(200 - 244*t2 + 456*Power(t2,2) - 
                     204*Power(t2,3) - 778*Power(t2,4) + 
                     1358*Power(t2,5) + 559*Power(t2,6) + 21*Power(t2,7)\
)) + s*(616 + 6000*t2 + 914*Power(t2,2) - 12806*Power(t2,3) - 
                  11924*Power(t2,4) + 1745*Power(t2,5) + 
                  3160*Power(t2,6) + 527*Power(t2,7) + 14*Power(t2,8) + 
                  Power(s1,2)*
                   (-288 + 1652*t2 - 1794*Power(t2,2) + 
                     396*Power(t2,3) - 790*Power(t2,4) + 
                     363*Power(t2,5) + 407*Power(t2,6) + 21*Power(t2,7)) \
+ s1*(288 - 3904*t2 + 7756*Power(t2,2) + 1296*Power(t2,3) - 
                     6257*Power(t2,4) + 629*Power(t2,5) + 
                     1736*Power(t2,6) + 234*Power(t2,7) + 4*Power(t2,8)))\
) + Power(t1,3)*(240 + 672*t2 + 1124*Power(t2,2) - 4654*Power(t2,3) + 
               1339*Power(t2,4) + 281*Power(t2,5) + 1134*Power(t2,6) + 
               35*Power(t2,7) - 128*Power(t2,8) - 19*Power(t2,9) + 
               Power(s1,3)*t2*
                (-4 + 2*t2 - 86*Power(t2,2) + 69*Power(t2,3) + 
                  49*Power(t2,4) + 4*Power(t2,5) - 12*Power(t2,6) - 
                  10*Power(t2,7)) + 
               Power(s,3)*t2*(-4 - 52*t2 - 162*Power(t2,2) - 
                  484*Power(t2,3) - 295*Power(t2,4) + 645*Power(t2,5) + 
                  375*Power(t2,6) + 29*Power(t2,7)) - 
               Power(s1,2)*(108 - 324*t2 - 208*Power(t2,2) + 
                  384*Power(t2,3) + 6*Power(t2,4) + 495*Power(t2,5) - 
                  604*Power(t2,6) + 176*Power(t2,7) + 44*Power(t2,8) + 
                  3*Power(t2,9)) - 
               s1*(24 + 404*t2 - 382*Power(t2,2) - 1200*Power(t2,3) + 
                  3949*Power(t2,4) - 3121*Power(t2,5) - 
                  144*Power(t2,6) + 193*Power(t2,7) + 99*Power(t2,8) + 
                  6*Power(t2,9)) - 
               Power(s,2)*(-36 - 36*(-11 + s1)*t2 - 
                  2*(-564 + 77*s1)*Power(t2,2) + 
                  (2044 + 66*s1)*Power(t2,3) + 
                  (-3453 + 875*s1)*Power(t2,4) - 
                  (6703 + 733*s1)*Power(t2,5) + 
                  (462 - 13*s1)*Power(t2,6) + 
                  (1982 + 573*s1)*Power(t2,7) + 
                  (354 + 68*s1)*Power(t2,8) + 10*Power(t2,9)) + 
               s*(-144 - 340*t2 + 904*Power(t2,2) + 9672*Power(t2,3) - 
                  1945*Power(t2,4) - 10883*Power(t2,5) - 
                  2884*Power(t2,6) + 1847*Power(t2,7) + 663*Power(t2,8) + 
                  36*Power(t2,9) + 
                  Power(s1,2)*t2*
                   (36 + 40*t2 + 634*Power(t2,2) - 942*Power(t2,3) + 
                     269*Power(t2,4) - 283*Power(t2,5) + 
                     210*Power(t2,6) + 49*Power(t2,7)) + 
                  s1*(-216 - 88*t2 - 936*Power(t2,2) - 1780*Power(t2,3) + 
                     6105*Power(t2,4) - 1994*Power(t2,5) - 
                     1877*Power(t2,6) + 918*Power(t2,7) + 
                     302*Power(t2,8) + 13*Power(t2,9))))))*
       T2(t1,1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*Power(1 - s2 + t1 - t2,2)*
       Power(t1 - s2*t2,3)*Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - 
         Power(t2,2),2)) + (8*(-(Power(t1,9)*
            (1 + Power(s,2) + Power(s1,2) + s2 + t2 + s1*(s2 + t2) - 
              s*(2 + 2*s1 + s2 + t2))) + 
         Power(t2,3)*(-(Power(1 + t2,2)*Power(1 + (-1 + s)*t2,3)*
               (2 - 3*t2 + Power(t2,2))) + 
            Power(s2,7)*t2*(-4 + Power(t2,2) + s1*(4 + t2)) + 
            Power(s2,6)*(4 + (26 + 4*s)*t2 - 5*Power(t2,2) - 
               (1 + 2*s)*Power(t2,3) + 5*Power(t2,4) + 
               2*Power(s1,2)*(1 + t2) + 
               s1*(-6 - 2*(13 + 2*s)*t2 - (-12 + s)*Power(t2,2) + 
                  Power(t2,3))) + 
            s2*(1 + (-1 + s)*t2)*
             (3 + (4 + 13*s)*t2 + 
               (-7 + 15*s + 4*Power(s,2))*Power(t2,2) + 
               (-11 - 10*s + 3*Power(s,2))*Power(t2,3) - 
               (-11 + 25*s + Power(s,2))*Power(t2,4) + 
               (1 + 3*s - 3*Power(s,2))*Power(t2,5) + 
               (-1 + 4*s)*Power(t2,6) - 
               s1*(-1 + Power(t2,2))*
                (6 - 8*t2 - 5*s*Power(t2,2) + (2 + s)*Power(t2,3))) + 
            Power(s2,5)*(-6 - Power(s1,3) - 2*(6 + s)*t2 + 
               (62 + 19*s)*Power(t2,2) + 
               (7 + s + Power(s,2))*Power(t2,3) - 
               3*(1 + 4*s)*Power(t2,4) + 9*Power(t2,5) + 
               Power(s1,2)*(4 + (15 + s)*t2 + 12*Power(t2,2)) - 
               s1*(-4 + 8*t2 + (90 + 17*s)*Power(t2,2) + 
                  (-12 + s)*Power(t2,3) + 3*Power(t2,4))) - 
            Power(s2,4)*(2 + (22 + 4*s)*t2 + 
               (40 + 4*s - 4*Power(s,2))*Power(t2,2) - 
               (47 + 38*s + 2*Power(s,2))*Power(t2,3) + 
               (-8 + s - 8*Power(s,2))*Power(t2,4) + 
               (4 + 23*s)*Power(t2,5) - 7*Power(t2,6) + 
               Power(s1,3)*(-3 + 4*t2 + 2*Power(t2,2)) + 
               Power(s1,2)*(23 + 9*t2 - (30 + 7*s)*Power(t2,2) - 
                  22*Power(t2,3)) + 
               s1*(-16 - 3*(12 + s)*t2 + 
                  (-12 + 11*s + Power(s,2))*Power(t2,2) + 
                  (109 + 29*s + Power(s,2))*Power(t2,3) - 
                  (7 + 2*s)*Power(t2,4) + 5*Power(t2,5))) + 
            Power(s2,3)*(4 + (13 + 4*s)*t2 + 
               (2 + 4*s - 2*Power(s,2))*Power(t2,2) + 
               (-37 + 40*s + 8*Power(s,2) - 2*Power(s,3))*Power(t2,3) + 
               (17 + 61*s + 5*Power(s,2) - Power(s,3))*Power(t2,4) + 
               (1 - 9*s + 17*Power(s,2))*Power(t2,5) - 
               2*(1 + 9*s)*Power(t2,6) + 2*Power(t2,7) - 
               2*Power(s1,3)*(1 - 2*t2 + Power(t2,3)) + 
               Power(s1,2)*(23 - (21 + s)*t2 - 
                  2*(15 + 4*s)*Power(t2,2) + (15 + 8*s)*Power(t2,3) + 
                  13*Power(t2,4)) + 
               s1*(-11 + 6*(1 + 2*s)*t2 + 
                  (34 + 29*s + Power(s,2))*Power(t2,2) + 
                  3*(7 - 11*s + Power(s,2))*Power(t2,3) - 
                  (54 + 22*s + 3*Power(s,2))*Power(t2,4) + 
                  3*(2 + s)*Power(t2,5) - 2*Power(t2,6))) + 
            Power(s2,2)*(-1 - (7 + 12*s)*t2 - 
               (2 + 35*s + 13*Power(s,2))*Power(t2,2) + 
               (34 - 40*s - 24*Power(s,2))*Power(t2,3) - 
               (27 - 60*s + 16*Power(s,2) + 4*Power(s,3))*Power(t2,4) + 
               (1 + 44*s + 6*Power(s,2) - 3*Power(s,3))*Power(t2,5) + 
               2*(1 - 6*s + 7*Power(s,2))*Power(t2,6) - 
               5*s*Power(t2,7) + 
               Power(s1,2)*(-1 + t2)*
                (6 - 7*t2 - 5*Power(t2,2) + (5 + 2*s)*Power(t2,3) + 
                  Power(t2,4)) + 
               s1*(-9 + (2 - 17*s)*t2 + 15*(1 + s)*Power(t2,2) - 
                  (4 - 43*s + Power(s,2))*Power(t2,3) + 
                  (3 - 35*s + 9*Power(s,2))*Power(t2,4) - 
                  (10 + 7*s + 3*Power(s,2))*Power(t2,5) + 
                  (3 + s)*Power(t2,6)))) + 
         Power(t1,8)*(-11 + 7*s2 + 3*Power(s2,2) + 4*t2 + 14*s2*t2 + 
            3*Power(s2,2)*t2 + 8*Power(t2,2) + 3*s2*Power(t2,2) + 
            Power(s1,3)*(1 + t2) - Power(s,3)*(2 + t2) + 
            Power(s,2)*(-1 + 13*t2 + 2*Power(t2,2) + s1*(5 + 3*t2) + 
               s2*(5 + 3*t2)) + 
            Power(s1,2)*(s2*(4 + 3*t2) + 2*(-5 + 3*t2 + Power(t2,2))) + 
            s1*(6 - 2*t2 + 5*Power(t2,2) + 2*Power(s2,2)*(2 + t2) + 
               s2*(-6 + 9*t2 + 2*Power(t2,2))) - 
            s*(-8 + 19*t2 + 10*Power(t2,2) + Power(s2,2)*(3 + 2*t2) + 
               Power(s1,2)*(4 + 3*t2) + s2*(9 + 17*t2 + 2*Power(t2,2)) + 
               s1*(-13 + 19*t2 + 4*Power(t2,2) + s2*(9 + 6*t2)))) + 
         Power(t1,2)*t2*(-6 - 3*(-13 - s1 + s*(5 + 6*s1))*t2 + 
            (-(Power(s,2)*(29 + 22*s1)) + 
               s*(118 + 34*s1 - 4*Power(s1,2)) + 
               2*(-23 + s1 + Power(s1,2)))*Power(t2,2) - 
            (54 + 24*Power(s,3) + 59*s1 + 5*Power(s1,2) + 
               2*Power(s,2)*(-58 + 7*s1) + 
               s*(77 - 90*s1 - 7*Power(s1,2)))*Power(t2,3) + 
            (104 - 11*Power(s,3) + 87*s1 + 2*Power(s1,2) + 
               Power(s,2)*(130 + 73*s1) - 
               s*(301 + 171*s1 + 2*Power(s1,2)))*Power(t2,4) + 
            (-9 + 62*Power(s,3) - 6*s1 + 6*Power(s1,2) - 
               Power(s,2)*(253 + 17*s1) + 
               s*(221 + 18*s1 - 6*Power(s1,2)))*Power(t2,5) + 
            (-40 + 8*Power(s,3) - 41*s1 - 8*Power(s1,2) - 
               Power(s,2)*(68 + 33*s1) + 
               2*s*(59 + 41*s1 + 4*Power(s1,2)))*Power(t2,6) + 
            (12 - 21*Power(s,3) + 14*s1 + 3*Power(s1,2) + 
               Power(s,2)*(71 + 18*s1) - s*(64 + 35*s1 + 3*Power(s1,2))\
)*Power(t2,7) + Power(s2,7)*Power(t2,2)*(4 - s1 + 3*t2) + 
            Power(s2,6)*t2*(-12 - (19 + 4*s)*t2 + 
               (32 - 9*s)*Power(t2,2) + 24*Power(t2,3) + 
               s1*(12 + (30 + s)*t2 - 5*Power(t2,2))) + 
            Power(s2,4)*(12 + 6*(16 + s)*t2 + 
               (255 + 104*s + 8*Power(s,2))*Power(t2,2) + 
               (-82 + 103*s + 15*Power(s,2))*Power(t2,3) + 
               (-108 - 184*s + 67*Power(s,2))*Power(t2,4) + 
               (141 - 202*s)*Power(t2,5) + 67*Power(t2,6) + 
               Power(s1,3)*(-9 + 42*t2 + 28*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s1,2)*(-3 - 6*(3 + s)*t2 - 
                  (39 + 25*s)*Power(t2,2) - (55 + 12*s)*Power(t2,3) + 
                  7*Power(t2,4)) - 
               s1*(18 + 3*(46 + 5*s)*t2 + 
                  (300 + 127*s - 7*Power(s,2))*Power(t2,2) + 
                  (-89 + 30*s + Power(s,2))*Power(t2,3) + 
                  3*(-43 + 8*s)*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s2,3)*(-18 - 3*(21 + 4*s)*t2 + 
               (133 + 116*s + 25*Power(s,2))*Power(t2,2) + 
               (350 + 539*s + 71*Power(s,2) - 16*Power(s,3))*
                Power(t2,3) + 
               (-113 + 355*s + 120*Power(s,2) - 14*Power(s,3))*
                Power(t2,4) + 
               (-88 - 395*s + 195*Power(s,2))*Power(t2,5) + 
               (112 - 213*s)*Power(t2,6) + 25*Power(t2,7) + 
               Power(s1,3)*t2*
                (-111 + 19*t2 + 45*Power(t2,2) + 13*Power(t2,3)) + 
               Power(s1,2)*(39 + 9*(22 + 5*s)*t2 + 
                  3*(43 + 9*s)*Power(t2,2) + (22 - 57*s)*Power(t2,3) - 
                  (52 + 29*s)*Power(t2,4) + 30*Power(t2,5)) + 
               s1*(21 + (-66 + 63*s)*t2 + 
                  (-360 - 21*s + Power(s,2))*Power(t2,2) + 
                  (-713 - 324*s + 44*Power(s,2))*Power(t2,3) + 
                  (24 - 23*s + 13*Power(s,2))*Power(t2,4) + 
                  (47 - 97*s)*Power(t2,5) - 3*Power(t2,6))) + 
            Power(s2,2)*(-9 - 3*(32 + 15*s)*t2 - 
               3*(35 + 58*s + 31*Power(s,2))*Power(t2,2) + 
               (178 + 208*s - 185*Power(s,2) - 22*Power(s,3))*
                Power(t2,3) + 
               (51 + 1045*s - 128*Power(s,2) - 58*Power(s,3))*
                Power(t2,4) + 
               (-103 + 361*s + 257*Power(s,2) - 50*Power(s,3))*
                Power(t2,5) + 
               (10 - 365*s + 207*Power(s,2))*Power(t2,6) + 
               (38 - 76*s)*Power(t2,7) + 
               Power(s1,3)*(18 + 45*t2 - 97*Power(t2,2) - 
                  12*Power(t2,3) + 18*Power(t2,4) + 10*Power(t2,5)) + 
               Power(s1,2)*(-78 - 3*(39 + 16*s)*t2 + 
                  (202 + 71*s)*Power(t2,2) + (87 + 91*s)*Power(t2,3) + 
                  (12 - 52*s)*Power(t2,4) - (4 + 35*s)*Power(t2,5) + 
                  18*Power(t2,6)) + 
               s1*(21 + (312 - 75*s)*t2 + 
                  (155 + 150*s - 55*Power(s,2))*Power(t2,2) + 
                  (-251 - 4*s + 6*Power(s,2))*Power(t2,3) + 
                  (-516 - 159*s + 77*Power(s,2))*Power(t2,4) + 
                  (-10 - 47*s + 48*Power(s,2))*Power(t2,5) + 
                  (30 - 96*s)*Power(t2,6) + Power(t2,7))) + 
            s2*(21 + 6*(2 + 9*s)*t2 + 
               (-80 - 181*s + 89*Power(s,2))*Power(t2,2) + 
               (57 - 542*s + 37*Power(s,2) + 62*Power(s,3))*
                Power(t2,3) + 
               (44 + 269*s - 300*Power(s,2) + 20*Power(s,3))*
                Power(t2,4) - 
               (46 - 843*s + 363*Power(s,2) + 47*Power(s,3))*
                Power(t2,5) - 
               (30 + 67*s - 201*Power(s,2) + 57*Power(s,3))*
                Power(t2,6) + 
               2*(11 - 65*s + 36*Power(s,2))*Power(t2,7) + 
               2*Power(s1,3)*
                (-6 + 12*t2 - Power(t2,2) - 5*Power(t2,3) - 
                  Power(t2,4) + Power(t2,6)) + 
               Power(s1,2)*(42 + 12*(-4 + s)*t2 - 
                  (73 + 77*s)*Power(t2,2) + (83 + 31*s)*Power(t2,3) + 
                  (15 + 64*s)*Power(t2,4) - 2*(23 + 8*s)*Power(t2,5) + 
                  (26 - 17*s)*Power(t2,6) + Power(t2,7)) + 
               s1*(-24 + 3*(-41 + 19*s)*t2 + 
                  (149 - 12*s + 69*Power(s,2))*Power(t2,2) + 
                  (239 + 22*s - 95*Power(s,2))*Power(t2,3) - 
                  (77 + 188*s + 10*Power(s,2))*Power(t2,4) + 
                  (-205 + 188*s + 28*Power(s,2))*Power(t2,5) + 
                  (29 - 71*s + 51*Power(s,2))*Power(t2,6) + 
                  (12 - 29*s)*Power(t2,7))) + 
            Power(s2,5)*(3*Power(s1,3) - 
               Power(s1,2)*t2*
                (15 + 25*t2 + 4*Power(t2,2) + s*(3 + 2*t2)) + 
               s1*t2*(t2*(89 + 132*t2 - 5*Power(t2,2)) + 
                  s*(-12 - 5*t2 + 6*Power(t2,2))) + 
               t2*(24 - 50*t2 - 73*Power(t2,2) + 
                  6*Power(s,2)*Power(t2,2) + 91*Power(t2,3) + 
                  63*Power(t2,4) + 
                  s*(12 + 11*t2 - 42*Power(t2,2) - 75*Power(t2,3))))) + 
         Power(t1,7)*(-5 + 42*s2 - 12*Power(s2,2) - 3*Power(s2,3) + 
            53*t2 + 16*s2*t2 - 45*Power(s2,2)*t2 - 9*Power(s2,3)*t2 - 
            58*s2*Power(t2,2) - 35*Power(s2,2)*Power(t2,2) - 
            3*Power(s2,3)*Power(t2,2) - 25*Power(t2,3) - 
            23*s2*Power(t2,3) - 3*Power(s2,2)*Power(t2,3) - 
            Power(s1,3)*(2 + t2)*(1 + s2 + t2 + 2*s2*t2 + Power(t2,2)) + 
            Power(s,3)*(t2*(8 + 7*t2 + Power(t2,2)) + 
               s2*(3 + 7*t2 + 2*Power(t2,2))) - 
            Power(s1,2)*(5 - 52*t2 + 14*Power(t2,2) + 9*Power(t2,3) + 
               2*Power(s2,2)*(3 + 5*t2 + Power(t2,2)) + 
               s2*(-22 - t2 + 21*Power(t2,2) + 2*Power(t2,3))) - 
            s1*(Power(s2,3)*(6 + 8*t2 + Power(t2,2)) + 
               Power(s2,2)*(-16 + 6*t2 + 18*Power(t2,2) + 
                  Power(t2,3)) + 
               2*(-3 + 7*t2 - 5*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(33 - 2*t2 + 25*Power(t2,2) + 10*Power(t2,3))) - 
            Power(s,2)*(-8 + 5*(-7 + 3*s1)*t2 + 
               (57 + 17*s1)*Power(t2,2) + (16 + 3*s1)*Power(t2,3) + 
               Power(s2,2)*(5 + 10*t2 + 2*Power(t2,2)) + 
               s2*(-3 + 46*t2 + 36*Power(t2,2) + 2*Power(t2,3) + 
                  s1*(8 + 19*t2 + 6*Power(t2,2)))) + 
            s*(-22 - 94*t2 + 67*Power(t2,2) + 44*Power(t2,3) + 
               Power(s2,3)*(3 + 6*t2 + Power(t2,2)) + 
               Power(s2,2)*(8 + 46*t2 + 28*Power(t2,2) + Power(t2,3)) + 
               s2*(-13 + 49*t2 + 106*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s1,2)*(2 + 10*t2 + 13*Power(t2,2) + 
                  3*Power(t2,3) + s2*(7 + 17*t2 + 6*Power(t2,2))) + 
               s1*(17 - 102*t2 + 59*Power(t2,2) + 25*Power(t2,3) + 
                  Power(s2,2)*(13 + 20*t2 + 4*Power(t2,2)) + 
                  s2*(-40 + 35*t2 + 57*Power(t2,2) + 4*Power(t2,3))))) - 
         Power(t1,3)*(2 + 41*t2 - s*t2 - 74*Power(t2,2) + 
            96*s*Power(t2,2) - 17*Power(s,2)*Power(t2,2) - 
            31*Power(t2,3) - 294*s*Power(t2,3) + 
            209*Power(s,2)*Power(t2,3) - 26*Power(s,3)*Power(t2,3) + 
            Power(s2,7)*Power(t2,3) + 117*Power(t2,4) - 
            316*s*Power(t2,4) + 110*Power(s,2)*Power(t2,4) - 
            14*Power(s,3)*Power(t2,4) - 22*Power(t2,5) + 
            586*s*Power(t2,5) - 533*Power(s,2)*Power(t2,5) + 
            98*Power(s,3)*Power(t2,5) - 54*Power(t2,6) + 
            141*s*Power(t2,6) - 47*Power(s,2)*Power(t2,6) - 
            Power(s,3)*Power(t2,6) + 21*Power(t2,7) - 
            130*s*Power(t2,7) + 132*Power(s,2)*Power(t2,7) - 
            35*Power(s,3)*Power(t2,7) + 
            Power(s2,6)*Power(t2,2)*
             (11 + (22 - 5*s)*t2 + 14*Power(t2,2)) + 
            Power(s2,5)*t2*(-12 - (78 + 11*s)*t2 + 
               (11 - 34*s + 4*Power(s,2))*Power(t2,2) + 
               (117 - 57*s)*Power(t2,3) + 51*Power(t2,4)) + 
            Power(s2,3)*(4 + 45*t2 + 
               (273 + 136*s + 21*Power(s,2))*Power(t2,2) + 
               (97 + 462*s + 70*Power(s,2) - 14*Power(s,3))*
                Power(t2,3) + 
               (-394 + 103*s + 179*Power(s,2) - 16*Power(s,3))*
                Power(t2,4) + 
               (38 - 656*s + 220*Power(s,2))*Power(t2,5) + 
               (231 - 253*s)*Power(t2,6) + 30*Power(t2,7)) - 
            Power(s2,2)*(7 + (46 + 29*s)*t2 + 
               (-9 - 4*s + 55*Power(s,2))*Power(t2,2) + 
               (-476 - 593*s + 156*Power(s,2) + 38*Power(s,3))*
                Power(t2,3) + 
               2*(44 - 654*s + 40*Power(s,2) + 41*Power(s,3))*
                Power(t2,4) + 
               (311 - 107*s - 488*Power(s,2) + 70*Power(s,3))*
                Power(t2,5) - 
               4*(26 - 165*s + 70*Power(s,2))*Power(t2,6) + 
               5*(-17 + 21*s)*Power(t2,7)) + 
            s2*(1 + 6*(-10 + 3*s)*t2 + 
               (-74 - 281*s + 49*Power(s,2))*Power(t2,2) + 
               (127 - 474*s - 60*Power(s,2) + 78*Power(s,3))*
                Power(t2,3) + 
               (93 + 866*s - 458*Power(s,2) + 7*Power(s,3))*
                Power(t2,4) - 
               (119 - 1341*s + 396*Power(s,2) + 105*Power(s,3))*
                Power(t2,5) - 
               (51 + 286*s - 408*Power(s,2) + 90*Power(s,3))*
                Power(t2,6) + (59 - 242*s + 110*Power(s,2))*Power(t2,7)) \
+ Power(s2,4)*t2*(32 + 41*t2 - 259*Power(t2,2) + Power(t2,3) + 
               244*Power(t2,4) + 68*Power(t2,5) + 
               Power(s,2)*t2*(2 + 13*t2 + 59*Power(t2,2)) + 
               s*(12 + 46*t2 - 28*Power(t2,2) - 254*Power(t2,3) - 
                  197*Power(t2,4))) + 
            Power(s1,3)*(-4 + Power(s2,5) + 8*t2 - 5*Power(t2,3) + 
               2*Power(t2,5) - 2*Power(t2,6) + Power(t2,7) + 
               Power(s2,4)*(-3 + 10*t2 + 8*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,3)*(2 + 11*t2 + 57*Power(t2,2) + 
                  43*Power(t2,3) + 25*Power(t2,4)) + 
               Power(s2,2)*(-2 - 87*t2 - 39*Power(t2,2) + 
                  19*Power(t2,3) + 33*Power(t2,4) + 30*Power(t2,5)) + 
               s2*(6 + 58*t2 - 94*Power(t2,2) + 13*Power(t2,3) - 
                  8*Power(t2,4) + 2*Power(t2,5) + 11*Power(t2,6))) + 
            Power(s1,2)*(14 - 2*(7 + s)*t2 - 2*(9 + 14*s)*Power(t2,2) + 
               3*(-5 + 17*s)*Power(t2,3) + (52 - 23*s)*Power(t2,4) - 
               (4 + 15*s)*Power(t2,5) + (-28 + 31*s)*Power(t2,6) + 
               (13 - 15*s)*Power(t2,7) + 
               Power(s2,5)*t2*(1 - s + 3*t2 + 4*Power(t2,2)) + 
               Power(s2,4)*(-3 - 2*(11 + 6*s)*t2 - 
                  (73 + 25*s)*Power(t2,2) - (43 + 12*s)*Power(t2,3) + 
                  21*Power(t2,4)) + 
               Power(s2,3)*(11 + (-50 + 43*s)*t2 - 
                  (87 + 37*s)*Power(t2,2) - (154 + 95*s)*Power(t2,3) - 
                  (70 + 57*s)*Power(t2,4) + 58*Power(t2,5)) + 
               Power(s2,2)*(-2 - 5*(-57 + 8*s)*t2 + 
                  5*(50 + 33*s)*Power(t2,2) + 
                  (-6 + 43*s)*Power(t2,3) - 
                  (189 + 109*s)*Power(t2,4) + 
                  (11 - 97*s)*Power(t2,5) + 45*Power(t2,6)) + 
               s2*(-20 + 4*(-50 + 3*s)*t2 + (169 - 85*s)*Power(t2,2) + 
                  (206 + 73*s)*Power(t2,3) + (11 + 104*s)*Power(t2,4) - 
                  (160 + 27*s)*Power(t2,5) + (68 - 64*s)*Power(t2,6) + 
                  6*Power(t2,7))) + 
            s1*(-14 - (5 + 18*s)*t2 + 
               (22 + 110*s - 28*Power(s,2))*Power(t2,2) + 
               Power(s2,7)*Power(t2,2) + 
               (-52 + 133*s - 41*Power(s,2))*Power(t2,3) + 
               (178 - 482*s + 165*Power(s,2))*Power(t2,4) + 
               (-88 + 180*s - 63*Power(s,2))*Power(t2,5) + 
               (-71 + 155*s - 57*Power(s,2))*Power(t2,6) + 
               (30 - 89*s + 45*Power(s,2))*Power(t2,7) + 
               Power(s2,6)*Power(t2,2)*(-6 - s + t2) + 
               Power(s2,5)*t2*
                (12 + (91 + 9*s)*t2 + (38 - 4*s)*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s2,3)*(-3 + (-72 + 23*s)*t2 + 
                  (-336 - 63*s + 17*Power(s,2))*Power(t2,2) + 
                  (-293 - 179*s + 50*Power(s,2))*Power(t2,3) + 
                  (290 + 13*s + 39*Power(s,2))*Power(t2,4) + 
                  (57 - 182*s)*Power(t2,5) + 19*Power(t2,6)) + 
               Power(s2,2)*(-5 - (24 + 41*s)*t2 + 
                  (-151 + 22*s - 93*Power(s,2))*Power(t2,2) + 
                  (-698 - 165*s + 40*Power(s,2))*Power(t2,3) + 
                  (-390 + 106*s + 117*Power(s,2))*Power(t2,4) + 
                  (65 - 152*s + 122*Power(s,2))*Power(t2,5) + 
                  (48 - 208*s)*Power(t2,6) + 5*Power(t2,7)) + 
               s2*(22 + (99 + 49*s)*t2 + 
                  (87 - 12*s + 97*Power(s,2))*Power(t2,2) + 
                  (167 - 105*s - 132*Power(s,2))*Power(t2,3) - 
                  (339 + 279*s + 61*Power(s,2))*Power(t2,4) + 
                  (-270 + 463*s + 71*Power(s,2))*Power(t2,5) + 
                  (32 - 207*s + 130*Power(s,2))*Power(t2,6) + 
                  (30 - 70*s)*Power(t2,7)) + 
               Power(s2,4)*t2*
                (-10 + 8*t2 + 325*Power(t2,2) + 99*Power(t2,3) + 
                  24*Power(t2,4) + Power(s,2)*t2*(7 + 3*t2) - 
                  s*(13 + 45*t2 - 40*Power(t2,2) + 48*Power(t2,3))))) - 
         t1*Power(t2,2)*(Power(s2,7)*t2*
             (-4 - s1*(-4 + t2) + 4*t2 + 3*Power(t2,2)) - 
            (1 + (-1 + s)*t2)*
             (6 + (-17 - s1 + s*(11 + 6*s1))*t2 + 
               (-7 + 11*Power(s,2) + 2*s*(-13 + s1) - 3*s1)*
                Power(t2,2) + 
               (37 + 5*Power(s,2) + 8*s1 - s*(38 + 13*s1))*
                Power(t2,3) + 
               (-10 - 21*Power(s,2) + s*(47 + s1))*Power(t2,4) + 
               (-5*Power(s,2) - 7*(2 + s1) + 7*s*(3 + s1))*
                Power(t2,5) + 
               (5 + 7*Power(s,2) + 3*s1 - 3*s*(5 + s1))*Power(t2,6)) + 
            Power(s2,6)*t2*(-2*Power(s1,2)*(3 + t2) + 
               s*(4 + s1*(-4 + t2) - 4*t2 - 7*Power(t2,2)) + 
               s1*(8 + 48*t2 - 3*Power(t2,2)) + 
               t2*(-31 + 14*t2 + 18*Power(t2,2))) + 
            Power(s2,5)*(12 + 2*(42 + 5*s)*t2 + 
               (70 + 41*s)*Power(t2,2) + 
               4*(-12 - 4*s + Power(s,2))*Power(t2,3) + 
               (22 - 48*s)*Power(t2,4) + 38*Power(t2,5) + 
               Power(s1,3)*(3 + 8*t2 + 2*Power(t2,2)) - 
               Power(s1,2)*(1 + 2*t2)*
                (-6 + (5 + s)*t2 + 3*Power(t2,2)) - 
               s1*(18 + 12*(8 + s)*t2 + (76 + 33*s)*Power(t2,2) - 
                  2*(49 + 2*s)*Power(t2,3) + 9*Power(t2,4))) + 
            Power(s2,4)*(-18 - 2*(23 + 5*s)*t2 + 
               2*(86 + 22*s + 5*Power(s,2))*Power(t2,2) + 
               (105 + 127*s + 9*Power(s,2))*Power(t2,3) + 
               (-45 - 52*s + 37*Power(s,2))*Power(t2,4) + 
               (25 - 107*s)*Power(t2,5) + 34*Power(t2,6) + 
               Power(s1,3)*(-15 - 16*t2 + 16*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s1,2)*(3 + 2*(47 + s)*t2 + 
                  (68 + 3*s)*Power(t2,2) + (5 - 4*s)*Power(t2,3) - 
                  Power(t2,4)) - 
               s1*(-12 - (-56 + s)*t2 + 
                  (288 + 82*s - Power(s,2))*Power(t2,2) + 
                  (194 + 76*s + 3*Power(s,2))*Power(t2,3) + 
                  (-65 + s)*Power(t2,4) + 14*Power(t2,5))) + 
            Power(s2,3)*(-6 - (67 + 4*s)*t2 + 
               (-117 + 35*s + 7*Power(s,2))*Power(t2,2) + 
               (125 + 250*s + 36*Power(s,2) - 9*Power(s,3))*
                Power(t2,3) + 
               (52 + 256*s + 40*Power(s,2) - 6*Power(s,3))*
                Power(t2,4) + 
               (-40 - 111*s + 90*Power(s,2))*Power(t2,5) + 
               (18 - 96*s)*Power(t2,6) + 11*Power(t2,7) + 
               Power(s1,3)*(24 - 17*t2 - 34*Power(t2,2) + 
                  12*Power(t2,3) + 3*Power(t2,4)) + 
               Power(s1,2)*(-33 + 13*(-8 + s)*t2 + 
                  (103 + 16*s)*Power(t2,2) - (-102 + s)*Power(t2,3) + 
                  (6 - 5*s)*Power(t2,4) + 6*Power(t2,5)) + 
               s1*(57 + (198 + 53*s)*t2 + 
                  (34 + 49*s - 2*Power(s,2))*Power(t2,2) + 
                  (-351 - 181*s + 20*Power(s,2))*Power(t2,3) - 
                  (130 + 53*s + 5*Power(s,2))*Power(t2,4) - 
                  3*(-9 + 5*s)*Power(t2,5) - 7*Power(t2,6))) + 
            Power(s2,2)*(9 + (16 - 31*s)*t2 + 
               (2 - 153*s - 61*Power(s,2))*Power(t2,2) - 
               (9 + 58*s + 104*Power(s,2) + 5*Power(s,3))*Power(t2,3) - 
               (26 - 406*s + 75*Power(s,2) + 23*Power(s,3))*
                Power(t2,4) + 
               (1 + 218*s + 67*Power(s,2) - 19*Power(s,3))*
                Power(t2,5) + s*(-106 + 83*s)*Power(t2,6) + 
               (7 - 30*s)*Power(t2,7) + 
               Power(s1,3)*(-12 + 25*t2 - 2*Power(t2,2) - 
                  16*Power(t2,3) + 4*Power(t2,4) + Power(t2,5)) + 
               Power(s1,2)*(24 + (11 - 20*s)*t2 - 
                  (82 + 9*s)*Power(t2,2) + (-7 + 39*s)*Power(t2,3) + 
                  (51 - 9*s)*Power(t2,4) - 4*s*Power(t2,5) + 
                  3*Power(t2,6)) + 
               s1*(-69 + (2 - 63*s)*t2 + 
                  (173 + 117*s - 10*Power(s,2))*Power(t2,2) - 
                  6*(-17 - 16*s + Power(s,2))*Power(t2,3) + 
                  (-189 - 148*s + 38*Power(s,2))*Power(t2,4) + 
                  (-33 - 19*s + Power(s,2))*Power(t2,5) - 
                  2*(-7 + 8*s)*Power(t2,6))) + 
            s2*(9 + 6*(-1 + 8*s)*t2 + 
               (-52 - 22*s + 66*Power(s,2))*Power(t2,2) + 
               (50 - 237*s + 47*Power(s,2) + 25*Power(s,3))*
                Power(t2,3) + 
               (37 - 31*s - 102*Power(s,2) + 13*Power(s,3))*
                Power(t2,4) - 
               (34 - 280*s + 157*Power(s,2) + 11*Power(s,3))*
                Power(t2,5) + 
               (-10 + s + 54*Power(s,2) - 20*Power(s,3))*Power(t2,6) + 
               (6 - 39*s + 26*Power(s,2))*Power(t2,7) + 
               s1*(18 + (-61 + 31*s)*t2 + 
                  (14 - 47*s + 17*Power(s,2))*Power(t2,2) + 
                  (57 + 31*s - 33*Power(s,2))*Power(t2,3) + 
                  (15 - 14*s + 4*Power(s,2))*Power(t2,4) + 
                  (-58 + 17*s + 16*Power(s,2))*Power(t2,5) + 
                  (13 - 13*s + 6*Power(s,2))*Power(t2,6) + 
                  (2 - 5*s)*Power(t2,7)) - 
               Power(s1,2)*(-1 + t2)*t2*
                (s*(6 - 7*t2 - 11*Power(t2,2) + 7*Power(t2,3) + 
                     Power(t2,4)) - 
                  2*(1 + t2 - 5*Power(t2,2) + Power(t2,3) + 
                     2*Power(t2,4))))) + 
         Power(t1,6)*(15 + 34*s2 - 52*Power(s2,2) + 9*Power(s2,3) + 
            Power(s2,4) + 22*t2 - 113*s2*t2 - 109*Power(s2,2)*t2 + 
            56*Power(s2,3)*t2 + 9*Power(s2,4)*t2 - 98*Power(t2,2) - 
            161*s2*Power(t2,2) + 93*Power(s2,2)*Power(t2,2) + 
            89*Power(s2,3)*Power(t2,2) + 9*Power(s2,4)*Power(t2,2) - 
            22*Power(t2,3) + 99*s2*Power(t2,3) + 
            141*Power(s2,2)*Power(t2,3) + 32*Power(s2,3)*Power(t2,3) + 
            Power(s2,4)*Power(t2,3) + 42*Power(t2,4) + 
            70*s2*Power(t2,4) + 22*Power(s2,2)*Power(t2,4) + 
            Power(s2,3)*Power(t2,4) + 
            Power(s1,3)*(1 + 4*t2 + 5*Power(t2,2) + Power(t2,3) + 
               4*Power(t2,4) + Power(s2,2)*(1 + 9*t2 + 6*Power(t2,2)) + 
               s2*(7 + 4*t2 + 17*Power(t2,2) + 11*Power(t2,3))) - 
            Power(s,3)*(Power(s2,2)*(2 + 6*t2 + 4*Power(t2,2)) + 
               t2*(1 + 4*t2 + 20*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(-2 + 14*t2 + 41*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s1,2)*(4 + 41*t2 - 120*Power(t2,2) + 12*Power(t2,3) + 
               19*Power(t2,4) + 
               2*Power(s2,3)*(2 + 6*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-13 - 19*t2 + 37*Power(t2,2) + 
                  15*Power(t2,3)) + 
               s2*(-21 - 100*t2 - 55*Power(t2,2) + 59*Power(t2,3) + 
                  9*Power(t2,4))) + 
            s1*(-7 - 7*t2 - 16*Power(t2,2) - 26*Power(t2,3) + 
               28*Power(t2,4) + 
               4*Power(s2,4)*(1 + 3*t2 + Power(t2,2)) + 
               Power(s2,3)*(-14 - 20*t2 + 30*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(s2,2)*(49 + 76*t2 + 15*Power(t2,2) + 
                  54*Power(t2,3) + 5*Power(t2,4)) + 
               s2*(-22 + 39*t2 + 27*Power(t2,2) + 34*Power(t2,3) + 
                  24*Power(t2,4))) + 
            Power(s,2)*(21 - 40*t2 - 191*Power(t2,2) + 
               122*Power(t2,3) + 57*Power(t2,4) + 
               Power(s2,3)*(1 + 9*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(3 + 29*t2 + 89*Power(t2,2) + 
                  23*Power(t2,3)) + 
               s2*(-21 - 39*t2 + 129*Power(t2,2) + 159*Power(t2,3) + 
                  16*Power(t2,4)) + 
               s1*(2*Power(s2,2)*(4 + 12*t2 + 7*Power(t2,2)) + 
                  s2*(-3 + 7*t2 + 90*Power(t2,2) + 41*Power(t2,3)) + 
                  3*(-3 + 8*t2 - Power(t2,2) + 11*Power(t2,3) + 
                     6*Power(t2,4)))) - 
            s*(39 - 99*t2 - 373*Power(t2,2) + 110*Power(t2,3) + 
               110*Power(t2,4) + 
               Power(s2,4)*(1 + 6*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(1 + 37*t2 + 65*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(s2,2)*(5 + 27*t2 + 228*Power(t2,2) + 
                  153*Power(t2,3) + 10*Power(t2,4)) + 
               s2*(-54 - 180*t2 - 9*Power(t2,2) + 325*Power(t2,3) + 
                  84*Power(t2,4)) + 
               Power(s1,2)*(-20 + 23*t2 + 3*Power(t2,2) + 
                  14*Power(t2,3) + 15*Power(t2,4) + 
                  Power(s2,2)*(6 + 27*t2 + 16*Power(t2,2)) + 
                  s2*(14 + t2 + 66*Power(t2,2) + 37*Power(t2,3))) + 
               s1*(21 + 150*t2 - 318*Power(t2,2) + 72*Power(t2,3) + 
                  71*Power(t2,4) + 
                  Power(s2,3)*(7 + 24*t2 + 11*Power(t2,2)) + 
                  Power(s2,2)*
                   (-29 - 18*t2 + 115*Power(t2,2) + 38*Power(t2,3)) + 
                  s2*(9 - 173*t2 - 10*Power(t2,2) + 197*Power(t2,3) + 
                     25*Power(t2,4))))) + 
         Power(t1,5)*(10*s2 - 49*Power(s2,2) + 28*Power(s2,3) - 
            3*Power(s2,4) - 29*t2 - 163*s2*t2 + 9*Power(s2,2)*t2 + 
            148*Power(s2,3)*t2 - 34*Power(s2,4)*t2 - 3*Power(s2,5)*t2 - 
            59*Power(t2,2) + 94*s2*Power(t2,2) + 
            368*Power(s2,2)*Power(t2,2) + 23*Power(s2,3)*Power(t2,2) - 
            90*Power(s2,4)*Power(t2,2) - 9*Power(s2,5)*Power(t2,2) + 
            85*Power(t2,3) + 335*s2*Power(t2,3) + 
            43*Power(s2,2)*Power(t2,3) - 241*Power(s2,3)*Power(t2,3) - 
            71*Power(s2,4)*Power(t2,3) - 3*Power(s2,5)*Power(t2,3) + 
            48*Power(t2,4) - 62*s2*Power(t2,4) - 
            265*Power(s2,2)*Power(t2,4) - 124*Power(s2,3)*Power(t2,4) - 
            10*Power(s2,4)*Power(t2,4) - 44*Power(t2,5) - 
            112*s2*Power(t2,5) - 65*Power(s2,2)*Power(t2,5) - 
            7*Power(s2,3)*Power(t2,5) + 
            Power(s,3)*t2*(3 + 5*t2 - 33*Power(t2,2) + 29*Power(t2,3) + 
               21*Power(t2,4) + Power(s2,3)*(1 + 2*t2) + 
               Power(s2,2)*(13 + 31*t2 + 23*Power(t2,2)) + 
               s2*(-17 + 25*t2 + 101*Power(t2,2) + 48*Power(t2,3))) - 
            Power(s1,3)*(-24 + 5*t2 + 4*Power(t2,2) + 7*Power(t2,3) - 
               5*Power(t2,4) + 6*Power(t2,5) + 
               Power(s2,3)*(1 + 7*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(5 + 3*t2 + 33*Power(t2,2) + 
                  25*Power(t2,3)) + 
               s2*(8 + 29*t2 - 8*Power(t2,2) + 21*Power(t2,3) + 
                  23*Power(t2,4))) - 
            Power(s1,2)*(62 - 3*t2 + 111*Power(t2,2) - 
               140*Power(t2,3) - 11*Power(t2,4) + 26*Power(t2,5) + 
               Power(s2,4)*(1 + 6*t2 + 6*Power(t2,2)) + 
               2*Power(s2,3)*t2*(-5 + 11*t2 + 14*Power(t2,2)) + 
               Power(s2,2)*(-42 - 121*t2 - 138*Power(t2,2) + 
                  59*Power(t2,3) + 42*Power(t2,4)) + 
               s2*(13 + 22*t2 - 179*Power(t2,2) - 165*Power(t2,3) + 
                  95*Power(t2,4) + 16*Power(t2,5))) - 
            s1*(-48 - 3*t2 + 30*Power(t2,2) - 105*Power(t2,3) - 
               49*Power(t2,4) + 40*Power(t2,5) + 
               Power(s2,5)*(1 + 8*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(-4 - 27*t2 + 8*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(s2,3)*(26 + 150*t2 + 60*Power(t2,2) + 
                  56*Power(t2,3) + 27*Power(t2,4)) + 
               Power(s2,2)*(-24 - 16*t2 + 217*Power(t2,2) + 
                  54*Power(t2,3) + 80*Power(t2,4) + 10*Power(t2,5)) + 
               s2*(11 - 122*t2 - 103*Power(t2,2) + 10*Power(t2,3) + 
                  29*Power(t2,4) + 38*Power(t2,5))) - 
            Power(s,2)*(-1 + 103*t2 - 62*Power(t2,2) - 
               473*Power(t2,3) + 134*Power(t2,4) + 118*Power(t2,5) + 
               2*Power(s2,4)*t2*(1 + 2*t2) + 
               3*Power(s2,3)*t2*(2 + 19*t2 + 14*Power(t2,2)) + 
               Power(s2,2)*t2*
                (4 + 58*t2 + 299*Power(t2,2) + 99*Power(t2,3)) + 
               s2*(1 - 91*t2 - 177*Power(t2,2) + 107*Power(t2,3) + 
                  366*Power(t2,4) + 54*Power(t2,5)) + 
               s1*(-2 - 37*t2 + 106*Power(t2,2) - 63*Power(t2,3) + 
                  15*Power(t2,4) + 45*Power(t2,5) + 
                  5*Power(s2,3)*(1 + 2*t2 + 2*Power(t2,2)) + 
                  Power(s2,2)*
                   (-13 + 30*t2 + 89*Power(t2,2) + 69*Power(t2,3)) + 
                  s2*(10 - 27*t2 - 43*Power(t2,2) + 166*Power(t2,3) + 
                     114*Power(t2,4)))) + 
            s*(16 + 197*t2 - 124*Power(t2,2) - 748*Power(t2,3) + 
               66*Power(t2,4) + 173*Power(t2,5) + 
               Power(s2,5)*t2*(2 + 3*t2) + 
               Power(s2,4)*t2*(7 + 50*t2 + 28*Power(t2,2)) + 
               Power(s2,3)*(3 + 28*t2 + 148*Power(t2,2) + 
                  275*Power(t2,3) + 66*Power(t2,4)) + 
               Power(s2,2)*(-17 - 153*t2 - 166*Power(t2,2) + 
                  467*Power(t2,3) + 430*Power(t2,4) + 40*Power(t2,5)) + 
               s2*(-1 - 135*t2 - 660*Power(t2,2) - 468*Power(t2,3) + 
                  552*Power(t2,4) + 195*Power(t2,5)) + 
               Power(s1,2)*(18 - 77*t2 + 59*Power(t2,2) - 
                  5*Power(t2,3) - 14*Power(t2,4) + 30*Power(t2,5) + 
                  Power(s2,3)*(7 + 16*t2 + 14*Power(t2,2)) + 
                  Power(s2,2)*t2*(26 + 94*t2 + 71*Power(t2,2)) + 
                  s2*(-24 + 29*t2 - 49*Power(t2,2) + 94*Power(t2,3) + 
                     89*Power(t2,4))) + 
               s1*(-57 + 35*t2 + 455*Power(t2,2) - 508*Power(t2,3) - 
                  9*Power(t2,4) + 120*Power(t2,5) + 
                  Power(s2,4)*(1 + 12*t2 + 11*Power(t2,2)) + 
                  Power(s2,3)*
                   (-2 - 44*t2 + 60*Power(t2,2) + 67*Power(t2,3)) + 
                  Power(s2,2)*
                   (-25 - 17*t2 - 195*Power(t2,2) + 257*Power(t2,3) + 
                     132*Power(t2,4)) + 
                  s2*(81 + 74*t2 - 225*Power(t2,2) - 251*Power(t2,3) + 
                     347*Power(t2,4) + 65*Power(t2,5))))) + 
         Power(t1,4)*(4 - 19*s2 + 7*Power(s2,2) + 12*Power(s2,3) - 
            4*Power(s2,4) - 42*t2 - 15*s2*t2 + 83*Power(s2,2)*t2 + 
            109*Power(s2,3)*t2 - 83*Power(s2,4)*t2 + 10*Power(s2,5)*t2 + 
            2*Power(t2,2) + 257*s2*Power(t2,2) + 
            280*Power(s2,2)*Power(t2,2) - 216*Power(s2,3)*Power(t2,2) - 
            114*Power(s2,4)*Power(t2,2) + 44*Power(s2,5)*Power(t2,2) + 
            3*Power(s2,6)*Power(t2,2) + 96*Power(t2,3) + 
            41*s2*Power(t2,3) - 385*Power(s2,2)*Power(t2,3) - 
            322*Power(s2,3)*Power(t2,3) + 137*Power(s2,4)*Power(t2,3) + 
            60*Power(s2,5)*Power(t2,3) + 3*Power(s2,6)*Power(t2,3) - 
            41*Power(t2,4) - 294*s2*Power(t2,4) - 
            302*Power(s2,2)*Power(t2,4) + 246*Power(s2,3)*Power(t2,4) + 
            194*Power(s2,4)*Power(t2,4) + 20*Power(s2,5)*Power(t2,4) - 
            58*Power(t2,5) - 21*s2*Power(t2,5) + 
            248*Power(s2,2)*Power(t2,5) + 234*Power(s2,3)*Power(t2,5) + 
            37*Power(s2,4)*Power(t2,5) + 33*Power(t2,6) + 
            104*s2*Power(t2,6) + 100*Power(s2,2)*Power(t2,6) + 
            20*Power(s2,3)*Power(t2,6) - 
            Power(s,3)*Power(t2,2)*
             (14 + 11*t2 - 85*Power(t2,2) + 20*Power(t2,3) + 
               35*Power(t2,4) + Power(s2,3)*(6 + 9*t2) + 
               Power(s2,2)*(32 + 68*t2 + 55*Power(t2,2)) + 
               s2*(-52 + 17*t2 + 135*Power(t2,2) + 85*Power(t2,3))) + 
            Power(s1,3)*(22 - 28*t2 - 3*Power(t2,2) + 2*Power(t2,3) + 
               6*Power(t2,4) - 6*Power(t2,5) + 4*Power(t2,6) + 
               2*Power(s2,4)*(1 + t2 + Power(t2,2)) + 
               3*Power(s2,3)*
                (-1 + 3*t2 + 9*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(9 + 45*t2 + 5*Power(t2,2) + 
                  43*Power(t2,3) + 40*Power(t2,4)) + 
               s2*(-30 - 70*t2 + 53*Power(t2,2) - 16*Power(t2,3) + 
                  11*Power(t2,4) + 23*Power(t2,5))) + 
            Power(s1,2)*(-64 + 52*t2 - 11*Power(t2,2) + 
               125*Power(t2,3) - 72*Power(t2,4) - 34*Power(t2,5) + 
               24*Power(t2,6) + Power(s2,5)*t2*(1 + 2*t2) + 
               Power(s2,4)*(1 + 3*t2 + 7*Power(t2,2) + 19*Power(t2,3)) + 
               Power(s2,3)*(-19 - 95*t2 - 127*Power(t2,2) - 
                  15*Power(t2,3) + 56*Power(t2,4)) + 
               Power(s2,2)*(12 - 36*t2 - 162*Power(t2,2) - 
                  267*Power(t2,3) + 46*Power(t2,4) + 59*Power(t2,5)) + 
               s2*(70 + 221*t2 + 154*Power(t2,2) - 117*Power(t2,3) - 
                  231*Power(t2,4) + 100*Power(t2,5) + 14*Power(t2,6))) + 
            s1*(45 - 28*t2 - 4*Power(t2,2) + 130*Power(t2,3) - 
               156*Power(t2,4) - 71*Power(t2,5) + 41*Power(t2,6) + 
               2*Power(s2,6)*t2*(1 + 2*t2) + 
               3*Power(s2,5)*t2*(-3 - 4*t2 + 4*Power(t2,2)) + 
               Power(s2,4)*(4 + 86*t2 + 144*Power(t2,2) + 
                  24*Power(t2,3) + 33*Power(t2,4)) + 
               Power(s2,3)*(-6 - 58*t2 + 196*Power(t2,2) + 
                  276*Power(t2,3) + 65*Power(t2,4) + 36*Power(t2,5)) + 
               Power(s2,2)*(2 - 110*t2 - 412*Power(t2,2) + 
                  59*Power(t2,3) + 93*Power(t2,4) + 71*Power(t2,5) + 
                  10*Power(t2,6)) + 
               s2*(-45 - 49*t2 - 119*Power(t2,2) - 346*Power(t2,3) - 
                  134*Power(t2,4) + 26*Power(t2,5) + 42*Power(t2,6))) + 
            Power(s,2)*t2*(-5 + 204*t2 + Power(s2,5)*t2 + 
               2*Power(t2,2) - 653*Power(t2,3) + 51*Power(t2,4) + 
               155*Power(t2,5) + Power(s2,4)*t2*(7 + 25*t2) + 
               Power(s2,3)*(5 + 32*t2 + 142*Power(t2,2) + 
                  135*Power(t2,3)) + 
               Power(s2,2)*(-10 - 50*t2 + 23*Power(t2,2) + 
                  512*Power(t2,3) + 220*Power(t2,4)) + 
               s2*(10 - 134*t2 - 387*Power(t2,2) - 144*Power(t2,3) + 
                  495*Power(t2,4) + 100*Power(t2,5)) + 
               s1*(-14 - 57*t2 + 187*Power(t2,2) - 97*Power(t2,3) - 
                  35*Power(t2,4) + 60*Power(t2,5) + 
                  2*Power(s2,4)*(1 + t2) + 
                  Power(s2,3)*(18 + 33*t2 + 34*Power(t2,2)) + 
                  Power(s2,2)*
                   (-61 + 51*t2 + 134*Power(t2,2) + 133*Power(t2,3)) + 
                  s2*(55 - 89*t2 - 90*Power(t2,2) + 149*Power(t2,3) + 
                     165*Power(t2,4)))) - 
            s*(-3 - 7*t2 + 365*Power(t2,2) + Power(s2,6)*Power(t2,2) + 
               72*Power(t2,3) - 862*Power(t2,4) - 58*Power(t2,5) + 
               181*Power(t2,6) + Power(s2,5)*Power(t2,2)*(11 + 21*t2) + 
               Power(s2,4)*t2*
                (10 + 48*t2 + 165*Power(t2,2) + 104*Power(t2,3)) + 
               Power(s2,3)*(-4 - 48*t2 - 106*Power(t2,2) + 
                  171*Power(t2,3) + 579*Power(t2,4) + 172*Power(t2,5)) + 
               Power(s2,2)*(3 - 77*t2 - 525*Power(t2,2) - 
                  794*Power(t2,3) + 362*Power(t2,4) + 692*Power(t2,5) + 
                  85*Power(t2,6)) + 
               s2*(4 + 127*t2 + 56*Power(t2,2) - 1085*Power(t2,3) - 
                  1158*Power(t2,4) + 537*Power(t2,5) + 275*Power(t2,6)) + 
               Power(s1,2)*(2 + 42*t2 - 101*Power(t2,2) + 
                  59*Power(t2,3) + 7*Power(t2,4) - 41*Power(t2,5) + 
                  30*Power(t2,6) + 
                  4*Power(s2,4)*(1 + t2 + Power(t2,2)) + 
                  Power(s2,3)*
                   (-12 + 47*t2 + 63*Power(t2,2) + 47*Power(t2,3)) + 
                  Power(s2,2)*
                   (12 - 85*t2 + 27*Power(t2,2) + 135*Power(t2,3) + 
                     121*Power(t2,4)) + 
                  s2*(-6 - 3*t2 - 23*Power(t2,2) - 101*Power(t2,3) + 
                     62*Power(t2,4) + 106*Power(t2,5))) + 
               s1*(6 - 138*t2 - 47*Power(t2,2) + 655*Power(t2,3) - 
                  435*Power(t2,4) - 129*Power(t2,5) + 130*Power(t2,6) + 
                  Power(s2,5)*t2*(2 + 5*t2) + 
                  Power(s2,4)*t2*(-10 - 11*t2 + 38*Power(t2,2)) + 
                  Power(s2,3)*
                   (-1 + 20*t2 - 48*Power(t2,2) + 31*Power(t2,3) + 
                     159*Power(t2,4)) + 
                  Power(s2,2)*
                   (12 + t2 + 120*Power(t2,2) - 316*Power(t2,3) + 
                     279*Power(t2,4) + 227*Power(t2,5)) + 
                  s2*(-17 + 115*t2 + 161*Power(t2,2) + 27*Power(t2,3) - 
                     513*Power(t2,4) + 349*Power(t2,5) + 90*Power(t2,6))))\
))*T3(t2,t1))/((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*Power(t1 - t2,2)*
       Power(1 - s2 + t1 - t2,2)*Power(t1 - s2*t2,3)) - 
    (8*((-2 + t2)*Power(-1 + t2,4)*Power(1 + t2,2)*
          Power(1 + (-1 + s)*t2,3) - 
         Power(s2,7)*(-1 + t2)*Power(t2,2)*
          (t2*(-7 - 2*t2 + Power(t2,2)) + s1*(1 + 6*t2 + Power(t2,2))) - 
         Power(t1,6)*(Power(s,2)*
             (-7 - 3*t2 - 3*Power(t2,2) + Power(t2,3)) + 
            (-1 + t2)*(-1 - 5*t2 - Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(3 - 2*t2 + Power(t2,2)) + 
               s1*t2*(5 - 2*t2 + Power(t2,2))) - 
            s*(-2 + 13*t2 + Power(t2,2) - Power(t2,3) + Power(t2,4) + 
               2*s1*(-5 + t2 - 3*Power(t2,2) + Power(t2,3)))) - 
         t1*Power(-1 + t2,3)*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(5 + 2*t2 - 12*Power(t2,2) - 2*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(-1 + t2,2)*
             (-8 - 14*t2 - Power(t2,2) + 2*Power(t2,3) + 
               s1*(-1 - 5*t2 - Power(t2,2) + 3*Power(t2,3))) - 
            s*(-1 + t2)*(-1 - 21*t2 - 35*Power(t2,2) + 9*Power(t2,4) + 
               s1*(6 + 8*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(t1,5)*(Power(s,3)*
             (2 + 7*t2 + 3*Power(t2,2) + Power(t2,3) - Power(t2,4)) + 
            Power(-1 + t2,2)*
             (-7 - 6*t2 - 4*Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,3)*(-1 + Power(t2,2)) + 
               2*s1*(-2 + 4*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(10 - 11*t2 + Power(t2,2) + 2*Power(t2,3))) + 
            Power(s,2)*(5 + t2 + 37*Power(t2,2) - 25*Power(t2,3) + 
               4*Power(t2,4) + 2*Power(t2,5) + 
               s1*(-1 - 4*t2 - 6*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4))) - 
            s*(-1 + t2)*(10 + 15*t2 - 29*Power(t2,2) - Power(t2,3) + 
               7*Power(t2,4) + 
               Power(s1,2)*t2*(-5 - 2*t2 + 3*Power(t2,2)) + 
               s1*(-17 + 43*t2 - 33*Power(t2,2) + 5*Power(t2,3) + 
                  4*Power(t2,4)))) + 
         Power(t1,2)*Power(-1 + t2,3)*
          (Power(s,3)*t2*(3 + 2*t2 - 17*Power(t2,2) + Power(t2,3) + 
               6*Power(t2,4)) - 
            Power(-1 + t2,2)*
             (-16 - 8*t2 - Power(t2,2) + 3*Power(t2,3) + 
               Power(s1,2)*(2 - t2 - 2*Power(t2,2) + 3*Power(t2,3)) + 
               s1*(-4 - 34*t2 - Power(t2,2) + 5*Power(t2,3))) + 
            s*(-1 + t2)*(10 - 70*t2 - 113*Power(t2,2) + 
               22*Power(t2,4) + 
               Power(s1,2)*(-4 + 3*t2 + Power(t2,2) - 5*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(19 + 55*t2 - 50*Power(t2,2) - 14*Power(t2,3) + 
                  17*Power(t2,4))) + 
            Power(s,2)*(-1 - 38*t2 - 10*Power(t2,2) + 100*Power(t2,3) + 
               5*Power(t2,4) - 23*Power(t2,5) + 
               s1*(4 + 8*t2 - 34*Power(t2,2) + 14*Power(t2,3) + 
                  12*Power(t2,4) - 9*Power(t2,5)))) + 
         Power(t1,4)*(Power(s,3)*
             (8 - 8*t2 - 12*Power(t2,2) + 5*Power(t2,3) - 
               7*Power(t2,4) + Power(t2,5) + Power(t2,6)) - 
            Power(-1 + t2,3)*
             (11 - 2*Power(t2,2) + 7*Power(t2,3) + 
               Power(s1,3)*(2 + Power(t2,3)) + 
               Power(s1,2)*(1 - 24*t2 + 2*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s1*(-16 - 2*t2 - 4*Power(t2,2) + 5*Power(t2,3))) + 
            s*Power(-1 + t2,2)*
             (Power(s1,2)*(-16 + 6*t2 - 6*Power(t2,2) + Power(t2,3) + 
                  3*Power(t2,4)) + 
               2*(-2 - 24*t2 - 50*Power(t2,2) + Power(t2,3) + 
                  10*Power(t2,4)) + 
               s1*(45 + 96*t2 - 77*Power(t2,2) + Power(t2,3) + 
                  13*Power(t2,4))) - 
            Power(s,2)*(-1 + t2)*
             (-4 + 82*t2 + 92*Power(t2,2) - 70*Power(t2,3) + 
               4*Power(t2,4) + 10*Power(t2,5) + 
               s1*(-18 - 30*t2 + 10*Power(t2,2) - 13*Power(t2,3) + 
                  2*Power(t2,4) + 3*Power(t2,5)))) - 
         Power(t1,3)*(-1 + t2)*
          (Power(s,3)*(4 - 15*t2 - 12*Power(t2,2) + 22*Power(t2,3) - 
               14*Power(t2,4) - 3*Power(t2,5) + 4*Power(t2,6)) - 
            Power(-1 + t2,3)*
             (23 - 4*t2 + Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,3)*(1 + t2 - Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(8 - 22*t2 + 4*Power(t2,3)) + 
               s1*(-39 - 22*t2 - 2*Power(t2,2) + 6*Power(t2,3))) + 
            s*Power(-1 + t2,2)*
             (-29 - 85*t2 - 143*Power(t2,2) + 3*Power(t2,3) + 
               28*Power(t2,4) + 
               Power(s1,2)*(-26 + 17*t2 - 3*Power(t2,2) - 
                  7*Power(t2,3) + 6*Power(t2,4)) + 
               s1*(89 + 91*t2 - 90*Power(t2,2) - 11*Power(t2,3) + 
                  20*Power(t2,4))) - 
            Power(s,2)*(-1 + t2)*
             (11 + 105*t2 + 78*Power(t2,2) - 111*Power(t2,3) - 
               2*Power(t2,4) + 21*Power(t2,5) + 
               s1*(-27 - 37*t2 + 42*Power(t2,2) - 18*Power(t2,3) - 
                  9*Power(t2,4) + 9*Power(t2,5)))) - 
         Power(s2,6)*t2*(2*Power(s1,2)*(-1 + t2)*Power(t2,2) + 
            t2*(1 + 2*(-14 + s)*t2 + (59 - 24*s)*Power(t2,2) + 
               3*(-9 + 4*s)*Power(t2,3) - 2*(5 + s)*Power(t2,4) + 
               5*Power(t2,5) + 
               t1*(-1 + t2)*(s*Power(-1 + t2,2) + 
                  3*(7 + 9*t2 + Power(t2,2) - Power(t2,3)))) + 
            s1*(-2*t1*(-1 - 11*t2 + 3*Power(t2,2) + 7*Power(t2,3) + 
                  2*Power(t2,4)) + 
               t2*(-(s*(-1 - 15*t2 + 3*Power(t2,2) + Power(t2,3))) + 
                  t2*(13 - 29*t2 + 15*Power(t2,2) + Power(t2,3))))) + 
         Power(s2,5)*(-(Power(s1,3)*Power(-1 + t2,3)) + 
            Power(s1,2)*(-1 + t2)*t2*
             (-1 + s + t1 + 2*t2 - 2*s*t2 + 6*t1*t2 + Power(t2,2) - 
               3*s*Power(t2,2) + 3*t1*Power(t2,2) - 2*Power(t2,3) + 
               2*t1*Power(t2,3)) + 
            s1*(Power(t1,2)*(1 + 29*t2 + 33*Power(t2,2) - 
                  43*Power(t2,3) - 14*Power(t2,4) - 6*Power(t2,5)) + 
               Power(t2,2)*(2*Power(s,2)*t2*(5 + t2) + 
                  Power(-1 + t2,2)*
                   (-8 + 26*t2 - 23*Power(t2,2) + 3*Power(t2,3)) + 
                  s*(3 + 2*t2 - 14*Power(t2,2) + 8*Power(t2,3) + 
                     Power(t2,4))) + 
               t1*t2*(s*(2 + 53*t2 + 17*Power(t2,2) + 5*Power(t2,3) - 
                     5*Power(t2,4)) + 
                  2*t2*(21 - 37*t2 + 2*Power(t2,2) + 11*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            t2*(t1*(2 - (77 - 5*s + Power(s,2))*t2 + 
                  (114 - 65*s + Power(s,2))*Power(t2,2) + 
                  (45 - 31*s - 13*Power(s,2))*Power(t2,3) + 
                  (-87 + 31*s + Power(s,2))*Power(t2,4) - 
                  4*(2 + 3*s)*Power(t2,5) + 11*Power(t2,6)) + 
               Power(t1,2)*(-1 + t2)*
                (s*Power(-1 + t2,2)*(2 + 3*t2) - 
                  3*(-7 - 23*t2 - 12*Power(t2,2) + Power(t2,3) + 
                     Power(t2,4))) + 
               t2*(-(Power(s,2)*t2*
                     (-1 + 31*t2 - 7*Power(t2,2) + Power(t2,3))) - 
                  Power(-1 + t2,2)*t2*
                   (29 - 52*t2 + 2*Power(t2,2) + 9*Power(t2,3)) + 
                  s*(1 - 10*t2 - 8*Power(t2,2) + 52*Power(t2,3) - 
                     47*Power(t2,4) + 12*Power(t2,5))))) + 
         Power(s2,4)*(Power(t1,2)*
             (-1 + 2*(35 - 2*s + Power(s,2))*t2 + 
               (-9 + 58*s + 4*Power(s,2))*Power(t2,2) + 
               (-243 + 139*s + 32*Power(s,2))*Power(t2,3) + 
               (115 - 17*s + 26*Power(s,2))*Power(t2,4) + 
               (94 - 15*s - 4*Power(s,2))*Power(t2,5) + 
               19*(-1 + s)*Power(t2,6) - 7*Power(t2,7)) + 
            Power(s1,3)*Power(-1 + t2,3)*
             (3 - 4*t2 - 2*Power(t2,2) + 2*t1*(1 + t2 + Power(t2,2))) - 
            Power(t1,3)*(-1 + t2)*
             (7 + 65*t2 + 80*Power(t2,2) + 16*Power(t2,3) - 
               7*Power(t2,4) - Power(t2,5) + 
               s*(1 + 4*t2 - 8*Power(t2,2) + 3*Power(t2,4))) + 
            Power(t2,2)*(-2*Power(s,3)*t2*(-2 + 7*t2 + Power(t2,2)) - 
               Power(-1 + t2,3)*
                (8 + 19*t2 - 26*Power(t2,2) + 8*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s*Power(-1 + t2,2)*
                (-8 - 32*t2 - 83*Power(t2,2) - 30*Power(t2,3) + 
                  23*Power(t2,4)) + 
               Power(s,2)*(2 + 22*t2 + 72*Power(t2,2) - 
                  112*Power(t2,3) + 24*Power(t2,4) - 8*Power(t2,5))) + 
            t1*t2*(2*Power(s,3)*Power(t2,2)*(1 + 5*t2) + 
               Power(s,2)*t2*
                (-1 + 68*t2 + 86*Power(t2,2) - 46*Power(t2,3) + 
                  13*Power(t2,4)) + 
               Power(-1 + t2,2)*t2*
                (84 - 127*t2 - 58*Power(t2,2) + 28*Power(t2,3) + 
                  13*Power(t2,4)) + 
               s*(-2 + 21*t2 + 90*Power(t2,2) - 230*Power(t2,3) + 
                  88*Power(t2,4) + 71*Power(t2,5) - 38*Power(t2,6))) + 
            Power(s1,2)*(-1 + t2)*
             ((-1 + t2)*(-3 - 4*t2 + (3 - 7*s)*Power(t2,2) - 
                  5*s*Power(t2,3) + 4*Power(t2,4)) - 
               Power(t1,2)*(1 + 10*t2 + 13*Power(t2,2) + 
                  6*Power(t2,4)) + 
               t1*(1 - 2*t2 - 10*Power(t2,2) + 17*Power(t2,3) - 
                  11*Power(t2,4) + 5*Power(t2,5) - 
                  4*s*(1 - t2 - 3*Power(t2,2) - 3*Power(t2,3) + 
                     Power(t2,4)))) + 
            s1*(Power(s,2)*t2*
                (2*t1*(-1 - 13*t2 - 6*Power(t2,2) - 11*Power(t2,3) + 
                     Power(t2,4)) + 
                  t2*(1 - 44*t2 + 40*Power(t2,2) + 2*Power(t2,3) + 
                     Power(t2,4))) + 
               (-1 + t2)*(Power(t1,2)*t2*
                   (45 + 16*t2 - 97*Power(t2,2) - 14*Power(t2,3) - 
                     10*Power(t2,4)) + 
                  4*Power(t1,3)*
                   (3 + 19*t2 + 14*Power(t2,2) + 3*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(-1 + t2,2)*t2*
                   (2 - 2*t2 + 33*Power(t2,2) - 21*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  t1*t2*(-18 + 69*t2 - 74*Power(t2,2) + 4*Power(t2,3) + 
                     18*Power(t2,4) + Power(t2,5))) + 
               s*(-(Power(-1 + t2,2)*t2*
                     (1 - 4*t2 - 64*Power(t2,2) - 13*Power(t2,3) + 
                       2*Power(t2,4))) + 
                  Power(t1,2)*
                   (-1 - 63*t2 - 92*Power(t2,2) - 12*Power(t2,3) - 
                     23*Power(t2,4) + 11*Power(t2,5)) - 
                  t1*t2*(1 + 44*t2 - 105*Power(t2,2) + 79*Power(t2,3) - 
                     34*Power(t2,4) + 15*Power(t2,5))))) + 
         Power(s2,2)*(Power(-1 + t2,3)*
             (1 + (7 + 12*s)*t2 + 
               (2 + 35*s + 13*Power(s,2))*Power(t2,2) + 
               (-34 + 40*s + 24*Power(s,2))*Power(t2,3) + 
               (27 - 60*s + 16*Power(s,2) + 4*Power(s,3))*Power(t2,4) + 
               (-1 - 44*s - 6*Power(s,2) + 3*Power(s,3))*Power(t2,5) - 
               2*(1 - 6*s + 7*Power(s,2))*Power(t2,6) + 5*s*Power(t2,7)) \
- Power(t1,5)*(-1 + t2)*(s*Power(-1 + t2,2)*(3 + 2*t2) + 
               3*(7 + 9*t2 + Power(t2,2) - Power(t2,3))) + 
            Power(s1,3)*t1*Power(-1 + t2,3)*
             (1 - 2*t2 - 4*Power(t2,2) + 4*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(1 + 9*t2 + 6*Power(t2,2)) - 
               t1*(5 + 6*Power(t2,2) + 7*Power(t2,3))) + 
            Power(t1,4)*(42 - 34*t2 - 134*Power(t2,2) + 
               89*Power(t2,3) + 57*Power(t2,4) - 17*Power(t2,5) - 
               3*Power(t2,6) + 
               Power(s,2)*(7 + 29*t2 + 53*Power(t2,2) + 
                  37*Power(t2,3) - 4*Power(t2,4) - 2*Power(t2,5)) + 
               s*t2*(39 + 119*t2 + 16*Power(t2,2) - 14*Power(t2,3) + 
                  19*Power(t2,4) + Power(t2,5))) - 
            t1*(-1 + t2)*(Power(s,3)*Power(t2,2)*
                (17 - 47*t2 - 13*Power(t2,2) - 9*Power(t2,3) + 
                  10*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (-1 + 3*t2 + 54*Power(t2,2) - Power(t2,3) + 
                  13*Power(t2,4) + 7*Power(t2,5)) + 
               Power(s,2)*t2*
                (16 + 102*t2 + 75*Power(t2,2) - 263*Power(t2,3) + 
                  78*Power(t2,4) + 33*Power(t2,5) - 41*Power(t2,6)) + 
               s*Power(-1 + t2,2)*
                (1 - 24*t2 - 266*Power(t2,2) - 376*Power(t2,3) - 
                  98*Power(t2,4) + 70*Power(t2,5) + 15*Power(t2,6))) + 
            Power(t1,2)*(Power(s,3)*t2*
                (5 - 10*t2 - 17*Power(t2,2) - 41*Power(t2,3) - 
                  20*Power(t2,4) + 11*Power(t2,5)) - 
               Power(-1 + t2,3)*
                (9 + 97*t2 - 69*Power(t2,2) + 8*Power(t2,3) + 
                  34*Power(t2,4) + 17*Power(t2,5)) + 
               Power(s,2)*t2*
                (67 + 238*t2 + 62*Power(t2,2) - 472*Power(t2,3) + 
                  113*Power(t2,4) + 34*Power(t2,5) - 42*Power(t2,6)) + 
               s*Power(-1 + t2,2)*
                (-3 - 89*t2 - 159*Power(t2,2) - 534*Power(t2,3) - 
                  114*Power(t2,4) + 103*Power(t2,5) + 16*Power(t2,6))) + 
            Power(t1,3)*(Power(s,3)*
                (2 + 6*t2 + 22*Power(t2,2) + 40*Power(t2,3) + 
                  6*Power(t2,4) - 4*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (26 + 9*t2 - 167*Power(t2,2) - 42*Power(t2,3) + 
                  41*Power(t2,4) + 13*Power(t2,5)) + 
               Power(s,2)*(-3 + 5*t2 + 168*Power(t2,2) + 
                  139*Power(t2,3) - 94*Power(t2,4) + 8*Power(t2,5) + 
                  17*Power(t2,6)) - 
               s*(1 - 116*t2 + 32*Power(t2,2) + 281*Power(t2,3) - 
                  145*Power(t2,4) - 120*Power(t2,5) + 60*Power(t2,6) + 
                  7*Power(t2,7))) - 
            Power(s1,2)*(-1 + t2)*
             (2*Power(t1,4)*(6 + 8*t2 - 3*Power(t2,2) + 
                  3*Power(t2,3) + Power(t2,4)) + 
               Power(-1 + t2,3)*
                (6 - 7*t2 - 5*Power(t2,2) + (5 + 2*s)*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(-1 + t2)*
                (20 + 22*t2 + 15*Power(t2,2) - 59*Power(t2,3) - 
                  7*Power(t2,4) + 9*Power(t2,5) + 
                  s*(-2 + 46*t2 + 47*Power(t2,2) + 4*Power(t2,3) - 
                     23*Power(t2,4))) + 
               t1*Power(-1 + t2,2)*
                (-14 + 4*t2 + Power(t2,2) + 9*Power(t2,3) + 
                  3*Power(t2,4) - 3*Power(t2,5) + 
                  s*(14 - 39*t2 - 33*Power(t2,2) + 15*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,3)*(13 + 13*t2 - 58*Power(t2,2) + 
                  36*Power(t2,3) + 5*Power(t2,4) - 9*Power(t2,5) + 
                  s*(2 - 9*t2 - 44*Power(t2,2) - 5*Power(t2,3) + 
                     16*Power(t2,4)))) + 
            s1*(2*Power(t1,5)*
                (-14 + 5*t2 + 9*Power(t2,2) - Power(t2,3) + Power(t2,4)) \
- Power(-1 + t2,3)*(-9 + (2 - 17*s)*t2 + 15*(1 + s)*Power(t2,2) - 
                  (4 - 43*s + Power(s,2))*Power(t2,3) + 
                  (3 - 35*s + 9*Power(s,2))*Power(t2,4) - 
                  (10 + 7*s + 3*Power(s,2))*Power(t2,5) + 
                  (3 + s)*Power(t2,6)) + 
               Power(t1,4)*(-32 + 44*t2 + 62*Power(t2,2) - 
                  79*Power(t2,3) + 15*Power(t2,4) - 9*Power(t2,5) - 
                  Power(t2,6) + 
                  s*(-57 - 75*t2 - 13*Power(t2,2) - 47*Power(t2,3) + 
                     8*Power(t2,4) + 4*Power(t2,5))) - 
               Power(t1,2)*(-1 + t2)*
                (Power(-1 + t2,2)*
                   (2 - 77*t2 - 51*Power(t2,2) + 17*Power(t2,3) + 
                     6*Power(t2,4) + Power(t2,5)) + 
                  Power(s,2)*
                   (-13 - 82*t2 - 92*Power(t2,2) - 85*Power(t2,3) - 
                     31*Power(t2,4) + 27*Power(t2,5)) + 
                  s*(45 + 140*t2 + 74*Power(t2,2) - 312*Power(t2,3) + 
                     43*Power(t2,4) + 52*Power(t2,5) - 42*Power(t2,6))) \
+ Power(t1,3)*(Power(-1 + t2,2)*
                   (7 - 41*t2 + 39*Power(t2,2) + 3*Power(t2,3) + 
                     10*Power(t2,4) + 2*Power(t2,5)) + 
                  2*Power(s,2)*
                   (-9 - 10*t2 - 4*Power(t2,2) - 35*Power(t2,3) - 
                     9*Power(t2,4) + 7*Power(t2,5)) + 
                  s*(-29 - 4*t2 + 179*Power(t2,2) - 245*Power(t2,3) + 
                     108*Power(t2,4) + 17*Power(t2,5) - 26*Power(t2,6))) \
+ t1*Power(-1 + t2,2)*(Power(-1 + t2,2)*
                   (-20 - 64*t2 - 118*Power(t2,2) + 26*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  Power(s,2)*t2*
                   (16 - 127*t2 - 20*Power(t2,2) + Power(t2,3) + 
                     10*Power(t2,4)) + 
                  s*(12 + 30*t2 + 171*Power(t2,2) - 200*Power(t2,3) - 
                     15*Power(t2,4) + 21*Power(t2,5) - 19*Power(t2,6))))) \
- s2*(-(Power(t1,6)*(-1 + t2)*
               (7 + s*Power(-1 + t2,2) + 2*t2 - Power(t2,2))) - 
            t1*Power(-1 + t2,3)*
             (-9 + (-19 - 40*s + 15*Power(s,2))*t2 + 
               (62 - 141*s + 5*Power(s,2) + 13*Power(s,3))*
                Power(t2,2) + 
               (-29 + 47*s - 60*Power(s,2) + 4*Power(s,3))*
                Power(t2,3) - 
               (4 - 163*s + 76*Power(s,2) + 8*Power(s,3))*Power(t2,4) - 
               (4 + 5*s - 36*Power(s,2) + 11*Power(s,3))*Power(t2,5) + 
               (3 - 24*s + 14*Power(s,2))*Power(t2,6)) + 
            Power(-1 + t2,3)*
             (3 + t2 + 16*s*t2 + 
               (-11 + 6*s + 17*Power(s,2))*Power(t2,2) + 
               2*(-2 - 16*s + 7*Power(s,2) + 2*Power(s,3))*
                Power(t2,3) + 
               (22 - 26*s - 14*Power(s,2) + 3*Power(s,3))*Power(t2,4) - 
               (10 - 39*s + 27*Power(s,2) + Power(s,3))*Power(t2,5) + 
               (-2 + 2*s + 6*Power(s,2) - 3*Power(s,3))*Power(t2,6) + 
               (1 - 5*s + 4*Power(s,2))*Power(t2,7)) + 
            Power(s1,3)*Power(t1,2)*Power(-1 + t2,3)*
             (Power(t1,2)*(2 + 5*t2 + 2*Power(t2,2)) - 
               t1*(7 - 2*t2 + 2*Power(t2,2) + 5*Power(t2,3)) + 
               2*(-1 + t2 - Power(t2,2) + Power(t2,4))) + 
            Power(t1,5)*(19 - 54*t2 + 7*Power(t2,2) + 33*Power(t2,3) - 
               2*Power(t2,4) - 3*Power(t2,5) + 
               Power(s,2)*(13 + 22*t2 + 24*Power(t2,2) + 
                  4*Power(t2,3) - 3*Power(t2,4)) + 
               s*(-1 + 31*t2 + 47*Power(t2,2) - 15*Power(t2,3) + 
                  8*Power(t2,4) + 2*Power(t2,5))) + 
            Power(t1,4)*(Power(-1 + t2,2)*
                (16 - 63*t2 - 35*Power(t2,2) + 8*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s,3)*(5 + 8*t2 + 26*Power(t2,2) + 
                  12*Power(t2,3) - Power(t2,4) - 2*Power(t2,5)) + 
               s*(17 + 67*t2 - 130*Power(t2,2) - 46*Power(t2,3) + 
                  125*Power(t2,4) - 19*Power(t2,5) - 14*Power(t2,6)) + 
               Power(s,2)*(-3 + 34*t2 + 109*Power(t2,2) + Power(t2,3) - 
                  44*Power(t2,4) + 21*Power(t2,5) + 2*Power(t2,6))) - 
            Power(t1,2)*(-1 + t2)*
             (-(Power(-1 + t2,3)*
                  (2 + 59*t2 - 6*Power(t2,2) + 10*Power(t2,3) + 
                    10*Power(t2,4))) + 
               Power(s,3)*t2*
                (1 - 25*t2 + Power(t2,2) - 24*Power(t2,3) - 
                  10*Power(t2,4) + 15*Power(t2,5)) + 
               s*Power(-1 + t2,2)*
                (7 - 181*t2 - 356*Power(t2,2) - 249*Power(t2,3) + 
                  58*Power(t2,4) + 43*Power(t2,5)) + 
               Power(s,2)*(1 + 120*t2 + 135*Power(t2,2) - 
                  274*Power(t2,3) - 111*Power(t2,4) + 186*Power(t2,5) - 
                  39*Power(t2,6) - 18*Power(t2,7))) + 
            Power(t1,3)*(-(Power(-1 + t2,3)*
                  (40 - 5*t2 - Power(t2,2) + 11*Power(t2,3) + 
                    19*Power(t2,4))) + 
               s*Power(-1 + t2,2)*
                (-28 - 33*t2 - 333*Power(t2,2) - 225*Power(t2,3) + 
                  63*Power(t2,4) + 36*Power(t2,5)) + 
               Power(s,3)*(6 - t2 - 19*Power(t2,2) - 8*Power(t2,3) - 
                  28*Power(t2,4) - 7*Power(t2,5) + 9*Power(t2,6)) + 
               Power(s,2)*(3 + 115*t2 + 232*Power(t2,2) - 
                  317*Power(t2,3) - 140*Power(t2,4) + 156*Power(t2,5) - 
                  39*Power(t2,6) - 10*Power(t2,7))) - 
            Power(s1,2)*t1*(-1 + t2)*
             (2*Power(-1 + t2,4)*
                (-1 - 2*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(10 + t2 - 2*Power(t2,2) + 3*Power(t2,3)) - 
               t1*Power(-1 + t2,3)*
                (1 - 22*t2 - 25*Power(t2,2) + 15*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*Power(-1 + t2,2)*
                (-9 - 46*t2 - 34*Power(t2,2) + 14*Power(t2,3) + 
                  3*Power(t2,4)) - 
               Power(t1,3)*(-22 + 27*t2 + 14*Power(t2,2) - 
                  29*Power(t2,3) + 8*Power(t2,4) + 2*Power(t2,5)) + 
               s*(-(Power(-1 + t2,3)*
                     (6 - 7*t2 - 11*Power(t2,2) + 7*Power(t2,3) + 
                       Power(t2,4))) + 
                  Power(t1,3)*
                   (-1 - 9*t2 - 21*Power(t2,2) + 5*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*Power(-1 + t2,2)*
                   (-10 - 31*t2 - 10*Power(t2,2) - 2*Power(t2,3) + 
                     14*Power(t2,4)) + 
                  Power(t1,2)*
                   (-26 + 18*t2 - 21*Power(t2,2) + 25*Power(t2,3) + 
                     23*Power(t2,4) - 19*Power(t2,5)))) + 
            s1*(Power(t1,6)*(-9 + 11*t2 - 3*Power(t2,2) + Power(t2,3)) - 
               Power(-1 + t2,4)*(1 + t2)*
                (6 + 2*(-7 + 3*s)*t2 + (8 - 13*s)*Power(t2,2) + 
                  (2 + 6*s - 5*Power(s,2))*Power(t2,3) + 
                  (-2 + s + Power(s,2))*Power(t2,4)) + 
               Power(t1,5)*(s*
                   (-43 - 5*t2 - 21*Power(t2,2) - 9*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  2*(8 - 26*t2 + 18*Power(t2,2) - Power(t2,3) + 
                     Power(t2,5))) - 
               t1*Power(-1 + t2,3)*
                (Power(-1 + t2,2)*
                   (-19 - 30*t2 - 32*Power(t2,2) + 11*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  Power(s,2)*t2*
                   (17 - 18*t2 + Power(t2,2) + Power(t2,3) + 
                     9*Power(t2,4)) + 
                  s*(13 - 8*t2 + 31*Power(t2,2) - 56*Power(t2,3) + 
                     35*Power(t2,4) - 10*Power(t2,5) - 5*Power(t2,6))) + 
               Power(t1,4)*(Power(-1 + t2,2)*
                   (-7 + 3*t2 + 10*Power(t2,2) + 4*Power(t2,4)) + 
                  Power(s,2)*
                   (-14 - t2 - 21*Power(t2,2) - 31*Power(t2,3) + 
                     Power(t2,4) + 6*Power(t2,5)) - 
                  s*(48 - 126*t2 + 73*Power(t2,2) + 65*Power(t2,3) - 
                     91*Power(t2,4) + 27*Power(t2,5) + 4*Power(t2,6))) + 
               Power(t1,3)*(-1 + t2)*
                (-(Power(-1 + t2,2)*
                     (-16 - 66*t2 + 5*Power(t2,2) + 3*Power(t2,3) + 
                       6*Power(t2,4))) + 
                  Power(s,2)*
                   (41 + 75*t2 + 24*Power(t2,2) + 54*Power(t2,3) + 
                     13*Power(t2,4) - 23*Power(t2,5)) + 
                  s*(-87 - 145*t2 + 178*Power(t2,2) + 177*Power(t2,3) - 
                     172*Power(t2,4) + 36*Power(t2,5) + 13*Power(t2,6))) \
- Power(t1,2)*Power(-1 + t2,2)*
                (-(Power(-1 + t2,2)*
                     (-35 - 139*t2 - 11*Power(t2,2) + 8*Power(t2,3) + 
                       6*Power(t2,4))) + 
                  Power(s,2)*
                   (12 + 76*t2 + 16*Power(t2,2) + 26*Power(t2,3) + 
                     17*Power(t2,4) - 27*Power(t2,5)) + 
                  s*(-63 - 188*t2 + 219*Power(t2,2) + 133*Power(t2,3) - 
                     139*Power(t2,4) + 24*Power(t2,5) + 14*Power(t2,6))))\
) + Power(s2,3)*(-(Power(s1,3)*Power(-1 + t2,3)*
               (Power(t1,2)*(1 + 7*t2 + 6*Power(t2,2)) + 
                 2*(1 - 2*t2 + Power(t2,3)) - 
                 3*t1*(-1 + 2*t2 + 2*Power(t2,2) + Power(t2,3)))) + 
            Power(s,3)*t2*(t1*t2*
                (-9 + 28*t2 + 12*Power(t2,2) + 20*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(t2,2)*(-6 + 25*t2 - 17*Power(t2,2) - 
                  3*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(-1 - 5*t2 - 27*Power(t2,2) - 
                  17*Power(t2,3) + 2*Power(t2,4))) - 
            (-1 + t2)*(3*Power(t1,4)*
                (-7 - 23*t2 - 12*Power(t2,2) + Power(t2,3) + 
                  Power(t2,4)) + 
               Power(-1 + t2,3)*t2*
                (-1 + t2 + 18*Power(t2,2) + Power(t2,3) + 
                  4*Power(t2,4) + 2*Power(t2,5)) - 
               t1*Power(-1 + t2,2)*t2*
                (17 + 76*t2 - 79*Power(t2,2) + 17*Power(t2,3) + 
                  28*Power(t2,4) + 5*Power(t2,5)) - 
               Power(t1,3)*(21 + 109*t2 - 115*Power(t2,2) - 
                  158*Power(t2,3) + Power(t2,4) + 21*Power(t2,5) + 
                  Power(t2,6)) + 
               Power(t1,2)*t2*
                (-81 + 163*t2 + 89*Power(t2,2) - 177*Power(t2,3) - 
                  36*Power(t2,4) + 38*Power(t2,5) + 4*Power(t2,6))) + 
            Power(s,2)*(-(Power(-1 + t2,2)*Power(t2,3)*
                  (26 + 69*t2 - 10*Power(t2,2) + 17*Power(t2,3))) + 
               Power(t1,2)*t2*
                (3 - 45*t2 - 232*Power(t2,2) + 16*Power(t2,3) + 
                  45*Power(t2,4) - 27*Power(t2,5)) + 
               Power(t1,3)*(-1 - 12*t2 - 35*Power(t2,2) - 
                  65*Power(t2,3) - 12*Power(t2,4) + 5*Power(t2,5)) + 
               t1*t2*(-5 - 77*t2 - 194*Power(t2,2) + 181*Power(t2,3) + 
                  136*Power(t2,4) - 80*Power(t2,5) + 39*Power(t2,6))) + 
            s*(Power(t1,4)*Power(-1 + t2,3)*(3 + 6*t2 + Power(t2,2)) + 
               Power(-1 + t2,3)*t2*
                (-2 - 28*t2 - 108*Power(t2,2) - 111*Power(t2,3) + 
                  5*Power(t2,4) + 18*Power(t2,5)) - 
               t1*Power(-1 + t2,2)*t2*
                (-11 - 93*t2 - 213*Power(t2,2) - 279*Power(t2,3) + 
                  34*Power(t2,4) + 42*Power(t2,5)) - 
               Power(t1,3)*(-1 + 17*t2 + 136*Power(t2,2) + 
                  102*Power(t2,3) - 45*Power(t2,4) + 21*Power(t2,5) + 
                  10*Power(t2,6)) + 
               Power(t1,2)*(1 - 10*t2 - 181*Power(t2,2) + 
                  267*Power(t2,3) + 105*Power(t2,4) - 230*Power(t2,5) + 
                  15*Power(t2,6) + 33*Power(t2,7))) + 
            Power(s1,2)*(-1 + t2)*
             (2*Power(t1,3)*(3 + 11*t2 + 2*Power(t2,2) + Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-9 - (-7 + s)*t2 + (6 - 8*s)*Power(t2,2) - 
                  (7 + 4*s)*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-1 + t2)*(13 + 14*t2 - 2*Power(t2,2) - 
                  12*Power(t2,3) - 19*Power(t2,4) + 6*Power(t2,5) + 
                  s*(-12 + 32*t2 + 31*Power(t2,2) + 4*Power(t2,3) - 
                     7*Power(t2,4))) + 
               Power(t1,2)*(-2*t2*
                   (-11 + 12*t2 + 5*Power(t2,2) - 12*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  s*(7 - 10*t2 - 35*Power(t2,2) - 16*Power(t2,3) + 
                     14*Power(t2,4)))) + 
            s1*(-(Power(t1,4)*
                  (-30 - 38*t2 + 53*Power(t2,2) + 9*Power(t2,3) + 
                    5*Power(t2,4) + Power(t2,5))) + 
               Power(-1 + t2,2)*
                (3 + (11 + 12*s)*t2 - 
                  (8 + 21*s + Power(s,2))*Power(t2,2) + 
                  (-5 - 84*s + 42*Power(s,2))*Power(t2,3) + 
                  (-37 + 81*s - 4*Power(s,2))*Power(t2,4) + 
                  (50 + 15*s + 3*Power(s,2))*Power(t2,5) - 
                  (16 + 3*s)*Power(t2,6) + 2*Power(t2,7)) + 
               Power(t1,3)*(s*
                   (25 + 129*t2 + 38*Power(t2,2) + 46*Power(t2,3) + 
                     13*Power(t2,4) - 11*Power(t2,5)) + 
                  2*(8 + 24*t2 - 81*Power(t2,2) + 26*Power(t2,3) + 
                     18*Power(t2,4) + 2*Power(t2,5) + 3*Power(t2,6))) - 
               t1*(-1 + t2)*(Power(s,2)*t2*
                   (-3 + 117*t2 + 47*Power(t2,2) + 29*Power(t2,3) - 
                     6*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (2 - 4*t2 + 94*Power(t2,2) - 20*Power(t2,3) - 
                     5*Power(t2,4) + Power(t2,5)) + 
                  s*(-5 - 32*t2 - 119*Power(t2,2) + 47*Power(t2,3) + 
                     128*Power(t2,4) - 43*Power(t2,5) + 24*Power(t2,6))) \
+ Power(t1,2)*(Power(s,2)*(5 + 25*t2 + 19*Power(t2,2) + 45*Power(t2,3) + 
                     36*Power(t2,4) - 10*Power(t2,5)) - 
                  2*Power(-1 + t2,2)*
                   (5 - 9*t2 - 17*Power(t2,2) + 27*Power(t2,3) + 
                     Power(t2,4) + 3*Power(t2,5)) + 
                  s*(-2 + 71*t2 - 135*Power(t2,2) + 35*Power(t2,3) + 
                     77*Power(t2,4) - 82*Power(t2,5) + 36*Power(t2,6))))))*
       T4(-1 + t2))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*Power(1 - s2 + t1 - t2,2)*
       Power(-1 + t2,2)*Power(t1 - s2*t2,3)) - 
    (8*(-(Power(s2,7)*Power(t2,2)*(s1 + t2)) + 
         Power(1 + t2,2)*Power(1 + (-1 + s)*t2,3)*
          (2 - 3*t2 + Power(t2,2)) - 
         Power(t1,6)*(1 + Power(s,2) + Power(s1,2) + t2 + s1*t2 - 
            s*(2 + 2*s1 + t2)) + 
         Power(s2,6)*t2*(-(t2*
               (-1 + t1*(-3 + s - 3*t2) + t2 - 2*s*t2 + 5*Power(t2,2))) + 
            s1*((s - t2)*t2 + t1*(2 + 4*t2))) + 
         Power(t1,5)*(-5 + t2 + 5*Power(t2,2) + Power(s1,3)*(1 + t2) - 
            Power(s,3)*(2 + t2) + 2*s1*(1 - t2 + Power(t2,2)) + 
            Power(s1,2)*(-8 + 3*t2 + 2*Power(t2,2)) + 
            Power(s,2)*(-1 + 10*t2 + 2*Power(t2,2) + s1*(5 + 3*t2)) - 
            s*(-10 + 13*t2 + 7*Power(t2,2) + Power(s1,2)*(4 + 3*t2) + 
               s1*(-11 + 13*t2 + 4*Power(t2,2)))) + 
         Power(t1,4)*(-5 + 16*t2 + 6*Power(t2,2) - 7*Power(t2,3) + 
            Power(s,3)*t2*(2 + 4*t2 + Power(t2,2)) + 
            Power(s1,2)*(1 + 24*t2 - 2*Power(t2,2) - 3*Power(t2,3)) - 
            Power(s1,3)*(2 + Power(t2,3)) - 
            s1*(12 + 2*t2 - 4*Power(t2,2) + 5*Power(t2,3)) - 
            Power(s,2)*(-8 - 32*t2 + 24*Power(t2,2) + 10*Power(t2,3) + 
               s1*(2 + 8*Power(t2,2) + 3*Power(t2,3))) + 
            s*(-16 - 70*t2 + 22*Power(t2,2) + 20*Power(t2,3) + 
               Power(s1,2)*(4 - 2*t2 + 4*Power(t2,2) + 3*Power(t2,3)) + 
               s1*(9 - 63*t2 + 14*Power(t2,2) + 13*Power(t2,3)))) - 
         t1*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(5 + 2*t2 - 12*Power(t2,2) - 2*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(-1 + t2,2)*(-8 - 14*t2 - Power(t2,2) + 2*Power(t2,3) + 
               s1*(-1 - 5*t2 - Power(t2,2) + 3*Power(t2,3))) - 
            s*(-1 + t2)*(-1 - 21*t2 - 35*Power(t2,2) + 9*Power(t2,4) + 
               s1*(6 + 8*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4)))) - 
         Power(t1,3)*(Power(s,3)*t2*
             (1 - 8*t2 + 5*Power(t2,2) + 4*Power(t2,3)) - 
            (-1 + t2)*(15 - 8*t2 - 3*Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,3)*(1 + t2 - Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(8 - 22*t2 + 4*Power(t2,3)) + 
               s1*(-29 - 22*t2 - 2*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s,2)*(-25 + 14*t2 + 92*Power(t2,2) - 19*Power(t2,3) - 
               21*Power(t2,4) + 
               s1*(13 - 24*t2 + 18*Power(t2,2) - 9*Power(t2,4))) + 
            s*(27 - 33*t2 - 139*Power(t2,2) + 3*Power(t2,3) + 
               28*Power(t2,4) + 
               Power(s1,2)*(-22 + 17*t2 - 3*Power(t2,2) - 
                  7*Power(t2,3) + 6*Power(t2,4)) + 
               s1*(41 + 93*t2 - 90*Power(t2,2) - 11*Power(t2,3) + 
                  20*Power(t2,4)))) + 
         Power(t1,2)*(Power(s,3)*t2*
             (3 + 2*t2 - 17*Power(t2,2) + Power(t2,3) + 6*Power(t2,4)) - 
            Power(-1 + t2,2)*(-16 - 8*t2 - Power(t2,2) + 3*Power(t2,3) + 
               Power(s1,2)*(2 - t2 - 2*Power(t2,2) + 3*Power(t2,3)) + 
               s1*(-4 - 34*t2 - Power(t2,2) + 5*Power(t2,3))) + 
            s*(-1 + t2)*(10 - 70*t2 - 113*Power(t2,2) + 22*Power(t2,4) + 
               Power(s1,2)*(-4 + 3*t2 + Power(t2,2) - 5*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(19 + 55*t2 - 50*Power(t2,2) - 14*Power(t2,3) + 
                  17*Power(t2,4))) + 
            Power(s,2)*(-1 - 38*t2 - 10*Power(t2,2) + 100*Power(t2,3) + 
               5*Power(t2,4) - 23*Power(t2,5) + 
               s1*(4 + 8*t2 - 34*Power(t2,2) + 14*Power(t2,3) + 
                  12*Power(t2,4) - 9*Power(t2,5)))) - 
         Power(s2,5)*(Power(s1,3) - 
            Power(s1,2)*t2*(-1 + s + t1 + 2*t1*t2) + 
            s1*(-(Power(t2,2)*
                  (8 + s*(-3 + t2) - 12*t2 + 3*Power(t2,2))) + 
               Power(t1,2)*(1 + 8*t2 + 6*Power(t2,2)) + 
               t1*t2*(-6*Power(t2,2) + s*(2 + 5*t2))) + 
            t2*(Power(t1,2)*(3 - 2*s + 9*t2 - 3*s*t2 + 3*Power(t2,2)) + 
               t1*(2 + (1 + 5*s - Power(s,2))*t2 + 
                  3*(-7 + 4*s)*Power(t2,2) - 11*Power(t2,3)) + 
               t2*(Power(s,2)*t2 + t2*(-11 + t2 + 9*Power(t2,2)) - 
                  s*(-1 + t2 + 12*Power(t2,2))))) + 
         Power(s2,4)*(Power(t1,2)*
             (1 + (5 + 4*s - 2*Power(s,2))*t2 + 
               (-27 + 32*s - 4*Power(s,2))*Power(t2,2) + 
               (-40 + 19*s)*Power(t2,3) - 7*Power(t2,4)) + 
            Power(s1,3)*(3 - 4*t2 - 2*Power(t2,2) + 
               2*t1*(1 + t2 + Power(t2,2))) + 
            Power(t1,3)*(1 + 9*t2 + 9*Power(t2,2) + Power(t2,3) - 
               s*(1 + 6*t2 + 3*Power(t2,2))) + 
            t1*t2*(Power(s,2)*t2*(1 + 13*t2) + 
               s*(2 + 3*t2 - 41*Power(t2,2) - 38*Power(t2,3)) + 
               t2*(-30 - 23*t2 + 37*Power(t2,2) + 13*Power(t2,3))) + 
            Power(t2,2)*(-8 + 5*t2 + 16*Power(t2,2) + 4*Power(t2,3) - 
               7*Power(t2,4) - 2*Power(s,2)*(1 + t2 + 4*Power(t2,2)) + 
               s*(8 - 16*t2 + 7*Power(t2,2) + 23*Power(t2,3))) + 
            Power(s1,2)*(3 + 7*t2 + (4 + 7*s)*Power(t2,2) + 
               6*Power(t2,3) - Power(t1,2)*(1 + 6*t2 + 6*Power(t2,2)) - 
               t1*(-1 + 5*Power(t2,2) - 5*Power(t2,3) + 
                  4*s*(1 + t2 + Power(t2,2)))) + 
            s1*(Power(t1,2)*t2*(3 - 8*t2 - 10*Power(t2,2)) + 
               4*Power(t1,3)*(1 + 3*t2 + Power(t2,2)) + 
               t1*t2*(-18 + 15*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               t2*(2 - 2*t2 + 7*Power(t2,2) - 27*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s,2)*t2*((-1 + t2)*t2 + 2*t1*(1 + t2)) + 
               s*(Power(t1,2)*(1 + 12*t2 + 11*Power(t2,2)) + 
                  t1*(t2 + 11*Power(t2,2) - 15*Power(t2,3)) - 
                  t2*(-1 + 3*t2 + 3*Power(t2,2) + 2*Power(t2,3))))) - 
         Power(s2,3)*(3*Power(t1,3) + 3*Power(t1,4) + t2 - 17*t1*t2 - 
            27*Power(t1,2)*t2 - 11*Power(t1,3)*t2 + 9*Power(t1,4)*t2 - 
            2*Power(t2,2) - 4*t1*Power(t2,2) - 
            65*Power(t1,2)*Power(t2,2) - 50*Power(t1,3)*Power(t2,2) + 
            3*Power(t1,4)*Power(t2,2) - 9*Power(t2,3) + 
            55*t1*Power(t2,3) + 40*Power(t1,2)*Power(t2,3) - 
            23*Power(t1,3)*Power(t2,3) + 13*Power(t2,4) + 
            35*t1*Power(t2,4) + 46*Power(t1,2)*Power(t2,4) - 
            Power(t1,3)*Power(t2,4) - 3*Power(t2,5) - 24*t1*Power(t2,5) + 
            4*Power(t1,2)*Power(t2,5) - 2*Power(t2,6) - 
            5*t1*Power(t2,6) + 2*Power(t2,7) - 
            Power(s,3)*t2*(-3*t1*t2*(1 + t2) + Power(t2,2)*(2 + t2) + 
               Power(t1,2)*(1 + 2*t2)) + 
            Power(s1,3)*(Power(t1,2)*(1 + 7*t2 + 6*Power(t2,2)) + 
               2*(1 - 2*t2 + Power(t2,3)) - 
               3*t1*(-1 + 2*t2 + 2*Power(t2,2) + Power(t2,3))) - 
            Power(s,2)*(Power(t1,3)*(1 + 9*t2 + 5*Power(t2,2)) - 
               3*Power(t1,2)*t2*(1 + 10*t2 + 9*Power(t2,2)) - 
               Power(t2,3)*(10 + 7*t2 + 17*Power(t2,2)) + 
               t1*t2*(5 + 14*t2 + 25*Power(t2,2) + 39*Power(t2,3))) + 
            s*(-(Power(t1,4)*(3 + 6*t2 + Power(t2,2))) + 
               Power(t1,3)*(1 + 28*t2 + 47*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(1 + 11*t2 - 25*Power(t2,2) - 
                  114*Power(t2,3) - 33*Power(t2,4)) + 
               t1*t2*(11 - 64*t2 - 51*Power(t2,2) + 84*Power(t2,3) + 
                  42*Power(t2,4)) + 
               t2*(2 + 28*t2 + 52*Power(t2,2) + 59*Power(t2,3) - 
                  9*Power(t2,4) - 18*Power(t2,5))) + 
            Power(s1,2)*(9 + (-7 + s)*t2 + (-6 + 8*s)*Power(t2,2) + 
               7*Power(t2,3) - 3*Power(t2,4) - 
               2*Power(t1,3)*(2 + 6*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(2*t2*(8 + t2 - 6*Power(t2,2)) + 
                  s*(7 + 16*t2 + 14*Power(t2,2))) + 
               t1*(13 + 27*t2 + 31*Power(t2,2) + 15*Power(t2,3) - 
                  6*Power(t2,4) + 
                  s*(-12 + 20*t2 + 15*Power(t2,2) + 7*Power(t2,3)))) + 
            s1*(3 + 2*(7 + 6*s)*t2 - 
               (-6 + 9*s + Power(s,2))*Power(t2,2) + 
               (-9 - 45*s + Power(s,2))*Power(t2,3) - 
               (26 + 14*s + 3*Power(s,2))*Power(t2,4) + 
               (14 + 3*s)*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,4)*(6 + 8*t2 + Power(t2,2)) + 
               Power(t1,3)*(s*(7 + 24*t2 + 11*Power(t2,2)) + 
                  2*(1 + t2 - 9*Power(t2,2) - 3*Power(t2,3))) - 
               t1*(-2 + 4*t2 - 16*Power(t2,2) + 66*Power(t2,3) + 
                  9*Power(t2,4) - Power(t2,5) + 
                  3*Power(s,2)*t2*(1 + 3*t2 + 2*Power(t2,2)) + 
                  s*(5 + 42*t2 + 6*Power(t2,2) + 19*Power(t2,3) - 
                     24*Power(t2,4))) + 
               Power(t1,2)*(5*Power(s,2)*(1 + 2*t2 + 2*Power(t2,2)) - 
                  s*(2 - 29*t2 + 18*Power(t2,2) + 36*Power(t2,3)) + 
                  2*(-5 - 5*t2 + 12*Power(t2,2) + 2*Power(t2,3) + 
                     3*Power(t2,4))))) + 
         Power(s2,2)*(1 + 7*t2 + 12*s*t2 + 2*Power(t2,2) + 
            35*s*Power(t2,2) + 13*Power(s,2)*Power(t2,2) - 
            34*Power(t2,3) + 40*s*Power(t2,3) + 
            24*Power(s,2)*Power(t2,3) + 27*Power(t2,4) - 
            60*s*Power(t2,4) + 16*Power(s,2)*Power(t2,4) + 
            4*Power(s,3)*Power(t2,4) - Power(t2,5) - 44*s*Power(t2,5) - 
            6*Power(s,2)*Power(t2,5) + 3*Power(s,3)*Power(t2,5) - 
            2*Power(t2,6) + 12*s*Power(t2,6) - 
            14*Power(s,2)*Power(t2,6) + 5*s*Power(t2,7) + 
            Power(t1,5)*(3 - 3*s + 3*t2 - 2*s*t2) + 
            Power(t1,4)*(-(Power(s,2)*(5 + 10*t2 + 2*Power(t2,2))) - 
               t2*(24 + 26*t2 + 3*Power(t2,2)) + 
               s*(8 + 37*t2 + 22*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*t1*(1 - 2*t2 - 4*Power(t2,2) + 4*Power(t2,3) + 
               Power(t2,4) + Power(t1,2)*(1 + 9*t2 + 6*Power(t2,2)) - 
               t1*(5 + 6*Power(t2,2) + 7*Power(t2,3))) + 
            Power(t1,3)*(-8 - 57*t2 + 54*Power(t2,3) + 13*Power(t2,4) - 
               2*Power(s,3)*(1 + 3*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(3 + 14*t2 + 59*Power(t2,2) + 
                  17*Power(t2,3)) + 
               s*(7 + 27*t2 - 102*Power(t2,2) - 81*Power(t2,3) - 
                  7*Power(t2,4))) + 
            Power(t1,2)*(-9 - 25*t2 + 57*Power(t2,2) + 76*Power(t2,3) - 
               22*Power(t2,4) - 17*Power(t2,5) + 
               Power(s,3)*t2*(7 + 13*t2 + 11*Power(t2,2)) - 
               Power(s,2)*t2*
                (-11 + t2 + 92*Power(t2,2) + 42*Power(t2,3)) + 
               s*(3 - 76*t2 - 157*Power(t2,2) + 29*Power(t2,3) + 
                  119*Power(t2,4) + 16*Power(t2,5))) - 
            t1*(-1 + 4*t2 + 27*Power(t2,2) - 43*Power(t2,3) + 
               14*Power(t2,4) + 6*Power(t2,5) - 7*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(5 + 11*t2 + 10*Power(t2,2)) + 
               Power(s,2)*t2*
                (16 + 26*t2 + 21*Power(t2,2) - 49*Power(t2,3) - 
                  41*Power(t2,4)) + 
               s*(1 - 24*t2 - 98*Power(t2,2) - 220*Power(t2,3) - 
                  86*Power(t2,4) + 70*Power(t2,5) + 15*Power(t2,6))) - 
            Power(s1,2)*(2*Power(t1,4)*(3 + 5*t2 + Power(t2,2)) + 
               (-1 + t2)*(6 - 7*t2 - 5*Power(t2,2) + 
                  (5 + 2*s)*Power(t2,3) + Power(t2,4)) + 
               Power(t1,3)*(11 + 25*t2 - 13*Power(t2,2) - 
                  9*Power(t2,3) + s*(6 + 27*t2 + 16*Power(t2,2))) - 
               Power(t1,2)*(20 + 48*t2 + 63*Power(t2,2) - 
                  2*Power(t2,3) - 9*Power(t2,4) + 
                  s*(-2 + 8*t2 + 19*Power(t2,2) + 23*Power(t2,3))) + 
               t1*(-14 + 4*t2 + Power(t2,2) + 9*Power(t2,3) + 
                  3*Power(t2,4) - 3*Power(t2,5) + 
                  s*(14 - 39*t2 - 21*Power(t2,2) + 15*Power(t2,3) + 
                     4*Power(t2,4)))) + 
            s1*(9 + (-2 + 17*s)*t2 - 15*(1 + s)*Power(t2,2) + 
               (4 - 43*s + Power(s,2))*Power(t2,3) + 
               (-3 + 35*s - 9*Power(s,2))*Power(t2,4) + 
               (10 + 7*s + 3*Power(s,2))*Power(t2,5) - 
               (3 + s)*Power(t2,6) + 2*Power(t1,5)*(2 + t2) + 
               Power(t1,4)*(4 - 6*t2 - 12*Power(t2,2) - Power(t2,3) + 
                  s*(13 + 20*t2 + 4*Power(t2,2))) + 
               Power(t1,3)*(-13 + 12*t2 + 3*Power(t2,2) + 
                  12*Power(t2,3) + 2*Power(t2,4) + 
                  2*Power(s,2)*(4 + 12*t2 + 7*Power(t2,2)) + 
                  s*(17 + 27*t2 - 61*Power(t2,2) - 26*Power(t2,3))) - 
               Power(t1,2)*(2 + t2 + 51*Power(t2,2) + 29*Power(t2,3) + 
                  6*Power(t2,4) + Power(t2,5) + 
                  Power(s,2)*
                   (-13 + 12*t2 + 23*Power(t2,2) + 27*Power(t2,3)) + 
                  s*(45 + 38*t2 + 93*Power(t2,2) - 32*Power(t2,3) - 
                     42*Power(t2,4))) + 
               t1*(20 + 44*t2 + 24*Power(t2,2) - 114*Power(t2,3) + 
                  21*Power(t2,4) + 5*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-16 - 9*t2 + 11*Power(t2,2) + 10*Power(t2,3)) - 
                  s*(12 + 42*t2 + 69*Power(t2,2) + 19*Power(t2,3) - 
                     2*Power(t2,4) + 19*Power(t2,5))))) + 
         s2*(-3 + (-1 + s)*Power(t1,6) - t2 - 16*s*t2 + 11*Power(t2,2) - 
            6*s*Power(t2,2) - 17*Power(s,2)*Power(t2,2) + 4*Power(t2,3) + 
            32*s*Power(t2,3) - 14*Power(s,2)*Power(t2,3) - 
            4*Power(s,3)*Power(t2,3) - 22*Power(t2,4) + 26*s*Power(t2,4) + 
            14*Power(s,2)*Power(t2,4) - 3*Power(s,3)*Power(t2,4) + 
            10*Power(t2,5) - 39*s*Power(t2,5) + 
            27*Power(s,2)*Power(t2,5) + Power(s,3)*Power(t2,5) + 
            2*Power(t2,6) - 2*s*Power(t2,6) - 6*Power(s,2)*Power(t2,6) + 
            3*Power(s,3)*Power(t2,6) - Power(t2,7) + 5*s*Power(t2,7) - 
            4*Power(s,2)*Power(t2,7) + 
            t1*(-9 + (-19 - 40*s + 15*Power(s,2))*t2 + 
               (62 - 141*s + 5*Power(s,2) + 13*Power(s,3))*Power(t2,2) + 
               (-29 + 47*s - 60*Power(s,2) + 4*Power(s,3))*Power(t2,3) - 
               (4 - 163*s + 76*Power(s,2) + 8*Power(s,3))*Power(t2,4) - 
               (4 + 5*s - 36*Power(s,2) + 11*Power(s,3))*Power(t2,5) + 
               (3 - 24*s + 14*Power(s,2))*Power(t2,6)) + 
            Power(t1,5)*(3 + 11*t2 + 3*Power(t2,2) + 
               Power(s,2)*(5 + 3*t2) - s*(9 + 14*t2 + 2*Power(t2,2))) + 
            Power(t1,4)*(16 + 17*t2 - 22*Power(t2,2) - 14*Power(t2,3) + 
               Power(s,3)*(3 + 7*t2 + 2*Power(t2,2)) - 
               Power(s,2)*(-3 + 31*t2 + 27*Power(t2,2) + 2*Power(t2,3)) + 
               s*(-23 + 16*t2 + 61*Power(t2,2) + 14*Power(t2,3))) + 
            Power(s1,3)*Power(t1,2)*
             (-(Power(t1,2)*(2 + 5*t2 + 2*Power(t2,2))) + 
               t1*(7 - 2*t2 + 2*Power(t2,2) + 5*Power(t2,3)) - 
               2*(-1 + t2 - Power(t2,2) + Power(t2,4))) - 
            Power(t1,3)*(-16 + 13*t2 + 61*Power(t2,2) + Power(t2,3) - 
               19*Power(t2,4) + 
               Power(s,3)*(-2 + 5*t2 + 20*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s,2)*(23 + 30*t2 - 21*Power(t2,2) - 
                  69*Power(t2,3) - 10*Power(t2,4)) + 
               s*(-28 - 115*t2 - 102*Power(t2,2) + 99*Power(t2,3) + 
                  36*Power(t2,4))) + 
            Power(t1,2)*(2 + 33*t2 - 53*Power(t2,2) + 16*Power(t2,3) + 
               12*Power(t2,4) - 10*Power(t2,5) + 
               Power(s,3)*t2*
                (-11 + t2 + 20*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s,2)*(1 + 14*t2 + 72*Power(t2,2) + 54*Power(t2,3) - 
                  75*Power(t2,4) - 18*Power(t2,5)) + 
               s*(7 - 13*t2 - 200*Power(t2,2) - 237*Power(t2,3) + 
                  58*Power(t2,4) + 43*Power(t2,5))) + 
            Power(s1,2)*t1*(Power(t1,4)*(4 + 3*t2) + 
               Power(t1,3)*(18 + 7*t2 - 12*Power(t2,2) - 2*Power(t2,3)) + 
               2*Power(-1 + t2,2)*
                (-1 - 2*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-11 - 52*t2 - 34*Power(t2,2) + 
                  14*Power(t2,3) + 3*Power(t2,4)) - 
               t1*(-1 + 23*t2 + 3*Power(t2,2) - 40*Power(t2,3) + 
                  14*Power(t2,4) + Power(t2,5)) + 
               s*(6 - 13*t2 - 4*Power(t2,2) + 18*Power(t2,3) - 
                  6*Power(t2,4) - Power(t2,5) + 
                  Power(t1,3)*(7 + 17*t2 + 6*Power(t2,2)) - 
                  Power(t1,2)*
                   (14 - 14*t2 + 15*Power(t2,2) + 19*Power(t2,3)) + 
                  t1*(-10 - 19*t2 - 10*Power(t2,2) - 2*Power(t2,3) + 
                     14*Power(t2,4)))) + 
            s1*(-Power(t1,6) + 
               Power(t1,5)*(-2 - 9*s + 6*t2 - 6*s*t2 + 2*Power(t2,2)) + 
               (-1 + Power(t2,2))*
                (6 + 2*(-7 + 3*s)*t2 + (8 - 13*s)*Power(t2,2) + 
                  (2 + 6*s - 5*Power(s,2))*Power(t2,3) + 
                  (-2 + s + Power(s,2))*Power(t2,4)) - 
               Power(t1,4)*(-1 + 2*t2 + 4*Power(t2,2) + 4*Power(t2,3) + 
                  Power(s,2)*(8 + 19*t2 + 6*Power(t2,2)) - 
                  s*(-30 + 14*t2 + 39*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,3)*(10 + 24*t2 + 17*Power(t2,2) + 3*Power(t2,3) + 
                  6*Power(t2,4) + 
                  Power(s,2)*(-1 - 11*t2 + 33*Power(t2,2) + 
                     23*Power(t2,3)) + 
                  s*(23 + 79*t2 + 61*Power(t2,2) - 62*Power(t2,3) - 
                     13*Power(t2,4))) + 
               Power(t1,2)*(-35 - 74*t2 + 98*Power(t2,2) + 
                  19*Power(t2,3) - 2*Power(t2,4) - 6*Power(t2,5) + 
                  Power(s,2)*(-12 + 32*t2 + 16*Power(t2,2) - 
                     10*Power(t2,3) - 27*Power(t2,4)) + 
                  s*(63 + 107*t2 + 38*Power(t2,2) - 101*Power(t2,3) + 
                     38*Power(t2,4) + 14*Power(t2,5))) + 
               t1*(Power(-1 + t2,2)*
                   (-19 - 30*t2 - 32*Power(t2,2) + 11*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  Power(s,2)*t2*
                   (17 - 18*t2 + Power(t2,2) + Power(t2,3) + 
                     9*Power(t2,4)) + 
                  s*(13 - 8*t2 + 31*Power(t2,2) - 56*Power(t2,3) + 
                     35*Power(t2,4) - 10*Power(t2,5) - 5*Power(t2,6))))))*
       T5(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(-s + s1 - t2)*(1 - s2 + t1 - t2)*
       Power(t1 - s2*t2,3)));
   return a;
};
