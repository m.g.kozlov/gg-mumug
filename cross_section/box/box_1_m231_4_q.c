#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m231_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(8*Power(s1,7)*(-1 + t1)*Power(t1,2)*t2*
          (-t1 + 2*Power(t1,2) + t2 - 3*t1*t2 + Power(t2,2)) + 
         Power(s1,6)*t1*(58*Power(t1,5)*t2 + 8*Power(t2,3)*(1 + t2) - 
            Power(t1,4)*(2 + (143 + 24*s + 2*s2)*t2 + 125*Power(t2,2)) + 
            Power(t1,3)*(4 + (91 + 32*s + 25*s2)*t2 + 
               5*(46 + 8*s + s2)*Power(t2,2) + 97*Power(t2,3)) - 
            Power(t1,2)*(2 + (8 + 8*s + 17*s2)*t2 + 
               (95 + 48*s + 40*s2)*Power(t2,2) + 
               (121 + 16*s - 3*s2)*Power(t2,3) + 24*Power(t2,4)) + 
            t1*t2*(s2*(6 + 11*t2 + 9*Power(t2,2)) + 
               2*(-5 + (7 + 4*s)*t2 + (2 + 8*s)*Power(t2,2) + 
                  8*Power(t2,3)))) + 
         Power(s1,5)*t1*(78*Power(t1,6)*t2 - 
            2*Power(t1,5)*(4 + (155 + 44*s)*t2 + 111*Power(t2,2)) + 
            Power(t1,4)*(14 + 
               (280 + 8*Power(s,2) + 94*s2 - 2*Power(s2,2) + 
                  s*(173 + 10*s2))*t2 + 
               (517 + 203*s + 12*s2)*Power(t2,2) + 265*Power(t2,3)) - 
            Power(t1,3)*(4 + (8 + 8*Power(s,2) + 115*s2 + 
                  2*Power(s2,2) + s*(45 + 46*s2))*t2 + 
               (208 + 16*Power(s,2) + 153*s2 + 12*Power(s2,2) + 
                  s*(325 + 8*s2))*Power(t2,2) + 
               (339 + 158*s - 6*s2)*Power(t2,3) + 133*Power(t2,4)) + 
            Power(t1,2)*(-2 + 
               2*(-37 + 10*s2 + 7*Power(s2,2) + s*(-2 + 6*s2))*t2 + 
               (-43 + 16*Power(s,2) + 69*s2 + 14*Power(s2,2) + 
                  s*(50 + 68*s2))*Power(t2,2) + 
               (-133 + 8*Power(s,2) + s*(167 - 2*s2) + 97*s2 + 
                  2*Power(s2,2))*Power(t2,3) + 
               (78 + 43*s - 12*s2)*Power(t2,4) + 24*Power(t2,5)) - 
            Power(t2,2)*(2*Power(s2,2)*(1 + t2) + 
               s2*(7 + 15*t2 + 10*Power(t2,2)) + 
               2*(-5 + 4*(3 + s)*t2 + (19 + 8*s)*Power(t2,2) + 
                  12*Power(t2,3))) + 
            t1*t2*(10 - 10*Power(s2,2) + 6*(7 + 2*s)*t2 + 
               (111 + 11*s - 8*Power(s,2))*Power(t2,2) - 
               3*(-47 + 5*s)*Power(t2,3) - 
               s2*(-25 + (17 + 12*s)*t2 + (-32 + 22*s)*Power(t2,2) + 
                  26*Power(t2,3)))) + 
         Power(s1,4)*(45*Power(t1,8)*t2 - 
            Power(t1,7)*(12 + (326 + 119*s - 16*s2)*t2 + 
               168*Power(t2,2)) + 
            Power(t1,6)*(16 + 
               (343 + 31*Power(s,2) + 190*s2 - 11*Power(s2,2) + 
                  s*(324 + 33*s2))*t2 + 
               4*(146 + 86*s + s2)*Power(t2,2) + 292*Power(t2,3)) - 
            Power(t1,5)*(-8 + 
               (-117 + 290*s2 + 37*Power(s2,2) - 2*Power(s2,3) + 
                  Power(s,2)*(49 + s2) + 
                  2*s*(43 + 63*s2 + 3*Power(s2,2)))*t2 + 
               (-50 + 88*Power(s,2) + 274*s2 + 39*Power(s2,2) + 
                  8*s*(80 + 3*s2))*Power(t2,2) + 
               3*(115 + 124*s + s2)*Power(t2,3) + 231*Power(t2,4)) + 
            Power(t1,4)*(-16 + 
               (-236 - 62*s2 + 115*Power(s2,2) + Power(s2,3) + 
                  Power(s,2)*(6 + 13*s2) + 
                  s*(-44 + 29*s2 + 8*Power(s2,2)))*t2 + 
               (-623 + 83*s2 + 86*Power(s2,2) + 3*Power(s2,3) + 
                  2*Power(s,2)*(46 + s2) + 
                  s*(-103 + 242*s2 + 8*Power(s2,2)))*Power(t2,2) + 
               (-819 + 331*s + 83*Power(s,2) + 216*s2 - 27*s*s2 + 
                  24*Power(s2,2))*Power(t2,3) + 
               2*(-43 + 90*s - 19*s2)*Power(t2,4) + 76*Power(t2,5)) - 
            Power(t1,3)*(-4 + 
               (-34 - 193*s2 + 67*Power(s2,2) + 8*Power(s2,3) + 
                  s*(3 - 16*s2 + 2*Power(s2,2)))*t2 + 
               (-231 - 178*s2 + 73*Power(s2,2) + 6*Power(s2,3) + 
                  Power(s,2)*(4 + 26*s2) + 
                  s*(-165 + 22*s2 + 8*Power(s2,2)))*Power(t2,2) + 
               (-725 - 240*s2 + 5*Power(s2,2) - Power(s2,3) + 
                  Power(s,2)*(37 + s2) + 
                  2*s*(-203 + 75*s2 + Power(s2,2)))*Power(t2,3) + 
               (-844 + 26*Power(s,2) + 115*s2 + 4*Power(s2,2) - 
                  18*s*(1 + s2))*Power(t2,4) + 
               3*(-37 + 11*s - 5*s2)*Power(t2,5) + 8*Power(t2,6)) + 
            Power(t2,3)*(Power(s2,3) + 2*Power(s2,2)*(1 + t2) + 
               2*t2*(1 + t2) + s2*(1 + 4*t2 + Power(t2,2))) + 
            t1*Power(t2,2)*(-9 - 3*(11 + s)*t2 - Power(s2,3)*t2 + 
               (13 + 37*s + 8*Power(s,2))*Power(t2,2) + 
               (7 + 30*s)*Power(t2,3) + 24*Power(t2,4) + 
               Power(s2,2)*(10 + (7 + 2*s)*t2 + 4*Power(t2,2)) + 
               s2*(-26 + 2*(19 + 6*s)*t2 + 5*(3 + 4*s)*Power(t2,2) + 
                  25*Power(t2,3))) - 
            Power(t1,2)*t2*(-11 - (103 + 6*s)*t2 + 
               2*(96 + 79*s + 5*Power(s,2))*Power(t2,2) + 
               (254 + 247*s + 6*Power(s,2))*Power(t2,3) + 
               (268 + 33*s)*Power(t2,4) + 16*Power(t2,5) + 
               Power(s2,3)*(-5 - 3*t2 + Power(t2,2)) + 
               2*Power(s2,2)*t2*(-8 + 14*t2 + Power(t2,2)) + 
               s2*(35 + 7*(19 + 4*s)*t2 + 
                  (120 + 27*s - 13*Power(s,2))*Power(t2,2) + 
                  (154 - 34*s)*Power(t2,3) - 31*Power(t2,4)))) + 
         Power(s1,3)*(8*Power(t1,9)*t2 - 
            Power(t1,8)*(8 + 3*(63 + 23*s - 8*s2)*t2 + 50*Power(t2,2)) + 
            Power(t1,7)*(4 + (157 + 42*Power(s,2) + 259*s2 - 
                  16*Power(s2,2) + s*(239 + 31*s2))*t2 + 
               (382 + 243*s - 7*s2)*Power(t2,2) + 141*Power(t2,3)) - 
            Power(t1,6)*(-24 + 
               (-333 + Power(s,3) + 373*s2 + 126*Power(s2,2) - 
                  4*Power(s2,3) + Power(s,2)*(127 + 7*s2) + 
                  2*s*(24 + 36*s2 + 5*Power(s2,2)))*t2 + 
               (-370 + 138*Power(s,2) + 407*s2 + 47*Power(s2,2) + 
                  4*s*(131 + 6*s2))*Power(t2,2) + 
               (286 + 344*s + 17*s2)*Power(t2,3) + 167*Power(t2,4)) + 
            Power(t1,5)*(-26 + 
               (-376 - 13*Power(s,3) - 251*s2 + 301*Power(s2,2) + 
                  16*Power(s2,3) + 14*Power(s,2)*(2 + 5*s2) + 
                  s*(-61 - 65*s2 + 3*Power(s2,2)))*t2 + 
               (-1264 + 9*Power(s,3) + 53*s2 + 248*Power(s2,2) + 
                  8*Power(s2,3) + 4*Power(s,2)*(52 + s2) + 
                  s*(-633 + 189*s2 + 17*Power(s2,2)))*Power(t2,2) + 
               (-1279 + 164*Power(s,2) + s*(101 - 49*s2) + 287*s2 + 
                  41*Power(s2,2))*Power(t2,3) + 
               (-167 + 241*s - 39*s2)*Power(t2,4) + 84*Power(t2,5)) + 
            Power(t1,4)*(4 + (9 + 2*Power(s,3) + 
                  Power(s,2)*(33 - 39*s2) + 442*s2 - 131*Power(s2,2) - 
                  54*Power(s2,3) + 4*s*(-8 + 25*s2 + 4*Power(s2,2)))*t2 \
+ (297 + 31*Power(s,3) + Power(s,2)*(26 - 144*s2) + 877*s2 - 
                  274*Power(s2,2) - 39*Power(s2,3) + 
                  s*(398 + 334*s2 + 9*Power(s2,2)))*Power(t2,2) + 
               (1351 - 15*Power(s,3) + 386*s2 + 17*Power(s2,2) + 
                  8*Power(s2,3) + Power(s,2)*(1 + 13*s2) - 
                  2*s*(-815 + 127*s2 + 6*Power(s2,2)))*Power(t2,3) - 
               2*(-799 + 41*Power(s,2) + 71*s2 + 2*Power(s2,2) - 
                  s*(228 + 29*s2))*Power(t2,4) + 
               (382 - 77*s + 39*s2)*Power(t2,5) - 16*Power(t2,6)) + 
            Power(t1,3)*(2 + (65 - 84*s2 - 53*Power(s2,2) + 
                  44*Power(s2,3) + s*(7 - 18*s2 - 9*Power(s2,2)))*t2 - 
               (-406 + 4*Power(s,3) + Power(s,2)*(72 - 68*s2) + 
                  332*s2 + 48*Power(s2,2) - 49*Power(s2,3) + 
                  3*s*(-33 + 73*s2 + 15*Power(s2,2)))*Power(t2,2) - 
               (23*Power(s,3) + Power(s,2)*(138 - 78*s2) + 
                  s*(522 + 439*s2 + 11*Power(s2,2)) + 
                  3*(-7 + 165*s2 + 28*Power(s2,2) + 5*Power(s2,3)))*
                Power(t2,3) + 
               (-453 + 7*Power(s,3) - 574*s2 - 53*Power(s2,2) - 
                  2*Power(s2,3) - 2*Power(s,2)*(59 + 5*s2) + 
                  s*(-1202 + 165*s2 + 5*Power(s2,2)))*Power(t2,4) + 
               (-821 + 14*Power(s,2) + 131*s2 + 2*Power(s2,2) - 
                  2*s*(155 + 8*s2))*Power(t2,5) + 
               2*(-74 + 3*s - 3*s2)*Power(t2,6)) - 
            Power(t2,3)*(1 + s2 + t2)*
             (-1 + (2 + s)*t2 - (-3 + s)*Power(t2,2) + 
               Power(s2,2)*(3 + 2*t2) + s2*t2*(2 - s + 2*t2)) - 
            t1*Power(t2,2)*(8 + (31 + 3*s)*t2 + 
               (-152 - 75*s + 6*Power(s,2))*Power(t2,2) + 
               (-147 - 41*s + 2*Power(s,2))*Power(t2,3) + 
               (-92 + 9*s)*Power(t2,4) + 8*Power(t2,5) + 
               Power(s2,3)*(5 - 7*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(-8 + (17 + 11*s)*t2 + 
                  (-17 + 5*s)*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-40 - 10*(12 + s)*t2 + 
                  (-41 + 39*s + 10*Power(s,2))*Power(t2,2) + 
                  (-53 + 30*s)*Power(t2,3) + 20*Power(t2,4))) + 
            Power(t1,2)*t2*(-7 - (85 + 3*s)*t2 + 
               (-350 - 142*s + 45*Power(s,2) + 2*Power(s,3))*
                Power(t2,2) + 
               (-194 + 143*s + 86*Power(s,2) + 5*Power(s,3))*
                Power(t2,3) + 
               (-219 + 262*s + 36*Power(s,2))*Power(t2,4) + 
               (123 + 38*s)*Power(t2,5) + 8*Power(t2,6) + 
               Power(s2,3)*(-10 - 13*t2 + 3*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*(25 + (113 + 19*s)*t2 + 
                  (46 + 34*s)*Power(t2,2) - (-47 + s)*Power(t2,3) + 
                  4*Power(t2,4)) - 
               s2*(17 - 8*(-34 + s)*t2 + 
                  (-150 - 156*s + 19*Power(s,2))*Power(t2,2) + 
                  (50 - 200*s + 4*Power(s,2))*Power(t2,3) + 
                  4*(-54 + 7*s)*Power(t2,4) + 20*Power(t2,5)))) - 
         Power(s1,2)*(Power(t1,10)*t2 + 
            Power(t1,9)*(2 + (69 + 13*s - 10*s2)*t2 + 3*Power(t2,2)) + 
            Power(t1,7)*(-18 + 
               (-304 + Power(s,3) + 255*s2 + 149*Power(s2,2) - 
                  2*Power(s2,3) + Power(s,2)*(142 + 11*s2) + 
                  s*(-29 - 50*s2 + 2*Power(s2,2)))*t2 + 
               (-362 + 76*Power(s,2) + 394*s2 + 25*Power(s2,2) + 
                  s*(155 + 8*s2))*Power(t2,2) + 
               2*(72 + 67*s + 3*s2)*Power(t2,3) + 45*Power(t2,4)) + 
            Power(t1,6)*(10 + 
               (251 + 28*Power(s,3) + 238*s2 - 315*Power(s2,2) - 
                  29*Power(s2,3) - Power(s,2)*(49 + 101*s2) + 
                  s*(46 + 173*s2 + 18*Power(s2,2)))*t2 - 
               (-805 + 14*Power(s,3) - 250*s2 + 312*Power(s2,2) + 
                  7*Power(s2,3) + Power(s,2)*(219 + 5*s2) + 
                  s*(-579 - 61*s2 + 10*Power(s2,2)))*Power(t2,2) - 
               (-787 + 103*Power(s,2) + s*(59 - 9*s2) + 250*s2 + 
                  10*Power(s2,2))*Power(t2,3) + 
               (10 - 134*s + 32*s2)*Power(t2,4) - 32*Power(t2,5)) + 
            Power(t1,5)*(10 - 
               (-92 + 3*Power(s,3) + Power(s,2)*(53 - 78*s2) + 
                  355*s2 - 111*Power(s2,2) - 84*Power(s2,3) + 
                  s*(7 + 172*s2 + 38*Power(s2,2)))*t2 - 
               (-301 + 75*Power(s,3) + Power(s,2)*(142 - 216*s2) + 
                  1219*s2 - 165*Power(s2,2) - 70*Power(s2,3) + 
                  s*(290 + 456*s2 + 70*Power(s2,2)))*Power(t2,2) + 
               (-582 + 26*Power(s,3) - 595*s2 - 19*Power(s2,2) - 
                  15*Power(s2,3) - 26*Power(s,2)*(7 + s2) + 
                  s*(-1741 + 174*s2 + 21*Power(s2,2)))*Power(t2,3) + 
               2*(-513 + 35*Power(s,2) + 22*s2 - 10*Power(s2,2) - 
                  3*s*(67 + 5*s2))*Power(t2,4) + 
               (-199 + 65*s - 43*s2)*Power(t2,5) + 8*Power(t2,6)) + 
            Power(t1,4)*(-10 + 
               (-143 - 5*Power(s,2) - 2*Power(s,3) + 25*s2 + 
                  92*Power(s2,2) - 73*Power(s2,3) + 
                  2*s*(9 + 32*s2 + 9*Power(s2,2)))*t2 - 
               (769 + Power(s,3) + 198*s2 - 481*Power(s2,2) + 
                  77*Power(s2,3) + Power(s,2)*(-143 + 61*s2) + 
                  s*(187 - 255*s2 - 127*Power(s2,2)))*Power(t2,2) + 
               (-1219 + 73*Power(s,3) + 934*s2 + 300*Power(s2,2) + 
                  43*Power(s2,3) - 8*Power(s,2)*(-54 + 17*s2) + 
                  s*(97 + 699*s2 + 61*Power(s2,2)))*Power(t2,3) + 
               (-124 - 14*Power(s,3) + 537*s2 + 186*Power(s2,2) + 
                  7*Power(s2,3) + Power(s,2)*(467 + 23*s2) + 
                  s*(1779 - 351*s2 - 16*Power(s2,2)))*Power(t2,4) + 
               (774 - 20*Power(s,2) - 122*s2 + 4*Power(s2,2) + 
                  s*(557 + 16*s2))*Power(t2,5) - 
               4*(-44 + 3*s - 3*s2)*Power(t2,6)) - 
            Power(t1,3)*(-2 + 
               (-35 - 52*s2 + 44*Power(s2,2) - 20*Power(s2,3) + 
                  3*s*(1 + 4*s2))*t2 + 
               (-96 - 6*Power(s,3) - 1039*s2 + 291*Power(s2,2) + 
                  38*Power(s2,3) + Power(s,2)*(-46 + 30*s2) + 
                  s*(96 - 48*s2 + 44*Power(s2,2)))*Power(t2,2) + 
               (-877 - 15*Power(s,3) - 685*s2 + 516*Power(s2,2) + 
                  50*Power(s2,3) + 2*Power(s,2)*(47 + 57*s2) + 
                  s*(-653 + 116*s2 + 111*Power(s2,2)))*Power(t2,3) + 
               (-1208 + 33*Power(s,3) + 143*s2 + 139*Power(s2,2) + 
                  Power(s2,3) - 4*Power(s,2)*(-58 + 7*s2) + 
                  s*(-588 + 652*s2 + 8*Power(s2,2)))*Power(t2,4) + 
               (-778 - Power(s,3) + 355*s2 + 36*Power(s2,2) + 
                  Power(s2,3) + 3*Power(s,2)*(74 + s2) + 
                  s*(501 - 182*s2 - 3*Power(s2,2)))*Power(t2,5) + 
               (147 + 219*s - 109*s2)*Power(t2,6) + 48*Power(t2,7)) - 
            Power(t1,8)*(-4 + 23*Power(s,2)*t2 - 7*Power(s2,2)*t2 + 
               s2*(199 - 3*t2)*t2 + 152*Power(t2,2) + 25*Power(t2,3) + 
               s*t2*(38 + 3*s2 + 66*t2)) - 
            Power(t2,3)*(1 + s2 + t2)*
             (-1 + (-1 + s)*t2 - 7*Power(t2,2) - (-1 + s)*Power(t2,3) + 
               Power(s2,2)*(2 - t2 + Power(t2,2)) + 
               s2*(1 + t2)*(-1 - (4 + s)*t2 + Power(t2,2))) + 
            t1*Power(t2,2)*(-2 - (59 + 9*s)*t2 + 
               (-125 - 18*s + 36*Power(s,2) + 2*Power(s,3))*
                Power(t2,2) + 
               (200 + 200*s + 33*Power(s,2) + 4*Power(s,3))*
                Power(t2,3) + 
               (173 + 118*s + 10*Power(s,2))*Power(t2,4) + 
               (101 + 5*s)*Power(t2,5) + 
               Power(s2,3)*(-10 - 5*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(31 + (85 + 2*s)*t2 + 
                  (5 - 2*s)*Power(t2,2) + (46 - 3*s)*Power(t2,3)) + 
               s2*(-8 - 74*t2 + 
                  (229 + 76*s - 30*Power(s,2))*Power(t2,2) + 
                  (63 + 34*s - 2*Power(s,2))*Power(t2,3) - 
                  5*(-23 + 2*s)*Power(t2,4) - 5*Power(t2,5))) - 
            Power(t1,2)*t2*(1 - (80 + 13*s)*t2 + 
               (-148 - 97*s + 77*Power(s,2) + 6*Power(s,3))*Power(t2,2) + 
               (518 + 660*s + 29*Power(s,2) + 15*Power(s,3))*
                Power(t2,3) + 
               (677 + 560*s + 19*Power(s,2) - 7*Power(s,3))*Power(t2,4) + 
               (624 + 92*s - 14*Power(s,2))*Power(t2,5) + 
               (40 - 6*s)*Power(t2,6) - 
               Power(s2,3)*t2*
                (62 + 29*t2 - 18*Power(t2,2) + Power(t2,3)) + 
               s2*(6 - 3*(-87 + 4*s)*t2 + 
                  (780 + 190*s - 60*Power(s,2))*Power(t2,2) + 
                  (178 + 3*s - 99*Power(s,2))*Power(t2,3) + 
                  (410 - 246*s + 7*Power(s,2))*Power(t2,4) + 
                  (-171 + 16*s)*Power(t2,5) + 6*Power(t2,6)) + 
               Power(s2,2)*t2*
                (99 - 161*t2 + 36*Power(t2,2) + 17*Power(t2,3) - 
                  2*Power(t2,4) + 
                  s*(3 - 27*t2 - 25*Power(t2,2) + Power(t2,3))))) + 
         t2*(Power(t1,9)*(-1 - 3*Power(s,2) + Power(s,3) - s*(-3 + t2) + 
               15*t2) + Power(t1,3)*
             (4 + (24 - 12*Power(s,2) + 88*s2 - 19*Power(s2,2) + 
                  10*Power(s2,3) - 4*s*(5 + 6*s2))*t2 + 
               (91 + 24*Power(s,3) + 670*s2 + 3*Power(s2,2) - 
                  117*Power(s2,3) - 6*Power(s,2)*(-29 + 20*s2) + 
                  s*(-249 + 20*s2 + 6*Power(s2,2)))*Power(t2,2) + 
               (513 + 84*Power(s,3) + 741*s2 - 533*Power(s2,2) - 
                  89*Power(s2,3) - 6*Power(s,2)*(-9 + 32*s2) - 
                  2*s*(7 - 56*s2 + 15*Power(s2,2)))*Power(t2,3) + 
               (488 - 102*Power(s,3) - 399*s2 - 218*Power(s2,2) + 
                  Power(s2,3) + Power(s,2)*(-305 + 116*s2) + 
                  s*(306 - 118*s2 - 23*Power(s2,2)))*Power(t2,4) - 
               (-49 + 166*Power(s,2) + s*(418 - 170*s2) + 93*s2 + 
                  20*Power(s2,2))*Power(t2,5) + 
               (-93 - 69*s + 61*s2)*Power(t2,6)) - 
            4*Power(t2,3)*(1 + t2)*(1 + s2 + t2)*
             (-1 - s*(-1 + t2)*t2 + Power(t2,2) + Power(s2,2)*(2 + t2) + 
               s2*(-1 - s*t2 + Power(t2,2))) - 
            Power(t1,8)*(-7 + 2*(-6 + 29*s2)*t2 + 
               (30 + s2)*Power(t2,2) + Power(s,3)*(2 + 4*t2) - 
               Power(s,2)*(9 + (5 + 3*s2)*t2) + 
               s*(12 + (-12 + s2)*t2 + 4*Power(t2,2))) + 
            Power(t1,7)*(-12 + (-90 + 59*s2 + 58*Power(s2,2))*t2 + 
               (-101 + 119*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (16 - 5*s2)*Power(t2,3) + 
               Power(s,3)*(-1 + 7*t2 + 6*Power(t2,2)) + 
               Power(s,2)*(-3 - 4*(-8 + s2)*t2 + 
                  (1 - 9*s2)*Power(t2,2)) + 
               s*(12 - (7 + 18*s2 + 2*Power(s2,2))*t2 + 
                  (11 + 12*s2 + 3*Power(s2,2))*Power(t2,2) + 
                  20*Power(t2,3))) - 
            4*t1*Power(t2,2)*(3 + (15 + 8*s + 3*Power(s,2))*t2 + 
               (7 + 15*s - 44*Power(s,2) - 3*Power(s,3))*Power(t2,2) - 
               (20 + 63*s - 4*Power(s,2) + 3*Power(s,3))*Power(t2,3) + 
               (-8 + 5*s - 10*Power(s,2))*Power(t2,4) - 
               5*(1 + s)*Power(t2,5) + 
               Power(s2,3)*(10 + 11*t2 - 10*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,2)*(-31 - 2*(18 + s)*t2 + 
                  (29 + 5*s)*Power(t2,2) + (-17 + 6*s)*Power(t2,3)) + 
               s2*(8 - (31 + 6*s)*t2 + 
                  (-76 - 12*s + 33*Power(s,2))*Power(t2,2) + 
                  (22 + 32*s - Power(s,2))*Power(t2,3) + 
                  2*(-8 + 5*s)*Power(t2,4) + 5*Power(t2,5))) + 
            Power(t1,5)*(17 + 
               (95 - 79*s2 + 37*Power(s2,2) + 38*Power(s2,3))*t2 + 
               (286 - 379*s2 - 57*Power(s2,2) + 27*Power(s2,3))*
                Power(t2,2) - 
               (17 + 462*s2 + 17*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,3) - 
               (137 + 42*s2 + 20*Power(s2,2) + Power(s2,3))*
                Power(t2,4) + (9 - 19*s2)*Power(t2,5) + 
               Power(s,3)*t2*
                (-8 - 73*t2 + 13*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(6 + 16*(1 + 3*s2)*t2 + 
                  (-70 + 94*s2)*Power(t2,2) + 
                  (14 - 30*s2)*Power(t2,3) + (14 - 3*s2)*Power(t2,4)) + 
               s*(-15 - 20*(4 + 5*s2 + Power(s2,2))*t2 - 
                  (74 + 28*s2 + 5*Power(s2,2))*Power(t2,2) + 
                  (-29 + 108*s2 + 32*Power(s2,2))*Power(t2,3) + 
                  (107 + 10*s2 + 3*Power(s2,2))*Power(t2,4) + 
                  21*Power(t2,5))) + 
            Power(t1,4)*(-15 - 
               (102 + 73*s2 - 39*Power(s2,2) + 34*Power(s2,3))*t2 + 
               (-416 - 349*s2 + 329*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,2) + 
               (-662 + 502*s2 + 334*Power(s2,2) + 32*Power(s2,3))*
                Power(t2,3) + 
               (-73 + 391*s2 + 77*Power(s2,2) + 9*Power(s2,3))*
                Power(t2,4) + (104 - 9*s2 + 6*Power(s2,2))*Power(t2,5) + 
               (-4 + 6*s2)*Power(t2,6) - 
               Power(s,3)*t2*
                (6 + 28*t2 - 117*Power(t2,2) + 5*Power(t2,3)) + 
               s*(6 + (77 + 76*s2 + 9*Power(s2,2))*t2 + 
                  (269 + 2*s2 + 21*Power(s2,2))*Power(t2,2) - 
                  (120 + 47*s2 + 5*Power(s2,2))*Power(t2,3) + 
                  (322 - 190*s2 - 17*Power(s2,2))*Power(t2,4) + 
                  24*Power(t2,5) - 6*Power(t2,6)) + 
               Power(s,2)*t2*(-8 - 41*t2 + 351*Power(t2,2) + 
                  155*Power(t2,3) - 6*Power(t2,4) + 
                  s2*(-6 + 26*t2 - 117*Power(t2,2) + 13*Power(t2,3)))) - 
            Power(t1,6)*(Power(s,3)*
                (-2 - 23*t2 + 13*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s,2)*(9 + (33 + 41*s2)*t2 + 
                  (100 - 21*s2)*Power(t2,2) + (11 - 9*s2)*Power(t2,3)) + 
               s*(-6 - (19 + 67*s2 + 13*Power(s2,2))*t2 + 
                  (3 + 6*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  (112 + 21*s2 + 6*Power(s2,2))*Power(t2,3) + 
                  30*Power(t2,4)) - 
               t2*(42 + 139*t2 + 184*Power(t2,2) - 6*Power(t2,3) + 
                  Power(s2,3)*(-14 - 2*t2 + Power(t2,2)) + 
                  Power(s2,2)*(-115 - 94*t2 + 12*Power(t2,2)) + 
                  s2*(87 + 136*t2 - 47*Power(t2,2) + 19*Power(t2,3)))) + 
            Power(t1,2)*t2*(4 + (43 + 50*s + 24*Power(s,2))*t2 + 
               (22 + 251*s - 348*Power(s,2) - 30*Power(s,3))*
                Power(t2,2) - 
               (264 + 437*s + 4*Power(s,2) + 62*Power(s,3))*Power(t2,3) + 
               (-170 - 127*s + 20*Power(s,2) + 36*Power(s,3))*
                Power(t2,4) + (-35 + 127*s + 56*Power(s,2))*Power(t2,5) + 
               8*(4 + 3*s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (130 + 121*t2 - 37*Power(t2,2) - 4*Power(t2,3)) - 
               s2*(24 + 164*t2 + 
                  (861 + 152*s - 258*Power(s,2))*Power(t2,2) + 
                  (273 - 106*s - 114*Power(s,2))*Power(t2,3) + 
                  (-141 - 166*s + 52*Power(s,2))*Power(t2,4) + 
                  (11 + 64*s)*Power(t2,5) + 24*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-307 + 64*t2 + 293*Power(t2,2) - 34*Power(t2,3) + 
                  8*Power(t2,4) + 
                  s*(-12 + t2 + 53*Power(t2,2) + 20*Power(t2,3))))) + 
         s1*((-15 + s)*Power(t1,10)*t2 + 
            Power(t1,9)*(-2 + 
               (-11 + 4*Power(s,2) + 61*s2 - s*(18 + 5*s2))*t2 + 
               (31 + 4*s + s2)*Power(t2,2)) + 
            Power(t1,8)*(4 + (87 + Power(s,3) - 74*s2 - 
                  58*Power(s2,2) - Power(s,2)*(59 + 5*s2) + 
                  s*(31 + 42*s2 + 2*Power(s2,2)))*t2 + 
               (170 + 5*s - 10*Power(s,2) - 132*s2 - 5*Power(s2,2))*
                Power(t2,2) + (-13 - 20*s + 5*s2)*Power(t2,3)) + 
            Power(t1,7)*(2 - (32 + 17*Power(s,3) + 63*s2 - 
                  115*Power(s2,2) - 14*Power(s2,3) - 
                  4*Power(s,2)*(9 + 11*s2) + 
                  s*(37 + 91*s2 + 13*Power(s2,2)))*t2 + 
               (-139 + Power(s,3) - 320*s2 + 116*Power(s2,2) + 
                  2*Power(s2,3) + 2*Power(s,2)*(52 + 3*s2) + 
                  s*(-47 - 45*s2 + Power(s2,2)))*Power(t2,2) + 
               (-344 + 14*Power(s,2) + 50*s2 - 9*Power(s2,2) + 
                  s*(46 + 15*s2))*Power(t2,3) + 
               (30*s - 19*(1 + s2))*Power(t2,4)) + 
            Power(t1,6)*(-10 + 
               (-109 + Power(s,2)*(17 - 39*s2) + 73*s2 - 
                  37*Power(s2,2) - 38*Power(s2,3) + 
                  4*s*(14 + 22*s2 + 5*Power(s2,2)))*t2 + 
               (-579 + 59*Power(s,3) + Power(s,2)*(224 - 86*s2) + 
                  616*s2 + 182*Power(s2,2) - 29*Power(s2,3) + 
                  s*(42 + 14*s2 + 19*Power(s2,2)))*Power(t2,2) + 
               (-156 - 5*Power(s,3) + 832*s2 + 27*Power(s2,2) + 
                  8*Power(s2,3) + Power(s,2)*(53 + 3*s2) + 
                  s*(253 - 76*s2 - 8*Power(s2,2)))*Power(t2,3) + 
               (331 - 14*Power(s,2) + s*(27 - 10*s2) + 48*s2 + 
                  20*Power(s2,2))*Power(t2,4) + 
               (36 - 21*s + 19*s2)*Power(t2,5)) + 
            Power(t1,5)*(8 + (101 - 4*Power(s,2) + 4*Power(s,3) + 
                  52*s2 - 39*Power(s2,2) + 34*Power(s2,3) - 
                  s*(47 + 46*s2 + 9*Power(s2,2)))*t2 + 
               (654 + 68*Power(s,3) + 575*s2 - 638*Power(s2,2) - 
                  31*Power(s2,3) - 8*Power(s,2)*(7 + 20*s2) + 
                  s*(-187 + 219*s2 + 3*Power(s2,2)))*Power(t2,2) - 
               (-1309 + 111*Power(s,3) + Power(s,2)*(636 - 100*s2) + 
                  520*s2 + 612*Power(s2,2) + 31*Power(s2,3) + 
                  s*(-472 - 83*s2 + 23*Power(s2,2)))*Power(t2,3) + 
               (433 + 3*Power(s,3) - 634*s2 - 87*Power(s2,2) - 
                  4*Power(s2,3) - 2*Power(s,2)*(129 + 2*s2) + 
                  s*(-624 + 199*s2 + 5*Power(s2,2)))*Power(t2,4) + 
               (-235 - 158*s + 6*Power(s,2) + 41*s2 - 6*Power(s2,2))*
                Power(t2,5) + (-28 + 6*s - 6*s2)*Power(t2,6)) + 
            Power(t1,4)*(-2 + 
               (-11 + 6*Power(s,2) - 67*s2 + 19*Power(s2,2) - 
                  10*Power(s2,3) + 4*s*(5 + 3*s2))*t2 - 
               (17 + 18*Power(s,3) + Power(s,2)*(230 - 210*s2) + 
                  992*s2 - 129*Power(s2,2) - 201*Power(s2,3) + 
                  s*(-227 + 252*s2 + 59*Power(s2,2)))*Power(t2,2) - 
               (530 + 166*Power(s,3) + Power(s,2)*(33 - 463*s2) + 
                  1560*s2 - 836*Power(s2,2) - 153*Power(s2,3) + 
                  s*(252 + 568*s2 + 54*Power(s2,2)))*Power(t2,3) + 
               (-1114 + 113*Power(s,3) + Power(s,2)*(276 - 118*s2) + 
                  198*s2 + 255*Power(s2,2) - 22*Power(s2,3) + 
                  s*(-1407 + 292*s2 + 41*Power(s2,2)))*Power(t2,4) + 
               (-621 + 236*Power(s,2) + 154*s2 - 40*s*(-9 + 5*s2))*
                Power(t2,5) + (61 + 134*s - 104*s2)*Power(t2,6) + 
               8*Power(t2,7)) + 
            4*Power(t2,4)*(1 + s2 + t2)*
             (-1 + (2 + s)*t2 - (-3 + s)*Power(t2,2) + 
               Power(s2,2)*(3 + 2*t2) + s2*t2*(2 - s + 2*t2)) + 
            2*t1*Power(t2,3)*(15 - 22*t2 + Power(s,3)*(-1 + t2)*t2 - 
               98*Power(t2,2) + 14*Power(t2,3) + 11*Power(t2,4) + 
               16*Power(t2,5) - 
               2*Power(s2,3)*(-6 + 3*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-15 + 64*t2 - 11*Power(t2,2) + 
                  10*Power(t2,3)) + 
               s*(-3 + 2*(10 - 12*s2 + 9*Power(s2,2))*t2 + 
                  2*(-51 + 44*s2 + 6*Power(s2,2))*Power(t2,2) + 
                  (52 + 50*s2)*Power(t2,3) + 17*Power(t2,4)) + 
               s2*(-79 - 196*t2 + 72*Power(t2,2) - 26*Power(t2,3) + 
                  41*Power(t2,4)) + 
               Power(s,2)*t2*(2 + 6*t2 + 8*Power(t2,2) + s2*(3 + 17*t2))) \
- Power(t1,2)*Power(t2,2)*(-34 - 137*t2 - 499*Power(t2,2) - 
               37*Power(t2,3) + 285*Power(t2,4) + 318*Power(t2,5) + 
               32*Power(t2,6) + 
               2*Power(s,3)*t2*(-2 + 7*t2 + 15*Power(t2,2)) + 
               Power(s2,3)*(-60 - 67*t2 + 42*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*(162 + 295*t2 + 71*Power(t2,2) + 
                  176*Power(t2,3) + 32*Power(t2,4)) + 
               s2*(-72 - 455*t2 + 200*Power(t2,2) + 167*Power(t2,3) + 
                  274*Power(t2,4) - 90*Power(t2,5)) + 
               s*(-12 + 4*(2 + 7*s2 + 17*Power(s2,2))*t2 + 
                  (-401 + 372*s2 + 109*Power(s2,2))*Power(t2,2) + 
                  (566 + 258*s2 - 4*Power(s2,2))*Power(t2,3) + 
                  (565 - 164*s2)*Power(t2,4) + 162*Power(t2,5)) + 
               2*Power(s,2)*t2*
                (1 + 131*t2 + 63*Power(t2,2) + 90*Power(t2,3) + 
                  s2*(6 - 71*t2 - 13*Power(t2,2)))) + 
            Power(t1,3)*t2*(-10 - 154*t2 - 433*Power(t2,2) - 
               34*Power(t2,3) + 663*Power(t2,4) + 684*Power(t2,5) + 
               60*Power(t2,6) + 
               Power(s2,3)*t2*
                (-203 - 221*t2 + 68*Power(t2,2) + 8*Power(t2,3)) - 
               2*Power(s,3)*t2*
                (1 - 13*t2 - 64*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s2,2)*t2*
                (378 + 83*t2 - 257*Power(t2,2) + 176*Power(t2,3) - 
                  4*Power(t2,4)) + 
               s*(-6 + 4*(-14 + 16*s2 + 9*Power(s2,2))*t2 + 
                  (-377 + 502*s2 + 153*Power(s2,2))*Power(t2,2) + 
                  (849 + 419*s2 + 27*Power(s2,2))*Power(t2,3) - 
                  3*(-487 + 154*s2 + 8*Power(s2,2))*Power(t2,4) + 
                  (189 + 80*s2)*Power(t2,5) - 36*Power(t2,6)) + 
               s2*(18 + 180*t2 + 896*Power(t2,2) + 955*Power(t2,3) + 
                  105*Power(t2,4) - 72*Power(t2,5) + 36*Power(t2,6)) + 
               2*Power(s,2)*t2*
                (-4 + 242*t2 + 91*Power(t2,2) + 140*Power(t2,3) - 
                  38*Power(t2,4) + 
                  s2*(3 - 193*t2 - 145*Power(t2,2) + 30*Power(t2,3)))))))/
     (Power(-1 + t1,2)*t1*(s - s2 + t1)*
       (Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2)*Power(t1 - t2,2)*
       (-1 + s1 + t1 - t2)*(s1*t1 - t2)*(-1 + t2)*t2*(s - s1 + t2)) + 
    (8*(2 + Power(-1 + s,3)*Power(t1,8) - 
         2*Power(s1,7)*Power(t1,2)*((-5 + t1)*t1 + s2*(1 + t1)) + 2*t2 + 
         s*t2 + s2*t2 - 6*s*s2*t2 - 7*Power(t2,2) - 11*s*Power(t2,2) - 
         Power(s,2)*Power(t2,2) + 4*s2*Power(t2,2) - 3*s*s2*Power(t2,2) + 
         4*Power(s,2)*s2*Power(t2,2) - 2*Power(s2,2)*Power(t2,2) + 
         4*s*Power(s2,2)*Power(t2,2) + 9*Power(t2,3) - 35*s*Power(t2,3) + 
         20*Power(s,2)*Power(t2,3) - 38*s2*Power(t2,3) + 
         66*s*s2*Power(t2,3) - 13*Power(s,2)*s2*Power(t2,3) + 
         8*Power(s2,2)*Power(t2,3) - 10*s*Power(s2,2)*Power(t2,3) - 
         Power(s2,3)*Power(t2,3) - 37*Power(t2,4) + 62*s*Power(t2,4) - 
         10*Power(s,2)*Power(t2,4) + 2*Power(s,3)*Power(t2,4) + 
         25*s2*Power(t2,4) + 3*s*s2*Power(t2,4) - 
         8*Power(s,2)*s2*Power(t2,4) - 27*Power(s2,2)*Power(t2,4) + 
         2*s*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) + 
         21*Power(t2,5) + 10*s*Power(t2,5) - 8*Power(s,2)*Power(t2,5) - 
         19*s2*Power(t2,5) + 6*s*s2*Power(t2,5) - 
         Power(s,2)*s2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) + 
         2*s*Power(s2,2)*Power(t2,5) - Power(s2,3)*Power(t2,5) + 
         10*Power(t2,6) - 3*s*Power(t2,6) - Power(s,2)*Power(t2,6) + 
         3*s2*Power(t2,6) + 2*s*s2*Power(t2,6) - 
         Power(s2,2)*Power(t2,6) - 
         (-1 + s)*Power(t1,7)*
          (3 + 4*Power(s,2)*t2 + (2 + 3*s2)*t2 - 3*s*(1 + (3 + s2)*t2)) + 
         Power(t1,6)*(1 - 7*(1 + s2)*t2 - 
            (3 + 5*s2 + 3*Power(s2,2))*Power(t2,2) + 
            Power(s,3)*(-4 - 2*t2 + 6*Power(t2,2)) - 
            Power(s,2)*(-12 + 4*(1 + s2)*t2 + (23 + 9*s2)*Power(t2,2)) + 
            s*(-9 + (16 + 11*s2)*t2 + 
               (22 + 17*s2 + 3*Power(s2,2))*Power(t2,2))) + 
         Power(t1,5)*(-9 - (10 + 3*s2)*t2 + 
            5*(2 + 2*s2 + Power(s2,2))*Power(t2,2) + 
            (5 + 6*s2 + 4*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            Power(s,3)*(-2 + 16*t2 + 7*Power(t2,2) - 4*Power(t2,3)) + 
            Power(s,2)*(-6 - 2*(33 + 4*s2)*t2 + 
               (-8 + 3*s2)*Power(t2,2) + 3*(7 + 3*s2)*Power(t2,3)) - 
            s*(-18 - (61 + 11*s2)*t2 + 
               (20 + 18*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (28 + 20*s2 + 6*Power(s2,2))*Power(t2,3))) + 
         Power(t1,4)*(3 + (34 + 15*s2)*t2 + 
            (10 + 35*s2 + 2*Power(s2,2))*Power(t2,2) - 
            (23 + 10*s2 + 2*Power(s2,2) + Power(s2,3))*Power(t2,3) - 
            (7 + 5*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            Power(s,3)*(3 + 8*t2 - 22*Power(t2,2) - 9*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*(-15 + (-1 + 12*s2)*t2 + 
               (127 + 32*s2)*Power(t2,2) + (22 + 9*s2)*Power(t2,3) - 
               (10 + 3*s2)*Power(t2,4)) + 
            s*(9 - 2*(29 + 14*s2)*t2 - 
               (151 + 70*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (9 + 5*s2 + Power(s2,2))*Power(t2,3) + 
               (20 + 13*s2 + 3*Power(s2,2))*Power(t2,4))) + 
         Power(t1,3)*(9 - (4 + 3*s2)*t2 - 
            (29 + 53*s2 + 8*Power(s2,2))*Power(t2,2) - 
            (27 + 28*s2 + 25*Power(s2,2))*Power(t2,3) - 
            (-34 - 5*s2 + Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            (5 + 2*s2 + 2*Power(s2,2))*Power(t2,5) + 
            Power(s,3)*(2 - 11*t2 - 11*Power(t2,2) + 9*Power(t2,3) + 
               5*Power(t2,4)) + 
            Power(s,2)*(3 + (57 + 11*s2)*t2 + 
               (47 - 34*s2)*Power(t2,2) - 2*(53 + 15*s2)*Power(t2,3) - 
               (20 + 11*s2)*Power(t2,4) + 2*Power(t2,5)) + 
            s*(-18 - 2*(31 + 2*s2)*t2 + 
               (14 + 105*s2 + 8*Power(s2,2))*Power(t2,2) + 
               (185 + 101*s2 + 16*Power(s2,2))*Power(t2,3) + 
               (5 + 11*s2 + 7*Power(s2,2))*Power(t2,4) - 
               (7 + 4*s2)*Power(t2,5))) - 
         Power(t1,2)*(5 + (29 + 9*s2)*t2 + 
            (4 + 10*s2 - 3*Power(s2,2))*Power(t2,2) - 
            2*(13 + 8*s2 + 9*Power(s2,2) + Power(s2,3))*Power(t2,3) - 
            (57 + 19*s2 + 13*Power(s2,2) + 2*Power(s2,3))*Power(t2,4) + 
            (24 + s2 - 3*Power(s2,2) - Power(s2,3))*Power(t2,5) + 
            (1 + s2)*Power(t2,6) + 
            Power(s,3)*t2*(5 - 15*t2 - 7*Power(t2,2) - 4*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*(-6 + (-5 + 8*s2)*t2 + 
               (92 + 22*s2)*Power(t2,2) + (74 - 22*s2)*Power(t2,3) + 
               2*(-16 + s2)*Power(t2,4) - (8 + 3*s2)*Power(t2,5)) + 
            s*(3 - (41 + 23*s2)*t2 + 
               (-144 - 32*s2 + 5*Power(s2,2))*Power(t2,2) + 
               (-49 + 59*s2 + 30*Power(s2,2))*Power(t2,3) + 
               (118 + 51*s2 + 4*Power(s2,2))*Power(t2,4) + 
               (6 + 11*s2 + 3*Power(s2,2))*Power(t2,5) - Power(t2,6))) - 
         t1*(3 - 3*(4 + s2)*t2 - 
            (23 + 19*s2 + 3*Power(s2,2))*Power(t2,2) + 
            (-10 - 54*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            (27 + 14*s2 - 26*Power(s2,2) + 6*Power(s2,3))*Power(t2,4) + 
            (40 + 8*s2 + 5*Power(s2,2) - 2*Power(s2,3))*Power(t2,5) + 
            (-9 + 2*s2 + Power(s2,2))*Power(t2,6) + 
            Power(s,3)*Power(t2,2)*
             (-3 + 9*t2 + 2*Power(t2,2) + 3*Power(t2,3)) + 
            s*(-6 + (-12 + s2)*t2 + 
               (-2 + 63*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (138 + 65*s2 - 23*Power(s2,2))*Power(t2,3) + 
               (25 + 14*s2 - 6*Power(s2,2))*Power(t2,4) + 
               (-37 - 3*s2 + 7*Power(s2,2))*Power(t2,5) - 
               2*(1 + s2)*Power(t2,6)) + 
            Power(s,2)*t2*(4 + 26*t2 - 65*Power(t2,2) - 42*Power(t2,3) - 
               2*Power(t2,4) + Power(t2,5) - 
               s2*(-6 + 24*t2 + 17*Power(t2,2) + 2*Power(t2,3) + 
                  8*Power(t2,4)))) - 
         2*Power(s1,6)*t1*(Power(s2,2)*(1 + Power(t1,2)) + 
            s2*(1 + t1)*(3*Power(t1,2) - 2*t2 - t1*(4 + s + 4*t2)) + 
            t1*(-1 + 4*Power(t1,3) + (15 + s)*t2 - 
               Power(t1,2)*(16 + 2*s + 3*t2) + 
               t1*(-1 + 12*t2 + s*(6 + t2)))) + 
         Power(s1,5)*(Power(s2,3)*(-1 + t1) + 
            Power(s2,2)*(-6*Power(t1,4) - Power(t1,2)*(6 + s - 7*t2) + 
               2*t2 + t1*(5 + s + 5*t2) + Power(t1,3)*(3 + 10*t2)) - 
            s2*(6*Power(t1,5) + 2*Power(t2,2) - 
               Power(t1,4)*(5 + 5*s + 19*t2) + 
               2*t1*(-2 + 4*(2 + s)*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-25 - 8*t2 + 12*Power(t2,2) + 
                  6*s*(-1 + 2*t2)) + 
               Power(t1,2)*(4 + 27*t2 + 28*Power(t2,2) + 3*s*(1 + 4*t2))\
) + t1*(-12*Power(t1,5) + Power(t1,4)*(46 + 17*s + 17*t2) + 
               2*t2*(-2 + (15 + 2*s)*t2) + 
               2*t1*(-4 + s - 6*t2 + 21*s*t2 + Power(s,2)*t2 + 
                  42*Power(t2,2) + 5*s*Power(t2,2)) - 
               Power(t1,3)*(-22 + 2*Power(s,2) + 39*t2 + 
                  6*Power(t2,2) + s*(25 + 22*t2)) + 
               2*Power(t1,2)*(-4 - 59*t2 + 6*Power(t2,2) + 
                  Power(s,2)*(3 + t2) + s*(1 + 10*t2 + 3*Power(t2,2))))) \
+ Power(s1,4)*(-8*Power(t1,7) + 
            Power(s2,3)*(3 - 5*t1 + 2*Power(t1,2) + 
               2*Power(t1,3)*(-1 + t2)) + 
            Power(t1,6)*(35 + 27*s + 16*t2) - 
            2*Power(t2,2)*(-1 + (5 + s)*t2) - 
            2*t1*(1 - 8*t2 + 3*(-3 + 8*s + Power(s,2))*Power(t2,2) + 
               (44 + 7*s)*Power(t2,3)) - 
            Power(t1,5)*(-34 + 10*Power(s,2) + 17*t2 + 10*Power(t2,2) + 
               s*(10 + 53*t2)) - 
            Power(s2,2)*(-3 + 6*Power(t1,5) + 5*t2 + 4*s*t2 + 
               5*Power(t2,2) - Power(t1,4)*(8 + 21*t2) + 
               t1*(2 + t2 - 2*s*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(7 - s - 14*t2 + 4*s*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(-12 - 5*s + 11*t2 + 28*Power(t2,2))) + 
            Power(t1,4)*(-10 - 220*t2 - 15*Power(t2,2) + 
               2*Power(t2,3) + Power(s,2)*(12 + 19*t2) + 
               s*(-31 - 6*t2 + 33*Power(t2,2))) + 
            Power(t1,3)*(-33 - 38*t2 + 201*Power(t2,2) + 
               8*Power(t2,3) - 2*Power(s,2)*t2*(1 + 4*t2) + 
               s*(10 + 106*t2 + 38*Power(t2,2) - 6*Power(t2,3))) - 
            Power(t1,2)*(-4 - 45*t2 - 166*Power(t2,2) + 
               72*Power(t2,3) + 
               Power(s,2)*(2 + 17*t2 + 10*Power(t2,2)) + 
               s*(8 + 3*t2 + 99*Power(t2,2) + 18*Power(t2,3))) + 
            s2*(-2*Power(t1,6) + Power(t1,5)*(-7 + 3*s + 15*t2) + 
               2*t2*(-2 + (4 + 3*s)*t2 + 4*Power(t2,2)) + 
               t1*(-10 + s - 2*t2 + 17*s*t2 + 2*Power(s,2)*t2 + 
                  48*Power(t2,2) + 24*s*Power(t2,2) + 32*Power(t2,3)) - 
               Power(t1,2)*(-9 + Power(s,2) + 59*t2 - 23*Power(t2,2) - 
                  32*Power(t2,3) + s*(4 + 16*t2 - 40*Power(t2,2))) + 
               Power(t1,4)*(Power(s,2) + s*(9 - 33*t2) + 
                  3*(12 + t2 - 7*Power(t2,2))) + 
               Power(t1,3)*(4 + 2*Power(s,2)*(-2 + t2) - 75*t2 - 
                  34*Power(t2,2) + 8*Power(t2,3) + 
                  s*(-11 - 18*t2 + 26*Power(t2,2))))) + 
         Power(s1,3)*(-2*Power(t1,8) + Power(t1,7)*(11 + 19*s + 5*t2) + 
            Power(t1,6)*(21 - 18*Power(s,2) + s*(11 - 48*t2) + 3*t2 - 
               4*Power(t2,2)) + 
            2*t2*(1 - (4 + s)*t2 + 
               (-4 + 9*s + 2*Power(s,2))*Power(t2,2) + 
               3*(5 + s)*Power(t2,3)) + 
            Power(s2,3)*(-2 + t1*(2 - 3*t2) + t2 + 3*Power(t2,2) + 
               Power(t1,4)*(-2 + 3*t2) + 
               Power(t1,3)*(-2 + 7*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(4 - 5*Power(t2,2))) + 
            Power(t1,4)*(-44 + Power(s,3)*(1 - 3*t2) - 87*t2 + 
               181*Power(t2,2) + 23*Power(t2,3) + 
               Power(s,2)*(11 + 22*t2 - 38*Power(t2,2)) - 
               2*s*t2*(-69 - 70*t2 + 8*Power(t2,2))) + 
            Power(t1,5)*(20 + Power(s,3) - 177*t2 - 31*Power(t2,2) + 
               Power(t2,3) + Power(s,2)*(1 + 46*t2) + 
               s*(-67 - 85*t2 + 43*Power(t2,2))) + 
            Power(t1,3)*(-8 + 95*t2 + 349*Power(t2,2) - 
               73*Power(t2,3) - 6*Power(t2,4) + 
               2*Power(s,3)*(1 - t2 + Power(t2,2)) + 
               Power(s,2)*(4 - 58*t2 - 49*Power(t2,2) + 
                  10*Power(t2,3)) + 
               s*(-1 + 92*t2 - 148*Power(t2,2) - 82*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(t1,2)*(-3 + 82*t2 + Power(s,3)*(-3 + t2)*t2 - 
               29*Power(t2,2) - 285*Power(t2,3) + 12*Power(t2,4) + 
               Power(s,2)*t2*(-11 + 27*t2 + 23*Power(t2,2)) + 
               s*(-2 - 10*t2 - 169*Power(t2,2) + 26*Power(t2,3) + 
                  14*Power(t2,4))) + 
            t1*(5 - 3*t2 - 66*Power(t2,2) + Power(s,3)*Power(t2,2) - 
               106*Power(t2,3) + 84*Power(t2,4) + 
               Power(s,2)*t2*(5 + 22*t2 + 15*Power(t2,2)) + 
               s*(-2 + 5*t2 - 6*Power(t2,2) + 122*Power(t2,3) + 
                  18*Power(t2,4))) + 
            Power(s2,2)*(-9 - 2*Power(t1,6) + (-1 + 12*s)*t2 - 
               (-7 + s)*Power(t2,2) + 2*Power(t2,3) + 
               2*Power(t1,5)*(2 + 7*t2) + 
               Power(t1,4)*(6 + (4 - 7*s)*t2 - 26*Power(t2,2)) + 
               t1*(12 - 18*t2 + 13*Power(t2,2) + 16*Power(t2,3) + 
                  s*(-1 - 22*t2 + Power(t2,2))) + 
               Power(t1,2)*(-19 + 21*t2 + 15*Power(t2,2) + 
                  40*Power(t2,3) + 3*s*(-3 + t2 + 4*Power(t2,2))) + 
               Power(t1,3)*(8 - 52*t2 - 41*Power(t2,2) + 
                  14*Power(t2,3) + 2*s*(8 - 9*t2 + 7*Power(t2,2)))) + 
            s2*(-3 - Power(t1,6)*(11 + s - 4*t2) + 5*(2 + s)*t2 + 
               (6 - 14*s - 5*Power(s,2))*Power(t2,2) - 
               (23 + 14*s)*Power(t2,3) - 12*Power(t2,4) + 
               Power(t1,5)*(8 + 3*Power(s,2) + s*(15 - 37*t2) + 24*t2 - 
                  11*Power(t2,2)) + 
               Power(t1,4)*(4 - 35*t2 - 35*Power(t2,2) + 
                  9*Power(t2,3) + Power(s,2)*(-8 + 6*t2) + 
                  s*(-13 + 60*Power(t2,2))) + 
               Power(t1,2)*(-14 - 11*t2 + 175*Power(t2,2) + 
                  2*Power(t2,3) - 18*Power(t2,4) + 
                  Power(s,2)*(1 + 18*t2 - 8*Power(t2,2)) + 
                  s*(18 + 39*t2 + 6*Power(t2,2) - 66*Power(t2,3))) + 
               Power(t1,3)*(15 - 120*t2 + 72*Power(t2,2) + 
                  42*Power(t2,3) - 2*Power(t2,4) + 
                  Power(s,2)*(-8 + 9*t2 - 10*Power(t2,2)) + 
                  s*(-35 + 22*t2 + 67*Power(t2,2) - 24*Power(t2,3))) - 
               t1*(-1 - 8*t2 + Power(s,2)*(-3 + t2)*t2 - 
                  41*Power(t2,2) + 46*Power(t2,3) + 28*Power(t2,4) + 
                  s*(12 - 15*t2 + 11*Power(t2,2) + 36*Power(t2,3))))) + 
         Power(s1,2)*(1 + 2*Power(t1,2) + 5*Power(t1,3) - 
            16*Power(t1,4) + Power(t1,5) + 9*Power(t1,6) - 
            2*Power(t1,7) - 5*t2 - 7*t1*t2 + 25*Power(t1,2)*t2 + 
            122*Power(t1,3)*t2 - 29*Power(t1,4)*t2 - 78*Power(t1,5)*t2 - 
            35*Power(t1,6)*t2 + 7*Power(t1,7)*t2 - Power(t2,2) - 
            64*t1*Power(t2,2) - 207*Power(t1,2)*Power(t2,2) + 
            49*Power(t1,3)*Power(t2,2) + 311*Power(t1,4)*Power(t2,2) + 
            49*Power(t1,5)*Power(t2,2) - 17*Power(t1,6)*Power(t2,2) + 
            29*Power(t2,3) + 84*t1*Power(t2,3) - 
            201*Power(t1,2)*Power(t2,3) - 303*Power(t1,3)*Power(t2,3) - 
            10*Power(t1,4)*Power(t2,3) + 13*Power(t1,5)*Power(t2,3) + 
            26*Power(t2,4) + 163*t1*Power(t2,4) + 
            133*Power(t1,2)*Power(t2,4) - 7*Power(t1,3)*Power(t2,4) - 
            3*Power(t1,4)*Power(t2,4) - 30*Power(t2,5) - 
            24*t1*Power(t2,5) + 6*Power(t1,2)*Power(t2,5) + 
            Power(s,3)*(3*Power(t1,6) + Power(t1,5)*(3 - 10*t2) + 
               t1*(7 - 2*t2)*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,4)*t2*(-9 + 11*t2) + 
               Power(t1,2)*t2*(-5 + 4*t2 - 6*Power(t2,2)) - 
               2*Power(t1,3)*t2*(1 - 6*t2 + 2*Power(t2,2))) + 
            Power(s2,3)*t2*(1 + Power(t1,5) + Power(t1,4)*(5 - 7*t2) - 
               7*t2 - 3*Power(t2,2) + 3*Power(t1,3)*t2*(-3 + 2*t2) + 
               t1*(-1 + 15*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-6 - 4*t2 + 11*Power(t2,2))) - 
            Power(s,2)*(14*Power(t1,7) + Power(t1,6)*(10 - 43*t2) + 
               Power(t2,3)*(11 + 7*t2) + 
               Power(t1,5)*(-17 - 65*t2 + 48*Power(t2,2)) + 
               Power(t1,4)*(-8 + 54*t2 + 124*Power(t2,2) - 
                  23*Power(t2,3)) + 
               t1*t2*(16 - 33*t2 + 51*Power(t2,2) + 17*Power(t2,3)) + 
               Power(t1,3)*(-10 + 55*t2 - 45*Power(t2,2) - 
                  91*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,2)*(-13 + 31*t2 - 120*Power(t2,2) - 
                  12*Power(t2,3) + 22*Power(t2,4))) + 
            s*(5*Power(t1,8) + Power(t1,7)*(13 - 15*t2) + 
               Power(t1,6)*(-58 - 71*t2 + 16*Power(t2,2)) + 
               Power(t1,5)*(-76 + 121*t2 + 126*Power(t2,2) - 
                  7*Power(t2,3)) + 
               t2*(-1 + 3*t2 + 7*Power(t2,2) - 47*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(t1,4)*(45 + 271*t2 - 32*Power(t2,2) - 
                  98*Power(t2,3) + Power(t2,4)) + 
               Power(t1,3)*(47 - 102*t2 - 321*Power(t2,2) - 
                  80*Power(t2,3) + 34*Power(t2,4)) + 
               t1*(12 - 3*t2 + 15*Power(t2,2) + 112*Power(t2,3) - 
                  78*Power(t2,4) - 10*Power(t2,5)) + 
               Power(t1,2)*(12 - 74*t2 - 11*Power(t2,2) + 
                  268*Power(t2,3) + 62*Power(t2,4) - 4*Power(t2,5))) + 
            Power(s2,2)*(6 - 3*Power(t2,2) - 5*Power(t2,3) + 
               2*Power(t2,4) + Power(t1,6)*(-1 + 3*t2) + 
               2*s*t2*(-7 + 7*t2 + 4*Power(t2,2)) + 
               Power(t1,4)*(6 - (30 + 17*s)*t2 + 
                  (-24 + 23*s)*Power(t2,2) + 13*Power(t2,3)) + 
               Power(t1,2)*(-11 + (3 - 38*s)*t2 + 
                  (53 + 29*s)*Power(t2,2) + (6 - 31*s)*Power(t2,3) - 
                  24*Power(t2,4)) - 
               t1*(7 - (40 + 37*s)*t2 + (33 + 28*s)*Power(t2,2) + 
                  3*(8 + s)*Power(t2,3) + 22*Power(t2,4)) - 
               Power(t1,5)*(5 - 7*t2 + 11*Power(t2,2) + s*(2 + 4*t2)) + 
               Power(t1,3)*(12 - 23*t2 + 66*Power(t2,2) + 
                  38*Power(t2,3) - 4*Power(t2,4) + 
                  s*(2 + 18*t2 + 22*Power(t2,2) - 16*Power(t2,3)))) + 
            s2*(9 - (3 + s)*Power(t1,7) + (2 - 12*s)*t2 + 
               (-17 - 11*s + 13*Power(s,2))*Power(t2,2) + 
               (-7 + 21*s - 2*Power(s,2))*Power(t2,3) + 
               (21 + 8*s)*Power(t2,4) + 8*Power(t2,5) + 
               Power(t1,6)*(6 + 3*Power(s,2) + s*(11 - 21*t2) + 13*t2 - 
                  Power(t2,2)) + 
               Power(t1,5)*(18 - 23*Power(t2,2) + 2*Power(t2,3) + 
                  2*Power(s,2)*(-4 + 5*t2) + 
                  s*(34 - 17*t2 + 50*Power(t2,2))) - 
               Power(t1,4)*(12 + 60*t2 + 5*Power(t2,2) - 
                  27*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*(-2 - 7*t2 + 27*Power(t2,2)) + 
                  s*(25 + t2 - 74*Power(t2,2) + 36*Power(t2,3))) + 
               Power(t1,3)*(-30 + 64*t2 + 107*Power(t2,2) + 
                  7*Power(t2,3) - 20*Power(t2,4) + 
                  Power(s,2)*
                   (1 + 16*t2 - 22*Power(t2,2) + 14*Power(t2,3)) + 
                  s*(-26 + 69*t2 - 106*Power(t2,2) - 114*Power(t2,3) + 
                     8*Power(t2,4))) + 
               t1*(-9 + 16*t2 + 10*Power(t2,2) - 125*Power(t2,3) + 
                  22*Power(t2,4) + 12*Power(t2,5) + 
                  Power(s,2)*t2*(-16 - 23*t2 + 8*Power(t2,2)) + 
                  s*(17 - 44*t2 - 14*Power(t2,2) + 4*Power(t2,3) + 
                     42*Power(t2,4))) + 
               Power(t1,2)*(21 - 35*t2 + 109*Power(t2,2) - 
                  156*Power(t2,3) - 18*Power(t2,4) + 4*Power(t2,5) + 
                  Power(s,2)*t2*(21 - 23*t2 + 26*Power(t2,2)) + 
                  s*(-10 + 110*t2 - 113*Power(t2,2) + 21*Power(t2,3) + 
                     46*Power(t2,4))))) - 
         s1*(3 + (1 - 5*s + 4*Power(s,2))*Power(t1,8) + t2 - 
            10*Power(t2,2) - 5*s*Power(t2,2) - Power(s,2)*Power(t2,2) - 
            15*Power(t2,3) + 15*s*Power(t2,3) + 
            19*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) + 
            39*Power(t2,4) + 24*s*Power(t2,4) - 
            28*Power(s,2)*Power(t2,4) + Power(s,3)*Power(t2,4) + 
            32*Power(t2,5) - 36*s*Power(t2,5) - 
            2*Power(s,2)*Power(t2,5) - 10*Power(t2,6) - 2*s*Power(t2,6) + 
            Power(t1,7)*(s - 3*Power(s,3) + Power(s,2)*(8 - 14*t2) + 
               24*s*t2 - 3*(1 + t2)) + 
            Power(s2,3)*Power(t2,2)*
             (-2 + 2*Power(t1,5) + Power(t1,4)*(2 - 5*t2) + t2 - 
               2*Power(t2,2) + 7*Power(t1,2)*Power(t2,2) + 
               t1*t2*(1 + t2) + Power(t1,3)*(-2 - 5*t2 + 2*Power(t2,2))) \
+ Power(t1,6)*(-10 + 7*t2 + 10*Power(t2,2) + Power(s,3)*(-2 + 11*t2) + 
               s*(43 + 5*t2 - 43*Power(t2,2)) + 
               Power(s,2)*(-27 - 40*t2 + 18*Power(t2,2))) - 
            Power(t1,5)*(-22 - 20*t2 + 47*Power(t2,2) + 19*Power(t2,3) + 
               Power(s,3)*t2*(-13 + 15*t2) + 
               s*(13 + 200*t2 + 46*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s,2)*(25 - 72*t2 - 77*Power(t2,2) + 10*Power(t2,3))) \
+ Power(t1,4)*(2 - 25*t2 - 84*Power(t2,2) + 78*Power(t2,3) + 
               14*Power(t2,4) + 
               Power(s,3)*(3 + 4*t2 - 29*Power(t2,2) + 9*Power(t2,3)) + 
               s*(-44 - 98*t2 + 317*Power(t2,2) + 83*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s,2)*(6 + 100*t2 - 38*Power(t2,2) - 
                  71*Power(t2,3) + 2*Power(t2,4))) - 
            Power(t1,3)*(17 + 23*t2 - 17*Power(t2,2) - 226*Power(t2,3) + 
               49*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,2)*(-17 + 21*t2 + 151*Power(t2,2) + 
                  49*Power(t2,3) - 31*Power(t2,4)) + 
               Power(s,3)*(-4 + 11*t2 + 9*Power(t2,2) - 
                  27*Power(t2,3) + 2*Power(t2,4)) + 
               s*(4 - 180*t2 - 262*Power(t2,2) + 208*Power(t2,3) + 
                  61*Power(t2,4) - 2*Power(t2,5))) + 
            Power(t1,2)*(4 + 25*t2 + 88*Power(t2,2) - 31*Power(t2,3) - 
               191*Power(t2,4) + 15*Power(t2,5) + 
               Power(s,3)*t2*
                (-13 + 14*t2 + 9*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s,2)*(17 - 34*t2 - 12*Power(t2,2) + 
                  105*Power(t2,3) + 55*Power(t2,4) - 5*Power(t2,5)) + 
               s*(6 + 85*t2 - 240*Power(t2,2) - 218*Power(t2,3) + 
                  44*Power(t2,4) + 20*Power(t2,5))) + 
            t1*(-2 - 2*t2 + 26*Power(t2,2) - 159*Power(t2,3) - 
               5*Power(t2,4) + 72*Power(t2,5) + 2*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(11 - 7*t2 - 4*Power(t2,2)) + 
               Power(s,2)*t2*
                (-15 + 5*t2 + 70*Power(t2,2) - 26*Power(t2,3) - 
                  13*Power(t2,4)) + 
               s*(16 + 4*t2 - 119*Power(t2,2) + 112*Power(t2,3) + 
                  141*Power(t2,4) + 10*Power(t2,5) - 2*Power(t2,6))) - 
            Power(s2,2)*t2*(-2 - Power(t1,6)*(-4 + t2) + 3*t2 + 
               28*Power(t2,2) + Power(t2,3) + 
               Power(t1,5)*(2 - 13*t2 + 3*Power(t2,2)) - 
               2*Power(t1,3)*t2*(-22 + 3*t2 + 8*Power(t2,2)) + 
               Power(t1,4)*(-14 + 20*t2 + 15*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(t1,2)*(12 + 2*t2 - 99*Power(t2,2) - 
                  7*Power(t2,3) + 5*Power(t2,4)) + 
               t1*(-2 - 55*t2 + 27*Power(t2,2) + 12*Power(t2,3) + 
                  11*Power(t2,4)) - 
               s*(-6 + Power(t1,6) + Power(t1,5)*(7 - 14*t2) + 22*t2 + 
                  12*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t1,4)*(-12 - 6*t2 + 19*Power(t2,2)) + 
                  Power(t1,2)*
                   (17 + 59*t2 - 15*Power(t2,2) - 23*Power(t2,3)) + 
                  t1*(7 - 77*t2 + 4*Power(t2,2) - 6*Power(t2,3)) - 
                  2*Power(t1,3)*(7 + t2 - 14*Power(t2,2) + 3*Power(t2,3))\
)) + s2*(6 + (3 - 13*s)*t2 + (-7 - 32*s + 12*Power(s,2))*Power(t2,2) + 
               (3 + 32*s - 21*Power(s,2))*Power(t2,3) - 
               (20 + 3*s + 4*Power(s,2))*Power(t2,4) + 
               (9 + 2*s)*Power(t2,5) + 2*Power(t2,6) - 
               Power(t1,7)*(-2 + s + Power(s,2) + 2*t2 - 5*s*t2) + 
               Power(t1,6)*(Power(s,2)*(4 - 9*t2) + t2*(-5 + 6*t2) + 
                  s*(-7 + 11*t2 - 14*Power(t2,2))) + 
               Power(t1,5)*(-12 + 41*t2 + 6*Power(t2,2) - 
                  6*Power(t2,3) + 
                  Power(s,2)*(6 - 4*t2 + 27*Power(t2,2)) + 
                  s*(8 - 23*t2 - 40*Power(t2,2) + 13*Power(t2,3))) + 
               Power(t1,4)*(6 + t2 - 42*Power(t2,2) - 14*Power(t2,3) + 
                  4*Power(t2,4) - 
                  Power(s,2)*
                   (4 + 18*t2 - 25*Power(t2,2) + 23*Power(t2,3)) + 
                  s*(14 + 49*t2 + 65*Power(t2,2) + 67*Power(t2,3) - 
                     4*Power(t2,4))) + 
               Power(t1,3)*(18 - 40*t2 - 45*Power(t2,2) + 
                  36*Power(t2,3) + 15*Power(t2,4) - 2*Power(t2,5) + 
                  Power(s,2)*
                   (-5 + 31*t2 - 4*Power(t2,2) - 50*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  s*(13 + 29*t2 - 27*Power(t2,2) + 14*Power(t2,3) + 
                     47*Power(t2,4))) + 
               Power(t1,2)*(-12 - 47*t2 + 131*Power(t2,2) + 
                  51*Power(t2,3) - 21*Power(t2,4) - 13*Power(t2,5) + 
                  Power(s,2)*t2*
                   (13 - 38*t2 + 10*Power(t2,2) + 25*Power(t2,3)) + 
                  s*(-7 + t2 + 9*Power(t2,2) - 135*Power(t2,3) - 
                     51*Power(t2,4) + 10*Power(t2,5))) + 
               t1*(-8 + 49*t2 - 49*Power(t2,2) + 50*Power(t2,3) - 
                  106*Power(t2,4) + 6*Power(t2,5) + 2*Power(t2,6) + 
                  Power(s,2)*t2*
                   (-17 + 18*t2 + 12*Power(t2,2) + 9*Power(t2,3)) + 
                  s*(6 - t2 + 69*Power(t2,2) - 79*Power(t2,3) + 
                     61*Power(t2,4) + 24*Power(t2,5))))))*
       B1(1 - s1 - t1 + t2,t1,t2))/
     ((s - s2 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,3)*(-1 + t2)*
       (s - s1 + t2)) - (8*(2*
          (-1 + Power(s,3) - s1 - 3*Power(s1,3) - 
            Power(s,2)*(3 + 4*s1) + s*(3 + 5*s1 + 6*Power(s1,2)) - 
            Power(s1,2)*(-10 + s2))*Power(t1,11) + 
         (-1 + s1 - t2)*Power(t2,4)*(1 + s2 + t2)*
          (1 + Power(s2,2)*(-2 + s1 - t2) + 2*s1*t2 + s*(-1 + t2)*t2 - 
            Power(t2,2) + s2*(1 + s1 + s*t2 + s1*t2 - Power(t2,2))) + 
         Power(t1,10)*(-28*Power(s1,4) + 
            Power(s,3)*(-6 + 3*s1 - 10*t2) + 
            Power(s1,3)*(63 - 14*s2 + 25*t2) + 6*(2 + t2 + s2*t2) + 
            s1*(15 - 4*s2 - 32*t2 + 8*s2*t2) + 
            Power(s1,2)*(-4 - 46*s2 + 2*Power(s2,2) - 27*t2 + 8*s2*t2) + 
            Power(s,2)*(24 - 23*Power(s1,2) + 32*t2 + 6*s2*t2 + 
               s1*(17 + 2*s2 + 36*t2)) + 
            s*(48*Power(s1,3) + Power(s1,2)*(9 + 2*s2 - 51*t2) - 
               2*(15 + 14*t2 + 6*s2*t2) - 
               s1*(41 - 2*s2 + 62*t2 + 10*s2*t2))) + 
         t1*Power(t2,3)*(Power(s2,3)*
             (-8 - 24*t2 - 14*Power(t2,2) + Power(t2,3)) - 
            Power(s2,2)*(-2 + (13 + s)*t2 + (34 + 5*s)*Power(t2,2) + 
               (21 + 5*s)*Power(t2,3) + 2*Power(t2,4)) + 
            (1 + t2)*(2 - (-19 + s)*t2 + 
               (-7 + 2*s + 2*Power(s,2))*Power(t2,2) + 
               (-13 - 6*s + 4*Power(s,2))*Power(t2,3) + 
               (-5 + 3*s)*Power(t2,4)) + 
            s2*(10 + 10*(4 + s)*t2 + 
               4*(8 + s + Power(s,2))*Power(t2,2) + 
               (-3 - 6*s + 4*Power(s,2))*Power(t2,3) - 
               2*(4 + s)*Power(t2,4) - 3*Power(t2,5)) - 
            Power(s1,3)*(s2 + 4*Power(s2,2) + 3*Power(s2,3) - 
               7*s2*Power(t2,2) - 4*Power(t2,2)*(1 + t2)) + 
            Power(s1,2)*(1 - (16 + s)*t2 - (29 + 4*s)*Power(t2,2) - 
               (30 + s)*Power(t2,3) - 8*Power(t2,4) + 
               Power(s2,3)*(5 + 3*t2) - 
               Power(s2,2)*(-1 + (6 + 7*s)*t2 + 10*Power(t2,2)) - 
               s2*(1 + (25 + 6*s)*t2 + (37 + 8*s)*Power(t2,2) + 
                  21*Power(t2,3))) + 
            s1*(-3 + (-3 + 2*s)*t2 + 
               (50 + 2*s - 2*Power(s,2))*Power(t2,2) + 
               (61 + 12*s - 4*Power(s,2))*Power(t2,3) + 
               (31 - 2*s)*Power(t2,4) + 4*Power(t2,5) + 
               Power(s2,3)*(6 + 13*t2 - Power(t2,2)) + 
               Power(s2,2)*(1 + (29 + 8*s)*t2 + 
                  3*(13 + 4*s)*Power(t2,2) + 12*Power(t2,3)) + 
               s2*(-8 + t2 - 4*s*t2 + 
                  (57 + 20*s - 4*Power(s,2))*Power(t2,2) + 
                  (49 + 10*s)*Power(t2,3) + 17*Power(t2,4)))) + 
         Power(t1,2)*Power(t2,2)*
          (-7 - 63*t2 - 5*s*t2 - 99*Power(t2,2) + 20*s*Power(t2,2) - 
            8*Power(s,2)*Power(t2,2) + 68*Power(t2,3) + 
            16*s*Power(t2,3) - 16*Power(s,2)*Power(t2,3) + 
            6*Power(s,3)*Power(t2,3) + 75*Power(t2,4) + 
            86*s*Power(t2,4) - 26*Power(s,2)*Power(t2,4) + 
            8*Power(s,3)*Power(t2,4) + 57*Power(t2,5) + 
            16*s*Power(t2,5) + 8*Power(s,2)*Power(t2,5) + 
            13*Power(t2,6) + 7*s*Power(t2,6) + 
            Power(s2,3)*(-6 + 27*t2 + 70*Power(t2,2) + 17*Power(t2,3) - 
               7*Power(t2,4)) + 
            Power(s2,2)*(27 + (33 + 9*s)*t2 + (39 + 28*s)*Power(t2,2) + 
               (95 + 24*s)*Power(t2,3) + (17 + 15*s)*Power(t2,4) - 
               6*Power(t2,5)) - 
            s2*(20 + (59 - 4*s)*t2 + 
               2*(45 + 5*s + 12*Power(s,2))*Power(t2,2) + 
               (103 - 88*s + 54*Power(s,2))*Power(t2,3) + 
               2*(-23 - 11*s + 8*Power(s,2))*Power(t2,4) + 
               (-17 + 2*s)*Power(t2,5) + 7*Power(t2,6)) - 
            Power(s1,4)*(2*Power(s2,2)*(-1 + t2) + 
               2*t2*(7 + 7*t2 + 8*Power(t2,2)) + 
               s2*(1 + 12*t2 + 17*Power(t2,2))) + 
            Power(s1,3)*(-17 + (46 + 13*s)*t2 + 
               (73 + 8*s)*Power(t2,2) + (52 + 15*s)*Power(t2,3) + 
               32*Power(t2,4) + Power(s2,3)*(6 + 7*t2) + 
               Power(s2,2)*(16 + 19*t2 + 10*Power(t2,2)) + 
               2*s2*(2 + 6*(4 + s)*t2 + (22 + 9*s)*Power(t2,2) + 
                  21*Power(t2,3))) + 
            Power(s1,2)*(12 + (71 + 3*s)*t2 + 
               (-21 - 19*s + 4*Power(s,2))*Power(t2,2) + 
               (15 - 39*s + 4*Power(s,2))*Power(t2,3) - 
               23*(-1 + s)*Power(t2,4) - 16*Power(t2,5) - 
               Power(s2,3)*(18 + 39*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-32 + (-65 + 19*s)*t2 + 
                  3*(-1 + s)*Power(t2,2) + 12*Power(t2,3)) + 
               s2*(35 + 3*(9 + 8*s)*t2 + 
                  (56 - 10*s - 5*Power(s,2))*Power(t2,2) + 
                  (101 - 54*s)*Power(t2,3) - 24*Power(t2,4))) + 
            s1*(15 - (21 + 4*s)*t2 + 
               (-164 - 27*s + 9*Power(s,2))*Power(t2,2) - 
               (192 + 40*s - 11*Power(s,2) + 5*Power(s,3))*Power(t2,3) + 
               (-194 + 7*s - 10*Power(s,2))*Power(t2,4) + 
               (-74 + s)*Power(t2,5) + 
               Power(s2,3)*(15 + 10*t2 - 4*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(s2,2)*(4 + 3*(-12 + 7*s)*t2 + 
                  (57 + 41*s)*Power(t2,2) + 3*(25 + 6*s)*Power(t2,3) + 
                  14*Power(t2,4)) + 
               s2*(-27 - (89 + 54*s)*t2 + 
                  4*(-27 - 20*s + 6*Power(s,2))*Power(t2,2) + 
                  (-283 - 26*s + 25*Power(s,2))*Power(t2,3) + 
                  2*(-85 + 19*s)*Power(t2,4) + 6*Power(t2,5)))) - 
         Power(t1,3)*t2*(-10 + 
            (s*(11 + 26*s2 + 7*Power(s2,2)) - 
               3*(27 + 16*s2 - 32*Power(s2,2) + 8*Power(s2,3)))*t2 + 
            (-225 - 20*s2 + 84*Power(s2,2) + 33*Power(s2,3) - 
               24*Power(s,2)*(1 + 2*s2) + 
               s*(80 + 54*s2 + 49*Power(s2,2)))*Power(t2,2) + 
            (20 + 26*Power(s,3) - 95*s2 + 75*Power(s2,2) + 
               83*Power(s2,3) - Power(s,2)*(49 + 158*s2) + 
               s*(92 + 222*s2 + 36*Power(s2,2)))*Power(t2,3) + 
            (231 + 45*Power(s,3) + 2*s2 + 95*Power(s2,2) + 
               5*Power(s2,3) - Power(s,2)*(119 + 81*s2) + 
               2*s*(121 + 71*s2 + 11*Power(s2,2)))*Power(t2,4) + 
            (162 - 2*Power(s,3) + 119*s2 - 6*Power(s,2)*s2 - 
               17*Power(s2,2) - 7*Power(s2,3) + 
               s*(90 - 2*s2 + 15*Power(s2,2)))*Power(t2,5) + 
            (80 + s - 12*Power(s,2) - 14*s2 + 18*s*s2 - 6*Power(s2,2))*
             Power(t2,6) - 3*(1 + s - s2)*Power(t2,7) - 
            Power(s1,5)*(4*t2*(5 + 2*t2 + 5*Power(t2,2)) + 
               s2*(1 + 8*t2 + 9*Power(t2,2))) + 
            Power(s1,4)*(-27 + (32 + 19*s)*t2 + 
               (15 + 4*s)*Power(t2,2) + (-22 + 31*s)*Power(t2,3) + 
               32*Power(t2,4) + 
               Power(s2,2)*(8 + 6*t2 - 4*Power(t2,2)) + 
               s2*(9 + (13 + 6*s)*t2 + 3*(-17 + 4*s)*Power(t2,2) + 
                  13*Power(t2,3))) + 
            Power(s1,3)*(11 + 2*(53 + 12*s)*t2 + 
               2*(87 + 2*s + Power(s,2))*Power(t2,2) + 
               (290 + 2*s - 8*Power(s,2))*Power(t2,3) + 
               (217 - 40*s)*Power(t2,4) + Power(s2,3)*t2*(11 + 5*t2) + 
               Power(s2,2)*(-12 + (10 - 4*s)*t2 + 
                  (53 - 4*s)*Power(t2,2) + 20*Power(t2,3)) + 
               s2*(53 + (31 + 8*s)*t2 + 
                  (204 + 62*s - 3*Power(s,2))*Power(t2,2) + 
                  (319 - 50*s)*Power(t2,3) - 2*Power(t2,4))) + 
            Power(s1,2)*(31 + 7*(14 + s)*t2 + 
               (-134 - 89*s + 11*Power(s,2))*Power(t2,2) - 
               (347 + 242*s - 32*Power(s,2) + 3*Power(s,3))*
                Power(t2,3) - 
               (302 + 175*s + 2*Power(s,2))*Power(t2,4) - 
               3*(67 + 10*s)*Power(t2,5) - 16*Power(t2,6) + 
               Power(s2,3)*(6 - 59*t2 - 63*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s2,2)*(-32 + (-103 + 28*s)*t2 + 
                  2*(-92 + 13*s)*Power(t2,2) - 
                  5*(7 + 3*s)*Power(t2,3) + 14*Power(t2,4)) + 
               s2*(-69 + 2*(-31 + 5*s)*t2 + 
                  (61 - 54*s - 17*Power(s,2))*Power(t2,2) + 
                  (-127 - 216*s + 27*Power(s,2))*Power(t2,3) + 
                  (-193 + 18*s)*Power(t2,4) + 28*Power(t2,5))) - 
            s1*(-1 + 8*(9 + 5*s)*t2 + 
               (391 + 38*s - 31*Power(s,2))*Power(t2,2) + 
               2*(158 + 24*s - 31*Power(s,2) + 9*Power(s,3))*
                Power(t2,3) + 
               (331 - 280*s + 40*Power(s,2) - 8*Power(s,3))*
                Power(t2,4) + 
               (203 - 166*s - 28*Power(s,2))*Power(t2,5) - 
               (17 + 42*s)*Power(t2,6) - 4*Power(t2,7) + 
               Power(s2,3)*(12 - 45*t2 - 86*Power(t2,2) - 
                  7*Power(t2,3) + 5*Power(t2,4)) + 
               Power(s2,2)*(-54 + 2*(-58 + 5*s)*t2 + 
                  2*(-52 + 19*s)*Power(t2,2) + (9 + 26*s)*Power(t2,3) + 
                  (87 - 4*s)*Power(t2,4) + 24*Power(t2,5)) + 
               s2*(10 + (127 + 92*s)*t2 + 
                  (250 + 288*s - 48*Power(s,2))*Power(t2,2) + 
                  (623 - 64*s - 34*Power(s,2))*Power(t2,3) + 
                  2*(264 - 91*s + 3*Power(s,2))*Power(t2,4) + 
                  (93 - 2*s)*Power(t2,5) + 33*Power(t2,6)))) + 
         Power(t1,9)*(-26 - 38*Power(s1,5) - 41*t2 - 32*s2*t2 + 
            10*Power(t2,2) - 18*s2*Power(t2,2) - 
            6*Power(s2,2)*Power(t2,2) + 
            Power(s1,4)*(146 - 22*s2 + 100*t2) + 
            Power(s1,3)*(-90 - 7*s2 + 4*Power(s2,2) - 69*t2 + 
               59*s2*t2 - 42*Power(t2,2)) + 
            Power(s,3)*(2 + Power(s1,2) + 29*t2 + 20*Power(t2,2) - 
               2*s1*(1 + 8*t2)) + 
            s1*(-29 - 47*t2 - 2*Power(s2,2)*(-2 + t2)*t2 + 
               62*Power(t2,2) + s2*(12 + 73*t2 - 32*Power(t2,2))) + 
            Power(s1,2)*(-83 - 148*t2 - 53*Power(t2,2) + 
               4*Power(s2,2)*(7 + 3*t2) + 
               s2*(93 + 81*t2 - 19*Power(t2,2))) - 
            Power(s,2)*(24 + 23*Power(s1,3) + (119 + 26*s2)*t2 + 
               24*(3 + s2)*Power(t2,2) - 
               Power(s1,2)*(83 + 3*s2 + 99*t2) + 
               s1*(-17 - 7*s2*(-2 + t2) + 49*t2 + 64*Power(t2,2))) + 
            s*(48 + 60*Power(s1,4) + 
               2*Power(s1,3)*(-70 + 2*s2 - 91*t2) + (137 + 58*s2)*t2 + 
               (68 + 46*s2 + 6*Power(s2,2))*Power(t2,2) - 
               Power(s1,2)*(86 + 80*t2 - 83*Power(t2,2) + 
                  50*s2*(1 + t2)) + 
               2*s1*(12 + 79*t2 - Power(s2,2)*t2 + 78*Power(t2,2) + 
                  s2*(4 + 8*t2 + 19*Power(t2,2))))) + 
         Power(t1,8)*(20 - 16*Power(s1,6) - 
            2*Power(s1,5)*(-64 + 5*s2 - 55*t2) + 89*t2 + 66*s2*t2 + 
            67*Power(t2,2) + 51*s2*Power(t2,2) + 
            30*Power(s2,2)*Power(t2,2) - 29*Power(t2,3) + 
            30*s2*Power(t2,3) + 14*Power(s2,2)*Power(t2,3) + 
            2*Power(s2,3)*Power(t2,3) + 
            Power(s1,4)*(-201 + 2*Power(s2,2) - 290*t2 - 
               153*Power(t2,2) + 8*s2*(3 + 11*t2)) + 
            Power(s1,3)*(-73 - 203*t2 - 119*Power(t2,2) + 
               34*Power(t2,3) + Power(s2,2)*(5 + 29*t2) + 
               s2*(94 - 157*t2 - 134*Power(t2,2))) + 
            Power(s,3)*(-10*Power(s1,2)*(-1 + t2) + 
               s1*(-3 + 13*t2 + 34*Power(t2,2)) - 
               2*(-3 + 5*t2 + 27*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s1,2)*(38 + 417*t2 + 337*Power(t2,2) + 
               136*Power(t2,3) - 2*Power(s2,3)*(3 + 2*t2) - 
               5*Power(s2,2)*(19 + 13*t2 + 9*Power(t2,2)) + 
               s2*(58 - 159*t2 - 59*Power(t2,2) + 26*Power(t2,3))) + 
            s1*(6 + 290*t2 + 179*Power(t2,2) - 
               4*Power(s2,3)*Power(t2,2) + 35*Power(t2,3) + 
               Power(s2,2)*t2*(-80 - 31*t2 + 8*Power(t2,2)) + 
               s2*(4 - 167*t2 - 21*Power(t2,2) + 58*Power(t2,3))) + 
            Power(s,2)*(-12 - 8*Power(s1,4) + (91 + 38*s2)*t2 + 
               5*(48 + 19*s2)*Power(t2,2) + 4*(22 + 9*s2)*Power(t2,3) + 
               Power(s1,3)*(48 + s2 + 97*t2) + 
               Power(s1,2)*(-20 + 6*s2*(-7 + t2) - 279*t2 - 
                  177*Power(t2,2)) + 
               s1*(-37 - 200*t2 + 15*Power(t2,2) + 56*Power(t2,3) + 
                  s2*(22 + 19*t2 - 45*Power(t2,2)))) + 
            s*(-12 + 24*Power(s1,5) + 2*Power(s1,4)*(-79 + s2 - 98*t2) - 
               2*(93 + 52*s2)*t2 - 
               (275 + 210*s2 + 28*Power(s2,2))*Power(t2,2) - 
               (111 + 74*s2 + 18*Power(s2,2))*Power(t2,3) + 
               Power(s1,3)*(27 + s2*(8 - 78*t2) + 426*t2 + 
                  287*Power(t2,2)) + 
               Power(s1,2)*(80 - 3*Power(s2,2)*(-7 + t2) + 617*t2 + 
                  241*Power(t2,2) - 66*Power(t2,3) + 
                  2*s2*(43 + 112*t2 + 84*Power(t2,2))) + 
               s1*(60 - 13*t2 - 275*Power(t2,2) - 200*Power(t2,3) + 
                  Power(s2,2)*t2*(-8 + 21*t2) - 
                  2*s2*(24 - 51*t2 + 14*Power(t2,2) + 27*Power(t2,3))))) \
+ Power(t1,7)*(10 - 56*t2 - 64*s2*t2 - 249*Power(t2,2) - 
            106*s2*Power(t2,2) - 26*Power(s2,2)*Power(t2,2) - 
            96*Power(t2,3) - 94*s2*Power(t2,3) - 
            59*Power(s2,2)*Power(t2,3) - 8*Power(s2,3)*Power(t2,3) - 
            7*Power(t2,4) - 39*s2*Power(t2,4) - 
            14*Power(s2,2)*Power(t2,4) - 4*Power(s2,3)*Power(t2,4) + 
            32*Power(s1,6)*(1 + t2) + 
            Power(s1,5)*(-127 - 244*t2 - 129*Power(t2,2) + 
               s2*(4 + 30*t2)) + 
            Power(s1,4)*(25 + 75*t2 + 177*Power(t2,2) + 
               121*Power(t2,3) + 6*Power(s2,2)*(-1 + 2*t2) + 
               s2*(43 - 174*t2 - 153*Power(t2,2))) + 
            Power(s,3)*(-4 - 29*t2 + 16*Power(t2,2) + 46*Power(t2,3) + 
               10*Power(t2,4) + 
               Power(s1,2)*(1 - 32*t2 + 24*Power(t2,2)) - 
               2*s1*(-1 + 6*t2 + 10*Power(t2,2) + 18*Power(t2,3))) - 
            Power(s1,3)*(-179 - 657*t2 - 893*Power(t2,2) - 
               301*Power(t2,3) + 16*Power(t2,4) + 
               Power(s2,3)*(1 + 5*t2) + 
               Power(s2,2)*(47 + 39*t2 + 57*Power(t2,2)) - 
               s2*(-28 + 98*t2 + 282*Power(t2,2) + 167*Power(t2,3))) + 
            Power(s1,2)*(124 + 42*t2 - 344*Power(t2,2) - 
               263*Power(t2,3) - 104*Power(t2,4) + 
               Power(s2,3)*(24 + 17*t2 - 4*Power(t2,2)) + 
               Power(s2,2)*(82 + 75*t2 + 34*Power(t2,2) + 
                  71*Power(t2,3)) + 
               s2*(-222 - 119*t2 + 511*Power(t2,2) + 148*Power(t2,3) - 
                  20*Power(t2,4))) + 
            s1*(36 - 183*t2 - 757*Power(t2,2) - 393*Power(t2,3) - 
               146*Power(t2,4) + 
               Power(s2,3)*t2*(12 + 20*t2 + 11*Power(t2,2)) + 
               Power(s2,2)*t2*
                (182 + 167*t2 + 57*Power(t2,2) - 10*Power(t2,3)) - 
               s2*(56 + 112*t2 - 137*Power(t2,2) + 199*Power(t2,3) + 
                  60*Power(t2,4))) + 
            Power(s,2)*(30 + (85 - 10*s2)*t2 - 
               5*(15 + 26*s2)*Power(t2,2) - 123*(2 + s2)*Power(t2,3) - 
               2*(31 + 12*s2)*Power(t2,4) + 8*Power(s1,4)*(1 + 3*t2) - 
               Power(s1,3)*(15 + 140*t2 + 153*Power(t2,2) + 
                  s2*(10 + 3*t2)) + 
               Power(s1,2)*(-39 + 8*t2 + 291*Power(t2,2) + 
                  167*Power(t2,3) + s2*(41 + 115*t2 - 36*Power(t2,2))) \
+ s1*(-5 + 196*t2 + 575*Power(t2,2) + 109*Power(t2,3) - 
                  24*Power(t2,4) + 
                  s2*(-10 - 3*t2 + 30*Power(t2,2) + 73*Power(t2,3)))) + 
            s*(-42 + 12*(-1 + 6*s2)*t2 + 
               (297 + 272*s2 + 56*Power(s2,2))*Power(t2,2) + 
               (355 + 294*s2 + 75*Power(s2,2))*Power(t2,3) + 
               3*(39 + 22*s2 + 6*Power(s2,2))*Power(t2,4) - 
               8*Power(s1,5)*(5 + 7*t2) + 
               Power(s1,4)*(69 + s2*(22 - 24*t2) + 392*t2 + 
                  253*Power(t2,2)) + 
               2*Power(s1,3)*
                (43 + 104*t2 - 200*Power(t2,2) - 124*Power(t2,3) + 
                  2*Power(s2,2)*(1 + t2) + 
                  s2*(2 + 29*t2 + 91*Power(t2,2))) + 
               Power(s1,2)*(10 - 469*t2 - 1541*Power(t2,2) - 
                  399*Power(t2,3) + 30*Power(t2,4) + 
                  Power(s2,2)*(-49 - 62*t2 + 18*Power(t2,2)) - 
                  2*s2*(21 + 144*t2 + 156*Power(t2,2) + 112*Power(t2,3))\
) - 2*s1*(32 + 123*t2 + 182*Power(t2,2) - 147*Power(t2,3) - 
                  70*Power(t2,4) + 
                  Power(s2,2)*t2*(-3 + 10*t2 + 24*Power(t2,2)) + 
                  s2*(-38 + 64*t2 + 198*Power(t2,2) + 43*Power(t2,3) - 
                     17*Power(t2,4))))) - 
         Power(t1,6)*(28 + 56*t2 - 26*s2*t2 - 185*Power(t2,2) - 
            204*s2*Power(t2,2) + 27*Power(s2,2)*Power(t2,2) + 
            6*Power(s2,3)*Power(t2,2) - 438*Power(t2,3) - 
            155*s2*Power(t2,3) - 74*Power(s2,2)*Power(t2,3) - 
            10*Power(s2,3)*Power(t2,3) - 153*Power(t2,4) - 
            205*s2*Power(t2,4) - 36*Power(s2,2)*Power(t2,4) - 
            15*Power(s2,3)*Power(t2,4) - 52*Power(t2,5) - 
            34*s2*Power(t2,5) - 10*Power(s2,2)*Power(t2,5) - 
            2*Power(s2,3)*Power(t2,5) + 24*Power(s1,6)*Power(1 + t2,2) - 
            Power(s1,5)*(46 + 121*t2 + 166*Power(t2,2) + 
               71*Power(t2,3) + s2*(3 - 28*t2 - 35*Power(t2,2))) + 
            Power(s1,4)*(-118 - 231*t2 - 436*Power(t2,2) - 
               63*Power(t2,3) + 56*Power(t2,4) + 
               2*Power(s2,2)*(2 + 5*t2 + 3*Power(t2,2)) - 
               2*s2*(-21 + 83*t2 + 116*Power(t2,2) + 59*Power(t2,3))) + 
            Power(s,3)*t2*(Power(s1,2)*(6 - 36*t2 + 22*Power(t2,2)) - 
               s1*(-9 + 62*t2 + 2*Power(t2,2) + 19*Power(t2,3)) + 
               2*(-10 - 36*t2 + 5*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,3)*(42 + 439*t2 + 1012*Power(t2,2) + 
               1119*Power(t2,3) + 265*Power(t2,4) - 9*Power(t2,5) + 
               Power(s2,3)*(-5 - 9*t2 + Power(t2,2)) - 
               Power(s2,2)*(63 - 7*t2 + 43*Power(t2,2) + 
                  46*Power(t2,3)) + 
               s2*(156 + 55*t2 + 127*Power(t2,2) + 95*Power(t2,3) + 
                  97*Power(t2,4))) + 
            Power(s1,2)*(130 + 640*t2 + 524*Power(t2,2) + 
               496*Power(t2,3) - 36*Power(t2,4) - 35*Power(t2,5) + 
               Power(s2,3)*(36 + 27*t2 - 25*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(s2,2)*(-14 - 37*t2 - 35*Power(t2,2) + 
                  46*Power(t2,3) + 52*Power(t2,4)) + 
               s2*(-120 - 180*t2 + 724*Power(t2,2) + 830*Power(t2,3) + 
                  287*Power(t2,4) - 10*Power(t2,5))) + 
            s1*(37 + 240*t2 - 388*Power(t2,2) - 839*Power(t2,3) - 
               489*Power(t2,4) - 106*Power(t2,5) + 
               Power(s2,3)*t2*
                (48 + 30*t2 + 31*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,2)*t2*
                (52 + 191*t2 + 123*Power(t2,2) + 78*Power(t2,3) - 
                  4*Power(t2,4)) - 
               s2*(84 + 478*t2 + 121*Power(t2,2) - 96*Power(t2,3) + 
                  182*Power(t2,4) + 40*Power(t2,5))) + 
            Power(s,2)*(12 + (127 + 20*s2)*t2 + 
               (248 + 3*s2)*Power(t2,2) - 3*(-35 + 52*s2)*Power(t2,3) - 
               (116 + 59*s2)*Power(t2,4) - 6*(4 + s2)*Power(t2,5) + 
               24*Power(s1,4)*t2*(1 + t2) + 
               s1*(-16 + (-68 + 11*s2)*t2 + 
                  (376 + 129*s2)*Power(t2,2) + 
                  (667 + 64*s2)*Power(t2,3) + 
                  7*(26 + 7*s2)*Power(t2,4) - 4*Power(t2,5)) - 
               Power(s1,3)*(2 + 37*t2 + 132*Power(t2,2) + 
                  107*Power(t2,3) + 3*s2*(-1 + 10*t2 + Power(t2,2))) + 
               Power(s1,2)*(1 - 137*t2 - 94*Power(t2,2) + 
                  29*Power(t2,3) + 84*Power(t2,4) + 
                  s2*(2 + 96*t2 + 93*Power(t2,2) - 42*Power(t2,3)))) + 
            s*(-42 - 2*(101 + 4*s2)*t2 + 
               (-94 + 54*s2 + 39*Power(s2,2))*Power(t2,2) + 
               (215 + 228*s2 + 139*Power(s2,2))*Power(t2,3) + 
               (359 + 178*s2 + 60*Power(s2,2))*Power(t2,4) + 
               (74 + 34*s2 + 6*Power(s2,2))*Power(t2,5) - 
               24*Power(s1,5)*(1 + 3*t2 + 2*Power(t2,2)) + 
               Power(s1,4)*(s2*(6 + 12*t2 - 30*Power(t2,2)) + 
                  2*t2*(43 + 160*t2 + 79*Power(t2,2))) + 
               Power(s1,3)*(19 + 383*t2 + 724*Power(t2,2) - 
                  7*Power(t2,3) - 123*Power(t2,4) + 
                  4*Power(s2,2)*(1 + 6*t2 + Power(t2,2)) + 
                  2*s2*(12 + 55*t2 + 71*Power(t2,2) + 73*Power(t2,3))) + 
               Power(s1,2)*(33 + 83*t2 - 777*Power(t2,2) - 
                  1761*Power(t2,3) - 413*Power(t2,4) + 11*Power(t2,5) + 
                  Power(s2,2)*
                   (-35 - 144*t2 - 34*Power(t2,2) + 27*Power(t2,3)) - 
                  2*s2*(8 + 18*t2 + 179*Power(t2,2) + 59*Power(t2,3) + 
                     67*Power(t2,4))) - 
               s1*(-3 + 158*t2 + 685*Power(t2,2) + 1079*Power(t2,3) - 
                  149*Power(t2,4) - 60*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (22 + 20*t2 + 85*Power(t2,2) + 41*Power(t2,3)) + 
                  s2*(-50 - 110*t2 + 624*Power(t2,2) + 540*Power(t2,3) + 
                     230*Power(t2,4) - 8*Power(t2,5))))) + 
         Power(t1,5)*(18 + 103*t2 + 126*Power(t2,2) - 
            196*s2*Power(t2,2) + 24*Power(s2,3)*Power(t2,2) - 
            367*Power(t2,3) - 216*s2*Power(t2,3) - 
            39*Power(s2,2)*Power(t2,3) - 7*Power(s2,3)*Power(t2,3) - 
            438*Power(t2,4) - 260*s2*Power(t2,4) - 
            66*Power(s2,2)*Power(t2,4) - 25*Power(s2,3)*Power(t2,4) - 
            207*Power(t2,5) - 196*s2*Power(t2,5) + 
            Power(s2,2)*Power(t2,5) - 5*Power(s2,3)*Power(t2,5) - 
            36*Power(t2,6) - 20*s2*Power(t2,6) - 
            4*Power(s2,2)*Power(t2,6) + 8*Power(s1,6)*Power(1 + t2,3) + 
            Power(s,3)*Power(t2,2)*
             (-42 - 110*t2 + 4*Power(t2,2) - 3*Power(t2,3) + 
               Power(s1,2)*(12 - 16*t2 + 7*Power(t2,2)) + 
               s1*(20 - 84*t2 + 14*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,5)*(15 - 18*t2 + 27*Power(t2,2) - 26*Power(t2,3) - 
               20*Power(t2,4) + 
               s2*(-14 + 39*t2 + 42*Power(t2,2) + 9*Power(t2,3))) + 
            Power(s1,4)*(-63 - 228*t2 - 435*Power(t2,2) - 
               568*Power(t2,3) - 104*Power(t2,4) + 16*Power(t2,5) + 
               2*Power(s2,2)*(7 - 6*t2 - Power(t2,2) + 2*Power(t2,3)) - 
               s2*(25 + 31*t2 + 296*Power(t2,2) + 101*Power(t2,3) + 
                  25*Power(t2,4))) + 
            Power(s1,3)*(-47 - 121*t2 + 323*Power(t2,2) + 
               405*Power(t2,3) + 578*Power(t2,4) + 121*Power(t2,5) - 
               4*Power(t2,6) + 
               Power(s2,3)*(-7 - 3*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-25 + 43*t2 + 59*Power(t2,2) - 
                  71*Power(t2,3) - 16*Power(t2,4)) + 
               s2*(139 + 72*t2 - 279*Power(t2,2) - 38*Power(t2,3) - 
                  109*Power(t2,4) + 19*Power(t2,5))) + 
            s1*(11 + 295*t2 + 571*Power(t2,2) - 323*Power(t2,3) - 
               365*Power(t2,4) - 363*Power(t2,5) - 22*Power(t2,6) + 
               Power(s2,3)*t2*
                (72 + t2 - 19*Power(t2,2) + 34*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-196 - 109*t2 + 30*Power(t2,2) + 108*Power(t2,3) + 
                  66*Power(t2,4)) + 
               s2*(-52 - 355*t2 - 336*Power(t2,2) + 581*Power(t2,3) + 
                  210*Power(t2,4) + 61*Power(t2,5) - 20*Power(t2,6))) + 
            Power(s1,2)*(35 + 357*t2 + 959*Power(t2,2) + 
               1108*Power(t2,3) + 1105*Power(t2,4) + 87*Power(t2,5) - 
               15*Power(t2,6) + 
               Power(s2,3)*(24 + 13*t2 - 15*Power(t2,2) - 
                  12*Power(t2,3) - 4*Power(t2,4)) + 
               Power(s2,2)*(-46 - 67*t2 + 23*Power(t2,2) + 
                  8*Power(t2,3) + 51*Power(t2,4) + 12*Power(t2,5)) + 
               s2*(19 + 249*t2 + 414*Power(t2,2) + 1222*Power(t2,3) + 
                  586*Power(t2,4) + 208*Power(t2,5) - 3*Power(t2,6))) + 
            Power(s,2)*t2*(38 + 199*t2 + 368*Power(t2,2) + 
               207*Power(t2,3) + 9*Power(t2,4) - 4*Power(t2,5) + 
               8*Power(s1,4)*t2*(3 + t2) + 
               s2*(12 + 102*t2 + 85*Power(t2,2) - 68*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s1,3)*(6 + 21*t2 + 36*Power(t2,2) + 
                  28*Power(t2,3) + s2*(-9 + 30*t2 + Power(t2,2))) + 
               Power(s1,2)*(-1 - 181*t2 - 130*Power(t2,2) - 
                  114*Power(t2,3) + 18*Power(t2,4) + 
                  s2*(11 + 42*t2 + 9*Power(t2,2) - 15*Power(t2,3))) + 
               s1*(-51 - 173*t2 + 332*Power(t2,2) + 296*Power(t2,3) + 
                  120*Power(t2,4) + 
                  s2*(-12 + 68*t2 + 185*Power(t2,2) + 32*Power(t2,3) + 
                     12*Power(t2,4)))) - 
            s*(12 + (141 + 34*s2)*t2 + 
               (330 + 124*s2 + 15*Power(s2,2))*Power(t2,2) + 
               (195 + 200*s2 - 90*Power(s2,2))*Power(t2,3) + 
               (6 - 54*s2 - 95*Power(s2,2))*Power(t2,4) - 
               (261 + 6*s2 + 7*Power(s2,2))*Power(t2,5) - 
               2*(15 + 4*s2)*Power(t2,6) + 
               8*Power(s1,5)*
                (1 + 3*t2 + 6*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s1,4)*(7 + 40*t2 + 30*Power(t2,2) - 
                  84*Power(t2,3) - 41*Power(t2,4) + 
                  s2*(-6 + 36*t2 + 6*Power(t2,2) + 8*Power(t2,3))) - 
               2*Power(s1,3)*
                (-1 + 31*t2 + 262*Power(t2,2) + 380*Power(t2,3) + 
                  91*Power(t2,4) - 14*Power(t2,5) + 
                  12*Power(s2,2)*t2*(1 + t2) + 
                  s2*(4 + 23*t2 + 117*Power(t2,2) + 43*Power(t2,3) + 
                     19*Power(t2,4))) + 
               Power(s1,2)*(-8 - 69*t2 - 333*Power(t2,2) + 
                  350*Power(t2,3) + 907*Power(t2,4) + 247*Power(t2,5) - 
                  3*Power(t2,6) + 
                  Power(s2,2)*
                   (7 + 102*t2 + 114*Power(t2,2) - 30*Power(t2,3) - 
                     12*Power(t2,4)) + 
                  2*s2*(6 - 37*t2 - 33*Power(t2,2) + 91*Power(t2,3) - 
                     27*Power(t2,4) + 15*Power(t2,5))) + 
               2*s1*(-7 - 28*t2 + 88*Power(t2,2) + 562*Power(t2,3) + 
                  649*Power(t2,4) + 35*Power(t2,5) - 11*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (10 + 21*t2 + 33*Power(t2,2) + 39*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  s2*(-6 - 88*t2 - 13*Power(t2,2) + 409*Power(t2,3) + 
                     171*Power(t2,4) + 91*Power(t2,5))))) + 
         Power(t1,4)*(-4 + (-55 - 2*s2 + 4*s*(7 + 3*s2))*t2 + 
            (-213 + 37*s2 + 98*Power(s2,2) - 36*Power(s2,3) - 
               4*Power(s,2)*(11 + 10*s2) + 
               s*(157 + 96*s2 + 27*Power(s2,2)))*Power(t2,2) + 
            (-110 + 46*Power(s,3) + 154*s2 + 59*Power(s2,2) + 
               17*Power(s2,3) - Power(s,2)*(141 + 190*s2) + 
               s*(251 + 258*s2 + 32*Power(s2,2)))*Power(t2,3) + 
            (391 + 98*Power(s,3) + 51*s2 + 92*Power(s2,2) + 
               49*Power(s2,3) - Power(s,2)*(290 + 137*s2) + 
               s*(285 + 304*s2 - 44*Power(s2,2)))*Power(t2,4) + 
            (296 - 4*Power(s,3) + 250*s2 + 19*Power(s2,2) + 
               2*Power(s2,3) - 2*Power(s,2)*(51 + s2) + 
               s*(133 + 6*s2 + 3*Power(s2,2)))*Power(t2,5) + 
            (179 + 2*Power(s,3) + 59*s2 - 14*Power(s2,2) - 
               2*Power(s2,3) - 6*Power(s,2)*(6 + s2) + 
               s*(-95 + 48*s2 + 6*Power(s2,2)))*Power(t2,6) + 
            (3 - 11*s + 10*s2)*Power(t2,7) - 
            8*Power(s1,6)*(t2 + Power(t2,3)) - 
            Power(s1,5)*(12 + (5 - 8*s)*t2 + 48*Power(t2,2) + 
               (65 - 16*s)*Power(t2,3) + 
               s2*(-5 + 6*t2 + 51*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,4)*(3 + (25 + 26*s)*t2 + 
               11*(17 + 4*s)*Power(t2,2) + 
               (233 + 78*s - 8*Power(s,2))*Power(t2,3) + 
               2*(106 + s)*Power(t2,4) + 32*Power(t2,5) - 
               6*Power(s2,2)*(1 - 3*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               s2*(22 + 20*t2 + (51 + 54*s)*Power(t2,2) - 
                  4*(-31 + s)*Power(t2,3) - 5*Power(t2,4))) + 
            Power(s1,3)*(16 + (161 + 13*s)*t2 + 
               (200 - 47*s + 6*Power(s,2))*Power(t2,2) - 
               3*(-49 + 80*s + 3*Power(s,2))*Power(t2,3) + 
               (280 - 311*s - 4*Power(s,2))*Power(t2,4) - 
               (89 + 75*s)*Power(t2,5) - 32*Power(t2,6) + 
               Power(s2,3)*(3 - t2 + 3*Power(t2,2) + Power(t2,3)) + 
               s2*(-28 - 12*(-3 + s)*t2 + 
                  (165 + 22*s - 9*Power(s,2))*Power(t2,2) + 
                  (483 - 178*s + 10*Power(s,2))*Power(t2,3) + 
                  (121 - 10*s)*Power(t2,4) + 50*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-38 - 51*t2 + 63*Power(t2,2) + 26*Power(t2,3) - 
                  4*s*(1 + 6*t2 + Power(t2,2)))) + 
            Power(s1,2)*(Power(s2,3)*
                (-6 + 7*t2 - 47*Power(t2,2) - 23*Power(t2,3) + 
                  12*Power(t2,4)) - 
               Power(s2,2)*(-15 + (24 - 23*s)*t2 + 
                  (118 - 90*s)*Power(t2,2) + (153 - 4*s)*Power(t2,3) + 
                  (27 + 23*s)*Power(t2,4) + 2*Power(t2,5)) - 
               s2*(20 + (309 - 4*s)*t2 + 
                  (220 + 126*s + 21*Power(s,2))*Power(t2,2) + 
                  (411 + 222*s - 40*Power(s,2))*Power(t2,3) + 
                  (798 - 44*s - 11*Power(s,2))*Power(t2,4) + 
                  17*(7 + 2*s)*Power(t2,5) + 45*Power(t2,6)) + 
               t2*(30 - 289*t2 - 811*Power(t2,2) + 
                  2*Power(s,3)*(-5 + t2)*Power(t2,2) - 987*Power(t2,3) - 
                  772*Power(t2,4) - 61*Power(t2,5) + 8*Power(t2,6) + 
                  Power(s,2)*t2*
                   (9 + 111*t2 + 46*Power(t2,2) + 48*Power(t2,3)) + 
                  s*(-3 - 102*t2 - 462*Power(t2,2) - 190*Power(t2,3) + 
                     126*Power(t2,4) + 63*Power(t2,5)))) + 
            s1*(Power(s2,3)*t2*
                (-48 + 43*t2 + 109*Power(t2,2) - 22*Power(t2,3) - 
                  11*Power(t2,4)) + 
               s2*(12 + (65 - 46*s)*t2 + 
                  (31 - 356*s + 40*Power(s,2))*Power(t2,2) + 
                  (-497 + 88*s - 38*Power(s,2))*Power(t2,3) + 
                  (-909 + 514*s - 81*Power(s,2))*Power(t2,4) - 
                  3*(73 - 30*s + Power(s,2))*Power(t2,5) + 
                  (-125 + 48*s)*Power(t2,6) + 6*Power(t2,7)) + 
               Power(s2,2)*t2*
                (196 + 286*t2 + 95*Power(t2,2) + 2*Power(t2,3) - 
                  114*Power(t2,4) - 18*Power(t2,5) + 
                  s*(2 + 11*t2 + 12*Power(t2,2) + 44*Power(t2,3) + 
                     21*Power(t2,4))) - 
               t2*(82 + 530*t2 + 525*Power(t2,2) + 68*Power(t2,3) + 
                  111*Power(t2,4) - 137*Power(t2,5) - 3*Power(t2,6) + 
                  Power(s,3)*Power(t2,2)*(26 - 45*t2 + 7*Power(t2,2)) + 
                  Power(s,2)*t2*
                   (-59 - 165*t2 + 145*Power(t2,2) - 7*Power(t2,3) + 
                     30*Power(t2,4)) + 
                  s*(51 + 66*t2 - 61*Power(t2,2) - 900*Power(t2,3) - 
                     737*Power(t2,4) - 125*Power(t2,5) + 6*Power(t2,6)))))\
)*R1(t1))/(Power(-1 + t1,3)*t1*(s - s2 + t1)*Power(t1 - t2,3)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)*(s - s1 + t2)) + 
    (8*(Power(s1,10)*(-4*Power(t1,4)*t2 + 4*Power(t1,2)*Power(t2,3)) - 
         Power(s1,9)*t1*(24*Power(t1,4)*t2 + 8*Power(t2,4) + 
            Power(t1,3)*(2 + (-9 - 4*s + 4*s2)*t2 + Power(t2,2)) - 
            Power(t1,2)*t2*(8 + 21*t2 + 41*Power(t2,2) + 
               s2*(-1 + 11*t2)) + 
            t1*Power(t2,2)*(-6 + 4*(4 + s)*t2 + 22*Power(t2,2) + 
               s2*(5 + 13*t2))) + 
         Power(s1,8)*(-60*Power(t1,6)*t2 + 4*Power(t2,5) - 
            4*Power(t1,5)*(3 - 2*(8 + 3*s - 3*s2)*t2 + 6*Power(t2,2)) + 
            Power(t1,4)*(-2 + (70 + 5*s - 16*s2)*t2 + 
               (151 + s + 72*s2)*Power(t2,2) + 207*Power(t2,3)) + 
            2*t1*Power(t2,3)*
             (-6 + 4*(4 + s)*t2 + 2*Power(s2,2)*t2 + 22*Power(t2,2) + 
               s2*(5 + 13*t2)) + 
            Power(t1,2)*Power(t2,2)*
             (-25 - 4*Power(s2,2)*(-3 + t2) + (-34 + s)*t2 + 
               (-65 + 17*s)*Power(t2,2) + 44*Power(t2,3) + 
               s2*(-17 + (17 + 12*s)*t2 + 38*Power(t2,2))) - 
            Power(t1,3)*t2*(-9 + 2*(9 + 7*s)*t2 - 12*Power(s2,2)*t2 + 
               2*(73 + 21*s)*Power(t2,2) + 191*Power(t2,3) + 
               s2*(5 + 3*(19 + 4*s)*t2 + 104*Power(t2,2)))) - 
         Power(s1,7)*(80*Power(t1,7)*t2 + 
            Power(t1,6)*(30 - 15*(13 + 4*s - 4*s2)*t2 + 
               101*Power(t2,2)) - 
            Power(t1,5)*(-12 + (241 + 28*s - 71*s2)*t2 + 
               (402 + 34*s + 173*s2)*Power(t2,2) + 575*Power(t2,3)) + 
            Power(t1,4)*(-4 - (78 + s - 27*s2)*t2 + 
               4*(78 + 54*s2 - 20*Power(s2,2) + 7*s*(4 + 3*s2))*
                Power(t2,2) + (483 + 241*s + 269*s2)*Power(t2,3) + 
               691*Power(t2,4)) + 
            Power(t2,4)*(-6 - 2*Power(s2,3) + 4*(4 + s)*t2 + 
               4*Power(s2,2)*t2 + 22*Power(t2,2) + s2*(5 + 13*t2)) + 
            t1*Power(t2,3)*(-26 - Power(s2,3)*(-5 + t2) + 
               (-21 + 2*s)*t2 + (-7 + 34*s)*Power(t2,2) + 
               88*Power(t2,3) + 
               2*Power(s2,2)*(12 + (3 + s)*t2 + 7*Power(t2,2)) + 
               s2*(-37 + 3*(25 + 8*s)*t2 + (76 - 8*s)*Power(t2,2))) + 
            Power(t1,2)*Power(t2,2)*
             (13 + (22 - 13*s)*t2 + Power(s2,2)*(57 - 55*t2)*t2 - 
               2*(213 + 62*s)*Power(t2,2) + (-513 + 41*s)*Power(t2,3) + 
               38*Power(t2,4) + Power(s2,3)*(3 + 5*t2) + 
               s2*(-41 - 8*(31 + 5*s)*t2 + (-249 + 52*s)*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(t1,3)*t2*(11 + (283 + 8*s)*t2 + 2*Power(s2,3)*t2 + 
               (340 + 6*s)*Power(t2,2) + (359 - 188*s)*Power(t2,3) - 
               299*Power(t2,4) + Power(s2,2)*t2*(-97 - 2*s + 67*t2) - 
               s2*(5 - (91 + 16*s)*t2 + 2*(33 + 64*s)*Power(t2,2) + 
                  150*Power(t2,3)))) + 
         Power(s1,6)*(-60*Power(t1,8)*t2 - 
            8*Power(t1,7)*(5 + (-41 - 10*s + 10*s2)*t2 + 
               23*Power(t2,2)) + 
            Power(t1,6)*(-30 + (437 + 63*s - 148*s2)*t2 + 
               3*(172 + 49*s + 60*s2)*Power(t2,2) + 889*Power(t2,3)) - 
            Power(t1,5)*(-16 + (-226 - 7*s + 68*s2)*t2 + 
               (1038 + 12*Power(s,2) + 458*s2 - 212*Power(s2,2) + 
                  s*(293 + 228*s2))*Power(t2,2) + 
               (685 + 756*s + 202*s2)*Power(t2,3) + 1243*Power(t2,4)) + 
            Power(t1,4)*(2 + (-43 - 5*s + 34*s2)*t2 + 
               (-1133 - 149*s2 + 4*Power(s,2)*s2 + 317*Power(s2,2) - 
                  8*Power(s2,3) + s*(-81 - 88*s2 + 6*Power(s2,2)))*
                Power(t2,2) + 
               (-1234 + 32*Power(s,2) + 221*s2 - 269*Power(s2,2) + 
                  2*s*(-89 + 224*s2))*Power(t2,3) + 
               (-1315 + 852*s + 4*s2)*Power(t2,4) + 729*Power(t2,5)) + 
            Power(t2,4)*(-9 + (-4 + s)*t2 + 17*(1 + s)*Power(t2,2) + 
               44*Power(t2,3) - Power(s2,3)*(3 + 5*t2) + 
               2*Power(s2,2)*(3 + (5 + 4*s)*t2 + 7*Power(t2,2)) + 
               s2*(-19 + (47 + 12*s)*t2 + (38 - 8*s)*Power(t2,2))) + 
            t1*Power(t2,3)*(7 + 66*t2 - 10*(41 + 13*s)*Power(t2,2) + 
               (-453 + 82*s + 4*Power(s,2))*Power(t2,3) + 
               76*Power(t2,4) + 
               Power(s2,3)*(21 + 32*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(15 + (83 - 13*s)*t2 + 
                  (-72 + 11*s)*Power(t2,2) + 16*Power(t2,3)) - 
               s2*(67 + (281 + 46*s)*t2 + 
                  2*(79 - 37*s + 2*Power(s,2))*Power(t2,2) + 
                  4*(-3 + 5*s)*Power(t2,3))) + 
            Power(t1,2)*Power(t2,2)*
             (31 + 4*(103 + s)*t2 + (377 + 30*s)*Power(t2,2) - 
               (299 + 539*s)*Power(t2,3) + (-913 + 69*s)*Power(t2,4) + 
               12*Power(t2,5) + 
               6*Power(s2,3)*(1 - 3*t2 + 3*Power(t2,2)) - 
               Power(s2,2)*(39 + (286 - 6*s)*t2 + 
                  (-83 + 32*s)*Power(t2,2) + 118*Power(t2,3)) + 
               s2*(-5 + 3*(107 + 8*s)*t2 + 
                  2*(-429 - 170*s + 6*Power(s,2))*Power(t2,2) + 
                  8*(-71 + 13*s)*Power(t2,3) - 64*Power(t2,4))) + 
            Power(t1,3)*t2*(-13 - 129*t2 + (569 + 157*s)*Power(t2,2) + 
               (2187 + 865*s - 24*Power(s,2))*Power(t2,3) + 
               (2257 - 392*s)*Power(t2,4) - 167*Power(t2,5) - 
               3*Power(s2,3)*t2*(5 + 11*t2) + 
               s2*(7 + (257 + 10*s)*t2 + 
                  (1489 + 362*s - 12*Power(s,2))*Power(t2,2) + 
                  (785 - 304*s)*Power(t2,3) + 174*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-17 - 438*t2 + 255*Power(t2,2) + s*(-1 + 15*t2)))) - 
         Power(s1,5)*(24*Power(t1,9)*t2 + 
            3*Power(t1,8)*(10 + (-109 - 20*s + 20*s2)*t2 + 
               57*Power(t2,2)) - 
            Power(t1,7)*(-40 + (460 + 72*s - 167*s2)*t2 + 
               (322 + 268*s + 45*s2)*Power(t2,2) + 756*Power(t2,3)) + 
            Power(t1,6)*(-24 - 2*(155 + 10*s - 52*s2)*t2 + 
               (1677 + 56*Power(s,2) + 622*s2 - 288*Power(s2,2) + 
                  s*(349 + 312*s2))*Power(t2,2) + 
               (348 + 1265*s - 204*s2)*Power(t2,3) + 1143*Power(t2,4)) + 
            Power(t1,5)*(-10 + (53 + 12*s - 82*s2)*t2 + 
               (2112 - 2*Power(s,3) + Power(s,2)*(23 - 18*s2) - 
                  44*s2 - 527*Power(s2,2) + 12*Power(s2,3) + 
                  s*(342 + 152*s2 - 4*Power(s2,2)))*Power(t2,2) + 
               (2033 - 199*Power(s,2) + s*(770 - 664*s2) - 680*s2 + 
                  443*Power(s2,2))*Power(t2,3) + 
               (2585 - 1766*s + 610*s2)*Power(t2,4) - 773*Power(t2,5)) + 
            Power(t1,4)*(2 + (99 + 11*s - 35*s2)*t2 + 
               (477 - 627*s2 + 85*Power(s2,2) + 27*Power(s2,3) - 
                  Power(s,2)*(2 + 7*s2) + 
                  s*(40 - 82*s2 + 17*Power(s2,2)))*Power(t2,2) + 
               (-2264 + 4*Power(s,3) - 3596*s2 + 1206*Power(s2,2) + 
                  93*Power(s2,3) + Power(s,2)*(-97 + 69*s2) - 
                  3*s*(265 + 334*s2 + 29*Power(s2,2)))*Power(t2,3) + 
               (-4998 + 255*Power(s,2) - 628*s2 - 429*Power(s2,2) + 
                  6*s*(-491 + 74*s2))*Power(t2,4) + 
               (-4745 + 1146*s - 732*s2)*Power(t2,5) + 213*Power(t2,6)) \
+ Power(t2,4)*(1 + (35 + 3*s)*t2 - (131 + 48*s)*Power(t2,2) + 
               (-131 + 41*s + 4*Power(s,2))*Power(t2,3) + 
               38*Power(t2,4) + 
               Power(s2,3)*(8 + 15*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(-9 + (26 + 17*s)*t2 + 
                  17*(-1 + s)*Power(t2,2) + 16*Power(t2,3)) - 
               s2*(37 + (94 + 6*s)*t2 + 
                  (13 - 22*s + 10*Power(s,2))*Power(t2,2) + 
                  (-6 + 20*s)*Power(t2,3))) + 
            t1*Power(t2,3)*(21 + 269*t2 + 
               2*(116 + 8*s + Power(s,2))*Power(t2,2) + 
               (-847 - 534*s + 16*Power(s,2) + 2*Power(s,3))*
                Power(t2,3) + 
               (-929 + 138*s + 6*Power(s,2))*Power(t2,4) + 
               24*Power(t2,5) + 
               Power(s2,3)*(22 + 22*t2 + 45*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(s2,2)*(-33 - (223 + 44*s)*t2 - 
                  2*(26 + 49*s)*Power(t2,2) + 
                  24*(-9 + s)*Power(t2,3) + 6*Power(t2,4)) - 
               s2*(-20 + (-318 + 56*s)*t2 + 
                  (1154 + 288*s - 37*Power(s,2))*Power(t2,2) + 
                  (654 - 140*s + 15*Power(s,2))*Power(t2,3) + 
                  4*(32 + 3*s)*Power(t2,4))) + 
            Power(t1,2)*Power(t2,2)*
             (-11 + (-86 + 22*s)*t2 + 
               (436 + 96*s - 6*Power(s,2))*Power(t2,2) + 
               (3153 + 1118*s - 95*Power(s,2) - 4*Power(s,3))*
                Power(t2,3) + 
               (2325 - 1035*s + 13*Power(s,2))*Power(t2,4) + 
               (-563 + 45*s)*Power(t2,5) + 
               Power(s2,3)*t2*(-143 - 175*t2 + 60*Power(t2,2)) - 
               Power(s2,2)*(18 + (150 - 33*s)*t2 + 
                  (878 - 114*s)*Power(t2,2) + 
                  (-503 + 119*s)*Power(t2,3) + 41*Power(t2,4)) + 
               s2*(-1 + (703 + 90*s)*t2 + 
                  (2724 + 492*s - 51*Power(s,2))*Power(t2,2) + 
                  (350 - 678*s + 63*Power(s,2))*Power(t2,3) + 
                  7*(31 + 4*s)*Power(t2,4) - 45*Power(t2,5))) - 
            Power(t1,3)*t2*(1 + (271 + 36*s)*t2 + 
               (2051 + 116*s - 6*Power(s,2))*Power(t2,2) + 
               (1909 + 152*s - 149*Power(s,2))*Power(t2,3) + 
               (-1004 - 2796*s + 131*Power(s,2))*Power(t2,4) + 
               (-2850 + 362*s)*Power(t2,5) + 16*Power(t2,6) + 
               2*Power(s2,3)*t2*(10 - 7*t2 + 47*Power(t2,2)) - 
               s2*(6 + (80 - 28*s)*t2 + 
                  (-932 - 144*s + 31*Power(s,2))*Power(t2,2) + 
                  (3078 + 1408*s - 99*Power(s,2))*Power(t2,3) + 
                  (724 - 108*s)*Power(t2,4) + 338*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-185 - 1144*t2 + 682*Power(t2,2) - 225*Power(t2,3) + 
                  s*(6 + 50*t2 - 186*Power(t2,2))))) - 
         Power(s1,4)*(4*Power(t1,10)*t2 + 
            4*Power(t1,9)*(3 - 6*(8 + s - s2)*t2 + 20*Power(t2,2)) - 
            Power(t1,8)*(-30 + (286 + 43*s - 104*s2)*t2 + 
               (66 + 247*s - 64*s2)*Power(t2,2) + 328*Power(t2,3)) + 
            Power(t1,7)*(-16 + (-219 - 28*s + 99*s2)*t2 + 
               (1517 + 104*Power(s,2) + 528*s2 - 212*Power(s2,2) + 
                  3*s*(63 + 76*s2))*Power(t2,2) + 
               (-98 + 1123*s - 421*s2)*Power(t2,3) + 498*Power(t2,4)) + 
            Power(t1,6)*(-18 + (11 + 6*s - 92*s2)*t2 + 
               (2077 - 10*Power(s,3) + Power(s,2)*(71 - 32*s2) - 
                  329*s2 - 469*Power(s2,2) + 8*Power(s2,3) + 
                  s*(586 + 86*s2 + 4*Power(s2,2)))*Power(t2,2) + 
               (1535 - 441*Power(s,2) + s*(1369 - 394*s2) - 1127*s2 + 
                  325*Power(s2,2))*Power(t2,3) + 
               (2520 - 1761*s + 874*s2)*Power(t2,4) - 335*Power(t2,5)) + 
            Power(t1,5)*(6 + (175 + 37*s - 67*s2)*t2 + 
               (822 - 5*Power(s,3) - 909*s2 + 133*Power(s2,2) + 
                  21*Power(s2,3) - Power(s,2)*(61 + 11*s2) + 
                  s*(277 - 150*s2 + 45*Power(s2,2)))*Power(t2,2) + 
               (-3384 + 33*Power(s,3) - 4364*s2 + 1556*Power(s2,2) + 
                  131*Power(s2,3) + Power(s,2)*(-396 + 125*s2) - 
                  s*(1602 + 1204*s2 + 169*Power(s2,2)))*Power(t2,3) + 
               (-5071 + 725*Power(s,2) + 1000*s2 - 205*Power(s2,2) - 
                  18*s*(284 + 5*s2))*Power(t2,4) + 
               (-4759 + 1346*s - 942*s2)*Power(t2,5) + 77*Power(t2,6)) + 
            Power(t1,3)*t2*(-6 + 
               (-180 + 35*s2 - 84*Power(s2,2) + s*(13 + 24*s2))*t2 - 
               (332 - 2127*s2 + 636*Power(s2,2) + 337*Power(s2,3) + 
                  Power(s,2)*(79 + 29*s2) + 
                  s*(91 - 620*s2 - 135*Power(s2,2)))*Power(t2,2) + 
               (2953 - 8*Power(s,3) + Power(s,2)*(94 - 255*s2) + 
                  8206*s2 - 2777*Power(s2,2) - 417*Power(s2,3) + 
                  s*(1460 + 1664*s2 + 371*Power(s2,2)))*Power(t2,3) + 
               (9931 + 4*Power(s,3) + 122*s2 + 1316*Power(s2,2) + 
                  156*Power(s2,3) + 2*Power(s,2)*(-415 + 82*s2) - 
                  2*s*(-1840 + 775*s2 + 162*Power(s2,2)))*Power(t2,4) + 
               (5787 + 207*Power(s,2) + 1847*s2 + 103*Power(s2,2) - 
                  s*(3649 + 310*s2))*Power(t2,5) + 
               5*(-201 + 17*s - 17*s2)*Power(t2,6)) + 
            Power(t2,4)*(-1 - 5*(14 + s)*t2 + 
               (-68 + 9*s + 4*Power(s,2))*Power(t2,2) + 
               (396 + 183*s - 16*Power(s,2) - 4*Power(s,3))*
                Power(t2,3) + (315 - 69*s - 6*Power(s,2))*Power(t2,4) - 
               12*Power(t2,5) + 
               Power(s2,3)*(-12 - 42*t2 - 35*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s2,2)*(24 - 2*(-35 + s)*t2 + 
                  (68 + 48*s)*Power(t2,2) + (98 - 18*s)*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s2*(4 + 4*(-23 + s)*t2 + 
                  (434 + 64*s + 11*Power(s,2))*Power(t2,2) + 
                  (236 - 36*s + 15*Power(s,2))*Power(t2,3) + 
                  4*(16 + 3*s)*Power(t2,4))) + 
            t1*Power(t2,3)*(-6 + 3*(10 + s)*t2 - 
               (131 + 46*s + 37*Power(s,2))*Power(t2,2) + 
               (-1813 - 537*s + 135*Power(s,2) + 13*Power(s,3))*
                Power(t2,3) - 
               (643 - 906*s + 54*Power(s,2) + 5*Power(s,3))*
                Power(t2,4) + (625 - 90*s)*Power(t2,5) + 
               Power(s2,3)*t2*
                (212 + 212*t2 - 93*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(6 + (61 + 8*s)*t2 + 
                  (765 + 7*s)*Power(t2,2) + 
                  2*(-74 + 97*s)*Power(t2,3) - 
                  5*(-20 + 3*s)*Power(t2,4)) + 
               s2*(-22 + (-793 + 28*s)*t2 - 
                  (1952 + 232*s + 39*Power(s,2))*Power(t2,2) + 
                  (539 + 442*s - 114*Power(s,2))*Power(t2,3) + 
                  (76 - 46*s + 15*Power(s,2))*Power(t2,4) + 
                  90*Power(t2,5))) + 
            Power(t1,4)*t2*(25 - 602*t2 - 4489*Power(t2,2) + 
               2*Power(s,3)*(7 - 17*t2)*Power(t2,2) - 
               4262*Power(t2,3) + 210*Power(t2,4) + 3500*Power(t2,5) + 
               4*Power(t2,6) - 
               2*Power(s2,3)*t2*(11 + 4*t2 + 95*Power(t2,2)) + 
               Power(s2,2)*t2*
                (361 + 2136*t2 - 1502*Power(t2,2) - 21*Power(t2,3)) + 
               2*s2*(12 + 127*t2 - 429*Power(t2,2) + 
                  2528*Power(t2,3) - 682*Power(t2,4) + 240*Power(t2,5)) \
- s*(6 + 2*(56 + 77*s2 + 12*Power(s2,2))*t2 + 
                  (1079 + 358*s2 + 130*Power(s2,2))*Power(t2,2) + 
                  (211 - 2334*s2 - 392*Power(s2,2))*Power(t2,3) - 
                  2*(3186 + 257*s2)*Power(t2,4) + 522*Power(t2,5)) + 
               Power(s,2)*t2*
                (25 + 96*t2 + 834*Power(t2,2) - 569*Power(t2,3) + 
                  s2*(6 + 102*t2 - 198*Power(t2,2)))) + 
            Power(t1,2)*Power(t2,2)*
             (13 + (381 - 5*s)*t2 + 
               (1828 + 203*s + 87*Power(s,2))*Power(t2,2) - 
               2*(-786 + 155*s + 124*Power(s,2) + 5*Power(s,3))*
                Power(t2,3) + 
               (-4727 - 3262*s + 381*Power(s,2) + 12*Power(s,3))*
                Power(t2,4) + 
               (-3999 + 964*s - 26*Power(s,2))*Power(t2,5) + 
               76*Power(t2,6) + 
               2*Power(s2,3)*t2*
                (68 + 82*t2 + 107*Power(t2,2) - 25*Power(t2,3)) + 
               s2*(6 + (94 - 56*s)*t2 + 
                  (956 - 298*s + 51*Power(s,2))*Power(t2,2) + 
                  (-6054 - 1562*s + 263*Power(s,2))*Power(t2,3) - 
                  2*(962 - 184*s + 37*Power(s,2))*Power(t2,4) + 
                  (-880 + 52*s)*Power(t2,5)) - 
               Power(s2,2)*t2*
                (413 + 1350*t2 - 450*Power(t2,2) + 743*Power(t2,3) + 
                  26*Power(t2,4) + 
                  s*(6 + 166*t2 + 462*Power(t2,2) - 112*Power(t2,3))))) + 
         Power(s1,3)*(Power(t1,10)*
             (-2 + (61 + 4*s - 4*s2)*t2 - 15*Power(t2,2)) + 
            Power(t1,9)*(-12 + 3*(33 + 4*s - 11*s2)*t2 + 
               (-21 + 114*s - 53*s2)*Power(t2,2) + 56*Power(t2,3)) - 
            Power(t1,8)*(-4 + (-76 - 19*s + 53*s2)*t2 + 
               (763 + 96*Power(s,2) + 249*s2 - 80*Power(s2,2) + 
                  s*(17 + 84*s2))*Power(t2,2) + 
               (-151 + 494*s - 234*s2)*Power(t2,3) + 76*Power(t2,4)) + 
            Power(t1,7)*(14 + (24 + 4*s + 49*s2)*t2 + 
               (-1127 + 20*Power(s,3) + 287*s2 + 212*Power(s2,2) - 
                  2*Power(s2,3) + Power(s,2)*(-81 + 28*s2) - 
                  2*s*(222 - 7*s2 + 3*Power(s2,2)))*Power(t2,2) + 
               (-405 + 449*Power(s,2) + 898*s2 - 86*Power(s2,2) + 
                  2*s*(-592 + 5*s2))*Power(t2,3) + 
               2*(-554 + 397*s - 212*s2)*Power(t2,4) + 36*Power(t2,5)) + 
            Power(t1,6)*(-6 + (-105 - 41*s + 57*s2)*t2 + 
               (-589 + 13*Power(s,3) + Power(s,2)*(155 - 9*s2) + 
                  820*s2 - 79*Power(s2,2) - 6*Power(s2,3) + 
                  s*(-525 + 106*s2 - 43*Power(s2,2)))*Power(t2,2) + 
               (2562 - 85*Power(s,3) + Power(s,2)*(610 - 75*s2) + 
                  2884*s2 - 977*Power(s2,2) - 90*Power(s2,3) + 
                  s*(1365 + 656*s2 + 135*Power(s2,2)))*Power(t2,3) + 
               (2011 - 845*Power(s,2) - 1863*s2 - 110*Power(s2,2) + 
                  s*(4442 + 594*s2))*Power(t2,4) + 
               (2002 - 617*s + 414*s2)*Power(t2,5) + 7*Power(t2,6)) - 
            Power(t1,4)*t2*(-36 - 
               (502 + 18*Power(s,2) + s*(29 - 108*s2) - 93*s2 + 
                  150*Power(s2,2))*t2 + 
               (-1057 + 9*Power(s,3) + Power(s,2)*(62 - 81*s2) + 
                  2485*s2 - 982*Power(s2,2) - 341*Power(s2,3) + 
                  s*(-293 + 1168*s2 + 137*Power(s2,2)))*Power(t2,2) + 
               (4261 - 95*Power(s,3) + Power(s,2)*(169 - 435*s2) + 
                  9766*s2 - 3719*Power(s2,2) - 499*Power(s2,3) + 
                  s*(4411 + 2398*s2 + 571*Power(s2,2)))*Power(t2,3) + 
               (86*Power(s,3) + 4*Power(s,2)*(-557 + 7*s2) + 
                  s*(6397 - 504*s2 - 260*Power(s2,2)) + 
                  2*(6235 - 781*s2 + 526*Power(s2,2) + 73*Power(s2,3))\
)*Power(t2,4) + (6821 + 383*Power(s,2) + 3207*s2 + 275*Power(s2,2) - 
                  7*s*(695 + 94*s2))*Power(t2,5) + 
               (-409 + 35*s - 35*s2)*Power(t2,6)) + 
            Power(t2,5)*(11 - 16*t2 - 340*Power(t2,2) + 
               116*Power(t2,3) + 229*Power(t2,4) - 
               Power(s,3)*Power(t2,2)*(5 + 3*t2) + 
               Power(s2,3)*(64 + 30*t2 - 49*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,2)*(-72 + 221*t2 + 88*Power(t2,2) + 
                  59*Power(t2,3)) + 
               s2*(-284 - 477*t2 + 373*Power(t2,2) + 119*Power(t2,3) + 
                  45*Power(t2,4)) - 
               s*(6 + (25 + 54*s2 - 124*Power(s2,2))*t2 + 
                  (43 - 80*s2 - 117*Power(s2,2))*Power(t2,2) + 
                  (-263 + 18*s2 + 9*Power(s2,2))*Power(t2,3) + 
                  45*Power(t2,4)) + 
               Power(s,2)*t2*
                (-7 + 64*t2 - 41*Power(t2,2) + 
                  s2*(6 - 63*t2 + 9*Power(t2,2)))) + 
            t1*Power(t2,4)*(167 + 833*t2 + 683*Power(t2,2) - 
               2*Power(s,3)*(-21 + t2)*Power(t2,2) - 4063*Power(t2,3) - 
               2364*Power(t2,4) + 104*Power(t2,5) + 
               Power(s2,3)*(224 + 422*t2 + 188*Power(t2,2) - 
                  82*Power(t2,3)) - 
               Power(s2,2)*(360 + 657*t2 - 93*Power(t2,2) + 
                  702*Power(t2,3) + 38*Power(t2,4)) - 
               2*s2*(-54 - 50*t2 + 2235*Power(t2,2) + 
                  828*Power(t2,3) + 373*Power(t2,4)) + 
               2*s*(9 - 7*(-15 + 31*s2 + 22*Power(s2,2))*t2 - 
                  (256 + 395*s2 + 203*Power(s2,2))*Power(t2,2) + 
                  (-767 + 146*s2 + 81*Power(s2,2))*Power(t2,3) + 
                  (421 + 38*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (3 - 55*t2 + 322*Power(t2,2) - 38*Power(t2,3) - 
                  6*s2*(3 - 24*t2 + 13*Power(t2,2)))) + 
            Power(t1,5)*t2*(-81 + 408*t2 + 4228*Power(t2,2) + 
               4212*Power(t2,3) + 1673*Power(t2,4) - 1494*Power(t2,5) - 
               8*Power(t2,6) + 
               6*Power(s,3)*t2*(-1 - 9*t2 + 22*Power(t2,2)) + 
               Power(s2,3)*t2*(8 + 11*t2 + 161*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-335 - 2000*t2 + 1232*Power(t2,2) + 323*Power(t2,3)) + 
               2*s*(6 + (119 + 111*s2 + 15*Power(s2,2))*t2 + 
                  (1259 + 214*s2 + 63*Power(s2,2))*Power(t2,2) + 
                  (339 - 726*s2 - 157*Power(s2,2))*Power(t2,3) - 
                  (3288 + 515*s2)*Power(t2,4) + 117*Power(t2,5)) - 
               s2*(30 + 420*t2 + 313*Power(t2,2) + 4537*Power(t2,3) - 
                  3048*Power(t2,4) + 202*Power(t2,5)) + 
               Power(s,2)*t2*
                (-25 - 320*t2 - 1680*Power(t2,2) + 801*Power(t2,3) + 
                  2*s2*(-6 - 55*t2 + 33*Power(t2,2)))) + 
            Power(t1,2)*Power(t2,3)*
             (-67 - 40*t2 + 2560*Power(t2,2) + 8167*Power(t2,3) + 
               1351*Power(t2,4) - 1739*Power(t2,5) + 
               3*Power(s,3)*Power(t2,2)*(-30 + 13*t2 + Power(t2,2)) - 
               Power(s2,3)*t2*
                (742 + 628*t2 - 341*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(-168 - 793*t2 - 3374*Power(t2,2) + 
                  1017*Power(t2,3) + 86*Power(t2,4)) + 
               s2*(116 + 3398*t2 + 8427*Power(t2,2) - 
                  2017*Power(t2,3) + 1042*Power(t2,4) - 290*Power(t2,5)\
) + s*(-18 + 2*(-50 + 277*s2 + 98*Power(s2,2))*t2 + 
                  (1423 + 1240*s2 + 280*Power(s2,2))*Power(t2,2) + 
                  (1425 - 1818*s2 - 697*Power(s2,2))*Power(t2,3) + 
                  (-4180 - 536*s2 + 9*Power(s2,2))*Power(t2,4) + 
                  290*Power(t2,5)) + 
               Power(s,2)*t2*
                (33 - 194*t2 - 865*Power(t2,2) + 450*Power(t2,3) + 
                  s2*(18 - 42*t2 + 317*Power(t2,2) - 9*Power(t2,3)))) + 
            Power(t1,3)*Power(t2,2)*
             (-51 - 1264*t2 - 5141*Power(t2,2) - 4022*Power(t2,3) + 
               7813*Power(t2,4) + 5813*Power(t2,5) + 
               4*Power(s,3)*Power(t2,2)*(17 - 22*t2 + 4*Power(t2,2)) + 
               2*Power(s2,3)*t2*
                (-101 - 164*t2 - 242*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,2)*t2*
                (935 + 3110*t2 - 1978*Power(t2,2) + 471*Power(t2,3) + 
                  74*Power(t2,4)) + 
               2*s2*(6 - 45*t2 - 585*Power(t2,2) + 5401*Power(t2,3) + 
                  31*Power(t2,4) + 830*Power(t2,5)) - 
               2*s*(-3 + 3*(21 - 7*s2 + 2*Power(s2,2))*t2 + 
                  (679 - 208*s2 - 58*Power(s2,2))*Power(t2,2) - 
                  2*(565 + 852*s2 + 258*Power(s2,2))*Power(t2,3) + 
                  (-4081 - 369*s2 + 42*Power(s2,2))*Power(t2,4) + 
                  (916 + 74*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(-47 + 272*t2 + 918*Power(t2,2) - 
                  1489*Power(t2,3) + 74*Power(t2,4) + 
                  6*s2*(-1 - 18*t2 - 94*Power(t2,2) + 3*Power(t2,3))))) + 
         Power(t2,2)*(2*Power(-1 + s,3)*Power(t1,10) + 
            Power(t1,5)*(-6 + 
               (-7 + 78*Power(s,2) + 16*s2 + 4*s*(-29 + 3*s2))*t2 + 
               (-132 - 126*Power(s,3) + Power(s,2)*(419 - 40*s2) + 
                  99*s2 + 6*Power(s2,2) + 
                  s*(-205 + 170*s2 - 22*Power(s2,2)))*Power(t2,2) + 
               (3 + 32*Power(s,3) + 155*s2 - 197*Power(s2,2) - 
                  20*Power(s2,3) - 3*Power(s,2)*(169 + 87*s2) + 
                  s*(1031 + 672*s2 + 101*Power(s2,2)))*Power(t2,3) + 
               (152 + 136*Power(s,3) - 408*s2 - 123*Power(s2,2) - 
                  6*Power(s2,3) - Power(s,2)*(594 + 223*s2) + 
                  s*(792 + 484*s2 + 93*Power(s2,2)))*Power(t2,4) + 
               2*(40 - Power(s,3) + 16*s2 + 5*Power(s2,2) + 
                  Power(s2,3) + 3*Power(s,2)*(4 + s2) - 
                  s*(37 + 17*s2 + 3*Power(s2,2)))*Power(t2,5)) + 
            Power(t1,3)*t2*(48 - 
               2*(-187 + 24*Power(s,2) - 74*s2 - 48*Power(s2,2) + 
                  s*(62 + 96*s2))*t2 + 
               (759 + 84*Power(s,3) - 6*s2 + 442*Power(s2,2) + 
                  132*Power(s2,3) + 2*Power(s,2)*(5 + 174*s2) - 
                  4*s*(212 + 307*s2 + 33*Power(s2,2)))*Power(t2,2) - 
               (216 + 302*Power(s,3) + 817*s2 - 545*Power(s2,2) - 
                  88*Power(s2,3) - Power(s,2)*(1877 + 412*s2) + 
                  s*(2737 + 998*s2 + 54*Power(s2,2)))*Power(t2,3) + 
               (-1560 - 141*Power(s,3) - 43*s2 - 294*Power(s2,2) - 
                  109*Power(s2,3) + 21*Power(s,2)*(18 + s2) + 
                  s*(329 + 436*s2 + 229*Power(s2,2)))*Power(t2,4) + 
               (-478 + 15*Power(s,3) - 589*s2 - 157*Power(s2,2) - 
                  15*Power(s2,3) - 3*Power(s,2)*(83 + 15*s2) + 
                  s*(847 + 406*s2 + 45*Power(s2,2)))*Power(t2,5) + 
               (1 + 5*s - 5*s2)*Power(t2,6)) - 
            2*Power(t1,9)*(-4 - (7 + 3*s2)*t2 + Power(s,3)*(1 + 5*t2) + 
               s*(9 + 2*(7 + 3*s2)*t2) - Power(s,2)*(6 + (16 + 3*s2)*t2)\
) + Power(t1,8)*(-10 - (1 + 24*s2)*t2 - 
               (19 + 28*s2 + 6*Power(s2,2))*Power(t2,2) + 
               4*Power(s,3)*(-1 - t2 + 5*Power(t2,2)) - 
               2*Power(s,2)*(-3 + (8 + 7*s2)*t2 + 
                  12*(3 + s2)*Power(t2,2)) + 
               s*(6 + (43 + 34*s2)*t2 + 
                  (77 + 46*s2 + 6*Power(s2,2))*Power(t2,2))) + 
            Power(t1,7)*(-4 + (-19 + 8*s2)*t2 + 
               (-50 - 31*s2 + 28*Power(s2,2))*Power(t2,2) + 
               (17 + 63*s2 + 14*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 
               Power(s,3)*t2*(29 + 51*t2 - 20*Power(t2,2)) + 
               Power(s,2)*(-12 + (-123 + 4*s2)*t2 + 
                  (-105 + 8*s2)*Power(t2,2) + 4*(22 + 9*s2)*Power(t2,3)\
) - s*(-18 + 24*(-5 + s2)*t2 + 
                  (-11 + 54*s2 + 16*Power(s2,2))*Power(t2,2) + 
                  (143 + 74*s2 + 18*Power(s2,2))*Power(t2,3))) + 
            16*Power(t2,4)*(4 + (-2 + 2*s + 3*Power(s,2))*t2 + 
               (-5 - 12*s + 11*Power(s,2))*Power(t2,2) - 
               (12 - 19*s + Power(s,2))*Power(t2,3) + 
               (13 + 9*s - 3*Power(s,2))*Power(t2,4) + 2*Power(t2,5) + 
               Power(s2,3)*(12 + 18*t2 - 4*Power(t2,3)) + 
               Power(s2,2)*(-24 + (-13 + 2*s)*t2 + 
                  3*(7 + 2*s)*Power(t2,2) + (-7 + 8*s)*Power(t2,3) - 
                  3*Power(t2,4)) + 
               s2*(-4 - 4*(13 + 4*s)*t2 + 
                  (-62 + 8*s - 6*Power(s,2))*Power(t2,2) + 
                  (13 + 10*s - 4*Power(s,2))*Power(t2,3) + 
                  (-5 + 6*s)*Power(t2,4))) - 
            4*t1*Power(t2,3)*
             (12 + (-110 - 27*s + 36*Power(s,2))*t2 - 
               (216 + 97*s - 136*Power(s,2) + 7*Power(s,3))*
                Power(t2,2) + 
               (-158 + 351*s - 86*Power(s,2) + 11*Power(s,3))*
                Power(t2,3) + 
               (220 + 119*s - 54*Power(s,2) + 4*Power(s,3))*
                Power(t2,4) + (52 - 10*s)*Power(t2,5) + 
               Power(s2,3)*t2*
                (48 + 17*t2 - 29*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(24 + 16*(1 + 2*s)*t2 + 
                  (124 + 67*s)*Power(t2,2) + 
                  (-58 + 97*s)*Power(t2,3) + 6*(-3 + 2*s)*Power(t2,4)) \
+ s2*(-88 - (355 + 32*s)*t2 + 
                  (-441 + 220*s - 93*Power(s,2))*Power(t2,2) + 
                  (191 + 180*s - 79*Power(s,2))*Power(t2,3) + 
                  (-29 + 72*s - 12*Power(s,2))*Power(t2,4) + 
                  10*Power(t2,5))) - 
            Power(t1,4)*t2*(94 + 150*t2 - 391*Power(t2,2) - 
               665*Power(t2,3) - 79*Power(t2,4) + 43*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(14 - 79*t2 - 25*Power(t2,2)) + 
               2*Power(s,3)*t2*
                (14 - 133*t2 - 43*Power(t2,2) + 36*Power(t2,3)) + 
               Power(s2,2)*t2*
                (146 + 317*t2 - 371*Power(t2,2) - 202*Power(t2,3) + 
                  4*Power(t2,4)) + 
               2*s2*(6 + 89*t2 + 76*Power(t2,2) + 174*Power(t2,3) - 
                  324*Power(t2,4) - 2*Power(t2,5)) + 
               s*(-84 - 4*(164 + 59*s2 + 3*Power(s2,2))*t2 + 
                  (-1435 - 70*s2 + 4*Power(s2,2))*Power(t2,2) + 
                  (1033 + 898*s2 + 322*Power(s2,2))*Power(t2,3) + 
                  (1129 + 636*s2 + 122*Power(s2,2))*Power(t2,4) - 
                  (5 + 8*s2)*Power(t2,5)) + 
               Power(s,2)*t2*
                (218 + 1355*t2 - 87*Power(t2,2) - 516*Power(t2,3) + 
                  4*Power(t2,4) + 
                  s2*(84 + 76*t2 - 245*Power(t2,2) - 169*Power(t2,3)))) \
+ Power(t1,6)*(14 + (61 + 6*s2)*t2 + 
               (-27 + 100*s2 + 22*Power(s2,2))*Power(t2,2) - 
               2*(10 - 94*s2 + Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) - 
               2*(26 + 36*s2 + 7*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,4) + 
               2*Power(s,3)*t2*
                (13 - 33*t2 - 62*Power(t2,2) + 5*Power(t2,3)) + 
               s*(-12 - (103 + 10*s2)*t2 + 
                  (-545 - 158*s2 + 16*Power(s2,2))*Power(t2,2) - 
                  2*(139 + 65*s2)*Power(t2,3) + 
                  2*(76 + 33*s2 + 9*Power(s2,2))*Power(t2,4)) + 
               Power(s,2)*t2*
                (-13 + 417*t2 + 384*Power(t2,2) - 62*Power(t2,3) + 
                  3*s2*(4 + 25*t2 + 35*Power(t2,2) - 8*Power(t2,3)))) + 
            2*Power(t1,2)*Power(t2,2)*
             (-32 + (-341 - 50*s + 72*Power(s,2))*t2 + 
               (-700 + 62*s + 249*Power(s,2) - 42*Power(s,3))*
                Power(t2,2) + 
               2*(-135 + 673*s - 311*Power(s,2) + 45*Power(s,3))*
                Power(t2,3) + 
               (810 + 212*s - 213*Power(s,2) + 40*Power(s,3))*
                Power(t2,4) + 
               (243 - 154*s + 26*Power(s,2))*Power(t2,5) + 
               2*Power(t2,6) + 
               2*Power(s2,3)*t2*
                (-24 - 49*t2 - 18*Power(t2,2) + 7*Power(t2,3)) - 
               2*s2*(24 + (157 - 80*s)*t2 + 
                  (172 - 433*s + 135*Power(s,2))*Power(t2,2) + 
                  2*(-179 - 166*s + 70*Power(s,2))*Power(t2,3) + 
                  (-23 - 41*s + 33*Power(s,2))*Power(t2,4) + 
                  (-64 + 26*s)*Power(t2,5)) + 
               Power(s2,2)*t2*
                (64 + 85*t2 - 234*Power(t2,2) + 43*Power(t2,3) + 
                  26*Power(t2,4) + 
                  2*s*(24 + 73*t2 + 85*Power(t2,2) + 6*Power(t2,3))))) + 
         Power(s1,2)*(8*Power(t1,11)*t2 + 
            Power(t1,10)*(-2 + (15 + s - 4*s2)*t2 + 
               3*(-3 + 7*s - 4*s2)*Power(t2,2)) + 
            Power(t1,8)*(4 + (18 + 3*s + 10*s2)*t2 + 
               (-311 + 20*Power(s,3) + 81*s2 + 38*Power(s2,2) + 
                  Power(s,2)*(-47 + 12*s2) + 
                  s*(-135 + 22*s2 - 2*Power(s2,2)))*Power(t2,2) + 
               (51 + 211*Power(s,2) + 307*s2 + 2*Power(s2,2) - 
                  2*s*(247 + 31*s2))*Power(t2,3) + 
               2*(-80 + 59*s - 26*s2)*Power(t2,4)) + 
            Power(t1,7)*(-2 + (-16 - 15*s + 18*s2)*t2 + 
               (-129 + 9*Power(s,3) + Power(s,2)*(147 - 23*s2) + 
                  350*s2 - 14*Power(s2,2) + 
                  s*(-365 + 40*s2 - 14*Power(s2,2)))*Power(t2,2) + 
               (1045 - 97*Power(s,3) + 979*s2 - 262*Power(s2,2) - 
                  24*Power(s2,3) + Power(s,2)*(452 + 15*s2) + 
                  3*s*(127 + 42*s2 + 12*Power(s2,2)))*Power(t2,3) + 
               (90 - 415*Power(s,2) - 895*s2 - 92*Power(s2,2) + 
                  s*(1747 + 350*s2))*Power(t2,4) + 
               (232 - 66*s + 24*s2)*Power(t2,5)) + 
            Power(t1,3)*Power(t2,2)*
             (-42 - (547 + 6*Power(s,2) + s*(64 - 180*s2) - 262*s2 + 
                  402*Power(s2,2))*t2 - 
               (246 + 54*Power(s,3) - 3892*s2 + 1797*Power(s2,2) + 
                  816*Power(s2,3) + Power(s,2)*(95 + 68*s2) + 
                  s*(32 - 2074*s2 - 386*Power(s2,2)))*Power(t2,2) + 
               (4210 - 88*Power(s,3) + 10937*s2 - 4590*Power(s2,2) - 
                  688*Power(s2,3) - 2*Power(s,2)*(389 + 245*s2) + 
                  s*(5615 + 2184*s2 + 634*Power(s2,2)))*Power(t2,3) + 
               (12639 + 217*Power(s,3) - 1677*s2 + 1739*Power(s2,2) + 
                  413*Power(s2,3) + Power(s,2)*(-2467 + 169*s2) + 
                  s*(1311 - 1682*s2 - 799*Power(s2,2)))*Power(t2,4) + 
               (3057 - 11*Power(s,3) + 3716*s2 + 666*Power(s2,2) + 
                  11*Power(s2,3) + 3*Power(s,2)*(342 + 11*s2) - 
                  3*s*(2060 + 564*s2 + 11*Power(s2,2)))*Power(t2,5) + 
               5*(-159 + 22*s - 22*s2)*Power(t2,6)) + 
            Power(t1,5)*t2*(34 + 
               (376 - 12*Power(s,2) + s*(111 - 72*s2) - 89*s2 + 
                  84*Power(s2,2))*t2 + 
               (1268 + 7*Power(s,3) - 1558*s2 + 541*Power(s2,2) + 
                  126*Power(s2,3) + 15*Power(s,2)*(-36 + 5*s2) + 
                  s*(797 - 764*s2 + 19*Power(s2,2)))*Power(t2,2) + 
               (-2043 + 221*Power(s,3) - 4598*s2 + 2076*Power(s2,2) + 
                  279*Power(s2,3) + Power(s,2)*(-149 + 289*s2) - 
                  s*(4835 + 1492*s2 + 392*Power(s2,2)))*Power(t2,3) - 
               (5148 + 164*Power(s,3) - 2569*s2 - 94*Power(s2,2) + 
                  27*Power(s2,3) - 6*Power(s,2)*(407 + 40*s2) + 
                  s*(5439 + 1294*s2 + 49*Power(s2,2)))*Power(t2,4) - 
               (2988 + 217*Power(s,2) + 1405*s2 + 157*Power(s2,2) - 
                  s*(2069 + 374*s2))*Power(t2,5) + 
               (-13 + 5*s - 5*s2)*Power(t2,6)) + 
            Power(t1,9)*t2*(10 - 191*t2 - 44*Power(s,2)*t2 + 
               12*Power(s2,2)*t2 + 37*Power(t2,2) + 
               s*(5 - 4*(-7 + 3*s2)*t2 - 83*Power(t2,2)) + 
               s2*(-12 - 49*t2 + 41*Power(t2,2))) - 
            2*Power(t2,5)*(7 + (82 + 26*s - 3*Power(s,2))*t2 + 
               (64 - 94*s + 32*Power(s,2) + 6*Power(s,3))*Power(t2,2) + 
               (-540 - 108*s + 33*Power(s,2) + 2*Power(s,3))*
                Power(t2,3) - 
               3*(81 - 40*s + 2*Power(s,2))*Power(t2,4) + 
               22*Power(t2,5) + 
               8*Power(s2,3)*
                (6 + 12*t2 + 3*Power(t2,2) - 2*Power(t2,3)) - 
               Power(s2,2)*(96 + (127 - 8*s)*t2 + 
                  2*(-2 + s)*Power(t2,2) + (95 - 34*s)*Power(t2,3) + 
                  6*Power(t2,4)) + 
               2*s2*(-8 - (42 + 11*s)*t2 - 
                  2*(139 + 13*s + 13*Power(s,2))*Power(t2,2) + 
                  (-113 + 11*s - 10*Power(s,2))*Power(t2,3) + 
                  (-51 + 6*s)*Power(t2,4))) + 
            t1*Power(t2,4)*(-30 - 3*(-7 + 6*Power(s,2))*t2 + 
               (-1368 - 487*s + 151*Power(s,2) + 14*Power(s,3))*
                Power(t2,2) + 
               (-2520 + 291*s + 342*Power(s,2) + 17*Power(s,3))*
                Power(t2,3) + 
               (1894 + 2025*s - 363*Power(s,2) - 7*Power(s,3))*
                Power(t2,4) - 5*(-263 + 65*s)*Power(t2,5) + 
               Power(s2,3)*t2*
                (560 + 344*t2 - 239*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(48 + (-34 + 64*s)*t2 + 
                  (2059 + 342*s)*Power(t2,2) + 
                  (70 + 551*s)*Power(t2,3) - 21*(-5 + s)*Power(t2,4)) + 
               s2*(-176 + 2*(-1229 + 94*s)*t2 - 
                  (4215 + 338*s + 300*Power(s,2))*Power(t2,2) + 
                  (2695 + 1052*s - 329*Power(s,2))*Power(t2,3) + 
                  3*(167 + 86*s + 7*Power(s,2))*Power(t2,4) + 
                  325*Power(t2,5))) + 
            Power(t1,6)*t2*(-63 - 11*t2 + 1708*Power(t2,2) + 
               1296*Power(t2,3) + 1305*Power(t2,4) - 115*Power(t2,5) + 
               2*Power(s2,3)*Power(t2,2)*(1 + 22*t2) + 
               2*Power(s,3)*t2*(-8 - 35*t2 + 91*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-120 - 830*t2 + 293*Power(t2,2) + 189*Power(t2,3)) + 
               s2*(-12 - 245*t2 - 699*Power(t2,2) - 2174*Power(t2,3) + 
                  1496*Power(t2,4) + 4*Power(t2,5)) + 
               s*(6 + 2*(89 + 35*s2 + 6*Power(s2,2))*t2 + 
                  (2082 + 232*s2 + 32*Power(s2,2))*Power(t2,2) - 
                  2*(-535 + 14*s2 + 21*Power(s2,2))*Power(t2,3) - 
                  3*(895 + 186*s2)*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s,2)*t2*
                (43 - 404*t2 - 1494*Power(t2,2) + 419*Power(t2,3) - 
                  2*s2*(3 + 26*t2 + 75*Power(t2,2)))) + 
            Power(t1,2)*Power(t2,3)*
             (86 + (1065 + 104*s + 18*Power(s,2))*t2 + 
               (3139 + 712*s - 57*Power(s,2) + 30*Power(s,3))*
                Power(t2,2) + 
               (1307 - 3210*s - 219*Power(s,2) + 14*Power(s,3))*
                Power(t2,3) - 
               (9279 + 4084*s - 1496*Power(s,2) + 44*Power(s,3))*
                Power(t2,4) + 
               (-4010 + 2242*s - 202*Power(s,2))*Power(t2,5) + 
               44*Power(t2,6) + 
               2*Power(s2,3)*t2*
                (260 + 486*t2 + 209*Power(t2,2) - 73*Power(t2,3)) + 
               2*s2*(24 + (231 - 206*s)*t2 + 
                  3*(-74 - 231*s + 46*Power(s,2))*Power(t2,2) + 
                  (-5423 - 1343*s + 345*Power(s,2))*Power(t2,3) + 
                  (-752 + 16*s - 29*Power(s,2))*Power(t2,4) + 
                  (-980 + 202*s)*Power(t2,5)) - 
               Power(s2,2)*t2*
                (1102 + 1985*t2 - 1673*Power(t2,2) + 1132*Power(t2,3) + 
                  202*Power(t2,4) + 
                  s*(48 + 726*t2 + 994*Power(t2,2) - 248*Power(t2,3)))) \
+ Power(t1,4)*Power(t2,2)*(71 - 1228*t2 - 5400*Power(t2,2) - 
               5360*Power(t2,3) + 2804*Power(t2,4) + 2469*Power(t2,5) + 
               20*Power(t2,6) - 
               Power(s2,3)*t2*
                (104 + 165*t2 + 441*Power(t2,2) + 10*Power(t2,3)) + 
               2*Power(s,3)*t2*
                (11 + 35*t2 - 163*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s2,2)*t2*
                (867 + 2890*t2 - 2046*Power(t2,2) - 627*Power(t2,3) + 
                  46*Power(t2,4)) + 
               s2*(6 + 260*t2 - 97*Power(t2,2) + 6855*Power(t2,3) - 
                  3470*Power(t2,4) + 660*Power(t2,5)) + 
               s*(12 - 2*(249 + 191*s2 + 3*Power(s2,2))*t2 - 
                  2*(1936 - 94*s2 + 77*Power(s2,2))*Power(t2,2) + 
                  (4103 + 2636*s2 + 946*Power(s2,2))*Power(t2,3) + 
                  (8381 + 2462*s2 + 90*Power(s2,2))*Power(t2,4) - 
                  4*(194 + 23*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(77 + 1218*t2 + 1728*Power(t2,2) - 
                  2177*Power(t2,3) + 46*Power(t2,4) - 
                  2*s2*(6 - 10*t2 + 173*Power(t2,2) + 75*Power(t2,3))))) \
- s1*t2*(2*(9 - 5*s + 4*Power(s,2))*Power(t1,10)*t2 - 
            2*Power(t1,9)*(2 + 
               (-13 + 5*Power(s,3) + Power(s,2)*(-10 + s2) + 
                  s*(-1 + s2) + 2*s2)*t2 + 
               (13 + 18*Power(s,2) + 14*s2 - 5*s*(8 + s2))*Power(t2,2)) + 
            Power(t1,7)*(8 + (35 - 49*Power(s,2) + 14*Power(s,3) + 
                  4*s2 + s*(-22 + 26*s2))*t2 + 
               (-271 + 34*Power(s,3) + 136*s2 + 84*Power(s2,2) + 
                  Power(s,2)*(190 + 27*s2) + 
                  s*(-542 - 76*s2 + 8*Power(s2,2)))*Power(t2,2) + 
               (2 - 104*Power(s,3) + 442*s2 + 36*Power(s2,2) + 
                  4*Power(s2,3) + Power(s,2)*(571 + 129*s2) - 
                  2*s*(320 + 114*s2 + 21*Power(s2,2)))*Power(t2,3) - 
               2*(78 + 28*Power(s,2) + 62*s2 + 4*Power(s2,2) - 
                  27*s*(5 + s2))*Power(t2,4)) + 
            Power(t1,6)*(-4 + 
               (-7 + 42*Power(s,2) + 64*s2 - s*(119 + 12*s2))*t2 + 
               (-157 - 45*Power(s,3) + Power(s,2)*(554 - 27*s2) + 
                  492*s2 + 4*Power(s2,2) + 
                  s*(-641 + 54*s2 - 54*Power(s2,2)))*Power(t2,2) + 
               (497 - 185*Power(s,3) + Power(s,2)*(89 - 66*s2) + 
                  957*s2 - 367*Power(s2,2) - 52*Power(s2,3) + 
                  s*(1281 + 462*s2 + 107*Power(s2,2)))*Power(t2,3) + 
               (313 + 106*Power(s,3) - 1057*s2 - 215*Power(s2,2) - 
                  18*Power(s2,3) - Power(s,2)*(973 + 199*s2) + 
                  s*(2033 + 798*s2 + 111*Power(s2,2)))*Power(t2,4) + 
               2*(118 + 12*Power(s,2) + 28*s2 + 5*Power(s2,2) - 
                  s*(70 + 17*s2))*Power(t2,5)) + 
            Power(t1,4)*t2*(64 + 
               (609 - 24*Power(s,2) + 56*s2 + 192*Power(s2,2) - 
                  6*s*(-3 + 32*s2))*t2 + 
               (1567 + 36*Power(s,3) - 933*s2 + 940*Power(s2,2) + 
                  262*Power(s2,3) + 3*Power(s,2)*(-193 + 90*s2) - 
                  2*s*(154 + 799*s2 + 72*Power(s2,2)))*Power(t2,2) + 
               (-907 + 94*Power(s,3) - 3131*s2 + 1976*Power(s2,2) + 
                  309*Power(s2,3) + 2*Power(s,2)*(967 + 223*s2) - 
                  s*(6216 + 2002*s2 + 401*Power(s2,2)))*Power(t2,3) - 
               (349*Power(s,3) - Power(s,2)*(2011 + 411*s2) + 
                  s*(1791 + 360*s2 - 42*Power(s2,2)) + 
                  2*(2385 - 542*s2 + 125*Power(s2,2) + 52*Power(s2,3)))*
                Power(t2,4) + 
               (-1962 + 11*Power(s,3) - 1580*s2 - 314*Power(s2,2) - 
                  11*Power(s2,3) - Power(s,2)*(466 + 33*s2) + 
                  s*(2299 + 780*s2 + 33*Power(s2,2)))*Power(t2,5) + 
               5*(-1 + 2*s - 2*s2)*Power(t2,6)) - 
            4*Power(t2,5)*(Power(s,3)*t2*(3 + 5*t2) + 
               Power(s2,3)*(-32 - 5*t2 + 21*Power(t2,2)) - 
               2*Power(s2,2)*
                (-18 + 66*t2 + 25*Power(t2,2) + 11*Power(t2,3)) + 
               s2*(127 + 217*t2 - 247*Power(t2,2) - 107*Power(t2,3) - 
                  30*Power(t2,4)) + 
               s*(15 + (1 + 60*s2 - 47*Power(s2,2))*t2 - 
                  (55 + 76*s2 + 49*Power(s2,2))*Power(t2,2) - 
                  (87 + 8*s2)*Power(t2,3) + 30*Power(t2,4)) - 
               2*(5 - 42*t2 - 9*Power(t2,2) + 120*Power(t2,3) + 
                  46*Power(t2,4)) + 
               Power(s,2)*t2*(-8 - 22*t2 + 30*Power(t2,2) + 
                  s2*(-15 + 23*t2))) + 
            4*t1*Power(t2,4)*(86 + 191*t2 - 33*Power(t2,2) - 
               891*Power(t2,3) - 179*Power(t2,4) + 18*Power(t2,5) + 
               3*Power(s,3)*t2*(3 + 10*t2 - 5*Power(t2,2)) + 
               Power(s2,3)*(136 + 245*t2 + 46*Power(t2,2) - 
                  35*Power(t2,3)) - 
               Power(s2,2)*(228 + 158*t2 - 295*Power(t2,2) + 
                  185*Power(t2,3) + 44*Power(t2,4)) - 
               s2*(-1 + 438*t2 + 1308*Power(t2,2) + 120*Power(t2,3) + 
                  215*Power(t2,4)) + 
               s*(45 - 3*(-26 + 60*s2 + 35*Power(s2,2))*t2 - 
                  2*(192 + 107*s2 + 51*Power(s2,2))*Power(t2,2) + 
                  (-56 + 98*s2 + 55*Power(s2,2))*Power(t2,3) + 
                  (269 + 88*s2)*Power(t2,4)) + 
               Power(s,2)*t2*(-18 + 39*t2 + 143*Power(t2,2) - 
                  44*Power(t2,3) + s2*(-45 + 42*t2 - 5*Power(t2,2)))) + 
            Power(t1,8)*t2*(-7 - 180*t2 + 39*Power(t2,2) + 
               2*Power(s2,2)*t2*(8 + t2) + Power(s,3)*(1 + 51*t2) + 
               2*s2*(-14 - 60*t2 + 49*Power(t2,2)) + 
               Power(s,2)*(-63 + s2*(10 - 28*t2) - 173*t2 + 
                  64*Power(t2,2)) + 
               s*(95 + 61*t2 + 2*Power(s2,2)*t2 - 220*Power(t2,2) - 
                  2*s2*(6 - 9*t2 + 19*Power(t2,2)))) - 
            Power(t1,5)*t2*(123 + 137*t2 - 1262*Power(t2,2) - 
               1598*Power(t2,3) - 604*Power(t2,4) + 122*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(18 - 119*t2 - 25*Power(t2,2)) + 
               2*Power(s,3)*t2*
                (16 - 16*t2 - 184*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,2)*t2*
                (296 + 1031*t2 - 482*Power(t2,2) - 379*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s2*(36 + 410*t2 + 788*Power(t2,2) + 1748*Power(t2,3) - 
                  1720*Power(t2,4) - 8*Power(t2,5)) - 
               2*s*(27 + (347 + 141*s2 + 18*Power(s2,2))*t2 + 
                  (1543 + 248*s2 + 50*Power(s2,2))*Power(t2,2) - 
                  (426 + 320*s2 + 119*Power(s2,2))*Power(t2,3) - 
                  (1510 + 585*s2 + 52*Power(s2,2))*Power(t2,4) + 
                  (5 + 4*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(31 + 1660*t2 + 1108*Power(t2,2) - 
                  923*Power(t2,3) + 4*Power(t2,4) + 
                  s2*(54 + 88*t2 + 145*Power(t2,2) - 133*Power(t2,3)))) + 
            Power(t1,2)*Power(t2,3)*
             (-200 + 719*t2 + 2848*Power(t2,2) + 5340*Power(t2,3) - 
               1028*Power(t2,4) - 671*Power(t2,5) - 
               Power(s,3)*t2*
                (36 + 208*t2 - 199*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-708 - 418*t2 + 379*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,2)*(-384 - 1168*t2 - 2815*Power(t2,2) + 
                  1064*Power(t2,3) + 463*Power(t2,4)) + 
               s2*(588 + 3572*t2 + 6507*Power(t2,2) - 
                  1687*Power(t2,3) + 1567*Power(t2,4) - 115*Power(t2,5)) \
+ s*(-180 + 2*(-19 + 568*s2 + 74*Power(s2,2))*t2 + 
                  (2943 + 98*s2 + 204*Power(s2,2))*Power(t2,2) - 
                  (2631 + 1664*s2 + 807*Power(s2,2))*Power(t2,3) - 
                  (3137 + 1322*s2 + 81*Power(s2,2))*Power(t2,4) + 
                  115*Power(t2,5)) + 
               Power(s,2)*t2*(24 - 1091*t2 - 704*Power(t2,2) + 
                  859*Power(t2,3) + 
                  s2*(180 + 102*t2 + 229*Power(t2,2) + 81*Power(t2,3)))) \
+ Power(t1,3)*Power(t2,2)*(-56 - 1533*t2 - 4159*Power(t2,2) - 
               2957*Power(t2,3) + 4333*Power(t2,4) + 1796*Power(t2,5) + 
               16*Power(t2,6) - 
               2*Power(s2,3)*t2*
                (98 + 185*t2 + 196*Power(t2,2) + Power(t2,3)) + 
               2*Power(s,3)*t2*
                (6 + 52*t2 - 117*Power(t2,2) + 79*Power(t2,3)) + 
               Power(s2,2)*t2*
                (720 + 1619*t2 - 1595*Power(t2,2) - 218*Power(t2,3) + 
                  98*Power(t2,4)) + 
               2*s2*(-42 - 270*t2 - 378*Power(t2,2) + 2491*Power(t2,3) - 
                  602*Power(t2,4) + 357*Power(t2,5)) + 
               2*s*(30 + (-171 + 8*s2 + 42*Power(s2,2))*t2 + 
                  (-947 + 891*s2 + 58*Power(s2,2))*Power(t2,2) + 
                  (3150 + 1333*s2 + 471*Power(s2,2))*Power(t2,3) + 
                  3*(665 + 266*s2 + 27*Power(s2,2))*Power(t2,4) - 
                  (425 + 98*s2)*Power(t2,5)) + 
               Power(s,2)*t2*(40 + 1415*t2 - 527*Power(t2,2) - 
                  1802*Power(t2,3) + 98*Power(t2,4) - 
                  2*s2*(30 + 197*t2 + 270*Power(t2,2) + 159*Power(t2,3))))\
))*R1(t2))/((s - s2 + t1)*Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,
        2)*Power(t1 - t2,3)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*
       (-1 + t2)*t2*(s - s1 + t2)) + 
    (8*(-12*Power(s1,9)*Power(t1,2) + 2*Power(-1 + s,3)*Power(t1,8) + 
         4*t1*t2*(7 + (20 - 27*Power(s,2) + s*(29 - 30*s2) + 35*s2 + 
               9*Power(s2,2))*t2 + 
            (60 + 13*Power(s,3) + 77*s2 + 24*Power(s2,2) + 
               3*Power(s2,3) + Power(s,2)*(-76 + 41*s2) - 
               s*(9 + 128*s2 + 17*Power(s2,2)))*Power(t2,2) + 
            (46 - 9*Power(s,3) + 51*s2 + 23*Power(s2,2) - 
               3*Power(s2,3) + Power(s,2)*(87 + 43*s2) - 
               s*(215 + 98*s2 + 31*Power(s2,2)))*Power(t2,3) - 
            (51 + 4*Power(s,3) + 17*s2 + 8*Power(s2,2) - 
               4*Power(s2,3) - 4*Power(s,2)*(7 + 3*s2) + 
               s*(31 + 20*s2 + 12*Power(s2,2)))*Power(t2,4) + 
            2*(-17 + 5*s - 5*s2)*Power(t2,5)) + 
         Power(t1,3)*(-4 + (23 + 84*Power(s,2) - 2*s2 + 
               2*s*(-67 + 6*s2))*t2 + 
            (34 - 78*Power(s,3) + Power(s,2)*(391 - 30*s2) - 85*s2 + 
               15*Power(s2,2) + 6*Power(s2,3) + 
               s*(-319 + 146*s2 - 10*Power(s2,2)))*Power(t2,2) - 
            (188 + 45*Power(s,3) + 101*s2 + 120*Power(s2,2) + 
               3*Power(s2,3) + Power(s,2)*(-16 + 65*s2) - 
               s*(325 + 208*s2 + 113*Power(s2,2)))*Power(t2,3) + 
            (-70 + 15*Power(s,3) - 159*s2 - 39*Power(s2,2) - 
               15*Power(s2,3) - Power(s,2)*(131 + 45*s2) + 
               s*(283 + 170*s2 + 45*Power(s2,2)))*Power(t2,4) + 
            (13 + 5*s - 5*s2)*Power(t2,5)) + 
         2*Power(s1,8)*t1*(2*Power(s2,2) - s2*t1 - 35*Power(t1,2) + 
            12*t2 + t1*(8 + 9*s + 18*t2)) - 
         2*(-1 + s)*Power(t1,7)*
          (4 + t2 + 3*s2*t2 + Power(s,2)*(1 + 3*t2) - 
            s*(5 + (7 + 3*s2)*t2)) + 
         Power(t1,6)*(-8 + t2 - 20*s2*t2 - 
            2*(4 + 5*s2 + 3*Power(s2,2))*Power(t2,2) + 
            Power(s,3)*(-4 - 9*t2 + 6*Power(t2,2)) + 
            Power(s,2)*(6 + (11 - 14*s2)*t2 - 
               2*(13 + 6*s2)*Power(t2,2)) + 
            s*(6 + (3 + 34*s2)*t2 + 
               (34 + 22*s2 + 6*Power(s2,2))*Power(t2,2))) + 
         Power(t1,5)*(-4 + 5*(-9 + 4*s2)*t2 + 
            2*(13 - 8*s2 + 11*Power(s2,2))*Power(t2,2) + 
            (19 + 26*s2 + 2*Power(s2,2) + 2*Power(s2,3))*Power(t2,3) + 
            Power(s,3)*t2*(22 + 39*t2 - 2*Power(t2,2)) + 
            Power(s,2)*(-12 + (-117 + 4*s2)*t2 - 
               (108 + 23*s2)*Power(t2,2) + 2*(8 + 3*s2)*Power(t2,3)) + 
            s*(18 - 8*(-17 + 3*s2)*t2 + 
               (41 + 25*s2 - 16*Power(s2,2))*Power(t2,2) - 
               3*(15 + 6*s2 + 2*Power(s2,2))*Power(t2,3))) + 
         Power(s1,7)*(-170*Power(t1,4) + Power(s2,3)*(2 + t1) - 
            12*Power(t2,2) - 4*t1*t2*(8 + 9*s + 18*t2) + 
            Power(t1,3)*(77 + 107*s + 167*t2) - 
            s2*t1*(8 + t1 + 5*s*t1 + 15*Power(t1,2) - 4*t2 - 8*s*t2 - 
               39*t1*t2) + Power(t1,2)*
             (20 - 6*Power(s,2) + 204*t2 - 64*s*t2 - 36*Power(t2,2)) + 
            2*Power(s2,2)*(12*Power(t1,2) - 2*t2 - t1*(3 + s + 7*t2))) - 
         8*Power(t2,2)*(Power(s,3)*t2*(1 + t2 + 2*Power(t2,2)) + 
            Power(s2,3)*t2*(1 - 3*t2 + 6*Power(t2,2)) + 
            2*Power(s2,2)*t2*
             (1 + 21*t2 - 2*Power(t2,2) + 3*Power(t2,3)) + 
            s2*(9 + 31*t2 - 3*Power(t2,2) + 53*Power(t2,3) + 
               6*Power(t2,4)) - 
            s*(-9 + (-31 + 9*Power(s2,2))*t2 + 
               (-33 + 16*s2 + 17*Power(s2,2))*Power(t2,2) + 
               5*(15 + 2*Power(s2,2))*Power(t2,3) + 
               2*(7 + 6*s2)*Power(t2,4)) - 
            2*(2 + 3*t2 - 12*Power(t2,2) - 3*Power(t2,3) + 
               8*Power(t2,4) + 2*Power(t2,5)) + 
            Power(s,2)*t2*(2 - 50*t2 + 6*Power(t2,3) + 
               s2*(-9 + 19*t2 + 2*Power(t2,2)))) + 
         2*Power(t1,2)*t2*(-36 - 
            (110 + 21*s2 + 28*Power(s2,2) + 2*Power(s2,3))*t2 - 
            (24 + 123*s2 - 80*Power(s2,2) + 9*Power(s2,3))*Power(t2,2) + 
            (92 + 197*s2 + 14*Power(s2,2) + 23*Power(s2,3))*
             Power(t2,3) + (84 + 75*s2 + 26*Power(s2,2))*Power(t2,4) + 
            2*Power(t2,5) + Power(s,3)*t2*
             (-20 + 49*t2 + 31*Power(t2,2)) + 
            s*(42 + (189 + 80*s2)*t2 + 
               (387 + 204*s2 - 37*Power(s2,2))*Power(t2,2) - 
               (93 + 16*s2 + 15*Power(s2,2))*Power(t2,3) - 
               (101 + 52*s2)*Power(t2,4)) + 
            Power(s,2)*t2*(4 - 356*t2 - 86*Power(t2,2) + 
               26*Power(t2,3) - s2*(42 + 59*t2 + 39*Power(t2,2)))) - 
         Power(t1,4)*(-10 + (-63 + 4*s2)*t2 + 
            (-40 - 119*s2 + 31*Power(s2,2))*Power(t2,2) + 
            (-7 + 2*s2 - 36*Power(s2,2) + 4*Power(s2,3))*Power(t2,3) + 
            (32 + 9*s2 + 4*Power(s2,2))*Power(t2,4) + 
            Power(s,3)*t2*(-28 + 17*t2 + 43*Power(t2,2)) + 
            s*(12 + (73 + 10*s2)*t2 + 
               (264 + 222*s2 - 19*Power(s2,2))*Power(t2,2) + 
               (157 + 122*s2 + 35*Power(s2,2))*Power(t2,3) - 
               2*(9 + 4*s2)*Power(t2,4)) + 
            Power(s,2)*t2*(40 - 193*t2 - 168*Power(t2,2) + 
               4*Power(t2,3) - 2*s2*(6 + 43*t2 + 41*Power(t2,2)))) + 
         Power(s1,6)*(-220*Power(t1,5) + 
            Power(s2,3)*(-4 + 4*Power(t1,2) - 7*t1*(-2 + t2) - 5*t2) + 
            2*Power(t2,2)*(8 + 9*s + 18*t2) + 
            4*Power(t1,4)*(36 + 66*s + 77*t2) + 
            2*t1*(2 + (-20 - 4*s + 6*Power(s,2))*t2 + 
               (-99 + 64*s + 2*Power(s,2))*Power(t2,2) + 36*Power(t2,3)) \
- Power(t1,3)*(-100 + 40*Power(s,2) - 592*t2 + 115*Power(t2,2) + 
               s*(4 + 293*t2)) + 
            Power(s2,2)*(-6 + 62*Power(t1,3) + 6*t2 + 8*s*t2 + 
               14*Power(t2,2) - Power(t1,2)*(39 + 7*s + 51*t2) + 
               t1*(-11 - 2*s - 38*t2 + 11*s*t2 + 16*Power(t2,2))) + 
            Power(t1,2)*(41 - 386*t2 - 557*Power(t2,2) + 
               12*Power(t2,3) + Power(s,2)*(-14 + 37*t2) + 
               s*(-45 - 344*t2 + 87*Power(t2,2))) - 
            s2*(46*Power(t1,4) + 3*Power(t1,3)*(1 + 9*s - 58*t2) + 
               2*t2*(-4 + t2 + 4*s*t2) - 
               Power(t1,2)*(-26 + 16*s + Power(s,2) + 17*t2 - 
                  82*Power(t2,2)) + 
               2*t1*(-6 + s - 15*t2 + 2*s*t2 + 2*Power(s,2)*t2 + 
                  39*Power(t2,2) + 10*s*Power(t2,2)))) + 
         Power(s1,5)*(-160*Power(t1,6) + 
            2*Power(t1,5)*(63 + 173*s + 141*t2) - 
            2*t2*(2 + (-10 - 4*s + 3*Power(s,2))*t2 + 
               2*(-16 + 16*s + Power(s,2))*Power(t2,2) + 18*Power(t2,3)) \
- Power(t1,4)*(-200 + 108*Power(s,2) - 831*t2 + 129*Power(t2,2) + 
               s*(6 + 532*t2)) + 
            Power(s2,3)*(6*Power(t1,3) + t2*(-10 + 7*t2) - 
               4*Power(t1,2)*(-9 + 7*t2) + 
               t1*(-16 - 31*t2 + 11*Power(t2,2))) + 
            Power(t1,3)*(135 + 3*Power(s,3) - 945*t2 - 
               1291*Power(t2,2) + 16*Power(t2,3) + 
               Power(s,2)*(-55 + 168*t2) + 
               s*(-189 - 1021*t2 + 279*Power(t2,2))) + 
            t1*(-6 - 96*t2 + 541*Power(t2,2) - 
               2*Power(s,3)*Power(t2,2) + 613*Power(t2,3) - 
               24*Power(t2,4) - 
               2*Power(s,2)*t2*(-13 + 41*t2 + 3*Power(t2,2)) + 
               s*(4 + 104*t2 + 387*Power(t2,2) - 174*Power(t2,3))) + 
            Power(t1,2)*(-54 + Power(s,3)*(6 - 9*t2) - 306*t2 - 
               534*Power(t2,2) + 419*Power(t2,3) + 
               Power(s,2)*(31 + 141*t2 - 51*Power(t2,2)) + 
               s*(-90 + 255*t2 + 887*Power(t2,2) - 45*Power(t2,3))) + 
            Power(s2,2)*(12 + 88*Power(t1,4) + (23 - 16*s)*t2 + 
               (14 - 17*s)*Power(t2,2) - 16*Power(t2,3) - 
               8*Power(t1,3)*(12 + s + 8*t2) + 
               t1*(-44 + (129 + 55*s)*t2 - 6*(-19 + 4*s)*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(t1,2)*(-38 - 107*t2 + 3*Power(t2,2) + 
                  s*(-8 + 42*t2))) + 
            s2*(6 - 74*Power(t1,5) - 2*(6 + 5*s)*t2 + 
               (-29 + 9*s + 10*Power(s,2))*Power(t2,2) + 
               (39 + 20*s)*Power(t2,3) + 
               Power(t1,4)*(-2 - 58*s + 306*t2) + 
               Power(t1,3)*(-36 + 6*Power(s,2) + s*(66 - 90*t2) + 
                  55*t2 - 255*Power(t2,2)) - 
               Power(t1,2)*(-100 + 101*t2 + 445*Power(t2,2) - 
                  45*Power(t2,3) + 5*Power(s,2)*(2 + t2) + 
                  12*s*(1 + t2 - 4*Power(t2,2))) + 
               t1*(19 + 32*t2 - 21*Power(t2,2) + 164*Power(t2,3) + 
                  Power(s,2)*t2*(-8 + 15*t2) + 
                  2*s*(11 - 38*t2 + 14*Power(t2,2) + 6*Power(t2,3))))) + 
         Power(s1,4)*(-2 - 62*Power(t1,7) + 6*t2 + 2*s*t2 + 
            55*Power(t2,2) - 59*s*Power(t2,2) - 
            18*Power(s,2)*Power(t2,2) - 232*Power(t2,3) - 
            150*s*Power(t2,3) + 45*Power(s,2)*Power(t2,3) + 
            4*Power(s,3)*Power(t2,3) - 223*Power(t2,4) + 
            87*s*Power(t2,4) + 6*Power(s,2)*Power(t2,4) + 
            12*Power(t2,5) + 2*Power(t1,6)*(22 + 127*s + 64*t2) + 
            Power(t1,5)*(198 - 152*Power(s,2) + s*(16 - 478*t2) + 
               621*t2 - 57*Power(t2,2)) + 
            Power(s2,3)*(4*Power(t1,4) + Power(t1,3)*(44 - 42*t2) + 
               t1*t2*(-54 + 61*t2 - 5*Power(t2,2)) - 
               7*t2*(-4 - 5*t2 + Power(t2,2)) + 
               Power(t1,2)*(-24 - 55*t2 + 29*Power(t2,2))) + 
            Power(t1,4)*(168 + 14*Power(s,3) - 877*t2 - 
               1277*Power(t2,2) - 4*Power(t2,3) + 
               Power(s,2)*(-86 + 302*t2) + 
               3*s*(-104 - 467*t2 + 105*Power(t2,2))) + 
            Power(t1,3)*(-125 + Power(s,3)*(22 - 42*t2) - 802*t2 - 
               756*Power(t2,2) + 617*Power(t2,3) + 
               Power(s,2)*(126 + 451*t2 - 193*Power(t2,2)) + 
               s*(-314 + 740*t2 + 1921*Power(t2,2) - 85*Power(t2,3))) + 
            Power(t1,2)*(-83 - 262*t2 + 2016*Power(t2,2) + 
               1954*Power(t2,3) - 76*Power(t2,4) + 
               Power(s,3)*(-4 - 8*t2 + 9*Power(t2,2)) + 
               s*(101 + 692*t2 + 1206*Power(t2,2) - 832*Power(t2,3)) + 
               Power(s,2)*(30 + 6*t2 - 483*Power(t2,2) + 26*Power(t2,3))\
) + t1*(-9 + 118*t2 + 328*Power(t2,2) - 28*Power(t2,3) - 
               493*Power(t2,4) + 
               Power(s,3)*t2*(-12 + 14*t2 + 5*Power(t2,2)) + 
               Power(s,2)*t2*(-32 - 199*t2 + 118*Power(t2,2)) + 
               s*(-20 + 213*t2 - 526*Power(t2,2) - 907*Power(t2,3) + 
                  90*Power(t2,4))) + 
            Power(s2,2)*(72*Power(t1,5) - 
               2*Power(t1,4)*(57 + s + 13*t2) - 
               Power(t1,3)*(48 + s*(12 - 58*t2) + 153*t2 + 
                  89*Power(t2,2)) + 
               Power(t1,2)*(-100 + (347 + 79*s)*t2 - 
                  49*(-5 + s)*Power(t2,2) + 26*Power(t2,3)) + 
               t1*(48 + (165 - 70*s)*t2 - (57 + 103*s)*Power(t2,2) + 
                  3*(-12 + 5*s)*Power(t2,3)) + 
               t2*(32 - 90*t2 - 63*Power(t2,2) + 6*Power(t2,3) + 
                  6*s*(2 - 7*t2 + 3*Power(t2,2)))) + 
            s2*(-12 - 66*Power(t1,6) + (-31 + 14*s)*t2 + 
               (-6 + 72*s - 17*Power(s,2))*Power(t2,2) + 
               (19 - 28*s - 15*Power(s,2))*Power(t2,3) - 
               2*(41 + 6*s)*Power(t2,4) + 
               Power(t1,5)*(2 - 62*s + 264*t2) + 
               Power(t1,4)*(-36 + 14*Power(s,2) + 127*t2 - 
                  273*Power(t2,2) - 8*s*(-13 + 25*t2)) + 
               Power(t1,3)*(236 - 378*t2 - 937*Power(t2,2) + 
                  85*Power(t2,3) + Power(s,2)*(-40 + 26*t2) + 
                  2*s*(-6 + 5*t2 + 141*Power(t2,2))) + 
               Power(t1,2)*(5 + 27*t2 + 342*Power(t2,2) + 
                  748*Power(t2,3) + Power(s,2)*t2*(-21 + 11*t2) + 
                  s*(48 - 92*t2 + 232*Power(t2,2) - 52*Power(t2,3))) + 
               t1*(46 - 281*t2 + 187*Power(t2,2) + 380*Power(t2,3) - 
                  90*Power(t2,4) + 
                  Power(s,2)*t2*(42 + 28*t2 - 15*Power(t2,2)) - 
                  s*(12 + 22*t2 - 183*Power(t2,2) + 82*Power(t2,3))))) + 
         Power(s1,3)*(4 - 10*Power(t1,8) + 
            (13 + s*(2 - 12*s2) - 34*s2 - 84*Power(s2,2))*t2 + 
            (-64 + 6*Power(s,3) + Power(s,2)*(25 - 2*s2) + 181*s2 - 
               151*Power(s2,2) - 6*Power(s2,3) + 
               s*(-129 + 22*s2 + 114*Power(s2,2)))*Power(t2,2) - 
            (122 + 15*Power(s,3) + 83*s2 - 90*Power(s2,2) + 
               41*Power(s2,3) + 7*Power(s,2)*(-14 + 5*s2) + 
               s*(-275 + 132*s2 - 91*Power(s2,2)))*Power(t2,3) + 
            (140 - 3*Power(s,3) - 109*s2 + 33*Power(s2,2) + 
               3*Power(s2,3) + Power(s,2)*(-67 + 9*s2) + 
               s*(313 + 34*s2 - 9*Power(s2,2)))*Power(t2,4) - 
            9*(-21 + 5*s - 5*s2)*Power(t2,5) + 
            Power(t1,7)*(-3 + 99*s - 31*s2 + 23*t2) + 
            Power(t1,6)*(94 - 118*Power(s,2) + 32*Power(s2,2) + 
               s*(44 - 33*s2 - 212*t2) + 241*t2 - 7*Power(t2,2) + 
               3*s2*(1 + 37*t2)) + 
            Power(t1,5)*(102 + 26*Power(s,3) + Power(s2,3) + 
               6*Power(s2,2)*(-11 + t2) - 291*t2 - 549*Power(t2,2) - 
               8*Power(t2,3) + 2*Power(s,2)*(-37 + 8*s2 + 134*t2) + 
               s2*(-32 + 153*t2 - 109*Power(t2,2)) + 
               s*(-246 + 2*Power(s2,2) + s2*(76 - 180*t2) - 985*t2 + 
                  141*Power(t2,2))) - 
            t1*(16 - 203*t2 - 131*Power(t2,2) + 1779*Power(t2,3) + 
               1267*Power(t2,4) - 104*Power(t2,5) + 
               Power(s,3)*t2*(-8 - 33*t2 + 29*Power(t2,2)) + 
               Power(s2,3)*t2*(-52 - 103*t2 + 55*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-152 + 455*t2 + 243*Power(t2,2) + 38*Power(t2,3)) + 
               s2*(48 + 119*t2 - 191*Power(t2,2) + 605*Power(t2,3) + 
                  731*Power(t2,4)) - 
               s*(12 + (-197 + 32*s2 + 48*Power(s2,2))*t2 + 
                  (-871 + 46*s2 - 101*Power(s2,2))*Power(t2,2) + 
                  (-419 - 298*s2 + 81*Power(s2,2))*Power(t2,3) + 
                  (827 + 76*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (112 - 209*t2 - 453*Power(t2,2) + 38*Power(t2,3) + 
                  s2*(12 + 67*t2 - 3*Power(t2,2)))) + 
            Power(t1,4)*(-63 + Power(s,3)*(28 - 78*t2) + 
               Power(s2,3)*(26 - 28*t2) - 838*t2 - 726*Power(t2,2) + 
               297*Power(t2,3) - 
               Power(s2,2)*(26 + 121*t2 + 125*Power(t2,2)) + 
               s2*(224 - 342*t2 - 895*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s,2)*(204 + 631*t2 - 233*Power(t2,2) + 
                  s2*(-60 + 74*t2)) + 
               s*(-416 + 650*t2 + 1775*Power(t2,2) - 35*Power(t2,3) + 
                  8*Power(s2,2)*(-1 + 4*t2) + 
                  2*s2*(8 + 17*t2 + 179*Power(t2,2)))) + 
            Power(t1,3)*(-195 - 355*t2 + 2098*Power(t2,2) + 
               2108*Power(t2,3) + 
               Power(s2,3)*(-16 - 37*t2 + 21*Power(t2,2)) + 
               Power(s,3)*(-16 - 41*t2 + 45*Power(t2,2)) + 
               Power(s2,2)*(-92 + 353*t2 + 187*Power(t2,2) + 
                  74*Power(t2,3)) + 
               Power(s,2)*(100 - (157 + 37*s2)*t2 - 
                  (941 + 69*s2)*Power(t2,2) + 74*Power(t2,3)) + 
               s2*(-31 - 111*t2 + 640*Power(t2,2) + 934*Power(t2,3)) + 
               s*(209 + 1447*t2 + 1568*Power(t2,2) - 1106*Power(t2,3) + 
                  Power(s2,2)*t2*(11 + 3*t2) + 
                  s2*(4 + 90*t2 + 474*Power(t2,2) - 148*Power(t2,3)))) + 
            Power(t1,2)*(59 + 487*t2 + 974*Power(t2,2) - 
               689*Power(t2,3) - 1111*Power(t2,4) + 
               Power(s2,3)*t2*(-90 + 119*t2 - 3*Power(t2,2)) + 
               Power(s,3)*t2*(-50 + 95*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(60 + 299*t2 - 91*Power(t2,2) + 
                  142*Power(t2,3)) - 
               Power(s,2)*(24 + (295 - 134*s2)*t2 + 
                  (607 + 17*s2)*Power(t2,2) + (-506 + 9*s2)*Power(t2,3)) \
+ s2*(70 - 513*t2 + 1013*Power(t2,2) + 836*Power(t2,3) - 
                  290*Power(t2,4)) + 
               s*(-2 + 689*t2 - 2151*Power(t2,2) - 2542*Power(t2,3) + 
                  290*Power(t2,4) + 
                  Power(s2,2)*t2*(-62 - 197*t2 + 9*Power(t2,2)) - 
                  6*s2*(2 + 27*t2 - 34*Power(t2,2) + 108*Power(t2,3))))) \
- s1*(2*(1 - 5*s + 4*Power(s,2))*Power(t1,8) + 
            Power(t1,2)*(16 + 
               (137 - 132*Power(s,2) + s*(214 - 36*s2) + 58*s2 + 
                  72*Power(s2,2))*t2 + 
               (564 - 6*Power(s,3) + 161*s2 + 181*Power(s2,2) - 
                  10*Power(s2,3) + Power(s,2)*(-779 + 178*s2) + 
                  s*(467 - 546*s2 - 18*Power(s2,2)))*Power(t2,2) + 
               (806 + 133*Power(s,3) + 805*s2 + 70*Power(s2,2) + 
                  67*Power(s2,3) + Power(s,2)*(62 + 49*s2) - 
                  s*(2341 + 428*s2 + 249*Power(s2,2)))*Power(t2,3) + 
               (-27*Power(s,3) + Power(s,2)*(489 + 81*s2) - 
                  s*(1079 + 582*s2 + 81*Power(s2,2)) + 
                  3*(-148 + 81*s2 + 31*Power(s2,2) + 9*Power(s2,3)))*
                Power(t2,4) + 5*(-91 + 23*s - 23*s2)*Power(t2,5)) - 
            4*t2*(-7 + (2 - 9*Power(s,2) - 11*s2 + 27*Power(s2,2) + 
                  s*(19 + 30*s2))*t2 + 
               (74 + 3*Power(s,3) - 37*s2 + 62*Power(s2,2) + 
                  5*Power(s2,3) - Power(s,2)*(22 + 25*s2) + 
                  s*(33 + 12*s2 - 23*Power(s2,2)))*Power(t2,2) + 
               (9*Power(s,3) + Power(s,2)*(-29 + 13*s2) + 
                  s*(-193 + 6*s2 - 41*Power(s2,2)) + 
                  s2*(85 - 37*s2 + 19*Power(s2,2)))*Power(t2,3) + 
               (-147 + 44*Power(s,2) - 23*s2 - 8*Power(s2,2) - 
                  9*s*(9 + 4*s2))*Power(t2,4) + 
               6*(-11 + 5*s - 5*s2)*Power(t2,5)) - 
            Power(t1,7)*(3 + 11*Power(s,3) + 
               s*(3 + s2*(2 - 10*t2) - 50*t2) + 12*t2 + 
               4*s2*(-1 + 4*t2) + Power(s,2)*(-23 + 2*s2 + 20*t2)) + 
            Power(t1,6)*(-25 + 40*t2 + 48*Power(t2,2) + 
               2*Power(s2,2)*t2*(2 + t2) + Power(s,3)*(2 + 33*t2) + 
               s2*(-4 - 3*t2 + 44*Power(t2,2)) + 
               Power(s,2)*(-69 - 144*t2 + 16*Power(t2,2) - 
                  5*s2*(-2 + 7*t2)) + 
               s*(94 + 65*t2 + 2*Power(s2,2)*t2 - 82*Power(t2,2) - 
                  6*s2*(2 - 5*t2 + 3*Power(t2,2)))) + 
            4*t1*t2*(1 + 59*t2 - 81*Power(t2,2) - 201*Power(t2,3) - 
               36*Power(t2,4) + 18*Power(t2,5) + 
               Power(s,3)*(t2 + 20*Power(t2,2) - 29*Power(t2,3)) + 
               Power(s2,3)*(t2 + 20*Power(t2,2) - 21*Power(t2,3)) - 
               Power(s2,2)*t2*
                (-51 + 155*t2 + 24*Power(t2,2) + 44*Power(t2,3)) - 
               s2*(15 + 102*t2 - 48*Power(t2,2) + 378*Power(t2,3) + 
                  121*Power(t2,4)) + 
               s*(-15 + (-126 + 2*s2 + 15*Power(s2,2))*t2 + 
                  (-340 - 62*s2 + 24*Power(s2,2))*Power(t2,2) + 
                  (334 - 36*s2 + 13*Power(s2,2))*Power(t2,3) + 
                  (175 + 88*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (-69 + 237*t2 + 116*Power(t2,2) - 44*Power(t2,3) + 
                  s2*(15 - 48*t2 + 37*Power(t2,2)))) + 
            Power(t1,3)*(-44 - 345*t2 - 229*Power(t2,2) + 
               565*Power(t2,3) + 629*Power(t2,4) + 16*Power(t2,5) + 
               Power(s2,3)*t2*(-8 - 29*t2 + 25*Power(t2,2)) + 
               Power(s,3)*t2*(-52 - 19*t2 + 131*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-116 + 173*t2 + 53*Power(t2,2) + 98*Power(t2,3)) + 
               s2*(-12 - 19*t2 - 377*Power(t2,2) + 703*Power(t2,3) + 
                  409*Power(t2,4)) + 
               s*(48 + (431 + 40*s2 + 12*Power(s2,2))*t2 + 
                  (1633 + 646*s2 - 65*Power(s2,2))*Power(t2,2) + 
                  (169 + 214*s2 + 81*Power(s2,2))*Power(t2,3) - 
                  (545 + 196*s2)*Power(t2,4)) + 
               Power(s,2)*t2*
                (196 - 907*t2 - 691*Power(t2,2) + 98*Power(t2,3) - 
                  3*s2*(16 + 37*t2 + 79*Power(t2,2)))) + 
            Power(t1,5)*(39 + 73*t2 + 11*Power(t2,2) + 
               4*Power(s2,3)*Power(t2,2) - 87*Power(t2,3) + 
               Power(s,3)*(16 + 43*t2 - 33*Power(t2,2)) + 
               Power(s2,2)*t2*(-22 + 29*t2 - 4*Power(t2,2)) + 
               s2*(-16 + 41*t2 + 51*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s,2)*(-52 + (69 + 47*s2)*t2 + 
                  7*(39 + 10*s2)*Power(t2,2) - 4*Power(t2,3)) - 
               s*(31 + 297*t2 + 405*Power(t2,2) - 36*Power(t2,3) + 
                  Power(s2,2)*t2*(-14 + 41*t2) - 
                  2*s2*(13 - 57*t2 - 85*Power(t2,2) + 4*Power(t2,3)))) + 
            Power(t1,4)*(15 + 43*t2 - 438*Power(t2,2) - 
               257*Power(t2,3) + 33*Power(t2,4) + 
               Power(s2,2)*t2*(22 - 73*t2 - 90*Power(t2,2)) + 
               Power(s2,3)*t2*(12 - 18*t2 - 11*Power(t2,2)) + 
               Power(s,3)*t2*(-18 - 145*t2 + 11*Power(t2,2)) + 
               Power(s,2)*(48 + (509 - 58*s2)*t2 + 
                  (367 + 89*s2)*Power(t2,2) - 11*(22 + 3*s2)*Power(t2,3)\
) + s2*(28 + 67*t2 - 196*Power(t2,2) - 417*Power(t2,3) - 
                  10*Power(t2,4)) + 
               s*(-98 - 575*t2 + 493*Power(t2,2) + 766*Power(t2,3) + 
                  10*Power(t2,4) + 
                  Power(s2,2)*t2*(-30 + 74*t2 + 33*Power(t2,2)) + 
                  s2*(-12 + 92*t2 + 77*Power(t2,2) + 332*Power(t2,3))))) \
+ Power(s1,2)*(2*(-2 + 8*s - 3*s2)*Power(t1,8) + 
            Power(t1,7)*(14 - 48*Power(s,2) + s2 + 6*Power(s2,2) + 
               s*(36 - 7*s2 - 37*t2) + 39*t2 + 18*s2*t2) - 
            t1*(-16 + (75 - 72*Power(s,2) + 98*s2 + 132*Power(s2,2) + 
                  s*(38 + 36*s2))*t2 - 
               (-662 + 2*Power(s,3) + Power(s,2)*(197 - 78*s2) + 
                  401*s2 - 499*Power(s2,2) + 54*Power(s2,3) + 
                  s*(-477 + 246*s2 + 166*Power(s2,2)))*Power(t2,2) + 
               (372 + 47*Power(s,3) + 979*s2 - 316*Power(s2,2) + 
                  129*Power(s2,3) + Power(s,2)*(-356 + 91*s2) + 
                  s*(-2195 + 168*s2 - 267*Power(s2,2)))*Power(t2,3) + 
               (-1422 + 7*Power(s,3) + Power(s,2)*(489 - 21*s2) + 
                  113*s2 + 21*Power(s2,2) - 7*Power(s2,3) + 
                  s*(-1477 - 510*s2 + 21*Power(s2,2)))*Power(t2,4) + 
               (-887 + 325*s - 325*s2)*Power(t2,5)) + 
            Power(t1,6)*(33 + 24*Power(s,3) + 5*Power(s2,2)*(-3 + t2) + 
               7*t2 - 78*Power(t2,2) + 
               Power(s,2)*(-46 + 9*s2 + 117*t2) - 
               3*s2*(6 - 28*t2 + 3*Power(t2,2)) + 
               s*(-81 + Power(s2,2) + s2*(24 - 72*t2) - 347*t2 + 
                  18*Power(t2,2))) + 
            Power(t1,5)*(21 + Power(s,3)*(12 - 72*t2) + 
               Power(s2,3)*(6 - 7*t2) - 344*t2 - 366*Power(t2,2) + 
               27*Power(t2,3) - 
               Power(s2,2)*(5 + 45*t2 + 51*Power(t2,2)) - 
               s2*(-80 + 98*t2 + 369*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s,2)*(166 + 433*t2 - 111*Power(t2,2) + 
                  s2*(-40 + 76*t2)) + 
               s*(-268 + 124*t2 + 695*Power(t2,2) + 5*Power(t2,3) + 
                  Power(s2,2)*(-2 + 3*t2) + 
                  2*s2*(15 - t2 + 81*Power(t2,2)))) + 
            Power(t1,2)*(-4 + 391*t2 + 57*Power(t2,2) - 
               2103*Power(t2,3) - 1385*Power(t2,4) + 44*Power(t2,5) + 
               Power(s,3)*t2*(32 + 53*t2 - 121*Power(t2,2)) + 
               Power(s2,3)*t2*(32 + 81*t2 - 69*Power(t2,2)) - 
               Power(s2,2)*t2*
                (-228 + 595*t2 + 301*Power(t2,2) + 202*Power(t2,3)) - 
               s2*(24 + 129*t2 - 303*Power(t2,2) + 1781*Power(t2,3) + 
                  1097*Power(t2,4)) + 
               s*(-24 + (-525 + 4*s2 + 24*Power(s2,2))*t2 + 
                  (-2237 - 498*s2 + 35*Power(s2,2))*Power(t2,2) + 
                  (223 - 394*s2 + 17*Power(s2,2))*Power(t2,3) + 
                  (1379 + 404*s2)*Power(t2,4)) + 
               Power(s,2)*t2*(-276 + 821*t2 + 1091*Power(t2,2) - 
                  202*Power(t2,3) + s2*(24 - 41*t2 + 173*Power(t2,2)))) + 
            Power(t1,4)*(-149 - 287*t2 + 628*Power(t2,2) + 
               882*Power(t2,3) + 20*Power(t2,4) - 
               Power(s2,3)*(4 + 8*t2 + Power(t2,2)) + 
               Power(s,3)*(-24 - 67*t2 + 61*Power(t2,2)) + 
               Power(s2,2)*(-30 + 151*t2 + 19*Power(t2,2) + 
                  46*Power(t2,3)) + 
               Power(s,2)*(116 - (217 + 57*s2)*t2 - 
                  (787 + 123*s2)*Power(t2,2) + 46*Power(t2,3)) + 
               s2*(-1 - 135*t2 + 222*Power(t2,2) + 368*Power(t2,3)) + 
               s*(137 + 1153*t2 + 1086*Power(t2,2) - 484*Power(t2,3) + 
                  7*Power(s2,2)*t2*(-5 + 9*t2) + 
                  s2*(-48 + 186*t2 + 426*Power(t2,2) - 92*Power(t2,3)))) \
+ 2*t2*(6 + (-60 + 51*s + 26*Power(s,2) - 2*Power(s,3))*t2 - 
               (2 - 181*s + 74*Power(s,2) + 5*Power(s,3))*Power(t2,2) + 
               3*(94 - 5*s - 22*Power(s,2) + Power(s,3))*Power(t2,3) + 
               (148 - 137*s + 6*Power(s,2))*Power(t2,4) - 
               22*Power(t2,5) + 
               Power(s2,3)*t2*(-20 - 27*t2 + 11*Power(t2,2)) + 
               s2*(42 + (69 - 76*s)*t2 + 
                  (-85 - 40*s + 71*Power(s,2))*Power(t2,2) + 
                  (127 + 72*s + 5*Power(s,2))*Power(t2,3) + 
                  (119 - 12*s)*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-(s*(42 + 15*t2 + 19*Power(t2,2))) + 
                  2*(5 + 57*t2 + 17*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(t1,3)*(57 + 391*t2 + 1262*Power(t2,2) + 
               25*Power(t2,3) - 559*Power(t2,4) + 
               Power(s,3)*t2*(-42 + 187*t2 - 11*Power(t2,2)) + 
               Power(s2,3)*t2*(-58 + 83*t2 + 11*Power(t2,2)) + 
               Power(s2,2)*(24 + 135*t2 + 63*Power(t2,2) + 
                  250*Power(t2,3)) + 
               Power(s,2)*(-60 + (-655 + 146*s2)*t2 - 
                  (705 + 101*s2)*Power(t2,2) + (610 + 33*s2)*Power(t2,3)) \
+ s2*(2 - 299*t2 + 953*Power(t2,2) + 942*Power(t2,3) - 110*Power(t2,4)) - 
               s*(-98 - 883*t2 + 2159*Power(t2,2) + 2252*Power(t2,3) - 
                  110*Power(t2,4) + 
                  Power(s2,2)*t2*(-22 + 169*t2 + 33*Power(t2,2)) + 
                  2*s2*(-6 + 109*t2 + 52*Power(t2,2) + 430*Power(t2,3)))))\
)*R2(1 - s1 - t1 + t2))/
     ((s - s2 + t1)*Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*
       (-1 + s1 + t1 - t2)*Power(s1*t1 - t2,2)*(-1 + t2)*(s - s1 + t2)) - 
    (8*(2*Power(s1,12)*(s2 - 3*t1)*Power(t1,2) + 
         Power(-1 + s,3)*Power(t1,12) + 
         2*Power(t1,3)*t2*(-10 + 
            5*(-6 + 18*Power(s,2) - s2 + s*(-28 + 6*s2))*t2 - 
            (259 + 85*Power(s,3) + 158*s2 - 25*Power(s2,2) - 
               4*Power(s2,3) + Power(s,2)*(-459 + 50*s2) + 
               s*(169 - 308*s2 + 21*Power(s2,2)))*Power(t2,2) - 
            (310 + 84*Power(s,3) + 893*s2 + 18*Power(s2,2) - 
               15*Power(s2,3) + Power(s,2)*(-80 + 457*s2) + 
               s*(-963 - 1498*s2 + 98*Power(s2,2)))*Power(t2,3) + 
            (-282 + 135*Power(s,3) - 367*s2 - 264*Power(s2,2) + 
               22*Power(s2,3) - 2*Power(s,2)*(713 + 212*s2) + 
               3*s*(843 + 350*s2 + 49*Power(s2,2)))*Power(t2,4) + 
            (492 + 106*Power(s,3) + 118*s2 + 18*Power(s2,2) - 
               9*Power(s2,3) - Power(s,2)*(370 + 221*s2) + 
               s*(5 + 152*s2 + 124*Power(s2,2)))*Power(t2,5) + 
            (111 + 39*Power(s,2) + 89*s2 + 39*Power(s2,2) - 
               2*s*(82 + 39*s2))*Power(t2,6)) + 
         Power(t1,5)*(2 + (6 - 60*Power(s,2) + s*(91 - 6*s2) + s2)*t2 + 
            (35 + 80*Power(s,3) + 116*s2 - 2*Power(s2,2) + 
               Power(s,2)*(-571 + 24*s2) + 
               s*(531 - 195*s2 + 4*Power(s2,2)))*Power(t2,2) + 
            (107 + 360*Power(s,3) + 704*s2 + 20*Power(s2,2) - 
               5*Power(s2,3) + Power(s,2)*(-1226 + 299*s2) + 
               s*(321 - 1126*s2 + 10*Power(s2,2)))*Power(t2,3) + 
            (431 + 30*Power(s,3) + 565*s2 + 377*Power(s2,2) - 
               18*Power(s2,3) + Power(s,2)*(984 + 482*s2) - 
               s*(2370 + 1437*s2 + 274*Power(s2,2)))*Power(t2,4) + 
            (-117 - 162*Power(s,3) + 163*s2 + 102*Power(s2,2) + 
               31*Power(s2,3) + Power(s,2)*(738 + 355*s2) - 
               2*s*(386 + 295*s2 + 112*Power(s2,2)))*Power(t2,5) - 
            (64 + 21*Power(s,2) + 29*s2 + 21*Power(s2,2) - 
               s*(79 + 42*s2))*Power(t2,6)) + 
         4*t1*Power(t2,2)*(15 + 
            (72 - 27*Power(s,2) + s*(18 - 54*s2) + 48*s2 + 
               9*Power(s2,2))*t2 + 
            (183 + 14*Power(s,3) + 118*s2 + 18*Power(s2,2) + 
               4*Power(s2,3) + Power(s,2)*(-62 + 72*s2) - 
               2*s*(74 + 122*s2 + 5*Power(s2,2)))*Power(t2,2) - 
            2*(-15 + 22*Power(s,3) - 143*s2 + 33*Power(s2,2) + 
               11*Power(s2,3) - Power(s,2)*(129 + 65*s2) + 
               s*(295 + 232*s2 - 80*Power(s2,2)))*Power(t2,3) - 
            (91 + 26*Power(s,3) + 210*s2 + 36*Power(s2,2) + 
               16*Power(s2,3) - 4*Power(s,2)*(77 + 9*s2) - 
               2*s*(-255 + 64*s2 + 11*Power(s2,2)))*Power(t2,4) + 
            (-348 - 24*Power(s,3) - 150*s2 - 23*Power(s2,2) + 
               2*Power(s2,3) + Power(s,2)*(33 + 50*s2) + 
               s*(308 + 30*s2 - 28*Power(s2,2)))*Power(t2,5) + 
            (5 - 14*Power(s,2) - 28*s2 - 14*Power(s2,2) + 
               s*(58 + 28*s2))*Power(t2,6) + 6*Power(t2,7)) - 
         (-1 + s)*Power(t1,11)*
          (4 + (2 + 3*s2)*t2 + Power(s,2)*(1 + 4*t2) - 
            s*(5 + 3*(3 + s2)*t2)) + 
         Power(t1,9)*(-6 + (-39 + 7*s2)*t2 + 
            (-7 - 15*s2 + 8*Power(s2,2))*Power(t2,2) + 
            (5 + 6*s2 + 4*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            Power(s,3)*(1 + 22*t2 + 41*Power(t2,2) - 4*Power(t2,3)) + 
            Power(s,2)*(-12 - (103 + s2)*t2 - 
               (115 + 18*s2)*Power(t2,2) + 3*(7 + 3*s2)*Power(t2,3)) + 
            s*(18 + (118 - 6*s2)*t2 + 
               (68 + 25*s2 - 8*Power(s2,2))*Power(t2,2) - 
               2*(14 + 10*s2 + 3*Power(s2,2))*Power(t2,3))) + 
         Power(t1,8)*(9 + (55 + 8*s2)*t2 + 
            (55 + 114*s2 - 6*Power(s2,2))*Power(t2,2) + 
            (10 + 38*s2 + 24*Power(s2,2) - 2*Power(s2,3))*Power(t2,3) - 
            (7 + 5*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            Power(s,3)*(2 + 28*t2 - 7*Power(t2,2) - 65*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*(-3 + (-30 + 13*s2)*t2 + 
               4*(43 + 21*s2)*Power(t2,2) + 
               3*(77 + 30*s2)*Power(t2,3) - (10 + 3*s2)*Power(t2,4)) + 
            s*(-9 - 2*(34 + 11*s2)*t2 + 
               (-273 - 193*s2 + 6*Power(s2,2))*Power(t2,2) - 
               (187 + 145*s2 + 23*Power(s2,2))*Power(t2,3) + 
               (20 + 13*s2 + 3*Power(s2,2))*Power(t2,4))) + 
         Power(t1,10)*(-3 + t2 - 10*s2*t2 - 
            (3 + 5*s2 + 3*Power(s2,2))*Power(t2,2) + 
            Power(s,3)*(-3 - 8*t2 + 6*Power(t2,2)) + 
            Power(s,2)*(6 + (13 - 7*s2)*t2 - (23 + 9*s2)*Power(t2,2)) + 
            s*t2*(-3 + 22*t2 + 3*Power(s2,2)*t2 + 17*s2*(1 + t2))) + 
         2*Power(s1,11)*t1*(Power(s2,2)*(1 + t1) + 
            s2*(6*Power(t1,2) - 2*t2 - t1*(4 + s + 4*t2)) + 
            t1*(-1 - 25*Power(t1,2) + (9 + s)*t2 + t1*(6 + 4*s + 13*t2))) \
+ Power(t1,7)*(Power(s,3)*t2*
             (-15 - 148*t2 - 122*Power(t2,2) + 44*Power(t2,3)) + 
            Power(s,2)*(6 + (141 - 2*s2)*t2 + 
               (593 - 24*s2)*Power(t2,2) + (159 - 48*s2)*Power(t2,3) - 
               2*(110 + 49*s2)*Power(t2,4) + 2*Power(t2,5)) + 
            s*(-9 + 18*(-10 + s2)*t2 + 
               (-485 + 154*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (222 + 228*s2 + 83*Power(s2,2))*Power(t2,3) + 
               (281 + 198*s2 + 64*Power(s2,2))*Power(t2,4) - 
               (7 + 4*s2)*Power(t2,5)) + 
            t2*(39 + 98*t2 - 77*Power(t2,2) + 
               2*Power(s2,3)*(1 - 5*t2)*Power(t2,2) - 29*Power(t2,3) + 
               5*Power(t2,4) + 
               Power(s2,2)*t2*
                (-2 - 97*t2 - 38*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-11 - 117*t2 - 80*Power(t2,2) - 66*Power(t2,3) + 
                  2*Power(t2,4)))) - 
         8*Power(t2,3)*(2*Power(s2,2)*t2*
             (1 + 9*t2 + 29*Power(t2,2) - Power(t2,3) + 2*Power(t2,4)) + 
            Power(s2,3)*t2*(1 - 7*t2 - 11*Power(t2,2) + 7*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s,3)*(t2 - 3*Power(t2,2) - 7*Power(t2,3) + 
               3*Power(t2,4) - 2*Power(t2,5)) + 
            2*t2*(6 + t2 + 42*Power(t2,2) - 16*Power(t2,3) - 
               32*Power(t2,4) - Power(t2,5)) - 
            s2*(-9 - 33*t2 - 92*Power(t2,2) + 28*Power(t2,3) + 
               Power(t2,4) + 5*Power(t2,5) + 4*Power(t2,6)) + 
            s*(9 - 3*(-11 + 4*s2 + 3*Power(s2,2))*t2 + 
               (100 - 184*s2 + 35*Power(s2,2))*Power(t2,2) + 
               (-132 - 56*s2 + 7*Power(s2,2))*Power(t2,3) - 
               (113 + 19*Power(s2,2))*Power(t2,4) + 
               (3 - 20*s2 - 6*Power(s2,2))*Power(t2,5) + 4*Power(t2,6)) \
+ Power(s,2)*t2*(3*s2*(-3 + 13*t2 + 9*Power(t2,2) + 3*Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*(1 - 21*t2 + 3*Power(t2,2) + 9*Power(t2,3) + 
                  8*Power(t2,4)))) - 
         2*Power(t1,4)*t2*(-25 + 5*(-31 + 2*s2)*t2 + 
            (-391 - 3*s2 + 27*Power(s2,2))*Power(t2,2) + 
            (46 - 249*s2 + 244*Power(s2,2) - 6*Power(s2,3))*
             Power(t2,3) + (425 + 334*s2 + 123*Power(s2,2) + 
               36*Power(s2,3))*Power(t2,4) + 
            (61 + 137*s2 + 58*Power(s2,2) + 20*Power(s2,3))*
             Power(t2,5) - (9 + 5*s2)*Power(t2,6) + 
            Power(s,3)*t2*(-30 + 15*t2 + 278*Power(t2,2) + 
               143*Power(t2,3) - 20*Power(t2,4)) + 
            s*(30 + 25*(10 + s2)*t2 + 
               (1290 + 127*s2 - 27*Power(s2,2))*Power(t2,2) + 
               (1620 + 119*s2 - 160*Power(s2,2))*Power(t2,3) - 
               (787 + 365*s2 + 99*Power(s2,2))*Power(t2,4) - 
               2*(188 + 123*s2 + 30*Power(s2,2))*Power(t2,5) + 
               5*Power(t2,6)) + 
            Power(s,2)*t2*(25 - 550*t2 - 1613*Power(t2,2) - 
               130*Power(t2,3) + 188*Power(t2,4) + 
               2*s2*(-15 - 78*t2 - 86*Power(t2,2) - 40*Power(t2,3) + 
                  30*Power(t2,4)))) + 
         Power(t1,6)*(-5 + 2*(-47 + s2)*t2 + 
            (-338 - 63*s2 + 5*Power(s2,2))*Power(t2,2) + 
            (-201 - 424*s2 + 91*Power(s2,2))*Power(t2,3) + 
            (146 + 19*s2 - 13*Power(s2,2) + 20*Power(s2,3))*
             Power(t2,4) + (69 + 67*s2 + 31*Power(s2,2) + 
               11*Power(s2,3))*Power(t2,5) - (1 + s2)*Power(t2,6) + 
            Power(s,3)*t2*(-20 - 67*t2 + 209*Power(t2,2) + 
               232*Power(t2,3) - 11*Power(t2,4)) + 
            s*(6 + (113 + 5*s2)*t2 + 
               (737 + 182*s2 - 7*Power(s2,2))*Power(t2,2) + 
               (1669 + 655*s2 - 77*Power(s2,2))*Power(t2,3) + 
               (221 + 221*s2 + 18*Power(s2,2))*Power(t2,4) - 
               (219 + 137*s2 + 33*Power(s2,2))*Power(t2,5) + Power(t2,6)\
) + Power(s,2)*t2*(26 - 153*t2 - 1377*Power(t2,2) - 674*Power(t2,3) + 
               106*Power(t2,4) + 
               s2*(-6 - 122*t2 - 326*Power(t2,2) - 270*Power(t2,3) + 
                  33*Power(t2,4)))) + 
         2*Power(t1,2)*Power(t2,2)*
          (-75 - (198 + 27*s2 + 37*Power(s2,2) + 2*Power(s2,3))*t2 + 
            (-231 + 307*s2 + 62*Power(s2,2) - 19*Power(s2,3))*
             Power(t2,2) + (608 + 192*s2 + 612*Power(s2,2) - 
               5*Power(s2,3))*Power(t2,3) + 
            (603 + 652*s2 + 210*Power(s2,2) + 43*Power(s2,3))*
             Power(t2,4) + (-202 + 59*s2 + 33*Power(s2,2) + 
               23*Power(s2,3))*Power(t2,5) - (57 + 31*s2)*Power(t2,6) + 
            Power(s,3)*t2*(-20 + 93*t2 + 139*Power(t2,2) + 
               43*Power(t2,3) - 23*Power(t2,4)) + 
            s*(90 + 3*(167 + 42*s2)*t2 + 
               (1561 - 380*s2 + 19*Power(s2,2))*Power(t2,2) + 
               (388 - 1048*s2 - 299*Power(s2,2))*Power(t2,3) - 
               (1906 + 764*s2 + 171*Power(s2,2))*Power(t2,4) - 
               (281 + 222*s2 + 69*Power(s2,2))*Power(t2,5) + 
               31*Power(t2,6)) + 
            Power(s,2)*t2*(-49 - 850*t2 - 948*Power(t2,2) + 
               442*Power(t2,3) + 189*Power(t2,4) + 
               s2*(-90 - 61*t2 + 245*Power(t2,2) + 85*Power(t2,3) + 
                  69*Power(t2,4)))) + 
         Power(s1,10)*(Power(s2,3) + 
            Power(s2,2)*(16*Power(t1,3) - 2*t2 - 
               5*Power(t1,2)*(-3 + 2*t2) - t1*(5 + s + 11*t2)) - 
            t1*(184*Power(t1,4) - Power(t1,3)*(94 + 69*s + 183*t2) + 
               2*t2*(-2 + 9*t2 + 2*s*t2) + 
               2*Power(t1,2)*
                (-3 + 2*s + Power(s,2) - 84*t2 + 14*s*t2 + 
                  21*Power(t2,2)) + 
               2*t1*(-4 + s + 14*t2 + 15*s*t2 + Power(s,2)*t2 + 
                  39*Power(t2,2) + 3*s*Power(t2,2))) + 
            s2*(24*Power(t1,4) + 2*Power(t2,2) - 
               Power(t1,3)*(61 + 17*s + 25*t2) + 
               4*t1*(-1 + 2*(2 + s)*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(3*s*(1 + 4*t2) + t2*(-17 + 12*t2)))) - 
         Power(s1,8)*(532*Power(t1,7) - 
            Power(t1,6)*(600 + 588*s + 955*t2) + 
            2*t2*(1 - (4 + s)*t2 + 
               (2 + 7*s + 2*Power(s,2))*Power(t2,2) + 
               (13 + 3*s)*Power(t2,3)) + 
            Power(t1,5)*(-315 + 88*Power(s,2) - 1445*t2 + 
               574*Power(t2,2) + s*(135 + 869*t2)) + 
            Power(s2,3)*(-2 + 7*t2 - Power(t2,2) + 
               Power(t1,3)*(-12 + 11*t2) + 
               t1*(19 + 16*t2 - 7*Power(t2,2)) - 
               2*Power(t1,2)*(15 - 8*t2 + 3*Power(t2,2))) - 
            Power(t1,4)*(191 + Power(s,3) - 2040*t2 - 
               2576*Power(t2,2) + 131*Power(t2,3) + 
               Power(s,2)*(-39 + 101*t2) + 
               s*(-235 - 1284*t2 + 388*Power(t2,2))) + 
            Power(t1,3)*(54 + 511*t2 + 576*Power(t2,2) - 
               1008*Power(t2,3) + 8*Power(t2,4) + 
               Power(s,3)*(-2 + 3*t2) + 
               Power(s,2)*(-14 - 101*t2 + 6*Power(t2,2)) + 
               s*(-11 - 306*t2 - 1112*Power(t2,2) + 42*Power(t2,3))) + 
            Power(t1,2)*(-12 + 205*t2 - 789*Power(t2,2) - 
               1085*Power(t2,3) + 90*Power(t2,4) + 
               Power(s,3)*t2*(-3 + 2*t2) + 
               Power(s,2)*t2*(-26 + 5*t2 + 10*Power(t2,2)) + 
               s*(-4 - 85*t2 - 559*Power(t2,2) + 140*Power(t2,3) + 
                  2*Power(t2,4))) + 
            t1*(5 + 3*t2 - 56*Power(t2,2) + Power(s,3)*Power(t2,2) - 
               128*Power(t2,3) + 126*Power(t2,4) + 
               Power(s,2)*t2*(5 + 10*t2 + 19*Power(t2,2)) + 
               s*(-2 + 5*t2 - 8*Power(t2,2) + 106*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s2,2)*(-9 - 112*Power(t1,5) + (-7 + 12*s)*t2 + 
               7*(-1 + s)*Power(t2,2) + 16*Power(t2,3) + 
               3*Power(t1,4)*(-10 + 43*t2) + 
               Power(t1,3)*(147 + s*(53 - 21*t2) + 303*t2 - 
                  60*Power(t2,2)) + 
               Power(t1,2)*(3 + 30*t2 - 217*Power(t2,2) + 
                  14*Power(t2,3) + s*(-8 - 35*t2 + 14*Power(t2,2))) + 
               t1*(24 - 114*t2 - 153*Power(t2,2) + 32*Power(t2,3) + 
                  s*(-1 - 50*t2 + 16*Power(t2,2)))) + 
            s2*(-3 + 84*Power(t1,6) + 
               Power(t1,5)*(334 + 140*s - 305*t2) + 5*(2 + s)*t2 + 
               (14 - 14*s - 5*Power(s,2))*Power(t2,2) - 
               (7 + 22*s)*Power(t2,3) - 12*Power(t2,4) - 
               2*Power(t1,4)*
                (40 + 4*Power(s,2) + 158*t2 - 108*Power(t2,2) + 
                  s*(97 + 43*t2)) + 
               Power(t1,3)*(58 - 810*t2 + 185*Power(t2,2) - 
                  29*Power(t2,3) + Power(s,2)*(-2 + 8*t2) + 
                  2*s*(-9 - 117*t2 + 29*Power(t2,2))) + 
               t1*(-2 - 38*t2 + Power(s,2)*(3 - 10*t2)*t2 + 
                  271*Power(t2,2) + 75*Power(t2,3) - 16*Power(t2,4) + 
                  s*(-12 + 18*t2 + 79*Power(t2,2) - 54*Power(t2,3))) + 
               Power(t1,2)*(-78 - 23*t2 + 366*Power(t2,2) + 
                  31*Power(t2,3) - 2*Power(t2,4) + 
                  Power(s,2)*(1 + 25*t2 - 10*Power(t2,2)) + 
                  s*(13 + 228*t2 + 245*Power(t2,2) - 24*Power(t2,3))))) + 
         Power(s1,9)*(-392*Power(t1,6) + 
            Power(t1,5)*(317 + 264*s + 557*t2) + 
            2*Power(t2,2)*(-1 + (3 + s)*t2) + 
            2*t1*(1 - 8*t2 + (10 + 18*s + 3*Power(s,2))*Power(t2,2) + 
               (39 + 6*s)*Power(t2,3)) - 
            Power(s2,3)*(3 + 2*Power(t1,2)*(-1 + t2) + 2*t2 + 
               t1*(-9 + 2*t2)) - 
            Power(t1,4)*(-91 + 20*Power(s,2) - 657*t2 + 
               240*Power(t2,2) + 37*s*(1 + 7*t2)) + 
            Power(s2,2)*(-3 + 56*Power(t1,4) + 
               Power(t1,3)*(39 - 57*t2) + 5*t2 + 4*s*t2 + 
               9*Power(t2,2) + 
               2*Power(t1,2)*
                (-22 + 2*s*(-3 + t2) - 46*t2 + 9*Power(t2,2)) + 
               t1*(-1 + (-22 + 4*s)*t2 + 26*Power(t2,2))) + 
            Power(t1,3)*(59 - 451*t2 - 725*Power(t2,2) + 
               30*Power(t2,3) + Power(s,2)*(-6 + 5*t2) + 
               s*(-40 - 309*t2 + 47*Power(t2,2))) + 
            Power(t1,2)*(-2 - 37*t2 - 216*Power(t2,2) + 
               126*Power(t2,3) + 
               Power(s,2)*(2 + 5*t2 + 8*Power(t2,2)) + 
               s*(8 + 7*t2 + 95*Power(t2,2) + 6*Power(t2,3))) - 
            s2*(2*Power(t1,4)*(97 + 32*s - 19*t2) + 
               2*t2*(-2 + (4 + 3*s)*t2 + 4*Power(t2,2)) - 
               Power(t1,3)*(15 + Power(s,2) + 33*t2 - Power(t2,2) + 
                  s*(41 + 59*t2)) + 
               Power(t1,2)*(27 - 221*t2 - 75*Power(t2,2) + 
                  8*Power(t2,3) + Power(s,2)*(-1 + 2*t2) + 
                  s*(-3 - 73*t2 + 26*Power(t2,2))) + 
               t1*(2*Power(s,2)*t2 + s*(1 + 17*t2 + 34*Power(t2,2)) + 
                  2*(-5 - 7*t2 + Power(t2,2) + 12*Power(t2,3))))) + 
         Power(s1,7)*(-1 - 476*Power(t1,8) + 5*t2 + s*t2 + 
            5*Power(t2,2) - 3*s*Power(t2,2) - 25*Power(t2,3) - 
            11*s*Power(t2,3) + 7*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) - 30*Power(t2,4) + 
            39*s*Power(t2,4) + 11*Power(s,2)*Power(t2,4) + 
            42*Power(t2,5) + 6*s*Power(t2,5) + 
            5*Power(t1,7)*(139 + 168*s + 201*t2) - 
            Power(t1,6)*(-558 + 224*Power(s,2) - 1997*t2 + 
               740*Power(t2,2) + s*(252 + 1595*t2)) + 
            Power(t1,5)*(359 + 8*Power(s,3) - 4425*t2 - 
               4784*Power(t2,2) + 225*Power(t2,3) + 
               Power(s,2)*(-109 + 403*t2) + 
               s*(-701 - 2953*t2 + 1041*Power(t2,2))) - 
            Power(s2,3)*(6*Power(t1,4)*(-5 + 4*t2) + 
               Power(t1,3)*(-50 + 46*t2 - 23*Power(t2,2)) + 
               t2*(-29 - 25*t2 + Power(t2,2)) + 
               Power(t1,2)*(50 + 66*t2 - 55*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-10 + 53*t2 - 25*Power(t2,2) + 9*Power(t2,3))) - 
            Power(t1,4)*(248 + 1868*t2 + 568*Power(t2,2) - 
               2875*Power(t2,3) + 24*Power(t2,4) + 
               Power(s,3)*(-14 + 25*t2) + 
               Power(s,2)*(-59 - 532*t2 + 198*Power(t2,2)) + 
               s*(93 - 1598*t2 - 4229*Power(t2,2) + 259*Power(t2,3))) + 
            Power(t1,3)*(-50 - 556*t2 + 5088*Power(t2,2) + 
               4518*Power(t2,3) - 575*Power(t2,4) + 
               Power(s,3)*t2*(4 + t2) + 
               Power(s,2)*(29 + 118*t2 - 407*Power(t2,2) + 
                  9*Power(t2,3)) + 
               3*s*(15 + 323*t2 + 747*Power(t2,2) - 531*Power(t2,3) + 
                  5*Power(t2,4))) + 
            t1*(-1 - 16*t2 + 239*Power(t2,2) - 601*Power(t2,3) - 
               727*Power(t2,4) + 90*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(-7 + 6*t2) + 
               Power(s,2)*t2*
                (16 - 47*t2 + 2*Power(t2,2) + 18*Power(t2,3)) + 
               s*(-12 - 66*Power(t2,2) - 469*Power(t2,3) + 
                  139*Power(t2,4) + 4*Power(t2,5))) + 
            Power(t1,2)*(Power(s,3)*t2*(-7 + 4*Power(t2,2)) + 
               Power(s,2)*(-13 - 24*t2 - 255*Power(t2,2) + 
                  5*Power(t2,3) + 4*Power(t2,4)) + 
               s*(-10 + 77*t2 - 691*Power(t2,2) - 1851*Power(t2,3) + 
                  111*Power(t2,4)) + 
               2*(-17 + 69*t2 + 546*Power(t2,2) - 170*Power(t2,3) - 
                  793*Power(t2,4) + 12*Power(t2,5))) + 
            Power(s2,2)*(-6 + 140*Power(t1,6) + 2*(9 + 7*s)*t2 - 
               (67 + 38*s)*Power(t2,2) + (-77 + 6*s)*Power(t2,3) + 
               14*Power(t2,4) - Power(t1,5)*(51 + 137*t2) + 
               t1*(58 + (39 - 99*s)*t2 - (119 + 83*s)*Power(t2,2) + 
                  (-299 + 27*s)*Power(t2,3) + 20*Power(t2,4)) + 
               Power(t1,4)*(-254 - 528*t2 + 19*Power(t2,2) + 
                  s*(-122 + 41*t2)) + 
               Power(t1,3)*(10 + 146*t2 + 598*Power(t2,2) - 
                  13*Power(t2,3) + s*(42 + 100*t2 - 47*Power(t2,2))) + 
               Power(t1,2)*(-77 + 596*t2 + 551*Power(t2,2) - 
                  201*Power(t2,3) + 4*Power(t2,4) + 
                  s*(5 + 243*t2 - 121*Power(t2,2) + 16*Power(t2,3)))) - 
            s2*(9 + 168*Power(t1,7) + 
               2*Power(t1,6)*(169 + 98*s - 320*t2) + (8 - 12*s)*t2 + 
               (11 - 21*s + 13*Power(s,2))*Power(t2,2) + 
               (-111 - 23*s + 8*Power(s,2))*Power(t2,3) + 
               (-25 + 28*s)*Power(t2,4) + 8*Power(t2,5) - 
               Power(t1,5)*(184 + 28*Power(s,2) + 795*t2 - 
                  646*Power(t2,2) - 8*s*(-59 + 7*t2)) + 
               Power(t1,4)*(20 + 14*Power(s,2) - 1268*t2 + 
                  1476*Power(t2,2) - 215*Power(t2,3) - 
                  s*(69 + 371*t2 + 150*Power(t2,2))) + 
               Power(t1,3)*(-225 + 302*t2 + 1341*Power(t2,2) - 
                  776*Power(t2,3) + 15*Power(t2,4) + 
                  Power(s,2)*(4 + 94*t2 - 23*Power(t2,2)) + 
                  s*(97 + 1008*t2 + 461*Power(t2,2) - 4*Power(t2,3))) + 
               Power(t1,2)*(-11 + 9*t2 + 1335*Power(t2,2) - 
                  447*Power(t2,3) + 55*Power(t2,4) + 
                  Power(s,2)*t2*(4 - 69*t2 + 14*Power(t2,2)) + 
                  s*(-75 + 156*t2 + 93*Power(t2,2) - 220*Power(t2,3) + 
                     8*Power(t2,4))) + 
               t1*(-21 + 202*t2 + 125*Power(t2,2) - 529*Power(t2,3) - 
                  65*Power(t2,4) + 4*Power(t2,5) + 
                  Power(s,2)*t2*(-16 - 63*t2 + 24*Power(t2,2)) + 
                  s*(17 - 9*t2 - 375*Power(t2,2) - 345*Power(t2,3) + 
                     38*Power(t2,4))))) + 
         Power(s1,3)*(-2*Power(t1,12) + 
            Power(t1,11)*(5 + 48*s - 4*s2 + 5*t2) + 
            2*t2*(10 - 5*(-3 + 26*s2 + 18*Power(s2,2) + s*(-1 + 6*s2))*
                t2 + (54 + 4*Power(s,3) - 35*s2 + 15*Power(s,2)*s2 - 
                  158*Power(s2,2) + 15*Power(s2,3) + 
                  2*s*(-62 + 55*s2 + 115*Power(s2,2)))*Power(t2,2) + 
               (-222 + 3*Power(s,3) + Power(s,2)*(181 - 242*s2) + 
                  351*s2 - 297*Power(s2,2) + 100*Power(s2,3) + 
                  s*(-101 + 212*s2 + 27*Power(s2,2)))*Power(t2,3) + 
               (-234 + 6*Power(s,3) + Power(s,2)*(39 - 105*s2) + 
                  121*s2 + 69*Power(s2,2) - 21*Power(s2,3) + 
                  s*(629 - 404*s2 + 96*Power(s2,2)))*Power(t2,4) + 
               (683 + 3*Power(s,3) - 201*s2 + 99*Power(s2,2) - 
                  6*Power(s2,3) - Power(s,2)*(53 + 12*s2) + 
                  s*(292 - 126*s2 + 15*Power(s2,2)))*Power(t2,5) + 
               (258 + 9*Power(s,2) + 54*s2 + 9*Power(s2,2) - 
                  s*(125 + 18*s2))*Power(t2,6) - 20*Power(t2,7)) + 
            2*Power(t1,2)*(-10 + 
               (132 - 24*Power(s,2) + 223*s2 + 72*Power(s2,2) - 
                  s*(101 + 6*s2))*t2 - 
               (58*Power(s,3) - 5*Power(s,2)*(-23 + 36*s2) + 
                  s*(-621 + 3*s2 + 242*Power(s2,2)) + 
                  2*(-226 + 149*s2 + Power(s2,2) + 42*Power(s2,3)))*
                Power(t2,2) + 
               (2697 - 136*Power(s,3) + 186*s2 + 935*Power(s2,2) - 
                  339*Power(s2,3) + Power(s,2)*(-1109 + 453*s2) + 
                  2*s*(-466 - 263*s2 + 575*Power(s2,2)))*Power(t2,3) + 
               (1195 + 248*Power(s,3) + 1152*s2 - 225*Power(s2,2) + 
                  226*Power(s2,3) + 2*Power(s,2)*(-101 + 63*s2) + 
                  s*(-10308 + 1823*s2 - 468*Power(s2,2)))*Power(t2,4) + 
               (-9349 - 102*Power(s,3) + 605*s2 + 77*Power(s2,2) + 
                  49*Power(s2,3) + Power(s,2)*(2027 + 253*s2) - 
                  s*(2929 + 1602*s2 + 200*Power(s2,2)))*Power(t2,5) - 
               (2585 + 105*Power(s,2) + 1476*s2 + 105*Power(s2,2) - 
                  3*s*(699 + 70*s2))*Power(t2,6) + 204*Power(t2,7)) + 
            Power(t1,10)*(-128*Power(s,2) + 2*Power(s2,2) + 
               s*(80 - 10*s2 - 139*t2) + (85 - 4*t2)*t2 + 
               2*s2*(-7 + 10*t2)) + 
            Power(t1,9)*(91 + 56*Power(s,3) - 257*t2 - 
               238*Power(t2,2) + Power(t2,3) + 
               3*Power(s2,2)*(-5 + 7*t2) + 
               Power(s,2)*(-99 + 28*s2 + 401*t2) + 
               s2*(30 + 181*t2 - 36*Power(t2,2)) + 
               s*(-375 - 1019*t2 + 143*Power(t2,2) - 8*s2*(-13 + 23*t2))\
) + Power(t1,8)*(34 + Power(s,3)*(22 - 203*t2) - 610*t2 - 
               494*Power(t2,2) + 211*Power(t2,3) + 
               Power(s2,3)*(2 + 4*t2) - 
               Power(s2,2)*(22 + 8*t2 + 95*Power(t2,2)) + 
               s2*(62 - 82*t2 - 646*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s,2)*(409 + 1376*t2 - 466*Power(t2,2) + 
                  2*s2*(-49 + 86*t2)) + 
               s*(-609 + 764*t2 + 2473*Power(t2,2) - 59*Power(t2,3) - 
                  Power(s2,2)*(18 + 29*t2) + 
                  3*s2*(61 - 17*t2 + 154*Power(t2,2)))) + 
            Power(t1,7)*(-304 - 752*t2 + 3808*Power(t2,2) + 
               2270*Power(t2,3) - 63*Power(t2,4) - 
               7*Power(s2,3)*t2*(-2 + 7*t2) + 
               Power(s,3)*(-60 - 312*t2 + 263*Power(t2,2)) + 
               Power(s2,2)*(52 + 194*t2 - 244*Power(t2,2) + 
                  125*Power(t2,3)) + 
               s2*(71 - 946*t2 - 577*Power(t2,2) + 936*Power(t2,3) - 
                  7*Power(t2,4)) - 
               Power(s,2)*(-285 + 676*t2 + 3515*Power(t2,2) - 
                  241*Power(t2,3) + s2*(6 + 170*t2 + 485*Power(t2,2))) \
+ s*(471 + 5345*t2 + 3973*Power(t2,2) - 2389*Power(t2,3) + 
                  7*Power(t2,4) + 
                  Power(s2,2)*(18 - 120*t2 + 271*Power(t2,2)) - 
                  s2*(343 + 630*t2 - 1621*Power(t2,2) + 366*Power(t2,3))\
)) - 2*t1*t2*(70 - 109*t2 - 613*Power(t2,2) + 1321*Power(t2,3) + 
               3571*Power(t2,4) - 704*Power(t2,5) - 720*Power(t2,6) + 
               Power(s,3)*t2*
                (-6 - 63*t2 + 22*Power(t2,2) + 57*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-30 - 69*t2 - 138*Power(t2,2) + 83*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-267 - 150*t2 + 1395*Power(t2,2) + 568*Power(t2,3) + 
                  94*Power(t2,4)) + 
               s2*(90 - 10*t2 + 816*Power(t2,2) - 803*Power(t2,3) - 
                  13*Power(t2,4) + 1033*Power(t2,5) - 89*Power(t2,6)) + 
               s*(-30 + (200 - 82*s2 - 90*Power(s2,2))*t2 + 
                  3*(334 - 350*s2 + 321*Power(s2,2))*Power(t2,2) + 
                  (1253 - 1096*s2 + 506*Power(s2,2))*Power(t2,3) - 
                  (1945 + 130*s2 + 197*Power(s2,2))*Power(t2,4) - 
                  15*(151 + 22*s2 + 2*Power(s2,2))*Power(t2,5) + 
                  89*Power(t2,6)) + 
               Power(s,2)*t2*
                (125 - 80*t2 - 251*Power(t2,2) - 886*Power(t2,3) + 
                  236*Power(t2,4) + 
                  s2*(30 + 33*t2 - 390*Power(t2,2) + 57*Power(t2,3) + 
                     30*Power(t2,4)))) + 
            Power(t1,6)*(28 + 1078*t2 + 4784*Power(t2,2) - 
               2304*Power(t2,3) - 2316*Power(t2,4) + 
               Power(s2,3)*(-4 - 86*t2 + 11*Power(t2,2) + 
                  78*Power(t2,3)) - 
               Power(s,3)*(30 + 27*t2 - 966*Power(t2,2) + 
                  142*Power(t2,3)) + 
               s2*(-241 + 95*t2 + 2245*Power(t2,2) + 
                  3471*Power(t2,3) - 637*Power(t2,4)) + 
               Power(s2,2)*(-11 + 76*t2 + 465*Power(t2,2) + 
                  873*Power(t2,3) - 48*Power(t2,4)) + 
               Power(s,2)*(-213 - 2652*t2 - 3179*Power(t2,2) + 
                  3651*Power(t2,3) - 48*Power(t2,4) + 
                  s2*(50 + 456*t2 - 635*Power(t2,2) + 362*Power(t2,3))) \
+ s*(586 + 2501*t2 - 11951*Power(t2,2) - 11495*Power(t2,3) + 
                  1023*Power(t2,4) + 
                  Power(s2,2)*
                   (1 + 523*t2 + 3*Power(t2,2) - 298*Power(t2,3)) + 
                  s2*(-13 - 1236*t2 - 565*Power(t2,2) - 
                     3752*Power(t2,3) + 96*Power(t2,4)))) - 
            2*Power(t1,3)*(-10 + 355*t2 + 986*Power(t2,2) - 
               3039*Power(t2,3) - 11761*Power(t2,4) - 126*Power(t2,5) + 
               1411*Power(t2,6) + 
               Power(s2,3)*t2*
                (16 + 66*t2 + 115*Power(t2,2) - 38*Power(t2,3) - 
                  25*Power(t2,4)) + 
               Power(s,3)*t2*
                (16 + 67*t2 + 111*Power(t2,2) - 493*Power(t2,3) + 
                  25*Power(t2,4)) - 
               Power(s2,2)*t2*
                (-182 + 14*t2 + 1765*Power(t2,2) + 868*Power(t2,3) + 
                  743*Power(t2,4)) + 
               s*(-6 + (-455 - 40*s2 + 6*Power(s2,2))*t2 + 
                  (-3329 + 1576*s2 - 923*Power(s2,2))*Power(t2,2) + 
                  (-6830 + 2305*s2 - 161*Power(s2,2))*Power(t2,3) + 
                  (7252 + 188*s2 - 187*Power(s2,2))*Power(t2,4) + 
                  (7373 + 2063*s2 + 75*Power(s2,2))*Power(t2,5) - 
                  309*Power(t2,6)) + 
               s2*(-6 + 247*t2 - 771*Power(t2,2) + 1157*Power(t2,3) - 
                  3298*Power(t2,4) - 3438*Power(t2,5) + 309*Power(t2,6)\
) + Power(s,2)*t2*(s2*(6 + 62*t2 + 331*Power(t2,2) + 718*Power(t2,3) - 
                     75*Power(t2,4)) - 
                  2*(74 + 49*t2 - 1281*Power(t2,2) - 1672*Power(t2,3) + 
                     660*Power(t2,4)))) + 
            Power(t1,5)*(241 + 1716*t2 - 109*Power(t2,2) - 
               17661*Power(t2,3) - 5611*Power(t2,4) + 936*Power(t2,5) - 
               Power(s2,2)*t2*
                (165 + 1177*t2 + 63*Power(t2,2) + 974*Power(t2,3)) + 
               Power(s2,3)*(2 + t2 + 41*Power(t2,2) + 
                  149*Power(t2,3) - 26*Power(t2,4)) + 
               Power(s,3)*(20 + 204*t2 + 485*Power(t2,2) - 
                  1066*Power(t2,3) + 26*Power(t2,4)) + 
               Power(s,2)*(-162 - 2*(395 + 4*s2)*t2 + 
                  (4731 + 427*s2)*Power(t2,2) + 
                  4*(2137 + 470*s2)*Power(t2,3) - 
                  2*(850 + 39*s2)*Power(t2,4)) + 
               s2*(61 - 160*t2 + 3383*Power(t2,2) - 2901*Power(t2,3) - 
                  5001*Power(t2,4) + 170*Power(t2,5)) + 
               s*(-182 - 3918*t2 - 17398*Power(t2,2) + 
                  1841*Power(t2,3) + 10755*Power(t2,4) - 
                  170*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-301 + 281*t2 - 963*Power(t2,2) + 78*Power(t2,3)) + 
                  s2*(113 + 1811*t2 + 2357*Power(t2,2) - 
                     2661*Power(t2,3) + 2674*Power(t2,4)))) + 
            Power(t1,4)*(-93 - 711*t2 - 5881*Power(t2,2) - 
               8747*Power(t2,3) + 17480*Power(t2,4) + 6996*Power(t2,5) - 
               144*Power(t2,6) + 
               Power(s2,3)*t2*
                (87 + 445*t2 - 279*Power(t2,2) - 234*Power(t2,3)) + 
               2*Power(s,3)*t2*
                (47 + 26*t2 - 636*Power(t2,2) + 220*Power(t2,3)) + 
               Power(s2,2)*(-6 + 186*t2 - 749*Power(t2,2) - 
                  1339*Power(t2,3) - 1542*Power(t2,4) + 284*Power(t2,5)) \
+ Power(s,2)*(48 + (788 - 128*s2)*t2 + (4376 - 693*s2)*Power(t2,2) + 
                  5*(241 + 60*s2)*Power(t2,3) - 
                  (7619 + 1114*s2)*Power(t2,4) + 284*Power(t2,5)) + 
               s2*(23 + 932*t2 - 2231*Power(t2,2) - 5269*Power(t2,3) - 
                  4021*Power(t2,4) + 3018*Power(t2,5)) + 
               s*(-112 - 2107*t2 + 105*Power(t2,2) + 32013*Power(t2,3) + 
                  16023*Power(t2,4) - 4266*Power(t2,5) + 
                  2*Power(s2,2)*t2*
                   (-30 - 1149*t2 + 332*Power(t2,2) + 454*Power(t2,3)) + 
                  s2*(-24 - 164*t2 + 2635*Power(t2,2) + 
                     929*Power(t2,3) + 7508*Power(t2,4) - 568*Power(t2,5)\
)))) - Power(s1,4)*(22*Power(t1,11) - 
            Power(t1,10)*(52 + 204*s - 30*s2 + 57*t2) + 
            t1*(10 - 5*(2 + 81*s2 + 36*Power(s2,2) - 3*s*(11 + 2*s2))*
                t2 + (199 + 24*Power(s,3) + 140*s2 + 2*Power(s2,2) + 
                  180*Power(s2,3) - 5*Power(s,2)*(25 + 36*s2) + 
                  s*(-379 + 75*s2 + 540*Power(s2,2)))*Power(t2,2) + 
               (-1493 + 156*Power(s,3) + Power(s,2)*(664 - 615*s2) + 
                  1446*s2 - 1530*Power(s2,2) + 465*Power(s2,3) + 
                  s*(-477 + 510*s2 - 862*Power(s2,2)))*Power(t2,3) + 
               (-2225 - 54*Power(s,3) + Power(s,2)*(700 - 230*s2) + 
                  655*s2 + 75*Power(s2,2) - 194*Power(s2,3) + 
                  s*(4832 - 2267*s2 + 418*Power(s2,2)))*Power(t2,4) - 
               (-5515 + 2*Power(s,3) + 1697*s2 - 472*Power(s2,2) + 
                  11*Power(s2,3) + Power(s,2)*(584 + 7*s2) + 
                  s*(-3380 + 226*s2 - 20*Power(s2,2)))*Power(t2,5) + 
               (2692 + 29*Power(s,2) + 581*s2 + 29*Power(s2,2) - 
                  s*(1001 + 58*s2))*Power(t2,6) - 144*Power(t2,7)) + 
            Power(t1,9)*(-101 + 280*Power(s,2) - 16*Power(s2,2) + 
               s2*(34 - 143*t2) - 407*t2 + 50*Power(t2,2) + 
               s*(-63 + 44*s2 + 559*t2)) - 
            Power(t1,8)*(224 + 70*Power(s,3) - 1454*t2 - 
               1238*Power(t2,2) + 15*Power(t2,3) + 
               Power(s2,2)*(-66 + 53*t2) + 
               Power(s,2)*(-146 + 56*s2 + 815*t2) + 
               s2*(110 + 508*t2 - 226*Power(t2,2)) + 
               s*(-960 + s2*(326 - 406*t2) - 2524*t2 + 552*Power(t2,2))) \
+ Power(t1,7)*(188 + 2069*t2 + 782*Power(t2,2) - 1162*Power(t2,3) - 
               3*Power(s2,3)*(4 + t2) + 5*Power(s,3)*(-11 + 49*t2) + 
               3*Power(s2,2)*(23 + 39*t2 + 84*Power(t2,2)) + 
               Power(s,2)*(-492 + 154*s2 - 2130*t2 - 213*s2*t2 + 
                  882*Power(t2,2)) + 
               s2*(-148 + 287*t2 + 2029*Power(t2,2) - 
                  149*Power(t2,3)) + 
               s*(832 - 2799*t2 - 6214*Power(t2,2) + 232*Power(t2,3) + 
                  Power(s2,2)*(67 + 41*t2) - 
                  2*s2*(159 + 24*t2 + 487*Power(t2,2)))) - 
            2*t2*(-15 - 15*t2 + 
               (100 - 2*s - 19*Power(s,2))*Power(t2,2) + 
               3*(7 - 72*s - 19*Power(s,2) + 6*Power(s,3))*
                Power(t2,3) + 
               (-484 - 25*s + 109*Power(s,2) + 4*Power(s,3))*
                Power(t2,4) - (82 - 238*s + Power(s,2))*Power(t2,5) + 
               (91 - 3*s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (30 + 45*t2 + 46*Power(t2,2) - 3*Power(t2,3)) + 
               s2*(-30 + 5*(-6 + 13*s)*t2 + 
                  (-241 + 197*s - 61*Power(s,2))*Power(t2,2) + 
                  (81 + 275*s + 60*Power(s,2))*Power(t2,3) + 
                  (258 + 101*s - 13*Power(s,2))*Power(t2,4) + 
                  (-65 + 6*s)*Power(t2,5) + 3*Power(t2,6)) + 
               Power(s2,2)*t2*
                (125 + 90*t2 - 264*Power(t2,2) - 174*Power(t2,3) - 
                  5*Power(t2,4) + 
                  2*s*(15 - 116*t2 - 80*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(t1,6)*(431 + 1042*t2 - 11304*Power(t2,2) - 
               6445*Power(t2,3) + 434*Power(t2,4) - 
               5*Power(s,3)*(-9 - 63*t2 + 56*Power(t2,2)) + 
               Power(s2,3)*(-4 - 8*t2 + 60*Power(t2,2)) - 
               Power(s2,2)*(109 + 554*t2 - 188*Power(t2,2) + 
                  266*Power(t2,3)) + 
               s2*(-142 + 1585*t2 + 755*Power(t2,2) - 
                  2843*Power(t2,3) + 36*Power(t2,4)) + 
               Power(s,2)*(-380 + 633*t2 + 4894*Power(t2,2) - 
                  424*Power(t2,3) + s2*(5 + 192*t2 + 535*Power(t2,2))) \
- s*(632 + 8096*t2 + 5789*Power(t2,2) - 5682*Power(t2,3) + 
                  36*Power(t2,4) + 
                  Power(s2,2)*(60 - 131*t2 + 315*Power(t2,2)) - 
                  s2*(591 + 1975*t2 - 2220*Power(t2,2) + 
                     690*Power(t2,3)))) + 
            Power(t1,4)*(-257 - 1539*t2 + 1241*Power(t2,2) + 
               27008*Power(t2,3) + 8199*Power(t2,4) - 
               2474*Power(t2,5) - 
               Power(s,3)*(10 + 72*t2 + 349*Power(t2,2) - 
                  663*Power(t2,3) + 13*Power(t2,4)) + 
               Power(s2,3)*(-10 + 17*t2 - 129*Power(t2,2) - 
                  30*Power(t2,3) + 13*Power(t2,4)) + 
               Power(s2,2)*(-105 + 223*t2 + 2207*Power(t2,2) + 
                  940*Power(t2,3) + 1001*Power(t2,4)) + 
               Power(s,2)*(75 + (758 - 67*s2)*t2 - 
                  (2506 + 409*s2)*Power(t2,2) - 
                  (7621 + 1122*s2)*Power(t2,3) + 
                  (1606 + 39*s2)*Power(t2,4)) + 
               s2*(-191 + 928*t2 - 3476*Power(t2,2) + 
                  2295*Power(t2,3) + 7665*Power(t2,4) - 340*Power(t2,5)\
) + s*(277 + 2954*t2 + 14951*Power(t2,2) - 2467*Power(t2,3) - 
                  14566*Power(t2,4) + 340*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (712 + 57*t2 + 489*Power(t2,2) - 39*Power(t2,3)) + 
                  s2*(32 - 2055*t2 - 4975*Power(t2,2) + 
                     1833*Power(t2,3) - 2607*Power(t2,4)))) + 
            Power(t1,5)*(-73 - 2768*t2 - 8109*Power(t2,2) + 
               7955*Power(t2,3) + 6462*Power(t2,4) - 56*Power(t2,5) + 
               Power(s2,3)*(23 + 192*t2 - 91*Power(t2,2) - 
                  67*Power(t2,3)) + 
               Power(s,3)*(35 + 148*t2 - 798*Power(t2,2) + 
                  118*Power(t2,3)) + 
               Power(s2,2)*(68 - 506*t2 - 751*Power(t2,2) - 
                  1188*Power(t2,3) + 76*Power(t2,4)) + 
               s2*(388 - 91*t2 - 2694*Power(t2,2) - 6073*Power(t2,3) + 
                  1678*Power(t2,4)) + 
               Power(s,2)*(190 + 2180*t2 + 3761*Power(t2,2) - 
                  4374*Power(t2,3) + 76*Power(t2,4) - 
                  s2*(50 + 456*t2 - 490*Power(t2,2) + 303*Power(t2,3))) \
+ s*(-688 - 1871*t2 + 17848*Power(t2,2) + 18038*Power(t2,3) - 
                  2272*Power(t2,4) + 
                  Power(s2,2)*
                   (-5 - 936*t2 + 114*Power(t2,2) + 252*Power(t2,3)) + 
                  s2*(-90 + 1168*t2 + 84*Power(t2,2) + 
                     4766*Power(t2,3) - 152*Power(t2,4)))) + 
            Power(t1,2)*(-55 + 248*t2 + 1420*Power(t2,2) - 
               2813*Power(t2,3) - 16380*Power(t2,4) - 
               1313*Power(t2,5) + 2709*Power(t2,6) + 
               Power(s2,3)*t2*
                (60 + 38*t2 + 236*Power(t2,2) - 178*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s,3)*t2*
                (12 - 7*t2 + 33*Power(t2,2) - 204*Power(t2,3) + 
                  5*Power(t2,4)) - 
               Power(s2,2)*t2*
                (-574 + 75*t2 + 3559*Power(t2,2) + 1745*Power(t2,3) + 
                  377*Power(t2,4)) + 
               s*(30 + (-495 - 251*s2 + 60*Power(s2,2))*t2 + 
                  (-2143 + 2226*s2 - 1931*Power(s2,2))*Power(t2,2) + 
                  (-6267 + 4565*s2 - 713*Power(s2,2))*Power(t2,3) + 
                  (4357 + 493*s2 + 318*Power(s2,2))*Power(t2,4) + 
                  (9209 + 1237*s2 + 15*Power(s2,2))*Power(t2,5) - 
                  315*Power(t2,6)) + 
               s2*(-60 + 330*t2 - 2371*Power(t2,2) + 
                  2438*Power(t2,3) + 463*Power(t2,4) - 
                  4515*Power(t2,5) + 315*Power(t2,6)) + 
               Power(s,2)*t2*
                (-60 - 253*t2 + 883*Power(t2,2) + 3418*Power(t2,3) - 
                  860*Power(t2,4) + 
                  s2*(-30 + 328*t2 + 666*Power(t2,2) + 64*Power(t2,3) - 
                     15*Power(t2,4)))) + 
            Power(t1,3)*(108 + 407*t2 + 5542*Power(t2,2) + 
               7379*Power(t2,3) - 20241*Power(t2,4) - 9447*Power(t2,5) + 
               344*Power(t2,6) + 
               2*Power(s2,3)*t2*
                (-83 - 278*t2 + 206*Power(t2,2) + 33*Power(t2,3)) - 
               Power(s,3)*t2*
                (83 + 190*t2 - 590*Power(t2,2) + 158*Power(t2,3)) + 
               s2*(152 - 777*t2 + 641*Power(t2,2) + 2342*Power(t2,3) + 
                  4736*Power(t2,4) - 3302*Power(t2,5)) + 
               Power(s2,2)*(48 - 274*t2 + 2032*Power(t2,2) + 
                  645*Power(t2,3) + 592*Power(t2,4) - 170*Power(t2,5)) + 
               Power(s,2)*(-6 + (-337 + 180*s2)*t2 + 
                  3*(-743 + 152*s2)*Power(t2,2) + 
                  (-2351 + 102*s2)*Power(t2,3) + 
                  (4766 + 382*s2)*Power(t2,4) - 170*Power(t2,5)) - 
               s*(79 - 1264*t2 + 403*Power(t2,2) + 23340*Power(t2,3) + 
                  15139*Power(t2,4) - 4359*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (128 - 2256*t2 + 841*Power(t2,2) + 290*Power(t2,3)) + 
                  s2*(24 - 398*t2 + 1030*Power(t2,2) - 
                     2276*Power(t2,3) + 4422*Power(t2,4) - 
                     340*Power(t2,5))))) + 
         Power(s1,2)*(5*s*Power(t1,12) + 
            2*Power(t1,3)*(10 + 
               (172 - 72*Power(s,2) - 7*s2 + 24*Power(s2,2) + 
                  s*(173 + 6*s2))*t2 - 
               (-860 + 36*Power(s,3) + Power(s,2)*(617 - 62*s2) + 
                  184*s2 + 46*Power(s2,2) + 50*Power(s2,3) + 
                  s*(-1049 + 129*s2 - 60*Power(s2,2)))*Power(t2,2) + 
               (2750 + 46*Power(s,3) + 2209*s2 + 373*Power(s2,2) - 
                  253*Power(s2,3) + Power(s,2)*(-1779 + 359*s2) + 
                  3*s*(-771 - 722*s2 + 464*Power(s2,2)))*Power(t2,3) + 
               (2032 + 454*Power(s,3) + 1443*s2 + 581*Power(s2,2) + 
                  188*Power(s2,3) + 2*Power(s,2)*(739 + 120*s2) - 
                  s*(14433 + 591*s2 + 574*Power(s2,2)))*Power(t2,4) - 
               (8282 + 316*Power(s,3) + 236*s2 - 339*Power(s2,2) - 
                  155*Power(s2,3) - Power(s,2)*(3229 + 787*s2) + 
                  s*(2314 + 2694*s2 + 626*Power(s2,2)))*Power(t2,5) - 
               (1878 + 203*Power(s,2) + 1273*s2 + 203*Power(s2,2) - 
                  14*s*(137 + 29*s2))*Power(t2,6) + 88*Power(t2,7)) - 
            2*t1*t2*(-30 + (72 - 54*Power(s,2) + 213*s2 + 
                  72*Power(s2,2) + 6*s*(-11 + 9*s2))*t2 + 
               (425 + 5*Power(s,3) + 72*s2 + 131*Power(s2,2) - 
                  34*Power(s2,3) + Power(s,2)*(61 + 100*s2) + 
                  s*(261 - 168*s2 - 287*Power(s2,2)))*Power(t2,2) - 
               (-1350 + 178*Power(s,3) + Power(s,2)*(590 - 661*s2) + 
                  205*s2 - 584*Power(s2,2) + 239*Power(s2,3) + 
                  s*(697 + 578*s2 - 412*Power(s2,2)))*Power(t2,3) + 
               (-374 + 69*Power(s,3) + 317*s2 - 530*Power(s2,2) + 
                  132*Power(s2,3) + 2*Power(s,2)*(134 + 61*s2) + 
                  s*(-4711 + 1350*s2 - 251*Power(s2,2)))*Power(t2,4) - 
               (4238 + 32*Power(s,3) + 188*s2 + 120*Power(s2,2) - 
                  29*Power(s2,3) - 31*Power(s,2)*(28 + 3*s2) + 
                  15*s*(-1 + 28*s2 + 6*Power(s2,2)))*Power(t2,5) - 
               (589 + 65*Power(s,2) + 657*s2 + 65*Power(s2,2) - 
                  2*s*(527 + 65*s2))*Power(t2,6) + 120*Power(t2,7)) - 
            Power(t1,11)*(7 + 34*Power(s,2) + 3*s2 - 7*t2 + 
               s*(-33 + s2 + 15*t2)) + 
            Power(t1,10)*(19 + 28*Power(s,3) - 3*t2 - 17*Power(t2,2) + 
               Power(s2,2)*(-1 + 3*t2) + 
               Power(s,2)*(-54 + 8*s2 + 113*t2) - 
               s2*(1 - 35*t2 + Power(t2,2)) + 
               s*(-56 + s2*(17 - 46*t2) - 238*t2 + 16*Power(t2,2))) - 
            Power(t1,9)*(-62 + 48*t2 - Power(s2,3)*t2 + 
               126*Power(t2,2) - 13*Power(t2,3) + 
               Power(s,3)*(1 + 105*t2) + 
               4*Power(s2,2)*(1 - 3*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(-204 + s2*(34 - 85*t2) - 562*t2 + 
                  138*Power(t2,2)) - 
               s2*(15 + 30*t2 - 100*Power(t2,2) + 2*Power(t2,3)) + 
               s*(283 + 80*t2 - 523*Power(t2,2) + 7*Power(t2,3) + 
                  Power(s2,2)*(2 + 9*t2) - 
                  2*s2*(27 - 37*t2 + 60*Power(t2,2)))) + 
            Power(t1,8)*(-140 - 327*t2 + Power(s2,3)*(4 - 17*t2)*t2 + 
               482*Power(t2,2) + 374*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,3)*(-45 - 181*t2 + 146*Power(t2,2)) + 
               Power(s2,2)*(10 + 22*t2 - 111*Power(t2,2) + 
                  28*Power(t2,3)) - 
               s2*(-37 + 345*t2 + 291*Power(t2,2) - 125*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s,2)*(-139 + 300*t2 + 1467*Power(t2,2) - 
                  73*Power(t2,3) + 2*s2*(2 + 57*t2 + 126*Power(t2,2))) \
+ s*(179 + 1883*t2 + 1705*Power(t2,2) - 491*Power(t2,3) + Power(t2,4) + 
                  Power(s2,2)*(2 - 45*t2 + 123*Power(t2,2)) + 
                  s2*(-107 + 54*t2 + 678*Power(t2,2) - 101*Power(t2,3)))\
) - Power(t1,4)*(65 + 773*t2 + 2538*Power(t2,2) - 3021*Power(t2,3) - 
               15081*Power(t2,4) - 1478*Power(t2,5) + 
               1004*Power(t2,6) + 
               Power(s2,2)*t2*
                (102 + 99*t2 - 1988*Power(t2,2) - 889*Power(t2,3) - 
                  1266*Power(t2,4)) + 
               Power(s2,3)*t2*
                (12 + 60*t2 + 127*Power(t2,2) + 52*Power(t2,3) - 
                  88*Power(t2,4)) + 
               Power(s,3)*t2*
                (60 + 312*t2 + 90*Power(t2,2) - 1523*Power(t2,3) + 
                  88*Power(t2,4)) + 
               s*(-60 + (-836 + 139*s2 - 30*Power(s2,2))*t2 + 
                  (-8567 + 2136*s2 - 654*Power(s2,2))*Power(t2,2) + 
                  (-17107 + 2026*s2 + 260*Power(s2,2))*Power(t2,3) + 
                  (12626 + 449*s2 - 879*Power(s2,2))*Power(t2,4) + 
                  (9722 + 3832*s2 + 264*Power(s2,2))*Power(t2,5) - 
                  294*Power(t2,6)) + 
               s2*(30 + 277*t2 + 77*Power(t2,2) + 2641*Power(t2,3) - 
                  6644*Power(t2,4) - 4063*Power(t2,5) + 294*Power(t2,6)\
) + Power(s,2)*t2*(-446 + 315*t2 + 8655*Power(t2,2) + 
                  6234*Power(t2,3) - 2566*Power(t2,4) + 
                  s2*(60 + 6*t2 + 657*Power(t2,2) + 2350*Power(t2,3) - 
                     264*Power(t2,4)))) - 
            Power(t1,7)*(11 + 38*t2 - 1309*Power(t2,2) - 
               61*Power(t2,3) + 336*Power(t2,4) + 
               Power(s2,3)*t2*(16 + 4*t2 - 41*Power(t2,2)) + 
               Power(s,3)*(10 - 95*t2 - 673*Power(t2,2) + 
                  89*Power(t2,3)) + 
               Power(s2,2)*(-2 + 19*t2 - 175*Power(t2,2) - 
                  281*Power(t2,3) + 14*Power(t2,4)) + 
               s2*(81 - 92*t2 - 664*Power(t2,2) - 1070*Power(t2,3) + 
                  83*Power(t2,4)) + 
               Power(s,2)*(167 + 1875*t2 + 1786*Power(t2,2) - 
                  1631*Power(t2,3) + 14*Power(t2,4) - 
                  s2*(25 + 232*t2 - 401*Power(t2,2) + 219*Power(t2,3))) \
+ s*(-320 - 1837*t2 + 3645*Power(t2,2) + 4046*Power(t2,3) - 
                  197*Power(t2,4) + 
                  Power(s2,2)*t2*(-155 + 25*t2 + 171*Power(t2,2)) + 
                  s2*(15 + 632*t2 + 323*Power(t2,2) + 
                     1512*Power(t2,3) - 28*Power(t2,4)))) - 
            2*Power(t2,2)*(-45 + 
               (18 - 45*s - 35*Power(s,2) + 2*Power(s,3))*t2 + 
               (219 - 287*s + 54*Power(s,2) + 15*Power(s,3))*
                Power(t2,2) + 
               (-380 - 164*s - 60*Power(s,2) + 33*Power(s,3))*
                Power(t2,3) + 
               (-647 + 480*s + 202*Power(s,2) + Power(s,3))*
                Power(t2,4) + 
               (298 + 449*s - 33*Power(s,2) + 5*Power(s,3))*
                Power(t2,5) - 17*(-9 + s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (20 + 63*t2 + 121*Power(t2,2) - 15*Power(t2,3) - 
                  5*Power(t2,4)) + 
               s2*(-90 + 3*(-55 + 78*s)*t2 + 
                  (-285 + 652*s - 223*Power(s,2))*Power(t2,2) + 
                  (264 + 280*s + 55*Power(s,2))*Power(t2,3) + 
                  (38 - 20*s - 33*Power(s,2))*Power(t2,4) - 
                  3*(65 - 18*s + 5*Power(s,2))*Power(t2,5) + 
                  17*Power(t2,6)) + 
               Power(s2,2)*t2*
                (49 + 110*t2 - 404*Power(t2,2) - 166*Power(t2,3) - 
                  21*Power(t2,4) + 
                  s*(90 - 271*t2 - 289*Power(t2,2) + 47*Power(t2,3) + 
                     15*Power(t2,4)))) - 
            2*Power(t1,2)*t2*
             (30 - 294*t2 - 521*Power(t2,2) + 3639*Power(t2,3) + 
               5039*Power(t2,4) - 2069*Power(t2,5) - 816*Power(t2,6) + 
               Power(s,3)*t2*
                (-18 - 75*t2 + 44*Power(t2,2) + 345*Power(t2,3) - 
                  36*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-18 - 159*t2 - 108*Power(t2,2) + 109*Power(t2,3) + 
                  36*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-231 + 13*t2 + 1594*Power(t2,2) + 769*Power(t2,3) + 
                  551*Power(t2,4)) - 
               2*s2*(-9 + 84*t2 - 422*Power(t2,2) + 14*Power(t2,3) - 
                  1413*Power(t2,4) - 722*Power(t2,5) + 136*Power(t2,6)) \
+ s*(18 - 6*(-89 - 4*s2 + 3*Power(s2,2))*t2 + 
                  (3630 - 1638*s2 + 1183*Power(s2,2))*Power(t2,2) + 
                  (2022 - 960*s2 + 272*Power(s2,2))*Power(t2,3) - 
                  (7360 + 1058*s2 + 101*Power(s2,2))*Power(t2,4) - 
                  4*(949 + 422*s2 + 27*Power(s2,2))*Power(t2,5) + 
                  272*Power(t2,6)) + 
               Power(s,2)*t2*
                (219 - 335*t2 - 2278*Power(t2,2) - 1319*Power(t2,3) + 
                  1137*Power(t2,4) + 
                  s2*(-18 - 5*t2 - 376*Power(t2,2) - 353*Power(t2,3) + 
                     108*Power(t2,4)))) + 
            Power(t1,6)*(116 + 1152*t2 + 769*Power(t2,2) - 
               4930*Power(t2,3) - 2000*Power(t2,4) + 99*Power(t2,5) + 
               Power(s2,3)*t2*
                (-2 + 6*t2 + 93*Power(t2,2) - 20*Power(t2,3)) + 
               Power(s,3)*(20 + 225*t2 + 294*Power(t2,2) - 
                  884*Power(t2,3) + 20*Power(t2,4)) - 
               Power(s2,2)*(13 + 46*t2 + 296*Power(t2,2) - 
                  249*Power(t2,3) + 388*Power(t2,4)) + 
               Power(s,2)*(-115 + (-451 + 50*s2)*t2 + 
                  (4059 + 470*s2)*Power(t2,2) + 
                  3*(1766 + 493*s2)*Power(t2,3) - 
                  (841 + 60*s2)*Power(t2,4)) + 
               s2*(-16 - 55*t2 + 2194*Power(t2,2) - 586*Power(t2,3) - 
                  1458*Power(t2,4) + 24*Power(t2,5)) + 
               s*(-88 - 2495*t2 - 10794*Power(t2,2) - 
                  1064*Power(t2,3) + 3864*Power(t2,4) - 
                  24*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-49 + 258*t2 - 688*Power(t2,2) + 60*Power(t2,3)) + 
                  s2*(82 + 759*t2 - 293*Power(t2,2) - 
                     1831*Power(t2,3) + 1229*Power(t2,4)))) + 
            Power(t1,5)*(6 - 314*t2 - 2115*Power(t2,2) - 
               5349*Power(t2,3) + 4996*Power(t2,4) + 2296*Power(t2,5) + 
               Power(s2,3)*t2*
                (25 + 157*t2 - 79*Power(t2,2) - 229*Power(t2,3)) + 
               Power(s,3)*t2*
                (40 - 309*t2 - 1307*Power(t2,2) + 481*Power(t2,3)) + 
               Power(s2,2)*(6 + 82*t2 - 5*Power(t2,2) - 
                  1368*Power(t2,3) - 973*Power(t2,4) + 169*Power(t2,5)) \
+ Power(s,2)*(60 + (989 - 85*s2)*t2 + (4762 - 653*s2)*Power(t2,2) + 
                  (-280 + 267*s2)*Power(t2,3) - 
                  (5573 + 1191*s2)*Power(t2,4) + 169*Power(t2,5)) + 
               s2*(79 + 570*t2 - 1829*Power(t2,2) - 4042*Power(t2,3) - 
                  2439*Power(t2,4) + 1005*Power(t2,5)) + 
               s*(-170 - 1795*t2 - 1521*Power(t2,2) + 
                  20634*Power(t2,3) + 9560*Power(t2,4) - 
                  1678*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-85 - 1157*t2 + 450*Power(t2,2) + 939*Power(t2,3)) + 
                  s2*(-30 + 36*t2 + 2980*Power(t2,2) + 
                     3038*Power(t2,3) + 5104*Power(t2,4) - 
                     338*Power(t2,5))))) + 
         Power(s1,5)*(-2 - 104*Power(t1,10) + 4*t2 - s*t2 + 89*s2*t2 + 
            6*s*s2*t2 + 60*Power(s2,2)*t2 - 55*Power(t2,2) - 
            s*Power(t2,2) + Power(s,2)*Power(t2,2) + 74*s2*Power(t2,2) - 
            105*s*s2*Power(t2,2) - 4*Power(s,2)*s2*Power(t2,2) + 
            32*Power(s2,2)*Power(t2,2) - 144*s*Power(s2,2)*Power(t2,2) - 
            80*Power(s2,3)*Power(t2,2) + 13*Power(t2,3) + 
            127*s*Power(t2,3) - 22*Power(s,2)*Power(t2,3) - 
            4*Power(s,3)*Power(t2,3) - 228*s2*Power(t2,3) - 
            210*s*s2*Power(t2,3) + 147*Power(s,2)*s2*Power(t2,3) + 
            324*Power(s2,2)*Power(t2,3) + 86*s*Power(s2,2)*Power(t2,3) - 
            109*Power(s2,3)*Power(t2,3) + 339*Power(t2,4) - 
            158*s*Power(t2,4) - 106*Power(s,2)*Power(t2,4) - 
            14*Power(s,3)*Power(t2,4) - 369*s2*Power(t2,4) + 
            253*s*s2*Power(t2,4) + 74*Power(s,2)*s2*Power(t2,4) + 
            113*Power(s2,2)*Power(t2,4) - 58*s*Power(s2,2)*Power(t2,4) + 
            10*Power(s2,3)*Power(t2,4) - 257*Power(t2,5) - 
            394*s*Power(t2,5) - 26*Power(s,2)*Power(t2,5) + 
            2*Power(s,3)*Power(t2,5) + 175*s2*Power(t2,5) + 
            146*s*s2*Power(t2,5) - 5*Power(s,2)*s2*Power(t2,5) - 
            82*Power(s2,2)*Power(t2,5) + 4*s*Power(s2,2)*Power(t2,5) - 
            Power(s2,3)*Power(t2,5) - 290*Power(t2,6) + 
            27*s*Power(t2,6) + Power(s,2)*Power(t2,6) + 
            3*s2*Power(t2,6) - 2*s*s2*Power(t2,6) + 
            Power(s2,2)*Power(t2,6) + 8*Power(t2,7) + 
            Power(t1,9)*(219 + 504*s - 96*s2 + 263*t2) - 
            Power(t1,8)*(-346 + 392*Power(s,2) - 56*Power(s2,2) - 
               1083*t2 + 232*Power(t2,2) - 86*s2*(-1 + 5*t2) + 
               s*(84 + 112*s2 + 1273*t2)) + 
            Power(t1,7)*(372 + 56*Power(s,3) + 
               45*Power(s2,2)*(-3 + t2) - 3750*t2 - 3325*Power(t2,2) + 
               80*Power(t2,3) + Power(s,2)*(-183 + 70*s2 + 1033*t2) + 
               s2*(205 + 913*t2 - 613*Power(t2,2)) + 
               s*(-1389 + s2*(598 - 518*t2) - 4015*t2 + 
                  1169*Power(t2,2))) - 
            Power(t1,6)*(472 + 10*Power(s2,3)*(-3 + t2) + 3511*t2 + 
               528*Power(t2,2) - 2980*Power(t2,3) + 8*Power(t2,4) + 
               Power(s,3)*(-64 + 189*t2) + 
               Power(s2,2)*(160 + 336*t2 + 325*Power(t2,2)) + 
               s2*(-191 + 33*t2 + 3435*Power(t2,2) - 358*Power(t2,3)) + 
               Power(s,2)*(-371 - 2148*t2 + 990*Power(t2,2) - 
                  2*s2*(-70 + 79*t2)) + 
               s*(734 - 4357*t2 - 9192*Power(t2,2) + 472*Power(t2,3) + 
                  Power(s2,2)*(136 + 15*t2) - 
                  s2*(315 + 187*t2 + 1160*Power(t2,2)))) + 
            Power(t1,5)*(-431 - 1019*t2 + 16724*Power(t2,2) + 
               10063*Power(t2,3) - 1086*Power(t2,4) + 
               Power(s2,3)*(21 - 30*t2 - 20*Power(t2,2)) + 
               Power(s,3)*(-18 - 178*t2 + 171*Power(t2,2)) + 
               Power(s2,2)*(115 + 772*t2 + 271*Power(t2,2) + 
                  269*Power(t2,3)) + 
               s2*(268 - 1738*t2 - 1311*Power(t2,2) + 
                  4114*Power(t2,3) - 74*Power(t2,4)) - 
               Power(s,2)*(-316 + 169*t2 + 4058*Power(t2,2) - 
                  396*Power(t2,3) + s2*(5 + 202*t2 + 314*Power(t2,2))) \
+ s*(474 + 7170*t2 + 6017*Power(t2,2) - 7258*Power(t2,3) + 
                  74*Power(t2,4) + 
                  Power(s2,2)*(100 - 10*t2 + 163*Power(t2,2)) - 
                  s2*(585 + 2805*t2 - 1513*Power(t2,2) + 
                     665*Power(t2,3)))) + 
            t1*(20 + 29*t2 - 402*Power(t2,2) - 222*Power(t2,3) + 
               3903*Power(t2,4) + 1365*Power(t2,5) - 677*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*
                (-3 + 11*t2 - 11*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*t2*
                (-390 - 120*t2 + 1293*Power(t2,2) + 950*Power(t2,3) - 
                  21*Power(t2,4)) - 
               Power(s2,3)*t2*
                (60 - 30*t2 + 101*Power(t2,2) - 58*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(30 - 77*t2 + 1124*Power(t2,2) - 457*Power(t2,3) - 
                  1739*Power(t2,4) + 610*Power(t2,5) - 27*Power(t2,6)) \
+ s*(-6 + (97 + 100*s2 - 30*Power(s2,2))*t2 + 
                  (130 - 564*s2 + 819*Power(s2,2))*Power(t2,2) + 
                  (1292 - 1753*s2 + 407*Power(s2,2))*Power(t2,3) + 
                  (523 - 684*s2 - 165*Power(s2,2))*Power(t2,4) + 
                  (-1775 + s2 + 3*Power(s2,2))*Power(t2,5) + 
                  27*Power(t2,6)) + 
               Power(s,2)*t2*
                (s2*(6 - 102*t2 - 305*Power(t2,2) + 118*Power(t2,3) - 
                     3*Power(t2,4)) + 
                  2*(2 + 6*t2 + 81*Power(t2,2) - 301*Power(t2,3) + 
                     10*Power(t2,4)))) + 
            Power(t1,4)*(68 + 2726*t2 + 7544*Power(t2,2) - 
               9626*Power(t2,3) - 8696*Power(t2,4) + 144*Power(t2,5) + 
               Power(s2,3)*(-55 - 230*t2 + 174*Power(t2,2) + 
                  14*Power(t2,3)) - 
               Power(s,3)*(19 + 130*t2 - 363*Power(t2,2) + 
                  41*Power(t2,3)) + 
               s2*(-290 + 54*t2 + 277*Power(t2,2) + 5892*Power(t2,3) - 
                  1844*Power(t2,4)) + 
               Power(s2,2)*(-133 + 1151*t2 + 937*Power(t2,2) + 
                  558*Power(t2,3) - 50*Power(t2,4)) + 
               Power(s,2)*(-134 - 1031*t2 - 2834*Power(t2,2) + 
                  2758*Power(t2,3) - 50*Power(t2,4) + 
                  s2*(25 + 238*t2 - 88*Power(t2,2) + 96*Power(t2,3))) + 
               s*(422 + 767*t2 - 13672*Power(t2,2) - 
                  16251*Power(t2,3) + 2328*Power(t2,4) + 
                  Power(s2,2)*
                   (10 + 988*t2 - 314*Power(t2,2) - 69*Power(t2,3)) + 
                  2*s2*(105 - 346*t2 + 231*Power(t2,2) - 
                     1431*Power(t2,3) + 50*Power(t2,4)))) + 
            Power(t1,3)*(82 + 899*t2 - 457*Power(t2,2) - 
               19763*Power(t2,3) - 7340*Power(t2,4) + 
               2547*Power(t2,5) + 
               Power(s2,3)*(20 - 84*t2 + 172*Power(t2,2) - 
                  96*Power(t2,3) + Power(t2,4)) - 
               Power(s,3)*(-2 + 15*t2 - 90*Power(t2,2) + 
                  173*Power(t2,3) + Power(t2,4)) - 
               Power(s2,2)*(-198 + 170*t2 + 2076*Power(t2,2) + 
                  1528*Power(t2,3) + 335*Power(t2,4)) + 
               Power(s,2)*(18 + (-389 + 167*s2)*t2 + 
                  (360 + 394*s2)*Power(t2,2) + 
                  3*(1187 + 57*s2)*Power(t2,3) + 
                  (-623 + 3*s2)*Power(t2,4)) + 
               s2*(160 - 1489*t2 + 2329*Power(t2,2) + 
                  1678*Power(t2,3) - 5064*Power(t2,4) + 232*Power(t2,5)\
) - s*(191 + 1007*t2 + 7107*Power(t2,2) + 768*Power(t2,3) - 
                  9487*Power(t2,4) + 232*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (772 + 358*t2 - 98*Power(t2,2) + 3*Power(t2,3)) - 
                  s2*(-157 + 1315*t2 + 4511*Power(t2,2) + 
                     119*Power(t2,3) + 958*Power(t2,4)))) + 
            Power(t1,2)*(-15 + 110*t2 - 1908*Power(t2,2) - 
               4285*Power(t2,3) + 8075*Power(t2,4) + 5563*Power(t2,5) - 
               192*Power(t2,6) - 
               Power(s,3)*t2*
                (-29 - 105*t2 + 97*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*t2*
                (200 + 380*t2 - 272*Power(t2,2) + 19*Power(t2,3)) + 
               Power(s2,2)*(-60 + 272*t2 - 2044*Power(t2,2) - 
                  590*Power(t2,3) + 421*Power(t2,4) + 17*Power(t2,5)) + 
               Power(s,2)*(-6 + (64 - 113*s2)*t2 + 
                  (527 - 232*s2)*Power(t2,2) + 
                  (1394 - 267*s2)*Power(t2,3) + 
                  7*(-148 + 3*s2)*Power(t2,4) + 17*Power(t2,5)) + 
               s2*(-160 + 231*t2 + 773*Power(t2,2) + 1341*Power(t2,3) - 
                  2998*Power(t2,4) + 1053*Power(t2,5)) + 
               s*(89 - 284*t2 - 429*Power(t2,2) + 7221*Power(t2,3) + 
                  7478*Power(t2,4) - 1511*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (175 - 1207*t2 + 579*Power(t2,2) - 39*Power(t2,3)) + 
                  s2*(30 - 318*t2 + 587*Power(t2,2) - 1871*Power(t2,3) + 
                     326*Power(t2,4) - 34*Power(t2,5))))) - 
         Power(s1,6)*(-3 + 280*Power(t1,9) - 3*t2 - 4*Power(t2,2) + 
            7*s*Power(t2,2) + Power(s,2)*Power(t2,2) + 93*Power(t2,3) - 
            21*s*Power(t2,3) - 27*Power(s,2)*Power(t2,3) + 
            2*Power(s,3)*Power(t2,3) - 169*Power(t2,4) - 
            150*s*Power(t2,4) + 2*Power(s,2)*Power(t2,4) + 
            3*Power(s,3)*Power(t2,4) - 184*Power(t2,5) + 
            46*s*Power(t2,5) + 8*Power(s,2)*Power(t2,5) + 
            30*Power(t2,6) + 2*s*Power(t2,6) - 
            Power(t1,8)*(502 + 798*s + 661*t2) + 
            Power(t1,7)*(-577 + 364*Power(s,2) - 1813*t2 + 
               550*Power(t2,2) + s*(245 + 1793*t2)) + 
            Power(t1,6)*(-439 - 28*Power(s,3) + 
               Power(s,2)*(174 - 829*t2) + 5355*t2 + 5163*Power(t2,2) - 
               190*Power(t2,3) + s*(1246 + 4244*t2 - 1450*Power(t2,2))) \
+ Power(t1,5)*(488 + 3394*t2 + 314*Power(t2,2) - 4009*Power(t2,3) + 
               24*Power(t2,4) + Power(s,3)*(-41 + 91*t2) + 
               2*Power(s,2)*(-90 - 693*t2 + 319*Power(t2,2)) + 
               s*(391 - 3626*t2 - 8187*Power(t2,2) + 503*Power(t2,3))) + 
            Power(s2,3)*(5*Power(t1,5)*(-8 + 5*t2) + 
               Power(t1,4)*(-45 + 60*t2 - 23*Power(t2,2)) + 
               t2*(20 + 2*t2 + 17*Power(t2,2) - 2*Power(t2,3)) - 
               t1*t2*(125 + 147*t2 - 67*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(70 + 160*t2 - 146*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,2)*(-20 + 110*t2 - 104*Power(t2,2) + 
                  65*Power(t2,3) - 2*Power(t2,4))) + 
            Power(t1,3)*(11 - 1108*t2 - 4081*Power(t2,2) + 
               4411*Power(t2,3) + 5688*Power(t2,4) - 112*Power(t2,5) + 
               Power(s,3)*(4 + 49*t2 - 73*Power(t2,2) - 
                  3*Power(t2,3)) + 
               s*(-84 - 247*t2 + 5107*Power(t2,2) + 8042*Power(t2,3) - 
                  1019*Power(t2,4)) + 
               Power(s,2)*(63 + 245*t2 + 1240*Power(t2,2) - 
                  753*Power(t2,3) + 6*Power(t2,4))) - 
            Power(t1,4)*(-254 - 831*t2 + 13056*Power(t2,2) + 
               9044*Power(t2,3) - 1197*Power(t2,4) + 
               Power(s,3)*(-3 - 43*t2 + 50*Power(t2,2)) + 
               Power(s,2)*(147 + 142*t2 - 1899*Power(t2,2) + 
                  169*Power(t2,3)) + 
               s*(201 + 3683*t2 + 4557*Power(t2,2) - 4951*Power(t2,3) + 
                  61*Power(t2,4))) + 
            Power(t1,2)*(10 - 248*t2 - 545*Power(t2,2) + 
               6300*Power(t2,3) + 3700*Power(t2,4) - 939*Power(t2,5) + 
               Power(s,3)*t2*
                (13 + 12*t2 - 4*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(-17 + 69*t2 + 157*Power(t2,2) - 
                  690*Power(t2,3) + 27*Power(t2,4)) + 
               s*(58 + 135*t2 + 1588*Power(t2,2) + 1694*Power(t2,3) - 
                  2504*Power(t2,4) + 36*Power(t2,5))) + 
            t1*(6 - 90*t2 + 97*Power(t2,2) + 1011*Power(t2,3) - 
               700*Power(t2,4) - 1108*Power(t2,5) + 24*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(-23 - 7*t2 + 7*Power(t2,2)) + 
               Power(s,2)*t2*
                (15 - 44*t2 - 286*Power(t2,2) - 27*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s*(-16 - 11*t2 + 221*Power(t2,2) - 580*Power(t2,3) - 
                  1392*Power(t2,4) + 96*Power(t2,5))) + 
            Power(s2,2)*(-112*Power(t1,7) + 
               3*Power(t1,6)*(46 + 15*t2) + 
               Power(t1,5)*(255 + s*(165 - 29*t2) + 541*t2 + 
                  176*Power(t2,2)) + 
               Power(t1,2)*(-155 + (22 + 403*s)*t2 + 
                  (902 + 280*s)*Power(t2,2) + 
                  (1065 - 152*s)*Power(t2,3) + (-44 + 6*s)*Power(t2,4)) \
+ t1*(30 - (120 + 73*s)*t2 + (749 + 345*s)*Power(t2,2) + 
                  (414 - 158*s)*Power(t2,3) + 
                  (-223 + 17*s)*Power(t2,4) + 5*Power(t2,5)) - 
               Power(t1,4)*(60 + 544*t2 + 709*Power(t2,2) + 
                  104*Power(t2,3) + s*(90 + 109*t2 - 11*Power(t2,2))) + 
               t2*(88 + 69*t2 - 98*Power(t2,2) - 139*Power(t2,3) + 
                  6*Power(t2,4) + 
                  s*(6 - 130*t2 - 84*Power(t2,2) + 7*Power(t2,3))) + 
               Power(t1,3)*(134 - 1215*t2 - 913*Power(t2,2) + 
                  169*Power(t2,3) + 6*Power(t2,4) - 
                  s*(10 + 635*t2 - 301*Power(t2,2) + 29*Power(t2,3)))) + 
            s2*(-6 + 168*Power(t1,8) + 
               Power(t1,7)*(208 + 182*s - 697*t2) + (15 + 13*s)*t2 - 
               (121 + 4*s + 12*Power(s,2))*Power(t2,2) + 
               (-87 + 176*s + 51*Power(s,2))*Power(t2,3) + 
               (220 + 159*s - 8*Power(s,2))*Power(t2,4) + 
               (33 - 14*s)*Power(t2,5) - 2*Power(t2,6) - 
               Power(t1,6)*(243 + 56*Power(s,2) + s*(676 - 350*t2) + 
                  1078*t2 - 865*Power(t2,2)) - 
               Power(t1,5)*(107 + 820*t2 - 3190*Power(t2,2) + 
                  412*Power(t2,3) + Power(s,2)*(-70 + 59*t2) + 
                  2*s*(93 + 167*t2 + 362*Power(t2,2))) + 
               Power(t1,4)*(-327 + 1123*t2 + 1873*Power(t2,2) - 
                  2871*Power(t2,3) + 61*Power(t2,4) + 
                  2*Power(s,2)*(3 + 87*t2 + 31*Power(t2,2)) + 
                  s*(329 + 2236*t2 - 130*Power(t2,2) + 273*Power(t2,3))) \
- Power(t1,2)*(62 - 891*t2 + 514*Power(t2,2) + 2248*Power(t2,3) - 
                  1040*Power(t2,4) + 36*Power(t2,5) + 
                  Power(s,2)*t2*
                   (102 + 232*t2 - 91*Power(t2,2) + 6*Power(t2,3)) + 
                  s*(-98 + 405*t2 + 2017*Power(t2,2) + 
                     851*Power(t2,3) - 17*Power(t2,4))) + 
               Power(t1,3)*(69 + 48*t2 + 1868*Power(t2,2) - 
                  2874*Power(t2,3) + 795*Power(t2,4) + 
                  Power(s,2)*
                   (-5 - 52*t2 - 115*Power(t2,2) + 19*Power(t2,3)) + 
                  s*(-185 + 384*t2 - 253*Power(t2,2) + 
                     444*Power(t2,3) - 12*Power(t2,4))) + 
               t1*(59 + 52*t2 - 307*Power(t2,2) - 1088*Power(t2,3) + 
                  475*Power(t2,4) - 23*Power(t2,5) + 
                  Power(s,2)*t2*
                   (17 + 79*t2 + 105*Power(t2,2) - 19*Power(t2,3)) + 
                  s*(-6 + 48*t2 - 276*Power(t2,2) + 342*Power(t2,3) + 
                     308*Power(t2,4) - 10*Power(t2,5))))) - 
         s1*((1 - 5*s + 4*Power(s,2))*Power(t1,12) + 
            Power(t1,4)*(-10 + 
               5*(-4 + 36*Power(s,2) + 27*s2 - 3*s*(29 + 2*s2))*t2 + 
               (-739 - 60*Power(s,3) + Power(s,2)*(1619 - 60*s2) + 
                  262*s2 + 100*Power(s2,2) + 24*Power(s2,3) + 
                  s*(-1679 + 273*s2 - 156*Power(s2,2)))*Power(t2,2) - 
               (1805 + 484*Power(s,3) + 3132*s2 + 46*Power(s2,2) - 
                  69*Power(s2,3) + Power(s,2)*(-3104 + 875*s2) + 
                  s*(-1819 - 4434*s2 + 942*Power(s2,2)))*Power(t2,3) - 
               (2847 + 550*Power(s,3) + 2829*s2 + 1537*Power(s2,2) + 
                  82*Power(s2,3) + Power(s,2)*(4318 + 778*s2) - 
                  s*(16366 + 4299*s2 + 790*Power(s2,2)))*Power(t2,4) + 
               (3697 + 566*Power(s,3) - 331*s2 - 316*Power(s2,2) - 
                  199*Power(s2,3) - Power(s,2)*(3544 + 1331*s2) + 
                  s*(3016 + 2630*s2 + 964*Power(s2,2)))*Power(t2,5) + 
               (1132 + 199*Power(s,2) + 695*s2 + 199*Power(s2,2) - 
                  s*(1215 + 398*s2))*Power(t2,6)) + 
            4*Power(t2,2)*(15 + 
               3*(6 + 3*Power(s,2) - 6*s2 - 9*Power(s2,2) - 
                  2*s*(4 + 9*s2))*t2 - 
               (59 + 4*Power(s,3) + 52*s2 + 80*Power(s2,2) + 
                  6*Power(s2,3) - 2*Power(s,2)*(8 + 25*s2) + 
                  s*(86 + 32*s2 - 72*Power(s2,2)))*Power(t2,2) + 
               2*(-75 + Power(s,3) + Power(s,2)*(74 - 70*s2) + 37*s2 - 
                  88*Power(s2,2) + 12*Power(s2,3) + 
                  s*(27 + 86*s2 + 41*Power(s2,2)))*Power(t2,3) + 
               (-6*Power(s,2)*(11 + 13*s2) + 
                  2*s*(231 - 74*s2 + 46*Power(s2,2)) + 
                  3*(39 + 6*s2 + 34*Power(s2,2) - 10*Power(s2,3)))*
                Power(t2,4) + 
               (386 + 2*Power(s,3) + 48*s2 + 43*Power(s2,2) - 
                  4*Power(s2,3) - Power(s,2)*(85 + 8*s2) + 
                  2*s*(-51 - 7*s2 + 5*Power(s2,2)))*Power(t2,5) + 
               (7 + 10*Power(s,2) + 58*s2 + 10*Power(s2,2) - 
                  4*s*(28 + 5*s2))*Power(t2,6) - 14*Power(t2,7)) + 
            2*Power(t1,2)*t2*(30 + 
               (237 - 72*Power(s,2) + s*(183 - 54*s2) + 48*s2 + 
                  54*Power(s2,2))*t2 + 
               (862 - 26*Power(s,3) + 219*s2 + 100*Power(s2,2) + 
                  Power(s2,3) + Power(s,2)*(-502 + 77*s2) + 
                  2*s*(93 - 207*s2 + 10*Power(s2,2)))*Power(t2,2) + 
               (1002 + 29*Power(s,3) + 1739*s2 + 137*Power(s2,2) - 
                  114*Power(s2,3) + 19*Power(s,2)*(-11 + 12*s2) + 
                  s*(-2749 - 2008*s2 + 897*Power(s2,2)))*Power(t2,3) + 
               (62 + 156*Power(s,3) - 391*s2 + 163*Power(s2,2) + 
                  97*Power(s2,3) + 5*Power(s,2)*(329 + 61*s2) + 
                  s*(-6295 + 72*s2 - 390*Power(s2,2)))*Power(t2,4) - 
               (3811 + 223*Power(s,3) + 879*s2 + 39*Power(s2,2) - 
                  88*Power(s2,3) - 3*Power(s,2)*(395 + 178*s2) + 
                  s*(-994 + 698*s2 + 399*Power(s2,2)))*Power(t2,5) - 
               (386 + 127*Power(s,2) + 480*s2 + 127*Power(s2,2) - 
                  s*(801 + 254*s2))*Power(t2,6) + 52*Power(t2,7)) - 
            Power(t1,11)*(-1 + 8*Power(s,3) + 2*s2*(-1 + t2) + 3*t2 + 
               Power(s,2)*(-19 + s2 + 14*t2) + 
               s*(9 + s2 - 24*t2 - 5*s2*t2)) + 
            Power(t1,10)*(Power(s2,2)*(-4 + t2)*t2 + 
               Power(s,3)*(4 + 31*t2) + 
               2*s2*(-1 - 9*t2 + 3*Power(t2,2)) + 
               2*(-12 - 5*t2 + 5*Power(t2,2)) + 
               Power(s,2)*(-55 + s2*(5 - 24*t2) - 131*t2 + 
                  18*Power(t2,2)) + 
               s*(76 + 86*t2 + Power(s2,2)*t2 - 43*Power(t2,2) - 
                  2*s2*(3 - 18*t2 + 7*Power(t2,2)))) + 
            Power(t1,9)*(33 + 61*t2 + 12*Power(t2,2) + 
               2*Power(s2,3)*Power(t2,2) - 19*Power(t2,3) + 
               Power(s,3)*(18 + 58*t2 - 45*Power(t2,2)) + 
               Power(s2,2)*t2*(2 + 27*t2 - 3*Power(t2,2)) + 
               Power(s,2)*(-42 + s2 + 25*t2 + 44*s2*t2 + 
                  314*Power(t2,2) + 72*s2*Power(t2,2) - 10*Power(t2,3)) \
+ s2*(-10 + 68*t2 + 57*Power(t2,2) - 6*Power(t2,3)) - 
               s*(26 + 260*t2 + 365*Power(t2,2) - 36*Power(t2,3) + 
                  Power(s2,2)*t2*(-6 + 29*t2) + 
                  s2*(-14 + 92*t2 + 161*Power(t2,2) - 13*Power(t2,3)))) + 
            Power(t1,8)*(-10*Power(s2,3)*Power(t2,3) + 
               2*Power(s2,2)*t2*
                (6 - 19*t2 - 21*Power(t2,2) + Power(t2,3)) + 
               Power(s,3)*(-1 - 84*t2 - 255*Power(t2,2) + 
                  29*Power(t2,3)) + 
               s2*(16 - 36*t2 - 25*Power(t2,2) - 146*Power(t2,3) + 
                  4*Power(t2,4)) + 
               2*(11 + 73*t2 - 65*Power(t2,2) - 46*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s*(-114 + (-723 + 124*s2 - 18*Power(s2,2))*t2 + 
                  (210 + 6*s2 + 32*Power(s2,2))*Power(t2,2) + 
                  7*(95 + 42*s2 + 7*Power(s2,2))*Power(t2,3) - 
                  2*(7 + 2*s2)*Power(t2,4)) + 
               Power(s,2)*(72 + 701*t2 + 644*Power(t2,2) - 
                  346*Power(t2,3) + 2*Power(t2,4) - 
                  s2*(5 + 46*t2 - 130*Power(t2,2) + 68*Power(t2,3)))) + 
            Power(t1,5)*(28 + 343*t2 + 1550*Power(t2,2) + 
               124*Power(t2,3) - 3207*Power(t2,4) - 827*Power(t2,5) + 
               69*Power(t2,6) - 
               Power(s2,3)*Power(t2,2)*
                (2 - 49*t2 + 14*Power(t2,2) + 55*Power(t2,3)) + 
               Power(s2,2)*t2*
                (2 + 84*t2 - 473*Power(t2,2) - 98*Power(t2,3) - 
                  379*Power(t2,4)) + 
               Power(s,3)*t2*
                (60 + 235*t2 - 295*Power(t2,2) - 981*Power(t2,3) + 
                  55*Power(t2,4)) + 
               s2*(6 - 15*t2 + 34*Power(t2,2) + 1901*Power(t2,3) - 
                  1419*Power(t2,4) - 1022*Power(t2,5) + 27*Power(t2,6)) \
- s*(30 + (405 - 92*s2 + 6*Power(s2,2))*t2 + 
                  (4292 - 252*s2 + 43*Power(s2,2))*Power(t2,2) + 
                  (9544 + 1003*s2 - 269*Power(s2,2))*Power(t2,3) + 
                  3*(-821 + 12*s2 + 121*Power(s2,2))*Power(t2,4) - 
                  (2779 + 1355*s2 + 165*Power(s2,2))*Power(t2,5) + 
                  27*Power(t2,6)) + 
               Power(s,2)*t2*(-240 + 544*t2 + 6238*Power(t2,2) + 
                  2918*Power(t2,3) - 976*Power(t2,4) + 
                  s2*(30 + 218*t2 + 733*Power(t2,2) + 1358*Power(t2,3) - 
                     165*Power(t2,4)))) - 
            4*t1*Power(t2,2)*(15 - 6*t2 - 41*Power(t2,2) + 
               922*Power(t2,3) + 311*Power(t2,4) - 652*Power(t2,5) - 
               101*Power(t2,6) + 
               Power(s2,3)*t2*
                (1 - 53*t2 - 27*Power(t2,2) + 57*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-73 + 10*t2 + 390*Power(t2,2) + 122*Power(t2,3) + 
                  95*Power(t2,4)) + 
               Power(s,3)*(t2 - 69*Power(t2,2) + 25*Power(t2,3) + 
                  81*Power(t2,4) - 14*Power(t2,5)) + 
               s2*(9 + 9*t2 + 318*Power(t2,2) - 10*Power(t2,3) + 
                  433*Power(t2,4) + 97*Power(t2,5) - 56*Power(t2,6)) + 
               s*(9 + (141 + 6*s2 - 9*Power(s2,2))*t2 + 
                  (978 - 624*s2 + 413*Power(s2,2))*Power(t2,2) + 
                  (-394 - 28*s2 + 135*Power(s2,2))*Power(t2,3) - 
                  (1547 + 248*s2 + 105*Power(s2,2))*Power(t2,4) - 
                  (395 + 338*s2 + 42*Power(s2,2))*Power(t2,5) + 
                  56*Power(t2,6)) + 
               Power(s,2)*t2*(107 - 154*t2 - 434*Power(t2,2) - 
                  66*Power(t2,3) + 243*Power(t2,4) + 
                  s2*(-9 + 61*t2 - 149*Power(t2,2) - 33*Power(t2,3) + 
                     42*Power(t2,4)))) + 
            2*Power(t1,3)*t2*(-90 - 429*t2 - 835*Power(t2,2) + 
               1731*Power(t2,3) + 3341*Power(t2,4) - 378*Power(t2,5) - 
               268*Power(t2,6) + 
               Power(s,3)*t2*
                (-30 - 81*t2 + 150*Power(t2,2) + 479*Power(t2,3) - 
                  54*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-6 - 43*t2 - 42*Power(t2,2) + 69*Power(t2,3) + 
                  54*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-121 - 140*t2 + 1107*Power(t2,2) + 514*Power(t2,3) + 
                  312*Power(t2,4)) + 
               s2*(-30 - 186*t2 + 162*Power(t2,2) - 187*Power(t2,3) + 
                  2511*Power(t2,4) + 705*Power(t2,5) - 111*Power(t2,6)) \
+ s*(90 + (708 + 98*s2 + 30*Power(s2,2))*t2 + 
                  (4104 - 706*s2 + 317*Power(s2,2))*Power(t2,2) + 
                  (3605 - 1316*s2 - 198*Power(s2,2))*Power(t2,3) - 
                  (6005 + 1378*s2 + 3*Power(s2,2))*Power(t2,4) - 
                  (2021 + 1146*s2 + 162*Power(s2,2))*Power(t2,5) + 
                  111*Power(t2,6)) + 
               Power(s,2)*t2*(183 - 1010*t2 - 3327*Power(t2,2) - 
                  384*Power(t2,3) + 834*Power(t2,4) + 
                  s2*(-90 - 33*t2 - 102*Power(t2,2) - 545*Power(t2,3) + 
                     162*Power(t2,4)))) - 
            Power(t1,7)*(50 + 417*t2 + 407*Power(t2,2) - 
               423*Power(t2,3) - 262*Power(t2,4) + 3*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(2 + 12*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*t2*
                (12 - 32*t2 + 142*Power(t2,2) - 59*Power(t2,3)) + 
               Power(s,3)*(10 + 123*t2 + 56*Power(t2,2) - 
                  375*Power(t2,3) + 7*Power(t2,4)) + 
               Power(s,2)*(-32 + (-145 + 57*s2)*t2 + 
                  4*(384 + 79*s2)*Power(t2,2) + 
                  3*(575 + 191*s2)*Power(t2,3) - 
                  (179 + 21*s2)*Power(t2,4)) + 
               s2*(-2 - 25*t2 + 697*Power(t2,2) + 106*Power(t2,3) - 
                  156*Power(t2,4) + 2*Power(t2,5)) + 
               s*(-47 - 727*t2 - 3033*Power(t2,2) - 1096*Power(t2,3) + 
                  567*Power(t2,4) - 2*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-4 + 80*t2 - 210*Power(t2,2) + 21*Power(t2,3)) + 
                  s2*(13 + 57*t2 - 723*Power(t2,2) - 739*Power(t2,3) + 
                     238*Power(t2,4)))) + 
            Power(t1,6)*(-1 + 20*t2 + 88*Power(t2,2) + 1231*Power(t2,3) - 
               247*Power(t2,4) - 239*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(-10 + 4*t2 + 85*Power(t2,2)) + 
               Power(s,3)*t2*(15 + 401*t2 + 645*Power(t2,2) - 
                  237*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (-72 + 518*t2 + 273*Power(t2,2) - 35*Power(t2,3)) + 
               s2*(-14 - 97*t2 + 675*Power(t2,2) + 1023*Power(t2,3) + 
                  730*Power(t2,4) - 85*Power(t2,5)) + 
               Power(s,2)*(-30 + (-600 + 43*s2)*t2 + 
                  (-2725 + 274*s2)*Power(t2,2) + 
                  (90 + 13*s2)*Power(t2,3) + 
                  (1888 + 559*s2)*Power(t2,4) - 35*Power(t2,5)) + 
               s*(61 + 806*t2 + 1633*Power(t2,2) - 5113*Power(t2,3) - 
                  2904*Power(t2,4) + 217*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (13 + 213*t2 - 283*Power(t2,2) - 407*Power(t2,3)) + 
                  s2*(6 - 78*t2 - 1197*Power(t2,2) - 1567*Power(t2,3) - 
                     1610*Power(t2,4) + 70*Power(t2,5))))))*
       T2(t2,1 - s1 - t1 + t2))/
     ((s - s2 + t1)*Power(Power(s1,2) + 2*s1*t1 + Power(t1,2) - 4*t2,2)*
       Power(-1 + s1 + t1 - t2,2)*Power(s1*t1 - t2,3)*(-1 + t2)*
       (s - s1 + t2)) + (8*((-1 + Power(s,3) - s1 - 2*Power(s1,3) - 
            Power(s,2)*(3 + 4*s1) + s*(3 + 5*s1 + 5*Power(s1,2)))*
          Power(t1,10) + Power(t1,9)*
          (4 - 8*Power(s1,4) + Power(s,3)*(-1 + 3*s1 - 7*t2) + 5*t2 + 
            3*s2*t2 + 2*s1*(2 + s2*(-1 + t2) + 3*t2) + 
            Power(s1,2)*(-2 - 3*s2 + 7*t2) + Power(s1,3)*(5 + 11*t2) + 
            Power(s,2)*(6 - 14*Power(s1,2) + 22*t2 + 3*s2*t2 + 
               s1*(-4 + s2 + 26*t2)) + 
            s*(-9 + 19*Power(s1,3) - 20*t2 - 6*s2*t2 - 
               Power(s1,2)*(-8 + s2 + 30*t2) + 
               s1*(-6 + s2 - 39*t2 - 5*s2*t2))) + 
         Power(t2,3)*(-2 - 14*t2 - 5*s*t2 + Power(t2,2) - 
            11*s*Power(t2,2) - 3*Power(s,2)*Power(t2,2) + 
            11*Power(t2,3) + 25*s*Power(t2,3) - 
            24*Power(s,2)*Power(t2,3) + 17*Power(t2,4) - 
            18*s*Power(t2,4) - 12*Power(s,2)*Power(t2,4) - 
            5*Power(t2,5) - 28*s*Power(t2,5) - 
            2*Power(s,2)*Power(t2,5) + 2*Power(s,3)*Power(t2,5) - 
            8*Power(t2,6) - 3*s*Power(t2,6) + Power(s,2)*Power(t2,6) - 
            Power(s2,3)*(4 + 22*t2 + 24*Power(t2,2) + Power(t2,3) - 
               2*Power(t2,4) + Power(t2,5)) + 
            Power(s2,2)*(10 + 2*(17 + s)*t2 + (4 - 18*s)*Power(t2,2) - 
               4*(13 + 5*s)*Power(t2,3) - (17 + 2*s)*Power(t2,4) + 
               4*(1 + s)*Power(t2,5) + Power(t2,6)) + 
            s2*(-4 + 3*(5 + 4*s)*t2 + 
               (52 + 65*s - 2*Power(s,2))*Power(t2,2) + 
               (72 + 34*s + 9*Power(s,2))*Power(t2,3) + 
               (21 + 5*s)*Power(t2,4) + 
               (9 - 2*s - 5*Power(s,2))*Power(t2,5) + 
               (3 - 2*s)*Power(t2,6)) + 
            Power(s1,5)*(Power(s2,3) - 2*Power(s2,2)*t2 + 
               2*s2*Power(t2,2) + 4*t2*(1 + t2)) - 
            Power(s1,4)*(-4 + 4*(3 + s)*t2 + (38 + 4*s)*Power(t2,2) - 
               2*(-9 + s)*Power(t2,3) + Power(s2,3)*(3 + 2*t2) - 
               Power(s2,2)*(-3 + (5 + 4*s)*t2 + 9*Power(t2,2)) + 
               2*s2*(1 + 2*t2 + 3*(2 + s)*Power(t2,2) + 4*Power(t2,3))) \
+ Power(s1,3)*(Power(s2,3)*(2 + 3*t2 + Power(t2,2)) - 
               Power(s2,2)*(-13 + (-19 + 12*s)*t2 + 
                  (13 + 7*s)*Power(t2,2) + 16*Power(t2,3)) + 
               s2*(-3 + (8 + s)*t2 + 
                  5*(8 + 4*s + Power(s,2))*Power(t2,2) + 
                  11*(3 + 2*s)*Power(t2,3) + 12*Power(t2,4)) + 
               2*(-3 + (-8 + s)*t2 + (32 + 13*s)*Power(t2,2) + 
                  (52 + 7*s - 2*Power(s,2))*Power(t2,3) - 
                  3*(-5 + s)*Power(t2,4))) - 
            Power(s1,2)*(3 - (27 + s)*t2 + 
               (-27 + 11*s + 2*Power(s,2))*Power(t2,2) - 
               (-105 - 67*s + Power(s,2) + 2*Power(s,3))*Power(t2,3) + 
               (116 + 23*s - 11*Power(s,2))*Power(t2,4) + 
               (22 - 6*s)*Power(t2,5) + 
               Power(s2,3)*(2 + 9*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(10 - 12*(-4 + s)*t2 + 63*Power(t2,2) - 
                  3*(5 + 2*s)*Power(t2,3) - 14*Power(t2,4)) + 
               s2*(-11 - 2*(23 + 7*s)*t2 + 
                  (21 + s + 13*Power(s,2))*Power(t2,2) + 
                  (59 + 53*s + 8*Power(s,2))*Power(t2,3) + 
                  (27 + 28*s)*Power(t2,4) + 8*Power(t2,5))) + 
            s1*(7 + (11 + 6*s)*t2 + 
               (-46 - s + 5*Power(s,2))*Power(t2,2) + 
               (-23 + 13*s + 23*Power(s,2) - 2*Power(s,3))*Power(t2,3) + 
               (55 + 80*s - 4*Power(s,2) - 3*Power(s,3))*Power(t2,4) + 
               (54 + 16*s - 8*Power(s,2))*Power(t2,5) - 
               2*(-3 + s)*Power(t2,6) + 
               Power(s2,3)*(6 + 30*t2 + 8*Power(t2,2) - 7*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,2)*(10 + (8 + 6*s)*t2 - 
                  (95 + 24*s)*Power(t2,2) - 2*(32 + 7*s)*Power(t2,3) + 
                  (11 + 7*s)*Power(t2,4) + 6*Power(t2,5)) + 
               s2*(-2 - (65 + 27*s)*t2 + 
                  (-95 - 76*s + 10*Power(s,2))*Power(t2,2) + 
                  (-9 + 10*s + 3*Power(s,2))*Power(t2,3) + 
                  (14 + 35*s + 8*Power(s,2))*Power(t2,4) + 
                  (3 + 14*s)*Power(t2,5) + 2*Power(t2,6)))) + 
         t1*Power(t2,2)*(6 + (31 + s - 29*s2 - 24*s*s2 + 
               2*Power(s2,2) - 2*s*Power(s2,2) + 8*Power(s2,3))*t2 + 
            (-13 - 80*s2 + 30*Power(s2,2) + 28*Power(s2,3) + 
               Power(s,2)*(9 + 14*s2) + 
               2*s*(-7 - 72*s2 + 21*Power(s2,2)))*Power(t2,2) - 
            (89 + 3*Power(s,3) + 113*s2 - 115*Power(s2,2) - 
               5*Power(s2,3) + Power(s,2)*(-105 + 37*s2) + 
               s*(146 + 52*s2 - 77*Power(s2,2)))*Power(t2,3) + 
            (-90 + Power(s,3) + Power(s,2)*(47 - 24*s2) - 49*s2 + 
               44*Power(s2,2) - 4*Power(s2,3) + 
               s*(63 + 68*s2 + 23*Power(s2,2)))*Power(t2,4) + 
            (7 - 8*Power(s,3) - 28*s2 - 17*Power(s2,2) + 
               3*Power(s2,3) + 3*Power(s,2)*(-6 + 5*s2) - 
               5*s*(-35 - 7*s2 + 2*Power(s2,2)))*Power(t2,5) + 
            (43 + Power(s,3) - 6*s2 - 4*Power(s2,2) - Power(s2,3) - 
               3*Power(s,2)*(3 + s2) + s*(10 + 13*s2 + 3*Power(s2,2)))*
             Power(t2,6) + (1 - s + s2)*Power(t2,7) - 
            2*Power(s1,6)*t2*(6 - Power(s2,2) + 6*t2 + 2*s2*t2) + 
            Power(s1,5)*(-3*Power(s2,3) - 
               Power(s2,2)*t2*(-1 + s + 11*t2) + 
               2*s2*(3 + 10*t2 + (11 + 4*s)*Power(t2,2) + 
                  8*Power(t2,3)) + 
               2*(-6 + 2*(8 + 3*s)*t2 + 6*(9 + s)*Power(t2,2) + 
                  (27 - 2*s)*Power(t2,3))) - 
            Power(s1,4)*(Power(s2,3)*(-9 - 10*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(3 + 4*(13 + 3*s)*t2 + 
                  (19 - 4*s)*Power(t2,2) - 26*Power(t2,3)) + 
               s2*(-18 + (56 + 19*s)*t2 + 
                  (122 + 17*s + 2*Power(s,2))*Power(t2,2) + 
                  (46 + 34*s)*Power(t2,3) + 24*Power(t2,4)) + 
               2*(-9 + (-35 + s)*t2 + (65 + 34*s)*Power(t2,2) - 
                  3*(-45 - 9*s + Power(s,2))*Power(t2,3) + 
                  (45 - 6*s)*Power(t2,4))) + 
            Power(s1,3)*(6 - 69*t2 + 
               (-227 - 33*s + Power(s,2))*Power(t2,2) + 
               (58 + 150*s + 20*Power(s,2) - Power(s,3))*Power(t2,3) + 
               (252 + 108*s - 19*Power(s,2))*Power(t2,4) + 
               (66 - 12*s)*Power(t2,5) + 
               Power(s2,3)*t2*(11 - 9*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(-15 + (82 + 43*s)*t2 + 
                  (220 + 47*s)*Power(t2,2) - 
                  8*(-5 + 2*s)*Power(t2,3) - 32*Power(t2,4)) + 
               s2*(-69 + 5*(-34 + 3*s)*t2 - 
                  18*(-7 + Power(s,2))*Power(t2,2) + 
                  (122 + 13*s + 10*Power(s,2))*Power(t2,3) + 
                  (5 + 54*s)*Power(t2,4) + 16*Power(t2,5))) + 
            Power(s1,2)*(-9 - (74 + 33*s)*t2 + 
               (73 + 7*s + 2*Power(s,2))*Power(t2,2) + 
               (378 + 199*s - 40*Power(s,2) - 13*Power(s,3))*
                Power(t2,3) + 
               (227 - 136*s - 65*Power(s,2) + 6*Power(s,3))*
                Power(t2,4) + 
               (-71 - 95*s + 18*Power(s,2))*Power(t2,5) + 
               2*(-9 + 2*s)*Power(t2,6) - 
               3*Power(s2,3)*
                (6 + 29*t2 + 15*Power(t2,2) - Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*(48 + (101 - 40*s)*t2 - 
                  (201 + 85*s)*Power(t2,2) - 
                  (253 + 26*s)*Power(t2,3) + 
                  (-31 + 27*s)*Power(t2,4) + 20*Power(t2,5)) + 
               s2*(51 - 13*(-22 + s)*t2 + 
                  (341 + 97*s + 61*Power(s,2))*Power(t2,2) + 
                  2*(11 + 71*s + 15*Power(s,2))*Power(t2,3) + 
                  (35 + 57*s - 24*Power(s,2))*Power(t2,4) + 
                  (41 - 38*s)*Power(t2,5) - 4*Power(t2,6))) + 
            s1*(-9 + 22*(1 + s)*t2 + 
               (149 + 101*s - 12*Power(s,2))*Power(t2,2) + 
               (79 - 61*s - 67*Power(s,2) + 17*Power(s,3))*Power(t2,3) + 
               (-247 - 327*s + 71*Power(s,2) + 14*Power(s,3))*
                Power(t2,4) + 
               (-218 + 11*s + 64*Power(s,2) - 7*Power(s,3))*
                Power(t2,5) + (-8 + 30*s - 5*Power(s,2))*Power(t2,6) + 
               Power(s2,3)*(12 + 58*t2 + 70*Power(t2,2) + 
                  29*Power(t2,3) - 4*Power(t2,4) + 5*Power(t2,5)) + 
               Power(s2,2)*(-30 + 4*(-34 + 3*s)*t2 - 
                  (155 + 3*s)*Power(t2,2) + (50 - 29*s)*Power(t2,3) + 
                  (102 + s)*Power(t2,4) + (13 - 17*s)*Power(t2,5) - 
                  5*Power(t2,6)) + 
               s2*(-6 + (-51 + 41*s)*t2 + 
                  (-137 + 46*s - 55*Power(s,2))*Power(t2,2) - 
                  (233 + 159*s + 27*Power(s,2))*Power(t2,3) - 
                  (57 + 202*s + 7*Power(s,2))*Power(t2,4) + 
                  (-49 - 66*s + 19*Power(s,2))*Power(t2,5) + 
                  (-23 + 10*s)*Power(t2,6)))) + 
         Power(t1,8)*(-3 - 12*Power(s1,5) - 21*t2 - 10*s2*t2 - 
            12*Power(t2,2) - 14*s2*Power(t2,2) - 
            3*Power(s2,2)*Power(t2,2) + 
            Power(s1,4)*(19 - 2*s2 + 40*t2) + 
            Power(s,3)*(-3 + s1 + 3*Power(s1,2) + 5*t2 - 20*s1*t2 + 
               21*Power(t2,2)) - 
            Power(s1,3)*(16 + 2*Power(s2,2) + 5*t2 + 25*Power(t2,2) - 
               s2*(9 + 4*t2)) + 
            s1*(4 - 22*t2 - Power(s2,2)*(-4 + t2)*t2 - 22*Power(t2,2) + 
               s2*(2 + 9*t2 - 12*Power(t2,2))) + 
            Power(s1,2)*(9 - 12*t2 - 38*Power(t2,2) + 
               Power(s2,2)*(-1 + 3*t2) + s2*(9 + 22*t2 - Power(t2,2))) - 
            Power(s,2)*(-6 + 18*Power(s1,3) + 
               Power(s1,2)*(4 - 3*s2 - 85*t2) + 7*(5 + s2)*t2 + 
               (71 + 18*s2)*Power(t2,2) + 
               s1*(-25 + s2*(5 - 6*t2) - 38*t2 + 72*Power(t2,2))) + 
            s*(27*Power(s1,4) - Power(s1,3)*(10 + s2 + 105*t2) - 
               2*Power(s1,2)*
                (28 + 40*t2 - 38*Power(t2,2) + s2*(-6 + 9*t2)) + 
               t2*(54 + 64*t2 + 3*Power(s2,2)*t2 + s2*(17 + 35*t2)) + 
               s1*(-31 + 37*t2 - Power(s2,2)*t2 + 130*Power(t2,2) + 
                  s2*(6 - 9*t2 + 29*Power(t2,2))))) + 
         Power(t1,7)*(-6 - 8*Power(s1,6) + 10*t2 + 7*s2*t2 + 
            52*Power(t2,2) + 45*s2*Power(t2,2) + 
            8*Power(s2,2)*Power(t2,2) + 21*Power(t2,3) + 
            30*s2*Power(t2,3) + 13*Power(s2,2)*Power(t2,3) + 
            Power(s2,3)*Power(t2,3) + Power(s1,5)*(30 - 6*s2 + 59*t2) - 
            Power(s1,4)*(75 + 6*Power(s2,2) + 80*t2 + 84*Power(t2,2) - 
               3*s2*(13 + 9*t2)) + 
            Power(s1,3)*(6 - 43*t2 - 52*Power(t2,2) + 30*Power(t2,3) + 
               2*Power(s2,2)*(-7 + 10*t2) - 
               s2*(-49 + t2 + 23*Power(t2,2))) + 
            Power(s,3)*(1 + Power(s1,3) + Power(s1,2)*(4 - 19*t2) + 
               21*t2 - 8*Power(t2,2) - 35*Power(t2,3) + 
               s1*(-3 - 11*t2 + 57*Power(t2,2))) + 
            Power(s,2)*(-12 - 10*Power(s1,4) - (61 + s2)*t2 + 
               (84 + 33*s2)*Power(t2,2) + 3*(44 + 15*s2)*Power(t2,3) + 
               Power(s1,3)*(-5 + 3*s2 + 102*t2) + 
               Power(s1,2)*(19 + s2*(-9 + t2) + 54*t2 - 
                  219*Power(t2,2)) - 
               s1*(-12 + s2 + 145*t2 - 16*s2*t2 + 149*Power(t2,2) + 
                  51*s2*Power(t2,2) - 110*Power(t2,3))) - 
            s1*(18 + 18*t2 - 75*Power(t2,2) + 
               2*Power(s2,3)*Power(t2,2) - 59*Power(t2,3) + 
               2*Power(s2,2)*t2*(1 + 12*t2 - 3*Power(t2,2)) + 
               s2*(-10 + 44*t2 + 15*Power(t2,2) - 30*Power(t2,3))) + 
            Power(s1,2)*(2 + 13*t2 + Power(s2,3)*t2 + 78*Power(t2,2) + 
               85*Power(t2,3) + 
               Power(s2,2)*(-4 + 7*t2 - 20*Power(t2,2)) + 
               s2*(5 - 96*t2 - 70*Power(t2,2) + 5*Power(t2,3))) + 
            s*(18 + 17*Power(s1,5) + Power(s1,4)*(-37 + 3*s2 - 142*t2) + 
               (28 - 6*s2)*t2 - 
               2*(75 + 43*s2 + 4*Power(s2,2))*Power(t2,2) - 
               (130 + 89*s2 + 15*Power(s2,2))*Power(t2,3) + 
               Power(s1,3)*(-67 + s2*(34 - 36*t2) - 11*t2 + 
                  246*Power(t2,2)) + 
               Power(s1,2)*(-38 + 333*t2 + 302*Power(t2,2) - 
                  105*Power(t2,3) - 2*Power(s2,2)*(1 + 2*t2) + 
                  2*s2*(12 - 21*t2 + 55*Power(t2,2))) + 
               s1*(26 + 248*t2 - 72*Power(t2,2) - 242*Power(t2,3) + 
                  Power(s2,2)*t2*(-6 + 17*t2) - 
                  s2*(14 + t2 - 47*Power(t2,2) + 70*Power(t2,3))))) - 
         Power(t1,6)*(-9 + 2*Power(s1,7) + 
            Power(s1,6)*(-28 + 6*s2 - 46*t2) - 43*t2 - 8*s2*t2 + 
            17*Power(t2,2) + 7*s2*Power(t2,2) + 
            6*Power(s2,2)*Power(t2,2) + 90*Power(t2,3) + 
            91*s2*Power(t2,3) + 30*Power(s2,2)*Power(t2,3) + 
            2*Power(s2,3)*Power(t2,3) + 33*Power(t2,4) + 
            41*s2*Power(t2,4) + 24*Power(s2,2)*Power(t2,4) + 
            4*Power(s2,3)*Power(t2,4) + 
            Power(s1,5)*(126 + 6*Power(s2,2) + 160*t2 + 
               121*Power(t2,2) - s2*(43 + 45*t2)) - 
            Power(s1,4)*(9 + 86*t2 + 89*Power(t2,2) + 98*Power(t2,3) + 
               Power(s2,2)*(-26 + 31*t2) + 
               s2*(109 - 51*t2 - 90*Power(t2,2))) - 
            Power(s1,3)*(38 + 278*t2 + 391*Power(t2,2) + 
               166*Power(t2,3) - 20*Power(t2,4) + 
               Power(s2,3)*(2 + 3*t2) - 
               2*Power(s2,2)*(7 - 6*t2 + 38*Power(t2,2)) + 
               s2*(-27 - 286*t2 - 83*Power(t2,2) + 56*Power(t2,3))) + 
            Power(s,3)*(-2 + 5*t2 + Power(s1,2)*(23 - 50*t2)*t2 + 
               62*Power(t2,2) + Power(t2,3) - 35*Power(t2,4) + 
               Power(s1,3)*(-2 + 6*t2) + 
               s1*(4 - 13*t2 - 47*Power(t2,2) + 90*Power(t2,3))) + 
            Power(s1,2)*(20 + 3*t2 - 44*Power(t2,2) + 135*Power(t2,3) + 
               100*Power(t2,4) + 2*Power(s2,3)*t2*(-2 + 5*t2) - 
               Power(s2,2)*(10 + 46*t2 - 30*Power(t2,2) + 
                  55*Power(t2,3)) + 
               s2*(13 + 172*t2 - 225*Power(t2,2) - 133*Power(t2,3) + 
                  10*Power(t2,4))) + 
            s1*(-8 - 63*t2 + 23*Power(t2,2) + 194*Power(t2,3) - 
               11*Power(s2,3)*Power(t2,3) + 104*Power(t2,4) + 
               2*Power(s2,2)*t2*
                (6 - 16*t2 - 30*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(16 + 9*t2 - 214*Power(t2,2) - 19*Power(t2,3) + 
                  42*Power(t2,4))) - 
            s*(-9 + 4*Power(s1,6) + Power(s1,5)*(-42 + 5*s2 - 93*t2) - 
               2*(61 + 11*s2)*t2 + 
               (-177 - 5*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (253 + 181*s2 + 31*Power(s2,2))*Power(t2,3) + 
               (181 + 130*s2 + 30*Power(s2,2))*Power(t2,4) + 
               Power(s1,4)*(-16 + s2*(38 - 36*t2) + 120*t2 + 
                  309*Power(t2,2)) + 
               s1*(24 + (-75 + 8*s2 + 18*Power(s2,2))*t2 + 
                  4*(-212 - 23*s2 + 4*Power(s2,2))*Power(t2,2) + 
                  (7 - 151*s2 - 64*Power(s2,2))*Power(t2,3) + 
                  5*(55 + 18*s2)*Power(t2,4)) + 
               Power(s1,3)*(-29 + 382*t2 + 222*Power(t2,2) - 
                  316*Power(t2,3) - Power(s2,2)*(8 + 5*t2) + 
                  s2*(19 - 99*t2 + 174*Power(t2,2))) + 
               Power(s1,2)*(49 + 308*t2 - 785*Power(t2,2) - 
                  603*Power(t2,3) + 85*Power(t2,4) + 
                  Power(s2,2)*(2 - 9*t2 + 35*Power(t2,2)) - 
                  s2*(37 + 123*t2 - 102*Power(t2,2) + 248*Power(t2,3)))) \
+ Power(s,2)*(3 + 2*Power(s1,5) - (66 + 13*s2)*t2 - 
               (239 + 17*s2)*Power(t2,2) + (101 + 57*s2)*Power(t2,3) + 
               5*(31 + 12*s2)*Power(t2,4) - Power(s1,4)*(s2 + 53*t2) - 
               Power(s1,2)*(19 + s2 - 99*t2 + 38*s2*t2 - 
                  236*Power(t2,2) - 48*s2*Power(t2,2) + 310*Power(t2,3)) \
+ Power(s1,3)*(6 - 43*t2 + 238*Power(t2,2) + s2*(3 + 5*t2)) + 
               s1*(-(s2*(5 + 4*t2 - 28*Power(t2,2) + 130*Power(t2,3))) + 
                  4*(3 + 23*t2 - 84*Power(t2,2) - 80*Power(t2,3) + 
                     25*Power(t2,4))))) + 
         Power(t1,5)*(-2*Power(s1,7)*(-8 + s2 - 10*t2) - 
            2*Power(s1,6)*(47 + Power(s2,2) + 68*t2 + 46*Power(t2,2) - 
               s2*(8 + 13*t2)) + 
            Power(s1,5)*(16 + 184*t2 + 228*Power(t2,2) + 
               135*Power(t2,3) + Power(s2,2)*(-19 + 16*t2) + 
               s2*(102 - 14*t2 - 99*Power(t2,2))) + 
            Power(s,3)*t2*(-11 + 11*t2 + 98*Power(t2,2) + 
               20*Power(t2,3) - 21*Power(t2,4) + 
               Power(s1,3)*(-9 + 14*t2) + 
               Power(s1,2)*(-5 + 58*t2 - 70*Power(t2,2)) + 
               5*s1*(5 - 4*t2 - 21*Power(t2,2) + 17*Power(t2,3))) + 
            Power(s1,4)*(72 + 462*t2 + 406*Power(t2,2) + 
               57*Power(t2,3) - 70*Power(t2,4) + 
               Power(s2,3)*(2 + 4*t2) + 
               Power(s2,2)*(-31 + 5*t2 - 85*Power(t2,2)) + 
               s2*(-41 - 354*t2 - 62*Power(t2,2) + 138*Power(t2,3))) + 
            Power(s1,3)*(-28 - 115*t2 + Power(s2,3)*(12 - 13*t2)*t2 - 
               643*Power(t2,2) - 786*Power(t2,3) - 229*Power(t2,4) + 
               7*Power(t2,5) + 
               3*Power(s2,2)*
                (14 + 42*t2 + 5*Power(t2,2) + 46*Power(t2,3)) + 
               s2*(-62 - 404*t2 + 398*Power(t2,2) + 208*Power(t2,3) - 
                  72*Power(t2,4))) + 
            t2*(-48 - 105*t2 + 50*Power(t2,2) + 137*Power(t2,3) + 
               44*Power(t2,4) + 
               2*Power(s2,3)*Power(t2,2)*(1 + 3*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-2 + 5*t2 + 44*Power(t2,2) + 26*Power(t2,3)) + 
               s2*(-11 - 71*t2 - 49*Power(t2,2) + 113*Power(t2,3) + 
                  40*Power(t2,4))) + 
            Power(s1,2)*(9 - 55*t2 - 421*Power(t2,2) - 
               514*Power(t2,3) + 71*Power(t2,4) + 65*Power(t2,5) + 
               2*Power(s2,3)*t2*(-8 - 9*t2 + 15*Power(t2,2)) + 
               Power(s2,2)*(2 + 7*t2 + 29*Power(t2,2) + 
                  93*Power(t2,3) - 79*Power(t2,4)) + 
               s2*(-1 + 198*t2 + 608*Power(t2,2) - 219*Power(t2,3) - 
                  179*Power(t2,4) + 10*Power(t2,5))) + 
            s1*(5 - 2*t2 - 99*Power(t2,2) + 68*Power(t2,3) + 
               325*Power(t2,4) + 112*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(2 + 2*t2 - 23*Power(t2,2)) + 
               2*Power(s2,2)*t2*
                (6 + t2 - 49*Power(t2,2) - 45*Power(t2,3) + 
                  8*Power(t2,4)) + 
               s2*(-2 + 83*t2 + 106*Power(t2,2) - 408*Power(t2,3) - 
                  38*Power(t2,4) + 38*Power(t2,5))) + 
            Power(s,2)*(6 - 2*(-15 + s2)*t2 - 
               (143 + 73*s2)*Power(t2,2) - 
               3*(164 + 21*s2)*Power(t2,3) + 7*(7 + 5*s2)*Power(t2,4) + 
               (118 + 45*s2)*Power(t2,5) + 2*Power(s1,5)*(1 + 5*t2) + 
               Power(s1,4)*(-8 + s2 + 5*t2 - 3*s2*t2 - 
                  107*Power(t2,2)) + 
               Power(s1,2)*(13 - 6*(12 + s2)*t2 + 
                  (206 - 77*s2)*Power(t2,2) + 
                  2*(251 + 61*s2)*Power(t2,3) - 260*Power(t2,4)) + 
               s1*(-17 + (41 - 33*s2)*t2 + 
                  2*(146 + 5*s2)*Power(t2,2) + 
                  (-362 + 71*s2)*Power(t2,3) - 
                  5*(83 + 33*s2)*Power(t2,4) + 54*Power(t2,5)) + 
               Power(s1,3)*(s2*(-1 + 20*t2 - 13*Power(t2,2)) + 
                  2*(2 + 5*t2 - 79*Power(t2,2) + 146*Power(t2,3)))) + 
            s*(-9 + 2*Power(s1,6)*(-11 + s2 - 15*t2) + 9*(3 + 2*s2)*t2 + 
               (323 + 160*s2 + 2*Power(s2,2))*Power(t2,2) - 
               15*(-33 - 7*s2 + Power(s2,2))*Power(t2,3) - 
               (279 + 199*s2 + 41*Power(s2,2))*Power(t2,4) - 
               (173 + 120*s2 + 30*Power(s2,2))*Power(t2,5) - 
               Power(s1,4)*(8 - 161*t2 + 35*Power(t2,2) + 
                  351*Power(t2,3) + Power(s2,2)*(7 + 4*t2) + 
                  s2*(-13 + 103*t2 - 114*Power(t2,2))) + 
               Power(s1,5)*(s2*(21 - 13*t2) + 
                  5*(2 + 26*t2 + 37*Power(t2,2))) + 
               Power(s1,3)*(12 + 174*t2 - 889*Power(t2,2) - 
                  606*Power(t2,3) + 239*Power(t2,4) + 
                  Power(s2,2)*(8 - t2 + 29*Power(t2,2)) - 
                  2*s2*(18 + 74*t2 - 67*Power(t2,2) + 160*Power(t2,3))) \
+ Power(s1,2)*(20 - 155*t2 - 947*Power(t2,2) + 880*Power(t2,3) + 
                  716*Power(t2,4) - 40*Power(t2,5) + 
                  Power(s2,2)*t2*(39 + 52*t2 - 97*Power(t2,2)) + 
                  s2*(-15 + 81*t2 + 133*Power(t2,2) - 260*Power(t2,3) + 
                     287*Power(t2,4))) + 
               s1*(-2 - 199*t2 - 55*Power(t2,2) + 1593*Power(t2,3) + 
                  184*Power(t2,4) - 195*Power(t2,5) + 
                  Power(s2,2)*t2*
                   (-4 - 64*t2 - 27*Power(t2,2) + 106*Power(t2,3)) + 
                  s2*(13 + 27*t2 + 102*Power(t2,2) + 248*Power(t2,3) + 
                     289*Power(t2,4) - 65*Power(t2,5))))) - 
         Power(t1,2)*t2*(6 - 3*(-11 - s2 + s*(5 + 6*s2))*t2 + 
            (1 - 22*s2 + 38*Power(s2,2) + Power(s,2)*(19 + 28*s2) + 
               s*(-89 - 104*s2 + 28*Power(s2,2)))*Power(t2,2) - 
            (160 + 14*Power(s,3) + 18*s2 - 69*Power(s2,2) + 
               3*Power(s2,3) + 3*Power(s,2)*(-64 + 19*s2) + 
               s*(274 - 54*s2 - 101*Power(s2,2)))*Power(t2,3) + 
            (5*Power(s,3) + Power(s,2)*(52 - 106*s2) + 
               s*(146 + 330*s2 + 59*Power(s2,2)) - 
               2*(75 + 23*s2 + 2*Power(s2,3)))*Power(t2,4) - 
            (-24 + 4*Power(s,3) + 52*s2 + 44*Power(s2,2) - 
               5*Power(s2,3) + 3*Power(s,2)*(49 + s2) + 
               3*s*(-163 - 49*s2 + Power(s2,2)))*Power(t2,5) + 
            (110 + 7*Power(s,3) + 10*s2 - 4*Power(s2,2) - 
               3*Power(s2,3) - Power(s,2)*(31 + 17*s2) + 
               s*(-3 + 25*s2 + 13*Power(s2,2)))*Power(t2,6) + 
            (8 + 2*Power(s,2) + 5*s2 + 2*Power(s2,2) - 2*s*(5 + 2*s2))*
             Power(t2,7) - 2*Power(s1,7)*t2*(6 + (6 + s2)*t2) + 
            2*Power(s1,6)*(-6 + 6*(2 + s)*t2 - 
               Power(s2,2)*(-3 + t2)*t2 + (49 + 6*s)*Power(t2,2) - 
               (-27 + s)*Power(t2,3) + 
               s2*(3 + 12*t2 + (4 + s)*Power(t2,2) + 4*Power(t2,3))) - 
            Power(s1,5)*(3*Power(s2,3) + 
               Power(s2,2)*(12 + 3*(15 + s)*t2 + 38*Power(t2,2) - 
                  10*Power(t2,3)) + 
               s2*(-18 + 6*(11 + 3*s)*t2 - 3*(-38 + s)*Power(t2,2) + 
                  (-7 + 12*s)*Power(t2,3) + 12*Power(t2,4)) - 
               2*(9 + 3*(20 + s)*t2 - (10 + 29*s)*Power(t2,2) + 
                  (-97 - 33*s + Power(s,2))*Power(t2,3) + 
                  3*(-15 + s)*Power(t2,4))) + 
            Power(s1,4)*(6 - 6*(12 + s)*t2 + 
               2*(-246 - 61*s + 2*Power(s,2))*Power(t2,2) + 
               (-383 + 59*s + 31*Power(s,2))*Power(t2,3) + 
               (72 + 147*s - 8*Power(s,2))*Power(t2,4) - 
               6*(-11 + s)*Power(t2,5) + 
               Power(s2,3)*(15 + 42*t2 - 8*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*(21 - 6*(-17 + s)*t2 + 
                  (166 + 25*s)*Power(t2,2) + (89 - 4*s)*Power(t2,3) - 
                  18*Power(t2,4)) + 
               s2*(-60 - 3*(88 + 9*s)*t2 + 
                  (91 + 12*s - 7*Power(s,2))*Power(t2,2) + 
                  (89 - 67*s + 2*Power(s,2))*Power(t2,3) + 
                  (-71 + 26*s)*Power(t2,4) + 8*Power(t2,5))) + 
            Power(s1,3)*(-12 - 3*(45 + 4*s)*t2 - 
               (89 + 17*s + 21*Power(s,2))*Power(t2,2) + 
               (749 + 507*s + 16*Power(s,2) - 6*Power(s,3))*
                Power(t2,3) + 
               (843 + 131*s - 116*Power(s,2) + 2*Power(s,3))*
                Power(t2,4) + 
               (103 - 142*s + 10*Power(s,2))*Power(t2,5) + 
               2*(-9 + s)*Power(t2,6) - 
               3*Power(s2,3)*
                (8 + 37*t2 + 19*Power(t2,2) - 9*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,2)*(3 + (48 + 45*s)*t2 + 
                  (-20 + 37*s)*Power(t2,2) - 
                  9*(18 + 7*s)*Power(t2,3) + 
                  (-113 + 14*s)*Power(t2,4) + 14*Power(t2,5)) + 
               s2*(15 + (324 + 87*s)*t2 + 
                  (700 + 140*s - 17*Power(s,2))*Power(t2,2) + 
                  (119 + 104*s + 33*Power(s,2))*Power(t2,3) + 
                  (7 + 195*s - 10*Power(s,2))*Power(t2,4) + 
                  (91 - 24*s)*Power(t2,5) - 2*Power(t2,6))) + 
            Power(s1,2)*(3 + (30 - 45*s)*t2 + 
               (354 + 79*s + 39*Power(s,2))*Power(t2,2) + 
               (611 + 293*s - 121*Power(s,2) - 32*Power(s,3))*
                Power(t2,3) + 
               (-85 - 789*s - 106*Power(s,2) + 31*Power(s,3))*
                Power(t2,4) - 
               (499 + 286*s - 132*Power(s,2) + 4*Power(s,3))*
                Power(t2,5) + (-70 + 51*s - 4*Power(s,2))*Power(t2,6) + 
               Power(s2,3)*(12 + 45*t2 + 39*Power(t2,2) + 
                  5*Power(t2,3) - 33*Power(t2,4) + 6*Power(t2,5)) - 
               Power(s2,2)*(12 + 3*(37 + 16*s)*t2 + 
                  3*(141 + 55*s)*Power(t2,2) + 
                  (356 + 27*s)*Power(t2,3) - 
                  2*(22 + 47*s)*Power(t2,4) + 
                  (-83 + 16*s)*Power(t2,5) + 4*Power(t2,6)) + 
               s2*(39 - 9*(-18 + 7*s)*t2 + 
                  (-40 + 6*s + 93*Power(s,2))*Power(t2,2) + 
                  (-591 + 2*s + 51*Power(s,2))*Power(t2,3) + 
                  (17 - 77*s - 89*Power(s,2))*Power(t2,4) + 
                  (57 - 200*s + 14*Power(s,2))*Power(t2,5) + 
                  (-37 + 8*s)*Power(t2,6))) + 
            s1*(-9 + 12*(1 + 5*s)*t2 + 
               (72 + 205*s - 41*Power(s,2))*Power(t2,2) + 
               (-91 - 292*s - 60*Power(s,2) + 52*Power(s,3))*
                Power(t2,3) + 
               (-599 - 627*s + 277*Power(s,2) + 25*Power(s,3))*
                Power(t2,4) + 
               (-425 + 356*s + 170*Power(s,2) - 41*Power(s,3))*
                Power(t2,5) + 
               (45 + 131*s - 44*Power(s,2) + 2*Power(s,3))*Power(t2,6) + 
               (3 - 2*s)*Power(t2,7) + 
               Power(s2,3)*t2*
                (24 + 94*t2 + 53*Power(t2,2) + 8*Power(t2,3) + 
                  17*Power(t2,4) - 2*Power(t2,5)) + 
               s2*(-18 + 3*(-61 + 13*s)*t2 - 
                  (377 + 79*s + 97*Power(s,2))*Power(t2,2) - 
                  (342 + 336*s + 89*Power(s,2))*Power(t2,3) + 
                  (22 - 362*s + 43*Power(s,2))*Power(t2,4) + 
                  (-155 - 88*s + 90*Power(s,2))*Power(t2,5) + 
                  (-68 + 73*s - 6*Power(s,2))*Power(t2,6) + 
                  2*Power(t2,7)) + 
               Power(s2,2)*t2*
                (t2*(55 + 408*t2 + 298*Power(t2,2) + Power(t2,3) - 
                     29*Power(t2,4)) + 
                  s*(12 + 85*t2 + 23*Power(t2,2) - 49*Power(t2,3) - 
                     66*Power(t2,4) + 6*Power(t2,5))))) + 
         Power(t1,3)*(2 + (21 + s2 - s*(17 + 6*s2))*t2 + 
            (17 - 2*Power(s2,2) + Power(s,2)*(29 + 22*s2) + 
               s*(-105 - 30*s2 + 4*Power(s2,2)))*Power(t2,2) - 
            (135 + 26*Power(s,3) + 16*s2 + 3*Power(s2,2) + 
               5*Power(s2,3) + Power(s,2)*(-189 + 41*s2) + 
               s*(201 - 146*s2 - 51*Power(s2,2)))*Power(t2,3) + 
            (-135 + 11*Power(s,3) - 92*s2 - 66*Power(s2,2) - 
               2*Power(s2,3) - Power(s,2)*(36 + 187*s2) + 
               s*(308 + 552*s2 + 59*Power(s2,2)))*Power(t2,4) + 
            (81 + 33*Power(s,3) - 98*s2 - 60*Power(s2,2) + 
               7*Power(s2,3) - 9*Power(s,2)*(45 + 7*s2) + 
               s*(790 + 274*s2 + 5*Power(s2,2)))*Power(t2,5) + 
            (172 + 20*Power(s,3) + 52*s2 + 8*Power(s2,2) - 
               Power(s2,3) - Power(s,2)*(50 + 33*s2) + 
               2*s*(-38 - 4*s2 + 7*Power(s2,2)))*Power(t2,6) + 
            (25 - Power(s,3) + 14*s2 + 9*Power(s2,2) + Power(s2,3) + 
               Power(s,2)*(16 + 3*s2) - s*(44 + 25*s2 + 3*Power(s2,2)))*
             Power(t2,7) - 4*Power(s1,8)*t2*(1 + t2) + 
            2*Power(s1,7)*(-2 + 12*Power(t2,2) + 9*Power(t2,3) + 
               2*s*t2*(1 + t2) + s2*(1 + 4*t2 - Power(t2,2))) - 
            2*Power(s1,6)*(-3 - (46 + 5*s)*t2 + 
               (-47 + 6*s)*Power(t2,2) + (-3 + 13*s)*Power(t2,3) + 
               15*Power(t2,4) + 
               Power(s2,2)*(2 + 3*t2 + 3*Power(t2,2)) + 
               s2*(-3 + 3*(3 + s)*t2 + 18*Power(t2,2) - 10*Power(t2,3))) \
+ Power(s1,5)*(Power(s2,3)*(1 + 8*t2) + 
               Power(s2,2)*(4 - (-35 + s)*t2 + 2*(5 + s)*Power(t2,2) + 
                  34*Power(t2,3)) - 
               s2*(20 + 2*(93 + 10*s)*t2 + (36 - 19*s)*Power(t2,2) + 
                  3*(-20 + 9*s)*Power(t2,3) + 49*Power(t2,4)) + 
               2*(1 - (17 + 3*s)*t2 + 
                  (-194 - 62*s + Power(s,2))*Power(t2,2) + 
                  (-250 - 37*s + 5*Power(s,2))*Power(t2,3) + 
                  (-74 + 31*s)*Power(t2,4) + 11*Power(t2,5))) + 
            Power(s1,4)*(-4 - 2*(52 + 3*s)*t2 - 
               2*(173 + 24*s)*Power(t2,2) + 
               (310 + 401*s + 49*Power(s,2))*Power(t2,3) + 
               (693 + 352*s - 47*Power(s,2))*Power(t2,4) - 
               7*(-29 + 9*s)*Power(t2,5) - 6*Power(t2,6) + 
               Power(s2,3)*(-3 - 16*t2 - 28*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,2)*(13 + 2*(34 + s)*t2 + 
                  (86 + 25*s)*Power(t2,2) + (51 - 12*s)*Power(t2,3) - 
                  71*Power(t2,4)) + 
               s2*(8 + (154 + 5*s)*t2 + 
                  (607 + 114*s - 7*Power(s,2))*Power(t2,2) + 
                  (162 - 59*s + 3*Power(s,2))*Power(t2,3) + 
                  (-139 + 103*s)*Power(t2,4) + 47*Power(t2,5))) + 
            Power(s1,2)*(1 + (16 - 25*s)*t2 + 
               (178 + 171*s + 93*Power(s,2))*Power(t2,2) + 
               (193 + 200*s - 160*Power(s,2) - 38*Power(s,3))*
                Power(t2,3) + 
               (-848 - 1523*s + 4*Power(s,2) + 68*Power(s,3))*
                Power(t2,4) - 
               (1048 + 226*s - 386*Power(s,2) + 23*Power(s,3))*
                Power(t2,5) + 
               (-109 + 226*s - 35*Power(s,2))*Power(t2,6) - 
               (-3 + s)*Power(t2,7) + 
               Power(s2,3)*t2*
                (25 + 97*t2 + 19*Power(t2,2) - 43*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s2,2)*(6 + (57 - 20*s)*t2 - 
                  (37 + 71*s)*Power(t2,2) + (76 + 43*s)*Power(t2,3) + 
                  9*(28 + 15*s)*Power(t2,4) + 
                  (156 - 71*s)*Power(t2,5) - 25*Power(t2,6)) + 
               s2*(9 - (70 + 81*s)*t2 + 
                  (-620 - 228*s + 55*Power(s,2))*Power(t2,2) + 
                  (-1024 - 203*s + 40*Power(s,2))*Power(t2,3) - 
                  2*(-121 + 127*s + 67*Power(s,2))*Power(t2,4) + 
                  (2 - 420*s + 69*Power(s,2))*Power(t2,5) + 
                  4*(-28 + 15*s)*Power(t2,6) + Power(t2,7))) - 
            Power(s1,3)*(Power(s2,3)*
                (-2 + 17*t2 + 19*Power(t2,2) - 43*Power(t2,3) + 
                  21*Power(t2,4)) + 
               Power(s2,2)*(19 + (152 - 13*s)*t2 + 
                  (358 + 27*s)*Power(t2,2) + 
                  (332 + 95*s)*Power(t2,3) + 
                  (151 - 47*s)*Power(t2,4) - 66*Power(t2,5)) + 
               s2*(-1 - 7*(24 + 11*s)*t2 + 
                  (-388 - 116*s + Power(s,2))*Power(t2,2) + 
                  (580 + 29*s - 50*Power(s,2))*Power(t2,3) + 
                  2*(7 - 133*s + 17*Power(s,2))*Power(t2,4) + 
                  11*(-19 + 12*s)*Power(t2,5) + 17*Power(t2,6)) + 
               t2*(-19 - 355*t2 - 1217*Power(t2,2) + 
                  Power(s,3)*(14 - 9*t2)*Power(t2,2) - 
                  819*Power(t2,3) + 189*Power(t2,4) + 84*Power(t2,5) + 
                  Power(s,2)*t2*
                   (41 + 14*t2 + 262*Power(t2,2) - 70*Power(t2,3)) + 
                  s*(8 + 3*t2 - 624*Power(t2,2) + 364*Power(t2,3) + 
                     483*Power(t2,4) - 24*Power(t2,5)))) + 
            s1*(-3 + 6*(-1 + 8*s)*t2 + 
               (-2 + 127*s - 83*Power(s,2))*Power(t2,2) + 
               (-245 - 604*s + 6*Power(s,2) + 78*Power(s,3))*
                Power(t2,3) + 
               (-696 - 720*s + 501*Power(s,2) + 17*Power(s,3))*
                Power(t2,4) + 
               (-428 + 1132*s + 155*Power(s,2) - 101*Power(s,3))*
                Power(t2,5) + 
               (184 + 280*s - 163*Power(s,2) + 15*Power(s,3))*
                Power(t2,6) + (23 - 20*s + 2*Power(s,2))*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*
                (2 + 13*t2 + 16*Power(t2,2) + 21*Power(t2,3) - 
                  11*Power(t2,4)) - 
               s2*(6 + (57 - 31*s)*t2 + 
                  (105 + 11*s + 69*Power(s,2))*Power(t2,2) + 
                  2*(-103 + 104*s + 66*Power(s,2))*Power(t2,3) + 
                  (-317 + 182*s - 90*Power(s,2))*Power(t2,4) + 
                  (279 - 51*s - 166*Power(s,2))*Power(t2,5) + 
                  (94 - 213*s + 41*Power(s,2))*Power(t2,6) + 
                  2*(-5 + 2*s)*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-2 + 131*t2 + 406*Power(t2,2) + 306*Power(t2,3) - 
                  53*Power(t2,4) - 69*Power(t2,5) + 2*Power(t2,6) + 
                  s*(6 + 77*t2 + 73*Power(t2,2) - 101*Power(t2,3) - 
                     94*Power(t2,4) + 37*Power(t2,5))))) + 
         Power(t1,4)*(-5 - 4*t2 + 2*s2*t2 + 92*Power(t2,2) + 
            50*s2*Power(t2,2) + 5*Power(s2,2)*Power(t2,2) + 
            126*Power(t2,3) + 137*s2*Power(t2,3) + 
            41*Power(s2,2)*Power(t2,3) - 99*Power(t2,4) + 
            114*s2*Power(t2,4) + 30*Power(s2,2)*Power(t2,4) - 
            6*Power(s2,3)*Power(t2,4) - 179*Power(t2,5) - 
            96*s2*Power(t2,5) - 31*Power(s2,2)*Power(t2,5) - 
            5*Power(s2,3)*Power(t2,5) - 42*Power(t2,6) - 
            28*s2*Power(t2,6) - 19*Power(s2,2)*Power(t2,6) - 
            4*Power(s2,3)*Power(t2,6) + 4*Power(s1,8)*(1 + t2) - 
            2*Power(s1,7)*(13 - 2*(-12 + s2)*t2 + 17*Power(t2,2)) + 
            2*Power(s1,6)*(2 + Power(s2,2)*(-3 + t2) + 40*t2 + 
               72*Power(t2,2) + 42*Power(t2,3) + 
               s2*(20 + 12*t2 - 17*Power(t2,2))) + 
            Power(s,3)*Power(t2,2)*
             (24 - 16*Power(s1,3)*(-1 + t2) - 14*t2 - 85*Power(t2,2) - 
               29*Power(t2,3) + 7*Power(t2,4) + 
               Power(s1,2)*(22 - 82*t2 + 55*Power(t2,2)) + 
               s1*(-62 + 7*t2 + 135*Power(t2,2) - 48*Power(t2,3))) + 
            Power(s1,5)*(34 + 324*t2 + 2*Power(s2,3)*t2 + 
               222*Power(t2,2) - 40*Power(t2,3) - 83*Power(t2,4) - 
               Power(s2,2)*(17 + 10*t2 + 36*Power(t2,2)) + 
               s2*(-12 - 172*t2 - 82*Power(t2,2) + 97*Power(t2,3))) + 
            Power(s1,4)*(Power(s2,3)*(4 + 16*t2 - 6*Power(t2,2)) + 
               Power(s2,2)*(29 + 123*t2 + 37*Power(t2,2) + 
                  113*Power(t2,3)) + 
               s2*(-66 - 433*t2 + 182*Power(t2,2) + 166*Power(t2,3) - 
                  112*Power(t2,4)) + 
               2*(-5 - 83*t2 - 454*Power(t2,2) - 474*Power(t2,3) - 
                  111*Power(t2,4) + 15*Power(t2,5))) + 
            Power(s1,3)*(-3 - 169*t2 - 535*Power(t2,2) + 
               101*Power(t2,3) + 680*Power(t2,4) + 181*Power(t2,5) - 
               Power(t2,6) + Power(s2,3)*
                (-4 - 34*t2 - 45*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,2)*(7 + 81*t2 + 38*Power(t2,2) + 
                  57*Power(t2,3) - 132*Power(t2,4)) + 
               s2*(30 + 346*t2 + 1031*Power(t2,2) - 157*Power(t2,3) - 
                  267*Power(t2,4) + 50*Power(t2,5))) + 
            s1*(1 - 30*t2 + 92*Power(t2,2) + 343*Power(t2,3) + 
               136*Power(t2,4) - 325*Power(t2,5) - 70*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*
                (10 - 8*t2 - 11*Power(t2,2) + 23*Power(t2,3)) + 
               Power(s2,2)*Power(t2,2)*
                (-110 - 89*t2 + 115*Power(t2,2) + 95*Power(t2,3) - 
                  9*Power(t2,4)) + 
               s2*(14 - 2*t2 - 365*Power(t2,2) - 359*Power(t2,3) + 
                  406*Power(t2,4) + 74*Power(t2,5) - 24*Power(t2,6))) + 
            Power(s1,2)*(1 + 37*t2 + 216*Power(t2,2) + 
               1074*Power(t2,3) + 1090*Power(t2,4) + 57*Power(t2,5) - 
               22*Power(t2,6) + 
               Power(s2,3)*t2*
                (-2 + 12*t2 + 33*Power(t2,2) - 40*Power(t2,3)) + 
               Power(s2,2)*(-13 - 132*t2 - 227*Power(t2,2) - 
                  262*Power(t2,3) - 162*Power(t2,4) + 62*Power(t2,5)) + 
               s2*(-6 + 157*t2 + 251*Power(t2,2) - 674*Power(t2,3) + 
                  103*Power(t2,4) + 176*Power(t2,5) - 5*Power(t2,6))) + 
            Power(s,2)*t2*(-22 - 105*t2 + 142*Power(t2,2) + 
               586*Power(t2,3) + 24*Power(t2,4) - 57*Power(t2,5) - 
               4*Power(s1,5)*(1 + 4*t2) + 
               Power(s1,4)*(12 + s2 - 29*t2 + s2*t2 + 103*Power(t2,2)) + 
               s2*(-6 + 14*t2 + 165*Power(t2,2) + 97*Power(t2,3) + 
                  15*Power(t2,4) - 18*Power(t2,5)) + 
               Power(s1,3)*(15 + 10*t2 + 285*Power(t2,2) - 
                  198*Power(t2,3) + s2*(-2 - 44*t2 + 39*Power(t2,2))) - 
               Power(s1,2)*(67 - 131*t2 + 182*Power(t2,2) + 
                  588*Power(t2,3) - 129*Power(t2,4) + 
                  s2*(10 + 6*t2 - 117*Power(t2,2) + 133*Power(t2,3))) + 
               s1*(66 - 51*t2 - 503*Power(t2,2) + 105*Power(t2,3) + 
                  334*Power(t2,4) - 16*Power(t2,5) + 
                  s2*(17 + 95*t2 - 61*Power(t2,2) - 149*Power(t2,3) + 
                     114*Power(t2,4)))) + 
            s*(6 + 5*(10 + s2)*t2 + 
               (30 - 92*s2 - 7*Power(s2,2))*Power(t2,2) - 
               (426 + 433*s2 + 23*Power(s2,2))*Power(t2,3) + 
               (-794 - 254*s2 + 7*Power(s2,2))*Power(t2,4) + 
               (197 + 109*s2 + 14*Power(s2,2))*Power(t2,5) + 
               (110 + 71*s2 + 15*Power(s2,2))*Power(t2,6) - 
               4*Power(s1,7)*(1 + t2) + 
               Power(s1,6)*(2 + 6*s2 + 46*t2 + 50*Power(t2,2)) - 
               Power(s1,5)*(Power(s2,2)*(1 + 2*t2) + 
                  s2*(-2 + 45*t2 - 23*Power(t2,2)) + 
                  t2*(-44 + 76*t2 + 165*Power(t2,2))) + 
               Power(s1,4)*(6 + 6*t2 - 435*Power(t2,2) - 
                  265*Power(t2,3) + 214*Power(t2,4) + 
                  3*Power(s2,2)*t2*(1 + 4*t2) - 
                  s2*(13 + 92*t2 - 91*Power(t2,2) + 158*Power(t2,3))) + 
               Power(s1,3)*(-6 - 19*t2 - 426*Power(t2,2) + 
                  967*Power(t2,3) + 758*Power(t2,4) - 105*Power(t2,5) + 
                  Power(s2,2)*
                   (1 + 16*t2 + 57*Power(t2,2) - 57*Power(t2,3)) + 
                  s2*(-6 + 40*t2 + 227*Power(t2,2) - 194*Power(t2,3) + 
                     291*Power(t2,4))) + 
               Power(s1,2)*(12 - 108*t2 + 67*Power(t2,2) + 
                  1570*Power(t2,3) - 343*Power(t2,4) - 522*Power(t2,5) + 
                  10*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (-9 - 91*t2 - 109*Power(t2,2) + 121*Power(t2,3)) + 
                  s2*(17 + 153*t2 + 72*Power(t2,2) + 114*Power(t2,3) + 
                     446*Power(t2,4) - 182*Power(t2,5))) - 
               s1*(16 + 20*t2 - 535*Power(t2,2) - 444*Power(t2,3) + 
                  1765*Power(t2,4) + 327*Power(t2,5) - 84*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (13 + 31*t2 - 104*Power(t2,2) - 62*Power(t2,3) + 
                     89*Power(t2,4)) + 
                  s2*(6 + 51*t2 + 6*Power(t2,2) + 109*Power(t2,3) + 
                     248*Power(t2,4) + 327*Power(t2,5) - 25*Power(t2,6))))\
))*T3(t2,t1))/((s - s2 + t1)*Power(t1 - t2,2)*Power(-1 + s1 + t1 - t2,2)*
       Power(s1*t1 - t2,3)*(-1 + t2)*(s - s1 + t2)) - 
    (8*(2 - 8*Power(s1,8)*(-1 + t1)*Power(t1,3) - 
         Power(-1 + s,3)*Power(t1,10) + 6*t2 + s*t2 + s2*t2 - 6*s*s2*t2 + 
         5*Power(t2,2) - 9*s*Power(t2,2) - Power(s,2)*Power(t2,2) + 
         6*s2*Power(t2,2) - 15*s*s2*Power(t2,2) + 
         4*Power(s,2)*s2*Power(t2,2) - 2*Power(s2,2)*Power(t2,2) + 
         4*s*Power(s2,2)*Power(t2,2) - 13*Power(t2,3) + s*Power(t2,3) - 
         12*Power(s,2)*Power(t2,3) - 4*Power(s,3)*Power(t2,3) + 
         12*s2*Power(t2,3) - 58*s*s2*Power(t2,3) + 
         27*Power(s,2)*s2*Power(t2,3) + 26*s*Power(s2,2)*Power(t2,3) - 
         Power(s2,3)*Power(t2,3) + 13*Power(t2,4) - 38*s*Power(t2,4) - 
         8*Power(s,3)*Power(t2,4) - 47*s2*Power(t2,4) - 
         7*s*s2*Power(t2,4) + 18*Power(s,2)*s2*Power(t2,4) + 
         21*Power(s2,2)*Power(t2,4) + 16*s*Power(s2,2)*Power(t2,4) - 
         2*Power(s2,3)*Power(t2,4) - Power(t2,5) - 22*s*Power(t2,5) - 
         6*Power(s,2)*Power(t2,5) - 2*Power(s,3)*Power(t2,5) - 
         33*s2*Power(t2,5) + 20*s*s2*Power(t2,5) + 
         Power(s,2)*s2*Power(t2,5) + 10*Power(s2,2)*Power(t2,5) + 
         Power(s2,3)*Power(t2,5) - 12*Power(t2,6) - s*Power(t2,6) - 
         5*Power(s,2)*Power(t2,6) + s2*Power(t2,6) + 2*s*s2*Power(t2,6) + 
         3*Power(s2,2)*Power(t2,6) - 4*s*Power(t2,7) + 4*s2*Power(t2,7) + 
         2*Power(s1,7)*Power(t1,2)*
          (-14*Power(t1,3) + Power(t1,4) + 
            s2*(-1 - 6*Power(t1,2) + Power(t1,3)) - 12*t2 - 
            t1*(15 + 8*s + 4*t2) + 2*Power(t1,2)*(17 + 4*s + 8*t2)) + 
         (-1 + s)*Power(t1,9)*
          (7 + (2 + 3*s2)*t2 + 4*Power(s,2)*(1 + t2) - 
            s*(11 + 3*(3 + s2)*t2)) + 
         Power(t1,8)*(18 + (15 + 19*s2)*t2 + 
            (3 + 5*s2 + 3*Power(s2,2))*Power(t2,2) - 
            Power(s,3)*(3 + 14*t2 + 6*Power(t2,2)) + 
            Power(s,2)*(21 + 8*(7 + 2*s2)*t2 + 
               (23 + 9*s2)*Power(t2,2)) - 
            s*(36 + 5*(12 + 7*s2)*t2 + 
               (22 + 17*s2 + 3*Power(s2,2))*Power(t2,2))) + 
         Power(t1,7)*(-16 - 2*(17 + 23*s2)*t2 - 
            (22 + 30*s2 + 17*Power(s2,2))*Power(t2,2) - 
            (5 + 6*s2 + 4*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            Power(s,3)*(-6 + 6*t2 + 17*Power(t2,2) + 4*Power(t2,3)) - 
            Power(s,2)*(-9 + (47 + 29*s2)*t2 + 
               (84 + 39*s2)*Power(t2,2) + 3*(7 + 3*s2)*Power(t2,3)) + 
            s*(12 + (86 + 75*s2)*t2 + 
               (108 + 86*s2 + 17*Power(s2,2))*Power(t2,2) + 
               (28 + 20*s2 + 6*Power(s2,2))*Power(t2,3))) + 
         Power(t1,6)*(-14 + (7 + 46*s2)*t2 + 
            (53 + 46*s2 + 39*Power(s2,2))*Power(t2,2) + 
            (39 + 34*s2 + 18*Power(s2,2) + 5*Power(s2,3))*Power(t2,3) + 
            (7 + 5*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            Power(s,3)*(9 + 28*t2 + 2*Power(t2,2) - 7*Power(t2,3) - 
               Power(t2,4)) + 
            Power(s,2)*(-45 + (-95 + 8*s2)*t2 + 
               (22 + 49*s2)*Power(t2,2) + (62 + 27*s2)*Power(t2,3) + 
               (10 + 3*s2)*Power(t2,4)) - 
            s*(-54 + (-60 + 53*s2)*t2 + 
               (99 + 133*s2 + 39*Power(s2,2))*Power(t2,2) + 
               (121 + 85*s2 + 25*Power(s2,2))*Power(t2,3) + 
               (20 + 13*s2 + 3*Power(s2,2))*Power(t2,4))) - 
         Power(t1,5)*(-42 - 4*(21 + s2)*t2 + 
            (45 - 39*s2 + 43*Power(s2,2))*Power(t2,2) + 
            (94 + 58*s2 + 17*Power(s2,2) + 11*Power(s2,3))*Power(t2,3) + 
            (58 + 25*s2 + 11*Power(s2,2) + 3*Power(s2,3))*Power(t2,4) + 
            (5 + 2*s2 + 2*Power(s2,2))*Power(t2,5) + 
            Power(s,3)*t2*(35 + 50*t2 + 11*Power(t2,2) + Power(t2,3)) + 
            Power(s,2)*(-27 - (181 + 37*s2)*t2 - 
               (225 + 31*s2)*Power(t2,2) + 3*(-7 + 3*s2)*Power(t2,3) + 
               (20 + s2)*Power(t2,4) + 2*Power(t2,5)) - 
            s*(-72 - (261 + 49*s2)*t2 + 
               (-196 - 41*s2 + 43*Power(s2,2))*Power(t2,2) + 
               (65 + 81*s2 + 36*Power(s2,2))*Power(t2,3) + 
               (75 + 41*s2 + 5*Power(s2,2))*Power(t2,4) + 
               (7 + 4*s2)*Power(t2,5))) + 
         Power(t1,4)*(-28 - (133 + 52*s2)*t2 + 
            5*(-2 - 37*s2 + 3*Power(s2,2))*Power(t2,2) + 
            (73 + 24*s2 - 32*Power(s2,2) + 13*Power(s2,3))*Power(t2,3) + 
            (114 + 46*s2 + 6*Power(s2,2) + 3*Power(s2,3))*Power(t2,4) + 
            (44 + 9*s2 + 5*Power(s2,2) - Power(s2,3))*Power(t2,5) + 
            (1 + s2)*Power(t2,6) + 
            Power(s,3)*(-5 + 3*t2 + 55*Power(t2,2) + 36*Power(t2,3) + 
               7*Power(t2,4) + Power(t2,5)) - 
            Power(s,2)*(-15 + (66 + 40*s2)*t2 + 
               (279 + 140*s2)*Power(t2,2) + 
               3*(72 + 23*s2)*Power(t2,3) + (26 + 15*s2)*Power(t2,4) + 
               3*s2*Power(t2,5)) + 
            s*(12 + (209 + 109*s2)*t2 + 
               (375 + 354*s2 - 13*Power(s2,2))*Power(t2,2) + 
               (240 + 138*s2 - Power(s2,2))*Power(t2,3) + 
               (-18 - 14*s2 + 5*Power(s2,2))*Power(t2,4) + 
               (-22 - 5*s2 + 3*Power(s2,2))*Power(t2,5) - Power(t2,6))) + 
         Power(t1,2)*(21 + (9 - 14*s2)*t2 - 
            (3 + 80*s2 + 23*Power(s2,2))*Power(t2,2) - 
            (107 - 10*s2 + 58*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            (22 - 128*s2 + 38*Power(s2,2) - 6*Power(s2,3))*Power(t2,4) + 
            (13 - 8*s2 + 5*Power(s2,2))*Power(t2,5) + 
            (29 - 4*s2 - 3*Power(s2,2))*Power(t2,6) - 
            Power(s,3)*t2*(5 + 7*t2 + 3*Power(t2,2) - 12*Power(t2,3) + 
               3*Power(t2,4)) + 
            Power(s,2)*(6 + (33 + 16*s2)*t2 + 
               (79 - 46*s2)*Power(t2,2) + (90 - 89*s2)*Power(t2,3) + 
               (14 - 40*s2)*Power(t2,4) + (2 + 6*s2)*Power(t2,5) - 
               3*Power(t2,6)) + 
            s*(-27 - 3*(22 + 5*s2)*t2 + 
               (-133 + 163*s2 + 35*Power(s2,2))*Power(t2,2) + 
               (100 + 93*s2 + 132*Power(s2,2))*Power(t2,3) + 
               (212 + 94*s2 + 34*Power(s2,2))*Power(t2,4) + 
               (130 - 7*s2 - 3*Power(s2,2))*Power(t2,5) + 
               (4 + 6*s2)*Power(t2,6))) + 
         2*Power(s1,6)*t1*(4*Power(t1,6) + 
            Power(s2,2)*(-1 + 2*t1 + 2*Power(t1,2) - 4*Power(t1,3) + 
               Power(t1,4)) + 12*Power(t2,2) - 
            Power(t1,5)*(30 + 2*s + 3*t2) + 
            t1*(1 + (45 + 23*s)*t2 + 36*Power(t2,2)) + 
            4*Power(t1,2)*(3 + Power(s,2) - 15*t2 - 6*Power(t2,2) + 
               s*(6 + t2)) + Power(t1,4)*(71 + 36*t2 + s*(25 + t2)) + 
            s2*(3*Power(t1,5) + 2*t2 - Power(t1,4)*(15 + s + 4*t2) + 
               t1*(4 + s + 7*t2) + Power(t1,2)*(-14 + 5*s + 14*t2) + 
               Power(t1,3)*(22 + 7*s + 23*t2)) - 
            Power(t1,3)*(58 + 4*Power(s,2) + 60*t2 + 24*Power(t2,2) + 
               s*(65 + 34*t2))) - 
         t1*(11 + (22 + s2)*t2 + 
            (13 + s2 - 11*Power(s2,2))*Power(t2,2) - 
            (64 - 30*s2 + 15*Power(s2,2) + 3*Power(s2,3))*Power(t2,3) + 
            (21 - 140*s2 + 54*Power(s2,2) - 6*Power(s2,3))*Power(t2,4) + 
            (-12 - 56*s2 + 17*Power(s2,2) + 2*Power(s2,3))*Power(t2,5) + 
            (5 - 4*s2 + Power(s2,2))*Power(t2,6) + 
            (12 - 8*s2)*Power(t2,7) + 
            Power(s,3)*Power(t2,2)*
             (-3 - 19*t2 - 8*Power(t2,2) + 7*Power(t2,3)) + 
            s*(-6 - (20 + 23*s2)*t2 + 
               (-84 - 7*s2 + 19*Power(s2,2))*Power(t2,2) + 
               (-8 - 103*s2 + 95*Power(s2,2))*Power(t2,3) + 
               (13 + 22*s2 + 38*Power(s2,2))*Power(t2,4) + 
               (97 + s2 - 5*Power(s2,2))*Power(t2,5) + 
               (40 - 18*s2)*Power(t2,6) + 8*Power(t2,7)) + 
            Power(s,2)*t2*(4 + 30*t2 + 63*Power(t2,2) + 70*Power(t2,3) + 
               18*Power(t2,4) + 17*Power(t2,5) + 
               s2*(6 + 4*t2 + 17*Power(t2,2) - 12*Power(t2,3) - 
                  4*Power(t2,4)))) + 
         Power(t1,3)*(-8 + (70 + 46*s2)*t2 + 
            (32 + 200*s2 + 17*Power(s2,2))*Power(t2,2) + 
            (43 + 14*s2 + 78*Power(s2,2) - 7*Power(s2,3))*Power(t2,3) + 
            (-77 + 9*s2 - 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            (-63 - 22*s2 - Power(s2,2) + 2*Power(s2,3))*Power(t2,5) + 
            (-13 - 2*s2 + Power(s2,2))*Power(t2,6) - 
            Power(s,3)*(-2 - 13*t2 + 14*Power(t2,2) + 34*Power(t2,3) + 
               5*Power(t2,4) + Power(t2,5)) + 
            Power(s,2)*(-21 + (-45 + s2)*t2 + 
               (45 + 136*s2)*Power(t2,2) + 139*(1 + s2)*Power(t2,3) + 
               23*(4 + s2)*Power(t2,4) + 4*(3 + s2)*Power(t2,5) + 
               Power(t2,6)) - 
            s*(-36 + 108*Power(t2,2) + 321*Power(t2,3) + 
               198*Power(t2,4) - 4*Power(t2,5) - 2*Power(t2,6) + 
               Power(s2,2)*Power(t2,2)*
                (25 + 79*t2 + 19*Power(t2,2) + 5*Power(t2,3)) + 
               s2*t2*(55 + 404*t2 + 292*Power(t2,2) + 79*Power(t2,3) + 
                  11*Power(t2,4) + 2*Power(t2,5)))) + 
         Power(s1,5)*(Power(s2,3)*Power(-1 + t1,3) + 12*Power(t1,8) - 
            8*Power(t2,3) - Power(t1,7)*(82 + 17*s + 17*t2) - 
            2*t1*t2*(2 + (45 + 22*s)*t2 + 44*Power(t2,2)) + 
            Power(t1,6)*(150 + 2*Power(s,2) + 97*t2 + 6*Power(t2,2) + 
               s*(89 + 22*t2)) + 
            Power(s2,2)*(-1 + t1)*
             (6*Power(t1,5) - 2*t2 + 3*Power(t1,3)*(1 + s + 7*t2) - 
               t1*(5 + s + 7*t2) - Power(t1,4)*(19 + 10*t2) + 
               Power(t1,2)*(15 + 2*s + 22*t2)) - 
            Power(t1,5)*(108 + 93*t2 + 54*Power(t2,2) + 
               2*Power(s,2)*(12 + t2) + 3*s*(85 + 48*t2 + 2*Power(t2,2))\
) - 2*Power(t1,2)*(4 + 43*t2 + 11*Power(s,2)*t2 + 24*Power(t2,2) + 
               24*Power(t2,3) + s*(-1 + 69*t2 + 54*Power(t2,2))) + 
            2*Power(t1,3)*(5 + Power(s,2)*(-7 + t2) + 171*t2 + 
               225*Power(t2,2) + 56*Power(t2,3) + 
               s*(-5 + 130*t2 + 66*Power(t2,2))) + 
            Power(t1,4)*(26 - 239*t2 - 12*Power(t2,2) + 
               32*Power(t2,3) + Power(s,2)*(48 + 46*t2) + 
               s*(191 + 216*t2 + 98*Power(t2,2))) + 
            s2*(6*Power(t1,7) - 2*Power(t2,2) - 
               Power(t1,6)*(23 + 5*s + 19*t2) - 
               4*t1*(-1 + 2*(2 + s)*t2 + 7*Power(t2,2)) + 
               Power(t1,5)*(73 + 80*t2 + 12*Power(t2,2) + 
                  4*s*(7 + 3*t2)) - 
               Power(t1,3)*(-57 + 10*Power(s,2) + 8*t2 + 
                  118*Power(t2,2) + 4*s*(6 + 11*t2)) - 
               Power(t1,2)*(12 - 31*t2 + 54*Power(t2,2) + 
                  s*(3 + 36*t2)) - 
               Power(t1,4)*(105 + 2*Power(s,2) + 68*t2 + 
                  62*Power(t2,2) + s*(-4 + 68*t2)))) + 
         Power(s1,4)*(8*Power(t1,9) - 
            Power(s2,3)*Power(-1 + t1,3)*
             (3 + 2*t1*(-2 + t2) + 2*Power(t1,2)*(-1 + t2) + 2*t2) - 
            Power(t1,8)*(57 + 27*s + 16*t2) + 
            2*Power(t2,2)*(1 + (15 + 7*s)*t2 + 16*Power(t2,2)) + 
            2*t1*(-1 + 8*t2 + (50 + 66*s + 9*Power(s,2))*Power(t2,2) + 
               (92 + 64*s)*Power(t2,3) + 56*Power(t2,4)) + 
            Power(t1,7)*(92 + 10*Power(s,2) + 77*t2 + 10*Power(t2,2) + 
               s*(108 + 53*t2)) - 
            Power(t1,6)*(19 - 88*t2 + 21*Power(t2,2) + 2*Power(t2,3) + 
               Power(s,2)*(36 + 19*t2) + 
               s*(180 + 160*t2 + 33*Power(t2,2))) + 
            Power(s2,2)*(-1 + t1)*
             (-3 + 6*Power(t1,6) + 5*t2 + 4*s*t2 + 9*Power(t2,2) - 
               3*Power(t1,5)*(8 + 7*t2) + 
               Power(t1,3)*(25 + 2*s + 33*t2 - 12*s*t2 - 
                  22*Power(t2,2)) - 
               t1*(-5 + 2*t2 + 4*s*t2 + 4*Power(t2,2)) + 
               Power(t1,4)*(11 + 5*s + 49*t2 + 4*s*t2 + 
                  18*Power(t2,2)) - 
               Power(t1,2)*(20 + 7*s + 64*t2 + 12*s*t2 + 61*Power(t2,2))\
) + Power(t1,4)*(24 + 457*t2 + 593*Power(t2,2) + 106*Power(t2,3) - 
               8*Power(t2,4) - 2*Power(s,3)*(-7 + 5*t2) - 
               2*Power(s,2)*(32 + 34*t2 + 35*Power(t2,2)) + 
               s*(-25 + 473*t2 + 86*Power(t2,2) - 52*Power(t2,3))) + 
            Power(t1,5)*(-47 + 2*Power(s,3) - 447*t2 - 
               191*Power(t2,2) + 4*Power(t2,3) + 
               2*Power(s,2)*(55 + 48*t2 + 4*Power(t2,2)) + 
               s*(130 + 211*t2 + 104*Power(t2,2) + 6*Power(t2,3))) + 
            Power(t1,2)*(8 + 23*t2 - 288*Power(t2,2) - 
               350*Power(t2,3) - 48*Power(t2,4) + 
               Power(s,2)*(-2 + 43*t2 + 34*Power(t2,2)) - 
               s*(8 - 17*t2 + 39*Power(t2,2) + 18*Power(t2,3))) - 
            Power(t1,3)*(7 + 198*t2 + 205*Power(t2,2) + 
               392*Power(t2,3) + 88*Power(t2,4) + 
               2*Power(s,3)*(2 + t2) + 
               2*Power(s,2)*(9 + 56*t2 + 55*Power(t2,2)) + 
               s*(-2 + 594*t2 + 790*Power(t2,2) + 258*Power(t2,3))) + 
            s2*(2*Power(t1,8) - 3*Power(t1,7)*(-1 + s + 5*t2) + 
               2*t2*(-2 + (4 + 3*s)*t2 + 7*Power(t2,2)) + 
               t1*(-10 + s - 2*t2 + 17*s*t2 + 2*Power(s,2)*t2 + 
                  22*Power(t2,2) + 64*s*Power(t2,2) + 72*Power(t2,3)) + 
               Power(t1,6)*(8 - Power(s,2) + 35*t2 + 21*Power(t2,2) + 
                  s*(5 + 33*t2)) - 
               Power(t1,5)*(69 + 82*t2 + 58*Power(t2,2) + 
                  8*Power(t2,3) + 2*Power(s,2)*(1 + t2) + 
                  2*s*(3 + 66*t2 + 13*Power(t2,2))) + 
               Power(t1,4)*(109 - 58*Power(t2,2) + 30*Power(t2,3) + 
                  Power(s,2)*(-40 + 22*t2) + 
                  5*s*(1 + 3*t2 + 20*Power(t2,2))) + 
               Power(t1,2)*(37 - 103*t2 - 89*Power(t2,2) + 
                  128*Power(t2,3) + Power(s,2)*(-1 + 26*t2) + 
                  s*(-6 + 56*t2 + 62*Power(t2,2))) + 
               Power(t1,3)*(-80 + 171*t2 + 154*Power(t2,2) + 
                  184*Power(t2,3) + 4*Power(s,2)*(11 + 3*t2) + 
                  s*(4 + 11*t2 + 154*Power(t2,2))))) + 
         Power(s1,3)*(2*Power(t1,10) - 
            2*t2*(-1 + (4 + s)*t2 + 
               (19 + 21*s + 2*Power(s,2))*Power(t2,2) + 
               (42 + 22*s)*Power(t2,3) + 24*Power(t2,4)) - 
            Power(t1,9)*(19*s + 5*(3 + t2)) + 
            Power(t1,8)*(27 + 18*Power(s,2) + 13*t2 + 4*Power(t2,2) + 
               s*(65 + 48*t2)) - 
            Power(s2,3)*Power(-1 + t1,3)*
             (-2 - 3*t2 - 6*Power(t1,2)*(-1 + t2)*t2 - Power(t2,2) + 
               Power(t1,3)*(-2 + 3*t2) + t1*(4 + 6*t2 - 7*Power(t2,2))) \
- Power(t1,7)*(1 + Power(s,3) - 138*t2 - 15*Power(t2,2) + Power(t2,3) + 
               Power(s,2)*(47 + 46*t2) + s*(-6 + 99*t2 + 43*Power(t2,2))\
) + Power(t1,6)*(-33 - 360*t2 - 293*Power(t2,2) - 19*Power(t2,3) + 
               3*Power(s,3)*(1 + t2) + 
               Power(s,2)*(93 + 104*t2 + 38*Power(t2,2)) + 
               s*(-149 - 158*t2 + 16*Power(t2,2) + 16*Power(t2,3))) + 
            Power(t1,5)*(20 + 224*t2 + 358*Power(t2,2) + 
               150*Power(t2,3) + 6*Power(t2,4) + 
               Power(s,3)*(17 - 20*t2 - 2*Power(t2,2)) - 
               Power(s,2)*(87 + 94*t2 + 85*Power(t2,2) + 
                  10*Power(t2,3)) + 
               s*(50 + 657*t2 + 251*Power(t2,2) + 18*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(t1,4)*(-3 + 15*t2 + 194*Power(t2,2) - 
               126*Power(t2,3) - 36*Power(t2,4) + 
               Power(s,3)*(-25 - 12*t2 + 17*Power(t2,2)) + 
               Power(s,2)*(-7 - 241*t2 - 139*Power(t2,2) + 
                  31*Power(t2,3)) + 
               s*(110 - 598*t2 - 991*Power(t2,2) - 250*Power(t2,3) + 
                  2*Power(t2,4))) + 
            t1*(5 - 3*t2 - 76*Power(t2,2) + Power(s,3)*Power(t2,2) + 
               14*Power(t2,3) - 30*Power(t2,4) - 48*Power(t2,5) + 
               Power(s,2)*t2*(5 - 38*t2 - 33*Power(t2,2)) - 
               s*(2 - 5*t2 + 26*Power(t2,2) + 182*Power(t2,3) + 
                  88*Power(t2,4))) + 
            Power(t1,3)*(15 + 6*t2 - 481*Power(t2,2) - 
               539*Power(t2,3) + 66*Power(t2,4) + 24*Power(t2,5) + 
               Power(s,3)*(6 - 28*t2 + 27*Power(t2,2)) + 
               Power(s,2)*(30 + 223*t2 + 307*Power(t2,2) + 
                  183*Power(t2,3)) + 
               s*(-63 + 69*t2 + 130*Power(t2,2) + 476*Power(t2,3) + 
                  166*Power(t2,4))) + 
            Power(t1,2)*(-17 - 30*t2 + 287*Power(t2,2) + 
               559*Power(t2,3) + 498*Power(t2,4) + 72*Power(t2,5) + 
               Power(s,3)*t2*(9 + 5*t2) + 
               Power(s,2)*t2*(49 + 37*t2 + 73*Power(t2,2)) + 
               s*(2 + 76*t2 + 665*Power(t2,2) + 684*Power(t2,3) + 
                  206*Power(t2,4))) + 
            Power(s2,2)*(-1 + t1)*
             (9 + 2*Power(t1,7) + (7 - 12*s)*t2 - 
               (13 + 7*s)*Power(t2,2) - 12*Power(t2,3) - 
               2*Power(t1,6)*(5 + 7*t2) - 
               Power(t1,4)*(8 + (-52 + 11*s)*t2 + 
                  7*(5 + 2*s)*Power(t2,2) + 14*Power(t2,3)) + 
               t1*(-27 + s + 25*t2 + 44*s*t2 + 60*Power(t2,2) + 
                  10*s*Power(t2,2) + 36*Power(t2,3)) + 
               Power(t1,5)*(16 + 34*t2 + 26*Power(t2,2) + 
                  s*(4 + 7*t2)) + 
               Power(t1,3)*(-11 - 111*t2 - 94*Power(t2,2) + 
                  s*(-11 - 27*t2 + 16*Power(t2,2))) + 
               Power(t1,2)*(29 + 7*t2 + 56*Power(t2,2) + 
                  70*Power(t2,3) + s*(6 - t2 + 35*Power(t2,2)))) + 
            s2*(-3 + Power(t1,8)*(11 + s - 4*t2) + 5*(2 + s)*t2 + 
               (14 - 14*s - 5*Power(s,2))*Power(t2,2) - 
               (25 + 38*s)*Power(t2,3) - 34*Power(t2,4) - 
               Power(t1,7)*(48 + 3*Power(s,2) + s*(23 - 37*t2) + 
                  10*t2 - 11*Power(t2,2)) + 
               Power(t1,6)*(49 + Power(s,2)*(10 - 6*t2) + 29*t2 + 
                  3*Power(t2,2) - 9*Power(t2,3) - 
                  2*s*(-4 + 51*t2 + 30*Power(t2,2))) + 
               Power(t1,2)*(-31 + 53*t2 - 23*Power(t2,2) + 
                  5*Power(t2,3) - 170*Power(t2,4) + 
                  Power(s,2)*(1 - 120*t2 - 19*Power(t2,2)) + 
                  s*(44 + 20*t2 + 42*Power(t2,2) - 96*Power(t2,3))) + 
               Power(t1,4)*(-154 + 192*t2 + 246*Power(t2,2) + 
                  141*Power(t2,3) + 2*Power(t2,4) + 
                  Power(s,2)*(89 + 18*t2 - 36*Power(t2,2)) + 
                  s*(-109 + 53*t2 + 136*Power(t2,2) - 42*Power(t2,3))) + 
               Power(t1,5)*(59 - 76*t2 - 39*Power(t2,2) - 
                  6*Power(t2,3) + 2*Power(t2,4) + 
                  Power(s,2)*(-53 + 35*t2 + 10*Power(t2,2)) + 
                  s*(96 + 73*t2 + 153*Power(t2,2) + 24*Power(t2,3))) + 
               t1*(7 - 8*t2 + Power(s,2)*(3 - 25*t2)*t2 + 
                  101*Power(t2,2) + 26*Power(t2,3) - 96*Power(t2,4) - 
                  s*(12 - 3*t2 + 109*Power(t2,2) + 92*Power(t2,3))) - 
               Power(t1,3)*(-110 + 186*t2 + 313*Power(t2,2) + 
                  132*Power(t2,3) + 124*Power(t2,4) + 
                  Power(s,2)*(44 - 70*t2 + 45*Power(t2,2)) + 
                  s*(5 + 89*t2 + 148*Power(t2,2) + 236*Power(t2,3))))) + 
         Power(s1,2)*(1 - 5*s*Power(t1,10) - 5*t2 - s*t2 - 
            5*Power(t2,2) + 3*s*Power(t2,2) + 43*Power(t2,3) + 
            19*s*Power(t2,3) + 9*Power(s,2)*Power(t2,3) - 
            2*Power(s,3)*Power(t2,3) + 48*Power(t2,4) + 
            91*s*Power(t2,4) + 5*Power(s,2)*Power(t2,4) + 
            78*Power(t2,5) + 42*s*Power(t2,5) + 32*Power(t2,6) + 
            Power(t1,9)*(2 + 14*Power(s,2) - 7*t2 + s*(7 + 15*t2)) + 
            Power(t1,8)*(-15 - 3*Power(s,3) + 51*t2 + 17*Power(t2,2) - 
               Power(s,2)*(38 + 43*t2) + s*(65 + 11*t2 - 16*Power(t2,2))\
) - Power(s2,3)*Power(-1 + t1,3)*t2*
             (1 + Power(t1,4) + Power(t1,3)*(4 - 7*t2) - 5*t2 + 
               Power(t2,2) + Power(t1,2)*(-4 - 6*t2 + 6*Power(t2,2)) + 
               t1*(-2 + 9*Power(t2,2))) + 
            Power(t1,7)*(31 - 85*t2 - 105*Power(t2,2) - 
               13*Power(t2,3) + 5*Power(s,3)*(1 + 2*t2) + 
               Power(s,2)*(11 + 87*t2 + 48*Power(t2,2)) + 
               s*(-149 - 282*t2 - 62*Power(t2,2) + 7*Power(t2,3))) + 
            Power(t1,6)*(-3 - 32*t2 - 8*Power(t2,2) + 62*Power(t2,3) + 
               3*Power(t2,4) + 
               Power(s,3)*(3 - 19*t2 - 11*Power(t2,2)) + 
               Power(s,2)*(36 + 33*t2 - 52*Power(t2,2) - 
                  23*Power(t2,3)) + 
               s*(13 + 376*t2 + 392*Power(t2,2) + 70*Power(t2,3) - 
                  Power(t2,4))) + 
            Power(t1,5)*(-66 + 184*t2 + 404*Power(t2,2) + 
               244*Power(t2,3) - 5*Power(t2,4) + 
               Power(s,3)*(-9 - 4*t2 + 20*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s*(185 + 117*t2 - 375*Power(t2,2) - 215*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s,2)*(-17 - 338*t2 - 137*Power(t2,2) - 
                  3*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,3)*(-45 - 37*t2 + 85*Power(t2,2) + 
               365*Power(t2,3) + 392*Power(t2,4) + 24*Power(t2,5) + 
               Power(s,3)*t2*(64 + 17*t2 - 40*Power(t2,2)) - 
               Power(s,2)*(20 - 15*t2 + 20*Power(t2,2) + 
                  64*Power(t2,3) + 91*Power(t2,4)) + 
               s*(25 - 81*t2 + 934*Power(t2,2) + 876*Power(t2,3) + 
                  60*Power(t2,4) - 30*Power(t2,5))) + 
            Power(t1,4)*(88 - 114*t2 - 402*Power(t2,2) - 
               639*Power(t2,3) - 140*Power(t2,4) - 6*Power(t2,5) + 
               Power(s,3)*(4 - 34*t2 + 41*Power(t2,2) - 
                  6*Power(t2,3)) + 
               Power(s,2)*(1 + 337*t2 + 448*Power(t2,2) + 
                  181*Power(t2,3) + 6*Power(t2,4)) + 
               s*(-137 - 267*t2 - 527*Power(t2,2) + 222*Power(t2,3) + 
                  67*Power(t2,4) + 4*Power(t2,5))) - 
            t1*(2 - t2 - 48*Power(t2,2) + 84*Power(t2,3) + 
               257*Power(t2,4) + 144*Power(t2,5) + 8*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(5 + 6*t2) + 
               Power(s,2)*t2*
                (16 + 43*t2 - 35*Power(t2,2) + 13*Power(t2,3)) + 
               s*(-12 - 3*t2 + 89*Power(t2,2) + 268*Power(t2,3) + 
                  112*Power(t2,4) + 40*Power(t2,5))) - 
            Power(t1,2)*(-9 - 44*t2 + 34*Power(t2,2) - 22*Power(t2,3) + 
               41*Power(t2,4) + 204*Power(t2,5) + 24*Power(t2,6) + 
               Power(s,3)*t2*(17 - 10*t2 + 22*Power(t2,2)) + 
               Power(s,2)*(-13 + 75*t2 + 244*Power(t2,2) + 
                  255*Power(t2,3) + 151*Power(t2,4)) + 
               s*(16 - 109*t2 + 260*Power(t2,2) + 711*Power(t2,3) + 
                  615*Power(t2,4) + 156*Power(t2,5))) - 
            Power(s2,2)*(-1 + t1)*
             (6 - 2*(-6 + 7*s)*t2 + (17 - 2*s)*Power(t2,2) - 
               (1 + 2*s)*Power(t2,3) - 2*Power(t2,4) + 
               Power(t1,7)*(-1 + 3*t2) + 
               Power(t1,2)*(34 + t2 - 59*s*t2 + 
                  (-136 + s)*Power(t2,2) + 22*(-1 + 2*s)*Power(t2,3) + 
                  34*Power(t2,4)) + 
               t1*(-25 + (-18 + 67*s)*t2 + 6*(13 + 8*s)*Power(t2,2) + 
                  (87 + 9*s)*Power(t2,3) + 44*Power(t2,4)) - 
               Power(t1,6)*(2 + 2*t2 + 11*Power(t2,2) + s*(2 + 4*t2)) + 
               Power(t1,5)*(17 - 31*t2 + 7*Power(t2,2) + 
                  13*Power(t2,3) + s*(6 - 7*t2 + 23*Power(t2,2))) - 
               Power(t1,3)*(7 + 22*t2 + 45*Power(t2,2) + 
                  80*Power(t2,3) + 12*Power(t2,4) + 
                  s*(-2 + 42*t2 + 43*Power(t2,2) - 5*Power(t2,3))) + 
               Power(t1,4)*(-22 + 57*t2 + 90*Power(t2,2) + 
                  3*Power(t2,3) - 4*Power(t2,4) - 
                  s*(6 - 59*t2 + 27*Power(t2,2) + 16*Power(t2,3)))) + 
            s2*(9 + (3 + s)*Power(t1,9) + (8 - 12*s)*t2 + 
               (-29 - 21*s + 13*Power(s,2))*Power(t2,2) + 
               (-55 + 77*s + 18*Power(s,2))*Power(t2,3) + 
               3*(9 + 20*s)*Power(t2,4) + 38*Power(t2,5) + 
               Power(t1,8)*(-18 - 3*Power(s,2) - 13*t2 + Power(t2,2) + 
                  3*s*(-5 + 7*t2)) + 
               Power(t1,7)*(31 + 48*t2 + 19*Power(t2,2) - 
                  2*Power(t2,3) - 2*Power(s,2)*(-9 + 5*t2) + 
                  s*(15 - 57*t2 - 50*Power(t2,2))) + 
               Power(t1,6)*(-2 - 11*t2 - 66*Power(t2,2) - 
                  19*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*(-37 + 19*t2 + 27*Power(t2,2)) + 
                  18*s*(4 + 3*t2 + 6*Power(t2,2) + 2*Power(t2,3))) + 
               Power(t1,5)*(-44 - 116*t2 + 156*Power(t2,2) + 
                  51*Power(t2,3) + 16*Power(t2,4) + 
                  Power(s,2)*
                   (33 + 12*t2 - 58*Power(t2,2) - 14*Power(t2,3)) - 
                  2*s*(78 - 54*t2 - 16*Power(t2,2) + 11*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,4)*(47 + 77*t2 - 150*Power(t2,2) - 
                  133*Power(t2,3) - 55*Power(t2,4) - 4*Power(t2,5) + 
                  2*Power(s,2)*
                   (-6 + 43*t2 - 27*Power(t2,2) + 9*Power(t2,3)) - 
                  s*(-73 + 369*t2 + 235*Power(t2,2) + 243*Power(t2,3) + 
                     14*Power(t2,4))) + 
               t1*(-33 + 28*t2 - 40*Power(t2,2) - 121*Power(t2,3) - 
                  50*Power(t2,4) + 76*Power(t2,5) + 
                  Power(s,2)*t2*(-16 + 69*t2 + 20*Power(t2,2)) + 
                  s*(17 + 12*t2 - 18*Power(t2,2) + 26*Power(t2,3) + 
                     42*Power(t2,4))) + 
               Power(t1,2)*(44 - 141*t2 + 212*Power(t2,2) + 
                  311*Power(t2,3) + 89*Power(t2,4) + 114*Power(t2,5) + 
                  Power(s,2)*t2*(159 + 10*t2 + 8*Power(t2,2)) + 
                  2*s*(-33 - 51*t2 + 26*Power(t2,2) + 9*Power(t2,3) + 
                     73*Power(t2,4))) + 
               Power(t1,3)*(-37 + 120*t2 - 103*Power(t2,2) - 
                  32*Power(t2,3) - 28*Power(t2,4) + 28*Power(t2,5) + 
                  Power(s,2)*
                   (1 - 250*t2 - 7*Power(t2,2) + 70*Power(t2,3)) + 
                  s*(59 + 345*t2 + 132*Power(t2,2) + 108*Power(t2,3) + 
                     134*Power(t2,4))))) + 
         s1*(-3 + (1 - 5*s + 4*Power(s,2))*Power(t1,10) - 3*t2 + 
            16*Power(t2,2) + 7*s*Power(t2,2) + Power(s,2)*Power(t2,2) - 
            11*Power(t2,3) + 11*s*Power(t2,3) + 
            3*Power(s,2)*Power(t2,3) + 6*Power(s,3)*Power(t2,3) - 
            31*Power(t2,4) + 6*s*Power(t2,4) - 8*Power(s,2)*Power(t2,4) + 
            5*Power(s,3)*Power(t2,4) - 48*s*Power(t2,5) + 
            4*Power(s,2)*Power(t2,5) - 24*Power(t2,6) - 8*s*Power(t2,6) - 
            8*Power(t2,7) - Power(t1,9)*
             (7 + 3*Power(s,3) + 3*t2 + 2*Power(s,2)*(4 + 7*t2) - 
               3*s*(7 + 8*t2)) + 
            Power(s2,3)*Power(-1 + t1,3)*Power(t2,2)*
             (-2 + 2*Power(t1,4) - 7*t2 - 5*Power(t1,3)*t2 + 
               2*Power(t2,2) + 2*Power(t1,2)*(-1 - t2 + Power(t2,2)) + 
               t1*(2 + 2*t2 + 5*Power(t2,2))) + 
            Power(t1,8)*(11 + 19*t2 + 10*Power(t2,2) + 
               Power(s,3)*(8 + 11*t2) + 
               Power(s,2)*(-25 + 16*t2 + 18*Power(t2,2)) - 
               s*(2 + 91*t2 + 43*Power(t2,2))) - 
            Power(t1,7)*(-17 + 33*t2 + 75*Power(t2,2) + 19*Power(t2,3) + 
               Power(s,3)*(3 + 25*t2 + 15*Power(t2,2)) + 
               s*(96 + 26*t2 - 126*Power(t2,2) - 36*Power(t2,3)) + 
               Power(s,2)*(-71 - 106*t2 - 5*Power(t2,2) + 10*Power(t2,3))\
) + Power(t1,6)*(Power(s,3)*(-5 + 5*t2 + 25*Power(t2,2) + 
                  9*Power(t2,3)) + 
               s*(141 + 381*t2 + 164*Power(t2,2) - 61*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s,2)*(-31 - 218*t2 - 174*Power(t2,2) - 
                  31*Power(t2,3) + 2*Power(t2,4)) + 
               2*(-35 + 3*t2 + 76*Power(t2,2) + 71*Power(t2,3) + 
                  7*Power(t2,4))) - 
            Power(t1,5)*(-77 - 13*t2 + 40*Power(t2,2) + 
               187*Power(t2,3) + 105*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*(2 - 12*t2 - 14*Power(t2,2) + 7*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s,2)*(-30 + 63*t2 + 230*Power(t2,2) + 
                  133*Power(t2,3) + 23*Power(t2,4)) + 
               s*(35 + 310*t2 + 634*Power(t2,2) + 248*Power(t2,3) + 
                  5*Power(t2,4) - 2*Power(t2,5))) - 
            Power(t1,4)*(28 - 48*t2 + 194*Power(t2,2) + 59*Power(t2,3) - 
               23*Power(t2,4) - 27*Power(t2,5) + 
               Power(s,3)*(-9 - 19*t2 + 25*Power(t2,2) + 
                  28*Power(t2,3) + Power(t2,4)) + 
               Power(s,2)*(3 - 98*t2 - 196*Power(t2,2) + 
                  72*Power(t2,3) + 47*Power(t2,4) + 5*Power(t2,5)) - 
               s*(-36 - 209*t2 + 341*Power(t2,2) + 623*Power(t2,3) + 
                  142*Power(t2,4) + 12*Power(t2,5))) + 
            Power(t1,3)*(-1 - 99*t2 + 201*Power(t2,2) + 
               175*Power(t2,3) + 262*Power(t2,4) - Power(t2,5) + 
               2*Power(t2,6) + 
               Power(s,3)*(-4 - 35*t2 + 26*Power(t2,2) - 
                  8*Power(t2,3) + 12*Power(t2,4)) + 
               Power(s,2)*(39 - 26*t2 - 412*Power(t2,2) - 
                  347*Power(t2,3) - 79*Power(t2,4) + 7*Power(t2,5)) - 
               s*(18 - 306*t2 - 147*Power(t2,2) + 162*Power(t2,3) + 
                  426*Power(t2,4) + 56*Power(t2,5) + 2*Power(t2,6))) + 
            Power(t1,2)*(-7 + 58*t2 - 24*Power(t2,2) - 32*Power(t2,3) - 
               274*Power(t2,4) - 175*Power(t2,5) + 14*Power(t2,6) + 
               Power(s,3)*t2*
                (13 - 26*t2 - 19*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s,2)*(-17 - 40*t2 + 31*Power(t2,2) + 
                  236*Power(t2,3) + 123*Power(t2,4) + 69*Power(t2,5)) + 
               s*(46 - 49*t2 - 49*Power(t2,2) - 453*Power(t2,3) - 
                  88*Power(t2,4) + 104*Power(t2,5) + 30*Power(t2,6))) + 
            t1*(10 - 6*t2 - 46*Power(t2,2) - 9*Power(t2,3) + 
               111*Power(t2,4) + 152*Power(t2,5) + 92*Power(t2,6) + 
               8*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(1 - t2 + 8*Power(t2,2)) + 
               Power(s,2)*t2*
                (15 + 105*t2 + 88*Power(t2,2) + 46*Power(t2,3) + 
                  45*Power(t2,4)) + 
               s*(-16 - 26*t2 - 59*Power(t2,2) + 254*Power(t2,3) + 
                  385*Power(t2,4) + 202*Power(t2,5) + 52*Power(t2,6))) + 
            Power(s2,2)*(-1 + t1)*t2*
             (2 + Power(t1,7)*(-4 + t2) + t2 + 34*Power(t2,2) + 
               17*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,6)*(10 + 10*t2 - 3*Power(t2,2)) + 
               Power(t1,5)*(4 - 49*t2 - 6*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(-34 + 38*t2 + 35*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,3)*(36 + 64*t2 + 43*Power(t2,2) - 
                  25*Power(t2,3) - 5*Power(t2,4)) + 
               Power(t1,2)*(-10 - 101*t2 - 118*Power(t2,2) - 
                  31*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(-4 + 36*t2 + 15*Power(t2,2) + 27*Power(t2,3) + 
                  19*Power(t2,4)) + 
               s*(-6 + Power(t1,7) + Power(t1,6)*(4 - 14*t2) + 10*t2 + 
                  26*Power(t2,2) + Power(t2,3) + 
                  Power(t1,5)*(-29 + 30*t2 + 19*Power(t2,2)) + 
                  7*Power(t1,2)*
                   (-4 - 6*t2 + 3*Power(t2,2) + 3*Power(t2,3)) - 
                  Power(t1,3)*
                   (13 - 13*t2 + 25*Power(t2,2) + 5*Power(t2,3)) - 
                  Power(t1,4)*
                   (-46 + 8*t2 + 23*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(25 + 11*t2 - 18*Power(t2,2) + 9*Power(t2,3)))) - 
            s2*(6 + (15 - 13*s)*t2 + 
               (21 - 46*s + 12*Power(s,2))*Power(t2,2) + 
               (-67 - 18*s + 41*Power(s,2))*Power(t2,3) + 
               (-78 + 83*s + 14*Power(s,2))*Power(t2,4) + 
               (11 + 30*s)*Power(t2,5) + 20*Power(t2,6) + 
               Power(t1,9)*(-2 + s + Power(s,2) + 2*t2 - 5*s*t2) + 
               Power(t1,8)*(8 - 3*t2 - 6*Power(t2,2) + 
                  Power(s,2)*(-8 + 9*t2) + s*(3 + 9*t2 + 14*Power(t2,2))) \
- Power(t1,7)*(2 + 41*t2 - 18*Power(t2,2) - 6*Power(t2,3) + 
                  Power(s,2)*(-17 + 26*t2 + 27*Power(t2,2)) + 
                  s*(29 - 20*t2 + 16*Power(t2,2) + 13*Power(t2,3))) + 
               Power(t1,6)*(Power(s,2)*
                   (-8 + 25*t2 + 71*Power(t2,2) + 23*Power(t2,3)) - 
                  2*(19 - 71*t2 - 8*Power(t2,2) + 5*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  s*(59 - 98*t2 - 95*Power(t2,2) - 15*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,5)*(74 - 145*t2 - 59*Power(t2,2) - 
                  28*Power(t2,3) + Power(t2,4) + 2*Power(t2,5) - 
                  Power(s,2)*
                   (13 + 27*t2 + 35*Power(t2,2) + 36*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  s*(-35 + 186*t2 + 241*Power(t2,2) + 155*Power(t2,3) + 
                     31*Power(t2,4))) - 
               Power(t1,4)*(38 + 12*t2 + 53*Power(t2,2) - 
                  197*Power(t2,3) - 17*Power(t2,4) - 5*Power(t2,5) + 
                  Power(s,2)*
                   (-16 - 73*t2 + 19*Power(t2,2) + 41*Power(t2,3) + 
                     Power(t2,4)) + 
                  s*(31 + 212*t2 + 20*Power(t2,2) + 176*Power(t2,3) + 
                     93*Power(t2,4) + 10*Power(t2,5))) + 
               Power(t1,3)*(-38 + 93*t2 + 188*Power(t2,2) - 
                  346*Power(t2,3) - 139*Power(t2,4) - 44*Power(t2,5) - 
                  2*Power(t2,6) + 
                  Power(s,2)*
                   (-5 - 106*t2 + 70*Power(t2,2) + 30*Power(t2,3) + 
                     31*Power(t2,4)) + 
                  s*(57 + 164*t2 - 444*Power(t2,2) - 87*Power(t2,3) - 
                     64*Power(t2,4) + 16*Power(t2,5))) + 
               t1*(-32 - 37*t2 - 19*Power(t2,2) + 88*Power(t2,3) + 
                  38*Power(t2,4) - 14*Power(t2,5) + 36*Power(t2,6) + 
                  Power(s,2)*t2*
                   (-17 + 52*t2 + 34*Power(t2,2) + Power(t2,3)) + 
                  s*(6 + 51*t2 - 45*Power(t2,2) - 63*Power(t2,3) - 
                     67*Power(t2,4) + 24*Power(t2,5))) + 
               Power(t1,2)*(62 - 14*t2 - 106*Power(t2,2) + 
                  160*Power(t2,3) + 165*Power(t2,4) + 40*Power(t2,5) + 
                  30*Power(t2,6) + 
                  Power(s,2)*t2*
                   (69 - 124*t2 - 51*Power(t2,2) + 21*Power(t2,3)) + 
                  s*(-31 - 102*t2 + 411*Power(t2,2) + 217*Power(t2,3) + 
                     106*Power(t2,4) + 84*Power(t2,5))))))*T4(t1))/
     (Power(-1 + t1,2)*(s - s2 + t1)*Power(-1 + s1 + t1 - t2,2)*
       Power(s1*t1 - t2,3)*(-1 + t2)*(s - s1 + t2)) - 
    (8*(2 + Power(-1 + s,3)*Power(t1,7) - 
         2*Power(s1,7)*Power(t1,2)*(s2 + t1) + 6*t2 + s*t2 + s2*t2 - 
         6*s*s2*t2 + 5*Power(t2,2) - 9*s*Power(t2,2) - 
         Power(s,2)*Power(t2,2) + 6*s2*Power(t2,2) - 15*s*s2*Power(t2,2) + 
         4*Power(s,2)*s2*Power(t2,2) - 2*Power(s2,2)*Power(t2,2) + 
         4*s*Power(s2,2)*Power(t2,2) - 17*Power(t2,3) - 19*s*Power(t2,3) + 
         24*Power(s,2)*Power(t2,3) + 10*s2*Power(t2,3) - 
         42*s*s2*Power(t2,3) - 13*Power(s,2)*s2*Power(t2,3) + 
         22*s*Power(s2,2)*Power(t2,3) - Power(s2,3)*Power(t2,3) - 
         Power(t2,4) + 4*s*Power(t2,4) + 8*Power(s,2)*Power(t2,4) - 
         41*s2*Power(t2,4) - 5*s*s2*Power(t2,4) - 
         2*Power(s,2)*s2*Power(t2,4) + 15*Power(s2,2)*Power(t2,4) + 
         4*s*Power(s2,2)*Power(t2,4) - 2*Power(s2,3)*Power(t2,4) - 
         Power(t2,5) + 22*s*Power(t2,5) - 2*Power(s,3)*Power(t2,5) - 
         7*s2*Power(t2,5) + 2*s*s2*Power(t2,5) + 
         5*Power(s,2)*s2*Power(t2,5) - 2*Power(s2,2)*Power(t2,5) - 
         4*s*Power(s2,2)*Power(t2,5) + Power(s2,3)*Power(t2,5) + 
         6*Power(t2,6) + s*Power(t2,6) - Power(s,2)*Power(t2,6) - 
         s2*Power(t2,6) + 2*s*s2*Power(t2,6) - Power(s2,2)*Power(t2,6) - 
         (-1 + s)*Power(t1,6)*
          (4 + (2 + 3*s2)*t2 + Power(s,2)*(1 + 4*t2) - 
            s*(5 + 3*(3 + s2)*t2)) + 
         Power(t1,4)*(-6 + t2 + 7*s2*t2 + 
            (13 + 15*s2 + 8*Power(s2,2))*Power(t2,2) + 
            (5 + 6*s2 + 4*Power(s2,2) + Power(s2,3))*Power(t2,3) + 
            Power(s,3)*(1 + 12*t2 + Power(t2,2) - 4*Power(t2,3)) + 
            Power(s,2)*(-12 - (43 + s2)*t2 + 3*(5 + 4*s2)*Power(t2,2) + 
               3*(7 + 3*s2)*Power(t2,3)) - 
            s*(-18 + (-28 + 6*s2)*t2 + 
               (42 + 35*s2 + 8*Power(s2,2))*Power(t2,2) + 
               (28 + 20*s2 + 6*Power(s2,2))*Power(t2,3))) + 
         Power(t1,3)*(9 + (25 + 8*s2)*t2 + 
            (-5 + 14*s2 - 6*Power(s2,2))*Power(t2,2) - 
            2*(12 + 8*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,3) - 
            (7 + 5*s2 + 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
            Power(s,3)*(2 - 2*t2 - 17*Power(t2,2) - 5*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,2)*(-3 + (30 + 13*s2)*t2 + 
               2*(46 + 7*s2)*Power(t2,2) + Power(t2,3) - 
               (10 + 3*s2)*Power(t2,4)) + 
            s*(-9 - 2*(34 + 11*s2)*t2 + 
               (-93 - 23*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (37 + 25*s2 + 7*Power(s2,2))*Power(t2,3) + 
               (20 + 13*s2 + 3*Power(s2,2))*Power(t2,4))) + 
         Power(t1,5)*(-3 - (9 + 10*s2)*t2 - 
            (3 + 5*s2 + 3*Power(s2,2))*Power(t2,2) + 
            Power(s,3)*(-3 + 2*t2 + 6*Power(t2,2)) - 
            Power(s,2)*(-6 + (17 + 7*s2)*t2 + (23 + 9*s2)*Power(t2,2)) + 
            s*t2*(27 + 22*t2 + 3*Power(s2,2)*t2 + 17*s2*(1 + t2))) + 
         2*Power(s1,6)*t1*(-(Power(s2,2)*(1 + t1)) + 
            t1*(1 - 4*Power(t1,2) + 3*t2 - s*t2 + t1*(6 + 2*s + 3*t2)) + 
            s2*(-3*Power(t1,2) + 2*t2 + t1*(4 + s + 4*t2))) + 
         Power(t1,2)*(Power(s,3)*t2*
             (-5 + 2*t2 + 8*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s,2)*(6 + (21 - 2*s2)*t2 - 17*(1 + 2*s2)*Power(t2,2) - 
               9*(9 + 2*s2)*Power(t2,3) - 2*(5 + 4*s2)*Power(t2,4) + 
               2*Power(t2,5)) + 
            s*(-9 + 65*Power(t2,2) + 124*Power(t2,3) - 15*Power(t2,4) - 
               7*Power(t2,5) + 
               Power(s2,2)*Power(t2,2)*(2 + 3*t2 + 4*Power(t2,2)) - 
               2*s2*t2*(-9 - 47*t2 - 27*Power(t2,2) + Power(t2,3) + 
                  2*Power(t2,4))) + 
            t2*(-21 - 12*t2 + 13*Power(t2,2) + 
               2*Power(s2,3)*Power(t2,2) + 29*Power(t2,3) + 
               5*Power(t2,4) + 
               Power(s2,2)*t2*
                (-2 - 13*t2 + 2*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-11 - 47*t2 - 8*Power(t2,2) + 10*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         t1*(-5 + 2*(-2 + s2)*t2 + 
            (2 + 17*s2 + 5*Power(s2,2))*Power(t2,2) + 
            (23 + 8*s2 + 15*Power(s2,2))*Power(t2,3) + 
            (2 + 5*s2 + 9*Power(s2,2))*Power(t2,4) + 
            (-17 - 3*s2 + Power(s2,2) + Power(s2,3))*Power(t2,5) - 
            (1 + s2)*Power(t2,6) - 
            Power(s,3)*Power(t2,2)*
             (-3 + t2 - 2*Power(t2,2) + Power(t2,3)) + 
            s*(6 + (23 + 5*s2)*t2 + 
               (57 - 38*s2 - 7*Power(s2,2))*Power(t2,2) - 
               (1 + 69*s2 + 17*Power(s2,2))*Power(t2,3) - 
               (79 + 31*s2 + 2*Power(s2,2))*Power(t2,4) + 
               (1 - 7*s2 - 3*Power(s2,2))*Power(t2,5) + Power(t2,6)) + 
            Power(s,2)*t2*(-4 - 33*t2 - 9*Power(t2,2) + 26*Power(t2,3) + 
               6*Power(t2,4) + 
               s2*(-6 + 8*t2 + 24*Power(t2,2) + 3*Power(t2,4)))) - 
         Power(s1,5)*(Power(s2,3) + 
            Power(s2,2)*(6*Power(t1,3) + Power(t1,2)*(5 - 10*t2) - 
               2*t2 - t1*(5 + s + 11*t2)) + 
            t1*(12*Power(t1,4) - Power(t1,3)*(28 + 17*s + 17*t2) + 
               2*t2*(2 + 3*t2 - 2*s*t2) + 
               2*Power(t1,2)*
                (-2 + 4*s + Power(s,2) + t2 + 11*s*t2 + 3*Power(t2,2)) - 
               2*t1*(-4 + s - 22*t2 - 3*s*t2 + Power(s,2)*t2 - 
                  9*Power(t2,2) + 3*s*Power(t2,2))) + 
            s2*(6*Power(t1,4) + 2*Power(t2,2) - 
               Power(t1,3)*(15 + 5*s + 19*t2) + 
               4*t1*(-1 + 2*(2 + s)*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(3*s*(1 + 4*t2) + t2*(11 + 12*t2)))) + 
         Power(s1,4)*(-8*Power(t1,6) + 
            Power(s2,3)*(3 + 2*t1*(-2 + t2) + 2*Power(t1,2)*(-1 + t2) + 
               2*t2) + Power(t1,5)*(27 + 27*s + 16*t2) + 
            2*Power(t2,2)*(1 + t2 - s*t2) - 
            2*t1*(1 - 8*t2 + (-26 + 3*Power(s,2))*Power(t2,2) + 
               (-9 + 6*s)*Power(t2,3)) + 
            Power(t1,4)*(9 - 10*Power(s,2) + t2 - 10*Power(t2,2) - 
               s*(9 + 53*t2)) + 
            Power(s2,2)*(3 - 6*Power(t1,4) - 5*t2 - 4*s*t2 - 
               9*Power(t2,2) + 3*Power(t1,3)*(2 + 7*t2) + 
               Power(t1,2)*(19 + s*(7 - 4*t2) + 17*t2 - 
                  18*Power(t2,2)) - 
               t1*(-1 + 4*(2 + s)*t2 + 26*Power(t2,2))) + 
            Power(t1,3)*(-5 - 111*t2 - 33*Power(t2,2) + 2*Power(t2,3) + 
               Power(s,2)*(-2 + 19*t2) + s*(-16 - 19*t2 + 33*Power(t2,2))\
) + Power(t1,2)*(2 + 17*t2 + 66*Power(t2,2) + 18*Power(t2,3) + 
               Power(s,2)*(-2 + 7*t2 - 8*Power(t2,2)) + 
               s*(-8 + 29*t2 + 45*Power(t2,2) - 6*Power(t2,3))) + 
            s2*(-2*Power(t1,5) + Power(t1,4)*(-5 + 3*s + 15*t2) + 
               2*t2*(-2 + (4 + 3*s)*t2 + 4*Power(t2,2)) + 
               t1*(-10 + s - 14*t2 + 17*s*t2 + 2*Power(s,2)*t2 + 
                  40*Power(t2,2) + 34*s*Power(t2,2) + 24*Power(t2,3)) + 
               Power(t1,3)*(Power(s,2) - s*(4 + 33*t2) - 
                  7*(3 + 2*t2 + 3*Power(t2,2))) + 
               Power(t1,2)*(7 - 43*t2 - 17*Power(t2,2) + 8*Power(t2,3) + 
                  Power(s,2)*(-1 + 2*t2) + s*(-3 - 7*t2 + 26*Power(t2,2)))\
)) + Power(s1,3)*(-2*Power(t1,7) + Power(t1,6)*(9 + 19*s + 5*t2) + 
            2*t2*(1 - (4 + s)*t2 + 
               (-10 + s + 2*Power(s,2))*Power(t2,2) + 
               3*(-1 + s)*Power(t2,3)) - 
            2*Power(t1,5)*(s + 9*Power(s,2) + 24*s*t2 + t2*(-5 + 2*t2)) + 
            Power(s2,3)*(-2 - 3*t2 - 6*Power(t1,2)*(-1 + t2)*t2 - 
               Power(t2,2) + Power(t1,3)*(-2 + 3*t2) + 
               t1*(4 + 6*t2 - 7*Power(t2,2))) + 
            Power(t1,4)*(-8 + Power(s,3) - 113*t2 - 39*Power(t2,2) + 
               Power(t2,3) + Power(s,2)*(-7 + 46*t2) + 
               s*(-75 - 53*t2 + 43*Power(t2,2))) + 
            Power(t1,3)*(-2 + Power(s,3)*(2 - 3*t2) - 2*t2 + 
               96*Power(t2,2) + 26*Power(t2,3) + 
               Power(s,2)*(-6 + 32*t2 - 38*Power(t2,2)) + 
               s*(-49 + 111*t2 + 111*Power(t2,2) - 16*Power(t2,3))) + 
            Power(t1,2)*(-2 + 3*t2 + 125*Power(t2,2) - 11*Power(t2,3) - 
               6*Power(t2,4) + Power(s,3)*t2*(-3 + 2*t2) + 
               Power(s,2)*t2*(-2 - 47*t2 + 10*Power(t2,2)) + 
               s*(-4 + 73*t2 - 61*Power(t2,2) - 70*Power(t2,3) + 
                  2*Power(t2,4))) + 
            t1*(5 + 3*t2 - 46*Power(t2,2) + Power(s,3)*Power(t2,2) - 
               94*Power(t2,3) - 18*Power(t2,4) + 
               Power(s,2)*t2*(5 - 2*t2 + 19*Power(t2,2)) + 
               s*(-2 + 5*t2 - 44*Power(t2,2) - 24*Power(t2,3) + 
                  12*Power(t2,4))) - 
            Power(s2,2)*(9 + 2*Power(t1,5) + (7 - 12*s)*t2 - 
               (13 + 7*s)*Power(t2,2) - 16*Power(t2,3) - 
               2*Power(t1,4)*(3 + 7*t2) + 
               Power(t1,3)*(2 + 7*s*t2 + 26*Power(t2,2)) + 
               t1*(-9 + s + 39*t2 + 20*s*t2 - 2*Power(t2,2) - 
                  16*s*Power(t2,2) - 32*Power(t2,3)) + 
               Power(t1,2)*(2 + 60*t2 + 29*Power(t2,2) - 
                  14*Power(t2,3) + s*(8 + 15*t2 - 14*Power(t2,2)))) + 
            s2*(-3 - Power(t1,5)*(11 + s - 4*t2) + 5*(2 + s)*t2 + 
               (14 - 14*s - 5*Power(s,2))*Power(t2,2) - 
               (23 + 22*s)*Power(t2,3) - 12*Power(t2,4) + 
               Power(t1,4)*(13 + 3*Power(s,2) + s*(18 - 37*t2) + 16*t2 - 
                  11*Power(t2,2)) + 
               Power(t1,3)*(31 + 7*t2 - 22*Power(t2,2) + 9*Power(t2,3) + 
                  Power(s,2)*(-1 + 6*t2) + s*(39 + t2 + 60*Power(t2,2))) \
+ Power(t1,2)*(-28 + 65*t2 + 76*Power(t2,2) + 37*Power(t2,3) - 
                  2*Power(t2,4) + 
                  Power(s,2)*(1 + 9*t2 - 10*Power(t2,2)) + 
                  s*(8 + 47*t2 + 57*Power(t2,2) - 24*Power(t2,3))) - 
               t1*(2 - 22*t2 - 53*Power(t2,2) + 23*Power(t2,3) + 
                  16*Power(t2,4) + Power(s,2)*t2*(-3 + 10*t2) + 
                  s*(12 - 18*t2 + 37*Power(t2,2) + 54*Power(t2,3))))) + 
         Power(s1,2)*(1 + 5*s*Power(t1,7) - 5*t2 - s*t2 - 5*Power(t2,2) + 
            3*s*Power(t2,2) + 25*Power(t2,3) + 23*s*Power(t2,3) - 
            3*Power(s,2)*Power(t2,3) - 2*Power(s,3)*Power(t2,3) + 
            38*Power(t2,4) + s*Power(t2,4) - 11*Power(s,2)*Power(t2,4) + 
            6*Power(t2,5) - 6*s*Power(t2,5) + 
            Power(t1,6)*(-2 - 14*Power(s,2) + s*(8 - 15*t2) + 7*t2) + 
            Power(t1,5)*(9 + 3*Power(s,3) - 30*t2 - 17*Power(t2,2) + 
               Power(s,2)*(-4 + 43*t2) + 8*s*(-7 - 7*t2 + 2*Power(t2,2))) \
+ Power(s2,3)*t2*(1 + Power(t1,4) + Power(t1,3)*(4 - 7*t2) - 5*t2 + 
               Power(t2,2) + Power(t1,2)*(-4 - 6*t2 + 6*Power(t2,2)) + 
               t1*(-2 + 9*Power(t2,2))) + 
            Power(t1,4)*(2 + Power(s,3)*(4 - 10*t2) - 8*t2 + 
               30*Power(t2,2) + 13*Power(t2,3) + 
               Power(s,2)*(19 + 42*t2 - 48*Power(t2,2)) + 
               s*(-38 + 141*t2 + 110*Power(t2,2) - 7*Power(t2,3))) + 
            Power(t1,3)*(-20 + 45*t2 + 11*Power(s,3)*(-1 + t2)*t2 + 
               155*Power(t2,2) + 13*Power(t2,3) - 3*Power(t2,4) + 
               Power(s,2)*(19 - 36*t2 - 92*Power(t2,2) + 
                  23*Power(t2,3)) + 
               s*(49 + 218*t2 - 86*Power(t2,2) - 91*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,2)*(9 + 5*t2 - 28*Power(t2,2) - 148*Power(t2,3) - 
               16*Power(t2,4) + 
               Power(s,3)*t2*(-5 + 13*t2 - 4*Power(t2,2)) + 
               Power(s,2)*(13 - 15*t2 + 11*Power(t2,2) + 
                  72*Power(t2,3) - 4*Power(t2,4)) + 
               s*(20 + 52*t2 - 209*Power(t2,2) - 31*Power(t2,3) + 
                  33*Power(t2,4))) + 
            t1*(1 - 14*t2 + 3*Power(t2,2) + 
               Power(s,3)*(7 - 6*t2)*Power(t2,2) - 29*Power(t2,3) + 
               53*Power(t2,4) + 6*Power(t2,5) + 
               Power(s,2)*t2*
                (-16 + 23*t2 + 20*Power(t2,2) - 18*Power(t2,3)) + 
               s*(12 - 62*Power(t2,2) + 113*Power(t2,3) + 
                  41*Power(t2,4) - 4*Power(t2,5))) + 
            Power(s2,2)*(6 - 2*(-6 + 7*s)*t2 + (17 - 2*s)*Power(t2,2) - 
               (13 + 6*s)*Power(t2,3) - 14*Power(t2,4) + 
               Power(t1,5)*(-1 + 3*t2) + 
               t1*(-13 + (6 + 39*s)*t2 + (94 + 8*s)*Power(t2,2) + 
                  (1 - 27*s)*Power(t2,3) - 20*Power(t2,4)) + 
               Power(t1,2)*(2 + t2 + 21*s*t2 + 
                  (53 + 19*s)*Power(t2,2) + (29 - 16*s)*Power(t2,3) - 
                  4*Power(t2,4)) - 
               Power(t1,4)*(4 - 4*t2 + 11*Power(t2,2) + s*(2 + 4*t2)) + 
               Power(t1,3)*(10 - 26*t2 - 15*Power(t2,2) + 
                  13*Power(t2,3) + s*(2 - 15*t2 + 23*Power(t2,2)))) - 
            s2*(-9 + (3 + s)*Power(t1,6) + 4*(-2 + 3*s)*t2 + 
               (29 + 21*s - 13*Power(s,2))*Power(t2,2) + 
               (25 - 39*s - 8*Power(s,2))*Power(t2,3) - 
               7*(3 + 4*s)*Power(t2,4) - 8*Power(t2,5) + 
               Power(t1,5)*(-9 - 3*Power(s,2) - 13*t2 + Power(t2,2) + 
                  3*s*(-4 + 7*t2)) + 
               Power(t1,4)*(-5 + Power(s,2)*(9 - 10*t2) + 9*t2 + 
                  22*Power(t2,2) - 2*Power(t2,3) + 
                  s*(-24 + 6*t2 - 50*Power(t2,2))) + 
               Power(t1,3)*(13 + 49*t2 - 21*Power(t2,2) - 
                  25*Power(t2,3) + Power(t2,4) + 
                  Power(s,2)*(-1 - 11*t2 + 27*Power(t2,2)) + 
                  s*(37 + 3*t2 - 42*Power(t2,2) + 36*Power(t2,3))) + 
               t1*(6 - 52*t2 + 103*Power(t2,2) + 78*Power(t2,3) + 
                  11*Power(t2,4) - 4*Power(t2,5) - 
                  4*Power(s,2)*t2*(-4 - 3*t2 + 6*Power(t2,2)) + 
                  s*(-17 + 24*t2 + 69*Power(t2,2) - 3*Power(t2,3) - 
                     38*Power(t2,4))) + 
               Power(t1,2)*(1 + 15*t2 + 52*Power(t2,2) + 24*Power(t2,3) + 
                  19*Power(t2,4) + 
                  Power(s,2)*t2*(9 + 23*t2 - 14*Power(t2,2)) + 
                  s*(15 + 90*t2 + 86*Power(t2,2) + 86*Power(t2,3) - 
                     8*Power(t2,4))))) - 
         s1*(3 + (1 - 5*s + 4*Power(s,2))*Power(t1,7) + 3*t2 - 
            16*Power(t2,2) - 7*s*Power(t2,2) - Power(s,2)*Power(t2,2) + 
            Power(t2,3) - 5*s*Power(t2,3) + 19*Power(s,2)*Power(t2,3) - 
            2*Power(s,3)*Power(t2,3) + 13*Power(t2,4) + 50*s*Power(t2,4) - 
            8*Power(s,2)*Power(t2,4) - 3*Power(s,3)*Power(t2,4) + 
            26*Power(t2,5) + 4*s*Power(t2,5) - 8*Power(s,2)*Power(t2,5) + 
            2*Power(t2,6) - 2*s*Power(t2,6) + 
            Power(t1,6)*(-4 - 3*Power(s,3) + Power(s,2)*(4 - 14*t2) - 
               3*t2 + 6*s*(1 + 4*t2)) + 
            Power(s2,3)*Power(t2,2)*
             (-2 + 2*Power(t1,4) - 7*t2 - 5*Power(t1,3)*t2 + 
               2*Power(t2,2) + 2*Power(t1,2)*(-1 - t2 + Power(t2,2)) + 
               t1*(2 + 2*t2 + 5*Power(t2,2))) + 
            Power(t1,5)*(Power(s,3)*(-1 + 11*t2) + 
               s*(31 - 19*t2 - 43*Power(t2,2)) + 
               2*(-2 + 5*t2 + 5*Power(t2,2)) + 
               Power(s,2)*(-25 - 26*t2 + 18*Power(t2,2))) + 
            Power(t1,4)*(18 + 6*t2 - 45*Power(t2,2) - 19*Power(t2,3) + 
               Power(s,3)*(3 + 8*t2 - 15*Power(t2,2)) + 
               Power(s,2)*(-12 + 70*t2 + 59*Power(t2,2) - 
                  10*Power(t2,3)) + 
               s*(-26 - 155*t2 - 3*Power(t2,2) + 36*Power(t2,3))) + 
            Power(t1,3)*(-8 - 9*t2 + 5*Power(t2,2) + 61*Power(t2,3) + 
               14*Power(t2,4) + 
               Power(s,3)*(4 - 4*t2 - 20*Power(t2,2) + 9*Power(t2,3)) + 
               s*(-24 - 3*t2 + 266*Power(t2,2) + 47*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s,2)*(12 + 56*t2 - 51*Power(t2,2) - 61*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(t1,2)*(5 + 22*t2 - 60*Power(t2,2) - 71*Power(t2,3) + 
               27*Power(t2,4) + 3*Power(t2,5) + 
               Power(s,3)*t2*(13 + t2 - 20*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(-17 + 5*t2 + 82*Power(t2,2) + 
                  20*Power(t2,3) - 29*Power(t2,4)) - 
               s*(2 + 127*t2 + 148*Power(t2,2) - 191*Power(t2,3) - 
                  47*Power(t2,4) + 2*Power(t2,5))) + 
            t1*(-1 + 15*t2 - 14*Power(t2,2) - 22*Power(t2,3) - 
               64*Power(t2,4) + 6*Power(t2,5) + 
               Power(s,3)*Power(t2,2)*(11 + 5*t2 - 7*Power(t2,2)) + 
               Power(s,2)*(-15*t2 + 47*Power(t2,3) + 34*Power(t2,4) - 
                  5*Power(t2,5)) + 
               s*(16 + 26*t2 - 22*Power(t2,2) - 103*Power(t2,3) + 
                  49*Power(t2,4) + 18*Power(t2,5))) + 
            Power(s2,2)*t2*(2 + Power(t1,5)*(-4 + t2) + t2 + 
               28*Power(t2,2) - 7*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,4)*(2 + 12*t2 - 3*Power(t2,2)) + 
               t1*t2*(38 + 65*t2 + Power(t2,2) - 5*Power(t2,3)) + 
               2*Power(t1,3)*(6 - 13*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               2*Power(t1,2)*
                (-6 - 13*t2 + 7*Power(t2,2) + 7*Power(t2,3)) + 
               s*(-6 + Power(t1,5) + Power(t1,4)*(6 - 14*t2) + 10*t2 + 
                  14*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t1,3)*(-18 + 2*t2 + 19*Power(t2,2)) + 
                  t1*(13 + 19*t2 - 14*Power(t2,2) - 17*Power(t2,3)) + 
                  Power(t1,2)*(4 + 10*t2 + 15*Power(t2,2) - 6*Power(t2,3))\
)) - s2*(-6 + (-15 + 13*s)*t2 + (-21 + 46*s - 12*Power(s,2))*Power(t2,2) + 
               (59 + 14*s - Power(s,2))*Power(t2,3) + 
               (22 - 27*s - 8*Power(s,2))*Power(t2,4) - 
               (5 + 14*s)*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,6)*(-2 + s + Power(s,2) + 2*t2 - 5*s*t2) + 
               Power(t1,5)*(2 + 3*t2 - 6*Power(t2,2) + 
                  Power(s,2)*(-5 + 9*t2) + 2*s*(3 - 3*t2 + 7*Power(t2,2))) \
+ Power(t1,4)*(10 - 38*t2 + 6*Power(t2,3) + 
                  Power(s,2)*(-1 + t2 - 27*Power(t2,2)) + 
                  s*(-14 + 17*t2 + 26*Power(t2,2) - 13*Power(t2,3))) + 
               Power(t1,3)*(-16 + 21*t2 + 34*Power(t2,2) + 
                  8*Power(t2,3) - 4*Power(t2,4) + 
                  s*t2*(-34 - 59*t2 - 54*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(s,2)*(5 + t2 - 10*Power(t2,2) + 23*Power(t2,3))) + 
               Power(t1,2)*(-2 + 35*t2 + 31*Power(t2,2) - 
                  40*Power(t2,3) - 11*Power(t2,4) + 2*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-18 + 16*t2 + 33*Power(t2,2) - 6*Power(t2,3)) + 
                  s*(13 + 27*t2 - 6*Power(t2,2) + 32*Power(t2,3) + 
                     43*Power(t2,4))) + 
               t1*(14 - 8*t2 - 38*Power(t2,2) + 91*Power(t2,3) + 
                  38*Power(t2,4) + 11*Power(t2,5) + 
                  Power(s,2)*t2*
                   (17 + 32*t2 - 11*Power(t2,2) - 19*Power(t2,3)) + 
                  s*(-6 - 12*t2 + 135*Power(t2,2) + 99*Power(t2,3) + 
                     24*Power(t2,4) - 10*Power(t2,5))))))*
       T5(1 - s1 - t1 + t2))/
     ((s - s2 + t1)*(-1 + s1 + t1 - t2)*Power(s1*t1 - t2,3)*(-1 + t2)*
       (s - s1 + t2)));
   return a;
};
