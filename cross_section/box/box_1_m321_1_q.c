#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m321_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(8*Power(s,7)*s2*(-1 + t1)*Power(t1,2)*
          (s2 + Power(s2,2) - 3*s2*t1 + t1*(-1 + 2*t1)) + 
         Power(s,6)*t1*(2*Power(-1 + t1,2)*Power(t1,2) - 
            8*Power(s2,4)*(1 - 5*t1 + 4*Power(t1,2)) + 
            Power(s2,3)*(-8 + 111*Power(t1,3) + 
               t1*(92 + 16*s1 - 25*t2) + 
               Power(t1,2)*(-183 - 16*s1 + 13*t2)) + 
            s2*t1*(10 + 30*Power(t1,4) + 
               Power(t1,2)*(93 + 32*s1 - 57*t2) - 6*t2 + 
               t1*(-40 - 8*s1 + 25*t2) + 
               Power(t1,3)*(-81 - 24*s1 + 26*t2)) + 
            Power(s2,2)*t1*(34 - 115*Power(t1,3) + 
               8*s1*(1 - 6*t1 + 5*Power(t1,2)) + 
               Power(t1,2)*(242 - 45*t2) - 19*t2 + t1*(-185 + 88*t2))) + 
         Power(s,5)*t1*(2*Power(-1 + t1,2)*Power(t1,2)*(-7 + 2*t1) + 
            24*Power(s2,5)*(1 - 3*t1 + 2*Power(t1,2)) + 
            Power(s2,4)*(42 - 194*Power(t1,3) + 
               s1*(16 - 81*t1 + 53*Power(t1,2)) + 
               Power(t1,2)*(322 - 47*t2) - 26*t2 + t1*(-194 + 109*t2)) - 
            Power(s2,2)*(-10 + 135*Power(t1,5) + 7*t2 + 2*Power(t2,2) + 
               Power(t1,4)*(-361 - 149*s1 + 171*t2) + 
               t1*(42 - 12*s1*(-5 + t2) - 133*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(391 + 16*Power(s1,2) + s1*(331 - 40*t2) - 
                  522*t2 + 36*Power(t2,2)) - 
               Power(t1,2)*(221 + 16*Power(s1,2) + s1*(302 - 100*t2) - 
                  561*t2 + 98*Power(t2,2))) + 
            Power(s2,3)*(16 + 8*Power(s1,2)*(-1 + t1)*t1 + 
               275*Power(t1,4) - 23*t2 - 2*Power(t2,2) + 
               2*Power(t1,3)*(-272 + 91*t2) + 
               t1*(-178 + 269*t2 - 30*Power(t2,2)) + 
               Power(t1,2)*(455 - 440*t2 + 8*Power(t2,2)) + 
               s1*(8 - 162*Power(t1,3) + Power(t1,2)*(281 - 14*t2) + 
                  t1*(-139 + 38*t2))) + 
            s2*t1*(-50 + 18*Power(t1,5) + 61*t2 - 10*Power(t2,2) + 
               Power(t1,4)*(-85 - 40*s1 + 42*t2) + 
               Power(t1,3)*(42 + 8*Power(s1,2) + s1*(131 - 26*t2) - 
                  143*t2 + 16*Power(t2,2)) + 
               t1*(42 + s1*(52 - 12*t2) - 158*t2 + 26*Power(t2,2)) + 
               Power(t1,2)*(9 - 8*Power(s1,2) + 258*t2 - 56*Power(t2,2) + 
                  s1*(-179 + 62*t2)))) + 
         Power(-1 + s2,2)*s2*(s2 - t1)*(-1 + t1)*t1*
          (-3*(-1 + s1)*Power(t1,5)*(1 + s1 - 2*t2) + 
            4*Power(s2,4)*(-1 + t1)*Power(s1 - t2,2) + 
            5*Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-41 + 11*s1 + 10*t2) + 
            Power(t1,2)*(-64 + 4*Power(s1,3) + 94*t2 - 83*Power(t2,2) + 
               21*Power(t2,3) + 2*Power(s1,2)*(-27 + 7*t2) + 
               s1*(63 + 15*t2 - 10*Power(t2,2))) + 
            Power(t1,3)*(28 - 12*Power(s1,3) + 22*t2 + Power(t2,2) - 
               6*Power(t2,3) + Power(s1,2)*(63 + 6*t2) - 
               3*s1*(31 + 2*t2 + Power(t2,2))) - 
            Power(t1,4)*(3 + 2*Power(s1,3) + Power(s1,2)*(6 - 10*t2) + 
               33*t2 - 36*Power(t2,2) + s1*(-41 + 37*t2 + 6*Power(t2,2))) \
+ Power(s2,3)*(-4 + Power(s1,3)*(4 + 6*t1) + 
               Power(s1,2)*(3 - 5*Power(t1,2) + t1*(2 - 10*t2) - 
                  20*t2) + 17*t2 - 10*Power(t2,2) - 12*Power(t2,3) + 
               Power(t1,2)*(-4 + 17*t2 - 18*Power(t2,2)) + 
               2*t1*(4 - 17*t2 + 14*Power(t2,2) + Power(t2,3)) + 
               s1*(-9 + 3*t2 + 28*Power(t2,2) + 
                  Power(t1,2)*(-9 + 19*t2) + 
                  2*t1*(9 - 11*t2 + Power(t2,2)))) + 
            s2*(2*Power(s1,3)*t1*(-4 + 14*t1 + 5*Power(t1,2)) - 
               31*Power(-1 + t2,2) - 
               3*Power(t1,4)*(2 - 7*t2 + 2*Power(t2,2)) + 
               t1*(63 - 44*t2 + 73*Power(t2,2) - 28*Power(t2,3)) + 
               Power(t1,3)*(13 + 38*t2 - 61*Power(t2,2) + 
                  6*Power(t2,3)) - 
               Power(t1,2)*(39 + 77*t2 - 25*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s1,2)*t1*
                (108 + 9*Power(t1,3) + Power(t1,2)*(6 - 30*t2) - 
                  28*t2 - t1*(123 + 32*t2)) + 
               s1*(11*Power(-1 + t2,2) - 9*Power(t1,4)*(1 + t2) + 
                  Power(t1,3)*(-67 + 74*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(172 + 25*t2 + 29*Power(t2,2)) + 
                  t1*(-107 - 68*t2 + 39*Power(t2,2)))) + 
            Power(s2,2)*(1 - 2*Power(s1,3)*(-2 + 10*t1 + 7*Power(t1,2)) - 
               50*t2 + 10*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,3)*(7 - 32*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-13 + 14*t2 + 16*Power(t2,2) - 
                  13*Power(t2,3)) + 
               t1*(5 + 68*t2 - 46*Power(t2,2) + 36*Power(t2,3)) + 
               Power(s1,2)*(-54 - 5*Power(t1,3) + 14*t2 + 
                  Power(t1,2)*(2 + 30*t2) + t1*(57 + 46*t2)) - 
               s1*(-44 - 53*t2 + 29*Power(t2,2) + 
                  2*Power(t1,3)*(-9 + 4*t2) + 
                  Power(t1,2)*(-8 + 23*t2 + 7*Power(t2,2)) + 
                  t1*(70 + 22*t2 + 54*Power(t2,2))))) - 
         Power(s,4)*(-2*Power(-1 + t1,2)*Power(t1,3)*
             (18 - 11*t1 + Power(t1,2)) + 
            8*Power(s2,6)*t1*(3 - 7*t1 + 4*Power(t1,2)) + 
            Power(s2,5)*(2 - 156*Power(t1,4) + t1*(7 + 50*s1 - 75*t2) + 
               Power(t1,3)*(216 + 58*s1 - 58*t2) - 
               3*Power(t1,2)*(27 + 44*s1 - 56*t2) + t2) + 
            Power(s2,4)*(293*Power(t1,5) + 2*Power(1 + t2,2) + 
               Power(t1,4)*(-408 - 227*s1 + 256*t2) + 
               Power(t1,2)*(40 - 305*s1 - 46*Power(s1,2) + 570*t2 + 
                  168*s1*t2 - 124*Power(t2,2)) + 
               t1*(-12 + 67*s1 + 8*Power(s1,2) - 157*t2 - 36*s1*t2 + 
                  22*Power(t2,2)) + 
               Power(t1,3)*(133 + 417*s1 + 14*Power(s1,2) - 673*t2 - 
                  36*s1*t2 + 28*Power(t2,2))) + 
            Power(s2,3)*(-218*Power(t1,6) + 
               Power(t1,5)*(369 + 293*s1 - 345*t2) + 
               t2*Power(1 + t2,2) - 
               Power(t1,4)*(77 + 37*Power(s1,2) + s1*(601 - 129*t2) - 
                  1035*t2 + 136*Power(t2,2)) + 
               Power(t1,2)*(133 + 766*t2 - 241*Power(t2,2) + 
                  12*Power(t2,3) + Power(s1,2)*(-50 + 13*t2) + 
                  s1*(-377 + 271*t2 - 26*Power(t2,2))) - 
               Power(t1,3)*(253 + Power(s1,2)*(-123 + t2) + 1378*t2 - 
                  458*Power(t2,2) + 2*Power(t2,3) + 
                  s1*(-738 + 508*t2 - 4*Power(t2,2))) + 
               t1*(-26 - 103*t2 + Power(t2,2) + Power(t2,3) + 
                  s1*(43 - 12*t2 - 2*Power(t2,2)))) - 
            s2*Power(t1,2)*(89 + 5*Power(t1,6) - 180*t2 + 
               50*Power(t2,2) - 5*Power(t2,3) + 
               Power(t1,5)*(-40 - 21*s1 + 19*t2) + 
               Power(t1,4)*(-8 + 9*Power(s1,2) + s1*(139 - 33*t2) - 
                  138*t2 + 19*Power(t2,2)) + 
               t1*(139 + 334*t2 - 127*Power(t2,2) + 10*Power(t2,3) + 
                  s1*(-143 + 76*t2 - 2*Power(t2,2))) + 
               Power(t1,3)*(436 + Power(s1,2)*(-31 + t2) + 198*t2 - 
                  106*Power(t2,2) + 5*Power(t2,3) - 
                  2*s1*(153 - 94*t2 + 4*Power(t2,2))) + 
               Power(t1,2)*(-609 + Power(s1,2)*(34 - 13*t2) - 317*t2 + 
                  224*Power(t2,2) - 22*Power(t2,3) + 
                  s1*(403 - 303*t2 + 34*Power(t2,2)))) + 
            Power(s2,2)*t1*(41 + 60*Power(t1,6) - 61*t2 + 
               Power(t1,5)*(-179 - 145*s1 + 160*t2) + 
               Power(t1,4)*(-23 + 32*Power(s1,2) - 590*t2 + 
                  97*Power(t2,2) - 7*s1*(-65 + 18*t2)) + 
               t1*(129 + 385*t2 - 112*Power(t2,2) + 3*Power(t2,3) + 
                  2*s1*(-93 + 44*t2)) + 
               Power(t1,3)*(574 + 2*Power(s1,2)*(-54 + t2) + 1135*t2 - 
                  458*Power(t2,2) + 13*Power(t2,3) - 
                  3*s1*(263 - 176*t2 + 4*Power(t2,2))) + 
               Power(t1,2)*(-554 + Power(s1,2)*(76 - 26*t2) - 1125*t2 + 
                  521*Power(t2,2) - 40*Power(t2,3) + 
                  s1*(713 - 538*t2 + 60*Power(t2,2))))) + 
         Power(s,3)*(8*Power(s2,7)*Power(-1 + t1,2)*t1 - 
            2*Power(-1 + t1,2)*Power(t1,3)*(21 - 20*t1 + 4*Power(t1,2)) + 
            Power(s2,6)*(-54*Power(t1,4) + 
               s1*(-1 + 49*t1 - 76*Power(t1,2) + 16*Power(t1,3)) + 
               Power(t1,3)*(36 - 22*t2) + 3*(2 + t2) - 
               3*t1*(18 + 23*t2) + 2*Power(t1,2)*(33 + 50*t2)) + 
            Power(s2,5)*(11 + 141*Power(t1,5) + (15 - 2*s1)*t2 + 
               6*Power(t2,2) + Power(t1,4)*(-57 - 115*s1 + 126*t2) + 
               t1*(-122 + 31*s1 + 30*Power(s1,2) - 124*t2 - 110*s1*t2 + 
                  74*Power(t2,2)) - 
               Power(t1,3)*(283 + 10*Power(s1,2) + 327*t2 - 
                  32*Power(t2,2) + 4*s1*(-43 + 4*t2)) + 
               Power(t1,2)*(310 - 68*Power(s1,2) + 298*t2 - 
                  184*Power(t2,2) + 4*s1*(-19 + 62*t2))) + 
            Power(s2,4)*(6 - 151*Power(t1,6) - 
               Power(s1,3)*Power(t1,2)*(5 + 7*t1) + 
               Power(t1,5)*(50 - 265*t2) + 15*t2 + 10*Power(t2,2) + 
               3*Power(t2,3) + 
               Power(t1,4)*(488 + 614*t2 - 170*Power(t2,2)) - 
               2*Power(t1,3)*
                (426 + 362*t2 - 347*Power(t2,2) + 4*Power(t2,3)) - 
               t1*(138 + 211*t2 - 90*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,2)*(597 + 499*t2 - 504*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(s1,2)*t1*
                (26 + 13*Power(t1,3) - 10*t2 + 
                  7*Power(t1,2)*(20 + t2) + t1*(-155 + 63*t2)) + 
               s1*(1 + 234*Power(t1,5) - Power(t2,2) + 
                  Power(t1,4)*(-241 + 108*t2) + 
                  Power(t1,3)*(129 - 737*t2 + 10*Power(t2,2)) + 
                  t1*(57 - 121*t2 + 17*Power(t2,2)) - 
                  2*Power(t1,2)*(54 - 303*t2 + 55*Power(t2,2)))) + 
            s2*Power(t1,2)*(-63 + 
               Power(t1,5)*(30 + Power(s1,2) + s1*(66 - 7*t2) - 68*t2) + 
               213*t2 - 75*Power(t2,2) + 10*Power(t2,3) + 
               Power(t1,6)*(-8 - 8*s1 + 6*t2) + 
               Power(t1,4)*(240 + Power(s1,3) + 121*t2 - 
                  76*Power(t2,2) + 6*Power(t2,3) - 
                  Power(s1,2)*(11 + 6*t2) + 
                  s1*(-213 + 152*t2 - 5*Power(t2,2))) + 
               t1*(-412 - 211*t2 + 168*Power(t2,2) - 25*Power(t2,3) + 
                  s1*(205 - 166*t2 + 17*Power(t2,2))) + 
               Power(t1,3)*(-1070 + 13*Power(s1,3) + 
                  Power(s1,2)*(3 - 25*t2) + 160*t2 + 204*Power(t2,2) - 
                  32*Power(t2,3) + s1*(428 - 463*t2 + 64*Power(t2,2))) - 
               Power(t1,2)*(-1283 + 2*Power(s1,3) + 
                  Power(s1,2)*(17 - 19*t2) + 185*t2 + 269*Power(t2,2) - 
                  53*Power(t2,3) + s1*(514 - 556*t2 + 88*Power(t2,2)))) + 
            Power(s2,2)*t1*(56 - 10*Power(t1,7) + 
               Power(t1,6)*(15 + 68*s1 - 59*t2) - 134*t2 + 
               28*Power(t2,2) - 5*Power(t2,3) + 
               Power(t1,5)*(29 - 9*Power(s1,2) + 263*t2 - 
                  45*Power(t2,2) + s1*(-229 + 78*t2)) + 
               Power(t1,3)*(1967 - 31*Power(s1,3) + 179*t2 - 
                  911*Power(t2,2) + 157*Power(t2,3) + 
                  Power(s1,2)*(-101 + 113*t2) + 
                  s1*(-801 + 1304*t2 - 238*Power(t2,2))) + 
               t1*(528 + 287*t2 - 228*Power(t2,2) + 38*Power(t2,3) + 
                  s1*(-301 + 224*t2 - 19*Power(t2,2))) + 
               Power(t1,4)*(-739 - 9*Power(s1,3) - 344*t2 + 
                  387*Power(t2,2) - 34*Power(t2,3) + 
                  Power(s1,2)*(26 + 19*t2) + 
                  s1*(510 - 577*t2 + 28*Power(t2,2))) + 
               Power(t1,2)*(-1846 + 4*Power(s1,3) + 
                  Power(s1,2)*(60 - 48*t2) - 324*t2 + 841*Power(t2,2) - 
                  144*Power(t2,3) + s1*(885 - 1077*t2 + 169*Power(t2,2)))\
) + Power(s2,3)*(1 + 66*Power(t1,7) + 5*t2 + 5*Power(t2,2) + 
               Power(t2,3) + Power(t1,6)*(-14 - 195*s1 + 208*t2) + 
               Power(t1,5)*(-398 + 5*Power(s1,2) + s1*(308 - 163*t2) - 
                  498*t2 + 159*Power(t2,2)) + 
               Power(t1,2)*(790 - 2*Power(s1,3) + 687*t2 - 
                  596*Power(t2,2) + 76*Power(t2,3) + 
                  Power(s1,2)*(-69 + 39*t2) + 
                  s1*(-428 + 644*t2 - 98*Power(t2,2))) + 
               Power(t1,4)*(1175 + 15*Power(s1,3) + 658*t2 - 
                  851*Power(t2,2) + 54*Power(t2,3) - 
                  Power(s1,2)*(87 + 20*t2) + 
                  s1*(-399 + 914*t2 - 33*Power(t2,2))) + 
               t1*(-174 - 163*t2 + 38*Power(t2,2) - 4*Power(t2,3) + 
                  s1*(95 - 58*t2 + 3*Power(t2,2))) + 
               Power(t1,3)*(-1446 + 23*Power(s1,3) + 
                  Power(s1,2)*(223 - 151*t2) - 729*t2 + 
                  1173*Power(t2,2) - 187*Power(t2,3) + 
                  s1*(451 - 1337*t2 + 284*Power(t2,2))))) + 
         Power(s,2)*(2*Power(-1 + t1,2)*Power(t1,3)*
             (11 - 14*t1 + 4*Power(t1,2)) + 
            Power(s2,7)*(-1 + t1)*
             (5*Power(t1,3) + 2*s1*(-1 + 5*t1 + 5*Power(t1,2)) + 
               Power(t1,2)*(16 - 7*t2) + 3*(2 + t2) - t1*(27 + 14*t2)) + 
            Power(s2,6)*(-24*Power(t1,5) + 
               2*Power(s1,2)*t1*(-16 + 11*t1 + 17*Power(t1,2)) + 
               Power(t1,4)*(-40 + 13*t2) + 
               t1*(38 - 15*t2 - 70*Power(t2,2)) + 
               Power(t1,3)*(174 - 45*t2 - 8*Power(t2,2)) - 
               2*(4 + 7*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-140 + 61*t2 + 108*Power(t2,2)) + 
               s1*(2 - 3*Power(t1,4) + Power(t1,3)*(47 - 28*t2) + 4*t2 + 
                  t1*(61 + 104*t2) - Power(t1,2)*(107 + 128*t2))) + 
            s2*Power(t1,2)*(4 + 
               Power(t1,6)*(-2 + 3*Power(s1,2) + s1*(16 - 6*t2) - 
                  6*t2) - 85*t2 + 25*Power(t2,2) + 
               Power(t1,3)*(955 - 42*Power(s1,3) - 544*t2 - 
                  70*Power(t2,2) + 22*Power(t2,3) + 
                  Power(s1,2)*(158 + 39*t2) + 
                  s1*(-436 + 445*t2 - 97*Power(t2,2))) + 
               t1*(419 - 180*t2 + 17*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(-154 + 174*t2 - 39*Power(t2,2))) + 
               Power(t1,5)*(-6 + 2*Power(s1,3) + 106*t2 - 
                  36*Power(t2,2) - Power(s1,2)*(1 + 10*t2) + 
                  s1*(-106 + 57*t2 + 6*Power(t2,2))) + 
               Power(t1,4)*(-251 + 8*Power(s1,3) + 
                  16*Power(s1,2)*(-4 + t2) - 51*t2 + 130*Power(t2,2) - 
                  6*Power(t2,3) + s1*(269 - 236*t2 + 7*Power(t2,2))) + 
               Power(t1,2)*(-1119 + 8*Power(s1,3) + 760*t2 - 
                  54*Power(t2,2) - 9*Power(t2,3) + 
                  3*Power(s1,2)*(-28 + 5*t2) + 
                  s1*(411 - 458*t2 + 75*Power(t2,2)))) + 
            Power(s2,5)*(-8 + 38*Power(t1,6) + 
               4*Power(s1,3)*t1*(-1 + 2*t1 + 5*Power(t1,2)) - 14*t2 - 
               6*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,5)*(58 + 49*t2) + 
               Power(t1,4)*(-244 + 3*t2 + 54*Power(t2,2)) + 
               Power(t1,2)*(-120 + 163*t2 + 259*Power(t2,2) - 
                  78*Power(t2,3)) + 
               Power(t1,3)*(211 - 150*t2 - 360*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(65 - 51*t2 - 31*Power(t2,2) + 33*Power(t2,3)) - 
               Power(s1,2)*t1*
                (27 + 82*Power(t1,3) - 40*t2 + 
                  Power(t1,2)*(16 + 27*t2) + t1*(-41 + 97*t2)) + 
               s1*(-65*Power(t1,5) + Power(t1,4)*(-84 + 38*t2) + 
                  t1*(100 + 63*t2 - 68*Power(t2,2)) + 
                  Power(t1,3)*(377 + 357*t2 - 6*Power(t2,2)) + 
                  2*(-1 + 2*t2 + Power(t2,2)) + 
                  2*Power(t1,2)*(-163 - 147*t2 + 84*Power(t2,2)))) + 
            Power(s2,4)*(-24*Power(t1,7) - 
               2*Power(s1,3)*t1*
                (1 - 18*t1 + 15*Power(t1,2) + 26*Power(t1,3)) - 
               Power(t1,6)*(71 + 104*t2) + 
               Power(t1,5)*(219 + 57*t2 - 53*Power(t2,2)) + 
               Power(t1,2)*(-399 + 370*t2 + 344*Power(t2,2) - 
                  164*Power(t2,3)) + 
               Power(t1,4)*(-376 + 422*t2 + 490*Power(t2,2) - 
                  84*Power(t2,3)) + 
               2*(-4 - 9*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               t1*(88 + 58*t2 - 78*Power(t2,2) + 29*Power(t2,3)) + 
               Power(t1,3)*(571 - 785*t2 - 603*Power(t2,2) + 
                  313*Power(t2,3)) + 
               2*Power(s1,2)*t1*
                (28*Power(t1,4) + 43*Power(t1,3)*(-1 + t2) + 
                  3*(2 + t2) - t1*(31 + 63*t2) + 
                  Power(t1,2)*(88 + 113*t2)) + 
               s1*(115*Power(t1,6) + Power(t1,5)*(6 + 25*t2) + 
                  Power(t1,3)*(229 + 562*t2 - 441*Power(t2,2)) + 
                  t1*(5 + 77*t2 - 31*Power(t2,2)) + 
                  2*(-1 + Power(t2,2)) + 
                  Power(t1,4)*(-249 - 512*t2 + 21*Power(t2,2)) + 
                  Power(t1,2)*(-104 - 344*t2 + 209*Power(t2,2)))) + 
            Power(s2,2)*t1*(-24 + 2*Power(t1,7)*(-7 + 8*s1 - 6*t2) + 
               98*t2 - 33*Power(t2,2) + 5*Power(t2,3) + 
               Power(t1,6)*(-15 - 12*Power(s1,2) + 37*t2 + 
                  6*Power(t2,2) + s1*(-53 + 29*t2)) + 
               Power(t1,4)*(493 + 290*Power(s1,2) - 30*Power(s1,3) - 
                  84*t2 - 400*Power(t2,2) + 90*Power(t2,3) + 
                  s1*(-642 + 630*t2 - 119*Power(t2,2))) - 
               Power(t1,5)*(-40 + 16*Power(s1,3) + 
                  Power(s1,2)*(50 - 52*t2) + 128*t2 - 143*Power(t2,2) + 
                  18*Power(t2,3) + s1*(-312 + 256*t2 + 7*Power(t2,2))) + 
               t1*(-642 + 339*t2 - 9*Power(t2,2) - 22*Power(t2,3) + 
                  s1*(254 - 276*t2 + 60*Power(t2,2))) - 
               Power(t1,2)*(-1828 + 18*Power(s1,3) + 1209*t2 + 
                  57*Power(t2,2) - 59*Power(t2,3) + 
                  12*Power(s1,2)*(-15 + 2*t2) + 
                  s1*(661 - 809*t2 + 145*Power(t2,2))) + 
               Power(t1,3)*(-1666 + 112*Power(s1,3) + 959*t2 + 
                  326*Power(t2,2) - 114*Power(t2,3) - 
                  4*Power(s1,2)*(108 + 31*t2) + 
                  s1*(774 - 888*t2 + 259*Power(t2,2)))) + 
            Power(s2,3)*(-2 + 5*Power(t1,8) - 7*t2 - 4*Power(t2,2) + 
               Power(t2,3) + Power(t1,7)*(56 - 73*s1 + 61*t2) + 
               Power(t1,6)*(-83 + Power(s1,2) + s1*(68 - 58*t2) - 
                  51*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(1109 - 102*Power(s1,3) - 732*t2 - 
                  575*Power(t2,2) + 235*Power(t2,3) + 
                  3*Power(s1,2)*(121 + 57*t2) + 
                  s1*(-336 + 720*t2 - 303*Power(t2,2))) + 
               t1*(285 - 99*t2 - 34*Power(t2,2) + 21*Power(t2,3) + 
                  s1*(-98 + 102*t2 - 23*Power(t2,2))) + 
               Power(t1,5)*(244 + 46*Power(s1,3) + 
                  Power(s1,2)*(131 - 101*t2) - 358*t2 - 
                  303*Power(t2,2) + 72*Power(t2,3) + 
                  s1*(-215 + 482*t2 - 14*Power(t2,2))) + 
               Power(t1,2)*(-845 + 12*Power(s1,3) + 
                  3*Power(s1,2)*(-36 + t2) + 309*t2 + 297*Power(t2,2) - 
                  112*Power(t2,3) + s1*(247 - 432*t2 + 99*Power(t2,2))) + 
               Power(t1,4)*(-769 + 44*Power(s1,3) + 877*t2 + 
                  588*Power(t2,2) - 289*Power(t2,3) - 
                  Power(s1,2)*(411 + 145*t2) + 
                  s1*(407 - 766*t2 + 385*Power(t2,2))))) - 
         s*(2*(-2 + t1)*Power(-1 + t1,3)*Power(t1,3) + 
            Power(s2,8)*Power(-1 + t1,2)*
             (-2 + s1 + 5*s1*t1 + t1*(2 - 5*t2) - t2) + 
            Power(s2,7)*(-1 + t1)*
             (-1 + 2*Power(s1,2)*t1*(3 + 11*t1) + 3*t2 + 2*Power(t2,2) + 
               5*Power(t1,3)*(-3 + 4*t2) + 
               Power(t1,2)*(29 - 31*t2 + 8*Power(t2,2)) + 
               t1*(-13 + 8*t2 + 18*Power(t2,2)) - 
               s1*(13*Power(t1,3) + 2*(-1 + t2) + t1*(11 + 24*t2) + 
                  Power(t1,2)*(-22 + 30*t2))) + 
            s2*Power(t1,2)*(-15 + 18*t2 - 25*Power(t2,2) + 
               10*Power(t2,3) + 
               2*Power(t1,6)*
                (-4 + 3*Power(s1,2) + s1*(4 - 6*t2) + 3*t2) + 
               t1*(206 - 297*t2 + 160*Power(t2,2) - 25*Power(t2,3) + 
                  s1*(-59 + 94*t2 - 35*Power(t2,2))) + 
               Power(t1,3)*(340 - 45*Power(s1,3) - 375*t2 + 
                  128*Power(t2,2) - 32*Power(t2,3) + 
                  Power(s1,2)*(255 + 7*t2) + 
                  s1*(-331 + 123*t2 - 34*Power(t2,2))) + 
               Power(t1,4)*(-51 + 19*Power(s1,3) - 81*t2 + 
                  108*Power(t2,2) + 6*Power(t2,3) + 
                  Power(s1,2)*(-135 + 14*t2) - 
                  s1*(-261 + 148*t2 + Power(t2,2))) + 
               Power(t1,5)*(-10 + 4*Power(s1,3) + 
                  Power(s1,2)*(3 - 20*t2) + 84*t2 - 72*Power(t2,2) + 
                  s1*(-94 + 93*t2 + 12*Power(t2,2))) + 
               Power(t1,2)*(-462 + 10*Power(s1,3) + 645*t2 - 
                  299*Power(t2,2) + 53*Power(t2,3) + 
                  Power(s1,2)*(-129 + 35*t2) + 
                  s1*(215 - 150*t2 + 22*Power(t2,2)))) + 
            Power(s2,6)*(2 + Power(t1,5)*(27 + s1 - 16*t2) - 
               4*(-1 + s1)*t2 + (2 + s1)*Power(t2,2) - Power(t2,3) - 
               Power(t1,4)*(45 + 57*Power(s1,2) + s1*(56 - 84*t2) - 
                  64*t2 + 38*Power(t2,2)) + 
               t1*(-46 - 8*Power(s1,3) + 5*t2 + 58*Power(t2,2) + 
                  35*Power(t2,3) + Power(s1,2)*(28 + 50*t2) + 
                  s1*(28 - 73*t2 - 77*Power(t2,2))) + 
               Power(t1,3)*(-33 + 19*Power(s1,3) + 
                  Power(s1,2)*(24 - 29*t2) - 67*t2 + 20*Power(t2,2) + 
                  8*Power(t2,3) + s1*(137 - 13*t2 + 2*Power(t2,2))) + 
               Power(t1,2)*(95 + Power(s1,3) + Power(s1,2)*(5 - 57*t2) + 
                  10*t2 - 42*Power(t2,2) - 54*Power(t2,3) + 
                  2*s1*(-55 + 3*t2 + 55*Power(t2,2)))) + 
            Power(s2,2)*t1*(6 + 3*t2 + 8*Power(t2,2) - 5*Power(t2,3) + 
               2*Power(t1,7)*(-1 + 3*Power(s1,2) + 12*t2 - 
                  2*s1*(4 + 3*t2)) - 
               Power(t1,4)*(172 + 39*Power(s1,3) - 641*t2 + 
                  135*Power(t2,2) + 58*Power(t2,3) + 
                  Power(s1,2)*(-458 + 87*t2) + 
                  s1*(697 + 3*t2 - 68*Power(t2,2))) + 
               Power(t1,6)*(71 + 4*Power(s1,3) - 39*t2 - 
                  60*Power(t2,2) - 4*Power(s1,2)*(4 + 5*t2) + 
                  2*s1*(-29 + 51*t2 + 6*Power(t2,2))) + 
               Power(t1,2)*(639 - 26*Power(s1,3) + 
                  Power(s1,2)*(330 - 88*t2) - 1015*t2 + 
                  533*Power(t2,2) - 114*Power(t2,3) + 
                  s1*(-343 + 135*t2 + 13*Power(t2,2))) + 
               Power(t1,3)*(-210 + 129*Power(s1,3) + 259*t2 - 
                  315*Power(t2,2) + 87*Power(t2,3) - 
                  Power(s1,2)*(687 + 61*t2) + 
                  2*s1*(300 + 52*t2 + 37*Power(t2,2))) - 
               Power(t1,5)*(10 + 8*Power(s1,3) + 
                  Power(s1,2)*(91 - 76*t2) + 343*t2 - 183*Power(t2,2) - 
                  12*Power(t2,3) + s1*(-405 + 154*t2 + 50*Power(t2,2))) + 
               t1*(-322 + 470*t2 - 214*Power(t2,2) + 18*Power(t2,3) + 
                  s1*(109 - 172*t2 + 63*Power(t2,2)))) + 
            Power(s2,4)*(Power(s1,3)*t1*
                (-6 + 59*t1 - 37*Power(t1,2) + 43*Power(t1,3) + 
                  61*Power(t1,4)) + t2 - 3*Power(t2,3) + 
               7*Power(t1,7)*(1 + 3*t2) + 
               Power(t1,6)*(52 + 47*t2 - 45*Power(t2,2)) + 
               Power(t1,4)*(-793 + 285*t2 + 35*Power(t2,2) - 
                  187*Power(t2,3)) + 
               Power(t1,3)*(720 + 21*t2 + 335*Power(t2,2) - 
                  14*Power(t2,3)) + 
               t1*(-134 + 81*t2 + 22*Power(t2,2) - 10*Power(t2,3)) + 
               Power(t1,2)*(-15 - 189*t2 - 206*Power(t2,2) + 
                  32*Power(t2,3)) + 
               Power(t1,5)*(163 - 267*t2 - 141*Power(t2,2) + 
                  62*Power(t2,3)) + 
               Power(s1,2)*t1*
                (15*Power(t1,5) + Power(t1,2)*(782 - 83*t2) - 
                  18*(-4 + t2) - 3*Power(t1,3)*(158 + 29*t2) - 
                  t1*(381 + 31*t2) - Power(t1,4)*(14 + 141*t2)) + 
               s1*(-1 - 26*Power(t1,7) + Power(t2,2) - 
                  Power(t1,6)*(65 + 14*t2) + 
                  Power(t1,2)*(398 + 446*t2 - 23*Power(t2,2)) + 
                  Power(t1,5)*(-126 + 373*t2 + 24*Power(t2,2)) + 
                  t1*(51 - 105*t2 + 29*Power(t2,2)) + 
                  4*Power(t1,4)*(262 - 7*t2 + 60*Power(t2,2)) + 
                  Power(t1,3)*(-1279 - 672*t2 + 89*Power(t2,2)))) + 
            Power(s2,5)*(-(Power(s1,3)*t1*
                  (10 - 30*t1 + 23*Power(t1,2) + 57*Power(t1,3))) - 
               Power(t1,6)*(19 + 14*t2) + t2*(2 + t2 + 3*Power(t2,2)) + 
               3*Power(t1,5)*(-11 - 9*t2 + 21*Power(t2,2)) + 
               Power(t1,4)*(79 + 106*t2 + 35*Power(t2,2) - 
                  58*Power(t2,3)) + 
               t1*(150 - 11*t2 + 2*Power(t2,2) - 6*Power(t2,3)) - 
               2*Power(t1,2)*(172 - 13*t2 + 28*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(t1,3)*(167 - 82*t2 - 45*Power(t2,2) + 
                  225*Power(t2,3)) + 
               Power(s1,2)*t1*
                (102 + 37*Power(t1,4) - 8*t2 - 3*t1*(113 + 21*t2) + 
                  Power(t1,3)*(1 + 104*t2) + Power(t1,2)*(199 + 147*t2)) \
+ s1*(2 + 25*Power(t1,6) + Power(t1,5)*(61 - 65*t2) + 2*t2 - 
                  2*Power(t2,2) - 
                  Power(t1,4)*(105 + 140*t2 + 9*Power(t2,2)) + 
                  t1*(-206 - 90*t2 + 37*Power(t2,2)) + 
                  Power(t1,2)*(498 + 324*t2 + 88*Power(t2,2)) - 
                  Power(t1,3)*(275 + 31*t2 + 294*Power(t2,2)))) + 
            Power(s2,3)*(-1 + Power(t1,8)*(-2 + 8*s1 - 6*t2) - 3*t2 - 
               Power(t2,2) + Power(t2,3) + 
               Power(t1,7)*(-10 + 50*s1 - 23*Power(s1,2) - 66*t2 + 
                  37*s1*t2 + 12*Power(t2,2)) + 
               Power(t1,4)*(-80 + 35*Power(s1,3) - 699*t2 - 
                  218*Power(t2,2) + 105*Power(t2,3) + 
                  Power(s1,2)*(-794 + 169*t2) + 
                  s1*(1191 + 570*t2 - 167*Power(t2,2))) + 
               Power(t1,2)*(-91 + 22*Power(s1,3) + 377*t2 - 
                  347*Power(t2,2) + 98*Power(t2,3) + 
                  Power(s1,2)*(-273 + 71*t2) + 
                  s1*(75 + 118*t2 - 62*Power(t2,2))) + 
               Power(t1,6)*(-230 - 27*Power(s1,3) + 207*t2 + 
                  154*Power(t2,2) - 18*Power(t2,3) + 
                  Power(s1,2)*(15 + 86*t2) + 
                  s1*(174 - 316*t2 - 29*Power(t2,2))) + 
               Power(t1,5)*(517 - 17*Power(s1,3) + 
                  Power(s1,2)*(364 - 59*t2) + 124*t2 - 115*Power(t2,2) + 
                  28*Power(t2,3) - 2*s1*(494 - 46*t2 + 9*Power(t2,2))) - 
               Power(t1,3)*(257 + 133*Power(s1,3) - 312*t2 - 
                  389*Power(t2,2) + 80*Power(t2,3) - 
                  3*Power(s1,2)*(237 + 31*t2) + 
                  s1*(461 + 579*t2 + 55*Power(t2,2))) + 
               t1*(s1*(-49 + 78*t2 - 29*Power(t2,2)) + 
                  2*(77 - 123*t2 + 63*Power(t2,2) - 7*Power(t2,3)))))))/
     (s*(-1 + s1)*(-1 + s2)*s2*
       (1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       Power(s2 - t1,2)*Power(-1 + t1,2)*t1*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(-s + s1 - t2)) + 
    (8*(-2*Power(s,7)*Power(t1,2)*(Power(t1,2) + t1*(-5 + t2) + t2) + 
         (s2 - t1)*(-1 + t1)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,
           2)*(2*Power(s2,3)*(-1 + t1) + 
            Power(t1,2)*(-15 + 7*s1 - 2*t2) + 3*(-1 + t2) - 
            t1*(-18 + s1 + 7*t2) + 
            Power(s2,2)*(-2*Power(t1,2) + 3*s1*(1 + t1) - 7*t2 + 
               t1*(2 + t2)) + 
            s2*(-15 + s1*(1 - 10*t1 - 3*Power(t1,2)) + 4*t2 + 
               6*t1*(3 + t2) + Power(t1,2)*(-3 + 2*t2))) - 
         Power(s,5)*(Power(t1,6) + Power(t1,5)*(-3*s1 + 8*(-4 + t2)) + 
            Power(t2,3) + t1*t2*(-4 + (6 + s1)*t2 - 2*Power(t2,2)) - 
            2*Power(t1,3)*(68 + 3*Power(s1,2) + s1*(29 - 9*t2) - 
               15*t2 + 2*Power(t2,2)) + 
            Power(t1,4)*(133 + 2*Power(s1,2) - 11*s1*(-5 + t2) - 
               41*t2 + 3*Power(t2,2)) + 
            2*Power(s2,2)*(-((-51 + s1)*Power(t1,2)) + 3*Power(t1,4) + 
               t1*(-17 + 2*s1 - 5*t2) + t2 + 
               Power(t1,3)*(-37 - 3*s1 + 6*t2)) + 
            Power(t1,2)*(-6 - 13*t2 - 13*Power(t2,2) + Power(t2,3) + 
               s1*(2 + 9*t2 - Power(t2,2))) + 
            s2*(-5*Power(t1,5) + Power(t1,4)*(107 + 10*s1 - 29*t2) - 
               2*Power(t2,2) - 
               2*Power(t1,3)*
                (133 + 26*s1 + Power(s1,2) - 28*t2 - 2*s1*t2) + 
               Power(t1,2)*(148 + 34*s1 - 2*Power(s1,2) - 27*t2 + 
                  4*s1*t2 - 9*Power(t2,2)) + 
               t1*(4 - 8*s1*t2 + 15*Power(t2,2)))) - 
         2*Power(s,6)*t1*(Power(t1,4) - 
            Power(t1,3)*(18 + 2*s1 + 3*s2 - 5*t2) + (2*s2 - t2)*t2 + 
            Power(t1,2)*(30 + 23*s2 + s1*(6 + s2 - t2) - 5*t2 - 
               4*s2*t2) + t1*(1 - (2 + s1)*t2 + Power(t2,2) + 
               s2*(s1 - 2*(8 + t2)))) + 
         Power(s,4)*(Power(t1,6)*(21 + 2*s1 - 3*t2) + 
            Power(t2,2)*(-3 + 2*t2) + 
            Power(t1,5)*(-96 - 44*s1 + 8*t2 + 6*s1*t2) + 
            t1*(2 + (-11 + s1)*t2 + (1 + 5*s1)*Power(t2,2) - 
               10*Power(t2,3)) + 
            2*Power(s2,3)*(-1 + t1)*
             (Power(t1,3) + s1*(1 - 2*t1 - 3*Power(t1,2)) + 
               4*Power(t1,2)*(-6 + t2) - 2*(3 + t2) + t1*(29 + 2*t2)) + 
            Power(t1,2)*(6 - 26*t2 - 14*Power(t2,2) + 9*Power(t2,3) + 
               Power(s1,2)*(2 + t2) + s1*(-2 + 5*t2 - 7*Power(t2,2))) - 
            Power(t1,4)*(-196 + Power(s1,2)*(-28 + t2) + 22*t2 - 
               27*Power(t2,2) + Power(t2,3) - 
               2*s1*(69 - 36*t2 + Power(t2,2))) + 
            Power(t1,3)*(-149 + 36*t2 - 17*Power(t2,2) + 
               Power(s1,2)*(-30 + 4*t2) - 
               2*s1*(53 - 29*t2 + Power(t2,2))) - 
            Power(s2,2)*(2 + 4*Power(t1,5) + 
               2*Power(t1,3)*(182 + 29*s1 + Power(s1,2) - 38*t2) + 
               (4 - 6*s1)*t2 + 11*Power(t2,2) + 
               Power(t1,4)*(-109 - 9*s1 + 27*t2) + 
               t1*(114 - 6*Power(s1,2) - 54*t2 - 34*Power(t2,2) + 
                  4*s1*(10 + 7*t2)) + 
               Power(t1,2)*(-373 + 107*t2 + 17*Power(t2,2) - 
                  s1*(93 + 10*t2))) + 
            s2*(2*Power(t1,6) - 2*Power(t1,5)*(38 + 3*s1 - 8*t2) + 
               t2*(4 - (1 + 4*s1)*t2 + 9*Power(t2,2)) + 
               t1*(4 + (23 - 19*s1 - 2*Power(s1,2))*t2 + 
                  (29 + 11*s1)*Power(t2,2) - 14*Power(t2,3)) - 
               Power(t1,3)*(513 + s1*(254 - 72*t2) - 172*t2 + 
                  29*Power(t2,2) + 2*Power(s1,2)*(14 + t2)) + 
               Power(t1,2)*(245 + 7*Power(s1,2) - 157*t2 - 
                  15*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(173 - 9*t2 - 5*Power(t2,2))) + 
               Power(t1,4)*(Power(s1,2) + s1*(111 - 14*t2) + 
                  8*(42 - 10*t2 + Power(t2,2))))) + 
         Power(s,3)*(12*Power(t1,7) + 3*(-1 + t2)*t2 - 
            Power(t1,6)*(60 + Power(s1,2) - 3*s1*(-8 + t2) + 7*t2) + 
            2*Power(s2,4)*Power(-1 + t1,2)*
             (-6 + s1 + 6*t1 + s1*t1 - t2 - t1*t2) + 
            t1*(-5 - 9*t2 + 3*Power(t2,2) + 9*Power(t2,3) + 
               s1*(2 + 8*t2 - 9*Power(t2,2))) + 
            Power(t1,5)*(148 - Power(s1,3) + 63*t2 + 10*Power(t2,2) + 
               Power(s1,2)*(13 + 2*t2) - s1*(-62 + 31*t2 + Power(t2,2))) \
+ Power(t1,3)*(109 - 2*Power(s1,3) + Power(s1,2)*(78 - 14*t2) + 67*t2 + 
               42*Power(t2,2) + 4*Power(t2,3) + 
               s1*(23 - 99*t2 + 4*Power(t2,2))) - 
            Power(t1,4)*(165 + Power(s1,3) + Power(s1,2)*(84 - 15*t2) + 
               168*t2 + 51*Power(t2,2) - 5*Power(t2,3) + 
               s1*(51 - 137*t2 + 13*Power(t2,2))) + 
            Power(t1,2)*(-39 + 15*t2 - 37*Power(t2,2) - 
               20*Power(t2,3) - Power(s1,2)*(8 + 3*t2) + 
               s1*(30 + 14*t2 + 25*Power(t2,2))) + 
            Power(s2,3)*(-26 + Power(t1,5) + 
               Power(s1,2)*(4 - 9*t1 + 3*Power(t1,2) - 2*Power(t1,3)) + 
               7*Power(t1,4)*(-6 + t2) + 29*t2 + 16*Power(t2,2) - 
               2*Power(t1,3)*(-89 + 8*t2) + 
               t1*(149 - 92*t2 - 31*Power(t2,2)) + 
               Power(t1,2)*(-260 + 76*t2 + 15*Power(t2,2)) - 
               2*s1*(2*Power(t1,4) + 9*(1 + t2) - 
                  2*Power(t1,3)*(3 + t2) + Power(t1,2)*(29 + 10*t2) - 
                  t1*(32 + 19*t2))) - 
            Power(s2,2)*(2 + Power(t1,6) + 
               Power(s1,3)*t1*(1 + t1 + 2*Power(t1,2)) - 5*t2 + 
               10*Power(t2,2) + 29*Power(t2,3) + 
               Power(t1,5)*(-60 + 8*t2) + 
               Power(t1,3)*(-511 + 139*t2 - 46*Power(t2,2)) + 
               Power(t1,4)*(274 - 5*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(419 - 301*t2 + 61*Power(t2,2) + 
                  10*Power(t2,3)) - 
               t1*(125 - 170*t2 + 21*Power(t2,2) + 37*Power(t2,3)) - 
               Power(s1,2)*(t1 + 4*Power(t1,4) - 5*t2 + 10*t1*t2 + 
                  2*Power(t1,3)*(7 + 2*t2) - Power(t1,2)*(17 + 5*t2)) - 
               s1*(2 + 5*Power(t1,5) + Power(t1,3)*(237 - 39*t2) + 
                  27*Power(t2,2) - Power(t1,4)*(55 + 6*t2) + 
                  t1*(159 + 5*t2 - 36*Power(t2,2)) + 
                  Power(t1,2)*(-342 + 48*t2 + 11*Power(t2,2)))) - 
            s2*(-2 + Power(t1,6)*(43 + 3*s1 - 4*t2) + t2 + 5*s1*t2 - 
               4*(3 + s1)*Power(t2,2) + 11*Power(t2,3) + 
               Power(t1,5)*(-192 + Power(s1,2) + s1*(-69 + t2) - 
                  23*t2 + Power(t2,2)) + 
               Power(t1,2)*(185 - 3*Power(s1,3) - 68*t2 + 
                  103*Power(t2,2) + 34*Power(t2,3) + 
                  Power(s1,2)*(76 + 3*t2) + 
                  s1*(213 - 171*t2 - 22*Power(t2,2))) + 
               Power(t1,4)*(410 - 3*Power(s1,3) + 43*t2 + 
                  58*Power(t2,2) - 3*Power(t2,3) + 
                  Power(s1,2)*(29 + 7*t2) + 
                  4*s1*(61 - 25*t2 + Power(t2,2))) - 
               Power(t1,3)*(421 + 2*Power(s1,3) - 
                  5*Power(s1,2)*(-21 + t2) - 11*t2 + 150*Power(t2,2) - 
                  4*Power(t2,3) + 2*s1*(181 - 135*t2 + 7*Power(t2,2))) - 
               t1*(23 + 4*t2 + 40*Power(t2,2) + 50*Power(t2,3) + 
                  Power(s1,2)*(5 + 11*t2) - 
                  s1*(15 + 39*t2 + 44*Power(t2,2))))) + 
         s*(-2*(3 + s1)*Power(t1,8) + Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-9 + 2*s1 + 11*t2) + 
            2*Power(t1,7)*(12 + s1 + 2*t2 + s1*t2) + 
            Power(t1,6)*(-76 + Power(s1,3) + s1*(21 - 6*t2) - 23*t2 + 
               Power(s1,2)*t2 - 6*Power(t2,2)) + 
            4*Power(s2,5)*Power(-1 + t1,2)*
             ((-3 + 2*t1 - t2)*t2 + s1*(1 + t2)) + 
            Power(t1,2)*(-1 + t2)*
             (50 - Power(s1,2) - 86*t2 + 21*Power(t2,2) + 
               4*s1*(-5 + 7*t2)) + 
            Power(t1,5)*(173 - 9*Power(s1,3) + 
               Power(s1,2)*(99 - 22*t2) + 116*t2 + 2*Power(t2,2) + 
               s1*(-207 - 10*t2 + 10*Power(t2,2))) + 
            Power(t1,4)*(-219 + 12*Power(s1,3) - 92*t2 + 
               8*Power(t2,2) - 7*Power(t2,3) + 
               Power(s1,2)*(-149 + 37*t2) + 
               s1*(369 - 106*t2 + 19*Power(t2,2))) - 
            Power(t1,3)*(-146 + 2*Power(s1,3) + 115*t2 - 
               75*Power(t2,2) + 6*Power(t2,3) + 
               7*Power(s1,2)*(-7 + 3*t2) + 
               s1*(201 - 164*t2 + 49*Power(t2,2))) - 
            s2*(Power(s1,3)*Power(t1,2)*
                (-6 + 41*t1 - 30*Power(t1,2) + 3*Power(t1,3)) - 
               2*Power(t1,7)*(11 + t2) - Power(-1 + t2,2)*(-5 + 7*t2) + 
               2*Power(t1,6)*(48 + 9*t2 + Power(t2,2)) - 
               Power(t1,5)*(228 + 148*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(180 + 92*t2 + 42*Power(t2,2) - 
                  14*Power(t2,3)) + 
               Power(t1,4)*(359 + 571*t2 + 49*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(t1,3)*(344 + 654*t2 - 9*Power(t2,2) + 
                  6*Power(t2,3)) + 
               2*t1*(-23 + 70*t2 - 56*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2 - 2*t2 + Power(t1,5)*(7 + t2) + 
                  4*Power(t1,3)*(59 + t2) - 
                  3*Power(t1,4)*(-7 + 4*t2) - 7*t1*(-19 + 7*t2) + 
                  Power(t1,2)*(-399 + 34*t2)) - 
               s1*(8*Power(t1,7) + Power(t1,6)*(-21 + t2) + 
                  2*Power(-1 + t2,2) + 
                  t1*(-15 + 46*t2 - 31*Power(t2,2)) + 
                  Power(t1,4)*(434 + 240*t2 - 20*Power(t2,2)) + 
                  Power(t1,5)*(-25 + 14*t2 - 2*Power(t2,2)) + 
                  Power(t1,3)*(-760 - 164*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*(377 - 133*t2 + 14*Power(t2,2)))) + 
            Power(s2,2)*(-2 + 
               3*Power(s1,3)*t1*
                (-2 + 17*t1 - 12*Power(t1,2) + Power(t1,3)) + 22*t2 - 
               23*Power(t2,2) + 3*Power(t2,3) - 
               Power(t1,6)*(35 + 9*t2) - 
               2*Power(t1,5)*(-71 - 36*t2 + Power(t2,2)) - 
               Power(t1,2)*(145 + 784*t2 + 196*Power(t2,2)) + 
               t1*(40 + 275*t2 + 13*Power(t2,2) - 28*Power(t2,3)) + 
               Power(t1,3)*(250 + 709*t2 + 245*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,4)*(-250 - 285*t2 - 37*Power(t2,2) + 
                  7*Power(t2,3)) + 
               Power(s1,2)*(1 + Power(t1,4)*(45 - 24*t2) - t2 + 
                  2*Power(t1,5)*(11 + t2) - 7*t1*(-17 + 5*t2) - 
                  Power(t1,2)*(350 + 73*t2) + Power(t1,3)*(163 + 95*t2)\
) - s1*(5 - 2*t2 - 3*Power(t2,2) - 4*Power(t1,5)*(6 + t2) + 
                  Power(t1,6)*(11 + t2) + 
                  t1*(201 + 126*t2 - 69*Power(t2,2)) + 
                  Power(t1,4)*(8 + 20*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(-424 - 627*t2 + 10*Power(t2,2)) + 
                  Power(t1,3)*(223 + 486*t2 + 19*Power(t2,2)))) + 
            Power(s2,4)*(-4 + Power(s1,3)*(5 - 3*t1) + 9*t2 - 
               42*Power(t2,2) - 28*Power(t2,3) + 
               Power(t1,4)*(-8 - 23*t2 + Power(t2,2)) + 
               4*Power(t1,3)*(7 + 23*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*(36 + 106*t2 + 43*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(20 + 28*t2 + 76*Power(t2,2) + 31*Power(t2,3)) + 
               Power(s1,2)*(1 + 8*Power(t1,3) - 30*t2 + 
                  3*Power(t1,2)*(1 + t2) + 3*t1*(-4 + 7*t2)) - 
               s1*(9 - 4*Power(t1,3)*(-5 + t2) - 21*t2 - 
                  53*Power(t2,2) + Power(t1,4)*(1 + t2) - 
                  2*Power(t1,2)*(17 - 10*t2 + Power(t2,2)) + 
                  t1*(4 + 4*t2 + 49*Power(t2,2)))) - 
            Power(s2,3)*(3 + Power(s1,3)*
                (-2 + 27*t1 - 18*Power(t1,2) + Power(t1,3)) + 77*t2 + 
               37*Power(t2,2) - 17*Power(t2,3) + 
               Power(t1,5)*(-27 - 22*t2 + Power(t2,2)) + 
               Power(t1,4)*(99 + 119*t2 + 3*Power(t2,2) - Power(t2,3)) + 
               3*Power(t1,3)*
                (-46 - 74*t2 - 27*Power(t2,2) + 2*Power(t2,3)) + 
               6*Power(t1,2)*
                (15 + 42*t2 + 48*Power(t2,2) + 5*Power(t2,3)) - 
               t1*(27 + 204*t2 + 248*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,3)*(27 - 8*t2) - 7*(-5 + t2) + 
                  Power(t1,4)*(23 + t2) + 2*Power(t1,2)*(7 + 45*t2) - 
                  t1*(99 + 100*t2)) - 
               s1*(25 + Power(t1,4)*(11 - 15*t2) + 95*t2 - 
                  34*Power(t2,2) + 2*Power(t1,5)*(3 + t2) + 
                  Power(t1,3)*(-14 + 40*t2 + 7*Power(t2,2)) - 
                  3*t1*(8 + 126*t2 + 25*Power(t2,2)) + 
                  Power(t1,2)*(-4 + 256*t2 + 78*Power(t2,2))))) + 
         Power(s,2)*(-1 - 9*t1 + 45*Power(t1,2) - 124*Power(t1,3) + 
            201*Power(t1,4) - 181*Power(t1,5) + 95*Power(t1,6) - 
            30*Power(t1,7) + 4*Power(t1,8) + 
            Power(s1,3)*Power(s2 - t1,2)*
             (3*t1*(2 - t1 + Power(t1,2)) + 
               s2*(-2 + t1 - 3*Power(t1,2) + 2*Power(t1,3))) + 24*t1*t2 - 
            108*Power(t1,2)*t2 - 122*Power(t1,3)*t2 + 
            335*Power(t1,4)*t2 - 158*Power(t1,5)*t2 + 33*Power(t1,6)*t2 - 
            4*Power(t1,7)*t2 + 3*Power(t2,2) - 22*t1*Power(t2,2) + 
            82*Power(t1,2)*Power(t2,2) - 79*Power(t1,3)*Power(t2,2) + 
            8*Power(t1,4)*Power(t2,2) - 20*Power(t1,5)*Power(t2,2) + 
            4*Power(t1,6)*Power(t2,2) - 2*Power(t2,3) + 
            7*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) - 
            7*Power(t1,3)*Power(t2,3) - 3*Power(t1,4)*Power(t2,3) + 
            Power(s2,4)*(10 - 13*t2 - 5*Power(t2,2) + 
               Power(t1,4)*(4 + t2) - 2*Power(t1,3)*(10 + 11*t2) + 
               Power(t1,2)*(38 + 28*t2 - 3*Power(t2,2)) + 
               t1*(-32 + 6*t2 + 6*Power(t2,2))) - 
            s2*(14*Power(t1,7) - 2*Power(t1,6)*(52 + 9*t2) + 
               t2*(1 - 3*t2 + 2*Power(t2,2)) + 
               2*Power(t1,5)*(133 + 74*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(289 + 717*t2 + 215*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,4)*(-355 - 453*t2 - 102*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(31 - 61*t2 + 56*Power(t2,2) + 14*Power(t2,3)) - 
               Power(t1,2)*(141 + 334*t2 + 228*Power(t2,2) + 
                  40*Power(t2,3))) + 
            Power(s2,3)*(13 - 16*Power(t1,5) - 33*t2 + 57*Power(t2,2) + 
               42*Power(t2,3) + Power(t1,4)*(81 + 57*t2) - 
               Power(t1,3)*(168 + 91*t2 + 25*Power(t2,2)) + 
               2*Power(t1,2)*
                (85 - 30*t2 + 51*Power(t2,2) + 5*Power(t2,3)) - 
               t1*(80 - 127*t2 + 124*Power(t2,2) + 48*Power(t2,3))) + 
            Power(s2,2)*(-(Power(t1,6)*(-22 + t2)) + 
               Power(t1,5)*(-133 - 53*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(295 + 187*t2 + 32*Power(t2,2) - 
                  3*Power(t2,3)) + 
               2*(5 - 2*t2 + 2*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(-317 - 222*t2 - 213*Power(t2,2) + 
                  9*Power(t2,3)) + 
               Power(t1,2)*(181 + 214*t2 + 375*Power(t2,2) + 
                  45*Power(t2,3)) - 
               t1*(58 + 121*t2 + 238*Power(t2,2) + 73*Power(t2,3))) + 
            Power(s1,2)*(s2 - t1)*
             (Power(s2,3)*(-5 + 8*t1 - 7*Power(t1,2) + 2*Power(t1,3)) + 
               Power(s2,2)*(5 - Power(t1,4) + t1*(10 - 20*t2) + 
                  Power(t1,3)*(1 - 2*t2) + 7*Power(t1,2)*(-1 + t2) + 
                  23*t2) - s2*
                (Power(t1,5) - 2*t2 - Power(t1,4)*(5 + 7*t2) - 
                  Power(t1,2)*(97 + 15*t2) + Power(t1,3)*(28 + 19*t2) + 
                  t1*(103 + 29*t2)) + 
               t1*(1 - 4*Power(t1,4) + Power(t1,3)*(41 - 2*t2) - 3*t2 - 
                  4*t1*(-31 + 5*t2) + Power(t1,2)*(-138 + 41*t2))) + 
            s1*(Power(s2,4)*(11 + Power(t1,4) - 
                  2*Power(t1,3)*(-3 + t2) + 12*t2 + 
                  4*Power(t1,2)*(-1 + 3*t2) - 2*t1*(7 + 9*t2)) + 
               t1*(6 - 8*Power(t1,6) + Power(t1,5)*(9 - 5*t2) - 13*t2 + 
                  7*Power(t2,2) + 
                  Power(t1,4)*(48 + 42*t2 + Power(t2,2)) + 
                  3*Power(t1,3)*(-85 - 19*t2 + 7*Power(t2,2)) - 
                  2*t1*(7 - 20*t2 + 20*Power(t2,2)) + 
                  Power(t1,2)*(214 + 41*t2 + 25*Power(t2,2))) + 
               s2*(-1 - Power(t1,6)*(-36 + t2) + 3*t2 - 2*Power(t2,2) + 
                  Power(t1,3)*(158 + 445*t2 - 40*Power(t2,2)) + 
                  Power(t1,5)*(-62 + 13*t2 + Power(t2,2)) + 
                  4*t1*(1 - 2*t2 + 8*Power(t2,2)) + 
                  Power(t1,4)*(64 - 188*t2 + 15*Power(t2,2)) - 
                  Power(t1,2)*(199 + 372*t2 + 44*Power(t2,2))) + 
               Power(s2,3)*(39 - 2*Power(t1,5) - 35*t2 - 
                  59*Power(t2,2) + Power(t1,4)*(1 + 10*t2) - 
                  Power(t1,3)*(25 + 31*t2) + 
                  Power(t1,2)*(132 + 15*t2 - 10*Power(t2,2)) + 
                  t1*(-145 + 21*t2 + 59*Power(t2,2))) + 
               Power(s2,2)*(-3 + Power(t1,6) - 6*t2 - 5*Power(t2,2) - 
                  7*Power(t1,5)*(5 + t2) + 
                  Power(t1,2)*(233 - 425*t2 - 38*Power(t2,2)) + 
                  Power(t1,3)*(-237 + 163*t2 - 13*Power(t2,2)) + 
                  Power(t1,4)*(85 + 5*t2 + 2*Power(t2,2)) + 
                  t1*(-44 + 346*t2 + 88*Power(t2,2))))))*B1(s,t1,s2))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*
       (-s + s1 - t2)) + (8*(2*Power(-1 + s,3)*s*(1 - 3*s + Power(s,2))*
          Power(t1,4)*(-2 + s + t1)*Power(-1 + s + t1,2) - 
         2*Power(s2,12)*(-1 + t1)*Power(s1 - t2,2) - 
         (-1 + s)*s*s2*Power(t1,3)*(-1 + s + t1)*
          (4*Power(s,7)*t1 - 2*(-1 + t1)*
             (9 + (36 - 11*s1)*t1 + (1 + s1)*Power(t1,3) + 
               Power(t1,2)*(-17 + 5*s1 - t2) - t2) - 
            Power(s,6)*(-8 + (19 + 4*s1)*t1 - 8*Power(t1,2) + t2) + 
            2*s*(-62 + (3 + 2*s1)*Power(t1,4) + 
               2*t1*(-50 + 28*s1 - 3*t2) + 
               Power(t1,2)*(181 - 69*s1 - 2*t2) + 
               Power(t1,3)*(-49 + 16*s1 - t2) + 11*t2) + 
            Power(s,3)*(-347 + 2*Power(t1,4) + 
               Power(t1,2)*(85 - 154*s1 - 20*t2) + 78*t2 + 
               2*Power(t1,3)*(5 + 10*s1 + t2) + t1*(-116 + 215*s1 + t2)) \
+ Power(s,5)*(-65 - (35 + 8*s1)*Power(t1,2) + 4*Power(t1,3) + 12*t2 + 
               t1*(28 + 33*s1 + 3*t2)) + 
            Power(s,4)*(211 - 2*(7 + 2*s1)*Power(t1,3) + 
               t1*(7 - 115*s1 - 9*t2) - 46*t2 + 
               Power(t1,2)*(39 + 55*s1 + 6*t2)) + 
            Power(s,2)*(299 - 2*(3 + s1)*Power(t1,4) - 63*t2 - 
               2*Power(t1,3)*(-21 + 20*s1 + t2) + 
               t1*(242 - 219*s1 + 15*t2) + 
               Power(t1,2)*(-333 + 213*s1 + 20*t2))) + 
         Power(s2,11)*(2*Power(s,2)*Power(-1 + t1,2) - 
            (s1 - t2)*(4 + Power(s1,2)*(1 + 3*t1) - 
               2*s1*(5*Power(t1,2) + t1*(-2 + t2) + 3*(-1 + t2)) - 
               10*t2 + 5*Power(t2,2) + Power(t1,2)*(4 + 6*t2) - 
               t1*(8 - 4*t2 + Power(t2,2))) + 
            2*s*(Power(s1,2)*(-2 + 3*t1) - Power(t1,2)*(1 + 2*t2) - 
               Power(1 + 2*t2,2) + t1*(2 + 6*t2 + 5*Power(t2,2)) + 
               s1*(9 + 7*Power(t1,2) + 6*t2 - 8*t1*(2 + t2)))) + 
         Power(s2,10)*(2 - 6*t1 + 6*Power(t1,2) - 2*Power(t1,3) + 
            Power(s,3)*(-2 + 8*t1 - 6*Power(t1,2)) + 
            Power(s1,3)*(2 + 19*t1 + 15*Power(t1,2)) - 16*t2 + 
            20*t1*t2 + 8*Power(t1,2)*t2 - 12*Power(t1,3)*t2 + 
            43*Power(t2,2) - 18*t1*Power(t2,2) - 
            19*Power(t1,2)*Power(t2,2) - 6*Power(t1,3)*Power(t2,2) - 
            27*Power(t2,3) - 9*t1*Power(t2,3) - 
            Power(s1,2)*(-21 + 20*Power(t1,3) + 23*t2 + 
               3*t1*(-4 + 21*t2) + Power(t1,2)*(13 + 22*t2)) + 
            s1*(12 - 62*t2 + 48*Power(t2,2) + 
               8*Power(t1,3)*(2 + 3*t2) + 
               Power(t1,2)*(-20 + 38*t2 + 7*Power(t2,2)) + 
               t1*(-8 + 53*Power(t2,2))) - 
            2*Power(s,2)*(-12 + 2*Power(s1,2) + 6*Power(t1,3) - 14*t2 - 
               3*Power(t2,2) - Power(t1,2)*(25 + 14*t2) + 
               t1*(31 + 30*t2 + 7*Power(t2,2)) + 
               2*s1*(8 + 14*Power(t1,2) - t1*(23 + 3*t2))) + 
            s*(Power(s1,3)*(7 + 10*t1) + 2*Power(t1,3)*(5 + 6*t2) - 
               Power(t1,2)*(18 + 48*t2 + 25*Power(t2,2)) + 
               2*(1 + 8*t2 + Power(t2,2) - 16*Power(t2,3)) + 
               t1*(6 + 20*t2 - 3*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s1,2)*(-22 + 9*Power(t1,2) + 38*t2 + 
                  3*t1*(13 + 7*t2)) + 
               s1*(-42 - 62*Power(t1,3) + 10*t2 + 63*Power(t2,2) + 
                  2*Power(t1,2)*(61 + 34*t2) + 
                  2*t1*(-9 - 13*t2 + 2*Power(t2,2))))) - 
         Power(s2,2)*Power(t1,2)*
          (-(Power(-1 + t1 - s1*t1 + t2,2)*
               (Power(t1,2)*(-13 + 5*s1 - 2*t2) + 3*(-1 + t2) - 
                 t1*(-16 + s1 + 5*t2))) + 
            Power(s,9)*(6 - 41*Power(t1,2) - 5*t2 + t1*(21 + 11*t2)) - 
            Power(s,8)*(29 + 92*Power(t1,3) - 62*t2 + 12*Power(t2,2) - 
               Power(t1,2)*(275 + 37*s1 + 14*t2) + 
               t1*(175 + 52*t2 + 2*s1*(7 + 6*t2))) + 
            Power(s,7)*(3 - 67*Power(t1,4) - 275*t2 + 96*Power(t2,2) - 
               3*Power(t2,3) + Power(t1,3)*(440 + 70*s1 + 7*t2) - 
               2*Power(t1,2)*
                (358 + 20*t2 + 3*Power(t2,2) + 6*s1*(28 + t2)) + 
               t1*(625 + t2 - 13*Power(t2,2) - 
                  2*s1*(-60 - 56*t2 + Power(t2,2)))) + 
            Power(s,5)*(6*(4 + s1)*Power(t1,5) - 
               3*(240 + 250*t2 - 152*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,4)*(-107 + 10*Power(s1,2) - 34*t2 + 
                  s1*(-219 + 4*t2)) + 
               t1*(816 - 1499*t2 - 238*Power(t2,2) + 20*Power(t2,3) + 
                  s1*(916 + 920*t2 - 42*Power(t2,2))) - 
               Power(t1,3)*(564 + 89*Power(s1,2) + 2*Power(s1,3) + 
                  10*t2 - 8*Power(t2,2) + 
                  4*s1*(-398 - 17*t2 + Power(t2,2))) + 
               Power(t1,2)*(1170 + 1221*t2 - 94*Power(t2,2) + 
                  8*Power(t2,3) + Power(s1,2)*(2 + 31*t2) - 
                  s1*(2738 + 484*t2 + 3*Power(t2,2)))) + 
            Power(s,6)*(256 - 14*Power(t1,5) + 
               Power(t1,4)*(224 + 45*s1 - 2*t2) + 614*t2 - 
               297*Power(t2,2) + 15*Power(t2,3) + 
               Power(t1,3)*(-614 + 12*Power(s1,2) - 57*t2 - 
                  s1*(499 + 8*t2)) + 
               Power(t1,2)*(640 - 215*t2 - 4*Power(s1,2)*t2 + 
                  43*Power(t2,2) - 2*Power(t2,3) + 2*s1*(649 + 58*t2)) \
+ t1*(-1124 + 510*t2 + 99*Power(t2,2) - 5*Power(t2,3) + 
                  s1*(-448 - 438*t2 + 13*Power(t2,2)))) - 
            s*(48 + (2 + 6*s1)*Power(t1,6) + 
               2*Power(t1,5)*
                (Power(s1,2) + s1*(9 - 4*t2) + 4*(-8 + t2)) - 57*t2 + 
               48*Power(t2,2) - 15*Power(t2,3) + 
               Power(t1,4)*(212 - 17*Power(s1,3) - 50*t2 + 
                  5*s1*(-29 + 4*t2) + Power(s1,2)*(66 + 4*t2)) + 
               Power(t1,2)*(-162 + Power(s1,2)*(36 - 35*t2) - 451*t2 + 
                  58*Power(t2,2) + 8*Power(t2,3) + 
                  s1*(-143 + 356*t2 - 57*Power(t2,2))) + 
               2*Power(t1,3)*
                (-45 + 6*Power(s1,3) + 104*t2 + 4*Power(t2,2) + 
                  Power(s1,2)*(-73 + 27*t2) + 
                  s1*(143 - 90*t2 - 6*Power(t2,2))) + 
               2*t1*(27 + 171*t2 - 78*Power(t2,2) + 10*Power(t2,3) + 
                  s1*(-11 - 52*t2 + 19*Power(t2,2)))) + 
            Power(s,3)*(-673 + 2*(-3 + s1)*Power(t1,6) - 51*t2 + 
               72*Power(t2,2) + 15*Power(t2,3) + 
               Power(t1,5)*(-94 - 22*Power(s1,2) + 4*s1*(5 + 2*t2)) + 
               Power(t1,4)*(7*Power(s1,3) + 4*Power(s1,2)*(36 + t2) + 
                  52*(23 + 2*t2) - 8*s1*(97 + 6*t2)) + 
               t1*(-1056 - 1899*t2 + 95*Power(t2,2) + 
                  s1*(750 + 848*t2 - 110*Power(t2,2))) - 
               Power(t1,3)*(3828 + 34*Power(s1,3) + 921*t2 - 
                  32*Power(t2,2) + Power(s1,2)*(89 + 42*t2) + 
                  8*s1*(-284 - 49*t2 + Power(t2,2))) + 
               Power(t1,2)*(4377 + 2336*t2 - 50*Power(t2,2) + 
                  2*Power(s1,2)*(-31 + 63*t2) + 
                  s1*(-1901 - 1292*t2 + 42*Power(t2,2)))) + 
            Power(s,4)*(942 + 2*Power(t1,6) + 
               2*Power(t1,5)*(11 + 2*Power(s1,2) - 6*t2) + 461*t2 - 
               345*Power(t2,2) + 15*Power(t2,3) + 
               Power(t1,4)*(-606 + 38*t2 + 4*s1*(140 + t2) - 
                  Power(s1,2)*(79 + 2*t2)) + 
               Power(t1,2)*(-3976 + Power(s1,2)*(15 - 89*t2) - 
                  2355*t2 + 85*Power(t2,2) - 10*Power(t2,3) + 
                  s1*(3217 + 1052*t2 - 3*Power(t2,2))) + 
               Power(t1,3)*(3055 + 15*Power(s1,3) + 507*t2 - 
                  28*Power(t2,2) + Power(s1,2)*(199 + 9*t2) + 
                  3*s1*(-915 - 78*t2 + 4*Power(t2,2))) + 
               t1*(392 + 2171*t2 + 190*Power(t2,2) - 25*Power(t2,3) + 
                  s1*(-1093 - 1134*t2 + 85*Power(t2,2)))) + 
            Power(s,2)*(260 + (6 + 4*s1)*Power(t1,6) - 104*t2 + 
               69*Power(t2,2) - 27*Power(t2,3) + 
               2*Power(t1,5)*
                (33 + 10*Power(s1,2) + 10*t2 - 4*s1*(5 + 2*t2)) - 
               Power(t1,4)*(509 + 32*Power(s1,2) + 19*Power(s1,3) + 
                  158*t2 - 4*s1*(81 + 16*t2)) + 
               Power(t1,2)*(-1939 + Power(s1,2)*(76 - 94*t2) - 
                  1343*t2 + 61*Power(t2,2) + 10*Power(t2,3) + 
                  s1*(325 + 916*t2 - 78*Power(t2,2))) + 
               Power(t1,3)*(1539 + 32*Power(s1,3) + 655*t2 - 
                  8*Power(t2,2) + Power(s1,2)*(-151 + 72*t2) - 
                  s1*(473 + 356*t2 + 8*Power(t2,2))) + 
               t1*(577 + 1050*t2 - 257*Power(t2,2) + 25*Power(t2,3) + 
                  s1*(-260 - 386*t2 + 87*Power(t2,2))))) + 
         Power(s2,3)*t1*(4*Power(s,10)*t1 - 
            Power(s,9)*t1*(20 + 4*s1 + 5*t1 - 17*t2) - 
            2*Power(-1 + s1,2)*Power(t1,5)*(-18 + 9*s1 - 4*t2) + 
            3*t1*(-16 + 7*s1 - t2)*Power(-1 + t2,2) - 
            6*Power(-1 + t2,3) - 
            (-1 + s1)*Power(t1,4)*
             (-78 + 23*Power(s1,2) + s1*(3 - 73*t2) + 105*t2 + 
               20*Power(t2,2)) + 
            Power(t1,2)*(-1 + t2)*
             (-72 - 20*Power(s1,2) - 61*t2 + 33*Power(t2,2) - 
               3*s1*(-47 + 7*t2)) + 
            Power(t1,3)*(12 + 5*Power(s1,3) - 177*t2 + 98*Power(t2,2) + 
               12*Power(t2,3) + Power(s1,2)*(-112 + 47*t2) + 
               s1*(129 + 74*t2 - 88*Power(t2,2))) + 
            Power(s,8)*(-165*Power(t1,3) + 2*(-6 + 5*t2) + 
               Power(t1,2)*(225 + 6*s1 + 94*t2) + 
               t1*(21 + 35*s1 - 168*t2 - 12*s1*t2 + 8*Power(t2,2))) + 
            Power(s,7)*(70 - 268*Power(t1,4) - 117*t2 + 
               24*Power(t2,2) + 5*Power(t2,3) + 
               Power(t1,3)*(962 + 103*s1 + 98*t2) - 
               Power(t1,2)*(1125 + 174*s1 + 320*t2 + 64*s1*t2 - 
                  3*Power(t2,2)) + 
               t1*(185 - 123*s1 + 647*t2 + 136*s1*t2 - 
                  143*Power(t2,2) + 5*Power(t2,3))) - 
            Power(s,6)*(147 + 136*Power(t1,5) - 472*t2 + 
               153*Power(t2,2) + 14*Power(t2,3) - 
               Power(t1,4)*(843 + 127*s1 + 37*t2) + 
               Power(t1,3)*(1754 - 32*Power(s1,2) + 105*t2 + 
                  25*Power(t2,2) + s1*(817 + 60*t2)) + 
               Power(t1,2)*(-2059 + 240*t2 + 12*Power(s1,2)*t2 + 
                  84*Power(t2,2) - 5*Power(t2,3) + 
                  s1*(-1040 - 502*t2 + 5*Power(t2,2))) + 
               t1*(900 + 876*t2 - 689*Power(t2,2) + 33*Power(t2,3) + 
                  s1*(-213 + 640*t2 + 6*Power(t2,2)))) + 
            Power(s,3)*(-192 + 14*Power(t1,7) + 
               2*Power(t1,6)*(-19 + 14*s1 + Power(s1,2) - 12*t2) - 
               403*t2 + 234*Power(t2,2) - 25*Power(t2,3) + 
               Power(t1,5)*(-532 + 7*Power(s1,3) - 
                  Power(s1,2)*(187 + 4*t2) + s1*(446 + 48*t2)) + 
               t1*(64 - 4091*t2 + 61*Power(t2,2) + 15*Power(t2,3) + 
                  s1*(37 + 1876*t2 - 186*Power(t2,2))) + 
               Power(t1,4)*(4391 + 10*Power(s1,3) + 730*t2 - 
                  8*Power(t2,2) + Power(s1,2)*(913 + 38*t2) + 
                  2*s1*(-1785 - 258*t2 + 8*Power(t2,2))) + 
               Power(t1,2)*(4704 + 5576*t2 + 91*Power(t2,2) + 
                  16*Power(t2,3) + 11*Power(s1,2)*(-19 + 40*t2) - 
                  2*s1*(941 + 1550*t2 + 8*Power(t2,2))) - 
               Power(t1,3)*(8265 + 105*Power(s1,3) + 3056*t2 + 
                  54*Power(t2,2) + Power(s1,2)*(593 + 248*t2) - 
                  s1*(5761 + 1672*t2 + 10*Power(t2,2)))) - 
            Power(s,5)*(-105 + 16*Power(t1,6) + 915*t2 - 
               381*Power(t2,2) - Power(t2,3) + 
               Power(t1,5)*(-205 - 50*s1 + 8*t2) + 
               Power(t1,4)*(286 - 53*Power(s1,2) + 114*t2 - 
                  4*Power(t2,2) + 4*s1*(151 + 6*t2)) + 
               t1*(-1643 + 622*t2 + 1282*Power(t2,2) - 
                  73*Power(t2,3) + s1*(173 - 1566*t2 - 3*Power(t2,2))) \
+ Power(t1,3)*(410 + 4*Power(s1,3) + 1117*t2 - 125*Power(t2,2) + 
                  8*Power(t2,3) + Power(s1,2)*(277 + 15*t2) + 
                  s1*(-2930 - 460*t2 + 9*Power(t2,2))) + 
               Power(t1,2)*(473 - 2989*t2 - 442*Power(t2,2) + 
                  32*Power(t2,3) - Power(s1,2)*(6 + 103*t2) + 
                  2*s1*(1421 + 841*t2 + 12*Power(t2,2)))) + 
            Power(s,4)*(76 + 28*Power(t1,6) + 912*t2 - 
               456*Power(t2,2) + 30*Power(t2,3) + 
               Power(t1,5)*(149 + 15*Power(s1,2) - 88*t2 + 
                  s1*(-47 + 16*t2)) + 
               Power(t1,4)*(-2322 + 3*Power(s1,3) + 
                  Power(s1,2)*(-436 + t2) - 271*t2 + 20*Power(t2,2) + 
                  s1*(1929 + 194*t2 - 20*Power(t2,2))) + 
               Power(t1,3)*(6189 + 34*Power(s1,3) + 3303*t2 - 
                  48*Power(t2,2) + 12*Power(t2,3) + 
                  Power(s1,2)*(807 + 121*t2) - 
                  2*s1*(2986 + 715*t2 + 2*Power(t2,2))) + 
               t1*(-1196 + 3295*t2 + 920*Power(t2,2) - 
                  65*Power(t2,3) + s1*(24 - 2206*t2 + 69*Power(t2,2))) \
+ Power(t1,2)*(-3573 + Power(s1,2)*(49 - 306*t2) - 6005*t2 - 
                  538*Power(t2,2) + 31*Power(t2,3) + 
                  s1*(3741 + 2934*t2 + 77*Power(t2,2)))) + 
            s*(-47 - 6*(-1 + s1)*Power(t1,7) + 75*t2 - 63*Power(t2,2) + 
               19*Power(t2,3) - 
               2*Power(t1,6)*
                (-11 + 5*Power(s1,2) + s1*(14 - 8*t2) + 14*t2) + 
               Power(t1,5)*(-25 + 3*Power(s1,3) + 
                  Power(s1,2)*(47 - 4*t2) + 108*t2 - s1*(21 + 16*t2)) - 
               Power(t1,3)*(-591 + 59*Power(s1,3) - 311*t2 + 
                  71*Power(t2,2) + 24*Power(t2,3) + 
                  Power(s1,2)*(-578 + 145*t2) + 
                  s1*(808 + 100*t2 - 151*Power(t2,2))) + 
               2*Power(t1,4)*
                (-172 + 37*Power(s1,3) - 121*t2 + 2*Power(t2,2) - 
                  Power(s1,2)*(88 + 41*t2) + 
                  s1*(89 + 54*t2 + 8*Power(t2,2))) + 
               t1*(40 - 711*t2 + 228*Power(t2,2) + 3*Power(t2,3) - 
                  3*s1*(19 - 98*t2 + 35*Power(t2,2))) + 
               Power(t1,2)*(-243 + 487*t2 + 208*Power(t2,2) - 
                  64*Power(t2,3) + Power(s1,2)*(-133 + 129*t2) + 
                  s1*(742 - 914*t2 + 88*Power(t2,2)))) - 
            Power(s,2)*(-141 + 2*(-6 + s1)*Power(t1,7) + 16*t2 - 
               15*Power(t2,2) + 10*Power(t2,3) + 
               4*Power(t1,6)*
                (-9 + 5*Power(s1,2) + 7*t2 - s1*(21 + 4*t2)) + 
               Power(t1,5)*(-523 - 217*Power(s1,2) + 8*Power(s1,3) - 
                  60*t2 + s1*(788 + 96*t2)) + 
               Power(t1,4)*(2266 + 64*Power(s1,3) + 29*t2 + 
                  40*Power(t2,2) + Power(s1,2)*(326 + 30*t2) - 
                  s1*(1967 + 320*t2 + 8*Power(t2,2))) + 
               Power(t1,3)*(-2940 - 129*Power(s1,3) + 
                  Power(s1,2)*(435 - 240*t2) - 839*t2 + 
                  25*Power(t2,2) - 8*Power(t2,3) + 
                  2*s1*(735 + 308*t2 + 30*Power(t2,2))) - 
               t1*(207 + 2416*t2 - 439*Power(t2,2) + 5*Power(t2,3) + 
                  3*s1*(9 - 324*t2 + 68*Power(t2,2))) + 
               Power(t1,2)*(1593 + 2570*t2 + 28*Power(t2,2) - 
                  11*Power(t2,3) + Power(s1,2)*(-267 + 334*t2) + 
                  s1*(490 - 2162*t2 + 99*Power(t2,2))))) + 
         Power(s2,9)*(-6 + 12*t1 - 12*Power(t1,3) + 6*Power(t1,4) - 
            2*Power(s,4)*(4 - 6*t1 + Power(t1,2)) - 
            2*Power(s1,3)*t1*(17 + 40*t1 + 15*Power(t1,2)) + 55*t2 - 
            69*t1*t2 - 15*Power(t1,2)*t2 + 17*Power(t1,3)*t2 + 
            12*Power(t1,4)*t2 - 126*Power(t2,2) + 22*t1*Power(t2,2) + 
            80*Power(t1,2)*Power(t2,2) + 22*Power(t1,3)*Power(t2,2) + 
            2*Power(t1,4)*Power(t2,2) + 57*Power(t2,3) + 
            69*t1*Power(t2,3) + 15*Power(t1,2)*Power(t2,3) + 
            3*Power(t1,3)*Power(t2,3) + 
            2*Power(s1,2)*(-21 + 10*Power(t1,4) + 11*t2 + 
               5*Power(t1,2)*(5 + 19*t2) + Power(t1,3)*(26 + 19*t2) + 
               t1*(-40 + 91*t2)) - 
            Power(s,3)*(32 - 50*Power(t1,3) + 
               4*Power(s1,2)*(-4 + 5*t1) + 15*t2 - 20*Power(t2,2) + 
               s1*(11 - 71*Power(t1,2) + t1*(50 - 40*t2) + 40*t2) + 
               Power(t1,2)*(158 + 67*t2) + 
               2*t1*(-70 - 43*t2 + 7*Power(t2,2))) - 
            s1*(39 - 154*t2 + 75*Power(t2,2) + 
               8*Power(t1,4)*(3 + 2*t2) + 
               3*Power(t1,3)*(-1 + 26*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-27 + 154*t2 + 113*Power(t2,2)) + 
               t1*(-33 - 94*t2 + 229*Power(t2,2))) - 
            Power(s,2)*(76 - 26*Power(t1,4) + 
               5*Power(s1,3)*(3 + 2*t1) - 16*t2 - 94*Power(t2,2) - 
               87*Power(t2,3) + Power(t1,3)*(174 + 59*t2) - 
               Power(t1,2)*(190 + 163*t2 + 32*Power(t2,2)) + 
               t1*(-34 + 76*t2 + 45*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s1,2)*(18 + 64*Power(t1,2) - 82*t2 - 
                  t1*(143 + 30*t2)) + 
               s1*(-233*Power(t1,3) + Power(t1,2)*(283 + 84*t2) + 
                  3*t1*(22 - 18*t2 + Power(t2,2)) + 
                  2*(-36 + 56*t2 + 75*Power(t2,2)))) + 
            s*(12 + Power(s1,3)*(7 - 82*t1 - 39*Power(t1,2)) - 106*t2 - 
               59*Power(t2,2) + 100*Power(t2,3) - 
               12*Power(t1,4)*(1 + t2) + 
               2*Power(t1,3)*(8 + 27*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(16 + 2*t2 - 14*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-32 + 62*t2 + 217*Power(t2,2) + 57*Power(t2,3)) + 
               Power(s1,2)*(-107 - 30*Power(t1,3) + 58*t2 + 
                  Power(t1,2)*(190 + 91*t2) + t1*(115 + 241*t2)) + 
               s1*(52 + 108*Power(t1,4) + 122*t2 - 137*Power(t2,2) - 
                  2*Power(t1,3)*(83 + 52*t2) - 
                  Power(t1,2)*(120 + 29*Power(t2,2)) - 
                  2*t1*(-63 + 177*t2 + 136*Power(t2,2))))) + 
         Power(s2,5)*(-4 + 7*s1 - 84*t1 - 63*s1*t1 + 20*Power(s1,2)*t1 + 
            174*Power(t1,2) - 345*s1*Power(t1,2) - 
            112*Power(s1,2)*Power(t1,2) + 10*Power(s1,3)*Power(t1,2) + 
            12*Power(t1,3) + 509*s1*Power(t1,3) - 
            460*Power(s1,2)*Power(t1,3) - 30*Power(s1,3)*Power(t1,3) - 
            180*Power(t1,4) + 200*s1*Power(t1,4) + 
            190*Power(s1,2)*Power(t1,4) - 180*Power(s1,3)*Power(t1,4) + 
            72*Power(t1,5) - 274*s1*Power(t1,5) + 
            324*Power(s1,2)*Power(t1,5) - 122*Power(s1,3)*Power(t1,5) + 
            10*Power(t1,6) - 34*s1*Power(t1,6) + 
            38*Power(s1,2)*Power(t1,6) - 14*Power(s1,3)*Power(t1,6) + 
            Power(s,8)*(4 - 28*t1 + 26*Power(t1,2)) - 5*t2 - 14*s1*t2 + 
            267*t1*t2 + 6*s1*t1*t2 - 20*Power(s1,2)*t1*t2 - 
            33*Power(t1,2)*t2 + 762*s1*Power(t1,2)*t2 - 
            18*Power(s1,2)*Power(t1,2)*t2 - 593*Power(t1,3)*t2 + 
            286*s1*Power(t1,3)*t2 + 390*Power(s1,2)*Power(t1,3)*t2 + 
            250*Power(t1,4)*t2 - 780*s1*Power(t1,4)*t2 + 
            510*Power(s1,2)*Power(t1,4)*t2 + 106*Power(t1,5)*t2 - 
            244*s1*Power(t1,5)*t2 + 138*Power(s1,2)*Power(t1,5)*t2 + 
            8*Power(t1,6)*t2 - 16*s1*Power(t1,6)*t2 + 
            8*Power(s1,2)*Power(t1,6)*t2 + 22*Power(t2,2) + 
            7*s1*Power(t2,2) - 182*t1*Power(t2,2) + 
            57*s1*t1*Power(t2,2) - 416*Power(t1,2)*Power(t2,2) - 
            187*s1*Power(t1,2)*Power(t2,2) + 
            306*Power(t1,3)*Power(t2,2) - 
            565*s1*Power(t1,3)*Power(t2,2) + 
            230*Power(t1,4)*Power(t2,2) - 
            280*s1*Power(t1,4)*Power(t2,2) + 
            40*Power(t1,5)*Power(t2,2) - 40*s1*Power(t1,5)*Power(t2,2) - 
            13*Power(t2,3) - t1*Power(t2,3) + 
            165*Power(t1,2)*Power(t2,3) + 145*Power(t1,3)*Power(t2,3) + 
            40*Power(t1,4)*Power(t2,3) + 
            Power(s,7)*(-12 - 88*Power(t1,3) + 17*t2 + 4*Power(t2,2) + 
               Power(t1,2)*(230 + 227*t2) - 
               t1*(41 + 170*t2 + 26*Power(t2,2)) + 
               s1*(-4 - 49*Power(t1,2) + t1*(30 + 8*t2))) + 
            Power(s,6)*(11 - 328*Power(t1,4) - 94*t2 - 
               4*Power(s1,2)*t1*t2 + 2*Power(t2,2) + 17*Power(t2,3) + 
               Power(t1,3)*(1277 + 493*t2) + 
               Power(t1,2)*(-1569 - 1013*t2 + 60*Power(t2,2)) + 
               t1*(467 + 652*t2 - 17*Power(t2,2) - 7*Power(t2,3)) + 
               s1*(27 - 91*Power(t1,3) - 28*Power(t1,2)*(-7 + t2) - 
                  12*t2 - 8*Power(t2,2) + 
                  t1*(-94 + 46*t2 + 11*Power(t2,2)))) + 
            Power(s,5)*(54 + 4*Power(s1,3)*Power(t1,2) - 
               204*Power(t1,5) + 224*t2 - 95*Power(t2,2) - 
               52*Power(t2,3) + 8*Power(t1,4)*(94 + 43*t2) + 
               Power(t1,3)*(-2321 + 3*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(2383 - 122*t2 - 692*Power(t2,2) + 
                  75*Power(t2,3)) + 
               t1*(-834 - 386*t2 + 809*Power(t2,2) + 119*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2 + 25*Power(t1,2) + 61*t2 - t1*(107 + 45*t2)) + 
               s1*(-81 - 14*Power(t1,4) + Power(t1,3)*(278 - 104*t2) + 
                  78*t2 + 31*Power(t2,2) + 
                  Power(t1,2)*(174 + 564*t2 + 23*Power(t2,2)) - 
                  2*t1*(46 + 291*t2 + 84*Power(t2,2)))) + 
            Power(s,3)*(327 + 4*Power(t1,7) + 
               2*Power(s1,3)*Power(t1,2)*
                (-5 - 96*t1 + 33*Power(t1,2)) - 
               8*Power(t1,6)*(-6 + t2) - 541*t2 - 174*Power(t2,2) + 
               32*Power(t2,3) + 
               Power(t1,5)*(278 - 29*t2 + 24*Power(t2,2)) + 
               2*Power(t1,4)*(-909 - 941*t2 + 83*Power(t2,2)) + 
               t1*(1617 + 2302*t2 + 390*Power(t2,2) - 50*Power(t2,3)) + 
               Power(t1,3)*(5077 + 5581*t2 + 998*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(t1,2)*(-5007 - 5399*t2 - 622*Power(t2,2) + 
                  86*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-131 - 227*Power(t1,4) + 312*t2 + 
                  Power(t1,3)*(-602 + 82*t2) - 2*t1*(691 + 303*t2) + 
                  Power(t1,2)*(2886 + 584*t2)) + 
               2*s1*(-68 + 144*Power(t1,5) + 42*Power(t1,6) + 142*t2 - 
                  Power(t2,2) + 
                  Power(t1,4)*(715 + 426*t2 - 53*Power(t2,2)) - 
                  t1*(403 + 582*t2 + 60*Power(t2,2)) + 
                  2*Power(t1,2)*(1256 + 838*t2 + 73*Power(t2,2)) - 
                  2*Power(t1,3)*(1700 + 1049*t2 + 104*Power(t2,2)))) + 
            Power(s,2)*(-210 + 64*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (140 + 46*t1 - 301*Power(t1,2) + 40*Power(t1,3)) + 
               Power(t1,6)*(3 - 80*t2) + 630*t2 - 118*Power(t2,2) + 
               19*Power(t2,3) + 
               2*Power(t1,5)*(-117 - 97*t2 + 36*Power(t2,2)) + 
               2*Power(t1,4)*
                (644 + 473*t2 + 165*Power(t2,2) + 4*Power(t2,3)) - 
               t1*(1238 - 564*t2 + 99*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(-3124 - 511*t2 + 592*Power(t2,2) + 
                  62*Power(t2,3)) + 
               Power(t1,2)*(3451 + 689*t2 - 334*Power(t2,2) + 
                  66*Power(t2,3)) + 
               Power(s1,2)*t1*
                (193 - 89*Power(t1,5) - 250*t2 + 
                  Power(t1,4)*(-226 + 42*t2) - 
                  2*Power(t1,2)*(889 + 53*t2) + t1*(221 + 150*t2) + 
                  Power(t1,3)*(2398 + 364*t2)) + 
               s1*(87 + 12*Power(t1,7) - 216*t2 + 38*Power(t2,2) + 
                  2*Power(t1,6)*(31 + 8*t2) + 
                  Power(t1,3)*(5755 + 1352*t2 - 238*Power(t2,2)) + 
                  Power(t1,5)*(770 + 232*t2 - 40*Power(t2,2)) + 
                  t1*(-28 + 466*t2 + 17*Power(t2,2)) + 
                  Power(t1,2)*(-3121 - 1152*t2 + 112*Power(t2,2)) - 
                  Power(t1,4)*(5581 + 1860*t2 + 164*Power(t2,2)))) + 
            s*(55 + 4*Power(t1,8) + 
               Power(s1,3)*Power(t1,2)*
                (-114 + 116*t1 - 79*Power(t1,2) - 106*Power(t1,3) + 
                  Power(t1,4)) - 6*Power(t1,7)*(-6 + t2) - 204*t2 + 
               81*Power(t2,2) - 12*Power(t2,3) - 
               Power(t1,6)*(119 + 40*t2) + 
               Power(t1,5)*(26 + 30*t2 + 88*Power(t2,2)) + 
               t1*(104 - 1094*t2 + 491*Power(t2,2) - 61*Power(t2,3)) + 
               Power(t1,4)*(333 + 188*t2 + 354*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-181 + 524*t2 + 250*Power(t2,2) + 
                  31*Power(t2,3)) + 
               Power(t1,3)*(-258 + 602*t2 + 472*Power(t2,2) + 
                  96*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-111 - 6*Power(t1,6) + 107*t2 + 
                  Power(t1,5)*(-59 + 4*t2) + 
                  2*Power(t1,4)*(337 + 53*t2) - 
                  2*Power(t1,2)*(7 + 73*t2) + t1*(671 + 87*t2) + 
                  Power(t1,3)*(581 + 276*t2)) + 
               s1*(-35 + 2*Power(t1,7) + 86*t2 - 29*Power(t2,2) + 
                  Power(t1,6)*(-29 + 32*t2) + 
                  t1*(368 - 86*t2 - 96*Power(t2,2)) - 
                  4*Power(t1,5)*(160 + 33*t2 + 12*Power(t2,2)) - 
                  2*Power(t1,3)*(-177 + 434*t2 + 64*Power(t2,2)) + 
                  Power(t1,2)*(185 - 1212*t2 + 165*Power(t2,2)) - 
                  Power(t1,4)*(205 + 1292*t2 + 186*Power(t2,2)))) + 
            Power(s,4)*(-225 - 16*Power(t1,6) + 
               6*Power(s1,3)*Power(t1,2)*(-5 + 4*t1) - 27*t2 + 
               278*Power(t2,2) + 9*Power(t2,3) + 
               33*Power(t1,5)*(-5 + 2*t2) + 
               Power(t1,4)*(420 + 540*t2 - 8*Power(t2,2)) + 
               t1*(-11 - 1783*t2 - 1462*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(891 + 5215*t2 + 2010*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(t1,3)*(-764 - 3801*t2 - 198*Power(t2,2) + 
                  65*Power(t2,3)) + 
               Power(s1,2)*t1*
                (27 - 89*Power(t1,3) - 206*t2 - 
                  Power(t1,2)*(617 + 26*t2) + t1*(853 + 432*t2)) + 
               s1*(135 + 126*Power(t1,5) + Power(t1,4)*(435 - 136*t2) - 
                  206*t2 - 37*Power(t2,2) + 
                  Power(t1,3)*(1003 + 1186*t2 - 29*Power(t2,2)) + 
                  t1*(685 + 1306*t2 + 299*Power(t2,2)) - 
                  Power(t1,2)*(2612 + 2894*t2 + 501*Power(t2,2))))) - 
         Power(s2,6)*(-30 - 18*t1 + 195*Power(t1,2) - 180*Power(t1,3) + 
            30*Power(t1,5) + 3*Power(t1,6) + 
            2*Power(s,7)*(5 - 12*t1 + Power(t1,2)) - 
            Power(s1,3)*t1*(-5 + 5*t1 + 180*Power(t1,2) + 
               250*Power(t1,3) + 71*Power(t1,4) + 3*Power(t1,5)) + 
            70*t2 + 330*t1*t2 - 531*Power(t1,2)*t2 - 
            165*Power(t1,3)*t2 + 255*Power(t1,4)*t2 + 
            39*Power(t1,5)*t2 + 2*Power(t1,6)*t2 - 25*Power(t2,2) - 
            466*t1*Power(t2,2) - 124*Power(t1,2)*Power(t2,2) + 
            450*Power(t1,3)*Power(t2,2) + 145*Power(t1,4)*Power(t2,2) + 
            20*Power(t1,5)*Power(t2,2) - 15*Power(t2,3) + 
            99*t1*Power(t2,3) + 255*Power(t1,2)*Power(t2,3) + 
            135*Power(t1,3)*Power(t2,3) + 30*Power(t1,4)*Power(t2,3) + 
            Power(s1,2)*(5 - 5*t2 + Power(t1,6)*(9 + 2*t2) + 
               60*Power(t1,3)*(-3 + 13*t2) - t1*(28 + 37*t2) + 
               5*Power(t1,2)*(-89 + 50*t2) + 
               Power(t1,5)*(164 + 67*t2) + 5*Power(t1,4)*(95 + 91*t2)) - 
            s1*(6 + 18*t2 - 24*Power(t2,2) + Power(t1,6)*(9 + 4*t2) + 
               Power(t1,5)*(123 + 106*t2 + 20*Power(t2,2)) + 
               t1*(258 - 404*t2 + 31*Power(t2,2)) + 
               5*Power(t1,4)*(45 + 136*t2 + 41*Power(t2,2)) + 
               15*Power(t1,3)*(-39 + 38*t2 + 45*Power(t2,2)) + 
               Power(t1,2)*(-36 - 974*t2 + 605*Power(t2,2))) + 
            Power(s,6)*(36 - 4*Power(s1,2)*t1 - 90*Power(t1,3) + 
               72*t2 + 22*Power(t2,2) + Power(t1,2)*(458 + 256*t2) - 
               t1*(337 + 328*t2 + 70*Power(t2,2)) + 
               s1*(-11 - 86*Power(t1,2) - 8*t2 + t1*(76 + 44*t2))) + 
            Power(s,5)*(-139 + 2*Power(s1,3)*t1 - 91*Power(t1,4) - 
               128*t2 + 46*Power(t2,2) + 60*Power(t2,3) + 
               Power(s1,2)*(-31*Power(t1,2) + t1*(2 - 15*t2) + 10*t2) + 
               Power(t1,3)*(648 + 395*t2) + 
               Power(t1,2)*(-1577 - 695*t2 + 31*Power(t2,2)) + 
               t1*(958 + 486*t2 - 81*Power(t2,2) - 21*Power(t2,3)) + 
               s1*(12 - 142*Power(t1,3) - 22*t2 - 51*Power(t2,2) + 
                  Power(t1,2)*(493 + 84*t2) + 
                  2*t1*(-98 - 15*t2 + 12*Power(t2,2)))) + 
            s*(129 + 18*Power(t1,7) - 
               Power(s1,3)*t1*
                (56 - 59*t1 + 116*Power(t1,2) + 301*Power(t1,3) + 
                  6*Power(t1,4)) + Power(t1,6)*(25 - 18*t2) - 494*t2 + 
               232*Power(t2,2) - 56*Power(t2,3) + 
               2*Power(t1,5)*(-144 - 53*t2 + 20*Power(t2,2)) + 
               t1*(114 - 604*t2 + 193*Power(t2,2) + 19*Power(t2,3)) + 
               Power(t1,4)*(563 + 49*t2 + 232*Power(t2,2) + 
                  24*Power(t2,3)) + 
               4*Power(t1,3)*
                (-20 + 221*t2 + 181*Power(t2,2) + 41*Power(t2,3)) + 
               Power(t1,2)*(-481 + 289*t2 + 511*Power(t2,2) + 
                  269*Power(t2,3)) + 
               Power(s1,2)*(-25 - 39*Power(t1,6) + 
                  50*Power(t1,2)*(-1 + t2) + 24*t2 + 
                  Power(t1,5)*(-44 + 40*t2) + 
                  6*Power(t1,3)*(50 + 103*t2) + t1*(245 + 107*t2) + 
                  Power(t1,4)*(1545 + 421*t2)) + 
               s1*(37 + 6*Power(t1,7) + 58*t2 - 35*Power(t2,2) + 
                  Power(t1,6)*(26 + 4*t2) + 
                  Power(t1,3)*(616 - 2324*t2 - 626*Power(t2,2)) + 
                  Power(t1,2)*(523 - 528*t2 - 404*Power(t2,2)) - 
                  4*Power(t1,5)*(24 - 12*t2 + 5*Power(t2,2)) + 
                  2*t1*(58 - 161*t2 + 14*Power(t2,2)) - 
                  Power(t1,4)*(1228 + 800*t2 + 203*Power(t2,2)))) + 
            Power(s,3)*(262 + 29*Power(t1,6) + 
               Power(s1,3)*t1*(30 - 203*t1 + 52*Power(t1,2)) + 474*t2 + 
               234*Power(t2,2) - 60*Power(t2,3) + 
               Power(t1,5)*(-263 + 19*t2) + 
               Power(t1,4)*(343 + 488*t2 + 28*Power(t2,2)) + 
               Power(t1,3)*(-772 - 3025*t2 - 197*Power(t2,2) + 
                  80*Power(t2,3)) + 
               t1*(-1061 - 2204*t2 - 820*Power(t2,2) + 
                  154*Power(t2,3)) + 
               Power(t1,2)*(1754 + 4715*t2 + 1742*Power(t2,2) + 
                  195*Power(t2,3)) + 
               Power(s1,2)*(-23 - 423*Power(t1,4) + 62*t2 + 
                  64*Power(t1,2)*(28 + 11*t2) + 
                  2*Power(t1,3)*(-54 + 55*t2) - t1*(641 + 320*t2)) + 
               s1*(-173 + 298*Power(t1,5) + 
                  Power(t1,4)*(436 - 124*t2) - 212*t2 - 
                  34*Power(t2,2) + 
                  Power(t1,3)*(66 + 1104*t2 - 86*Power(t2,2)) + 
                  2*t1*(745 + 800*t2 + 114*Power(t2,2)) - 
                  4*Power(t1,2)*(738 + 975*t2 + 212*Power(t2,2)))) + 
            Power(s,4)*(75 + 32*Power(t1,5) + 
               Power(s1,3)*t1*(-23 + 18*t1) - 114*t2 - 
               259*Power(t2,2) - 71*Power(t2,3) + 
               Power(t1,4)*(-436 + 206*t2) + 
               Power(t1,3)*(-315 + 634*t2) + 
               Power(t1,2)*(871 - 1890*t2 - 709*Power(t2,2) + 
                  100*Power(t2,3)) + 
               t1*(-344 + 1270*t2 + 1294*Power(t2,2) + 
                  215*Power(t2,3)) + 
               Power(s1,2)*(4 - 159*Power(t1,3) - 39*t2 - 
                  2*Power(t1,2)*(56 + 19*t2) + t1*(234 + 235*t2)) + 
               s1*(73 + 139*Power(t1,4) + Power(t1,3)*(810 - 60*t2) + 
                  166*t2 + 100*Power(t2,2) + 
                  Power(t1,2)*(-553 + 526*t2 + 25*Power(t2,2)) - 
                  t1*(294 + 1040*t2 + 455*Power(t2,2)))) + 
            Power(s,2)*(-407 + 2*Power(t1,7) + 
               Power(s1,3)*t1*
                (42 + 119*t1 - 470*Power(t1,2) + 44*Power(t1,3)) + 
               Power(t1,6)*(87 - 2*t2) + 184*t2 + 134*Power(t2,2) - 
               50*Power(t2,3) + 
               Power(t1,5)*(-141 - 45*t2 + 16*Power(t2,2)) + 
               Power(t1,4)*(-314 - 801*t2 + 141*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(854 + 674*t2 - 274*Power(t2,2) + 78*Power(t2,3)) + 
               Power(t1,3)*(1385 + 1786*t2 + 709*Power(t2,2) + 
                  133*Power(t2,3)) + 
               Power(t1,2)*(-1466 - 216*t2 + 629*Power(t2,2) + 
                  269*Power(t2,3)) + 
               Power(s1,2)*(39 - 283*Power(t1,5) - 52*t2 - 
                  10*Power(t1,2)*(158 + 3*t2) + 6*t1*(56 + 5*t2) + 
                  3*Power(t1,4)*(7 + 52*t2) + 
                  Power(t1,3)*(2842 + 766*t2)) + 
               s1*(68 + 111*Power(t1,5) + 101*Power(t1,6) + 36*t2 - 
                  4*Power(t2,2) + 
                  Power(t1,2)*(4151 + 1028*t2 - 472*Power(t2,2)) + 
                  Power(t1,4)*(535 + 468*t2 - 114*Power(t2,2)) + 
                  2*t1*(-677 - 408*t2 + 39*Power(t2,2)) - 
                  Power(t1,3)*(5192 + 3446*t2 + 533*Power(t2,2))))) + 
         Power(s2,8)*(19 - 42*t1 + 18*Power(t1,2) + 8*Power(t1,3) + 
            3*Power(t1,4) - 6*Power(t1,5) + 
            2*Power(s,5)*(6 - 20*t1 + 13*Power(t1,2)) + 
            2*Power(s1,3)*(-1 + 9*t1 + 70*Power(t1,2) + 
               75*Power(t1,3) + 15*Power(t1,4)) - 131*t2 + 125*t1*t2 + 
            107*Power(t1,2)*t2 - 69*Power(t1,3)*t2 - 28*Power(t1,4)*t2 - 
            4*Power(t1,5)*t2 + 195*Power(t2,2) + 142*t1*Power(t2,2) - 
            238*Power(t1,2)*Power(t2,2) - 86*Power(t1,3)*Power(t2,2) - 
            13*Power(t1,4)*Power(t2,2) - 55*Power(t2,3) - 
            169*t1*Power(t2,3) - 87*Power(t1,2)*Power(t2,3) - 
            23*Power(t1,3)*Power(t2,3) - 2*Power(t1,4)*Power(t2,3) - 
            2*Power(s1,2)*(-15 + 5*Power(t1,5) - t2 + 
               2*Power(t1,4)*(17 + 8*t2) + 
               15*Power(t1,2)*(-1 + 17*t2) + 
               5*Power(t1,3)*(20 + 27*t2) + t1*(-109 + 99*t2)) + 
            s1*(78 - 176*t2 + 40*Power(t2,2) + 4*Power(t1,5)*(4 + t2) + 
               Power(t1,4)*(31 + 66*t2 + 13*Power(t2,2)) + 
               Power(t1,3)*(41 + 318*t2 + 131*Power(t2,2)) + 
               t1*(-5 - 474*t2 + 385*Power(t2,2)) + 
               Power(t1,2)*(-161 + 262*t2 + 439*Power(t2,2))) + 
            Power(s,4)*(-65*Power(t1,3) + 2*Power(s1,2)*(-7 + 15*t1) + 
               Power(t1,2)*(90 + 38*t2) - 
               10*(6 + 6*t2 + 5*Power(t2,2)) + 
               t1*(49 + 50*t2 + 70*Power(t2,2)) - 
               s1*(-49 + Power(t1,2) - 60*t2 + 4*t1*(22 + 25*t2))) + 
            Power(s,3)*(81 + 13*Power(s1,3) - 103*Power(t1,4) - 
               141*t2 - 196*Power(t2,2) - 130*Power(t2,3) + 
               76*Power(t1,3)*(7 + t2) - 
               Power(t1,2)*(393 + 223*t2 + 3*Power(t2,2)) + 
               t1*(-127 + 229*t2 + 130*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s1,2)*(-6 + 146*Power(t1,2) - 88*t2 - 
                  5*t1*(31 + 2*t2)) + 
               s1*(-284*Power(t1,3) + Power(t1,2)*(3 - 28*t2) + 
                  t1*(348 + 20*t2 - 10*Power(t2,2)) + 
                  2*(-40 + 64*t2 + 95*Power(t2,2)))) + 
            s*(-98 + Power(s1,3)*
                (-3 - 2*t1 + 271*Power(t1,2) + 56*Power(t1,3)) + 
               4*Power(t1,5)*(-2 + t2) + 222*t2 + 119*Power(t2,2) - 
               84*Power(t2,3) + 
               Power(t1,4)*(23 - 6*t2 - 13*Power(t2,2)) + 
               t1*(203 + 215*t2 - 387*Power(t2,2) - 233*Power(t2,3)) - 
               Power(t1,3)*(15 - 99*t2 + 4*Power(t2,2) + 
                  20*Power(t2,3)) - 
               Power(t1,2)*(105 + 534*t2 + 331*Power(t2,2) + 
                  97*Power(t2,3)) + 
               Power(s1,2)*(29 + 90*Power(t1,4) - 26*t2 - 
                  4*t1*(-89 + 78*t2) - Power(t1,3)*(279 + 151*t2) - 
                  Power(t1,2)*(812 + 589*t2)) + 
               s1*(67 - 92*Power(t1,5) - 208*t2 + 62*Power(t2,2) + 
                  Power(t1,4)*(78 + 64*t2) + 
                  Power(t1,3)*(296 + 22*t2 + 66*Power(t2,2)) + 
                  2*t1*(-259 + 86*t2 + 300*Power(t2,2)) + 
                  Power(t1,2)*(169 + 1182*t2 + 462*Power(t2,2)))) + 
            Power(s,2)*(66 - 26*Power(t1,5) + 
               Power(s1,3)*(-20 + 130*t1 + 23*Power(t1,2)) + 212*t2 + 
               76*Power(t2,2) - 119*Power(t2,3) + 
               3*Power(t1,4)*(71 + 12*t2) - 
               Power(t1,3)*(86 + 211*t2 + 33*Power(t2,2)) + 
               Power(t1,2)*(-353 + 461*t2 + 242*Power(t2,2) - 
                  30*Power(t2,3)) - 
               t1*(-186 + 730*t2 + 728*Power(t2,2) + 155*Power(t2,3)) + 
               Power(s1,2)*(147 + 257*Power(t1,3) - 8*t2 - 
                  Power(t1,2)*(443 + 132*t2) - 2*t1*(172 + 203*t2)) + 
               s1*(-375*Power(t1,4) + Power(t1,3)*(271 + 164*t2) + 
                  8*Power(t1,2)*(47 - 36*t2 + 5*Power(t2,2)) + 
                  4*(-51 - 69*t2 + 23*Power(t2,2)) + 
                  t1*(164 + 1226*t2 + 585*Power(t2,2))))) + 
         Power(s2,7)*(-38 + 66*t1 + 18*Power(t1,2) - 72*Power(t1,3) + 
            18*Power(t1,4) + 6*Power(t1,5) + 2*Power(t1,6) + 
            Power(s,6)*(2 + 20*t1 - 30*Power(t1,2)) - 
            Power(s1,3)*(-1 - 5*t1 + 90*Power(t1,2) + 260*Power(t1,3) + 
               145*Power(t1,4) + 15*Power(t1,5)) + 154*t2 + 52*t1*t2 - 
            449*Power(t1,2)*t2 + 139*Power(t1,3)*t2 + 
            91*Power(t1,4)*t2 + 13*Power(t1,5)*t2 - 142*Power(t2,2) - 
            438*t1*Power(t2,2) + 244*Power(t1,2)*Power(t2,2) + 
            278*Power(t1,3)*Power(t2,2) + 54*Power(t1,4)*Power(t2,2) + 
            4*Power(t1,5)*Power(t2,2) + 15*Power(t2,3) + 
            195*t1*Power(t2,3) + 207*Power(t1,2)*Power(t2,3) + 
            75*Power(t1,3)*Power(t2,3) + 12*Power(t1,4)*Power(t2,3) + 
            Power(s1,2)*(2*Power(t1,6) - 13*t2 + 
               15*Power(t1,4)*(18 + 13*t2) + Power(t1,5)*(40 + 13*t2) + 
               3*t1*(-64 + 19*t2) + 20*Power(t1,2)*(-19 + 29*t2) + 
               20*Power(t1,3)*(13 + 34*t2)) - 
            s1*(60 + 4*Power(t1,6) - 68*t2 - 15*Power(t2,2) + 
               Power(t1,5)*(31 + 26*t2 + 4*Power(t2,2)) + 
               5*Power(t1,4)*(29 + 54*t2 + 16*Power(t2,2)) + 
               5*Power(t1,2)*(-73 - 50*t2 + 149*Power(t2,2)) + 
               t1*(202 - 732*t2 + 275*Power(t2,2)) + 
               Power(t1,3)*(-77 + 754*t2 + 423*Power(t2,2))) + 
            Power(s,5)*(Power(s1,2)*(4 - 18*t1) - 5*Power(t1,3) + 
               5*Power(t1,2)*(54 + 23*t2) + 
               2*(59 + 55*t2 + 24*Power(t2,2)) - 
               t1*(395 + 280*t2 + 98*Power(t2,2)) + 
               s1*(-31 - 80*Power(t1,2) - 36*t2 + 12*t1*(11 + 8*t2))) + 
            Power(s,4)*(-119 + 111*Power(t1,4) + 
               Power(s1,3)*(-4 + 5*t1) + 66*t2 + 154*Power(t2,2) + 
               115*Power(t2,3) + 12*Power(t1,3)*(-31 + 8*t2) + 
               Power(s1,2)*(8 - 114*Power(t1,2) + t1*(57 - 15*t2) + 
                  47*t2) - 2*Power(t1,2)*
                (141 + 33*t2 + 5*Power(t2,2)) - 
               5*t1*(-109 + 18*t2 + 30*Power(t2,2) + 7*Power(t2,3)) + 
               s1*(39 + 52*Power(t1,3) - 52*t2 - 135*Power(t2,2) + 
                  4*Power(t1,2)*(127 + 35*t2) + 
                  t1*(-436 - 100*t2 + 25*Power(t2,2)))) + 
            s*(147 + 26*Power(t1,6) - 
               Power(s1,3)*(11 - 18*t1 + 59*Power(t1,2) + 
                  404*Power(t1,3) + 34*Power(t1,4)) - 394*t2 + 
               82*Power(t2,2) - 24*Power(t2,3) + 
               Power(t1,5)*(-26 - 24*t2 + 4*Power(t2,2)) + 
               Power(t1,4)*(-168 - 203*t2 + 69*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,3)*(583 + 535*t2 + 256*Power(t2,2) + 
                  96*Power(t2,3)) + 
               t1*(-21 - 471*t2 + 81*Power(t2,2) + 241*Power(t2,3)) + 
               Power(t1,2)*(-541 + 557*t2 + 880*Power(t2,2) + 
                  281*Power(t2,3)) + 
               Power(s1,2)*(-90*Power(t1,5) + 5*(7 + 6*t2) + 
                  t1*(-82 + 92*t2) + Power(t1,4)*(133 + 117*t2) + 
                  Power(t1,2)*(-273 + 638*t2) + 
                  Power(t1,3)*(1649 + 705*t2)) + 
               s1*(-23 + 38*Power(t1,6) + Power(t1,5)*(20 - 8*t2) + 
                  36*t2 + 6*Power(t2,2) + 
                  Power(t1,2)*(1086 - 1562*t2 - 927*Power(t2,2)) + 
                  t1*(98 + 264*t2 - 304*Power(t2,2)) + 
                  Power(t1,4)*(-257 + 16*t2 - 61*Power(t2,2)) - 
                  2*Power(t1,3)*(481 + 745*t2 + 204*Power(t2,2)))) + 
            Power(s,3)*(-80 + 84*Power(t1,5) + 
               Power(s1,3)*(11 - 90*t1 + 15*Power(t1,2)) - 310*t2 - 
               230*Power(t2,2) + 16*Power(t2,3) + 
               Power(t1,4)*(-641 + 18*t2) + 
               Power(t1,3)*(360 + 566*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(278 - 1679*t2 - 543*Power(t2,2) + 
                  75*Power(t2,3)) + 
               t1*(85 + 1649*t2 + 1246*Power(t2,2) + 235*Power(t2,3)) + 
               Power(s1,2)*(-367*Power(t1,3) + 
                  29*Power(t1,2)*(9 + 2*t2) - 3*(35 + 22*t2) + 
                  t1*(449 + 402*t2)) + 
               s1*(126 + 431*Power(t1,4) + Power(t1,3)*(282 - 112*t2) + 
                  304*t2 + 66*Power(t2,2) + 
                  Power(t1,2)*(-761 + 516*t2 - 10*Power(t2,2)) - 
                  2*t1*(145 + 772*t2 + 340*Power(t2,2)))) + 
            Power(s,2)*(24 + 12*Power(t1,6) + 
               Power(s1,3)*(3 + 82*t1 - 364*Power(t1,2) + 
                  2*Power(t1,3)) + 14*t2 + 40*Power(t2,2) - 
               26*Power(t2,3) - Power(t1,5)*(48 + 7*t2) + 
               Power(t1,4)*(-172 + 123*t2 + 31*Power(t2,2)) + 
               Power(t1,3)*(263 - 1090*t2 - 116*Power(t2,2) + 
                  55*Power(t2,3)) + 
               Power(t1,2)*(438 + 2044*t2 + 965*Power(t2,2) + 
                  217*Power(t2,3)) + 
               t1*(-517 - 324*t2 + 225*Power(t2,2) + 292*Power(t2,3)) + 
               Power(s1,2)*(-395*Power(t1,4) + 2*(53 + t2) + 
                  8*t1*(-93 + 2*t2) + Power(t1,3)*(443 + 216*t2) + 
                  Power(t1,2)*(1643 + 794*t2)) + 
               s1*(-239 + 287*Power(t1,5) - 160*t2 + 16*Power(t2,2) - 
                  3*Power(t1,4)*(7 + 36*t2) + 
                  t1*(1582 + 688*t2 - 366*Power(t2,2)) + 
                  Power(t1,3)*(-202 + 534*t2 - 111*Power(t2,2)) - 
                  Power(t1,2)*(2167 + 3152*t2 + 828*Power(t2,2))))) + 
         Power(s2,4)*(2*Power(s,9)*(4 - 9*t1)*t1 + 
            4*Power(-1 + s1,2)*Power(t1,6)*(-8 + 6*s1 - 3*t2) - 
            3*t1*(-10 + 7*s1 - 7*t2)*Power(-1 + t2,2) + 
            3*Power(-1 + t2,3) + 
            Power(s,8)*t1*(-32 - 2*t1 + 35*Power(t1,2) + 
               s1*(-8 + 19*t1) + 34*t2 - 98*t1*t2 + 4*Power(t2,2)) - 
            3*Power(t1,2)*(-1 + t2)*
             (10 + 51*s1 - 10*Power(s1,2) - 75*t2 + 9*s1*t2 + 
               15*Power(t2,2)) + 
            2*(-1 + s1)*Power(t1,5)*
             (3 + 45*Power(s1,2) + 77*t2 + 20*Power(t2,2) - 
               s1*(74 + 71*t2)) - 
            Power(t1,3)*(208 + 10*Power(s1,3) - 413*t2 + 2*Power(t2,2) + 
               93*Power(t2,3) + 2*Power(s1,2)*(-84 + 19*t2) + 
               s1*(-87 + 542*t2 - 225*Power(t2,2))) + 
            Power(t1,4)*(189 + 40*Power(s1,3) + 38*t2 - 
               207*Power(t2,2) - 30*Power(t2,3) - 
               90*Power(s1,2)*(-2 + 3*t2) + 
               s1*(-461 + 306*t2 + 215*Power(t2,2))) + 
            Power(s,7)*(-6 + 331*Power(t1,4) + 
               Power(t1,3)*(-829 + 4*s1 - 306*t2) + 5*t2 - 
               2*Power(t2,3) + 
               Power(t1,2)*(480 + 651*t2 - 37*Power(t2,2) + 
                  s1*(-84 + 44*t2)) + 
               t1*(45 - 251*t2 + Power(t2,3) - 
                  2*s1*(-31 + 12*t2 + Power(t2,2)))) + 
            s*(13 + 2*(-5 + s1)*Power(t1,8) - 20*t2 + 15*Power(t2,2) - 
               4*Power(t2,3) + 
               2*Power(t1,7)*
                (-6 + 7*Power(s1,2) + s1*(10 - 4*t2) + 12*t2) + 
               Power(t1,6)*(65 + 13*Power(s1,3) + s1*(143 - 32*t2) - 
                  32*t2 - Power(s1,2)*(93 + 4*t2)) + 
               Power(t1,3)*(-209 + 116*Power(s1,3) - 1249*t2 - 
                  76*Power(t2,2) + 36*Power(t2,3) + 
                  Power(s1,2)*(-893 + 81*t2) + 
                  s1*(436 + 1134*t2 - 282*Power(t2,2))) + 
               Power(t1,4)*(-10 - 129*Power(s1,3) + 226*t2 - 
                  165*Power(t2,2) + 16*Power(t2,3) + 
                  Power(s1,2)*(127 + 208*t2) + 
                  s1*(-23 + 156*t2 - 50*Power(t2,2))) + 
               Power(t1,2)*(262 + 560*t2 - 525*Power(t2,2) + 
                  61*Power(t2,3) - 3*Power(s1,2)*(-61 + 59*t2) + 
                  6*s1*(-146 + 117*t2 + 5*Power(t2,2))) + 
               2*Power(t1,5)*
                (2 + 7*Power(s1,3) - 43*t2 - 32*Power(t2,2) - 
                  Power(s1,2)*(153 + 19*t2) + 
                  s1*(103 + 130*t2 + 12*Power(t2,2))) + 
               t1*(-103 + 577*t2 - 153*Power(t2,2) - 11*Power(t2,3) + 
                  4*s1*(23 - 69*t2 + 24*Power(t2,2)))) + 
            Power(s,6)*(33 + 366*Power(t1,5) - 54*t2 + 6*Power(t2,2) + 
               11*Power(t2,3) - Power(t1,4)*(1462 + 81*s1 + 264*t2) + 
               Power(t1,3)*(2676 - 24*Power(s1,2) + 501*t2 - 
                  9*Power(t2,2) + s1*(289 + 124*t2)) - 
               Power(t1,2)*(1609 + 1145*t2 - 12*Power(s1,2)*t2 - 
                  448*Power(t2,2) + 30*Power(t2,3) + 
                  s1*(79 + 440*t2 + 6*Power(t2,2))) + 
               t1*(115 + 772*t2 - 256*Power(t2,2) - 37*Power(t2,3) + 
                  s1*(-210 + 214*t2 + 27*Power(t2,2)))) + 
            Power(s,5)*(-71 + 114*Power(t1,6) + 182*t2 - 
               45*Power(t2,2) - 16*Power(t2,3) - 
               Power(t1,5)*(519 + 92*s1 + 74*t2) + 
               Power(t1,4)*(1212 - 81*Power(s1,2) - 114*t2 + 
                  35*Power(t2,2) + 10*s1*(49 + 12*t2)) + 
               t1*(-678 - 715*t2 + 935*Power(t2,2) + 35*Power(t2,3) + 
                  s1*(406 - 724*t2 - 76*Power(t2,2))) + 
               Power(t1,3)*(-1847 + 2005*t2 + 170*Power(t2,2) - 
                  28*Power(t2,3) + Power(s1,2)*(293 + 45*t2) + 
                  2*s1*(-985 - 475*t2 + 3*Power(t2,2))) + 
               Power(t1,2)*(1619 - 1266*t2 - 1575*Power(t2,2) + 
                  65*Power(t2,3) - 3*Power(s1,2)*(2 + 41*t2) + 
                  2*s1*(557 + 879*t2 + 69*Power(t2,2)))) + 
            Power(s,3)*(-24 - 2*(33 + 2*s1)*Power(t1,7) + 201*t2 - 
               114*Power(t2,2) + 22*Power(t2,3) + 
               Power(t1,6)*(-14 - 113*s1 + 43*Power(s1,2) + 124*t2 - 
                  24*s1*t2) + 
               Power(t1,5)*(1359 - 36*Power(s1,3) + 
                  Power(s1,2)*(537 - 16*t2) + 388*t2 - 48*Power(t2,2) + 
                  4*s1*(-340 - 71*t2 + 10*Power(t2,2))) + 
               2*Power(t1,3)*
                (4735 + 50*Power(s1,3) + 2780*t2 + 101*Power(t2,2) + 
                  8*Power(t2,3) + 9*Power(s1,2)*(75 + 31*t2) - 
                  2*s1*(1903 + 834*t2 + 37*Power(t2,2))) + 
               t1*(-936 + 2853*t2 + 202*Power(t2,2) - 41*Power(t2,3) + 
                  2*s1*(192 - 656*t2 + 39*Power(t2,2))) + 
               Power(t1,2)*(-3481 + Power(s1,2)*(255 - 564*t2) - 
                  4901*t2 - 471*Power(t2,2) + 34*Power(t2,3) + 
                  2*s1*(733 + 1380*t2 + 72*Power(t2,2))) + 
               Power(t1,4)*(63*Power(s1,3) - 
                  2*Power(s1,2)*(1153 + 114*t2) + 
                  s1*(7072 + 2180*t2 + 42*Power(t2,2)) - 
                  2*(3398 + 1569*t2 + 83*Power(t2,2)))) + 
            Power(s,4)*(71 + 4*Power(t1,7) - 279*t2 + 111*Power(t2,2) - 
               3*Power(t2,3) + Power(t1,6)*(-33 - 38*s1 + 12*t2) + 
               Power(t1,5)*(-557 + 3*Power(s1,2) + 78*t2 + 20*s1*t2 - 
                  16*Power(t2,2)) + 
               Power(t1,4)*(2900 + 819*Power(s1,2) - 14*Power(s1,3) + 
                  2276*t2 - 163*Power(t2,2) + 10*Power(t2,3) + 
                  s1*(-2590 - 810*t2 + 49*Power(t2,2))) + 
               t1*(1189 - 1123*t2 - 1092*Power(t2,2) + 55*Power(t2,3) + 
                  s1*(-487 + 1278*t2 + 53*Power(t2,2))) - 
               Power(t1,3)*(4842 + 8*Power(s1,3) + 6811*t2 + 
                  802*Power(t2,2) - 39*Power(t2,3) + 
                  Power(s1,2)*(1235 + 356*t2) - 
                  s1*(6005 + 3102*t2 + 197*Power(t2,2))) + 
               Power(t1,2)*(1318 + 5407*t2 + 1658*Power(t2,2) - 
                  69*Power(t2,3) + Power(s1,2)*(-57 + 384*t2) - 
                  s1*(2288 + 3022*t2 + 279*Power(t2,2)))) + 
            Power(s,2)*(-13 - 14*Power(t1,8) - 44*t2 + 36*Power(t2,2) - 
               11*Power(t2,3) + 
               4*Power(t1,7)*(-7 - 2*s1 + 2*Power(s1,2) + 5*t2) - 
               Power(t1,6)*(72 - 128*Power(s1,2) + 11*Power(s1,3) - 
                  80*t2 + s1*(391 + 80*t2)) + 
               Power(t1,2)*(1337 + s1*(575 - 1676*t2) + 951*t2 + 
                  232*Power(t2,2) - 16*Power(t2,3) + 
                  Power(s1,2)*(-345 + 438*t2)) + 
               t1*(362 - 2108*t2 + 372*Power(t2,2) - 23*Power(t2,3) + 
                  s1*(-218 + 802*t2 - 155*Power(t2,2))) + 
               Power(t1,3)*(-4672 - 198*Power(s1,3) + 
                  Power(s1,2)*(293 - 290*t2) - 217*t2 + 37*Power(t2,2) + 
                  30*Power(t2,3) + s1*(3221 + 756*t2 + 2*Power(t2,2))) + 
               2*Power(t1,5)*(-470 + 44*Power(s1,3) - 229*t2 - 
                  12*Power(t2,2) - Power(s1,2)*(517 + 34*t2) + 
                  s1*(1531 + 266*t2 + 12*Power(t2,2))) + 
               Power(t1,4)*(56*Power(s1,3) + Power(s1,2)*(1089 + 98*t2) + 
                  s1*(-4657 - 992*t2 + 32*Power(t2,2)) + 
                  2*(2020 + 96*t2 - 67*Power(t2,2) + 2*Power(t2,3))))))*
       R1q(s2))/(s*(-1 + s1)*(-1 + s2)*s2*
       Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2),2)*
       Power(s2 - t1,3)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-s + s1 - t2)) - (8*(8*Power(s,6)*(-1 + t1)*Power(t1,4)*
          (Power(s2,3) - 3*Power(s2,2)*t1 + 
            t1*(-1 + 2*t1 - 2*Power(t1,2)) + 
            s2*(1 - 2*t1 + 4*Power(t1,2))) - 
         Power(s2 - t1,2)*Power(-1 + t1,2)*Power(t1,2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (2*Power(s2,3)*(-1 + t1) + Power(t1,2)*(-15 + 7*s1 - 2*t2) + 
            3*(-1 + t2) - t1*(-18 + s1 + 7*t2) + 
            Power(s2,2)*(-2*Power(t1,2) + 3*s1*(1 + t1) - 7*t2 + 
               t1*(2 + t2)) + 
            s2*(-15 + s1*(1 - 10*t1 - 3*Power(t1,2)) + 4*t2 + 
               6*t1*(3 + t2) + Power(t1,2)*(-3 + 2*t2))) - 
         Power(s,5)*Power(t1,3)*
          (4*Power(s2,4)*(5 - 12*t1 + 7*Power(t1,2)) + 
            t1*(-12 + 34*Power(t1,5) + 
               Power(t1,3)*(145 + 40*s1 - 36*t2) + 
               t1*(55 + 8*s1 - 22*t2) - 
               3*Power(t1,2)*(38 + 8*s1 - 9*t2) - 
               2*Power(t1,4)*(48 + 12*s1 - 7*t2) + 5*t2) + 
            Power(s2,3)*(8 - 105*Power(t1,3) + 
               Power(t1,2)*(182 + 16*s1 - 7*t2) + 9*t2 + 
               t1*(-97 - 16*s1 + 10*t2)) + 
            Power(s2,2)*(20 + 159*Power(t1,4) + 
               3*Power(t1,2)*(89 + 16*s1 - 2*t2) + 8*t2 + 
               Power(t1,3)*(-314 - 48*s1 + 13*t2) - 3*t1*(32 + 17*t2)) + 
            s2*(-122*Power(t1,5) + Power(t1,4)*(300 + 56*s1 - 26*t2) + 
               t2 + t1*(-45 - 8*s1 + 2*t2) + 
               Power(t1,2)*(190 + 24*s1 + 15*t2) + 
               Power(t1,3)*(-359 - 72*s1 + 44*t2))) - 
         Power(s,4)*Power(t1,2)*
          (-4*Power(s2,5)*Power(-1 + t1,2)*(-4 + 9*t1) + 
            Power(s2,4)*(14 + 162*Power(t1,4) + 
               Power(t1,2)*(340 + 82*s1 - 47*t2) + 17*t2 - 
               t1*(131 + 31*s1 + t2) + 
               Power(t1,3)*(-385 - 39*s1 + 19*t2)) + 
            Power(s2,2)*(275*Power(t1,6) + t2 - 2*Power(t2,2) + 
               Power(t1,5)*(-727 - 219*s1 + 77*t2) + 
               Power(t1,4)*(1026 + 424*s1 + 24*Power(s1,2) - 167*t2 - 
                  18*s1*t2) + 
               Power(t1,2)*(315 + 84*s1 + 130*t2 + 54*s1*t2 - 
                  66*Power(t2,2)) - 
               t1*(49 + 19*s1 + 7*t2 + 6*s1*t2 - 12*Power(t2,2)) - 
               2*Power(t1,3)*
                (420 + 171*s1 + 12*Power(s1,2) - 19*t2 - 21*s1*t2 + 
                  8*Power(t2,2))) + 
            Power(s2,3)*(-287*Power(t1,5) + 
               2*Power(t1,4)*(344 + 73*s1 - 26*t2) - 
               t1*(121 + 4*s1 + 120*t2 + 12*s1*t2 - 8*Power(t2,2)) + 
               2*(7 + 6*t2 + Power(t2,2)) - 
               4*Power(t1,3)*
                (188 + 71*s1 + 2*Power(s1,2) - 26*t2 - 2*s1*t2 + 
                  Power(t2,2)) + 
               2*Power(t1,2)*
                (229 + 71*s1 + 4*Power(s1,2) + 28*t2 - 10*s1*t2 + 
                  9*Power(t2,2))) + 
            Power(t1,2)*(57 + 26*Power(t1,6) - 
               2*Power(t1,5)*(39 + 22*s1 - 9*t2) - 47*t2 + 
               6*Power(t2,2) + 
               t1*(-145 - 47*s1 + 161*t2 + 6*s1*t2 - 20*Power(t2,2)) + 
               Power(t1,4)*(97 + 130*s1 + 8*Power(s1,2) - 64*t2 - 
                  14*s1*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(120 + 160*s1 - 191*t2 - 6*s1*t2 + 
                  10*Power(t2,2)) - 
               Power(t1,3)*(77 + 235*s1 + 8*Power(s1,2) - 159*t2 - 
                  38*s1*t2 + 24*Power(t2,2))) + 
            s2*t1*(-27 - 140*Power(t1,6) + 
               4*Power(t1,5)*(105 + 39*s1 - 17*t2) + 4*t2 + 
               8*Power(t2,2) - 
               4*Power(t1,2)*
                (79 + 60*s1 - 38*t2 + 9*s1*t2 - 12*Power(t2,2)) + 
               2*t1*(57 + 33*s1 - 38*t2 - 9*Power(t2,2)) - 
               Power(t1,4)*(685 + 352*s1 + 24*Power(s1,2) - 228*t2 - 
                  24*s1*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(634 + 466*s1 + 24*Power(s1,2) - 336*t2 - 
                  60*s1*t2 + 46*Power(t2,2)))) + 
         Power(s,3)*t1*(-4*Power(s2,6)*Power(-1 + t1,3)*(-1 + 5*t1) + 
            Power(s2,5)*(-1 + t1)*
             (4 + 105*Power(t1,4) + Power(t1,2)*(139 + 69*s1 - 48*t2) + 
               7*t2 - t1*(49 + 15*s1 + 4*t2) + 
               Power(t1,3)*(-199 - 24*s1 + 15*t2)) + 
            Power(s2,4)*t1*(31 - 219*Power(t1,5) - 
               4*Power(s1,2)*t1*(2 - 9*t1 + Power(t1,2)) + 
               Power(t1,4)*(572 - 58*t2) + 46*t2 - 20*Power(t2,2) + 
               Power(t1,3)*(-637 + 207*t2 - 10*Power(t2,2)) - 
               t1*(212 + 19*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(465 - 176*t2 + 60*Power(t2,2)) + 
               s1*(8 + 119*Power(t1,4) + Power(t1,2)*(365 - 98*t2) + 
                  18*t1*(-7 + t2) + 18*t2 + 2*Power(t1,3)*(-183 + 7*t2))\
) + Power(s2,3)*(249*Power(t1,7) + 
               Power(t1,6)*(-624 - 242*s1 + 76*t2) + 
               t2*(1 + 4*t2 + 3*Power(t2,2)) + 
               Power(t1,5)*(776 + 21*Power(s1,2) + s1*(589 - 34*t2) - 
                  210*t2 + 7*Power(t2,2)) - 
               t1*(3 + 21*t2 + 31*Power(t2,2) + 7*Power(t2,3) - 
                  s1*(13 + 12*t2)) + 
               Power(t1,4)*(-745 + Power(s1,2)*(-124 + t2) + 300*t2 - 
                  113*Power(t2,2) + Power(t2,3) - 
                  2*s1*(343 - 136*t2 + Power(t2,2))) - 
               Power(t1,2)*(63 + 13*t2 - 179*Power(t2,2) + 
                  2*Power(t2,3) + Power(s1,2)*(-2 + 3*t2) - 
                  2*s1*(-50 - 66*t2 + 5*Power(t2,2))) + 
               Power(t1,3)*(410 + Power(s1,2)*(41 - 10*t2) - 133*t2 - 
                  106*Power(t2,2) - 7*Power(t2,3) + 
                  2*s1*(213 + t2 + 8*Power(t2,2)))) - 
            Power(t1,3)*(-92 + 7*Power(t1,7) + 110*t2 - 
               24*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,6)*(-31 - 30*s1 + 9*t2) + 
               Power(t1,5)*(-41 + 9*Power(s1,2) + s1*(98 - 12*t2) + 
                  7*t2 - Power(t2,2)) + 
               t1*(129 + s1*(110 - 32*t2) - 367*t2 + 87*Power(t2,2) - 
                  7*Power(t2,3)) + 
               Power(t1,4)*(412 + Power(s1,2)*(-16 + t2) + 31*t2 - 
                  33*Power(t2,2) + Power(t2,3) + 
                  s1*(-333 + 82*t2 - 2*Power(t2,2))) + 
               Power(t1,3)*(-667 + Power(s1,2)*(17 - 10*t2) - 232*t2 + 
                  72*Power(t2,2) - 7*Power(t2,3) + 
                  8*s1*(71 - 17*t2 + 2*Power(t2,2))) + 
               Power(t1,2)*(283 + Power(s1,2)*(2 - 3*t2) + 442*t2 - 
                  89*Power(t2,2) - 2*Power(t2,3) + 
                  s1*(-413 + 74*t2 + 10*Power(t2,2)))) + 
            Power(s2,2)*t1*(17 - 168*Power(t1,7) + 
               Power(t1,6)*(482 + 253*s1 - 70*t2) - 24*Power(t2,2) - 
               6*Power(t2,3) - 
               Power(t1,5)*(721 + 39*Power(s1,2) + s1*(550 - 38*t2) - 
                  208*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(-171 - 507*t2 - 315*Power(t2,2) + 
                  30*Power(t2,3) + Power(s1,2)*(-6 + 9*t2) + 
                  s1*(481 + 256*t2 - 42*Power(t2,2))) + 
               Power(t1,4)*(600 - 3*Power(s1,2)*(-52 + t2) - 707*t2 + 
                  177*Power(t2,2) + 2*Power(t2,3) + 
                  2*s1*(455 - 146*t2 + Power(t2,2))) + 
               t1*(26 + 147*t2 + 102*Power(t2,2) + 7*Power(t2,3) + 
                  4*s1*(-25 - 8*t2 + Power(t2,2))) + 
               Power(t1,3)*(-65 + 929*t2 + 114*Power(t2,2) + 
                  3*Power(t2,3) + 15*Power(s1,2)*(-5 + 2*t2) - 
                  2*s1*(497 + 21*t2 + 18*Power(t2,2)))) + 
            s2*Power(t1,2)*(-97 + 60*Power(t1,7) + 79*t2 + 
               20*Power(t2,2) + Power(t1,6)*(-221 - 136*s1 + 46*t2) + 
               Power(t1,5)*(269 + 31*Power(s1,2) + s1*(332 - 30*t2) - 
                  123*t2 + 14*Power(t2,2)) + 
               t1*(88 - 445*t2 - 26*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(197 - 12*t2 - 4*Power(t2,2))) + 
               Power(t1,4)*(248 + 3*Power(s1,2)*(-28 + t2) + 576*t2 - 
                  169*Power(t2,2) + 4*Power(t2,3) - 
                  2*s1*(419 - 100*t2 + Power(t2,2))) + 
               Power(t1,3)*(-825 + Power(s1,2)*(59 - 30*t2) - 1080*t2 + 
                  118*Power(t2,2) - 15*Power(t2,3) + 
                  s1*(1247 - 114*t2 + 36*Power(t2,2))) + 
               Power(t1,2)*(478 + Power(s1,2)*(6 - 9*t2) + 947*t2 + 
                  55*Power(t2,2) - 30*Power(t2,3) + 
                  s1*(-802 - 68*t2 + 42*Power(t2,2))))) + 
         s*(s2 - t1)*(-1 + t1)*
          (Power(s2,6)*Power(-1 + t1,2)*
             (2 + s1*(-1 + t1 + 6*Power(t1,2)) - 
               2*Power(t1,2)*(-3 + t2) + t2 - t1*(8 + 5*t2)) - 
            Power(s2,5)*(-1 + t1)*
             (3 + Power(t1,4)*(24 + 17*s1 - 5*t2) + (5 - 2*s1)*t2 + 
               2*Power(t2,2) - 
               Power(t1,3)*(61 + 16*Power(s1,2) + s1*(33 - 10*t2) + 
                  6*t2 + 2*Power(t2,2)) - 
               t1*(19 + 4*Power(s1,2) + 30*t2 + 14*Power(t2,2) - 
                  5*s1*(1 + 2*t2)) + 
               Power(t1,2)*(53 - 10*Power(s1,2) + 36*t2 - 
                  16*Power(t2,2) + s1*(11 + 42*t2))) - 
            Power(t1,3)*((7 + s1)*Power(t1,7) + t2 + 6*Power(t2,2) - 
               3*Power(t2,3) + 
               Power(t1,6)*(-30 + 2*Power(s1,2) - 9*t2 - 
                  s1*(1 + 2*t2)) + 
               Power(t1,4)*(346 - 19*Power(s1,3) - 122*t2 - 
                  16*Power(t2,2) + Power(t2,3) + 
                  18*Power(s1,2)*(4 + t2) + 
                  s1*(-185 + 54*t2 - 9*Power(t2,2))) + 
               Power(t1,5)*(-37 + Power(s1,3) + 59*t2 + Power(t2,2) - 
                  2*Power(s1,2)*(10 + t2) + 
                  s1*(21 + 2*t2 + Power(t2,2))) + 
               t1*(-125 + 70*t2 - 41*Power(t2,2) + 4*Power(t2,3) + 
                  2*s1*(22 - 18*t2 + 7*Power(t2,2))) + 
               Power(t1,2)*(460 + Power(s1,2)*(20 - 13*t2) - 174*t2 + 
                  32*Power(t2,2) + 9*Power(t2,3) - 
                  2*s1*(115 - 80*t2 + 16*Power(t2,2))) + 
               Power(t1,3)*(-621 - 4*Power(s1,3) + 175*t2 + 
                  18*Power(t2,2) + 11*Power(t2,3) + 
                  Power(s1,2)*(-74 + 63*t2) - 
                  2*s1*(-175 + 89*t2 + 20*Power(t2,2)))) + 
            Power(s2,4)*(2 + 
               Power(s1,3)*Power(t1,2)*(5 + 7*t1 + 10*Power(t1,2)) + 
               Power(t1,6)*(33 - 8*t2) + 5*t2 + 2*Power(t2,2) + 
               Power(t2,3) + 
               Power(t1,5)*(-122 + 33*t2 - 3*Power(t2,2)) + 
               Power(t1,4)*(198 + 29*t2 - 58*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(119 + 198*t2 + 66*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(36 + 71*t2 + 26*Power(t2,2) + 9*Power(t2,3)) - 
               Power(t1,3)*(194 + 186*t2 - 19*Power(t2,2) + 
                  22*Power(t2,3)) - 
               Power(s1,2)*t1*
                (2 + 51*Power(t1,4) + 4*t2 + t1*(-6 + 4*t2) + 
                  Power(t1,3)*(-20 + 9*t2) + Power(t1,2)*(-27 + 49*t2)) \
+ s1*(1 + 15*Power(t1,6) - Power(t2,2) + 3*Power(t1,5)*(-35 + 12*t2) + 
                  Power(t1,4)*(185 + 96*t2) + 
                  Power(t1,2)*(-9 - 36*t2 + 6*Power(t2,2)) + 
                  t1*(13 + 18*t2 + 9*Power(t2,2)) + 
                  2*Power(t1,3)*(-50 - 57*t2 + 26*Power(t2,2)))) + 
            Power(s2,3)*(1 - 2*Power(t1,7)*(2 + s1 - 5*t2) + 3*t2 + 
               Power(t2,2) - Power(t2,3) + 
               Power(t1,6)*(8 + 59*Power(s1,2) + s1*(116 - 42*t2) - 
                  96*t2 + 7*Power(t2,2)) + 
               t1*(-30 + s1 - 67*t2 + 16*s1*t2 - 28*Power(t2,2) + 
                  5*s1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,4)*(313 - 40*Power(s1,3) + 343*t2 + 
                  59*Power(t2,2) + 17*Power(t2,3) + 
                  2*Power(s1,2)*(11 + 85*t2) + 
                  s1*(222 + 12*t2 - 146*Power(t2,2))) + 
               Power(t1,2)*(182 + 374*t2 + 133*Power(t2,2) + 
                  4*Power(t2,3) + 5*Power(s1,2)*(3 + 2*t2) - 
                  6*s1*(11 + 19*t2 + 5*Power(t2,2))) - 
               Power(t1,5)*(98 + 29*Power(s1,3) + 
                  Power(s1,2)*(47 - 28*t2) - 77*t2 - 45*Power(t2,2) - 
                  5*Power(t2,3) + s1*(313 + 74*t2 + 6*Power(t2,2))) - 
               Power(t1,3)*(372 + 19*Power(s1,3) + 644*t2 + 
                  217*Power(t2,2) - 60*Power(t2,3) - 
                  7*Power(s1,2)*(-7 + 8*t2) + 
                  s1*(-42 - 202*t2 + 87*Power(t2,2)))) + 
            s2*Power(t1,2)*(-12 + 41*t2 - 20*Power(t2,2) + 
               3*Power(t2,3) + Power(t1,7)*(28 + 3*s1 + t2) + 
               Power(t1,6)*(-130 + 9*Power(s1,2) + s1*(10 - 4*t2) - 
                  31*t2 - 2*Power(t2,2)) + 
               Power(t1,5)*(3 - 7*Power(s1,3) + 103*t2 + 
                  18*Power(t2,2) + Power(t2,3) + 
                  Power(s1,2)*(-65 + 6*t2) + 
                  s1*(-24 + 32*t2 - 7*Power(t2,2))) + 
               Power(t1,4)*(724 - 64*Power(s1,3) + 53*t2 - 
                  70*Power(t2,2) + 9*Power(t2,3) + 
                  2*Power(s1,2)*(101 + 54*t2) - 
                  6*s1*(50 + 8*t2 + 7*Power(t2,2))) + 
               t1*(-197 - 109*t2 + 41*Power(t2,2) - 21*Power(t2,3) + 
                  s1*(82 - 38*t2 + 22*Power(t2,2))) + 
               Power(t1,2)*(882 + Power(s1,2)*(51 - 24*t2) + 265*t2 - 
                  110*Power(t2,2) + 56*Power(t2,3) - 
                  2*s1*(235 - 88*t2 + 23*Power(t2,2))) - 
               Power(t1,3)*(1298 + 17*Power(s1,3) + 
                  Power(s1,2)*(197 - 174*t2) + 323*t2 - 
                  143*Power(t2,2) - 40*Power(t2,3) + 
                  s1*(-699 + 118*t2 + 191*Power(t2,2)))) - 
            Power(s2,2)*t1*(Power(t1,7)*(32 + 4*s1 + 6*t2) + 
               2*t2*(5 + 4*t2 - 3*Power(t2,2)) + 
               Power(t1,6)*(-149 + 31*Power(s1,2) + s1*(61 - 18*t2) - 
                  85*t2 + 4*Power(t2,2)) + 
               Power(t1,4)*(524 - 78*Power(s1,3) + 490*t2 - 
                  77*Power(t2,2) + 36*Power(t2,3) + 
                  Power(s1,2)*(173 + 211*t2) + 
                  s1*(16 - 172*t2 - 127*Power(t2,2))) + 
               t1*(-133 - 142*t2 - 43*Power(t2,2) + 12*Power(t2,3) + 
                  2*s1*(20 + 7*t2 + 6*Power(t2,2))) + 
               Power(t1,5)*(61 - 27*Power(s1,3) + 84*t2 + 
                  51*Power(t2,2) - 2*Power(t2,3) + 
                  3*Power(s1,2)*(-26 + 9*t2) - 
                  2*s1*(107 - 10*t2 + 7*Power(t2,2))) + 
               Power(t1,2)*(593 + Power(s1,2)*(44 - 5*t2) + 681*t2 + 
                  33*Power(t2,2) + 20*Power(t2,3) - 
                  s1*(293 + 82*t2 + 35*Power(t2,2))) + 
               Power(t1,3)*(-27*Power(s1,3) + 
                  Power(s1,2)*(-170 + 163*t2) + 
                  s1*(386 + 238*t2 - 232*Power(t2,2)) + 
                  4*(-232 - 261*t2 + 6*Power(t2,2) + 18*Power(t2,3))))) + 
         Power(s,2)*(4*Power(s2,7)*Power(-1 + t1,4)*t1 - 
            Power(s2,6)*Power(-1 + t1,2)*
             (-2 + 24*Power(t1,4) + Power(t1,2)*(-2 + 20*s1 - 23*t2) - 
               t2 - t1*(-3 + s1 + t2) + Power(t1,3)*(-23 + 5*s1 + t2)) + 
            Power(s2,5)*(-1 + t1)*
             (59*Power(t1,6) - 2*Power(1 + t2,2) + 
               Power(t1,5)*(-59 + 2*s1 + 12*t2) - 
               2*Power(t1,4)*
                (38 - 21*s1 + 9*Power(s1,2) + 46*t2 - 3*Power(t2,2)) + 
               t1*(19 - 4*s1 + 24*t2 - 8*s1*t2 + 16*Power(t2,2)) - 
               Power(t1,2)*(49 - 34*s1 + 4*Power(s1,2) + 84*t2 + 
                  18*Power(t2,2)) - 
               2*Power(t1,3)*(-54 + 37*s1 + 15*Power(s1,2) - 72*t2 - 
                  56*s1*t2 + 27*Power(t2,2))) + 
            Power(s2,4)*(-79*Power(t1,8) + 
               Power(t1,7)*(105 + 44*s1 - 12*t2) + t2*Power(1 + t2,2) + 
               t1*(-17 + s1 - 35*t2 + 6*s1*t2 - 31*Power(t2,2) + 
                  7*s1*Power(t2,2) - 13*Power(t2,3)) + 
               Power(t1,6)*(201 + 60*Power(s1,2) + 42*t2 - 
                  8*Power(t2,2) - 7*s1*(5 + 2*t2)) - 
               Power(t1,3)*(339 + 3*Power(s1,3) + 567*t2 + 
                  239*Power(t2,2) + 10*Power(t2,3) + 
                  Power(s1,2)*(5 + 9*t2) - 5*s1*(35 + 18*t2)) + 
               Power(t1,4)*(521 - 2*Power(s1,3) + 648*t2 - 
                  58*Power(t2,2) + 23*Power(t2,3) + 
                  Power(s1,2)*(-116 + 47*t2) + 
                  s1*(-145 + 364*t2 - 53*Power(t2,2))) + 
               Power(t1,5)*(-525 - 7*Power(s1,3) - 290*t2 + 
                  178*Power(t2,2) - 3*Power(t2,3) + 
                  3*Power(s1,2)*(19 + t2) + 
                  s1*(12 - 368*t2 + 3*Power(t2,2))) + 
               Power(t1,2)*(133 + Power(s1,2)*(4 - 5*t2) + 213*t2 + 
                  156*Power(t2,2) + 14*Power(t2,3) + 
                  s1*(-52 - 78*t2 + 7*Power(t2,2)))) - 
            Power(t1,4)*(Power(t1,8) - Power(t1,7)*(19 + 5*s1 + t2) + 
               Power(t1,6)*(18 + 5*Power(s1,2) + s1*(17 - 8*t2) + 
                  35*t2 + Power(t2,2)) - 
               3*(-18 + 26*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               t1*(58 + 256*t2 - 76*Power(t2,2) + 13*Power(t2,3) + 
                  s1*(-120 + 48*t2 - 7*Power(t2,2))) + 
               Power(t1,2)*(-782 - 258*t2 + 125*Power(t2,2) - 
                  12*Power(t2,3) + Power(s1,2)*(-5 + 11*t2) + 
                  s1*(490 - 200*t2 + Power(t2,2))) + 
               Power(t1,5)*(334 + Power(s1,3) + 
                  Power(s1,2)*(13 - 3*t2) - 139*t2 - 7*Power(t2,2) - 
                  Power(t2,3) + s1*(-153 + 38*t2 + 3*Power(t2,2))) + 
               Power(t1,4)*(-1155 + 10*Power(s1,3) + 197*t2 + 
                  41*Power(t2,2) - 7*Power(t2,3) - 
                  Power(s1,2)*(52 + 21*t2) + 
                  3*s1*(175 - 48*t2 + 7*Power(t2,2))) + 
               Power(t1,3)*(1491 + Power(s1,3) + 
                  Power(s1,2)*(39 - 23*t2) - 12*t2 - 105*Power(t2,2) - 
                  2*Power(t2,3) + 2*s1*(-377 + 133*t2 + 9*Power(t2,2)))) \
+ Power(s2,3)*t1*(1 - 3*(41 + 33*s1)*Power(t1,7) + 64*Power(t1,8) - 
               4*t2 - 11*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,6)*(-33 - 67*Power(s1,2) + 140*t2 + 
                  2*Power(t2,2) + 2*s1*(-9 + 17*t2)) + 
               Power(t1,2)*(-653 - 988*t2 - 480*Power(t2,2) + 
                  18*Power(t2,3) + Power(s1,2)*(-17 + 26*t2) + 
                  s1*(320 + 318*t2 - 50*Power(t2,2))) + 
               Power(t1,5)*(496 + 22*Power(s1,3) + 228*t2 - 
                  233*Power(t2,2) - 4*Power(t2,3) - 
                  2*Power(s1,2)*(43 + 6*t2) + 
                  s1*(206 + 408*t2 - 3*Power(t2,2))) + 
               t1*(116 + 204*t2 + 143*Power(t2,2) + 28*Power(t2,3) - 
                  s1*(42 + 60*t2 + 19*Power(t2,2))) + 
               Power(t1,3)*(1271 + 10*Power(s1,3) + 1888*t2 + 
                  562*Power(t2,2) - 64*Power(t2,3) + 
                  Power(s1,2)*(30 + 4*t2) + 
                  s1*(-641 - 412*t2 + 54*Power(t2,2))) + 
               Power(t1,4)*(-1139 + 16*Power(s1,3) - 1468*t2 + 
                  17*Power(t2,2) - 22*Power(t2,3) - 
                  2*Power(s1,2)*(-70 + 81*t2) + 
                  2*s1*(137 - 144*t2 + 81*Power(t2,2)))) + 
            s2*Power(t1,3)*(98 + 9*Power(t1,8) - 134*t2 + 
               20*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,7)*(-101 - 35*s1 + 6*t2) + 
               Power(t1,6)*(197 + 9*Power(s1,2) + s1*(26 - 10*t2) + 
                  92*t2 + Power(t2,2)) + 
               Power(t1,5)*(648 + 10*Power(s1,3) - 86*t2 - 
                  105*Power(t2,2) + 4*Power(t2,3) - 
                  6*Power(s1,2)*(-3 + 2*t2) + 
                  s1*(-280 + 84*t2 + 9*Power(t2,2))) + 
               Power(t1,2)*(-1905 - 1376*t2 + 161*Power(t2,2) + 
                  6*Power(t2,3) + 19*Power(s1,2)*(-1 + 2*t2) - 
                  2*s1*(-592 + 83*t2 + 29*Power(t2,2))) + 
               Power(t1,4)*(-2743 + 32*Power(s1,3) - 574*t2 + 
                  266*Power(t2,2) - 40*Power(t2,3) - 
                  2*Power(s1,2)*(59 + 55*t2) + 
                  2*s1*(631 - 84*t2 + 45*Power(t2,2))) + 
               Power(t1,3)*(3599 + 6*Power(s1,3) + 
                  Power(s1,2)*(110 - 60*t2) + 1406*t2 - 
                  280*Power(t2,2) - 36*Power(t2,3) + 
                  s1*(-1885 + 228*t2 + 114*Power(t2,2))) + 
               t1*(s1*(-272 + 32*t2 - 11*Power(t2,2)) + 
                  3*(66 + 222*t2 - 21*Power(t2,2) + 8*Power(t2,3)))) - 
            Power(s2,2)*Power(t1,2)*
             (39 + 32*Power(t1,8) - 41*t2 - 28*Power(t2,2) + 
               Power(t1,7)*(-163 - 88*s1 + 6*t2) + 
               Power(t1,6)*(286 - 21*Power(s1,2) + 160*t2 + 
                  2*s1*(-7 + 9*t2)) + 
               Power(t1,5)*(463 + 24*Power(s1,3) + 199*t2 - 
                  231*Power(t2,2) + 4*Power(t2,3) - 
                  18*Power(s1,2)*(2 + t2) + 
                  s1*(11 + 198*t2 + 6*Power(t2,2))) + 
               t1*(259 + 529*t2 + 181*Power(t2,2) + 8*Power(t2,3) - 
                  s1*(193 + 70*t2 + 16*Power(t2,2))) + 
               Power(t1,3)*(3089 + 12*Power(s1,3) + 
                  Power(s1,2)*(100 - 42*t2) + 2882*t2 + 
                  146*Power(t2,2) - 96*Power(t2,3) + 
                  2*s1*(-817 - 184*t2 + 75*Power(t2,2))) + 
               Power(t1,4)*(-2331 + 36*Power(s1,3) - 1851*t2 + 
                  274*Power(t2,2) - 50*Power(t2,3) - 
                  4*Power(s1,2)*(4 + 51*t2) + 
                  2*s1*(476 - 30*t2 + 89*Power(t2,2))) + 
               Power(t1,2)*(3*Power(s1,2)*(-9 + 16*t2) + 
                  s1*(966 + 282*t2 - 102*Power(t2,2)) + 
                  2*(-837 - 942*t2 - 171*Power(t2,2) + 31*Power(t2,3))))))*
       R1q(t1))/(s*(-1 + s1)*(-1 + s2)*Power(s2 - t1,3)*Power(-1 + t1,3)*t1*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-s + s1 - t2)) - 
    (8*(12*Power(s,9)*Power(t1,2) + 
         2*Power(s,8)*t1*(s2*(12 - 36*t1) + 10*Power(t1,2) + 
            2*Power(t2,2) + t1*(-37 - 9*s1 + 8*t2)) + 
         Power(s,7)*(11*Power(t1,4) + 
            12*Power(s2,2)*(1 - 10*t1 + 15*Power(t1,2)) - 
            2*Power(t2,3) + Power(t1,3)*(-84 - 25*s1 + 19*t2) + 
            t1*t2*(8 - 2*(12 + s1)*t2 + Power(t2,2)) + 
            Power(t1,2)*(146 + 6*Power(s1,2) + s1*(132 - 17*t2) - 
               110*t2 + 17*Power(t2,2)) + 
            s2*(-103*Power(t1,3) + 5*Power(t1,2)*(64 + 16*s1 - 19*t2) + 
               4*Power(t2,2) + 
               2*t1*(-62 - 18*s1 + 12*t2 + 4*s1*t2 - 13*Power(t2,2)))) + 
         Power(s,6)*(Power(t1,5) - 
            48*Power(s2,3)*(1 - 5*t1 + 5*Power(t1,2)) + 
            2*Power(t2,2)*(-3 + 5*t2) + 
            Power(t1,4)*(-19 - 15*s1 + 11*t2) + 
            Power(t1,3)*(59 + 2*Power(s1,2) + s1*(147 - 10*t2) - 
               111*t2 + 7*Power(t2,2)) + 
            t1*(4 + 2*(-23 + s1)*t2 + (41 + 16*s1)*Power(t2,2) - 
               9*Power(t2,3)) + 
            Power(t1,2)*(24 + Power(s1,2)*(-56 + t2) + 298*t2 - 
               103*Power(t2,2) + 5*Power(t2,3) + 
               s1*(-347 + 129*t2 - 9*Power(t2,2))) + 
            Power(s2,2)*(-50 + 4*Power(s1,2)*t1 + 215*Power(t1,3) + 
               8*t2 - 22*Power(t2,2) + 
               s1*(-18 - 135*Power(t1,2) + t1*(116 - 44*t2) + 8*t2) + 
               18*Power(t1,2)*(-29 + 13*t2) + 
               2*t1*(182 - 61*t2 + 35*Power(t2,2))) + 
            s2*(-48*Power(t1,4) + Power(t1,3)*(210 + 94*s1 - 99*t2) + 
               t2*(8 - 2*(7 + 4*s1)*t2 + 17*Power(t2,2)) - 
               Power(t1,2)*(359 + 5*Power(s1,2) + s1*(434 - 93*t2) - 
                  442*t2 + 94*Power(t2,2)) + 
               t1*(160 - 4*Power(s1,2)*(-3 + t2) - 186*t2 + 
                  119*Power(t2,2) - 7*Power(t2,3) + 
                  s1*(236 - 68*t2 + 11*Power(t2,2))))) - 
         Power(-1 + s2,4)*(-((-1 + s1)*Power(t1,5)*(1 + s1 - 2*t2)) + 
            2*Power(s2,4)*(-1 + t1)*Power(s1 - t2,2) + 
            2*Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-19 + 4*s1 + 5*t2) + 
            Power(t1,2)*(-30 + 2*Power(s1,3) + 51*t2 - 46*Power(t2,2) + 
               9*Power(t2,3) + Power(s1,2)*(-23 + 3*t2) + 
               s1*(22 + 13*t2 - Power(t2,2))) - 
            Power(t1,4)*(Power(s1,3) + Power(s1,2)*(3 - 4*t2) + 
               (13 - 12*t2)*t2 + s1*(-14 + 11*t2 + 2*Power(t2,2))) + 
            Power(t1,3)*(12 - 5*Power(s1,3) + t2 + 11*Power(t2,2) - 
               2*Power(t2,3) + Power(s1,2)*(27 + 5*t2) - 
               s1*(32 + 12*t2 + 5*Power(t2,2))) + 
            Power(s2,3)*(-2 + Power(s1,3)*(1 + 3*t1) + 
               Power(s1,2)*(1 - 3*Power(t1,2) + t1*(2 - 5*t2) - 7*t2) + 
               7*t2 - 4*Power(t2,2) - 5*Power(t2,3) + 
               Power(t1,2)*(-2 + 7*t2 - 8*Power(t2,2)) + 
               t1*(4 - 14*t2 + 12*Power(t2,2) + Power(t2,3)) + 
               s1*(-3 + t2 + 11*Power(t2,2) + Power(t1,2)*(-3 + 9*t2) + 
                  t1*(6 - 10*t2 + Power(t2,2)))) + 
            s2*(Power(s1,3)*t1*(-4 + 11*t1 + 5*Power(t1,2)) + 
               (-15 + t2)*Power(-1 + t2,2) + 
               Power(t1,4)*(-2 + 7*t2 - 2*Power(t2,2)) + 
               t1*(29 - 33*t2 + 47*Power(t2,2) - 11*Power(t2,3)) + 
               Power(t1,3)*(3 + 15*t2 - 19*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(t1,2)*(15 + 20*t2 + 9*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s1,2)*t1*
                (46 + 3*Power(t1,3) + Power(t1,2)*(4 - 13*t2) - 6*t2 - 
                  t1*(53 + 17*t2)) + 
               s1*(4*Power(-1 + t2,2) - 3*Power(t1,4)*(1 + t2) + 
                  2*t1*(-18 - 21*t2 + 5*Power(t2,2)) + 
                  Power(t1,3)*(-22 + 20*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*(57 + 33*t2 + 17*Power(t2,2)))) - 
            Power(s2,2)*(-1 + Power(s1,3)*(-2 + 7*t1 + 7*Power(t1,2)) + 
               18*t2 + Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,3)*(-3 + 12*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(5 - 6*t2 - 3*Power(t2,2) + 5*Power(t2,3)) - 
               t1*(1 + 24*t2 - 10*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s1,2)*(23 + Power(t1,3) + 
                  Power(t1,2)*(1 - 14*t2) - 3*t2 - t1*(25 + 19*t2)) + 
               s1*(-14 - 29*t2 + 9*Power(t2,2) + 
                  Power(t1,3)*(-6 + 4*t2) + 
                  Power(t1,2)*(-2 + 3*t2 + 4*Power(t2,2)) + 
                  t1*(22 + 22*t2 + 23*Power(t2,2))))) + 
         Power(s,5)*(12*Power(s2,4)*(6 - 20*t1 + 15*Power(t1,2)) + 
            Power(t1,4)*(-62 + 3*Power(s1,2) + s1*(62 - 7*t2) - 52*t2) + 
            Power(t1,5)*(8 - 2*s1 + 2*t2) - 
            6*t2*(1 - 4*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*(88 + Power(s1,2)*(4 - 18*t1) - 
               230*Power(t1,3) - 35*t2 + 48*Power(t2,2) - 
               5*Power(t1,2)*(-72 + 61*t2) + 
               s1*(36 - 118*t1 + 105*Power(t1,2) - 36*t2 + 96*t1*t2) + 
               t1*(-301 + 250*t2 - 98*Power(t2,2))) + 
            t1*(-22 + 63*t2 + 20*Power(t2,2) + 25*Power(t2,3) + 
               s1*(4 + 10*t2 - 54*Power(t2,2))) + 
            Power(t1,3)*(278 + 3*Power(s1,3) + 275*t2 - 
               52*Power(t2,2) + 2*Power(t2,3) - 
               Power(s1,2)*(32 + 9*t2) + s1*(-307 + 90*t2 + Power(t2,2))\
) + Power(t1,2)*(-443 + 6*Power(s1,3) - 360*t2 + 200*Power(t2,2) - 
               24*Power(t2,3) - 7*Power(s1,2)*(-23 + 2*t2) + 
               s1*(308 - 339*t2 + 56*Power(t2,2))) + 
            Power(s2,2)*(26 - 2*Power(s1,3)*t1 + 82*Power(t1,4) - 
               82*t2 + 28*Power(t2,2) - 60*Power(t2,3) + 
               Power(t1,3)*(-59 + 210*t2) + 
               Power(t1,2)*(125 - 684*t2 + 207*Power(t2,2)) + 
               t1*(-92 + 498*t2 - 231*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s1,2)*(6 - 27*Power(t1,2) - 10*t2 + 
                  t1*(-8 + 15*t2)) + 
               s1*(104 - 138*Power(t1,3) + Power(t1,2)*(451 - 201*t2) - 
                  31*t2 + 51*Power(t2,2) + 
                  t1*(-479 + 246*t2 - 24*Power(t2,2)))) + 
            s2*(4 - 4*Power(t1,5) + Power(t1,4)*(-24 + 53*s1 - 51*t2) - 
               2*(13 + 5*s1)*t2 + (39 + 32*s1)*Power(t2,2) - 
               52*Power(t2,3) + 
               Power(t1,3)*(173 + 15*Power(s1,2) + 310*t2 - 
                  27*Power(t2,2) + s1*(-301 + 46*t2)) + 
               t1*(242 + 486*t2 - 128*Power(t2,2) + 56*Power(t2,3) + 
                  Power(s1,2)*(-98 + 32*t2) + 
                  s1*(-504 + 188*t2 - 87*Power(t2,2))) + 
               Power(t1,2)*(-567 - 9*Power(s1,3) - 717*t2 + 
                  354*Power(t2,2) - 30*Power(t2,3) + 
                  2*Power(s1,2)*(36 + t2) + 
                  s1*(848 - 468*t2 + 41*Power(t2,2))))) + 
         Power(s,4)*(-24*Power(s2,5)*(2 - 5*t1 + 3*Power(t1,2)) + 
            Power(t1,5)*(-35 + Power(s1,2) - 2*s1*(-2 + t2) - 6*t2) + 
            t1*(21 + (79 - 68*s1)*t2 + (-179 + 100*s1)*Power(t2,2) - 
               25*Power(t2,3)) + 
            2*(-1 + 9*t2 - 15*Power(t2,2) + 5*Power(t2,3)) + 
            Power(t1,2)*(487 - 26*Power(s1,3) + 49*t2 - 
               64*Power(t2,2) + 37*Power(t2,3) + 
               Power(s1,2)*(-157 + 43*t2) + 
               s1*(176 + 381*t2 - 133*Power(t2,2))) + 
            Power(t1,3)*(-560 - 7*Power(s1,3) - 351*t2 + 
               127*Power(t2,2) - 6*Power(t2,3) + 
               Power(s1,2)*(89 + 31*t2) + 
               s1*(187 - 256*t2 + Power(t2,2))) + 
            Power(t1,4)*(218 + Power(s1,3) + 111*t2 - 12*Power(t2,2) - 
               Power(s1,2)*(15 + 4*t2) + 
               s1*(-92 + 43*t2 + 2*Power(t2,2))) + 
            Power(s2,4)*(130*Power(t1,3) + 2*Power(s1,2)*(-7 + 15*t1) + 
               10*(6 - 5*t2)*t2 + 10*Power(t1,2)*(-3 + 22*t2) + 
               t1*(-57 - 260*t2 + 70*Power(t2,2)) + 
               s1*(-9 - 40*Power(t1,2) + 60*t2 - 4*t1*(-8 + 25*t2))) + 
            Power(s2,3)*(57 - 68*Power(t1,4) + 
               Power(s1,3)*(-4 + 5*t1) + 153*t2 + 24*Power(t2,2) + 
               115*Power(t2,3) - Power(t1,3)*(259 + 230*t2) + 
               Power(t1,2)*(326 + 509*t2 - 220*Power(t2,2)) - 
               t1*(159 + 454*t2 - 210*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s1,2)*(7 + 41*Power(t1,2) + 47*t2 - 
                  t1*(29 + 15*t2)) + 
               s1*(-112 + 112*Power(t1,3) + 39*t2 - 135*Power(t2,2) + 
                  Power(t1,2)*(-56 + 205*t2) + 
                  5*t1*(31 - 62*t2 + 5*Power(t2,2)))) - 
            s2*(12 + Power(s1,3)*t1*(-12 - 23*t1 + 18*Power(t1,2)) - 
               25*t2 + 7*Power(t2,2) - 45*Power(t2,3) + 
               8*Power(t1,5)*(4 + t2) + 
               Power(s1,2)*t1*
                (-242 + 7*Power(t1,3) + Power(t1,2)*(16 - 45*t2) + 
                  t1*(152 - 25*t2) + 94*t2) - 
               Power(t1,4)*(222 + 93*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(916 + 345*t2 - 139*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(t1,2)*(1530 + 428*t2 - 219*Power(t2,2) + 
                  80*Power(t2,3)) + 
               t1*(655 + 307*t2 + 73*Power(t2,2) + 99*Power(t2,3)) + 
               s1*(2 - 8*Power(t1,5) + Power(t1,4)*(69 - 31*t2) - 
                  36*t2 + 52*Power(t2,2) + 
                  t1*(-107 + 286*t2 - 212*Power(t2,2)) + 
                  Power(t1,3)*(-379 + 252*t2 + 9*Power(t2,2)) + 
                  3*Power(t1,2)*(172 - 179*t2 + 51*Power(t2,2)))) + 
            Power(s2,2)*(158 + 6*Power(t1,5) + 
               2*Power(s1,3)*t1*(-2 + 13*t1) + 134*t2 - 27*Power(t2,2) + 
               86*Power(t2,3) + 2*Power(t1,4)*(84 + 47*t2) + 
               Power(t1,3)*(-432 - 242*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(1222 + 490*t2 - 421*Power(t2,2) + 
                  75*Power(t2,3)) - 
               t1*(968 + 596*t2 - 28*Power(t2,2) + 145*Power(t2,3)) - 
               Power(s1,2)*(48 + 40*Power(t1,3) - 33*t2 + 
                  Power(t1,2)*(-77 + 32*t2) + t1*(17 + 95*t2)) - 
               s1*(155 + 78*Power(t1,4) - 83*t2 + 99*Power(t2,2) + 
                  Power(t1,3)*(-71 + 80*t2) + 
                  t1*(-701 + 214*t2 - 215*Power(t2,2)) + 
                  Power(t1,2)*(651 - 633*t2 + 70*Power(t2,2))))) + 
         s*Power(-1 + s2,2)*(2*Power(-1 + t2,2)*(-2 + 5*t2) - 
            2*Power(t1,5)*(-4 + s1 + 2*Power(s1,2) + 3*t2 - 4*s1*t2) - 
            t1*(-1 + t2)*(82 - 119*t2 + 21*Power(t2,2) + 
               s1*(-24 + 26*t2)) + 
            Power(t1,4)*(10 - 4*Power(s1,3) - 52*t2 + 48*Power(t2,2) + 
               Power(s1,2)*(-7 + 16*t2) + 
               s1*(42 - 55*t2 - 8*Power(t2,2))) + 
            Power(t1,2)*(-94 + 14*Power(s1,3) + 244*t2 - 
               226*Power(t2,2) + 32*Power(t2,3) + 
               Power(s1,2)*(-119 + 2*t2) + 
               s1*(58 + 71*t2 + 16*Power(t2,2))) - 
            Power(t1,3)*(2 + 17*Power(s1,3) + 3*t2 - 20*Power(t2,2) + 
               6*Power(t2,3) - 11*Power(s1,2)*(8 + t2) + 
               s1*(74 - 10*t2 + 19*Power(t2,2))) + 
            Power(s2,5)*(-8 + Power(s1,2)*(-4 + 6*t1) + 
               Power(t1,2)*(-8 + t2) - 3*t2 - 8*Power(t2,2) + 
               2*t1*(8 + t2 + 5*Power(t2,2)) + 
               s1*(9 + 5*Power(t1,2) + 12*t2 - 2*t1*(7 + 8*t2))) + 
            Power(s2,4)*(13 + Power(s1,3)*(7 + 10*t1) + 24*t2 - 
               28*Power(t2,2) - 32*Power(t2,3) + 
               4*Power(t1,3)*(4 + t2) + 
               Power(t1,2)*(-23 + 14*t2 - 31*Power(t2,2)) + 
               t1*(-6 - 42*t2 + 41*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s1,2)*(6 + Power(t1,2) - 38*t2 - 
                  t1*(25 + 21*t2)) + 
               s1*(-46 - 12*Power(t1,3) + 33*t2 + 63*Power(t2,2) + 
                  Power(t1,2)*(-12 + 37*t2) + 
                  t1*(70 - 34*t2 + 4*Power(t2,2)))) - 
            s2*(61 + Power(s1,3)*t1*
                (24 - 33*t1 + Power(t1,2) + 4*Power(t1,3)) - 146*t2 + 
               93*Power(t2,2) - 8*Power(t2,3) + 
               4*Power(t1,5)*(1 + 3*t2) + 
               Power(t1,4)*(28 + 27*t2 - 40*Power(t2,2)) + 
               Power(t1,2)*(13 - 45*t2 + 94*Power(t2,2) - 
                  18*Power(t2,3)) + 
               Power(t1,3)*(-60 - 64*t2 - 17*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-46 + 216*t2 - 268*Power(t2,2) + 42*Power(t2,3)) + 
               Power(s1,2)*t1*
                (4*Power(t1,4) - 2*(95 + 2*t2) + 
                  3*Power(t1,2)*(-43 + 6*t2) - 
                  2*Power(t1,3)*(-5 + 8*t2) + t1*(167 + 30*t2)) + 
               s1*(-4*Power(t1,5)*(1 + 2*t2) + 
                  t1*(15 + 216*t2 - 9*Power(t2,2)) + 
                  Power(t1,3)*(183 + 54*t2 + 2*Power(t2,2)) - 
                  2*(7 - 15*t2 + 8*Power(t2,2)) + 
                  Power(t1,4)*(-69 + 38*t2 + 8*Power(t2,2)) - 
                  Power(t1,2)*(111 + 54*t2 + 29*Power(t2,2)))) + 
            Power(s2,2)*(Power(s1,3)*
                (10 - 27*t1 + 33*Power(t1,2) + 14*Power(t1,3)) + 
               2*Power(t1,5)*(2 + t2) + 
               Power(t1,4)*(14 + 38*t2 - 8*Power(t2,2)) + 
               2*t2*(30 - 37*t2 + Power(t2,2)) + 
               t1*(125 - 111*t2 + 137*Power(t2,2) - 22*Power(t2,3)) + 
               Power(t1,3)*(41 + 17*t2 - 70*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(t1,2)*(184 + 6*t2 + 149*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s1,2)*(-71 + 17*Power(t1,4) + 
                  Power(t1,3)*(23 - 57*t2) - 6*t2 + 7*t1*(26 + t2) - 
                  Power(t1,2)*(315 + 34*t2)) - 
               s1*(19 + 2*Power(t1,5) - 97*t2 + Power(t2,2) + 
                  Power(t1,4)*(32 + 19*t2) + 
                  Power(t1,3)*(117 - 86*t2 - 21*Power(t2,2)) + 
                  t1*(159 + 136*t2 - 8*Power(t2,2)) - 
                  Power(t1,2)*(329 + 300*t2 + 62*Power(t2,2)))) + 
            Power(s2,3)*(-56 + 
               Power(s1,3)*(11 - 35*t1 - 20*Power(t1,2)) - 37*t2 - 
               13*Power(t2,2) + 12*Power(t2,3) - 
               Power(t1,4)*(12 + 7*t2) + 
               Power(t1,3)*(1 - 50*t2 + 33*Power(t2,2)) + 
               Power(t1,2)*(-10 + 54*t2 + 20*Power(t2,2) - 
                  30*Power(t2,3)) + 
               t1*(77 + 40*t2 + 44*Power(t2,2) + 78*Power(t2,3)) + 
               Power(s1,2)*(-103 - 20*Power(t1,3) + 12*t2 + 
                  Power(t1,2)*(20 + 62*t2) + t1*(187 + 74*t2)) + 
               s1*(110 + 9*Power(t1,4) + Power(t1,3)*(54 - 10*t2) + 
                  96*t2 - 30*Power(t2,2) - 
                  Power(t1,2)*(23 + 46*t2 + 11*Power(t2,2)) - 
                  t1*(150 + 208*t2 + 123*Power(t2,2))))) + 
         Power(s,3)*(12*Power(s2,6)*Power(-1 + t1,2) - 
            4*Power(t1,5)*(-12 + s1 + Power(s1,2) - t2 - 2*s1*t2) + 
            2*(2 - 6*t2 + 5*Power(t2,3)) + 
            Power(t1,3)*(228 - 2*Power(s1,3) + 221*t2 - 
               128*Power(t2,2) + 4*Power(t2,3) - 
               2*Power(s1,2)*(28 + 17*t2) + 
               s1*(110 + 316*t2 - 14*Power(t2,2))) + 
            Power(t1,4)*(-195 - 4*Power(s1,3) - 136*t2 + 
               48*Power(t2,2) + 4*Power(s1,2)*(5 + 4*t2) + 
               s1*(48 - 98*t2 - 8*Power(t2,2))) - 
            Power(s2,5)*(72 + 35*Power(t1,3) + 
               4*Power(s1,2)*(-4 + 5*t1) + 50*t2 - 20*Power(t2,2) + 
               s1*(9 - 18*Power(t1,2) + t1*(8 - 40*t2) + 40*t2) + 
               Power(t1,2)*(96 + 81*t2) + 
               t1*(-197 - 140*t2 + 14*Power(t2,2))) + 
            t1*(52 - 318*t2 + 312*Power(t2,2) - 5*Power(t2,3) - 
               2*s1*(14 - 66*t2 + 55*Power(t2,2))) + 
            Power(t1,2)*(-109 + 44*Power(s1,3) + 386*t2 - 
               279*Power(t2,2) - 8*Power(t2,3) - 
               4*Power(s1,2)*(12 + 13*t2) + 
               s1*(-426 - 131*t2 + 152*Power(t2,2))) + 
            Power(s2,4)*(9 + 13*Power(s1,3) + 27*Power(t1,4) - 76*t2 - 
               96*Power(t2,2) - 130*Power(t2,3) + 
               3*Power(t1,3)*(97 + 45*t2) + 
               Power(t1,2)*(-324 - 186*t2 + 95*Power(t2,2)) + 
               t1*(12 + 100*t2 - 70*Power(t2,2) + 35*Power(t2,3)) - 
               Power(s1,2)*(36 + 4*Power(t1,2) + 88*t2 + 
                  t1*(-9 + 10*t2)) + 
               s1*(-26 - 73*Power(t1,3) + 54*t2 + 190*Power(t2,2) - 
                  Power(t1,2)*(181 + 75*t2) + 
                  t1*(269 + 140*t2 - 10*Power(t2,2)))) + 
            Power(s2,2)*(Power(s1,3)*
                (6 + 21*t1 - 37*Power(t1,2) + 31*Power(t1,3)) + 
               4*Power(t1,5)*(11 + 3*t2) + 
               Power(t1,4)*(-68 + 8*t2 - 8*Power(t2,2)) - 
               2*(91 + 8*t2 + 54*Power(t2,2) + 8*Power(t2,3)) + 
               2*Power(t1,3)*
                (329 + 58*t2 - 78*Power(t2,2) + 10*Power(t2,3)) - 
               4*Power(t1,2)*
                (327 - 14*t2 + 61*Power(t2,2) + 20*Power(t2,3)) + 
               t1*(861 - 294*t2 + 306*Power(t2,2) + 82*Power(t2,3)) + 
               Power(s1,2)*(89 + 16*Power(t1,4) + 
                  Power(t1,3)*(103 - 94*t2) - 48*t2 - 
                  Power(t1,2)*(227 + 20*t2) + 2*t1*(-11 + 53*t2)) + 
               s1*(-113 - 12*Power(t1,5) - 22*t2 + 62*Power(t2,2) - 
                  2*Power(t1,4)*(26 + 27*t2) + 
                  6*Power(t1,2)*(84 + 21*t2 + 26*Power(t2,2)) + 
                  Power(t1,3)*(-370 + 268*t2 + 26*Power(t2,2)) - 
                  2*t1*(-49 + 56*t2 + 110*Power(t2,2)))) + 
            s2*(1 + 32*Power(t1,5) - 
               Power(s1,3)*t1*
                (40 + 20*t1 - 31*Power(t1,2) + 4*Power(t1,3)) + 92*t2 - 
               126*Power(t2,2) + 
               2*Power(t1,4)*(-98 - 29*t2 + 20*Power(t2,2)) + 
               2*Power(t1,3)*
                (351 + 54*t2 - 47*Power(t2,2) + 4*Power(t2,3)) - 
               Power(t1,2)*(782 + 17*t2 + 240*Power(t2,2) + 
                  36*Power(t2,3)) + 
               t1*(244 - 264*t2 + 310*Power(t2,2) + 40*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-4*Power(t1,4) + Power(t1,2)*(20 - 64*t2) + 
                  t1*(113 - 32*t2) + 4*Power(t1,3)*(1 + 4*t2) + 
                  4*(-35 + 31*t2)) + 
               s1*(10 - 56*t2 + 8*Power(t1,5)*t2 + 48*Power(t2,2) + 
                  t1*(667 + 72*t2 - 198*Power(t2,2)) + 
                  Power(t1,4)*(58 - 88*t2 - 8*Power(t2,2)) + 
                  2*Power(t1,3)*(-47 + 94*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(-347 - 44*t2 + 138*Power(t2,2)))) + 
            Power(s2,3)*(-4*Power(t1,5) + 
               Power(s1,3)*(1 - 9*t1 - 31*Power(t1,2)) - 
               2*Power(t1,4)*(100 + 43*t2) + 
               2*Power(t1,3)*(80 - 18*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-629 + 50*t2 + 204*Power(t2,2) - 
                  100*Power(t2,3)) - 
               2*(128 + 17*t2 + 5*Power(t2,2) + 12*Power(t2,3)) + 
               t1*(886 + 236*t2 + 228*Power(t2,2) + 200*Power(t2,3)) + 
               Power(s1,2)*(-49 + 9*Power(t1,3) - 40*t2 + 
                  2*Power(t1,2)*(-75 + 44*t2) + t1*(361 + 132*t2)) + 
               s1*(66*Power(t1,4) + Power(t1,3)*(239 + 60*t2) + 
                  10*Power(t1,2)*(22 - 42*t2 + 5*Power(t2,2)) + 
                  2*(67 + 8*t2 + 26*Power(t2,2)) - 
                  2*t1*(349 + 144*t2 + 155*Power(t2,2))))) + 
         Power(s,2)*(-118*t1 + 44*s1*t1 + 21*Power(t1,2) + 
            139*s1*Power(t1,2) + 190*Power(s1,2)*Power(t1,2) - 
            36*Power(s1,3)*Power(t1,2) + 97*Power(t1,3) - 
            70*s1*Power(t1,3) - 64*Power(s1,2)*Power(t1,3) + 
            18*Power(s1,3)*Power(t1,3) + 13*Power(t1,4) - 
            31*s1*Power(t1,4) - 4*Power(s1,2)*Power(t1,4) + 
            6*Power(s1,3)*Power(t1,4) - 29*Power(t1,5) + 
            4*s1*Power(t1,5) + 6*Power(s1,2)*Power(t1,5) - 12*t2 + 
            372*t1*t2 - 118*s1*t1*t2 - 472*Power(t1,2)*t2 - 
            81*s1*Power(t1,2)*t2 + 23*Power(s1,2)*Power(t1,2)*t2 - 
            49*Power(t1,3)*t2 - 162*s1*Power(t1,3)*t2 + 
            6*Power(s1,2)*Power(t1,3)*t2 + 105*Power(t1,4)*t2 + 
            106*s1*Power(t1,4)*t2 - 24*Power(s1,2)*Power(t1,4)*t2 + 
            4*Power(t1,5)*t2 - 12*s1*Power(t1,5)*t2 + 30*Power(t2,2) - 
            285*t1*Power(t2,2) + 72*s1*t1*Power(t2,2) + 
            409*Power(t1,2)*Power(t2,2) - 83*s1*Power(t1,2)*Power(t2,2) + 
            37*Power(t1,3)*Power(t2,2) + 26*s1*Power(t1,3)*Power(t2,2) - 
            72*Power(t1,4)*Power(t2,2) + 12*s1*Power(t1,4)*Power(t2,2) - 
            18*Power(t2,3) + 29*t1*Power(t2,3) - 
            33*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            Power(s2,6)*(42 - 4*Power(s1,2) + 3*Power(t1,3) + 20*t2 + 
               6*Power(t2,2) + 10*Power(t1,2)*(5 + t2) - 
               t1*(95 + 34*t2 + 14*Power(t2,2)) + 
               s1*(-9 - 15*Power(t1,2) + 4*t1*(7 + 3*t2))) + 
            Power(s2,5)*(-91 - 4*Power(t1,4) - 
               5*Power(s1,3)*(3 + 2*t1) - 26*t2 + 74*Power(t2,2) + 
               87*Power(t2,3) - Power(t1,3)*(115 + 39*t2) + 
               Power(t1,2)*(91 + 24*t2 + 18*Power(t2,2)) + 
               t1*(121 + 66*t2 - 21*Power(t2,2) - 21*Power(t2,3)) + 
               Power(s1,2)*(18 - 15*Power(t1,2) + 82*t2 + 
                  t1*(47 + 30*t2)) + 
               s1*(95 + 42*Power(t1,3) + Power(t1,2)*(107 - 33*t2) - 
                  86*t2 - 150*Power(t2,2) - 3*t1*(91 + Power(t2,2)))) + 
            s2*(53 + Power(s1,3)*t1*
                (48 + 4*t1 - 21*Power(t1,2) + 4*Power(t1,3)) + 
               2*Power(s1,2)*t1*
                (-80 + 2*Power(t1,4) + Power(t1,3)*(3 - 8*t2) + 
                  5*t1*(-6 + t2) + 13*Power(t1,2)*(-2 + t2) - 34*t2) - 
               202*t2 + 156*Power(t2,2) - 5*Power(t2,3) + 
               4*Power(t1,5)*(5 + 4*t2) - 
               2*Power(t1,4)*(18 + 17*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(-208 + 226*t2 + 16*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,3)*(-15 - t2 - 58*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(236 + 200*t2 - 259*Power(t2,2) + 19*Power(t2,3)) - 
               s1*(18 - 52*t2 + 8*Power(t1,5)*t2 + 32*Power(t2,2) + 
                  t1*(523 - 328*t2 - 27*Power(t2,2)) + 
                  Power(t1,4)*(30 - 26*t2 - 8*Power(t2,2)) + 
                  Power(t1,2)*(-319 + 49*t2 + 2*Power(t2,2)) + 
                  Power(t1,3)*(61 - 160*t2 + 6*Power(t2,2)))) + 
            Power(s2,2)*(-101 + 
               Power(s1,3)*(-14 - 27*t1 + 31*Power(t1,2) - 
                  4*Power(t1,3) + 6*Power(t1,4)) - 80*t2 + 
               72*Power(t2,2) + 4*Power(t2,3) + 
               4*Power(t1,5)*(4 + 5*t2) + 
               Power(t1,4)*(72 + 80*t2 - 64*Power(t2,2)) + 
               4*Power(t1,3)*t2*(1 - 22*t2 + 3*Power(t2,2)) - 
               2*Power(t1,2)*
                (15 + 212*t2 - 92*Power(t2,2) + 17*Power(t2,3)) + 
               t1*(-13 + 84*t2 - 322*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s1,2)*(1 + 6*Power(t1,5) + 
                  Power(t1,2)*(2 - 20*t2) - 24*Power(t1,4)*(-1 + t2) + 
                  34*t2 - 3*Power(t1,3)*(21 + 2*t2) + 2*t1*(5 + 9*t2)) + 
               s1*(299 - 134*t2 - 6*Power(t2,2) - 
                  12*Power(t1,5)*(1 + t2) + 
                  t1*(-141 + 356*t2 - 22*Power(t2,2)) + 
                  2*Power(t1,4)*(-93 + 23*t2 + 6*Power(t2,2)) + 
                  2*Power(t1,3)*(130 + 62*t2 + 7*Power(t2,2)) + 
                  Power(t1,2)*(220 - 222*t2 + 8*Power(t2,2)))) - 
            Power(s2,3)*(Power(s1,3)*
                (-13 + 28*t1 + 26*Power(t1,2) + 25*Power(t1,3)) + 
               8*Power(t1,5)*(3 + t2) + 
               Power(t1,4)*(100 + 94*t2 - 12*Power(t2,2)) - 
               Power(t1,2)*(439 + 102*t2 + 414*Power(t2,2)) + 
               t1*(143 - 494*t2 + 192*Power(t2,2) - 98*Power(t2,3)) - 
               2*(14 - 74*t2 + 85*Power(t2,2) + 3*Power(t2,3)) + 
               2*Power(t1,3)*
                (86 + 52*t2 - 77*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s1,2)*(26*Power(t1,4) + 2*(-8 + t2) - 
                  t1*(35 + 38*t2) - 2*Power(t1,3)*(-55 + 51*t2) - 
                  Power(t1,2)*(441 + 94*t2)) + 
               s1*(-45 - 8*Power(t1,5) + 166*t2 + 26*Power(t2,2) - 
                  2*Power(t1,4)*(53 + 23*t2) + 
                  Power(t1,3)*(-473 + 176*t2 + 34*Power(t2,2)) + 
                  2*Power(t1,2)*(450 + 327*t2 + 55*Power(t2,2)) + 
                  t1*(38 - 80*t2 + 88*Power(t2,2)))) + 
            Power(s2,4)*(197 + Power(t1,5) + 
               Power(s1,3)*t1*(49 + 27*t1) + 32*t2 - 28*Power(t2,2) - 
               74*Power(t2,3) + Power(t1,4)*(87 + 39*t2) + 
               Power(t1,3)*(74 + 125*t2 - 45*Power(t2,2)) + 
               Power(t1,2)*(-11 - 170*t2 - 81*Power(t2,2) + 
                  75*Power(t2,3)) - 
               t1*(356 + 126*t2 + 187*Power(t2,2) + 155*Power(t2,3)) + 
               Power(s1,2)*(129 + 33*Power(t1,3) + 
                  Power(t1,2)*(50 - 107*t2) - 50*t2 - 2*t1*(230 + 73*t2)) \
- s1*(300 + 35*Power(t1,4) + 82*t2 - 118*Power(t2,2) + 
                  2*Power(t1,3)*(114 + 5*t2) + 
                  Power(t1,2)*(62 - 207*t2 + 5*Power(t2,2)) - 
                  t1*(743 + 462*t2 + 270*Power(t2,2))))))*R2q(s))/
     (s*(-1 + s1)*(-1 + s2)*Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2),2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-s + s1 - t2)) + (8*(2*Power(s,12)*Power(t1,2)*(3*t1 - t2) - 
         Power(-1 + s2,6)*(s2 - t1)*(-1 + t1)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         2*Power(s,11)*t1*(7*Power(t1,3) + t2*(-2*s2 + t2) - 
            Power(t1,2)*(26 + 4*s1 + 24*s2 + t2) + 
            t1*(-1 + (7 + s1)*t2 + s2*(10 - s1 + 9*t2))) + 
         Power(s,10)*(13*Power(t1,5) - Power(t2,3) - 
            Power(t1,4)*(105 + 15*s1 + 4*t2) + 
            t1*t2*(4 - (16 + s1)*t2 + Power(t2,2)) + 
            Power(t1,3)*(152 + 2*Power(s1,2) + t2 + 3*Power(t2,2) + 
               s1*(80 + t2)) + 
            2*Power(s2,2)*(84*Power(t1,3) + 
               Power(t1,2)*(-70 + 8*s1 - 36*t2) - t2 + 
               t1*(11 - 2*s1 + 16*t2)) + 
            Power(t1,2)*(16 - 23*t2 + 3*Power(t2,2) - s1*(2 + 19*t2)) + 
            s2*(-99*Power(t1,4) + 2*Power(t2,2) + 
               Power(t1,3)*(354 + 42*s1 + 17*t2) + 
               t1*(-4 + 4*(5 + 2*s1)*t2 - 19*Power(t2,2)) + 
               Power(t1,2)*(-138 + 2*Power(s1,2) - 85*t2 - 
                  2*s1*(6 + 7*t2)))) + 
         Power(s,9)*(4*Power(t1,6) - Power(t1,5)*(88 + 14*s1 + t2) + 
            Power(t2,2)*(-3 + 7*t2) + 
            t1*(2 + (-31 + s1)*t2 + 2*(24 + 5*s1)*Power(t2,2) - 
               11*Power(t2,3)) + 
            Power(t1,4)*(238 + 15*t2 + 3*s1*(45 + 2*t2)) - 
            2*Power(s2,3)*(-4 + 168*Power(t1,3) + 
               s1*(1 - 14*t1 + 28*Power(t1,2)) - 7*t2 - 
               42*Power(t1,2)*(5 + 2*t2) + t1*(66 + 56*t2)) + 
            Power(t1,2)*(-42 - 70*t2 - 19*Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(2 + t2) + s1*(8 + 71*t2)) + 
            Power(t1,3)*(-39 + Power(s1,2)*(-26 + t2) + 86*t2 - 
               24*Power(t2,2) + Power(t2,3) - 
               s1*(324 + 9*t2 + 2*Power(t2,2))) + 
            Power(s2,2)*(-2 + 301*Power(t1,4) + 6*(1 + s1)*t2 - 
               17*Power(t2,2) - Power(t1,3)*(1023 + 71*s1 + 64*t2) + 
               2*t1*(-59 + 3*Power(s1,2) - 41*t2 + 39*Power(t2,2) - 
                  s1*(4 + 29*t2)) + 
               Power(t1,2)*(749 - 12*Power(s1,2) + 202*t2 + 
                  s1*(41 + 40*t2))) - 
            s2*(79*Power(t1,5) - Power(t1,4)*(562 + 69*s1 + 28*t2) + 
               t2*(-4 + (11 + 4*s1)*t2 - 12*Power(t2,2)) + 
               t1*(-24 + (25 + 59*s1 + 2*Power(s1,2))*t2 - 
                  (107 + 10*s1)*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(832 - 5*Power(s1,2) - 2*t2 + 
                  23*Power(t2,2) + s1*(371 + 3*t2)) + 
               Power(t1,2)*(-275 + 40*t2 + 14*Power(t2,2) + 
                  Power(s1,2)*(15 + 2*t2) - s1*(233 + 107*t2)))) + 
         Power(s,8)*(-4*(6 + s1)*Power(t1,6) + 
            t2*(-3 + 18*t2 - 20*Power(t2,2)) + 
            Power(t1,5)*(157 + Power(s1,2) - 6*t2 + s1*(118 + t2)) + 
            2*Power(s2,4)*(-20 + 210*Power(t1,3) + 
               s1*(6 - 42*t1 + 56*Power(t1,2)) - 21*t2 - 
               14*Power(t1,2)*(25 + 9*t2) + t1*(165 + 112*t2)) + 
            t1*(-15 + 83*t2 - 58*Power(t2,2) + 45*Power(t2,3) + 
               s1*(2 + 3*t2 - 44*Power(t2,2))) + 
            Power(t1,4)*(139 + Power(s1,3) + 55*t2 + 2*Power(t2,2) - 
               2*Power(s1,2)*(6 + t2) + s1*(-476 - 58*t2 + Power(t2,2))) \
+ Power(t1,2)*(-8 + 370*t2 + 29*Power(t2,2) - 10*Power(t2,3) - 
               2*Power(s1,2)*(9 + 4*t2) + 
               2*s1*(11 - 60*t2 + 3*Power(t2,2))) + 
            Power(t1,3)*(-776 + 2*Power(s1,3) + 
               Power(s1,2)*(142 - 8*t2) - 421*t2 + 74*Power(t2,2) - 
               8*Power(t2,3) + s1*(573 + 19*t2 + 20*Power(t2,2))) + 
            Power(s2,3)*(-32 - 511*Power(t1,4) + 
               Power(s1,2)*(4 - 35*t1 + 28*Power(t1,2)) - 7*t2 + 
               61*Power(t2,2) + 2*Power(t1,3)*(807 + 70*t2) - 
               2*Power(t1,2)*(782 + 105*t2) + 
               t1*(497 + 71*t2 - 180*Power(t2,2)) - 
               s1*(4 + 7*Power(t1,3) + 40*t2 + 
                  Power(t1,2)*(5 + 56*t2) - 2*t1*(13 + 89*t2))) - 
            Power(s2,2)*(-8 - 201*Power(t1,5) + 
               Power(s1,3)*t1*(1 + 2*t1) + 7*t2 - 48*Power(t2,2) + 
               62*Power(t2,3) + 7*Power(t1,4)*(173 + 12*t2) + 
               Power(t1,2)*(924 - 607*t2 - 7*Power(t2,2)) - 
               2*Power(t1,3)*(841 - 20*t2 + 38*Power(t2,2)) + 
               t1*(-147 + 224*t2 + 260*Power(t2,2) - 45*Power(t2,3)) + 
               Power(s1,2)*(63*Power(t1,3) + t1*(41 - 11*t2) + 5*t2 - 
                  Power(t1,2)*(125 + 14*t2)) + 
               s1*(-2 + 101*Power(t1,4) + 6*Power(t1,3)*(-101 + t2) + 
                  30*t2 - 39*Power(t2,2) + Power(t1,2)*(889 + 261*t2) + 
                  t1*(-237 - 292*t2 + 44*Power(t2,2)))) - 
            s2*(-2 + 20*Power(t1,6) + (21 + 5*s1)*t2 - 
               2*(23 + 12*s1)*Power(t2,2) + 62*Power(t2,3) - 
               Power(t1,5)*(374 + 57*s1 + 5*t2) + 
               Power(t1,4)*(786 - 25*Power(s1,2) + 66*t2 - 
                  Power(t2,2) + s1*(492 + 29*t2)) + 
               Power(t1,2)*(-295 - 3*Power(s1,3) + 
                  Power(s1,2)*(8 - 7*t2) - 950*t2 - 12*Power(t2,2) + 
                  6*Power(t2,3) + s1*(963 + 265*t2 + 6*Power(t2,2))) + 
               Power(t1,3)*(138 + 3*Power(s1,3) + 507*t2 - 
                  128*Power(t2,2) + 8*Power(t2,3) + 
                  Power(s1,2)*(29 + 10*t2) - 
                  s1*(1401 + 65*t2 + 14*Power(t2,2))) + 
               t1*(41 + 67*t2 + 156*Power(t2,2) - 86*Power(t2,3) - 
                  Power(s1,2)*(5 + 21*t2) + 
                  s1*(15 - 128*t2 + 82*Power(t2,2))))) - 
         s*Power(-1 + s2,4)*(-2*Power(-1 + s1,2)*Power(t1,6) + 
            2*Power(s2,6)*(-1 + t1)*Power(s1 - t2,2) + 
            Power(-1 + t2,3) - 
            t1*Power(-1 + t2,2)*(-15 + 2*s1 + 11*t2) - 
            (-1 + s1)*Power(t1,5)*
             (7 - 11*s1 + 6*Power(s1,2) + 7*t2 - 3*s1*t2) + 
            Power(t1,2)*(-1 + t2)*
             (40 - Power(s1,2) - 49*t2 + 10*Power(t2,2) + 
               3*s1*(-7 + 9*t2)) + 
            Power(t1,3)*(42 + 2*Power(s1,3) + Power(s1,2)*(5 - 17*t2) - 
               60*t2 + 13*Power(t2,2) + 3*Power(t2,3) + 
               s1*(-46 + 86*t2 - 28*Power(t2,2))) + 
            Power(t1,4)*(-21 + Power(s1,3) + 2*t2 + 12*Power(t2,2) + 
               3*Power(s1,2)*(-7 + 8*t2) + 
               s1*(41 - 32*t2 - 6*Power(t2,2))) + 
            Power(s2,5)*(-3*Power(s1,3)*(-2 + t1) - 
               Power(s1,2)*(-3 + t1 + 2*Power(t1,2) + 25*t2 - 
                  16*t1*t2) + 
               s1*(-1 + Power(t1,3)*(-1 + t2) + t2 + 32*Power(t2,2) + 
                  Power(t1,2)*(1 + 9*t2) + 
                  t1*(1 - 11*t2 - 23*Power(t2,2))) + 
               t2*(1 - Power(t1,3)*(-1 + t2) - 4*t2 - 13*Power(t2,2) - 
                  Power(t1,2)*(1 + 7*t2) + 
                  t1*(-1 + 12*t2 + 10*Power(t2,2)))) + 
            Power(s2,2)*(3*Power(s1,3)*t1*
                (2 + 7*t1 - 24*Power(t1,2) + 5*Power(t1,3)) - 
               Power(t1,5)*(3 + t2) + 
               t2*(-15 + 29*t2 - 14*Power(t2,2)) + 
               Power(t1,4)*(-2 - 12*t2 + 11*Power(t2,2)) + 
               Power(t1,3)*(24 + 22*t2 - 6*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,2)*(-30 - 19*t2 + 34*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(11 + 25*t2 - 68*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s1,2)*(1 - 57*Power(t1,4) + t1*(7 - 43*t2) + 
                  Power(t1,5)*(-11 + t2) - t2 + 
                  14*Power(t1,3)*(13 + 4*t2) + 
                  Power(t1,2)*(-122 + 77*t2)) + 
               s1*(14*Power(t1,5) + 
                  Power(t1,4)*(31 + 20*t2 - 6*Power(t2,2)) + 
                  6*(1 - 3*t2 + 2*Power(t2,2)) - 
                  4*Power(t1,3)*(37 + 11*t2 + 5*Power(t2,2)) + 
                  t1*(-56 + 84*t2 + 8*Power(t2,2)) - 
                  3*Power(t1,2)*(-51 + 14*t2 + 28*Power(t2,2)))) + 
            Power(s2,4)*(3 + Power(s1,3)*(6 - 34*t1 + 13*Power(t1,2)) - 
               10*t2 - 16*Power(t2,2) + 29*Power(t2,3) + 
               Power(t1,3)*t2*(2 + 9*t2 - Power(t2,2)) + 
               Power(t1,4)*(-1 + Power(t2,2)) + 
               t1*(-8 + 22*t2 + 37*Power(t2,2) - 7*Power(t2,3)) - 
               Power(t1,2)*(-6 + 14*t2 + 31*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s1,2)*(-31 + Power(t1,3)*(-9 + t2) + 13*t2 - 
                  Power(t1,2)*(7 + 40*t2) + t1*(47 + 71*t2)) + 
               s1*(-2 + Power(t1,3)*(4 - 6*t2) - 
                  2*Power(t1,4)*(-1 + t2) + 56*t2 - 48*Power(t2,2) - 
                  2*t1*(-6 + 55*t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*(-16 + 62*t2 + 33*Power(t2,2)))) + 
            s2*(2*Power(t1,6) - 
               Power(s1,3)*Power(t1,2)*
                (6 + 9*t1 - 34*Power(t1,2) + 4*Power(t1,3)) - 
               5*Power(t1,5)*(-1 + t2) + 
               Power(-1 + t2,2)*(-11 + 7*t2) + 
               Power(t1,4)*(-28 + 13*t2 - 23*Power(t2,2)) + 
               Power(t1,2)*(-47 + 28*t2 + 49*Power(t2,2) - 
                  24*Power(t2,3)) + 
               2*t1*(17 - 28*t2 + 6*Power(t2,2) + 5*Power(t2,3)) - 
               Power(t1,3)*(-45 + 9*t2 + 13*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2*Power(t1,5) + Power(t1,2)*(77 - 68*t2) - 
                  4*Power(t1,4)*(-6 + t2) + 2*(-1 + t2) - 
                  2*Power(t1,3)*(45 + 11*t2) + t1*(-11 + 47*t2)) + 
               s1*(-4*Power(t1,6) + 2*Power(-1 + t2,2) + 
                  Power(t1,5)*(-25 + 9*t2) + 
                  Power(t1,4)*(91 + 13*t2 + 12*Power(t2,2)) - 
                  3*t1*(9 - 22*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*(77 - 131*t2 + 18*Power(t2,2)) + 
                  Power(t1,3)*(-114 + 47*t2 + 52*Power(t2,2)))) - 
            Power(s2,3)*(1 + Power(s1,3)*
                (2 + 19*t1 - 72*Power(t1,2) + 21*Power(t1,3)) + 
               Power(t1,5)*(-1 + t2) + 8*t2 - 21*Power(t2,2) + 
               10*Power(t2,3) + Power(t1,4)*(-2 + 3*t2 + Power(t2,2)) + 
               Power(t1,3)*(11 - 44*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*(11 - 83*t2 + 14*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(2 - 51*t2 + 32*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s1,2)*(1 + 2*Power(t1,4)*(-9 + t2) - 13*t2 - 
                  Power(t1,3)*(45 + 28*t2) + t1*(-97 + 46*t2) + 
                  Power(t1,2)*(159 + 83*t2)) - 
               s1*(25 + Power(t1,4)*(-15 + t2) + Power(t1,5)*(-1 + t2) - 
                  39*t2 + 2*Power(t2,2) + 
                  Power(t1,3)*(5 - 84*t2 - 4*Power(t2,2)) + 
                  2*Power(t1,2)*(32 + 75*t2 + 3*Power(t2,2)) + 
                  t1*(-78 - 29*t2 + 86*Power(t2,2))))) + 
         Power(s,6)*(4 + 2*(-3 + s1)*Power(t1,7) - 27*t2 + 
            42*Power(t2,2) - 14*Power(t2,3) - 
            2*Power(t1,6)*(-63 + 2*Power(s1,2) - 13*t2 + s1*(51 + t2)) + 
            Power(t1,5)*(-Power(s1,3) + Power(s1,2)*(80 + 3*t2) + 
               s1*(434 + 59*t2) - 4*(271 + 81*t2)) + 
            2*Power(s2,6)*(84*Power(t1,3) + 
               s1*(20 - 70*t1 + 56*Power(t1,2)) - 
               42*Power(t1,2)*(5 + 2*t2) - 5*(8 + 7*t2) + 
               t1*(165 + 112*t2)) + 
            t1*(-10 - 166*t2 + 255*Power(t2,2) + 84*Power(t2,3) + 
               s1*(-12 + 139*t2 - 182*Power(t2,2))) + 
            Power(t1,4)*(2568 + 11*Power(s1,3) + 890*t2 + 
               54*Power(t2,2) - Power(s1,2)*(374 + 41*t2) - 
               s1*(-314 + 233*t2 + Power(t2,2))) + 
            Power(t1,3)*(-1297 + 52*Power(s1,3) + 
               Power(s1,2)*(649 - 77*t2) - 894*t2 + 11*Power(t2,2) - 
               32*Power(t2,3) + s1*(-1873 - 53*t2 + 127*Power(t2,2))) + 
            Power(t1,2)*(-773 + 1095*t2 - 319*Power(t2,2) - 
               55*Power(t2,3) - 2*Power(s1,2)*(47 + 28*t2) + 
               s1*(396 + 20*t2 + 147*Power(t2,2))) + 
            Power(s2,5)*(-46 + Power(s1,2)*(43 - 93*t1) - 
               329*Power(t1,4) + 196*Power(t1,2)*(-3 + t2) + 165*t2 + 
               133*Power(t2,2) + 2*Power(t1,3)*(366 + 91*t2) - 
               5*t1*(-48 + 95*t2 + 42*Power(t2,2)) + 
               s1*(-41 - 287*Power(t1,3) - 156*t2 + 
                  Power(t1,2)*(275 + 28*t2) + t1*(15 + 266*t2))) + 
            Power(s2,3)*(166 - 40*Power(t1,6) + 
               2*Power(s1,3)*
                (6 - 35*t1 + 9*Power(t1,2) + 5*Power(t1,3)) + 344*t2 - 
               170*Power(t2,2) - 329*Power(t2,3) + 
               Power(t1,5)*(454 + 5*t2) + 
               Power(t1,4)*(458 - 70*t2 + 21*Power(t2,2)) + 
               Power(t1,2)*(4924 + 3454*t2 - 853*Power(t2,2) - 
                  21*Power(t2,3)) - 
               Power(t1,3)*(3999 + 1525*t2 - 318*Power(t2,2) + 
                  56*Power(t2,3)) + 
               t1*(-1907 - 1854*t2 + 1324*Power(t2,2) + 
                  518*Power(t2,3)) + 
               2*Power(s1,2)*
                (95*Power(t1,4) - 59*t2 - Power(t1,3)*(522 + 37*t2) + 
                  3*t1*(-30 + 49*t2) + Power(t1,2)*(615 + 50*t2)) + 
               s1*(-225 + 15*Power(t1,5) + 44*t2 + 340*Power(t2,2) - 
                  3*Power(t1,4)*(56 + 9*t2) + 
                  t1*(1782 - 699*t2 - 598*Power(t2,2)) + 
                  Power(t1,3)*(1827 + 465*t2 + 70*Power(t2,2)) - 
                  Power(t1,2)*(3659 + 447*t2 + 102*Power(t2,2)))) - 
            Power(s2,4)*(-71 - 215*Power(t1,5) + 
               Power(s1,3)*(-11 - 7*t1 + 30*Power(t1,2)) - 311*t2 + 
               216*Power(t2,2) + 343*Power(t2,3) + 
               5*Power(t1,4)*(129 + 28*t2) + 
               Power(t1,3)*(247 + 244*t2 - 154*Power(t2,2)) + 
               Power(t1,2)*(-1191 - 2039*t2 + 287*Power(t2,2)) + 
               t1*(645 + 1778*t2 - 254*Power(t2,2) - 210*Power(t2,3)) + 
               Power(s1,2)*(-67 + 175*Power(t1,3) + t1*(470 - 16*t2) + 
                  125*t2 - Power(t1,2)*(593 + 70*t2)) + 
               s1*(213 - 165*Power(t1,4) + 32*t2 - 392*Power(t2,2) + 
                  3*Power(t1,3)*(67 + 28*t2) + 
                  Power(t1,2)*(916 + 361*t2) + 
                  t1*(-1048 - 535*t2 + 203*Power(t2,2)))) + 
            s2*(18 + 4*Power(t1,7) + 
               2*Power(s1,3)*Power(t1,2)*
                (11 - 56*t1 + 24*Power(t1,2)) - 28*t2 + 
               37*Power(t2,2) - 107*Power(t2,3) + 
               2*Power(t1,6)*(21 + 8*t2) - 
               Power(t1,5)*(1111 + 351*t2) + 
               Power(t1,4)*(5209 + 1375*t2 + 109*Power(t2,2)) + 
               Power(t1,2)*(3058 + 2348*t2 - 989*Power(t2,2) - 
                  87*Power(t2,3)) - 
               Power(t1,3)*(7651 + 2159*t2 - 124*Power(t2,2) + 
                  72*Power(t2,3)) + 
               t1*(474 - 1123*t2 + 724*Power(t2,2) + 274*Power(t2,3)) + 
               Power(s1,2)*t1*
                (43 + 4*Power(t1,5) + Power(t1,2)*(794 - 66*t2) + 
                  Power(t1,3)*(127 - 26*t2) + 157*t2 - 
                  Power(t1,4)*(52 + 3*t2) - 2*t1*(569 + 14*t2)) + 
               s1*(7 - 90*Power(t1,6) - 69*t2 + 92*Power(t2,2) + 
                  Power(t1,5)*(772 + 71*t2) + 
                  Power(t1,4)*(-1829 - 618*t2 + 10*Power(t2,2)) + 
                  Power(t1,2)*(950 + 749*t2 + 84*Power(t2,2)) + 
                  Power(t1,3)*(498 + 301*t2 + 200*Power(t2,2)) - 
                  2*t1*(97 + 70*t2 + 208*Power(t2,2)))) + 
            Power(s2,2)*(-52 - 112*Power(t1,6) + 
               2*Power(s1,3)*t1*
                (-27 + 78*t1 - 44*Power(t1,2) + 4*Power(t1,3)) + 
               187*t2 - 74*Power(t2,2) - 231*Power(t2,3) - 
               3*Power(t1,5)*(41 + 34*t2) + 
               Power(t1,4)*(3320 + 807*t2 + 80*Power(t2,2)) + 
               Power(t1,2)*(7050 + 3154*t2 - 1344*Power(t2,2) - 
                  93*Power(t2,3)) - 
               Power(t1,3)*(8678 + 2488*t2 - 193*Power(t2,2) + 
                  96*Power(t2,3)) + 
               t1*(-1683 - 1744*t2 + 1269*Power(t2,2) + 
                  450*Power(t2,3)) + 
               Power(s1,2)*(1 - 45*Power(t1,5) - 61*t2 + 
                  Power(t1,4)*(596 + 3*t2) - 
                  Power(t1,2)*(249 + 70*t2) - 
                  Power(t1,3)*(954 + 103*t2) + t1*(391 + 269*t2)) + 
               s1*(33 - 8*Power(t1,6) + Power(t1,5)*(366 - 14*t2) - 
                  37*t2 + 224*Power(t2,2) + 
                  t1*(792 - 849*t2 - 553*Power(t2,2)) + 
                  Power(t1,4)*(-1677 - 520*t2 + 15*Power(t2,2)) + 
                  Power(t1,2)*(-2836 + 701*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*(3860 + 835*t2 + 211*Power(t2,2))))) + 
         Power(s,7)*(-1 + 2*Power(t1,6)*(9 + 18*s1 - 2*t2) + 15*t2 - 
            42*Power(t2,2) + 28*Power(t2,3) - 
            2*Power(s2,5)*(168*Power(t1,3) + 
               5*s1*(3 - 14*t1 + 14*Power(t1,2)) - 5*(8 + 7*t2) + 
               20*t1*(11 + 7*t2) - 14*Power(t1,2)*(25 + 9*t2)) + 
            Power(t1,5)*(203 - 15*Power(s1,2) + 90*t2 - 
               s1*(367 + 18*t2)) + 
            Power(t1,3)*(1800 - 16*Power(s1,3) + 866*t2 - 
               101*Power(t2,2) + 24*Power(t2,3) + 
               Power(s1,2)*(-411 + 32*t2) + 
               s1*(23 + 15*t2 - 74*Power(t2,2))) + 
            Power(t1,2)*(295 - 777*t2 + 62*Power(t2,2) + 
               35*Power(t2,3) + Power(s1,2)*(59 + 28*t2) + 
               s1*(-172 + 71*t2 - 45*Power(t2,2))) + 
            Power(t1,4)*(-1548 - 6*Power(s1,3) - 407*t2 - 
               16*Power(t2,2) + Power(s1,2)*(109 + 16*t2) + 
               s1*(667 + 185*t2 - 4*Power(t2,2))) + 
            t1*(s1*(-4 - 43*t2 + 112*Power(t2,2)) - 
               5*(-7 + 12*t2 + 7*Power(t2,2) + 18*Power(t2,3))) + 
            Power(s2,4)*(92 + 525*Power(t1,4) + 
               Power(s1,2)*(-21 + 82*t1 - 28*Power(t1,2)) - 54*t2 - 
               119*Power(t2,2) + Power(t1,2)*(1549 + 14*t2) - 
               Power(t1,3)*(1473 + 196*t2) + 
               t1*(-679 + 176*t2 + 252*Power(t2,2)) + 
               s1*(19 + 189*Power(t1,3) + 110*t2 + 
                  Power(t1,2)*(-159 + 28*t2) - t1*(25 + 294*t2))) - 
            s2*(10 - 82*Power(t1,6) + 
               Power(s1,3)*Power(t1,2)*(17 - 28*t1 + 7*Power(t1,2)) + 
               Power(t1,5)*(269 - 41*t2) - 53*t2 + 96*Power(t2,2) - 
               123*Power(t2,3) + 
               Power(t1,4)*(1333 + 343*t2 + 23*Power(t2,2)) + 
               Power(t1,2)*(2076 + 2246*t2 - 309*Power(t2,2) - 
                  46*Power(t2,3)) - 
               Power(t1,3)*(4167 + 1703*t2 - 226*Power(t2,2) + 
                  44*Power(t2,3)) + 
               t1*(65 - 447*t2 + 133*Power(t2,2) + 240*Power(t2,3)) + 
               Power(s1,2)*t1*
                (24 - 10*Power(t1,4) + Power(t1,2)*(61 - 46*t2) + 
                  Power(t1,3)*(143 - 5*t2) + 80*t2 - t1*(359 + 7*t2)) + 
               s1*(1 + 355*Power(t1,5) - 12*Power(t1,6) - 28*t2 + 
                  62*Power(t2,2) + t1*(-72 + 69*t2 - 259*Power(t2,2)) + 
                  2*Power(t1,2)*(-644 - 5*t2 + 3*Power(t2,2)) + 
                  Power(t1,4)*(-1454 - 259*t2 + 6*Power(t2,2)) + 
                  Power(t1,3)*(2447 + 250*t2 + 100*Power(t2,2)))) + 
            Power(s2,3)*(21 - 275*Power(t1,5) + 
               2*Power(s1,3)*(-1 + t1 + 6*Power(t1,2)) - 113*t2 - 
               23*Power(t2,2) + 183*Power(t2,3) + 
               2*Power(t1,4)*(649 + 70*t2) + 
               Power(t1,3)*(-1315 + 141*t2 - 140*Power(t2,2)) + 
               Power(t1,2)*(522 - 1627*t2 + 91*Power(t2,2)) + 
               t1*(-143 + 1094*t2 + 205*Power(t2,2) - 
                  120*Power(t2,3)) + 
               Power(s1,2)*(-19 + 161*Power(t1,3) + t1*(222 - 22*t2) + 
                  38*t2 - Power(t1,2)*(389 + 42*t2)) + 
               s1*(79 - 3*Power(t1,4) + 95*t2 - 164*Power(t2,2) + 
                  7*Power(t1,3)*(-43 + 6*t2) + 
                  Power(t1,2)*(1291 + 374*t2) + 
                  t1*(-799 - 569*t2 + 115*Power(t2,2)))) + 
            Power(s2,2)*(-6 + 40*Power(t1,6) + 
               Power(s1,3)*t1*(15 - 16*t1 + 7*Power(t1,2)) - 46*t2 - 
               70*Power(t2,2) + 210*Power(t2,3) - 
               Power(t1,5)*(608 + 9*t2) + 
               Power(t1,4)*(665 + 107*t2 - 7*Power(t2,2)) + 
               t1*(442 + 1077*t2 - 169*Power(t2,2) - 286*Power(t2,3)) + 
               Power(t1,2)*(-2093 - 2858*t2 + 270*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,3)*(1704 + 1230*t2 - 277*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(s1,2)*(-113*Power(t1,4) + t1*(4 - 110*t2) + 
                  27*t2 + Power(t1,3)*(468 + 38*t2) - 
                  Power(t1,2)*(346 + 47*t2)) + 
               s1*(-13 - 76*Power(t1,5) + 69*t2 - 152*Power(t2,2) + 
                  Power(t1,4)*(583 + 50*t2) + 
                  Power(t1,2)*(2981 + 433*t2 + 39*Power(t2,2)) - 
                  Power(t1,3)*(2299 + 227*t2 + 42*Power(t2,2)) + 
                  t1*(-811 - 139*t2 + 290*Power(t2,2))))) - 
         Power(s,2)*Power(-1 + s2,2)*
          (4 - 70*t1 + 16*s1*t1 + 185*Power(t1,2) - 72*s1*Power(t1,2) - 
            6*Power(s1,2)*Power(t1,2) - 251*Power(t1,3) + 
            201*s1*Power(t1,3) + 35*Power(s1,2)*Power(t1,3) - 
            16*Power(s1,3)*Power(t1,3) + 219*Power(t1,4) - 
            253*s1*Power(t1,4) + 46*Power(s1,2)*Power(t1,4) + 
            9*Power(s1,3)*Power(t1,4) - 101*Power(t1,5) + 
            134*s1*Power(t1,5) - 54*Power(s1,2)*Power(t1,5) + 
            15*Power(s1,3)*Power(t1,5) + 16*Power(t1,6) - 
            22*s1*Power(t1,6) + 12*Power(s1,2)*Power(t1,6) - 
            2*Power(t1,7) - 4*s1*Power(t1,7) - 15*t2 + 190*t1*t2 - 
            33*s1*t1*t2 - 364*Power(t1,2)*t2 + 161*s1*Power(t1,2)*t2 + 
            8*Power(s1,2)*Power(t1,2)*t2 + 171*Power(t1,3)*t2 - 
            276*s1*Power(t1,3)*t2 + 47*Power(s1,2)*Power(t1,3)*t2 + 
            40*Power(t1,4)*t2 + 33*s1*Power(t1,4)*t2 - 
            69*Power(s1,2)*Power(t1,4)*t2 - 24*Power(t1,5)*t2 + 
            51*s1*Power(t1,5)*t2 - 15*Power(s1,2)*Power(t1,5)*t2 + 
            2*Power(t1,6)*t2 - 2*s1*Power(t1,6)*t2 + 18*Power(t2,2) - 
            165*t1*Power(t2,2) + 17*s1*t1*Power(t2,2) + 
            232*Power(t1,2)*Power(t2,2) - 
            105*s1*Power(t1,2)*Power(t2,2) + 6*Power(t1,3)*Power(t2,2) + 
            91*s1*Power(t1,3)*Power(t2,2) - 58*Power(t1,4)*Power(t2,2) + 
            31*s1*Power(t1,4)*Power(t2,2) - 7*Power(t2,3) + 
            45*t1*Power(t2,3) - 35*Power(t1,2)*Power(t2,3) - 
            16*Power(t1,3)*Power(t2,3) + 
            Power(s2,7)*(-2 + Power(s1,2)*(9 - 13*t1 + 2*Power(t1,2)) - 
               Power(t1,3)*(-2 + t2) + t2 + 11*Power(t2,2) - 
               Power(t1,2)*(6 + t2) + t1*(6 + t2 - 13*Power(t2,2)) - 
               s1*(-5 + Power(t1,3) + t1*(15 - 26*t2) + 20*t2 + 
                  Power(t1,2)*(-11 + 2*t2))) + 
            s2*(48 + 2*Power(t1,7) + 
               2*Power(s1,3)*Power(t1,2)*
                (20 + 5*t1 - 43*Power(t1,2) + 7*Power(t1,3)) - 122*t2 + 
               95*Power(t2,2) - 21*Power(t2,3) + 
               2*Power(t1,6)*(7 + 2*t2) - Power(t1,5)*(31 + 43*t2) + 
               Power(t1,4)*(216 - 7*t2 + 45*Power(t2,2)) - 
               t1*(160 - 217*t2 + 73*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,3)*(-469 - 16*t2 - 72*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,2)*(380 - 33*t2 - 169*Power(t2,2) + 
                  59*Power(t2,3)) + 
               Power(s1,2)*t1*
                (11 - 12*Power(t1,5) - 15*t2 + 
                  Power(t1,4)*(-64 + 5*t2) + 
                  Power(t1,3)*(199 + 52*t2) - 2*t1*(39 + 64*t2) + 
                  2*Power(t1,2)*(-115 + 86*t2)) + 
               s1*(-11 + 8*Power(t1,7) - 4*Power(t1,6)*(-4 + t2) + 
                  23*t2 - 12*Power(t2,2) + Power(t1,5)*(4 + 27*t2) + 
                  Power(t1,3)*(696 - 23*t2 - 126*Power(t2,2)) + 
                  Power(t1,4)*(-337 + 38*t2 - 36*Power(t2,2)) + 
                  Power(t1,2)*(-420 + 397*t2 - 30*Power(t2,2)) + 
                  2*t1*(22 - 55*t2 + 49*Power(t2,2)))) + 
            Power(s2,2)*(26 + 2*Power(t1,7) - 
               Power(s1,3)*t1*
                (32 + 44*t1 - 108*Power(t1,2) + 33*Power(t1,3) + 
                  9*Power(t1,4)) + 20*t2 - 58*Power(t2,2) + 
               30*Power(t2,3) - 6*Power(t1,6)*(6 + t2) + 
               4*Power(t1,5)*(-3 + 7*t2) + 
               Power(t1,4)*(105 + 19*t2 + 68*Power(t2,2)) + 
               Power(t1,3)*(-199 + 29*t2 + 105*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(325 + 18*t2 + 68*Power(t2,2) + 
                  18*Power(t2,3)) - 
               t1*(211 + 108*t2 - 194*Power(t2,2) + 79*Power(t2,3)) + 
               Power(s1,2)*(-5 + Power(t1,2)*(333 - 174*t2) + 7*t2 - 
                  Power(t1,3)*(191 + 4*t2) + Power(t1,5)*(5 + 11*t2) + 
                  2*Power(t1,4)*(95 + 22*t2) + t1*(45 + 121*t2)) - 
               s1*(-5 + 4*Power(t1,7) + Power(t1,6)*(14 - 6*t2) + 
                  5*t2 + 16*Power(t2,2) + Power(t1,5)*(-118 + 47*t2) + 
                  2*t1*(-209 + 179*t2 + Power(t2,2)) + 
                  Power(t1,4)*(134 + 183*t2 + 22*Power(t2,2)) + 
                  Power(t1,3)*(-223 + 217*t2 + 54*Power(t2,2)) - 
                  2*Power(t1,2)*(-306 + 25*t2 + 57*Power(t2,2)))) + 
            Power(s2,6)*(-11 - 2*Power(t1,4) + 
               Power(s1,3)*(-17 - 3*t1 + 2*Power(t1,2)) - 13*t2 + 
               54*Power(t2,2) + 68*Power(t2,3) + 
               Power(s1,2)*(1 - 9*Power(t1,3) + t1*(2 - 32*t2) + 
                  Power(t1,2)*(25 - 2*t2) + 93*t2) + 
               Power(t1,3)*(11 - t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-27 + 6*t2 + 56*Power(t2,2)) + 
               t1*(29 + 8*t2 - 96*Power(t2,2) - 45*Power(t2,3)) + 
               s1*(27 + 2*Power(t1,4) + Power(t1,2)*(70 - 40*t2) - 
                  28*t2 - 144*Power(t2,2) - 3*Power(t1,3)*(11 + t2) + 
                  t1*(-66 + 33*t2 + 80*Power(t2,2)))) - 
            Power(s2,3)*(-22 - 2*Power(t1,6) + 2*Power(t1,7) + 
               Power(s1,3)*(-8 - 22*t1 + 28*Power(t1,2) - 
                  82*Power(t1,3) - 20*Power(t1,4) + 4*Power(t1,5)) - 
               92*t2 + 95*Power(t2,2) - 38*Power(t2,3) - 
               Power(t1,5)*(79 + 34*t2) + 
               Power(t1,4)*(56 + 55*t2 + 46*Power(t2,2)) + 
               Power(t1,3)*(138 + 17*t2 + 60*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(59 + 45*t2 - 89*Power(t2,2) + 48*Power(t2,3)) + 
               Power(t1,2)*(-152 + 9*t2 + 318*Power(t2,2) + 
                  58*Power(t2,3)) + 
               Power(s1,2)*(2 + t1*(131 - 79*t2) + 
                  Power(t1,5)*(-6 + t2) + 40*t2 + 
                  4*Power(t1,2)*(-23 + 26*t2) + 
                  Power(t1,4)*(-131 + 32*t2) + 
                  Power(t1,3)*(526 + 202*t2)) - 
               s1*(-156 + 20*Power(t1,6) + 151*t2 - 16*Power(t2,2) - 
                  Power(t1,5)*(33 + 31*t2) + 
                  t1*(86 - 27*t2 - 48*Power(t2,2)) + 
                  Power(t1,4)*(-345 + 69*t2 + 28*Power(t2,2)) + 
                  3*Power(t1,2)*(-73 + 102*t2 + 72*Power(t2,2)) + 
                  Power(t1,3)*(647 + 392*t2 + 120*Power(t2,2)))) + 
            Power(s2,5)*(-18 + 
               Power(s1,3)*(-10 + 92*t1 + 8*Power(t1,2) - 
                  8*Power(t1,3)) + Power(t1,5)*(-2 + t2) + 73*t2 + 
               5*Power(t2,2) - 129*Power(t2,3) + 
               Power(t1,4)*(2 + 2*t2 - 7*Power(t2,2)) + 
               t1*(99 - 141*t2 - 171*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(65 - 38*t2 - 52*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-146 + 103*t2 + 127*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(s1,2)*(127 + 12*Power(t1,4) - 52*t2 - 
                  2*Power(t1,3)*t2 + 64*Power(t1,2)*(1 + t2) - 
                  t1*(301 + 276*t2)) + 
               s1*(-84 - Power(t1,5) - 178*t2 + 204*Power(t2,2) + 
                  Power(t1,4)*(52 + 5*t2) + 
                  Power(t1,3)*(-154 + 7*t2 - 2*Power(t2,2)) - 
                  3*Power(t1,2)*(-36 + 59*t2 + 22*Power(t2,2)) + 
                  t1*(79 + 539*t2 + 150*Power(t2,2)))) + 
            Power(s2,4)*(11 + 4*Power(t1,6) + 
               Power(s1,3)*(3 + t1 - 138*Power(t1,2) - 16*Power(t1,3) + 
                  10*Power(t1,4)) - 36*t2 - 30*Power(t2,2) + 
               21*Power(t2,3) + Power(t1,5)*(-13 + 4*t2) + 
               Power(t1,4)*(-84 + t2 - 2*Power(t2,2)) + 
               Power(t1,3)*(179 - 127*t2 + 68*Power(t2,2)) + 
               Power(t1,2)*(-63 + 280*t2 + 4*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-34 - 122*t2 + 235*Power(t2,2) + 143*Power(t2,3)) - 
               Power(s1,2)*(18 + 5*Power(t1,5) + t1*(173 - 123*t2) + 
                  Power(t1,4)*(18 - 5*t2) + 8*t2 + 
                  Power(t1,3)*(199 + 11*t2) - 
                  16*Power(t1,2)*(43 + 21*t2)) - 
               s1*(-38 + 46*Power(t1,5) - 57*t2 + 16*Power(t2,2) + 
                  Power(t1,4)*(-135 - 38*t2 + Power(t2,2)) + 
                  Power(t1,3)*(-181 - 120*t2 + 29*Power(t2,2)) + 
                  Power(t1,2)*(626 + 695*t2 + 129*Power(t2,2)) + 
                  t1*(-318 + 70*t2 + 295*Power(t2,2))))) + 
         Power(s,4)*(178*t1 - 70*s1*t1 - 918*Power(t1,2) + 
            357*s1*Power(t1,2) - 22*Power(s1,2)*Power(t1,2) + 
            1722*Power(t1,3) - 2570*s1*Power(t1,3) + 
            135*Power(s1,2)*Power(t1,3) + 90*Power(s1,3)*Power(t1,3) - 
            947*Power(t1,4) + 2611*s1*Power(t1,4) - 
            490*Power(s1,2)*Power(t1,4) - 20*Power(s1,3)*Power(t1,4) - 
            256*Power(t1,5) - 783*s1*Power(t1,5) + 
            230*Power(s1,2)*Power(t1,5) - 15*Power(s1,3)*Power(t1,5) + 
            230*Power(t1,6) + 12*s1*Power(t1,6) - 
            40*Power(s1,2)*Power(t1,6) - 12*Power(t1,7) + 
            10*s1*Power(t1,7) + 15*t2 - 624*t1*t2 + 209*s1*t1*t2 + 
            1195*Power(t1,2)*t2 - 226*s1*Power(t1,2)*t2 - 
            56*Power(s1,2)*Power(t1,2)*t2 - 234*Power(t1,3)*t2 + 
            385*s1*Power(t1,3)*t2 - 121*Power(s1,2)*Power(t1,3)*t2 + 
            344*Power(t1,4)*t2 + 122*s1*Power(t1,4)*t2 + 
            35*Power(s1,2)*Power(t1,4)*t2 - 366*Power(t1,5)*t2 - 
            25*s1*Power(t1,5)*t2 + 30*Power(s1,2)*Power(t1,5)*t2 + 
            56*Power(t1,6)*t2 - 42*Power(t2,2) + 547*t1*Power(t2,2) - 
            140*s1*t1*Power(t2,2) - 691*Power(t1,2)*Power(t2,2) + 
            315*s1*Power(t1,2)*Power(t2,2) - 
            213*Power(t1,3)*Power(t2,2) - 
            53*s1*Power(t1,3)*Power(t2,2) + 
            142*Power(t1,4)*Power(t2,2) - 
            65*s1*Power(t1,4)*Power(t2,2) + 28*Power(t2,3) - 
            84*t1*Power(t2,3) + 29*Power(t1,2)*Power(t2,3) + 
            24*Power(t1,3)*Power(t2,3) + 
            2*Power(s2,8)*(-1 + t1)*
             (4 + 3*Power(t1,2) + s1*(-6 + 8*t1) + 7*t2 - t1*(7 + 9*t2)) \
+ Power(s2,7)*(76 - 21*Power(t1,4) + 
               Power(s1,2)*(10 + 19*t1 - 28*Power(t1,2)) + 99*t2 + 
               7*Power(t2,2) + Power(t1,3)*(-42 + 44*t2) + 
               Power(t1,2)*(236 + 98*t2) + 
               t1*(-247 - 223*t2 + 12*Power(t2,2)) + 
               s1*(-69*Power(t1,3) + t1*(88 - 42*t2) - 2*(23 + 8*t2) + 
                  Power(t1,2)*(9 + 40*t2))) + 
            Power(s2,6)*(130 + 19*Power(t1,5) - 
               5*Power(s1,3)*(-7 - 8*t1 + 6*Power(t1,2)) + 
               Power(t1,4)*(103 - 28*t2) + 57*t2 - 504*Power(t2,2) - 
               357*Power(t2,3) + 
               Power(t1,2)*(1094 + 425*t2 - 371*Power(t2,2)) + 
               4*Power(t1,3)*(-175 - 33*t2 + 7*Power(t2,2)) + 
               t1*(-657 - 412*t2 + 688*Power(t2,2) + 210*Power(t2,3)) + 
               Power(s1,2)*(12 + 35*Power(t1,3) - 270*t2 + 
                  t1*(-261 + 2*t2) + 3*Power(t1,2)*(45 + 14*t2)) + 
               s1*(-88 + 81*Power(t1,4) + Power(t1,3)*(76 - 42*t2) + 
                  298*t2 + 574*Power(t2,2) - 
                  Power(t1,2)*(385 + 67*t2) + 
                  t1*(407 + 34*t2 - 245*Power(t2,2)))) + 
            Power(s2,3)*(299 + 18*Power(t1,7) + 
               2*Power(s1,3)*
                (10 - 128*t1 + 261*Power(t1,2) - 195*Power(t1,3) - 
                  4*Power(t1,4) + 6*Power(t1,5)) - 74*t2 - 
               323*Power(t2,2) + 26*Power(t2,3) + 
               4*Power(t1,6)*(35 + 4*t2) - 
               6*Power(t1,5)*(119 + 65*t2) + 
               Power(t1,4)*(2186 + 873*t2 + 178*Power(t2,2)) + 
               t1*(-763 + 631*t2 + 652*Power(t2,2) - 76*Power(t2,3)) + 
               2*Power(t1,3)*
                (-2016 + 125*t2 + 70*Power(t2,2) + 24*Power(t2,3)) + 
               2*Power(t1,2)*
                (1530 - 384*t2 - 87*Power(t2,2) + 31*Power(t2,3)) + 
               Power(s1,2)*(-257 + 12*Power(t1,6) - 3*t2 - 
                  2*Power(t1,5)*(92 + 7*t2) + 
                  Power(t1,4)*(713 + 67*t2) - 
                  2*Power(t1,2)*(998 + 95*t2) + t1*(1157 + 108*t2) + 
                  Power(t1,3)*(890 + 232*t2)) + 
               s1*(422 - 90*Power(t1,6) + 354*t2 - 8*Power(t2,2) + 
                  2*Power(t1,5)*(105 + 97*t2) + 
                  Power(t1,3)*(693 - 518*t2 - 128*Power(t2,2)) + 
                  2*t1*(-923 - 330*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(1697 + 510*t2 + 12*Power(t2,2)) - 
                  2*Power(t1,4)*(738 + 323*t2 + 22*Power(t2,2)))) + 
            Power(s2,5)*(162 - 4*Power(t1,6) + 
               Power(s1,3)*(38 - 178*t1 - 85*Power(t1,2) + 
                  65*Power(t1,3)) + Power(t1,5)*(2 - 9*t2) - 147*t2 - 
               125*Power(t2,2) + 213*Power(t2,3) + 
               Power(t1,4)*(606 + 10*t2 + 35*Power(t2,2)) - 
               Power(t1,2)*(-1815 + 122*t2 + 741*Power(t2,2) + 
                  21*Power(t2,3)) - 
               Power(t1,3)*(1770 + 149*t2 - 164*Power(t2,2) + 
                  56*Power(t2,3)) + 
               t1*(-773 + 781*t2 + 1490*Power(t2,2) + 
                  266*Power(t2,3)) + 
               Power(s1,2)*(-107 + 5*Power(t1,4) - 13*t2 + 
                  Power(t1,2)*(749 + 34*t2) - 
                  Power(t1,3)*(429 + 46*t2) + t1*(319 + 717*t2)) + 
               s1*(-20 - 41*Power(t1,5) + 469*t2 - 264*Power(t2,2) + 
                  Power(t1,4)*(-140 + 29*t2) + 
                  t1*(25 - 2222*t2 - 660*Power(t2,2)) + 
                  Power(t1,2)*(-703 + 31*t2 - 60*Power(t2,2)) + 
                  Power(t1,3)*(531 + 379*t2 + 42*Power(t2,2)))) + 
            Power(s2,2)*(-268 - 24*Power(t1,7) + 
               Power(s1,3)*t1*
                (-9 + 412*t1 - 490*Power(t1,2) + 209*Power(t1,3) - 
                  7*Power(t1,4)) + 364*t2 - 308*Power(t2,2) + 
               15*Power(t2,3) + 2*Power(t1,6)*(97 + 24*t2) - 
               2*Power(t1,5)*(607 + 224*t2) + 
               Power(t1,4)*(2579 + 485*t2 + 80*Power(t2,2)) + 
               t1*(62 + 400*t2 + 746*Power(t2,2) - 46*Power(t2,3)) + 
               3*Power(t1,2)*
                (160 - 141*t2 - 155*Power(t2,2) + 6*Power(t2,3)) + 
               2*Power(t1,3)*
                (-985 + 124*t2 - 9*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s1,2)*(10 + 24*Power(t1,6) + t1*(896 - 88*t2) - 
                  55*t2 + Power(t1,4)*(-818 + 9*t2) - 
                  Power(t1,5)*(151 + 14*t2) + 
                  Power(t1,3)*(2447 + 66*t2) - 
                  Power(t1,2)*(2429 + 88*t2)) + 
               s1*(5 + 18*Power(t1,7) + 214*t2 + 30*Power(t2,2) - 
                  12*Power(t1,6)*(3 + 2*t2) + 
                  Power(t1,5)*(593 + 156*t2) + 
                  Power(t1,3)*(-4068 + 60*t2 - 38*Power(t2,2)) + 
                  t1*(-3090 - 566*t2 + 3*Power(t2,2)) - 
                  2*Power(t1,4)*(-158 + 72*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(5383 + 325*t2 + 66*Power(t2,2)))) - 
            s2*(75 + 14*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (97 + 207*t1 - 202*Power(t1,2) + 38*Power(t1,3)) - 
               306*t2 + 279*Power(t2,2) - 33*Power(t2,3) - 
               20*Power(t1,6)*(13 + 2*t2) + 
               Power(t1,5)*(854 + 321*t2) - 
               Power(t1,4)*(939 + 437*t2 + 115*Power(t2,2)) + 
               Power(t1,2)*(1855 + 160*t2 + 357*Power(t2,2) - 
                  7*Power(t2,3)) - 
               Power(t1,3)*(692 - 281*t2 + 152*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(-961 + 1085*t2 - 718*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s1,2)*t1*
                (5 - 4*Power(t1,5) + t1*(769 - 188*t2) + 
                  2*Power(t1,4)*(-105 + t2) - 127*t2 + 
                  17*Power(t1,2)*(-119 + 2*t2) + 
                  Power(t1,3)*(1184 + 3*t2)) + 
               s1*(-30 - 4*Power(t1,7) + Power(t1,5)*(195 - 30*t2) + 
                  95*t2 - 64*Power(t2,2) + Power(t1,6)*(26 + 8*t2) + 
                  Power(t1,2)*(-5101 + 13*t2 - 24*Power(t2,2)) + 
                  Power(t1,4)*(-2794 + 223*t2 + 12*Power(t2,2)) + 
                  Power(t1,3)*(6283 + 101*t2 + 18*Power(t2,2)) + 
                  t1*(205 + 276*t2 + 200*Power(t2,2)))) - 
            Power(s2,4)*(-308 + 52*Power(t1,6) + 
               Power(s1,3)*(-51 + 221*t1 - 334*Power(t1,2) - 
                  68*Power(t1,3) + 47*Power(t1,4)) + 190*t2 + 
               346*Power(t2,2) - 42*Power(t2,3) + 
               Power(t1,5)*(295 + 66*t2) - 
               Power(t1,4)*(1387 + 535*t2 + 90*Power(t2,2)) + 
               Power(t1,3)*(2354 + 130*t2 + 253*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(t1,2)*(-2732 + 1411*t2 + 1041*Power(t2,2) + 
                  95*Power(t2,3)) + 
               t1*(1839 - 436*t2 - 267*Power(t2,2) + 240*Power(t2,3)) + 
               Power(s1,2)*(164 + 25*Power(t1,5) + 43*t2 - 
                  2*Power(t1,4)*(223 + 10*t2) + 
                  Power(t1,3)*(1101 + 97*t2) - t1*(451 + 158*t2) + 
                  Power(t1,2)*(472 + 698*t2)) - 
               s1*(53 + 12*Power(t1,6) + 408*t2 - 12*Power(t2,2) - 
                  7*Power(t1,5)*(-24 + 5*t2) + 
                  Power(t1,4)*(-426 - 514*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*(1130 + 733*t2 + 195*Power(t2,2)) + 
                  t1*(447 - 765*t2 + 214*Power(t2,2)) + 
                  Power(t1,2)*(-627 + 2248*t2 + 411*Power(t2,2))))) + 
         Power(s,3)*(5 - 155*t1 + 48*s1*t1 + 513*Power(t1,2) - 
            182*s1*Power(t1,2) - 7*Power(s1,2)*Power(t1,2) - 
            1014*Power(t1,3) + 991*s1*Power(t1,3) + 
            67*Power(s1,2)*Power(t1,3) - 52*Power(s1,3)*Power(t1,3) + 
            1000*Power(t1,4) - 1283*s1*Power(t1,4) + 
            191*Power(s1,2)*Power(t1,4) + 24*Power(s1,3)*Power(t1,4) - 
            349*Power(t1,5) + 581*s1*Power(t1,5) - 
            143*Power(s1,2)*Power(t1,5) + 20*Power(s1,3)*Power(t1,5) - 
            64*s1*Power(t1,6) + 32*Power(s1,2)*Power(t1,6) - 
            10*s1*Power(t1,7) - 
            2*Power(s2,9)*Power(-1 + t1,2)*(s1 - t2) - 27*t2 + 
            452*t1*t2 - 113*s1*t1*t2 - 825*Power(t1,2)*t2 + 
            275*s1*Power(t1,2)*t2 + 28*Power(s1,2)*Power(t1,2)*t2 + 
            232*Power(t1,3)*t2 - 463*s1*Power(t1,3)*t2 + 
            88*Power(s1,2)*Power(t1,3)*t2 + 25*Power(t1,4)*t2 - 
            67*s1*Power(t1,4)*t2 - 88*Power(s1,2)*Power(t1,4)*t2 + 
            78*Power(t1,5)*t2 + 82*s1*Power(t1,5)*t2 - 
            30*Power(s1,2)*Power(t1,5)*t2 - 16*Power(t1,6)*t2 - 
            4*s1*Power(t1,6)*t2 + 42*Power(t2,2) - 387*t1*Power(t2,2) + 
            64*s1*t1*Power(t2,2) + 506*Power(t1,2)*Power(t2,2) - 
            231*s1*Power(t1,2)*Power(t2,2) + 
            109*Power(t1,3)*Power(t2,2) + 
            130*s1*Power(t1,3)*Power(t2,2) - 
            120*Power(t1,4)*Power(t2,2) + 
            64*s1*Power(t1,4)*Power(t2,2) - 20*Power(t2,3) + 
            90*t1*Power(t2,3) - 55*Power(t1,2)*Power(t2,3) - 
            32*Power(t1,3)*Power(t2,3) + 
            Power(s2,8)*(-24 + Power(t1,4) + 
               Power(s1,2)*(11 - 30*t1 + 12*Power(t1,2)) + 
               Power(t1,3)*(21 - 10*t2) - 18*t2 + 19*Power(t2,2) - 
               Power(t1,2)*(71 + 20*t2) + 
               t1*(73 + 44*t2 - 30*Power(t2,2)) + 
               s1*(23 + 7*Power(t1,3) - 30*t2 - 
                  7*Power(t1,2)*(-5 + 2*t2) + t1*(-61 + 62*t2))) + 
            Power(s2,7)*(-35 - Power(t1,5) + 
               6*Power(s1,3)*(-5 - 4*t1 + 2*Power(t1,2)) - 9*t2 + 
               227*Power(t2,2) + 197*Power(t2,3) + 
               Power(t1,4)*(-34 + 4*t2) + 
               Power(s1,2)*(5 - 37*Power(t1,3) + t1*(80 - 26*t2) + 
                  Power(t1,2)*(25 - 14*t2) + 200*t2) + 
               Power(t1,3)*(161 + 35*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-248 - 37*t2 + 193*Power(t2,2)) + 
               t1*(157 + 42*t2 - 325*Power(t2,2) - 120*Power(t2,3)) - 
               s1*(-43 + 9*Power(t1,4) + Power(t1,3)*(121 - 6*t2) + 
                  127*t2 + 364*Power(t2,2) + 
                  Power(t1,2)*(-227 + 34*t2) + 
                  t1*(175 + 17*t2 - 169*Power(t2,2)))) + 
            Power(s2,2)*(102 - 14*Power(t1,7) + 
               Power(s1,3)*t1*
                (-45 - 258*t1 + 235*Power(t1,2) - 86*Power(t1,3) + 
                  36*Power(t1,4)) + Power(t1,5)*(68 - 159*t2) + 62*t2 - 
               98*Power(t2,2) + 42*Power(t2,3) + 
               Power(t1,6)*(-26 + 4*t2) + 
               Power(t1,4)*(779 + 153*t2 + 31*Power(t2,2)) + 
               Power(t1,2)*(1153 - 200*t2 + 270*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,3)*(-1424 - 182*t2 + 155*Power(t2,2) + 
                  12*Power(t2,3)) - 
               t1*(638 + 793*t2 - 243*Power(t2,2) + 54*Power(t2,3)) - 
               Power(s1,2)*(10 + 4*Power(t1,6) - 25*t2 + 
                  Power(t1,4)*(-945 + 2*t2) + 
                  Power(t1,5)*(258 + 8*t2) - 
                  Power(t1,2)*(1434 + 19*t2) + 
                  2*Power(t1,3)*(602 + 23*t2) - 8*t1*(-32 + 29*t2)) + 
               s1*(33 + 18*Power(t1,7) - 155*t2 + 16*Power(t2,2) - 
                  6*Power(t1,6)*(-17 + 2*t2) + 
                  4*Power(t1,5)*(69 + 13*t2) + 
                  t1*(2009 - 347*t2 - 134*Power(t2,2)) - 
                  3*Power(t1,3)*(-749 + 129*t2 + 6*Power(t2,2)) - 
                  Power(t1,4)*(1313 + 132*t2 + 8*Power(t2,2)) + 
                  Power(t1,2)*(-2257 - 267*t2 + 39*Power(t2,2)))) + 
            s2*(88 + 2*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (95 + 110*t1 - 175*Power(t1,2) + 26*Power(t1,3)) - 
               249*t2 + 184*Power(t2,2) - 23*Power(t2,3) + 
               4*Power(t1,6)*(22 + 9*t2) - 3*Power(t1,5)*(83 + 43*t2) + 
               Power(t1,4)*(663 - 41*t2 + 75*Power(t2,2)) + 
               Power(t1,3)*(-1527 - 127*t2 - 290*Power(t2,2) + 
                  36*Power(t2,3)) - 
               t1*(409 - 257*t2 + 39*Power(t2,2) + 56*Power(t2,3)) + 
               Power(t1,2)*(1344 + 716*t2 - 509*Power(t2,2) + 
                  78*Power(t2,3)) + 
               Power(s1,2)*t1*
                (20 - 16*Power(t1,5) + t1*(31 - 231*t2) - 56*t2 + 
                  4*Power(t1,4)*(-49 + 5*t2) + 
                  Power(t1,3)*(649 + 95*t2) + 
                  Power(t1,2)*(-1045 + 102*t2)) + 
               s1*(-25 + 6*Power(t1,7) + 60*t2 - 34*Power(t2,2) - 
                  2*Power(t1,6)*(19 + 2*t2) + 
                  Power(t1,5)*(245 + 22*t2) + 
                  Power(t1,3)*(2935 + 422*t2 - 164*Power(t2,2)) + 
                  Power(t1,4)*(-1406 + 177*t2 - 70*Power(t2,2)) + 
                  2*Power(t1,2)*(-1115 + 181*t2 + 75*Power(t2,2)) + 
                  t1*(50 + 97*t2 + 97*Power(t2,2)))) + 
            Power(s2,3)*(77 + 22*Power(t1,7) - 
               2*Power(s1,3)*
                (-1 - 77*t1 + 89*Power(t1,2) - 110*Power(t1,3) + 
                  81*Power(t1,4) + 3*Power(t1,5)) + 303*t2 - 
               81*Power(t2,2) + 19*Power(t2,3) - 
               10*Power(t1,6)*(3 + 2*t2) + Power(t1,5)*(-35 + 34*t2) + 
               2*Power(t1,4)*(-276 + 102*t2 + 67*Power(t2,2)) + 
               t1*(-853 + 504*t2 + 185*Power(t2,2) - 56*Power(t2,3)) + 
               Power(t1,3)*(-27 + 329*t2 + 168*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(t1,2)*(1398 + 115*t2 + 35*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(s1,2)*(143 - 8*Power(t1,6) + 
                  Power(t1,2)*(1571 - 56*t2) - 74*t2 + 
                  6*Power(t1,5)*(17 + 2*t2) - 10*t1*(67 + 7*t2) + 
                  Power(t1,4)*(700 + 68*t2) + 
                  Power(t1,3)*(-1611 + 140*t2)) - 
               s1*(617 + 14*Power(t1,7) + Power(t1,6)*(62 - 20*t2) - 
                  141*t2 - 8*Power(t2,2) + 6*Power(t1,5)*(53 + 6*t2) + 
                  t1*(-737 + 227*t2 - 55*Power(t2,2)) + 
                  Power(t1,2)*(2619 - 158*t2 + 12*Power(t2,2)) + 
                  3*Power(t1,4)*(-87 + 130*t2 + 12*Power(t2,2)) + 
                  Power(t1,3)*(-1163 + 334*t2 + 120*Power(t2,2)))) + 
            Power(s2,6)*(50 + 
               Power(s1,3)*(2 + 167*t1 + 62*Power(t1,2) - 
                  37*Power(t1,3)) + 182*t2 + 5*Power(t1,5)*t2 - 
               184*Power(t2,2) - 398*Power(t2,3) + 
               Power(t1,4)*(-105 + t2 - 21*Power(t2,2)) + 
               t1*(28 - 585*t2 - 391*Power(t2,2) + 14*Power(t2,3)) + 
               Power(t1,2)*(-345 + 330*t2 + 218*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(t1,3)*(372 - 106*t2 - 123*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(s1,2)*(172 + 35*Power(t1,4) - 209*t2 + 
                  2*Power(t1,3)*(37 + 5*t2) + 
                  Power(t1,2)*(-80 + 43*t2) - 4*t1*(151 + 141*t2)) + 
               s1*(-173 + 8*Power(t1,5) + Power(t1,4)*(177 - 6*t2) - 
                  227*t2 + 632*Power(t2,2) + 
                  Power(t1,2)*(3 - 173*t2 - 39*Power(t2,2)) - 
                  7*Power(t1,3)*(45 + 15*t2 + 2*Power(t2,2)) + 
                  t1*(473 + 1415*t2 + 282*Power(t2,2)))) - 
            Power(s2,4)*(-287 + 10*Power(t1,7) + 
               2*Power(s1,3)*
                (15 - 53*t1 + 62*Power(t1,2) - 119*Power(t1,3) - 
                  21*Power(t1,4) + 6*Power(t1,5)) + 151*t2 + 
               163*Power(t2,2) - 24*Power(t2,3) + 
               Power(t1,6)*(54 + 4*t2) - Power(t1,5)*(205 + 156*t2) + 
               Power(t1,4)*(-161 + 211*t2 + 82*Power(t2,2)) + 
               Power(t1,3)*(-197 + 62*t2 + 301*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(754 + 757*t2 + 770*Power(t2,2) + 
                  129*Power(t2,3)) + 
               t1*(32 + 126*t2 + 139*Power(t2,2) + 178*Power(t2,3)) + 
               Power(s1,2)*(-119 + 4*Power(t1,6) + t1*(906 - 12*t2) - 
                  8*t2 - Power(t1,5)*(53 + 6*t2) + 
                  Power(t1,4)*(167 + 70*t2) + 
                  Power(t1,3)*(777 + 340*t2) + 
                  Power(t1,2)*(-537 + 426*t2)) - 
               s1*(-199 + 66*Power(t1,6) + Power(t1,5)*(59 - 134*t2) + 
                  236*t2 - 24*Power(t2,2) + 
                  Power(t1,4)*(-65 + 189*t2 + 56*Power(t2,2)) + 
                  t1*(879 + 103*t2 + 204*Power(t2,2)) + 
                  Power(t1,3)*(-378 + 955*t2 + 286*Power(t2,2)) + 
                  Power(t1,2)*(793 + 1251*t2 + 423*Power(t2,2)))) + 
            Power(s2,5)*(-230 + 22*Power(t1,6) + 
               Power(s1,3)*(-8 - 38*t1 - 249*Power(t1,2) - 
                  74*Power(t1,3) + 37*Power(t1,4)) - 95*t2 + 
               54*Power(t2,2) + 159*Power(t2,3) + 
               Power(t1,5)*(41 + 15*t2) - 
               Power(t1,4)*(313 + 135*t2 + 17*Power(t2,2)) + 
               Power(t1,3)*(41 - 109*t2 + 278*Power(t2,2) - 
                  12*Power(t2,3)) + 
               Power(t1,2)*(210 + 676*t2 + 57*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(229 + 209*t2 + 883*Power(t2,2) + 360*Power(t2,3)) + 
               Power(s1,2)*(8 - 6*Power(t1,5) + 50*t2 - 
                  Power(t1,4)*(113 + 3*t2) + Power(t1,3)*(53 + 46*t2) + 
                  2*t1*(63 + 236*t2) + Power(t1,2)*(957 + 637*t2)) - 
               s1*(4*Power(t1,6) - 7*Power(t1,5)*(-21 + 2*t2) + 
                  Power(t1,4)*(-118 - 229*t2 + 6*Power(t2,2)) + 
                  3*(-71 - 34*t2 + 78*Power(t2,2)) + 
                  Power(t1,3)*(-511 + 94*t2 + 100*Power(t2,2)) + 
                  2*Power(t1,2)*(404 + 779*t2 + 165*Power(t2,2)) + 
                  t1*(444 + 973*t2 + 737*Power(t2,2))))) + 
         Power(s,5)*(-5 - 91*t1 + 50*s1*t1 + 1068*Power(t1,2) - 
            481*s1*Power(t1,2) + 75*Power(s1,2)*Power(t1,2) - 
            717*Power(t1,3) + 3272*s1*Power(t1,3) - 
            521*Power(s1,2)*Power(t1,3) - 90*Power(s1,3)*Power(t1,3) - 
            1157*Power(t1,4) - 2174*s1*Power(t1,4) + 
            607*Power(s1,2)*Power(t1,4) - Power(s1,3)*Power(t1,4) + 
            1285*Power(t1,5) + 150*s1*Power(t1,5) - 
            193*Power(s1,2)*Power(t1,5) + 6*Power(s1,3)*Power(t1,5) - 
            316*Power(t1,6) + 104*s1*Power(t1,6) + 
            22*Power(s1,2)*Power(t1,6) + 16*Power(t1,7) - 
            6*s1*Power(t1,7) + 15*t2 + 494*t1*t2 - 225*s1*t1*t2 - 
            1261*Power(t1,2)*t2 + 45*s1*Power(t1,2)*t2 + 
            70*Power(s1,2)*Power(t1,2)*t2 + 486*Power(t1,3)*t2 - 
            97*s1*Power(t1,3)*t2 + 118*Power(s1,2)*Power(t1,3)*t2 - 
            879*Power(t1,4)*t2 + 52*s1*Power(t1,4)*t2 + 
            32*Power(s1,2)*Power(t1,4)*t2 + 512*Power(t1,5)*t2 - 
            58*s1*Power(t1,5)*t2 - 15*Power(s1,2)*Power(t1,5)*t2 - 
            60*Power(t1,6)*t2 + 4*s1*Power(t1,6)*t2 - 
            487*t1*Power(t2,2) + 196*s1*t1*Power(t2,2) + 
            608*Power(t1,2)*Power(t2,2) - 
            273*s1*Power(t1,2)*Power(t2,2) + 
            157*Power(t1,3)*Power(t2,2) - 82*s1*Power(t1,3)*Power(t2,2) - 
            108*Power(t1,4)*Power(t2,2) + 30*s1*Power(t1,4)*Power(t2,2) - 
            14*Power(t2,3) + 29*Power(t1,2)*Power(t2,3) + 
            10*Power(t1,3)*Power(t2,3) - 
            2*Power(s2,7)*(-20 + 24*Power(t1,3) + 
               s1*(15 - 42*t1 + 28*Power(t1,2)) - 21*t2 - 
               2*Power(t1,2)*(35 + 18*t2) + t1*(66 + 56*t2)) + 
            Power(s2,6)*(-66 + 119*Power(t1,4) + 
               4*Power(s1,2)*(-10 + 10*t1 + 7*Power(t1,2)) - 190*t2 - 
               77*Power(t2,2) - 3*Power(t1,2)*(59 + 70*t2) - 
               Power(t1,3)*(129 + 112*t2) + 
               t1*(244 + 470*t2 + 84*Power(t2,2)) + 
               s1*(54 + 203*Power(t1,3) + 110*t2 - 2*t1*(25 + 49*t2) - 
                  Power(t1,2)*(173 + 56*t2))) - 
            Power(s2,2)*(-184 + 14*Power(t1,7) + 
               Power(s1,3)*t1*
                (-68 + 377*t1 - 366*Power(t1,2) + 65*Power(t1,3) + 
                  4*Power(t1,4)) + 407*t2 - 376*Power(t2,2) - 
               64*Power(t2,3) + 8*Power(t1,6)*(19 + 3*t2) - 
               Power(t1,5)*(1513 + 525*t2) + 
               Power(t1,4)*(5941 + 1682*t2 + 217*Power(t2,2)) + 
               Power(t1,2)*(6487 + 701*t2 - 1528*Power(t2,2) - 
                  54*Power(t2,3)) - 
               Power(t1,3)*(9499 + 1170*t2 - 26*Power(t2,2) + 
                  48*Power(t2,3)) + 
               t1*(-1659 - 985*t2 + 1662*Power(t2,2) + 
                  156*Power(t2,3)) + 
               Power(s1,2)*(5 + 12*Power(t1,6) - 75*t2 + 
                  6*Power(t1,3)*(96 + 5*t2) + 
                  3*Power(t1,4)*(241 + 6*t2) - 
                  Power(t1,5)*(203 + 11*t2) - 
                  Power(t1,2)*(1852 + 199*t2) + t1*(971 + 205*t2)) + 
               s1*(-98*Power(t1,6) + 2*Power(t1,5)*(299 + 74*t2) - 
                  Power(t1,4)*(2205 + 886*t2 + 6*Power(t2,2)) + 
                  2*Power(t1,2)*(1057 + 698*t2 + 60*Power(t2,2)) + 
                  2*(19 + 57*t2 + 74*Power(t2,2)) + 
                  Power(t1,3)*(613 + 414*t2 + 136*Power(t2,2)) - 
                  2*t1*(617 + 706*t2 + 182*Power(t2,2)))) + 
            Power(s2,3)*(-437 + 88*Power(t1,6) + 
               Power(s1,3)*(-26 + 217*t1 - 338*Power(t1,2) + 
                  49*Power(t1,3) + 18*Power(t1,4)) - 242*t2 + 
               503*Power(t2,2) + 106*Power(t2,3) + 
               Power(t1,5)*(481 + 119*t2) - 
               Power(t1,4)*(3284 + 925*t2 + 125*Power(t2,2)) + 
               t1*(3310 + 685*t2 - 1818*Power(t2,2) - 252*Power(t2,3)) + 
               Power(t1,3)*(7649 + 1432*t2 + 64*Power(t2,2) + 
                  100*Power(t2,3)) + 
               Power(t1,2)*(-7803 - 487*t2 + 1874*Power(t2,2) + 
                  116*Power(t2,3)) + 
               Power(s1,2)*(60*Power(t1,5) - 
                  2*Power(t1,4)*(399 + 10*t2) + 9*(15 + 13*t2) + 
                  Power(t1,3)*(1785 + 124*t2) + 
                  Power(t1,2)*(-135 + 361*t2) - t1*(527 + 410*t2)) - 
               s1*(-129 + 8*Power(t1,6) + 443*t2 + 182*Power(t2,2) - 
                  5*Power(t1,5)*(-39 + 7*t2) + 
                  Power(t1,4)*(-1017 - 641*t2 + 20*Power(t2,2)) - 
                  9*t1*(-82 + 203*t2 + 50*Power(t2,2)) + 
                  2*Power(t1,2)*(-1133 + 844*t2 + 87*Power(t2,2)) + 
                  Power(t1,3)*(3185 + 1182*t2 + 252*Power(t2,2)))) + 
            Power(s2,5)*(-202 - 93*Power(t1,5) + 
               Power(s1,3)*(-26 - 29*t1 + 40*Power(t1,2)) - 257*t2 + 
               515*Power(t2,2) + 427*Power(t2,3) + 
               6*Power(t1,4)*(5 + 14*t2) + 
               Power(t1,3)*(1106 + 239*t2 - 98*Power(t2,2)) + 
               Power(t1,2)*(-1941 - 1337*t2 + 427*Power(t2,2)) + 
               t1*(1134 + 1304*t2 - 719*Power(t2,2) - 252*Power(t2,3)) + 
               Power(s1,2)*(-71 + 63*Power(t1,3) + 233*t2 - 
                  35*Power(t1,2)*(13 + 2*t2) + t1*(489 + 2*t2)) + 
               s1*(193 - 185*Power(t1,4) - 207*t2 - 588*Power(t2,2) + 
                  4*Power(t1,3)*(50 + 21*t2) + 
                  Power(t1,2)*(490 + 234*t2) + 
                  t1*(-753 - 230*t2 + 259*Power(t2,2)))) + 
            s2*(11 + 14*Power(t1,7) + 
               Power(s1,3)*Power(t1,2)*
                (28 + 213*t1 - 140*Power(t1,2) + 10*Power(t1,3)) - 
               143*t2 + 166*Power(t2,2) + 19*Power(t2,3) - 
               4*Power(t1,6)*(86 + 15*t2) + 
               5*Power(t1,5)*(434 + 139*t2) - 
               Power(t1,4)*(5228 + 1499*t2 + 187*Power(t2,2)) + 
               Power(t1,3)*(4845 + 1165*t2 + 10*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(-818 - 1080*t2 + 1155*Power(t2,2) + 
                  36*Power(t2,3)) - 
               t1*(984 - 1513*t2 + 1063*Power(t2,2) + 88*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-10*Power(t1,5) + t1*(1466 - 43*t2) + 
                  Power(t1,4)*(22 + 4*t2) - 30*(1 + 6*t2) + 
                  2*Power(t1,2)*(-965 + 14*t2) + 
                  Power(t1,3)*(600 + 22*t2)) - 
               s1*(10*Power(t1,7) - 6*Power(t1,6)*(21 + 2*t2) + 
                  Power(t1,5)*(635 + 149*t2) + 
                  Power(t1,4)*(422 - 505*t2 - 16*Power(t2,2)) + 
                  10*(2 - 10*t2 + 9*Power(t2,2)) + 
                  2*Power(t1,2)*(2324 + 409*t2 + 69*Power(t2,2)) + 
                  Power(t1,3)*(-4553 + 130*t2 + 100*Power(t2,2)) - 
                  t1*(291 + 305*t2 + 371*Power(t2,2)))) + 
            Power(s2,4)*(-409 + 20*Power(t1,6) + 
               Power(s1,3)*(-40 + 140*t1 + 31*Power(t1,2) - 
                  50*Power(t1,3)) - 226*t2 + 405*Power(t2,2) + 
               166*Power(t2,3) + Power(t1,5)*(-136 + 5*t2) + 
               Power(t1,4)*(-1071 + 5*t2 - 35*Power(t2,2)) + 
               t1*(2288 + 549*t2 - 2143*Power(t2,2) - 532*Power(t2,3)) + 
               Power(t1,2)*(-4646 - 1604*t2 + 1128*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(t1,3)*(3915 + 932*t2 - 235*Power(t2,2) + 
                  70*Power(t2,3)) + 
               Power(s1,2)*(17 - 130*Power(t1,4) + t1*(195 - 551*t2) + 
                  167*t2 + Power(t1,3)*(995 + 80*t2) - 
                  Power(t1,2)*(1527 + 101*t2)) + 
               s1*(268 + 50*Power(t1,5) - 422*t2 - 240*Power(t2,2) - 
                  5*Power(t1,4)*(9 + 4*t2) - 
                  Power(t1,3)*(902 + 565*t2 + 70*Power(t2,2)) + 
                  Power(t1,2)*(2292 + 271*t2 + 129*Power(t2,2)) + 
                  t1*(-1250 + 1937*t2 + 792*Power(t2,2))))))*T2q(s2,s))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*
       Power(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2),2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-s + s1 - t2)) - 
    (8*(4*Power(s,8)*(1 + s2)*(s2 - t1)*Power(t1,3) - 
         (-1 + s2)*Power(s2 - t1,4)*(-1 + t1)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) - 
         2*Power(s,7)*Power(t1,2)*
          (2*s2*t1*(7 + s1 - 10*t1 - s1*t1 + 2*Power(t1,2) - 3*t2) + 
            Power(s2,3)*(-6 + 7*t1 - t2) - 
            Power(s2,2)*(6 + 13*Power(t1,2) + t1*(-18 - 2*s1 + t2)) + 
            t1*(2 + Power(t1,3) - t2 + Power(t1,2)*(6 + t2) + 
               t1*(-1 - 2*s1 + 2*t2))) - 
         2*Power(s,6)*t1*(Power(s2,2)*t1*
             (-22*Power(t1,3) + s1*(6 - 22*t1 + 3*Power(t1,2)) + 
               Power(t1,2)*(68 + t2) + 
               t1*(-51 + 25*t2 - 3*Power(t2,2)) - 
               3*(-8 + 6*t2 + Power(t2,2))) + 
            Power(t1,2)*(-11 + Power(t1,4) + 10*t2 - 2*Power(t2,2) + 
               t1*(-32 + s1*(13 - 3*t2) + 3*t2) + 
               Power(t1,3)*(6 - 2*s1 + 5*t2) + 
               Power(t1,2)*(2*(2 + t2) - s1*(3 + t2))) - 
            Power(s2,4)*(9*Power(t1,2) + 2*(3 + t2) + 
               t1*(s1 - 4*(4 + t2))) + 
            Power(s2,3)*(-6 + 27*Power(t1,3) + Power(t2,2) - 
               Power(t1,2)*(53 + s1 + 2*t2) + 
               t1*(29 - 4*t2 + s1*(6 + t2))) + 
            s2*t1*(6 + 3*Power(t1,4) - 3*t2 + 
               Power(t1,2)*(23 + 19*s1 - 18*t2 + Power(t2,2)) + 
               Power(t1,3)*(s1 - 9*(4 + t2)) + 
               t1*(-5 + 42*t2 - 6*Power(t2,2) + s1*(-19 + 3*t2)))) + 
         Power(s,5)*(-2*Power(s2,5)*
             (-2 + 5*Power(t1,3) + Power(t1,2)*(-14 + 3*s1 - 6*t2) - 
               t2 + t1*(11 - 2*s1 + 6*t2)) + 
            Power(s2,4)*(4 + 47*Power(t1,4) + 
               Power(t1,3)*(-132 + 18*s1 - 25*t2) - 2*Power(t2,2) - 
               Power(t1,2)*(-102 + 2*s1 + 2*Power(s1,2) + t2 - 
                  4*s1*t2) + 
               t1*(-24 - 12*s1 + 18*t2 - 8*s1*t2 + 9*Power(t2,2))) + 
            Power(s2,3)*(-68*Power(t1,5) + Power(t2,3) + 
               Power(t1,4)*(253 - 19*s1 + 17*t2) + 
               Power(t1,3)*(-160 + 10*Power(s1,2) + 54*t2 - 
                  19*Power(t2,2) - s1*(42 + 5*t2)) + 
               Power(t1,2)*(18 - 137*t2 - 12*Power(t2,2) + 
                  s1*(86 + 15*t2)) + 
               t1*(-28 + 32*t2 + 12*Power(t2,2) - Power(t2,3) + 
                  s1*(-12 + Power(t2,2)))) - 
            Power(t1,3)*(46 + Power(t1,5) - 58*t2 + 20*Power(t2,2) - 
               Power(t2,3) + Power(t1,4)*(21 - 3*s1 + 8*t2) + 
               Power(t1,3)*(-8 + 2*Power(s1,2) - 11*s1*(-2 + t2) - 
                  13*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-208 - 2*Power(s1,2) + 45*t2 - 
                  5*Power(t2,2) + s1*(38 + t2)) + 
               t1*(204 - 74*t2 - 8*Power(t2,2) + Power(t2,3) - 
                  s1*(72 - 38*t2 + Power(t2,2)))) + 
            Power(s2,2)*t1*(34*Power(t1,5) + 
               Power(t1,4)*(-233 + 11*s1 - 25*t2) - 
               3*(4 - 2*t2 + Power(t2,3)) + 
               Power(t1,3)*(106 - 16*Power(s1,2) - 22*t2 + 
                  17*Power(t2,2) + s1*(52 + 9*t2)) + 
               Power(t1,2)*(160 + 2*Power(s1,2) + 277*t2 - 
                  42*Power(t2,2) + 2*Power(t2,3) + 
                  s1*(-192 + 13*t2 - 2*Power(t2,2))) - 
               3*t1*(6 + 58*t2 - 10*Power(t2,2) - Power(t2,3) + 
                  s1*(-26 + 6*t2 + Power(t2,2)))) + 
            s2*Power(t1,2)*(-2*Power(t1,5) + 
               Power(t1,4)*(105 - 7*s1 + 29*t2) + 
               Power(t1,3)*(-36 + 10*Power(s1,2) + s1*(10 - 19*t2) - 
                  28*t2 + 3*Power(t2,2)) + 
               3*(18 - 18*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               t1*(228 + 112*t2 - 82*Power(t2,2) + 7*Power(t2,3) + 
                  s1*(-138 + 56*t2 + Power(t2,2))) + 
               Power(t1,2)*(-378 - 4*Power(s1,2) - 89*t2 + 
                  24*Power(t2,2) + s1*(156 - 19*t2 + 2*Power(t2,2))))) + 
         Power(s,4)*(2*Power(s2,6)*(-1 + t1)*
             (2 - 3*t1 + Power(t1,2) + s1*(-1 + 3*t1) + 2*t2 - 4*t1*t2) \
+ Power(s2,5)*(2 + 2*Power(s1,2)*(-3 + t1)*t1 - 16*Power(t1,4) + 
               12*t2 + 7*Power(t2,2) - 
               s1*(4 - 37*Power(t1,2) + 29*Power(t1,3) + 
                  t1*(2 - 18*t2) + 6*t2) - 3*Power(t1,2)*(27 + 7*t2) + 
               Power(t1,3)*(73 + 31*t2) + 
               t1*(24 - 24*t2 - 13*Power(t2,2))) + 
            Power(s2,4)*(-4 + 36*Power(t1,5) + 8*t2 + Power(t2,2) - 
               7*Power(t2,3) - Power(t1,4)*(233 + 31*t2) + 
               Power(t1,2)*(59 + 154*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(201 - 9*t2 + 20*Power(t2,2)) + 
               t1*(-66 - 101*t2 - 15*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-13*Power(t1,2) + 2*t2 + t1*(41 + 2*t2)) + 
               s1*(53*Power(t1,4) - 2*Power(t1,3)*(15 + t2) - 
                  7*Power(t1,2)*(13 + 10*t2) + 
                  t1*(52 + 19*t2 - 5*Power(t2,2)) + 4*(-1 + Power(t2,2))\
)) + Power(t1,3)*(44 + Power(t1,4)*(49 + 6*s1*(-2 + t2) - 11*t2) + 
               Power(t1,5)*(-13 + 2*s1 - 3*t2) - 68*t2 + 
               27*Power(t2,2) - 2*Power(t2,3) - 
               Power(t1,2)*(470 - 253*t2 + 14*Power(t2,2) + 
                  Power(t2,3) + Power(s1,2)*(2 + t2) + 
                  4*s1*(-19 + 9*t2)) + 
               t1*(284 - 167*t2 - 36*Power(t2,2) + 6*Power(t2,3) + 
                  s1*(-104 + 87*t2 - 5*Power(t2,2))) - 
               Power(t1,3)*(-103 + Power(s1,2)*(-20 + t2) + 23*t2 - 
                  21*Power(t2,2) + Power(t2,3) - 
                  2*s1*(5 - 26*t2 + Power(t2,2)))) + 
            s2*Power(t1,2)*(-84 + 120*t2 - 39*Power(t2,2) + 
               Power(t1,5)*(83 - 8*s1 + 13*t2) + 
               Power(t1,4)*(-165 + s1 + 7*Power(s1,2) + 11*t2 - 
                  14*s1*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-438 + 53*t2 - 52*Power(t2,2) + 
                  3*Power(t2,3) + Power(s1,2)*(-85 + 3*t2) + 
                  s1*(107 + 114*t2)) + 
               t1*(-580 + 137*t2 + 195*Power(t2,2) - 11*Power(t2,3) + 
                  s1*(254 - 185*t2 - 3*Power(t2,2))) - 
               Power(t1,2)*(-1232 + Power(s1,2)*(-8 + t2) + 336*t2 + 
                  106*Power(t2,2) - 20*Power(t2,3) + 
                  s1*(290 - 139*t2 + 5*Power(t2,2)))) + 
            Power(s2,2)*t1*(10*Power(t1,6) + 
               Power(t1,5)*(-238 + 23*s1 - 10*t2) + 
               3*(14 - 16*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-1110 - 164*t2 + 235*Power(t2,2) - 
                  20*Power(t2,3) + Power(s1,2)*(-10 + 7*t2) + 
                  s1*(392 - 155*t2 + Power(t2,2))) + 
               Power(t1,4)*(-23*Power(s1,2) + s1*(54 + 8*t2) + 
                  4*(62 + t2 + 3*Power(t2,2))) - 
               Power(t1,3)*(-644 + Power(s1,2)*(-139 + t2) - 99*t2 + 
                  7*Power(t2,2) - 3*Power(t2,3) + 
                  s1*(333 + 106*t2 + 6*Power(t2,2))) + 
               3*t1*(114 + 51*t2 - 77*Power(t2,2) - 3*Power(t2,3) + 
                  s1*(-68 + 39*t2 + 7*Power(t2,2)))) + 
            Power(s2,3)*(-4 - 32*Power(t1,6) + 2*t2 + 3*Power(t2,2) - 
               2*Power(t2,3) + 
               Power(t1,4)*(-262 + 27*Power(s1,2) + 2*s1*(-21 + t2) + 
                  14*t2 - 34*Power(t2,2)) + 
               Power(t1,5)*(-47*s1 + 8*(42 + t2)) + 
               t1*(-48 - 113*t2 + 53*Power(t2,2) + 27*Power(t2,3) + 
                  s1*(58 - 19*t2 - 17*Power(t2,2))) + 
               Power(t1,3)*(-386 - 261*t2 + 49*Power(t2,2) - 
                  7*Power(t2,3) - Power(s1,2)*(109 + 3*t2) + 
                  s1*(307 + 96*t2 + 4*Power(t2,2))) + 
               Power(t1,2)*(418 + Power(s1,2)*(4 - 7*t2) + 318*t2 - 
                  89*Power(t2,2) - 10*Power(t2,3) + 
                  s1*(-226 + 39*t2 + 9*Power(t2,2))))) - 
         s*Power(s2 - t1,3)*(2*Power(-1 + s1,2)*Power(t1,6) + 
            2*Power(s2,5)*(-1 + t1)*Power(s1 - t2,2) - 
            Power(-1 + t2,3) + 2*t1*Power(-1 + t2,2)*(-5 + s1 + 3*t2) + 
            Power(t1,2)*(-1 + t2)*
             (-20 + Power(s1,2) + s1*(6 - 12*t2) + 24*t2 - 
               5*Power(t2,2)) + 
            (-1 + s1)*Power(t1,5)*
             (2 + Power(s1,2) + 7*t2 - s1*(1 + 3*t2)) + 
            Power(t1,4)*(1 + 4*Power(s1,3) + 13*t2 - 12*Power(t2,2) - 
               9*Power(s1,2)*(1 + t2) + 2*s1*(2 + t2 + 3*Power(t2,2))) + 
            Power(t1,3)*(-12 - 2*Power(s1,3) + 15*t2 + 2*Power(t2,2) - 
               3*Power(t2,3) + 2*Power(s1,2)*(5 + t2) + 
               s1*(1 - 26*t2 + 13*Power(t2,2))) + 
            Power(s2,3)*(3 + Power(s1,3)*(2 - 7*t1 - 7*Power(t1,2)) - 
               9*t2 - 7*Power(t2,2) + 11*Power(t2,3) + 
               Power(t1,3)*t2*(3 + 8*t2 - Power(t2,2)) + 
               Power(t1,4)*(-1 + Power(t2,2)) - 
               Power(t1,2)*(-6 + 15*t2 + 23*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-8 + 21*t2 + 21*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,3)*(-9 + t2) + 3*(-5 + t2) + 
                  9*t1*(2 + 3*t2) + Power(t1,2)*(6 + 5*t2)) + 
               s1*(-3 + Power(t1,3)*(3 - 5*t2) - 
                  2*Power(t1,4)*(-1 + t2) + 31*t2 - 16*Power(t2,2) + 
                  t1*(13 - 65*t2 - 23*Power(t2,2)) + 
                  Power(t1,2)*(-15 + 41*t2 + 3*Power(t2,2)))) + 
            Power(s2,4)*(Power(s1,3)*(1 + 2*t1) + 
               Power(s1,2)*(1 + t1 - 2*Power(t1,2) - 10*t2 + t1*t2) + 
               s1*(-1 + Power(t1,3)*(-1 + t2) + 5*t2 + 
                  17*Power(t2,2) + Power(t1,2)*(1 + 9*t2) + 
                  t1*(1 - 15*t2 - 8*Power(t2,2))) + 
               t2*(1 - Power(t1,3)*(-1 + t2) - 6*t2 - 8*Power(t2,2) - 
                  Power(t1,2)*(1 + 7*t2) + 
                  t1*(-1 + 14*t2 + 5*Power(t2,2)))) - 
            s2*(Power(s1,3)*Power(t1,2)*(-6 + 13*t1 + 5*Power(t1,2)) + 
               2*Power(t1,5)*(1 + t2) + Power(-1 + t2,2)*(-7 + 3*t2) - 
               Power(t1,4)*(4 + 11*Power(t2,2)) + 
               Power(t1,2)*(-17 + 27*t2 + 5*Power(t2,2) - 
                  9*Power(t2,3)) + 
               Power(t1,3)*(7 - 9*t2 - 5*Power(t2,3)) - 
               t1*(-19 + 37*t2 - 19*Power(t2,2) + Power(t2,3)) - 
               Power(s1,2)*t1*
                (2 + Power(t1,4)*(-11 + t2) - 2*t2 - t1*(35 + t2) + 
                  Power(t1,3)*(6 + 13*t2) + Power(t1,2)*(38 + 25*t2)) + 
               s1*(2*Power(-1 + t2,2) - Power(t1,5)*(13 + t2) + 
                  t1*(1 + 10*t2 - 11*Power(t2,2)) + 
                  Power(t1,4)*(12 + 11*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*(-22 - 29*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*(20 + 13*t2 + 24*Power(t2,2)))) + 
            Power(s2,2)*(2 + 3*Power(s1,3)*t1*
                (-2 + 5*t1 + 3*Power(t1,2)) + Power(t1,4)*(1 - 3*t2) - 
               Power(t1,5)*(-1 + t2) - 2*t2 - Power(t2,2) + 
               Power(t2,3) + t1*
                (-10 + 27*t2 + 4*Power(t2,2) - 15*Power(t2,3)) + 
               Power(t1,2)*(17 - 53*t2 + 6*Power(t2,2) - 
                  3*Power(t2,3)) - 
               Power(t1,3)*(11 - 32*t2 + 9*Power(t2,2) + Power(t2,3)) - 
               Power(s1,2)*(1 + 4*t1*(-10 + t2) + 
                  2*Power(t1,4)*(-9 + t2) - t2 + 
                  Power(t1,3)*(9 + 16*t2) + Power(t1,2)*(48 + 33*t2)) + 
               s1*(7 + Power(t1,5)*(-1 + t2) - 8*t2 + Power(t2,2) - 
                  Power(t1,4)*(13 + t2) + 
                  2*t1*(-10 - 17*t2 + 9*Power(t2,2)) + 
                  Power(t1,3)*(23 - 29*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(4 + 71*t2 + 24*Power(t2,2))))) - 
         Power(s,3)*(s2 - t1)*
          (2*Power(s2,6)*Power(-1 + t1,2)*(s1 - t2) - 
            Power(s2,5)*(2 + Power(t1,4) + 
               Power(s1,2)*(-4 + 5*t1 + 2*Power(t1,2)) - 11*t2 - 
               6*Power(t2,2) + Power(t1,2)*(31 + 9*t2) - 
               Power(t1,3)*(17 + 11*t2) + 
               t1*(-17 + 9*t2 + 5*Power(t2,2)) + 
               2*s1*(1 + 7*Power(t1,3) - 4*t1*(-1 + t2) + 5*t2 - 
                  2*Power(t1,2)*(5 + t2))) + 
            Power(s2,4)*(22 + 3*Power(t1,5) - 
               Power(s1,3)*t1*(1 + 2*t1) + 9*t2 - 18*Power(t2,2) - 
               17*Power(t2,3) - Power(t1,4)*(75 + 8*t2) + 
               Power(t1,2)*(20 + 68*t2 - 14*Power(t2,2)) + 
               Power(t1,3)*(109 - 13*t2 + 6*Power(t2,2)) + 
               t1*(-79 - 79*t2 + 15*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s1,2)*(2*Power(t1,3) + t1*(-37 + t2) - 5*t2 + 
                  Power(t1,2)*(38 + 4*t2)) + 
               s1*(-6 + 33*Power(t1,4) + 6*t2 + 19*Power(t2,2) - 
                  2*Power(t1,3)*(5 + 7*t2) - Power(t1,2)*(75 + 43*t2) + 
                  t1*(81 + 59*t2 - 9*Power(t2,2)))) + 
            s2*t1*(32 - 52*t2 + 26*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,6)*(9 - 3*s1 + 2*t2) + 
               Power(t1,2)*(-955 + 755*t2 + 160*Power(t2,2) - 
                  13*Power(t2,3) + 5*Power(s1,2)*(3 + t2) + 
                  s1*(121 - 268*t2 - 10*Power(t2,2))) + 
               t1*(400 - 205*t2 - 112*Power(t2,2) + 13*Power(t2,3) + 
                  s1*(-166 + 141*t2 - 2*Power(t2,2))) + 
               Power(t1,5)*(7*Power(s1,2) - s1*(32 + t2) - 
                  3*(23 - 17*t2 + Power(t2,2))) + 
               Power(t1,4)*(-49 + 5*Power(s1,3) - 90*t2 - 
                  43*Power(t2,2) + 3*Power(t2,3) - 
                  Power(s1,2)*(64 + 5*t2) + 
                  s1*(162 + 63*t2 + 4*Power(t2,2))) + 
               Power(t1,3)*(632 + 7*Power(s1,3) + 
                  Power(s1,2)*(115 - 16*t2) - 241*t2 + 53*Power(t2,2) + 
                  7*Power(t2,3) + s1*(-302 - 89*t2 + 8*Power(t2,2)))) - 
            Power(t1,2)*(16 - 23*t2 + 7*Power(t2,2) + 
               Power(t1,5)*(-18 + Power(s1,2) + 13*t2 - 3*s1*t2) + 
               t1*(215 - 174*t2 - 14*Power(t2,2) + 5*Power(t2,3) + 
                  s1*(-82 + 82*t2 - 9*Power(t2,2))) + 
               Power(t1,4)*(20 + Power(s1,3) - 50*t2 - 6*Power(t2,2) - 
                  2*Power(s1,2)*(6 + t2) + s1*(8 + 32*t2 + Power(t2,2))\
) + Power(t1,2)*(-410 + Power(s1,2)*(8 - 3*t2) + 393*t2 + 
                  3*Power(t2,2) - 5*Power(t2,3) + 
                  s1*(40 - 92*t2 + 6*Power(t2,2))) + 
               Power(t1,3)*(177 + 2*Power(s1,3) + 
                  Power(s1,2)*(28 - 3*t2) - 78*t2 + 25*Power(t2,2) - 
                  3*Power(t2,3) + s1*(-47 - 59*t2 + 10*Power(t2,2)))) + 
            Power(s2,3)*(22 - 3*Power(t1,6) + 
               Power(s1,3)*Power(t1,2)*(5 + 7*t1) + 
               Power(t1,5)*(108 - 7*t2) + 29*t2 - 12*Power(t2,2) - 
               7*Power(t2,3) + 
               Power(t1,4)*(-158 + 62*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(408 + 106*t2 - 75*Power(t2,2) - 
                  9*Power(t2,3)) - 
               Power(t1,3)*(180 + 103*t2 - 36*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(-197 + 5*t2 + 136*Power(t2,2) + 49*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-1 + 7*Power(t1,3) + t1*(121 - 12*t2) + 21*t2 - 
                  Power(t1,2)*(101 + 9*t2)) + 
               s1*(-14 - 35*Power(t1,5) + t2 + 4*Power(t2,2) + 
                  7*Power(t1,4)*(-8 + 3*t2) + 
                  t1*(59 - 108*t2 - 54*Power(t2,2)) + 
                  Power(t1,3)*(312 + 61*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(-358 - 71*t2 + 12*Power(t2,2)))) + 
            Power(s2,2)*(-10 + Power(t1,7) + 11*t2 - Power(t2,2) + 
               Power(t1,6)*(-59 + 17*s1 + 4*t2) + 
               Power(t1,5)*(131 - 13*Power(s1,2) + s1*(82 - 13*t2) - 
                  82*t2 + 12*Power(t2,2)) + 
               Power(t1,3)*(-772 - 9*Power(s1,3) + 95*t2 + 
                  56*Power(t2,2) - 21*Power(t2,3) + 
                  Power(s1,2)*(-175 + 24*t2) + 
                  s1*(534 + 52*t2 - Power(t2,2))) + 
               Power(t1,4)*(218 - 9*Power(s1,3) + 68*t2 + 
                  38*Power(t2,2) + 8*Power(s1,2)*(15 + t2) - 
                  s1*(385 + 57*t2 + 5*Power(t2,2))) - 
               t1*(197 + 28*t2 - 140*Power(t2,2) + 11*Power(t2,3) + 
                  s1*(-98 + 60*t2 + 11*Power(t2,2))) + 
               Power(t1,2)*(688 - 280*t2 - 371*Power(t2,2) + 
                  8*Power(t2,3) - 6*Power(s1,2)*(1 + 4*t2) + 
                  s1*(-134 + 278*t2 + 51*Power(t2,2))))) - 
         Power(s,2)*Power(s2 - t1,2)*
          (Power(s2,5)*(2 + Power(s1,2)*(-1 - 3*t1 + 2*Power(t1,2)) - 
               Power(t1,2)*(-6 + t2) + t2 + Power(t2,2) - 
               Power(t1,3)*(2 + t2) + t1*(-6 + t2 - 3*Power(t2,2)) + 
               s1*(1 + 3*Power(t1,3) - Power(t1,2)*(1 + 2*t2) + 
                  t1*(-3 + 6*t2))) + 
            Power(s2,4)*(-13 + Power(s1,3)*(-2 - 3*t1 + 2*Power(t1,2)) - 
               11*t2 + 25*Power(t2,2) + 18*Power(t2,3) + 
               Power(t1,4)*(8 + t2) + 
               Power(t1,3)*(-17 + 8*t2 + 2*Power(t2,2)) + 
               3*Power(t1,2)*(-1 - 7*t2 + 6*Power(t2,2)) + 
               t1*(25 + 23*t2 - 30*Power(t2,2) - 10*Power(t2,3)) + 
               Power(s1,2)*(-5 - 3*Power(t1,3) + 13*t2 - 
                  2*Power(t1,2)*t2 + t1*(23 + 3*t2)) - 
               s1*(-23 + 9*Power(t1,4) + Power(t1,3)*(2 - 6*t2) + 5*t2 + 
                  29*Power(t2,2) + Power(t1,2)*(-45 + 13*t2) + 
                  t1*(57 + 18*t2 - 10*Power(t2,2)))) + 
            t1*(-2*Power(t1,7) - Power(-1 + t2,2)*(-3 + 2*t2) + 
               Power(t1,6)*(4 - 2*s1 + 4*t2) + 
               t1*(-1 + t2)*(78 - 28*t2 + 5*Power(t2,2) + 
                  s1*(-24 + 7*t2)) - 
               Power(t1,5)*(1 + 6*Power(s1,2) + 23*t2 + 2*Power(t2,2) - 
                  s1*(17 + 9*t2)) + 
               Power(t1,4)*(2 - Power(s1,3) + 43*t2 + 4*Power(t2,2) + 
                  Power(s1,2)*(19 + 6*t2) + 
                  s1*(-25 - 55*t2 + Power(t2,2))) - 
               Power(t1,3)*(79 + 6*Power(s1,3) - 84*t2 - 
                  28*Power(t2,2) + Power(t2,3) - 
                  Power(s1,2)*(23 + 7*t2) + 
                  4*s1*(-1 + 10*t2 + Power(t2,2))) + 
               Power(t1,2)*(151 + 3*Power(s1,2)*(-1 + t2) - 206*t2 + 
                  29*Power(t2,2) - 3*s1*(6 - 17*t2 + 5*Power(t2,2)))) + 
            s2*(6*Power(t1,7) - Power(-1 + t2,2)*(-3 + 4*t2) + 
               t1*(-1 + t2)*(-118 + s1*(41 - 7*t2) + t2 + 
                  7*Power(t2,2)) + 
               Power(t1,6)*(-3*Power(s1,2) + s1*(10 + t2) - 
                  2*(9 + 7*t2)) + 
               Power(t1,5)*(29 - 2*Power(s1,3) + 73*t2 + 
                  6*Power(t2,2) + Power(s1,2)*(28 + t2) - 
                  s1*(113 + 3*t2 + 3*Power(t2,2))) + 
               Power(t1,4)*(-87 + 6*Power(s1,3) - 22*t2 - 
                  58*Power(t2,2) - 2*Power(s1,2)*(41 + 8*t2) + 
                  s1*(193 + 97*t2 + 9*Power(t2,2))) + 
               Power(t1,2)*(-304 - 4*Power(s1,2)*(-1 + t2) + 412*t2 - 
                  24*Power(t2,2) - 6*Power(t2,3) + 
                  s1*(17 - 84*t2 + 13*Power(t2,2))) + 
               Power(t1,3)*(253 + 20*Power(s1,3) - 320*t2 - 
                  37*Power(t2,2) - 11*Power(t2,3) - 
                  Power(s1,2)*(55 + 43*t2) + 
                  s1*(-66 + 157*t2 + 40*Power(t2,2)))) + 
            Power(s2,3)*(2*Power(s1,3)*t1*(6 + 5*t1 - 3*Power(t1,2)) + 
               Power(t1,5)*(-8 + t2) + 
               Power(t1,4)*(12 - 31*t2 + 2*Power(t2,2)) - 
               2*(9 - 18*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-140 + t2 + 38*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,3)*(59 + 55*t2 - 27*Power(t2,2) + 
                  5*Power(t2,3)) - 
               t1*(-95 + 62*t2 + 89*Power(t2,2) + 33*Power(t2,3)) + 
               Power(s1,2)*(t1 - 3*Power(t1,4) + 2*(-1 + t2) - 
                  55*t1*t2 + 5*Power(t1,3)*(5 + t2) - 
                  Power(t1,2)*(87 + 10*t2)) + 
               s1*(3 + 9*Power(t1,5) + Power(t1,4)*(13 - 5*t2) - 26*t2 + 
                  5*Power(t2,2) + Power(t1,3)*(-157 + 17*t2) + 
                  Power(t1,2)*(256 + 19*t2 - 7*Power(t2,2)) + 
                  t1*(-124 + 127*t2 + 70*Power(t2,2)))) + 
            Power(s2,2)*(-40 + 
               6*Power(s1,3)*Power(t1,2)*(-4 - 2*t1 + Power(t1,2)) + 
               13*t2 + 39*Power(t2,2) - 12*Power(t2,3) - 
               Power(t1,6)*(2 + t2) + 
               Power(t1,5)*(13 + 34*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-226 + 219*t2 + 163*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,4)*(-84 - 67*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(198 - 46*t2 + 45*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(141 - 152*t2 - 105*Power(t2,2) + 38*Power(t2,3)) + 
               Power(s1,2)*t1*
                (1 + 7*Power(t1,4) - t2 - 4*Power(t1,3)*(11 + t2) + 
                  6*t1*(6 + 13*t2) + Power(t1,2)*(128 + 17*t2)) - 
               s1*(18*Power(t1,5) + 3*Power(t1,6) + 17*(-1 + t2) + 
                  Power(t1,4)*(-211 + 16*t2 - 3*Power(t2,2)) + 
                  t1*(2 - 59*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*(368 + 43*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*(-163 + 239*t2 + 77*Power(t2,2))))))*
       T3q(s2,t1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*Power(s2 - t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-s + s1 - t2)) + 
    (8*(8*Power(s,8)*(-1 + t1)*Power(t1,3) + 
         (-1 + s2)*(s2 - t1)*Power(-1 + t1,4)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         2*Power(s,7)*Power(t1,2)*
          (Power(t1,4) - 4*s2*(3 - 7*t1 + 4*Power(t1,2)) + 
            t1*(9 + 8*s1 - 8*t2) - 2*Power(t1,2)*(7 + 4*s1 - t2) - t2 + 
            Power(t1,3)*(10 + t2)) + 
         2*Power(s,6)*t1*(Power(t1,6) + 
            12*Power(s2,2)*Power(-1 + t1,2)*(-1 + 2*t1) + Power(t2,2) + 
            Power(t1,5)*(12 - 2*s1 + 5*t2) + 
            t1*(-1 + (2 + s1)*t2 - 3*Power(t2,2)) + 
            Power(t1,3)*(-16 + 4*Power(s1,2) - s1*(-31 + t2) - 13*t2 + 
               Power(t2,2)) - Power(t1,4)*(5 + 3*t2 + s1*(23 + t2)) + 
            Power(t1,2)*(9 - 4*Power(s1,2) + 27*t2 - 11*Power(t2,2) + 
               s1*(-24 + 13*t2)) + 
            s2*(-3*Power(t1,5) + t1*(16 + 23*s1 - 23*t2) + 
               Power(t1,4)*(-29 + s1 - 4*t2) + 
               Power(t1,3)*(56 + 22*s1 - 3*t2) - 2*t2 + 
               Power(t1,2)*(-40 - 52*s1 + 38*t2))) + 
         Power(s,5)*(Power(t1,8) - 
            8*Power(s2,3)*Power(-1 + t1,3)*(-1 + 4*t1) - Power(t2,3) + 
            Power(t1,7)*(28 - 3*s1 + 8*t2) + 
            t1*t2*(4 - (6 + s1)*t2 + 4*Power(t2,2)) + 
            Power(t1,6)*(-18 + 2*Power(s1,2) - 13*t2 + 3*Power(t2,2) - 
               s1*(57 + 11*t2)) + 
            Power(t1,5)*(-106 + 24*Power(s1,2) + 34*t2 - 
               8*Power(t2,2) + s1*(39 + 16*t2)) + 
            2*Power(s2,2)*(-1 + t1)*
             (-((33 + 16*s1)*Power(t1,3)) + 3*Power(t1,5) + 
               Power(t1,2)*(3 + 62*s1 - 53*t2) + t2 + 
               Power(t1,4)*(32 - 3*s1 + 6*t2) + t1*(-5 - 22*s1 + 25*t2)) \
- Power(t1,3)*(66 + 60*t2 - 52*Power(t2,2) + 10*Power(t2,3) + 
               2*Power(s1,2)*(-17 + 5*t2) + 
               s1*(10 + 72*t2 - 21*Power(t2,2))) + 
            Power(t1,4)*(155 + 22*t2 - 50*Power(t2,2) + Power(t2,3) - 
               2*Power(s1,2)*(24 + t2) + s1*(33 + 52*t2 + Power(t2,2))) \
+ Power(t1,2)*(6 + 5*t2 + 21*Power(t2,2) - 6*Power(t2,3) + 
               s1*(-2 - 9*t2 + 3*Power(t2,2))) + 
            s2*(-5*Power(t1,7) + Power(t1,6)*(-59 + 10*s1 - 29*t2) + 
               2*Power(t2,2) + t1*(-4 + 8*s1*t2 - 15*Power(t2,2)) + 
               Power(t1,5)*(31 - 2*Power(s1,2) + 52*t2 + 
                  4*s1*(24 + t2)) - 
               Power(t1,4)*(-205 + 2*Power(s1,2) - 20*t2 + 
                  3*Power(t2,2) + 4*s1*(55 + 3*t2)) + 
               Power(t1,3)*(-246 + 50*Power(s1,2) - 176*t2 + 
                  91*Power(t2,2) - 4*s1*(-52 + 29*t2)) + 
               Power(t1,2)*(78 - 22*Power(s1,2) + 133*t2 - 
                  51*Power(t2,2) + s1*(-94 + 68*t2)))) + 
         s*Power(-1 + t1,3)*(2*Power(-1 + s1,2)*Power(t1,6) + 
            2*Power(s2,5)*(-1 + t1)*Power(s1 - t2,2) - 
            Power(-1 + t2,3) + 2*t1*Power(-1 + t2,2)*(-5 + s1 + 3*t2) + 
            Power(t1,2)*(-1 + t2)*
             (-20 + Power(s1,2) + s1*(6 - 12*t2) + 24*t2 - 
               5*Power(t2,2)) + 
            (-1 + s1)*Power(t1,5)*
             (2 + Power(s1,2) + 7*t2 - s1*(1 + 3*t2)) + 
            Power(t1,4)*(1 + 4*Power(s1,3) + 13*t2 - 12*Power(t2,2) - 
               9*Power(s1,2)*(1 + t2) + 2*s1*(2 + t2 + 3*Power(t2,2))) + 
            Power(t1,3)*(-12 - 2*Power(s1,3) + 15*t2 + 2*Power(t2,2) - 
               3*Power(t2,3) + 2*Power(s1,2)*(5 + t2) + 
               s1*(1 - 26*t2 + 13*Power(t2,2))) + 
            Power(s2,3)*(3 + Power(s1,3)*(2 - 7*t1 - 7*Power(t1,2)) - 
               9*t2 - 7*Power(t2,2) + 11*Power(t2,3) + 
               Power(t1,3)*t2*(3 + 8*t2 - Power(t2,2)) + 
               Power(t1,4)*(-1 + Power(t2,2)) - 
               Power(t1,2)*(-6 + 15*t2 + 23*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-8 + 21*t2 + 21*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s1,2)*(Power(t1,3)*(-9 + t2) + 3*(-5 + t2) + 
                  9*t1*(2 + 3*t2) + Power(t1,2)*(6 + 5*t2)) + 
               s1*(-3 + Power(t1,3)*(3 - 5*t2) - 
                  2*Power(t1,4)*(-1 + t2) + 31*t2 - 16*Power(t2,2) + 
                  t1*(13 - 65*t2 - 23*Power(t2,2)) + 
                  Power(t1,2)*(-15 + 41*t2 + 3*Power(t2,2)))) + 
            Power(s2,4)*(Power(s1,3)*(1 + 2*t1) + 
               Power(s1,2)*(1 + t1 - 2*Power(t1,2) - 10*t2 + t1*t2) + 
               s1*(-1 + Power(t1,3)*(-1 + t2) + 5*t2 + 
                  17*Power(t2,2) + Power(t1,2)*(1 + 9*t2) + 
                  t1*(1 - 15*t2 - 8*Power(t2,2))) + 
               t2*(1 - Power(t1,3)*(-1 + t2) - 6*t2 - 8*Power(t2,2) - 
                  Power(t1,2)*(1 + 7*t2) + 
                  t1*(-1 + 14*t2 + 5*Power(t2,2)))) - 
            s2*(Power(s1,3)*Power(t1,2)*(-6 + 13*t1 + 5*Power(t1,2)) + 
               2*Power(t1,5)*(1 + t2) + Power(-1 + t2,2)*(-7 + 3*t2) - 
               Power(t1,4)*(4 + 11*Power(t2,2)) + 
               Power(t1,2)*(-17 + 27*t2 + 5*Power(t2,2) - 
                  9*Power(t2,3)) + 
               Power(t1,3)*(7 - 9*t2 - 5*Power(t2,3)) - 
               t1*(-19 + 37*t2 - 19*Power(t2,2) + Power(t2,3)) - 
               Power(s1,2)*t1*
                (2 + Power(t1,4)*(-11 + t2) - 2*t2 - t1*(35 + t2) + 
                  Power(t1,3)*(6 + 13*t2) + Power(t1,2)*(38 + 25*t2)) + 
               s1*(2*Power(-1 + t2,2) - Power(t1,5)*(13 + t2) + 
                  t1*(1 + 10*t2 - 11*Power(t2,2)) + 
                  Power(t1,4)*(12 + 11*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*(-22 - 29*t2 + 15*Power(t2,2)) + 
                  Power(t1,3)*(20 + 13*t2 + 24*Power(t2,2)))) + 
            Power(s2,2)*(2 + 3*Power(s1,3)*t1*
                (-2 + 5*t1 + 3*Power(t1,2)) + Power(t1,4)*(1 - 3*t2) - 
               Power(t1,5)*(-1 + t2) - 2*t2 - Power(t2,2) + 
               Power(t2,3) + t1*
                (-10 + 27*t2 + 4*Power(t2,2) - 15*Power(t2,3)) + 
               Power(t1,2)*(17 - 53*t2 + 6*Power(t2,2) - 
                  3*Power(t2,3)) - 
               Power(t1,3)*(11 - 32*t2 + 9*Power(t2,2) + Power(t2,3)) - 
               Power(s1,2)*(1 + 4*t1*(-10 + t2) + 
                  2*Power(t1,4)*(-9 + t2) - t2 + 
                  Power(t1,3)*(9 + 16*t2) + Power(t1,2)*(48 + 33*t2)) + 
               s1*(7 + Power(t1,5)*(-1 + t2) - 8*t2 + Power(t2,2) - 
                  Power(t1,4)*(13 + t2) + 
                  2*t1*(-10 - 17*t2 + 9*Power(t2,2)) + 
                  Power(t1,3)*(23 - 29*t2 + 11*Power(t2,2)) + 
                  Power(t1,2)*(4 + 71*t2 + 24*Power(t2,2))))) + 
         Power(s,4)*(8*Power(s2,4)*Power(-1 + t1,4) + 
            Power(t2,2)*(-3 + 2*t2) + 
            t1*(2 + (-11 + s1)*t2 + (7 + 5*s1)*Power(t2,2) - 
               12*Power(t2,3)) + Power(t1,8)*(-2*s1 + 3*(5 + t2)) - 
            2*Power(t1,7)*(12 - 7*t2 + 3*s1*(9 + t2)) - 
            2*Power(s2,3)*Power(-1 + t1,2)*
             (2 + Power(t1,4) + 
               s1*(-7 + 32*t1 + 5*Power(t1,2) - 3*Power(t1,3)) + 
               Power(t1,2)*(6 - 8*t2) + 9*t2 + 
               Power(t1,3)*(13 + 4*t2) - 2*t1*(11 + 16*t2)) + 
            Power(t1,2)*(2 - 12*t2 - 7*Power(t2,2) + 25*Power(t2,3) + 
               Power(s1,2)*(2 + t2) + s1*(-2 + 3*t2 - 15*Power(t2,2))) + 
            Power(t1,6)*(-11 - 15*t2 - 15*Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(32 + t2) + s1*(28 + 28*t2 - 2*Power(t2,2))) \
+ Power(t1,5)*(19 + 2*Power(s1,3) - 30*t2 - 7*Power(t2,2) - 
               2*Power(t2,3) - 2*Power(s1,2)*(37 + 7*t2) + 
               2*s1*(134 - 11*t2 + 6*Power(t2,2))) - 
            Power(t1,3)*(25 + 4*Power(s1,3) + 
               Power(s1,2)*(20 - 18*t2) - 51*t2 + 38*Power(t2,2) + 
               2*Power(t2,3) + s1*(-158 - 19*t2 + 23*Power(t2,2))) + 
            Power(t1,4)*(22 + 14*Power(s1,3) + 
               Power(s1,2)*(60 - 42*t2) + 63*Power(t2,2) - 
               24*Power(t2,3) + s1*(-396 - 23*t2 + 59*Power(t2,2))) + 
            Power(s2,2)*(-1 + t1)*
             (2 + 4*Power(t1,6) + 
               Power(t1,4)*(23 - 59*s1 + 2*Power(s1,2) - 69*t2) + 
               (4 - 6*s1)*t2 + 7*Power(t2,2) + 
               Power(t1,5)*(53 - 9*s1 + 27*t2) + 
               t1*(-78 + 18*Power(s1,2) + s1*(52 - 66*t2) - 98*t2 + 
                  57*Power(t2,2)) + 
               Power(t1,3)*(-295 - 38*Power(s1,2) + 57*t2 - 
                  5*Power(t2,2) + s1*(107 + 50*t2)) + 
               Power(t1,2)*(291 - 58*Power(s1,2) + 79*t2 - 
                  135*Power(t2,2) + s1*(-91 + 174*t2))) - 
            s2*(2*Power(t1,8) + Power(t1,7)*(54 - 6*s1 + 16*t2) + 
               t2*(-4 + t2 + 4*s1*t2 - 7*Power(t2,2)) + 
               t1*(-4 + (-23 + 19*s1 + 2*Power(s1,2))*t2 - 
                  (15 + 17*s1)*Power(t2,2) + 26*Power(t2,3)) + 
               Power(t1,5)*(-225 - 2*Power(s1,2)*(-3 + t2) + 150*t2 - 
                  49*Power(t2,2) + 44*s1*(3 + 2*t2)) + 
               Power(t1,6)*(-70 + Power(s1,2) - 34*t2 + 8*Power(t2,2) - 
                  s1*(115 + 14*t2)) + 
               Power(t1,2)*(103 + 91*t2 - 106*Power(t2,2) + 
                  6*Power(t2,3) + Power(s1,2)*(-67 + 26*t2) + 
                  s1*(107 + 125*t2 - 45*Power(t2,2))) + 
               Power(t1,4)*(529 + 10*Power(s1,3) - 229*t2 - 
                  89*Power(t2,2) + 5*Power(t2,3) - 
                  2*Power(s1,2)*(56 + 9*t2) + 
                  s1*(212 + 89*t2 + 7*Power(t2,2))) + 
               Power(t1,3)*(-389 + 2*Power(s1,3) + 
                  Power(s1,2)*(172 - 44*t2) + 33*t2 + 250*Power(t2,2) - 
                  42*Power(t2,3) + s1*(-330 - 307*t2 + 87*Power(t2,2))))) \
- Power(s,2)*Power(-1 + t1,2)*
          (1 + 9*t1 - 55*Power(t1,2) + 108*Power(t1,3) - 
            95*Power(t1,4) + 29*Power(t1,5) + 11*Power(t1,6) - 
            10*Power(t1,7) + 2*Power(t1,8) + 
            Power(s1,3)*Power(s2 - t1,2)*
             (t1*(6 - 31*t1 - 5*Power(t1,2)) + 
               s2*(2 + 7*t1 + 19*Power(t1,2) + 2*Power(t1,3))) - 
            6*Power(s2,5)*Power(-1 + t1,2)*t2 - 30*t1*t2 + 
            84*Power(t1,2)*t2 - 78*Power(t1,3)*t2 + 27*Power(t1,4)*t2 - 
            10*Power(t1,5)*t2 + 11*Power(t1,6)*t2 - 4*Power(t1,7)*t2 - 
            3*Power(t2,2) + 28*t1*Power(t2,2) - 
            52*Power(t1,2)*Power(t2,2) + 9*Power(t1,3)*Power(t2,2) + 
            16*Power(t1,4)*Power(t2,2) + 2*Power(t1,6)*Power(t2,2) + 
            2*Power(t2,3) - 7*t1*Power(t2,3) + 
            5*Power(t1,2)*Power(t2,3) + 23*Power(t1,3)*Power(t2,3) + 
            7*Power(t1,4)*Power(t2,3) + 
            Power(s2,4)*(-1 + t1)*
             (Power(t1,3)*(-2 + t2) + 25*t2*(1 + t2) + 
               Power(t1,2)*(4 + 13*t2) + t1*(-2 - 39*t2 + 9*Power(t2,2))\
) + Power(s2,3)*(-5 + 2*Power(t1,5) - 37*t2 - 19*Power(t2,2) - 
               4*Power(t2,3) + Power(t1,4)*(-7 + 3*t2) + 
               Power(t1,3)*(14 + 61*t2 - 41*Power(t2,2)) + 
               t1*(16 + 141*t2 + 50*Power(t2,2) - 36*Power(t2,3)) + 
               2*Power(t1,2)*
                (-10 - 84*t2 + 5*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s2,2)*(-(Power(t1,6)*(-2 + t2)) + 
               Power(t1,5)*(-13 - 25*t2 + 2*Power(t2,2)) - 
               2*(7 - 3*t2 + 5*Power(t2,2)) + 
               Power(t1,4)*(-7 + 31*t2 + 34*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,3)*(93 + 76*t2 - 39*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-137 - 134*t2 - 11*Power(t2,2) + 
                  45*Power(t2,3)) + 
               t1*(76 + 47*t2 + 24*Power(t2,2) + 45*Power(t2,3))) - 
            s2*(4*Power(t1,7) - 2*Power(t1,6)*(13 + 8*t2) + 
               t2*(-7 + 9*t2 - 2*Power(t2,2)) + 
               2*Power(t1,5)*(9 + 27*t2 + 2*Power(t2,2)) + 
               t1*(-45 + 39*t2 - 32*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,4)*(107 - 59*t2 + 8*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,3)*(-229 + 45*t2 - 3*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(t1,2)*(171 - 56*t2 + 14*Power(t2,2) + 
                  60*Power(t2,3))) + 
            Power(s1,2)*(s2 - t1)*
             (Power(s2,3)*(-7 - 18*t1 + 23*Power(t1,2) + 
                  2*Power(t1,3)) + 
               s2*(-Power(t1,5) - 2*t2 + Power(t1,4)*(-29 + 7*t2) + 
                  5*Power(t1,2)*(11 + 21*t2) + t1*(-49 + 29*t2) + 
                  Power(t1,3)*(24 + 41*t2)) - 
               Power(s2,2)*(Power(t1,4) + 3*(3 + t2) + 
                  Power(t1,3)*(13 + 2*t2) + Power(t1,2)*(-1 + 27*t2) + 
                  t1*(-22 + 58*t2)) + 
               t1*(-1 + 14*Power(t1,4) + t1*(50 - 18*t2) + 3*t2 - 
                  7*Power(t1,3)*(1 + 2*t2) - Power(t1,2)*(56 + 61*t2))) \
+ s1*(6*Power(s2,5)*Power(-1 + t1,2) + 
               Power(s2,4)*(-1 + t1)*
                (-13 + Power(t1,3) + t1*(17 - 34*t2) - 32*t2 - 
                  Power(t1,2)*(5 + 2*t2)) + 
               t1*(-6 + 6*Power(t1,6) + 13*t2 - 7*Power(t2,2) + 
                  3*Power(t1,5)*(1 + t2) + 
                  Power(t1,2)*(-32 + 59*t2 - 55*Power(t2,2)) + 
                  Power(t1,3)*(49 - 41*t2 - 47*Power(t2,2)) - 
                  Power(t1,4)*(38 + 10*t2 + 3*Power(t2,2)) + 
                  2*t1*(9 - 12*t2 + 11*Power(t2,2))) + 
               Power(s2,2)*(3 + Power(t1,6) + Power(t1,5)*(43 - 7*t2) + 
                  16*t2 - 3*Power(t2,2) + 
                  Power(t1,2)*(299 - 113*t2 - 166*Power(t2,2)) + 
                  Power(t1,3)*(-187 + 109*t2 - 35*Power(t2,2)) + 
                  Power(t1,4)*(-33 - 41*t2 + 2*Power(t2,2)) - 
                  2*t1*(63 - 18*t2 + 34*Power(t2,2))) + 
               Power(s2,3)*(41 - 2*Power(t1,5) + 19*t2 + 
                  5*Power(t2,2) + 5*Power(t1,4)*(-5 + 2*t2) + 
                  13*Power(t1,3)*(1 + 5*t2) + 
                  Power(t1,2)*(98 - 35*t2 - 2*Power(t2,2)) + 
                  t1*(-125 - 59*t2 + 87*Power(t2,2))) + 
               s2*(1 - 3*t2 + 2*Power(t2,2) - Power(t1,6)*(24 + t2) + 
                  Power(t1,5)*(10 - t2 + Power(t2,2)) - 
                  2*t1*(4 + 9*t2 + 3*Power(t2,2)) + 
                  Power(t1,4)*(144 - 52*t2 + 33*Power(t2,2)) + 
                  Power(t1,2)*(105 - 90*t2 + 106*Power(t2,2)) + 
                  Power(t1,3)*(-228 + 165*t2 + 134*Power(t2,2))))) + 
         Power(s,3)*(-1 + t1)*
          (-3*(-1 + t2)*t2 + Power(t1,7)*
             (12 - 28*s1 + Power(s1,2) + 15*t2 - 3*s1*t2) - 
            2*Power(s2,4)*Power(-1 + t1,2)*
             (8 + s1*(-6 - 10*t1 + Power(t1,2)) + 7*t2 - 
               Power(t1,2)*(1 + t2) + t1*(-7 + 9*t2)) - 
            Power(t1,3)*(174 + 10*Power(s1,3) - 76*t2 + 17*Power(t2,2) + 
               24*Power(t2,3) + Power(s1,2)*(-52 + 3*t2) + 
               s1*(-167 + 79*t2 - 31*Power(t2,2))) + 
            Power(t1,6)*(Power(s1,3) - 2*Power(s1,2)*(-15 + t2) + 
               s1*(4 + Power(t2,2)) - 2*(3 + 20*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(38 - 46*t2 + 42*Power(t2,2) + 15*Power(t2,3) + 
               Power(s1,2)*(8 + 3*t2) - 4*s1*(7 + t2 + 6*Power(t2,2))) + 
            t1*(5 + 6*t2 - 6*Power(t2,2) - 5*Power(t2,3) + 
               s1*(-2 - 8*t2 + 9*Power(t2,2))) + 
            Power(t1,5)*(-107 + 6*Power(s1,3) + 49*t2 - 3*Power(t2,2) - 
               3*Power(t2,3) - Power(s1,2)*(43 + 23*t2) + 
               s1*(181 - 6*t2 + 14*Power(t2,2))) + 
            Power(t1,4)*(232 + 37*Power(s1,3) - 63*t2 - 9*Power(t2,2) - 
               17*Power(t2,3) - Power(s1,2)*(48 + 77*t2) + 
               s1*(-294 + 100*t2 + 71*Power(t2,2))) - 
            Power(s2,3)*(-1 + t1)*
             (-20 + Power(t1,5) - 
               Power(s1,2)*(-4 + 31*t1 + 55*Power(t1,2) + 
                  2*Power(t1,3)) + Power(t1,3)*(16 - 46*t2) - 15*t2 + 
               22*Power(t2,2) + Power(t1,4)*(18 + 7*t2) + 
               t1*(95 - 42*t2 - 93*Power(t2,2)) + 
               Power(t1,2)*(-110 + 96*t2 - 13*Power(t2,2)) + 
               s1*(6 - 4*Power(t1,4) - 22*t2 + 4*Power(t1,3)*t2 + 
                  2*t1*(10 + 59*t2) + Power(t1,2)*(-22 + 68*t2))) + 
            Power(s2,2)*(2 + Power(t1,7) + 
               Power(s1,3)*t1*
                (1 + 6*t1 + 25*Power(t1,2) + 2*Power(t1,3)) - 13*t2 + 
               12*Power(t2,2) + 17*Power(t2,3) + 
               Power(t1,6)*(29 + 8*t2) + 
               Power(t1,4)*(-105 + 164*t2 - 72*Power(t2,2)) + 
               Power(t1,5)*(-36 - 11*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-138 + 263*t2 + 204*Power(t2,2) - 
                  59*Power(t2,3)) - 
               t1*(-23 + 29*t2 + 99*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(224 - 382*t2 - 51*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(s1,2)*(4*Power(t1,5) + t1*(37 - 19*t2) - 5*t2 + 
                  Power(t1,4)*(50 + 4*t2) + Power(t1,3)*(27 + 41*t2) + 
                  Power(t1,2)*(-118 + 81*t2)) + 
               s1*(-2 - 5*Power(t1,6) + 6*Power(t1,5)*(-14 + t2) - 
                  19*Power(t2,2) + Power(t1,4)*(46 + 117*t2) + 
                  t1*(131 + 81*t2 - 25*Power(t2,2)) + 
                  Power(t1,3)*(307 + 33*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*(-393 - 237*t2 + 137*Power(t2,2)))) + 
            s2*(-2 + Power(t1,7)*(-13 + 3*s1 - 4*t2) + t2 + 5*s1*t2 - 
               2*(3 + 2*s1)*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,6)*(11 + Power(s1,2) - 41*t2 + Power(t2,2) + 
                  s1*(92 + t2)) + 
               Power(t1,4)*(77 - 31*Power(s1,3) + 128*t2 - 
                  16*Power(t2,2) + 5*Power(t2,3) + 
                  Power(s1,2)*(96 + 62*t2) + 
                  s1*(-470 + 72*t2 - 44*Power(t2,2))) + 
               Power(t1,2)*(126 + 9*Power(s1,3) + 16*t2 + 
                  85*Power(t2,2) + 32*Power(t2,3) - 
                  5*Power(s1,2)*(5 + 2*t2) + 
                  s1*(-290 + 6*t2 - 6*Power(t2,2))) + 
               Power(t1,5)*(6 - 3*Power(s1,3) + 40*t2 + 41*Power(t2,2) - 
                  3*Power(t2,3) + Power(s1,2)*(-34 + 7*t2) + 
                  s1*(-39 - 61*t2 + 4*Power(t2,2))) + 
               t1*(-25 + 25*t2 - 32*Power(t2,2) - 35*Power(t2,3) - 
                  Power(s1,2)*(5 + 11*t2) + 
                  s1*(15 + 32*t2 + 30*Power(t2,2))) - 
               Power(t1,3)*(180 + 43*Power(s1,3) + 
                  Power(s1,2)*(33 - 156*t2) + 165*t2 + 73*Power(t2,2) - 
                  62*Power(t2,3) + s1*(-689 + 55*t2 + 184*Power(t2,2))))))*
       T4q(t1))/(Power(s,2)*(-1 + s1)*(-1 + s2)*Power(-1 + t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*(-s + s1 - t2)) + 
    (8*(2*Power(s,7)*Power(t1,2)*(t1 + t2) + 
         2*Power(s,6)*t1*(Power(t1,3) + (2*s2 - t2)*t2 + 
            Power(t1,2)*(1 - 2*s1 - 3*s2 + 5*t2) + 
            t1*(1 + s2*(2 + s1 - 4*t2) - (2 + s1)*t2)) + 
         Power(s,5)*(Power(t1,5) + Power(t2,3) + 
            Power(t1,4)*(27 - 3*s1 + 8*t2) + 
            t1*t2*(-4 + (6 + s1)*t2 - Power(t2,2)) + 
            Power(t1,3)*(-34 + 2*Power(s1,2) + s1*(12 - 11*t2) - 11*t2 + 
               3*Power(t2,2)) + 
            2*Power(s2,2)*(3*Power(t1,3) + t1*(1 + 2*s1 - 6*t2) + t2 + 
               Power(t1,2)*(-4 - 3*s1 + 6*t2)) + 
            Power(t1,2)*(-6 - 17*t2 - 3*Power(t2,2) + s1*(2 + 9*t2)) + 
            s2*(-5*Power(t1,4) + Power(t1,3)*(-14 + 10*s1 - 29*t2) - 
               2*Power(t2,2) + 
               Power(t1,2)*(12 - 14*s1 - 2*Power(s1,2) + 29*t2 + 
                  4*s1*t2) + t1*(4 - 8*s1*t2 + 9*Power(t2,2)))) + 
         Power(s,4)*((3 - 2*t2)*Power(t2,2) + 
            Power(t1,5)*(31 - 2*s1 + 3*t2) - 
            Power(t1,4)*(87 + 20*s1 - 29*t2 + 6*s1*t2) - 
            2*Power(s2,3)*(-1 + t1)*
             (s1 - t1 - 3*s1*t1 + Power(t1,2) - 2*t2 + 4*t1*t2) + 
            t1*(-2 - (-11 + s1)*t2 + (2 - 5*s1)*Power(t2,2) + 
               6*Power(t2,3)) - 
            Power(t1,2)*(8 - 45*t2 - 4*Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(2 + t2) + s1*(-2 + 6*t2)) + 
            Power(s2,2)*(2 + 4*Power(t1,4) + 
               Power(t1,2)*(-45 + 19*s1 + 2*Power(s1,2) - 39*t2) + 
               4*t2 - 6*s1*t2 + 7*Power(t2,2) + 
               Power(t1,3)*(25 - 9*s1 + 27*t2) + 
               t1*(16 - 8*s1 - 6*Power(s1,2) + 6*t2 + 18*s1*t2 - 
                  13*Power(t2,2))) + 
            Power(t1,3)*(89 + Power(s1,2)*(-12 + t2) - 125*t2 - 
               3*Power(t2,2) + Power(t2,3) + 
               s1*(26 + 44*t2 - 2*Power(t2,2))) + 
            s2*(-2*Power(t1,5) + t2*(-4 + t2 + 4*s1*t2 - 7*Power(t2,2)) - 
               Power(t1,3)*(-146 + Power(s1,2) + s1*(5 - 14*t2) - 8*t2 + 
                  8*Power(t2,2)) + 
               t1*(-4 + (-35 + 19*s1 + 2*Power(s1,2))*t2 - 
                  (12 + 5*s1)*Power(t2,2) + 5*Power(t2,3)) + 
               2*Power(t1,4)*(3*s1 - 8*(4 + t2)) + 
               Power(t1,2)*(-77 + 64*t2 + 17*Power(t2,2) + 
                  Power(s1,2)*(17 + 2*t2) - 13*s1*(1 + 4*t2)))) - 
         (s2 - t1)*(-1 + t1)*(2*(-1 + s1)*Power(t1,5)*(1 + s1 - 2*t2) + 
            Power(s2,4)*Power(s1 - t2,3) - 
            3*(-1 + s1)*t1*Power(-1 + t2,2) + Power(-1 + t2,3) - 
            Power(t1,2)*(-1 + t2)*
             (-1 + 7*Power(s1,2) + 24*t2 + 6*Power(t2,2) - 
               4*s1*(5 + 4*t2)) + 
            Power(t1,3)*(-5 + Power(s1,3) - 40*t2 + 42*Power(t2,2) + 
               4*Power(t2,3) + Power(s1,2)*(-9 + 8*t2) + 
               s1*(49 - 36*t2 - 14*Power(t2,2))) + 
            Power(t1,4)*(6 + 14*t2 - 4*Power(s1,2)*t2 - 24*Power(t2,2) + 
               s1*(-26 + 30*t2 + 4*Power(t2,2))) - 
            Power(s2,3)*(s1 - t2)*
             (-6 + Power(s1,2)*(1 + 3*t1) + t1*(12 - 5*t2) + t2 + 
               4*Power(t2,2) + Power(t1,2)*(-6 + 4*t2) + 
               s1*(5 + 2*Power(t1,2) - 5*t2 - t1*(7 + 3*t2))) + 
            s2*(-(Power(s1,3)*Power(t1,2)*(3 + t1)) - 
               Power(-1 + t2,2)*(-1 + 4*t2) + 
               Power(t1,2)*(15 + 68*t2 - 86*Power(t2,2)) + 
               2*Power(t1,4)*(2 - 7*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(13 + 16*t2 - 46*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-7 - 32*t2 + 27*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*t1*
                (-6*Power(t1,3) + t1*(13 - 10*t2) + 14*(-1 + t2) + 
                  Power(t1,2)*(7 + 8*t2)) + 
               s1*(3*Power(-1 + t2,2) + 6*Power(t1,4)*(1 + t2) + 
                  t1*(49 - 26*t2 - 23*Power(t2,2)) + 
                  Power(t1,3)*(43 - 68*t2 - 2*Power(t2,2)) + 
                  Power(t1,2)*(-101 + 94*t2 + 10*Power(t2,2)))) + 
            Power(s2,2)*(2 + 3*Power(s1,3)*t1*(1 + t1) + 25*t2 - 
               27*Power(t2,2) - 
               2*Power(t1,3)*(1 - 8*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(6 - 7*t2 - 20*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(6 + 34*t2 - 55*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*(7 + t1 + 6*Power(t1,3) - 7*t2 - 4*t1*t2 - 
                  7*Power(t1,2)*(2 + t2)) + 
               s1*(-29 - 12*Power(t1,3) + 22*t2 + 7*Power(t2,2) + 
                  Power(t1,2)*(-5 + 40*t2 - 2*Power(t2,2)) + 
                  t1*(46 - 62*t2 + 13*Power(t2,2))))) + 
         Power(s,3)*(14*Power(t1,6) - 
            2*Power(s2,4)*Power(-1 + t1,2)*(s1 - t2) - 3*(-1 + t2)*t2 + 
            Power(t1,5)*(-48 + Power(s1,2) + 35*t2 - 3*s1*(12 + t2)) + 
            Power(t1,4)*(88 + Power(s1,3) - 2*Power(s1,2)*(-1 + t2) - 
               238*t2 + 16*Power(t2,2) + s1*(108 + 16*t2 + Power(t2,2))) \
+ Power(t1,3)*(-107 + 2*Power(s1,3) + Power(s1,2)*(20 - 13*t2) + 
               326*t2 - 43*Power(t2,2) + 3*Power(t2,3) + 
               s1*(-151 + 5*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(48 - 25*t2 + 21*Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,2)*(8 + 3*t2) - 2*s1*(16 + 10*t2 + 3*Power(t2,2))\
) + t1*(5 + 12*t2 - 12*Power(t2,2) - 5*Power(t2,3) + 
               s1*(-2 - 8*t2 + 9*Power(t2,2))) + 
            Power(s2,3)*(6 - Power(t1,4) + 
               Power(s1,2)*(-4 + 5*t1 + 2*Power(t1,2)) - 5*t2 - 
               6*Power(t2,2) - Power(t1,3)*(15 + 7*t2) + 
               Power(t1,2)*(37 + 7*t2) + t1*(-27 + t2 + 5*Power(t2,2)) + 
               2*s1*(1 + 2*Power(t1,3) + 5*t2 - 2*Power(t1,2)*t2 - 
                  t1*(1 + 4*t2))) + 
            Power(s2,2)*(2 + Power(t1,5) + Power(s1,3)*t1*(1 + 2*t1) - 
               13*t2 + 12*Power(t2,2) + 17*Power(t2,3) + 
               Power(t1,4)*(43 + 8*t2) + 
               Power(t1,3)*(-143 + 23*t2 + 6*Power(t2,2)) - 
               2*Power(t1,2)*(-74 + 55*t2 + 6*Power(t2,2)) + 
               t1*(-51 + 107*t2 + 3*Power(t2,2) - 10*Power(t2,3)) - 
               Power(s1,2)*(4*Power(t1,3) + t1*(-23 + t2) - 5*t2 + 
                  4*Power(t1,2)*(4 + t2)) - 
               s1*(2 + 5*Power(t1,4) + Power(t1,3)*(28 - 6*t2) + 
                  19*Power(t2,2) - Power(t1,2)*(73 + 39*t2) + 
                  t1*(53 + 57*t2 - 9*Power(t2,2)))) + 
            s2*(-2 + Power(t1,5)*(-41 + 3*s1 - 4*t2) + t2 + 5*s1*t2 - 
               2*(3 + 2*s1)*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,4)*(155 + Power(s1,2) - 69*t2 + Power(t2,2) + 
                  s1*(60 + t2)) + 
               Power(t1,2)*(148 - 3*Power(s1,3) - 393*t2 + 
                  15*Power(t2,2) + 7*Power(t2,3) + 
                  Power(s1,2)*(-29 + 4*t2) + 
                  s1*(154 + 85*t2 - 6*Power(t2,2))) - 
               Power(t1,3)*(231 + 3*Power(s1,3) - 374*t2 + 
                  13*Power(t2,2) + 3*Power(t2,3) - 
                  Power(s1,2)*(8 + 7*t2) + 
                  s1*(168 + 61*t2 - 4*Power(t2,2))) + 
               t1*(-29 + 27*t2 - 44*Power(t2,2) - 21*Power(t2,3) - 
                  Power(s1,2)*(5 + 11*t2) + 
                  s1*(15 + 42*t2 + 22*Power(t2,2))))) - 
         s*(Power(t1,7)*(-2 + 6*s1 - 4*t2) + 
            2*Power(s2,5)*(-1 + t1)*Power(s1 - t2,2) + Power(-1 + t2,3) - 
            2*t1*Power(-1 + t2,2)*(-5 + s1 + 3*t2) + 
            Power(t1,6)*(10 - 6*Power(s1,2) + 36*t2 + 4*s1*(-7 + 2*t2)) + 
            Power(t1,2)*(-1 + t2)*
             (20 - Power(s1,2) - 24*t2 + 5*Power(t2,2) + 
               6*s1*(-1 + 2*t2)) + 
            Power(t1,3)*(6 - 2*Power(s1,3) - 117*t2 + 80*Power(t2,2) + 
               Power(t2,3) + Power(s1,2)*(-6 + 20*t2) + 
               s1*(91 - 64*t2 - 29*Power(t2,2))) + 
            Power(t1,5)*(-22 + Power(s1,3) - 155*t2 + 70*Power(t2,2) - 
               4*Power(t2,3) + Power(s1,2)*(2 + 11*t2) + 
               s1*(135 - 92*t2 - 6*Power(t2,2))) + 
            Power(t1,4)*(19 + 4*Power(s1,3) + Power(s1,2)*(9 - 39*t2) + 
               219*t2 - 140*Power(t2,2) + 
               2*s1*(-104 + 81*t2 + 17*Power(t2,2))) + 
            Power(s2,4)*(Power(s1,3)*(1 + 2*t1) + 
               Power(s1,2)*(1 + t1 - 2*Power(t1,2) - 10*t2 + t1*t2) + 
               s1*(-3 - Power(t1,3)*(-1 + t2) + 7*t2 + 17*Power(t2,2) + 
                  5*Power(t1,2)*(-1 + 3*t2) + 
                  t1*(7 - 21*t2 - 8*Power(t2,2))) + 
               t2*(-1 - 8*t2 - 8*Power(t2,2) + Power(t1,3)*(3 + t2) - 
                  Power(t1,2)*(7 + 13*t2) + 5*t1*(1 + 4*t2 + Power(t2,2))\
)) + Power(s2,3)*(-3 + Power(s1,3)*(2 - 7*t1 - 7*Power(t1,2)) + 45*t2 - 
               21*Power(t2,2) + 9*Power(t2,3) + 
               Power(t1,2)*t2*(87 - 47*t2 - 7*Power(t2,2)) - 
               Power(t1,4)*(-3 + 8*t2 + Power(t2,2)) + 
               Power(t1,3)*(-6 - 9*t2 + 16*Power(t2,2) + Power(t2,3)) + 
               t1*(6 - 115*t2 + 53*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s1,2)*(-19 - Power(t1,3)*(-1 + t2) + 5*t2 + 
                  3*t1*(12 + 7*t2) + Power(t1,2)*(-18 + 11*t2)) + 
               s1*(-11 + Power(t1,3)*(23 - 9*t2) + 
                  2*Power(t1,4)*(-4 + t2) + 29*t2 - 16*Power(t2,2) + 
                  t1*(29 - 57*t2 - 23*Power(t2,2)) + 
                  Power(t1,2)*(-33 + 35*t2 + 3*Power(t2,2)))) + 
            Power(s2,2)*(-2 + 
               3*Power(s1,3)*t1*(-2 + 5*t1 + 3*Power(t1,2)) + 
               Power(t1,5)*(-7 + t2) + 2*t2 + Power(t2,2) - 
               Power(t2,3) + Power(t1,4)*(23 + 75*t2 - 4*Power(t2,2)) + 
               t1*(4 - 201*t2 + 134*Power(t2,2) - 27*Power(t2,3)) - 
               Power(t1,3)*(25 + 350*t2 - 133*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(t1,2)*(7 + 473*t2 - 264*Power(t2,2) + 
                  27*Power(t2,3)) + 
               Power(s1,2)*(1 + Power(t1,3)*(43 - 14*t2) + 
                  2*Power(t1,4)*(-5 + t2) - t2 + 2*t1*(16 + 5*t2) - 
                  3*Power(t1,2)*(22 + 17*t2)) - 
               s1*(7 + Power(t1,5)*(-19 + t2) - 8*t2 + Power(t2,2) + 
                  Power(t1,4)*(59 + 3*t2) + 
                  Power(t1,3)*(-187 + 129*t2 - 19*Power(t2,2)) + 
                  Power(t1,2)*(280 - 301*t2 - 6*Power(t2,2)) - 
                  2*t1*(70 - 88*t2 + 15*Power(t2,2)))) - 
            s2*(Power(s1,3)*Power(t1,2)*(-6 + 13*t1 + 5*Power(t1,2)) - 
               Power(-1 + t2,2)*(-7 + 3*t2) - 2*Power(t1,6)*(3 + 4*t2) + 
               4*Power(t1,5)*(7 + 23*t2 + Power(t2,2)) + 
               Power(t1,2)*(5 - 267*t2 + 187*Power(t2,2) - 
                  15*Power(t2,3)) + 
               t1*(-19 + 37*t2 - 19*Power(t2,2) + Power(t2,3)) - 
               Power(t1,4)*(50 + 404*t2 - 165*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(t1,3)*(35 + 567*t2 - 350*Power(t2,2) + 
                  25*Power(t2,3)) + 
               Power(s1,2)*t1*
                (2 + Power(t1,4)*(-15 + t2) - 2*t2 + 7*t1*(1 + 5*t2) + 
                  Power(t1,3)*(26 + 9*t2) - Power(t1,2)*(20 + 79*t2)) + 
               s1*(18*Power(t1,6) - 2*Power(-1 + t2,2) + 
                  Power(t1,5)*(-69 + 7*t2) + 
                  Power(t1,2)*(220 - 211*t2 - 15*Power(t2,2)) + 
                  Power(t1,4)*(296 - 203*t2 + 8*Power(t2,2)) + 
                  t1*(-1 - 10*t2 + 11*Power(t2,2)) + 
                  Power(t1,3)*(-462 + 413*t2 + 34*Power(t2,2))))) - 
         Power(s,2)*(-1 - 10*t1 + 45*Power(t1,2) - 57*Power(t1,3) + 
            24*Power(t1,4) - 3*Power(t1,5) + 4*Power(t1,6) - 
            2*Power(t1,7) + Power(s1,3)*Power(s2 - t1,2)*
             (t1*(6 + t1) + s2*(-2 - 3*t1 + 2*Power(t1,2))) + 30*t1*t2 - 
            54*Power(t1,2)*t2 + 262*Power(t1,3)*t2 - 377*Power(t1,4)*t2 + 
            157*Power(t1,5)*t2 - 18*Power(t1,6)*t2 + 3*Power(t2,2) - 
            25*t1*Power(t2,2) + 27*Power(t1,2)*Power(t2,2) - 
            122*Power(t1,3)*Power(t2,2) + 90*Power(t1,4)*Power(t2,2) - 
            6*Power(t1,5)*Power(t2,2) - 2*Power(t2,3) + 5*t1*Power(t2,3) + 
            5*Power(t1,3)*Power(t2,3) - 10*Power(t1,4)*Power(t2,3) + 
            Power(s2,4)*(2 + Power(t1,2)*(6 - 7*t2) + 
               Power(t1,3)*(-2 + t2) - t2 + Power(t2,2) + 
               t1*(-6 + 7*t2 - 3*Power(t2,2))) + 
            Power(s2,3)*(5 + 6*Power(t1,4) - 27*t2 + 21*Power(t2,2) + 
               18*Power(t2,3) + Power(t1,3)*(-29 + 17*t2) + 
               Power(t1,2)*(45 - 56*t2 + 15*Power(t2,2)) - 
               t1*(27 - 66*t2 + 23*Power(t2,2) + 10*Power(t2,3))) + 
            s2*(8*Power(t1,6) + Power(t1,5)*(-22 + 36*t2) + 
               t1*(-45 + 32*t2 - 23*Power(t2,2)) + 
               t2*(-7 + 9*t2 - 2*Power(t2,2)) + 
               Power(t1,4)*(50 - 378*t2 + 32*Power(t2,2)) + 
               3*Power(t1,2)*(40 - 166*t2 + 73*Power(t2,2)) + 
               Power(t1,3)*(-111 + 815*t2 - 162*Power(t2,2) + 
                  14*Power(t2,3))) - 
            Power(s2,2)*(Power(t1,5)*(10 + t2) + 
               Power(t1,4)*(-43 + 32*t2 - 2*Power(t2,2)) - 
               2*(7 - 3*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(90 - 275*t2 + 36*Power(t2,2) + 
                  3*Power(t2,3)) - 
               3*Power(t1,2)*(35 - 161*t2 + 25*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(62 - 247*t2 + 104*Power(t2,2) + 27*Power(t2,3))) + 
            Power(s1,2)*(s2 - t1)*
             (Power(s2,3)*(-1 - 3*t1 + 2*Power(t1,2)) - 
               Power(s2,2)*(3 + Power(t1,3) - 13*t2 + 2*Power(t1,2)*t2 - 
                  3*t1*(5 + t2)) - 
               s2*(Power(t1,4) + Power(t1,3)*(10 - 7*t2) - 2*t2 + 
                  2*Power(t1,2)*(1 + 8*t2) + t1*(29 + 23*t2)) + 
               t1*(1 + 8*Power(t1,3) + Power(t1,2)*(13 - 12*t2) - 3*t2 + 
                  t1*(11 + 31*t2))) + 
            s1*(Power(s2,4)*(3 + Power(t1,3) + Power(t1,2)*(5 - 2*t2) + 
                  t1*(-9 + 6*t2)) - 
               Power(s2,3)*(-23 + 2*Power(t1,4) + 
                  Power(t1,3)*(21 - 10*t2) + 5*t2 + 29*Power(t2,2) + 
                  Power(t1,2)*(-66 + 19*t2) + 
                  t1*(66 + 12*t2 - 10*Power(t2,2))) + 
               t1*(6 + 24*Power(t1,5) + Power(t1,4)*(-95 + t2) - 13*t2 + 
                  7*Power(t2,2) + t1*(-12 + 11*t2 - 15*Power(t2,2)) + 
                  Power(t1,3)*(241 - 81*t2 + 3*Power(t2,2)) + 
                  4*Power(t1,2)*(-41 + 37*t2 + 4*Power(t2,2))) + 
               s2*(-1 + 3*t2 - 2*Power(t2,2) - Power(t1,5)*(52 + t2) + 
                  Power(t1,2)*(256 - 237*t2 - 30*Power(t2,2)) + 
                  Power(t1,4)*(222 + 6*t2 + Power(t2,2)) + 
                  t1*(7 + 21*t2 + 4*Power(t2,2)) - 
                  2*Power(t1,3)*(216 - 29*t2 + 7*Power(t2,2))) + 
               Power(s2,2)*(-3 + Power(t1,5) + Power(t1,4)*(44 - 7*t2) - 
                  16*t2 + 3*Power(t2,2) + 
                  Power(t1,2)*(250 + 43*t2 - 3*Power(t2,2)) + 
                  Power(t1,3)*(-181 + 2*Power(t2,2)) + 
                  t1*(-111 + 86*t2 + 47*Power(t2,2))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + s2)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),3)*
       (-s + s1 - t2)));
   return a;
};
