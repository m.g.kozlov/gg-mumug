#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m123_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(2*Power(s,6)*(1 + s1*(2 + t1 + t2)) + 
         Power(s,5)*(2*Power(s1,2)*(-2 + s2 - 3*t1 - 10*t2) + 
            10*(-1 + t2) + s1*
             (-78 - t1 - 3*Power(t1,2) + s2*(8 + t1 - 3*t2) + 41*t2 + 
               10*t1*t2 + 13*Power(t2,2))) + 
         Power(s,4)*(Power(s1,3)*(-12 - 8*s2 + 6*t1 + 46*t2) + 
            4*(4 - 10*t2 + 5*Power(t2,2)) + 
            Power(s1,2)*(154 + Power(s2,2) + 13*Power(t1,2) - 60*t2 - 
               80*Power(t2,2) - 15*t1*(1 + 3*t2) + 
               s2*(-1 - 14*t1 + 43*t2)) + 
            s1*(283 - 389*t2 + Power(s2,2)*t2 - 16*Power(t1,2)*t2 + 
               132*Power(t2,2) + 28*Power(t2,3) + 
               t1*(-73 + 28*t2 + 28*Power(t2,2)) + 
               s2*(-29 + 11*t2 - 21*Power(t2,2) + 5*t1*(4 + t2)))) + 
         Power(s,3)*(-10 + 2*Power(s1,4)*(10 + 5*s2 - t1 - 20*t2) + 
            48*t2 - 60*Power(t2,2) + 20*Power(t2,3) + 
            Power(s1,3)*(-108 - 6*Power(s2,2) - 21*Power(t1,2) + 
               s2*(16 + 33*t1 - 117*t2) + 7*t2 + 117*Power(t2,2) + 
               t1*(5 + 88*t2)) - 
            Power(s1,2)*(444 + Power(t1,2)*(32 - 52*t2) - 651*t2 + 
               187*Power(t2,2) + 112*Power(t2,3) + 
               Power(s2,2)*(9 + 8*t2) + 
               t1*(-192 + 95*t2 + 128*Power(t2,2)) + 
               s2*(45 - 28*t2 - 148*Power(t2,2) + t1*(-23 + 44*t2))) + 
            s1*(-407 + 939*t2 - 715*Power(t2,2) + 199*Power(t2,3) + 
               26*Power(t2,4) + 
               Power(t1,2)*(32 + 3*t2 - 30*Power(t2,2)) + 
               Power(s2,2)*(14 - 3*t2 + 8*Power(t2,2)) + 
               t1*(255 - 413*t2 + 118*Power(t2,2) + 44*Power(t2,3)) + 
               s2*(13 + 31*t2 - 38*Power(t2,2) - 46*Power(t2,3) + 
                  2*t1*(-51 + 40*t2 + Power(t2,2))))) + 
         s1*(1 + s1 - t2)*(-1 + t2)*
          (-8 + 47*t2 + 5*s2*t2 - 4*Power(s2,2)*t2 - 39*Power(t2,2) - 
            47*s2*Power(t2,2) + Power(s2,2)*Power(t2,2) + Power(t2,3) + 
            41*s2*Power(t2,3) - 5*Power(s2,2)*Power(t2,3) - Power(t2,4) + 
            s2*Power(t2,4) + Power(t1,2)*(-8 - 5*t2 + 5*Power(t2,2)) + 
            t1*(16 + (-74 + 31*s2)*t2 + (102 - 22*s2)*Power(t2,2) + 
               (-44 + 7*s2)*Power(t2,3)) + 
            4*Power(s1,3)*(-1 + 2*Power(s2,2) + 2*Power(t1,2) + 
               t1*(-1 + t2) + t2 - 2*s2*(-1 + 2*t1 + t2)) - 
            Power(s1,2)*(3*Power(t1,2)*(7 + t2) + 
               Power(s2,2)*(7 + 17*t2) + 4*(-1 + Power(t2,2)) + 
               t1*(-49 + 32*t2 + 17*Power(t2,2)) - 
               s2*(-57 + 36*t2 + 21*Power(t2,2) + 16*t1*(2 + t2))) + 
            s1*(-31 + 27*t2 + 3*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(21 + 8*t2 - 5*Power(t2,2)) + 
               2*Power(s2,2)*(2 + 3*t2 + 7*Power(t2,2)) + 
               t1*(42 - 135*t2 + 80*Power(t2,2) + 13*Power(t2,3)) - 
               s2*(5 - 104*t2 + 85*Power(t2,2) + 14*Power(t2,3) + 
                  t1*(31 + 10*t2 + 7*Power(t2,2))))) + 
         s*(2*Power(-1 + t2,2)*t2*(1 - 3*t2 + Power(t2,2)) - 
            4*Power(s1,5)*(2 + Power(s2,2) + Power(t1,2) - 
               7*t1*(-1 + t2) - 2*t2 + s2*(-9 - 2*t1 + 9*t2)) - 
            2*Power(s1,4)*(23 - 2*Power(t1,2)*(-4 + t2) - 49*t2 + 
               21*Power(t2,2) + 5*Power(t2,3) + 
               Power(s2,2)*(-2 + 8*t2) + 
               2*t1*(-8 - 17*t2 + 25*Power(t2,2)) - 
               2*s2*(-6 - 27*t2 + 33*Power(t2,2) + t1*(5 + t2))) + 
            Power(s1,3)*(-124 + 303*t2 - 285*Power(t2,2) + 
               85*Power(t2,3) + 21*Power(t2,4) + 
               Power(t1,2)*(-23 + 88*t2 - 29*Power(t2,2)) + 
               2*Power(s2,2)*(1 - 9*t2 + 26*Power(t2,2)) + 
               t1*(259 - 462*t2 + 59*Power(t2,2) + 144*Power(t2,3)) - 
               s2*(200 - 397*t2 + 18*Power(t2,2) + 179*Power(t2,3) + 
                  t1*(-23 + 90*t2 + 5*Power(t2,2)))) - 
            Power(s1,2)*(208 - 623*t2 + 677*Power(t2,2) - 
               369*Power(t2,3) + 95*Power(t2,4) + 12*Power(t2,5) + 
               Power(t1,2)*(-42 + 28*t2 + 70*Power(t2,2) - 
                  36*Power(t2,3)) + 
               Power(s2,2)*(-15 + 22*t2 - 35*Power(t2,2) + 
                  48*Power(t2,3)) + 
               t1*(-300 + 1013*t2 - 1032*Power(t2,2) + 
                  237*Power(t2,3) + 82*Power(t2,4)) + 
               s2*(83 - 502*t2 + 717*Power(t2,2) - 200*Power(t2,3) - 
                  98*Power(t2,4) + 
                  t1*(103 - 150*t2 + 19*Power(t2,2) - 12*Power(t2,3)))) + 
            s1*(-1 + t2)*(89 - 323*t2 + 324*Power(t2,2) - 
               136*Power(t2,3) + 45*Power(t2,4) + Power(t2,5) + 
               Power(t1,2)*(3 + 44*t2 - 14*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,2)*(-4 + 23*t2 - 9*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(-122 + 441*t2 - 482*Power(t2,2) + 157*Power(t2,3) + 
                  10*Power(t2,4)) - 
               s2*(3 + 65*t2 - 208*Power(t2,2) + 129*Power(t2,3) + 
                  15*Power(t2,4) + 
                  t1*(-39 + 165*t2 - 93*Power(t2,2) + 19*Power(t2,3))))) + 
         Power(s,2)*(2 - 4*Power(s1,5)*(2 + s2 - 3*t2) - 20*t2 + 
            48*Power(t2,2) - 40*Power(t2,3) + 10*Power(t2,4) + 
            Power(s1,4)*(38 + 9*Power(s2,2) + 15*Power(t1,2) + 
               t1*(39 - 81*t2) + 4*t2 - 50*Power(t2,2) + 
               s2*(-59 - 28*t1 + 113*t2)) + 
            Power(s1,3)*(229 - 380*t2 + 95*Power(t2,2) + 92*Power(t2,3) - 
               8*Power(t1,2)*(-7 + 6*t2) + Power(s2,2)*(13 + 15*t2) + 
               t1*(-155 + 7*t2 + 196*Power(t2,2)) + 
               s2*(106 + 55*t2 - 253*Power(t2,2) + t1*(-79 + 51*t2))) + 
            s1*(279 - 928*t2 + 1074*Power(t2,2) - 582*Power(t2,3) + 
               147*Power(t2,4) + 10*Power(t2,5) + 
               Power(t1,2)*(-34 + 75*t2 + Power(t2,2) - 24*Power(t2,3)) + 
               Power(s2,2)*(-18 + 35*t2 - 17*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(-289 + 856*t2 - 789*Power(t2,2) + 192*Power(t2,3) + 
                  34*Power(t2,4)) + 
               s2*(5 - 120*t2 + 269*Power(t2,2) - 116*Power(t2,3) - 
                  42*Power(t2,4) - 
                  2*t1*(-60 + 139*t2 - 68*Power(t2,2) + 7*Power(t2,3)))) - 
            Power(s1,2)*(-451 + 1056*t2 - 861*Power(t2,2) + 
               224*Power(t2,3) + 64*Power(t2,4) + 
               Power(t1,2)*(22 + 89*t2 - 70*Power(t2,2)) + 
               Power(s2,2)*(15 + 2*t2 + 38*Power(t2,2)) + 
               t1*(437 - 804*t2 + 219*Power(t2,2) + 158*Power(t2,3)) + 
               s2*(t1*(-87 - 37*t2 + 32*Power(t2,2)) - 
                  2*(73 - 211*t2 + 66*Power(t2,2) + 95*Power(t2,3)))))))/
     (s*(-1 + s1)*s1*(s - s2 + t1)*Power(-s + s1 - t2,2)*(1 - s + s1 - t2)*
       Power(-1 + s + t2,2)) + 
    (8*(Power(s,7)*(Power(t1,2) - 2*t1*t2 - Power(t2,2)) + 
         4*(-1 + t2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (Power(s1,3) + t2 - 2*Power(t2,2) - Power(s1,2)*(1 + 2*t2) + 
            s1*(-1 + 3*t2 + Power(t2,2))) + 
         Power(s,6)*(Power(t1,2)*(-5 - 7*s1 + 3*t2) + 
            t2*(s2*(-2 + t2) + (7 - 4*t2)*t2 + s1*(-2 + 4*t2)) + 
            t1*(2 + (4 + s2)*t2 - 9*Power(t2,2) + s1*(-4 + 2*s2 + 11*t2))\
) + Power(s,5)*(1 - 6*t2 + 7*s2*t2 - 29*Power(t2,2) - 
            14*s2*Power(t2,2) + 19*Power(t2,3) + 4*s2*Power(t2,3) - 
            6*Power(t2,4) + Power(t1,2)*(5 - 13*t2 + 3*Power(t2,2)) + 
            t1*(-8 + (27 - 4*s2)*t2 + 3*(6 + s2)*Power(t2,2) - 
               17*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,2) + 20*Power(t1,2) + 
               t1*(19 - 24*t2) + (7 - 6*t2)*t2 - s2*(2 + 12*t1 + t2)) - 
            s1*(2 + (-17 - 9*s2 + Power(s2,2))*t2 + 
               4*(8 + s2)*Power(t2,2) - 12*Power(t2,3) + 
               Power(t1,2)*(-26 + 19*t2) + 
               t1*(2 + s2*(5 - 3*t2) + 42*t2 - 40*Power(t2,2)))) + 
         Power(s,4)*(-3 + 26*t2 + s2*t2 + 24*Power(t2,2) + 
            57*s2*Power(t2,2) - 64*Power(t2,3) - 29*s2*Power(t2,3) + 
            18*Power(t2,4) + 6*s2*Power(t2,4) - 4*Power(t2,5) + 
            Power(t1,2)*(13 + 4*t2 - 10*Power(t2,2) + Power(t2,3)) + 
            t1*(2 - (82 + 3*s2)*t2 + (71 - 12*s2)*Power(t2,2) + 
               (32 + 3*s2)*Power(t2,3) - 17*Power(t2,4)) + 
            Power(s1,3)*(-5*Power(s2,2) - 30*Power(t1,2) + 
               4*(-2 + t2)*t2 + s2*(7 + 28*t1 + 3*t2) + t1*(-33 + 26*t2)\
) + Power(s1,2)*(15 - 43*t2 + 48*Power(t2,2) - 12*Power(t2,3) + 
               Power(s2,2)*(-2 + 9*t2) + Power(t1,2)*(-51 + 46*t2) + 
               s2*(t1*(24 - 31*t2) + 3*(-9 + t2)*t2) + 
               t1*(-22 + 119*t2 - 66*Power(t2,2))) + 
            s1*(-5 - 56*t2 + 2*Power(s2,2)*(1 - 2*t2)*t2 + 
               132*Power(t2,2) - 52*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,2)*(-20 + 61*t2 - 18*Power(t2,2)) + 
               3*t1*(19 - 6*t2 - 42*Power(t2,2) + 19*Power(t2,3)) + 
               s2*(1 - 30*t2 + 52*Power(t2,2) - 12*Power(t2,3) + 
                  t1*(-7 + t2 + Power(t2,2))))) + 
         Power(s,3)*(-2 - 14*t2 - 30*s2*t2 + 10*Power(t2,2) - 
            36*s2*Power(t2,2) - 4*Power(s2,2)*Power(t2,2) + 
            28*Power(t2,3) + 117*s2*Power(t2,3) - 44*Power(t2,4) - 
            26*s2*Power(t2,4) + 7*Power(t2,5) + 4*s2*Power(t2,5) - 
            Power(t2,6) - Power(t1,2)*
             (34 - 47*t2 + 12*Power(t2,2) + Power(t2,3)) + 
            t1*(28 + (11 + 30*s2)*t2 - 2*(88 + 7*s2)*Power(t2,2) + 
               (83 - 13*s2)*Power(t2,3) + (30 + s2)*Power(t2,4) - 
               9*Power(t2,5)) + 
            Power(s1,4)*(9*Power(s2,2) + 25*Power(t1,2) + 
               t1*(25 - 14*t2) - (-3 + t2)*t2 - s2*(8 + 32*t1 + 3*t2)) + 
            Power(s1,3)*(-14 + Power(t1,2)*(47 - 54*t2) + 
               Power(s2,2)*(9 - 24*t2) + 30*t2 - 26*Power(t2,2) + 
               4*Power(t2,3) + 2*t1*(24 - 67*t2 + 24*Power(t2,2)) + 
               s2*(-11 + 43*t2 + 2*Power(t2,2) + t1*(-41 + 65*t2))) + 
            Power(s1,2)*(Power(s2,2)*(-4 - 14*t2 + 21*Power(t2,2)) + 
               Power(t1,2)*(26 - 99*t2 + 36*Power(t2,2)) - 
               t1*(46 + 135*t2 - 246*Power(t2,2) + 63*Power(t2,3)) - 
               2*(19 - 76*t2 + 78*Power(t2,2) - 19*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(10 + 66*t2 - 83*Power(t2,2) + 9*Power(t2,3) + 
                  t1*(16 + 44*t2 - 36*Power(t2,2)))) + 
            s1*(33 - 10*t2 - 159*Power(t2,2) + 
               Power(s2,2)*(5 - 6*t2)*Power(t2,2) + 152*Power(t2,3) - 
               22*Power(t2,4) + 4*Power(t2,5) - 
               Power(t1,2)*(20 + 18*t2 - 48*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(-81 + 208*t2 + 10*Power(t2,2) - 157*Power(t2,3) + 
                  38*Power(t2,4)) + 
               2*s2*(-1 + 41*t2 - 94*Power(t2,2) + 37*Power(t2,3) - 
                  6*Power(t2,4) + 
                  t1*(9 - 21*t2 + 13*Power(t2,2) + Power(t2,3))))) + 
         Power(s,2)*(12 - 40*t1 + 28*Power(t1,2) - 61*t2 + 40*s2*t2 + 
            134*t1*t2 - 40*s2*t1*t2 - 73*Power(t1,2)*t2 + 
            56*Power(t2,2) - 47*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) - 24*t1*Power(t2,2) + 
            79*s2*t1*Power(t2,2) + 58*Power(t1,2)*Power(t2,2) + 
            18*Power(t2,3) - 51*s2*Power(t2,3) - 
            14*Power(s2,2)*Power(t2,3) - 158*t1*Power(t2,3) - 
            23*s2*t1*Power(t2,3) - 14*Power(t1,2)*Power(t2,3) - 
            19*Power(t2,4) + 98*s2*Power(t2,4) + 44*t1*Power(t2,4) - 
            6*s2*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) - 
            7*Power(t2,5) - 11*s2*Power(t2,5) + 16*t1*Power(t2,5) + 
            Power(t2,6) + s2*Power(t2,6) - 2*t1*Power(t2,6) + 
            Power(s1,5)*(-7*Power(s2,2) + s2*(3 + 18*t1 + t2) + 
               t1*(-7 - 11*t1 + 3*t2)) + 
            Power(s1,4)*(1 - 4*t2 + 3*Power(t2,2) + 
               Power(s2,2)*(-12 + 25*t2) + Power(t1,2)*(-20 + 31*t2) + 
               t1*(-32 + 59*t2 - 13*Power(t2,2)) + 
               s2*(17 - 29*t2 - 2*Power(t2,2) - 6*t1*(-5 + 9*t2))) - 
            Power(s1,3)*(-38 + 86*t2 - 52*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,2)*(16 - 71*t2 + 30*Power(t2,2)) + 
               Power(s2,2)*(-6 - 32*t2 + 33*Power(t2,2)) + 
               t1*(32 - 194*t2 + 173*Power(t2,2) - 23*Power(t2,3)) + 
               s2*(-4 + 76*t2 - 61*Power(t2,2) + Power(t2,3) + 
                  t1*(6 + 81*t2 - 57*Power(t2,2)))) + 
            Power(s1,2)*(-16 - 53*t2 + 140*Power(t2,2) - 
               71*Power(t2,3) + 
               Power(t1,2)*(14 + 7*t2 - 63*Power(t2,2) + 
                  11*Power(t2,3)) + 
               Power(s2,2)*(4 - 18*t2 - 28*Power(t2,2) + 
                  19*Power(t2,3)) + 
               t1*(56 - 16*t2 - 260*Power(t2,2) + 193*Power(t2,3) - 
                  21*Power(t2,4)) + 
               s2*(37 - 155*t2 + 219*Power(t2,2) - 58*Power(t2,3) + 
                  5*Power(t2,4) + 
                  t1*(-41 + 93*t2 + 26*Power(t2,2) - 24*Power(t2,3)))) + 
            s1*(2*Power(s2,2)*t2*
                (-4 + 13*t2 + 4*Power(t2,2) - 2*Power(t2,3)) + 
               Power(t1,2)*(25 - 48*t2 + 15*Power(t2,2) + 
                  15*Power(t2,3) - Power(t2,4)) + 
               5*(-3 + 12*t2 - 11*Power(t2,2) - 4*Power(t2,3) + 
                  6*Power(t2,4)) + 
               2*t1*(-5 - 78*t2 + 133*Power(t2,2) + 17*Power(t2,3) - 
                  44*Power(t2,4) + 5*Power(t2,5)) + 
               s2*(-8 - 46*t2 + 226*Power(t2,2) - 258*Power(t2,3) + 
                  34*Power(t2,4) - 4*Power(t2,5) + 
                  t1*(8 + 18*t2 - 88*Power(t2,2) + 31*Power(t2,3) + 
                     3*Power(t2,4))))) + 
         s*(-8 + 2*Power(s1,6)*Power(s2 - t1,2) + 16*t1 - 8*Power(t1,2) + 
            64*t2 - 16*s2*t2 - 100*t1*t2 + 16*s2*t1*t2 + 
            36*Power(t1,2)*t2 - 113*Power(t2,2) + 44*s2*Power(t2,2) + 
            168*t1*Power(t2,2) - 64*s2*t1*Power(t2,2) - 
            51*Power(t1,2)*Power(t2,2) + 53*Power(t2,3) - 
            59*s2*Power(t2,3) + 16*Power(s2,2)*Power(t2,3) - 
            39*t1*Power(t2,3) + 65*s2*t1*Power(t2,3) + 
            24*Power(t1,2)*Power(t2,3) + 19*Power(t2,4) + 
            2*s2*Power(t2,4) - 18*Power(s2,2)*Power(t2,4) - 
            56*t1*Power(t2,4) - 12*s2*t1*Power(t2,4) - 
            3*Power(t1,2)*Power(t2,4) - 17*Power(t2,5) + 
            31*s2*Power(t2,5) + 7*t1*Power(t2,5) - s2*t1*Power(t2,5) + 
            2*Power(t2,6) - 2*s2*Power(t2,6) + 4*t1*Power(t2,6) - 
            Power(s1,5)*(s2 - t1)*
             (6 + t1*(3 - 7*t2) - 6*t2 + s2*(-5 + 9*t2)) + 
            Power(s1,2)*(Power(-1 + t2,2)*
                (19 - 25*t2 + 14*Power(t2,2)) + 
               2*Power(s2,2)*t2*
                (20 - 20*t2 - 16*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(1 + 18*t2 - 32*Power(t2,2) - 
                  14*Power(t2,3) + Power(t2,4)) + 
               t1*(-16 + 111*t2 - 9*Power(t2,2) - 135*Power(t2,3) + 
                  49*Power(t2,4)) + 
               s2*(-20 + (71 - 93*t1)*t2 + (-201 + 139*t1)*Power(t2,2) + 
                  (153 + 13*t1)*Power(t2,3) - (3 + 7*t1)*Power(t2,4))) + 
            Power(s1,4)*(-4*Power(-1 + t2,2) + 
               3*Power(t1,2)*(3 - 8*t2 + 3*Power(t2,2)) + 
               2*Power(s2,2)*(1 - 12*t2 + 8*Power(t2,2)) + 
               t1*(33 - 68*t2 + 35*Power(t2,2)) - 
               s2*(13 - 28*t2 + 15*Power(t2,2) + 
                  t1*(11 - 48*t2 + 25*Power(t2,2)))) + 
            Power(s1,3)*(-4*Power(-1 + t2,2) + 
               t1*(5 - 117*t2 + 175*Power(t2,2) - 63*Power(t2,3)) + 
               Power(t1,2)*(-15 + 11*t2 + 33*Power(t2,2) - 
                  5*Power(t2,3)) - 
               2*Power(s2,2)*
                (6 - 4*t2 - 21*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(-25 + 105*t2 - 91*Power(t2,2) + 11*Power(t2,3) + 
                  t1*(39 - 43*t2 - 63*Power(t2,2) + 19*Power(t2,3)))) + 
            s1*(-(Power(s2,2)*Power(t2,2)*
                  (44 - 48*t2 - 9*Power(t2,2) + Power(t2,3))) - 
               2*Power(-1 + t2,2)*
                (18 + 7*t2 - 21*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(-8 + 14*t2 - 7*Power(t2,2) + 
                  11*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(44 - 80*t2 - 117*Power(t2,2) + 157*Power(t2,3) + 
                  15*Power(t2,4) - 19*Power(t2,5)) + 
               s2*(16 - 24*t2 + 13*Power(t2,2) + 107*Power(t2,3) - 
                  115*Power(t2,4) + 3*Power(t2,5) + 
                  t1*(-16 + 64*t2 - 11*Power(t2,2) - 73*Power(t2,3) + 
                     11*Power(t2,4) + Power(t2,5))))))*
       B1(s,1 - s + s1 - t2,s1))/
     (s*(-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (16*(-2*(-1 + s1)*Power(s1,2)*Power(-1 + t2,2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (-1 + Power(s1,2) + 2*t2 - s1*(1 + t2)) - 
         Power(s,8)*t2*(-1 + Power(s1,2) + s1*(2 - t1 + t2)) + 
         Power(s,7)*(t2*(-8 + 5*t2) + Power(s1,3)*(-1 + 5*t2) - 
            Power(s1,2)*(2 - t1 + Power(t1,2) - 16*t2 + Power(t2,2)) + 
            s1*(1 - (-13 + s2 + 7*t1)*t2 + 
               (-5 + s2 + 4*t1)*Power(t2,2) - 4*Power(t2,3))) + 
         Power(s,6)*(Power(s1,4)*(4 - 10*t2) + 
            2*t2*(13 - 17*t2 + 5*Power(t2,2)) + 
            Power(s1,3)*(14 + 6*Power(t1,2) + t1*(3 - 2*s2 - 5*t2) - 
               (40 + 3*s2)*t2 + 9*Power(t2,2)) + 
            s1*(-7 + (-42 + 10*s2 + 19*t1)*t2 + 
               (55 - 11*s2 - 24*t1)*Power(t2,2) + 
               (-4 + 4*s2 + 6*t1)*Power(t2,3) - 6*Power(t2,4)) - 
            Power(s1,2)*(-12 + 65*t2 - 47*Power(t2,2) - 6*Power(t2,3) + 
               Power(t1,2)*(-4 + 3*t2) - t1*(-8 + 12*t2 + Power(t2,2)) + 
               s2*(1 + (3 + 2*t1)*t2 + 2*Power(t2,2)))) + 
         Power(s,5)*(2*Power(s1,5)*(-3 + 5*t2) - 
            Power(s1,4)*(22 + Power(s2,2) + 14*Power(t1,2) + 
               s2*(1 - 10*t1 - 11*t2) - 3*t1*(-4 + t2) - 29*t2 + 
               8*Power(t2,2)) + 
            t2*(-45 + 92*t2 - 56*Power(t2,2) + 10*Power(t2,3)) + 
            s1*(19 - 3*(-29 + 11*s2 + 9*t1)*t2 + 
               (-206 + 56*s2 + 54*t1)*Power(t2,2) - 
               6*(-17 + 5*s2 + 5*t1)*Power(t2,3) + 
               (-2 + 6*s2 + 4*t1)*Power(t2,4) - 4*Power(t2,5)) + 
            Power(s1,3)*(-33 + 96*t2 + 3*Power(s2,2)*t2 - 
               107*Power(t2,2) - 3*Power(t2,3) + 
               4*Power(t1,2)*(-4 + 5*t2) + 
               t1*(8 + 15*t2 - 26*Power(t2,2)) + 
               s2*(-7 + t1*(2 - 8*t2) + 24*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(-42 + 185*t2 - Power(s2,2)*(-9 + t2)*t2 - 
               153*Power(t2,2) + 57*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,2)*(-3 + 12*t2 - 5*Power(t2,2)) + 
               s2*(9 - 31*t2 + (7 - 4*t1)*Power(t2,2) - 
                  11*Power(t2,3)) + 
               t1*(19 - 54*t2 + 17*Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s,4)*(Power(s1,6)*(4 - 5*t2) + 
            t2*(45 - 128*t2 + 121*Power(t2,2) - 44*Power(t2,3) + 
               5*Power(t2,4)) - 
            s1*(26 + (116 - 52*s2 - 23*t1)*t2 + 
               2*(-213 + 65*s2 + 30*t1)*Power(t2,2) + 
               (371 - 104*s2 - 54*t1)*Power(t2,3) + 
               2*(-50 + 17*s2 + 8*t1)*Power(t2,4) - 
               (-2 + 4*s2 + t1)*Power(t2,5) + Power(t2,6)) + 
            Power(s1,5)*(4*Power(s2,2) + 16*Power(t1,2) + 
               s2*(2 - 18*t1 - 15*t2) + 2*(7 - 3*t2)*t2 + t1*(9 + 6*t2)) \
- Power(s1,2)*(-90 + 422*t2 - 382*Power(t2,2) + 130*Power(t2,3) - 
               48*Power(t2,4) - 7*Power(t2,5) + 
               Power(s2,2)*t2*(30 - 36*t2 + Power(t2,2)) + 
               Power(t1,2)*(10 + 4*t2 - 20*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(14 - 98*t2 + 68*Power(t2,2) + 16*Power(t2,3) - 
                  17*Power(t2,4)) + 
               s2*(24 - 12*(12 + t1)*t2 + 4*(41 + 3*t1)*Power(t2,2) + 
                  4*(-10 + t1)*Power(t2,3) + 17*Power(t2,4))) + 
            Power(s1,4)*(5 + Power(t1,2)*(25 - 48*t2) + 
               Power(s2,2)*(3 - 14*t2) - 116*t2 + 83*Power(t2,2) + 
               7*Power(t2,3) + t1*(6 - 78*t2 + 47*Power(t2,2)) + 
               s2*(23 + 10*t2 - 20*Power(t2,2) + 16*t1*(-1 + 3*t2))) + 
            Power(s1,3)*(66 - 50*t2 + 164*Power(t2,2) - 
               139*Power(t2,3) - 11*Power(t2,4) + 
               Power(s2,2)*(9 - 35*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(13 - 59*t2 + 28*Power(t2,2)) - 
               t1*(36 + 28*t2 - 79*Power(t2,2) + 56*Power(t2,3)) + 
               s2*(-16 + 15*t2 + 28*Power(t2,2) + 30*Power(t2,3) + 
                  t1*(2 + 48*t2 - 12*Power(t2,2))))) + 
         Power(s,3)*(Power(s1,7)*(-1 + t2) + 
            t2*(-26 + 97*t2 - 127*Power(t2,2) + 71*Power(t2,3) - 
               16*Power(t2,4) + Power(t2,5)) + 
            s1*(19 + (93 - 43*s2 - 13*t1)*t2 + 
               (-485 + 149*s2 + 36*t1)*Power(t2,2) + 
               (629 - 170*s2 - 44*t1)*Power(t2,3) + 
               (-304 + 82*s2 + 22*t1)*Power(t2,4) + 
               (49 - 17*s2 - 3*t1)*Power(t2,5) + (-1 + s2)*Power(t2,6)) \
+ Power(s1,6)*(14 - 5*Power(s2,2) + t1 - 9*Power(t1,2) - 25*t2 - 
               7*t1*t2 + 11*Power(t2,2) + s2*(-3 + 14*t1 + 9*t2)) + 
            Power(s1,5)*(-53 + 94*t2 - 32*Power(t2,2) - 9*Power(t2,3) + 
               6*Power(t1,2)*(-4 + 9*t2) + Power(s2,2)*(-8 + 26*t2) - 
               2*t1*(18 - 58*t2 + 25*Power(t2,2)) + 
               s2*(21 + t1*(28 - 76*t2) - 92*t2 + 41*Power(t2,2))) + 
            Power(s1,2)*(-102 + 593*t2 - 776*Power(t2,2) + 
               289*Power(t2,3) - 34*Power(t2,4) + 29*Power(t2,5) + 
               Power(t2,6) + 
               Power(s2,2)*t2*
                (36 - 94*t2 + 44*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(25 - 38*t2 - 14*Power(t2,2) + 
                  16*Power(t2,3) - 2*Power(t2,4)) - 
               s2*(-28 + (218 + 20*t1)*t2 - 
                  8*(55 + 8*t1)*Power(t2,2) + 
                  2*(139 + 7*t1)*Power(t2,3) + 
                  (-35 + 4*t1)*Power(t2,4) + 9*Power(t2,5)) + 
               t1*(-11 - 64*t2 + 100*Power(t2,2) - 4*Power(t2,3) - 
                  30*Power(t2,4) + 11*Power(t2,5))) + 
            Power(s1,4)*(69 + 44*t2 - 269*Power(t2,2) + 
               151*Power(t2,3) + 5*Power(t2,4) + 
               Power(t1,2)*(-27 + 80*t2 - 51*Power(t2,2)) + 
               Power(s2,2)*(-19 + 38*t2 - 5*Power(t2,2)) + 
               4*t1*(-11 + 28*t2 - 48*Power(t2,2) + 20*Power(t2,3)) + 
               s2*(9 + 25*t2 + 71*Power(t2,2) - 61*Power(t2,3) + 
                  t1*(36 - 98*t2 + 46*Power(t2,2)))) - 
            Power(s1,3)*(124 - 34*t2 - 117*Power(t2,2) - 
               89*Power(t2,3) + 112*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,2)*(1 - 46*t2 + 73*Power(t2,2) - 
                  18*Power(t2,3)) + 
               Power(s2,2)*(21 - 86*t2 + 73*Power(t2,2) + 
                  6*Power(t2,3)) + 
               2*t1*(-29 - 8*t2 + 65*Power(t2,2) - 64*Power(t2,3) + 
                  26*Power(t2,4)) + 
               s2*(-70 + 233*t2 - 96*Power(t2,2) - 15*Power(t2,3) - 
                  32*Power(t2,4) + 
                  2*t1*(3 + 27*t2 - 43*Power(t2,2) + Power(t2,3))))) + 
         s*(-1 + t2)*(Power(s1,7)*
             (6*Power(s2,2) + 6*Power(t1,2) - 2*s2*(5 + 6*t1 - 5*t2) - 
               10*t1*(-1 + t2) + Power(-1 + t2,2)) + 
            Power(-1 + t2,2)*t2*(1 - 3*t2 + Power(t2,2)) + 
            s1*(-1 + t2)*(1 - (-9 + 3*s2 + t1)*t2 + 
               3*(-15 + 4*s2)*Power(t2,2) - 
               (-46 + 9*s2 + t1)*Power(t2,3) + (-11 + 2*s2)*Power(t2,4)) \
+ Power(s1,2)*(t1*Power(-1 + t2,2)*
                (17 - 7*t2 - 3*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(11 - 27*t2 + 4*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-3 + 18*t2 - 8*Power(t2,2) + 3*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (6 - 68*t2 + 28*Power(t2,2) + 15*Power(t2,3)) + 
               s2*(-3 + 12*(2 + t1)*t2 - (77 + 48*t1)*Power(t2,2) + 
                  2*(45 + 7*t1)*Power(t2,3) + 
                  2*(-15 + t1)*Power(t2,4) - 4*Power(t2,5))) + 
            Power(s1,6)*(2*Power(s2,2)*(-8 + t2) - 
               Power(-1 + t2,2)*(-13 + 2*t2) - 
               2*Power(t1,2)*(4 + 3*t2) + 
               t1*(-7 - 10*t2 + 17*Power(t2,2)) + 
               s2*(5 + 14*t2 - 19*Power(t2,2) + 4*t1*(6 + t2))) + 
            Power(s1,5)*(Power(s2,2)*(-1 + 17*t2 - 20*Power(t2,2)) + 
               Power(-1 + t2,2)*(-30 - 20*t2 + Power(t2,2)) + 
               Power(t1,2)*(-9 + t2 + 4*Power(t2,2)) - 
               6*t1*(6 - 11*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(57 - 107*t2 + 37*Power(t2,2) + 13*Power(t2,3) + 
                  2*t1*(3 - 5*t2 + 6*Power(t2,2)))) + 
            Power(s1,4)*(5*Power(-1 + t2,2)*
                (-3 + 14*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(15 + 27*t2 - 4*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,2)*(20 - 14*t2 + 15*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(50 - 51*t2 - 17*Power(t2,2) + 11*Power(t2,3) + 
                  7*Power(t2,4)) - 
               2*s2*(33 - 18*t2 - 49*Power(t2,2) + 32*Power(t2,3) + 
                  2*Power(t2,4) + 
                  t1*(13 + 12*t2 + 8*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(s1,3)*(-(Power(-1 + t2,2)*
                  (-48 + 17*t2 + 49*Power(t2,2) + 3*Power(t2,3))) + 
               Power(t1,2)*(7 - 49*t2 + 4*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(3 - 38*t2 + 23*Power(t2,2) - 
                  19*Power(t2,3) - 3*Power(t2,4)) - 
               t1*(22 - 13*t2 - 15*Power(t2,2) + Power(t2,3) + 
                  3*Power(t2,4) + 2*Power(t2,5)) + 
               s2*(-17 + 146*t2 - 196*Power(t2,2) + 40*Power(t2,3) + 
                  27*Power(t2,4) + 
                  2*t1*(-2 + 25*t2 + 14*Power(t2,2) - 4*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s,2)*(-(Power(-1 + t2,2)*t2*
               (-8 + 22*t2 - 13*Power(t2,2) + 2*Power(t2,3))) - 
            s1*(-1 + t2)*(-7 + (-47 + 18*s2 + 5*t1)*t2 + 
               (231 - 65*s2 - 7*t1)*Power(t2,2) + 
               (-266 + 63*s2 + 9*t1)*Power(t2,3) + 
               (98 - 23*s2 - 3*t1)*Power(t2,4) + 3*(-3 + s2)*Power(t2,5)) \
+ 2*Power(s1,7)*(Power(s2,2) + Power(t1,2) + t1*(-1 + t2) - 
               2*Power(-1 + t2,2) - s2*(-1 + 2*t1 + t2)) + 
            Power(s1,2)*(Power(s2,2)*t2*
                (-18 + 82*t2 - 77*Power(t2,2) + 20*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,2)*(-24 + 63*t2 - 22*Power(t2,2) - 
                  13*Power(t2,3) + 4*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (52 - 300*t2 + 131*Power(t2,2) + 37*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s2*(-15 + (139 + 18*t1)*t2 - 
                  2*(201 + 46*t1)*Power(t2,2) + 
                  12*(37 + 5*t1)*Power(t2,3) - 166*Power(t2,4) + 
                  (1 - 2*t1)*Power(t2,5) - Power(t2,6)) + 
               t1*(26 - 30*t2 - 17*Power(t2,2) + 20*Power(t2,3) + 
                  5*Power(t2,4) - 6*Power(t2,5) + 2*Power(t2,6))) + 
            Power(s1,6)*(Power(t1,2)*(17 - 29*t2) + 
               Power(s2,2)*(9 - 21*t2) + Power(-1 + t2,2)*(19 + 2*t2) + 
               t1*(44 - 78*t2 + 34*Power(t2,2)) + 
               s2*(-44 + 78*t2 - 34*Power(t2,2) + t1*(-26 + 50*t2))) + 
            Power(s1,5)*(Power(-1 + t2,2)*(37 - 91*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(9 + 5*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(15 - 35*t2 + 36*Power(t2,2)) - 
               t1*(9 + 78*t2 - 143*Power(t2,2) + 56*Power(t2,3)) + 
               s2*(30 + 37*t2 - 124*Power(t2,2) + 57*Power(t2,3) + 
                  t1*(-24 + 30*t2 - 38*Power(t2,2)))) + 
            Power(s1,4)*(Power(-1 + t2,2)*
                (-99 + 50*t2 + 96*Power(t2,2)) + 
               Power(t1,2)*(5 - 30*t2 + 48*Power(t2,2) - 
                  19*Power(t2,3)) + 
               Power(s2,2)*(15 - 46*t2 + 14*Power(t2,2) + 
                  21*Power(t2,3)) + 
               t1*(60 - 76*t2 + 83*Power(t2,2) - 110*Power(t2,3) + 
                  43*Power(t2,4)) + 
               s2*(-49 - 50*t2 + 145*Power(t2,2) - 12*Power(t2,3) - 
                  34*Power(t2,4) + 
                  2*t1*(-6 + 32*t2 - 31*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(-2*Power(-1 + t2,2)*
                (-63 - 30*t2 + 58*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s2,2)*(15 - 99*t2 + 122*Power(t2,2) - 
                  46*Power(t2,3) - 10*Power(t2,4)) + 
               Power(t1,2)*(1 - 41*t2 + 42*Power(t2,2) - 
                  24*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(-51 + 13*t2 + 103*Power(t2,2) - 102*Power(t2,3) + 
                  56*Power(t2,4) - 19*Power(t2,5)) + 
               s2*(-68 + 372*t2 - 438*Power(t2,2) + 83*Power(t2,3) + 
                  42*Power(t2,4) + 9*Power(t2,5) + 
                  t1*(4 + 56*t2 - 56*Power(t2,2) + 26*Power(t2,3) + 
                     6*Power(t2,4))))))*R1q(s1))/
     (s*(-1 + s1)*s1*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (s - s2 + t1)*(-s + s1 - t2)*Power(-1 + s + t2,3)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (16*(Power(s,10)*t2 + Power(s,9)*
          (s1 + Power(t1,2) - 7*s1*t2 - 4*t1*t2 + t2*(-6 - s2 + 8*t2)) + 
         2*Power(1 + s1 - t2,2)*Power(-1 + t2,2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (Power(s1,3) + t2 - 2*Power(t2,2) - Power(s1,2)*(1 + 2*t2) + 
            s1*(-1 + 3*t2 + Power(t2,2))) + 
         Power(s,8)*(Power(t1,2)*(-5 + 6*t2) + 
            3*Power(s1,2)*(-2 + 7*t2) + 
            t1*(2 + 19*t2 + 2*s2*t2 - 29*Power(t2,2)) + 
            t2*(13 + 6*s2 - 42*t2 - 9*s2*t2 + 27*Power(t2,2)) - 
            s1*(5 + s2 + 6*t1 - 2*s2*t1 + 8*Power(t1,2) - 44*t2 - 
               6*s2*t2 - 21*t1*t2 + 48*Power(t2,2))) + 
         Power(s,7)*(1 - 30*t2 + 2*s2*t2 - 5*Power(s2,2)*t2 + 
            82*Power(t2,2) + 43*s2*Power(t2,2) + 
            Power(s2,2)*Power(t2,2) - 124*Power(t2,3) - 
            33*s2*Power(t2,3) + 50*Power(t2,4) - 
            5*Power(s1,3)*(-3 + 7*t2) + 
            Power(t1,2)*(7 - 25*t2 + 15*Power(t2,2)) + 
            Power(s1,2)*(27 + Power(s2,2) + 27*Power(t1,2) + 
               t1*(29 - 43*t2) + s2*(3 - 14*t1 - 17*t2) - 121*t2 + 
               117*Power(t2,2)) - 
            t1*(8 + (16 + 9*s2)*t2 - (121 + 12*s2)*Power(t2,2) + 
               93*Power(t2,3)) + 
            s1*(6 + Power(t1,2)*(33 - 47*t2) - 100*t2 - Power(s2,2)*t2 + 
               237*Power(t2,2) - 136*Power(t2,3) + 
               t1*(9 - 128*t2 + 134*Power(t2,2)) + 
               s2*(7 - 45*t2 + 43*Power(t2,2) + t1*(-4 + 8*t2)))) + 
         Power(s,6)*(-3 + 113*t2 - 61*s2*t2 + 21*Power(s2,2)*t2 - 
            172*Power(t2,2) + 21*s2*Power(t2,2) - 
            38*Power(s2,2)*Power(t2,2) + 217*Power(t2,3) + 
            124*s2*Power(t2,3) + 6*Power(s2,2)*Power(t2,3) - 
            202*Power(t2,4) - 65*s2*Power(t2,4) + 55*Power(t2,5) + 
            5*Power(s1,4)*(-4 + 7*t2) - 
            Power(s1,3)*(49 + 6*Power(s2,2) + 50*Power(t1,2) + 
               t1*(54 - 40*t2) + s2*(2 - 40*t1 - 30*t2) - 158*t2 + 
               144*Power(t2,2)) + 
            Power(t1,2)*(7 + 14*t2 - 49*Power(t2,2) + 20*Power(t2,3)) + 
            t1*(6 + (-75 + 33*s2)*t2 - 2*(54 + 23*s2)*Power(t2,2) + 
               5*(67 + 6*s2)*Power(t2,3) - 173*Power(t2,4)) + 
            Power(s1,2)*(-29 + 279*t2 - 499*Power(t2,2) + 
               262*Power(t2,3) + Power(s2,2)*(-2 + 15*t2) + 
               Power(t1,2)*(-93 + 151*t2) + 
               t1*(-63 + 342*t2 - 248*Power(t2,2)) + 
               s2*(-35 + t1*(34 - 90*t2) + 89*t2 - 80*Power(t2,2))) + 
            s1*(-22 + 123*t2 - 436*Power(t2,2) + 561*Power(t2,3) - 
               206*Power(t2,4) - 
               5*Power(s2,2)*(1 - 6*t2 + 2*Power(t2,2)) - 
               3*Power(t1,2)*(14 - 55*t2 + 40*Power(t2,2)) + 
               t1*(29 + 199*t2 - 616*Power(t2,2) + 375*Power(t2,3)) + 
               s2*(3 + 69*t2 - 230*Power(t2,2) + 120*Power(t2,3) + 
                  t1*(-7 - 19*t2 + 18*Power(t2,2))))) + 
         Power(s,5)*(-3*Power(s1,5)*(-5 + 7*t2) + 
            Power(t1,2)*(-35 + 84*t2 - 29*Power(t2,2) - 
               45*Power(t2,3) + 15*Power(t2,4)) + 
            t1*(20 + (208 - 84*s2)*t2 + (-321 + 161*s2)*Power(t2,2) - 
               (242 + 95*s2)*Power(t2,3) + 5*(105 + 8*s2)*Power(t2,4) - 
               205*Power(t2,5)) + 
            Power(s1,4)*(31 + 14*Power(s2,2) + 55*Power(t1,2) + 
               t1*(46 - 10*t2) - 90*t2 + 86*Power(t2,2) - 
               5*s2*(12*t1 + 7*t2)) + 
            t2*(-275 + 539*t2 - 409*Power(t2,2) + 316*Power(t2,3) - 
               200*Power(t2,4) + 36*Power(t2,5) + 
               Power(s2,2)*(-37 + 129*t2 - 119*Power(t2,2) + 
                  15*Power(t2,3)) + 
               s2*(157 - 349*t2 + 95*Power(t2,2) + 185*Power(t2,3) - 
                  75*Power(t2,4))) + 
            Power(s1,3)*(63 + Power(s2,2)*(15 - 60*t2) - 354*t2 + 
               477*Power(t2,2) - 225*Power(t2,3) - 
               4*Power(t1,2)*(-37 + 65*t2) + 
               t1*(135 - 482*t2 + 242*Power(t2,2)) + 
               s2*(36 - 17*t2 + 60*Power(t2,2) + 4*t1*(-26 + 63*t2))) + 
            Power(s1,2)*(15 - 273*t2 + 940*Power(t2,2) - 
               955*Power(t2,3) + 304*Power(t2,4) + 
               Power(s2,2)*(20 - 92*t2 + 62*Power(t2,2)) + 
               Power(t1,2)*(111 - 452*t2 + 364*Power(t2,2)) + 
               t1*(14 - 650*t2 + 1315*Power(t2,2) - 623*Power(t2,3)) + 
               s2*(35 - 202*t2 + 279*Power(t2,2) - 117*Power(t2,3) + 
                  t1*(-22 + 265*t2 - 264*Power(t2,2)))) + 
            s1*(102 - 286*t2 + 473*Power(t2,2) - 881*Power(t2,3) + 
               740*Power(t2,4) - 180*Power(t2,5) + 
               Power(s2,2)*(16 - 120*t2 + 184*Power(t2,2) - 
                  33*Power(t2,3)) - 
               Power(t1,2)*(15 + 122*t2 - 350*Power(t2,2) + 
                  175*Power(t2,3)) + 
               2*t1*(-56 + 66*t2 + 411*Power(t2,2) - 705*Power(t2,3) + 
                  300*Power(t2,4)) + 
               s2*(-58 + 148*t2 + 179*Power(t2,2) - 474*Power(t2,3) + 
                  165*Power(t2,4) + 
                  t1*(46 - 80*t2 - 87*Power(t2,2) + 40*Power(t2,3))))) + 
         Power(s,4)*(10 - 50*t1 + 49*Power(t1,2) + 363*t2 - 202*s2*t2 + 
            35*Power(s2,2)*t2 - 217*t1*t2 + 122*s2*t1*t2 - 
            187*Power(t1,2)*t2 - 1078*Power(t2,2) + 727*s2*Power(t2,2) - 
            184*Power(s2,2)*Power(t2,2) + 889*t1*Power(t2,2) - 
            364*s2*t1*Power(t2,2) + 258*Power(t1,2)*Power(t2,2) + 
            1064*Power(t2,3) - 838*s2*Power(t2,3) + 
            324*Power(s2,2)*Power(t2,3) - 694*t1*Power(t2,3) + 
            322*s2*t1*Power(t2,3) - 116*Power(t1,2)*Power(t2,3) - 
            524*Power(t2,4) + 218*s2*Power(t2,4) - 
            200*Power(s2,2)*Power(t2,4) - 278*t1*Power(t2,4) - 
            100*s2*t1*Power(t2,4) - 15*Power(t1,2)*Power(t2,4) + 
            279*Power(t2,5) + 150*s2*Power(t2,5) + 
            20*Power(s2,2)*Power(t2,5) + 505*t1*Power(t2,5) + 
            30*s2*t1*Power(t2,5) + 6*Power(t1,2)*Power(t2,5) - 
            126*Power(t2,6) - 51*s2*Power(t2,6) - 159*t1*Power(t2,6) + 
            13*Power(t2,7) + Power(s1,6)*(-6 + 7*t2) - 
            Power(s1,5)*(16*Power(s2,2) + 36*Power(t1,2) + 
               s2*(3 - 50*t1 - 26*t2) + t1*(14 + 11*t2) + 
               2*(-3 + t2 + 6*Power(t2,2))) + 
            Power(s1,4)*(-70 + 206*t2 - 174*Power(t2,2) + 
               59*Power(t2,3) + Power(s2,2)*(-39 + 106*t2) + 
               Power(t1,2)*(-147 + 260*t2) + 
               t1*(-159 + 409*t2 - 153*Power(t2,2)) + 
               s2*(37 + t1*(160 - 338*t2) - 137*t2 + 15*Power(t2,2))) - 
            Power(s1,3)*(27 - 473*t2 + 1080*Power(t2,2) - 
               780*Power(t2,3) + 176*Power(t2,4) + 
               Power(s2,2)*(42 - 183*t2 + 166*Power(t2,2)) + 
               Power(t1,2)*(171 - 666*t2 + 554*Power(t2,2)) + 
               t1*(137 - 1022*t2 + 1576*Power(t2,2) - 
                  600*Power(t2,3)) + 
               s2*(51 + 9*t2 - 227*Power(t2,2) + 92*Power(t2,3) + 
                  t1*(-129 + 663*t2 - 620*Power(t2,2)))) + 
            Power(s1,2)*(-61 + 128*t2 - 785*Power(t2,2) + 
               1620*Power(t2,3) - 1078*Power(t2,4) + 201*Power(t2,5) + 
               2*Power(s2,2)*
                (-22 + 131*t2 - 196*Power(t2,2) + 53*Power(t2,3)) + 
               Power(t1,2)*(-1 + 381*t2 - 924*Power(t2,2) + 
                  486*Power(t2,3)) + 
               t1*(130 + 304*t2 - 2051*Power(t2,2) + 
                  2492*Power(t2,3) - 872*Power(t2,4)) + 
               s2*(61 + 28*t2 - 310*Power(t2,2) + 230*Power(t2,3) - 
                  8*Power(t2,4) - 
                  t1*(67 + 213*t2 - 836*Power(t2,2) + 432*Power(t2,3)))) \
+ s1*(-205 + 765*t2 - 871*Power(t2,2) + 798*Power(t2,3) - 
               1011*Power(t2,4) + 606*Power(t2,5) - 92*Power(t2,6) + 
               Power(t1,2)*(89 - 212*t2 - 82*Power(t2,2) + 
                  410*Power(t2,3) - 160*Power(t2,4)) + 
               Power(s2,2)*(-21 + 200*t2 - 532*Power(t2,2) + 
                  450*Power(t2,3) - 50*Power(t2,4)) + 
               t1*(166 - 759*t2 + 310*Power(t2,2) + 1538*Power(t2,3) - 
                  1830*Power(t2,4) + 595*Power(t2,5)) + 
               s2*(119 - 600*t2 + 716*Power(t2,2) + 102*Power(t2,3) - 
                  465*Power(t2,4) + 110*Power(t2,5) + 
                  2*t1*(-44 + 181*t2 - 93*Power(t2,2) - 
                     125*Power(t2,3) + 35*Power(t2,4))))) - 
         s*(-1 + t2)*(Power(s1,7)*
             (6*Power(s2,2) + 6*Power(t1,2) - 2*s2*(5 + 6*t1 - 5*t2) - 
               10*t1*(-1 + t2) + Power(-1 + t2,2)) + 
            Power(-1 + t2,2)*
             (-2 + (-25 + 8*s2)*t2 + 
               (96 - 59*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (-99 + 106*s2 - 24*Power(s2,2))*Power(t2,3) + 
               (29 - 60*s2 + 30*Power(s2,2))*Power(t2,4) + 
               (-2 + s2 - Power(s2,2))*Power(t2,5) + 2*Power(t2,6) - 
               Power(t1,2)*(2 + t2 - 3*Power(t2,2) - 12*Power(t2,3) + 
                  Power(t2,4)) + 
               t1*(4 + (26 - 8*s2)*t2 + (-101 + 43*s2)*Power(t2,2) + 
                  (95 - 58*s2)*Power(t2,3) + (-17 + s2)*Power(t2,4) - 
                  6*Power(t2,5) + 3*Power(t2,6))) + 
            Power(s1,6)*(Power(t1,2)*(12 - 26*t2) + 
               Power(s2,2)*(4 - 18*t2) - 
               Power(-1 + t2,2)*(-17 + 6*t2) + 
               3*t1*(9 - 26*t2 + 17*Power(t2,2)) + 
               s2*(-29 + 82*t2 - 53*Power(t2,2) + 4*t1*(-4 + 11*t2))) + 
            Power(s1,5)*(Power(s2,2)*(4 - 11*t2 + 3*Power(t2,2)) + 
               Power(-1 + t2,2)*(14 - 78*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(26 - 87*t2 + 57*Power(t2,2)) - 
               2*t1*(-3 + 59*t2 - 112*Power(t2,2) + 56*Power(t2,3)) + 
               s2*(11 + 89*t2 - 217*Power(t2,2) + 117*Power(t2,3) - 
                  2*t1*(17 - 53*t2 + 32*Power(t2,2)))) + 
            Power(s1,4)*(Power(t1,2)*t2*
                (-109 + 225*t2 - 80*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (27 + 32*t2 - 144*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*(19 - 68*t2 + 38*Power(t2,2) + 
                  47*Power(t2,3)) + 
               3*t1*(7 - 30*t2 + 94*Power(t2,2) - 118*Power(t2,3) + 
                  47*Power(t2,4)) + 
               s2*(13 - 75*t2 - 51*Power(t2,2) + 251*Power(t2,3) - 
                  138*Power(t2,4) + 
                  t1*(-26 + 214*t2 - 316*Power(t2,2) + 56*Power(t2,3)))) \
+ Power(s1,3)*(Power(s2,2)*(20 - 131*t2 + 265*Power(t2,2) - 
                  120*Power(t2,3) - 68*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (6 + 80*t2 + 14*Power(t2,2) - 136*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(t1,2)*(-34 + 83*t2 + 103*Power(t2,2) - 
                  254*Power(t2,3) + 68*Power(t2,4)) - 
               2*t1*(-6 + 56*t2 - 151*Power(t2,2) + 224*Power(t2,3) - 
                  180*Power(t2,4) + 57*Power(t2,5)) + 
               s2*(-57 + 135*t2 - 19*Power(t2,2) - 29*Power(t2,3) - 
                  122*Power(t2,4) + 92*Power(t2,5) + 
                  t1*(29 + 27*t2 - 415*Power(t2,2) + 471*Power(t2,3) - 
                     44*Power(t2,4)))) + 
            s1*(-1 + t2)*(-25 + 176*t2 - 343*Power(t2,2) + 
               224*Power(t2,3) - 28*Power(t2,4) + 14*Power(t2,5) - 
               19*Power(t2,6) + Power(t2,7) + 
               Power(s2,2)*t2*
                (12 - 80*t2 + 181*Power(t2,2) - 130*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(t1,2)*(-1 - 10*t2 + 81*Power(t2,2) - 
                  87*Power(t2,3) - 10*Power(t2,4) + 5*Power(t2,5)) - 
               2*t1*(-13 + 85*t2 - 149*Power(t2,2) + 71*Power(t2,3) + 
                  30*Power(t2,4) - 33*Power(t2,5) + 10*Power(t2,6)) + 
               s2*(8 - 118*t2 + 379*Power(t2,2) - 436*Power(t2,3) + 
                  154*Power(t2,4) + 10*Power(t2,5) + 5*Power(t2,6) + 
                  t1*(-8 + 86*t2 - 223*Power(t2,2) + 114*Power(t2,3) + 
                     83*Power(t2,4) - 8*Power(t2,5)))) + 
            Power(s1,2)*(t1*Power(-1 + t2,2)*
                (-47 + 47*t2 + 96*Power(t2,2) - 114*Power(t2,3) + 
                  61*Power(t2,4)) + 
               Power(t1,2)*(-12 + 116*t2 - 227*Power(t2,2) + 
                  43*Power(t2,3) + 120*Power(t2,4) - 30*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (-57 + 99*t2 + 50*Power(t2,2) - 10*Power(t2,3) - 
                  69*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*(6 - 76*t2 + 289*Power(t2,2) - 
                  427*Power(t2,3) + 182*Power(t2,4) + 36*Power(t2,5)) + 
               s2*(-(Power(-1 + t2,2)*
                     (51 - 220*t2 + 141*Power(t2,2) + 56*Power(t2,3) + 
                       33*Power(t2,4))) + 
                  t1*(35 - 186*t2 + 176*Power(t2,2) + 264*Power(t2,3) - 
                     337*Power(t2,4) + 28*Power(t2,5))))) + 
         Power(s,3)*(-(Power(s1,7)*(-1 + t2)) + 
            Power(s1,6)*(-14 + 9*Power(s2,2) + 13*Power(t1,2) + 
               s2*(5 - 22*t1 - 11*t2) + 25*t2 - 11*Power(t2,2) + 
               t1*(-3 + 9*t2)) + 
            (-1 + t2)*(15 + (277 - 146*s2 + 18*Power(s2,2))*t2 + 
               (-869 + 581*s2 - 116*Power(s2,2))*Power(t2,2) + 
               (812 - 760*s2 + 246*Power(s2,2))*Power(t2,3) - 
               2*(144 - 157*s2 + 90*Power(s2,2))*Power(t2,4) + 
               (104 + 42*s2 + 15*Power(s2,2))*Power(t2,5) - 
               (50 + 19*s2)*Power(t2,6) + 2*Power(t2,7) + 
               Power(t1,2)*(35 - 129*t2 + 203*Power(t2,2) - 
                  133*Power(t2,3) + 6*Power(t2,4) + Power(t2,5)) + 
               t1*(-48 + (-158 + 99*s2)*t2 + 
                  (767 - 329*s2)*Power(t2,2) + 
                  (-754 + 295*s2)*Power(t2,3) + 
                  (40 - 43*s2)*Power(t2,4) + 
                  4*(55 + 3*s2)*Power(t2,5) - 79*Power(t2,6))) + 
            Power(s1,5)*(38 + Power(t1,2)*(93 - 151*t2) + 
               Power(s2,2)*(47 - 93*t2) - 47*t2 - 17*Power(t2,2) + 
               26*Power(t2,3) + 2*t1*(59 - 117*t2 + 43*Power(t2,2)) + 
               s2*(-87 + 178*t2 - 61*Power(t2,2) + 8*t1*(-17 + 30*t2))) \
+ Power(s1,4)*(91 - 511*t2 + 748*Power(t2,2) - 348*Power(t2,3) + 
               20*Power(t2,4) + 
               Power(s2,2)*(52 - 215*t2 + 203*Power(t2,2)) + 
               Power(t1,2)*(158 - 557*t2 + 451*Power(t2,2)) + 
               t1*(185 - 919*t2 + 1195*Power(t2,2) - 417*Power(t2,3)) + 
               s2*(-41 + 439*t2 - 715*Power(t2,2) + 273*Power(t2,3) - 
                  4*t1*(47 - 182*t2 + 158*Power(t2,2)))) - 
            Power(s1,3)*(49 + 143*t2 - 1181*Power(t2,2) + 
               1763*Power(t2,3) - 867*Power(t2,4) + 93*Power(t2,5) + 
               Power(s2,2)*(-50 + 299*t2 - 453*Power(t2,2) + 
                  168*Power(t2,3)) + 
               Power(t1,2)*(-43 + 577*t2 - 1166*Power(t2,2) + 
                  600*Power(t2,3)) + 
               t1*(-23 + 790*t2 - 2427*Power(t2,2) + 
                  2428*Power(t2,3) - 748*Power(t2,4)) + 
               s2*(-31 + 67*t2 + 399*Power(t2,2) - 791*Power(t2,3) + 
                  336*Power(t2,4) + 
                  t1*(31 - 700*t2 + 1465*Power(t2,2) - 728*Power(t2,3)))\
) + Power(s1,2)*(151 - 230*t2 + 207*Power(t2,2) - 1040*Power(t2,3) + 
               1632*Power(t2,4) - 805*Power(t2,5) + 85*Power(t2,6) + 
               Power(s2,2)*(47 - 344*t2 + 825*Power(t2,2) - 
                  650*Power(t2,3) + 69*Power(t2,4)) + 
               Power(t1,2)*(-100 + 156*t2 + 510*Power(t2,2) - 
                  998*Power(t2,3) + 379*Power(t2,4)) + 
               t1*(-204 + 403*t2 + 922*Power(t2,2) - 
                  2977*Power(t2,3) + 2567*Power(t2,4) - 713*Power(t2,5)\
) - s2*(167 - 474*t2 + 293*Power(t2,2) + 26*Power(t2,3) + 
                  119*Power(t2,4) - 133*Power(t2,5) + 
                  t1*(-141 + 230*t2 + 705*Power(t2,2) - 
                     1298*Power(t2,3) + 398*Power(t2,4)))) + 
            s1*(206 - 1073*t2 + 1726*Power(t2,2) - 1160*Power(t2,3) + 
               710*Power(t2,4) - 710*Power(t2,5) + 329*Power(t2,6) - 
               28*Power(t2,7) + 
               Power(t1,2)*(-85 + 395*t2 - 564*Power(t2,2) + 
                  76*Power(t2,3) + 285*Power(t2,4) - 93*Power(t2,5)) + 
               Power(s2,2)*(14 - 173*t2 + 652*Power(t2,2) - 
                  1004*Power(t2,3) + 560*Power(t2,4) - 35*Power(t2,5)) + 
               t1*(-139 + 970*t2 - 1716*Power(t2,2) + 368*Power(t2,3) + 
                  1549*Power(t2,4) - 1396*Power(t2,5) + 366*Power(t2,6)) \
+ s2*(-113 + 807*t2 - 1771*Power(t2,2) + 1454*Power(t2,3) - 
                  199*Power(t2,4) - 201*Power(t2,5) + 21*Power(t2,6) + 
                  2*t1*(41 - 261*t2 + 421*Power(t2,2) - 66*Power(t2,3) - 
                     185*Power(t2,4) + 36*Power(t2,5))))) + 
         Power(s,2)*(-2*Power(s1,7)*
             (Power(s2,2) + Power(t1,2) + t1*(-1 + t2) - 
               2*Power(-1 + t2,2) - s2*(-1 + 2*t1 + t2)) + 
            Power(s1,6)*(-3*Power(-1 + t2,2)*(3 + 4*t2) + 
               3*Power(s2,2)*(-9 + 13*t2) + Power(t1,2)*(-35 + 47*t2) + 
               t1*(-52 + 94*t2 - 42*Power(t2,2)) + 
               s2*(52 + t1*(62 - 86*t2) - 94*t2 + 42*Power(t2,2))) + 
            Power(-1 + t2,2)*(9 + (118 - 55*s2 + 4*Power(s2,2))*t2 + 
               (-405 + 263*s2 - 40*Power(s2,2))*Power(t2,2) + 
               (384 - 387*s2 + 107*Power(s2,2))*Power(t2,3) - 
               2*(56 - 96*s2 + 49*Power(s2,2))*Power(t2,4) + 
               (23 + 2*s2 + 6*Power(s2,2))*Power(t2,5) - 
               (14 + 3*s2)*Power(t2,6) + 
               Power(t1,2)*(13 - 37*t2 + 62*Power(t2,2) - 
                  64*Power(t2,3) + 5*Power(t2,4)) + 
               t1*(-22 + (-79 + 43*s2)*t2 - 
                  8*(-47 + 21*s2)*Power(t2,2) + 
                  (-388 + 175*s2)*Power(t2,3) + 
                  (69 - 10*s2)*Power(t2,4) + (55 + 2*s2)*Power(t2,5) - 
                  23*Power(t2,6))) + 
            Power(s1,5)*(Power(t1,2)*(-77 + 245*t2 - 184*Power(t2,2)) + 
               Power(s2,2)*(-31 + 125*t2 - 110*Power(t2,2)) + 
               Power(-1 + t2,2)*(-75 + 125*t2 + 2*Power(t2,2)) + 
               t1*(-110 + 469*t2 - 568*Power(t2,2) + 209*Power(t2,3)) + 
               s2*(83 - 410*t2 + 531*Power(t2,2) - 204*Power(t2,3) + 
                  2*t1*(54 - 185*t2 + 147*Power(t2,2)))) + 
            Power(s1,4)*(Power(-1 + t2,2)*
                (5 + 300*t2 - 384*Power(t2,2) + 32*Power(t2,3)) + 
               Power(s2,2)*(-28 + 167*t2 - 247*Power(t2,2) + 
                  104*Power(t2,3)) + 
               Power(t1,2)*(-58 + 421*t2 - 701*Power(t2,2) + 
                  334*Power(t2,3)) + 
               t1*(-57 + 567*t2 - 1446*Power(t2,2) + 1351*Power(t2,3) - 
                  415*Power(t2,4)) + 
               s2*(-41 - 135*t2 + 822*Power(t2,2) - 1007*Power(t2,3) + 
                  361*Power(t2,4) + 
                  t1*(86 - 600*t2 + 972*Power(t2,2) - 450*Power(t2,3)))) \
+ Power(s1,3)*(Power(t1,2)*(52 + 57*t2 - 713*Power(t2,2) + 
                  948*Power(t2,3) - 326*Power(t2,4)) + 
               Power(s2,2)*(-41 + 268*t2 - 568*Power(t2,2) + 
                  377*Power(t2,3) - 18*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-39 + 118*t2 + 407*Power(t2,2) - 506*Power(t2,3) + 
                  48*Power(t2,4)) + 
               t1*(13 + 228*t2 - 1295*Power(t2,2) + 2360*Power(t2,3) - 
                  1770*Power(t2,4) + 464*Power(t2,5)) + 
               s2*(43 + t2 + 18*Power(t2,2) - 559*Power(t2,3) + 
                  803*Power(t2,4) - 306*Power(t2,5) + 
                  t1*(-43 - 245*t2 + 1257*Power(t2,2) - 
                     1389*Power(t2,3) + 384*Power(t2,4)))) - 
            s1*(-1 + t2)*(-106 + 632*t2 - 1083*Power(t2,2) + 
               637*Power(t2,3) - 158*Power(t2,4) + 183*Power(t2,5) - 
               111*Power(t2,6) + 6*Power(t2,7) + 
               Power(s2,2)*(-4 + 72*t2 - 323*Power(t2,2) + 
                  581*Power(t2,3) - 364*Power(t2,4) + 6*Power(t2,5)) + 
               Power(t1,2)*(29 - 172*t2 + 371*Power(t2,2) - 
                  211*Power(t2,3) - 81*Power(t2,4) + 32*Power(t2,5)) + 
               t1*(75 - 546*t2 + 1058*Power(t2,2) - 537*Power(t2,3) - 
                  384*Power(t2,4) + 459*Power(t2,5) - 129*Power(t2,6)) + 
               s2*(51 - 460*t2 + 1168*Power(t2,2) - 1138*Power(t2,3) + 
                  339*Power(t2,4) + 32*Power(t2,5) + 12*Power(t2,6) + 
                  t1*(-39 + 306*t2 - 622*Power(t2,2) + 216*Power(t2,3) + 
                     241*Power(t2,4) - 38*Power(t2,5)))) + 
            Power(s1,2)*(-(Power(s2,2)*
                  (28 - 245*t2 + 792*Power(t2,2) - 1068*Power(t2,3) + 
                    488*Power(t2,4) + 13*Power(t2,5))) + 
               Power(-1 + t2,2)*
                (-148 + 140*t2 + 145*Power(t2,2) + 237*Power(t2,3) - 
                  329*Power(t2,4) + 28*Power(t2,5)) + 
               Power(t1,2)*(70 - 350*t2 + 365*Power(t2,2) + 
                  319*Power(t2,3) - 575*Power(t2,4) + 163*Power(t2,5)) + 
               t1*(133 - 490*t2 + 265*Power(t2,2) + 1144*Power(t2,3) - 
                  2110*Power(t2,4) + 1378*Power(t2,5) - 320*Power(t2,6)) \
+ s2*(150 - 715*t2 + 1121*Power(t2,2) - 674*Power(t2,3) + 
                  217*Power(t2,4) - 219*Power(t2,5) + 120*Power(t2,6) - 
                  t1*(103 - 429*t2 + 155*Power(t2,2) + 987*Power(t2,3) - 
                     1018*Power(t2,4) + 186*Power(t2,5))))))*
       R1q(1 - s + s1 - t2))/
     (s*(-1 + s1)*(s - s2 + t1)*Power(-s + s1 - t2,3)*(1 - s + s1 - t2)*
       Power(-1 + s + t2,3)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(2*Power(s,6)*t2 + Power(s,5)*
          (2*Power(t1,2) + s1*(2 - 10*t2) - 9*t1*t2 + 
            t2*(-2 - 2*s2 + 7*t2)) + 
         Power(s,4)*(-(Power(t1,2)*(4 + t2)) + 
            4*Power(s1,2)*(-2 + 5*t2) + 
            t1*(4 + 15*t2 + 4*s2*t2 - 11*Power(t2,2)) + 
            t2*(-55 + 14*s2 + 11*t2 - 9*s2*t2 + 6*Power(t2,2)) + 
            s1*(-13*t1 - 12*Power(t1,2) + t2 + 29*t1*t2 - 
               28*Power(t2,2) + s2*(-2 + 4*t1 + 11*t2))) + 
         Power(s,3)*(2 + 150*t2 - 10*Power(s2,2)*t2 - 144*Power(t2,2) + 
            12*s2*Power(t2,2) + 2*Power(s2,2)*Power(t2,2) + 
            10*Power(t2,3) - 8*s2*Power(t2,3) + Power(t2,4) - 
            4*Power(s1,3)*(-3 + 5*t2) + 
            Power(t1,2)*(-4 + 2*t2 - 3*Power(t2,2)) + 
            t1*(-4 + (-3 + 2*s2)*t2 + (32 + s2)*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s1,2)*(-10 + 2*Power(s2,2) + 28*Power(t1,2) + 
               s2*(5 - 20*t1 - 24*t2) + 23*t2 + 44*Power(t2,2) - 
               4*t1*(-9 + 8*t2)) - 
            s1*(59 - 5*Power(t1,2) - 180*t2 + 2*Power(s2,2)*t2 + 
               47*Power(t2,2) + 18*Power(t2,3) + 
               t1*(10 + 34*t2 - 17*Power(t2,2)) + 
               s2*(-16 + 35*t2 - 34*Power(t2,2) + t1*(-4 + 3*t2)))) + 
         Power(s,2)*(2*Power(s1,4)*(-4 + 5*t2) + 
            Power(t1,2)*(16 + 5*Power(t2,2)) + 
            t1*(-12 + (33 - 30*s2)*t2 + (-71 + 7*s2)*Power(t2,2) + 
               (16 - 3*s2)*Power(t2,3)) - 
            Power(s1,3)*(-34 + 8*Power(s2,2) + 32*Power(t1,2) + 
               s2*(3 - 36*t1 - 26*t2) + 39*t2 + 32*Power(t2,2) - 
               4*t1*(-8 + 3*t2)) - 
            t2*(104 - 190*t2 + 61*Power(t2,2) + Power(t2,3) - 
               2*Power(s2,2)*(11 - 6*t2 + Power(t2,2)) + 
               s2*(54 - 62*t2 - 4*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*(109 - 133*t2 + 92*Power(t2,2) + 
               20*Power(t2,3) + 2*Power(s2,2)*(1 + 5*t2) + 
               Power(t1,2)*(19 + 6*t2) + 
               t1*(30 + 43*t2 + 5*Power(t2,2)) - 
               s2*(27 + 14*t2 + 50*Power(t2,2) + t1*(19 + 15*t2))) + 
            s1*(87 - 335*t2 + 143*Power(t2,2) - 34*Power(t2,3) - 
               2*Power(t2,4) - 
               2*Power(s2,2)*(5 - 9*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(9 + 2*t2 + 7*Power(t2,2)) + 
               t1*(11 + 133*t2 - 51*Power(t2,2) - 5*Power(t2,3)) + 
               s2*(16 - 39*t2 + 11*Power(t2,2) + 21*Power(t2,3) + 
                  6*t1*(-1 - 4*t2 + Power(t2,2))))) - 
         Power(-1 + s1,2)*(-4 + 
            Power(s1,3)*(4*Power(s2,2) + 4*Power(t1,2) + 
               s2*(3 - 8*t1 - 3*t2) + 2*(-1 + t2) + t1*(-1 + t2)) + 
            21*t2 + 4*s2*t2 - 2*Power(s2,2)*t2 - 15*Power(t2,2) - 
            21*s2*Power(t2,2) - Power(t2,3) + 16*s2*Power(t2,3) - 
            2*Power(s2,2)*Power(t2,3) - Power(t2,4) + s2*Power(t2,4) + 
            Power(t1,2)*(-4 - t2 + Power(t2,2)) + 
            t1*(8 + 2*(-18 + 7*s2)*t2 + (44 - 9*s2)*Power(t2,2) + 
               (-16 + 3*s2)*Power(t2,3)) + 
            Power(s1,2)*(1 + 2*t2 - 3*Power(t2,2) - 
               3*Power(t1,2)*(3 + t2) - 4*Power(s2,2)*(1 + 2*t2) - 
               2*t1*(-10 + 7*t2 + 3*Power(t2,2)) + 
               s2*(-23 + 14*t2 + 9*Power(t2,2) + 3*t1*(5 + 3*t2))) + 
            s1*(-13 + 10*t2 + Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(9 + 4*t2 - Power(t2,2)) + 
               Power(s2,2)*(2 + 4*t2 + 6*Power(t2,2)) + 
               t1*(20 - 56*t2 + 31*Power(t2,2) + 5*Power(t2,3)) - 
               s2*(4 - 44*t2 + 33*Power(t2,2) + 7*Power(t2,3) + 
                  2*t1*(7 + 3*t2 + 2*Power(t2,2))))) + 
         s*(-6 - 2*Power(s1,5)*(-1 + t2) + 30*t2 + 46*s2*t2 - 
            14*Power(s2,2)*t2 - 27*Power(t2,2) - 102*s2*Power(t2,2) + 
            10*Power(s2,2)*Power(t2,2) + 20*Power(t2,3) + 
            24*s2*Power(t2,3) - 4*Power(s2,2)*Power(t2,3) - Power(t2,4) + 
            2*s2*Power(t2,4) - Power(t1,2)*(14 + 2*t2 + Power(t2,2)) + 
            t1*(20 + (-72 + 38*s2)*t2 + (94 - 17*s2)*Power(t2,2) + 
               6*(-5 + s2)*Power(t2,3)) + 
            Power(s1,4)*(-26 + 10*Power(s2,2) + 18*Power(t1,2) + 
               s2*(3 - 28*t1 - 14*t2) + 19*t2 + 9*Power(t2,2) + 
               t1*(8 + t2)) - 
            Power(s1,3)*(-31 - 26*t2 + 55*Power(t2,2) + 8*Power(t2,3) + 
               2*Power(s2,2)*(5 + 8*t2) + Power(t1,2)*(37 + 8*t2) + 
               t1*(10 + 40*t2 + 17*Power(t2,2)) - 
               s2*(-22 + 63*t2 + 34*Power(t2,2) + 23*t1*(2 + t2))) + 
            Power(s1,2)*(-62 + 5*t2 + 38*Power(t2,2) + 40*Power(t2,3) + 
               Power(t2,4) + Power(t1,2)*(18 + 6*t2 - 5*Power(t2,2)) + 
               2*Power(s2,2)*(2 - t2 + 6*Power(t2,2)) + 
               t1*(154 - 117*t2 + 70*Power(t2,2) + 12*Power(t2,3)) - 
               s2*(43 - 14*t2 + 98*Power(t2,2) + 20*Power(t2,3) + 
                  t1*(30 - 14*t2 + 11*Power(t2,2)))) + 
            s1*(-3 + 46*t2 - 25*Power(t2,2) - 48*Power(t2,3) - 
               4*Power(t2,4) + Power(t1,2)*(15 + 4*t2 + 6*Power(t2,2)) + 
               t1*(16 - 192*t2 + 129*Power(t2,2) - 26*Power(t2,3)) - 
               2*Power(s2,2)*
                (-6 + 4*t2 - 5*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-50 + 159*t2 - 30*Power(t2,2) + 32*Power(t2,3) + 
                  2*Power(t2,4) + 
                  t1*(-16 - 7*t2 - 24*Power(t2,2) + 6*Power(t2,3))))))*
       R2q(s))/(s*(-1 + s1)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(s - s2 + t1)*(-s + s1 - t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(Power(s,8)*(Power(t1,2) - 2*t1*t2 - Power(t2,2)) + 
         Power(s,7)*(Power(t1,2)*(-6 - 9*s1 + 2*t2) + 
            t2*(s2*(-2 + t2) + (8 - 3*t2)*t2 + s1*(-2 + 6*t2)) + 
            t1*(2 + (6 + s2)*t2 - 3*Power(t2,2) + s1*(-4 + 2*s2 + 15*t2))\
) + Power(s,5)*(-4 + 31*t2 - 8*s2*t2 - 79*Power(t2,2) + 
            83*s2*Power(t2,2) + 20*Power(t2,3) - 28*s2*Power(t2,3) + 
            Power(t2,4) + 3*s2*Power(t2,4) - Power(t2,5) + 
            Power(t1,2)*(4 + 11*t2 - 3*Power(t2,2)) + 
            t1*(12 + (-97 + 2*s2)*t2 + (53 - 6*s2)*Power(t2,2) + 
               (-28 + s2)*Power(t2,3) + 5*Power(t2,4)) + 
            Power(s1,3)*(-7*Power(s2,2) - 77*Power(t1,2) + 
               4*t2*(-6 + 5*t2) + s2*(11 + 54*t1 + 5*t2) + 
               t1*(-75 + 85*t2)) + 
            Power(s1,2)*(19 - 58*t2 + 56*Power(t2,2) - 30*Power(t2,3) + 
               Power(s2,2)*(-3 + 10*t2) + Power(t1,2)*(-93 + 60*t2) + 
               t1*(-11 + 121*t2 - 80*Power(t2,2)) + 
               s2*(2 + t1*(36 - 22*t2) - 44*t2 + 13*Power(t2,2))) + 
            s1*(-5 - 21*t2 - 3*Power(s2,2)*(-1 + t2)*t2 + 
               48*Power(t2,2) - 29*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,2)*(-42 + 57*t2 - 9*Power(t2,2)) + 
               s2*(1 + (-51 + 8*t1)*t2 + (80 - 9*t1)*Power(t2,2) - 
                  15*Power(t2,3)) + 
               t1*(61 - 63*t2 - 16*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s,6)*(1 - 6*t2 + 9*s2*t2 - 17*Power(t2,2) - 
            17*s2*Power(t2,2) + 11*Power(t2,3) + 3*s2*Power(t2,3) - 
            3*Power(t2,4) + Power(t1,2)*(11 - 10*t2 + Power(t2,2)) + 
            t1*(-10 + (19 - 5*s2)*t2 + (1 + 2*s2)*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,2) + 35*Power(t1,2) + 
               t1*(27 - 48*t2) + (11 - 15*t2)*t2 - s2*(2 + 16*t1 + t2)) \
- s1*(2 + (-19 - 13*s2 + Power(s2,2))*t2 + (35 + 6*s2)*Power(t2,2) - 
               15*Power(t2,3) + Power(t1,2)*(-38 + 17*t2) + 
               t1*(2 + 39*t2 - 25*Power(t2,2) + s2*(7 + t2)))) + 
         Power(s,4)*(2 + 20*t1 - 45*Power(t1,2) - 42*t2 - 26*s2*t2 + 
            112*t1*t2 + 30*s2*t1*t2 + 25*Power(t1,2)*t2 + 
            327*Power(t2,2) - 118*s2*Power(t2,2) - 
            12*Power(s2,2)*Power(t2,2) - 143*t1*Power(t2,2) - 
            7*s2*t1*Power(t2,2) - 6*Power(t1,2)*Power(t2,2) - 
            264*Power(t2,3) + 109*s2*Power(t2,3) + 91*t1*Power(t2,3) + 
            2*s2*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) + 
            50*Power(t2,4) - 16*s2*Power(t2,4) - 33*t1*Power(t2,4) - 
            2*Power(t2,5) + s2*Power(t2,5) + 2*t1*Power(t2,5) + 
            Power(s1,4)*(20*Power(s2,2) + 105*Power(t1,2) + 
               t1*(110 - 90*t2) + (26 - 15*t2)*t2 - 
               2*s2*(12 + 50*t1 + 5*t2)) + 
            Power(s1,3)*(-38 + Power(s2,2)*(13 - 37*t2) + 42*t2 - 
               33*Power(t2,2) + 30*Power(t2,3) - 
               5*Power(t1,2)*(-21 + 23*t2) + 
               2*t1*(8 - 110*t2 + 65*Power(t2,2)) + 
               s2*(-8 + 99*t2 - 12*Power(t2,2) + t1*(-62 + 90*t2))) + 
            Power(s1,2)*(10*Power(t1,2)*(4 - 11*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-1 - 19*t2 + 20*Power(t2,2)) + 
               t1*(-113 + 74*t2 + 66*Power(t2,2) - 40*Power(t2,3)) - 
               2*(6 - 13*t2 + 24*Power(t2,2) - 3*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s2*(2 + 128*t2 - 159*Power(t2,2) + 29*Power(t2,3) + 
                  t1*(11 + 13*t2 + 3*Power(t2,2)))) - 
            s1*(-37 + 205*t2 - 418*Power(t2,2) + 74*Power(t2,3) - 
               8*Power(t2,4) - 3*Power(t2,5) + 
               Power(s2,2)*t2*(3 - 14*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(17 + 32*t2 - 20*Power(t2,2) + Power(t2,3)) + 
               t1*(116 - 187*t2 + 170*Power(t2,2) - 68*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s2*(3 - 123*t2 + 310*Power(t2,2) - 103*Power(t2,3) + 
                  12*Power(t2,4) + 
                  t1*(-22 + 6*t2 - 21*Power(t2,2) + 9*Power(t2,3))))) + 
         Power(s,3)*(12 - 70*t1 + 74*Power(t1,2) - 27*t2 + 74*s2*t2 + 
            44*t1*t2 - 75*s2*t1*t2 - 80*Power(t1,2)*t2 - 
            345*Power(t2,2) - 34*s2*Power(t2,2) + 
            40*Power(s2,2)*Power(t2,2) + 111*t1*Power(t2,2) + 
            48*s2*t1*Power(t2,2) + 34*Power(t1,2)*Power(t2,2) + 
            494*Power(t2,3) - 83*s2*Power(t2,3) - 
            18*Power(s2,2)*Power(t2,3) - 121*t1*Power(t2,3) - 
            20*s2*t1*Power(t2,3) - 4*Power(t1,2)*Power(t2,3) - 
            197*Power(t2,4) + 47*s2*Power(t2,4) + 58*t1*Power(t2,4) + 
            3*s2*t1*Power(t2,4) + 18*Power(t2,5) - 3*s2*Power(t2,5) - 
            10*t1*Power(t2,5) + 
            Power(s1,5)*(-30*Power(s2,2) - 91*Power(t1,2) + 
               2*t2*(-7 + 3*t2) + 2*s2*(13 + 55*t1 + 5*t2) + 
               t1*(-90 + 57*t2)) + 
            Power(s1,4)*(20 + 26*t2 - 7*Power(t2,2) - 15*Power(t2,3) + 
               10*Power(t1,2)*(-4 + 13*t2) + 
               4*Power(s2,2)*(-4 + 17*t2) + 
               t1*(2 + 240*t2 - 115*Power(t2,2)) + 
               s2*(12 + t1*(28 - 155*t2) - 139*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(-16 - 36*t2 - 9*Power(t2,2) + 40*Power(t2,3) + 
               12*Power(t2,4) + 
               Power(s2,2)*(6 + 29*t2 - 51*Power(t2,2)) + 
               Power(t1,2)*(16 + 70*t2 - 50*Power(t2,2)) + 
               t1*(94 - 88*t2 - 123*Power(t2,2) + 70*Power(t2,3)) + 
               s2*(15 - 129*t2 + 188*Power(t2,2) - 27*Power(t2,3) + 
                  t1*(-52 + 3*t2 + 38*Power(t2,2)))) + 
            Power(s1,2)*(-103 + 667*t2 - 602*Power(t2,2) + 
               48*Power(t2,3) - 34*Power(t2,4) - 3*Power(t2,5) + 
               2*Power(t1,2)*
                (15 - 5*t2 - 13*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(7 + 18*t2 - 45*Power(t2,2) + 
                  14*Power(t2,3)) + 
               t1*(10 - 79*t2 + 183*Power(t2,2) - 56*Power(t2,3) - 
                  9*Power(t2,4)) + 
               s2*(36 - 357*t2 + 472*Power(t2,2) - 130*Power(t2,3) + 
                  18*Power(t2,4) + 
                  t1*(-32 + 29*t2 - 29*Power(t2,2) + 15*Power(t2,3)))) + 
            s1*(-40 + 454*t2 - 1145*Power(t2,2) + 717*Power(t2,3) - 
               56*Power(t2,4) + 7*Power(t2,5) + 
               Power(t1,2)*(43 - 46*t2 + 10*Power(t2,2)) - 
               Power(s2,2)*t2*
                (23 - 18*t2 - 16*Power(t2,2) + Power(t2,3)) + 
               t1*(74 - 22*t2 + 204*Power(t2,2) - 193*Power(t2,3) + 
                  55*Power(t2,4) - 2*Power(t2,5)) + 
               s2*(-5 - 109*t2 + 319*Power(t2,2) - 336*Power(t2,3) + 
                  39*Power(t2,4) - 3*Power(t2,5) + 
                  t1*(-18 + 2*t2 + 3*Power(t2,2) + 9*Power(t2,3) - 
                     3*Power(t2,4))))) + 
         s*(16 - 40*t1 + 24*Power(t1,2) - 96*t2 + 40*s2*t2 + 143*t1*t2 - 
            40*s2*t1*t2 - 45*Power(t1,2)*t2 + 120*Power(t2,2) - 
            142*s2*Power(t2,2) + 24*Power(s2,2)*Power(t2,2) - 
            97*t1*Power(t2,2) + 46*s2*t1*Power(t2,2) + 
            33*Power(t1,2)*Power(t2,2) - 85*Power(t2,3) + 
            135*s2*Power(t2,3) - 22*Power(s2,2)*Power(t2,3) + 
            t1*Power(t2,3) - 29*s2*t1*Power(t2,3) - 
            4*Power(t1,2)*Power(t2,3) + 46*Power(t2,4) - 
            40*s2*Power(t2,4) + 8*Power(s2,2)*Power(t2,4) + 
            t1*Power(t2,4) + 5*s2*t1*Power(t2,4) - Power(t2,5) + 
            s2*Power(t2,5) - 2*t1*Power(t2,5) + 
            Power(s1,7)*(-11*Power(s2,2) + s2*(3 + 26*t1 + t2) + 
               t1*(-7 - 15*t1 + 3*t2)) + 
            Power(s1,6)*(-7 + 12*t2 - 5*Power(t2,2) + 
               Power(t1,2)*(27 + 32*t2) + Power(s2,2)*(11 + 34*t2) + 
               t1*(-9 + 49*t2 - 10*Power(t2,2)) - 
               s2*(-14 + 43*t2 + Power(t2,2) + 8*t1*(5 + 8*t2))) + 
            Power(s1,5)*(21 - 11*t2 - 19*Power(t2,2) + 9*Power(t2,3) + 
               Power(t1,2)*(2 - 55*t2 - 21*Power(t2,2)) - 
               2*Power(s2,2)*(-8 + 22*t2 + 19*Power(t2,2)) + 
               t1*(41 - 77*t2 - 57*Power(t2,2) + 13*Power(t2,3)) + 
               s2*(-32 + 42*t2 + 72*Power(t2,2) - 2*Power(t2,3) + 
                  t1*(-24 + 113*t2 + 51*Power(t2,2)))) + 
            Power(s1,4)*(-79 + 50*t2 + 44*Power(t2,2) - 
               10*Power(t2,3) - 5*Power(t2,4) + 
               Power(t1,2)*(-18 - 9*t2 + 37*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,2)*(-33 + 22*t2 + 37*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(-108 + 92*t2 + 122*Power(t2,2) + 8*Power(t2,3) - 
                  8*Power(t2,4)) - 
               s2*(-128 + 134*t2 + 71*Power(t2,2) + 32*Power(t2,3) - 
                  3*Power(t2,4) + 
                  t1*(-56 + 9*t2 + 93*Power(t2,2) + 12*Power(t2,3)))) + 
            s1*(17 + 25*t2 + 143*Power(t2,2) - 184*Power(t2,3) - 
               8*Power(t2,4) + 7*Power(t2,5) + 
               Power(t1,2)*(-40 + 53*t2 - 41*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-40 + 25*t2 - 38*Power(t2,2) + 17*Power(t2,3)) + 
               t1*(21 - 142*t2 + 64*Power(t2,2) + 38*Power(t2,3) - 
                  11*Power(t2,4) + 6*Power(t2,5)) + 
               s2*(-24 + 96*t2 - 153*Power(t2,2) + 141*Power(t2,3) - 
                  29*Power(t2,4) - 7*Power(t2,5) + 
                  t1*(24 + 54*t2 - 30*Power(t2,2) + 11*Power(t2,3) - 
                     3*Power(t2,4)))) - 
            Power(s1,3)*(-28 - 234*t2 + 308*Power(t2,2) - 
               23*Power(t2,3) - 22*Power(t2,4) - Power(t2,5) + 
               Power(t1,2)*(-5 - 18*t2 - 14*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*(-17 + 16*t2 + 31*Power(t2,2) - 
                  2*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-129 + 100*t2 + 67*Power(t2,2) + 63*Power(t2,3) - 
                  15*Power(t2,4) - 2*Power(t2,5)) + 
               s2*(99 + 103*t2 - 317*Power(t2,2) + 23*Power(t2,3) + 
                  7*Power(t2,4) + Power(t2,5) + 
                  t1*(10 + 11*t2 + Power(t2,2) - 25*Power(t2,3) + 
                     Power(t2,4)))) + 
            Power(s1,2)*(20 - 278*t2 + 121*Power(t2,2) + 
               183*Power(t2,3) - 39*Power(t2,4) - 7*Power(t2,5) + 
               Power(t1,2)*(15 + 6*t2 - 22*Power(t2,2)) + 
               Power(s2,2)*(16 - 20*t2 + 79*Power(t2,2) - 
                  24*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(-27 + 32*t2 + 45*Power(t2,2) + 3*Power(t2,3) + 
                  3*Power(t2,4) - 6*Power(t2,5)) - 
               s2*(22 - 229*t2 + 214*Power(t2,2) + 91*Power(t2,3) - 
                  41*Power(t2,4) - 7*Power(t2,5) + 
                  t1*(32 + 43*t2 - 27*Power(t2,2) - 5*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(-1 + s1,3)*(4 + 2*Power(s1,5)*Power(s2 - t1,2) - 8*t1 + 
            4*Power(t1,2) - 27*t2 + 8*s2*t2 + 36*t1*t2 - 8*s2*t1*t2 - 
            9*Power(t1,2)*t2 + 41*Power(t2,2) - 29*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) - 35*t1*Power(t2,2) + 
            11*s2*t1*Power(t2,2) + 8*Power(t1,2)*Power(t2,2) - 
            17*Power(t2,3) + 20*s2*Power(t2,3) - 
            2*Power(s2,2)*Power(t2,3) + 8*t1*Power(t2,3) - 
            8*s2*t1*Power(t2,3) - Power(t1,2)*Power(t2,3) - 
            Power(t2,4) + s2*Power(t2,4) - t1*Power(t2,4) + 
            s2*t1*Power(t2,4) - 
            Power(s1,4)*(s2 - t1)*
             (6 - 6*t2 - t1*(1 + 5*t2) + s2*(-1 + 7*t2)) + 
            Power(s1,3)*(4*Power(-1 + t2,2) + 
               Power(t1,2)*(-3 + 3*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(-6 + t2 + 9*Power(t2,2)) + 
               t1*(-5 - 6*t2 + 11*Power(t2,2)) + 
               s2*(1 + 14*t2 - 15*Power(t2,2) + 
                  t1*(9 - 4*t2 - 13*Power(t2,2)))) - 
            Power(s1,2)*(2*Power(-1 + t2,2)*(1 + 3*t2) + 
               Power(t1,2)*(-1 - 8*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-4 - 10*t2 + 5*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-15 + 8*t2 + Power(t2,2) + 6*Power(t2,3)) + 
               s2*(9 - 2*t2 + 5*Power(t2,2) - 12*Power(t2,3) + 
                  t1*(9 + 10*t2 - 5*Power(t2,2) - 6*Power(t2,3)))) + 
            s1*(Power(-1 + t2,2)*(15 + 3*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-8 - 2*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-3 + 3*t2 - 8*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-12 - 4*t2 + 13*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*(8 - 38*t2 + 23*Power(t2,2) + 4*Power(t2,3) + 
                  3*Power(t2,4) + 
                  t1*(-8 + 2*t2 - 9*Power(t2,2) + 2*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s,2)*(-23 + 78*t1 - 59*Power(t1,2) + 113*t2 - 79*s2*t2 - 
            189*t1*t2 + 79*s2*t1*t2 + 88*Power(t1,2)*t2 + 
            28*Power(t2,2) + 198*s2*Power(t2,2) - 
            48*Power(s2,2)*Power(t2,2) + 43*t1*Power(t2,2) - 
            72*s2*t1*Power(t2,2) - 51*Power(t1,2)*Power(t2,2) - 
            190*Power(t2,3) - 116*s2*Power(t2,3) + 
            38*Power(s2,2)*Power(t2,3) + 63*t1*Power(t2,3) + 
            38*s2*t1*Power(t2,3) + 6*Power(t1,2)*Power(t2,3) + 
            154*Power(t2,4) - 9*s2*Power(t2,4) - 
            8*Power(s2,2)*Power(t2,4) - 32*t1*Power(t2,4) - 
            7*s2*t1*Power(t2,4) - 38*Power(t2,5) + 5*s2*Power(t2,5) + 
            10*t1*Power(t2,5) + 
            Power(s1,6)*(25*Power(s2,2) + 49*Power(t1,2) + 
               t1*(39 - 20*t2) - (-3 + t2)*t2 - s2*(14 + 72*t1 + 5*t2)) + 
            Power(s1,2)*(73 - 656*t2 + 1176*Power(t2,2) - 
               457*Power(t2,3) - 20*Power(t2,4) - 10*Power(t2,5) + 
               Power(t1,2)*(-5 + 26*t2 - 34*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*(-16 + 45*t2 - 5*Power(t2,2) - 
                  32*Power(t2,3) + 3*Power(t2,4)) - 
               t1*(-141 + 100*t2 + 71*Power(t2,2) - 121*Power(t2,3) + 
                  38*Power(t2,4) + 2*Power(t2,5)) + 
               s2*(-17 + (89 - 5*t1)*t2 + (-483 + 26*t1)*Power(t2,2) + 
                  (330 - 16*t1)*Power(t2,3) + (-13 + 5*t1)*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(s1,4)*(-17 + 56*t2 + 41*Power(t2,2) - 37*Power(t2,3) - 
               3*Power(t2,4) + 9*Power(s2,2)*(-2 + t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(-33 + 30*t2 + 45*Power(t2,2)) + 
               t1*(-50 + 123*t2 + 118*Power(t2,2) - 50*Power(t2,3)) + 
               s2*(-5 + 5*t2 - 149*Power(t2,2) + 12*Power(t2,3) + 
                  t1*(68 - 92*t2 - 72*Power(t2,2)))) + 
            Power(s1,5)*(8 - 41*t2 - 67*Power(s2,2)*t2 + 
               16*Power(t2,2) + 3*Power(t2,3) - 
               3*Power(t1,2)*(8 + 29*t2) + 
               t1*(-4 - 151*t2 + 53*Power(t2,2)) + 
               s2*(t1*(33 + 139*t2) + 2*(-7 + 55*t2 + Power(t2,2)))) + 
            Power(s1,3)*(259 - 563*t2 + 195*Power(t2,2) + 
               10*Power(t2,3) + 30*Power(t2,4) + Power(t2,5) + 
               Power(s2,2)*(9 - 27*t2 + 26*Power(t2,2) - 
                  24*Power(t2,3)) - 
               2*Power(t1,2)*
                (2 - 28*t2 + 6*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(106 - 79*t2 - 154*Power(t2,2) + 14*Power(t2,3) + 
                  19*Power(t2,4)) - 
               s2*(157 - 426*t2 + 230*Power(t2,2) - 75*Power(t2,3) + 
                  12*Power(t2,4) + 
                  t1*(14 + 54*t2 - 63*Power(t2,2) + Power(t2,3)))) + 
            s1*(-4 - 216*t2 + 577*Power(t2,2) - 705*Power(t2,3) + 
               231*Power(t2,4) + 7*Power(t2,5) + 
               Power(t1,2)*(12 + 15*t2 - 12*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,2)*t2*
                (56 - 68*t2 + 34*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(-22 - 48*t2 + 11*Power(t2,2) + 108*Power(t2,3) - 
                  45*Power(t2,4) + 8*Power(t2,5)) + 
               s2*(23 - 58*t2 + 102*Power(t2,2) + 99*Power(t2,3) - 
                  118*Power(t2,4) + 
                  t1*(-15 - 35*t2 - 9*Power(t2,2) + 11*Power(t2,3) + 
                     2*Power(t2,4))))))*T2q(s1,s))/
     (s*(-1 + s1)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (s - s2 + t1)*(-s + s1 - t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(Power(s,8)*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
         Power(s,7)*(Power(t1,2)*(6 + 6*s1 - 5*t2) + 
            t2*(s1*(2 - 3*t2) - s2*(-2 + t2) + 2*t2*(-4 + 3*t2)) - 
            t1*(2 + (6 + s2)*t2 - 13*Power(t2,2) + s1*(-4 + 2*s2 + 9*t2))\
) + Power(s,6)*(-1 + 6*t2 - 9*s2*t2 + 21*Power(t2,2) + 
            19*s2*Power(t2,2) - 40*Power(t2,3) - 6*s2*Power(t2,3) + 
            15*Power(t2,4) + Power(t1,2)*
             (-11 + 25*t2 - 10*Power(t2,2)) + 
            t1*(10 + 5*(-5 + s2)*t2 - (37 + 5*s2)*Power(t2,2) + 
               37*Power(t2,3)) + 
            Power(s1,2)*(-Power(s2,2) - 14*Power(t1,2) + 
               15*t1*(-1 + t2) + s2*(2 + 10*t1 + t2) + t2*(-5 + 3*t2)) + 
            s1*(2 + 29*Power(t1,2)*(-1 + t2) + 
               (-19 - 7*s2 + Power(s2,2))*t2 + (38 + 3*s2)*Power(t2,2) - 
               15*Power(t2,3) + 
               t1*(-4 + s2*(7 - 8*t2) + 59*t2 - 51*Power(t2,2)))) + 
         Power(s,5)*(4 - 34*t2 + 8*s2*t2 + 25*Power(t2,2) - 
            68*s2*Power(t2,2) + 90*Power(t2,3) + 64*s2*Power(t2,3) - 
            81*Power(t2,4) - 15*s2*Power(t2,4) + 20*Power(t2,5) - 
            Power(t1,2)*(4 + 29*t2 - 39*Power(t2,2) + 10*Power(t2,3)) + 
            t1*(-12 + (121 - 2*s2)*t2 + 20*(-4 + s2)*Power(t2,2) - 
               2*(49 + 5*s2)*Power(t2,3) + 60*Power(t2,4)) + 
            Power(s1,3)*(4*Power(s2,2) + 16*Power(t1,2) + 
               t1*(18 - 11*t2) - (-3 + t2)*t2 - s2*(5 + 18*t1 + 2*t2)) + 
            s1*(2 + 53*t2 - 173*Power(t2,2) + 136*Power(t2,3) - 
               32*Power(t2,4) + 3*Power(s2,2)*t2*(-1 + 2*t2) + 
               3*Power(t1,2)*(16 - 38*t2 + 19*Power(t2,2)) + 
               s2*(-1 + (34 + 13*t1)*t2 - (55 + 13*t1)*Power(t2,2) + 
                  15*Power(t2,3)) - 
               t1*(49 + 58*t2 - 242*Power(t2,2) + 120*Power(t2,3))) + 
            Power(s1,2)*(-13 + Power(s2,2)*(3 - 10*t2) + 43*t2 - 
               58*Power(t2,2) + 16*Power(t2,3) - 
               9*Power(t1,2)*(-6 + 7*t2) + 
               9*t1*(5 - 16*t2 + 8*Power(t2,2)) + 
               s2*(-2 + 23*t2 + 2*Power(t2,2) + t1*(-33 + 49*t2)))) + 
         Power(s,4)*(-2 + 51*t2 + 26*s2*t2 - 161*Power(t2,2) + 
            39*s2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) + 
            33*Power(t2,3) - 185*s2*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,3) + 146*Power(t2,4) + 
            110*s2*Power(t2,4) - 82*Power(t2,5) - 20*s2*Power(t2,5) + 
            15*Power(t2,6) + Power(t1,2)*
             (45 - 55*t2 - 13*Power(t2,2) + 28*Power(t2,3) - 
               5*Power(t2,4)) + 
            t1*(-20 - 2*(62 + 15*s2)*t2 + (357 + 9*s2)*Power(t2,2) + 
               (-97 + 25*s2)*Power(t2,3) - 2*(73 + 5*s2)*Power(t2,4) + 
               60*Power(t2,5)) + 
            Power(s1,4)*(-5*Power(s2,2) + s2*(3 + 14*t1 + t2) + 
               t1*(-7 - 9*t1 + 3*t2)) + 
            Power(s1,3)*(5 - 37*t2 + 47*Power(t2,2) - 15*Power(t2,3) + 
               Power(s2,2)*(-13 + 25*t2) + Power(t1,2)*(-51 + 65*t2) + 
               t1*(-66 + 127*t2 - 43*Power(t2,2)) + 
               s2*(20 + t1*(56 - 82*t2) - 31*t2 - 7*Power(t2,2))) + 
            Power(s1,2)*(43 - 187*t2 + 283*Power(t2,2) - 
               173*Power(t2,3) + 34*Power(t2,4) + 
               Power(t1,2)*(-73 + 188*t2 - 114*Power(t2,2)) + 
               Power(s2,2)*(1 + 25*t2 - 35*Power(t2,2)) + 
               t1*(-32 + 317*t2 - 428*Power(t2,2) + 141*Power(t2,3)) + 
               s2*(-9 - 62*t2 + 85*Power(t2,2) - 12*Power(t2,3) + 
                  t1*(22 - 117*t2 + 103*Power(t2,2)))) + 
            s1*(-34 + 42*t2 + 233*Power(t2,2) - 408*Power(t2,3) + 
               203*Power(t2,4) - 36*Power(t2,5) + 
               Power(s2,2)*t2*(3 - 26*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(-25 + 134*t2 - 179*Power(t2,2) + 
                  60*Power(t2,3)) + 
               t1*(140 - 206*t2 - 275*Power(t2,2) + 469*Power(t2,3) - 
                  154*Power(t2,4)) + 
               s2*(3 - 89*t2 + 240*Power(t2,2) - 162*Power(t2,3) + 
                  34*Power(t2,4) + 
                  t1*(-22 + 49*t2 + 6*Power(t2,2) - 15*Power(t2,3))))) + 
         Power(s,3)*(-12 + 2*Power(s1,5)*Power(s2 - t1,2) + 70*t1 - 
            74*Power(t1,2) + 30*t2 - 74*s2*t2 - 116*t1*t2 + 
            75*s2*t1*t2 + 185*Power(t1,2)*t2 + 108*Power(t2,2) + 
            177*s2*Power(t2,2) - 20*Power(s2,2)*Power(t2,2) - 
            257*t1*Power(t2,2) - 124*s2*t1*Power(t2,2) - 
            146*Power(t1,2)*Power(t2,2) - 218*Power(t2,3) + 
            56*s2*Power(t2,3) + 8*Power(s2,2)*Power(t2,3) + 
            414*t1*Power(t2,3) + 62*s2*t1*Power(t2,3) + 
            20*Power(t1,2)*Power(t2,3) + 24*Power(t2,4) - 
            250*s2*Power(t2,4) + 6*Power(s2,2)*Power(t2,4) - 
            18*t1*Power(t2,4) + 4*s2*t1*Power(t2,4) + 
            10*Power(t1,2)*Power(t2,4) + 102*Power(t2,5) + 
            106*s2*Power(t2,5) - 130*t1*Power(t2,5) - 
            5*s2*t1*Power(t2,5) - Power(t1,2)*Power(t2,5) - 
            40*Power(t2,6) - 15*s2*Power(t2,6) + 37*t1*Power(t2,6) + 
            6*Power(t2,7) + Power(s1,4)*
             (Power(t1,2)*(26 - 32*t2) + Power(s2,2)*(16 - 22*t2) + 
               4*Power(-1 + t2,2)*(-2 + 3*t2) + 
               9*t1*(3 - 4*t2 + Power(t2,2)) + 
               3*s2*(-5 + 4*t2 + Power(t2,2) + 2*t1*(-7 + 9*t2))) + 
            Power(s1,3)*(-(Power(-1 + t2,3)*(-35 + 29*t2)) + 
               Power(s2,2)*(9 - 65*t2 + 60*Power(t2,2)) + 
               Power(t1,2)*(54 - 153*t2 + 103*Power(t2,2)) + 
               t1*(89 - 297*t2 + 289*Power(t2,2) - 81*Power(t2,3)) + 
               s2*(t1*(-51 + 194*t2 - 151*Power(t2,2)) + 
                  19*(-1 + 5*t2 - 5*Power(t2,2) + Power(t2,3)))) - 
            Power(s1,2)*(-(Power(-1 + t2,2)*
                  (-15 + 160*t2 - 117*Power(t2,2) + 32*Power(t2,3))) + 
               Power(s2,2)*(7 + 24*t2 - 105*Power(t2,2) + 
                  54*Power(t2,3)) + 
               Power(t1,2)*(-36 + 185*t2 - 276*Power(t2,2) + 
                  107*Power(t2,3)) + 
               t1*(13 + 333*t2 - 836*Power(t2,2) + 639*Power(t2,3) - 
                  149*Power(t2,4)) + 
               s2*(8 - 165*t2 + 334*Power(t2,2) - 223*Power(t2,3) + 
                  46*Power(t2,4) - 
                  t1*(31 + 45*t2 - 233*Power(t2,2) + 117*Power(t2,3)))) \
+ s1*(Power(s2,2)*t2*(7 + 31*t2 - 70*Power(t2,2) + 18*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-55 + 145*t2 + 156*Power(t2,2) - 91*Power(t2,3) + 
                  21*Power(t2,4)) + 
               Power(t1,2)*(-10 - 28*t2 + 141*Power(t2,2) - 
                  155*Power(t2,3) + 38*Power(t2,4)) - 
               t1*(134 - 479*t2 + 184*Power(t2,2) + 548*Power(t2,3) - 
                  504*Power(t2,4) + 117*Power(t2,5)) + 
               s2*(5 + 72*t2 - 420*Power(t2,2) + 544*Power(t2,3) - 
                  243*Power(t2,4) + 42*Power(t2,5) + 
                  t1*(18 - 111*t2 + 108*Power(t2,2) + 29*Power(t2,3) - 
                     16*Power(t2,4))))) + 
         Power(s,2)*(-1 + t2)*
          (-23 + 78*t1 - 59*Power(t1,2) + 
            2*Power(s1,5)*(3*Power(s2,2) - 6*s2*t1 + 3*Power(t1,2) - 
               2*Power(-1 + t2,2)) + 123*t2 - 79*s2*t2 - 249*t1*t2 + 
            79*s2*t1*t2 + 146*Power(t1,2)*t2 - 114*Power(t2,2) + 
            262*s2*Power(t2,2) - 32*Power(s2,2)*Power(t2,2) + 
            48*t1*Power(t2,2) - 122*s2*t1*Power(t2,2) - 
            126*Power(t1,2)*Power(t2,2) - 40*Power(t2,3) - 
            102*s2*Power(t2,3) + 12*Power(s2,2)*Power(t2,3) + 
            165*t1*Power(t2,3) + 86*s2*t1*Power(t2,3) + 
            22*Power(t1,2)*Power(t2,3) + 42*Power(t2,4) - 
            124*s2*Power(t2,4) + 6*Power(s2,2)*Power(t2,4) - 
            3*t1*Power(t2,4) - 14*s2*t1*Power(t2,4) + 
            3*Power(t1,2)*Power(t2,4) + 16*Power(t2,5) + 
            49*s2*Power(t2,5) - 52*t1*Power(t2,5) - s2*t1*Power(t2,5) - 
            5*Power(t2,6) - 6*s2*Power(t2,6) + 13*t1*Power(t2,6) + 
            Power(t2,7) + Power(s1,4)*
             (Power(s2,2)*(18 - 36*t2) + 
               4*Power(-1 + t2,2)*(-2 + 3*t2) - 
               6*Power(t1,2)*(-4 + 7*t2) + 
               t1*(33 - 64*t2 + 31*Power(t2,2)) + 
               s2*(-25 + 48*t2 - 23*Power(t2,2) + 6*t1*(-7 + 13*t2))) + 
            Power(s1,3)*(-(Power(-1 + t2,2)*
                  (21 - 8*t2 + 15*Power(t2,2))) + 
               Power(s2,2)*(3 - 81*t2 + 54*Power(t2,2)) + 
               Power(t1,2)*(24 - 121*t2 + 73*Power(t2,2)) + 
               t1*(97 - 305*t2 + 289*Power(t2,2) - 81*Power(t2,3)) + 
               s2*(-39 + 163*t2 - 179*Power(t2,2) + 55*Power(t2,3) + 
                  t1*(-19 + 186*t2 - 119*Power(t2,2)))) - 
            Power(s1,2)*(-(Power(-1 + t2,2)*
                  (39 + 61*t2 - 7*Power(t2,2) + 11*Power(t2,3))) + 
               Power(s2,2)*(8 + 9*t2 - 114*Power(t2,2) + 
                  33*Power(t2,3)) + 
               Power(t1,2)*(-2 + 47*t2 - 162*Power(t2,2) + 
                  53*Power(t2,3)) + 
               t1*(38 + 199*t2 - 570*Power(t2,2) + 421*Power(t2,3) - 
                  88*Power(t2,4)) + 
               s2*(22 - 196*t2 + 349*Power(t2,2) - 228*Power(t2,3) + 
                  53*Power(t2,4) + 
                  t1*(-51 + 55*t2 + 189*Power(t2,2) - 65*Power(t2,3)))) \
+ s1*(Power(s2,2)*t2*(32 + 10*t2 - 65*Power(t2,2) + 9*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-11 + 142*t2 + 40*Power(t2,2) - 8*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(t1,2)*(-9 + 22*t2 + 29*Power(t2,2) - 
                  71*Power(t2,3) + 15*Power(t2,4)) - 
               t1*(28 - 305*t2 + 208*Power(t2,2) + 264*Power(t2,3) - 
                  246*Power(t2,4) + 51*Power(t2,5)) + 
               s2*(23 - 77*t2 - 190*Power(t2,2) + 372*Power(t2,3) - 
                  155*Power(t2,4) + 27*Power(t2,5) + 
                  t1*(-15 - 68*t2 + 75*Power(t2,2) + 46*Power(t2,3) - 
                     10*Power(t2,4))))) + 
         s*Power(-1 + t2,2)*(-16 + 40*t1 - 24*Power(t1,2) + 100*t2 - 
            40*s2*t2 - 159*t1*t2 + 40*s2*t1*t2 + 57*Power(t1,2)*t2 - 
            141*Power(t2,2) + 148*s2*Power(t2,2) - 
            20*Power(s2,2)*Power(t2,2) + 110*t1*Power(t2,2) - 
            56*s2*t1*Power(t2,2) - 51*Power(t1,2)*Power(t2,2) + 
            46*Power(t2,3) - 88*s2*Power(t2,3) + 
            8*Power(s2,2)*Power(t2,3) + 11*t1*Power(t2,3) + 
            44*s2*t1*Power(t2,3) + 7*Power(t1,2)*Power(t2,3) + 
            12*Power(t2,4) - 29*s2*Power(t2,4) + 
            2*Power(s2,2)*Power(t2,4) + 6*t1*Power(t2,4) - 
            8*s2*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) + 10*s2*Power(t2,5) - 10*t1*Power(t2,5) + 
            Power(t2,6) - s2*Power(t2,6) + 2*t1*Power(t2,6) + 
            2*Power(s1,5)*(s2 - t1)*(-4 + 3*s2 - 3*t1 + 4*t2) + 
            Power(s1,4)*(Power(t1,2)*(20 - 14*t2) + 
               12*Power(-1 + t2,2) - 6*Power(s2,2)*(-3 + 2*t2) + 
               t1*(31 - 56*t2 + 25*Power(t2,2)) + 
               s2*(-31 + 56*t2 - 25*Power(t2,2) + t1*(-38 + 26*t2))) + 
            Power(s1,3)*(-(Power(-1 + t2,2)*(7 + 27*t2)) + 
               Power(s2,2)*(-3 - 47*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-4 - 55*t2 + 15*Power(t2,2)) + 
               t1*(37 - 124*t2 + 119*Power(t2,2) - 32*Power(t2,3)) + 
               s2*(-12 + 73*t2 - 92*Power(t2,2) + 31*Power(t2,3) + 
                  t1*(9 + 98*t2 - 19*Power(t2,2)))) + 
            Power(s1,2)*(6*Power(s2,2)*(-2 - t2 + 9*Power(t2,2)) + 
               Power(-1 + t2,2)*(22 + 13*t2 + 21*Power(t2,2)) - 
               2*Power(t1,2)*
                (3 + t2 - 27*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(-46 - 25*t2 + 165*Power(t2,2) - 117*Power(t2,3) + 
                  23*Power(t2,4)) + 
               2*s2*(6 + 26*t2 - 57*Power(t2,2) + 35*Power(t2,3) - 
                  10*Power(t2,4) + 
                  t1*(19 - 18*t2 - 40*Power(t2,2) + 3*Power(t2,3)))) + 
            s1*(Power(s2,2)*t2*(32 + t2 - 27*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (29 + 42*t2 + 4*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(4 + 6*t2 + 14*Power(t2,2) - 
                  21*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(27 + 90*t2 - 97*Power(t2,2) - 64*Power(t2,3) + 
                  54*Power(t2,4) - 10*Power(t2,5)) + 
               s2*(24 - 124*t2 + 24*Power(t2,2) + 105*Power(t2,3) - 
                  36*Power(t2,4) + 7*Power(t2,5) - 
                  t1*(24 + 18*t2 - 7*Power(t2,2) - 24*Power(t2,3) + 
                     Power(t2,4))))) - 
         Power(-1 + t2,3)*(4 + 2*Power(s1,5)*Power(s2 - t1,2) - 8*t1 + 
            4*Power(t1,2) - 27*t2 + 8*s2*t2 + 36*t1*t2 - 8*s2*t1*t2 - 
            9*Power(t1,2)*t2 + 41*Power(t2,2) - 29*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) - 35*t1*Power(t2,2) + 
            11*s2*t1*Power(t2,2) + 8*Power(t1,2)*Power(t2,2) - 
            17*Power(t2,3) + 20*s2*Power(t2,3) - 
            2*Power(s2,2)*Power(t2,3) + 8*t1*Power(t2,3) - 
            8*s2*t1*Power(t2,3) - Power(t1,2)*Power(t2,3) - Power(t2,4) + 
            s2*Power(t2,4) - t1*Power(t2,4) + s2*t1*Power(t2,4) - 
            Power(s1,4)*(s2 - t1)*
             (6 - 6*t2 - t1*(1 + 5*t2) + s2*(-1 + 7*t2)) + 
            Power(s1,3)*(4*Power(-1 + t2,2) + 
               Power(t1,2)*(-3 + 3*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(-6 + t2 + 9*Power(t2,2)) + 
               t1*(-5 - 6*t2 + 11*Power(t2,2)) + 
               s2*(1 + 14*t2 - 15*Power(t2,2) + 
                  t1*(9 - 4*t2 - 13*Power(t2,2)))) - 
            Power(s1,2)*(2*Power(-1 + t2,2)*(1 + 3*t2) + 
               Power(t1,2)*(-1 - 8*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-4 - 10*t2 + 5*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-15 + 8*t2 + Power(t2,2) + 6*Power(t2,3)) + 
               s2*(9 - 2*t2 + 5*Power(t2,2) - 12*Power(t2,3) + 
                  t1*(9 + 10*t2 - 5*Power(t2,2) - 6*Power(t2,3)))) + 
            s1*(Power(-1 + t2,2)*(15 + 3*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*t2*(-8 - 2*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-3 + 3*t2 - 8*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-12 - 4*t2 + 13*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*(8 - 38*t2 + 23*Power(t2,2) + 4*Power(t2,3) + 
                  3*Power(t2,4) + 
                  t1*(-8 + 2*t2 - 9*Power(t2,2) + 2*Power(t2,3) + 
                     Power(t2,4))))))*T3q(1 - s + s1 - t2,s1))/
     (s*(-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*Power(-1 + s + t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(Power(s,7)*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
         Power(s,6)*(Power(t1,2)*(3 + 8*s1 - 4*t2) + 
            t2*(s1*(2 - 5*t2) - s2*(-2 + t2) + t2*(3 + 5*t2)) - 
            t1*(2 + s2*t2 - 11*Power(t2,2) + s1*(-4 + 2*s2 + 13*t2))) + 
         Power(s,5)*(-1 + 6*t2 - 3*s2*t2 - 5*Power(t2,2) + 
            10*s2*Power(t2,2) + 10*Power(t2,3) - 5*s2*Power(t2,3) + 
            10*Power(t2,4) + Power(t1,2)*(1 + 10*t2 - 6*Power(t2,2)) + 
            t1*(4 + (-29 + 2*s2)*t2 - 2*(1 + 2*s2)*Power(t2,2) + 
               26*Power(t2,3)) + 
            Power(s1,2)*(-Power(s2,2) - 27*Power(t1,2) + 
               s2*(2 + 14*t1 + t2) + t2*(-9 + 10*t2) + t1*(-23 + 35*t2)) \
+ s1*(2 + (3 - 11*s2 + Power(s2,2))*t2 + (-7 + 5*s2)*Power(t2,2) - 
               20*Power(t2,3) + Power(t1,2)*(-17 + 29*t2) + 
               t1*(12 + s2 + 28*t2 - 4*s2*t2 - 60*Power(t2,2)))) - 
         Power(s,3)*(-4 + 13*t2 - 16*s2*t2 + 138*Power(t2,2) - 
            55*s2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) - 
            88*Power(t2,3) + 84*s2*Power(t2,3) + 14*Power(t2,4) - 
            29*s2*Power(t2,4) - 9*Power(t2,5) + 10*s2*Power(t2,5) - 
            5*Power(t2,6) + Power(t1,2)*
             (-12 + 40*t2 - 28*Power(t2,2) - 3*Power(t2,3) + 
               Power(t2,4)) + 
            t1*(16 + (-47 + 16*s2)*t2 - 7*(12 + s2)*Power(t2,2) + 
               (108 - 13*s2)*Power(t2,3) + 4*(6 + s2)*Power(t2,4) - 
               26*Power(t2,5)) + 
            Power(s1,4)*(14*Power(s2,2) + 55*Power(t1,2) + 
               t1*(58 - 40*t2) + (11 - 5*t2)*t2 - 
               3*s2*(5 + 20*t1 + 2*t2)) + 
            s1*(18 + (-117 + 46*s2)*t2 + 
               (79 - 97*s2 - 3*Power(s2,2))*Power(t2,2) - 
               5*(10 - 21*s2 + 2*Power(s2,2))*Power(t2,3) + 
               (40 - 30*s2)*Power(t2,4) + 20*Power(t2,5) + 
               Power(t1,2)*(-27 + 32*t2 + 43*Power(t2,2) - 
                  26*Power(t2,3)) + 
               t1*(3 - 3*(-53 + 8*s2)*t2 + (-211 + 37*s2)*Power(t2,2) - 
                  137*Power(t2,3) + 112*Power(t2,4))) + 
            Power(s1,3)*(-5 + Power(t1,2)*(38 - 130*t2) + 
               Power(s2,2)*(1 - 38*t2) - 39*t2 + 6*Power(t2,2) + 
               20*Power(t2,3) + 2*t1*(2 - 117*t2 + 70*Power(t2,2)) + 
               s2*(3 + 83*t2 + 2*Power(t2,2) + t1*(-9 + 124*t2))) + 
            Power(s1,2)*(5 + 15*t2 + 77*Power(t2,2) - 48*Power(t2,3) - 
               30*Power(t2,4) + 
               2*Power(s2,2)*(-2 + t2 + 17*Power(t2,2)) + 
               Power(t1,2)*(-12 - 79*t2 + 100*Power(t2,2)) + 
               t1*(-93 + 99*t2 + 287*Power(t2,2) - 186*Power(t2,3)) + 
               s2*(5 + 14*t2 - 144*Power(t2,2) + 24*Power(t2,3) + 
                  t1*(35 - 19*t2 - 68*Power(t2,2))))) + 
         Power(s,4)*(1 - 15*t2 - 7*s2*t2 + 42*Power(t2,2) - 
            32*s2*Power(t2,2) - 13*Power(t2,3) + 23*s2*Power(t2,3) + 
            13*Power(t2,4) - 10*s2*Power(t2,4) + 10*Power(t2,5) + 
            Power(t1,2)*(-11 + 9*t2 + 11*Power(t2,2) - 4*Power(t2,3)) + 
            t1*(6 + (32 + 7*s2)*t2 + (-86 + 8*s2)*Power(t2,2) - 
               6*(2 + s2)*Power(t2,3) + 34*Power(t2,4)) + 
            Power(s1,3)*(6*Power(s2,2) + 50*Power(t1,2) + 
               t1*(52 - 50*t2) + 5*(3 - 2*t2)*t2 - s2*(9 + 40*t1 + 4*t2)\
) + Power(s1,2)*(-9 + Power(t1,2)*(37 - 85*t2) - 18*t2 - 
               11*Power(s2,2)*t2 + 5*Power(t2,2) + 30*Power(t2,3) + 
               2*t1*(-9 - 66*t2 + 65*Power(t2,2)) + 
               s2*(4 + 40*t2 - 6*Power(t2,2) + t1*(-5 + 46*t2))) + 
            s1*(10 - 6*t2 + 28*Power(t2,2) + 5*Power(s2,2)*Power(t2,2) - 
               31*Power(t2,3) - 30*Power(t2,4) + 
               Power(t1,2)*(-5 - 48*t2 + 40*Power(t2,2)) + 
               t1*(-45 + 103*t2 + 84*Power(t2,2) - 114*Power(t2,3)) - 
               s2*(1 - 11*t2 + 53*Power(t2,2) - 20*Power(t2,3) + 
                  t1*(-9 + 10*t2 + Power(t2,2))))) + 
         Power(s,2)*(-4 + 8*t1 - 4*Power(t1,2) + 35*t2 - 8*s2*t2 - 
            68*t1*t2 + 8*s2*t1*t2 + 33*Power(t1,2)*t2 + 
            102*Power(t2,2) - 43*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) + 17*t1*Power(t2,2) - 
            3*s2*t1*Power(t2,2) - 57*Power(t1,2)*Power(t2,2) - 
            246*Power(t2,3) + 151*s2*Power(t2,3) - 
            10*Power(s2,2)*Power(t2,3) + 114*t1*Power(t2,3) - 
            15*s2*t1*Power(t2,3) + 32*Power(t1,2)*Power(t2,3) + 
            73*Power(t2,4) - 83*s2*Power(t2,4) - 65*t1*Power(t2,4) + 
            11*s2*t1*Power(t2,4) - 2*Power(t1,2)*Power(t2,4) - 
            9*Power(t2,5) + 19*s2*Power(t2,5) - 20*t1*Power(t2,5) - 
            s2*t1*Power(t2,5) + 4*Power(t2,6) - 5*s2*Power(t2,6) + 
            11*t1*Power(t2,6) + Power(t2,7) + 
            Power(s1,5)*(16*Power(s2,2) + 36*Power(t1,2) + 
               t1*(32 - 17*t2) - (-3 + t2)*t2 - s2*(11 + 50*t1 + 4*t2)) \
+ Power(s1,4)*(9 + Power(t1,2)*(17 - 110*t2) + 
               Power(s2,2)*(3 - 58*t2) - 36*t2 + 10*Power(t2,2) + 
               5*Power(t2,3) + 5*t1*(6 - 38*t2 + 15*Power(t2,2)) + 
               s2*(-12 + 86*t2 + 7*Power(t2,2) + t1*(-7 + 151*t2))) + 
            Power(s1,3)*(Power(s2,2)*(-14 + 3*t2 + 78*Power(t2,2)) + 
               Power(t1,2)*(-16 - 49*t2 + 120*Power(t2,2)) - 
               t1*(72 + 13*t2 - 353*Power(t2,2) + 134*Power(t2,3)) - 
               2*(8 - 50*Power(t2,2) + 23*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s2*(18 + 39*t2 - 199*Power(t2,2) + 8*Power(t2,3) - 
                  2*t1*(-26 + 10*t2 + 79*Power(t2,2)))) + 
            Power(s1,2)*(75 - 127*t2 + 43*Power(t2,2) - 
               95*Power(t2,3) + 54*Power(t2,4) + 10*Power(t2,5) + 
               Power(t1,2)*(-20 + 51*t2 + 42*Power(t2,2) - 
                  54*Power(t2,3)) + 
               Power(s2,2)*(4 + 18*t2 - 15*Power(t2,2) - 
                  46*Power(t2,3)) + 
               t1*(7 + 184*t2 - 123*Power(t2,2) - 290*Power(t2,3) + 
                  122*Power(t2,4)) + 
               s2*(-47 + 91*t2 - 113*Power(t2,2) + 203*Power(t2,3) - 
                  26*Power(t2,4) + 
                  t1*(1 - 95*t2 + 60*Power(t2,2) + 62*Power(t2,3)))) + 
            s1*(-23 - 181*t2 + 385*Power(t2,2) - 125*Power(t2,3) + 
               40*Power(t2,4) - 25*Power(t2,5) - 5*Power(t2,6) + 
               Power(s2,2)*t2*
                (-8 + 6*t2 + 9*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,2)*(-21 + 73*t2 - 71*Power(t2,2) - 
                  8*Power(t2,3) + 8*Power(t2,4)) + 
               t1*(44 - 16*t2 - 218*Power(t2,2) + 171*Power(t2,3) + 
                  115*Power(t2,4) - 57*Power(t2,5)) + 
               s2*(8 + 90*t2 - 260*Power(t2,2) + 169*Power(t2,3) - 
                  98*Power(t2,4) + 20*Power(t2,5) + 
                  t1*(-8 + 2*t2 + 58*Power(t2,2) - 44*Power(t2,3) - 
                     4*Power(t2,4))))) + 
         s*(s1 - t2)*(8 - 16*t1 + 8*Power(t1,2) + 20*t2 - 8*s2*t2 + 
            8*s2*t1*t2 - 20*Power(t1,2)*t2 - 149*Power(t2,2) + 
            88*s2*Power(t2,2) - 8*Power(s2,2)*Power(t2,2) + 
            67*t1*Power(t2,2) - 24*s2*t1*Power(t2,2) + 
            20*Power(t1,2)*Power(t2,2) + 139*Power(t2,3) - 
            109*s2*Power(t2,3) + 8*Power(s2,2)*Power(t2,3) - 
            66*t1*Power(t2,3) + 23*s2*t1*Power(t2,3) - 
            11*Power(t1,2)*Power(t2,3) - 20*Power(t2,4) + 
            27*s2*Power(t2,4) + 17*t1*Power(t2,4) - 
            5*s2*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) + 
            3*Power(t2,5) - 5*s2*Power(t2,5) + 6*t1*Power(t2,5) - 
            Power(t2,6) + s2*Power(t2,6) - 2*t1*Power(t2,6) + 
            Power(s1,5)*(-9*Power(s2,2) + s2*(3 + 22*t1 + t2) + 
               t1*(-7 - 13*t1 + 3*t2)) + 
            Power(s1,4)*(-7 + 12*t2 - 5*Power(t2,2) + 
               Power(s2,2)*(-3 + 32*t2) + Power(t1,2)*(-1 + 36*t2) + 
               t1*(-24 + 59*t2 - 13*Power(t2,2)) + 
               s2*(17 + t1*(2 - 66*t2) - 37*t2 - 2*Power(t2,2))) + 
            Power(s1,3)*(Power(s2,2)*(16 - 3*t2 - 42*Power(t2,2)) + 
               Power(t1,2)*(11 + 2*t2 - 34*Power(t2,2)) + 
               2*(4 + 8*t2 - 19*Power(t2,2) + 7*Power(t2,3)) + 
               t1*(25 + 32*t2 - 112*Power(t2,2) + 23*Power(t2,3)) + 
               s2*(-13 - 42*t2 + 88*Power(t2,2) - Power(t2,3) + 
                  5*t1*(-7 + 3*t2 + 14*Power(t2,2)))) + 
            Power(s1,2)*(-40 + 39*t2 - 22*Power(t2,2) + 
               37*Power(t2,3) - 14*Power(t2,4) + 
               Power(t1,2)*(3 - 31*t2 + 3*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(s2,2)*(-8 - 24*t2 + 15*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-25 - 34*t2 + 13*Power(t2,2) + 87*Power(t2,3) - 
                  21*Power(t2,4)) + 
               s2*(56 - 47*t2 + 48*Power(t2,2) - 82*Power(t2,3) + 
                  5*Power(t2,4) + 
                  t1*(8 + 57*t2 - 29*Power(t2,2) - 30*Power(t2,3)))) - 
            s1*(28 - 181*t2 + 178*Power(t2,2) - 33*Power(t2,3) + 
               14*Power(t2,4) - 6*Power(t2,5) + 
               Power(s2,2)*t2*(-16 + 9*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(-12 + 31*t2 - 39*Power(t2,2) + 
                  5*Power(t2,3) + Power(t2,4)) + 
               t1*(-16 + 26*t2 - 59*Power(t2,2) + 38*Power(t2,3) + 
                  33*Power(t2,4) - 10*Power(t2,5)) + 
               s2*(-8 + 144*t2 - 169*Power(t2,2) + 50*Power(t2,3) - 
                  33*Power(t2,4) + 4*Power(t2,5) + 
                  t1*(8 - 16*t2 + 45*Power(t2,2) - 17*Power(t2,3) - 
                     4*Power(t2,4))))) + 
         Power(s1 - t2,2)*(4 + 2*Power(s1,5)*Power(s2 - t1,2) - 8*t1 + 
            4*Power(t1,2) - 27*t2 + 8*s2*t2 + 36*t1*t2 - 8*s2*t1*t2 - 
            9*Power(t1,2)*t2 + 41*Power(t2,2) - 29*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) - 35*t1*Power(t2,2) + 
            11*s2*t1*Power(t2,2) + 8*Power(t1,2)*Power(t2,2) - 
            17*Power(t2,3) + 20*s2*Power(t2,3) - 
            2*Power(s2,2)*Power(t2,3) + 8*t1*Power(t2,3) - 
            8*s2*t1*Power(t2,3) - Power(t1,2)*Power(t2,3) - Power(t2,4) + 
            s2*Power(t2,4) - t1*Power(t2,4) + s2*t1*Power(t2,4) - 
            Power(s1,4)*(s2 - t1)*
             (6 - 6*t2 - t1*(1 + 5*t2) + s2*(-1 + 7*t2)) + 
            Power(s1,3)*(4*Power(-1 + t2,2) + 
               Power(t1,2)*(-3 + 3*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(-6 + t2 + 9*Power(t2,2)) + 
               t1*(-5 - 6*t2 + 11*Power(t2,2)) + 
               s2*(1 + 14*t2 - 15*Power(t2,2) + 
                  t1*(9 - 4*t2 - 13*Power(t2,2)))) - 
            Power(s1,2)*(2*Power(-1 + t2,2)*(1 + 3*t2) + 
               Power(t1,2)*(-1 - 8*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-4 - 10*t2 + 5*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(-15 + 8*t2 + Power(t2,2) + 6*Power(t2,3)) + 
               s2*(9 - 2*t2 + 5*Power(t2,2) - 12*Power(t2,3) + 
                  t1*(9 + 10*t2 - 5*Power(t2,2) - 6*Power(t2,3)))) + 
            s1*(Power(-1 + t2,2)*(15 + 3*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*t2*(-8 - 2*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-3 + 3*t2 - 8*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-12 - 4*t2 + 13*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*(8 - 38*t2 + 23*Power(t2,2) + 4*Power(t2,3) + 
                  3*Power(t2,4) + 
                  t1*(-8 + 2*t2 - 9*Power(t2,2) + 2*Power(t2,3) + 
                     Power(t2,4))))))*T4q(-s + s1 - t2))/
     (s*(-1 + s1)*(s - s2 + t1)*Power(-s + s1 - t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(Power(s,6)*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) - 
         2*Power(s1 - t2,2)*Power(-1 + t2,2)*
          (-5 + 2*Power(s1,2)*(s2 - t1) + 2*t1 + 3*Power(t1,2) + 4*t2 - 
            Power(s2,2)*t2 - 12*t1*t2 + Power(t2,2) + 
            s2*(3 + t1*(-3 + t2) + 8*t2 - Power(t2,2)) + 
            s1*(2 + Power(s2,2) - 3*Power(t1,2) + s2*(-11 + 2*t1 - t2) - 
               2*t2 + 3*t1*(3 + t2))) + 
         Power(s,5)*(Power(t1,2)*(3 + 6*s1 - 2*t2) + 
            t2*(s1*(2 - 3*t2) - s2*(-2 + t2) + t2*(-1 + 3*t2)) - 
            t1*(2 + s2*t2 - 7*Power(t2,2) + s1*(-4 + 2*s2 + 9*t2))) + 
         Power(s,4)*(-1 + 6*t2 - 3*s2*t2 - 5*Power(t2,2) + 
            10*s2*Power(t2,2) - 8*Power(t2,3) - 3*s2*Power(t2,3) + 
            3*Power(t2,4) + Power(t1,2)*(1 + 4*t2 - Power(t2,2)) + 
            t1*(4 + (-25 + 2*s2)*t2 - 2*(-1 + s2)*Power(t2,2) + 
               10*Power(t2,3)) + 
            Power(s1,2)*(-Power(s2,2) - 14*Power(t1,2) + 
               15*t1*(-1 + t2) + s2*(2 + 10*t1 + t2) + t2*(-5 + 3*t2)) + 
            s1*(2 + 11*Power(t1,2)*(-1 + t2) + 
               (-5 - 7*s2 + Power(s2,2))*t2 + (11 + 3*s2)*Power(t2,2) - 
               6*Power(t2,3) + 
               t1*(8 + s2 + 20*t2 - 2*s2*t2 - 24*Power(t2,2)))) + 
         Power(s,3)*(1 - 13*t2 - 7*s2*t2 + 6*Power(t2,2) - 
            6*s2*Power(t2,2) - 4*Power(s2,2)*Power(t2,2) + 
            17*Power(t2,3) + 19*s2*Power(t2,3) - 12*Power(t2,4) - 
            3*s2*Power(t2,4) + Power(t2,5) + Power(t1,2)*(-11 + 7*t2) + 
            t1*(6 + (24 + 7*s2)*t2 + (-46 + 6*s2)*Power(t2,2) - 
               (2 + s2)*Power(t2,3) + 7*Power(t2,4)) + 
            Power(s1,3)*(4*Power(s2,2) + 16*Power(t1,2) + 
               t1*(18 - 11*t2) - (-3 + t2)*t2 - s2*(5 + 18*t1 + 2*t2)) + 
            Power(s1,2)*(-9 + 12*t2 - 7*Power(s2,2)*t2 + 
               27*t1*(-2 + t2)*t2 - 14*Power(t2,2) + 3*Power(t2,3) - 
               3*Power(t1,2)*(-4 + 7*t2) + 
               s2*(4 + 20*t2 - Power(t2,2) + t1*(-3 + 19*t2))) + 
            s1*(8 - 6*t2 - 20*Power(t2,2) + 3*Power(s2,2)*Power(t2,2) + 
               19*Power(t2,3) - 3*Power(t2,4) + 
               3*Power(t1,2)*(-1 - 4*t2 + 2*Power(t2,2)) + 
               t1*(-37 + 41*t2 + 36*Power(t2,2) - 23*Power(t2,3)) - 
               s2*(1 - 13*t2 + 31*Power(t2,2) - 6*Power(t2,3) + 
                  t1*(-9 + 8*t2 + Power(t2,2))))) + 
         Power(s,2)*(4 - 16*t1 + 12*Power(t1,2) - 15*t2 + 16*s2*t2 + 
            35*t1*t2 - 16*s2*t1*t2 - 18*Power(t1,2)*t2 + 25*Power(t2,2) - 
            13*s2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) - 
            16*t1*Power(t2,2) + 19*s2*t1*Power(t2,2) + 
            Power(t1,2)*Power(t2,2) - 36*Power(t2,3) - 
            31*s2*Power(t2,3) - 6*Power(s2,2)*Power(t2,3) + 
            13*t1*Power(t2,3) + 7*s2*t1*Power(t2,3) - 
            3*Power(t1,2)*Power(t2,3) + 29*Power(t2,4) + 
            15*s2*Power(t2,4) - 4*t1*Power(t2,4) - 7*Power(t2,5) - 
            s2*Power(t2,5) + 2*t1*Power(t2,5) + 
            Power(s1,4)*(-5*Power(s2,2) + s2*(3 + 14*t1 + t2) + 
               t1*(-7 - 9*t1 + 3*t2)) + 
            s1*(-16 + (17 - 10*s2 - 8*Power(s2,2))*t2 + 
               (35 + 53*s2 + 9*Power(s2,2))*Power(t2,2) + 
               (-47 - 28*s2 + 3*Power(s2,2))*Power(t2,3) + 
               (11 + 3*s2)*Power(t2,4) + 
               Power(t1,2)*(5 - 10*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               t1*(9 + 3*(-15 + 8*s2)*t2 + (1 - 28*s2)*Power(t2,2) + 
                  (25 - 2*s2)*Power(t2,3) - 8*Power(t2,4))) + 
            Power(s1,3)*(1 - 4*t2 + 3*Power(t2,2) + 
               Power(s2,2)*(-1 + 13*t2) + Power(t1,2)*(-3 + 17*t2) - 
               2*t1*(6 - 20*t2 + 5*Power(t2,2)) - 
               s2*(-5 + 22*t2 + Power(t2,2) + t1*(-2 + 28*t2))) + 
            Power(s1,2)*(4 - 23*t2 + 26*Power(t2,2) - 7*Power(t2,3) + 
               Power(s2,2)*(4 - 2*t2 - 11*Power(t2,2)) + 
               Power(t1,2)*(5 + 5*t2 - 9*Power(t2,2)) + 
               t1*(19 + 16*t2 - 58*Power(t2,2) + 13*Power(t2,3)) + 
               s2*(-3 - 17*t2 + 32*Power(t2,2) - 2*Power(t2,3) + 
                  t1*(-17 + 9*t2 + 16*Power(t2,2))))) + 
         s*(-4 + 2*Power(s1,5)*Power(s2 - t1,2) + 8*t1 - 4*Power(t1,2) + 
            27*t2 - 8*s2*t2 - 36*t1*t2 + 8*s2*t1*t2 + 9*Power(t1,2)*t2 - 
            57*Power(t2,2) + 13*s2*Power(t2,2) + 73*t1*Power(t2,2) - 
            29*s2*t1*Power(t2,2) + 6*Power(t1,2)*Power(t2,2) + 
            55*Power(t2,3) + 30*s2*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,3) - 96*t1*Power(t2,3) + 
            16*s2*t1*Power(t2,3) - 7*Power(t1,2)*Power(t2,3) - 
            29*Power(t2,4) - 41*s2*Power(t2,4) + 51*t1*Power(t2,4) + 
            s2*t1*Power(t2,4) - 2*Power(t1,2)*Power(t2,4) + 
            10*Power(t2,5) + 6*s2*Power(t2,5) - 2*Power(t2,6) - 
            Power(s1,4)*(s2 - t1)*
             (6 - 6*t2 - t1*(1 + 5*t2) + s2*(-1 + 7*t2)) + 
            Power(s1,3)*(-8*Power(-1 + t2,2) + 
               Power(t1,2)*(-3 + 3*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(-6 + t2 + 9*Power(t2,2)) + 
               t1*(3 - 14*t2 + 11*Power(t2,2)) - 
               s2*(-5 + 2*t2 + 3*Power(t2,2) + 
                  t1*(-9 + 4*t2 + 13*Power(t2,2)))) + 
            Power(s1,2)*(Power(s2,2)*t2*(14 - 5*t2 - 5*Power(t2,2)) - 
               2*Power(-1 + t2,2)*(-3 - 8*t2 + Power(t2,2)) - 
               Power(t1,2)*(-1 - 20*t2 + 16*Power(t2,2) + Power(t2,3)) + 
               t1*(15 - 56*t2 + 39*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-19 + 60*t2 - 35*Power(t2,2) - 6*Power(t2,3) + 
                  t1*(3 - 42*t2 + 25*Power(t2,2) + 6*Power(t2,3)))) + 
            s1*(Power(s2,2)*Power(t2,2)*(-10 + 3*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-15 + 9*t2 - 14*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(3 - 19*t2 - 6*Power(t2,2) + 16*Power(t2,3)) + 
               t1*(12 - 64*t2 + 141*Power(t2,2) - 82*Power(t2,3) - 
                  7*Power(t2,4)) + 
               s2*(8 + 6*t2 - 95*Power(t2,2) + 84*Power(t2,3) - 
                  3*Power(t2,4) - 
                  t1*(8 - 26*t2 - 17*Power(t2,2) + 22*Power(t2,3) + 
                     Power(t2,4))))))*T5q(s))/
     (s*(-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)));
   return a;
};
