#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m312_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(Power(s,5)*s2*(-14 + 2*Power(t1,2) + t2 + s2*t2 - Power(t2,2) + 
            t1*(s2 + 3*(5 + t2))) + 
         Power(s,4)*(2*(-1 + t1 + Power(t1,2)) + 
            Power(s2,3)*(4 + s1 - 2*t1 - 7*t2) + 
            Power(s2,2)*(45 + s1 - 43*t1 + 3*s1*t1 - 6*Power(t1,2) - 
               2*t2 - 3*s1*t2 - 8*t1*t2 + Power(t2,2)) + 
            s2*(61 + 3*Power(t1,3) - 30*t2 - Power(t2,2) + 
               11*Power(t1,2)*(6 + t2) + 
               t1*(-119 + 33*t2 - 6*Power(t2,2)) + 
               s1*(14 + 5*t2 - t1*(20 + 3*t2)))) + 
         s2*(1 + s2 - t1)*(-1 + t1)*
          (-3*(-1 + s1)*Power(t1,4) + 8*Power(-1 + t2,2) + 
            Power(s2,3)*(-4 + 4*Power(s1,2) + s1*(-3 + 3*t1 - 8*t2) + 
               t1*(4 - 7*t2) + 7*t2 + 4*Power(t2,2)) + 
            Power(t1,3)*(19 + 2*Power(s1,2) - 36*t2 + 3*s1*(4 + t2)) + 
            Power(t1,2)*(-71 + 6*Power(s1,2) + 112*t2 + 9*Power(t2,2) - 
               7*s1*(5 + 3*t2)) + 
            t1*(41 - 4*Power(s1,2) - 60*t2 - 13*Power(t2,2) + 
               2*s1*(13 + 5*t2)) - 
            Power(s2,2)*(-15 + 2*Power(s1,2)*(1 + t1) + Power(t2,2) + 
               Power(t1,2)*(-7 + 2*t2) + 
               t1*(22 - 2*t2 + 3*Power(t2,2)) + 
               s1*(19 + 5*Power(t1,2) - 7*t2 - t1*(24 + t2))) - 
            s2*(65 + 4*Power(s1,2)*(-1 + t1 + Power(t1,2)) + 
               Power(t1,3)*(14 - 9*t2) - 108*t2 + 11*Power(t2,2) - 
               16*t1*(5 - 10*t2 + Power(t2,2)) + 
               Power(t1,2)*(1 - 43*t2 + 9*Power(t2,2)) + 
               s1*(26 - 5*Power(t1,3) + Power(t1,2)*(33 - 4*t2) + 10*t2 - 
                  2*t1*(27 + 7*t2)))) + 
         Power(s,3)*(2*(2 - 5*t1 + Power(t1,2) + 3*Power(t1,3)) + 
            Power(s2,4)*(-8 - 2*s1 + t1 + 11*t2) - 
            Power(s2,3)*(46 + 2*Power(s1,2) - 8*Power(t1,2) + 
               2*s1*(-2 + 6*t1 - 5*t2) + 9*t2 + Power(t2,2) - 
               t1*(47 + 7*t2)) + 
            Power(s2,2)*(Power(s1,2)*(5 - 4*t1) - 14*Power(t1,3) - 
               Power(t1,2)*(108 + 31*t2) - 
               2*(89 - 74*t2 + 2*Power(t2,2)) + 
               2*t1*(125 - 52*t2 + 5*Power(t2,2)) + 
               s1*(Power(t1,2) - 12*(5 + t2) + t1*(83 + 2*t2))) - 
            s2*(72 + 3*Power(t1,4) + 
               Power(s1,2)*(-14 + 11*t1 + 2*Power(t1,2)) - 114*t2 - 
               28*Power(t2,2) - Power(t1,3)*(116 + 15*t2) + 
               3*Power(t1,2)*(101 - 45*t2 + 4*Power(t2,2)) + 
               t1*(-300 + 294*t2 + 13*Power(t2,2)) + 
               s1*(83 - 3*Power(t1,3) + 40*t2 + 
                  4*Power(t1,2)*(19 + 3*t2) - 2*t1*(75 + 26*t2)))) + 
         s*(2*Power(-1 + t1,2)*t1*(-1 + t1 + Power(t1,2)) - 
            2*Power(s2,5)*(4 + Power(s1,2) - 4*t1 + Power(t2,2) + 
               s1*(4*t1 - 2*(2 + t2))) + 
            Power(s2,4)*(Power(s1,2)*(10 - 8*t1) - 5*Power(t1,3) - 
               2*Power(t1,2)*(-1 + t2) + 4*(2 + 4*t2 + 3*Power(t2,2)) - 
               t1*(5 + 14*t2 + 10*Power(t2,2)) + 
               s1*(-37 + 5*Power(t1,2) - 14*t2 + 2*t1*(16 + 5*t2))) - 
            s2*(-1 + t1)*(9 + 3*Power(t1,5) + 
               Power(s1,2)*(4 - 28*t1 + 21*Power(t1,2) + 
                  6*Power(t1,3)) - 4*t2 + 29*Power(t2,2) - 
               2*Power(t1,4)*(10 + t2) + 
               t1*(168 - 309*t2 - 61*Power(t2,2)) + 
               Power(t1,3)*(119 - 146*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-279 + 461*t2 + 32*Power(t2,2)) + 
               s1*(-9*Power(t1,4) + 3*Power(t1,3)*(21 + 4*t2) - 
                  2*(13 + 5*t2) + 4*t1*(39 + 19*t2) - 
                  4*Power(t1,2)*(46 + 21*t2))) + 
            Power(s2,3)*(-97 + 7*Power(t1,4) + 
               Power(s1,2)*(21 - 27*t1 + 8*Power(t1,2)) + 247*t2 - 
               2*Power(t2,2) + Power(t1,3)*(-29 + 30*t2) + 
               Power(t1,2)*(12 + 103*t2 - 3*Power(t2,2)) + 
               t1*(107 - 380*t2 + 7*Power(t2,2)) + 
               s1*(15*Power(t1,3) - 8*Power(t1,2)*(22 + t2) - 
                  2*(71 + 19*t2) + t1*(303 + 42*t2))) + 
            Power(s2,2)*(-198 + Power(t1,5) + 
               Power(s1,2)*(20 - 58*t1 + 28*Power(t1,2) + 
                  8*Power(t1,3)) + 365*t2 - 5*Power(t2,2) - 
               2*Power(t1,4)*(2 + 15*t2) + 
               Power(t1,2)*(-538 + 1039*t2 - 43*Power(t2,2)) + 
               t1*(597 - 1133*t2 + 20*Power(t2,2)) + 
               Power(t1,3)*(142 - 241*t2 + 26*Power(t2,2)) + 
               s1*(-21*Power(t1,4) + 6*Power(t1,3)*(34 + t2) - 
                  4*(35 + 12*t2) + 8*t1*(59 + 19*t2) - 
                  Power(t1,2)*(515 + 106*t2)))) + 
         Power(s,2)*(2*(-1 + 5*t1 - 6*Power(t1,2) - Power(t1,3) + 
               3*Power(t1,4)) + Power(s2,5)*(4 + s1 - 5*t2) + 
            Power(s2,4)*(23 + 4*Power(s1,2) - 4*Power(t1,2) + 
               s1*(-13 + 17*t1 - 11*t2) + 10*t2 + 3*Power(t2,2) - 
               t1*(27 + 2*t2)) + 
            Power(s2,3)*(89 + 17*Power(t1,3) + Power(s1,2)*(-11 + 8*t1) - 
               111*t2 - 3*Power(t2,2) + Power(t1,2)*(30 + 33*t2) + 
               t1*(-100 + 53*t2 + 2*Power(t2,2)) - 
               s1*(-80 + 8*Power(t1,2) - 13*t2 + t1*(91 + t2))) - 
            Power(s2,2)*(-262 + 6*Power(t1,4) + 
               Power(s1,2)*(33 - 33*t1 + 2*Power(t1,2)) + 450*t2 + 
               7*Power(t2,2) + 43*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(-352 + 282*t2 - 26*Power(t2,2)) + 
               t1*(554 - 797*t2 + 22*Power(t2,2)) + 
               s1*(15*Power(t1,3) - 12*Power(t1,2)*(19 + t2) - 
                  7*(29 + 11*t2) + t1*(402 + 88*t2))) - 
            s2*(-24 + 7*Power(t1,5) + 
               Power(s1,2)*(18 - 49*t1 + 24*Power(t1,2) + 6*Power(t1,3)) + 
               97*t2 + 47*Power(t2,2) - Power(t1,4)*(91 + 9*t2) + 
               t1*(315 - 564*t2 - 90*Power(t2,2)) + 
               Power(t1,3)*(324 - 211*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(-531 + 687*t2 + 32*Power(t2,2)) + 
               s1*(-9*Power(t1,4) - 5*(19 + 9*t2) + 
                  2*Power(t1,3)*(55 + 9*t2) + 5*t1*(63 + 29*t2) - 
                  Power(t1,2)*(321 + 116*t2))))))/
     (s*(-1 + s2)*s2*(-s + s2 - t1)*(1 - s + s2 - t1)*(-1 + t1)*
       Power(-1 + s + t1,2)*(-1 + t2)) + 
    (8*(-2*(-1 + t1)*(-Power(s2,2) + Power(s2,3) + t1*(-2 + 3*t1) - 
            s2*(-2 + 2*t1 + Power(t1,2)))*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,6)*(3*Power(t1,2) - (-4 + s2)*t1*t2 + 
            (1 + s2)*Power(t2,2)) + 
         Power(s,4)*(1 - 2*(-12 + s1)*Power(t1,4) - 14*t2 + 
            25*Power(t2,2) + t1*(-19 + 12*s1 - 16*t2 - 25*Power(t2,2)) + 
            Power(t1,2)*(20 + Power(s1,2) - 47*t2 + 2*s1*(6 + t2)) + 
            Power(t1,3)*(-56 + Power(s1,2) + 38*t2 + s1*(-27 + 2*t2)) + 
            Power(s2,3)*(Power(s1,2) - 3*Power(t1,2) + t1*(3 - 10*t2) + 
               10*t2*(1 + t2) - s1*(1 + 2*t1 + 8*t2)) + 
            Power(s2,2)*(-3 - Power(s1,2)*(-1 + t1) + 5*Power(t1,3) + 
               s1*(4 + 3*t1*(-3 + t2) - 13*t2) + t2 + 27*Power(t2,2) + 
               Power(t1,2)*(-4 + 9*t2) + t1*(5 - 13*t2 - 12*Power(t2,2))\
) - s2*(-5 + 2*Power(t1,4) + (7 + 12*s1)*t2 - 32*Power(t2,2) + 
               Power(t1,3)*(23 - 4*s1 + t2) + 
               Power(t1,2)*(-3 + Power(s1,2) + s1*(-41 + t2) + 38*t2 - 
                  3*Power(t2,2)) + 
               t1*(8 + 2*Power(s1,2) + s1*(12 - 7*t2) - 8*t2 + 
                  24*Power(t2,2)))) + 
         Power(s,5)*(-((-16 + s1 + s2)*Power(t1,3)) + 
            (2 + 2*s2*(2 + s1 - 5*t2) + 
               Power(s2,2)*(-3 + 2*s1 - 5*t2) - 8*t2)*t2 + 
            Power(t1,2)*(-7 + Power(s2,2) + s1*(-8 + t2) + 19*t2 - 
               2*s2*(3 + t2)) + 
            t1*(Power(s2,2)*(-1 + 5*t2) + 3*(2 - 3*t2 + Power(t2,2)) + 
               s2*(1 - 8*t2 + 3*Power(t2,2)) + 
               s1*(-2 + Power(s2,2) + s2*(4 + t2)))) - 
         Power(s,3)*(6 + (-13 + s1)*Power(t1,5) - 36*t2 + 
            38*Power(t2,2) - Power(t1,4)*
             (-83 + 2*Power(s1,2) + s1*(-33 + t2) + 37*t2) + 
            t1*(-10 + 24*s1 - 37*t2 - 71*Power(t2,2)) + 
            Power(s2,4)*(3*Power(s1,2) - 3*Power(t1,2) + 
               t1*(3 - 10*t2) - 3*s1*(1 + 4*t2) + 2*t2*(6 + 5*t2)) + 
            Power(t1,3)*(-101 + 102*t2 + 4*Power(t2,2) - 
               s1*(69 + 5*t2)) + 
            Power(t1,2)*(45 + 6*Power(s1,2) + 48*t2 + 19*Power(t2,2) + 
               s1*(-56 + 17*t2)) + 
            Power(s2,2)*(-1 - 5*Power(t1,4) - 
               Power(s1,2)*(-6 + 10*t1 + Power(t1,2)) + 
               Power(t1,3)*(1 - 5*t2) + 11*t2 + 42*Power(t2,2) + 
               t1*(22 - 56*t2 - 49*Power(t2,2)) + 
               Power(t1,2)*(-31 + 8*t2 + 9*Power(t2,2)) + 
               s1*(2 + 9*Power(t1,3) + Power(t1,2)*(42 - 5*t2) - 
                  27*t2 + 3*t1*(-5 + 11*t2))) + 
            Power(s2,3)*(-5 - 5*Power(s1,2)*(-1 + t1) + 7*Power(t1,3) + 
               19*t2 + 31*Power(t2,2) + Power(t1,2)*(-13 + 15*t2) + 
               t1*(11 - 42*t2 - 18*Power(t2,2)) + 
               s1*(2 - 4*Power(t1,2) - 26*t2 + t1*(4 + 15*t2))) + 
            s2*(7 - 6*(-3 + s1)*Power(t1,4) + Power(t1,5) + 
               (25 - 24*s1)*t2 + 42*Power(t2,2) + 
               Power(t1,2)*(33 + 5*Power(s1,2) + s1*(86 - 6*t2) - 
                  57*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(-49 + 5*Power(s1,2) + 61*t2 - Power(t2,2) + 
                  s1*(-76 + 3*t2)) - 
               t1*(14 + 12*Power(s1,2) + 85*t2 + 57*Power(t2,2) - 
                  2*s1*(13 + 19*t2)))) - 
         s*(2*Power(t1,6)*(3 + s1 - 2*t2) + 
            Power(s2,6)*(s1 - t2)*(-1 + s1 + t1 - t2) + 
            8*Power(-1 + t2,2) + Power(t1,5)*(-35 - 31*s1 + 30*t2) - 
            8*t1*(1 - 6*t2 + 5*Power(t2,2)) + 
            Power(t1,2)*(13 + 8*Power(s1,2) - 10*t2 + 53*Power(t2,2) + 
               4*s1*(-17 + 5*t2)) + 
            Power(t1,4)*(73 + 17*Power(s1,2) - t2 + s1*(-21 + 8*t2)) - 
            Power(t1,3)*(57 + 24*Power(s1,2) + 47*t2 + 20*Power(t2,2) + 
               2*s1*(-59 + 15*t2)) - 
            Power(s2,5)*(-1 + t1)*
             (1 + 3*Power(s1,2) + 5*t2 + 3*Power(t2,2) - 
               t1*(1 + 3*t2) + s1*(4*t1 - 6*(1 + t2))) + 
            Power(s2,3)*(2*Power(s1,2)*
                (2 - 5*t1 + 4*Power(t1,2) + Power(t1,3)) - 
               (-1 + t2)*t2 + Power(t1,4)*(3 + t2) + 
               t1*(3 - 46*t2 + Power(t2,2)) - 
               Power(t1,3)*(3 + 22*t2 + Power(t2,2)) + 
               Power(t1,2)*(-3 + 66*t2 + 5*Power(t2,2)) - 
               2*s1*(5 + 2*Power(t1,4) - t2 + 7*Power(t1,2)*(3 + t2) - 
                  Power(t1,3)*(5 + t2) - t1*(23 + t2))) + 
            s2*(Power(t1,4)*(50 - 3*Power(s1,2) - 4*s1*(-20 + t2) - 
                  63*t2) + Power(t1,5)*
                (-7 - 4*s1 + Power(s1,2) + 6*t2) - 
               Power(t1,3)*(87 + 38*Power(s1,2) + s1*(14 - 38*t2) + 
                  42*t2 - 3*Power(t2,2)) - 
               16*t1*(-3 + Power(s1,2) + s1*(-7 + t2) + 11*t2 - 
                  Power(t2,2)) + 4*(-7 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(24 + 52*Power(s1,2) + 251*t2 - 
                  27*Power(t2,2) - 2*s1*(87 + 5*t2))) + 
            Power(s2,4)*(-3 + Power(s1,2)*(3 - 10*t1 + 2*Power(t1,2)) + 
               27*t2 + 6*Power(t2,2) - 3*Power(t1,3)*(1 + t2) + 
               t1*(3 - 48*t2 - 14*Power(t2,2)) + 
               3*Power(t1,2)*(1 + 8*t2 + Power(t2,2)) + 
               s1*(6*Power(t1,3) - 6*Power(t1,2)*(3 + t2) - 
                  5*(3 + 2*t2) + t1*(27 + 26*t2))) + 
            Power(s2,2)*(3 - Power(t1,5) + 
               Power(s1,2)*(8 - 32*t1 + 28*Power(t1,2) + 
                  2*Power(t1,3) - 3*Power(t1,4)) + 58*t2 - 
               5*Power(t2,2) + 3*Power(t1,4)*(1 + t2) + 
               2*Power(t1,3)*(-10 + 9*t2 + Power(t2,2)) + 
               2*Power(t1,2)*(19 + 7*t2 + 7*Power(t2,2)) - 
               t1*(23 + 93*t2 + 8*Power(t2,2)) + 
               s1*(Power(t1,4) + Power(t1,5) + Power(t1,2)*(4 - 38*t2) - 
                  4*(11 + t2) - 2*Power(t1,3)*(14 + t2) + t1*(66 + 38*t2)\
))) + Power(s,2)*(2*Power(t1,6) + 
            Power(t1,5)*(-40 - 16*s1 + Power(s1,2) + 18*t2) + 
            t1*(5 + 16*s1 + 20*t2 - 85*Power(t2,2)) + 
            4*(3 - 10*t2 + 7*Power(t2,2)) + 
            Power(s2,5)*(3*Power(s1,2) + t1 - Power(t1,2) + 
               s1*(-3 + 2*t1 - 8*t2) - 5*t1*t2 + t2*(6 + 5*t2)) + 
            2*Power(t1,2)*(12 + 6*Power(s1,2) + 58*t2 + 30*Power(t2,2) + 
               s1*(-65 + 14*t2)) - 
            Power(t1,3)*(115 + 15*Power(s1,2) + 21*t2 + 2*Power(t2,2) + 
               s1*(-43 + 27*t2)) - 
            Power(t1,4)*(Power(s1,2) - s1*(88 + 3*t2) + 
               2*(-56 + 47*t2 + Power(t2,2))) + 
            Power(s2,4)*(-1 - 7*Power(s1,2)*(-1 + t1) + 3*Power(t1,3) + 
               19*t2 + 16*Power(t2,2) + Power(t1,2)*(-5 + 11*t2) - 
               3*t1*(-1 + 11*t2 + 4*Power(t2,2)) + 
               s1*(-8 - 8*Power(t1,2) - 21*t2 + t1*(19 + 17*t2))) + 
            s2*(-21 + 2*(-1 + s1)*Power(t1,5) + (60 - 16*s1)*t2 + 
               21*Power(t2,2) - 
               Power(t1,4)*(-44 + 5*Power(s1,2) + s1*(-44 + t2) + 
                  35*t2) + Power(t1,3)*
                (-107 + 115*t2 + 4*Power(t2,2) + 2*s1*(-77 + 2*t2)) + 
               t1*(22 - 24*Power(s1,2) - 256*t2 - 34*Power(t2,2) + 
                  4*s1*(29 + 10*t2)) + 
               Power(t1,2)*(64 + 37*Power(s1,2) + 135*t2 + 
                  17*Power(t2,2) - s1*(27 + 43*t2))) + 
            Power(s2,2)*(6 + Power(t1,5) + 
               Power(s1,2)*(12 - 29*t1 + 10*Power(t1,2) + 
                  6*Power(t1,3)) + Power(t1,4)*(-1 + t2) + 44*t2 + 
               18*Power(t2,2) + Power(t1,3)*(9 + t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(-3 + 39*t2 + 14*Power(t2,2)) - 
               t1*(12 + 81*t2 + 37*Power(t2,2)) + 
               s1*(-34 - 8*Power(t1,4) + Power(t1,2)*(32 - 18*t2) - 
                  20*t2 + Power(t1,3)*(-35 + 3*t2) + t1*(41 + 43*t2))) + 
            Power(s2,3)*(-9 - 3*Power(t1,4) + 
               Power(s1,2)*(7 - 16*t1 + 2*Power(t1,2)) + 
               Power(t1,3)*(5 - 7*t2) + 39*t2 + 22*Power(t2,2) + 
               t1*(31 - 103*t2 - 40*Power(t2,2)) + 
               3*Power(t1,2)*(-8 + 17*t2 + 3*Power(t2,2)) + 
               s1*(12*Power(t1,3) - 3*(3 + 7*t2) - 
                  Power(t1,2)*(9 + 11*t2) + t1*(26 + 48*t2)))))*
       B1(s,1 - s + s2 - t1,s2))/
     (s*(-1 + s2)*(-1 + t1)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-1 + t2)) + (8*(2*(-1 + s2)*Power(s2,2)*
          (2 + Power(s2,2) + s2*(-1 + t1) - 3*t1)*Power(-1 + t1,2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         2*Power(s,7)*s2*t1*(2 + Power(t1,2) + t1*(-3 + t2) - 3*t2 + 
            2*s2*(-2 + 2*t1 + t2)) - 
         Power(s,6)*(2*t1*(-1 + t1 + Power(t1,2)) + 
            2*Power(s2,3)*(4 + 9*Power(t1,2) - 2*t1*(7 + 2*s1 - 4*t2) - 
               2*t2 + Power(t2,2)) + 
            s2*t1*(34 - 6*Power(t1,3) + 
               2*s1*(-4 + 3*t1 + Power(t1,2)) + 
               Power(t1,2)*(31 - 6*t2) - 29*t2 + t1*(-61 + 33*t2)) + 
            Power(s2,2)*(-19*Power(t1,3) + 
               Power(t1,2)*(59 - 2*s1 + t2) + 
               2*(-2 + 3*t2 + Power(t2,2)) + 
               t1*(-44 - 9*t2 - 2*Power(t2,2) + s1*(6 + 4*t2)))) + 
         Power(s,5)*(2*t1*(-5 + 8*t1 + 2*Power(t1,2) - 3*Power(t1,3)) + 
            2*Power(s2,4)*(6 + 6*Power(t1,2) + s1*(4 - 9*t1 - 2*t2) - 
               4*t2 + 4*Power(t2,2) + t1*(-12 + 11*t2)) + 
            s2*(2 + 6*Power(t1,5) + t1*(109 - 44*s1 - 51*t2) + 
               Power(t1,4)*(-50 - 6*s1 + 6*t2) - 
               Power(t1,3)*(-169 + s1 + 62*t2) + 
               Power(t1,2)*(-268 + 63*s1 + 107*t2)) + 
            Power(s2,3)*(46 - 8*Power(s1,2)*t1 - 33*Power(t1,3) + 
               Power(t1,2)*(150 - 4*t2) + 5*t2 + 18*Power(t2,2) + 
               s1*(-2 + 5*t1)*(3 + t1 + 4*t2) - 
               t1*(157 + 21*t2 + 16*Power(t2,2))) + 
            Power(s2,2)*(-30 - 8*Power(s1,2)*(-2 + t1)*t1 + 
               14*Power(t1,4) + 19*t2 + 14*Power(t2,2) - 
               2*Power(t1,3)*(49 + 9*t2) + 
               Power(t1,2)*(126 + 55*t2 + 4*Power(t2,2)) - 
               2*t1*(35 + 45*t2 + 6*Power(t2,2)) + 
               s1*(8 + 5*Power(t1,3) - 4*Power(t1,2)*(-10 + t2) - 
                  t1*(27 + 4*t2)))) - 
         Power(s,4)*(2*t1*(-10 + 22*t1 - 5*Power(t1,2) - 
               9*Power(t1,3) + 3*Power(t1,4)) + 
            2*Power(s2,5)*(2 + Power(s1,2) + Power(t1,2) + 
               s1*(3 - 5*t1 - 6*t2) - t2 + 6*Power(t2,2) + 
               t1*(-2 + 5*t2)) + 
            s2*(8 - 2*Power(t1,6) + 
               Power(t1,3)*(492 - 72*s1 - 149*t2) + 
               Power(t1,5)*(31 + 6*s1 - 2*t2) - 
               2*t1*(-90 + 48*s1 + 17*t2) + 
               Power(t1,4)*(-161 - 26*s1 + 53*t2) + 
               2*Power(t1,2)*(-293 + 100*s1 + 65*t2)) + 
            Power(s2,3)*(103 + 19*Power(t1,4) + 
               2*Power(s1,2)*(-8 + 9*t1) + 37*t2 + 42*Power(t2,2) - 
               3*Power(t1,3)*(65 + 11*t2) + 
               Power(t1,2)*(361 + 98*t2 + 20*Power(t2,2)) - 
               2*t1*(150 + 98*t2 + 27*Power(t2,2)) + 
               s1*(23 + 9*Power(t1,3) - 12*t2 - 
                  5*Power(t1,2)*(-9 + 4*t2) + t1*(19 + 20*t2))) + 
            Power(s2,4)*(46 + Power(t1,3) - 2*Power(s1,2)*(-5 + 8*t1) + 
               33*t2 + 38*Power(t2,2) + Power(t1,2)*(107 + 14*t2) - 
               t1*(164 + 77*t2 + 42*Power(t2,2)) - 
               s1*(13 + Power(t1,2) + 36*t2 - 2*t1*(17 + 24*t2))) + 
            Power(s2,2)*(-81 - 3*Power(t1,5) + 
               2*Power(s1,2)*t1*(38 - 46*t1 + 9*Power(t1,2)) + 4*t2 + 
               40*Power(t2,2) + 17*Power(t1,4)*(4 + t2) + 
               t1*(106 - 111*t2 - 30*Power(t2,2)) - 
               2*Power(t1,3)*(28 + 28*t2 + Power(t2,2)) + 
               Power(t1,2)*(22 + 72*t2 + 8*Power(t2,2)) - 
               s1*(-36 + 4*Power(t1,4) + Power(t1,3)*(139 + 4*t2) + 
                  11*t1*(21 + 8*t2) - 2*Power(t1,2)*(203 + 36*t2)))) + 
         s*(-1 + t1)*(-2*Power(-1 + t1,2)*t1*(-1 + t1 + Power(t1,2)) - 
            2*Power(s2,7)*(-7 + 3*s1 + 7*t1 - 3*t2)*(s1 - t2) - 
            s2*Power(-1 + t1,2)*
             (2 + 2*Power(t1,3)*(-7 + 4*s1 - 2*t2) + 
               Power(t1,2)*(-47 + 11*s1 + 2*t2) + t1*(23 - 12*s1 + 5*t2)\
) + Power(s2,6)*(-6 + Power(s1,2)*(22 - 8*t1) - 6*Power(t1,3) + 
               Power(t1,2)*(6 - 13*t2) + 33*t2 + 10*Power(t2,2) + 
               t1*(6 - 20*t2 + 4*Power(t2,2)) + 
               s1*(-33 + 13*Power(t1,2) - 32*t2 + 4*t1*(5 + t2))) + 
            Power(s2,5)*(25 + 6*Power(t1,4) + 
               2*Power(s1,2)*(2 - 7*t1 + 3*Power(t1,2)) - 18*t2 + 
               24*Power(t2,2) + Power(t1,3)*(9 + 7*t2) + 
               Power(t1,2)*(-11 + 28*t2 - 6*Power(t2,2)) - 
               t1*(29 + 17*t2 + 22*Power(t2,2)) + 
               s1*(-2*Power(t1,3) + Power(t1,2)*(-65 + 4*t2) + 
                  4*t1*(19 + 7*t2) - 3*(3 + 8*t2))) + 
            Power(s2,3)*(45 + 
               Power(s1,2)*(28 - 24*t1 + 48*Power(t1,2) - 
                  42*Power(t1,3)) + 14*Power(t1,3)*(-8 + t2) - 32*t2 + 
               54*Power(t2,2) + Power(t1,4)*(57 + 2*t2) + 
               t1*(-88 + 60*t2 - 34*Power(t2,2)) - 
               2*Power(t1,2)*(-49 + 22*t2 + 5*Power(t2,2)) + 
               s1*(-47 - 27*Power(t1,4) + 2*Power(t1,5) - 60*t2 + 
                  16*Power(t1,3)*(6 + t2) - 4*t1*(-41 + 3*t2) + 
                  4*Power(t1,2)*(-47 + 9*t2))) + 
            Power(s2,4)*(-29 - 23*Power(t1,4) + 
               2*Power(s1,2)*
                (-8 - 15*t1 + 13*Power(t1,2) + 4*Power(t1,3)) + 
               Power(t1,3)*(4 - 24*t2) + 30*t2 - 60*Power(t2,2) + 
               2*t1*(8 - 20*t2 + 13*Power(t2,2)) + 
               Power(t1,2)*(32 + 34*t2 + 22*Power(t2,2)) + 
               s1*(11 + Power(t1,4) - 4*Power(t1,3)*(-8 + t2) + 
                  32*t1*(-1 + t2) + 64*t2 - 4*Power(t1,2)*(3 + 17*t2))) \
+ Power(s2,2)*(11 + 2*Power(s1,2)*t1*
                (-14 + 20*t1 - 11*Power(t1,2) + 4*Power(t1,3)) + 41*t2 - 
               22*Power(t2,2) - 4*Power(t1,5)*(4 + t2) - 
               4*Power(t1,3)*(5 + 16*t2) + Power(t1,4)*(7 + 16*t2) + 
               Power(t1,2)*(94 + 131*t2 - 6*Power(t2,2)) + 
               2*t1*(-38 - 60*t2 + 13*Power(t2,2)) + 
               s1*(-12 + 10*Power(t1,5) - Power(t1,4)*(55 + 8*t2) + 
                  4*Power(t1,3)*(32 + 13*t2) + t1*(66 + 76*t2) - 
                  Power(t1,2)*(137 + 116*t2)))) + 
         Power(s,3)*(-2*t1*(10 - 28*t1 + 17*Power(t1,2) + 
               8*Power(t1,3) - 8*Power(t1,4) + Power(t1,5)) + 
            2*Power(s2,6)*(-2 + 2*s1 + t1 - 4*t2)*(s1 - t2) - 
            s2*(-1 + t1)*(12 + 2*(3 + s1)*Power(t1,5) + 
               Power(t1,3)*(247 - 16*s1 - 79*t2) + 
               t1*(178 - 104*s1 + 4*t2) + 
               Power(t1,4)*(-41 - 29*s1 + 22*t2) + 
               2*Power(t1,2)*(-257 + 89*s1 + 26*t2)) + 
            Power(s2,4)*(46 - 22*Power(t1,4) + 
               2*Power(s1,2)*(2 - 7*t1 + 2*Power(t1,2)) + 104*t2 + 
               32*Power(t2,2) - 7*Power(t1,3)*(12 + 7*t2) + 
               2*Power(t1,2)*(119 + 76*t2 + 18*Power(t2,2)) - 
               t1*(178 + 219*t2 + 66*Power(t2,2)) + 
               s1*(-73 + 20*Power(t1,3) - 20*t2 + 8*t1*(10 + 7*t2) - 
                  Power(t1,2)*(15 + 32*t2))) + 
            Power(s2,5)*(50 + 19*Power(t1,3) - 
               6*Power(s1,2)*(-2 + 3*t1) + 53*t2 + 36*Power(t2,2) + 
               4*Power(t1,2)*(7 + 12*t2) - 
               t1*(97 + 117*t2 + 50*Power(t2,2)) + 
               s1*(-30 - 39*Power(t1,2) - 44*t2 + t1*(85 + 64*t2))) + 
            Power(s2,3)*(47 - 6*Power(t1,5) + 
               2*Power(s1,2)*
                (-30 + 88*t1 - 66*Power(t1,2) + 7*Power(t1,3)) + 
               19*t2 + 12*Power(t2,2) + Power(t1,4)*(111 + 23*t2) + 
               2*Power(t1,2)*(73 + 46*t2 + 2*Power(t2,2)) - 
               Power(t1,3)*(148 + 37*t2 + 6*Power(t2,2)) - 
               t1*(150 + 33*t2 + 20*Power(t2,2)) + 
               s1*(143 - 4*Power(t1,4) + 44*t2 - 
                  2*Power(t1,3)*(89 + 2*t2) - 4*t1*(108 + 37*t2) + 
                  Power(t1,2)*(407 + 120*t2))) + 
            Power(s2,2)*(-107 - 
               2*Power(s1,2)*t1*
                (-66 + 118*t1 - 63*Power(t1,2) + 6*Power(t1,3)) - 
               50*t2 + 60*Power(t2,2) + Power(t1,4)*(-53 + 2*t2) - 
               Power(t1,5)*(27 + 4*t2) + 
               t1*(401 + 81*t2 - 52*Power(t2,2)) + 
               4*Power(t1,2)*(-99 - 45*t2 + Power(t2,2)) + 
               Power(t1,3)*(182 + 113*t2 + 2*Power(t2,2)) + 
               s1*(60 + Power(t1,5) - 28*Power(t1,3)*(25 + 4*t2) + 
                  Power(t1,4)*(137 + 4*t2) - t1*(451 + 208*t2) + 
                  Power(t1,2)*(991 + 292*t2)))) - 
         Power(s,2)*(-2*Power(-1 + t1,2)*t1*
             (5 - 7*t1 - 3*Power(t1,2) + 2*Power(t1,3)) + 
            2*Power(s2,7)*(s1 - t2)*(-1 + s1 + t1 - t2) - 
            s2*Power(-1 + t1,2)*
             (-8 + t1*(-98 + 56*s1 - 15*t2) + 
               Power(t1,2)*(241 - 74*s1 - 11*t2) + 
               2*Power(t1,4)*(-1 + 5*s1 - 2*t2) + 
               Power(t1,3)*(-23 - 18*s1 + 26*t2)) + 
            Power(s2,5)*(-13 - 27*Power(t1,4) - 
               2*Power(s1,2)*t1*(-9 + 5*t1) + Power(t1,3)*(14 - 41*t2) + 
               19*t2 + 4*Power(t2,2) - 
               t1*(14 + 55*t2 + 22*Power(t2,2)) + 
               Power(t1,2)*(40 + 77*t2 + 26*Power(t2,2)) + 
               s1*(23 + 27*Power(t1,3) - 4*t2 + t1*(-43 + 4*t2) - 
                  Power(t1,2)*(7 + 16*t2))) + 
            Power(s2,6)*(18 + Power(s1,2)*(8 - 16*t1) + 6*Power(t1,3) + 
               41*t2 + 20*Power(t2,2) + Power(t1,2)*(6 + 45*t2) - 
               2*t1*(15 + 43*t2 + 14*Power(t2,2)) + 
               s1*(-41 - 45*Power(t1,2) - 28*t2 + t1*(86 + 44*t2))) - 
            Power(s2,4)*(11 - 9*Power(t1,5) + 
               Power(s1,2)*(42 - 90*t1 + 44*Power(t1,2)) + 67*t2 + 
               6*Power(t2,2) - Power(t1,4)*(52 + 19*t2) + 
               3*Power(t1,2)*(23 + 4*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(67 + 10*t2 + 8*Power(t2,2)) - 
               2*t1*(43 + 35*t2 + 21*Power(t2,2)) + 
               s1*(-45 + 8*Power(t1,4) + Power(t1,3)*(87 - 4*t2) - 
                  64*t2 - Power(t1,2)*(151 + 92*t2) + t1*(101 + 168*t2))) \
+ Power(s2,3)*(-57 + 2*Power(t1,6) - 
               2*Power(s1,2)*
                (36 - 105*t1 + 137*Power(t1,2) - 66*Power(t1,3) + 
                  3*Power(t1,4)) + 3*t2 - 54*Power(t2,2) - 
               Power(t1,5)*(33 + 2*t2) - Power(t1,4)*(78 + 23*t2) + 
               Power(t1,3)*(275 + 78*t2 + 20*Power(t2,2)) + 
               2*t1*(71 + 69*t2 + 29*Power(t2,2)) - 
               Power(t1,2)*(251 + 194*t2 + 34*Power(t2,2)) + 
               s1*(169 - 4*Power(t1,5) + 100*t2 + 
                  2*Power(t1,4)*(69 + 2*t2) - 
                  3*Power(t1,3)*(163 + 40*t2) - t1*(681 + 188*t2) + 
                  Power(t1,2)*(867 + 224*t2))) + 
            Power(s2,2)*(-67 + 6*Power(t1,6) + 
               2*Power(s1,2)*t1*
                (50 - 112*t1 + 92*Power(t1,2) - 29*Power(t1,3) + 
                  Power(t1,4)) - 74*t2 + 50*Power(t2,2) + 
               Power(t1,5)*(49 + 12*t2) - Power(t1,4)*(130 + 107*t2) + 
               t1*(370 + 240*t2 - 66*Power(t2,2)) + 
               Power(t1,3)*(325 + 318*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-553 - 389*t2 + 16*Power(t2,2)) + 
               s1*(44 - 54*Power(t1,5) + 4*Power(t1,4)*(95 + 13*t2) - 
                  t1*(339 + 196*t2) - Power(t1,3)*(897 + 244*t2) + 
                  Power(t1,2)*(866 + 380*t2)))))*R1q(s2))/
     (s*(-1 + s2)*s2*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-1 + t1)*Power(-1 + s + t1,3)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(-1 + t2)) - 
    (8*(Power(s,9)*t1*(-2 + t1 + t2) + 
         2*Power(1 + s2 - t1,2)*Power(-1 + t1,2)*
          (-Power(s2,2) + Power(s2,3) + t1*(-2 + 3*t1) - 
            s2*(-2 + 2*t1 + Power(t1,2)))*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,8)*(5*Power(t1,3) - 2*Power(t2,2) + 
            Power(t1,2)*(-19 + 2*t2) + 
            s2*(-2 - 6*Power(t1,2) + t1*(9 + s1 - 7*t2) + t2 - 
               2*Power(t2,2)) + 
            t1*(27 + 2*s1 - 17*t2 - s1*t2 + Power(t2,2))) + 
         Power(s,7)*(9*Power(t1,4) + Power(t1,3)*(-69 + s1 - 7*t2) + 
            4*t2*(-1 + 4*t2) + 
            Power(s2,2)*(6 + 14*Power(t1,2) + s1*(1 - 6*t1 - 4*t2) - 
               2*t2 + 12*Power(t2,2) + 5*t1*(-3 + 4*t2)) - 
            Power(t1,2)*(-155 + Power(s1,2) + 97*t2 - 6*Power(t2,2) + 
               s1*(-36 + 5*t2)) + 
            t1*(-116 + 2*Power(s1,2) + 59*t2 - 20*Power(t2,2) + 
               s1*(-29 + 8*t2)) + 
            s2*(25 - Power(s1,2)*t1 - 21*Power(t1,3) + 
               Power(t1,2)*(82 - 12*t2) - 16*t2 + 23*Power(t2,2) + 
               t1*(-128 + 59*t2 - 19*Power(t2,2)) + 
               s1*(2 + Power(t1,2) - 5*t2 + 2*t1*(-1 + 5*t2)))) - 
         Power(s,6)*(2 - 5*Power(t1,5) - 28*t2 + 54*Power(t2,2) + 
            Power(t1,4)*(135 - 6*s1 + 28*t2) + 
            Power(t1,3)*(-351 - 173*s1 + 6*Power(s1,2) + 258*t2 + 
               9*s1*t2 - 15*Power(t2,2)) + 
            Power(s2,3)*(6 + s1 + 2*Power(s1,2) - 11*t1 - 12*s1*t1 + 
               16*Power(t1,2) + 2*t2 - 20*s1*t2 + 28*t1*t2 + 
               30*Power(t2,2)) + 
            Power(t1,2)*(500 - 25*Power(s1,2) + s1*(304 - 32*t2) - 
               340*t2 + 69*Power(t2,2)) + 
            t1*(18*Power(s1,2) + s1*(-151 + 18*t2) - 
               17*(10 - 5*t2 + 6*Power(t2,2))) - 
            Power(s2,2)*(-79 + 24*Power(t1,3) + 
               Power(s1,2)*(-3 + 8*t1) + 24*t2 - 84*Power(t2,2) + 
               Power(t1,2)*(-137 + 15*t2) + 
               t1*(247 - 28*t2 + 88*Power(t2,2)) + 
               s1*(1 + 3*Power(t1,2) + 41*t2 - 2*t1*(15 + 31*t2))) + 
            s2*(89 + 13*Power(t1,4) - 
               2*Power(s1,2)*(1 - 5*t1 + 2*Power(t1,2)) - 51*t2 + 
               95*Power(t2,2) - 42*Power(t1,3)*(6 + t2) + 
               t1*(-566 + 203*t2 - 153*Power(t2,2)) + 
               Power(t1,2)*(643 - 254*t2 + 66*Power(t2,2)) + 
               s1*(16*Power(t1,3) + Power(t1,2)*(98 - 42*t2) - 
                  31*(-1 + t2) + t1*(-72 + 71*t2)))) - 
         s*(-1 + t1)*(2*Power(s2,7)*(-7 + 3*s1 + 7*t1 - 3*t2)*
             (s1 - t2) - Power(s2,6)*
             (-6 - 6*Power(t1,3) + 2*Power(s1,2)*(1 + 6*t1) + 
               Power(t1,2)*(6 - 61*t2) - 15*t2 - 10*Power(t2,2) + 
               t1*(6 + 76*t2 + 24*Power(t2,2)) + 
               s1*(15 + 61*Power(t1,2) + 8*t2 - 4*t1*(19 + 9*t2))) - 
            Power(-1 + t1,2)*
             (-4*Power(-1 + t2,2) - 8*t1*(-1 + t2)*(-7 + s1 + t2) + 
               Power(t1,6)*(-5 + s1 + 2*t2) + 
               2*Power(t1,3)*
                (-80 - 22*Power(s1,2) + s1*(103 - 29*t2) + 49*t2 + 
                  Power(t2,2)) - 
               Power(t1,5)*(24 + Power(s1,2) + 22*t2 - s1*(26 + t2)) + 
               Power(t1,4)*(73 + 42*Power(s1,2) + 34*t2 + 
                  Power(t2,2) - s1*(149 + 5*t2)) + 
               2*Power(t1,2)*
                (87 + 4*Power(s1,2) - 92*t2 + 7*Power(t2,2) + 
                  s1*(-46 + 30*t2))) - 
            Power(s2,5)*(9 + 26*Power(t1,4) + 
               Power(s1,2)*(19 - 26*t1 + 3*Power(t1,2)) + 2*t2 - 
               5*Power(t2,2) + Power(t1,3)*(-35 + 113*t2) + 
               Power(t1,2)*(1 - 164*t2 - 53*Power(t2,2)) + 
               t1*(-1 + 49*t2 + 54*Power(t2,2)) + 
               s1*(-27 - 110*Power(t1,3) + t1*(4 - 36*t2) - 10*t2 + 
                  Power(t1,2)*(133 + 54*t2))) + 
            s2*(-1 + t1)*(-2*Power(t1,7) + 
               Power(s1,2)*t1*
                (-16 + 136*t1 - 288*Power(t1,2) + 169*Power(t1,3) + 
                  Power(t1,4)) + 3*Power(t1,6)*(-9 + t2) + 
               Power(t1,2)*(466 - 244*t2 - 70*Power(t2,2)) - 
               2*t1*(145 - 142*t2 + Power(t2,2)) + 
               Power(t1,5)*(-16 - 101*t2 + Power(t2,2)) + 
               12*(5 - 6*t2 + Power(t2,2)) + 
               Power(t1,4)*(151 + 314*t2 + 7*Power(t2,2)) + 
               2*Power(t1,3)*(-171 - 92*t2 + 27*Power(t2,2)) + 
               s1*(6*Power(t1,6) - 2*Power(t1,3)*(-465 + t2) + 
                  8*(-1 + t2) - 16*t1*(-11 + 7*t2) + 
                  Power(t1,5)*(91 + 8*t2) + 
                  4*Power(t1,2)*(-165 + 53*t2) - 
                  Power(t1,4)*(535 + 118*t2))) + 
            Power(s2,4)*(-3 + 44*Power(t1,5) + 
               Power(s1,2)*(-38 + 131*t1 - 98*Power(t1,2) + 
                  17*Power(t1,3)) - 16*t2 - 17*Power(t2,2) + 
               Power(t1,4)*(-59 + 116*t2) + 
               t1*(30 + 42*t2 + 32*Power(t2,2)) - 
               2*Power(t1,3)*(-5 + 89*t2 + 32*Power(t2,2)) + 
               Power(t1,2)*(-22 + 36*t2 + 61*Power(t2,2)) + 
               s1*(41 - 107*Power(t1,4) + 49*t2 + 
                  Power(t1,2)*(170 + 3*t2) + Power(t1,3)*(74 + 61*t2) - 
                  t1*(178 + 137*t2))) + 
            2*Power(s2,3)*(9 - 18*Power(t1,6) + 
               Power(s1,2)*(-16 + 112*t1 - 202*Power(t1,2) + 
                  103*Power(t1,3) - 2*Power(t1,4)) + 
               Power(t1,5)*(11 - 34*t2) - 32*t2 + 11*Power(t2,2) + 
               Power(t1,2)*(18 - 214*t2 - 49*Power(t2,2)) + 
               t1*(-27 + 138*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(-16 + 119*t2 + 10*Power(t2,2)) + 
               Power(t1,4)*(23 + 23*t2 + 18*Power(t2,2)) + 
               s1*(44 + 31*Power(t1,5) + Power(t1,4)*(27 - 19*t2) - 
                  t1*(199 + 97*t2) - Power(t1,3)*(247 + 97*t2) + 
                  Power(t1,2)*(344 + 223*t2))) + 
            Power(s2,2)*(14*Power(t1,7) - 
               2*Power(s1,2)*
                (4 - 62*t1 + 236*Power(t1,2) - 310*Power(t1,3) + 
                  128*Power(t1,4) + 3*Power(t1,5)) + 
               Power(t1,6)*(28 + 17*t2) + 
               t1*(182 + 50*t2 - 116*Power(t2,2)) - 
               2*Power(t1,5)*(37 - 54*t2 + 4*Power(t2,2)) + 
               4*(-15 + 9*t2 + 5*Power(t2,2)) + 
               2*Power(t1,3)*(55 + 443*t2 + 9*Power(t2,2)) - 
               Power(t1,4)*(4 + 575*t2 + 42*Power(t2,2)) + 
               2*Power(t1,2)*(-98 - 261*t2 + 65*Power(t2,2)) + 
               s1*(76 - 118*Power(t1,5) - 23*Power(t1,6) - 44*t2 + 
                  2*Power(t1,2)*(647 + 51*t2) + 2*t1*(-263 + 69*t2) - 
                  2*Power(t1,3)*(736 + 241*t2) + 
                  Power(t1,4)*(769 + 282*t2)))) + 
         Power(s,5)*(-5*Power(t1,6) + 
            5*Power(t1,5)*(-33 + 3*s1 - 7*t2) - 
            5*Power(t1,4)*(-77 + 3*Power(s1,2) + s1*(-79 + t2) + 
               86*t2 - 4*Power(t2,2)) + 4*(3 - 20*t2 + 25*Power(t2,2)) + 
            Power(t1,2)*(427 - 133*Power(s1,2) + s1*(1051 - 64*t2) - 
               365*t2 + 247*Power(t2,2)) + 
            Power(t1,3)*(-732 + 101*Power(s1,2) + 876*t2 - 
               115*Power(t2,2) + 2*s1*(-551 + 20*t2)) + 
            t1*(32 + 46*Power(s1,2) + 24*t2 - 254*Power(t2,2) + 
               s1*(-339 + 32*t2)) + 
            Power(s2,4)*(2 + 8*Power(s1,2) + 9*Power(t1,2) + 10*t2 + 
               40*Power(t2,2) + t1*(-3 + 17*t2) - s1*(5 + 8*t1 + 40*t2)) \
+ s2*(69 + 40*Power(t1,5) + Power(s1,2)*
                (-16 + 115*t1 - 127*Power(t1,2) + 33*Power(t1,3)) - 
               58*t2 + 187*Power(t2,2) + Power(t1,4)*(391 + 145*t2) + 
               t1*(-948 + 397*t2 - 447*Power(t2,2)) + 
               Power(t1,3)*(-1305 + 553*t2 - 115*Power(t2,2)) + 
               Power(t1,2)*(1923 - 912*t2 + 377*Power(t2,2)) + 
               s1*(144 - 60*Power(t1,4) + Power(t1,2)*(852 - 164*t2) - 
                  67*t2 + 6*t1*(-104 + 27*t2) + 
                  Power(t1,3)*(-467 + 60*t2))) + 
            Power(s2,3)*(109 + Power(s1,2)*(17 - 34*t1) + 
               10*Power(t1,3) + 47*t2 + 154*Power(t2,2) + 
               2*Power(t1,2)*(58 + 19*t2) - 
               t1*(271 + 156*t2 + 188*Power(t2,2)) + 
               s1*(-38 - 46*Power(t1,2) - 121*t2 + 3*t1*(47 + 60*t2))) + 
            Power(s2,2)*(276 - 58*Power(t1,4) + 
               Power(s1,2)*t1*(-1 + 6*t1) - 52*t2 + 217*Power(t2,2) - 
               Power(t1,3)*(319 + 157*t2) + 
               t1*(-1049 + 13*t2 - 439*Power(t2,2)) + 
               Power(t1,2)*(1086 + 29*t2 + 240*Power(t2,2)) + 
               s1*(16 + 95*Power(t1,3) - 119*t2 - 
                  3*Power(t1,2)*(15 + 64*t2) + t1*(73 + 290*t2)))) - 
         Power(s,4)*(9*Power(t1,7) + Power(t1,6)*(137 - 20*s1 + 14*t2) - 
            4*Power(t1,4)*(-61 - 490*s1 + 50*Power(s1,2) + 321*t2 - 
               25*Power(t2,2)) + 2*(14 - 60*t2 + 55*Power(t2,2)) + 
            5*Power(t1,5)*(-37 + 4*Power(s1,2) + 97*t2 - 
               3*Power(t2,2) - s1*(100 + t2)) + 
            Power(s2,5)*(12*Power(s1,2) + 2*Power(t1,2) + 
               s1*(-11 + 3*t1 - 40*t2) - t1*t2 + t2*(13 + 30*t2)) + 
            t1*(351 + 50*Power(s1,2) - 98*t2 - 351*Power(t2,2) + 
               s1*(-379 + 61*t2)) + 
            Power(t1,2)*(-643 - 249*Power(s1,2) + 270*t2 + 
               445*Power(t2,2) - 2*s1*(-856 + 73*t2)) + 
            Power(t1,3)*(61 + 380*Power(s1,2) + 733*t2 - 
               288*Power(t2,2) + s1*(-2773 + 88*t2)) + 
            Power(s2,4)*(99 + Power(s1,2)*(39 - 70*t1) + 
               39*Power(t1,3) + 143*t2 + 164*Power(t2,2) + 
               4*Power(t1,2)*(15 + 32*t2) - 
               t1*(208 + 331*t2 + 217*Power(t2,2)) + 
               s1*(-106 - 124*Power(t1,2) - 179*t2 + 5*t1*(56 + 53*t2))) \
- s2*(141 + 80*Power(t1,6) + 
               Power(s1,2)*(30 - 292*t1 + 634*Power(t1,2) - 
                  428*Power(t1,3) + 70*Power(t1,4)) - 58*t2 - 
               197*Power(t2,2) + 3*Power(t1,5)*(123 + 55*t2) + 
               Power(t1,2)*(-1909 + 1503*t2 - 770*Power(t2,2)) + 
               Power(t1,4)*(-1337 + 817*t2 - 110*Power(t2,2)) + 
               Power(t1,3)*(2457 - 2149*t2 + 438*Power(t2,2)) + 
               t1*(237 - 222*t2 + 649*Power(t2,2)) + 
               s1*(-251 - 95*Power(t1,5) + t1*(1607 - 241*t2) + 
                  Power(t1,3)*(2783 - 30*t2) + 85*t2 + 
                  4*Power(t1,4)*(-231 + 5*t2) + 
                  2*Power(t1,2)*(-1593 + 71*t2))) + 
            Power(s2,2)*(310 + 192*Power(t1,5) + 
               Power(s1,2)*(-59 + 232*t1 - 199*Power(t1,2) + 
                  48*Power(t1,3)) - 138*t2 + 254*Power(t2,2) + 
               Power(t1,4)*(299 + 378*t2) + 
               t1*(-1716 + 349*t2 - 739*Power(t2,2)) - 
               Power(t1,3)*(1835 + 132*t2 + 320*Power(t2,2)) + 
               Power(t1,2)*(2806 - 269*t2 + 809*Power(t2,2)) + 
               s1*(241 - 230*Power(t1,4) + Power(t1,2)*(589 - 462*t2) - 
                  111*t2 + 2*Power(t1,3)*(-93 + 116*t2) + 
                  t1*(-608 + 313*t2))) + 
            Power(s2,3)*(-162*Power(t1,4) + 
               2*Power(s1,2)*(7 - 30*t1 + 28*Power(t1,2)) - 
               Power(t1,3)*(139 + 350*t2) + 
               3*(100 + 43*t2 + 87*Power(t2,2)) + 
               Power(t1,2)*(975 + 758*t2 + 414*Power(t2,2)) - 
               t1*(986 + 621*t2 + 647*Power(t2,2)) + 
               s1*(-101 + 272*Power(t1,3) - 201*t2 - 
                  Power(t1,2)*(511 + 422*t2) + t1*(426 + 583*t2)))) - 
         Power(s,2)*(2*Power(s2,7)*(s1 - t2)*(-1 + s1 + t1 - t2) + 
            Power(s2,6)*(18 + Power(s1,2)*(26 - 34*t1) + 
               6*Power(t1,3) + 69*t2 + 38*Power(t2,2) + 
               Power(t1,2)*(6 + 73*t2) - 
               2*t1*(15 + 71*t2 + 23*Power(t2,2)) + 
               s1*(-69 - 73*Power(t1,2) - 64*t2 + 2*t1*(71 + 40*t2))) + 
            Power(s2,5)*(43 - 55*Power(t1,4) + 
               2*Power(s1,2)*(5 - 34*t1 + 33*Power(t1,2)) + 
               Power(t1,3)*(42 - 256*t2) + 110*t2 + 74*Power(t2,2) + 
               2*Power(t1,2)*(62 + 299*t2 + 81*Power(t2,2)) - 
               2*t1*(77 + 226*t2 + 114*Power(t2,2)) + 
               s1*(-74 + 248*Power(t1,3) - 84*t2 - 
                  6*Power(t1,2)*(91 + 38*t2) + 4*t1*(93 + 74*t2))) + 
            Power(-1 + t1,2)*
             (18 + Power(t1,7) + Power(t1,6)*(31 - 6*s1 - 8*t2) - 
               44*t2 + 26*Power(t2,2) + 
               Power(t1,2)*(-656 - 62*Power(s1,2) + 
                  s1*(458 - 158*t2) + 566*t2 + 38*Power(t2,2)) + 
               2*t1*(115 + 2*Power(s1,2) - 96*t2 - 21*Power(t2,2) + 
                  4*s1*(-8 + 5*t2)) + 
               Power(t1,5)*(96 + 6*Power(s1,2) + 136*t2 - 
                  Power(t2,2) - s1*(153 + 5*t2)) + 
               Power(t1,4)*(-265 - 125*Power(s1,2) - 248*t2 + 
                  3*Power(t2,2) + s1*(664 + 22*t2)) + 
               Power(t1,3)*(551 + 174*Power(s1,2) - 210*t2 - 
                  27*Power(t2,2) + s1*(-899 + 107*t2))) - 
            s2*(-1 + t1)*(-210 + 19*Power(t1,7) + 
               2*Power(s1,2)*
                (-2 + 56*t1 - 276*Power(t1,2) + 441*Power(t1,3) - 
                  227*Power(t1,4) + 12*Power(t1,5)) + 200*t2 + 
               14*Power(t2,2) + Power(t1,6)*(125 + 8*t2) + 
               Power(t1,3)*(654 + 1124*t2 - 174*Power(t2,2)) + 
               Power(t1,5)*(-95 + 464*t2 - 14*Power(t2,2)) + 
               Power(t1,4)*(-186 - 1396*t2 + 27*Power(t2,2)) - 
               2*t1*(-451 + 349*t2 + 72*Power(t2,2)) + 
               Power(t1,2)*(-1209 + 298*t2 + 299*Power(t2,2)) - 
               s1*(34*Power(t1,6) + t1*(798 - 282*t2) + 
                  30*Power(t1,5)*(16 + t2) + 
                  194*Power(t1,3)*(18 + t2) + 8*(-8 + 5*t2) + 
                  7*Power(t1,2)*(-369 + 47*t2) - 
                  Power(t1,4)*(2157 + 295*t2))) + 
            Power(s2,3)*(111 - 166*Power(t1,6) - 
               2*Power(s1,2)*
                (50 - 249*t1 + 361*Power(t1,2) - 179*Power(t1,3) + 
                  22*Power(t1,4)) + Power(t1,5)*(176 - 326*t2) - 
               166*t2 + 39*Power(t2,2) + 
               t1*(-499 + 512*t2 - 97*Power(t2,2)) + 
               2*Power(t1,4)*(220 + 328*t2 + 97*Power(t2,2)) + 
               Power(t1,2)*(959 - 404*t2 + 273*Power(t2,2)) - 
               Power(t1,3)*(1021 + 272*t2 + 419*Power(t2,2)) + 
               s1*(217 + 268*Power(t1,5) + 57*t2 - 
                  2*Power(t1,4)*(109 + 81*t2) + 
                  Power(t1,3)*(-675 + 101*t2) - t1*(859 + 377*t2) + 
                  Power(t1,2)*(1267 + 401*t2))) + 
            Power(s2,4)*(40 + 145*Power(t1,5) - 
               Power(s1,2)*(59 - 114*t1 + 31*Power(t1,2) + 
                  20*Power(t1,3)) + 38*t2 + 30*Power(t2,2) + 
               Power(t1,4)*(-187 + 394*t2) - 
               Power(t1,3)*(217 + 1006*t2 + 253*Power(t2,2)) - 
               t1*(236 + 386*t2 + 265*Power(t2,2)) + 
               Power(t1,2)*(455 + 960*t2 + 492*Power(t2,2)) + 
               s1*(35 - 354*Power(t1,4) + 31*t2 + t1*(41 + 157*t2) + 
                  Power(t1,3)*(727 + 283*t2) - 
                  Power(t1,2)*(449 + 479*t2))) + 
            Power(s2,2)*(88*Power(t1,7) + 
               2*Power(s1,2)*
                (-23 + 231*t1 - 669*Power(t1,2) + 752*Power(t1,3) - 
                  313*Power(t1,4) + 24*Power(t1,5)) + 
               Power(t1,6)*(40 + 133*t2) + 
               t1*(107 + 480*t2 - 313*Power(t2,2)) + 
               Power(t1,3)*(-1047 + 2682*t2 - 209*Power(t2,2)) - 
               4*Power(t1,5)*(143 - 49*t2 + 18*Power(t2,2)) + 
               2*(-53 + 8*t2 + 39*Power(t2,2)) + 
               Power(t1,4)*(1029 - 1621*t2 + 145*Power(t2,2)) + 
               Power(t1,2)*(461 - 1886*t2 + 375*Power(t2,2)) + 
               s1*(276 - 119*Power(t1,6) + 6*Power(t1,5)*(-68 + t2) - 
                  84*t2 + t1*(-1719 + 91*t2) + 
                  Power(t1,4)*(2494 + 439*t2) + 
                  Power(t1,2)*(3979 + 561*t2) - 
                  Power(t1,3)*(4503 + 1021*t2)))) + 
         Power(s,3)*(2*Power(s2,6)*(-4 + 4*s1 + 3*t1 - 6*t2)*(s1 - t2) + 
            Power(s2,2)*(55 - 198*Power(t1,6) + 
               Power(s1,2)*(-94 + 587*t1 - 1020*Power(t1,2) + 
                  595*Power(t1,3) - 92*Power(t1,4)) - 124*t2 + 
               169*Power(t2,2) - 2*Power(t1,5)*(64 + 171*t2) + 
               Power(t1,3)*(-2803 + 1238*t2 - 623*Power(t2,2)) + 
               t1*(-969 + 860*t2 - 569*Power(t2,2)) + 
               4*Power(t1,4)*(366 - 6*t2 + 55*Power(t2,2)) + 
               Power(t1,2)*(2579 - 1646*t2 + 783*Power(t2,2)) + 
               s1*(431 + 240*Power(t1,5) + Power(t1,4)*(501 - 108*t2) + 
                  16*Power(t1,3)*(-142 + t2) - 77*t2 + 
                  2*t1*(-946 + 3*t2) + 3*Power(t1,2)*(1010 + 69*t2))) - 
            (-1 + t1)*(5*Power(t1,7) + Power(t1,6)*(84 - 15*s1 - 7*t2) + 
               4*(8 - 25*t2 + 18*Power(t2,2)) + 
               Power(t1,2)*(-1079 - 178*Power(s1,2) + 
                  s1*(1187 - 199*t2) + 744*t2 + 219*Power(t2,2)) + 
               Power(t1,5)*(87 + 15*Power(s1,2) + 350*t2 - 
                  6*Power(t2,2) - s1*(377 + 9*t2)) + 
               2*t1*(213 + 12*Power(s1,2) - 126*t2 - 97*Power(t2,2) + 
                  2*s1*(-55 + 18*t2)) + 
               Power(t1,4)*(-319 - 205*Power(s1,2) - 761*t2 + 
                  36*Power(t2,2) + s1*(1508 + 31*t2)) + 
               Power(t1,3)*(770 + 345*Power(s1,2) + 26*t2 - 
                  126*Power(t2,2) + s1*(-2083 + 103*t2))) + 
            Power(s2,5)*(62 + Power(s1,2)*(45 - 71*t1) + 
               27*Power(t1,3) + 143*t2 + 105*Power(t2,2) + 
               24*Power(t1,2)*(1 + 6*t2) - 
               t1*(113 + 303*t2 + 139*Power(t2,2)) + 
               s1*(-141*Power(t1,2) - 2*(63 + 73*t2) + t1*(283 + 206*t2))\
) + s2*(-298 + 59*Power(t1,7) + 
               Power(s1,2)*(-20 + 288*t1 - 1025*Power(t1,2) + 
                  1340*Power(t1,3) - 642*Power(t1,4) + 65*Power(t1,5)) + 
               216*t2 + 106*Power(t2,2) + 6*Power(t1,6)*(40 + 13*t2) + 
               Power(t1,5)*(-735 + 809*t2 - 57*Power(t2,2)) - 
               4*Power(t1,3)*(201 - 705*t2 + 148*Power(t2,2)) + 
               Power(t1,4)*(1136 - 2781*t2 + 237*Power(t2,2)) - 
               2*t1*(-501 + 275*t2 + 250*Power(t2,2)) + 
               Power(t1,2)*(-600 - 592*t2 + 812*Power(t2,2)) - 
               s1*(-192 + 79*Power(t1,6) + t1*(1740 - 350*t2) + 76*t2 + 
                  Power(t1,5)*(916 + 30*t2) + 
                  3*Power(t1,3)*(2113 + 88*t2) + 
                  Power(t1,2)*(-4931 + 267*t2) - 
                  Power(t1,4)*(3951 + 275*t2))) + 
            Power(s2,4)*(150 - 149*Power(t1,4) + 
               Power(s1,2)*(22 - 105*t1 + 99*Power(t1,2)) + 229*t2 + 
               185*Power(t2,2) - 9*Power(t1,3)*(-3 + 47*t2) + 
               Power(t1,2)*(501 + 1033*t2 + 370*Power(t2,2)) - 
               t1*(529 + 851*t2 + 531*Power(t2,2)) + 
               s1*(379*Power(t1,3) - 5*(29 + 37*t2) - 
                  Power(t1,2)*(813 + 455*t2) + t1*(591 + 600*t2))) + 
            Power(s2,3)*(Power(s1,2)*
                (-85 + 216*t1 - 117*Power(t1,2) + 6*Power(t1,3)) + 
               s1*(109 - 420*Power(t1,4) - 23*t2 + t1*(-79 + 288*t2) + 
                  Power(t1,3)*(587 + 398*t2) - Power(t1,2)*(261 + 695*t2)\
) + 2*(138 + 133*Power(t1,5) - 28*t2 + 74*Power(t2,2) + 
                  Power(t1,4)*(-42 + 271*t2) - 
                  Power(t1,3)*(563 + 577*t2 + 208*Power(t2,2)) - 
                  t1*(580 + 157*t2 + 300*Power(t2,2)) + 
                  Power(t1,2)*(914 + 523*t2 + 440*Power(t2,2))))))*
       R1q(1 - s + s2 - t1))/
     (s*(-1 + s2)*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*(-1 + t1)*
       Power(-1 + s + t1,3)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*
       (-1 + t2)) - (8*(3*Power(s,5)*t1*(-2 + t1 + t2) + 
         Power(s,4)*(4*Power(t1,3) + Power(t1,2)*(-15 + t2) - 
            2*Power(t2,2) + s2*
             (-6 - 3*Power(t1,2) + t1*(13 + 3*s1 - 10*t2) + 3*t2 - 
               2*Power(t2,2)) + 
            t1*(29 + 6*s1 - 27*t2 - 3*s1*t2 + Power(t2,2))) - 
         Power(s,3)*(Power(t1,4) + Power(t1,3)*(6 + 3*s1 - 2*t2) + 
            2*(2 - 5*t2)*t2 + 
            Power(t1,2)*(-2 - 22*s1 + 3*Power(s1,2) + 22*t2 + 
               Power(t2,2)) + 
            t1*(38 - 6*Power(s1,2) + s1*(19 - 4*t2) - 15*t2 + 
               4*Power(t2,2)) + 
            Power(s2,2)*(-4 + 3*Power(t1,2) + 3*t2 - 8*Power(t2,2) - 
               2*t1*(3 + 4*t2) + s1*(-3 + 3*t1 + 4*t2)) + 
            s2*(-23 + 3*Power(s1,2)*t1 + Power(t1,2) + 3*Power(t1,3) + 
               24*t2 - 13*Power(t2,2) + 
               s1*(-6 + 2*Power(t1,2) + t1*(16 - 11*t2) + 7*t2) + 
               t1*(23 - 56*t2 + 6*Power(t2,2)))) - 
         Power(-1 + s2,2)*(-((-1 + s1)*Power(t1,4)) + 
            4*Power(-1 + t2,2) + 
            2*Power(s2,3)*(-1 + Power(s1,2) + t1 + 
               s1*(-1 + t1 - 2*t2) + 2*t2 - 2*t1*t2 + Power(t2,2)) + 
            Power(t1,3)*(7 + Power(s1,2) - 12*t2 + s1*(3 + t2)) + 
            t1*(17 - 2*Power(s1,2) - 28*t2 - 5*Power(t2,2) + 
               3*s1*(5 + t2)) + 
            Power(t1,2)*(-29 + 3*Power(s1,2) + 48*t2 + 3*Power(t2,2) - 
               s1*(17 + 8*t2)) + 
            Power(s2,2)*(6 - Power(s1,2)*(1 + t1) - 3*t2 + 
               Power(t2,2) + Power(t1,2)*(2 + t2) + 
               t1*(-8 + 2*t2 - 3*Power(t2,2)) + 
               s1*(-5 - 3*Power(t1,2) + 2*t2 + 2*t1*(4 + t2))) - 
            s2*(29 + 2*Power(s1,2)*(-1 + t1 + Power(t1,2)) + 
               Power(t1,3)*(5 - 3*t2) - 52*t2 + 7*Power(t2,2) + 
               t1*(-35 + 69*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(1 - 14*t2 + 3*Power(t2,2)) - 
               s1*(2*Power(t1,3) + Power(t1,2)*(-9 + t2) - 3*(5 + t2) + 
                  t1*(22 + 6*t2)))) - 
         Power(s,2)*(-((1 + s1)*Power(t1,4)) + 
            Power(t1,3)*(35 + Power(s1,2) + s1*(-13 + t2)) + 
            Power(t1,2)*(-32 - 9*Power(s1,2) + s1*(39 - 4*t2) + 19*t2 + 
               Power(t2,2)) + 2*(1 - 8*t2 + 9*Power(t2,2)) + 
            Power(s2,3)*(-8 + 2*Power(s1,2) - 3*Power(t1,2) + 
               s1*(-4 + 5*t1 - 12*t2) + t1*(21 - 6*t2) + 7*t2 + 
               12*Power(t2,2)) + 
            t1*(48 + 10*Power(s1,2) - 59*t2 - 10*Power(t2,2) + 
               s1*(-21 + 2*t2)) + 
            s2*(13 - 2*Power(t1,4) + 
               Power(s1,2)*(-6 + 4*t1 - 4*Power(t1,2)) + 9*t2 + 
               13*Power(t2,2) + 7*Power(t1,3)*(1 + t2) + 
               t1*(66 - 135*t2 - 2*Power(t2,2)) - 
               Power(t1,2)*(38 + 12*t2 + 5*Power(t2,2)) + 
               s1*(17 - 13*t2 + Power(t1,2)*(13 + t2) + t1*(47 + 14*t2))\
) + Power(s2,2)*(-12 + Power(s1,2)*(5 - 3*t1) + 2*Power(t1,3) - 27*t2 + 
               17*Power(t2,2) + Power(t1,2)*(-27 + 4*t2) + 
               t1*(9 + 33*t2 - 12*Power(t2,2)) + 
               s1*(7 - 7*Power(t1,2) - 20*t2 + t1*(-2 + 15*t2)))) + 
         s*(6 + (1 - 2*s1)*Power(t1,4) - 20*t2 + 14*Power(t2,2) + 
            Power(s2,4)*(-8 + 4*Power(s1,2) + s1*(-9 + 7*t1 - 12*t2) + 
               t1*(10 - 11*t2) + 11*t2 + 8*Power(t2,2)) + 
            Power(t1,3)*(20 + 2*Power(s1,2) - 22*t2 + s1*(5 + 2*t2)) + 
            t1*(84 + 2*Power(s1,2) - 78*t2 - 12*Power(t2,2) + 
               s1*(7 + 4*t2)) + 
            Power(t1,2)*(-3*Power(s1,2) - 12*s1*(-1 + t2) + 
               5*(-19 + 16*t2 + Power(t2,2))) + 
            Power(s2,3)*(19 - Power(s1,2)*t1 + Power(t1,3) - 22*t2 + 
               3*Power(t2,2) + Power(t1,2)*(-9 + 4*t2) + 
               t1*(-17 + 14*t2 - 10*Power(t2,2)) + 
               s1*(8 - 8*Power(t1,2) - 3*t2 + 3*t1*(4 + 3*t2))) + 
            Power(s2,2)*(-38 - Power(t1,4) + 
               Power(s1,2)*(4 - 10*t1 - 3*Power(t1,2)) + 
               8*Power(t1,3)*(-1 + t2) + 137*t2 - 12*Power(t2,2) + 
               Power(t1,2)*(27 + 14*t2 - 7*Power(t2,2)) + 
               t1*(42 - 191*t2 + 16*Power(t2,2)) + 
               s1*(-87 + 5*Power(t1,3) + 2*Power(t1,2)*(-10 + t2) + 
                  4*t2 + 2*t1*(53 + 6*t2))) + 
            s2*(-71 + 4*Power(t1,4) + 
               Power(s1,2)*(-4 + 5*t1 + 2*Power(t1,2) + 2*Power(t1,3)) + 
               74*t2 - 13*Power(t2,2) - Power(t1,3)*(5 + 22*t2) + 
               Power(t1,2)*(-43 + 154*t2 + 2*Power(t2,2)) + 
               t1*(81 - 130*t2 + 6*Power(t2,2)) + 
               s1*(16 - 2*Power(t1,4) - 9*t2 + 2*Power(t1,3)*(5 + t2) - 
                  6*Power(t1,2)*(14 + 3*t2) + t1*(24 + 19*t2)))))*R2q(s))/
     (s*(-1 + s2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-1 + t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(-1 + t2)) - 
    (8*(-(Power(s,7)*(2*Power(t1,3) - (-4 + s2)*t1*t2 + 
              (1 + s2)*Power(t2,2) + Power(t1,2)*(-1 + 2*t2))) + 
         Power(s,6)*(-4*Power(t1,4) + 
            Power(t1,3)*(9 + s1 + 7*s2 - 4*t2) + 
            Power(t1,2)*(-16 - Power(s2,2) + 3*t2 + 
               s1*(4 - 2*s2 + t2) + s2*(-4 + 13*t2)) + 
            t2*(-2 + 9*t2 + Power(s2,2)*(3 - 2*s1 + 7*t2) + 
               s2*(-4 - 2*s1 + 13*t2)) - 
            t1*(6 - 13*t2 + 2*Power(t2,2) + Power(s2,2)*(-1 + 7*t2) + 
               s2*(-7 - 11*t2 + 2*Power(t2,2)) + 
               s1*(-2 + Power(s2,2) + s2*(4 + t2)))) + 
         Power(-1 + s2,3)*(2*(-1 + s1)*Power(t1,5) + 
            Power(s2,5)*(s1 - t2)*(-1 + s1 + t1 - t2) - 
            4*Power(-1 + t2,2) + t1*(-1 - 10*t2 + 11*Power(t2,2)) + 
            Power(t1,2)*(23 + 4*Power(s1,2) + 3*t2 - 10*Power(t2,2) - 
               4*s1*(4 + t2)) + 
            Power(t1,4)*(13 + 2*Power(s1,2) - s1*(13 + 2*t2)) - 
            Power(t1,3)*(29 + 7*Power(s1,2) + t2 - 2*Power(t2,2) - 
               s1*(27 + 8*t2)) + 
            Power(s2,4)*(1 + Power(s1,2)*(1 - 2*t1) + 3*t2 + 
               Power(t2,2) + Power(t1,2)*(1 + 2*t2) - 
               t1*(2 + 5*t2 + 2*Power(t2,2)) + 
               s1*(-3*Power(t1,2) - 2*(2 + t2) + t1*(7 + 4*t2))) - 
            Power(s2,3)*(-3 + Power(s1,2)*(1 + t1) - 7*t2 + 
               2*Power(t2,2) + Power(t1,3)*(2 + t2) - 
               Power(t1,2)*(7 + 5*t2 + Power(t2,2)) + 
               t1*(8 + 11*t2 + Power(t2,2)) + 
               s1*(9 - 3*Power(t1,3) - 2*t2 + Power(t1,2)*(11 + 2*t2) - 
                  t1*(17 + 4*t2))) - 
            s2*(-17 + Power(t1,4)*(-3 + 4*s1 + Power(s1,2) - 2*t2) + 
               22*t2 - 5*Power(t2,2) + 
               t1*(55 + 8*Power(s1,2) - 36*t2 + 13*Power(t2,2) - 
                  8*s1*(4 + t2)) + 
               Power(t1,2)*(-62 - 13*Power(s1,2) + 9*t2 - 
                  11*Power(t2,2) + 7*s1*(9 + 2*t2)) + 
               Power(t1,3)*(27 + 3*Power(s1,2) + 7*t2 + 2*Power(t2,2) - 
                  s1*(35 + 4*t2))) + 
            Power(s2,2)*(8 + Power(t1,4) + 
               Power(s1,2)*(4 - 5*t1 + Power(t1,2) + 2*Power(t1,3)) + 
               9*t2 - Power(t2,2) - 3*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(17 + 23*t2 - 2*Power(t2,2)) + 
               t1*(-20 - 29*t2 + 5*Power(t2,2)) - 
               s1*(-7*Power(t1,3) + Power(t1,4) + 4*(4 + t2) + 
                  Power(t1,2)*(35 + 4*t2) - t1*(45 + 4*t2)))) + 
         Power(s,5)*(-1 - 2*Power(t1,5) + 
            Power(t1,4)*(9 + 3*s1 - 2*t2) + 16*t2 - 34*Power(t2,2) + 
            Power(t1,3)*(-24 + Power(s1,2) + 13*t2 + s1*t2) + 
            t1*(25 - 14*s1 + 5*t2 + 19*Power(t2,2)) - 
            Power(t1,2)*(-40 + 5*Power(s1,2) + t2 + 2*Power(t2,2) + 
               s1*(4 + 3*t2)) + 
            Power(s2,3)*(-Power(s1,2) + 5*Power(t1,2) + 
               s1*(1 + 4*t1 + 12*t2) + t1*(-5 + 21*t2) - t2*(16 + 21*t2)\
) + Power(s2,2)*(7 + Power(s1,2)*(-1 + t1) - 10*Power(t1,3) + 
               Power(t1,2)*(1 - 37*t2) + 2*t2 - 48*Power(t2,2) + 
               2*t1*(-11 + 5*t2 + 6*Power(t2,2)) + 
               s1*(-4 + 7*Power(t1,2) + 19*t2 + t1*(14 + t2))) + 
            s2*(-5 + 11*Power(t1,4) + (15 + 14*s1)*t2 - 54*Power(t2,2) + 
               Power(t1,3)*(-13 - 6*s1 + 18*t2) + 
               t1*(-23 + 4*s1 + 2*Power(s1,2) + 23*t2 + 
                  21*Power(t2,2)) + 
               Power(t1,2)*(75 + 3*Power(s1,2) - 19*t2 - Power(t2,2) - 
                  s1*(33 + 8*t2)))) + 
         Power(s,4)*(7 + 2*s1*Power(t1,5) - 52*t2 + 70*Power(t2,2) + 
            Power(t1,4)*(21 - 13*s1 + Power(s1,2) + 4*t2) + 
            t1*(-34 + 38*s1 - 64*t2 - 67*Power(t2,2)) + 
            Power(t1,3)*(5 + 21*s1 - 14*Power(s1,2) - 21*t2 - 
               2*Power(t2,2)) + 
            Power(t1,2)*(-18 - 46*s1 + 23*Power(s1,2) + 37*t2 + 
               18*Power(t2,2)) + 
            5*Power(s2,4)*(Power(s1,2) - 2*Power(t1,2) + 
               t1*(2 - 7*t2) + 7*t2*(1 + t2) - s1*(1 + t1 + 6*t2)) + 
            Power(s2,3)*(Power(s1,2)*(8 - 6*t1) + 10*Power(t1,3) + 
               15*Power(t1,2)*(1 + 4*t2) - 
               6*t1*(-2 + 12*t2 + 5*Power(t2,2)) + 
               5*(-3 + 4*t2 + 16*Power(t2,2)) + 
               s1*(7 - 11*Power(t1,2) - 52*t2 + 2*t1*(-4 + 5*t2))) + 
            Power(s2,2)*(-12 - 10*Power(t1,4) + 
               Power(s1,2)*(7 - 10*t1 - 10*Power(t1,2)) + 33*t2 + 
               100*Power(t2,2) - Power(t1,3)*(17 + 33*t2) + 
               t1*(58 - 197*t2 - 59*Power(t2,2)) + 
               Power(t1,2)*(-96 + 51*t2 + 5*Power(t2,2)) + 
               s1*(2 + 13*Power(t1,3) - 54*t2 + t1*(5 + 18*t2) + 
                  Power(t1,2)*(94 + 20*t2))) + 
            s2*(14 + 4*Power(t1,5) - 2*(2 + 19*s1)*t2 + 97*Power(t2,2) + 
               Power(t1,4)*(2 - 9*s1 + 6*t2) + 
               2*Power(t1,3)*
                (28 + Power(s1,2) - 10*t2 - s1*(19 + 3*t2)) + 
               Power(t1,2)*(-116 + 16*Power(s1,2) + 94*t2 + 
                  11*Power(t2,2) + s1*(53 + 14*t2)) + 
               t1*(58 - 22*Power(s1,2) - 218*t2 - 82*Power(t2,2) + 
                  s1*(52 + 30*t2)))) + 
         s*(-16 - 8*t1 - 8*s1*t1 + 5*Power(t1,2) + 6*s1*Power(t1,2) + 
            4*Power(s1,2)*Power(t1,2) + 64*Power(t1,3) - 
            30*s1*Power(t1,3) + 2*Power(s1,2)*Power(t1,3) - 
            53*Power(t1,4) + 11*s1*Power(t1,4) - 
            7*Power(s1,2)*Power(t1,4) + 8*Power(t1,5) + 
            10*s1*Power(t1,5) + 40*t2 - 19*t1*t2 - 35*Power(t1,2)*t2 - 
            9*s1*Power(t1,2)*t2 + 27*Power(t1,3)*t2 + 
            13*s1*Power(t1,3)*t2 + 6*Power(t1,4)*t2 - 8*Power(t1,5)*t2 - 
            24*Power(t2,2) + 55*t1*Power(t2,2) - 
            42*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) + 
            Power(s2,7)*(-5*Power(s1,2) + Power(t1,2) + 
               t1*(-1 + 7*t2) - t2*(8 + 7*t2) + s1*(5 - 4*t1 + 12*t2)) + 
            Power(s2,5)*(3 + Power(t1,4) + 
               Power(s1,2)*(2 - t1 + Power(t1,2)) - 35*t2 + 
               14*Power(t2,2) + 2*Power(t1,3)*(5 + 3*t2) - 
               s1*(-22 + 10*Power(t1,3) + t1*(8 - 6*t2) + 
                  Power(t1,2)*(1 - 4*t2) + 14*t2) + 
               t1*(-5 + 35*t2 - 7*Power(t2,2)) - 
               Power(t1,2)*(9 + 9*t2 + 5*Power(t2,2))) + 
            Power(s2,4)*(-29 - 
               Power(s1,2)*(11 - 31*t1 + 20*Power(t1,2) + 
                  9*Power(t1,3)) + Power(t1,3)*(1 - 7*t2) + 20*t2 - 
               8*Power(t2,2) + t1*(85 + 31*t2 - 13*Power(t2,2)) + 
               Power(t1,2)*(-57 - 71*t2 + 10*Power(t2,2)) + 
               s1*(3*Power(t1,4) - 4*t1*(32 + t2) + 4*(8 + 3*t2) + 
                  Power(t1,3)*(32 + 3*t2) + Power(t1,2)*(88 + 9*t2))) + 
            Power(s2,6)*(-5 - 2*Power(t1,3) + Power(s1,2)*(1 + 9*t1) - 
               2*t2 + 2*Power(t2,2) - Power(t1,2)*(7 + 13*t2) + 
               2*t1*(7 + 11*t2 + 6*Power(t2,2)) + 
               s1*(4 + 11*Power(t1,2) - 5*t2 - t1*(22 + 19*t2))) - 
            s2*(-67 + 2*Power(t1,5)*(11 + 8*s1 - 8*t2) + 
               (143 - 8*s1)*t2 - 48*Power(t2,2) + 
               Power(t1,4)*(-67 - 16*s1 + 10*Power(s1,2) + 6*t2) + 
               Power(t1,2)*(115 + 59*Power(s1,2) + 43*t2 - 
                  51*Power(t2,2) - 17*s1*(7 + 2*t2)) + 
               t1*(8 - 8*Power(s1,2) - 179*t2 + 91*Power(t2,2) + 
                  2*s1*(7 + 8*t2)) + 
               Power(t1,3)*(-11 - 57*Power(s1,2) + 32*t2 + 
                  8*Power(t2,2) + s1*(76 + 22*t2))) - 
            Power(s2,2)*(76 + 
               Power(s1,2)*(12 - 48*t1 + 8*Power(t1,2) + 
                  9*Power(t1,3) + 3*Power(t1,4)) + 
               8*Power(t1,5)*(-2 + t2) - 94*t2 + 26*Power(t2,2) + 
               Power(t1,4)*(13 + 6*t2) + 
               t1*(-61 + 154*t2 - 26*Power(t2,2)) + 
               Power(t1,3)*(103 + 28*t2 + 8*Power(t2,2)) - 
               Power(t1,2)*(115 + 111*t2 + 24*Power(t2,2)) - 
               s1*(48 + 22*Power(t1,4) + 6*Power(t1,5) - 15*t2 - 
                  3*Power(t1,2)*(11 + 24*t2) + 
                  Power(t1,3)*(38 + 24*t2) + t1*(-90 + 31*t2))) + 
            Power(s2,3)*(40 - 2*Power(t1,5) + 
               Power(s1,2)*(9 - 31*t1 - 14*Power(t1,2) + 
                  23*Power(t1,3) + 4*Power(t1,4)) + 
               6*Power(t1,4)*(-3 + t2) + 34*t2 + Power(t2,2) + 
               Power(t1,2)*(-29 + 60*t2 - 38*Power(t2,2)) + 
               Power(t1,3)*(83 + 34*t2 + 8*Power(t2,2)) + 
               t1*(-74 - 101*t2 + 18*Power(t2,2)) + 
               s1*(-79 - 20*Power(t1,4) + 2*t2 + 2*t1*(73 + t2) - 
                  2*Power(t1,3)*(41 + 9*t2) + Power(t1,2)*(2 + 34*t2)))) \
+ Power(s,2)*(25 - 82*t2 + 61*Power(t2,2) + 
            Power(t1,5)*(-58 + 8*s1 + 4*t2) + 
            t1*(11 + 32*s1 - 23*t2 - 112*Power(t2,2)) - 
            Power(t1,3)*(141 - 137*s1 + 37*Power(s1,2) + 48*t2 + 
               12*Power(t2,2)) + 
            Power(t1,2)*(71 + 17*Power(s1,2) + 3*s1*(-34 + t2) + 
               115*t2 + 68*Power(t2,2)) + 
            Power(t1,4)*(112 + 15*Power(s1,2) - 12*t2 - 
               2*s1*(19 + t2)) + 
            Power(s2,6)*(10*Power(s1,2) - 5*Power(t1,2) + 
               t1*(5 - 21*t2) + 5*s1*(-2 + t1 - 6*t2) + t2*(25 + 21*t2)) \
+ Power(s2,5)*(6 + 7*Power(t1,3) - 2*Power(s1,2)*(-5 + 8*t1) + 16*t2 + 
               21*Power(t2,2) + Power(t1,2)*(20 + 37*t2) - 
               t1*(33 + 73*t2 + 30*Power(t2,2)) + 
               s1*(-6 - 16*Power(t1,2) - 22*t2 + 5*t1*(8 + 7*t2))) + 
            Power(s2,2)*(-36 - 8*Power(t1,5) + 
               Power(s1,2)*(33 - 85*t1 + 77*Power(t1,2) - 
                  27*Power(t1,3) - 4*Power(t1,4)) + 
               Power(t1,4)*(29 - 22*t2) + 75*t2 + 9*Power(t2,2) + 
               t1*(42 - 159*t2 - 32*Power(t2,2)) - 
               3*Power(t1,3)*(39 + 18*t2 + 4*Power(t2,2)) + 
               2*Power(t1,2)*(37 + 83*t2 + 25*Power(t2,2)) + 
               s1*(36*Power(t1,4) + 2*Power(t1,5) + 
                  16*Power(t1,3)*(2 + t2) - 8*(10 + t2) + 
                  7*t1*(13 + 6*t2) - 6*Power(t1,2)*(13 + 10*t2))) + 
            Power(s2,3)*(18 + 
               Power(s1,2)*(17 - 62*t1 + 39*Power(t1,2) + 
                  16*Power(t1,3)) + 96*t2 + 8*Power(t2,2) + 
               Power(t1,4)*(13 + 2*t2) + 4*Power(t1,3)*(-2 + 5*t2) + 
               Power(t1,2)*(129 + 200*t2 + 2*Power(t2,2)) - 
               4*t1*(32 + 59*t2 + 6*Power(t2,2)) - 
               2*s1*(3*Power(t1,4) + 8*(3 + t2) + 
                  Power(t1,3)*(52 + 5*t2) + Power(t1,2)*(53 + 8*t2) - 
                  t1*(89 + 25*t2))) + 
            Power(s2,4)*(-2*Power(t1,4) + 
               Power(s1,2)*(10 - 19*t1 - 6*Power(t1,2)) - 
               6*Power(t1,3)*(5 + 3*t2) - t1*t2*(185 + 24*t2) - 
               3*(-4 - 36*t2 + Power(t2,2)) + 
               Power(t1,2)*(16 + 43*t2 + 10*Power(t2,2)) + 
               s1*(15*Power(t1,3) - 4*(14 + t2) + 
                  Power(t1,2)*(48 + 5*t2) + t1*(46 + 30*t2))) - 
            s2*(49 - Power(t1,4)*(64 + 5*Power(s1,2) + 2*s1*(-8 + t2)) + 
               2*Power(t1,5)*(3 + s1 - 6*t2) + 2*(-81 + 16*s1)*t2 + 
               21*Power(t2,2) + 
               Power(t1,3)*(47 + 64*Power(s1,2) + 76*t2 + 
                  8*Power(t2,2) - 10*s1*(8 + t2)) + 
               Power(t1,2)*(17 - 129*Power(s1,2) - 223*t2 - 
                  30*Power(t2,2) + s1*(226 + 76*t2)) + 
               t1*(-31 + 58*Power(s1,2) + 295*t2 + 2*Power(t2,2) - 
                  s1*(144 + 83*t2)))) - 
         Power(s,3)*(19 - 88*t2 + 85*Power(t2,2) + 
            Power(t1,5)*(-26 + 6*s1 + 4*t2) + 
            t1*(-11 + 50*s1 - 82*t2 - 118*Power(t2,2)) + 
            Power(t1,4)*(-36*s1 + 7*Power(s1,2) + 4*(29 + t2)) + 
            Power(t1,2)*(60 + 35*Power(s1,2) + 114*t2 + 52*Power(t2,2) - 
               2*s1*(63 + 2*t2)) + 
            5*Power(s2,5)*(2*Power(s1,2) - 2*Power(t1,2) + 
               t1*(2 - 7*t2) - 2*s1*(1 + 4*t2) + t2*(8 + 7*t2)) - 
            Power(t1,3)*(41*Power(s1,2) - 6*s1*(17 + t2) + 
               8*(8 + 4*t2 + Power(t2,2))) + 
            Power(s2,4)*(-6 + 10*Power(t1,3) - 
               2*Power(s1,2)*(-8 + 7*t1) + 32*t2 + 65*Power(t2,2) + 
               3*Power(t1,2)*(9 + 20*t2) - 
               4*t1*(6 + 27*t2 + 10*Power(t2,2)) + 
               s1*(-14*Power(t1,2) - 58*t2 + 6*t1*(4 + 5*t2))) + 
            Power(s2,3)*(6 - 4*Power(t1,4) + 
               Power(s1,2)*(18 - 23*t1 - 12*Power(t1,2)) + 120*t2 + 
               64*Power(t2,2) - Power(t1,3)*(43 + 32*t2) + 
               t1*(22 - 310*t2 - 66*Power(t2,2)) + 
               2*Power(t1,2)*(-13 + 34*t2 + 5*Power(t2,2)) + 
               2*s1*(8*Power(t1,3) + 5*Power(t1,2)*(11 + 2*t2) - 
                  4*(4 + 7*t2) + t1*(24 + 23*t2))) + 
            Power(s2,2)*(-32 + 2*Power(t1,5) + 
               Power(s1,2)*(23 - 61*t1 + 31*Power(t1,2) + 
                  12*Power(t1,3)) + 132*t2 + 70*Power(t2,2) + 
               Power(t1,3)*(25 + 6*t2) + Power(t1,4)*(25 + 6*t2) + 
               t1*(28 - 428*t2 - 98*Power(t2,2)) + 
               Power(t1,2)*(19 + 236*t2 + 16*Power(t2,2)) - 
               2*s1*(19 + 5*Power(t1,4) + 28*t2 - 
                  Power(t1,2)*(10 + 3*t2) + Power(t1,3)*(56 + 6*t2) - 
                  t1*(59 + 40*t2))) + 
            s2*(-2 + 2*(-5 + 2*s1)*Power(t1,5) + 
               2*Power(t1,4)*(16 + s1 - 5*t2) + (72 - 50*s1)*t2 + 
               65*Power(t2,2) - 
               Power(t1,3)*(32 + 27*Power(s1,2) - 2*s1*(-11 + t2) + 
                  42*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-56 + 88*Power(s1,2) + 286*t2 + 
                  50*Power(t2,2) - 2*s1*(45 + 17*t2)) + 
               t1*(-62*Power(s1,2) + 2*s1*(75 + 44*t2) - 
                  5*(-21 + 73*t2 + 22*Power(t2,2))))))*T2q(s2,s))/
     (s*(-1 + s2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-1 + t1)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-1 + t2)) - 
    (8*(Power(s,7)*(3*Power(t1,2) - (-4 + s2)*t1*t2 + 
            (1 + s2)*Power(t2,2)) + 
         Power(s,6)*(-((-22 + s1 + s2)*Power(t1,3)) + 
            (2 + 2*s2*(2 + s1 - 5*t2) + 
               Power(s2,2)*(-3 + 2*s1 - 4*t2) - 9*t2)*t2 + 
            Power(t1,2)*(-18 - 3*s2 + Power(s2,2) + s1*(-8 + t2) + 
               27*t2 - 4*s2*t2) + 
            t1*(6 - 13*t2 + 5*Power(t2,2) + Power(s2,2)*(-1 + 4*t2) + 
               s2*(1 - 3*t2 + 5*Power(t2,2)) + 
               s1*(-2 + Power(s2,2) + s2*(4 + t2)))) - 
         Power(-1 + t1,3)*(2*(-1 + s1)*Power(t1,5) + 
            Power(s2,5)*(s1 - t2)*(-1 + s1 + t1 - t2) - 
            4*Power(-1 + t2,2) + t1*(-1 - 10*t2 + 11*Power(t2,2)) + 
            Power(t1,2)*(23 + 4*Power(s1,2) + 3*t2 - 10*Power(t2,2) - 
               4*s1*(4 + t2)) + 
            Power(t1,4)*(13 + 2*Power(s1,2) - s1*(13 + 2*t2)) - 
            Power(t1,3)*(29 + 7*Power(s1,2) + t2 - 2*Power(t2,2) - 
               s1*(27 + 8*t2)) + 
            Power(s2,4)*(1 + Power(s1,2)*(1 - 2*t1) + 3*t2 + 
               Power(t2,2) + Power(t1,2)*(1 + 2*t2) - 
               t1*(2 + 5*t2 + 2*Power(t2,2)) + 
               s1*(-3*Power(t1,2) - 2*(2 + t2) + t1*(7 + 4*t2))) - 
            Power(s2,3)*(-3 + Power(s1,2)*(1 + t1) - 7*t2 + 
               2*Power(t2,2) + Power(t1,3)*(2 + t2) - 
               Power(t1,2)*(7 + 5*t2 + Power(t2,2)) + 
               t1*(8 + 11*t2 + Power(t2,2)) + 
               s1*(9 - 3*Power(t1,3) - 2*t2 + Power(t1,2)*(11 + 2*t2) - 
                  t1*(17 + 4*t2))) - 
            s2*(-17 + Power(t1,4)*(-3 + 4*s1 + Power(s1,2) - 2*t2) + 
               22*t2 - 5*Power(t2,2) + 
               t1*(55 + 8*Power(s1,2) - 36*t2 + 13*Power(t2,2) - 
                  8*s1*(4 + t2)) + 
               Power(t1,2)*(-62 - 13*Power(s1,2) + 9*t2 - 
                  11*Power(t2,2) + 7*s1*(9 + 2*t2)) + 
               Power(t1,3)*(27 + 3*Power(s1,2) + 7*t2 + 2*Power(t2,2) - 
                  s1*(35 + 4*t2))) + 
            Power(s2,2)*(8 + Power(t1,4) + 
               Power(s1,2)*(4 - 5*t1 + Power(t1,2) + 2*Power(t1,3)) + 
               9*t2 - Power(t2,2) - 3*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(17 + 23*t2 - 2*Power(t2,2)) + 
               t1*(-20 - 29*t2 + 5*Power(t2,2)) - 
               s1*(-7*Power(t1,3) + Power(t1,4) + 4*(4 + t2) + 
                  Power(t1,2)*(35 + 4*t2) - t1*(45 + 4*t2)))) + 
         Power(s,5)*(1 + (57 - 4*s1)*Power(t1,4) - 16*t2 + 
            34*Power(t2,2) + Power(t1,3)*
             (-107 + Power(s1,2) + 4*s1*(-10 + t2) + 74*t2) + 
            t1*(-25 + 14*s1 + t2 - 43*Power(t2,2)) + 
            Power(t1,2)*(82 + Power(s1,2) + s1*(28 - 5*t2) - 76*t2 + 
               11*Power(t2,2)) + 
            Power(s2,3)*(Power(s1,2) - 2*Power(t1,2) + t1*(2 - 6*t2) + 
               t2*(7 + 6*t2) - s1*(1 + t1 + 6*t2)) - 
            s2*(-5 + 6*Power(t1,4) + (9 + 14*s1)*t2 - 36*Power(t2,2) + 
               Power(t1,3)*(27 - 3*s1 + 8*t2) + 
               Power(t1,2)*(-31 + Power(s1,2) + 26*t2 - 
                  10*Power(t2,2) - s1*(37 + 2*t2)) + 
               t1*(19 + 2*Power(s1,2) - 18*t2 + 42*Power(t2,2) - 
                  2*s1*(-9 + 5*t2))) + 
            Power(s2,2)*(-3 - Power(s1,2)*(-1 + t1) + 6*Power(t1,3) + 
               8*t2 + 24*Power(t2,2) + 2*Power(t1,2)*(-5 + 8*t2) + 
               t1*(7 - 29*t2 - 18*Power(t2,2)) + 
               s1*(4 + 2*Power(t1,2) - 13*t2 + t1*(-6 + 8*t2)))) + 
         s*Power(-1 + t1,2)*(2*Power(t1,6) + 
            Power(s2,5)*(-5 + 3*s1 + 5*t1 - 3*t2)*(s1 - t2) + 
            Power(t1,5)*(-6 - 20*s1 + Power(s1,2) + 8*t2) + 
            t1*(4 + 8*s1 + 35*t2 - 67*Power(t2,2)) + 
            8*(2 - 5*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(-88 + 64*s1 - 20*Power(s1,2) + 16*t2 + 
               27*s1*t2 + 56*Power(t2,2)) + 
            Power(t1,4)*(-26 - 12*Power(s1,2) - 32*t2 + 
               s1*(102 + 11*t2)) + 
            Power(t1,3)*(98 + 32*Power(s1,2) + 13*t2 - 12*Power(t2,2) - 
               2*s1*(77 + 20*t2)) + 
            Power(s2,4)*(1 + Power(s1,2)*(7 - 2*t1) + 3*Power(t1,3) + 
               14*t2 + 9*Power(t2,2) + Power(t1,2)*(-5 + 12*t2) + 
               t1*(1 - 26*t2 - 4*Power(t2,2)) - 
               2*s1*(5 + 4*Power(t1,2) + 8*t2 - 3*t1*(3 + t2))) - 
            Power(s2,3)*(5 + 6*Power(t1,4) + 
               Power(s1,2)*(11 + 4*t1 + 3*Power(t1,2)) + t2 + 
               7*Power(t2,2) + Power(t1,3)*(-7 + 10*t2) - 
               Power(t1,2)*(-1 + 31*t2 + Power(t2,2)) + 
               t1*(-5 + 20*t2 + 12*Power(t2,2)) - 
               s1*(3*Power(t1,3) + 21*(1 + t2) + 
                  Power(t1,2)*(3 + 5*t2) + t1*(-27 + 10*t2))) + 
            s2*(-55 + 2*(-6 + s1)*Power(t1,5) + (95 - 8*s1)*t2 - 
               12*Power(t2,2) + 
               Power(t1,4)*(28 - 19*t2 + s1*(42 + t2)) + 
               t1*(183 + 24*Power(s1,2) - 181*t2 + 38*Power(t2,2) - 
                  2*s1*(59 + 6*t2)) + 
               Power(t1,2)*(-197 - 43*Power(s1,2) + 49*t2 - 
                  35*Power(t2,2) + 3*s1*(97 + 13*t2)) + 
               Power(t1,3)*(53 + 18*Power(s1,2) + 56*t2 + 
                  8*Power(t2,2) - s1*(217 + 18*t2))) + 
            Power(s2,2)*(-14 + 3*Power(t1,5) + 
               Power(s1,2)*(-4 + 22*t1 - 9*Power(t1,2) + Power(t1,3)) - 
               13*t2 + 3*Power(t2,2) + Power(t1,4)*(8 + 3*t2) + 
               Power(t1,3)*(-26 + 5*t2) + 
               t1*(27 + 69*t2 + Power(t2,2)) + 
               Power(t1,2)*(2 - 64*t2 + 6*Power(t2,2)) + 
               s1*(30 - 2*Power(t1,4) + 9*t2 - 
                  2*Power(t1,3)*(19 + 3*t2) + 
                  3*Power(t1,2)*(48 + 7*t2) - 2*t1*(67 + 22*t2)))) + 
         Power(s,4)*(-7 + (71 - 6*s1)*Power(t1,5) + 52*t2 - 
            70*Power(t2,2) + Power(t1,2)*
             (-161 + Power(s1,2) + 2*s1*(-16 + t2) - 3*t2 - 
               86*Power(t2,2)) + 
            Power(t1,3)*(280 - 5*Power(s1,2) + s1*(173 - 10*t2) - 
               190*t2 + 13*Power(t2,2)) + 
            t1*(37 - 38*s1 + 22*t2 + 145*Power(t2,2)) + 
            2*Power(t1,4)*(-110 + 2*Power(s1,2) + 54*t2 + 
               s1*(-43 + 3*t2)) - 
            Power(s2,4)*(2*Power(s1,2) + t1 - Power(t1,2) + 
               s1*(-2 + t1 - 6*t2) - 4*t1*t2 + t2*(5 + 4*t2)) + 
            s2*(-11 - 12*Power(t1,5) + 
               Power(t1,4)*(-61 + 11*s1 - 10*t2) + (-26 + 38*s1)*t2 - 
               61*Power(t2,2) - 
               Power(t1,2)*(163 + 9*Power(s1,2) + s1*(125 - 39*t2) - 
                  16*t2 + 72*Power(t2,2)) + 
               t1*(84 + 14*Power(s1,2) + s1*(24 - 78*t2) + 63*t2 + 
                  124*Power(t2,2)) + 
               Power(t1,3)*(163 - 6*Power(s1,2) - 59*t2 + 
                  10*Power(t2,2) + s1*(106 + t2))) - 
            Power(s2,2)*(-1 - 19*Power(t1,4) + 
               Power(s1,2)*(7 - 11*t1 + 2*Power(t1,2)) + 
               Power(t1,3)*(10 - 33*t2) + 16*t2 + 52*Power(t2,2) + 
               t1*(3 - 83*t2 - 86*Power(t2,2)) + 
               Power(t1,2)*(7 + 84*t2 + 32*Power(t2,2)) + 
               s1*(6 + 7*Power(t1,3) - 32*t2 - 
                  2*Power(t1,2)*(-9 + 7*t2) + 5*t1*(-3 + 10*t2))) + 
            Power(s2,3)*(2 - 8*Power(t1,3) + Power(s1,2)*(-5 + 6*t1) + 
               Power(t1,2)*(15 - 23*t2) - 27*t2 - 25*Power(t2,2) + 
               3*t1*(-3 + 18*t2 + 8*Power(t2,2)) + 
               s1*(3 + Power(t1,2) + 25*t2 - t1*(8 + 25*t2)))) + 
         Power(s,2)*(-1 + t1)*
          (25 - (-15 + s1)*Power(t1,6) + 
            3*Power(s2,5)*(s1 - t2)*(-1 + s1 + t1 - t2) - 82*t2 + 
            61*Power(t2,2) + Power(t1,5)*
             (-71 + 4*Power(s1,2) + s1*(-65 + t2) + 41*t2) + 
            t1*(-3 + 32*s1 + 45*t2 - 174*Power(t2,2)) + 
            Power(t1,4)*(87 - 25*Power(s1,2) - 132*t2 + 2*Power(t2,2) + 
               s1*(273 + 17*t2)) + 
            Power(t1,2)*(-79 - 31*Power(s1,2) + 102*t2 + 
               153*Power(t2,2) + s1*(80 + 47*t2)) + 
            Power(t1,3)*(26 + 51*Power(s1,2) + 26*t2 - 43*Power(t2,2) - 
               s1*(319 + 63*t2)) - 
            s2*(58 + 3*Power(t1,6) + 4*(-39 + 8*s1)*t2 - 
               6*Power(t2,2) + Power(t1,5)*(43 - 9*s1 + 2*t2) + 
               Power(t1,2)*(75 + 26*Power(s1,2) + 34*s1*(-12 + t2) - 
                  217*t2 - 14*Power(t2,2)) + 
               t1*(-153 - 6*Power(s1,2) + s1*(120 - 71*t2) + 386*t2 + 
                  17*Power(t2,2)) + 
               Power(t1,3)*(126 - 24*Power(s1,2) - 65*t2 + 
                  7*Power(t2,2) + s1*(414 + t2)) + 
               Power(t1,4)*(-152 + 7*Power(s1,2) + 50*t2 - 
                  Power(t2,2) - s1*(117 + 2*t2))) + 
            Power(s2,4)*(7 + Power(s1,2)*(9 - 12*t1) + 5*Power(t1,3) + 
               24*t2 + 15*Power(t2,2) + Power(t1,2)*(-3 + 26*t2) - 
               t1*(9 + 50*t2 + 18*Power(t2,2)) - 
               2*s1*(8 + 9*Power(t1,2) + 12*t2 - t1*(17 + 15*t2))) + 
            Power(s2,2)*(21 + 16*Power(t1,5) + 
               Power(s1,2)*(17 - 15*t1 + 10*Power(t1,2) + 
                  8*Power(t1,3)) + 32*t2 + 29*Power(t2,2) + 
               Power(t1,4)*(18 + 19*t2) - 
               Power(t1,3)*(135 + 55*t2 + 8*Power(t2,2)) - 
               3*t1*(31 + 37*t2 + 19*Power(t2,2)) + 
               Power(t1,2)*(173 + 115*t2 + 56*Power(t2,2)) - 
               s1*(14 + 15*Power(t1,4) + t1*(13 - 20*t2) + 24*t2 + 
                  Power(t1,3)*(41 + 8*t2) + Power(t1,2)*(-83 + 28*t2))) \
+ Power(s2,3)*(13 - 18*Power(t1,4) + 
               Power(s1,2)*(6 - 26*t1 + 4*Power(t1,2)) + 
               Power(t1,3)*(13 - 40*t2) + 76*t2 + 23*Power(t2,2) + 
               Power(t1,2)*(41 + 130*t2 + 21*Power(t2,2)) - 
               t1*(49 + 166*t2 + 60*Power(t2,2)) + 
               s1*(-32 + 22*Power(t1,3) - 23*t2 - 
                  Power(t1,2)*(50 + 19*t2) + t1*(60 + 74*t2)))) + 
         Power(s,3)*(19 + (46 - 4*s1)*Power(t1,6) + 
            Power(s2,5)*(s1 - t2)*(-1 + s1 + t1 - t2) - 88*t2 + 
            85*Power(t2,2) + Power(t1,5)*
             (-205 + 6*Power(s1,2) + 4*s1*(-25 + t2) + 90*t2) + 
            t1*(-29 + 50*s1 + 32*t2 - 250*Power(t2,2)) + 
            Power(t1,4)*(355 - 23*Power(s1,2) - 252*t2 + 8*Power(t2,2) + 
               2*s1*(179 + t2)) + 
            Power(t1,3)*(-270 + 33*Power(s1,2) + 72*t2 - 
               87*Power(t2,2) - 2*s1*(166 + 15*t2)) + 
            Power(t1,2)*(84 - 17*Power(s1,2) + 146*t2 + 
               243*Power(t2,2) + s1*(28 + 26*t2)) + 
            Power(s2,4)*(1 + Power(s1,2)*(7 - 8*t1) + 3*Power(t1,3) + 
               18*t2 + 13*Power(t2,2) + Power(t1,2)*(-5 + 14*t2) + 
               t1*(1 - 32*t2 - 14*Power(t2,2)) - 
               2*s1*(5 + 3*Power(t1,2) + 10*t2 - t1*(8 + 11*t2))) + 
            s2*(-14 - 10*Power(t1,6) + 
               Power(t1,5)*(-65 + 15*s1 - 7*t2) + (114 - 50*s1)*t2 + 
               47*Power(t2,2) - 
               2*t1*(21 + 11*Power(s1,2) + s1*(22 - 79*t2) + 175*t2 + 
                  72*Power(t2,2)) + 
               Power(t1,2)*(239 + 24*Power(s1,2) + s1*(268 - 147*t2) + 
                  274*t2 + 143*Power(t2,2)) + 
               Power(t1,4)*(273 - 11*Power(s1,2) - 67*t2 + 
                  5*Power(t2,2) + s1*(147 + t2)) + 
               Power(t1,3)*(-381 + 6*Power(s1,2) + 36*t2 - 
                  54*Power(t2,2) + s1*(-386 + 44*t2))) + 
            Power(s2,2)*(25 + 27*Power(t1,5) + 
               Power(s1,2)*(19 - 45*t1 + 28*Power(t1,2) + 
                  4*Power(t1,3)) + 37*t2 + 55*Power(t2,2) + 
               Power(t1,4)*(-2 + 37*t2) - 
               Power(t1,3)*(127 + 128*t2 + 26*Power(t2,2)) + 
               Power(t1,2)*(177 + 260*t2 + 122*Power(t2,2)) - 
               t1*(100 + 206*t2 + 145*Power(t2,2)) - 
               2*s1*(8 + 10*Power(t1,4) + Power(t1,3)*(7 - 3*t2) + 
                  21*t2 + Power(t1,2)*(3 + 43*t2) - t1*(28 + 55*t2))) + 
            Power(s2,3)*(-3 - 18*Power(t1,4) + 
               2*Power(s1,2)*(4 - 11*t1 + 6*Power(t1,2)) + 
               Power(t1,3)*(25 - 46*t2) + 54*t2 + 37*Power(t2,2) + 
               Power(t1,2)*(1 + 144*t2 + 37*Power(t2,2)) - 
               t1*(5 + 152*t2 + 76*Power(t2,2)) + 
               s1*(-10 + 18*Power(t1,3) - 37*t2 - 
                  Power(t1,2)*(44 + 41*t2) + t1*(36 + 82*t2)))))*
       T3q(s2,1 - s + s2 - t1))/
     (s*(-1 + s2)*(-1 + t1)*Power(-1 + s + t1,2)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-1 + t2)) - 
    (8*(-(Power(s,6)*(3*Power(t1,2) - (-4 + s2)*t1*t2 + 
              (1 + s2)*Power(t2,2))) + 
         Power(s,5)*((-13 + s1 + s2)*Power(t1,3) - 
            Power(t1,2)*(-1 + Power(s2,2) + s1*(-8 + t2) + 21*t2 - 
               3*s2*(3 + t2)) + 
            t2*(-2 + 6*t2 + Power(s2,2)*(3 - 2*s1 + 6*t2) + 
               s2*(-4 - 2*s1 + 9*t2)) - 
            t1*(6 - t2 + 4*Power(t2,2) + Power(s2,2)*(-1 + 6*t2) + 
               s2*(1 - 14*t2 + 4*Power(t2,2)) + 
               s1*(-2 + Power(s2,2) + s2*(4 + t2)))) + 
         Power(s,4)*(-1 + (-20 + 3*s1)*Power(t1,4) + 10*t2 - 
            13*Power(t2,2) - Power(t1,2)*
             (30 - 2*s1 + Power(s1,2) - 12*t2 + 7*Power(t2,2)) + 
            t1*(7 - 8*s1 + 16*t2 + 25*Power(t2,2)) - 
            Power(t1,3)*(1 + Power(s1,2) + 41*t2 + s1*(-31 + 3*t2)) + 
            Power(s2,3)*(-Power(s1,2) + 4*Power(t1,2) + 
               s1*(1 + 3*t1 + 10*t2) + t1*(-4 + 15*t2) - t2*(13 + 15*t2)\
) + s2*(-5 + 3*Power(t1,4) + t2 + 8*s1*t2 - 24*Power(t2,2) + 
               Power(t1,3)*(25 - 5*s1 + 3*t2) + 
               Power(t1,2)*(9 + Power(s1,2) + s1*(-47 + t2) + 63*t2 - 
                  6*Power(t2,2)) + 
               t1*(12 + 2*Power(s1,2) + s1*(2 - 11*t2) + 3*t2 + 
                  29*Power(t2,2))) + 
            Power(s2,2)*(3 + Power(s1,2)*(-1 + t1) - 7*Power(t1,3) + 
               9*t2 - 27*Power(t2,2) - Power(t1,2)*(3 + 16*t2) + 
               t1*(-2 - 2*t2 + 20*Power(t2,2)) - 
               s1*(4 + Power(t1,2) - 11*t2 + t1*(-11 + 4*t2)))) - 
         Power(s2 - t1,2)*(2*(-1 + s1)*Power(t1,5) + 
            Power(s2,5)*(s1 - t2)*(-1 + s1 + t1 - t2) - 
            4*Power(-1 + t2,2) + t1*(-1 - 10*t2 + 11*Power(t2,2)) + 
            Power(t1,2)*(23 + 4*Power(s1,2) + 3*t2 - 10*Power(t2,2) - 
               4*s1*(4 + t2)) + 
            Power(t1,4)*(13 + 2*Power(s1,2) - s1*(13 + 2*t2)) - 
            Power(t1,3)*(29 + 7*Power(s1,2) + t2 - 2*Power(t2,2) - 
               s1*(27 + 8*t2)) + 
            Power(s2,4)*(1 + Power(s1,2)*(1 - 2*t1) + 3*t2 + 
               Power(t2,2) + Power(t1,2)*(1 + 2*t2) - 
               t1*(2 + 5*t2 + 2*Power(t2,2)) + 
               s1*(-3*Power(t1,2) - 2*(2 + t2) + t1*(7 + 4*t2))) - 
            Power(s2,3)*(-3 + Power(s1,2)*(1 + t1) - 7*t2 + 
               2*Power(t2,2) + Power(t1,3)*(2 + t2) - 
               Power(t1,2)*(7 + 5*t2 + Power(t2,2)) + 
               t1*(8 + 11*t2 + Power(t2,2)) + 
               s1*(9 - 3*Power(t1,3) - 2*t2 + Power(t1,2)*(11 + 2*t2) - 
                  t1*(17 + 4*t2))) - 
            s2*(-17 + Power(t1,4)*(-3 + 4*s1 + Power(s1,2) - 2*t2) + 
               22*t2 - 5*Power(t2,2) + 
               t1*(55 + 8*Power(s1,2) - 36*t2 + 13*Power(t2,2) - 
                  8*s1*(4 + t2)) + 
               Power(t1,2)*(-62 - 13*Power(s1,2) + 9*t2 - 
                  11*Power(t2,2) + 7*s1*(9 + 2*t2)) + 
               Power(t1,3)*(27 + 3*Power(s1,2) + 7*t2 + 2*Power(t2,2) - 
                  s1*(35 + 4*t2))) + 
            Power(s2,2)*(8 + Power(t1,4) + 
               Power(s1,2)*(4 - 5*t1 + Power(t1,2) + 2*Power(t1,3)) + 
               9*t2 - Power(t2,2) - 3*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(17 + 23*t2 - 2*Power(t2,2)) + 
               t1*(-20 - 29*t2 + 5*Power(t2,2)) - 
               s1*(-7*Power(t1,3) + Power(t1,4) + 4*(4 + t2) + 
                  Power(t1,2)*(35 + 4*t2) - t1*(45 + 4*t2)))) + 
         Power(s,3)*((-11 + 3*s1)*Power(t1,5) + 
            t1*(3 + 8*s1 + 9*t2 - 48*Power(t2,2)) + 
            Power(t1,3)*(-51 + s1 - 3*Power(s1,2) + 17*t2 - s1*t2 - 
               6*Power(t2,2)) + 4*(1 - 4*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(24 + 4*Power(s1,2) + s1*(-48 + t2) + 82*t2 + 
               38*Power(t2,2)) - 
            Power(t1,4)*(11 + 3*Power(s1,2) + 39*t2 + s1*(-46 + 3*t2)) + 
            Power(s2,4)*(4*Power(s1,2) - 6*Power(t1,2) + 
               t1*(6 - 20*t2) + 2*t2*(11 + 10*t2) - 
               2*s1*(2 + t1 + 10*t2)) + 
            Power(s2,3)*(-8 + Power(s1,2)*(4 - 7*t1) + 15*Power(t1,3) + 
               38*Power(t2,2) + 2*Power(t1,2)*(-7 + 17*t2) + 
               s1*(8 - 2*Power(t1,2) - 23*t2 + 26*t1*t2) - 
               5*t1*(-2 + 9*t2 + 8*Power(t2,2))) - 
            Power(s2,2)*(12*Power(t1,4) + 
               Power(s1,2)*(-4 + 11*t1 + Power(t1,2)) + 
               Power(t1,3)*(-2 + 15*t2) + 
               Power(t1,2)*(39 + 39*t2 - 24*Power(t2,2)) - 
               2*(5 + 8*t2 + 17*Power(t2,2)) + 
               t1*(-9 + 43*t2 + 66*Power(t2,2)) + 
               s1*(6 - 13*Power(t1,3) + t1*(5 - 43*t2) + 21*t2 + 
                  Power(t1,2)*(-68 + 9*t2))) + 
            s2*(-2 + 3*Power(t1,5) + (9 - 8*s1)*t2 + 29*Power(t2,2) + 
               Power(t1,4)*(17 - 12*s1 + t2) + 
               Power(t1,2)*(53 + 10*Power(s1,2) + s1*(4 - 27*t2) + 
                  24*t2 + 33*Power(t2,2)) + 
               Power(t1,3)*(44 + 7*Power(s1,2) + 97*t2 - 
                  4*Power(t2,2) + 2*s1*(-55 + 3*t2)) - 
               2*t1*(19 + 4*Power(s1,2) + 47*t2 + 36*Power(t2,2) - 
                  s1*(19 + 18*t2)))) + 
         s*(s2 - t1)*(-2*Power(t1,6) + 8*Power(-1 + t2,2) + 
            Power(t1,5)*(2 - 4*s1 + Power(s1,2) + 4*t2) - 
            Power(t1,3)*(46 - 86*s1 + 18*Power(s1,2) + 68*t2 - 
               15*s1*t2 + 2*Power(t2,2)) - 
            4*t1*(-7 + 4*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(-25 + 8*Power(s1,2) + 81*t2 + 4*Power(t2,2) - 
               6*s1*(7 + t2)) + 
            Power(t1,4)*(35 + 5*Power(s1,2) + 4*t2 - s1*(29 + 3*t2)) + 
            Power(s2,5)*(4*Power(s1,2) + t1 - Power(t1,2) + 
               s1*(-4 + 3*t1 - 10*t2) - 6*t1*t2 + t2*(7 + 6*t2)) - 
            Power(s2,4)*(-3*Power(t1,3) + Power(s1,2)*(-4 + 9*t1) + 
               Power(t1,2)*(2 - 13*t2) - 3*t2*(4 + 3*t2) + 
               t1*(1 + 30*t2 + 14*Power(t2,2)) + 
               s1*(8 + 11*Power(t1,2) + 11*t2 - 3*t1*(8 + 7*t2))) + 
            Power(s2,3)*(2 - 3*Power(t1,4) + 
               Power(s1,2)*(2 - 9*t1 + 2*Power(t1,2)) + 
               Power(t1,3)*(3 - 8*t2) + 30*t2 + 
               Power(t1,2)*(3 + 26*t2 + 10*Power(t2,2)) - 
               t1*(5 + 56*t2 + 17*Power(t2,2)) + 
               s1*(-24 + 15*Power(t1,3) - t2 - 
                  Power(t1,2)*(30 + 13*t2) + t1*(47 + 26*t2))) + 
            Power(s2,2)*(36 + Power(t1,5) + 
               Power(s1,2)*(8 - 22*t1 + 11*Power(t1,2) + 
                  8*Power(t1,3)) + Power(t1,4)*(-6 + t2) + 19*t2 + 
               5*Power(t2,2) + 
               Power(t1,3)*(11 + 2*t2 - 2*Power(t2,2)) - 
               4*t1*(22 + 22*t2 + Power(t2,2)) + 
               Power(t1,2)*(46 + 82*t2 + 4*Power(t2,2)) - 
               s1*(34 + 9*Power(t1,4) + 14*t2 - 3*Power(t1,3)*t2 + 
                  Power(t1,2)*(91 + 30*t2) - t1*(118 + 33*t2))) + 
            s2*(2*(3 + s1)*Power(t1,5) - 
               Power(t1,4)*(15 + 6*Power(s1,2) + s1*(-14 + t2) + 9*t2) - 
               4*(11 - 12*t2 + Power(t2,2)) - 
               Power(t1,3)*(84 + 11*Power(s1,2) + 26*t2 + 
                  4*Power(t2,2) - 9*s1*(9 + 2*t2)) - 
               t1*(11 + 16*Power(s1,2) + 100*t2 + 9*Power(t2,2) - 
                  4*s1*(19 + 5*t2)) + 
               Power(t1,2)*(148 + 38*Power(s1,2) + 94*t2 + 
                  22*Power(t2,2) - s1*(180 + 47*t2)))) - 
         Power(s,2)*(-((1 + s1)*Power(t1,6)) + 4*Power(-1 + t2,2) + 
            Power(t1,5)*(13 + 3*Power(s1,2) + s1*(-29 + t2) + 19*t2) + 
            Power(t1,4)*(49 - 17*s1 + 5*Power(s1,2) - 2*t2 + 
               2*Power(t2,2)) - 7*t1*(1 - 6*t2 + 5*Power(t2,2)) + 
            Power(t1,2)*(-44 + 4*Power(s1,2) + 71*t2 + 49*Power(t2,2) - 
               2*s1*(17 + t2)) - 
            Power(t1,3)*(34 + 15*Power(s1,2) + 131*t2 + 23*Power(t2,2) - 
               3*s1*(33 + 2*t2)) + 
            Power(s2,5)*(6*Power(s1,2) - 4*Power(t1,2) + 
               t1*(4 - 15*t2) + 2*s1*(-3 + t1 - 10*t2) + 3*t2*(6 + 5*t2)) \
+ Power(s2,4)*(-6 + Power(s1,2)*(6 - 15*t1) + 13*Power(t1,3) + 14*t2 + 
               27*Power(t2,2) + 3*Power(t1,2)*(-5 + 12*t2) + 
               t1*(8 - 67*t2 - 40*Power(t2,2)) + 
               s1*(-12*Power(t1,2) - 23*t2 + t1*(26 + 44*t2))) - 
            s2*(9 - (7 + 9*s1)*Power(t1,5) + Power(t1,6) + 10*t2 - 
               19*Power(t2,2) + 
               Power(t1,4)*(58 + 12*Power(s1,2) + 63*t2 - Power(t2,2) + 
                  s1*(-93 + 5*t2)) + 
               t1*(-51 + 8*Power(s1,2) + 148*t2 + 55*Power(t2,2) - 
                  4*s1*(13 + 5*t2)) + 
               Power(t1,3)*(107 + 17*Power(s1,2) + 40*t2 + 
                  15*Power(t2,2) - s1*(47 + 31*t2)) - 
               Power(t1,2)*(121 + 37*Power(s1,2) + 250*t2 + 
                  65*Power(t2,2) - s1*(187 + 60*t2))) + 
            Power(s2,3)*(4 - 15*Power(t1,4) + 
               Power(s1,2)*(7 - 19*t1 + 6*Power(t1,2)) + 
               Power(t1,3)*(23 - 27*t2) + 38*t2 + 18*Power(t2,2) + 
               t1*(20 - 90*t2 - 64*Power(t2,2)) + 
               Power(t1,2)*(-36 + 43*t2 + 36*Power(t2,2)) + 
               s1*(-21 + 25*Power(t1,3) + Power(t1,2)*(1 - 31*t2) - 
                  16*t2 + t1*(29 + 61*t2))) + 
            Power(s2,2)*(9 + 7*Power(t1,5) + 
               Power(s1,2)*(4 - 29*t1 + 25*Power(t1,2) + 
                  12*Power(t1,3)) + 6*Power(t1,4)*(-3 + t2) + 45*t2 + 
               22*Power(t2,2) + 
               Power(t1,3)*(73 + 50*t2 - 12*Power(t2,2)) + 
               2*Power(t1,2)*(20 + 63*t2 + 23*Power(t2,2)) - 
               t1*(91 + 157*t2 + 60*Power(t2,2)) - 
               s1*(23*Power(t1,4) + Power(t1,3)*(85 - 11*t2) + 
                  18*(1 + t2) + Power(t1,2)*(59 + 69*t2) - 
                  t1*(109 + 70*t2)))))*T4q(1 - s + s2 - t1))/
     (s*(-1 + s2)*(-s + s2 - t1)*(-1 + t1)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(-1 + t2)) - 
    (8*(-(Power(s,5)*(3*Power(t1,2) - (-4 + s2)*t1*t2 + 
              (1 + s2)*Power(t2,2))) + 
         2*Power(s2 - t1,2)*Power(-1 + t1,2)*
          (-7 + 6*t1 + Power(t1,2) + 4*t2 - Power(s2,2)*t2 - 12*t1*t2 + 
            3*Power(t2,2) + 3*s2*
             (1 + t1*(-1 + t2) + 2*t2 - Power(t2,2)) + 
            s1*(4 + Power(s2,2) - Power(t1,2) + 3*s2*(-3 + t2) - 4*t2 + 
               t1*(5 + t2))) + 
         Power(s,3)*(-1 + (-1 + s1)*Power(t1,4) + 10*t2 - 
            13*Power(t2,2) - Power(t1,3)*
             (11 + Power(s1,2) + s1*(-15 + t2) + 17*t2) - 
            Power(t1,2)*(10 + 2*s1 + Power(s1,2) + 6*t2 - 
               2*Power(t2,2)) + t1*(7 - 8*s1 + 20*t2 + 13*Power(t2,2)) + 
            Power(s2,3)*(-Power(s1,2) + 2*Power(t1,2) + t1*(-2 + 6*t2) - 
               t2*(7 + 6*t2) + s1*(1 + t1 + 6*t2)) + 
            s2*(-5 + Power(t1,3) + Power(t1,4) + 
               Power(s1,2)*t1*(2 + t1) - 3*t2 - 12*Power(t2,2) + 
               s1*(-3*Power(t1,3) + t1*(6 - 7*t2) + 
                  Power(t1,2)*(-23 + t2) + 8*t2) + t1*t2*(13 + 9*t2) + 
               Power(t1,2)*(17 + 11*t2 - Power(t2,2))) + 
            Power(s2,2)*(3 + Power(s1,2)*(-1 + t1) - 3*Power(t1,3) - 
               4*Power(t1,2)*(-1 + t2) + t2 - 12*Power(t2,2) + 
               s1*(-4 + Power(t1,2) + t1*(3 - 2*t2) + 7*t2) + 
               t1*(-4 + 8*t2 + 6*Power(t2,2)))) + 
         Power(s,4)*((-7 + s1 + s2)*Power(t1,3) + 
            Power(t1,2)*(1 - Power(s2,2) - s1*(-8 + t2) - 13*t2 + 
               s2*(3 + t2)) + 
            t2*(-2 + 6*t2 + Power(s2,2)*(3 - 2*s1 + 4*t2) + 
               s2*(-4 - 2*s1 + 7*t2)) - 
            t1*(6 - t2 + 2*Power(t2,2) + Power(s2,2)*(-1 + 4*t2) + 
               s2*(1 - 6*t2 + 2*Power(t2,2)) + 
               s1*(-2 + Power(s2,2) + s2*(4 + t2)))) + 
         Power(s,2)*(6*Power(t1,5) + 
            t1*(5 + 8*s1 - 11*t2 - 22*Power(t2,2)) - 
            Power(t1,3)*(-21 + Power(s1,2) + s1*(-15 + t2) + 32*t2 - 
               6*Power(t2,2)) + 4*(1 - 4*t2 + 3*Power(t2,2)) - 
            Power(t1,4)*(-6*s1 + Power(s1,2) + 10*(3 + t2)) + 
            Power(s2,4)*(2*Power(s1,2) + t1 - Power(t1,2) + 
               s1*(-2 + t1 - 6*t2) - 4*t1*t2 + t2*(5 + 4*t2)) + 
            Power(t1,2)*(-6 + 4*Power(s1,2) + 64*t2 + 10*Power(t2,2) - 
               s1*(24 + 7*t2)) + 
            Power(s2,3)*(-2 + Power(s1,2)*(2 - 3*t1) + 2*Power(t1,3) + 
               6*t2 + 7*Power(t2,2) + Power(t1,2)*(-3 + 5*t2) - 
               3*t1*(-1 + 5*t2 + 2*Power(t2,2)) + 
               s1*(-4*Power(t1,2) - 7*t2 + t1*(8 + 7*t2))) + 
            s2*(-4 - 2*(2 + s1)*Power(t1,4) + (29 - 8*s1)*t2 + 
               3*Power(t2,2) + 
               Power(t1,3)*(43 + 3*Power(s1,2) + s1*(-14 + t2) + 5*t2) + 
               Power(t1,2)*(-37 + 4*Power(s1,2) + 72*t2 - 
                  10*Power(t2,2) - s1*(28 + 3*t2)) - 
               2*t1*(-1 + 4*Power(s1,2) + 42*t2 + Power(t2,2) - 
                  s1*(11 + 10*t2))) - 
            Power(s2,2)*(Power(t1,4) + 
               Power(s1,2)*(-4 + 5*t1 + Power(t1,2)) + Power(t1,3)*t2 - 
               4*t2*(3 + t2) + Power(t1,2)*(16 - 15*t2 - 2*Power(t2,2)) + 
               t1*(-17 + 40*t2 + 8*Power(t2,2)) + 
               s1*(6 - 5*Power(t1,3) + 2*Power(t1,2)*(-1 + t2) + 5*t2 - 
                  t1*(13 + 11*t2)))) - 
         s*(6*(2 + s1)*Power(t1,5) - 2*Power(t1,6) + 
            Power(s2,5)*(s1 - t2)*(-1 + s1 + t1 - t2) + 
            4*Power(-1 + t2,2) + t1*(1 + 10*t2 - 11*Power(t2,2)) + 
            Power(t1,4)*(-39 + 2*Power(s1,2) + 56*t2 - 2*Power(t2,2) - 
               s1*(39 + 4*t2)) + 
            Power(t1,3)*(59 - 7*Power(s1,2) - 93*t2 - 12*Power(t2,2) + 
               s1*(35 + 24*t2)) + 
            Power(t1,2)*(4*Power(s1,2) - 2*s1*(1 + 9*t2) + 
               5*(-7 + 7*t2 + 4*Power(t2,2))) + 
            Power(s2,4)*(1 + Power(s1,2)*(1 - 2*t1) + 3*t2 + 
               Power(t2,2) + Power(t1,2)*(1 + 2*t2) - 
               t1*(2 + 5*t2 + 2*Power(t2,2)) + 
               s1*(-3*Power(t1,2) - 2*(2 + t2) + t1*(7 + 4*t2))) - 
            Power(s2,3)*(5 + Power(s1,2)*(1 + t1) - 17*t2 + 
               2*Power(t2,2) + Power(t1,3)*(4 + t2) - 
               Power(t1,2)*(3 + 11*t2 + Power(t2,2)) + 
               t1*(-6 + 27*t2 + Power(t2,2)) + 
               s1*(9 - 3*Power(t1,3) - 2*t2 + Power(t1,2)*(7 + 2*t2) - 
                  t1*(13 + 4*t2))) + 
            Power(s2,2)*(8 + 3*Power(t1,4) + 
               Power(s1,2)*(4 - 5*t1 + Power(t1,2) + 2*Power(t1,3)) + 
               13*t2 + Power(t1,3)*t2 - Power(t2,2) + 
               Power(t1,2)*(-1 + 59*t2 - 14*Power(t2,2)) + 
               t1*(-10 - 73*t2 + 17*Power(t2,2)) - 
               s1*(-3*Power(t1,3) + Power(t1,4) + 
                  Power(t1,2)*(69 - 10*t2) + 2*(9 + t2) + t1*(-85 + 12*t2)\
)) + s2*(-17 + 2*Power(t1,5) + 22*t2 - 5*Power(t2,2) - 
               Power(t1,4)*(13 + 8*s1 + Power(s1,2) + 8*t2) + 
               Power(t1,3)*(37 - 3*Power(s1,2) + s1*(99 - 8*t2) - 99*t2 + 
                  20*Power(t2,2)) + 
               t1*(51 - 8*Power(s1,2) - 96*t2 + 5*Power(t2,2) + 
                  20*s1*(1 + t2)) + 
               Power(t1,2)*(-60 + 13*Power(s1,2) + 181*t2 - 
                  19*Power(t2,2) - s1*(111 + 14*t2)))))*T5q(s))/
     (s*(-1 + s2)*(-1 + t1)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (-1 + t2)));
   return a;
};
