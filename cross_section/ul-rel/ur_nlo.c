#include <stdio.h>
#include <math.h>

#define Pi       3.141592653589793238462643383279502884


double ur_born(double *);
long double ur_bubl(double *);
long double ur_tri(double *);
long double ur_box(double *);
long double ur_pent(double *);

long double ur_ir(double *);

long double ur_lble(double *);
long double ur_lblm(double *);


double ur_nlo1_log(double *);

double ur_col_nlog(double *);




double ur_nlo1(double *vars){
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4], omega=vars[5], mf=vars[6];;
    double a=0., pp=0., bb=0., tt=0., bu=0., ir=0., ll=0.;
    
    
    
    bu = ur_bubl(vars);
    tt = ur_tri(vars);
    bb = ur_box(vars);
    pp = ur_pent(vars);
    
    ir = ur_ir(vars);
    ll = ur_nlo1_log(vars);
    
    
    
  
    
    
    // because nlo=2*Re(Mnlo*(Mborn)^*)/4
    a=(tt + bu + pp + bb + ir)/2. + (ll)/4.;// 1/2 from 2*(1/4),where 2 from interference 2*Mborn*Mcorr, 1/4 from polarization of photons
    
    if( isnanl(a)!=0 )
    { 
        printf("UR nlo1 function is NAN:\n");
        printf("pent=%e box=%e  tri=%e  buble=%e  ir=%e  ur_nlo1_log=%e\n",pp,bb,tt,bu,ir,ll);
    };
    
 
  return a;
  
};

double ur_nlo1_m0(double *vars){
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4], omega=vars[5], theta=vars[6];
    double a=0., pp=0., bb=0., tt=0., bu=0., ir=0., col=0.;
    
    
    
    bu = ur_bubl(vars);
    tt = ur_tri(vars);
    bb = ur_box(vars);
    pp = ur_pent(vars);
    
    ir = ur_ir(vars);
    col = ur_col_nlog(vars);
    
    
  
    
    
    // because nlo=2*Re(Mnlo*(Mborn)^*)/4
    a=(tt + bu + pp + bb + ir)/2. + (col)/4.;// 1/2 from 2*(1/4),where 2 from interference 2*Mborn*Mcorr, 1/4 from polarization of photons
    
    if( isnanl(a)!=0 )
    { 
        printf("UR nlo1 function is NAN:\n");
        printf("pent=%e box=%e  tri=%e  buble=%e  ir=%e col=%e\n",pp,bb,tt,bu,ir,col);
    };
    
 
  return a;
  
};





double ur_nlo1_log(double *vars)
{
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4], omega=vars[5], mf=vars[6];
    double a=0., omega=vars[5], mf=vars[6];
    double s=vars[0], s1=vars[1], s2=vars[2];
    double aG=1./(16.*Pi*Pi);
    
    a = -8.*aG*(2.*log(mf))*(log(2.*omega/sqrt(s-s1-s2))+3./4.)*ur_born(vars);
    
    //if (isnan(a)) printf("ur_nlo1_log: log(mf)=%e, log(2.*omega/sqrt(s-s1-s2))=%e\n",log(mf),log(2.*omega/sqrt(s-s1-s2)));
    
    
    
    
    return a;
};


double ur_col_nlog(double *vars)
{
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4], omega=vars[5], theta=vars[6];
    double a=0., omega=vars[5], theta=vars[6];
    double s=vars[0], s1=vars[1], s2=vars[2];
    double aG=1./pow(4.*Pi,2);
    double eps=2.*omega/sqrt(s-s1-s2);
    double tg2=pow(tan(theta),2);
    
    a = 8.*aG*( 1. + log(eps) - log((s-s1-s2)*tg2/4.)*( log(eps)+3./4. ) )*ur_born(vars);
    
    if (isnan(a)) printf("ur_nlo1_log: born=%f\n",ur_born(vars));
    
    
    return a;
};



double ur_nlo2(double *vars){
    //double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4], =vars[5], mf=vars[6];;
    double a=0., pp=0., bb=0., tt=0., bu=0., ir=0., ll=0.;
    
    
    
    bu = ur_bubl(vars);
    tt = ur_tri(vars);
    bb = ur_box(vars);
    pp = ur_pent(vars);
    
    ir = ur_ir(vars);
    ll = ur_nlo1_log(vars);
    
    
  
    
    
    // because nlo=2*Re(Mnlo*(Mborn)^*)/4
    a=(tt + bu + pp + bb + ir)/2. + (ll)/4.;// 1/2 from 2*(1/4),where 2 from interference 2*Mborn*Mcorr, 1/4 from polarization of photons
    
    if( isnanl(a)!=0 )
    { 
        printf("UR nlo1 function is NAN:\n");
        printf("pent=%e box=%e  tri=%e  buble=%e  ir=%e\n",pp,bb,tt,bu,ir);
    };
    
 
  return a;
  
};