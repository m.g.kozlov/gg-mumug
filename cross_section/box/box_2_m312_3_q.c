#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m312_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(-16 + 2*Power(s,5) + 5*s2 + 22*Power(s2,2) + 2*Power(s2,3) + 
         2*Power(s2,4) + 27*t1 - 38*s2*t1 - 32*Power(s2,2)*t1 - 
         8*Power(s2,3)*t1 - 2*Power(s2,4)*t1 + 47*s2*Power(t1,2) + 
         16*Power(s2,2)*Power(t1,2) + 6*Power(s2,3)*Power(t1,2) - 
         17*Power(t1,3) - 16*s2*Power(t1,3) - 6*Power(s2,2)*Power(t1,3) + 
         6*Power(t1,4) + 2*s2*Power(t1,4) + 
         2*Power(s1,2)*(8 + Power(s2,3) - 14*t1 + 7*Power(t1,2) - 
            Power(t1,3) - Power(s2,2)*(1 + 3*t1) + 
            3*s2*(2 - 2*t1 + Power(t1,2))) - 
         Power(s,4)*(14 + 2*s1 - 2*s2 + t1 - 9*t2) + 20*t2 + 45*s2*t2 + 
         23*Power(s2,2)*t2 - 3*Power(s2,3)*t2 - 69*t1*t2 - 94*s2*t1*t2 - 
         13*Power(s2,2)*t1*t2 + 3*Power(s2,3)*t1*t2 + 75*Power(t1,2)*t2 + 
         38*s2*Power(t1,2)*t2 - 10*Power(s2,2)*Power(t1,2)*t2 - 
         22*Power(t1,3)*t2 + 11*s2*Power(t1,3)*t2 - 4*Power(t1,4)*t2 + 
         16*Power(t2,2) + 6*s2*Power(t2,2) - 8*Power(s2,2)*Power(t2,2) + 
         2*Power(s2,3)*Power(t2,2) - 22*t1*Power(t2,2) + 
         6*s2*t1*Power(t2,2) + 2*Power(t1,2)*Power(t2,2) - 
         6*s2*Power(t1,2)*Power(t2,2) + 4*Power(t1,3)*Power(t2,2) + 
         Power(s,3)*(26 - 12*Power(s2,2) - 26*t1 - 13*Power(t1,2) + 
            s1*(17 - 3*s2 + 3*t1 - 6*t2) + 13*s2*(2 + 2*t1 - t2) - 
            30*t2 + 19*t1*t2 + 6*Power(t2,2)) + 
         Power(s,2)*(-45 + 10*Power(s2,3) + 2*Power(s1,2)*(6 + s2 - t1) + 
            50*t1 - 6*Power(t1,2) - 15*Power(t1,3) + 38*t2 - 78*t1*t2 + 
            7*Power(t1,2)*t2 + 16*t1*Power(t2,2) - 
            Power(s2,2)*(2 + 39*t1 + t2) + 
            s2*(-22 + 9*t1 + 44*Power(t1,2) + 71*t2 - 7*t1*t2 - 
               10*Power(t2,2)) + 
            s1*(-33 + 12*Power(s2,2) + 51*t1 + 17*Power(t1,2) - 12*t2 - 
               14*t1*t2 + s2*(-48 - 29*t1 + 8*t2))) + 
         s1*(Power(s2,3)*(5 - 5*t1 - 4*t2) - 
            s2*(46 + 33*Power(t1,2) + 15*Power(t1,3) + 18*t2 - 
               2*t1*(47 + 3*t2)) + 
            Power(s2,2)*(-22 + 15*Power(t1,2) + 10*t2 + t1*(7 + 6*t2)) + 
            (-1 + t1)*(5*Power(t1,3) - 2*Power(t1,2)*(-13 + t2) + 
               4*(5 + 8*t2) - 2*t1*(25 + 9*t2))) + 
         s*(47 - 2*Power(s2,4) - 61*t1 + 7*Power(t1,2) + 12*Power(t1,3) - 
            5*Power(t1,4) - 2*Power(s1,2)*
             (14 + 2*Power(s2,2) + s2*(5 - 4*t1) - 13*t1 + 2*Power(t1,2)) \
- 37*t2 + 113*t1*t2 - 70*Power(t1,2)*t2 - 7*Power(t1,3)*t2 - 
            22*Power(t2,2) + 2*t1*Power(t2,2) + 
            14*Power(t1,2)*Power(t2,2) + 
            Power(s2,3)*(-12 + 16*t1 + 5*t2) - 
            Power(s2,2)*(18 + 31*Power(t1,2) + 38*t2 - 2*Power(t2,2) + 
               t1*(-31 + 15*t2)) + 
            s2*(-1 + 22*Power(t1,3) - 89*t2 + 8*Power(t2,2) + 
               Power(t1,2)*(-31 + 17*t2) + 
               t1*(26 + 105*t2 - 16*Power(t2,2))) + 
            s1*(38 - 7*Power(s2,3) + 17*Power(t1,3) + 
               Power(t1,2)*(55 - 10*t2) + 50*t2 + 
               Power(s2,2)*(26 + 31*t1 + 2*t2) - t1*(109 + 28*t2) + 
               s2*(83 - 41*Power(t1,2) + 2*t2 + t1*(-77 + 8*t2))))))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*(-1 + s + t1)*
       (-1 + s1 + t1 - t2)) + (8*
       (24 + 8*s2 + 4*Power(s1,3)*(-1 + s2)*s2 + 26*Power(s2,2) + 
         3*Power(s2,3) - Power(s2,4) - 36*s2*t1 - 20*Power(s2,2)*t1 - 
         Power(s2,3)*t1 + Power(s2,4)*t1 - 14*Power(t1,2) + 
         15*s2*Power(t1,2) - 7*Power(s2,2)*Power(t1,2) - 
         2*Power(s2,3)*Power(t1,2) - 6*Power(t1,3) + 13*s2*Power(t1,3) + 
         Power(s2,2)*Power(t1,3) - 4*Power(t1,4) - 28*t2 + 23*s2*t2 - 
         3*Power(s2,2)*t2 + 23*Power(s2,3)*t2 + 2*Power(s2,4)*t2 - 
         Power(s2,5)*t2 + 21*t1*t2 + 48*s2*t1*t2 - 15*Power(s2,2)*t1*t2 + 
         8*Power(s2,3)*t1*t2 + 2*Power(s2,4)*t1*t2 - Power(t1,2)*t2 - 
         10*s2*Power(t1,2)*t2 - 12*Power(s2,2)*Power(t1,2)*t2 - 
         Power(s2,3)*Power(t1,2)*t2 + 6*Power(t1,3)*t2 + 
         2*s2*Power(t1,3)*t2 + 4*Power(t2,2) - 31*s2*Power(t2,2) + 
         2*Power(s2,2)*Power(t2,2) - 11*Power(s2,3)*Power(t2,2) - 
         5*t1*Power(t2,2) - 4*s2*t1*Power(t2,2) + 
         19*Power(s2,2)*t1*Power(t2,2) - 2*Power(s2,3)*t1*Power(t2,2) - 
         2*Power(t1,2)*Power(t2,2) - 4*s2*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,2) + 4*s2*Power(t2,3) - 
         6*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) + 
         2*s2*t1*Power(t2,3) - 2*Power(s2,2)*t1*Power(t2,3) + 
         Power(s,3)*(-8 - 7*t1 - 2*s2*(t1 - t2) + 
            Power(s2,2)*(8 + t1 - t2) + 7*t2) + 
         Power(s1,2)*(4 + 2*Power(s2,4) - 4*t1 - 3*Power(t1,2) + 
            Power(s2,2)*(-9 + 2*Power(t1,2) - 2*t1*(-8 + t2) - 14*t2) + 
            Power(s2,3)*(-9 - 4*t1 + 2*t2) + 
            s2*(-3*Power(t1,2) + 2*t1*(4 + t2) + 4*(-8 + 3*t2))) + 
         Power(s,2)*(7 - 17*t1 - 15*Power(t1,2) - 11*t2 + 14*t1*t2 + 
            Power(t2,2) - Power(s2,3)*(14 + t2) + 
            Power(s2,2)*(-17 + 10*t1 + 8*t2 + 4*t1*t2 - 4*Power(t2,2)) - 
            s2*(-32 - 15*t1 + Power(t1,2) + 12*t2 + 2*t1*t2 - 
               3*Power(t2,2)) + 
            s1*(12 + 3*Power(s2,3) + 6*t1 + s2*(-9 + 5*t1 - 3*t2) - t2 + 
               Power(s2,2)*(-14 - 3*t1 + 4*t2))) + 
         s1*(28 + Power(s2,5) - 8*t2 - Power(s2,4)*(3 + t1 + 2*t2) + 
            Power(t1,2)*(3 + 5*t2) + t1*(-29 + 9*t2) - 
            Power(s2,3)*(t1 + Power(t1,2) - 6*t1*t2 + 
               4*(7 - 5*t2 + Power(t2,2))) + 
            Power(s2,2)*(1 + Power(t1,3) + Power(t1,2)*(5 - 4*t2) + 
               7*t2 + 16*Power(t2,2) + t1*(23 - 35*t2 + 4*Power(t2,2))) \
- s2*(Power(t1,3) - Power(t1,2)*(1 + 7*t2) + 
               4*t1*(12 + t2 + Power(t2,2)) + 
               3*(5 - 21*t2 + 4*Power(t2,2)))) - 
         s*(28 + 11*t1 + 15*Power(t1,2) + 12*Power(t1,3) - 29*t2 + 
            12*t1*t2 - 13*Power(t1,2)*t2 + 5*Power(t2,2) + 
            t1*Power(t2,2) + Power(s2,4)*(t1 - 3*(2 + t2)) + 
            Power(s1,2)*(4 + 2*Power(s2,3) + 3*t1 + 
               s2*(-17 + 3*t1 - 2*t2) + Power(s2,2)*(-1 - 2*t1 + 2*t2)) + 
            Power(s2,3)*(-22 - 2*Power(t1,2) + 16*t2 - 4*Power(t2,2) + 
               t1*(3 + 8*t2)) + 
            s2*(9 - Power(t1,3) + 14*t2 - 5*Power(t2,2) - 
               2*Power(t2,3) + Power(t1,2)*(-29 + 2*t2) + 
               t1*(-78 + 28*t2 + Power(t2,2))) + 
            Power(s2,2)*(27 + Power(t1,3) + Power(t1,2)*(4 - 5*t2) + 
               10*t2 + 2*Power(t2,3) + t1*(27 - 16*t2 + 2*Power(t2,2))) + 
            s1*(37 + 4*Power(s2,4) - 15*t1 - 6*Power(t1,2) - 9*t2 - 
               4*t1*t2 + Power(s2,3)*(-21 - 6*t1 + 2*t2) + 
               Power(s2,2)*(-29 + 27*t1 + 2*Power(t1,2) + t2 - 
                  4*Power(t2,2)) + 
               s2*(-15 + 2*t1 - 4*Power(t1,2) + 22*t2 - 4*t1*t2 + 
                  4*Power(t2,2)))))*B1(1 - s1 - t1 + t2,s2,1 - s + s2 - t1)\
)/((-1 + s2)*Power(-s + s2 - t1,2)*(1 - s + s*s2 - s1*s2 - t1 + s2*t2)) + 
    (32*(-(Power(-1 + t1,2)*(-6 + t1 + Power(t1,2))) + 
         Power(s2,2)*(-3 + 2*t1 + s1*(4 + 2*t1 + Power(t1,2)) + 
            Power(t1,2)*(1 - 2*t2) - 5*t2) + Power(s2,4)*(s1 - t2) + 
         Power(s2,3)*(2 - s1*(3 + 2*t1) + 2*t1*(-1 + t2) + 3*t2) + 
         Power(s,3)*(-2 + s2 + Power(s2,2) - t1 + s2*t2) + 
         s2*(-3 + s1*(-4 + 4*t1 - 3*Power(t1,2)) + t1*(8 - 3*t2) + 
            Power(t1,2)*(-5 + t2) + 4*t2 + Power(t1,3)*t2) + 
         s*(-13 + 16*t1 - 3*Power(t1,3) + Power(s2,3)*(4 - s1 + t2) + 
            Power(s2,2)*(-5 + s1*(1 + t1) + t2 - 3*t1*(1 + t2)) + 
            s2*(10 + s1*(4 - 5*t1) + t1*(-8 + t2) - 3*t2 + 
               Power(t1,2)*(2 + 3*t2))) - 
         Power(s,2)*(Power(s2,3) + 3*(-3 + t1 + Power(t1,2)) + 
            Power(s2,2)*(3 - t1 + t2) + s2*(2*s1 - 3*(-1 + t1 + t1*t2))))*
       R1q(s2))/(Power(-1 + s2,2)*Power(-s + s2 - t1,2)*Power(-1 + s + t1,2)) \
- (16*(32 + Power(s,7) + 66*s2 + 25*Power(s2,2) - 21*Power(s2,3) - 
         9*Power(s2,4) + 3*Power(s2,5) - 
         Power(s,6)*(12 + 2*s1 + 7*s2 - 3*t1) - 154*t1 - 248*s2*t1 - 
         46*Power(s2,2)*t1 + 63*Power(s2,3)*t1 + 9*Power(s2,4)*t1 - 
         2*Power(s2,5)*t1 + 295*Power(t1,2) + 337*s2*Power(t1,2) - 
         2*Power(s2,2)*Power(t1,2) - 54*Power(s2,3)*Power(t1,2) - 
         3*Power(s2,4)*Power(t1,2) - Power(s2,5)*Power(t1,2) - 
         278*Power(t1,3) - 183*s2*Power(t1,3) + 
         39*Power(s2,2)*Power(t1,3) + 15*Power(s2,3)*Power(t1,3) + 
         3*Power(s2,4)*Power(t1,3) + 123*Power(t1,4) + 
         17*s2*Power(t1,4) - 17*Power(s2,2)*Power(t1,4) - 
         3*Power(s2,3)*Power(t1,4) - 14*Power(t1,5) + 11*s2*Power(t1,5) + 
         Power(s2,2)*Power(t1,5) - 4*Power(t1,6) + 
         Power(s1,3)*(2*Power(s2,4) - Power(s2,3)*(1 + 7*t1) + 
            Power(-1 + t1,2)*(4 - 2*t1 + Power(t1,2)) + 
            Power(s2,2)*(-1 - 2*t1 + 9*Power(t1,2)) + 
            s2*(6 - 8*t1 + 7*Power(t1,2) - 5*Power(t1,3))) + 32*t2 + 
         79*s2*t2 + 38*Power(s2,2)*t2 - 21*Power(s2,3)*t2 - 
         5*Power(s2,4)*t2 + 5*Power(s2,5)*t2 - 2*Power(s2,6)*t2 - 
         119*t1*t2 - 217*s2*t1*t2 - 65*Power(s2,2)*t1*t2 + 
         13*Power(s2,3)*t1*t2 - 8*Power(s2,4)*t1*t2 + 
         7*Power(s2,5)*t1*t2 + 163*Power(t1,2)*t2 + 
         206*s2*Power(t1,2)*t2 + 57*Power(s2,2)*Power(t1,2)*t2 + 
         17*Power(s2,3)*Power(t1,2)*t2 - 11*Power(s2,4)*Power(t1,2)*t2 - 
         96*Power(t1,3)*t2 - 84*s2*Power(t1,3)*t2 - 
         29*Power(s2,2)*Power(t1,3)*t2 + 11*Power(s2,3)*Power(t1,3)*t2 + 
         19*Power(t1,4)*t2 + 14*s2*Power(t1,4)*t2 - 
         7*Power(s2,2)*Power(t1,4)*t2 + Power(t1,5)*t2 + 
         2*s2*Power(t1,5)*t2 - 4*Power(t2,2) + 
         7*Power(s2,2)*Power(t2,2) + 2*Power(s2,3)*Power(t2,2) + 
         2*Power(s2,4)*Power(t2,2) + 4*Power(s2,5)*Power(t2,2) + 
         24*t1*Power(t2,2) + 13*s2*t1*Power(t2,2) - 
         25*Power(s2,2)*t1*Power(t2,2) - 26*Power(s2,3)*t1*Power(t2,2) - 
         14*Power(s2,4)*t1*Power(t2,2) - 40*Power(t1,2)*Power(t2,2) + 
         2*s2*Power(t1,2)*Power(t2,2) + 
         47*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         20*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         21*Power(t1,3)*Power(t2,2) - 25*s2*Power(t1,3)*Power(t2,2) - 
         17*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         2*Power(t1,4)*Power(t2,2) + 10*s2*Power(t1,4)*Power(t2,2) - 
         3*Power(t1,5)*Power(t2,2) - 4*Power(t2,3) - 5*s2*Power(t2,3) + 
         2*Power(s2,2)*Power(t2,3) + Power(s2,3)*Power(t2,3) - 
         2*Power(s2,4)*Power(t2,3) + 9*t1*Power(t2,3) + 
         4*s2*t1*Power(t2,3) + 7*Power(s2,3)*t1*Power(t2,3) - 
         6*Power(t1,2)*Power(t2,3) - 2*s2*Power(t1,2)*Power(t2,3) - 
         8*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         Power(t1,3)*Power(t2,3) + 3*s2*Power(t1,3)*Power(t2,3) + 
         Power(s,5)*(12 + Power(s1,2) + 18*Power(s2,2) - 40*t1 + 
            3*Power(t1,2) + s1*(26 + 10*s2 - 5*t1 - t2) - 20*t2 - 
            6*t1*t2 - s2*(-54 + 19*t1 + t2)) + 
         Power(s,4)*(52 - 22*Power(s2,3) - 23*t1 - 56*Power(t1,2) + 
            Power(t1,3) + Power(s2,2)*(-100 + 41*t1) + 91*t2 - 
            56*t1*t2 - 21*Power(t1,2)*t2 - 8*Power(t2,2) - 
            6*t1*Power(t2,2) + Power(t2,3) + 
            Power(s1,2)*(-12 - 2*s2 + t1 + t2) + 
            s1*(-95 - 15*Power(s2,2) + 82*t1 - 3*Power(t1,2) + 
               2*s2*(-49 + 9*t1) + 20*t2 + 5*t1*t2 - 2*Power(t2,2)) + 
            s2*(-29 + 142*t1 - 20*Power(t1,2) + 72*t2 + 23*t1*t2 + 
               2*Power(t2,2))) + 
         Power(s1,2)*(4*Power(s2,5) - 2*Power(s2,4)*(-1 + 7*t1 + 3*t2) + 
            Power(s2,2)*(-10*Power(t1,3) + Power(t1,2)*(30 - 26*t2) + 
               4*t1*(-3 + t2) + 4*(1 + t2)) + 
            s2*(-1 + t1)*(1 + 2*Power(t1,3) + 17*t2 + 
               13*Power(t1,2)*t2 - 3*t1*(7 + t2)) - 
            Power(-1 + t1,2)*
             (4 + 12*t2 + 2*Power(t1,2)*(4 + t2) - t1*(17 + 5*t2)) + 
            Power(s2,3)*(18*Power(t1,2) + 3*t2 + t1*(-22 + 21*t2))) + 
         s1*(2*Power(s2,6) - Power(s2,5)*(5 + 7*t1 + 8*t2) + 
            2*Power(s2,4)*(1 + 4*Power(t1,2) - 2*t2 + 3*Power(t2,2) + 
               7*t1*(1 + 2*t2)) + 
            s2*(-1 + t1)*(77 + Power(t1,4) - t2 - 16*Power(t2,2) - 
               3*Power(t1,3)*(7 + 4*t2) + 2*t1*(-66 + 17*t2) + 
               Power(t1,2)*(66 + 15*t2 - 11*Power(t2,2))) - 
            Power(s2,2)*(37 + 2*Power(t1,4) + 11*t2 + 5*Power(t2,2) - 
               27*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(79 + 77*t2 - 25*Power(t2,2)) + 
               t1*(-70 - 37*t2 + 2*Power(t2,2))) + 
            Power(-1 + t1,2)*
             (Power(t1,3)*(-2 + 3*t2) + 
               t1*(53 - 33*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-18 + 12*t2 + Power(t2,2)) + 
               4*(-8 + 2*t2 + 3*Power(t2,2))) - 
            Power(s2,3)*(-17 + 2*Power(t1,3) + 2*t2 + 3*Power(t2,2) + 
               Power(t1,2)*(39 + 38*t2) + 
               t1*(-4 - 48*t2 + 21*Power(t2,2)))) + 
         Power(s,3)*(-155 + 13*Power(s2,4) + 337*t1 - 144*Power(t1,2) - 
            50*Power(t1,3) + Power(s1,3)*(-2 - s2 + t1) - 185*t2 + 
            276*t1*t2 - 53*Power(t1,2)*t2 - 27*Power(t1,3)*t2 + 
            20*Power(t2,2) - 16*t1*Power(t2,2) - 
            21*Power(t1,2)*Power(t2,2) - Power(t2,3) + 
            3*t1*Power(t2,3) + Power(s2,3)*(96 - 39*t1 + 8*t2) + 
            Power(s1,2)*(26 - 5*Power(s2,2) - 3*Power(t1,2) + 
               t1*(-38 + t2) + 3*t2 + s2*(31 + 8*t1 + t2)) + 
            Power(s2,2)*(22 + 39*Power(t1,2) - 103*t2 - 
               10*Power(t2,2) - t1*(196 + 45*t2)) + 
            s2*(-133 - 12*Power(t1,3) - 210*t2 + 18*Power(t2,2) - 
               Power(t2,3) + 5*Power(t1,2)*(32 + 13*t2) + 
               2*t1*(31 + 69*t2 + 14*Power(t2,2))) + 
            s1*(181 + 3*Power(s2,3) + Power(t1,3) - 46*t2 + 
               Power(s2,2)*(143 - 10*t1 + 15*t2) + 
               Power(t1,2)*(94 + 24*t2) + 
               t1*(-286 + 54*t2 - 5*Power(t2,2)) + 
               s2*(232 + 7*Power(t1,2) - 49*t2 + Power(t2,2) - 
                  t1*(229 + 36*t2)))) + 
         Power(s,2)*(192 - 3*Power(s2,5) - 614*t1 + 643*Power(t1,2) - 
            186*Power(t1,3) - 36*Power(t1,4) + 
            Power(s1,3)*(8 + 4*Power(s2,2) + s2*(2 - 7*t1) - 8*t1 + 
               3*Power(t1,2)) + 2*Power(s2,4)*(-24 + 8*t1 - 7*t2) + 
            209*t2 - 479*t1*t2 + 301*Power(t1,2)*t2 - 
            17*Power(t1,3)*t2 - 15*Power(t1,4)*t2 - 20*Power(t2,2) + 
            58*t1*Power(t2,2) - 6*Power(t1,2)*Power(t2,2) - 
            27*Power(t1,3)*Power(t2,2) - 5*Power(t2,3) - 
            t1*Power(t2,3) + 3*Power(t1,2)*Power(t2,3) + 
            Power(s2,3)*(-2 - 31*Power(t1,2) + 75*t2 + 18*Power(t2,2) + 
               4*t1*(31 + 13*t2)) + 
            Power(s2,2)*(101 + 23*Power(t1,3) + 150*t2 - 
               10*Power(t2,2) - 3*Power(t2,3) - 
               2*Power(t1,2)*(83 + 41*t2) - 
               t1*(33 + 121*t2 + 52*Power(t2,2))) + 
            s2*(238 - 5*Power(t1,4) + 287*t2 - 25*Power(t2,2) + 
               3*Power(t2,3) + Power(t1,3)*(116 + 59*t2) + 
               Power(t1,2)*(193 + 89*t2 + 60*Power(t2,2)) + 
               t1*(-522 - 438*t2 - 4*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*(16*Power(s2,3) - 5*Power(t1,3) - 
               3*Power(t1,2)*(16 + t2) - 3*(8 + 7*t2) - 
               Power(s2,2)*(23 + 35*t1 + 11*t2) + t1*(82 + 15*t2) + 
               s2*(-40 + 24*Power(t1,2) - t2 + 15*t1*(3 + t2))) + 
            s1*(-203 + 11*Power(s2,4) + Power(t1,4) + 44*t2 + 
               18*Power(t2,2) + 4*Power(t1,3)*(11 + 8*t2) - 
               Power(s2,3)*(101 + 21*t1 + 34*t2) + 
               t1*(463 - 140*t2 - 6*Power(t2,2)) + 
               Power(t1,2)*(-304 + 54*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(-184 + 8*Power(t1,2) + 33*t2 + 
                  10*Power(t2,2) + 3*t1*(76 + 29*t2)) + 
               s2*(-287 + Power(t1,3) + 65*t2 - 4*Power(t2,2) - 
                  3*Power(t1,2)*(67 + 28*t2) + 
                  t1*(489 - 41*t2 - 9*Power(t2,2))))) + 
         s*(Power(s1,3)*(-10 - 5*Power(s2,3) + 17*t1 - 10*Power(t1,2) + 
               3*Power(t1,3) + Power(s2,2)*(1 + 13*t1) + 
               s2*(-7 + 9*t1 - 11*Power(t1,2))) + 
            Power(s2,5)*(10 - 2*t1 + 9*t2) + 
            Power(s2,4)*(-6 + 10*Power(t1,2) - 29*t2 - 14*Power(t2,2) - 
               t1*(28 + 31*t2)) + 
            Power(s2,3)*(-11 - 15*Power(t1,3) - 26*t2 - 2*Power(t2,2) + 
               5*Power(t2,3) + Power(t1,2)*(65 + 49*t2) + 
               t1*(-15 + 47*t2 + 44*Power(t2,2))) + 
            Power(s2,2)*(-66 + 8*Power(t1,4) - 85*t2 + 3*Power(t2,2) - 
               3*Power(t2,3) - 4*Power(t1,3)*(21 + 11*t2) + 
               Power(t1,2)*(8 - 56*t2 - 59*Power(t2,2)) + 
               t1*(126 + 157*t2 + 46*Power(t2,2) - 11*Power(t2,3))) - 
            (-1 + t1)*(-122 - 127*t2 + 12*Power(t2,2) + 9*Power(t2,3) + 
               3*Power(t1,4)*(6 + t2) + 
               Power(t1,3)*(109 + 2*t2 + 15*Power(t2,2)) + 
               t1*(369 + 257*t2 - 48*Power(t2,2) - 2*Power(t2,3)) - 
               Power(t1,2)*(372 + 133*t2 - 11*Power(t2,2) + Power(t2,3))) \
+ s2*(-189 - Power(t1,5) - 227*t2 + 5*Power(t2,2) + 3*Power(t2,3) + 
               5*Power(t1,4)*(11 + 4*t2) + 
               Power(t1,3)*(120 + 37*t2 + 44*Power(t2,2)) + 
               t1*(585 + 494*t2 - 20*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-570 - 315*t2 - 47*Power(t2,2) + 
                  5*Power(t2,3))) + 
            Power(s1,2)*(-14*Power(s2,4) + 
               Power(s2,2)*(14 - 40*Power(t1,2) + t1*(16 - 37*t2) - 
                  5*t2) + Power(s2,3)*(2 + 40*t1 + 15*t2) - 
               (-1 + t1)*(13 + 2*Power(t1,3) + 29*t2 + 
                  Power(t1,2)*(32 + 5*t2) - t1*(57 + 16*t2)) + 
               s2*(12 + 16*Power(t1,3) + 17*t2 + 
                  3*Power(t1,2)*(4 + 9*t2) - t1*(58 + 17*t2))) + 
            s1*(-9*Power(s2,5) + Power(s2,4)*(35 + 25*t1 + 28*t2) - 
               Power(s2,3)*(20*Power(t1,2) + t1*(95 + 84*t2) + 
                  15*(-3 + Power(t2,2))) + 
               (-1 + t1)*(-125 + 25*t2 + 28*Power(t2,2) + 
                  Power(t1,3)*(4 + 17*t2) + 
                  t1*(246 - 105*t2 - 11*Power(t2,2)) + 
                  Power(t1,2)*(-123 + 43*t2 + Power(t2,2))) + 
               s2*(220 + 3*Power(t1,4) - 17*t2 - 13*Power(t2,2) - 
                  4*Power(t1,3)*(23 + 15*t2) + 
                  Power(t1,2)*(347 + 35*t2 - 21*Power(t2,2)) + 
                  t1*(-487 + 78*t2 + 7*Power(t2,2))) + 
               Power(s2,2)*(93 + Power(t1,3) - 17*t2 + 7*Power(t2,2) + 
                  Power(t1,2)*(148 + 99*t2) + 
                  t1*(-214 - 62*t2 + 35*Power(t2,2))))))*
       R1q(1 - s + s2 - t1))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(1 - s + s2 - t1)*
       Power(-1 + s + t1,2)*(-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + 
         Power(s1,2) - 2*s2 - 2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 
         2*t2 + 2*s*t2 - 2*s1*t2 - 2*s2*t2 + Power(t2,2))) - 
    (16*(64 + Power(s,5) + 25*s2 - 38*Power(s2,2) + 7*Power(s2,3) - 
         3*Power(s2,4) + Power(s2,5) - 149*t1 - 11*s2*t1 + 
         21*Power(s2,2)*t1 + 8*Power(s2,3)*t1 - Power(s2,4)*t1 + 
         101*Power(t1,2) + 4*s2*Power(t1,2) - 
         12*Power(s2,2)*Power(t1,2) - Power(s2,3)*Power(t1,2) - 
         20*Power(t1,3) + 7*s2*Power(t1,3) + Power(s2,2)*Power(t1,3) - 
         4*Power(t1,4) + Power(s1,3)*
          (3*Power(s2,2) - 2*s2*(1 + 3*t1) + 3*(4 - 2*t1 + Power(t1,2))) \
- Power(s,4)*(3 + 2*s1 + s2 + t1 - 5*t2) + 68*t2 + 21*s2*t2 - 
         36*Power(s2,2)*t2 + 13*Power(s2,3)*t2 - 5*Power(s2,4)*t2 - 
         77*t1*t2 - 3*s2*t1*t2 - 18*Power(s2,2)*t1*t2 + 
         11*Power(s2,3)*t1*t2 + 11*Power(t1,2)*t2 + 
         20*s2*Power(t1,2)*t2 - 8*Power(s2,2)*Power(t1,2)*t2 + 
         Power(t1,3)*t2 + 2*s2*Power(t1,3)*t2 - 8*Power(t2,2) + 
         7*s2*Power(t2,2) - 8*Power(s2,2)*Power(t2,2) + 
         2*Power(s2,3)*Power(t2,2) + 21*t1*Power(t2,2) - 
         13*s2*t1*Power(t2,2) - 5*Power(s2,2)*t1*Power(t2,2) + 
         Power(t1,2)*Power(t2,2) + 6*s2*Power(t1,2)*Power(t2,2) - 
         3*Power(t1,3)*Power(t2,2) - 12*Power(t2,3) + 11*s2*Power(t2,3) + 
         2*Power(s2,2)*Power(t2,3) - 3*t1*Power(t2,3) - 
         5*s2*t1*Power(t2,3) + 3*Power(t1,2)*Power(t2,3) + 
         Power(s,3)*(-1 + Power(s1,2) - 4*Power(s2,2) - 11*t1 - 
            2*Power(t1,2) + s1*(10 - 4*s2 + 5*t1 - 11*t2) + 
            s2*(20 + 6*t1 - 6*t2) - 3*t2 - 2*t1*t2 + 10*Power(t2,2)) + 
         Power(s1,2)*(8*Power(s2,3) - Power(t1,2)*(14 + 3*t2) - 
            Power(s2,2)*(11 + 16*t1 + 4*t2) - 4*(2 + 9*t2) + 
            t1*(31 + 9*t2) + s2*
             (-3 + 8*Power(t1,2) + 15*t2 + t1*(5 + 7*t2))) + 
         Power(s,2)*(8*Power(s2,3) - 2*Power(s2,2)*(17 + 5*t1 + 4*t2) + 
            Power(s1,2)*(-1 + 8*s2 - 7*t1 + 6*t2) + 
            s1*(-25 + 20*Power(s2,2) + 28*t1 + 7*Power(t1,2) + 5*t2 - 
               t1*t2 - 12*Power(t2,2) + s2*(-41 - 25*t1 + 8*t2)) + 
            s2*(11 + 3*Power(t1,2) + 23*t2 - 16*Power(t2,2) + 
               t1*(42 + 19*t2)) - 
            2*(-7 - 8*t2 + 2*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(8 + 5*t2) + t1*(9 + 2*t2 - 4*Power(t2,2)))) + 
         s1*(6*Power(s2,4) + t1*(76 - 52*t2) + 3*Power(t1,3)*t2 - 
            Power(s2,3)*(17 + 11*t1 + 10*t2) + 
            Power(t1,2)*(-10 + 13*t2 - 3*Power(t2,2)) + 
            4*(-17 + 4*t2 + 9*Power(t2,2)) + 
            Power(s2,2)*(34 + 4*Power(t1,2) + 19*t2 - Power(t2,2) + 
               3*t1*(10 + 7*t2)) + 
            s2*(Power(t1,3) + 4*t1*Power(1 + t2,2) - 
               Power(t1,2)*(29 + 14*t2) - 4*(5 + t2 + 6*Power(t2,2)))) - 
         s*(81 + 5*Power(s2,4) + 3*Power(s1,3)*(2 + s2 - t1) - 123*t1 + 
            49*Power(t1,2) + 12*Power(t1,3) + 
            Power(s1,2)*(5 + 17*Power(s2,2) + 8*Power(t1,2) + 
               s2*(-25*t1 + 2*(-8 + t2)) - 3*t1*(-5 + t2) - 9*t2) + 
            93*t2 - 63*t1*t2 + 3*Power(t1,3)*t2 + 15*Power(t2,2) + 
            3*t1*Power(t2,2) + 5*Power(t1,2)*Power(t2,2) + 
            3*Power(t2,3) - 9*t1*Power(t2,3) - 
            2*Power(s2,3)*(10 + 3*t1 + 7*t2) + 
            Power(s2,2)*(17 + 33*t2 - 4*Power(t2,2) + t1*(39 + 28*t2)) + 
            s2*(Power(t1,3) - 18*Power(t1,2)*(2 + t2) + 
               t1*(-7 - 18*t2 + Power(t2,2)) + 
               4*(-3 - 3*t2 - 4*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(20*Power(s2,3) - 4*(23 + 5*t2) - 
               Power(t1,2)*(18 + 13*t2) - 
               Power(s2,2)*(48 + 31*t1 + 13*t2) + 
               t1*(71 - 18*t2 + 15*Power(t2,2)) + 
               s2*(1 + 11*Power(t1,2) + 32*t2 - 13*Power(t2,2) + 
                  6*t1*(9 + 4*t2)))))*R2q(1 - s1 - t1 + t2))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(-24 - 30*s2 - 4*Power(s1,6)*s2 - 8*Power(s2,2) - 17*Power(s2,3) + 
         9*Power(s2,4) + 3*Power(s2,5) - Power(s2,6) + 70*t1 + 62*s2*t1 + 
         58*Power(s2,2)*t1 + 3*Power(s2,3)*t1 - 21*Power(s2,4)*t1 - 
         Power(s2,5)*t1 + Power(s2,6)*t1 - 38*Power(t1,2) - 
         29*s2*Power(t1,2) - 45*Power(s2,2)*Power(t1,2) + 
         31*Power(s2,3)*Power(t1,2) + 11*Power(s2,4)*Power(t1,2) - 
         2*Power(s2,5)*Power(t1,2) - 60*Power(t1,3) - 39*s2*Power(t1,3) - 
         21*Power(s2,2)*Power(t1,3) - 17*Power(s2,3)*Power(t1,3) + 
         Power(s2,4)*Power(t1,3) + 80*Power(t1,4) + 44*s2*Power(t1,4) + 
         16*Power(s2,2)*Power(t1,4) - 28*Power(t1,5) - 8*s2*Power(t1,5) - 
         Power(s,6)*(3 + s2)*(t1 - t2) - 28*t2 - 41*s2*t2 - 
         59*Power(s2,2)*t2 - 41*Power(s2,3)*t2 - Power(s2,4)*t2 + 
         13*Power(s2,5)*t2 + 2*Power(s2,6)*t2 - Power(s2,7)*t2 - 
         11*t1*t2 + 22*s2*t1*t2 + 150*Power(s2,2)*t1*t2 + 
         49*Power(s2,3)*t1*t2 - 39*Power(s2,4)*t1*t2 - 
         13*Power(s2,5)*t1*t2 + 2*Power(s2,6)*t1*t2 + 
         173*Power(t1,2)*t2 + 67*s2*Power(t1,2)*t2 - 
         49*Power(s2,2)*Power(t1,2)*t2 + 40*Power(s2,3)*Power(t1,2)*t2 + 
         26*Power(s2,4)*Power(t1,2)*t2 - Power(s2,5)*Power(t1,2)*t2 - 
         184*Power(t1,3)*t2 - 87*s2*Power(t1,3)*t2 - 
         62*Power(s2,2)*Power(t1,3)*t2 - 27*Power(s2,3)*Power(t1,3)*t2 + 
         44*Power(t1,4)*t2 + 36*s2*Power(t1,4)*t2 + 
         12*Power(s2,2)*Power(t1,4)*t2 + 8*Power(t1,5)*t2 + 
         24*Power(t2,2) + 10*s2*Power(t2,2) - 
         38*Power(s2,2)*Power(t2,2) - 39*Power(s2,3)*Power(t2,2) - 
         18*Power(s2,4)*Power(t2,2) + 8*Power(s2,5)*Power(t2,2) + 
         3*Power(s2,6)*Power(t2,2) - 142*t1*Power(t2,2) - 
         127*s2*t1*Power(t2,2) - 9*Power(s2,2)*t1*Power(t2,2) + 
         74*Power(s2,3)*t1*Power(t2,2) - 16*Power(s2,4)*t1*Power(t2,2) - 
         10*Power(s2,5)*t1*Power(t2,2) + 165*Power(t1,2)*Power(t2,2) + 
         174*s2*Power(t1,2)*Power(t2,2) + 
         9*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         15*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         11*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         22*Power(t1,3)*Power(t2,2) - 29*s2*Power(t1,3)*Power(t2,2) + 
         5*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         4*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         28*Power(t1,4)*Power(t2,2) - 16*s2*Power(t1,4)*Power(t2,2) + 
         32*Power(t2,3) + 36*s2*Power(t2,3) + 
         32*Power(s2,2)*Power(t2,3) - 13*Power(s2,3)*Power(t2,3) - 
         11*Power(s2,4)*Power(t2,3) + Power(s2,5)*Power(t2,3) - 
         36*t1*Power(t2,3) - 117*s2*t1*Power(t2,3) - 
         60*Power(s2,2)*t1*Power(t2,3) + 22*Power(s2,3)*t1*Power(t2,3) + 
         4*Power(s2,4)*t1*Power(t2,3) - 27*Power(t1,2)*Power(t2,3) + 
         49*s2*Power(t1,2)*Power(t2,3) - 
         30*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         13*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         24*Power(t1,3)*Power(t2,3) + 31*s2*Power(t1,3)*Power(t2,3) + 
         8*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         4*Power(t1,4)*Power(t2,3) + 24*s2*Power(t2,4) + 
         16*Power(s2,2)*Power(t2,4) + 2*Power(s2,3)*Power(t2,4) - 
         5*Power(s2,4)*Power(t2,4) + 24*t1*Power(t2,4) - 
         13*s2*t1*Power(t2,4) + 3*Power(s2,2)*t1*Power(t2,4) + 
         12*Power(s2,3)*t1*Power(t2,4) - 11*Power(t1,2)*Power(t2,4) - 
         23*s2*Power(t1,2)*Power(t2,4) - 
         3*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         6*Power(t1,3)*Power(t2,4) - 4*s2*Power(t1,3)*Power(t2,4) - 
         4*Power(t2,5) + 5*s2*Power(t2,5) - Power(s2,2)*Power(t2,5) - 
         t1*Power(t2,5) + 15*s2*t1*Power(t2,5) - 
         6*Power(s2,2)*t1*Power(t2,5) + 2*Power(t1,2)*Power(t2,5) + 
         6*s2*Power(t1,2)*Power(t2,5) - 4*s2*Power(t2,6) + 
         2*Power(s2,2)*Power(t2,6) - 2*s2*t1*Power(t2,6) + 
         Power(s1,5)*(4 - 4*t1 + 3*Power(t1,2) + 
            Power(s2,2)*(5 - 2*t2) + 2*s2*(t1*(-12 + t2) + 12*t2)) - 
         Power(s,5)*(5 + 6*t1 + 3*Power(t1,2) + 2*t2 + 7*t1*t2 - 
            10*Power(t2,2) + Power(s2,2)*(-2 - 3*t1 + 2*t2) + 
            s1*(-8 + s2 + 3*Power(s2,2) - 15*t1 - 4*s2*t1 + 10*t2 + 
               5*s2*t2) + s2*(1 + 2*Power(t1,2) + 3*t1*(-4 + t2) + 
               9*t2 - 5*Power(t2,2))) + 
         Power(s1,4)*(3*Power(s2,4) + Power(s2,3)*(23 - 7*t1) - 20*t2 + 
            3*t1*(7 + 5*t2) - 2*Power(t1,2)*(7 + 5*t2) + 
            Power(s2,2)*(39 + 5*Power(t1,2) - 21*t2 + 10*Power(t2,2) - 
               2*t1*(16 + 3*t2)) - 
            s2*(-27 + Power(t1,3) + Power(t1,2)*(15 - 6*t2) - 5*t2 + 
               60*Power(t2,2) + t1*(33 - 111*t2 + 10*Power(t2,2)))) + 
         Power(s1,3)*(7*Power(s2,5) + 
            Power(s2,3)*(-17 + 9*Power(t1,2) + 9*t1*(-2 + t2) - 71*t2) + 
            Power(s2,4)*(24 - 15*t1 - 4*t2) + Power(t1,3)*(-8 + 6*t2) + 
            t1*(63 - 87*t2 - 20*Power(t2,2)) + 8*(-4 + 5*Power(t2,2)) + 
            Power(t1,2)*(-20 + 53*t2 + 10*Power(t2,2)) - 
            Power(s2,2)*(86 + Power(t1,3) + 133*t2 - 34*Power(t2,2) + 
               20*Power(t2,3) + 6*Power(t1,2)*(1 + 2*t2) - 
               3*t1*(55 + 31*t2 + 8*Power(t2,2))) + 
            s2*(-63 - 105*t2 - 20*Power(t2,2) + 80*Power(t2,3) + 
               Power(t1,3)*(-16 + 7*t2) - 
               4*Power(t1,2)*(35 - 17*t2 + 6*Power(t2,2)) + 
               2*t1*(109 + 56*t2 - 102*Power(t2,2) + 10*Power(t2,3)))) + 
         Power(s1,2)*(24 + 5*Power(s2,6) + 
            Power(s2,5)*(3 - 9*t1 - 13*t2) + 4*Power(t1,4)*(-4 + t2) + 
            96*t2 - 40*Power(t2,3) + 
            Power(t1,2)*(232 + 13*t2 - 75*Power(t2,2)) - 
            2*Power(t1,3)*(38 - 20*t2 + 9*Power(t2,2)) + 
            t1*(-167 - 162*t2 + 135*Power(t2,2) + 10*Power(t2,3)) + 
            Power(s2,4)*(-51 + 3*Power(t1,2) - 59*t2 - 6*Power(t2,2) + 
               t1*(21 + 34*t2)) + 
            Power(s2,3)*(-41 + Power(t1,3) + 21*t2 + 75*Power(t2,2) - 
               Power(t1,2)*(29 + 31*t2) + 
               t1*(131 + 58*t2 + 15*Power(t2,2))) + 
            Power(s2,2)*(3 + 204*t2 + 165*Power(t2,2) - 
               26*Power(t2,3) + 20*Power(t2,4) + 
               Power(t1,3)*(9 + 10*t2) + 
               2*Power(t1,2)*(8 - 9*t2 + 3*Power(t2,2)) - 
               3*t1*(25 + 130*t2 + 29*Power(t2,2) + 12*Power(t2,3))) + 
            s2*(35 - 8*Power(t1,4) + 162*t2 + 153*Power(t2,2) + 
               30*Power(t2,3) - 60*Power(t2,4) - 
               3*Power(t1,3)*(24 - 21*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(296 + 329*t2 - 114*Power(t2,2) + 
                  36*Power(t2,3)) - 
               t1*(235 + 553*t2 + 138*Power(t2,2) - 186*Power(t2,3) + 
                  20*Power(t2,4)))) + 
         s1*(Power(s2,7) - 4*Power(t1,5) - Power(s2,6)*(3 + t1 + 8*t2) + 
            Power(t1,4)*(-60 + 44*t2 - 8*Power(t2,2)) + 
            t1*(17 + 309*t2 + 135*Power(t2,2) - 93*Power(t2,3)) + 
            2*Power(t1,3)*(106 + 49*t2 - 28*Power(t2,2) + 
               9*Power(t2,3)) + 
            Power(t1,2)*(-195 - 397*t2 + 34*Power(t2,2) + 
               47*Power(t2,3) - 5*Power(t2,4)) + 
            4*(7 - 12*t2 - 24*Power(t2,2) + 5*Power(t2,4)) - 
            Power(s2,5)*(14 + Power(t1,2) + 11*t2 - 5*Power(t2,2) - 
               t1*(16 + 19*t2)) + 
            Power(s2,4)*(14 + Power(t1,3) + 69*t2 + 46*Power(t2,2) + 
               12*Power(t2,3) - Power(t1,2)*(19 + 14*t2) + 
               t1*(18 - 5*t2 - 23*Power(t2,2))) + 
            Power(s2,3)*(52 + 80*t2 + 9*Power(t2,2) - 29*Power(t2,3) + 
               Power(t1,3)*(10 + 3*t2) + 
               Power(t1,2)*(11 + 14*t2 + 35*Power(t2,2)) - 
               t1*(94 + 205*t2 + 62*Power(t2,2) + 29*Power(t2,3))) + 
            Power(s2,2)*(47 - 4*Power(t1,4) + 35*t2 - 150*Power(t2,2) - 
               87*Power(t2,3) + 9*Power(t2,4) - 10*Power(t2,5) + 
               Power(t1,3)*(33 - 14*t2 - 17*Power(t2,2)) + 
               Power(t1,2)*(72 - 25*t2 + 54*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-140 + 84*t2 + 285*Power(t2,2) + 23*Power(t2,3) + 
                  24*Power(t2,4))) + 
            s2*(35 - 45*t2 - 135*Power(t2,2) - 99*Power(t2,3) - 
               20*Power(t2,4) + 24*Power(t2,5) + 
               8*Power(t1,4)*(-5 + 3*t2) + 
               Power(t1,3)*(112 + 101*t2 - 78*Power(t2,2) + 
                  13*Power(t2,3)) - 
               2*Power(t1,2)*
                (58 + 235*t2 + 119*Power(t2,2) - 42*Power(t2,3) + 
                  12*Power(t2,4)) + 
               2*t1*(6 + 181*t2 + 226*Power(t2,2) + 36*Power(t2,3) - 
                  42*Power(t2,4) + 5*Power(t2,5)))) + 
         Power(s,4)*(-3 - t1 - 22*Power(t1,2) - 32*t2 - 3*t1*t2 - 
            4*Power(t1,2)*t2 - 15*Power(t2,2) - 12*t1*Power(t2,2) + 
            16*Power(t2,3) - Power(s2,3)*(8 + 2*t1 + 3*t2) + 
            Power(s1,2)*(-24 + 9*Power(s2,2) - 30*t1 + 16*t2 + 
               s2*(12 - 6*t1 + 7*t2)) + 
            Power(s2,2)*(5 + 4*Power(t1,2) + 12*t2 - 15*Power(t2,2) + 
               t1*(-15 + 16*t2)) + 
            s2*(15 - Power(t1,3) + Power(t1,2)*(5 - 11*t2) + 13*t2 - 
               26*Power(t2,2) + 7*Power(t2,3) + 
               t1*(23 + 36*t2 + 5*Power(t2,2))) + 
            s1*(26 + 13*Power(s2,3) + 15*Power(t1,2) + 39*t2 - 
               32*Power(t2,2) + Power(s2,2)*(-5 - 17*t1 + 6*t2) + 
               6*t1*(4 + 7*t2) + 
               s2*(8*Power(t1,2) + t1*(-48 + t2) - 
                  2*(20 - 7*t2 + 7*Power(t2,2))))) + 
         Power(s,3)*(3 + 5*t1 - 16*Power(t1,2) - 16*Power(t1,3) + 
            11*t2 - 31*t1*t2 - 45*Power(t1,2)*t2 + 10*Power(t1,3)*t2 - 
            35*Power(t2,2) + 10*t1*Power(t2,2) - 
            16*Power(t1,2)*Power(t2,2) - 29*Power(t2,3) - 
            8*t1*Power(t2,3) + 14*Power(t2,4) + 
            2*Power(s2,3)*(-4 + t1 - 7*t2 - 16*t1*t2 + 6*Power(t2,2)) - 
            2*Power(s2,4)*(t1 - 6*(1 + t2)) - 
            Power(s1,3)*(-24 + 9*Power(s2,2) - 30*t1 + 14*t2 + 
               s2*(26 - 4*t1 + t2)) + 
            Power(s2,2)*(-28 + Power(t1,3) - 54*t2 + 4*Power(t2,2) - 
               24*Power(t2,3) + Power(t1,2)*(-3 + 30*t2) + 
               3*t1*(-4 - 11*t2 + Power(t2,2))) + 
            s2*(22 + 101*t2 + 49*Power(t2,2) - 37*Power(t2,3) + 
               Power(t2,4) - Power(t1,3)*(8 + 7*t2) + 
               Power(t1,2)*(66 + 13*t2 - 15*Power(t2,2)) + 
               t1*(-18 + 39*t2 + 62*Power(t2,2) + 21*Power(t2,3))) - 
            Power(s1,2)*(4 + 32*Power(s2,3) + 30*Power(t1,2) + 77*t2 - 
               42*Power(t2,2) + Power(s2,2)*(41 - 33*t1 + 6*t2) + 
               4*t1*(9 + 17*t2) + 
               s2*(12*Power(t1,2) - t1*(88 + 13*t2) - 
                  3*(36 + 5*t2 + Power(t2,2)))) + 
            s1*(-23 - 22*Power(s2,4) + 39*t2 + 82*Power(t2,2) - 
               42*Power(t2,3) + 4*Power(s2,3)*(6 + 7*t1 + 5*t2) + 
               Power(t1,2)*(64 + 46*t2) + 
               t1*(38 + 26*t2 + 46*Power(t2,2)) + 
               Power(s2,2)*(100 - 17*Power(t1,2) + 37*t2 + 
                  39*Power(t2,2) - 4*t1*(-4 + 9*t2)) + 
               s2*(-100 + 4*Power(t1,3) - 157*t2 + 48*Power(t2,2) - 
                  3*Power(t2,3) + 3*Power(t1,2)*(-8 + 9*t2) - 
                  2*t1*(44 + 75*t2 + 19*Power(t2,2))))) + 
         Power(s,2)*(-13 - 2*t1 + 68*Power(t1,2) - 68*Power(t1,3) + 
            4*Power(t1,4) + Power(s2,5)*(-8 + 3*t1 - 13*t2) + 11*t2 - 
            64*t1*t2 + 63*Power(t1,2)*t2 - 56*Power(t1,3)*t2 + 
            4*Power(t1,4)*t2 + 62*Power(t2,2) - 105*t1*Power(t2,2) - 
            3*Power(t1,2)*Power(t2,2) + 14*Power(t1,3)*Power(t2,2) + 
            12*Power(t2,3) - 4*t1*Power(t2,3) - 
            30*Power(t1,2)*Power(t2,3) - 21*Power(t2,4) + 
            7*t1*Power(t2,4) + 5*Power(t2,5) + 
            Power(s1,4)*(-4 + 3*Power(s2,2) - 15*t1 - 
               s2*(t1 + 4*(-4 + t2)) + 5*t2) + 
            Power(s2,4)*(4 - 4*Power(t1,2) + 15*t2 + 4*Power(t2,2) + 
               t1*(9 + 30*t2)) + 
            Power(s2,3)*(34 + Power(t1,3) + Power(t1,2)*(1 - 28*t2) + 
               90*t2 + 38*Power(t2,2) + 28*Power(t2,3) - 
               t1*(22 + 27*t2 + 31*Power(t2,2))) + 
            Power(s2,2)*(-16 - 87*t2 - 63*Power(t2,2) + 
               11*Power(t2,3) - 9*Power(t2,4) + 
               Power(t1,3)*(9 + 10*t2) + 
               Power(t1,2)*(-23 + 24*t2 + 45*Power(t2,2)) - 
               2*t1*(8 + 71*t2 + 51*Power(t2,2) + 18*Power(t2,3))) + 
            s2*(-3 - 8*Power(t1,4) - 41*t2 + 21*Power(t2,2) + 
               35*Power(t2,3) - 28*Power(t2,4) - 4*Power(t2,5) + 
               Power(t1,3)*(56 - 17*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(-28 + 62*t2 + 36*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(22 + 166*t2 + 83*Power(t2,2) + 47*Power(t2,3) + 
                  18*Power(t2,4))) + 
            Power(s1,3)*(-50 + 25*Power(s2,3) + 
               Power(s2,2)*(82 - 27*t1) + 30*Power(t1,2) + 33*t2 - 
               20*Power(t2,2) + t1*(40 + 38*t2) + 
               s2*(8*Power(t1,2) - t1*(104 + 15*t2) + 
                  4*(-9 - 5*t2 + 4*Power(t2,2)))) + 
            Power(s1,2)*(75 + 42*Power(s2,4) + 
               Power(s2,3)*(49 - 57*t1 - 22*t2) + 112*t2 - 
               75*Power(t2,2) + 30*Power(t2,3) - 
               18*Power(t1,2)*(2 + 5*t2) - 
               4*t1*(22 + 21*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(-197 + 27*Power(t1,2) - 153*t2 - 
                  18*Power(t2,2) + 3*t1*(-17 + 6*t2)) + 
               s2*(-50 - 6*Power(t1,3) + 107*t2 - 36*Power(t2,2) - 
                  24*Power(t2,3) - 3*Power(t1,2)*(-6 + 5*t2) + 
                  3*t1*(86 + 85*t2 + 17*Power(t2,2)))) + 
            s1*(-8 + 18*Power(s2,5) + Power(t1,3)*(64 - 14*t2) - 
               137*t2 - 74*Power(t2,2) + 67*Power(t2,3) - 
               20*Power(t2,4) - 2*Power(s2,4)*(16 + 11*t1 + 23*t2) + 
               Power(t1,2)*(-44 + 39*t2 + 90*Power(t2,2)) + 
               t1*(41 + 193*t2 + 48*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,3)*(-126 + 9*Power(t1,2) - 87*t2 - 
                  31*Power(t2,2) + t1*(68 + 88*t2)) + 
               Power(s2,2)*(122 - 3*Power(t1,3) + 260*t2 + 
                  60*Power(t2,2) + 24*Power(t2,3) - 
                  24*Power(t1,2)*(1 + 3*t2) + 
                  t1*(146 + 153*t2 + 45*Power(t2,2))) + 
               s2*(73 + 29*t2 + 21*Power(t1,3)*t2 - 106*Power(t2,2) + 
                  68*Power(t2,3) + 16*Power(t2,4) + 
                  6*Power(t1,2)*(-8 - 9*t2 + Power(t2,2)) - 
                  t1*(234 + 341*t2 + 198*Power(t2,2) + 53*Power(t2,3))))) \
+ s*(42 - 75*t1 - 33*Power(t1,2) + 140*Power(t1,3) - 80*Power(t1,4) + 
            4*Power(t1,5) + 37*t2 + 88*t1*t2 - 259*Power(t1,2)*t2 + 
            126*Power(t1,3)*t2 - 4*Power(t1,4)*t2 - 46*Power(t2,2) + 
            227*t1*Power(t2,2) - 132*Power(t1,2)*Power(t2,2) - 
            56*Power(t1,3)*Power(t2,2) + 8*Power(t1,4)*Power(t2,2) - 
            36*Power(t2,3) + 25*t1*Power(t2,3) + 
            49*Power(t1,2)*Power(t2,3) - 2*Power(t1,3)*Power(t2,3) + 
            4*Power(t2,4) - 32*t1*Power(t2,4) - 
            13*Power(t1,2)*Power(t2,4) - Power(t2,5) + 7*t1*Power(t2,5) + 
            Power(s2,6)*(2 - t1 + 6*t2) + 
            Power(s1,5)*(-4 + 3*t1 + s2*(3 + 2*t2)) + 
            Power(s2,5)*(1 + 2*Power(t1,2) - 9*t2 - 9*Power(t2,2) - 
               t1*(6 + 13*t2)) - 
            Power(s2,4)*(19 + Power(t1,3) + 60*t2 + 34*Power(t2,2) + 
               12*Power(t2,3) - 2*Power(t1,2)*(1 + 5*t2) - 
               t1*(18 + 44*t2 + 33*Power(t2,2))) + 
            Power(s2,3)*(-12 + 19*t2 + 47*Power(t2,2) + 21*Power(t2,3) + 
               13*Power(t2,4) - Power(t1,3)*(2 + 3*t2) - 
               Power(t1,2)*(32 + 59*t2 + 41*Power(t2,2)) + 
               t1*(56 + 145*t2 + 68*Power(t2,2) + 11*Power(t2,3))) + 
            Power(s2,2)*(5 + 4*Power(t1,4) + 63*t2 + 69*Power(t2,2) + 
               15*Power(t2,3) + 8*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,3)*(-17 + 46*t2 + 17*Power(t2,2)) + 
               Power(t1,2)*(5 - 53*t2 - 51*Power(t2,2) + 
                  16*Power(t2,3)) - 
               t1*(12 + 196*t2 + 183*Power(t2,2) + 49*Power(t2,3) + 
                  32*Power(t2,4))) + 
            s2*(9 + 16*t2 - 24*Power(t2,2) - 4*Power(t2,3) + 
               17*Power(t2,4) - 12*Power(t2,5) - 2*Power(t2,6) - 
               8*Power(t1,4)*(-2 + 3*t2) + 
               Power(t1,3)*(8 + 27*t2 + 22*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(t1,2)*(-8 + 125*t2 + 85*Power(t2,2) + 
                  5*Power(t2,3) + 13*Power(t2,4)) + 
               2*t1*(-8 - 36*t2 + 7*Power(t2,3) + 12*Power(t2,4) + 
                  Power(t2,5))) - 
            Power(s1,4)*(-1 + 6*Power(s2,3) + 15*Power(t1,2) + 
               Power(s2,2)*(43 - 8*t1 - 4*t2) - 15*t2 + t1*(18 + 5*t2) + 
               s2*(23 + 2*Power(t1,2) + 24*t2 + 10*Power(t2,2) - 
                  2*t1*(38 + t2))) + 
            Power(s1,3)*(63 - 23*Power(s2,4) - 7*t2 - 20*Power(t2,2) + 
               Power(s2,3)*(-80 + 38*t1 + 5*t2) + 
               Power(t1,2)*(8 + 58*t2) - 
               2*t1*(55 - 43*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(21 - 19*Power(t1,2) + 121*t2 - 
                  16*Power(t2,2) + t1*(82 + 8*t2)) + 
               s2*(96 + 4*Power(t1,3) + Power(t1,2)*(16 - 7*t2) + 
                  52*t2 + 66*Power(t2,2) + 20*Power(t2,3) - 
                  4*t1*(34 + 63*t2 + 2*Power(t2,2)))) - 
            s1*(31 + 7*Power(s2,6) - 117*t2 - 135*Power(t2,2) + 
               13*Power(t2,3) + 4*Power(t1,4)*(-3 + 2*t2) - 
               Power(s2,5)*(17 + 8*t1 + 33*t2) - 
               4*Power(t1,3)*(-34 + 24*t2 + Power(t2,2)) - 
               2*Power(t1,2)*
                (138 + 166*t2 - 45*Power(t2,2) + 27*Power(t2,3)) + 
               t1*(107 + 534*t2 + 160*Power(t2,2) - 114*Power(t2,3) + 
                  25*Power(t2,4)) - 
               Power(s2,4)*(72 + Power(t1,2) + 57*t2 + Power(t2,2) - 
                  t1*(67 + 72*t2)) + 
               Power(s2,3)*(62 + 2*Power(t1,3) + 211*t2 + 
                  122*Power(t2,2) + 33*Power(t2,3) - 
                  Power(t1,2)*(52 + 59*t2) + 
                  t1*(100 + 40*t2 - 16*Power(t2,2))) + 
               Power(s2,2)*(94 + 180*t2 + 9*Power(t2,2) - 
                  19*Power(t2,3) + 16*Power(t2,4) + 
                  2*Power(t1,3)*(9 + 10*t2) + 
                  Power(t1,2)*(41 - 18*t2 + 51*Power(t2,2)) - 
                  2*t1*(154 + 281*t2 + 90*Power(t2,2) + 44*Power(t2,3))) \
+ s2*(7 - 16*Power(t1,4) - 102*t2 + 2*Power(t1,3)*(23 - 15*t2)*t2 - 
                  104*Power(t2,2) + 28*Power(t2,3) - 51*Power(t2,4) - 
                  10*Power(t2,5) + 
                  Power(t1,2)*
                   (172 + 223*t2 - 6*Power(t2,2) + 37*Power(t2,3)) + 
                  2*t1*(-41 + 21*t2 + 82*Power(t2,2) + 74*Power(t2,3) + 
                     4*Power(t2,4)))) + 
            Power(s1,2)*(-71 - 24*Power(s2,5) - 162*t2 + 15*Power(t2,2) + 
               10*Power(t2,3) - 2*Power(t1,3)*(20 + t2) + 
               Power(s2,4)*(-23 + 39*t1 + 34*t2) + 
               Power(t1,2)*(-200 + 33*t2 - 84*Power(t2,2)) + 
               t1*(307 + 245*t2 - 150*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s2,3)*(164 - 18*Power(t1,2) + 181*t2 + 
                  21*Power(t2,2) - t1*(28 + 65*t2)) + 
               Power(s2,2)*(3*Power(t1,3) + Power(t1,2)*(33 + 54*t2) - 
                  t1*(379 + 213*t2 + 72*Power(t2,2)) + 
                  3*(37 - 9*t2 - 35*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(-3*Power(t1,3)*(-8 + 7*t2) + 
                  3*Power(t1,2)*(46 - 9*t2 + 11*Power(t2,2)) + 
                  2*t1*(21 + 143*t2 + 150*Power(t2,2) + 6*Power(t2,3)) - 
                  2*(39 + 98*t2 + 9*Power(t2,2) + 42*Power(t2,3) + 
                     10*Power(t2,4))))))*
       T2q(1 - s + s2 - t1,1 - s1 - t1 + t2))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(-24 - 24*s2 - 8*Power(s2,2) + Power(s2,3) + 104*t1 + 80*s2*t1 + 
         22*Power(s2,2)*t1 - 3*Power(s2,3)*t1 - 168*Power(t1,2) - 
         95*s2*Power(t1,2) - 18*Power(s2,2)*Power(t1,2) + 
         3*Power(s2,3)*Power(t1,2) + 120*Power(t1,3) + 
         45*s2*Power(t1,3) + 2*Power(s2,2)*Power(t1,3) - 
         Power(s2,3)*Power(t1,3) - 32*Power(t1,4) - 5*s2*Power(t1,4) + 
         2*Power(s2,2)*Power(t1,4) - s2*Power(t1,5) + 
         2*Power(s1,3)*s2*(6 + Power(s2,3) - 4*t1 - 3*Power(s2,2)*t1 + 
            2*Power(t1,2) - Power(t1,3) + s2*t1*(-4 + 3*t1)) + 
         Power(s,5)*(3 + s2)*(t1 - t2) - 36*t2 - 47*s2*t2 - 
         26*Power(s2,2)*t2 - 7*Power(s2,3)*t2 + Power(s2,4)*t2 + 
         111*t1*t2 + 131*s2*t1*t2 + 58*Power(s2,2)*t1*t2 + 
         12*Power(s2,3)*t1*t2 - 2*Power(s2,4)*t1*t2 - 
         113*Power(t1,2)*t2 - 122*s2*Power(t1,2)*t2 - 
         37*Power(s2,2)*Power(t1,2)*t2 - 3*Power(s2,3)*Power(t1,2)*t2 + 
         Power(s2,4)*Power(t1,2)*t2 + 39*Power(t1,3)*t2 + 
         39*s2*Power(t1,3)*t2 + 4*Power(s2,2)*Power(t1,3)*t2 - 
         2*Power(s2,3)*Power(t1,3)*t2 - 3*Power(t1,4)*t2 - 
         s2*Power(t1,4)*t2 + Power(s2,2)*Power(t1,4)*t2 + 
         2*Power(t1,5)*t2 - 12*Power(t2,2) - 37*s2*Power(t2,2) - 
         19*Power(s2,2)*Power(t2,2) - 6*Power(s2,3)*Power(t2,2) + 
         21*t1*Power(t2,2) + 81*s2*t1*Power(t2,2) + 
         30*Power(s2,2)*t1*Power(t2,2) + 6*Power(s2,3)*t1*Power(t2,2) - 
         14*Power(t1,2)*Power(t2,2) - 47*s2*Power(t1,2)*Power(t2,2) - 
         13*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         7*Power(t1,3)*Power(t2,2) + 5*s2*Power(t1,3)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         2*Power(t1,4)*Power(t2,2) - 2*s2*Power(t1,4)*Power(t2,2) - 
         12*s2*Power(t2,3) + 2*Power(s2,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t2,3) - 2*Power(s2,4)*Power(t2,3) + 
         6*s2*t1*Power(t2,3) + 4*Power(s2,2)*t1*Power(t2,3) + 
         4*Power(s2,3)*t1*Power(t2,3) - 2*s2*Power(t1,2)*Power(t2,3) - 
         4*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         2*s2*Power(t1,3)*Power(t2,3) + 
         Power(s,4)*(-7 + t1 + 11*Power(t1,2) - 
            s1*(-4 + Power(s2,2) + 4*t1 + s2*(7 + 3*t1) - 3*t2) + 3*t2 - 
            8*t1*t2 - 3*Power(t2,2) + Power(s2,2)*(2 - 4*t1 + 3*t2) + 
            s2*(9 + 2*Power(t1,2) + 15*t2 - 2*t1*(7 + t2))) + 
         Power(s1,2)*(-12 + 20*t1 - 9*Power(t1,2) + Power(t1,4) - 
            6*Power(s2,4)*t2 - 
            2*Power(s2,3)*(4 + Power(t1,2) - t2 - t1*(5 + 8*t2)) - 
            2*s2*(Power(t1,4) + 18*(1 + t2) - 3*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(24 + 5*t2) - t1*(40 + 11*t2)) + 
            Power(s2,2)*(-23 + 4*Power(t1,3) + 2*t2 + 20*t1*(2 + t2) - 
               Power(t1,2)*(21 + 16*t2))) + 
         Power(s,3)*(44 - 60*t1 + 3*Power(t1,2) + 15*Power(t1,3) + 
            Power(s1,2)*(-2*Power(s2,2) + t1 + s2*(9 + 4*t1)) + 
            Power(s2,3)*(-2 + 3*t1 - t2) + 32*t2 + 6*t1*t2 - 
            4*Power(t1,2)*t2 + 7*Power(t2,2) - 11*t1*Power(t2,2) + 
            Power(s2,2)*(-28 - 6*Power(t1,2) - 31*t2 + 2*t1*(17 + t2)) + 
            s1*(-37 - 14*Power(t1,2) + 
               s2*(6 - 4*Power(t1,2) + t1*(5 - 8*t2) - 21*t2) - 7*t2 + 
               5*t1*(3 + 2*t2) + Power(s2,2)*(25 + 4*t1 + 2*t2)) + 
            s2*(-15 + 2*Power(t1,3) - 22*t2 + 12*Power(t2,2) - 
               2*Power(t1,2)*(16 + 3*t2) + 
               t1*(42 + 24*t2 + 4*Power(t2,2)))) + 
         s1*(-(Power(s2,4)*(1 - 2*t1 + Power(t1,2) - 6*Power(t2,2))) - 
            (-1 + t1)*(36 + 2*Power(t1,4) + 24*t2 - 
               Power(t1,3)*(1 + t2) + Power(t1,2)*(38 + 6*t2) - 
               t1*(75 + 17*t2)) + 
            Power(s2,3)*(8 + Power(t1,3) + 14*t2 - 4*Power(t2,2) + 
               2*Power(t1,2)*(3 + t2) - t1*(15 + 16*t2 + 14*Power(t2,2))\
) + Power(s2,2)*(30 + Power(t1,4) + 42*t2 - 4*Power(t2,2) - 
               2*Power(t1,3)*(7 + 3*t2) - 
               2*t1*(36 + 35*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(55 + 34*t2 + 14*Power(t2,2))) + 
            s2*(47 - Power(t1,5) + 73*t2 + 36*Power(t2,2) + 
               4*Power(t1,4)*(2 + t2) - 
               Power(t1,3)*(54 + 11*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(135 + 95*t2 + 8*Power(t2,2)) - 
               t1*(135 + 161*t2 + 20*Power(t2,2)))) + 
         Power(s,2)*(-81 + 2*Power(s1,3)*s2*(s2 - t1) + 199*t1 - 
            130*Power(t1,2) + 3*Power(t1,3) + 9*Power(t1,4) - 83*t2 - 
            Power(s2,4)*t2 + 101*t1*t2 + 6*Power(t1,3)*t2 - 
            13*Power(t2,2) + 21*t1*Power(t2,2) - 
            15*Power(t1,2)*Power(t2,2) + 
            Power(s2,3)*(23 - 25*t1 + 2*Power(t1,2) + 25*t2 + 
               4*Power(t2,2)) + 
            s2*(-25 + 2*Power(t1,4) - 38*t2 - 37*Power(t2,2) + 
               2*Power(t2,3) - Power(t1,3)*(27 + 10*t2) + 
               t1*t2*(-7 + 17*t2 + 2*Power(t2,2)) + 
               2*Power(t1,2)*(25 + 7*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(30 - 4*Power(t1,3) + 15*t2 - 21*Power(t2,2) + 
               8*Power(t1,2)*(6 + t2) - 
               2*t1*(37 + 15*t2 + 5*Power(t2,2))) + 
            Power(s1,2)*(-8 + 6*Power(s2,3) + 3*Power(t1,2) - 
               Power(s2,2)*(21 + 12*t1 + 4*t2) + 
               2*s2*(-17 + 3*Power(t1,2) + t2 + 3*t1*(2 + t2))) + 
            s1*(Power(s2,4) - Power(s2,3)*(24 + t1 + 10*t2) - 
               3*(6*Power(t1,3) - 7*(4 + t2) - Power(t1,2)*(7 + 4*t2) + 
                  t1*(37 + 7*t2)) + 
               Power(s2,2)*(-5 + 42*t2 + 2*Power(t2,2) + 
                  2*t1*(6 + 11*t2)) - 
               s2*(-52 - 71*t2 + 4*Power(t2,2) + 
                  3*Power(t1,2)*(-9 + 4*t2) + 
                  t1*(38 + 29*t2 + 6*Power(t2,2))))) + 
         s*(68 - 237*t1 + 275*Power(t1,2) - 109*Power(t1,3) + 
            Power(t1,4) + 2*Power(t1,5) - 
            4*Power(s1,3)*s2*(2 + s2 + Power(s2,2) - t1 - 2*s2*t1 + 
               Power(t1,2)) + 87*t2 - 196*t1*t2 + 108*Power(t1,2)*t2 - 
            6*Power(t1,3)*t2 + 7*Power(t1,4)*t2 + 21*Power(t2,2) - 
            27*t1*Power(t2,2) + 21*Power(t1,2)*Power(t2,2) - 
            9*Power(t1,3)*Power(t2,2) - 
            2*Power(s2,4)*t2*(1 - t1 + 2*t2) + 
            Power(s2,3)*(-12 + Power(t1,3) + 7*t2 + 20*Power(t2,2) + 
               2*Power(t2,3) - 7*Power(t1,2)*(2 + t2) + 
               5*t1*(5 + 2*Power(t2,2))) + 
            Power(s2,2)*(4 - 2*Power(t1,4) + 41*t2 + 44*Power(t2,2) + 
               2*Power(t1,3)*(11 + 5*t2) - 
               Power(t1,2)*(34 + 7*t2 + 8*Power(t2,2)) - 
               2*t1*(-5 + 22*t2 + 11*Power(t2,2) + 2*Power(t2,3))) + 
            s2*(45 + Power(t1,5) + 69*t2 + 44*Power(t2,2) + 
               6*Power(t2,3) - 5*Power(t1,4)*(2 + t2) + 
               4*Power(t1,3)*(3 + t2) - 
               t1*(95 + 122*t2 + 84*Power(t2,2)) + 
               Power(t1,2)*(47 + 54*t2 + 10*Power(t2,2) + 4*Power(t2,3))) \
+ Power(s1,2)*(20 - 4*Power(s2,4) - 17*t1 + 3*Power(t1,3) + 
               10*Power(s2,3)*(2 + t1 + t2) + 
               Power(s2,2)*(50 - 6*Power(t1,2) + 8*t2 - 
                  10*t1*(3 + 2*t2)) + 
               s2*(43 + 22*t2 + 3*Power(t1,2)*(3 + 4*t2) - 
                  2*t1*(41 + 4*t2))) - 
            s1*(87 + 10*Power(t1,4) + 2*Power(s2,4)*(-1 + t1 - 4*t2) + 
               41*t2 - Power(t1,3)*(13 + 6*t2) + 
               Power(t1,2)*(113 + 21*t2) - t1*(197 + 44*t2) + 
               Power(s2,3)*(-6*Power(t1,2) + t1*(-2 + 20*t2) + 
                  8*(1 + 5*t2 + Power(t2,2))) + 
               Power(s2,2)*(51 + 4*Power(t1,3) + 
                  Power(t1,2)*(15 - 14*t2) + 94*t2 + 4*Power(t2,2) - 
                  2*t1*(35 + 26*t2 + 8*Power(t2,2))) + 
               s2*(74 - 23*Power(t1,3) + 87*t2 + 20*Power(t2,2) - 
                  t1*(149 + 166*t2 + 4*Power(t2,2)) + 
                  Power(t1,2)*(98 + 19*t2 + 12*Power(t2,2))))))*
       T3q(s2,1 - s + s2 - t1))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s + t1)*(-1 + s1 + t1 - t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)) + 
    (8*(-24 - 2*s2 + 2*Power(s2,2) + Power(s2,3) - Power(s2,4) + 
         4*Power(s1,3)*s2*(3 + s2) + 58*t1 - 4*s2*t1 + Power(s2,3)*t1 + 
         Power(s2,4)*t1 - 38*Power(t1,2) + 3*s2*Power(t1,2) - 
         3*Power(s2,2)*Power(t1,2) - 2*Power(s2,3)*Power(t1,2) + 
         4*Power(t1,3) + 3*s2*Power(t1,3) + Power(s2,2)*Power(t1,3) + 
         Power(s,3)*(-3 + 2*s2 + Power(s2,2))*(t1 - t2) - 36*t2 - 
         29*s2*t2 + Power(s2,2)*t2 + Power(s2,3)*t2 - Power(s2,5)*t2 + 
         57*t1*t2 + 32*s2*t1*t2 - 7*Power(s2,2)*t1*t2 + 
         4*Power(s2,3)*t1*t2 + 2*Power(s2,4)*t1*t2 - 13*Power(t1,2)*t2 - 
         2*Power(s2,2)*Power(t1,2)*t2 - Power(s2,3)*Power(t1,2)*t2 - 
         6*Power(t1,3)*t2 - 2*s2*Power(t1,3)*t2 - 12*Power(t2,2) - 
         37*s2*Power(t2,2) + 2*Power(s2,2)*Power(t2,2) - 
         Power(s2,3)*Power(t2,2) + 9*t1*Power(t2,2) + 
         12*s2*t1*Power(t2,2) - 3*Power(s2,2)*t1*Power(t2,2) - 
         2*Power(s2,3)*t1*Power(t2,2) + 6*Power(t1,2)*Power(t2,2) + 
         8*s2*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,2) - 12*s2*Power(t2,3) + 
         2*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) - 
         6*s2*t1*Power(t2,3) - 2*Power(s2,2)*t1*Power(t2,3) + 
         Power(s,2)*(7 - 3*t1 - 9*Power(t1,2) + 3*t2 + 6*t1*t2 + 
            3*Power(t2,2) - Power(s2,3)*(2 + t2) + 
            s2*(-4 + 11*t1 + Power(t1,2) - 8*t2 + 6*t1*t2 - 
               7*Power(t2,2)) - 
            Power(s2,2)*(1 + 2*t2 - 4*t1*t2 + 4*Power(t2,2)) + 
            s1*(-4 + 3*Power(s2,3) - 3*t2 + 
               Power(s2,2)*(2 - 3*t1 + 4*t2) + s2*(-1 - 5*t1 + 7*t2))) + 
         Power(s1,2)*(2*Power(s2,4) + 3*(-4 + 4*t1 + Power(t1,2)) + 
            Power(s2,3)*(1 - 4*t1 + 2*t2) + 
            Power(s2,2)*(-7 + 2*Power(t1,2) - 6*t2 - 2*t1*t2) + 
            s2*(3*Power(t1,2) - 6*t1*(-4 + t2) - 4*(10 + 9*t2))) + 
         s1*(36 + Power(s2,5) + 6*Power(t1,3) + 
            Power(t1,2)*(15 - 9*t2) + 24*t2 - 
            Power(s2,4)*(1 + t1 + 2*t2) - t1*(59 + 21*t2) + 
            Power(s2,3)*(t1 - Power(t1,2) + 6*t1*t2 - 
               4*(1 + Power(t2,2))) + 
            s2*(31 + Power(t1,3) + Power(t1,2)*(3 - 11*t2) + 77*t2 + 
               36*Power(t2,2) + 12*t1*(-3 - 3*t2 + Power(t2,2))) + 
            Power(s2,2)*(1 + Power(t1,3) + 5*t2 - 
               Power(t1,2)*(1 + 4*t2) + t1*(7 + 3*t2 + 4*Power(t2,2)))) - 
         s*(-22 + 19*t1 - Power(t1,2) + 6*Power(t1,3) + 
            Power(s2,4)*(-2 + t1 - 3*t2) - 33*t2 + 10*t1*t2 + 
            3*Power(t1,2)*t2 - 9*Power(t2,2) - 9*t1*Power(t2,2) + 
            Power(s2,3)*(2 + t1 - 2*Power(t1,2) + 8*t1*t2 - 
               4*Power(t2,2)) + 
            Power(s1,2)*(2*Power(s2,3) - 3*(4 + t1) + 
               Power(s2,2)*(9 - 2*t1 + 2*t2) + s2*(9 - 3*t1 + 6*t2)) + 
            s2*(33 + Power(t1,3) + 40*t2 + 21*Power(t2,2) + 
               6*Power(t2,3) - Power(t1,2)*(5 + 6*t2) - 
               t1*(34 + 22*t2 + Power(t2,2))) + 
            Power(s2,2)*(-3 + Power(t1,3) + 4*t2 - 5*Power(t1,2)*t2 + 
               2*Power(t2,3) + t1*(5 + 4*t2 + 2*Power(t2,2))) + 
            s1*(35 + 4*Power(s2,4) - 11*t1 - 6*Power(t1,2) + 21*t2 + 
               12*t1*t2 + Power(s2,3)*(-3 - 6*t1 + 2*t2) + 
               Power(s2,2)*(-13 + 5*t1 + 2*Power(t1,2) - 9*t2 - 
                  4*Power(t2,2)) + 
               s2*(4*Power(t1,2) + 4*t1*(7 + t2) - 
                  3*(13 + 10*t2 + 4*Power(t2,2))))))*T4q(-1 + s2))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)) - 
    (8*(-16 - 22*s2 + 4*Power(s1,4)*s2 - 4*Power(s2,2) + Power(s2,3) + 
         54*t1 + 44*s2*t1 + 6*Power(s2,2)*t1 - 2*Power(s2,3)*t1 - 
         56*Power(t1,2) - 21*s2*Power(t1,2) + Power(s2,3)*Power(t1,2) + 
         14*Power(t1,3) - 2*s2*Power(t1,3) - 2*Power(s2,2)*Power(t1,3) + 
         4*Power(t1,4) + s2*Power(t1,4) - 36*t2 - 7*s2*t2 - 
         28*Power(s2,2)*t2 - 2*Power(s2,3)*t2 + Power(s2,4)*t2 + 
         43*t1*t2 + 24*s2*t1*t2 + 24*Power(s2,2)*t1*t2 - 
         Power(s2,4)*t1*t2 + 8*Power(t1,2)*t2 - 13*s2*Power(t1,2)*t2 + 
         5*Power(s2,2)*Power(t1,2)*t2 + 2*Power(s2,3)*Power(t1,2)*t2 - 
         13*Power(t1,3)*t2 - 4*s2*Power(t1,3)*t2 - 
         Power(s2,2)*Power(t1,3)*t2 - 2*Power(t1,4)*t2 - 
         34*s2*Power(t2,2) + 13*Power(s2,2)*Power(t2,2) - 
         7*Power(s2,3)*Power(t2,2) + Power(s2,4)*Power(t2,2) - 
         10*t1*Power(t2,2) + 3*s2*t1*Power(t2,2) - 
         5*Power(s2,2)*t1*Power(t2,2) - 2*Power(s2,3)*t1*Power(t2,2) + 
         8*Power(t1,2)*Power(t2,2) + 12*s2*Power(t1,2)*Power(t2,2) - 
         Power(s2,2)*Power(t1,2)*Power(t2,2) + 4*Power(t1,3)*Power(t2,2) + 
         2*s2*Power(t1,3)*Power(t2,2) + 4*Power(t2,3) - 5*s2*Power(t2,3) + 
         7*Power(s2,2)*Power(t2,3) + t1*Power(t2,3) - 
         13*s2*t1*Power(t2,3) + 4*Power(s2,2)*t1*Power(t2,3) - 
         2*Power(t1,2)*Power(t2,3) - 4*s2*Power(t1,2)*Power(t2,3) + 
         4*s2*Power(t2,4) - 2*Power(s2,2)*Power(t2,4) + 
         2*s2*t1*Power(t2,4) + 
         Power(s1,3)*(-4 + 2*Power(s2,3) + 4*t1 - 3*Power(t1,2) + 
            2*s2*(Power(t1,2) - t1*(-10 + t2) - 8*t2) + 
            Power(s2,2)*(-9 - 4*t1 + 2*t2)) + 
         Power(s,3)*(16 + 8*Power(s2,2) + t1 - 5*Power(t1,2) - t2 + 
            10*t1*t2 - 5*Power(t2,2) + 
            s1*(-8 - 5*t1 + s2*(8 + t1 - t2) + 5*t2) + 
            s2*(-24 + 3*t1 + Power(t1,2) - 3*t2 - 2*t1*t2 + Power(t2,2))) \
+ Power(s1,2)*(Power(s2,4) - Power(t1,3) + 
            Power(s2,3)*(-10 + t1 - 4*t2) + 12*t2 + 
            4*Power(t1,2)*(5 + t2) - t1*(17 + 7*t2) + 
            Power(s2,2)*(5 - 5*Power(t1,2) + 25*t2 - 6*Power(t2,2) + 
               t1*(7 + 12*t2)) + 
            s2*(-27 + 3*Power(t1,3) - 8*Power(t1,2)*(-1 + t2) - 5*t2 + 
               24*Power(t2,2) + t1*(-1 - 53*t2 + 6*Power(t2,2)))) + 
         Power(s,2)*(-13 - 8*Power(s2,3) + 46*t1 + 2*Power(t1,2) - 
            7*Power(t1,3) + 2*t2 - 8*t1*t2 + 9*Power(t1,2)*t2 + 
            6*Power(t2,2) + 3*t1*Power(t2,2) - 5*Power(t2,3) + 
            Power(s2,2)*(46 + 23*t2 + Power(t2,2) - t1*(2 + t2)) + 
            Power(s1,2)*(8 + 3*Power(s2,2) + 8*t1 - 5*t2 + 
               s2*(-15 - 3*t1 + 4*t2)) + 
            s1*(9 - 17*t1 + Power(t1,2) + 
               Power(s2,2)*(-29 + 3*t1 - 4*t2) - 14*t2 - 11*t1*t2 + 
               10*Power(t2,2) + 
               s2*(28 + 12*t1 - 3*Power(t1,2) + 12*t2 + 11*t1*t2 - 
                  8*Power(t2,2))) + 
            s2*(-29 - 18*t2 + 3*Power(t2,2) + 4*Power(t2,3) + 
               4*Power(t1,2)*(3 + t2) - t1*(47 + 15*t2 + 8*Power(t2,2)))) \
+ s1*(2*Power(t1,4) + Power(t1,3)*(11 - 3*t2) + 
            Power(s2,4)*(-1 + t1 - 2*t2) - 12*(-3 + Power(t2,2)) + 
            Power(t1,2)*(-4 - 28*t2 + Power(t2,2)) + 
            Power(s2,3)*(3 - Power(t1,2) + t1*(-2 + t2) + 17*t2 + 
               2*Power(t2,2)) + t1*(-45 + 27*t2 + 2*Power(t2,2)) - 
            Power(s2,2)*(-32 + Power(t1,3) + 18*t2 + 23*Power(t2,2) - 
               6*Power(t2,3) - 3*Power(t1,2)*(1 + 2*t2) + 
               2*t1*(17 + t2 + 6*Power(t2,2))) + 
            s2*(9 + Power(t1,4) + 61*t2 + 10*Power(t2,2) - 
               16*Power(t2,3) - Power(t1,3)*(2 + 5*t2) + 
               2*Power(t1,2)*(12 - 10*t2 + 5*Power(t2,2)) - 
               2*t1*(16 + t2 - 23*Power(t2,2) + 3*Power(t2,3)))) + 
         s*(18 - 69*t1 + 48*Power(t1,2) + 5*Power(t1,3) - 2*Power(t1,4) + 
            43*t2 - 2*t1*t2 - 20*Power(t1,2)*t2 - 3*Power(t1,3)*t2 + 
            2*Power(t2,2) + 14*t1*Power(t2,2) + 
            12*Power(t1,2)*Power(t2,2) + Power(t2,3) - 7*t1*Power(t2,3) + 
            Power(s1,3)*(4 + s2 - 2*Power(s2,2) - 3*t1 + 2*s2*t1 - 
               2*s2*t2) - Power(s2,3)*
             (Power(t1,2) - t1*(7 + 4*t2) + 3*(2 + 7*t2 + Power(t2,2))) - 
            s2*(-37 + Power(t1,4) + 46*t2 + 7*Power(t2,2) - 
               6*Power(t2,3) - 2*Power(t2,4) - 3*Power(t1,3)*(1 + 2*t2) + 
               Power(t1,2)*(25 - 9*t2 + 7*Power(t2,2)) + 
               2*t1*(7 + 13*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(-24 + 2*Power(t1,3) + 53*t2 + 13*Power(t2,2) - 
               4*Power(t2,3) - 2*Power(t1,2)*(4 + 5*t2) + 
               t1*(30 + 13*t2 + 12*Power(t2,2))) - 
            Power(s1,2)*(5 + 4*Power(s2,3) - 7*Power(t1,2) - 
               Power(s2,2)*(29 + 4*t1) + t1*(-28 + t2) + 7*t2 + 
               s2*(1 - 4*t2 - 6*Power(t2,2) + 4*t1*(10 + t2))) + 
            s1*(-45 + 8*Power(t1,3) + Power(t1,2)*(2 - 19*t2) + 3*t2 + 
               2*Power(t2,2) + Power(s2,3)*(22 - 5*t1 + 7*t2) + 
               t1*(17 - 42*t2 + 11*Power(t2,2)) + 
               Power(s2,2)*(-55 + 8*Power(t1,2) - 42*t2 + 6*Power(t2,2) - 
                  t1*(9 + 16*t2)) + 
               s2*(31 - 3*Power(t1,3) + 8*t2 - 11*Power(t2,2) - 
                  6*Power(t2,3) + Power(t1,2)*(-23 + 7*t2) + 
                  2*t1*(26 + 29*t2 + Power(t2,2))))))*T5q(1 - s1 - t1 + t2))/
     ((-1 + s2)*Power(-s + s2 - t1,2)*(-1 + s1 + t1 - t2)*
       (1 - s + s*s2 - s1*s2 - t1 + s2*t2)));
   return a;
};
