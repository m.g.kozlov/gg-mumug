#include "../h/nlo_functions.h"
#include "../h/dilog.h"

#define Pi       3.141592653589793238462643383279502884L
#define Power pq



long double ur_pent(double *vars)
{
    long double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    long double aG=1/Power(4.*Pi,2);
    long double a=aG*((16*(2*s1*s2*(s2 - t1)*t1*(s1 - t2)*(t1 - t2)*(s1 + t1 - t2)*
          (s1 - s2 + t1 - t2)*t2*(s2 - t1 + t2)*
          Power(s1*(-s2 + t1) + s2*t2,2) + 
         Power(s,6)*(t1 - t2)*
          (-(s2*t1*(2*Power(s2,2) - 3*s2*(t1 - t2) + Power(t1 - t2,2))*
               (t1 - t2)*Power(t2,2)) + 
            Power(s1,3)*t1*(Power(s2,2)*(t1 - 5*t2) + 
               2*t1*t2*(-t1 + t2) - 
               s2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,2)*(-3*Power(t1,2)*Power(t1 - t2,2)*t2 + 
               Power(s2,3)*(5*t1 - t2)*t2 - 
               s2*t1*(Power(t1,3) - 14*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,3) - 17*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) - Power(t2,3))) + 
            s1*t2*(5*s2*t1*Power(t1 - t2,3) - 
               Power(t1,2)*Power(t1 - t2,3) + 
               Power(s2,3)*(3*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-7*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + Power(t2,3)))) + 
         Power(s,5)*(t1 - t2)*
          (-(s2*t1*(t1 - t2)*Power(t2,2)*
               (-5*Power(s2,3) + 4*Power(s2,2)*(4*t1 - t2) + 
                 Power(t1 - t2,2)*(5*t1 + 2*t2) + 
                 s2*(-16*Power(t1,2) + 13*t1*t2 + 3*Power(t2,2)))) + 
            Power(s1,4)*(5*Power(t1,2)*(t1 - t2)*t2 + 
               Power(s2,3)*(3*t1 + 4*t2) + 
               s2*t1*(4*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-7*Power(t1,2) + 7*t1*t2 + 4*Power(t2,2))) - 
            Power(s1,3)*(Power(s2,4)*(4*t1 + 3*t2) - 
               12*Power(s2,3)*(Power(t1,2) - Power(t2,2)) - 
               4*Power(t1,2)*t2*
                (Power(t1,2) - 5*t1*t2 + 4*Power(t2,2)) - 
               4*s2*t1*(Power(t1,3) - 6*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*(12*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) + 9*Power(t2,3))) + 
            s1*t2*(12*s2*t1*Power(t1 - t2,3)*(t1 + t2) - 
               Power(t1,2)*Power(t1 - t2,3)*(2*t1 + 5*t2) + 
               Power(s2,4)*(-2*Power(t1,2) + 15*t1*t2 - 4*Power(t2,2)) + 
               4*Power(s2,3)*
                (3*Power(t1,3) - 12*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
                  Power(t2,3)) - 
               Power(s2,2)*t1*
                (20*Power(t1,3) - 58*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s1,2)*(-(Power(t1,2)*Power(t1 - t2,2)*t2*
                  (3*t1 + 16*t2)) + 
               Power(s2,4)*(-4*Power(t1,2) - 7*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*(9*Power(t1,3) + 22*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) + 12*Power(t2,3)) + 
               s2*t1*t2*(15*Power(t1,3) + 23*Power(t1,2)*t2 - 
                  58*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*(-5*Power(t1,4) - 28*Power(t1,3)*t2 + 
                  28*t1*Power(t2,3) + 5*Power(t2,4)))) - 
         Power(s,4)*(Power(s1,5)*
             (Power(s2,3)*(5*Power(t1,2) - 2*t1*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*t2*
                (4*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(5*Power(t1,3) - 19*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(10*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s2*t1*(t1 - t2)*Power(t2,2)*
             (Power(s2,4)*(3*t1 - 4*t2) - 
               s2*Power(t1 - t2,2)*
                (28*Power(t1,2) + 22*t1*t2 - 3*Power(t2,2)) + 
               Power(t1 - t2,3)*
                (8*Power(t1,2) + 10*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(-18*Power(t1,2) + 16*t1*t2 + 
                  3*Power(t2,2)) + 
               Power(s2,2)*(35*Power(t1,3) - 32*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 11*Power(t2,3))) + 
            Power(s1,4)*(Power(s2,4)*
                (-3*Power(t1,2) + 8*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,3)*(9*Power(t1,3) - 32*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 14*Power(t2,3)) - 
               Power(t1,2)*t2*
                (3*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  34*t1*Power(t2,2) + 18*Power(t2,3)) + 
               s2*t1*(2*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  68*Power(t1,2)*Power(t2,2) - 62*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*(-8*Power(t1,4) + 38*Power(t1,3)*t2 - 
                  62*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) + 
                  17*Power(t2,4))) + 
            s1*t2*(-11*s2*t1*Power(t1 - t2,4)*
                (Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               Power(t1,2)*Power(t1 - t2,4)*
                (Power(t1,2) + 10*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,5)*(Power(t1,3) + 14*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (24*Power(t1,4) - 25*Power(t1,3)*t2 - 
                  85*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,4)*(3*Power(t1,4) - 62*Power(t1,3)*t2 + 
                  68*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,3)*(18*Power(t1,5) - 104*Power(t1,4)*t2 + 
                  74*Power(t1,3)*Power(t2,2) + 
                  51*Power(t1,2)*Power(t2,3) - 44*t1*Power(t2,4) + 
                  5*Power(t2,5))) + 
            Power(s1,2)*(-(Power(t1,2)*Power(t1 - t2,3)*t2*
                  (3*Power(t1,2) - 22*t1*t2 - 28*Power(t2,2))) - 
               Power(s2,5)*(4*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) + 10*Power(t2,3)) - 
               s2*t1*Power(t1 - t2,2)*
                (2*Power(t1,4) - 4*Power(t1,3)*t2 + 
                  85*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,4)*(17*Power(t1,4) + 14*Power(t1,3)*t2 - 
                  62*Power(t1,2)*Power(t2,2) + 38*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (13*Power(t1,4) + 29*Power(t1,3)*t2 + 
                  107*Power(t1,2)*Power(t2,2) + 29*t1*Power(t2,3) + 
                  13*Power(t2,4)) + 
               Power(s2,3)*(-24*Power(t1,5) - 19*Power(t1,4)*t2 + 
                  52*Power(t1,3)*Power(t2,2) + 
                  18*Power(t1,2)*Power(t2,3) - 41*t1*Power(t2,4) + 
                  15*Power(t2,5))) + 
            Power(s1,3)*(-(Power(t1,2)*Power(t1 - t2,2)*t2*
                  (11*Power(t1,2) - 3*t1*t2 - 35*Power(t2,2))) - 
               20*Power(s2,3)*Power(Power(t1,2) - Power(t2,2),2) + 
               Power(s2,5)*(-4*Power(t1,2) - 2*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,4)*(14*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  32*t1*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,2)*(15*Power(t1,5) - 41*Power(t1,4)*t2 + 
                  18*Power(t1,3)*Power(t2,2) + 
                  52*Power(t1,2)*Power(t2,3) - 19*t1*Power(t2,4) - 
                  24*Power(t2,5)) - 
               s2*t1*(5*Power(t1,5) - 44*Power(t1,4)*t2 + 
                  51*Power(t1,3)*Power(t2,2) + 
                  74*Power(t1,2)*Power(t2,3) - 104*t1*Power(t2,4) + 
                  18*Power(t2,5)))) + 
         s*(-(s2*(s2 - t1)*t1*(t1 - t2)*Power(t2,4)*
               (-4*Power(t1,2)*Power(t1 - t2,3) + 
                 4*s2*t1*Power(t1 - t2,2)*(3*t1 - t2) + Power(s2,4)*t2 + 
                 Power(s2,3)*(4*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
                 Power(s2,2)*(-12*Power(t1,3) + 22*Power(t1,2)*t2 - 
                    11*t1*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,6)*(s2 - t1)*
             (Power(t1,4)*t2*(-t1 + t2) + 
               Power(s2,4)*(Power(t1,2) - Power(t2,2)) - 
               Power(s2,3)*(3*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*t1*
                (3*Power(t1,3) + 5*Power(t1,2)*t2 - 15*t1*Power(t2,2) + 
                  6*Power(t2,3)) - 
               s2*Power(t1,2)*
                (Power(t1,3) + Power(t1,2)*t2 - 8*t1*Power(t2,2) + 
                  7*Power(t2,3))) - 
            Power(s1,5)*(s2 - t1)*
             (Power(s2,5)*(Power(t1,2) - Power(t2,2)) + 
               Power(s2,4)*(-5*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 6*Power(t2,3)) + 
               2*Power(t1,3)*t2*
                (Power(t1,3) - 5*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
                  2*Power(t2,3)) + 
               s2*Power(t1,2)*
                (2*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) + 24*t1*Power(t2,3) - 
                  23*Power(t2,4)) + 
               Power(s2,3)*(9*Power(t1,4) - 14*Power(t1,3)*t2 - 
                  29*Power(t1,2)*Power(t2,2) + 38*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-7*Power(t1,4) + 12*Power(t1,3)*t2 + 
                  34*Power(t1,2)*Power(t2,2) - 72*t1*Power(t2,3) + 
                  29*Power(t2,4))) + 
            s1*Power(t2,3)*(-4*Power(t1,4)*Power(t1 - t2,4)*t2 + 
               8*s2*Power(t1,3)*Power(t1 - t2,4)*(t1 + t2) + 
               Power(s2,6)*(7*Power(t1,3) - 7*Power(t1,2)*t2 + 
                  Power(t2,3)) - 
               Power(s2,2)*Power(t1,2)*Power(t1 - t2,2)*
                (26*Power(t1,3) - 55*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,5)*(-19*Power(t1,4) + 12*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,3)*t1*
                (23*Power(t1,5) - 128*Power(t1,4)*t2 + 
                  197*Power(t1,3)*Power(t2,2) - 
                  116*Power(t1,2)*Power(t2,3) + 28*t1*Power(t2,4) - 
                  4*Power(t2,5)) + 
               Power(s2,4)*(7*Power(t1,5) + 44*Power(t1,4)*t2 - 
                  92*Power(t1,3)*Power(t2,2) + 
                  53*Power(t1,2)*Power(t2,3) - 12*t1*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(s1,3)*t2*(-(Power(t1,4)*Power(t1 - t2,2)*t2*
                  (5*Power(t1,2) - 26*t1*t2 + 24*Power(t2,2))) + 
               Power(s2,6)*(7*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,5)*(-34*Power(t1,4) + 110*Power(t1,3)*t2 - 
                  63*Power(t1,2)*Power(t2,2) - 26*t1*Power(t2,3) + 
                  16*Power(t2,4)) + 
               Power(s2,4)*(64*Power(t1,5) - 210*Power(t1,4)*t2 + 
                  126*Power(t1,3)*Power(t2,2) + 
                  107*Power(t1,2)*Power(t2,3) - 96*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               s2*Power(t1,3)*
                (-4*Power(t1,5) + 28*Power(t1,4)*t2 - 
                  116*Power(t1,3)*Power(t2,2) + 
                  197*Power(t1,2)*Power(t2,3) - 128*t1*Power(t2,4) + 
                  23*Power(t2,5)) + 
               Power(s2,2)*Power(t1,2)*
                (25*Power(t1,5) - 97*Power(t1,4)*t2 + 
                  148*Power(t1,3)*Power(t2,2) - 
                  65*Power(t1,2)*Power(t2,3) - 58*t1*Power(t2,4) + 
                  46*Power(t2,5)) - 
               Power(s2,3)*t1*
                (58*Power(t1,5) - 196*Power(t1,4)*t2 + 
                  139*Power(t1,3)*Power(t2,2) + 
                  139*Power(t1,2)*Power(t2,3) - 196*t1*Power(t2,4) + 
                  58*Power(t2,5))) + 
            Power(s1,4)*(-(Power(s2,6)*
                  (Power(t1,3) - 8*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                    4*Power(t2,3))) + 
               Power(s2,5)*(5*Power(t1,4) - 44*Power(t1,3)*t2 + 
                  34*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(t1,4)*t2*
                (Power(t1,4) - 14*Power(t1,3)*t2 + 
                  42*Power(t1,2)*Power(t2,2) - 45*t1*Power(t2,3) + 
                  16*Power(t2,4)) + 
               s2*Power(t1,3)*
                (Power(t1,5) - 12*Power(t1,4)*t2 + 
                  53*Power(t1,3)*Power(t2,2) - 
                  92*Power(t1,2)*Power(t2,3) + 44*t1*Power(t2,4) + 
                  7*Power(t2,5)) - 
               Power(s2,4)*(10*Power(t1,5) - 93*Power(t1,4)*t2 + 
                  86*Power(t1,3)*Power(t2,2) + 
                  86*Power(t1,2)*Power(t2,3) - 93*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,3)*t1*
                (10*Power(t1,5) - 96*Power(t1,4)*t2 + 
                  107*Power(t1,3)*Power(t2,2) + 
                  126*Power(t1,2)*Power(t2,3) - 210*t1*Power(t2,4) + 
                  64*Power(t2,5)) - 
               Power(s2,2)*Power(t1,2)*
                (5*Power(t1,5) - 50*Power(t1,4)*t2 + 
                  91*Power(t1,3)*Power(t2,2) + 
                  6*Power(t1,2)*Power(t2,3) - 131*t1*Power(t2,4) + 
                  76*Power(t2,5))) - 
            Power(s1,2)*Power(t2,2)*
             (-8*Power(t1,4)*(t1 - 2*t2)*Power(t1 - t2,3)*t2 + 
               Power(s2,6)*(13*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 4*Power(t2,3)) + 
               s2*Power(t1,3)*Power(t1 - t2,2)*
                (2*Power(t1,3) + 12*Power(t1,2)*t2 - 55*t1*Power(t2,2) + 
                  26*Power(t2,3)) + 
               Power(s2,5)*(-52*Power(t1,4) + 96*Power(t1,3)*t2 - 
                  33*Power(t1,2)*Power(t2,2) - 18*t1*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-46*Power(t1,5) + 58*Power(t1,4)*t2 + 
                  65*Power(t1,3)*Power(t2,2) - 
                  148*Power(t1,2)*Power(t2,3) + 97*t1*Power(t2,4) - 
                  25*Power(t2,5)) + 
               Power(s2,4)*(76*Power(t1,5) - 131*Power(t1,4)*t2 + 
                  6*Power(t1,3)*Power(t2,2) + 
                  91*Power(t1,2)*Power(t2,3) - 50*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               7*Power(s2,2)*(Power(t1,7) - Power(t1,5)*Power(t2,2) - 
                  Power(t1,4)*Power(t2,3) + Power(t1,2)*Power(t2,5)))) + 
         Power(s,3)*(Power(s1,6)*(s2 - t1)*t1*
             (Power(s2,2)*(2*t1 - 3*t2) + t1*t2*(-t1 + t2) + 
               s2*(-2*Power(t1,2) + 6*t1*t2 - 5*Power(t2,2))) - 
            s2*t1*(t1 - t2)*Power(t2,2)*
             (Power(s2,5)*t2 + 
               Power(s2,4)*(4*Power(t1,2) - 2*t1*t2 - 6*Power(t2,2)) + 
               t1*Power(t1 - t2,3)*
                (4*Power(t1,2) + 16*t1*t2 + 5*Power(t2,2)) - 
               s2*Power(t1 - t2,2)*
                (16*Power(t1,3) + 48*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) - 3*Power(t2,3)) - 
               Power(s2,3)*(16*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  34*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(24*Power(t1,4) + 20*Power(t1,3)*t2 - 
                  81*Power(t1,2)*Power(t2,2) + 35*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,5)*(Power(s2,4)*
                (4*Power(t1,2) + 4*t1*t2 - 7*Power(t2,2)) + 
               2*Power(t1,2)*t2*
                (-3*Power(t1,3) + 2*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
                  2*Power(t2,3)) - 
               Power(s2,3)*(12*Power(t1,3) + 14*Power(t1,2)*t2 - 
                  35*t1*Power(t2,2) + 4*Power(t2,3)) + 
               s2*t1*(-4*Power(t1,4) + 10*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) - 40*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(12*Power(t1,4) + 6*Power(t1,3)*t2 - 
                  61*Power(t1,2)*Power(t2,2) + 38*t1*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,5)*
                (-7*Power(t1,2) + 4*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,4)*(31*Power(t1,3) - 36*Power(t1,2)*t2 - 
                  36*t1*Power(t2,2) + 31*Power(t2,3)) + 
               Power(t1,2)*t2*
                (-8*Power(t1,4) + 42*Power(t1,3)*t2 - 
                  41*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) + 
                  16*Power(t2,4)) + 
               Power(s2,3)*(-50*Power(t1,4) + 81*Power(t1,3)*t2 + 
                  63*Power(t1,2)*Power(t2,2) - 110*t1*Power(t2,3) + 
                  17*Power(t2,4)) + 
               Power(s2,2)*(35*Power(t1,5) - 86*Power(t1,4)*t2 + 
                  18*Power(t1,3)*Power(t2,2) + 
                  136*Power(t1,2)*Power(t2,3) - 88*t1*Power(t2,4) - 
                  10*Power(t2,5)) - 
               s2*t1*(9*Power(t1,5) - 45*Power(t1,4)*t2 + 
                  90*Power(t1,3)*Power(t2,2) + 
                  16*Power(t1,2)*Power(t2,3) - 80*t1*Power(t2,4) + 
                  7*Power(t2,5))) + 
            s1*t2*(Power(s2,6)*t2*
                (6*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*Power(t1 - t2,4)*t2*
                (5*Power(t1,2) + 16*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*Power(t1 - t2,4)*
                (4*Power(t1,3) + 33*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,5)*(Power(t1,4) - 40*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) - 
                  4*Power(t2,4)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (13*Power(t1,5) + 27*Power(t1,4)*t2 - 
                  122*Power(t1,3)*Power(t2,2) - 
                  29*Power(t1,2)*Power(t2,3) + 8*t1*Power(t2,4) - 
                  Power(t2,5)) - 
               Power(s2,4)*(7*Power(t1,5) - 80*Power(t1,4)*t2 + 
                  16*Power(t1,3)*Power(t2,2) + 
                  90*Power(t1,2)*Power(t2,3) - 45*t1*Power(t2,4) + 
                  9*Power(t2,5)) + 
               Power(s2,3)*(15*Power(t1,6) - 57*Power(t1,5)*t2 - 
                  98*Power(t1,4)*Power(t2,2) + 
                  243*Power(t1,3)*Power(t2,3) - 
                  103*Power(t1,2)*Power(t2,4) + 2*t1*Power(t2,5) - 
                  2*Power(t2,6))) + 
            Power(s1,2)*(Power(s2,6)*t2*
                (-5*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*Power(t1 - t2,3)*t2*
                (3*Power(t1,3) + 4*Power(t1,2)*t2 - 48*t1*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(s2,5)*(3*Power(t1,4) + 38*Power(t1,3)*t2 - 
                  61*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               s2*t1*Power(t1 - t2,2)*
                (Power(t1,5) - 8*Power(t1,4)*t2 + 
                  29*Power(t1,3)*Power(t2,2) + 
                  122*Power(t1,2)*Power(t2,3) - 27*t1*Power(t2,4) - 
                  13*Power(t2,5)) - 
               2*Power(s2,2)*Power(t1 - t2,2)*
                (3*Power(t1,5) + 14*Power(t1,4)*t2 + 
                  46*Power(t1,3)*Power(t2,2) + 
                  46*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,4)*(-10*Power(t1,5) - 88*Power(t1,4)*t2 + 
                  136*Power(t1,3)*Power(t2,2) + 
                  18*Power(t1,2)*Power(t2,3) - 86*t1*Power(t2,4) + 
                  35*Power(t2,5)) + 
               Power(s2,3)*(12*Power(t1,6) + 78*Power(t1,5)*t2 - 
                  83*Power(t1,4)*Power(t2,2) - 
                  94*Power(t1,3)*Power(t2,3) + 
                  128*Power(t1,2)*Power(t2,4) - 57*t1*Power(t2,5) + 
                  13*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,6)*t2*(-3*t1 + 2*t2) + 
               Power(t1,2)*Power(t1 - t2,2)*t2*
                (2*Power(t1,3) + 37*Power(t1,2)*t2 - 44*t1*Power(t2,2) - 
                  24*Power(t2,3)) - 
               Power(s2,5)*(4*Power(t1,3) - 35*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,4)*(17*Power(t1,4) - 110*Power(t1,3)*t2 + 
                  63*Power(t1,2)*Power(t2,2) + 81*t1*Power(t2,3) - 
                  50*Power(t2,4)) - 
               Power(s2,3)*(24*Power(t1,5) - 131*Power(t1,4)*t2 + 
                  108*Power(t1,3)*Power(t2,2) + 
                  108*Power(t1,2)*Power(t2,3) - 131*t1*Power(t2,4) + 
                  24*Power(t2,5)) + 
               Power(s2,2)*(13*Power(t1,6) - 57*Power(t1,5)*t2 + 
                  128*Power(t1,4)*Power(t2,2) - 
                  94*Power(t1,3)*Power(t2,3) - 
                  83*Power(t1,2)*Power(t2,4) + 78*t1*Power(t2,5) + 
                  12*Power(t2,6)) + 
               s2*t1*(-2*Power(t1,6) + 2*Power(t1,5)*t2 - 
                  103*Power(t1,4)*Power(t2,2) + 
                  243*Power(t1,3)*Power(t2,3) - 
                  98*Power(t1,2)*Power(t2,4) - 57*t1*Power(t2,5) + 
                  15*Power(t2,6)))) + 
         Power(s,2)*(-(Power(s1,6)*(s2 - t1)*
               (2*Power(t1,3)*(t1 - t2)*t2 + 
                 Power(s2,3)*(3*Power(t1,2) - 2*t1*t2 - Power(t2,2)) - 
                 Power(s2,2)*(6*Power(t1,3) - 7*Power(t1,2)*t2 - 
                    2*t1*Power(t2,2) + Power(t2,3)) + 
                 s2*t1*(3*Power(t1,3) - 7*Power(t1,2)*t2 + 
                    t1*Power(t2,2) + 5*Power(t2,3)))) - 
            s2*t1*(t1 - t2)*Power(t2,3)*
             (Power(s2,4)*t1*(8*t1 - 13*t2) + 2*Power(s2,5)*t2 + 
               8*Power(t1,2)*Power(t1 - t2,3)*(t1 + t2) - 
               2*s2*t1*Power(t1 - t2,2)*
                (16*Power(t1,2) + 6*t1*t2 - 5*Power(t2,2)) + 
               Power(s2,3)*(-32*Power(t1,3) + 40*Power(t1,2)*t2 - 
                  5*Power(t2,3)) + 
               Power(s2,2)*(48*Power(t1,4) - 65*Power(t1,3)*t2 - 
                  2*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                  3*Power(t2,4))) + 
            Power(s1,5)*(s2 - t1)*
             (2*Power(s2,4)*Power(t1 - t2,2) + 
               Power(t1,2)*Power(t2,2)*
                (13*Power(t1,2) - 21*t1*t2 + 8*Power(t2,2)) - 
               Power(s2,3)*(7*Power(t1,3) - 38*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(8*Power(t1,4) - 55*Power(t1,3)*t2 + 
                  63*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               s2*t1*(-3*Power(t1,4) + 21*Power(t1,3)*t2 - 
                  55*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(s1,4)*(Power(s2,6)*
                (Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) - 
               Power(s2,5)*(6*Power(t1,3) + 25*Power(t1,2)*t2 - 
                  42*t1*Power(t2,2) + 9*Power(t2,3)) + 
               2*Power(s2,4)*(7*Power(t1,4) + 45*Power(t1,3)*t2 - 
                  111*Power(t1,2)*Power(t2,2) + 45*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(t1,3)*t2*
                (-5*Power(t1,4) + 5*Power(t1,3)*t2 + 
                  40*Power(t1,2)*Power(t2,2) - 72*t1*Power(t2,3) + 
                  32*Power(t2,4)) + 
               Power(s2,2)*t1*
                (9*Power(t1,5) + 61*Power(t1,4)*t2 - 
                  279*Power(t1,3)*Power(t2,2) + 
                  351*Power(t1,2)*Power(t2,3) - 89*t1*Power(t2,4) - 
                  46*Power(t2,5)) + 
               s2*Power(t1,2)*
                (-2*Power(t1,5) + 60*Power(t1,3)*Power(t2,2) - 
                  196*Power(t1,2)*Power(t2,3) + 147*t1*Power(t2,4) - 
                  6*Power(t2,5)) + 
               Power(s2,3)*(-16*Power(t1,5) - 123*Power(t1,4)*t2 + 
                  397*Power(t1,3)*Power(t2,2) - 
                  277*Power(t1,2)*Power(t2,3) + t1*Power(t2,4) + 
                  20*Power(t2,5))) - 
            s1*Power(t2,2)*(8*Power(t1,3)*Power(t1 - t2,4)*t2*(t1 + t2) - 
               6*s2*Power(t1,2)*Power(t1 - t2,4)*
                (2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,6)*(5*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,5)*(12*Power(t1,4) + 43*Power(t1,3)*t2 - 
                  68*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (39*Power(t1,4) - 52*Power(t1,3)*t2 - 
                  65*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,4)*(6*Power(t1,5) - 147*Power(t1,4)*t2 + 
                  196*Power(t1,3)*Power(t2,2) - 
                  60*Power(t1,2)*Power(t2,3) + 2*Power(t2,5)) + 
               Power(s2,3)*(-40*Power(t1,6) + 211*Power(t1,5)*t2 - 
                  227*Power(t1,4)*Power(t2,2) + 
                  18*Power(t1,3)*Power(t2,3) + 
                  43*Power(t1,2)*Power(t2,4) - 7*t1*Power(t2,5) + 
                  2*Power(t2,6))) + 
            Power(s1,2)*t2*(2*Power(t1,3)*Power(t1 - t2,3)*t2*
                (5*Power(t1,2) - 6*t1*t2 - 16*Power(t2,2)) + 
               Power(s2,6)*(-6*Power(t1,3) + Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) - 9*Power(t2,3)) + 
               Power(s2,5)*(28*Power(t1,4) + 21*Power(t1,3)*t2 - 
                  118*Power(t1,2)*Power(t2,2) + 76*t1*Power(t2,3) - 
                  11*Power(t2,4)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (4*Power(t1,4) + 64*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) + 64*t1*Power(t2,3) + 
                  4*Power(t2,4)) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*
                (2*Power(t1,4) + 13*Power(t1,3)*t2 - 
                  65*Power(t1,2)*Power(t2,2) - 52*t1*Power(t2,3) + 
                  39*Power(t2,4)) + 
               Power(s2,4)*(-46*Power(t1,5) - 89*Power(t1,4)*t2 + 
                  351*Power(t1,3)*Power(t2,2) - 
                  279*Power(t1,2)*Power(t2,3) + 61*t1*Power(t2,4) + 
                  9*Power(t2,5)) + 
               Power(s2,3)*(30*Power(t1,6) + 122*Power(t1,5)*t2 - 
                  389*Power(t1,4)*Power(t2,2) + 
                  340*Power(t1,3)*Power(t2,3) - 
                  118*Power(t1,2)*Power(t2,4) + t1*Power(t2,5) + 
                  11*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,6)*
                (Power(t1,3) - 3*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  9*Power(t2,3)) - 
               Power(t1,3)*Power(t1 - t2,2)*t2*
                (3*Power(t1,3) - 19*Power(t1,2)*t2 - 17*t1*Power(t2,2) + 
                  48*Power(t2,3)) + 
               Power(s2,5)*(-8*Power(t1,4) + 7*Power(t1,3)*t2 + 
                  86*Power(t1,2)*Power(t2,2) - 93*t1*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,4)*(20*Power(t1,5) + Power(t1,4)*t2 - 
                  277*Power(t1,3)*Power(t2,2) + 
                  397*Power(t1,2)*Power(t2,3) - 123*t1*Power(t2,4) - 
                  16*Power(t2,5)) - 
               2*Power(s2,3)*(11*Power(t1,6) + 5*Power(t1,5)*t2 - 
                  168*Power(t1,4)*Power(t2,2) + 
                  306*Power(t1,3)*Power(t2,3) - 
                  168*Power(t1,2)*Power(t2,4) + 5*t1*Power(t2,5) + 
                  11*Power(t2,6)) + 
               Power(s2,2)*t1*
                (11*Power(t1,6) + Power(t1,5)*t2 - 
                  118*Power(t1,4)*Power(t2,2) + 
                  340*Power(t1,3)*Power(t2,3) - 
                  389*Power(t1,2)*Power(t2,4) + 122*t1*Power(t2,5) + 
                  30*Power(t2,6)) + 
               s2*Power(t1,2)*(-2*Power(t1,6) + 7*Power(t1,5)*t2 - 
                  43*Power(t1,4)*Power(t2,2) - 
                  18*Power(t1,3)*Power(t2,3) + 
                  227*Power(t1,2)*Power(t2,4) - 211*t1*Power(t2,5) + 
                  40*Power(t2,6))))))/
     (s*s1*s2*(s2 - t1)*t1*(s + t1)*(s - s2 + t1)*(s1 - t2)*(t1 - t2)*
       (s1 + t1 - t2)*(s1 - s2 + t1 - t2)*t2*(s + t2)*(s - s1 + t2)*
       (s2 - t1 + t2)) + (4*Power(Pi,2)*
       (16*Power(s1,2)*Power(s2,2)*(s1 + s2)*(s2 - t1)*t1*(s1 - t2)*
          Power(s1 + t1 - t2,2)*t2*Power(s2 - t1 + t2,2)*
          Power(s1*(-s2 + t1) + s2*t2,2) - 
         4*s*Power(s1,2)*Power(s2,2)*(s1 + s2)*t1*Power(s1 + t1 - t2,2)*
          t2*Power(s2 - t1 + t2,2)*
          (7*Power(s1,3)*Power(s2 - t1,2) + 
            s2*Power(t2,2)*(7*Power(s2,2) - 2*t1*(t1 - 3*t2) - 
               5*s2*(t1 + 2*t2)) + 
            Power(s1,2)*(s2 - t1)*
             (7*Power(s2,2) + 5*t1*(2*t1 + t2) - s2*(17*t1 + 24*t2)) + 
            s1*t2*(-14*Power(s2,3) + 2*Power(t1,2)*(3*t1 - t2) - 
               21*s2*t1*(t1 + t2) + Power(s2,2)*(29*t1 + 27*t2))) + 
         Power(s,8)*(s2*t1*Power(t1 - t2,3)*Power(t2,2)*
             Power(s2 - t1 + t2,2) + 
            Power(s1,3)*(-(Power(t1,2)*Power(t1 - t2,3)*t2) + 
               2*s2*Power(t1 - t2,2)*
                (Power(t1,3) - 3*t1*Power(t2,2) + Power(t2,3)) + 
               2*Power(s2,3)*
                (Power(t1,3) - 3*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,4) + 27*Power(t1,3)*t2 - 
                  11*Power(t1,2)*Power(t2,2) - 16*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            s1*Power(t1 - t2,2)*
             (-(Power(t1,2)*Power(t1 - t2,3)*t2) + 
               2*s2*Power(t1 - t2,2)*(Power(t1,3) + Power(t2,3)) + 
               2*Power(s2,3)*
                (Power(t1,3) - 3*Power(t1,2)*t2 + Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,4) + 27*Power(t1,3)*t2 - 
                  23*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,2)*(t1 - t2)*
             (-2*Power(t1,2)*Power(t1 - t2,3)*t2 + 
               s2*Power(t1 - t2,2)*
                (4*Power(t1,3) - 23*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*(4*Power(t1,3) - 12*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(-8*Power(t1,4) + 54*Power(t1,3)*t2 - 
                  54*t1*Power(t2,3) + 8*Power(t2,4)))) + 
         Power(s,7)*(-(s2*t1*Power(t1 - t2,2)*t2*Power(s2 - t1 + t2,2)*
               (s2*(6*Power(t1,2) - 5*t1*t2 - 2*Power(t2,2)) + 
                 t2*(-3*Power(t1,2) + t1*t2 + 2*Power(t2,2)))) - 
            s1*(t1 - t2)*(Power(t1,2)*Power(t1 - t2,4)*t2*
                (2*t1 + 3*t2) + 
               Power(s2,4)*(10*Power(t1,4) - 30*Power(t1,3)*t2 - 
                  10*Power(t1,2)*Power(t2,2) + 23*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (18*Power(t1,4) - 58*Power(t1,3)*t2 - 
                  115*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*Power(t1 - t2,3)*
                (4*Power(t1,4) - 7*Power(t1,3)*t2 - 
                  8*Power(t1,2)*Power(t2,2) - 7*t1*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s2,3)*(24*Power(t1,5) - 119*Power(t1,4)*t2 + 
                  77*Power(t1,3)*Power(t2,2) + 
                  57*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) + 
                  10*Power(t2,5))) + 
            Power(s1,4)*(8*Power(s2,4)*Power(t1 + t2,2) + 
               t1*Power(t1 - t2,2)*t2*
                (2*Power(t1,2) + 5*t1*t2 - 6*Power(t2,2)) + 
               Power(s2,3)*(-23*Power(t1,3) + 28*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(22*Power(t1,4) - 124*Power(t1,3)*t2 + 
                  81*Power(t1,2)*Power(t2,2) + 78*t1*Power(t2,3) - 
                  12*Power(t2,4)) - 
               s2*(7*Power(t1,5) - 30*Power(t1,4)*t2 + 
                  33*Power(t1,3)*Power(t2,2) + 
                  20*Power(t1,2)*Power(t2,3) - 40*t1*Power(t2,4) + 
                  10*Power(t2,5))) + 
            Power(s1,3)*(t1*Power(t1 - t2,3)*t2*
                (2*Power(t1,2) + 7*t1*t2 - 12*Power(t2,2)) + 
               Power(s2,4)*(6*Power(t1,3) + 48*Power(t1,2)*t2 + 
                  28*t1*Power(t2,2) - 23*Power(t2,3)) - 
               s2*Power(t1 - t2,2)*
                (10*Power(t1,4) - 39*Power(t1,3)*t2 + 
                  18*Power(t1,2)*Power(t2,2) + 95*t1*Power(t2,3) - 
                  24*Power(t2,4)) - 
               Power(s2,3)*(22*Power(t1,4) + 17*Power(t1,3)*t2 - 
                  28*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) + 
                  22*Power(t2,4)) + 
               Power(s2,2)*(26*Power(t1,5) - 204*Power(t1,4)*t2 + 
                  437*Power(t1,3)*Power(t2,2) - 
                  84*Power(t1,2)*Power(t2,3) - 200*t1*Power(t2,4) + 
                  25*Power(t2,5))) + 
            Power(s1,2)*(-(t1*Power(t1 - t2,4)*t2*
                  (2*Power(t1,2) + t1*t2 + 6*Power(t2,2))) - 
               2*Power(s2,2)*Power(t1 - t2,2)*
                (7*Power(t1,4) - 5*Power(t1,3)*t2 - 
                  283*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               s2*Power(t1 - t2,3)*
                (Power(t1,4) + 9*Power(t1,3)*t2 - 
                  115*Power(t1,2)*Power(t2,2) - 58*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,4)*(-12*Power(t1,4) + 78*Power(t1,3)*t2 + 
                  81*Power(t1,2)*Power(t2,2) - 124*t1*Power(t2,3) + 
                  22*Power(t2,4)) + 
               Power(s2,3)*(25*Power(t1,5) - 200*Power(t1,4)*t2 - 
                  84*Power(t1,3)*Power(t2,2) + 
                  437*Power(t1,2)*Power(t2,3) - 204*t1*Power(t2,4) + 
                  26*Power(t2,5)))) + 
         Power(s,6)*(s2*t1*Power(t1 - t2,2)*t2*Power(s2 - t1 + t2,2)*
             (Power(s2,2)*(12*Power(t1,2) - 21*t1*t2 + 2*Power(t2,2)) - 
               t2*(-2*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(-10*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,5)*(6*Power(s2,5)*(t1 + t2) + 
               t1*Power(t1 - t2,2)*t2*
                (2*Power(t1,2) - 21*t1*t2 + 12*Power(t2,2)) - 
               Power(s2,4)*(35*Power(t1,2) + 52*t1*t2 + 
                  12*Power(t2,2)) + 
               2*Power(s2,3)*
                (30*Power(t1,3) - 23*Power(t1,2)*t2 - 
                  62*t1*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s2,2)*(-39*Power(t1,4) + 208*Power(t1,3)*t2 - 
                  188*Power(t1,2)*Power(t2,2) - 144*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               2*s2*(4*Power(t1,5) - 35*Power(t1,4)*t2 + 
                  74*Power(t1,3)*Power(t2,2) - 
                  9*Power(t1,2)*Power(t2,3) - 33*t1*Power(t2,4) + 
                  7*Power(t2,5))) + 
            Power(s1,4)*(-(Power(s2,5)*
                  (12*Power(t1,2) + 52*t1*t2 + 35*Power(t2,2))) + 
               t1*Power(t1 - t2,2)*t2*
                (10*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  58*t1*Power(t2,2) - 34*Power(t2,3)) + 
               9*Power(s2,4)*
                (3*Power(t1,3) - 13*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,3)*(-17*Power(t1,4) + 304*Power(t1,3)*t2 - 
                  126*Power(t1,2)*Power(t2,2) - 120*t1*Power(t2,3) + 
                  115*Power(t2,4)) + 
               Power(s2,2)*(Power(t1,5) + 41*Power(t1,4)*t2 - 
                  701*Power(t1,3)*Power(t2,2) + 
                  590*Power(t1,2)*Power(t2,3) + 221*t1*Power(t2,4) + 
                  9*Power(t2,5)) + 
               s2*(Power(t1,6) - 58*Power(t1,5)*t2 + 
                  300*Power(t1,4)*Power(t2,2) - 
                  282*Power(t1,3)*Power(t2,3) - 
                  159*Power(t1,2)*Power(t2,4) + 242*t1*Power(t2,5) - 
                  44*Power(t2,6))) + 
            s1*(-(Power(t1,2)*Power(t1 - t2,5)*t2*
                  (Power(t1,2) + 6*t1*t2 + 2*Power(t2,2))) + 
               Power(s2,3)*Power(t1 - t2,2)*
                (48*Power(t1,5) - 180*Power(t1,4)*t2 - 
                  241*Power(t1,3)*Power(t2,2) + 
                  133*Power(t1,2)*Power(t2,3) + 88*t1*Power(t2,4) - 
                  20*Power(t2,5)) - 
               Power(s2,2)*Power(t1 - t2,3)*
                (20*Power(t1,5) - 57*Power(t1,4)*t2 - 
                  191*Power(t1,3)*Power(t2,2) - 
                  249*Power(t1,2)*Power(t2,3) + 67*t1*Power(t2,4) - 
                  11*Power(t2,5)) + 
               s2*Power(t1 - t2,4)*
                (2*Power(t1,5) - 8*Power(t1,4)*t2 - 
                  45*Power(t1,3)*Power(t2,2) - 
                  45*Power(t1,2)*Power(t2,3) - 8*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               2*Power(s2,5)*
                (7*Power(t1,5) - 33*Power(t1,4)*t2 - 
                  9*Power(t1,3)*Power(t2,2) + 
                  74*Power(t1,2)*Power(t2,3) - 35*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(s2,4)*(-44*Power(t1,6) + 242*Power(t1,5)*t2 - 
                  159*Power(t1,4)*Power(t2,2) - 
                  282*Power(t1,3)*Power(t2,3) + 
                  300*Power(t1,2)*Power(t2,4) - 58*t1*Power(t2,5) + 
                  Power(t2,6))) - 
            Power(s1,3)*(-(t1*Power(t1 - t2,3)*t2*
                  (13*Power(t1,3) + 5*Power(t1,2)*t2 + 
                    15*t1*Power(t2,2) - 32*Power(t2,3))) + 
               2*Power(s2,5)*
                (14*Power(t1,3) + 62*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 30*Power(t2,3)) + 
               Power(s2,4)*(-115*Power(t1,4) + 120*Power(t1,3)*t2 + 
                  126*Power(t1,2)*Power(t2,2) - 304*t1*Power(t2,3) + 
                  17*Power(t2,4)) + 
               s2*Power(t1 - t2,2)*
                (20*Power(t1,5) - 88*Power(t1,4)*t2 - 
                  133*Power(t1,3)*Power(t2,2) + 
                  241*Power(t1,2)*Power(t2,3) + 180*t1*Power(t2,4) - 
                  48*Power(t2,5)) + 
               Power(s2,3)*(166*Power(t1,5) - 896*Power(t1,4)*t2 + 
                  791*Power(t1,3)*Power(t2,2) + 
                  791*Power(t1,2)*Power(t2,3) - 896*t1*Power(t2,4) + 
                  166*Power(t2,5)) + 
               Power(s2,2)*(-99*Power(t1,6) + 681*Power(t1,5)*t2 - 
                  187*Power(t1,4)*Power(t2,2) - 
                  2154*Power(t1,3)*Power(t2,3) + 
                  1956*Power(t1,2)*Power(t2,4) - 238*t1*Power(t2,5) + 
                  41*Power(t2,6))) + 
            Power(s1,2)*(t1*Power(t1 - t2,4)*t2*
                (4*Power(t1,3) + 5*Power(t1,2)*t2 - 12*t1*Power(t2,2) - 
                  10*Power(t2,3)) + 
               Power(s2,5)*(4*Power(t1,4) - 144*Power(t1,3)*t2 - 
                  188*Power(t1,2)*Power(t2,2) + 208*t1*Power(t2,3) - 
                  39*Power(t2,4)) - 
               s2*Power(t1 - t2,3)*
                (11*Power(t1,5) - 67*Power(t1,4)*t2 + 
                  249*Power(t1,3)*Power(t2,2) + 
                  191*Power(t1,2)*Power(t2,3) + 57*t1*Power(t2,4) - 
                  20*Power(t2,5)) + 
               Power(s2,4)*(9*Power(t1,5) + 221*Power(t1,4)*t2 + 
                  590*Power(t1,3)*Power(t2,2) - 
                  701*Power(t1,2)*Power(t2,3) + 41*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               3*Power(s2,2)*Power(t1 - t2,2)*
                (13*Power(t1,5) - 103*Power(t1,4)*t2 + 
                  331*Power(t1,3)*Power(t2,2) + 
                  331*Power(t1,2)*Power(t2,3) - 103*t1*Power(t2,4) + 
                  13*Power(t2,5)) + 
               Power(s2,3)*(-41*Power(t1,6) + 238*Power(t1,5)*t2 - 
                  1956*Power(t1,4)*Power(t2,2) + 
                  2154*Power(t1,3)*Power(t2,3) + 
                  187*Power(t1,2)*Power(t2,4) - 681*t1*Power(t2,5) + 
                  99*Power(t2,6)))) - 
         Power(s,5)*(s2*t1*Power(t1 - t2,2)*t2*Power(s2 - t1 + t2,2)*
             (t1*Power(t2,2)*(-4*Power(t1,2) + t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(6*Power(t1,2) - 23*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(-10*Power(t1,3) - 7*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(4*Power(t1,4) + 24*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(s1,6)*(2*Power(s2,5)*(6*t1 + 7*t2) + 
               Power(s2,4)*(-45*Power(t1,2) - 66*t1*t2 + 
                  4*Power(t2,2)) + 
               t1*Power(t1 - t2,2)*t2*
                (8*Power(t1,2) - 23*t1*t2 + 6*Power(t2,2)) + 
               2*Power(s2,3)*
                (28*Power(t1,3) - 9*Power(t1,2)*t2 - 
                  72*t1*Power(t2,2) - 14*Power(t2,3)) - 
               Power(s2,2)*(25*Power(t1,4) - 148*Power(t1,3)*t2 + 
                  188*Power(t1,2)*Power(t2,2) + 124*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               2*s2*(Power(t1,5) - 35*Power(t1,4)*t2 + 
                  104*Power(t1,3)*Power(t2,2) - 
                  23*Power(t1,2)*Power(t2,3) - 26*t1*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(s1,5)*(2*Power(s2,6)*(7*t1 + 6*t2) - 
               Power(s2,5)*(79*Power(t1,2) + 142*t1*t2 + 
                  79*Power(t2,2)) + 
               Power(s2,4)*(159*Power(t1,3) - 114*Power(t1,2)*t2 - 
                  194*t1*Power(t2,2) - 69*Power(t2,3)) + 
               t1*Power(t1 - t2,2)*t2*
                (20*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  51*t1*Power(t2,2) - 22*Power(t2,3)) + 
               Power(s2,3)*(-153*Power(t1,4) + 668*Power(t1,3)*t2 - 
                  296*Power(t1,2)*Power(t2,2) - 196*t1*Power(t2,3) + 
                  123*Power(t2,4)) + 
               Power(s2,2)*(75*Power(t1,5) - 462*Power(t1,4)*t2 - 
                  104*Power(t1,3)*Power(t2,2) + 
                  844*Power(t1,2)*Power(t2,3) + 58*t1*Power(t2,4) + 
                  77*Power(t2,5)) - 
               2*s2*(8*Power(t1,6) - 33*Power(t1,5)*t2 - 
                  84*Power(t1,4)*Power(t2,2) + 
                  206*Power(t1,3)*Power(t2,3) + 
                  12*Power(t1,2)*Power(t2,4) - 97*t1*Power(t2,5) + 
                  12*Power(t2,6))) - 
            Power(s1,3)*(2*Power(s2,6)*
                (14*Power(t1,3) + 72*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 28*Power(t2,3)) + 
               t1*Power(t1 - t2,3)*t2*
                (4*Power(t1,4) - 2*Power(t1,3)*t2 + 
                  72*Power(t1,2)*Power(t2,2) - 45*t1*Power(t2,3) - 
                  18*Power(t2,4)) + 
               Power(s2,5)*(-123*Power(t1,4) + 196*Power(t1,3)*t2 + 
                  296*Power(t1,2)*Power(t2,2) - 668*t1*Power(t2,3) + 
                  153*Power(t2,4)) + 
               Power(s2,4)*(191*Power(t1,5) - 1170*Power(t1,4)*t2 + 
                  1898*Power(t1,2)*Power(t2,3) - 1043*t1*Power(t2,4) + 
                  269*Power(t2,5)) + 
               Power(s2,3)*(-121*Power(t1,6) + 907*Power(t1,5)*t2 + 
                  1627*Power(t1,4)*Power(t2,2) - 
                  4972*Power(t1,3)*Power(t2,3) + 
                  1627*Power(t1,2)*Power(t2,4) + 907*t1*Power(t2,5) - 
                  121*Power(t2,6)) + 
               s2*Power(t1 - t2,2)*
                (4*Power(t1,6) - 89*Power(t1,5)*t2 + 
                  634*Power(t1,4)*Power(t2,2) - 
                  392*Power(t1,3)*Power(t2,3) - 
                  258*Power(t1,2)*Power(t2,4) - 99*t1*Power(t2,5) + 
                  24*Power(t2,6)) + 
               Power(s2,2)*(21*Power(t1,7) - 
                  2357*Power(t1,5)*Power(t2,2) + 
                  4004*Power(t1,4)*Power(t2,3) + 
                  646*Power(t1,3)*Power(t2,4) - 
                  3256*Power(t1,2)*Power(t2,5) + 1099*t1*Power(t2,6) - 
                  157*Power(t2,7))) + 
            s1*(Power(t1,3)*Power(t1 - t2,5)*Power(t2,2)*(3*t1 + 4*t2) + 
               s2*t1*Power(t1 - t2,4)*t2*
                (Power(t1,4) + 54*Power(t1,3)*t2 + 
                  72*Power(t1,2)*Power(t2,2) + 54*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               2*Power(s2,6)*
                (3*Power(t1,5) - 26*Power(t1,4)*t2 - 
                  23*Power(t1,3)*Power(t2,2) + 
                  104*Power(t1,2)*Power(t2,3) - 35*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               Power(s2,2)*Power(t1 - t2,3)*
                (6*Power(t1,6) - 12*Power(t1,5)*t2 - 
                  166*Power(t1,4)*Power(t2,2) - 
                  304*Power(t1,3)*Power(t2,3) - 
                  118*Power(t1,2)*Power(t2,4) + 59*t1*Power(t2,5) - 
                  8*Power(t2,6)) - 
               Power(s2,3)*Power(t1 - t2,2)*
                (24*Power(t1,6) - 99*Power(t1,5)*t2 - 
                  258*Power(t1,4)*Power(t2,2) - 
                  392*Power(t1,3)*Power(t2,3) + 
                  634*Power(t1,2)*Power(t2,4) - 89*t1*Power(t2,5) + 
                  4*Power(t2,6)) - 
               2*Power(s2,5)*
                (12*Power(t1,6) - 97*Power(t1,5)*t2 + 
                  12*Power(t1,4)*Power(t2,2) + 
                  206*Power(t1,3)*Power(t2,3) - 
                  84*Power(t1,2)*Power(t2,4) - 33*t1*Power(t2,5) + 
                  8*Power(t2,6)) + 
               Power(s2,4)*(36*Power(t1,7) - 260*Power(t1,6)*t2 + 
                  93*Power(t1,5)*Power(t2,2) + 
                  285*Power(t1,4)*Power(t2,3) + 
                  334*Power(t1,3)*Power(t2,4) - 
                  775*Power(t1,2)*Power(t2,5) + 317*t1*Power(t2,6) - 
                  30*Power(t2,7))) + 
            Power(s1,2)*(-(t1*Power(t1 - t2,4)*t2*
                  (4*Power(t1,4) + 15*Power(t1,3)*t2 + 
                    4*Power(t1,2)*Power(t2,2) - 24*t1*Power(t2,3) - 
                    4*Power(t2,4))) - 
               Power(s2,6)*(12*Power(t1,4) + 124*Power(t1,3)*t2 + 
                  188*Power(t1,2)*Power(t2,2) - 148*t1*Power(t2,3) + 
                  25*Power(t2,4)) + 
               Power(s2,5)*(77*Power(t1,5) + 58*Power(t1,4)*t2 + 
                  844*Power(t1,3)*Power(t2,2) - 
                  104*Power(t1,2)*Power(t2,3) - 462*t1*Power(t2,4) + 
                  75*Power(t2,5)) + 
               s2*Power(t1 - t2,3)*
                (8*Power(t1,6) - 59*Power(t1,5)*t2 + 
                  118*Power(t1,4)*Power(t2,2) + 
                  304*Power(t1,3)*Power(t2,3) + 
                  166*Power(t1,2)*Power(t2,4) + 12*t1*Power(t2,5) - 
                  6*Power(t2,6)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (64*Power(t1,6) - 430*Power(t1,5)*t2 + 
                  645*Power(t1,4)*Power(t2,2) + 
                  1192*Power(t1,3)*Power(t2,3) + 
                  645*Power(t1,2)*Power(t2,4) - 430*t1*Power(t2,5) + 
                  64*Power(t2,6)) + 
               Power(s2,4)*(-166*Power(t1,6) + 694*Power(t1,5)*t2 - 
                  2734*Power(t1,4)*Power(t2,2) + 
                  828*Power(t1,3)*Power(t2,3) + 
                  2218*Power(t1,2)*Power(t2,4) - 1202*t1*Power(t2,5) + 
                  149*Power(t2,6)) + 
               Power(s2,3)*(157*Power(t1,7) - 1099*Power(t1,6)*t2 + 
                  3256*Power(t1,5)*Power(t2,2) - 
                  646*Power(t1,4)*Power(t2,3) - 
                  4004*Power(t1,3)*Power(t2,4) + 
                  2357*Power(t1,2)*Power(t2,5) - 21*Power(t2,7))) + 
            Power(s1,4)*(Power(s2,6)*
                (4*Power(t1,2) - 66*t1*t2 - 45*Power(t2,2)) - 
               Power(s2,5)*(69*Power(t1,3) + 194*Power(t1,2)*t2 + 
                  114*t1*Power(t2,2) - 159*Power(t2,3)) + 
               t1*Power(t1 - t2,2)*t2*
                (12*Power(t1,4) + 22*Power(t1,3)*t2 - 
                  56*Power(t1,2)*Power(t2,2) - 17*t1*Power(t2,3) + 
                  30*Power(t2,4)) + 
               Power(s2,4)*(215*Power(t1,4) + 138*Power(t1,3)*t2 - 
                  102*Power(t1,2)*Power(t2,2) + 138*t1*Power(t2,3) + 
                  215*Power(t2,4)) + 
               Power(s2,3)*(-269*Power(t1,5) + 1043*Power(t1,4)*t2 - 
                  1898*Power(t1,3)*Power(t2,2) + 1170*t1*Power(t2,4) - 
                  191*Power(t2,5)) + 
               Power(s2,2)*(149*Power(t1,6) - 1202*Power(t1,5)*t2 + 
                  2218*Power(t1,4)*Power(t2,2) + 
                  828*Power(t1,3)*Power(t2,3) - 
                  2734*Power(t1,2)*Power(t2,4) + 694*t1*Power(t2,5) - 
                  166*Power(t2,6)) + 
               s2*(-30*Power(t1,7) + 317*Power(t1,6)*t2 - 
                  775*Power(t1,5)*Power(t2,2) + 
                  334*Power(t1,4)*Power(t2,3) + 
                  285*Power(t1,3)*Power(t2,4) + 
                  93*Power(t1,2)*Power(t2,5) - 260*t1*Power(t2,6) + 
                  36*Power(t2,7)))) + 
         Power(s,2)*(-(Power(s2,3)*(s2 - t1)*t1*Power(t1 - t2,2)*
               Power(t2,4)*Power(s2 - t1 + t2,2)*
               (2*Power(s2,2) - 2*t1*(4*t1 + t2) + s2*(8*t1 + t2))) + 
            Power(s1,8)*(s2 - t1)*
             (2*Power(s2,5)*t2 + 2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               Power(s2,4)*(Power(t1,2) + t1*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(-3*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*t1*
                (3*Power(t1,3) + 7*Power(t1,2)*t2 - 33*t1*Power(t2,2) + 
                  5*Power(t2,3)) - 
               s2*Power(t1,2)*
                (Power(t1,3) + 4*Power(t1,2)*t2 - 27*t1*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(s1,7)*(6*Power(s2,7)*(t1 + t2) - 
               Power(t1,4)*Power(t1 - t2,2)*t2*(5*t1 + 2*t2) + 
               Power(s2,6)*(-32*Power(t1,2) + 45*t1*t2 - 
                  5*Power(t2,2)) + 
               Power(s2,5)*(72*Power(t1,3) - 260*Power(t1,2)*t2 + 
                  65*t1*Power(t2,2) - 28*Power(t2,3)) + 
               Power(s2,3)*t1*
                (62*Power(t1,4) - 439*Power(t1,3)*t2 + 
                  365*Power(t1,2)*Power(t2,2) + 136*t1*Power(t2,3) - 
                  53*Power(t2,4)) + 
               s2*Power(t1,3)*
                (4*Power(t1,4) - 23*Power(t1,3)*t2 - 
                  16*Power(t1,2)*Power(t2,2) + 83*t1*Power(t2,3) - 
                  16*Power(t2,4)) - 
               Power(s2,4)*(88*Power(t1,4) - 488*Power(t1,3)*t2 + 
                  270*Power(t1,2)*Power(t2,2) + 27*t1*Power(t2,3) + 
                  17*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-24*Power(t1,4) + 188*Power(t1,3)*t2 - 
                  147*Power(t1,2)*Power(t2,2) - 163*t1*Power(t2,3) + 
                  43*Power(t2,4))) + 
            Power(s1,6)*(2*Power(s2,8)*t1 + 
               Power(s2,7)*(-5*Power(t1,2) + 45*t1*t2 - 
                  32*Power(t2,2)) - 
               Power(t1,4)*Power(t1 - t2,2)*t2*
                (4*Power(t1,2) + 3*t1*t2 - 26*Power(t2,2)) - 
               10*Power(s2,6)*
                (Power(t1,3) + 13*Power(t1,2)*t2 + 13*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,5)*(55*Power(t1,4) - Power(t1,3)*t2 + 
                  649*Power(t1,2)*Power(t2,2) - 394*t1*Power(t2,3) + 
                  76*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-30*Power(t1,5) + 340*Power(t1,4)*t2 - 
                  885*Power(t1,3)*Power(t2,2) + 
                  523*Power(t1,2)*Power(t2,3) + 228*t1*Power(t2,4) - 
                  73*Power(t2,5)) + 
               s2*Power(t1,3)*
                (5*Power(t1,5) - 65*Power(t1,4)*t2 + 
                  162*Power(t1,3)*Power(t2,2) - 
                  95*Power(t1,2)*Power(t2,3) - 7*t1*Power(t2,4) - 
                  16*Power(t2,5)) + 
               Power(s2,4)*(-90*Power(t1,5) + 410*Power(t1,4)*t2 - 
                  1326*Power(t1,3)*Power(t2,2) + 
                  929*Power(t1,2)*Power(t2,3) - 56*t1*Power(t2,4) + 
                  54*Power(t2,5)) + 
               Power(s2,3)*t1*
                (73*Power(t1,5) - 595*Power(t1,4)*t2 + 
                  1557*Power(t1,3)*Power(t2,2) - 
                  981*Power(t1,2)*Power(t2,3) - 340*t1*Power(t2,4) + 
                  165*Power(t2,5))) + 
            s1*Power(s2,2)*Power(t2,3)*
             (-2*Power(t1,3)*(4*t1 - 5*t2)*Power(t1 - t2,4)*t2 + 
               Power(s2,6)*(8*Power(t1,3) - 31*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + Power(t2,3)) + 
               s2*Power(t1,2)*Power(t1 - t2,3)*
                (16*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  60*t1*Power(t2,2) + 15*Power(t2,3)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (56*Power(t1,4) - 64*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s2,5)*(-16*Power(t1,4) + 83*Power(t1,3)*t2 - 
                  16*Power(t1,2)*Power(t2,2) - 23*t1*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s2,4)*(16*Power(t1,5) + 7*Power(t1,4)*t2 + 
                  95*Power(t1,3)*Power(t2,2) - 
                  162*Power(t1,2)*Power(t2,3) + 65*t1*Power(t2,4) - 
                  5*Power(t2,5)) + 
               Power(s2,3)*(64*Power(t1,6) - 170*Power(t1,5)*t2 + 
                  277*Power(t1,4)*Power(t2,2) - 
                  295*Power(t1,3)*Power(t2,3) + 
                  157*Power(t1,2)*Power(t2,4) - 35*t1*Power(t2,5) + 
                  2*Power(t2,6))) + 
            Power(s1,5)*(Power(s2,8)*
                (4*Power(t1,2) - t1*t2 + Power(t2,2)) - 
               Power(t1,4)*Power(t1 - t2,2)*t2*
                (Power(t1,3) - 2*Power(t1,2)*t2 - 35*t1*Power(t2,2) + 
                  46*Power(t2,3)) + 
               Power(s2,7)*(-28*Power(t1,3) + 65*Power(t1,2)*t2 - 
                  260*t1*Power(t2,2) + 72*Power(t2,3)) + 
               Power(s2,6)*(76*Power(t1,4) - 394*Power(t1,3)*t2 + 
                  649*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) + 
                  55*Power(t2,4)) - 
               Power(s2,5)*(102*Power(t1,5) - 889*Power(t1,4)*t2 + 
                  905*Power(t1,3)*Power(t2,2) + 
                  905*Power(t1,2)*Power(t2,3) - 889*t1*Power(t2,4) + 
                  102*Power(t2,5)) + 
               Power(s2,3)*t1*
                (-16*Power(t1,6) + 353*Power(t1,5)*t2 - 
                  148*Power(t1,4)*Power(t2,2) - 
                  1290*Power(t1,3)*Power(t2,3) + 
                  912*Power(t1,2)*Power(t2,4) + 438*t1*Power(t2,5) - 
                  195*Power(t2,6)) + 
               Power(s2,4)*(68*Power(t1,6) - 900*Power(t1,5)*t2 + 
                  863*Power(t1,4)*Power(t2,2) + 
                  1512*Power(t1,3)*Power(t2,3) - 
                  1841*Power(t1,2)*Power(t2,4) + 436*t1*Power(t2,5) - 
                  86*Power(t2,6)) + 
               s2*Power(t1,3)*
                (2*Power(t1,6) - 35*Power(t1,5)*t2 + 
                  157*Power(t1,4)*Power(t2,2) - 
                  295*Power(t1,3)*Power(t2,3) + 
                  277*Power(t1,2)*Power(t2,4) - 170*t1*Power(t2,5) + 
                  64*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (-4*Power(t1,6) + 24*Power(t1,5)*t2 - 
                  361*Power(t1,4)*Power(t2,2) + 
                  877*Power(t1,3)*Power(t2,3) - 
                  370*Power(t1,2)*Power(t2,4) - 271*t1*Power(t2,5) + 
                  71*Power(t2,6))) + 
            Power(s1,3)*t2*(-2*Power(t1,4)*Power(t1 - t2,4)*Power(t2,2)*
                (t1 + 4*t2) + 
               3*Power(s2,8)*
                (Power(t1,3) - 13*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  2*Power(t2,3)) - 
               s2*Power(t1,3)*Power(t1 - t2,3)*t2*
                (15*Power(t1,3) - 60*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s2,7)*(-53*Power(t1,4) + 136*Power(t1,3)*t2 + 
                  365*Power(t1,2)*Power(t2,2) - 439*t1*Power(t2,3) + 
                  62*Power(t2,4)) - 
               2*Power(s2,2)*Power(t1,2)*Power(t1 - t2,2)*
                (6*Power(t1,5) - 68*Power(t1,4)*t2 + 
                  186*Power(t1,3)*Power(t2,2) - 
                  35*Power(t1,2)*Power(t2,3) + 8*t1*Power(t2,4) - 
                  5*Power(t2,5)) + 
               Power(s2,3)*t1*Power(t1 - t2,2)*
                (12*Power(t1,5) - 241*Power(t1,4)*t2 + 
                  655*Power(t1,3)*Power(t2,2) + 
                  655*Power(t1,2)*Power(t2,3) - 241*t1*Power(t2,4) + 
                  12*Power(t2,5)) + 
               Power(s2,6)*(165*Power(t1,5) - 340*Power(t1,4)*t2 - 
                  981*Power(t1,3)*Power(t2,2) + 
                  1557*Power(t1,2)*Power(t2,3) - 595*t1*Power(t2,4) + 
                  73*Power(t2,5)) + 
               Power(s2,5)*(-195*Power(t1,6) + 438*Power(t1,5)*t2 + 
                  912*Power(t1,4)*Power(t2,2) - 
                  1290*Power(t1,3)*Power(t2,3) - 
                  148*Power(t1,2)*Power(t2,4) + 353*t1*Power(t2,5) - 
                  16*Power(t2,6)) + 
               Power(s2,4)*(80*Power(t1,7) - 75*Power(t1,6)*t2 - 
                  907*Power(t1,5)*Power(t2,2) + 
                  323*Power(t1,4)*Power(t2,3) + 
                  2105*Power(t1,3)*Power(t2,4) - 
                  1989*Power(t1,2)*Power(t2,5) + 506*t1*Power(t2,6) - 
                  33*Power(t2,7))) - 
            Power(s1,2)*s2*Power(t2,2)*
             (-2*Power(t1,4)*(5*t1 - 4*t2)*Power(t1 - t2,4)*t2 + 
               29*s2*Power(t1,3)*Power(t1 - t2,4)*t2*(t1 + t2) + 
               Power(s2,7)*(11*Power(t1,3) - 60*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,6)*(-43*Power(t1,4) + 163*Power(t1,3)*t2 + 
                  147*Power(t1,2)*Power(t2,2) - 188*t1*Power(t2,3) + 
                  24*Power(t2,4)) - 
               2*Power(s2,2)*t1*Power(t1 - t2,2)*
                (5*Power(t1,5) - 8*Power(t1,4)*t2 + 
                  35*Power(t1,3)*Power(t2,2) - 
                  186*Power(t1,2)*Power(t2,3) + 68*t1*Power(t2,4) - 
                  6*Power(t2,5)) + 
               Power(s2,5)*(73*Power(t1,5) - 228*Power(t1,4)*t2 - 
                  523*Power(t1,3)*Power(t2,2) + 
                  885*Power(t1,2)*Power(t2,3) - 340*t1*Power(t2,4) + 
                  30*Power(t2,5)) + 
               Power(s2,4)*(-71*Power(t1,6) + 271*Power(t1,5)*t2 + 
                  370*Power(t1,4)*Power(t2,2) - 
                  877*Power(t1,3)*Power(t2,3) + 
                  361*Power(t1,2)*Power(t2,4) - 24*t1*Power(t2,5) + 
                  4*Power(t2,6)) + 
               Power(s2,3)*(40*Power(t1,7) - 201*Power(t1,6)*t2 + 
                  146*Power(t1,5)*Power(t2,2) - 
                  352*Power(t1,4)*Power(t2,3) + 
                  851*Power(t1,3)*Power(t2,4) - 
                  607*Power(t1,2)*Power(t2,5) + 129*t1*Power(t2,6) - 
                  6*Power(t2,7))) + 
            Power(s1,4)*(Power(t1,4)*Power(t1 - t2,3)*Power(t2,2)*
                (3*Power(t1,2) + 9*t1*t2 - 32*Power(t2,2)) + 
               Power(s2,8)*(2*Power(t1,3) + 2*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*Power(t1,3)*Power(t1 - t2,2)*t2*
                (Power(t1,4) - 20*Power(t1,3)*t2 - 
                  3*Power(t1,2)*Power(t2,2) + 64*t1*Power(t2,3) - 
                  56*Power(t2,4)) - 
               Power(s2,7)*(17*Power(t1,4) + 27*Power(t1,3)*t2 + 
                  270*Power(t1,2)*Power(t2,2) - 488*t1*Power(t2,3) + 
                  88*Power(t2,4)) + 
               Power(s2,6)*(54*Power(t1,5) - 56*Power(t1,4)*t2 + 
                  929*Power(t1,3)*Power(t2,2) - 
                  1326*Power(t1,2)*Power(t2,3) + 410*t1*Power(t2,4) - 
                  90*Power(t2,5)) + 
               Power(s2,5)*(-86*Power(t1,6) + 436*Power(t1,5)*t2 - 
                  1841*Power(t1,4)*Power(t2,2) + 
                  1512*Power(t1,3)*Power(t2,3) + 
                  863*Power(t1,2)*Power(t2,4) - 900*t1*Power(t2,5) + 
                  68*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (6*Power(t1,7) - 129*Power(t1,6)*t2 + 
                  607*Power(t1,5)*Power(t2,2) - 
                  851*Power(t1,4)*Power(t2,3) + 
                  352*Power(t1,3)*Power(t2,4) - 
                  146*Power(t1,2)*Power(t2,5) + 201*t1*Power(t2,6) - 
                  40*Power(t2,7)) + 
               Power(s2,4)*(74*Power(t1,7) - 733*Power(t1,6)*t2 + 
                  2592*Power(t1,5)*Power(t2,2) - 
                  1962*Power(t1,4)*Power(t2,3) - 
                  1962*Power(t1,3)*Power(t2,4) + 
                  2592*Power(t1,2)*Power(t2,5) - 733*t1*Power(t2,6) + 
                  74*Power(t2,7)) + 
               Power(s2,3)*t1*
                (-33*Power(t1,7) + 506*Power(t1,6)*t2 - 
                  1989*Power(t1,5)*Power(t2,2) + 
                  2105*Power(t1,4)*Power(t2,3) + 
                  323*Power(t1,3)*Power(t2,4) - 
                  907*Power(t1,2)*Power(t2,5) - 75*t1*Power(t2,6) + 
                  80*Power(t2,7)))) + 
         Power(s,4)*(-(s2*t1*Power(t1 - t2,2)*Power(t2,2)*
               Power(s2 - t1 + t2,2)*
               (Power(s2,4)*(8*t1 - 7*t2) + 
                 2*Power(t1,2)*Power(t2,2)*(-t1 + t2) + 
                 Power(s2,3)*
                  (2*Power(t1,2) - 43*t1*t2 + 4*Power(t2,2)) + 
                 s2*t1*(12*Power(t1,3) + 8*Power(t1,2)*t2 - 
                    18*t1*Power(t2,2) - 9*Power(t2,3)) + 
                 Power(s2,2)*(-22*Power(t1,3) + 36*Power(t1,2)*t2 + 
                    33*t1*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(s1,7)*(Power(t1,2)*(7*t1 - 8*t2)*Power(t1 - t2,2)*t2 + 
               2*Power(s2,5)*(3*t1 + 5*t2) + 
               Power(s2,4)*(-17*Power(t1,2) - 40*t1*t2 + 
                  12*Power(t2,2)) + 
               2*Power(s2,3)*
                (7*Power(t1,3) + 10*Power(t1,2)*t2 - 
                  39*t1*Power(t2,2) - 3*Power(t2,3)) - 
               2*s2*t1*(Power(t1,4) + 15*Power(t1,3)*t2 - 
                  62*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) + 
                  8*Power(t2,4)) - 
               Power(s2,2)*(Power(t1,4) - 33*Power(t1,3)*t2 + 
                  81*Power(t1,2)*Power(t2,2) + 48*t1*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(s1,6)*(26*Power(s2,6)*(t1 + t2) + 
               Power(t1,2)*Power(t1 - t2,2)*t2*
                (10*Power(t1,2) + 13*t1*t2 + 14*Power(t2,2)) - 
               Power(s2,5)*(117*Power(t1,2) + 130*t1*t2 + 
                  41*Power(t2,2)) + 
               Power(s2,4)*(207*Power(t1,3) - 80*Power(t1,2)*t2 - 
                  155*t1*Power(t2,2) - 109*Power(t2,3)) + 
               Power(s2,3)*(-181*Power(t1,4) + 590*Power(t1,3)*t2 - 
                  316*Power(t1,2)*Power(t2,2) - 96*t1*Power(t2,3) + 
                  9*Power(t2,4)) - 
               2*s2*t1*(7*Power(t1,5) - 68*Power(t1,4)*t2 + 
                  58*Power(t1,3)*Power(t2,2) + 
                  115*Power(t1,2)*Power(t2,3) - 26*t1*Power(t2,4) - 
                  30*Power(t2,5)) + 
               Power(s2,2)*(79*Power(t1,5) - 552*Power(t1,4)*t2 + 
                  481*Power(t1,3)*Power(t2,2) + 
                  495*Power(t1,2)*Power(t2,3) - 37*t1*Power(t2,4) + 
                  51*Power(t2,5))) + 
            Power(s1,5)*(2*Power(s2,7)*(5*t1 + 3*t2) - 
               Power(s2,6)*(41*Power(t1,2) + 130*t1*t2 + 
                  117*Power(t2,2)) - 
               Power(t1,2)*Power(t1 - t2,2)*t2*
                (7*Power(t1,3) - 39*Power(t1,2)*t2 + 
                  103*t1*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,5)*(40*Power(t1,3) - 73*Power(t1,2)*t2 - 
                  73*t1*Power(t2,2) + 40*Power(t2,3)) + 
               Power(s2,4)*(36*Power(t1,4) + 469*Power(t1,3)*t2 - 
                  109*Power(t1,2)*Power(t2,2) - 125*t1*Power(t2,3) + 
                  332*Power(t2,4)) + 
               Power(s2,3)*(-88*Power(t1,5) + 48*Power(t1,4)*t2 - 
                  1164*Power(t1,3)*Power(t2,2) + 
                  880*Power(t1,2)*Power(t2,3) + 419*t1*Power(t2,4) + 
                  46*Power(t2,5)) + 
               Power(s2,2)*(53*Power(t1,6) - 556*Power(t1,5)*t2 + 
                  2099*Power(t1,4)*Power(t2,2) - 
                  861*Power(t1,3)*Power(t2,3) - 
                  1676*Power(t1,2)*Power(t2,4) + 531*t1*Power(t2,5) - 
                  123*Power(t2,6)) + 
               s2*t1*(-10*Power(t1,6) + 243*Power(t1,5)*t2 - 
                  881*Power(t1,4)*Power(t2,2) + 
                  730*Power(t1,3)*Power(t2,3) + 
                  111*Power(t1,2)*Power(t2,4) - 65*t1*Power(t2,5) - 
                  80*Power(t2,6))) - 
            s1*t2*(2*Power(t1,4)*Power(t1 - t2,5)*Power(t2,2) + 
               s2*Power(t1,2)*Power(t1 - t2,4)*t2*
                (15*Power(t1,3) + 52*Power(t1,2)*t2 + 
                  52*t1*Power(t2,2) + 15*Power(t2,3)) + 
               2*Power(s2,7)*
                (8*Power(t1,4) + 14*Power(t1,3)*t2 - 
                  62*Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*Power(t1 - t2,2)*t2*
                (178*Power(t1,5) + 103*Power(t1,4)*t2 + 
                  184*Power(t1,3)*Power(t2,2) - 
                  516*Power(t1,2)*Power(t2,3) + 129*t1*Power(t2,4) - 
                  12*Power(t2,5)) - 
               2*Power(s2,6)*
                (30*Power(t1,5) + 26*Power(t1,4)*t2 - 
                  115*Power(t1,3)*Power(t2,2) - 
                  58*Power(t1,2)*Power(t2,3) + 68*t1*Power(t2,4) - 
                  7*Power(t2,5)) + 
               Power(s2,2)*t1*Power(t1 - t2,3)*
                (4*Power(t1,5) - 77*Power(t1,4)*t2 - 
                  69*Power(t1,3)*Power(t2,2) - 
                  270*Power(t1,2)*Power(t2,3) + 81*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,5)*(80*Power(t1,6) + 65*Power(t1,5)*t2 - 
                  111*Power(t1,4)*Power(t2,2) - 
                  730*Power(t1,3)*Power(t2,3) + 
                  881*Power(t1,2)*Power(t2,4) - 243*t1*Power(t2,5) + 
                  10*Power(t2,6)) + 
               Power(s2,4)*(-40*Power(t1,7) - 145*Power(t1,6)*t2 + 
                  90*Power(t1,5)*Power(t2,2) + 
                  827*Power(t1,4)*Power(t2,3) - 
                  999*Power(t1,3)*Power(t2,4) + 
                  202*Power(t1,2)*Power(t2,5) + 79*t1*Power(t2,6) - 
                  14*Power(t2,7))) + 
            Power(s1,4)*(Power(s2,7)*
                (12*Power(t1,2) - 40*t1*t2 - 17*Power(t2,2)) - 
               Power(s2,6)*(109*Power(t1,3) + 155*Power(t1,2)*t2 + 
                  80*t1*Power(t2,2) - 207*Power(t2,3)) + 
               Power(s2,5)*(332*Power(t1,4) - 125*Power(t1,3)*t2 - 
                  109*Power(t1,2)*Power(t2,2) + 469*t1*Power(t2,3) + 
                  36*Power(t2,4)) - 
               Power(t1,2)*Power(t1 - t2,2)*t2*
                (16*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  80*Power(t1,2)*Power(t2,2) - 155*t1*Power(t2,3) + 
                  58*Power(t2,4)) - 
               2*Power(s2,4)*
                (237*Power(t1,5) - 859*Power(t1,4)*t2 + 
                  774*Power(t1,3)*Power(t2,2) + 
                  774*Power(t1,2)*Power(t2,3) - 859*t1*Power(t2,4) + 
                  237*Power(t2,5)) + 
               Power(s2,3)*(342*Power(t1,6) - 2276*Power(t1,5)*t2 + 
                  2292*Power(t1,4)*Power(t2,2) + 
                  3192*Power(t1,3)*Power(t2,3) - 
                  3628*Power(t1,2)*Power(t2,4) + 289*t1*Power(t2,5) - 
                  143*Power(t2,6)) + 
               s2*t1*(14*Power(t1,7) - 79*Power(t1,6)*t2 - 
                  202*Power(t1,5)*Power(t2,2) + 
                  999*Power(t1,4)*Power(t2,3) - 
                  827*Power(t1,3)*Power(t2,4) - 
                  90*Power(t1,2)*Power(t2,5) + 145*t1*Power(t2,6) + 
                  40*Power(t2,7)) + 
               Power(s2,2)*(-117*Power(t1,7) + 973*Power(t1,6)*t2 - 
                  476*Power(t1,5)*Power(t2,2) - 
                  2633*Power(t1,4)*Power(t2,3) + 
                  902*Power(t1,3)*Power(t2,4) + 
                  2211*Power(t1,2)*Power(t2,5) - 880*t1*Power(t2,6) + 
                  143*Power(t2,7))) + 
            Power(s1,3)*(Power(s2,7)*
                (-6*Power(t1,3) - 78*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,6)*(9*Power(t1,4) - 96*Power(t1,3)*t2 - 
                  316*Power(t1,2)*Power(t2,2) + 590*t1*Power(t2,3) - 
                  181*Power(t2,4)) - 
               Power(t1,2)*Power(t1 - t2,3)*t2*
                (6*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  31*Power(t1,2)*Power(t2,2) - 42*t1*Power(t2,3) + 
                  46*Power(t2,4)) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*
                (12*Power(t1,5) - 129*Power(t1,4)*t2 + 
                  516*Power(t1,3)*Power(t2,2) - 
                  184*Power(t1,2)*Power(t2,3) - 103*t1*Power(t2,4) - 
                  178*Power(t2,5)) + 
               Power(s2,5)*(46*Power(t1,5) + 419*Power(t1,4)*t2 + 
                  880*Power(t1,3)*Power(t2,2) - 
                  1164*Power(t1,2)*Power(t2,3) + 48*t1*Power(t2,4) - 
                  88*Power(t2,5)) + 
               Power(s2,4)*(-143*Power(t1,6) + 289*Power(t1,5)*t2 - 
                  3628*Power(t1,4)*Power(t2,2) + 
                  3192*Power(t1,3)*Power(t2,3) + 
                  2292*Power(t1,2)*Power(t2,4) - 2276*t1*Power(t2,5) + 
                  342*Power(t2,6)) + 
               Power(s2,3)*(154*Power(t1,7) - 1196*Power(t1,6)*t2 + 
                  5840*Power(t1,5)*Power(t2,2) - 
                  4841*Power(t1,4)*Power(t2,3) - 
                  4841*Power(t1,3)*Power(t2,4) + 
                  5840*Power(t1,2)*Power(t2,5) - 1196*t1*Power(t2,6) + 
                  154*Power(t2,7)) + 
               Power(s2,2)*(-72*Power(t1,8) + 821*Power(t1,7)*t2 - 
                  3610*Power(t1,6)*Power(t2,2) + 
                  3769*Power(t1,5)*Power(t2,3) + 
                  743*Power(t1,4)*Power(t2,4) - 
                  1106*Power(t1,3)*Power(t2,5) - 
                  1022*Power(t1,2)*Power(t2,6) + 558*t1*Power(t2,7) - 
                  81*Power(t2,8))) - 
            Power(s1,2)*(-(Power(t1,2)*Power(t1 - t2,4)*Power(t2,2)*
                  (9*Power(t1,3) + 14*Power(t1,2)*t2 - 
                    8*t1*Power(t2,2) - 12*Power(t2,3))) + 
               Power(s2,7)*(8*Power(t1,4) + 48*Power(t1,3)*t2 + 
                  81*Power(t1,2)*Power(t2,2) - 33*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*t1*Power(t1 - t2,3)*t2*
                (3*Power(t1,5) + 81*Power(t1,4)*t2 - 
                  270*Power(t1,3)*Power(t2,2) - 
                  69*Power(t1,2)*Power(t2,3) - 77*t1*Power(t2,4) + 
                  4*Power(t2,5)) - 
               Power(s2,6)*(51*Power(t1,5) - 37*Power(t1,4)*t2 + 
                  495*Power(t1,3)*Power(t2,2) + 
                  481*Power(t1,2)*Power(t2,3) - 552*t1*Power(t2,4) + 
                  79*Power(t2,5)) + 
               Power(s2,5)*(123*Power(t1,6) - 531*Power(t1,5)*t2 + 
                  1676*Power(t1,4)*Power(t2,2) + 
                  861*Power(t1,3)*Power(t2,3) - 
                  2099*Power(t1,2)*Power(t2,4) + 556*t1*Power(t2,5) - 
                  53*Power(t2,6)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (18*Power(t1,7) - 91*Power(t1,6)*t2 - 
                  208*Power(t1,5)*Power(t2,2) + 
                  788*Power(t1,4)*Power(t2,3) + 
                  788*Power(t1,3)*Power(t2,4) - 
                  208*Power(t1,2)*Power(t2,5) - 91*t1*Power(t2,6) + 
                  18*Power(t2,7)) + 
               Power(s2,4)*(-143*Power(t1,7) + 880*Power(t1,6)*t2 - 
                  2211*Power(t1,5)*Power(t2,2) - 
                  902*Power(t1,4)*Power(t2,3) + 
                  2633*Power(t1,3)*Power(t2,4) + 
                  476*Power(t1,2)*Power(t2,5) - 973*t1*Power(t2,6) + 
                  117*Power(t2,7)) + 
               Power(s2,3)*(81*Power(t1,8) - 558*Power(t1,7)*t2 + 
                  1022*Power(t1,6)*Power(t2,2) + 
                  1106*Power(t1,5)*Power(t2,3) - 
                  743*Power(t1,4)*Power(t2,4) - 
                  3769*Power(t1,3)*Power(t2,5) + 
                  3610*Power(t1,2)*Power(t2,6) - 821*t1*Power(t2,7) + 
                  72*Power(t2,8)))) + 
         Power(s,3)*(-(Power(s2,2)*t1*Power(t1 - t2,2)*Power(t2,3)*
               Power(s2 - t1 + t2,2)*
               (2*Power(s2,4) + 2*Power(s2,3)*(7*t1 - 3*t2) + 
                 2*Power(t1,2)*
                  (4*Power(t1,2) - 3*t1*t2 - 2*Power(t2,2)) - 
                 Power(s2,2)*
                  (20*Power(t1,2) + 23*t1*t2 + 4*Power(t2,2)) + 
                 s2*t1*(-4*Power(t1,2) + 33*t1*t2 + 9*Power(t2,2)))) + 
            Power(s1,8)*(-2*Power(s2,5)*t2 - 
               2*Power(t1,3)*Power(t1 - t2,2)*t2 - 
               Power(s2,4)*(Power(t1,2) - 10*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(3*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  16*t1*Power(t2,2) - 2*Power(t2,3)) + 
               s2*Power(t1,2)*
                (Power(t1,3) + 4*Power(t1,2)*t2 - 27*t1*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-3*Power(t1,3) + 4*Power(t1,2)*t2 + 11*t1*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(s1,7)*(2*Power(t1,3)*(t1 - 5*t2)*Power(t1 - t2,2)*t2 - 
               4*Power(s2,6)*(3*t1 + 4*t2) + 
               Power(s2,5)*(49*Power(t1,2) + 41*t1*t2 - 7*Power(t2,2)) + 
               Power(s2,4)*(-77*Power(t1,3) + 44*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) + 34*Power(t2,3)) + 
               s2*Power(t1,2)*
                (2*Power(t1,4) - 65*Power(t1,3)*t2 + 
                  132*Power(t1,2)*Power(t2,2) + 43*t1*Power(t2,3) - 
                  32*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-19*Power(t1,4) + 202*Power(t1,3)*t2 - 
                  327*Power(t1,2)*Power(t2,2) - 92*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*(57*Power(t1,4) - 208*Power(t1,3)*t2 + 
                  168*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) + 
                  25*Power(t2,4))) + 
            Power(s1,6)*(-4*Power(s2,7)*(4*t1 + 3*t2) + 
               11*Power(s2,6)*(7*Power(t1,2) + 4*t1*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*Power(t1 - t2,2)*t2*
                (14*Power(t1,2) - 13*t1*t2 + 46*Power(t2,2)) + 
               Power(s2,5)*(-153*Power(t1,3) + 157*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 91*Power(t2,3)) + 
               Power(s2,4)*(164*Power(t1,4) - 612*Power(t1,3)*t2 + 
                  168*Power(t1,2)*Power(t2,2) + 122*t1*Power(t2,3) - 
                  97*Power(t2,4)) - 
               s2*Power(t1,2)*
                (7*Power(t1,5) + 29*Power(t1,4)*t2 - 
                  312*Power(t1,3)*Power(t2,2) + 
                  367*Power(t1,2)*Power(t2,3) + 69*t1*Power(t2,4) - 
                  80*Power(t2,5)) + 
               Power(s2,2)*t1*
                (39*Power(t1,5) - 198*Power(t1,4)*t2 - 
                  428*Power(t1,3)*Power(t2,2) + 
                  772*Power(t1,2)*Power(t2,3) + 312*t1*Power(t2,4) - 
                  78*Power(t2,5)) - 
               Power(s2,3)*(104*Power(t1,5) - 636*Power(t1,4)*t2 + 
                  74*Power(t1,3)*Power(t2,2) + 
                  708*Power(t1,2)*Power(t2,3) - 118*t1*Power(t2,4) + 
                  99*Power(t2,5))) + 
            Power(s1,5)*(-2*Power(s2,8)*t1 + 
               Power(s2,7)*(-7*Power(t1,2) + 41*t1*t2 + 49*Power(t2,2)) + 
               Power(s2,6)*(91*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  157*t1*Power(t2,2) - 153*Power(t2,3)) + 
               Power(t1,3)*Power(t1 - t2,2)*t2*
                (14*Power(t1,3) + 3*Power(t1,2)*t2 - 5*t1*Power(t2,2) - 
                  50*Power(t2,3)) + 
               Power(s2,5)*(-268*Power(t1,4) + 222*Power(t1,3)*t2 - 
                  404*Power(t1,2)*Power(t2,2) + 222*t1*Power(t2,3) - 
                  268*Power(t2,4)) + 
               Power(s2,4)*(374*Power(t1,5) - 1202*Power(t1,4)*t2 + 
                  1763*Power(t1,3)*Power(t2,2) + 
                  27*Power(t1,2)*Power(t2,3) - 741*t1*Power(t2,4) + 
                  119*Power(t2,5)) + 
               s2*Power(t1,2)*
                (-16*Power(t1,6) + 164*Power(t1,5)*t2 - 
                  338*Power(t1,4)*Power(t2,2) + 
                  115*Power(t1,3)*Power(t2,3) - 
                  52*Power(t1,2)*Power(t2,4) + 263*t1*Power(t2,5) - 
                  120*Power(t2,6)) + 
               Power(s2,2)*t1*
                (105*Power(t1,6) - 1006*Power(t1,5)*t2 + 
                  2043*Power(t1,4)*Power(t2,2) - 
                  243*Power(t1,3)*Power(t2,3) - 
                  943*Power(t1,2)*Power(t2,4) - 276*t1*Power(t2,5) + 
                  82*Power(t2,6)) + 
               Power(s2,3)*(-277*Power(t1,6) + 1781*Power(t1,5)*t2 - 
                  3245*Power(t1,4)*Power(t2,2) + 
                  13*Power(t1,3)*Power(t2,3) + 
                  2513*Power(t1,2)*Power(t2,4) - 767*t1*Power(t2,5) + 
                  185*Power(t2,6))) + 
            s1*s2*Power(t2,2)*
             (-2*Power(t1,3)*Power(t1 - t2,4)*t2*
                (5*Power(t1,2) + 8*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,7)*(6*Power(t1,3) - 27*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + Power(t2,3)) + 
               s2*Power(t1,2)*Power(t1 - t2,3)*
                (12*Power(t1,4) - 11*Power(t1,3)*t2 + 
                  78*Power(t1,2)*Power(t2,2) + 27*t1*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s2,6)*(-32*Power(t1,4) + 43*Power(t1,3)*t2 + 
                  132*Power(t1,2)*Power(t2,2) - 65*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,5)*(80*Power(t1,5) - 69*Power(t1,4)*t2 - 
                  367*Power(t1,3)*Power(t2,2) + 
                  312*Power(t1,2)*Power(t2,3) - 29*t1*Power(t2,4) - 
                  7*Power(t2,5)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (56*Power(t1,5) - 158*Power(t1,4)*t2 + 
                  414*Power(t1,3)*Power(t2,2) - 
                  319*Power(t1,2)*Power(t2,3) + 8*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,4)*(-120*Power(t1,6) + 263*Power(t1,5)*t2 - 
                  52*Power(t1,4)*Power(t2,2) + 
                  115*Power(t1,3)*Power(t2,3) - 
                  338*Power(t1,2)*Power(t2,4) + 164*t1*Power(t2,5) - 
                  16*Power(t2,6)) + 
               Power(s2,3)*(110*Power(t1,7) - 423*Power(t1,6)*t2 + 
                  898*Power(t1,5)*Power(t2,2) - 
                  1411*Power(t1,4)*Power(t2,3) + 
                  1339*Power(t1,3)*Power(t2,4) - 
                  626*Power(t1,2)*Power(t2,5) + 121*t1*Power(t2,6) - 
                  8*Power(t2,7))) + 
            Power(s1,4)*(-(Power(s2,8)*
                  (4*Power(t1,2) - 10*t1*t2 + Power(t2,2))) + 
               Power(s2,7)*(34*Power(t1,3) + 48*Power(t1,2)*t2 + 
                  44*t1*Power(t2,2) - 77*Power(t2,3)) + 
               Power(t1,3)*Power(t1 - t2,2)*t2*
                (4*Power(t1,4) - 3*Power(t1,3)*t2 - 
                  66*Power(t1,2)*Power(t2,2) + 63*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,6)*(-97*Power(t1,4) + 122*Power(t1,3)*t2 + 
                  168*Power(t1,2)*Power(t2,2) - 612*t1*Power(t2,3) + 
                  164*Power(t2,4)) + 
               Power(s2,5)*(119*Power(t1,5) - 741*Power(t1,4)*t2 + 
                  27*Power(t1,3)*Power(t2,2) + 
                  1763*Power(t1,2)*Power(t2,3) - 1202*t1*Power(t2,4) + 
                  374*Power(t2,5)) + 
               Power(s2,4)*(-47*Power(t1,6) + 710*Power(t1,5)*t2 + 
                  592*Power(t1,4)*Power(t2,2) - 
                  2784*Power(t1,3)*Power(t2,3) + 
                  592*Power(t1,2)*Power(t2,4) + 710*t1*Power(t2,5) - 
                  47*Power(t2,6)) + 
               Power(s2,3)*(-29*Power(t1,7) + 144*Power(t1,6)*t2 - 
                  2612*Power(t1,5)*Power(t2,2) + 
                  4251*Power(t1,4)*Power(t2,3) + 
                  1318*Power(t1,3)*Power(t2,4) - 
                  4139*Power(t1,2)*Power(t2,5) + 1278*t1*Power(t2,6) - 
                  181*Power(t2,7)) + 
               Power(s2,2)*t1*
                (32*Power(t1,7) - 418*Power(t1,6)*t2 + 
                  2419*Power(t1,5)*Power(t2,2) - 
                  3828*Power(t1,4)*Power(t2,3) + 
                  809*Power(t1,3)*Power(t2,4) + 
                  1244*Power(t1,2)*Power(t2,5) - 244*t1*Power(t2,6) + 
                  12*Power(t2,7)) + 
               s2*Power(t1,2)*
                (-8*Power(t1,7) + 121*Power(t1,6)*t2 - 
                  626*Power(t1,5)*Power(t2,2) + 
                  1339*Power(t1,4)*Power(t2,3) - 
                  1411*Power(t1,3)*Power(t2,4) + 
                  898*Power(t1,2)*Power(t2,5) - 423*t1*Power(t2,6) + 
                  110*Power(t2,7))) + 
            Power(s1,2)*t2*(2*Power(t1,3)*Power(t1 - t2,4)*Power(t2,2)*
                (2*Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,8)*(6*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,7)*(12*Power(t1,4) - 92*Power(t1,3)*t2 - 
                  327*Power(t1,2)*Power(t2,2) + 202*t1*Power(t2,3) - 
                  19*Power(t2,4)) + 
               s2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (30*Power(t1,4) - 27*Power(t1,3)*t2 - 
                  78*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(s2,6)*(-78*Power(t1,5) + 312*Power(t1,4)*t2 + 
                  772*Power(t1,3)*Power(t2,2) - 
                  428*Power(t1,2)*Power(t2,3) - 198*t1*Power(t2,4) + 
                  39*Power(t2,5)) + 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (20*Power(t1,6) - 193*Power(t1,5)*t2 + 
                  250*Power(t1,4)*Power(t2,2) + 
                  74*Power(t1,3)*Power(t2,3) + 
                  250*Power(t1,2)*Power(t2,4) - 193*t1*Power(t2,5) + 
                  20*Power(t2,6)) + 
               Power(s2,5)*(82*Power(t1,6) - 276*Power(t1,5)*t2 - 
                  943*Power(t1,4)*Power(t2,2) - 
                  243*Power(t1,3)*Power(t2,3) + 
                  2043*Power(t1,2)*Power(t2,4) - 1006*t1*Power(t2,5) + 
                  105*Power(t2,6)) + 
               Power(s2,4)*(12*Power(t1,7) - 244*Power(t1,6)*t2 + 
                  1244*Power(t1,5)*Power(t2,2) + 
                  809*Power(t1,4)*Power(t2,3) - 
                  3828*Power(t1,3)*Power(t2,4) + 
                  2419*Power(t1,2)*Power(t2,5) - 418*t1*Power(t2,6) + 
                  32*Power(t2,7)) + 
               Power(s2,3)*(-54*Power(t1,8) + 492*Power(t1,7)*t2 - 
                  1293*Power(t1,6)*Power(t2,2) + 
                  214*Power(t1,5)*Power(t2,3) + 
                  1360*Power(t1,4)*Power(t2,4) - 
                  266*Power(t1,3)*Power(t2,5) - 
                  647*Power(t1,2)*Power(t2,6) + 212*t1*Power(t2,7) - 
                  18*Power(t2,8))) + 
            Power(s1,3)*(Power(s2,8)*
                (-2*Power(t1,3) + 16*Power(t1,2)*t2 - 14*t1*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)*
                (9*Power(t1,3) + 16*Power(t1,2)*t2 - 49*t1*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s2,7)*(25*Power(t1,4) + 7*Power(t1,3)*t2 + 
                  168*Power(t1,2)*Power(t2,2) - 208*t1*Power(t2,3) + 
                  57*Power(t2,4)) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (3*Power(t1,5) + 8*Power(t1,4)*t2 - 
                  319*Power(t1,3)*Power(t2,2) + 
                  414*Power(t1,2)*Power(t2,3) - 158*t1*Power(t2,4) + 
                  56*Power(t2,5)) - 
               Power(s2,6)*(99*Power(t1,5) - 118*Power(t1,4)*t2 + 
                  708*Power(t1,3)*Power(t2,2) + 
                  74*Power(t1,2)*Power(t2,3) - 636*t1*Power(t2,4) + 
                  104*Power(t2,5)) + 
               Power(s2,5)*(185*Power(t1,6) - 767*Power(t1,5)*t2 + 
                  2513*Power(t1,4)*Power(t2,2) + 
                  13*Power(t1,3)*Power(t2,3) - 
                  3245*Power(t1,2)*Power(t2,4) + 1781*t1*Power(t2,5) - 
                  277*Power(t2,6)) + 
               Power(s2,4)*(-181*Power(t1,7) + 1278*Power(t1,6)*t2 - 
                  4139*Power(t1,5)*Power(t2,2) + 
                  1318*Power(t1,4)*Power(t2,3) + 
                  4251*Power(t1,3)*Power(t2,4) - 
                  2612*Power(t1,2)*Power(t2,5) + 144*t1*Power(t2,6) - 
                  29*Power(t2,7)) + 
               Power(s2,2)*t1*
                (-18*Power(t1,8) + 212*Power(t1,7)*t2 - 
                  647*Power(t1,6)*Power(t2,2) - 
                  266*Power(t1,5)*Power(t2,3) + 
                  1360*Power(t1,4)*Power(t2,4) + 
                  214*Power(t1,3)*Power(t2,5) - 
                  1293*Power(t1,2)*Power(t2,6) + 492*t1*Power(t2,7) - 
                  54*Power(t2,8)) + 
               Power(s2,3)*(90*Power(t1,8) - 861*Power(t1,7)*t2 + 
                  2838*Power(t1,6)*Power(t2,2) - 
                  1127*Power(t1,5)*Power(t2,3) - 
                  1900*Power(t1,4)*Power(t2,4) - 
                  1127*Power(t1,3)*Power(t2,5) + 
                  2838*Power(t1,2)*Power(t2,6) - 861*t1*Power(t2,7) + 
                  90*Power(t2,8))))))/
     (3.*Power(s,2)*Power(s1,2)*Power(s2,2)*Power(t1,2)*
       Power(s - s2 + t1,2)*Power(s1 + t1 - t2,2)*Power(t2,2)*
       Power(s - s1 + t2,2)*Power(s2 - t1 + t2,2)) + 
    (16*(2*Power(s1,6)*s2*t1*(-s2 + t1) + 
         Power(s,6)*(s2 + t1)*(t1 - t2)*t2 + 
         4*Power(s2,3)*t1*(-s2 + t1)*Power(t2,4) + 
         Power(s1,5)*(-2*Power(s2,4) + Power(s2,3)*t1 - 
            8*s2*Power(t1,2)*t2 - Power(t1,3)*t2 + 
            Power(s2,2)*t1*(t1 + 7*t2)) + 
         Power(s1,2)*s2*Power(t2,2)*
          (-4*Power(s2,4) + 4*Power(s2,3)*t1 + 
            4*Power(t1,2)*t2*(-t1 + t2) + 
            s2*t1*(6*Power(t1,2) - 10*t1*t2 + Power(t2,2)) - 
            Power(s2,2)*(4*Power(t1,2) - 6*t1*t2 + Power(t2,2))) + 
         s1*Power(s2,2)*Power(t2,3)*
          (2*Power(s2,3) + 2*Power(s2,2)*(t1 + t2) + 
            s2*(-2*Power(t1,2) - 8*t1*t2 + Power(t2,2)) - 
            2*t1*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
         Power(s1,3)*t2*(3*Power(s2,5) + 
            s2*Power(t1,2)*(6*t1 - 5*t2)*t2 - 
            2*Power(t1,3)*Power(t2,2) - 5*Power(s2,4)*(t1 + t2) - 
            Power(s2,2)*t1*(4*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,2) - 4*t1*t2 + Power(t2,2))) - 
         Power(s1,4)*(Power(s2,5) + s2*Power(t1,2)*(2*t1 - 9*t2)*t2 - 
            3*Power(t1,3)*Power(t2,2) - Power(s2,4)*(2*t1 + 5*t2) + 
            Power(s2,3)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*t1*(-Power(t1,2) + 8*t1*t2 + 5*Power(t2,2))) + 
         Power(s,5)*(t1*t2*(Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) - 
            s2*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
            s1*(t1*(5*t1 - 4*t2)*t2 + Power(s2,2)*(2*t1 + 3*t2) + 
               s2*(2*Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)))) + 
         Power(s,4)*(t1*Power(t2,2)*
             (3*Power(t1,2) - t1*t2 - 2*Power(t2,2)) - 
            Power(s2,3)*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            s2*t2*(-2*Power(t1,3) - 7*Power(t1,2)*t2 + 
               12*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,2)*(Power(t1,3) + 3*Power(t1,2)*t2 - 
               7*t1*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s1,2)*(-2*Power(s2,3) + 2*t1*(5*t1 - 3*t2)*t2 + 
               Power(s2,2)*(6*t1 + 9*t2) + 
               s2*(10*Power(t1,2) + 10*t1*t2 - 3*Power(t2,2))) + 
            s1*(3*s2*Power(t2,2)*(-4*t1 + t2) + 
               Power(s2,3)*(4*t1 + 6*t2) + 
               Power(s2,2)*(-3*Power(t1,2) + 11*t1*t2 - 
                  10*Power(t2,2)) + 
               t1*t2*(-5*Power(t1,2) - 8*t1*t2 + 9*Power(t2,2)))) + 
         s*(Power(s1,5)*(2*Power(s2,3) + 6*Power(s2,2)*t1 - 
               Power(t1,2)*t2 - s2*t1*(10*t1 + t2)) + 
            Power(s1,4)*(7*Power(s2,4) + 30*s2*Power(t1,2)*t2 + 
               Power(t1,2)*t2*(5*t1 + 2*t2) - 
               Power(s2,3)*(7*t1 + 8*t2) + 
               Power(s2,2)*(-3*Power(t1,2) - 20*t1*t2 + Power(t2,2))) + 
            Power(s2,2)*Power(t2,3)*
             (-6*Power(s2,2)*t1 + 
               2*t1*(2*Power(t1,2) - 2*t1*t2 - Power(t2,2)) + 
               s2*(2*Power(t1,2) + 8*t1*t2 + Power(t2,2))) + 
            Power(s1,3)*(2*Power(s2,5) + 
               Power(t1,2)*Power(t2,2)*(-12*t1 + t2) - 
               2*Power(s2,4)*(3*t1 + 8*t2) + 
               2*s2*t1*t2*(4*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(7*Power(t1,2) + 12*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,3) + 21*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 2*Power(t2,3))) + 
            2*s1*s2*Power(t2,2)*
             (Power(s2,4) - 3*Power(s2,3)*t2 + 
               t1*t2*Power(-2*t1 + t2,2) + 
               Power(s2,2)*(3*Power(t1,2) - 4*t1*t2 - 3*Power(t2,2)) - 
               s2*(6*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*t2*(-4*Power(s2,5) + 
               2*Power(t1,2)*(3*t1 - t2)*Power(t2,2) + 
               Power(s2,4)*(9*t1 + 14*t2) + 
               Power(s2,3)*(-13*Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
               s2*t1*t2*(-18*Power(t1,2) + 9*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(12*Power(t1,3) - 12*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*
             (-6*Power(s2,3) + t1*(5*t1 - t2)*t2 + 
               5*s2*t1*(4*t1 + t2) + Power(s2,2)*(-4*t1 + 3*t2)) + 
            Power(s1,3)*(-8*Power(s2,4) + Power(s2,3)*(15*t1 + 22*t2) + 
               2*Power(s2,2)*(Power(t1,2) + 11*t1*t2 - 4*Power(t2,2)) + 
               s2*t2*(-40*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               t1*t2*(-10*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) - 
            Power(s1,2)*(Power(s2,5) - 3*Power(s2,4)*(2*t1 + 5*t2) + 
               2*s2*t1*t2*(6*Power(t1,2) - 3*t1*t2 - 4*Power(t2,2)) + 
               t1*Power(t2,2)*
                (-18*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(9*Power(t1,2) + 2*t1*t2 + 22*Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,3) + 15*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 7*Power(t2,3))) + 
            s2*Power(t2,2)*(-4*Power(s2,3)*t1 + 
               2*t1*t2*(-2*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-2*Power(t1,2) + 10*t1*t2 + Power(t2,2)) + 
               s2*(6*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) - 2*Power(t2,3))) + 
            s1*t2*(Power(s2,5) + 
               2*Power(t1,2)*Power(t2,2)*(-3*t1 + 2*t2) - 
               Power(s2,4)*(3*t1 + 7*t2) + 
               Power(s2,3)*(11*Power(t1,2) - 6*t1*t2 + 8*Power(t2,2)) + 
               s2*t2*(18*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-12*Power(t1,3) - 6*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,3)*
             (6*Power(s2,3) + 2*t1*t2*(-5*t1 + 2*t2) - 
               Power(s2,2)*(4*t1 + 9*t2) + 
               s2*(-20*Power(t1,2) - 10*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(3*Power(s2,4) - Power(s2,3)*(13*t1 + 20*t2) + 
               t1*t2*(10*Power(t1,2) + 12*t1*t2 - 9*Power(t2,2)) + 
               s2*t2*(20*Power(t1,2) + 12*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,2) - 16*t1*t2 + 15*Power(t2,2))) + 
            t2*(-(Power(s2,4)*t1) + 
               2*Power(t1,2)*(t1 - t2)*Power(t2,2) - 
               Power(s2,3)*(3*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) - 2*Power(t2,3)) + 
               s2*t2*(-6*Power(t1,3) - Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + Power(t2,3))) - 
            s1*(2*Power(s2,4)*(t1 + 2*t2) + 
               Power(s2,3)*(-5*Power(t1,2) + 4*t1*t2 - 12*Power(t2,2)) + 
               t1*Power(t2,2)*
                (12*Power(t1,2) - 3*t1*t2 - 4*Power(t2,2)) + 
               s2*t2*(-8*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,3) + Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 7*Power(t2,3)))))*dilog(1 - s1/s))/
     ((s - s1)*s1*Power(s2,2)*t1*(s - s2 + t1)*Power(t2,2)*
       Power(s - s1 + t2,2)) - 
    (16*(Power(s2,3)*(s2 - 2*t1)*(t1 - t2)*Power(t2,3)*(s2 - t1 + t2) + 
         Power(s1,6)*s2*t1*(3*s2 - 3*t1 + 2*t2) - 
         Power(s1,5)*t1*(-5*Power(s2,3) + Power(t1,2)*(t1 - t2) + 
            8*Power(s2,2)*t2 + 
            4*s2*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*s2*(3*Power(s2,5)*t2 - 8*Power(s2,4)*t1*t2 + 
            Power(s2,2)*Power(t1,2)*
             (2*Power(t1,2) + 7*t1*t2 - 21*Power(t2,2)) - 
            Power(s2,3)*t2*(Power(t1,2) - 17*t1*t2 + Power(t2,2)) + 
            2*t1*Power(t1 - t2,2)*t2*
             (4*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) - 
            2*s2*Power(t1 - t2,2)*
             (Power(t1,3) + 5*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
               Power(t2,3))) + 
         Power(s1,4)*(Power(s2,4)*(t1 + t2) + 
            Power(s2,3)*(6*Power(t1,2) - 23*t1*t2 + Power(t2,2)) - 
            Power(s2,2)*t1*(Power(t1,2) - 17*t1*t2 + Power(t2,2)) - 
            Power(t1,3)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
            s2*t1*(-5*Power(t1,3) + 16*Power(t1,2)*t2 - 
               27*t1*Power(t2,2) + 12*Power(t2,3))) + 
         Power(s1,3)*(5*Power(s2,5)*t2 + 
            2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
            Power(s2,2)*Power(t2,2)*
             (-21*Power(t1,2) + 7*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,4)*(Power(t1,2) - 23*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*(3*Power(t1,3) + 10*Power(t1,2)*t2 + 
               10*t1*Power(t2,2) + 3*Power(t2,3)) + 
            s2*t1*(-4*Power(t1,4) + 17*Power(t1,3)*t2 - 
               28*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
               10*Power(t2,4))) + 
         s1*Power(s2,2)*t2*(Power(s2,4)*(2*t1 - 3*t2) - 
            4*Power(s2,3)*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            2*t1*Power(t1 - t2,2)*
             (2*Power(t1,2) - 3*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(12*Power(t1,3) - 27*Power(t1,2)*t2 + 
               16*t1*Power(t2,2) - 5*Power(t2,3)) + 
            s2*(-10*Power(t1,4) + 25*Power(t1,3)*t2 - 
               28*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) - 
               4*Power(t2,4))) + 
         Power(s,4)*(s2*Power(t1 - t2,2)*t2*(s2 - t1 + t2) + 
            Power(s1,2)*(t1*Power(t1 - t2,2) + 
               2*Power(s2,2)*(t1 + t2) - 
               2*s2*(Power(t1,2) - Power(t2,2))) + 
            s1*(t1 - t2)*(t1*Power(t1 - t2,2) + 
               2*Power(s2,2)*(t1 + t2) - 2*s2*(Power(t1,2) - Power(t2,2))\
)) - Power(s,3)*(s2*(t1 - t2)*t2*
             (Power(s2,2)*(2*t1 - 3*t2) + 
               Power(t1 - t2,2)*(2*t1 + t2) - 
               2*s2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
            Power(s1,3)*(2*Power(s2,2)*(4*t1 + t2) + 
               t1*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
               s2*(-8*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(2*t1*(t1 - 2*t2)*Power(t1 - t2,2) + 
               2*Power(s2,3)*(t1 + 4*t2) + 
               4*Power(s2,2)*(Power(t1,2) - 7*t1*t2 + Power(t2,2)) - 
               s2*(5*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*(-(t1*Power(t1 - t2,3)*(t1 + 2*t2)) + 
               2*Power(s2,3)*(Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
               3*s2*Power(t1 - t2,2)*
                (Power(t1,2) + 4*t1*t2 + Power(t2,2)) - 
               Power(s2,2)*(4*Power(t1,3) + 14*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*t1*
             (13*Power(s2,2) - 13*s2*t1 + 3*Power(t1,2) + 8*s2*t2 - 
               4*t1*t2 + Power(t2,2)) + 
            Power(s1,3)*(8*Power(s2,3)*(t1 + t2) - 
               3*t1*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(3*Power(t1,2) - 49*t1*t2 + 7*Power(t2,2)) - 
               s2*(8*Power(t1,3) - 48*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + Power(t2,3))) + 
            s2*(t1 - t2)*t2*(Power(s2,3)*(t1 - 3*t2) - 
               3*Power(s2,2)*t1*(t1 - 2*t2) - 
               2*t1*Power(t1 - t2,2)*t2 + 
               s2*(2*Power(t1,3) - 2*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 3*Power(t2,3))) + 
            s1*(Power(s2,4)*(8*t1 - 13*t2)*t2 + 
               2*Power(t1,2)*Power(t1 - t2,3)*t2 - 
               s2*Power(t1 - t2,2)*
                (4*Power(t1,3) + 7*Power(t1,2)*t2 + 7*t1*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(s2,3)*(Power(t1,3) + 33*Power(t1,2)*t2 - 
                  48*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,4) + 26*Power(t1,3)*t2 - 
                  44*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,2)*(13*Power(s2,4)*t2 - 
               t1*Power(t1 - t2,2)*(3*Power(t1,2) - 2*Power(t2,2)) + 
               Power(s2,3)*(7*Power(t1,2) - 49*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,3) + 20*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*(Power(t1,4) + 13*Power(t1,3)*t2 - 
                  44*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) + 
                  4*Power(t2,4)))) - 
         s*(Power(s1,5)*t1*(10*Power(s2,2) - 10*s2*t1 + Power(t1,2) + 
               6*s2*t2 - t1*t2) + 
            Power(s1,4)*(-2*Power(t1,4) + 2*Power(t1,2)*Power(t2,2) + 
               Power(s2,3)*(11*t1 + t2) + 
               s2*t1*(-9*Power(t1,2) + 43*t1*t2 - 24*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 30*t1*t2 + Power(t2,2))) - 
            Power(s2,2)*(t1 - t2)*Power(t2,2)*
             (Power(s2,3) - 2*Power(s2,2)*(t1 + t2) + 
               2*t1*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) - 
               s2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(Power(s2,4)*(t1 + 11*t2) + 
               Power(s2,3)*(11*Power(t1,2) - 68*t1*t2 + 
                  11*Power(t2,2)) + 
               Power(t1,2)*(-3*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-3*Power(t1,3) + 47*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-7*Power(t1,4) + 22*Power(t1,3)*t2 - 
                  49*Power(t1,2)*Power(t2,2) + 28*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,2)*(10*Power(s2,5)*t2 + 
               2*Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2)*t2 + 
               Power(s2,4)*(Power(t1,2) - 30*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  47*t1*Power(t2,2) - 3*Power(t2,3)) + 
               4*Power(s2,2)*
                (Power(t1,4) + 5*Power(t1,3)*t2 - 
                  15*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               2*s2*(4*Power(t1,5) - 8*Power(t1,4)*t2 + 
                  10*Power(t1,3)*Power(t2,2) - 
                  14*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) + 
                  Power(t2,5))) + 
            s1*s2*(2*Power(s2,4)*(3*t1 - 5*t2)*t2 + 
               Power(s2,3)*t2*
                (-24*Power(t1,2) + 43*t1*t2 - 9*Power(t2,2)) + 
               8*t1*Power(t1 - t2,2)*t2*
                (Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,4) + 28*Power(t1,3)*t2 - 
                  49*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                  7*Power(t2,4)) - 
               2*s2*(Power(t1,5) + 7*Power(t1,4)*t2 - 
                  14*Power(t1,3)*Power(t2,2) + 
                  10*Power(t1,2)*Power(t2,3) - 8*t1*Power(t2,4) + 
                  4*Power(t2,5)))))*dilog(1 - s/(s - s1 - s2)))/
     (Power(s1,2)*Power(s2,2)*t1*(s - s2 + t1)*(s1 + t1 - t2)*t2*
       (s - s1 + t2)*(s2 - t1 + t2)) - 
    (16*(Power(s1,5)*s2*(Power(s2,3) - 3*Power(s2,2)*t1 + 
            4*s2*Power(t1,2) - 2*Power(t1,3)) + 
         Power(s2,3)*t1*(Power(s2,2) - 3*s2*t1 + 2*Power(t1,2))*
          Power(t2,3) + Power(s,6)*t1*(t1 - t2)*(s1 + t2) + 
         s1*Power(s2,2)*Power(t2,2)*
          (-2*Power(s2,4) + 8*Power(s2,3)*t1 + 
            s2*Power(t1,2)*(5*t1 - 6*t2) + 4*Power(t1,3)*(-t1 + t2) + 
            Power(s2,2)*t1*(-9*t1 + 2*t2)) + 
         Power(s1,4)*(Power(s2,2) - 2*s2*t1 + 2*Power(t1,2))*
          (2*Power(s2,3) + 2*Power(t1,2)*t2 + s2*t1*(-t1 + t2) - 
            Power(s2,2)*(t1 + 2*t2)) + 
         Power(s1,3)*(-(Power(s2,5)*t2) - 4*Power(t1,4)*Power(t2,2) + 
            Power(s2,4)*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2)) + 
            s2*Power(t1,3)*(-Power(t1,2) + 8*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*
             (Power(t1,2) - 6*t1*t2 + 4*Power(t2,2)) - 
            Power(s2,3)*t1*(Power(t1,2) - 4*t1*t2 + 5*Power(t2,2))) + 
         Power(s1,2)*s2*t2*(2*Power(s2,5) - Power(s2,4)*(7*t1 + t2) + 
            Power(s2,3)*(5*Power(t1,2) + 8*t1*t2 - Power(t2,2)) + 
            2*Power(t1,3)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*t1*(Power(t1,2) - 10*t1*t2 + 4*Power(t2,2)) - 
            s2*Power(t1,2)*(Power(t1,2) - 10*t1*t2 + 6*Power(t2,2))) + 
         Power(s,5)*(t1*t2*(-4*s2*t1 + 3*Power(t1,2) + 5*s2*t2 - 
               2*t1*t2 - Power(t2,2)) + 
            Power(s1,2)*(3*s2*t1 - 2*Power(t1,2) + 2*s2*t2 + 4*t1*t2 - 
               Power(t2,2)) + 
            s1*(-3*s2*Power(t1,2) + Power(t1,3) + 5*s2*t1*t2 - 
               4*Power(t1,2)*t2 + 2*s2*Power(t2,2) + 2*t1*Power(t2,2))) + 
         Power(s,4)*(Power(s1,3)*
             (2*Power(s2,2) - 6*s2*t1 + Power(t1,2) - 4*s2*t2 - 
               4*t1*t2 + Power(t2,2)) - 
            Power(s1,2)*(2*Power(t1,3) - 7*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) + Power(t2,3) + 
               Power(s2,2)*(9*t1 + 6*t2) + 
               s2*(-10*Power(t1,2) + 11*t1*t2 - 3*Power(t2,2))) + 
            t1*t2*(2*Power(s2,2)*(3*t1 - 5*t2) + 
               t1*(2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
               s2*(-9*Power(t1,2) + 8*t1*t2 + 5*Power(t2,2))) + 
            s1*(-3*s2*Power(t1,2)*(t1 - 4*t2) + 
               Power(s2,2)*(3*Power(t1,2) - 10*t1*t2 - 10*Power(t2,2)) - 
               t1*(Power(t1,3) + 12*Power(t1,2)*t2 - 7*t1*Power(t2,2) - 
                  2*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,4)*
             (-3*Power(s2,2) + t1*t2 + 2*s2*(2*t1 + t2)) + 
            Power(s1,3)*(-6*Power(s2,3) + Power(s2,2)*(20*t1 + 13*t2) + 
               s2*(-12*Power(t1,2) + 4*t1*t2 - 5*Power(t2,2)) + 
               t1*(Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) + 
            t1*t2*(2*Power(t1,2)*(t1 - t2)*t2 + 
               Power(s2,3)*(-4*t1 + 10*t2) + 
               Power(s2,2)*(9*Power(t1,2) - 12*t1*t2 - 
                  10*Power(t2,2)) + 
               s2*t1*(-4*Power(t1,2) - 3*t1*t2 + 12*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,3)*(9*t1 + 4*t2) + 
               Power(s2,2)*(-15*Power(t1,2) + 16*t1*t2 - 
                  2*Power(t2,2)) + 
               t1*(2*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*(7*Power(t1,3) - 11*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  4*Power(t2,3))) + 
            s1*(Power(s2,2)*t1*
                (3*Power(t1,2) - 12*t1*t2 - 20*Power(t2,2)) + 
               Power(s2,3)*(-Power(t1,2) + 10*t1*t2 + 20*Power(t2,2)) + 
               s2*t1*(Power(t1,3) + 22*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(t1,2)*(-Power(t1,3) - 11*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 6*Power(t2,3)))) - 
         s*(2*Power(s1,5)*s2*Power(s2 - t1,2) + 
            Power(s2,2)*t1*Power(t2,2)*
             (-Power(s2,3) + s2*t1*(t1 - 12*t2) - 
               2*Power(t1,2)*(t1 - 3*t2) + Power(s2,2)*(2*t1 + 5*t2)) + 
            Power(s1,4)*(7*Power(s2,4) - 6*s2*Power(t1,3) - 
               6*Power(t1,3)*t2 - 2*Power(s2,3)*(8*t1 + 3*t2) + 
               Power(s2,2)*t1*(14*t1 + 9*t2)) + 
            s1*s2*t2*(2*Power(t1,3)*Power(t1 - 2*t2,2) + 
               30*Power(s2,3)*t1*t2 - Power(s2,4)*(t1 + 10*t2) + 
               s2*Power(t1,2)*
                (Power(t1,2) + 9*t1*t2 - 18*Power(t2,2)) + 
               2*Power(s2,2)*t1*(Power(t1,2) - 10*t1*t2 + 4*Power(t2,2))\
) + Power(s1,3)*(2*Power(s2,5) - Power(s2,4)*(8*t1 + 7*t2) + 
               Power(s2,2)*t1*
                (-4*Power(t1,2) + 2*t1*t2 - 13*Power(t2,2)) - 
               2*s2*Power(t1,2)*
                (3*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) + 
               Power(t1,3)*(Power(t1,2) + 8*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(12*Power(t1,2) + 7*Power(t2,2))) + 
            Power(s1,2)*(6*Power(s2,5)*t2 + 
               Power(s2,4)*(Power(t1,2) - 20*t1*t2 - 3*Power(t2,2)) - 
               2*Power(t1,3)*t2*
                (Power(t1,2) + 2*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,3)*(-2*Power(t1,3) + 7*Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) - 4*Power(t2,3)) - 
               2*s2*Power(t1,2)*
                (Power(t1,3) - 8*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,2)*t1*
                (Power(t1,3) - 3*Power(t1,2)*t2 - 12*t1*Power(t2,2) + 
                  12*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,5)*s2*(s2 - t1) + 
            Power(s1,4)*(8*Power(s2,3) + 4*Power(t1,2)*t2 - 
               3*Power(s2,2)*(5*t1 + 2*t2) + s2*t1*(7*t1 + 3*t2)) + 
            Power(s1,3)*(6*Power(s2,4) - Power(s2,3)*(22*t1 + 15*t2) + 
               s2*t1*(-8*Power(t1,2) + 6*t1*t2 - 11*Power(t2,2)) - 
               Power(t1,2)*(Power(t1,2) + 10*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(22*Power(t1,2) + 2*t1*t2 + 9*Power(t2,2))) + 
            s2*t1*t2*(Power(s2,3)*(t1 - 5*t2) + 
               2*Power(t1,2)*t2*(-2*t1 + 3*t2) + 
               s2*t1*(2*Power(t1,2) + 3*t1*t2 - 18*Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,2) + 8*t1*t2 + 10*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,4)*(-3*t1 + 4*t2) + 
               Power(s2,3)*(8*Power(t1,2) - 22*t1*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(2*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,2)*(-7*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*t1*(-5*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 12*Power(t2,3))) - 
            s1*(5*Power(s2,4)*t2*(t1 + 4*t2) + 
               Power(s2,3)*t1*(Power(t1,2) - 4*t1*t2 - 40*Power(t2,2)) + 
               2*Power(s2,2)*t1*t2*
                (4*Power(t1,2) + 3*t1*t2 - 6*Power(t2,2)) + 
               2*Power(t1,3)*t2*(Power(t1,2) + 2*t1*t2 - 2*Power(t2,2)) + 
               s2*Power(t1,2)*
                (Power(t1,3) - 12*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  18*Power(t2,3)))))*dilog(1 - s2/s))/
     (Power(s1,2)*(s - s2)*s2*Power(t1,2)*Power(s - s2 + t1,2)*t2*
       (s - s1 + t2)) - (16*(s1*s2*
          (Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*(s1 - t2)*
          (s1*(s2 - t1) - s2*t2) + 
         Power(s,4)*(Power(s1,2)*(t1 - t2) + 
            s1*(2*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
            t1*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) - 
         Power(s,3)*(Power(s1,3)*(s2 + 3*t1 - 2*t2) + 
            Power(s1,2)*(6*Power(t1,2) + s2*(t1 - 5*t2) - 13*t1*t2 + 
               2*Power(t2,2)) + 
            t1*(-(t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
               s2*(2*Power(t1,2) - 9*t1*t2 + 8*Power(t2,2))) + 
            s1*(s2*(2*Power(t1,2) - 15*t1*t2 + 5*Power(t2,2)) + 
               t1*(6*Power(t1,2) - 13*t1*t2 + 12*Power(t2,2)))) + 
         Power(s,2)*(Power(s1,4)*(2*s2 + 3*t1 - t2) + 
            Power(s1,3)*(3*Power(s2,2) + 6*Power(t1,2) + 
               s2*(t1 - 8*t2) - 11*t1*t2 + Power(t2,2)) + 
            t1*t2*(s2*(3*t1 - 5*t2)*t2 + Power(t2,2)*(-t1 + t2) + 
               Power(s2,2)*(-4*t1 + 7*t2)) + 
            Power(s1,2)*(-11*Power(s2,2)*t2 + 
               s2*(4*Power(t1,2) - 20*t1*t2 + 6*Power(t2,2)) + 
               t1*(6*Power(t1,2) - 11*t1*t2 + 15*Power(t2,2))) + 
            s1*(Power(s2,2)*t2*(-16*t1 + 9*t2) + 
               t1*t2*(-2*Power(t1,2) + 7*t1*t2 - 9*Power(t2,2)) + 
               s2*t1*(4*Power(t1,2) - 9*t1*t2 + 16*Power(t2,2)))) - 
         s*(Power(s1,5)*(s2 + t1) + 
            s2*t1*Power(t2,2)*(2*Power(s2,2) - 2*s2*t2 + Power(t2,2)) + 
            Power(s1,4)*(4*Power(s2,2) + t1*(2*t1 - 3*t2) - 
               s2*(t1 + 3*t2)) + 
            s1*t2*(5*Power(s2,2)*t1*t2 + s2*t1*(t1 - 6*t2)*t2 - 
               t1*(t1 - 2*t2)*Power(t2,2) + Power(s2,3)*(-6*t1 + 7*t2)) + 
            Power(s1,3)*(4*Power(s2,3) + 2*s2*Power(t1 - t2,2) - 
               2*Power(s2,2)*(t1 + 5*t2) + 
               t1*(2*Power(t1,2) - 3*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(-11*Power(s2,3)*t2 + 
               6*Power(s2,2)*t2*(-t1 + t2) - 
               t1*t2*(Power(t1,2) - 3*t1*t2 + 6*Power(t2,2)) + 
               2*s2*(Power(t1,3) + 4*t1*Power(t2,2)))))*
       dilog(1 - s2/(-s + s2 - t1)))/
     (s*s1*s2*t1*(s - s2 + t1)*t2*Power(s - s1 + t2,2)) - 
    (16*(Power(s1,5)*Power(s2 - t1,2) + 
         2*Power(s,4)*t1*(s1 + t1)*(t1 - t2) + 
         Power(s1,4)*(s2 - t1)*
          (2*Power(s2,2) - t1*(t1 - 3*t2) - 3*s2*t2) + 
         s2*Power(t2,3)*(2*Power(s2,3) - 4*Power(s2,2)*t1 + 
            3*s2*Power(t1,2) + Power(t1,2)*(-2*t1 + t2)) + 
         s1*Power(t2,2)*(-4*Power(s2,4) + Power(s2,3)*(11*t1 + t2) + 
            Power(t1,2)*t2*(-3*t1 + 2*t2) + 
            s2*t1*(3*Power(t1,2) + 3*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*(-8*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
         Power(s1,3)*(Power(s2,3)*(2*t1 - 3*t2) + 
            2*Power(t1,2)*t2*(-2*t1 + 3*t2) + 
            s2*t1*(Power(t1,2) + 8*t1*t2 - 9*Power(t2,2)) + 
            Power(s2,2)*(-3*Power(t1,2) - 2*t1*t2 + 4*Power(t2,2))) + 
         Power(s1,2)*t2*(2*Power(s2,4) - 9*Power(s2,3)*t1 + 
            6*Power(t1,2)*(t1 - t2)*t2 + 
            Power(s2,2)*(9*Power(t1,2) + 9*t1*t2 - 3*Power(t2,2)) + 
            s2*t1*(-3*Power(t1,2) - 11*t1*t2 + 7*Power(t2,2))) - 
         s*(Power(s1,4)*(3*Power(s2,2) - 7*s2*t1 + 4*Power(t1,2) - 
               2*s2*t2) + Power(t2,2)*
             (3*s2*Power(t1,3) + 6*Power(s2,3)*t2 - 
               8*Power(s2,2)*t1*t2 + Power(t1,2)*t2*(-t1 + t2)) + 
            Power(s1,3)*(2*Power(s2,3) - 4*Power(s2,2)*(t1 + 2*t2) + 
               t1*(4*Power(t1,2) - 7*t1*t2 - 3*Power(t2,2)) + 
               s2*(-3*Power(t1,2) + 5*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3)*t1 + 
               Power(s2,2)*(-7*Power(t1,2) - 12*t1*t2 + 
                  4*Power(t2,2)) + 
               t1*t2*(-11*Power(t1,2) + 7*t1*t2 + 5*Power(t2,2)) + 
               s2*(4*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 5*Power(t2,3))) + 
            s1*t2*(-2*Power(s2,3)*(3*t1 + 4*t2) + 
               t1*t2*(9*Power(t1,2) - 4*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(10*Power(t1,2) + 19*t1*t2 + Power(t2,2)) - 
               s2*(7*Power(t1,3) + 6*Power(t1,2)*t2 + 5*t1*Power(t2,2) - 
                  2*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,2)*
             (-6*Power(t1,2) + 5*t1*t2 - Power(t2,2) + 2*s2*(2*t1 + t2)) \
+ s1*(-6*Power(t1,3) + 7*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
               Power(t2,3) + 2*s2*t1*(t1 + 3*t2)) - 
            2*(2*Power(t1,2)*t2*(-t1 + t2) + 
               s2*(Power(t1,3) - Power(t1,2)*t2 + Power(t2,3)))) + 
         Power(s,2)*(Power(s1,3)*
             (2*Power(s2,2) - 9*s2*t1 + 7*Power(t1,2) - 4*s2*t2 - 
               3*t1*t2 + Power(t2,2)) - 
            Power(s1,2)*(-7*Power(t1,3) + 9*Power(t1,2)*t2 - 
               2*t1*Power(t2,2) + 2*Power(t2,3) + 
               2*Power(s2,2)*(t1 + 2*t2) + 
               s2*(4*Power(t1,2) + 6*t1*t2 - 5*Power(t2,2))) + 
            t2*(3*Power(t1,2)*(t1 - t2)*t2 + 6*Power(s2,2)*Power(t2,2) - 
               4*s2*t1*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
            s1*(-2*Power(s2,2)*
                (2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) + 
               s2*(5*Power(t1,3) + 4*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - Power(t2,3)) + 
               t2*(-11*Power(t1,3) + 6*Power(t1,2)*t2 + Power(t2,3)))))*
       dilog(1 - (-s + s2 - t1)/s2))/
     (s1*s2*(-s + s2 - t1)*Power(t1,2)*(-s + s1 - t2)*(s1 + t1 - t2)*t2) - 
    (16*(Power(s,5)*Power(t1 - t2,2) + 
         s1*s2*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*t2*
          (s1*(s2 - t1) - s2*t2) + 
         Power(s,4)*((t1 - t2)*
             (-3*s2*t1 + Power(t1,2) + 4*s2*t2 - 2*t1*t2) + 
            2*s1*(s2*t1 - 2*Power(t1,2) + 3*t1*t2 - Power(t2,2))) + 
         Power(s,3)*(Power(s2,2)*
             (2*Power(t1,2) - 9*t1*t2 + 7*Power(t2,2)) + 
            t2*(-Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
               Power(t2,3)) + 
            s2*(-2*Power(t1,3) + 6*Power(t1,2)*t2 - 7*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,2) + 6*Power(t1,2) - 7*t1*t2 + 
               2*Power(t2,2) - s2*(5*t1 + t2)) - 
            s1*(3*Power(t1,3) - 6*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
               2*Power(t2,3) + Power(s2,2)*(4*t1 + 3*t2) + 
               s2*(-12*Power(t1,2) + 17*t1*t2 - 3*Power(t2,2)))) - 
         s*(-(s2*(s2 - t1)*Power(t2,2)*
               (2*Power(s2,2) + 2*s2*t2 + Power(t2,2))) + 
            Power(s1,4)*(t1*(-t1 + t2) + s2*(t1 + t2)) + 
            Power(s1,3)*(-Power(s2,3) + Power(t1,3) + 
               Power(s2,2)*(6*t1 + 4*t2) - 
               2*s2*(3*Power(t1,2) - t1*t2 + Power(t2,2))) + 
            s1*t2*(2*Power(s2,4) + Power(s2,3)*(2*t1 - 7*t2) + 
               Power(s2,2)*(t1 - 3*t2)*t2 - 
               t1*(t1 - 2*t2)*Power(t2,2) - 
               s2*t2*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(4*Power(s2,3)*(t1 + 2*t2) + 
               t1*t2*(Power(t1,2) - 2*Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               s2*(2*Power(t1,3) - Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  2*Power(t2,3)))) - 
         Power(s,2)*(Power(s1,3)*
             (Power(s2,2) + Power(-2*t1 + t2,2) - 2*s2*(2*t1 + t2)) + 
            Power(s1,2)*(Power(s2,3) - 3*Power(t1,3) + 
               3*Power(t1,2)*t2 - 2*Power(t2,3) - 
               Power(s2,2)*(10*t1 + 7*t2) + 
               s2*(15*Power(t1,2) - 13*t1*t2 + 2*Power(t2,2))) + 
            t2*(t1*(t1 - t2)*Power(t2,2) + Power(s2,3)*(-4*t1 + 6*t2) + 
               s2*t2*(3*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2))) + 
            s1*(-(Power(s2,3)*(2*t1 + 5*t2)) + 
               2*Power(s2,2)*(4*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 7*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)) + 
               t2*(-2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)))))*dilog(1 - s2/t1))/
     (s*s1*s2*t1*(s - s2 + t1)*Power(t2,2)*(s - s1 + t2)) - 
    (16*(Power(s1,4)*t1*(-s2 + t1)*t2 + 
         s2*Power(t2,3)*(Power(s2,3) + s2*t1*(3*t1 - 2*t2) + 
            Power(s2,2)*(-4*t1 + t2) + Power(t1,2)*(-2*t1 + t2)) - 
         Power(s,5)*(t2*(-t1 + t2) + s2*(t1 + t2)) + 
         s1*Power(t2,2)*(-Power(s2,4) + 8*Power(s2,3)*t1 + 
            Power(t1,2)*t2*(-3*t1 + 2*t2) + 
            s2*t1*(3*Power(t1,2) + 2*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*(-8*Power(t1,2) + Power(t2,2))) + 
         Power(s1,3)*(-(Power(t1,3)*t2) + Power(s2,3)*(t1 + t2) + 
            s2*Power(t1,2)*(t1 + 4*t2) + 
            Power(s2,2)*(-2*Power(t1,2) - 4*t1*t2 + Power(t2,2))) - 
         Power(s1,2)*t2*(2*Power(s2,3)*(2*t1 + t2) + 
            Power(t1,2)*t2*(-3*t1 + 2*t2) + 
            s2*t1*(3*Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,2)*(-7*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2))) + 
         Power(s,3)*(Power(s2,2)*(13*t1 - 3*t2)*t2 - 
            Power(s2,3)*(t1 + 3*t2) + 
            Power(s1,2)*(3*Power(s2,2) - 6*s2*t1 + 2*s2*t2 + 4*t1*t2 - 
               Power(t2,2)) + 
            t2*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
               Power(t2,3)) - 
            s2*(Power(t1,3) + 5*Power(t1,2)*t2 - t1*Power(t2,2) + 
               Power(t2,3)) + 
            s1*(2*Power(s2,3) + s2*(14*t1 - t2)*t2 + 
               Power(s2,2)*(-6*t1 + t2) + t1*t2*(-3*t1 + t2))) + 
         Power(s,4)*(2*t1*(t1 - t2)*t2 + s2*t2*(-8*t1 + 3*t2) + 
            Power(s2,2)*(2*t1 + 3*t2) + 
            s1*(-Power(s2,2) + 4*s2*t1 + t2*(-2*t1 + t2))) + 
         s*(2*Power(s1,3)*(Power(s2,3) - Power(s2,2)*t1 - 
               2*Power(t1,2)*t2 + s2*(5*t1 - t2)*t2) + 
            Power(s1,4)*(Power(s2,2) + t1*t2 + s2*(-t1 + t2)) - 
            Power(t2,2)*(3*Power(s2,2)*t2*(-3*t1 + t2) + 
               Power(t1,2)*t2*(-t1 + t2) + 3*Power(s2,3)*(t1 + t2) + 
               s2*t1*(3*Power(t1,2) + 5*t1*t2 - 4*Power(t2,2))) + 
            s1*t2*(Power(s2,4) + Power(s2,3)*(5*t1 + 3*t2) - 
               3*Power(s2,2)*t1*(3*t1 + 5*t2) - 
               t1*t2*(3*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               s2*(7*Power(t1,3) + 3*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4) + Power(s2,3)*(-2*t1 + t2) + 
               t1*t2*(2*Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) + 12*t1*t2 + 3*Power(t2,2)) - 
               3*s2*(Power(t1,3) + 2*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
                  Power(t2,3)))) + 
         Power(s,2)*(Power(s1,3)*
             (-3*Power(s2,2) + 4*s2*t1 - 2*s2*t2 - 4*t1*t2 + Power(t2,2)) \
- Power(s1,2)*(4*Power(s2,3) + 16*s2*t1*t2 + 
               Power(s2,2)*(-6*t1 + 3*t2) + 
               t2*(-4*Power(t1,2) - 2*t1*t2 + Power(t2,2))) - 
            s1*(Power(s2,4) - 2*Power(s2,3)*(t1 - t2) + 
               2*Power(t1,3)*t2 + t1*Power(t2,3) - Power(t2,4) - 
               3*s2*t1*(Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,2) + 17*t1*t2 + 2*Power(t2,2))) + 
            t2*(Power(s2,4) + 2*t1*(t1 - t2)*Power(t2,2) + 
               Power(s2,3)*(-6*t1 + t2) + 
               Power(s2,2)*(3*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  3*Power(t2,3)))))*dilog(1 - t1/s2))/
     (s1*s2*t1*Power(s - s2 + t1,2)*(-s + s1 - t2)*t2*(s2 - t1 + t2)) + 
    (16*(-(s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*(s2 - t1)*
            (s1*(s2 - t1) - s2*t2)) + 
         Power(s,4)*(Power(s2,2)*(t1 - t2) + 
            t2*(-3*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) - 
            s2*(Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
         Power(s,3)*(Power(s2,3)*(-2*t1 + 3*t2) - 
            t1*t2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,2) - 13*t1*t2 + 6*Power(t2,2)) + 
            s2*t2*(12*Power(t1,2) - 13*t1*t2 + 6*Power(t2,2)) + 
            s1*(Power(s2,3) + Power(s2,2)*(-5*t1 + t2) + 
               s2*(5*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2)) + 
               t2*(8*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)))) - 
         Power(s,2)*(-(Power(s2,4)*(t1 - 3*t2)) + 
            Power(t1,3)*(t1 - t2)*t2 + 
            Power(s1,2)*(3*Power(s2,3) - 11*Power(s2,2)*t1 + 
               s2*t1*(9*t1 - 16*t2) + t1*(7*t1 - 4*t2)*t2) + 
            s2*t1*t2*(-9*Power(t1,2) + 7*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,3)*(Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,2)*t2*(15*Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
            s1*(2*Power(s2,4) + Power(s2,3)*(-8*t1 + t2) + 
               Power(t1,2)*t2*(-5*t1 + 3*t2) + 
               Power(s2,2)*(6*Power(t1,2) - 20*t1*t2 + 4*Power(t2,2)) + 
               s2*t2*(16*Power(t1,2) - 9*t1*t2 + 4*Power(t2,2)))) + 
         s*(Power(s1,3)*(4*Power(s2,3) - 11*Power(s2,2)*t1 + 
               s2*t1*(7*t1 - 6*t2) + 2*Power(t1,2)*t2) + 
            Power(s1,2)*(4*Power(s2,4) + 6*Power(s2,2)*t1*(t1 - t2) + 
               5*s2*Power(t1,2)*t2 - 2*Power(t1,3)*t2 - 
               2*Power(s2,3)*(5*t1 + t2)) + 
            s2*t2*(Power(s2,4) + Power(t1,3)*(2*t1 - t2) + 
               Power(s2,3)*(-3*t1 + 2*t2) - 
               s2*t1*(6*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(6*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) + 
            s1*(Power(s2,5) + 2*Power(s2,3)*Power(t1 - t2,2) + 
               Power(t1,4)*t2 + s2*Power(t1,2)*t2*(-6*t1 + t2) - 
               Power(s2,4)*(3*t1 + t2) + 
               2*Power(s2,2)*(4*Power(t1,2)*t2 + Power(t2,3)))))*
       dilog(1 - s1/(-s + s1 - t2)))/
     (s*s1*s2*t1*Power(s - s2 + t1,2)*t2*(s - s1 + t2)) + 
    (16*(2*Power(s,4)*Power(t1 - t2,2)*t2 + 
         Power(s2,2)*(t1 - t2)*Power(t2,2)*
          (-(t1*(2*t1 + t2)) + s2*(3*t1 + 2*t2)) + 
         Power(s1,4)*(2*Power(s2,3) + t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-5*t1 + 2*t2) + 
            s2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
         Power(s1,3)*(2*Power(s2,4) - 4*Power(s2,3)*t1 + 
            Power(s2,2)*(Power(t1,2) - 2*t1*t2 - Power(t2,2)) - 
            t1*t2*(Power(t1,2) + t1*t2 - Power(t2,2)) + 
            s2*(Power(t1,3) + 5*t1*Power(t2,2))) + 
         s1*s2*t2*(-2*Power(s2,3)*t1 + 
            Power(t1,2)*(2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(5*Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)) - 
            s2*(5*Power(t1,3) - 4*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
               2*Power(t2,3))) + 
         Power(s1,2)*(2*Power(s2,4)*(t1 - t2) + 
            Power(t1,3)*Power(t2,2) + 
            Power(s2,3)*(-6*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*(6*Power(t1,3) - 8*Power(t1,2)*t2 + 
               11*t1*Power(t2,2) + Power(t2,3)) - 
            s2*(2*Power(t1,4) - 4*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) + Power(t2,4))) \
+ Power(s,3)*(Power(t1 - t2,2)*t2*(t1 + 2*t2) - 
            2*s2*(Power(t1,3) - 4*t1*Power(t2,2) + 3*Power(t2,3)) + 
            s1*(-2*s2*t1*(t1 - 3*t2) + 
               t2*(-7*Power(t1,2) + 11*t1*t2 - 4*Power(t2,2)))) - 
         Power(s,2)*(Power(s1,2)*
             (4*Power(s2,2)*t1 + s2*t1*(-7*t1 + 16*t2) + 
               t2*(-9*Power(t1,2) + 11*t1*t2 - 3*Power(t2,2))) + 
            (t1 - t2)*(t1*Power(t2,2)*(-t1 + t2) + 
               Power(s2,2)*(-2*Power(t1,2) + 2*t1*t2 + 6*Power(t2,2)) + 
               s2*(Power(t1,3) + 3*Power(t1,2)*t2 - t1*Power(t2,2) - 
                  6*Power(t2,3))) + 
            s1*(2*Power(s2,2)*t1*(t1 + 5*t2) + 
               s2*(-6*Power(t1,3) + 2*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 10*Power(t2,3)) + 
               t2*(3*Power(t1,3) + Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  2*Power(t2,3)))) + 
         s*(-(Power(s1,3)*(2*Power(s2,3) + Power(s2,2)*(-9*t1 + 2*t2) + 
                 s2*(8*Power(t1,2) - 15*t1*t2 + Power(t2,2)) + 
                 t2*(5*Power(t1,2) - 5*t1*t2 + Power(t2,2)))) + 
            s2*(t1 - t2)*t2*(2*Power(s2,2)*(t1 + t2) + 
               s2*(2*Power(t1,2) - 5*t1*t2 - 6*Power(t2,2)) + 
               t1*(-2*Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3)*(t1 + t2) + 
               2*Power(s2,2)*(Power(t1,2) + 4*t1*t2 + Power(t2,2)) + 
               t2*(3*Power(t1,3) + 2*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3)) - 
               s2*(5*Power(t1,3) - 3*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  4*Power(t2,3))) + 
            s1*(2*Power(t1,2)*Power(t2,2)*(-t1 + t2) + 
               2*Power(s2,3)*t1*(2*t1 + 3*t2) - 
               Power(s2,2)*(7*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(3*Power(t1,4) - Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  4*Power(t2,4)))))*
       dilog(1 - (-s + s2 - t1)/(-s + s1 - t2)))/
     (Power(s1,2)*s2*(-s + s2 - t1)*t1*(s1 + t1 - t2)*t2*(s - s1 + t2)) - 
    (16*(2*Power(s1,4)*Power(s2 - t1,2)*t1 - 
         2*Power(s,4)*(t1 - t2)*t2*(s2 + t2) + 
         s2*(s2 - t1)*Power(t2,2)*
          (Power(s2,3) + s2*t1*(4*t1 - 3*t2) + 
            Power(s2,2)*(-2*t1 + t2) + Power(t1,2)*(-2*t1 + 3*t2)) + 
         Power(s1,3)*(s2 - t1)*
          (2*Power(s2,3) - Power(s2,2)*(t1 - 2*t2) + 4*Power(t1,2)*t2 - 
            s2*t1*(t1 + 7*t2)) + 
         Power(s1,2)*(Power(s2,5) + 3*Power(t1,3)*Power(t2,2) - 
            Power(s2,4)*(3*t1 + 2*t2) + 
            s2*Power(t1,2)*(Power(t1,2) - 5*t1*t2 - 8*Power(t2,2)) - 
            3*Power(s2,2)*t1*(Power(t1,2) - 3*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,3)*(4*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2))) + 
         s1*t2*(-2*Power(s2,5) + Power(s2,4)*(6*t1 - t2) + 
            Power(t1,3)*(t1 - 2*t2)*t2 + 
            Power(s2,2)*t1*(7*Power(t1,2) - 11*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,3)*(-9*Power(t1,2) + 8*t1*t2 + Power(t2,2)) + 
            s2*Power(t1,2)*(-2*Power(t1,2) + 3*t1*t2 + 3*Power(t2,2))) + 
         Power(s,3)*(4*t1*Power(t2,2)*(-t1 + t2) - 
            Power(s2,2)*(Power(t1,2) - 5*t1*t2 + 6*Power(t2,2)) + 
            s2*(Power(t1,3) - 4*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
               6*Power(t2,3)) + 
            2*s1*(-Power(t1,3) + t1*Power(t2,2) - Power(t2,3) + 
               s2*t2*(3*t1 + t2) + Power(s2,2)*(t1 + 2*t2))) + 
         Power(s,2)*(3*Power(t1,2)*Power(t2,2)*(-t1 + t2) + 
            Power(s2,3)*(Power(t1,2) - 3*t1*t2 + 7*Power(t2,2)) + 
            s2*t1*(Power(t1,3) + 6*t1*Power(t2,2) - 11*Power(t2,3)) + 
            Power(s2,2)*(-2*Power(t1,3) + 2*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + 7*Power(t2,3)) + 
            2*Power(s1,2)*(Power(s2,3) + 3*Power(t1,3) - 
               Power(s2,2)*(2*t1 + t2) - 
               s2*(2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) - 
            s1*(Power(s2,3)*(4*t1 + 9*t2) + 
               4*t1*t2*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-5*Power(t1,2) + 6*t1*t2 + 4*Power(t2,2)) + 
               s2*(Power(t1,3) - 12*Power(t1,2)*t2 - 4*t1*Power(t2,2) - 
                  5*Power(t2,3)))) - 
         s*(2*Power(s1,3)*(Power(s2,3) + 3*Power(t1,3) + 
               Power(s2,2)*t2 - s2*t1*(4*t1 + 3*t2)) + 
            t2*(4*Power(s2,4)*t2 + Power(t1,3)*(t1 - t2)*t2 + 
               Power(s2,2)*t1*
                (5*Power(t1,2) + 7*t1*t2 - 11*Power(t2,2)) + 
               Power(s2,3)*(-3*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               s2*Power(t1,2)*(-2*Power(t1,2) - 4*t1*t2 + 9*Power(t2,2))) \
+ Power(s1,2)*(3*Power(s2,4) - 8*Power(t1,3)*t2 - 
               4*Power(s2,3)*(2*t1 + t2) + 
               Power(s2,2)*(4*Power(t1,2) - 12*t1*t2 - 7*Power(t2,2)) + 
               s2*t1*(Power(t1,2) + 19*t1*t2 + 10*Power(t2,2))) + 
            s1*(3*Power(t1,2)*Power(t2,3) - Power(s2,4)*(2*t1 + 7*t2) + 
               Power(s2,3)*(5*Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)) + 
               s2*t1*(2*Power(t1,3) - 5*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,2)*(-5*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 4*Power(t2,3)))))*
       dilog(1 - (-s + s1 - t2)/s1))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*Power(t2,2)*(s2 - t1 + t2)) + 
    (16*(2*Power(s,4)*t1*Power(t1 - t2,2) + 
         2*Power(s1,4)*s2*(s2 - t1)*(s2 + t2) + 
         Power(s2,2)*t1*t2*(t1*Power(t2,2) + Power(s2,2)*(-t1 + t2) + 
            s2*(Power(t1,2) - t1*t2 - Power(t2,2))) + 
         Power(s1,3)*(2*Power(s2,4) - 4*Power(s2,3)*t2 + 
            Power(s2,2)*(-2*Power(t1,2) + 5*t1*t2 - 6*Power(t2,2)) - 
            Power(t1,2)*(2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
            s2*t1*(2*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) + 
         Power(s1,2)*(Power(s2,4)*(2*t1 - 5*t2) + 
            Power(t1,2)*t2*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
            Power(s2,3)*(-Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
            s2*t1*(2*Power(t1,3) + 3*Power(t1,2)*t2 - 
               4*t1*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,2)*(Power(t1,3) + 11*Power(t1,2)*t2 - 
               8*t1*Power(t2,2) + 6*Power(t2,3))) + 
         s1*s2*(t1*Power(t2,2)*
             (2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,3)*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*(5*Power(t1,2)*t2 + Power(t2,3)) - 
            s2*(Power(t1,4) - Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
               2*Power(t2,4))) + 
         Power(s,3)*(-(t1*(t1 - t2)*
               (4*s2*t1 - 2*Power(t1,2) - 7*s2*t2 + t1*t2 + Power(t2,2))\
) - 2*s1*(3*Power(t1,3) - 3*s2*t1*t2 - 4*Power(t1,2)*t2 + 
               Power(t2,2)*(s2 + t2))) + 
         Power(s,2)*(2*Power(s1,2)*
             (3*Power(t1,3) - 2*Power(t1,2)*t2 - t1*t2*(5*s2 + 2*t2) + 
               t2*(-2*Power(s2,2) - s2*t2 + Power(t2,2))) - 
            s1*(6*Power(t1,4) - 5*Power(t1,3)*t2 + 
               Power(s2,2)*(16*t1 - 7*t2)*t2 - 
               4*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
               Power(t2,4) + 
               s2*(-10*Power(t1,3) + 7*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) - 6*Power(t2,3))) + 
            t1*(t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(3*Power(t1,2) - 11*t1*t2 + 9*Power(t2,2)) - 
               s2*(2*Power(t1,3) - 6*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  3*Power(t2,3)))) + 
         s*(2*Power(s1,3)*(-Power(s2,3) - Power(t1,3) + t1*Power(t2,2) + 
               Power(s2,2)*(t1 + t2) + s2*t2*(3*t1 + 2*t2)) + 
            s2*t1*(2*t1*(t1 - t2)*Power(t2,2) - 
               Power(s2,2)*(Power(t1,2) - 5*t1*t2 + 5*Power(t2,2)) + 
               s2*(Power(t1,3) - 4*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
                  3*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*(-2*t1 + 9*t2) + 
               2*Power(s2,2)*(Power(t1,2) + 4*t1*t2 + Power(t2,2)) + 
               t1*(6*Power(t1,3) - Power(t1,2)*t2 - 7*t1*Power(t2,2) + 
                  2*Power(t2,3)) - 
               s2*(8*Power(t1,3) + 6*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  7*Power(t2,3))) - 
            s1*(Power(s2,3)*(Power(t1,2) - 15*t1*t2 + 8*Power(t2,2)) + 
               t1*t2*(2*Power(t1,3) - Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(-4*Power(t1,4) + 3*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 3*Power(t2,4)\
))))*dilog(1 - (-s + s1 - t2)/(-s + s2 - t1)))/
     (s1*Power(s2,2)*t1*(s - s2 + t1)*(-s + s1 - t2)*t2*(s2 - t1 + t2)) - 
    (16*(Power(s1,5)*(s2 - t1)*t1*(Power(s2,2) + Power(t2,2)) + 
         Power(s,5)*t1*(t1 - t2)*(Power(s2,2) + Power(t2,2)) - 
         Power(s1,4)*(Power(s2,2) + Power(t2,2))*
          (Power(s2,3) + 6*s2*Power(t1,2) - Power(t1,3) - 
            Power(s2,2)*(6*t1 + t2)) + 
         Power(s2,3)*(s2 - t1)*Power(t2,3)*
          (Power(s2,2) + t2*(-t1 + 2*t2) + s2*(t1 + 3*t2)) + 
         Power(s1,3)*(3*Power(t1,2)*Power(t2,4) + 
            Power(s2,5)*(4*t1 + 7*t2) + 
            2*s2*t1*Power(t2,2)*(Power(t1,2) + 3*t1*t2 - Power(t2,2)) - 
            Power(s2,2)*Power(t2,2)*
             (8*Power(t1,2) + 11*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,4)*(-6*Power(t1,2) - 9*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,3)*(2*Power(t1,3) + 5*Power(t1,2)*t2 + 
               8*Power(t2,3))) + 
         Power(s1,2)*t2*(2*Power(s2,6) - 
            Power(t1,2)*Power(t2,3)*(t1 + 2*t2) - 
            Power(s2,5)*(5*t1 + 7*t2) + 
            Power(s2,4)*(8*Power(t1,2) + 9*t1*t2 - 12*Power(t2,2)) - 
            s2*t1*Power(t2,2)*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*Power(t2,2)*
             (10*Power(t1,2) + 3*t1*t2 + Power(t2,2)) - 
            Power(s2,3)*(3*Power(t1,3) + 6*Power(t1,2)*t2 - 
               4*t1*Power(t2,2) + 14*Power(t2,3))) + 
         s1*s2*Power(t2,2)*(-3*Power(s2,5) + 2*Power(s2,4)*(t1 - t2) + 
            t1*Power(t2,3)*(-3*t1 + 2*t2) - 
            Power(s2,3)*(2*Power(t1,2) + t1*t2 - 4*Power(t2,2)) + 
            Power(s2,2)*(Power(t1,3) + 5*Power(t1,2)*t2 - 
               4*t1*Power(t2,2) + 7*Power(t2,3)) + 
            s2*(-(Power(t1,3)*t2) + t1*Power(t2,3))) + 
         Power(s,4)*(Power(s2,2) + Power(t2,2))*
          (t1*(-3*s2*t1 + Power(t1,2) + 4*s2*t2 - t1*t2) + 
            s1*(-5*Power(t1,2) + 5*t1*t2 - Power(t2,2) + s2*(2*t1 + t2))) \
- Power(s,2)*(Power(s1,3)*(Power(s2,2) + Power(t2,2))*
             (2*Power(s2,2) + 10*Power(t1,2) - 7*t1*t2 + Power(t2,2) - 
               3*s2*(3*t1 + t2)) + 
            s1*(-2*Power(s2,5)*(t1 + 2*t2) + 
               Power(t2,4)*(-5*Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               Power(s2,4)*(10*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
               2*Power(s2,2)*Power(t2,2)*
                (6*Power(t1,2) + t1*t2 + 9*Power(t2,2)) - 
               Power(s2,3)*(6*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 2*Power(t2,3)) - 
               s2*Power(t2,2)*
                (6*Power(t1,3) + 4*Power(t1,2)*t2 - 15*t1*Power(t2,2) + 
                  Power(t2,3))) + 
            t2*(Power(t1,2)*(t1 - t2)*Power(t2,3) + 
               Power(s2,5)*(-3*t1 + 2*t2) + 
               Power(s2,4)*t2*(3*t1 + 5*t2) + 
               Power(s2,2)*Power(t2,2)*
                (Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
               s2*t1*Power(t2,2)*
                (2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(3*Power(t1,3) + Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*(Power(s2,5) - Power(s2,4)*(14*t1 + 13*t2) + 
               3*Power(s2,3)*(9*Power(t1,2) - 4*t1*t2 - 3*Power(t2,2)) + 
               s2*Power(t2,2)*
                (27*Power(t1,2) - 12*t1*t2 + 2*Power(t2,2)) + 
               Power(t2,2)*(-6*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 2*Power(t2,3)) - 
               3*Power(s2,2)*(2*Power(t1,3) - Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s,3)*(t1*Power(t2,4)*(-t1 + t2) + 
            Power(s2,4)*(2*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
            Power(s1,2)*(Power(s2,2) + Power(t2,2))*
             (Power(s2,2) - 7*s2*t1 + 10*Power(t1,2) - 3*s2*t2 - 
               9*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*Power(t2,2)*
             (2*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2)) + 
            s2*t1*Power(t2,2)*(-2*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
            Power(s2,3)*(-2*Power(t1,3) + 3*Power(t2,3)) - 
            s1*(-3*s2*t1*(5*t1 - 4*t2)*Power(t2,2) + 
               4*Power(s2,4)*(t1 + t2) + 
               Power(s2,3)*(-15*Power(t1,2) + 12*t1*t2 + 
                  4*Power(t2,2)) + 
               Power(t2,2)*(4*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 14*Power(t2,3)))) + 
         s*(Power(s1,4)*(Power(s2,2) + Power(t2,2))*
             (Power(s2,2) + t1*(5*t1 - 2*t2) - s2*(5*t1 + t2)) + 
            Power(s1,3)*(2*Power(s2,5) - 2*Power(s2,4)*(8*t1 + 5*t2) + 
               Power(s2,3)*(21*Power(t1,2) - 4*t1*t2 - 4*Power(t2,2)) + 
               s2*Power(t2,2)*
                (21*Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) + 
               t1*Power(t2,2)*(-4*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,3) + Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) - 14*Power(t2,3))) + 
            s2*Power(t2,2)*(Power(s2,5) - Power(t1,2)*Power(t2,3) + 
               Power(s2,4)*(3*t1 + t2) + 
               s2*t1*t2*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) - 
               Power(s2,3)*(3*Power(t1,2) + t1*t2 + 4*Power(t2,2)) - 
               Power(s2,2)*(Power(t1,3) + 3*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 4*Power(t2,3))) - 
            Power(s1,2)*(t1*(7*t1 - 2*t2)*Power(t2,4) + 
               6*Power(s2,5)*(t1 + 2*t2) + 
               Power(s2,4)*(-14*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) - 
               2*Power(s2,2)*Power(t2,2)*
                (9*Power(t1,2) + 10*t1*t2 + 9*Power(t2,2)) + 
               s2*Power(t2,2)*
                (6*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,3)*(6*Power(t1,3) + 10*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 7*Power(t2,3))) + 
            s1*t2*(-Power(s2,6) - Power(s2,5)*(t1 - 10*t2) + 
               Power(s2,2)*Power(t2,2)*
                (-9*Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
               t1*Power(t2,3)*(2*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(s2,4)*(-8*Power(t1,2) - 7*t1*t2 + 17*Power(t2,2)) + 
               s2*Power(t2,2)*
                (4*Power(t1,3) - 7*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,3)*(6*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) + 15*Power(t2,3)))))*
       dilog(1 - t1/(-s2 + t1 - t2)))/
     (s1*Power(s2,2)*t1*(s - s2 + t1)*Power(t2,2)*Power(s - s1 + t2,2)*
       (s2 + t2)) + (16*(2*Power(s1,6)*s2*t1*(-s2 + t1) + 
         2*Power(s,6)*t1*(t1 - t2)*t2 + 
         Power(s1,5)*(s2*Power(t1,2)*(t1 - 5*t2) + 2*Power(t1,3)*t2 - 
            Power(s2,3)*(2*t1 + t2) + Power(s2,2)*t1*(t1 + 2*t2)) + 
         Power(s2,2)*Power(s2 - t1,2)*t1*Power(t2,2)*
          (-(t1*(2*t1 + t2)) + s2*(3*t1 + 2*t2)) - 
         s1*s2*Power(s2 - t1,2)*t2*
          (Power(s2,2)*t1*(-t1 + t2) + Power(s2,3)*(t1 + t2) + 
            Power(t1,3)*(-2*t1 + 3*t2) + 
            s2*t1*(3*Power(t1,2) - 8*t1*t2 - 2*Power(t2,2))) - 
         Power(s1,4)*(Power(s2,4)*(t1 + t2) - 
            2*s2*Power(t1,2)*t2*(t1 + t2) + Power(t1,3)*t2*(2*t1 + t2) + 
            Power(s2,2)*t1*t2*(-3*t1 + 2*t2) - 
            Power(s2,3)*(Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
         Power(s1,3)*(Power(t1,4)*(2*t1 - t2)*t2 + 
            Power(s2,5)*(2*t1 + t2) + 
            3*Power(s2,3)*t1*(7*Power(t1,2) + Power(t2,2)) + 
            Power(s2,4)*(-11*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*t1*(-17*Power(t1,3) + 18*Power(t1,2)*t2 - 
               6*t1*Power(t2,2) + Power(t2,3)) + 
            s2*Power(t1,2)*(5*Power(t1,3) - 14*Power(t1,2)*t2 + 
               6*t1*Power(t2,2) + Power(t2,3))) + 
         Power(s1,2)*s2*(-2*Power(t1,6) + 5*Power(t1,5)*t2 + 
            Power(t1,3)*Power(t2,3) + Power(s2,5)*(t1 + t2) - 
            Power(s2,4)*(7*Power(t1,2) + 4*t1*t2 + Power(t2,2)) + 
            Power(s2,3)*t1*(17*Power(t1,2) - 2*t1*t2 + 8*Power(t2,2)) + 
            s2*Power(t1,2)*(10*Power(t1,3) - 17*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - 3*Power(t2,3)) + 
            Power(s2,2)*t1*(-19*Power(t1,3) + 17*Power(t1,2)*t2 - 
               10*t1*Power(t2,2) + 3*Power(t2,3))) + 
         Power(s,5)*(t1*t2*(-6*s1*t1 + 5*Power(t1,2) + 4*s1*t2 - 
               3*t1*t2 - 2*Power(t2,2)) + 
            s2*(-2*Power(t1,3) - 6*Power(t1,2)*t2 + s1*Power(t2,2) + 
               t1*t2*(s1 + 10*t2))) + 
         s*(Power(s1,5)*s2*(t1*(-6*t1 + t2) + s2*(4*t1 + t2)) + 
            Power(s1,4)*(3*Power(t1,2)*t2*(-2*t1 + t2) + 
               2*Power(s2,3)*(2*t1 + t2) + 
               Power(s2,2)*(-5*Power(t1,2) + 12*t1*t2 - 
                  2*Power(t2,2)) + 
               s2*t1*(-2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) + 
            s2*(s2 - t1)*t1*t2*
             (2*Power(s2,3)*(t1 + t2) - 
               Power(s2,2)*t2*(13*t1 + 10*t2) + 
               Power(t1,2)*(2*Power(t1,2) - t1*t2 - 2*Power(t2,2)) + 
               2*s2*t1*(-2*Power(t1,2) + 5*t1*t2 + 5*Power(t2,2))) - 
            Power(s1,3)*(3*Power(s2,4)*(t1 + t2) + 
               Power(t1,2)*t2*
                (-5*Power(t1,2) + 4*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(-16*Power(t1,2) - 28*t1*t2 + 
                  3*Power(t2,2)) + 
               Power(s2,2)*t1*
                (24*Power(t1,2) + t1*t2 + 5*Power(t2,2)) - 
               2*s2*t1*(5*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - Power(t2,3))) + 
            s1*(s2 - t1)*(Power(t1,4)*Power(t2,2) + 
               5*Power(s2,3)*t1*(-2*Power(t1,2) + Power(t2,2)) + 
               Power(s2,4)*(3*Power(t1,2) + 5*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*t1*
                (10*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  27*t1*Power(t2,2) - 8*Power(t2,3)) + 
               s2*Power(t1,2)*
                (-3*Power(t1,3) - 7*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) + 4*Power(t2,3))) - 
            Power(s1,2)*(Power(s2,5)*(3*t1 + 4*t2) + 
               Power(t1,3)*t2*(Power(t1,2) - 5*t1*t2 + Power(t2,2)) - 
               Power(s2,4)*(23*Power(t1,2) + 19*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*t1*
                (52*Power(t1,2) - 5*t1*t2 + 21*Power(t2,2)) + 
               s2*Power(t1,2)*
                (15*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-47*Power(t1,3) + 40*Power(t1,2)*t2 - 
                  24*t1*Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s,4)*(Power(s1,2)*
             (Power(s2,2)*t2 + t1*(7*t1 - 3*t2)*t2 + 
               s2*(Power(t1,2) - 7*t1*t2 - Power(t2,2))) + 
            s1*(s2*t1*(8*Power(t1,2) + 14*t1*t2 - 15*Power(t2,2)) + 
               t1*t2*(-14*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(3*Power(t1,2) + 5*t1*t2 + 5*Power(t2,2))) + 
            t1*(Power(s2,2)*(6*Power(t1,2) + 4*t1*t2 - 20*Power(t2,2)) + 
               t1*t2*(4*Power(t1,2) + t1*t2 - 5*Power(t2,2)) + 
               s2*(-5*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) + 10*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,3)*
             (Power(s2,2)*(t1 - t2) + t1*t2*(-4*t1 + t2) - 
               s2*(4*Power(t1,2) - 12*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(-(Power(s2,3)*(t1 + 4*t2)) + 
               t1*t2*(10*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) + 
               s2*t1*(-11*Power(t1,2) - 13*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(7*Power(t1,2) + 25*t1*t2 + 4*Power(t2,2))) + 
            s1*(Power(t1,2)*t2*
                (-10*Power(t1,2) - 3*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(9*Power(t1,2) + 10*t1*t2 + 
                  10*Power(t2,2)) + 
               Power(s2,2)*t1*
                (-29*Power(t1,2) - 9*t1*t2 + 20*Power(t2,2)) + 
               s2*t1*(19*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  24*t1*Power(t2,2) - 8*Power(t2,3))) + 
            t1*(Power(t1,2)*t2*(Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,3)*(-6*Power(t1,2) + 4*t1*t2 + 20*Power(t2,2)) + 
               Power(s2,2)*(10*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  30*t1*Power(t2,2) - 20*Power(t2,3)) + 
               s2*t1*(-4*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 20*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*
             (Power(t1,2)*t2 - Power(s2,2)*(3*t1 + t2) + 
               s2*(7*Power(t1,2) - 7*t1*t2 + Power(t2,2))) + 
            Power(s1,3)*(3*Power(s2,3)*t2 - 
               Power(s2,2)*(Power(t1,2) + 32*t1*t2 - 3*Power(t2,2)) + 
               t1*t2*(3*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               s2*t1*(6*Power(t1,2) + 5*t1*t2 + Power(t2,2))) - 
            s1*(Power(t1,3)*t2*
                (2*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,3)*t1*
                (-34*Power(t1,2) - 3*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,4)*(9*Power(t1,2) + 10*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*t1*
                (39*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  42*t1*Power(t2,2) - 12*Power(t2,3)) - 
               2*s2*Power(t1,2)*
                (7*Power(t1,3) + 6*Power(t1,2)*t2 - 7*t1*Power(t2,2) - 
                  6*Power(t2,3))) + 
            Power(s1,2)*(3*Power(s2,4)*(t1 + 2*t2) + 
               Power(t1,2)*t2*
                (3*Power(t1,2) + 10*t1*t2 - 3*Power(t2,2)) - 
               3*Power(s2,3)*
                (8*Power(t1,2) + 11*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*t1*
                (46*Power(t1,2) + 3*t1*t2 + 15*Power(t2,2)) + 
               s2*t1*(-24*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  18*t1*Power(t2,2) + 9*Power(t2,3))) + 
            t1*(Power(t1,3)*(t1 - t2)*Power(t2,2) + 
               2*Power(s2,4)*(Power(t1,2) - 3*t1*t2 - 5*Power(t2,2)) + 
               Power(s2,2)*t1*
                (4*Power(t1,3) + 12*Power(t1,2)*t2 - 21*t1*Power(t2,2) - 
                  30*Power(t2,3)) + 
               s2*Power(t1,2)*
                (-Power(t1,3) - 7*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(s2,3)*(-5*Power(t1,3) + Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) + 20*Power(t2,3)))))*
       dilog(1 - (-s + s1 - t2)/(-s2 + t1 - t2)))/
     (Power(s1,2)*s2*(-s + s1 + s2 - t1)*Power(t1,2)*Power(s - s2 + t1,2)*
       t2*(s - s1 + t2)) - (16*
       (Power(s,6)*t1*(2*s1 + 3*t1 - 3*t2)*t2 + 
         s1*s2*(Power(s1,2) - Power(s2,2))*t1*(s1 - t2)*(s1 + t1 - t2)*
          (s1*(-s2 + t1) + s2*t2) - 
         Power(s,5)*t2*(4*Power(s1,2)*t1 + 
            s1*(6*s2*t1 + 4*Power(t1,2) - s2*t2 - 7*t1*t2) + 
            s2*(10*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
            t1*(-5*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2))) + 
         Power(s,4)*(2*Power(s1,3)*t1*t2 + 
            Power(s1,2)*(Power(s2,2)*t2 - 5*t1*Power(t2,2) + 
               s2*(Power(t1,2) + 4*t1*t2 - Power(t2,2))) + 
            t2*(Power(s2,2)*(13*Power(t1,2) - 13*t1*t2 + 
                  3*Power(t2,2)) + 
               t1*(3*Power(t1,3) + 2*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) - Power(t2,3)) - 
               s2*(15*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + Power(t2,3))) + 
            s1*(4*Power(s2,2)*(2*t1 - t2)*t2 + 
               t1*t2*(-9*Power(t1,2) + 4*t1*t2 + 5*Power(t2,2)) + 
               s2*(Power(t1,3) + 4*Power(t1,2)*t2 - 13*t1*Power(t2,2) + 
                  2*Power(t2,3)))) - 
         Power(s,3)*(2*Power(s1,4)*s2*t1 + 
            Power(s1,3)*(-(t1*Power(t2,2)) + Power(s2,2)*(-t1 + t2) + 
               s2*(8*Power(t1,2) - 12*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3)*(t1 + t2) + 
               Power(s2,2)*(Power(t1,2) - t1*t2 - 5*Power(t2,2)) + 
               t1*t2*(-4*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2)) + 
               s2*(7*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - Power(t2,3))) + 
            t2*(-Power(t1,5) - 2*Power(t1,4)*t2 + 
               2*Power(t1,3)*Power(t2,2) + t1*Power(t2,4) + 
               Power(s2,3)*(8*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(9*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) - Power(t2,3)) - 
               Power(s2,2)*(16*Power(t1,3) + Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 3*Power(t2,3))) + 
            s1*(Power(s2,3)*(2*Power(t1,2) + 4*t1*t2 - 5*Power(t2,2)) + 
               s2*Power(t1,2)*
                (Power(t1,2) - 18*t1*t2 + 16*Power(t2,2)) + 
               t1*t2*(5*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,2)*(2*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 7*Power(t2,3)))) - 
         s*(2*Power(s1,6)*s2*t1 + 
            Power(s1,5)*s2*(s2*t1 + 8*Power(t1,2) - s2*t2 - 6*t1*t2) + 
            s2*(-s2 + t1)*Power(t2,2)*
             (2*Power(t1,4) - Power(t1,3)*t2 - t1*Power(t2,3) + 
               2*s2*Power(t1,2)*(-2*t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2))) + 
            Power(s1,4)*(Power(s2,3)*t1 - Power(t1,3)*t2 + 
               Power(s2,2)*(7*Power(t1,2) - 13*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(7*Power(t1,2) - 12*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,3)*(Power(s2,3)*t1*(t1 - 10*t2) + 
               Power(s2,4)*(2*t1 + t2) + 
               s2*Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*t2*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(7*Power(t1,3) - 21*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 3*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*t1*t2*(-13*t1 + 18*t2) + 
               Power(s2,4)*(2*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2)) - 
               Power(t1,2)*t2*
                (Power(t1,3) + t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*t1*t2*(Power(t1,3) - Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,4) - 5*Power(t1,3)*t2 + 
                  13*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            s1*t2*(Power(s2,4)*t2*(-2*t1 + 3*t2) - 
               2*Power(s2,3)*t1*
                (4*Power(t1,2) - 8*t1*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*t2*
                (Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*t1*
                (6*Power(t1,3) - 5*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) + 
               s2*t1*(-2*Power(t1,4) - Power(t1,3)*t2 + t1*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(s,2)*(4*Power(s1,5)*s2*t1 + 
            Power(s1,4)*(Power(s2,2)*(t1 - t2) + Power(t1,2)*t2 + 
               s2*(14*Power(t1,2) - 16*t1*t2 + Power(t2,2))) + 
            Power(s1,3)*(-(Power(t1,3)*t2) + 2*t1*Power(t2,3) + 
               2*Power(s2,3)*(t1 + t2) + 
               Power(s2,2)*(9*Power(t1,2) - 22*t1*t2 + 2*Power(t2,2)) + 
               s2*(12*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) - 2*Power(t2,3))) + 
            s1*(2*Power(s2,4)*(Power(t1,2) - Power(t2,2)) + 
               Power(s2,3)*t2*
                (-2*Power(t1,2) - 5*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*t1*
                (Power(t1,3) - 19*Power(t1,2)*t2 + 29*t1*Power(t2,2) - 
                  14*Power(t2,3)) + 
               2*s2*t1*t2*(5*Power(t1,3) - Power(t1,2)*t2 - 
                  3*Power(t2,3)) - 
               2*t1*t2*(Power(t1,4) + Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) - 
                  Power(t2,4))) + 
            Power(s1,2)*(Power(s2,4)*(2*t1 + t2) + 
               Power(s2,3)*(2*Power(t1,2) - 4*t1*t2 - 7*Power(t2,2)) + 
               t1*t2*(Power(t1,3) + 3*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(9*Power(t1,3) - 34*Power(t1,2)*t2 + 
                  34*t1*Power(t2,2) - Power(t2,3)) + 
               s2*(2*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            t2*(Power(s2,4)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               Power(t1,2)*t2*
                (Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(s2,2)*t1*
                (6*Power(t1,3) + 13*Power(t1,2)*t2 - 12*t1*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s2,3)*(6*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 3*Power(t2,3)) - 
               2*s2*(Power(t1,5) + 4*Power(t1,4)*t2 - 
                  3*Power(t1,3)*Power(t2,2) - t1*Power(t2,4)))))*
       dilog(1 - (-s2 + t1 - t2)/(s - s1 - s2)))/
     (s*s1*s2*Power(t1,2)*(s - s2 + t1)*(s1 + t1 - t2)*t2*
       Power(s - s1 + t2,2)) - 
    (16*(Power(s,5)*Power(t1 - t2,2) + 
         s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*t1*
          (s1*(s2 - t1) - s2*t2) + 
         Power(s,4)*(s1*(-4*Power(t1,2) + 7*t1*t2 + (2*s2 - 3*t2)*t2) + 
            (t1 - t2)*(-2*s2*t1 + 4*s2*t2 + 2*t1*t2 - Power(t2,2))) + 
         Power(s,3)*(Power(s1,2)*
             (Power(s2,2) - 3*s2*t1 + 7*Power(t1,2) - 4*s2*t2 - 
               9*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,2) - 7*t1*t2 + 6*Power(t2,2)) + 
            t1*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
               Power(t2,3)) - 
            s2*(2*Power(t1,3) + 2*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               3*Power(t2,3)) - 
            s1*(-2*Power(t1,3) + 7*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               2*Power(t2,3) + Power(s2,2)*(t1 + 5*t2) + 
               s2*(-3*Power(t1,2) + 17*t1*t2 - 12*Power(t2,2)))) - 
         Power(s,2)*(Power(s2,3)*Power(t1 - 2*t2,2) + 
            Power(t1,3)*t2*(-t1 + t2) + 
            Power(s1,3)*(Power(s2,2) - 5*s2*t1 + 6*Power(t1,2) - 
               2*s2*t2 - 4*t1*t2) + 
            Power(s2,2)*(-2*Power(t1,3) + 3*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            s2*t1*(2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,3) - Power(s2,2)*(7*t1 + 10*t2) + 
               t1*(4*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               2*s2*(3*Power(t1,2) - 6*t1*t2 + 4*Power(t2,2))) + 
            s1*(-2*Power(s2,3)*(t1 + 2*t2) + 
               Power(t1,2)*(2*Power(t1,2) - 3*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,2) - 13*t1*t2 + 15*Power(t2,2)) + 
               s2*(Power(t1,3) + Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  4*Power(t2,3)))) - 
         s*(2*Power(s1,4)*(s2 - t1)*t1 + 
            Power(s1,3)*(-Power(s2,3) + 2*Power(t1,2)*(-t1 + t2) + 
               4*Power(s2,2)*(2*t1 + t2) + s2*t1*(-7*t1 + 2*t2)) + 
            Power(s1,2)*(-(Power(t1,3)*(t1 - 2*t2)) + 
               s2*Power(t1,2)*(-3*t1 + t2) + Power(s2,3)*(4*t1 + 6*t2) + 
               Power(s2,2)*(-2*Power(t1,2) + t1*t2 - 6*Power(t2,2))) + 
            s2*t2*(Power(s2,3)*(t1 - t2) + Power(t1,3)*(2*t1 - t2) + 
               Power(s2,2)*Power(t2,2) + 
               s2*(-2*Power(t1,3) + t1*Power(t2,2))) + 
            s1*(Power(t1,4)*t2 + Power(s2,4)*(t1 + t2) - 
               s2*Power(t1,2)*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               2*Power(s2,3)*(Power(t1,2) - t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) - 4*Power(t1,2)*t2 - 
                  t1*Power(t2,2) + 2*Power(t2,3)))))*dilog(1 - s1/t2))/
     (s*s1*s2*Power(t1,2)*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (16*(-(Power(s1,5)*(Power(s2,2) + t1*t2 + s2*(-t1 + t2))) + 
         Power(s2,2)*Power(t2,2)*
          (-Power(s2,3) + s2*(3*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            t1*(-2*Power(t1,2) + t1*t2 + Power(t2,2))) + 
         Power(s1,4)*(-Power(s2,3) + 2*Power(s2,2)*t2 - 
            t1*(t1 - 2*t2)*t2 + 
            s2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
         s1*s2*t2*(2*Power(s2,4) - Power(s2,3)*(t1 + 3*t2) + 
            Power(s2,2)*(-4*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) + 
            Power(t1,2)*(2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            s2*(Power(t1,3) + 2*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
               2*Power(t2,3))) - 
         Power(s1,3)*(2*Power(s2,4) - 4*Power(s2,3)*(t1 + t2) + 
            Power(s2,2)*(4*Power(t1,2) + 6*t1*t2 - 3*Power(t2,2)) + 
            t1*t2*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            s2*(-2*Power(t1,3) + 3*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               3*Power(t2,3))) - 
         Power(s1,2)*(Power(s2,5) - Power(t1,3)*Power(t2,2) - 
            Power(s2,4)*(t1 + 5*t2) + 
            Power(s2,3)*(-Power(t1,2) + 6*t1*t2 + Power(t2,2)) + 
            s2*t2*(-2*Power(t1,3) + 3*Power(t1,2)*t2 + t1*Power(t2,2) - 
               Power(t2,3)) + 
            Power(s2,2)*(Power(t1,3) + 3*Power(t1,2)*t2 - 
               11*t1*Power(t2,2) + 6*Power(t2,3))) + 
         Power(s,4)*((t1 - t2)*t2*(s2 - t1 + t2) + 
            s1*(t2*(-t1 + t2) + s2*(t1 + t2))) + 
         Power(s,3)*(Power(s2,2)*Power(t1 - 2*t2,2) + 
            Power(t1 - t2,3)*t2 - 
            s2*(Power(t1,3) - 4*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,2) + (4*t1 - 3*t2)*t2 - 
               2*s2*(2*t1 + t2)) + 
            s1*(2*s2*t1*t2 + 4*Power(t1 - t2,2)*t2 - 
               2*Power(s2,2)*(t1 + 2*t2))) + 
         Power(s,2)*(t1*Power(t1 - t2,2)*Power(t2,2) - 
            Power(s2,3)*(Power(t1,2) - 5*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,3) - 5*Power(t1,2)*t2 + 
               6*t1*Power(t2,2) - 3*Power(t2,3)) - 
            s2*(Power(t1,4) - 5*Power(t1,2)*Power(t2,2) + 
               7*t1*Power(t2,3) - 3*Power(t2,4)) + 
            Power(s1,3)*(-Power(s2,2) + 3*t2*(-2*t1 + t2) + 
               2*s2*(2*t1 + t2)) + 
            Power(s1,2)*(-2*Power(s2,3) + Power(s2,2)*(10*t1 + 9*t2) + 
               t2*(-6*Power(t1,2) + 12*t1*t2 - 5*Power(t2,2)) + 
               s2*(-4*Power(t1,2) - 8*t1*t2 + 6*Power(t2,2))) + 
            s1*(Power(s2,3)*(t1 + 7*t2) + 
               s2*t2*(-8*Power(t1,2) + 22*t1*t2 - 11*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
               t2*(-3*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 2*Power(t2,3)))) + 
         s*(Power(s1,4)*(Power(s2,2) - 2*s2*t1 + (4*t1 - t2)*t2) + 
            Power(s1,3)*(2*Power(s2,3) - 7*Power(s2,2)*(t1 + t2) + 
               s2*(3*Power(t1,2) + 10*t1*t2 - 7*Power(t2,2)) + 
               2*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
            s2*t2*(-2*Power(s2,3)*(t1 - 2*t2) + 
               Power(s2,2)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               s2*(2*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 3*Power(t2,3)) + 
               t1*(-2*Power(t1,3) + 3*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            Power(s1,2)*(2*Power(s2,4) - Power(s2,3)*(7*t1 + 12*t2) + 
               2*Power(s2,2)*(2*Power(t1,2) + 5*t1*t2 - Power(t2,2)) - 
               s2*(Power(t1,3) - 5*Power(t1,2)*t2 + 20*t1*Power(t2,2) - 
                  11*Power(t2,3)) - 
               t2*(-3*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3))) - 
            s1*(6*Power(s2,4)*t2 + 2*Power(t1,2)*(t1 - t2)*Power(t2,2) + 
               Power(s2,3)*(Power(t1,2) + t1*t2 - 8*Power(t2,2)) - 
               2*Power(s2,2)*t2*
                (6*Power(t1,2) - 9*t1*t2 + 5*Power(t2,2)) + 
               s2*(-Power(t1,4) + 3*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
                  4*Power(t2,4)))))*dilog(1 - t1/t2))/
     (Power(s1,2)*s2*(-s + s2 - t1)*t1*t2*(s - s1 + t2)*(s2 - t1 + t2)) - 
    (8*(-(Power(s,3)*(2*Power(s1,3) + 3*Power(s1,2)*(t1 - t2) + 
              4*s1*Power(t1 - t2,2) + Power(t1 - t2,3))) - 
         s2*(2*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
          Power(t2,3) + 2*Power(s1,4)*
          (Power(s2,2) + Power(t1,2) - t1*t2 + Power(t2,2) + 
            s2*(-2*t1 + t2)) + 
         Power(s1,3)*(2*Power(s2,3) - 4*Power(s2,2)*(t1 + t2) + 
            t2*(-4*Power(t1,2) + 9*t1*t2 - 6*Power(t2,2)) + 
            s2*(2*Power(t1,2) + 8*t1*t2 - 5*Power(t2,2))) + 
         s1*Power(t2,2)*(6*Power(s2,3) + (t1 - 2*t2)*Power(t1 - t2,2) + 
            Power(s2,2)*(-8*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
         Power(s1,2)*t2*(-6*Power(s2,3) + 10*Power(s2,2)*t1 + 
            6*Power(t1 - t2,2)*t2 + 
            s2*(-4*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) + 
         Power(s,2)*(2*Power(s1,4) + Power(s1,3)*(3*s2 + t1 - 4*t2) - 
            3*s2*Power(t1 - t2,2)*t2 + 
            s1*(t1 - t2)*(Power(t1,2) + s2*(t1 - 9*t2) - 5*t1*t2 + 
               4*Power(t2,2)) + 
            Power(s1,2)*(6*Power(t1 - t2,2) - s2*(2*t1 + 9*t2))) + 
         s*(-2*Power(s1,4)*(s2 - t1) - 
            (4*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
             (t1 - t2)*Power(t2,2) - 
            4*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               Power(t2,2) - s2*(2*t1 + t2)) + 
            2*s1*t2*(Power(s2,2)*(t1 - 6*t2) - 3*Power(t1 - t2,2)*t2 + 
               s2*(Power(t1,2) + t1*t2 - 2*Power(t2,2))) + 
            Power(s1,2)*(-2*s2*t1*(t1 + 3*t2) + 
               2*Power(s2,2)*(t1 + 6*t2) + 
               3*t2*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)))))*
       dilog((s1 + t1 - t2)/s))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) + 
    (16*(-(Power(s,3)*(2*Power(s1,3) + 3*Power(s1,2)*(t1 - t2) + 
              4*s1*Power(t1 - t2,2) + Power(t1 - t2,3))) - 
         s2*(2*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
          Power(t2,3) + 2*Power(s1,4)*
          (Power(s2,2) + Power(t1,2) - t1*t2 + Power(t2,2) + 
            s2*(-2*t1 + t2)) + 
         Power(s1,3)*(2*Power(s2,3) - 4*Power(s2,2)*(t1 + t2) + 
            t2*(-4*Power(t1,2) + 9*t1*t2 - 6*Power(t2,2)) + 
            s2*(2*Power(t1,2) + 8*t1*t2 - 5*Power(t2,2))) + 
         s1*Power(t2,2)*(6*Power(s2,3) + (t1 - 2*t2)*Power(t1 - t2,2) + 
            Power(s2,2)*(-8*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
         Power(s1,2)*t2*(-6*Power(s2,3) + 10*Power(s2,2)*t1 + 
            6*Power(t1 - t2,2)*t2 + 
            s2*(-4*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) + 
         Power(s,2)*(2*Power(s1,4) + Power(s1,3)*(3*s2 + t1 - 4*t2) - 
            3*s2*Power(t1 - t2,2)*t2 + 
            s1*(t1 - t2)*(Power(t1,2) + s2*(t1 - 9*t2) - 5*t1*t2 + 
               4*Power(t2,2)) + 
            Power(s1,2)*(6*Power(t1 - t2,2) - s2*(2*t1 + 9*t2))) + 
         s*(-2*Power(s1,4)*(s2 - t1) - 
            (4*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
             (t1 - t2)*Power(t2,2) - 
            4*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               Power(t2,2) - s2*(2*t1 + t2)) + 
            2*s1*t2*(Power(s2,2)*(t1 - 6*t2) - 3*Power(t1 - t2,2)*t2 + 
               s2*(Power(t1,2) + t1*t2 - 2*Power(t2,2))) + 
            Power(s1,2)*(-2*s2*t1*(t1 + 3*t2) + 
               2*Power(s2,2)*(t1 + 6*t2) + 
               3*t2*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)))))*
       dilog((s1 + t1 - t2)/(s1 + s2)))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) + 
    (16*(2*Power(s1,3)*Power(s2 - t1,3) - 2*Power(s2,3)*Power(t2,3) + 
         Power(s,3)*Power(t1 - t2,2)*(t1 + t2) + 
         2*s*Power(s2,2)*Power(t2,2)*(-t1 + 2*t2) - 
         Power(s,2)*s2*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
         2*Power(s1,2)*(s2 - t1)*
          (3*s2*(-s2 + t1)*t2 + s*(t1*(-2*t1 + t2) + 2*s2*(t1 + t2))) + 
         s1*(6*Power(s2,2)*(s2 - t1)*Power(t2,2) + 
            2*s*s2*t2*(t1*(t1 + t2) - s2*(t1 + 4*t2)) + 
            Power(s,2)*(-(t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
               s2*(3*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)))))*
       dilog(-(s/(-s + s1 + t1 - t2))))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) + 
    (16*(Power(s1,3)*Power(s2 - t1,2)*(s2 + t1) - 
         2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*(2*s2 - t1)*Power(t1 - t2,3) - 
         s*(3*Power(s2,2) - 4*s2*t1 + Power(t1,2))*Power(t1 - t2,3) + 
         s2*(Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*(2*Power(t1,3) - 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - Power(t2,3)) + 
            2*s2*t1*(-2*Power(t1,3) + 4*Power(t1,2)*t2 - 
               3*t1*Power(t2,2) + Power(t2,3))) - 
         Power(s1,2)*(-(s2*(s2 - t1)*
               (s2*(4*t1 - 3*t2) + t1*(-4*t1 + t2))) + 
            s*(Power(s2,2)*(t1 - 3*t2) + 3*Power(t1,2)*(t1 - t2) + 
               2*s2*t1*(-2*t1 + t2))) + 
         s1*(Power(t1,3)*Power(t1 - t2,2) + 
            Power(s2,2)*t1*(-5*Power(t1,2) + 14*t1*t2 - 5*Power(t2,2)) + 
            Power(s2,3)*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
            2*Power(s,2)*(t1 - t2)*(2*t1*(-t1 + t2) + s2*(t1 + 2*t2)) + 
            2*s*(Power(t1,2)*Power(t1 - t2,2) - 
               s2*t1*(2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)))))*
       dilog((s1 + t1 - t2)/(-s + s1 + t1 - t2)))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) - 
    (8*(2*Power(s1,3)*Power(s2 - t1,3) - 2*Power(s2,3)*Power(t2,3) + 
         Power(s,3)*Power(t1 - t2,2)*(t1 + t2) + 
         2*s*Power(s2,2)*Power(t2,2)*(-t1 + 2*t2) - 
         Power(s,2)*s2*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
         2*Power(s1,2)*(s2 - t1)*
          (3*s2*(-s2 + t1)*t2 + s*(t1*(-2*t1 + t2) + 2*s2*(t1 + t2))) + 
         s1*(6*Power(s2,2)*(s2 - t1)*Power(t2,2) + 
            2*s*s2*t2*(t1*(t1 + t2) - s2*(t1 + 4*t2)) + 
            Power(s,2)*(-(t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
               s2*(3*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)))))*
       dilog(-(s/(-s2 + t1 - t2))))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (8*(Power(s1,3)*Power(s2 - t1,2)*(s2 + t1) - 
         2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*(2*s2 - t1)*Power(t1 - t2,3) - 
         s*(3*Power(s2,2) - 4*s2*t1 + Power(t1,2))*Power(t1 - t2,3) + 
         s2*(Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*(2*Power(t1,3) - 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - Power(t2,3)) + 
            2*s2*t1*(-2*Power(t1,3) + 4*Power(t1,2)*t2 - 
               3*t1*Power(t2,2) + Power(t2,3))) - 
         Power(s1,2)*(-(s2*(s2 - t1)*
               (s2*(4*t1 - 3*t2) + t1*(-4*t1 + t2))) + 
            s*(Power(s2,2)*(t1 - 3*t2) + 3*Power(t1,2)*(t1 - t2) + 
               2*s2*t1*(-2*t1 + t2))) + 
         s1*(Power(t1,3)*Power(t1 - t2,2) + 
            Power(s2,2)*t1*(-5*Power(t1,2) + 14*t1*t2 - 5*Power(t2,2)) + 
            Power(s2,3)*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
            2*Power(s,2)*(t1 - t2)*(2*t1*(-t1 + t2) + s2*(t1 + 2*t2)) + 
            2*s*(Power(t1,2)*Power(t1 - t2,2) - 
               s2*t1*(2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)))))*
       dilog((s1 + t1 - t2)/(-s2 + t1 - t2)))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) - 
    (8*(Power(s1,3)*Power(s2 - t1,2)*(s2 + t1) - 
         2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*(2*s2 - t1)*Power(t1 - t2,3) - 
         s*(3*Power(s2,2) - 4*s2*t1 + Power(t1,2))*Power(t1 - t2,3) + 
         s2*(Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*(2*Power(t1,3) - 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - Power(t2,3)) + 
            2*s2*t1*(-2*Power(t1,3) + 4*Power(t1,2)*t2 - 
               3*t1*Power(t2,2) + Power(t2,3))) - 
         Power(s1,2)*(-(s2*(s2 - t1)*
               (s2*(4*t1 - 3*t2) + t1*(-4*t1 + t2))) + 
            s*(Power(s2,2)*(t1 - 3*t2) + 3*Power(t1,2)*(t1 - t2) + 
               2*s2*t1*(-2*t1 + t2))) + 
         s1*(Power(t1,3)*Power(t1 - t2,2) + 
            Power(s2,2)*t1*(-5*Power(t1,2) + 14*t1*t2 - 5*Power(t2,2)) + 
            Power(s2,3)*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
            2*Power(s,2)*(t1 - t2)*(2*t1*(-t1 + t2) + s2*(t1 + 2*t2)) + 
            2*s*(Power(t1,2)*Power(t1 - t2,2) - 
               s2*t1*(2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)))))*
       dilog((-s2 + t1 - t2)/(-s1 - s2)))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) - 
    (8*(2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*Power(t1 - t2,2)*(2*s2 + t1 - t2)*t2 + 
         s*(3*Power(s2,2) + 2*s2*(t1 - t2) + Power(t1 - t2,2))*(t1 - t2)*
          Power(t2,2) + s2*(Power(s2,2) + Power(t1 - t2,2))*Power(t2,3) + 
         Power(s1,3)*(Power(s2,3) - Power(t1,3) + 4*Power(t1,2)*t2 - 
            3*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s2,2)*(-3*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) + 
         s1*(2*Power(s,2)*(t1 - t2)*
             (-2*Power(t1 - t2,2) + s2*(2*t1 + t2)) + 
            Power(t2,2)*(-Power(s2,3) - Power(s2,2)*(t1 - 4*t2) - 
               (t1 - 2*t2)*Power(t1 - t2,2) + 
               s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) - 
            2*s*t2*(Power(s2,2)*(t1 - 2*t2) + 2*Power(t1 - t2,3) + 
               s2*(-3*Power(t1,2) + t1*t2 + 2*Power(t2,2)))) + 
         Power(s1,2)*(s*(3*Power(t1 - t2,3) + Power(s2,2)*(3*t1 - t2) + 
               2*s2*(-3*Power(t1,2) + 5*t1*t2 + Power(t2,2))) - 
            t2*(Power(s2,3) - 2*Power(t1,3) - 4*Power(s2,2)*(t1 - 2*t2) + 
               6*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 4*Power(t2,3) + 
               s2*(5*Power(t1,2) - 14*t1*t2 + 5*Power(t2,2)))))*
       dilog(s/(s - s2 + t1 - t2)))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) - 
    (8*(2*Power(s1,3)*Power(s2 - t1,3) - 2*Power(s2,3)*Power(t2,3) + 
         Power(s,3)*Power(t1 - t2,2)*(t1 + t2) + 
         2*s*Power(s2,2)*Power(t2,2)*(-t1 + 2*t2) - 
         Power(s,2)*s2*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
         2*Power(s1,2)*(s2 - t1)*
          (3*s2*(-s2 + t1)*t2 + s*(t1*(-2*t1 + t2) + 2*s2*(t1 + t2))) + 
         s1*(6*Power(s2,2)*(s2 - t1)*Power(t2,2) + 
            2*s*s2*t2*(t1*(t1 + t2) - s2*(t1 + 4*t2)) + 
            Power(s,2)*(-(t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
               s2*(3*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)))))*
       dilog((-s2 + t1 - t2)/(s - s2 + t1 - t2)))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) + 
    (8*(2*Power(s1*(s2 - t1) - s2*t2,3) + 
         2*Power(s,4)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) + 
         Power(s,3)*(-2*Power(s1,3) - 2*Power(s2,3) - 
            4*s2*Power(t1 - t2,2) + Power(t1 - t2,2)*(t1 + t2) + 
            Power(s2,2)*(3*t1 + t2) + Power(s1,2)*(-4*s2 + t1 + 3*t2) - 
            4*s1*(Power(s2,2) + Power(t1 - t2,2) - s2*(t1 + t2))) - 
         2*s*(2*Power(s1,3)*Power(s2 - t1,2) + 
            Power(s2,2)*(2*s2 + t1 - 2*t2)*Power(t2,2) + 
            Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) - 4*s2*t1 + 2*Power(t1,2) - 6*s2*t2 - t1*t2) \
- s1*s2*t2*(4*Power(s2,2) + t1*(t1 + t2) - s2*(5*t1 + 6*t2))) + 
         Power(s,2)*(3*Power(s1,3)*(s2 - t1) + 
            Power(s1,2)*(6*Power(s2,2) - 12*s2*t1 + 6*Power(t1,2) - 
               9*s2*t2 - 2*t1*t2) - 
            s2*t2*(3*Power(s2,2) + Power(t1,2) + 2*s2*(t1 - 3*t2) - 
               4*t1*t2 + 3*Power(t2,2)) + 
            s1*(3*Power(s2,3) - 3*Power(s2,2)*(3*t1 + 4*t2) - 
               t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(9*Power(t1,2) - 6*t1*t2 + 9*Power(t2,2)))))*
       dilog(-(s/(-s1 - t1 + t2))))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (8*(2*Power(s1,3)*Power(s2 - t1,3) - 
         Power(s,3)*(2*Power(s2,3) - 3*Power(s2,2)*(t1 - t2) + 
            4*s2*Power(t1 - t2,2) - Power(t1 - t2,3)) + 
         2*Power(s1,2)*Power(s2 - t1,2)*
          (Power(s2,2) - 2*s2*t2 + t1*(-t1 + t2)) + 
         s2*(6*s2*Power(t1,2)*Power(t1 - t2,2) - 
            Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*t1*(-6*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
            2*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
         s1*(s2 - t1)*(2*Power(s2,3)*(t1 - 2*t2) + 
            Power(t1,2)*Power(t1 - t2,2) + 2*s2*t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-3*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2))) + 
         s*(-6*s2*Power(t1,2)*Power(t1 - t2,2) + 
            Power(t1,2)*Power(t1 - t2,3) + 2*Power(s2,4)*t2 - 
            4*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            3*Power(s2,2)*t1*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) - 
            2*Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) + 2*t1*(t1 - t2) - s2*(4*t1 + t2)) - 
            2*s1*(Power(s2,4) - Power(t1,2)*Power(t1 - t2,2) + 
               Power(s2,2)*t2*(3*t1 + t2) - 2*Power(s2,3)*(t1 + 2*t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 - Power(t2,2)))) + 
         Power(s,2)*(s2*(2*Power(s2,3) + 6*s2*Power(t1 - t2,2) - 
               Power(t1 - t2,2)*(4*t1 - t2) + Power(s2,2)*(-4*t1 + t2)) + 
            s1*(3*Power(s2,3) - 3*t1*Power(t1 - t2,2) - 
               Power(s2,2)*(9*t1 + 2*t2) + 
               s2*(9*Power(t1,2) - 10*t1*t2 + Power(t2,2)))))*
       dilog((-s1 - t1 + t2)/(-s1 - s2)))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) + 
    (16*(Power(s,3)*(2*Power(s1,2) + s1*(t1 - t2) + 3*Power(t1 - t2,2)) - 
         2*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
            Power(t2,2) + s2*(-2*t1 + t2)) + 
         s1*(-(Power(s2,3)*(t1 - 6*t2)) + 
            s2*t1*(Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)) - 
            Power(t1 - t2,2)*(Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*(-3*Power(s2,3) + Power(t1,3) - 5*t1*Power(t2,2) + 
            4*Power(t2,3) + Power(s2,2)*(7*t1 + 2*t2) + 
            s2*(-5*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) - 
         s2*(Power(t1 - t2,2)*(2*Power(t1,2) + t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,2) - t1*t2 + 3*Power(t2,2)) + 
            s2*(-4*Power(t1,3) + 4*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
               2*Power(t2,3))) + 
         Power(s,2)*(-2*Power(s1,3) + Power(s1,2)*(-3*s2 + t1 + 2*t2) + 
            s1*(5*s2*t1 - 7*Power(t1,2) + 6*s2*t2 + 11*t1*t2 - 
               4*Power(t2,2)) + 
            (t1 - t2)*(2*t1*(t1 - t2) + s2*(-4*t1 + 7*t2))) + 
         s*(2*Power(s1,3)*(s2 - t1) + 
            Power(t1 - t2,2)*(Power(t1,2) + Power(t2,2)) + 
            Power(s2,2)*(3*Power(t1,2) - 6*t1*t2 + 7*Power(t2,2)) + 
            s2*(-4*Power(t1,3) + 8*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            2*Power(s1,2)*(2*Power(s2,2) + 3*Power(t1,2) - 3*t1*t2 + 
               2*Power(t2,2) - s2*(5*t1 + t2)) - 
            s1*(3*Power(t1,3) - 3*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
               5*Power(t2,3) + Power(s2,2)*(5*t1 + 11*t2) + 
               s2*(-8*Power(t1,2) + 2*Power(t2,2)))))*
       dilog(((s - s1 - s2)*(-s1 - t1 + t2))/
         ((s1 + s2)*(-s + s1 + t1 - t2))))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,2)) - 
    (8*(-(Power(s,3)*(2*Power(s1,3) + 3*Power(s1,2)*(t1 - t2) + 
              4*s1*Power(t1 - t2,2) + Power(t1 - t2,3))) - 
         s2*(2*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
          Power(t2,3) + 2*Power(s1,4)*
          (Power(s2,2) + Power(t1,2) - t1*t2 + Power(t2,2) + 
            s2*(-2*t1 + t2)) + 
         Power(s1,3)*(2*Power(s2,3) - 4*Power(s2,2)*(t1 + t2) + 
            t2*(-4*Power(t1,2) + 9*t1*t2 - 6*Power(t2,2)) + 
            s2*(2*Power(t1,2) + 8*t1*t2 - 5*Power(t2,2))) + 
         s1*Power(t2,2)*(6*Power(s2,3) + (t1 - 2*t2)*Power(t1 - t2,2) + 
            Power(s2,2)*(-8*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
         Power(s1,2)*t2*(-6*Power(s2,3) + 10*Power(s2,2)*t1 + 
            6*Power(t1 - t2,2)*t2 + 
            s2*(-4*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) + 
         Power(s,2)*(2*Power(s1,4) + Power(s1,3)*(3*s2 + t1 - 4*t2) - 
            3*s2*Power(t1 - t2,2)*t2 + 
            s1*(t1 - t2)*(Power(t1,2) + s2*(t1 - 9*t2) - 5*t1*t2 + 
               4*Power(t2,2)) + 
            Power(s1,2)*(6*Power(t1 - t2,2) - s2*(2*t1 + 9*t2))) + 
         s*(-2*Power(s1,4)*(s2 - t1) - 
            (4*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
             (t1 - t2)*Power(t2,2) - 
            4*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               Power(t2,2) - s2*(2*t1 + t2)) + 
            2*s1*t2*(Power(s2,2)*(t1 - 6*t2) - 3*Power(t1 - t2,2)*t2 + 
               s2*(Power(t1,2) + t1*t2 - 2*Power(t2,2))) + 
            Power(s1,2)*(-2*s2*t1*(t1 + 3*t2) + 
               2*Power(s2,2)*(t1 + 6*t2) + 
               3*t2*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)))))*
       dilog(s/(s - s1 - t1 + t2)))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) + 
    (8*(2*Power(s1*(s2 - t1) - s2*t2,3) + 
         2*Power(s,4)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) + 
         Power(s,3)*(-2*Power(s1,3) - 2*Power(s2,3) - 
            4*s2*Power(t1 - t2,2) + Power(t1 - t2,2)*(t1 + t2) + 
            Power(s2,2)*(3*t1 + t2) + Power(s1,2)*(-4*s2 + t1 + 3*t2) - 
            4*s1*(Power(s2,2) + Power(t1 - t2,2) - s2*(t1 + t2))) - 
         2*s*(2*Power(s1,3)*Power(s2 - t1,2) + 
            Power(s2,2)*(2*s2 + t1 - 2*t2)*Power(t2,2) + 
            Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) - 4*s2*t1 + 2*Power(t1,2) - 6*s2*t2 - t1*t2) \
- s1*s2*t2*(4*Power(s2,2) + t1*(t1 + t2) - s2*(5*t1 + 6*t2))) + 
         Power(s,2)*(3*Power(s1,3)*(s2 - t1) + 
            Power(s1,2)*(6*Power(s2,2) - 12*s2*t1 + 6*Power(t1,2) - 
               9*s2*t2 - 2*t1*t2) - 
            s2*t2*(3*Power(s2,2) + Power(t1,2) + 2*s2*(t1 - 3*t2) - 
               4*t1*t2 + 3*Power(t2,2)) + 
            s1*(3*Power(s2,3) - 3*Power(s2,2)*(3*t1 + 4*t2) - 
               t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(9*Power(t1,2) - 6*t1*t2 + 9*Power(t2,2)))))*
       dilog((-s1 - t1 + t2)/(s - s1 - t1 + t2)))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (8*(2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*Power(t1 - t2,2)*(2*s2 + t1 - t2)*t2 + 
         s*(3*Power(s2,2) + 2*s2*(t1 - t2) + Power(t1 - t2,2))*(t1 - t2)*
          Power(t2,2) + s2*(Power(s2,2) + Power(t1 - t2,2))*Power(t2,3) + 
         Power(s1,3)*(Power(s2,3) - Power(t1,3) + 4*Power(t1,2)*t2 - 
            3*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s2,2)*(-3*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) + 
         s1*(2*Power(s,2)*(t1 - t2)*
             (-2*Power(t1 - t2,2) + s2*(2*t1 + t2)) + 
            Power(t2,2)*(-Power(s2,3) - Power(s2,2)*(t1 - 4*t2) - 
               (t1 - 2*t2)*Power(t1 - t2,2) + 
               s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) - 
            2*s*t2*(Power(s2,2)*(t1 - 2*t2) + 2*Power(t1 - t2,3) + 
               s2*(-3*Power(t1,2) + t1*t2 + 2*Power(t2,2)))) + 
         Power(s1,2)*(s*(3*Power(t1 - t2,3) + Power(s2,2)*(3*t1 - t2) + 
               2*s2*(-3*Power(t1,2) + 5*t1*t2 + Power(t2,2))) - 
            t2*(Power(s2,3) - 2*Power(t1,3) - 4*Power(s2,2)*(t1 - 2*t2) + 
               6*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 4*Power(t2,3) + 
               s2*(5*Power(t1,2) - 14*t1*t2 + 5*Power(t2,2)))))*
       dilog((s2 - t1 + t2)/s))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) + 
    (16*(2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*Power(t1 - t2,2)*(2*s2 + t1 - t2)*t2 + 
         s*(3*Power(s2,2) + 2*s2*(t1 - t2) + Power(t1 - t2,2))*(t1 - t2)*
          Power(t2,2) + s2*(Power(s2,2) + Power(t1 - t2,2))*Power(t2,3) + 
         Power(s1,3)*(Power(s2,3) - Power(t1,3) + 4*Power(t1,2)*t2 - 
            3*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s2,2)*(-3*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) + 
         s1*(2*Power(s,2)*(t1 - t2)*
             (-2*Power(t1 - t2,2) + s2*(2*t1 + t2)) + 
            Power(t2,2)*(-Power(s2,3) - Power(s2,2)*(t1 - 4*t2) - 
               (t1 - 2*t2)*Power(t1 - t2,2) + 
               s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) - 
            2*s*t2*(Power(s2,2)*(t1 - 2*t2) + 2*Power(t1 - t2,3) + 
               s2*(-3*Power(t1,2) + t1*t2 + 2*Power(t2,2)))) + 
         Power(s1,2)*(s*(3*Power(t1 - t2,3) + Power(s2,2)*(3*t1 - t2) + 
               2*s2*(-3*Power(t1,2) + 5*t1*t2 + Power(t2,2))) - 
            t2*(Power(s2,3) - 2*Power(t1,3) - 4*Power(s2,2)*(t1 - 2*t2) + 
               6*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 4*Power(t2,3) + 
               s2*(5*Power(t1,2) - 14*t1*t2 + 5*Power(t2,2)))))*
       dilog((s2 - t1 + t2)/(s1 + s2)))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) - 
    (8*(2*Power(s1,3)*Power(s2 - t1,3) - 
         Power(s,3)*(2*Power(s2,3) - 3*Power(s2,2)*(t1 - t2) + 
            4*s2*Power(t1 - t2,2) - Power(t1 - t2,3)) + 
         2*Power(s1,2)*Power(s2 - t1,2)*
          (Power(s2,2) - 2*s2*t2 + t1*(-t1 + t2)) + 
         s2*(6*s2*Power(t1,2)*Power(t1 - t2,2) - 
            Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*t1*(-6*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
            2*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
         s1*(s2 - t1)*(2*Power(s2,3)*(t1 - 2*t2) + 
            Power(t1,2)*Power(t1 - t2,2) + 2*s2*t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-3*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2))) + 
         s*(-6*s2*Power(t1,2)*Power(t1 - t2,2) + 
            Power(t1,2)*Power(t1 - t2,3) + 2*Power(s2,4)*t2 - 
            4*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            3*Power(s2,2)*t1*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) - 
            2*Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) + 2*t1*(t1 - t2) - s2*(4*t1 + t2)) - 
            2*s1*(Power(s2,4) - Power(t1,2)*Power(t1 - t2,2) + 
               Power(s2,2)*t2*(3*t1 + t2) - 2*Power(s2,3)*(t1 + 2*t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 - Power(t2,2)))) + 
         Power(s,2)*(s2*(2*Power(s2,3) + 6*s2*Power(t1 - t2,2) - 
               Power(t1 - t2,2)*(4*t1 - t2) + Power(s2,2)*(-4*t1 + t2)) + 
            s1*(3*Power(s2,3) - 3*t1*Power(t1 - t2,2) - 
               Power(s2,2)*(9*t1 + 2*t2) + 
               s2*(9*Power(t1,2) - 10*t1*t2 + Power(t2,2)))))*
       dilog((s2 - t1 + t2)/(-s1 - t1 + t2)))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) - 
    (16*(2*Power(s1*(s2 - t1) - s2*t2,3) + 
         2*Power(s,4)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) + 
         Power(s,3)*(-2*Power(s1,3) - 2*Power(s2,3) - 
            4*s2*Power(t1 - t2,2) + Power(t1 - t2,2)*(t1 + t2) + 
            Power(s2,2)*(3*t1 + t2) + Power(s1,2)*(-4*s2 + t1 + 3*t2) - 
            4*s1*(Power(s2,2) + Power(t1 - t2,2) - s2*(t1 + t2))) - 
         2*s*(2*Power(s1,3)*Power(s2 - t1,2) + 
            Power(s2,2)*(2*s2 + t1 - 2*t2)*Power(t2,2) + 
            Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) - 4*s2*t1 + 2*Power(t1,2) - 6*s2*t2 - t1*t2) \
- s1*s2*t2*(4*Power(s2,2) + t1*(t1 + t2) - s2*(5*t1 + 6*t2))) + 
         Power(s,2)*(3*Power(s1,3)*(s2 - t1) + 
            Power(s1,2)*(6*Power(s2,2) - 12*s2*t1 + 6*Power(t1,2) - 
               9*s2*t2 - 2*t1*t2) - 
            s2*t2*(3*Power(s2,2) + Power(t1,2) + 2*s2*(t1 - 3*t2) - 
               4*t1*t2 + 3*Power(t2,2)) + 
            s1*(3*Power(s2,3) - 3*Power(s2,2)*(3*t1 + 4*t2) - 
               t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(9*Power(t1,2) - 6*t1*t2 + 9*Power(t2,2)))))*
       dilog(-(s/(-s + s2 - t1 + t2))))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) + 
    (16*(-4*(s1 + s2)*Power(s1*(s2 - t1) - s2*t2,2) + 
         2*Power(s,3)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) - 
         Power(s,2)*(s1 + s2)*
          (2*Power(s1,2) + 2*Power(s2,2) + s1*(2*s2 - t1 - 3*t2) + 
            4*Power(t1 - t2,2) - s2*(3*t1 + t2)) + 
         s*(s1 + s2)*(3*Power(s1,2)*(s2 - t1) + 
            s2*t2*(-3*s2 - 2*t1 + 6*t2) + 
            s1*(3*Power(s2,2) + 2*t1*(3*t1 - t2) - 9*s2*(t1 + t2))))*
       dilog((s*(s - s1 - s2))/((-s + s1 + t1 - t2)*(-s + s2 - t1 + t2))))/
     (Power(s,2)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) + 
    (16*(-(Power(s1,3)*(3*Power(s2,2) + 3*Power(t1,2) - t1*t2 + 
              2*Power(t2,2) + s2*(-6*t1 + t2))) + 
         Power(s,3)*(2*Power(s2,2) + 3*Power(t1 - t2,2) + 
            s2*(-t1 + t2)) + Power(s,2)*
          (-2*Power(s2,3) + 2*Power(t1 - t2,2)*t2 + 
            Power(s2,2)*(2*t1 + t2) + 
            s2*(-4*Power(t1,2) + 11*t1*t2 - 7*Power(t2,2)) + 
            s1*(-3*Power(s2,2) + 6*s2*t1 - 7*Power(t1,2) + 5*s2*t2 + 
               11*t1*t2 - 4*Power(t2,2))) + 
         Power(s1,2)*(-2*Power(s2,3) - 2*Power(t1,3) + 
            2*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 4*Power(t2,3) + 
            Power(s2,2)*(2*t1 + 7*t2) + 
            s2*(2*Power(t1,2) - 9*t1*t2 + Power(t2,2))) - 
         s1*(2*Power(s2,3)*(t1 - 2*t2) - 
            s2*t2*(-3*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
            Power(t1 - t2,2)*(Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(-3*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) - 
         s2*(2*Power(s2,2)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(t1 - t2,2)*(2*Power(t1,2) + t1*t2 + Power(t2,2)) - 
            s2*(4*Power(t1,3) - 5*Power(t1,2)*t2 + Power(t2,3))) + 
         s*(-2*Power(s2,3)*t2 + 
            Power(t1 - t2,2)*(Power(t1,2) + Power(t2,2)) + 
            Power(s1,2)*(4*Power(s2,2) - 11*s2*t1 + 7*Power(t1,2) - 
               5*s2*t2 - 6*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*(4*Power(t1,2) - 6*t1*t2 + 6*Power(t2,2)) + 
            s2*(-5*Power(t1,3) + 5*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            2*s1*(Power(s2,3) + Power(t1,3) - 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - 2*Power(t2,3) - 
               Power(s2,2)*(t1 + 5*t2) - s2*(Power(t1,2) - 4*Power(t2,2)))\
))*dilog(((s - s1 - s2)*(-s2 + t1 - t2))/((s1 + s2)*(-s + s2 - t1 + t2))))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,2)) + 
    (16*(2*Power(s1,3)*Power(s2 - t1,3) - 
         Power(s,3)*(2*Power(s2,3) - 3*Power(s2,2)*(t1 - t2) + 
            4*s2*Power(t1 - t2,2) - Power(t1 - t2,3)) + 
         2*Power(s1,2)*Power(s2 - t1,2)*
          (Power(s2,2) - 2*s2*t2 + t1*(-t1 + t2)) + 
         s2*(6*s2*Power(t1,2)*Power(t1 - t2,2) - 
            Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*t1*(-6*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
            2*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
         s1*(s2 - t1)*(2*Power(s2,3)*(t1 - 2*t2) + 
            Power(t1,2)*Power(t1 - t2,2) + 2*s2*t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-3*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2))) + 
         s*(-6*s2*Power(t1,2)*Power(t1 - t2,2) + 
            Power(t1,2)*Power(t1 - t2,3) + 2*Power(s2,4)*t2 - 
            4*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            3*Power(s2,2)*t1*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) - 
            2*Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) + 2*t1*(t1 - t2) - s2*(4*t1 + t2)) - 
            2*s1*(Power(s2,4) - Power(t1,2)*Power(t1 - t2,2) + 
               Power(s2,2)*t2*(3*t1 + t2) - 2*Power(s2,3)*(t1 + 2*t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 - Power(t2,2)))) + 
         Power(s,2)*(s2*(2*Power(s2,3) + 6*s2*Power(t1 - t2,2) - 
               Power(t1 - t2,2)*(4*t1 - t2) + Power(s2,2)*(-4*t1 + t2)) + 
            s1*(3*Power(s2,3) - 3*t1*Power(t1 - t2,2) - 
               Power(s2,2)*(9*t1 + 2*t2) + 
               s2*(9*Power(t1,2) - 10*t1*t2 + Power(t2,2)))))*
       dilog((s2 - t1 + t2)/(-s + s2 - t1 + t2)))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) - 
    (16*(Power(s1,4)*Power(t1,2)*(-s2 + t1) - 
         Power(s,5)*(t1*(t1 - t2) + s1*(t1 + t2)) + 
         Power(s1,3)*(Power(t1,3)*(t1 - 4*t2) + 8*s2*Power(t1,2)*t2 + 
            Power(s2,3)*(t1 + t2) - 2*Power(s2,2)*t1*(t1 + 2*t2)) + 
         s2*t1*Power(t2,2)*(Power(s2,3) + Power(t1,2)*(2*t1 - 3*t2) - 
            Power(s2,2)*t2 + s2*t1*(-2*t1 + 3*t2)) + 
         s1*t2*(-(Power(s2,4)*t1) + Power(t1,3)*(t1 - 2*t2)*t2 + 
            Power(s2,3)*t2*(4*t1 + t2) + 
            Power(s2,2)*t1*(3*Power(t1,2) - 5*t1*t2 - 3*Power(t2,2)) + 
            s2*Power(t1,2)*(-2*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*(Power(t1,3)*t2*(-2*t1 + 3*t2) + 
            Power(s2,3)*(Power(t1,2) - 4*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*t1*(-2*Power(t1,2) + 7*t1*t2 + 7*Power(t2,2)) + 
            s2*(Power(t1,4) - 8*Power(t1,2)*Power(t2,2))) + 
         Power(s,4)*(Power(s1,2)*(-s2 + 3*t1 + 2*t2) + 
            s1*(3*Power(t1,2) + 4*s2*t2 - 8*t1*t2) + 
            t1*(s2*(t1 - 2*t2) + 2*t2*(-t1 + t2))) + 
         Power(s,3)*(Power(s1,3)*(2*s2 - 3*t1 - t2) + 
            t1*(-Power(t1,3) - Power(s2,2)*(t1 - 4*t2) + 
               Power(t1,2)*t2 + s2*(t1 - 3*t2)*t2 - t1*Power(t2,2) + 
               Power(t2,3)) - 
            s1*(Power(t1,3) + s2*t1*(t1 - 14*t2) - 
               2*Power(s2,2)*(t1 - 3*t2) - Power(t1,2)*t2 + 
               5*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s1,2)*(3*Power(s2,2) + s2*(t1 - 6*t2) + 
               t1*(-3*t1 + 13*t2))) + 
         Power(s,2)*(Power(s1,4)*(-s2 + t1) + 
            Power(s1,3)*(-4*Power(s2,2) - 2*s2*t1 + Power(t1,2) + 
               2*s2*t2 - 6*t1*t2) + 
            Power(s1,2)*(-3*Power(s2,3) - 3*Power(s2,2)*(t1 - 2*t2) - 
               s2*(2*Power(t1,2) + 17*t1*t2 + 2*Power(t2,2)) + 
               t1*(3*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2))) + 
            s1*(-2*Power(s2,3)*(t1 - 2*t2) - 16*Power(s2,2)*t1*t2 + 
               3*s2*t2*(2*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               t1*(3*Power(t1,3) - 6*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  4*Power(t2,3))) + 
            t1*(Power(s2,3)*(t1 - 4*t2) + 2*Power(t1,2)*t2*(-t1 + t2) + 
               Power(s2,2)*(-Power(t1,2) + 2*t1*t2 + 4*Power(t2,2)) + 
               s2*(Power(t1,3) - Power(t1,2)*t2 - 2*Power(t2,3)))) + 
         s*(Power(s1,4)*s2*(s2 + t1) + 
            Power(s1,3)*(2*Power(s2,3) + Power(s2,2)*(t1 - 2*t2) - 
               3*Power(t1,2)*(t1 + t2) + s2*t1*(3*t1 + 5*t2)) + 
            t1*t2*(Power(s2,4) - 4*Power(s2,3)*t2 + 
               Power(t1,2)*t2*(-t1 + t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(-Power(t1,2) + 2*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,4) - 3*Power(t1,3)*(t1 - 3*t2) - 
               2*Power(s2,3)*t2 - 3*s2*t1*t2*(5*t1 + 3*t2) + 
               Power(s2,2)*(3*Power(t1,2) + 12*t1*t2 + 4*Power(t2,2))) + 
            s1*(-2*Power(s2,3)*t1*(t1 - 5*t2) + Power(s2,4)*(t1 - t2) + 
               Power(t1,2)*t2*(4*Power(t1,2) - 5*t1*t2 - 3*Power(t2,2)) + 
               3*Power(s2,2)*(Power(t1,3) - 3*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) - Power(t2,3)) + 
               s2*t1*(-2*Power(t1,3) + Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 7*Power(t2,3)))))*dilog(1 - t2/s1))/
     (s1*s2*(-s + s2 - t1)*t1*(s1 + t1 - t2)*t2*Power(s - s1 + t2,2)) - 
    (16*(-(Power(s1,5)*Power(s2 - t1,2)) - 
         Power(s1,4)*s2*(s2 - t1)*(2*s2 - 3*t1 - t2) - 
         Power(s2,2)*t1*t2*(Power(s2,3) - t1*Power(t2,2) + 
            Power(s2,2)*(-2*t1 + t2) + 
            s2*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
         Power(s1,3)*(-Power(s2,4) + 4*Power(s2,3)*(t1 + t2) + 
            s2*t1*(-3*Power(t1,2) + 4*t1*t2 - 4*Power(t2,2)) + 
            Power(s2,2)*(-Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
            Power(t1,2)*(Power(t1,2) - t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*(-Power(s2,5) + 2*Power(s2,4)*t1 + 
            Power(s2,3)*(3*Power(t1,2) - 6*t1*t2 - 4*Power(t2,2)) + 
            Power(t1,2)*t2*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
            s2*t1*(2*Power(t1,3) - 5*Power(t1,2)*t2 + 
               2*t1*Power(t2,2) + Power(t2,3)) - 
            Power(s2,2)*(6*Power(t1,3) - 11*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) + Power(t2,3))) + 
         s1*s2*(Power(s2,4)*(-t1 + t2) + 
            Power(s2,3)*(3*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
            t1*Power(t2,2)*(2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            s2*t1*(Power(t1,3) - Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(s2,2)*(-3*Power(t1,3) + 6*Power(t1,2)*t2 - 
               3*t1*Power(t2,2) + 2*Power(t2,3))) + 
         Power(s,4)*(t1*(t1 - t2)*(s2 - t1 + t2) + 
            s1*(t1*(-t1 + t2) + s2*(t1 + t2))) + 
         Power(s,3)*(Power(s1,2)*
             (Power(s2,2) + Power(-2*t1 + t2,2) - 2*s2*(2*t1 + t2)) - 
            s1*(-3*Power(t1,3) - 2*s2*t1*t2 + 6*Power(t1,2)*t2 - 
               4*t1*Power(t2,2) + Power(t2,3) + 
               2*Power(s2,2)*(t1 + 2*t2)) + 
            t1*(4*s2*Power(t1 - t2,2) - Power(t1 - t2,3) + 
               Power(s2,2)*(-3*t1 + 4*t2))) + 
         Power(s,2)*(Power(s1,3)*
             (-2*Power(s2,2) - 6*Power(t1,2) + 5*t1*t2 - Power(t2,2) + 
               s2*(7*t1 + t2)) + 
            s1*(3*Power(t1,4) - 7*Power(t1,3)*t2 + 
               5*Power(t1,2)*Power(t2,2) - Power(t2,4) + 
               2*Power(s2,3)*(t1 + 2*t2) + 
               s2*t1*(-11*Power(t1,2) + 22*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,2)*(6*Power(t1,2) - 8*t1*t2 - 4*Power(t2,2))) + 
            Power(s1,2)*(-Power(s2,3) - 3*Power(t1,3) + 
               6*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 2*Power(t2,3) + 
               Power(s2,2)*(9*t1 + 10*t2) + 
               s2*(-6*Power(t1,2) + t1*t2 + Power(t2,2))) + 
            t1*(3*Power(s2,3)*(t1 - 2*t2) + t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(-5*Power(t1,2) + 12*t1*t2 - 6*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 6*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  3*Power(t2,3)))) + 
         s*(2*Power(s1,4)*(Power(s2,2) - 3*s2*t1 + t1*(2*t1 - t2)) + 
            Power(s1,3)*(2*Power(s2,3) - Power(s2,2)*(12*t1 + 7*t2) + 
               s2*(8*Power(t1,2) - t1*t2 - Power(t2,2)) + 
               t1*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            s2*t1*(-(Power(s2,3)*(t1 - 4*t2)) + 
               2*t1*(t1 - t2)*Power(t2,2) + 
               2*Power(s2,2)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
               s2*(Power(t1,3) - 4*Power(t1,2)*t2 + 5*t1*Power(t2,2) - 
                  3*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4) - 7*Power(s2,3)*(t1 + t2) - 
               2*Power(s2,2)*(Power(t1,2) - 5*t1*t2 - 2*Power(t2,2)) + 
               2*s2*t1*(5*Power(t1,2) - 9*t1*t2 + 6*Power(t2,2)) + 
               t1*(-3*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(-2*Power(s2,4)*t2 + 
               Power(s2,3)*(-7*Power(t1,2) + 10*t1*t2 + 3*Power(t2,2)) + 
               t1*t2*(-2*Power(t1,3) + Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*(11*Power(t1,3) - 20*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - Power(t2,3)) + 
               s2*(-4*Power(t1,4) + 11*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + Power(t2,4)\
))))*dilog(1 - t2/t1))/
     (s1*Power(s2,2)*t1*(s - s2 + t1)*(-s + s1 - t2)*(s1 + t1 - t2)*t2) + 
    (16*(2*Power(s,6)*t1*t2*(-t1 + t2) + 
         Power(s1,6)*s2*(s2 - t1)*(t1 + t2) + 
         Power(s2,3)*t1*Power(t2,3)*
          (2*Power(s2,2) + t2*(-t1 + 2*t2) - s2*(t1 + 2*t2)) + 
         Power(s1,5)*(Power(s2,3)*(t1 + 2*t2) + s2*t1*t2*(t1 + 3*t2) + 
            Power(t1,2)*t2*(2*t1 + 3*t2) - 
            Power(s2,2)*(Power(t1,2) + 4*t1*t2 + 7*Power(t2,2))) - 
         Power(s1,4)*(Power(s2,4)*(t1 + t2) + 
            Power(t1,2)*Power(t2,2)*(5*t1 + 8*t2) + 
            Power(s2,2)*t2*(-8*Power(t1,2) + 2*t1*t2 - 17*Power(t2,2)) + 
            s2*t1*t2*(-2*Power(t1,2) - 9*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*(-Power(t1,2) + 8*t1*t2 + 11*Power(t2,2))) + 
         Power(s1,3)*(-(Power(s2,5)*(t1 + 2*t2)) + 
            Power(t1,2)*Power(t2,3)*(4*t1 + 7*t2) + 
            Power(s2,4)*(Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
            3*Power(s2,3)*t2*(Power(t1,2) + 7*Power(t2,2)) + 
            s2*t1*Power(t2,2)*
             (-4*Power(t1,2) - 20*t1*t2 + 9*Power(t2,2)) + 
            Power(s2,2)*t2*(3*Power(t1,3) - 10*Power(t1,2)*t2 + 
               17*t1*Power(t2,2) - 19*Power(t2,3))) + 
         s1*s2*Power(t2,2)*(2*Power(s2,5) + Power(s2,4)*(-5*t1 + t2) + 
            2*Power(s2,3)*t1*(t1 + t2) + t1*Power(t2,3)*(-3*t1 + 2*t2) + 
            s2*t2*(Power(t1,3) + 5*t1*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s2,2)*(Power(t1,3) + 6*Power(t1,2)*t2 - 
               14*t1*Power(t2,2) + 5*Power(t2,3))) + 
         Power(s1,2)*t2*(-2*Power(s2,6) + Power(s2,5)*(2*t1 + t2) - 
            Power(t1,2)*Power(t2,3)*(t1 + 2*t2) + 
            Power(s2,4)*t1*(-2*t1 + 3*t2) + 
            s2*t1*Power(t2,2)*
             (2*Power(t1,2) + 14*t1*t2 - 7*Power(t2,2)) + 
            Power(s2,3)*(Power(t1,3) - 6*Power(t1,2)*t2 + 
               18*t1*Power(t2,2) - 17*Power(t2,3)) + 
            Power(s2,2)*t2*(-3*Power(t1,3) + 4*Power(t1,2)*t2 - 
               17*t1*Power(t2,2) + 10*Power(t2,3))) + 
         Power(s,5)*(t1*t2*(4*s2*t1 - 2*Power(t1,2) - 6*s2*t2 - 
               3*t1*t2 + 5*Power(t2,2)) + 
            s1*(s2*t1*(t1 + t2) - 
               2*t2*(-5*Power(t1,2) + 3*t1*t2 + Power(t2,2)))) + 
         Power(s,4)*(Power(s1,2)*
             (Power(s2,2)*t1 - 20*Power(t1,2)*t2 + 4*t1*Power(t2,2) + 
               6*Power(t2,3) - 
               s2*(5*Power(t1,2) + 5*t1*t2 + 3*Power(t2,2))) + 
            t1*t2*(Power(s2,2)*(-3*t1 + 7*t2) + 
               s2*(2*Power(t1,2) + 5*t1*t2 - 14*Power(t2,2)) + 
               t2*(-5*Power(t1,2) + t1*t2 + 4*Power(t2,2))) + 
            s1*(Power(s2,2)*(-Power(t1,2) - 7*t1*t2 + Power(t2,2)) + 
               s2*t2*(-15*Power(t1,2) + 14*t1*t2 + 8*Power(t2,2)) + 
               t2*(10*Power(t1,3) + 15*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) - 5*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,3)*
             (-(Power(s2,2)*(4*t1 + t2)) + 
               2*t2*(10*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) + 
               s2*(10*Power(t1,2) + 10*t1*t2 + 9*Power(t2,2))) + 
            t1*t2*(Power(s2,3)*(t1 - 4*t2) + 
               s2*t2*(4*Power(t1,2) - 3*t1*t2 - 10*Power(t2,2)) + 
               Power(t2,2)*(-4*Power(t1,2) + 3*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,2) + 4*t1*t2 + 10*Power(t2,2))) \
- s1*(Power(s2,3)*(Power(t1,2) - 12*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-Power(t1,2) + 13*t1*t2 + 11*Power(t2,2)) + 
               s2*t2*(8*Power(t1,3) + 24*Power(t1,2)*t2 - 
                  20*t1*Power(t2,2) - 19*Power(t2,3)) + 
               Power(t2,2)*(-20*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*(-t1 + t2) + 
               s2*t2*(20*Power(t1,2) - 9*t1*t2 - 29*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) + 25*t1*t2 + 7*Power(t2,2)) + 
               t2*(-20*Power(t1,3) - 30*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 10*Power(t2,3)))) - 
         s*(s2*t1*Power(t2,2)*
             (-3*Power(s2,3)*(t1 - 2*t2) + t1*Power(t2,3) + 
               Power(s2,2)*(Power(t1,2) + 4*t1*t2 - 5*Power(t2,2)) + 
               s2*t2*(Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
            Power(s1,5)*(-2*t1*t2*(t1 + t2) + 
               Power(s2,2)*(4*t1 + 3*t2) - 
               s2*(5*Power(t1,2) + 5*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,4)*(3*Power(s2,3)*(t1 + t2) + 
               s2*Power(t2,2)*(5*t1 + 13*t2) + 
               t1*t2*(10*Power(t1,2) + 15*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(4*Power(t1,2) + 19*t1*t2 + 23*Power(t2,2))) \
- s1*t2*(Power(s2,5)*(t1 - 6*t2) + 
               t1*Power(t2,3)*(2*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(s2,4)*(2*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*t2*
                (6*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 15*Power(t2,3)) - 
               2*Power(s2,3)*
                (Power(t1,3) - 7*Power(t1,2)*t2 + 9*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               s2*Power(t2,2)*
                (-4*Power(t1,3) - 9*Power(t1,2)*t2 + 7*t1*Power(t2,2) + 
                  3*Power(t2,3))) - 
            Power(s1,3)*(2*Power(s2,4)*(t1 + 2*t2) + 
               Power(s2,2)*t2*
                (-21*Power(t1,2) + 5*t1*t2 - 52*Power(t2,2)) + 
               t1*Power(t2,2)*
                (20*Power(t1,2) + 23*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,3)*(-3*Power(t1,2) + 28*t1*t2 + 
                  16*Power(t2,2)) + 
               4*s2*t2*(-2*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*(-(Power(s2,5)*(t1 + 4*t2)) + 
               t1*Power(t2,3)*
                (12*Power(t1,2) + 11*t1*t2 - 6*Power(t2,2)) + 
               Power(s2,4)*(2*Power(t1,2) - 12*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*t2*(5*Power(t1,2) + t1*t2 + 24*Power(t2,2)) + 
               Power(s2,2)*t2*
                (9*Power(t1,3) - 24*Power(t1,2)*t2 + 
                  40*t1*Power(t2,2) - 47*Power(t2,3)) + 
               s2*Power(t2,2)*
                (-12*Power(t1,3) - 37*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 13*Power(t2,3)))) + 
         Power(s,2)*(t1*t2*(Power(s2,4)*t2 + t1*Power(t2,3)*(-t1 + t2) + 
               s2*Power(t2,2)*
                (2*Power(t1,2) - 5*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*t2*(-3*Power(t1,2) + 10*t1*t2 + 3*Power(t2,2))\
) + Power(s1,4)*(3*Power(s2,2)*(2*t1 + t2) + 
               2*t2*(-5*Power(t1,2) - 3*t1*t2 + Power(t2,2)) - 
               s2*(10*Power(t1,2) + 10*t1*t2 + 9*Power(t2,2))) + 
            Power(s1,3)*(3*Power(s2,3)*t1 - 
               3*Power(s2,2)*
                (2*Power(t1,2) + 11*t1*t2 + 8*Power(t2,2)) + 
               s2*t2*(-10*Power(t1,2) + 3*t1*t2 + 34*Power(t2,2)) + 
               t2*(20*Power(t1,3) + 30*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  5*Power(t2,3))) + 
            s1*(Power(s2,3)*t2*(Power(t1,2) + 5*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(Power(t1,2) - 7*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*t2*
                (9*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 24*Power(t2,3)) + 
               Power(t2,3)*(12*Power(t1,3) + Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) - Power(t2,3)) + 
               2*s2*Power(t2,2)*
                (-6*Power(t1,3) - 7*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  7*Power(t2,3))) - 
            Power(s1,2)*(Power(s2,4)*(t1 + 3*t2) + 
               Power(s2,3)*(-3*Power(t1,2) + 32*t1*t2 + Power(t2,2)) - 
               Power(s2,2)*t2*
                (15*Power(t1,2) + 3*t1*t2 + 46*Power(t2,2)) + 
               Power(t2,2)*(30*Power(t1,3) + 21*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*t2*(-12*Power(t1,3) - 42*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + 39*Power(t2,3)))))*
       dilog(1 - (-s + s2 - t1)/(-s1 - t1 + t2)))/
     (s1*Power(s2,2)*t1*(s - s2 + t1)*(-s + s1 + s2 - t2)*Power(t2,2)*
       Power(s - s1 + t2,2)) - 
    (16*(-(Power(s1,6)*t1*(2*Power(s2,2) - 3*s2*t1 + Power(t1,2))) + 
         Power(s,5)*(Power(s1,2) + Power(t1,2))*(t1 - t2)*t2 + 
         Power(s2,2)*Power(t1,2)*Power(t2,2)*
          (Power(s2,3) - 3*s2*Power(t1,2) - Power(s2,2)*t2 + 
            Power(t1,2)*(2*t1 + t2)) + 
         Power(s1,5)*(Power(s2,4) - 3*Power(t1,4) + 
            2*s2*Power(t1,2)*(t1 - t2) - Power(s2,3)*(7*t1 + 4*t2) + 
            Power(s2,2)*t1*(7*t1 + 5*t2)) + 
         s1*s2*Power(t1,2)*t2*
          (-Power(s2,4) + 6*Power(s2,3)*t2 + 
            Power(t1,3)*(-2*t1 + 3*t2) + 
            2*Power(s2,2)*(Power(t1,2) - 3*t1*t2 - Power(t2,2)) + 
            s2*t1*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,4)*(-(Power(s2,4)*(t1 + 6*t2)) + 
            Power(s2,2)*t1*(12*Power(t1,2) - 9*t1*t2 - 8*Power(t2,2)) + 
            Power(t1,3)*(-2*Power(t1,2) + 4*t1*t2 + Power(t2,2)) + 
            s2*Power(t1,2)*(-4*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
            Power(s2,3)*(-5*Power(t1,2) + 9*t1*t2 + 6*Power(t2,2))) + 
         Power(s1,2)*s2*(-(Power(t1,5)*t2) + Power(s2,4)*Power(t2,2) + 
            Power(t1,3)*Power(t2,3) + 
            Power(s2,2)*Power(t1,2)*
             (2*Power(t1,2) + 11*t1*t2 + 8*Power(t2,2)) - 
            s2*Power(t1,3)*(Power(t1,2) + 3*t1*t2 + 10*Power(t2,2)) - 
            Power(s2,3)*(Power(t1,3) + 6*Power(t1,2)*t2 + Power(t2,3))) - 
         Power(s1,3)*(Power(s2,5)*t2 + Power(t1,4)*t2*(-2*t1 + t2) - 
            Power(s2,4)*(Power(t1,2) + 6*Power(t2,2)) + 
            s2*Power(t1,2)*(7*Power(t1,3) - 4*Power(t1,2)*t2 + 
               5*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(8*Power(t1,3) + 5*t1*Power(t2,2) + 
               2*Power(t2,3)) - 
            Power(s2,2)*t1*(14*Power(t1,3) - 4*Power(t1,2)*t2 + 
               6*t1*Power(t2,2) + 3*Power(t2,3))) - 
         Power(s,4)*(Power(s1,2) + Power(t1,2))*
          (Power(t2,2)*(-t1 + t2) - 
            s2*(Power(t1,2) - 5*t1*t2 + 5*Power(t2,2)) + 
            s1*((4*t1 - 3*t2)*t2 + s2*(t1 + 2*t2))) + 
         Power(s,3)*(-(Power(s1,4)*
               (Power(s2,2) + Power(t1,2) - 6*t1*t2 + 2*Power(t2,2) - 
                 4*s2*(t1 + t2))) + 
            Power(s1,3)*(-3*Power(t1,3) + 2*Power(t2,3) + 
               Power(s2,2)*(3*t1 + 7*t2) + 
               s2*(4*Power(t1,2) + 12*t1*t2 - 15*Power(t2,2))) + 
            s1*Power(t1,2)*(3*s2*(4*t1 - 5*t2)*t2 + 
               Power(s2,2)*(3*t1 + 7*t2) + 
               t2*(-2*Power(t1,2) - t1*t2 + 2*Power(t2,2))) + 
            Power(t1,2)*(Power(t1,2)*t2*(-t1 + t2) + 
               Power(s2,2)*(-2*Power(t1,2) + 9*t1*t2 - 
                  10*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 2*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,2)*
                (-3*Power(t1,2) + 9*t1*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(-2*Power(t1,2) + 7*t1*t2 - 2*Power(t2,2)) + 
               s2*(14*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 4*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,5)*
             (Power(s2,2) + t1*(2*t1 - 3*t2) - 2*s2*(2*t1 + t2)) + 
            Power(s1,4)*(2*Power(s2,3) + Power(t1,2)*(5*t1 + 3*t2) - 
               Power(s2,2)*(13*t1 + 14*t2) + 
               s2*(2*Power(t1,2) - 7*t1*t2 + 10*Power(t2,2))) + 
            Power(t1,2)*(Power(t1,2)*Power(t2,2)*(-t1 + t2) + 
               s2*Power(t1,2)*(Power(t1,2) + 3*t1*t2 - 5*Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 7*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*(-2*Power(t1,3) + 4*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 6*Power(t2,3))) + 
            Power(s1,2)*(Power(t1,3)*
                (-2*Power(t1,2) + 5*t1*t2 + Power(t2,2)) + 
               2*s2*Power(t1,2)*
                (9*Power(t1,2) + t1*t2 + 6*Power(t2,2)) + 
               Power(s2,3)*(3*Power(t1,2) - 7*t1*t2 + 10*Power(t2,2)) - 
               3*Power(s2,2)*
                (9*Power(t1,3) + 6*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  2*Power(t2,3))) + 
            Power(s1,3)*(-3*Power(s2,3)*(t1 + 3*t2) - 
               3*Power(s2,2)*
                (3*Power(t1,2) + 4*t1*t2 - 9*Power(t2,2)) + 
               s2*(2*Power(t1,3) - Power(t1,2)*t2 - 5*t1*Power(t2,2) - 
                  6*Power(t2,3)) + 
               t1*(Power(t1,3) - 5*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  3*Power(t2,3))) - 
            s1*Power(t1,2)*(3*Power(s2,3)*(t1 + 3*t2) + 
               Power(s2,2)*(-2*Power(t1,2) + 12*t1*t2 - 
                  27*Power(t2,2)) + 
               t1*t2*(-2*Power(t1,2) + 3*t1*t2 - 2*Power(t2,2)) + 
               s2*(Power(t1,3) - 15*Power(t1,2)*t2 + 4*t1*Power(t2,2) + 
                  6*Power(t2,3)))) + 
         s*(Power(s1,6)*(s2 - t1)*t1 - 
            Power(s1,5)*(2*Power(s2,3) + s2*t1*(10*t1 - t2) - 
               6*Power(s2,2)*(2*t1 + t2) + Power(t1,2)*(t1 + 3*t2)) + 
            s2*Power(t1,2)*t2*
             (Power(s2,3)*(2*t1 - 5*t2) + 
               s2*Power(t1,2)*(-2*t1 + 7*t2) - 
               Power(s2,2)*(2*Power(t1,2) + t1*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(2*Power(t1,2) - t1*t2 - 2*Power(t2,2))) + 
            Power(s1,4)*(-Power(s2,4) + 2*Power(s2,3)*(5*t1 + 8*t2) + 
               Power(s2,2)*(3*Power(t1,2) - 8*t1*t2 - 14*Power(t2,2)) + 
               Power(t1,2)*(4*Power(t1,2) + t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(-17*Power(t1,2) + 7*t1*t2 + 8*Power(t2,2))) - 
            Power(s1,2)*(s2*Power(t1,3)*
                (-6*Power(t1,2) + t1*t2 - 9*Power(t2,2)) + 
               Power(t1,3)*t2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
               Power(s2,4)*(Power(t1,2) - 2*t1*t2 + 5*Power(t2,2)) + 
               2*Power(s2,2)*Power(t1,2)*
                (9*Power(t1,2) + 10*t1*t2 + 9*Power(t2,2)) + 
               Power(s2,3)*(-14*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 4*Power(t2,3))) + 
            Power(s1,3)*(Power(s2,4)*(t1 + 5*t2) + 
               Power(s2,3)*(4*Power(t1,2) + 4*t1*t2 - 21*Power(t2,2)) + 
               Power(t1,2)*(4*Power(t1,3) - 7*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + Power(t2,3)) - 
               s2*t1*(15*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(7*Power(t1,3) + Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) + 6*Power(t2,3))) + 
            s1*Power(t1,2)*(Power(t1,3)*Power(t2,2) + 
               Power(s2,4)*(t1 + 5*t2) + 
               Power(s2,3)*(-2*Power(t1,2) + 4*t1*t2 - 21*Power(t2,2)) - 
               s2*t1*(Power(t1,3) - 3*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(2*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 6*Power(t2,3)))))*
       dilog(1 - t2/(-s1 - t1 + t2)))/
     (Power(s1,2)*s2*Power(t1,2)*(s1 + t1)*Power(s - s2 + t1,2)*
       (-s + s1 - t2)*t2) - (16*
       (Power(s,6)*t1*t2*(2*s2 - 3*t1 + 3*t2) + 
         s1*s2*(Power(s1,2) - Power(s2,2))*(s2 - t1)*t2*(s2 - t1 + t2)*
          (s1*(s2 - t1) - s2*t2) - 
         Power(s,5)*t1*(t2*(4*Power(s2,2) - 7*s2*t1 + 3*Power(t1,2) + 
               4*s2*t2 + 2*t1*t2 - 5*Power(t2,2)) + 
            s1*(-(s2*t1) + Power(t1,2) + 6*s2*t2 - 10*t1*t2 + 
               10*Power(t2,2))) + 
         Power(s,4)*(Power(s1,2)*t1*
             (Power(s2,2) - 4*s2*t1 + 3*Power(t1,2) + 8*s2*t2 - 
               13*t1*t2 + 13*Power(t2,2)) + 
            t1*t2*(2*Power(s2,3) - 5*Power(s2,2)*t1 - Power(t1,3) - 
               4*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 3*Power(t2,3) + 
               s2*(5*Power(t1,2) + 4*t1*t2 - 9*Power(t2,2))) + 
            s1*(Power(s2,2)*(-Power(t1,2) + 4*t1*t2 + Power(t2,2)) + 
               s2*(2*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + Power(t2,3)) - 
               t1*(Power(t1,3) - 9*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  15*Power(t2,3)))) - 
         Power(s,3)*(Power(s1,3)*
             (2*Power(s2,2)*(t1 + t2) + 
               s2*(-5*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2)) + 
               t1*(3*Power(t1,2) - 8*t1*t2 + 8*Power(t2,2))) + 
            s1*(2*Power(s2,4)*t2 + 
               s2*Power(t2,2)*
                (16*Power(t1,2) - 18*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 12*t1*t2 + 8*Power(t2,2)) - 
               t1*t2*(Power(t1,3) + 12*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) - 9*Power(t2,3)) - 
               Power(s2,2)*(Power(t1,3) - 13*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) - 7*Power(t2,3))) + 
            t1*t2*(-(Power(s2,3)*t1) + Power(t1,4) + 
               2*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) - 
               Power(t2,4) + 
               Power(s2,2)*(5*Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
               s2*(-5*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*(t1 - t2) + 
               Power(s2,2)*(-5*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               s2*(7*Power(t1,3) - 9*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 2*Power(t2,3)) - 
               t1*(3*Power(t1,3) - 11*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  16*Power(t2,3)))) - 
         s*(Power(s1,4)*(s2 - t1)*
             (Power(s2,2)*(t1 + 2*t2) - 
               2*s2*(Power(t1,2) - Power(t2,2)) + 
               t1*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,3)*t2*(Power(s2,4) + 
               Power(s2,2)*t1*(18*t1 - 13*t2) + 
               Power(s2,3)*(-10*t1 + t2) - 
               2*s2*t1*(5*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 6*Power(t2,2))) - 
            s2*t1*Power(t2,2)*
             (2*Power(t1,4) + Power(s2,3)*t2 - Power(t1,3)*t2 - 
               t1*Power(t2,3) + 
               Power(s2,2)*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               s2*(-4*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3))) + 
            s1*t2*(2*Power(s2,6) + Power(s2,5)*(-6*t1 + 8*t2) + 
               Power(s2,3)*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,4)*(6*Power(t1,2) - 12*t1*t2 + 7*Power(t2,2)) - 
               Power(t1,2)*t2*
                (Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-4*Power(t1,3) + 2*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3)) + 
               s2*t1*(2*Power(t1,4) + Power(t1,3)*t2 - t1*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,5)*(-t1 + t2) + 
               Power(s2,4)*(3*Power(t1,2) - 13*t1*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*t2*
                (Power(t1,3) + 3*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*t1*t2*(-Power(t1,3) + Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,3)*(-3*Power(t1,3) + 24*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,4) - 12*Power(t1,3)*t2 + 
                  13*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
                  Power(t2,4)))) + 
         Power(s,2)*(Power(s1,4)*
             (Power(s2,2)*(t1 + 2*t2) - 
               2*s2*(Power(t1,2) - Power(t2,2)) + 
               t1*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,3)*(2*Power(s2,3)*(t1 + t2) + 
               s2*t1*(8*Power(t1,2) - 5*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(-7*Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
               t1*(3*Power(t1,3) - 7*Power(t1,2)*t2 + 4*t1*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*(-t1 + t2) + 
               Power(s2,3)*(2*Power(t1,2) - 22*t1*t2 + 9*Power(t2,2)) - 
               Power(s2,2)*(Power(t1,3) - 34*Power(t1,2)*t2 + 
                  34*t1*Power(t2,2) - 9*Power(t2,3)) + 
               s2*t2*(-14*Power(t1,3) + 29*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + Power(t2,3)) + 
               t1*t2*(Power(t1,3) - 12*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 6*Power(t2,3))) + 
            t1*t2*(Power(s2,4)*t2 + 
               Power(s2,3)*(2*Power(t1,2) - Power(t2,2)) + 
               t1*t2*(-Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + Power(t2,3)) + 
               2*s2*(Power(t1,4) + 2*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) - t1*Power(t2,3) - Power(t2,4))\
) + s1*(4*Power(s2,5)*t2 + Power(s2,4)*
                (Power(t1,2) - 16*t1*t2 + 14*Power(t2,2)) - 
               2*s2*t1*t2*(3*Power(t1,3) + t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,3)*(-2*Power(t1,3) + 21*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 12*Power(t2,3)) + 
               2*t1*t2*(Power(t1,4) + 3*Power(t1,2)*Power(t2,2) - 
                  4*t1*Power(t2,3) - Power(t2,4)) + 
               Power(s2,2)*(Power(t1,4) - 5*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  2*Power(t2,4)))))*
       dilog(1 - (-s1 - t1 + t2)/(s - s1 - s2)))/
     (s*s1*s2*t1*Power(s - s2 + t1,2)*Power(t2,2)*(s - s1 + t2)*
       (s2 - t1 + t2)) + (4*((2*s2*(s - s2 + t1)*t2*(s - s1 + t2)*
            (-(Power(s1,2)*Power(s2,3)*
                 (Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*t1*
                 Power(s2 - t1 + t2,3)*(s1*(s2 - t1) - s2*t2)) + 
              2*Power(s,7)*s1*t1*(t1 - t2)*
               (Power(s2,3) - 3*Power(s2,2)*(t1 - t2) + 
                 3*s2*Power(t1 - t2,2) + Power(t1 - t2,2)*t2) + 
              Power(s,6)*(Power(t1,2)*(t1 - t2)*t2*
                  Power(s2 - t1 + t2,3) - 
                 2*s1*(-(t1*Power(t1 - t2,3)*t2*(t1 + t2)) + 
                    Power(s2,4)*
                     (2*Power(t1,2) - 6*t1*t2 - Power(t2,2)) + 
                    3*Power(s2,2)*Power(t1 - t2,2)*
                     (2*Power(t1,2) - 5*t1*t2 - Power(t2,2)) + 
                    s2*Power(t1 - t2,2)*t2*
                     (Power(t1,2) - 2*t1*t2 - Power(t2,2)) - 
                    Power(s2,3)*
                     (6*Power(t1,3) - 23*Power(t1,2)*t2 + 
                       14*t1*Power(t2,2) + 3*Power(t2,3))) + 
                 Power(s1,2)*
                  (-(Power(t1 - t2,3)*(5*t1 - t2)*t2) + 
                    2*Power(s2,4)*(2*t1 + t2) + 
                    Power(s2,3)*
                     (-18*Power(t1,2) + 11*t1*t2 + 5*Power(t2,2)) + 
                    3*Power(s2,2)*
                     (10*Power(t1,3) - 17*Power(t1,2)*t2 + 
                       6*t1*Power(t2,2) + Power(t2,3)) - 
                    s2*(18*Power(t1,4) - 59*Power(t1,3)*t2 + 
                       59*Power(t1,2)*Power(t2,2) - 
                       19*t1*Power(t2,3) + Power(t2,4)))) + 
              Power(s,5)*(-(Power(t1,2)*t2*Power(s2 - t1 + t2,3)*
                    (3*s2*t1 - 4*s2*t2 - t1*t2 + Power(t2,2))) + 
                 Power(s1,3)*
                  (2*Power(s2,5) + Power(t1 - t2,3)*(3*t1 - t2)*t2 + 
                    Power(s2,4)*(-15*t1 + 2*t2) + 
                    5*Power(s2,3)*
                     (8*Power(t1,2) - 6*t1*t2 - Power(t2,2)) - 
                    Power(s2,2)*
                     (47*Power(t1,3) - 77*Power(t1,2)*t2 + 
                       21*t1*Power(t2,2) + 7*Power(t2,3)) + 
                    s2*(20*Power(t1,4) - 62*Power(t1,3)*t2 + 
                       61*Power(t1,2)*Power(t2,2) - 
                       12*t1*Power(t2,3) - Power(t2,4))) - 
                 Power(s1,2)*
                  (2*Power(s2,5)*(5*t1 + 3*t2) + 
                    Power(t1 - t2,3)*t2*
                     (4*Power(t1,2) + t1*t2 - Power(t2,2)) + 
                    Power(s2,4)*
                     (-45*Power(t1,2) + 33*t1*t2 + 15*Power(t2,2)) + 
                    Power(s2,3)*
                     (75*Power(t1,3) - 144*Power(t1,2)*t2 + 
                       65*t1*Power(t2,2) + 10*Power(t2,3)) + 
                    Power(s2,2)*t1*
                     (-47*Power(t1,3) + 157*Power(t1,2)*t2 - 
                       179*t1*Power(t2,2) + 67*Power(t2,3)) + 
                    s2*t1*(3*Power(t1,4) - 18*Power(t1,3)*t2 + 
                       32*Power(t1,2)*Power(t2,2) - 
                       38*t1*Power(t2,3) + 21*Power(t2,4))) - 
                 s1*(-(Power(t1,2)*Power(t1 - t2,3)*(3*t1 - t2)*t2) + 
                    2*Power(s2,5)*
                     (Power(t1,2) + 15*t1*t2 + 5*Power(t2,2)) + 
                    s2*t1*Power(t1 - t2,2)*
                     (6*Power(t1,3) + 14*Power(t1,2)*t2 - 
                       15*t1*Power(t2,2) - 14*Power(t2,3)) - 
                    2*Power(s2,2)*Power(t1 - t2,2)*
                     (11*Power(t1,3) + 18*Power(t1,2)*t2 + 
                       11*t1*Power(t2,2) - 5*Power(t2,3)) + 
                    Power(s2,4)*
                     (-12*Power(t1,3) - 87*Power(t1,2)*t2 + 
                       46*t1*Power(t2,2) + 30*Power(t2,3)) + 
                    Power(s2,3)*
                     (24*Power(t1,4) + 72*Power(t1,3)*t2 - 
                       115*Power(t1,2)*Power(t2,2) - 
                       12*t1*Power(t2,3) + 30*Power(t2,4)))) + 
              s*s2*(-(Power(s2,3)*Power(t1,2)*Power(t2,3)*
                    Power(s2 - t1 + t2,3)) + 
                 Power(s1,5)*s2*(s2 - t1)*
                  (Power(s2,4) - Power(s2,3)*(t1 - 3*t2) + 
                    Power(s2,2)*(-2*Power(t1,2) + 3*Power(t2,2)) - 
                    t1*(Power(t1,3) - 4*Power(t1,2)*t2 + 
                       3*t1*Power(t2,2) - 2*Power(t2,3)) + 
                    s2*(3*Power(t1,3) - 7*Power(t1,2)*t2 + 
                       3*t1*Power(t2,2) + Power(t2,3))) + 
                 Power(s1,4)*
                  (2*Power(s2,7) - 4*Power(s2,5)*t1*(4*t1 - 3*t2) + 
                    4*Power(s2,6)*t2 + 
                    Power(t1,3)*Power(t1 - t2,3)*t2 + 
                    Power(s2,2)*Power(t1,2)*
                     (7*Power(t1,3) - 16*Power(t1,2)*t2 + 
                       8*t1*Power(t2,2) - 5*Power(t2,3)) + 
                    Power(s2,4)*
                     (29*Power(t1,3) - 49*Power(t1,2)*t2 + 
                       24*t1*Power(t2,2) - 4*Power(t2,3)) - 
                    s2*Power(t1,2)*
                     (Power(t1,4) - 4*Power(t1,2)*Power(t2,2) + 
                       4*t1*Power(t2,3) - 3*Power(t2,4)) + 
                    Power(s2,3)*
                     (-21*Power(t1,4) + 48*Power(t1,3)*t2 - 
                       33*Power(t1,2)*Power(t2,2) + 
                       12*t1*Power(t2,3) - 2*Power(t2,4))) - 
                 s1*Power(s2,2)*t2*
                  (2*Power(s2,6)*(t1 + t2) - 
                    s2*Power(t1,2)*Power(t1 - t2,2)*
                     (8*Power(t1,2) + t1*t2 - 12*Power(t2,2)) - 
                    2*Power(s2,5)*
                     (7*Power(t1,2) + 3*t1*t2 - 3*Power(t2,2)) + 
                    Power(t1,2)*Power(t1 - t2,2)*t2*
                     (2*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
                    Power(s2,4)*
                     (38*Power(t1,3) - 9*Power(t1,2)*t2 - 
                       24*t1*Power(t2,2) + 6*Power(t2,3)) + 
                    Power(s2,2)*t1*
                     (32*Power(t1,4) - 51*Power(t1,3)*t2 - 
                       3*Power(t1,2)*Power(t2,2) + 
                       27*t1*Power(t2,3) - 6*Power(t2,4)) + 
                    Power(s2,3)*
                     (-50*Power(t1,4) + 47*Power(t1,3)*t2 + 
                       24*Power(t1,2)*Power(t2,2) - 
                       22*t1*Power(t2,3) + 2*Power(t2,4))) + 
                 Power(s1,3)*
                  (Power(s2,7)*(10*t1 - t2) - 
                    Power(t1,3)*Power(t1 - t2,3)*Power(t2,2) - 
                    Power(s2,6)*
                     (38*Power(t1,2) - 22*t1*t2 + Power(t2,2)) + 
                    Power(s2,5)*
                     (58*Power(t1,3) - 55*Power(t1,2)*t2 - 
                       6*t1*Power(t2,2) + 3*Power(t2,3)) + 
                    2*s2*Power(t1,2)*t2*
                     (2*Power(t1,4) - 5*Power(t1,3)*t2 + 
                       4*Power(t1,2)*Power(t2,2) - Power(t2,4)) + 
                    Power(s2,4)*
                     (-46*Power(t1,4) + 57*Power(t1,3)*t2 + 
                       25*Power(t1,2)*Power(t2,2) - 
                       41*t1*Power(t2,3) + 5*Power(t2,4)) + 
                    Power(s2,2)*t1*
                     (-4*Power(t1,5) + Power(t1,4)*t2 + 
                       13*Power(t1,3)*Power(t2,2) - 
                       17*Power(t1,2)*Power(t2,3) + 
                       14*t1*Power(t2,4) - 3*Power(t2,5)) + 
                    2*Power(s2,3)*
                     (10*Power(t1,5) - 14*Power(t1,4)*t2 - 
                       10*Power(t1,3)*Power(t2,2) + 
                       23*Power(t1,2)*Power(t2,3) - 
                       13*t1*Power(t2,4) + Power(t2,5))) + 
                 Power(s1,2)*s2*
                  (2*Power(s2,7)*(t1 + t2) + 
                    Power(s2,6)*
                     (-10*Power(t1,2) - 16*t1*t2 + 5*Power(t2,2)) + 
                    Power(t1,2)*Power(t1 - t2,2)*t2*
                     (Power(t1,3) - 4*Power(t1,2)*t2 + 
                       5*t1*Power(t2,2) - Power(t2,3)) + 
                    Power(s2,5)*
                     (22*Power(t1,3) + 38*Power(t1,2)*t2 - 
                       45*t1*Power(t2,2) + 2*Power(t2,3)) - 
                    Power(s2,4)*
                     (26*Power(t1,4) + 39*Power(t1,3)*t2 - 
                       88*Power(t1,2)*Power(t2,2) + 
                       23*t1*Power(t2,3) + 4*Power(t2,4)) + 
                    Power(s2,3)*
                     (16*Power(t1,5) + 24*Power(t1,4)*t2 - 
                       74*Power(t1,3)*Power(t2,2) + 
                       18*Power(t1,2)*Power(t2,3) + 
                       17*t1*Power(t2,4) - 4*Power(t2,5)) + 
                    s2*t1*t2*
                     (4*Power(t1,5) - 7*Power(t1,4)*t2 - 
                       6*Power(t1,3)*Power(t2,2) + 
                       21*Power(t1,2)*Power(t2,3) - 
                       14*t1*Power(t2,4) + 2*Power(t2,5)) - 
                    Power(s2,2)*
                     (4*Power(t1,6) + 14*Power(t1,5)*t2 - 
                       39*Power(t1,4)*Power(t2,2) + 
                       5*Power(t1,3)*Power(t2,3) + 
                       27*Power(t1,2)*Power(t2,4) - 
                       15*t1*Power(t2,5) + Power(t2,6)))) + 
              Power(s,2)*(-(Power(s2,3)*Power(t1,2)*(s2 + t1 - 4*t2)*
                    Power(t2,2)*Power(s2 - t1 + t2,3)) - 
                 2*Power(s1,5)*s2*(s2 - t1)*
                  (Power(s2,4) - 3*Power(s2,3)*(t1 - t2) + 
                    Power(s2,2)*
                     (4*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
                    Power(t1,2)*
                     (Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)) + 
                    s2*(-3*Power(t1,3) + 5*Power(t1,2)*t2 - 
                       3*t1*Power(t2,2) + Power(t2,3))) - 
                 Power(s1,4)*
                  (7*Power(s2,7) - 3*Power(s2,6)*(7*t1 - 5*t2) + 
                    Power(t1,3)*Power(t1 - t2,3)*t2 + 
                    Power(s2,5)*
                     (27*Power(t1,2) - 20*t1*t2 + 3*Power(t2,2)) + 
                    Power(s2,4)*
                     (-19*Power(t1,3) + 5*Power(t1,2)*t2 + 
                       27*t1*Power(t2,2) - 11*Power(t2,3)) - 
                    s2*Power(t1,2)*
                     (3*Power(t1,4) - 6*Power(t1,3)*t2 + 
                       Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) - 
                       6*Power(t2,4)) + 
                    Power(s2,3)*
                     (3*Power(t1,4) + 6*Power(t1,3)*t2 - 
                       16*Power(t1,2)*Power(t2,2) + 
                       30*t1*Power(t2,3) - 6*Power(t2,4)) + 
                    Power(s2,2)*t1*
                     (6*Power(t1,4) - 13*Power(t1,3)*t2 + 
                       Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                       4*Power(t2,4))) + 
                 Power(s1,3)*
                  (-2*Power(s2,8) + 
                    Power(t1,3)*Power(t1 - t2,3)*Power(t2,2) + 
                    Power(s2,7)*(-7*t1 + t2) + 
                    Power(s2,6)*
                     (39*Power(t1,2) - 31*t1*t2 + 8*Power(t2,2)) + 
                    Power(s2,5)*
                     (-69*Power(t1,3) + 65*Power(t1,2)*t2 + 
                       5*t1*Power(t2,2) - 2*Power(t2,3)) + 
                    Power(s2,4)*
                     (73*Power(t1,4) - 71*Power(t1,3)*t2 - 
                       73*Power(t1,2)*Power(t2,2) + 
                       83*t1*Power(t2,3) - 14*Power(t2,4)) + 
                    s2*Power(t1,2)*t2*
                     (-11*Power(t1,4) + 33*Power(t1,3)*t2 - 
                       34*Power(t1,2)*Power(t2,2) + 
                       9*t1*Power(t2,3) + 3*Power(t2,4)) + 
                    Power(s2,3)*
                     (-48*Power(t1,5) + 53*Power(t1,4)*t2 + 
                       61*Power(t1,3)*Power(t2,2) - 
                       98*Power(t1,2)*Power(t2,3) + 
                       62*t1*Power(t2,4) - 7*Power(t2,5)) + 
                    Power(s2,2)*t1*
                     (14*Power(t1,5) - 6*Power(t1,4)*t2 - 
                       49*Power(t1,3)*Power(t2,2) + 
                       69*Power(t1,2)*Power(t2,3) - 
                       44*t1*Power(t2,4) + 8*Power(t2,5))) + 
                 s1*Power(s2,2)*
                  (-(s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                       (24*Power(t1,2) - 4*t1*t2 - 35*Power(t2,2))) + 
                    2*Power(s2,6)*
                     (Power(t1,2) + 6*t1*t2 + 5*Power(t2,2)) - 
                    Power(s2,5)*
                     (10*Power(t1,3) + 63*Power(t1,2)*t2 + 
                       20*t1*Power(t2,2) - 30*Power(t2,3)) - 
                    Power(t1,2)*Power(t1 - t2,2)*t2*
                     (Power(t1,3) - 7*Power(t1,2)*t2 + 
                       14*t1*Power(t2,2) - 6*Power(t2,3)) + 
                    2*Power(s2,4)*
                     (9*Power(t1,4) + 69*Power(t1,3)*t2 - 
                       26*Power(t1,2)*Power(t2,2) - 
                       51*t1*Power(t2,3) + 15*Power(t2,4)) + 
                    Power(s2,2)*t1*
                     (4*Power(t1,5) + 99*Power(t1,4)*t2 - 
                       187*Power(t1,3)*Power(t2,2) + 
                       26*Power(t1,2)*Power(t2,3) + 
                       82*t1*Power(t2,4) - 26*Power(t2,5)) + 
                    Power(s2,3)*
                     (-14*Power(t1,5) - 161*Power(t1,4)*t2 + 
                       188*Power(t1,3)*Power(t2,2) + 
                       78*Power(t1,2)*Power(t2,3) - 
                       96*t1*Power(t2,4) + 10*Power(t2,5))) - 
                 Power(s1,2)*s2*
                  (Power(s2,7)*(8*t1 + 6*t2) + 
                    Power(s2,6)*
                     (-38*Power(t1,2) - 36*t1*t2 + 15*Power(t2,2)) + 
                    Power(t1,2)*Power(t1 - t2,2)*t2*
                     (Power(t1,3) - 6*Power(t1,2)*t2 + 
                       9*t1*Power(t2,2) - 2*Power(t2,3)) + 
                    Power(s2,5)*
                     (84*Power(t1,3) + 79*Power(t1,2)*t2 - 
                       110*t1*Power(t2,2) + 5*Power(t2,3)) - 
                    Power(s2,4)*
                     (104*Power(t1,4) + 72*Power(t1,3)*t2 - 
                       224*Power(t1,2)*Power(t2,2) + 
                       43*t1*Power(t2,3) + 15*Power(t2,4)) + 
                    Power(s2,3)*
                     (68*Power(t1,5) + 47*Power(t1,4)*t2 - 
                       220*Power(t1,3)*Power(t2,2) + 
                       43*Power(t1,2)*Power(t2,3) + 
                       66*t1*Power(t2,4) - 15*Power(t2,5)) + 
                    s2*t1*t2*
                     (24*Power(t1,5) - 64*Power(t1,4)*t2 + 
                       39*Power(t1,3)*Power(t2,2) + 
                       28*Power(t1,2)*Power(t2,3) - 
                       33*t1*Power(t2,4) + 6*Power(t2,5)) - 
                    Power(s2,2)*
                     (18*Power(t1,6) + 49*Power(t1,5)*t2 - 
                       169*Power(t1,4)*Power(t2,2) + 
                       86*Power(t1,3)*Power(t2,3) + 
                       55*Power(t1,2)*Power(t2,4) - 
                       49*t1*Power(t2,5) + 4*Power(t2,6)))) + 
              Power(s,4)*(s2*Power(t1,2)*t2*Power(s2 - t1 + t2,3)*
                  (3*s2*(t1 - 2*t2) + t2*(-3*t1 + 4*t2)) + 
                 Power(s1,4)*
                  (-3*Power(s2,5) + Power(s2,4)*(16*t1 - 7*t2) + 
                    Power(t1,2)*t2*(Power(t1,2) + Power(t2,2)) - 
                    3*Power(s2,3)*
                     (11*Power(t1,2) - 11*t1*t2 + Power(t2,2)) + 
                    Power(s2,2)*
                     (30*Power(t1,3) - 52*Power(t1,2)*t2 + 
                       18*t1*Power(t2,2) + 3*Power(t2,3)) + 
                    s2*(-10*Power(t1,4) + 25*Power(t1,3)*t2 - 
                       26*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                       2*Power(t2,4))) + 
                 s1*(2*Power(t1,2)*Power(t1 - t2,4)*Power(t2,2) - 
                    s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                     (17*Power(t1,2) - 14*t1*t2 - 9*Power(t2,2)) + 
                    10*Power(s2,6)*
                     (Power(t1,2) + 4*t1*t2 + 2*Power(t2,2)) + 
                    2*Power(s2,2)*t1*Power(t1 - t2,2)*
                     (8*Power(t1,3) + 34*Power(t1,2)*t2 - 
                       15*t1*Power(t2,2) - 18*Power(t2,3)) + 
                    Power(s2,5)*
                     (-46*Power(t1,3) - 119*Power(t1,2)*t2 + 
                       24*t1*Power(t2,2) + 60*Power(t2,3)) + 
                    2*Power(s2,4)*
                     (39*Power(t1,4) + 58*Power(t1,3)*t2 - 
                       69*Power(t1,2)*Power(t2,2) - 
                       54*t1*Power(t2,3) + 30*Power(t2,4)) - 
                    2*Power(s2,3)*
                     (29*Power(t1,5) + 24*Power(t1,4)*t2 - 
                       75*Power(t1,3)*Power(t2,2) - 
                       33*Power(t1,2)*Power(t2,3) + 
                       64*t1*Power(t2,4) - 10*Power(t2,5))) + 
                 Power(s1,3)*
                  (-6*Power(s2,6) + Power(s2,5)*(37*t1 - 5*t2) + 
                    Power(s2,4)*
                     (-97*Power(t1,2) + 61*t1*t2 + 16*Power(t2,2)) + 
                    Power(s2,3)*
                     (123*Power(t1,3) - 172*Power(t1,2)*t2 + 
                       35*t1*Power(t2,2) + 18*Power(t2,3)) + 
                    Power(s2,2)*
                     (-65*Power(t1,4) + 161*Power(t1,3)*t2 - 
                       147*Power(t1,2)*Power(t2,2) + 
                       37*t1*Power(t2,3) - 2*Power(t2,4)) + 
                    2*t1*t2*
                     (Power(t1,4) - 4*Power(t1,3)*t2 + 
                       7*Power(t1,2)*Power(t2,2) - 
                       5*t1*Power(t2,3) + Power(t2,4)) + 
                    s2*(8*Power(t1,5) - 27*Power(t1,4)*t2 + 
                       40*Power(t1,3)*Power(t2,2) - 
                       34*Power(t1,2)*Power(t2,3) + 
                       28*t1*Power(t2,4) - 5*Power(t2,5))) + 
                 Power(s1,2)*
                  (4*Power(s2,6)*(t1 + t2) + 
                    Power(s2,5)*
                     (-18*Power(t1,2) + 43*t1*t2 + 10*Power(t2,2)) - 
                    t1*Power(t1 - t2,2)*t2*
                     (4*Power(t1,3) - 6*Power(t1,2)*t2 - 
                       t1*Power(t2,2) + 2*Power(t2,3)) + 
                    Power(s2,4)*
                     (15*Power(t1,3) - 156*Power(t1,2)*t2 + 
                       114*t1*Power(t2,2) + 10*Power(t2,3)) + 
                    Power(s2,3)*
                     (27*Power(t1,4) + 125*Power(t1,3)*t2 - 
                       265*Power(t1,2)*Power(t2,2) + 
                       92*t1*Power(t2,3) + 10*Power(t2,4)) + 
                    Power(s2,2)*
                     (-45*Power(t1,5) + 32*Power(t1,4)*t2 + 
                       63*Power(t1,3)*Power(t2,2) - 
                       47*Power(t1,2)*Power(t2,3) - 
                       8*t1*Power(t2,4) + 10*Power(t2,5)) + 
                    s2*(15*Power(t1,6) - 16*Power(t1,5)*t2 - 
                       38*Power(t1,4)*Power(t2,2) + 
                       57*Power(t1,3)*Power(t2,3) + 
                       5*Power(t1,2)*Power(t2,4) - 27*t1*Power(t2,5) + 
                       4*Power(t2,6)))) + 
              Power(s,3)*(-(Power(s2,2)*Power(t1,2)*t2*
                    Power(s2 - t1 + t2,3)*
                    (s2*(t1 - 4*t2) - 3*(t1 - 2*t2)*t2)) + 
                 Power(s1,5)*
                  (Power(s2,5) + Power(s2,4)*(-5*t1 + 3*t2) - 
                    Power(t1,2)*t2*(Power(t1,2) + Power(t2,2)) + 
                    3*Power(s2,3)*
                     (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
                    2*s2*t1*(Power(t1,3) - 2*Power(t1,2)*t2 + 
                       3*t1*Power(t2,2) - Power(t2,3)) + 
                    Power(s2,2)*
                     (-7*Power(t1,3) + 14*Power(t1,2)*t2 - 
                       9*t1*Power(t2,2) + Power(t2,3))) + 
                 Power(s1,4)*
                  (8*Power(s2,6) + Power(s2,5)*(-35*t1 + 18*t2) + 
                    Power(t1,2)*Power(t2,2)*
                     (Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)) + 
                    Power(s2,4)*
                     (70*Power(t1,2) - 60*t1*t2 + 6*Power(t2,2)) - 
                    Power(s2,3)*
                     (74*Power(t1,3) - 97*Power(t1,2)*t2 + 
                       12*t1*Power(t2,2) + 10*Power(t2,3)) + 
                    Power(s2,2)*
                     (38*Power(t1,4) - 70*Power(t1,3)*t2 + 
                       43*Power(t1,2)*Power(t2,2) + 
                       16*t1*Power(t2,3) - 6*Power(t2,4)) + 
                    s2*t1*(-7*Power(t1,4) + 15*Power(t1,3)*t2 - 
                       16*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
                       3*Power(t2,4))) + 
                 Power(s1,3)*
                  (6*Power(s2,7) + Power(s2,6)*(-23*t1 + 3*t2) + 
                    6*Power(s2,5)*
                     (8*Power(t1,2) - 3*t1*t2 - 3*Power(t2,2)) - 
                    Power(s2,4)*
                     (51*Power(t1,3) - 73*Power(t1,2)*t2 + 
                       13*t1*Power(t2,2) + 12*Power(t2,3)) + 
                    Power(t1,2)*t2*
                     (3*Power(t1,4) - 11*Power(t1,3)*t2 + 
                       14*Power(t1,2)*Power(t2,2) - 
                       5*t1*Power(t2,3) - Power(t2,4)) + 
                    Power(s2,3)*
                     (4*Power(t1,4) - 63*Power(t1,3)*t2 + 
                       134*Power(t1,2)*Power(t2,2) - 
                       71*t1*Power(t2,3) + 12*Power(t2,4)) + 
                    s2*t1*(-12*Power(t1,5) + 21*Power(t1,4)*t2 + 
                       14*Power(t1,3)*Power(t2,2) - 
                       52*Power(t1,2)*Power(t2,3) + 
                       40*t1*Power(t2,4) - 7*Power(t2,5)) + 
                    Power(s2,2)*
                     (28*Power(t1,5) - 29*Power(t1,4)*t2 - 
                       53*Power(t1,3)*Power(t2,2) + 
                       78*Power(t1,2)*Power(t2,3) - 
                       60*t1*Power(t2,4) + 9*Power(t2,5))) - 
                 s1*s2*(-3*s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                     (10*Power(t1,2) - 5*t1*t2 - 11*Power(t2,2)) + 
                    Power(s2,6)*
                     (8*Power(t1,2) + 30*t1*t2 + 20*Power(t2,2)) - 
                    Power(s2,5)*
                     (38*Power(t1,3) + 115*Power(t1,2)*t2 + 
                       14*t1*Power(t2,2) - 60*Power(t2,3)) - 
                    Power(t1,2)*Power(t1 - t2,2)*t2*
                     (Power(t1,3) - 7*Power(t1,2)*t2 + 
                       13*t1*Power(t2,2) - 6*Power(t2,3)) + 
                    6*Power(s2,4)*
                     (11*Power(t1,4) + 30*Power(t1,3)*t2 - 
                       19*Power(t1,2)*Power(t2,2) - 
                       27*t1*Power(t2,3) + 10*Power(t2,4)) + 
                    Power(s2,2)*t1*
                     (14*Power(t1,5) + 103*Power(t1,4)*t2 - 
                       253*Power(t1,3)*Power(t2,2) + 
                       91*Power(t1,2)*Power(t2,3) + 
                       88*t1*Power(t2,4) - 44*Power(t2,5)) + 
                    Power(s2,3)*
                     (-50*Power(t1,5) - 167*Power(t1,4)*t2 + 
                       265*Power(t1,3)*Power(t2,2) + 
                       100*Power(t1,2)*Power(t2,3) - 
                       162*t1*Power(t2,4) + 20*Power(t2,5))) + 
                 Power(s1,2)*
                  (4*Power(s2,7)*(2*t1 + t2) + 
                    2*Power(s2,5)*t1*
                     (46*Power(t1,2) + 55*t1*t2 - 69*Power(t2,2)) - 
                    Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                     (2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
                    Power(s2,6)*
                     (-37*Power(t1,2) - 43*t1*t2 + 10*Power(t2,2)) - 
                    Power(s2,4)*
                     (134*Power(t1,4) + 66*Power(t1,3)*t2 - 
                       293*Power(t1,2)*Power(t2,2) + 
                       70*t1*Power(t2,3) + 20*Power(t2,4)) + 
                    Power(s2,3)*
                     (100*Power(t1,5) - 20*Power(t1,4)*t2 - 
                       199*Power(t1,3)*Power(t2,2) + 
                       58*Power(t1,2)*Power(t2,3) + 
                       68*t1*Power(t2,4) - 20*Power(t2,5)) + 
                    s2*t1*t2*
                     (24*Power(t1,5) - 71*Power(t1,4)*t2 + 
                       60*Power(t1,3)*Power(t2,2) + 
                       5*Power(t1,2)*Power(t2,3) - 24*t1*Power(t2,4) + 
                       6*Power(t2,5)) - 
                    Power(s2,2)*
                     (29*Power(t1,6) + 15*Power(t1,5)*t2 - 
                       157*Power(t1,4)*Power(t2,2) + 
                       130*Power(t1,3)*Power(t2,3) + 
                       31*Power(t1,2)*Power(t2,4) - 57*t1*Power(t2,5) + 
                       6*Power(t2,6))))))/
          (Power(s - s2,2)*Power(s2 - t1 + t2,3)) - 
         (s1*t1*(s - s2 + t1)*t2*
            (Power(s,8)*t1*(2*Power(s2,4) + 5*Power(s2,3)*(t1 - t2) - 
                 6*Power(s2,2)*Power(t1 - t2,2) + 
                 7*s2*Power(t1 - t2,3) - 2*Power(t1 - t2,4)) + 
              2*Power(s1,3)*Power(s2,2)*
               (2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*(s1 - t2)*
               Power(s2 - t1 + t2,3)*(s1*(s2 - t1) - s2*t2) + 
              Power(s,7)*(-(t1*
                    (2*Power(s2,5) + Power(s2,4)*(8*t1 - 15*t2) + 
                      2*Power(t1 - t2,4)*(t1 + 2*t2) + 
                      Power(s2,2)*Power(t1 - t2,2)*(22*t1 + 5*t2) - 
                      2*s2*Power(t1 - t2,3)*(7*t1 + 6*t2) + 
                      10*Power(s2,3)*
                       (-2*Power(t1,2) + t1*t2 + Power(t2,2)))) + 
                 s1*(2*t1*(6*t1 - 5*t2)*Power(t1 - t2,3) + 
                    Power(s2,4)*(-7*t1 + 4*t2) - 
                    s2*Power(t1 - t2,2)*
                     (41*Power(t1,2) - 40*t1*t2 - 4*Power(t2,2)) + 
                    Power(s2,3)*
                     (-27*Power(t1,2) + 46*t1*t2 + 12*Power(t2,2)) + 
                    3*Power(s2,2)*
                     (13*Power(t1,3) - 28*Power(t1,2)*t2 + 
                       11*t1*Power(t2,2) + 4*Power(t2,3)))) + 
              Power(s,6)*(t1*
                  (Power(s2,5)*(6*t1 - 8*t2) - 
                    2*Power(t1 - t2,4)*t2*(2*t1 + t2) - 
                    2*Power(s2,4)*
                     (6*Power(t1,2) + 8*t1*t2 - 23*Power(t2,2)) - 
                    2*Power(s2,2)*Power(t1 - t2,2)*
                     (3*Power(t1,2) + 20*t1*t2 - 11*Power(t2,2)) + 
                    s2*Power(t1 - t2,3)*
                     (3*Power(t1,2) + 28*t1*t2 - Power(t2,2)) + 
                    Power(s2,3)*
                     (9*Power(t1,3) + 37*Power(t1,2)*t2 - 
                       59*t1*Power(t2,2) + 13*Power(t2,3))) + 
                 2*Power(s1,2)*
                  (2*Power(s2,5) - 
                    5*t1*(3*t1 - 2*t2)*Power(t1 - t2,3) - 
                    Power(s2,4)*(t1 + 4*t2) + 
                    Power(s2,3)*
                     (33*Power(t1,2) - 91*t1*t2 - 24*Power(t2,2)) - 
                    Power(s2,2)*
                     (50*Power(t1,3) - 138*Power(t1,2)*t2 + 
                       45*t1*Power(t2,2) + 28*Power(t2,3)) + 
                    s2*(49*Power(t1,4) - 152*Power(t1,3)*t2 + 
                       141*Power(t1,2)*Power(t2,2) - 
                       28*t1*Power(t2,3) - 10*Power(t2,4))) + 
                 2*s1*(Power(s2,5)*(3*t1 - 2*t2) + 
                    t1*Power(t1 - t2,3)*
                     (6*Power(t1,2) + 5*t1*t2 - 8*Power(t2,2)) + 
                    Power(s2,4)*
                     (26*Power(t1,2) - 38*t1*t2 + 3*Power(t2,2)) - 
                    s2*Power(t1 - t2,2)*
                     (41*Power(t1,3) + 2*Power(t1,2)*t2 - 
                       31*t1*Power(t2,2) - 9*Power(t2,3)) + 
                    Power(s2,3)*
                     (-66*Power(t1,3) + 37*Power(t1,2)*t2 + 
                       50*t1*Power(t2,2) + 21*Power(t2,3)) + 
                    Power(s2,2)*
                     (72*Power(t1,4) - 94*Power(t1,3)*t2 - 
                       9*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) + 
                       25*Power(t2,4)))) - 
              Power(s,5)*(t1*
                  (2*Power(s2,6)*t1 + 
                    2*t1*Power(t1 - t2,4)*Power(t2,2) + 
                    Power(s2,4)*t2*
                     (33*Power(t1,2) - 32*t1*t2 - 27*Power(t2,2)) - 
                    2*Power(s2,5)*
                     (2*Power(t1,2) + 7*t1*t2 - 12*Power(t2,2)) - 
                    2*s2*Power(t1 - t2,3)*t2*
                     (3*Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)) + 
                    2*Power(s2,3)*t1*
                     (2*Power(t1,3) - 15*Power(t1,2)*t2 + 
                       17*t1*Power(t2,2) - 4*Power(t2,3)) - 
                    Power(s2,2)*Power(t1 - t2,2)*
                     (2*Power(t1,3) - 13*Power(t1,2)*t2 + 
                       8*t1*Power(t2,2) + 15*Power(t2,3))) + 
                 2*Power(s1,3)*
                  (10*Power(s2,5) - 
                    10*t1*Power(t1 - t2,3)*(2*t1 - t2) + 
                    Power(s2,4)*(-24*t1 + 10*t2) + 
                    Power(s2,3)*
                     (50*Power(t1,2) - 202*t1*t2 - 30*Power(t2,2)) - 
                    Power(s2,2)*
                     (64*Power(t1,3) - 270*Power(t1,2)*t2 + 
                       87*t1*Power(t2,2) + 50*Power(t2,3)) + 
                    s2*(60*Power(t1,4) - 214*Power(t1,3)*t2 + 
                       195*Power(t1,2)*Power(t2,2) - 
                       24*t1*Power(t2,3) - 20*Power(t2,4))) + 
                 s1*(-2*Power(s2,6)*(t1 - 4*t2) - 
                    2*t1*Power(t1 - t2,3)*t2*
                     (10*Power(t1,2) - 4*t1*t2 - 3*Power(t2,2)) + 
                    Power(s2,5)*
                     (36*Power(t1,2) - 40*t1*t2 + 66*Power(t2,2)) + 
                    Power(s2,4)*
                     (-75*Power(t1,3) - 106*Power(t1,2)*t2 + 
                       115*t1*Power(t2,2) + 138*Power(t2,3)) + 
                    s2*Power(t1 - t2,2)*
                     (15*Power(t1,4) + 118*Power(t1,3)*t2 - 
                       71*Power(t1,2)*Power(t2,2) - 
                       32*t1*Power(t2,3) - 12*Power(t2,4)) + 
                    Power(s2,3)*
                     (65*Power(t1,4) + 252*Power(t1,3)*t2 - 
                       269*Power(t1,2)*Power(t2,2) - 
                       230*t1*Power(t2,3) + 98*Power(t2,4)) - 
                    Power(s2,2)*
                     (39*Power(t1,5) + 182*Power(t1,4)*t2 - 
                       372*Power(t1,3)*Power(t2,2) + 
                       72*Power(t1,2)*Power(t2,3) + 
                       85*t1*Power(t2,4) - 6*Power(t2,5))) + 
                 2*Power(s1,2)*
                  (6*Power(s2,6) + 3*Power(s2,5)*(-6*t1 + t2) + 
                    t1*Power(t1 - t2,3)*
                     (15*Power(t1,2) + 10*t1*t2 - 12*Power(t2,2)) + 
                    Power(s2,4)*
                     (91*Power(t1,2) - 112*t1*t2 + 6*Power(t2,2)) + 
                    Power(s2,3)*
                     (-186*Power(t1,3) + 128*Power(t1,2)*t2 + 
                       189*t1*Power(t2,2) + 60*Power(t2,3)) + 
                    3*Power(s2,2)*
                     (64*Power(t1,4) - 65*Power(t1,3)*t2 - 
                       57*Power(t1,2)*Power(t2,2) + 
                       18*t1*Power(t2,3) + 28*Power(t2,4)) + 
                    s2*(-100*Power(t1,5) + 175*Power(t1,4)*t2 + 
                       33*Power(t1,3)*Power(t2,2) - 
                       149*Power(t1,2)*Power(t2,3) + 
                       8*t1*Power(t2,4) + 33*Power(t2,5)))) + 
              Power(s,2)*(-2*Power(s2,3)*(s2 - t1)*t1*Power(t2,3)*
                  Power(s2 - t1 + t2,3) - 
                 2*s1*Power(s2,2)*Power(t2,2)*
                  (Power(s2,6) + Power(s2,5)*(-3*t1 + 5*t2) + 
                    Power(s2,4)*t2*(-11*t1 + 8*t2) + 
                    t1*Power(t1 - t2,2)*t2*
                     (8*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
                    s2*Power(t1 - t2,2)*
                     (3*Power(t1,3) - 19*Power(t1,2)*t2 + 
                       5*t1*Power(t2,2) - Power(t2,3)) + 
                    2*Power(s2,3)*
                     (4*Power(t1,3) - Power(t1,2)*t2 - 
                       7*t1*Power(t2,2) + 2*Power(t2,3)) - 
                    Power(s2,2)*
                     (9*Power(t1,4) - 25*Power(t1,3)*t2 + 
                       18*Power(t1,2)*Power(t2,2) - 
                       6*t1*Power(t2,3) + Power(t2,4))) + 
                 2*Power(s1,6)*
                  (10*Power(s2,5) - Power(t1,2)*Power(t1 - t2,3) + 
                    Power(s2,4)*(-25*t1 + 28*t2) + 
                    8*Power(s2,3)*
                     (2*Power(t1,2) - 12*t1*t2 + 3*Power(t2,2)) + 
                    Power(s2,2)*
                     (2*Power(t1,3) + 102*Power(t1,2)*t2 - 
                       57*t1*Power(t2,2) + 4*Power(t2,3)) - 
                    s2*(2*Power(t1,4) + 37*Power(t1,3)*t2 - 
                       36*Power(t1,2)*Power(t2,2) + 
                       7*t1*Power(t2,3) + 2*Power(t2,4))) + 
                 2*Power(s1,5)*
                  (30*Power(s2,6) + 
                    Power(t1,2)*Power(t1 - t2,3)*(6*t1 + t2) + 
                    Power(s2,5)*(-111*t1 + 63*t2) + 
                    Power(s2,4)*
                     (172*Power(t1,2) - 220*t1*t2 + 15*Power(t2,2)) + 
                    Power(s2,3)*
                     (-156*Power(t1,3) + 236*Power(t1,2)*t2 + 
                       114*t1*Power(t2,2) - 33*Power(t2,3)) + 
                    3*Power(s2,2)*
                     (32*Power(t1,4) - 34*Power(t1,3)*t2 - 
                       87*Power(t1,2)*Power(t2,2) + 
                       56*t1*Power(t2,3) - 3*Power(t2,4)) + 
                    s2*(-37*Power(t1,5) + 40*Power(t1,4)*t2 + 
                       114*Power(t1,3)*Power(t2,2) - 
                       140*Power(t1,2)*Power(t2,3) + 
                       35*t1*Power(t2,4) + 6*Power(t2,5))) + 
                 2*Power(s1,3)*
                  (5*Power(s2,8) + 
                    Power(t1,2)*Power(t1 - t2,3)*(4*t1 - t2)*
                     Power(t2,2) - Power(s2,7)*(23*t1 + 7*t2) + 
                    Power(s2,6)*
                     (46*Power(t1,2) + 32*t1*t2 - 17*Power(t2,2)) + 
                    Power(s2,5)*
                     (-46*Power(t1,3) - 82*Power(t1,2)*t2 + 
                       47*t1*Power(t2,2) + 22*Power(t2,3)) + 
                    Power(s2,4)*
                     (17*Power(t1,4) + 133*Power(t1,3)*t2 + 
                       32*Power(t1,2)*Power(t2,2) - 
                       259*t1*Power(t2,3) + 23*Power(t2,4)) + 
                    Power(s2,3)*
                     (5*Power(t1,5) - 125*Power(t1,4)*t2 - 
                       89*Power(t1,3)*Power(t2,2) + 
                       389*Power(t1,2)*Power(t2,3) - 
                       91*t1*Power(t2,4) - 23*Power(t2,5)) + 
                    s2*t1*t2*
                     (-12*Power(t1,5) + 22*Power(t1,4)*t2 - 
                       34*Power(t1,3)*Power(t2,2) + 
                       69*Power(t1,2)*Power(t2,3) - 
                       70*t1*Power(t2,4) + 25*Power(t2,5)) + 
                    Power(s2,2)*
                     (-4*Power(t1,6) + 61*Power(t1,5)*t2 + 
                       Power(t1,4)*Power(t2,2) - 
                       106*Power(t1,3)*Power(t2,3) - 
                       10*Power(t1,2)*Power(t2,4) + 
                       65*t1*Power(t2,5) - 19*Power(t2,6))) + 
                 Power(s1,4)*
                  (44*Power(s2,7) - 
                    2*Power(t1,2)*Power(t1 - t2,3)*(10*t1 - t2)*t2 + 
                    Power(s2,6)*(-170*t1 + 46*t2) + 
                    Power(s2,5)*
                     (276*Power(t1,2) - 88*t1*t2 - 50*Power(t2,2)) + 
                    Power(s2,4)*
                     (-254*Power(t1,3) - 118*Power(t1,2)*t2 + 
                       484*t1*Power(t2,2) + 6*Power(t2,3)) + 
                    Power(s2,3)*
                     (155*Power(t1,4) + 337*Power(t1,3)*t2 - 
                       686*Power(t1,2)*Power(t2,2) - 
                       248*t1*Power(t2,3) + 118*Power(t2,4)) + 
                    Power(s2,2)*
                     (-66*Power(t1,5) - 224*Power(t1,4)*t2 + 
                       242*Power(t1,3)*Power(t2,2) + 
                       600*Power(t1,2)*Power(t2,3) - 
                       466*t1*Power(t2,4) + 52*Power(t2,5)) + 
                    s2*(15*Power(t1,6) + 67*Power(t1,5)*t2 - 
                       49*Power(t1,4)*Power(t2,2) - 
                       295*Power(t1,3)*Power(t2,3) + 
                       374*Power(t1,2)*Power(t2,4) - 
                       116*t1*Power(t2,5) - 8*Power(t2,6))) - 
                 Power(s1,2)*s2*t2*
                  (4*Power(s2,7) - 2*Power(s2,6)*(11*t1 + 2*t2) + 
                    2*Power(s2,5)*
                     (30*Power(t1,2) + 11*t1*t2 - 5*Power(t2,2)) - 
                    t1*Power(t1 - t2,2)*t2*
                     (9*Power(t1,3) - 27*Power(t1,2)*t2 + 
                       14*t1*Power(t2,2) - 2*Power(t2,3)) + 
                    Power(s2,4)*
                     (-76*Power(t1,3) - 58*Power(t1,2)*t2 + 
                       92*t1*Power(t2,2) + 30*Power(t2,3)) + 
                    2*Power(s2,3)*
                     (14*Power(t1,4) + 50*Power(t1,3)*t2 - 
                       42*Power(t1,2)*Power(t2,2) - 
                       73*t1*Power(t2,3) + 25*Power(t2,4)) + 
                    Power(s2,2)*
                     (18*Power(t1,5) - 103*Power(t1,4)*t2 + 
                       19*Power(t1,3)*Power(t2,2) + 
                       192*Power(t1,2)*Power(t2,3) - 
                       122*t1*Power(t2,4) + 14*Power(t2,5)) - 
                    2*s2*(6*Power(t1,6) - 26*Power(t1,5)*t2 + 
                       31*Power(t1,4)*Power(t2,2) - 
                       2*Power(t1,3)*Power(t2,3) - 
                       19*Power(t1,2)*Power(t2,4) + 8*t1*Power(t2,5) + 
                       2*Power(t2,6)))) - 
              Power(s,3)*(Power(s2,2)*t1*Power(t2,2)*
                  (2*Power(s2,5) - 4*Power(s2,4)*t1 + 
                    2*Power(s2,3)*(5*t1 - 3*t2)*t2 - 
                    t1*(7*t1 - 4*t2)*Power(t1 - t2,2)*t2 - 
                    2*s2*Power(t1 - t2,2)*
                     (Power(t1,2) - 10*t1*t2 + 3*Power(t2,2)) + 
                    Power(s2,2)*
                     (4*Power(t1,3) - 27*Power(t1,2)*t2 + 
                       36*t1*Power(t2,2) - 16*Power(t2,3))) + 
                 Power(s1,5)*
                  (40*Power(s2,5) - 
                    2*t1*Power(t1 - t2,3)*(6*t1 - t2) + 
                    Power(s2,4)*(-101*t1 + 100*t2) + 
                    Power(s2,3)*
                     (75*Power(t1,2) - 434*t1*t2 + 60*Power(t2,2)) - 
                    Power(s2,2)*
                     (19*Power(t1,3) - 492*Power(t1,2)*t2 + 
                       225*t1*Power(t2,2) + 20*Power(t2,3)) + 
                    s2*(17*Power(t1,4) - 214*Power(t1,3)*t2 + 
                       201*Power(t1,2)*Power(t2,2) - 
                       20*t1*Power(t2,3) - 20*Power(t2,4))) + 
                 Power(s1,4)*
                  (76*Power(s2,6) + 
                    Power(s2,4)*t1*(472*t1 - 571*t2) + 
                    Power(s2,5)*(-274*t1 + 134*t2) + 
                    2*t1*Power(t1 - t2,3)*
                     (15*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
                    Power(s2,3)*
                     (-544*Power(t1,3) + 632*Power(t1,2)*t2 + 
                       622*t1*Power(t2,2) - 44*Power(t2,3)) + 
                    Power(s2,2)*
                     (430*Power(t1,4) - 371*Power(t1,3)*t2 - 
                       1032*Power(t1,2)*Power(t2,2) + 
                       545*t1*Power(t2,3) + 68*Power(t2,4)) + 
                    s2*(-190*Power(t1,5) + 250*Power(t1,4)*t2 + 
                       366*Power(t1,3)*Power(t2,2) - 
                       524*Power(t1,2)*Power(t2,3) + 
                       80*t1*Power(t2,4) + 54*Power(t2,5))) + 
                 Power(s1,3)*
                  (38*Power(s2,7) + Power(s2,6)*(-148*t1 + 44*t2) - 
                    2*t1*Power(t1 - t2,3)*t2*
                     (20*Power(t1,2) - 4*t1*t2 - Power(t2,2)) + 
                    Power(s2,5)*
                     (276*Power(t1,2) - 76*t1*t2 + 50*Power(t2,2)) + 
                    Power(s2,4)*
                     (-314*Power(t1,3) - 292*Power(t1,2)*t2 + 
                       547*t1*Power(t2,2) + 238*Power(t2,3)) + 
                    Power(s2,3)*
                     (232*Power(t1,4) + 656*Power(t1,3)*t2 - 
                       947*Power(t1,2)*Power(t2,2) - 
                       708*t1*Power(t2,3) + 284*Power(t2,4)) + 
                    Power(s2,2)*
                     (-114*Power(t1,5) - 440*Power(t1,4)*t2 + 
                       499*Power(t1,3)*Power(t2,2) + 
                       870*Power(t1,2)*Power(t2,3) - 
                       727*t1*Power(t2,4) + 62*Power(t2,5)) + 
                    s2*(30*Power(t1,6) + 148*Power(t1,5)*t2 - 
                       253*Power(t1,4)*Power(t2,2) - 
                       260*Power(t1,3)*Power(t2,3) + 
                       503*Power(t1,2)*Power(t2,4) - 
                       146*t1*Power(t2,5) - 28*Power(t2,6))) + 
                 Power(s1,2)*
                  (4*Power(s2,8) + 
                    6*Power(t1,2)*Power(t1 - t2,3)*(2*t1 - t2)*
                     Power(t2,2) + Power(s2,7)*(-22*t1 + 8*t2) + 
                    Power(s2,6)*(60*Power(t1,2) + 74*Power(t2,2)) - 
                    2*Power(s2,5)*
                     (38*Power(t1,3) + 66*Power(t1,2)*t2 + 
                       59*t1*Power(t2,2) - 78*Power(t2,3)) + 
                    Power(s2,4)*
                     (28*Power(t1,4) + 316*Power(t1,3)*t2 + 
                       234*Power(t1,2)*Power(t2,2) - 
                       777*t1*Power(t2,3) + 46*Power(t2,4)) + 
                    2*Power(s2,3)*
                     (9*Power(t1,5) - 156*Power(t1,4)*t2 - 
                       121*Power(t1,3)*Power(t2,2) + 
                       471*Power(t1,2)*Power(t2,3) - 
                       93*t1*Power(t2,4) - 47*Power(t2,5)) + 
                    Power(s2,2)*
                     (-12*Power(t1,6) + 156*Power(t1,5)*t2 - 
                       20*Power(t1,4)*Power(t2,2) - 
                       263*Power(t1,3)*Power(t2,3) + 
                       48*Power(t1,2)*Power(t2,4) + 
                       131*t1*Power(t2,5) - 52*Power(t2,6)) + 
                    2*s2*t2*(-18*Power(t1,6) + 30*Power(t1,5)*t2 - 
                       17*Power(t1,4)*Power(t2,2) + 
                       35*Power(t1,3)*Power(t2,3) - 
                       51*Power(t1,2)*Power(t2,4) + 
                       20*t1*Power(t2,5) + Power(t2,6))) + 
                 s1*s2*t2*(4*Power(s2,7) - 
                    2*Power(s2,6)*(6*t1 - 7*t2) - 
                    2*Power(s2,5)*t2*(26*t1 + t2) + 
                    Power(s2,3)*t2*
                     (-137*Power(t1,3) + 132*Power(t1,2)*t2 + 
                       100*t1*Power(t2,2) - 46*Power(t2,3)) + 
                    2*Power(s2,4)*
                     (10*Power(t1,3) + 52*Power(t1,2)*t2 - 
                       39*t1*Power(t2,2) - 23*Power(t2,3)) + 
                    t1*Power(t1 - t2,2)*t2*
                     (9*Power(t1,3) - 24*Power(t1,2)*t2 + 
                       14*t1*Power(t2,2) - 2*Power(t2,3)) - 
                    Power(s2,2)*
                     (24*Power(t1,5) - 109*Power(t1,4)*t2 + 
                       100*Power(t1,3)*Power(t2,2) + 
                       30*Power(t1,2)*Power(t2,3) - 
                       44*t1*Power(t2,4) + 8*Power(t2,5)) + 
                    s2*(12*Power(t1,6) - 47*Power(t1,5)*t2 + 
                       90*Power(t1,4)*Power(t2,2) - 
                       91*Power(t1,3)*Power(t2,3) + 
                       38*Power(t1,2)*Power(t2,4) - 6*t1*Power(t2,5) + 
                       4*Power(t2,6)))) + 
              s*s1*(2*Power(s2,3)*(s2 - t1)*t1*Power(t2,3)*
                  Power(s2 - t1 + t2,3) - 
                 2*Power(s1,6)*s2*(s2 - t1)*
                  (2*Power(s2,3) + Power(t1,3) - 
                    3*Power(s2,2)*(t1 - 2*t2) + 6*Power(t1,2)*t2 - 
                    6*t1*Power(t2,2) + 2*Power(t2,3) + 
                    6*s2*t2*(-2*t1 + t2)) - 
                 s1*Power(s2,2)*Power(t2,2)*
                  (4*Power(s2,6) - 2*Power(s2,5)*(7*t1 - 6*t2) - 
                    t1*Power(t1 - t2,2)*t2*
                     (9*Power(t1,2) - 8*t1*t2 + 2*Power(t2,2)) + 
                    2*Power(s2,4)*
                     (11*Power(t1,2) - 17*t1*t2 + 7*Power(t2,2)) - 
                    2*s2*Power(t1 - t2,2)*
                     (2*Power(t1,3) - 13*Power(t1,2)*t2 + 
                       6*t1*Power(t2,2) - Power(t2,3)) + 
                    Power(s2,3)*
                     (-22*Power(t1,3) + 48*Power(t1,2)*t2 - 
                       32*t1*Power(t2,2) + 10*Power(t2,3)) + 
                    Power(s2,2)*
                     (14*Power(t1,4) - 51*Power(t1,3)*t2 + 
                       60*Power(t1,2)*Power(t2,2) - 
                       32*t1*Power(t2,3) + 6*Power(t2,4))) - 
                 2*Power(s1,5)*
                  (12*Power(s2,6) + Power(t1,3)*Power(t1 - t2,3) + 
                    Power(s2,5)*(-46*t1 + 30*t2) + 
                    Power(s2,4)*
                     (69*Power(t1,2) - 95*t1*t2 + 18*Power(t2,2)) - 
                    Power(s2,3)*
                     (52*Power(t1,3) - 99*Power(t1,2)*t2 + 
                       9*t1*Power(t2,2) + 6*Power(t2,3)) + 
                    Power(s2,2)*
                     (22*Power(t1,4) - 36*Power(t1,3)*t2 - 
                       39*Power(t1,2)*Power(t2,2) + 
                       35*t1*Power(t2,3) - 6*Power(t2,4)) + 
                    s2*t1*(-6*Power(t1,4) + 5*Power(t1,3)*t2 + 
                       27*Power(t1,2)*Power(t2,2) - 
                       30*t1*Power(t2,3) + 10*Power(t2,4))) + 
                 Power(s1,2)*s2*t2*
                  (12*Power(s2,7) + Power(s2,6)*(-46*t1 + 22*t2) + 
                    Power(s2,5)*
                     (74*Power(t1,2) - 56*t1*t2 + 4*Power(t2,2)) - 
                    Power(t1,2)*Power(t1 - t2,2)*t2*
                     (3*Power(t1,2) - 10*t1*t2 + 4*Power(t2,2)) + 
                    2*Power(s2,4)*t1*
                     (-31*Power(t1,2) + 25*t1*t2 + 5*Power(t2,2)) + 
                    Power(s2,3)*
                     (22*Power(t1,4) - 7*Power(t1,3)*t2 + 
                       14*Power(t1,2)*Power(t2,2) - 
                       64*t1*Power(t2,3) + 16*Power(t2,4)) + 
                    Power(s2,2)*
                     (4*Power(t1,5) - 25*Power(t1,4)*t2 - 
                       28*Power(t1,3)*Power(t2,2) + 
                       116*Power(t1,2)*Power(t2,3) - 
                       68*t1*Power(t2,4) + 10*Power(t2,5)) - 
                    s2*t1*(4*Power(t1,5) - 19*Power(t1,4)*t2 + 
                       16*Power(t1,3)*Power(t2,2) + 
                       17*Power(t1,2)*Power(t2,3) - 
                       32*t1*Power(t2,4) + 14*Power(t2,5))) - 
                 Power(s1,4)*
                  (22*Power(s2,7) - 
                    4*Power(t1,3)*Power(t1 - t2,3)*t2 + 
                    Power(s2,6)*(-86*t1 + 24*t2) + 
                    4*Power(s2,5)*
                     (33*Power(t1,2) - 13*t1*t2 - 11*Power(t2,2)) - 
                    Power(s2,4)*
                     (103*Power(t1,3) + 6*Power(t1,2)*t2 - 
                       196*t1*Power(t2,2) + 56*Power(t2,3)) + 
                    Power(s2,3)*
                     (47*Power(t1,4) + 76*Power(t1,3)*t2 - 
                       240*Power(t1,2)*Power(t2,2) + 
                       28*t1*Power(t2,3) + 6*Power(t2,4)) + 
                    s2*t1*(3*Power(t1,5) + 12*Power(t1,4)*t2 + 
                       5*Power(t1,3)*Power(t2,2) - 
                       90*Power(t1,2)*Power(t2,3) + 
                       96*t1*Power(t2,4) - 32*Power(t2,5)) + 
                    Power(s2,2)*
                     (-15*Power(t1,5) - 50*Power(t1,4)*t2 + 
                       71*Power(t1,3)*Power(t2,2) + 
                       126*Power(t1,2)*Power(t2,3) - 
                       106*t1*Power(t2,4) + 16*Power(t2,5))) + 
                 Power(s1,3)*
                  (-8*Power(s2,8) - 
                    2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2) + 
                    2*Power(s2,7)*(17*t1 + 6*t2) + 
                    Power(s2,6)*
                     (-58*Power(t1,2) - 48*t1*t2 + 58*Power(t2,2)) + 
                    2*Power(s2,5)*
                     (24*Power(t1,3) + 43*Power(t1,2)*t2 - 
                       71*t1*Power(t2,2) + 15*Power(t2,3)) + 
                    Power(s2,4)*
                     (-16*Power(t1,4) - 99*Power(t1,3)*t2 + 
                       78*Power(t1,2)*Power(t2,2) + 
                       84*t1*Power(t2,3) - 18*Power(t2,4)) + 
                    2*s2*t1*t2*
                     (3*Power(t1,5) - 6*Power(t1,4)*t2 + 
                       14*Power(t1,3)*Power(t2,2) - 
                       27*Power(t1,2)*Power(t2,3) + 
                       24*t1*Power(t2,4) - 8*Power(t2,5)) - 
                    2*Power(s2,3)*
                     (Power(t1,5) - 39*Power(t1,4)*t2 - 
                       8*Power(t1,3)*Power(t2,2) + 
                       98*Power(t1,2)*Power(t2,3) - 
                       30*t1*Power(t2,4) + Power(t2,5)) + 
                    Power(s2,2)*
                     (2*Power(t1,6) - 35*Power(t1,5)*t2 + 
                       4*Power(t1,4)*Power(t2,2) + 
                       47*Power(t1,3)*Power(t2,3) + 
                       26*Power(t1,2)*Power(t2,4) - 
                       40*t1*Power(t2,5) + 8*Power(t2,6)))) + 
              Power(s,4)*(Power(s1,4)*
                  (40*Power(s2,5) + Power(s2,3)*t1*(105*t1 - 541*t2) - 
                    10*t1*Power(t1 - t2,3)*(3*t1 - t2) + 
                    Power(s2,4)*(-102*t1 + 80*t2) - 
                    2*Power(s2,2)*
                     (41*Power(t1,3) - 330*Power(t1,2)*t2 + 
                       123*t1*Power(t2,2) + 40*Power(t2,3)) + 
                    s2*(75*Power(t1,4) - 377*Power(t1,3)*t2 + 
                       345*Power(t1,2)*Power(t2,2) - 
                       27*t1*Power(t2,3) - 40*Power(t2,4))) - 
                 s2*t1*t2*(2*Power(s2,6) - 4*Power(s2,5)*(t1 - t2) - 
                    t1*(3*t1 - 4*t2)*Power(t1 - t2,3)*t2 + 
                    Power(s2,4)*
                     (4*Power(t1,2) - 20*t1*t2 + 26*Power(t2,2)) - 
                    2*s2*Power(t1 - t2,2)*
                     (2*Power(t1,3) - 3*Power(t1,2)*t2 + 
                       14*t1*Power(t2,2) - 4*Power(t2,3)) + 
                    Power(s2,3)*
                     (-8*Power(t1,3) + 36*Power(t1,2)*t2 - 
                       52*t1*Power(t2,2) + 8*Power(t2,3)) + 
                    Power(s2,2)*
                     (10*Power(t1,4) - 31*Power(t1,3)*t2 + 
                       57*Power(t1,2)*Power(t2,2) - 
                       58*t1*Power(t2,3) + 22*Power(t2,4))) + 
                 Power(s1,2)*
                  (12*Power(s2,7) + Power(s2,6)*(-50*t1 + 26*t2) - 
                    2*t1*Power(t1 - t2,3)*t2*
                     (20*Power(t1,2) - 6*t1*t2 - 3*Power(t2,2)) + 
                    2*Power(s2,5)*
                     (69*Power(t1,2) - 32*t1*t2 + 67*Power(t2,2)) + 
                    Power(s2,4)*
                     (-210*Power(t1,3) - 270*Power(t1,2)*t2 + 
                       292*t1*Power(t2,2) + 334*Power(t2,3)) + 
                    s2*Power(t1 - t2,2)*
                     (30*Power(t1,4) + 222*Power(t1,3)*t2 - 
                       Power(t1,2)*Power(t2,2) - 135*t1*Power(t2,3) - 
                       32*Power(t2,4)) + 
                    Power(s2,3)*
                     (176*Power(t1,4) + 602*Power(t1,3)*t2 - 
                       675*Power(t1,2)*Power(t2,2) - 
                       707*t1*Power(t2,3) + 278*Power(t2,4)) + 
                    Power(s2,2)*
                     (-96*Power(t1,5) - 416*Power(t1,4)*t2 + 
                       592*Power(t1,3)*Power(t2,2) + 
                       420*Power(t1,2)*Power(t2,3) - 
                       478*t1*Power(t2,4) + 32*Power(t2,5))) + 
                 2*Power(s1,3)*
                  (24*Power(s2,6) + 
                    2*Power(s2,4)*t1*(95*t1 - 112*t2) + 
                    Power(s2,5)*(-84*t1 + 33*t2) + 
                    2*t1*Power(t1 - t2,3)*
                     (10*Power(t1,2) + 5*t1*t2 - 4*Power(t2,2)) + 
                    Power(s2,3)*
                     (-290*Power(t1,3) + 257*Power(t1,2)*t2 + 
                       344*t1*Power(t2,2) + 42*Power(t2,3)) + 
                    6*Power(s2,2)*
                     (45*Power(t1,4) - 39*Power(t1,3)*t2 - 
                       76*Power(t1,2)*Power(t2,2) + 
                       32*t1*Power(t2,3) + 16*Power(t2,4)) + 
                    s2*(-130*Power(t1,5) + 200*Power(t1,4)*t2 + 
                       132*Power(t1,3)*Power(t2,2) - 
                       257*Power(t1,2)*Power(t2,3) + 
                       16*t1*Power(t2,4) + 45*Power(t2,5))) - 
                 2*s1*(Power(s2,7)*(t1 - 6*t2) - 
                    Power(t1,2)*(4*t1 - 3*t2)*Power(t1 - t2,3)*
                     Power(t2,2) + 
                    Power(s2,6)*
                     (-8*Power(t1,2) + 13*t1*t2 - 32*Power(t2,2)) + 
                    Power(s2,5)*
                     (14*Power(t1,3) + 25*Power(t1,2)*t2 + 
                       41*t1*Power(t2,2) - 46*Power(t2,3)) - 
                    Power(s2,4)*
                     (4*Power(t1,4) + 84*Power(t1,3)*t2 + 
                       45*Power(t1,2)*Power(t2,2) - 
                       190*t1*Power(t2,3) + 7*Power(t2,4)) + 
                    Power(s2,3)*
                     (-7*Power(t1,5) + 83*Power(t1,4)*t2 + 
                       27*Power(t1,3)*Power(t2,2) - 
                       176*Power(t1,2)*Power(t2,3) + 
                       28*t1*Power(t2,4) + 25*Power(t2,5)) - 
                    s2*t2*(-12*Power(t1,6) + 18*Power(t1,5)*t2 + 
                       12*Power(t1,4)*Power(t2,2) - 
                       28*Power(t1,3)*Power(t2,3) + 
                       9*Power(t1,2)*Power(t2,4) + Power(t2,6)) + 
                    Power(s2,2)*
                     (4*Power(t1,6) - 43*Power(t1,5)*t2 + 
                       31*Power(t1,4)*Power(t2,2) + 
                       42*Power(t1,3)*Power(t2,3) - 
                       32*Power(t1,2)*Power(t2,4) - 13*t1*Power(t2,5) + 
                       11*Power(t2,6))))))/
          (Power(s - s1,2)*Power(s2 - t1 + t2,3)) + 
         (2*s2*t1*t2*(s - s1 + t2)*
            (Power(s,8)*(4*Power(s1,3) + 5*Power(s1,2)*(t1 - t2) + 
                 4*s1*Power(t1 - t2,2) + Power(t1 - t2,3))*(t1 - t2)*t2 \
- Power(s1,2)*Power(s2,3)*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*
               (s2 - t1)*Power(s1 + t1 - t2,3)*(s1*(s2 - t1) - s2*t2) + 
              Power(s,7)*(-(Power(t1 - t2,3)*t2*
                    (5*s2*t1 - 2*Power(t1,2) - 6*s2*t2 + t1*t2 + 
                      Power(t2,2))) - 
                 2*Power(s1,4)*(3*(t1 - t2)*t2 + s2*(t1 + t2)) + 
                 s1*Power(t1 - t2,3)*
                  (7*t2*(t1 + t2) - 2*s2*(t1 + 12*t2)) + 
                 Power(s1,3)*
                  (t2*(5*Power(t1,2) + 8*t1*t2 - 13*Power(t2,2)) + 
                    s2*(-6*Power(t1,2) - 28*t1*t2 + 24*Power(t2,2))) + 
                 Power(s1,2)*(t1 - t2)*
                  (t2*(6*Power(t1,2) + 7*t1*t2 - 13*Power(t2,2)) + 
                    s2*(-6*Power(t1,2) - 31*t1*t2 + 32*Power(t2,2)))) + 
              Power(s,6)*(Power(s1,5)*
                  (-2*Power(s2,2) + (t1 - 3*t2)*t2 + 2*s2*(t1 + t2)) + 
                 Power(t1 - t2,3)*t2*
                  (5*Power(s2,2)*(2*t1 - 3*t2) + 
                    t1*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
                    s2*(-8*Power(t1,2) + 5*t1*t2 + 6*Power(t2,2))) + 
                 Power(s1,4)*
                  (Power(s2,2)*(4*t1 + 15*t2) + 
                    t2*(-21*Power(t1,2) + 10*t1*t2 + 8*Power(t2,2)) - 
                    3*s2*(Power(t1,2) - 9*t1*t2 + 12*Power(t2,2))) + 
                 Power(s1,3)*
                  (Power(s2,2)*
                     (24*Power(t1,2) + 95*t1*t2 - 66*Power(t2,2)) - 
                    s2*(21*Power(t1,3) + 44*Power(t1,2)*t2 + 
                       40*t1*Power(t2,2) - 78*Power(t2,3)) - 
                    t2*(11*Power(t1,3) - 37*Power(t1,2)*t2 + 
                       17*t1*Power(t2,2) + 9*Power(t2,3))) + 
                 Power(s1,2)*
                  (-2*Power(t1 - t2,2)*t2*
                     (5*Power(t1,2) - 12*t1*t2 - 3*Power(t2,2)) + 
                    Power(s2,2)*
                     (28*Power(t1,3) + 59*Power(t1,2)*t2 - 
                       183*t1*Power(t2,2) + 86*Power(t2,3)) + 
                    s2*(-25*Power(t1,4) - 17*Power(t1,3)*t2 + 
                       30*Power(t1,2)*Power(t2,2) + 
                       90*t1*Power(t2,3) - 78*Power(t2,4))) + 
                 s1*(t1 - t2)*
                  (2*Power(t1 - t2,2)*Power(t2,2)*(7*t1 + t2) + 
                    Power(s2,2)*
                     (10*Power(t1,3) + 41*Power(t1,2)*t2 - 
                       115*t1*Power(t2,2) + 60*Power(t2,3)) + 
                    s2*(-9*Power(t1,4) - 25*Power(t1,3)*t2 + 
                       42*Power(t1,2)*Power(t2,2) + 
                       32*t1*Power(t2,3) - 40*Power(t2,4)))) + 
              s*s2*(2*Power(s1,8)*s2*
                  (2*Power(s2,2) - 3*s2*t1 + Power(t1,2)) - 
                 Power(s2,3)*Power(s2 - t1,2)*Power(t1 - t2,3)*
                  Power(t2,3) + 
                 Power(s1,7)*
                  (11*Power(s2,4) + s2*Power(t1,2)*(6*t1 - 7*t2) - 
                    Power(t1,3)*t2 - Power(s2,3)*(6*t1 + 17*t2) + 
                    Power(s2,2)*t1*(-11*t1 + 23*t2)) + 
                 Power(s1,6)*
                  (12*Power(s2,5) + Power(s2,4)*(12*t1 - 43*t2) + 
                    Power(t1,3)*t2*(-3*t1 + 4*t2) + 
                    Power(s2,2)*t1*
                     (-2*Power(t1,2) + 28*t1*t2 - 37*Power(t2,2)) + 
                    s2*Power(t1,2)*
                     (7*Power(t1,2) - 17*t1*t2 + 11*Power(t2,2)) + 
                    Power(s2,3)*
                     (-29*Power(t1,2) + 24*t1*t2 + 29*Power(t2,2))) + 
                 Power(s1,5)*
                  (2*Power(s2,6) + Power(s2,5)*(30*t1 - 47*t2) - 
                    Power(s2,2)*t1*t2*
                     (Power(t1,2) + 28*t1*t2 - 33*Power(t2,2)) - 
                    3*Power(t1,3)*t2*
                     (Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
                    Power(s2,4)*
                     (-22*Power(t1,2) - 22*t1*t2 + 65*Power(t2,2)) + 
                    s2*Power(t1,2)*
                     (5*Power(t1,3) - 17*Power(t1,2)*t2 + 
                       25*t1*Power(t2,2) - 12*Power(t2,3)) - 
                    5*Power(s2,3)*
                     (3*Power(t1,3) - 13*Power(t1,2)*t2 + 
                       8*t1*Power(t2,2) + 5*Power(t2,3))) + 
                 Power(s1,4)*
                  (6*Power(s2,6)*(t1 - t2) - 
                    Power(t1,3)*(t1 - 4*t2)*Power(t1 - t2,2)*t2 + 
                    Power(s2,5)*
                     (18*Power(t1,2) - 91*t1*t2 + 71*Power(t2,2)) - 
                    Power(s2,4)*
                     (28*Power(t1,3) - 92*Power(t1,2)*t2 + 
                       11*t1*Power(t2,2) + 49*Power(t2,3)) + 
                    Power(s2,2)*t1*
                     (-8*Power(t1,4) + 31*Power(t1,3)*t2 - 
                       15*Power(t1,2)*Power(t2,2) + 
                       15*t1*Power(t2,3) - 17*Power(t2,4)) + 
                    s2*Power(t1,2)*
                     (3*Power(t1,4) - 16*Power(t1,3)*t2 + 
                       32*Power(t1,2)*Power(t2,2) - 
                       30*t1*Power(t2,3) + 10*Power(t2,4)) + 
                    Power(s2,3)*
                     (9*Power(t1,4) - 38*Power(t1,3)*t2 - 
                       27*Power(t1,2)*Power(t2,2) + 
                       40*t1*Power(t2,3) + 11*Power(t2,4))) + 
                 Power(s1,2)*s2*
                  (-(Power(t1,2)*Power(t1 - t2,2)*t2*
                       (Power(t1,3) - 4*Power(t1,2)*t2 + 
                        5*t1*Power(t2,2) - Power(t2,3))) + 
                    2*Power(s2,5)*
                     (Power(t1,3) - 6*Power(t1,2)*t2 + 
                       9*t1*Power(t2,2) - Power(t2,3)) + 
                    s2*Power(t1,2)*t2*
                     (7*Power(t1,4) - 16*Power(t1,3)*t2 + 
                       8*Power(t1,2)*Power(t2,2) + 
                       6*t1*Power(t2,3) - 5*Power(t2,4)) + 
                    Power(s2,4)*
                     (-6*Power(t1,4) + 35*Power(t1,3)*t2 - 
                       39*Power(t1,2)*Power(t2,2) - 
                       26*t1*Power(t2,3) + 20*Power(t2,4)) - 
                    Power(s2,2)*t1*
                     (4*Power(t1,5) - 20*Power(t1,4)*t2 + 
                       13*Power(t1,3)*Power(t2,2) + 
                       17*Power(t1,2)*Power(t2,3) + 
                       6*t1*Power(t2,4) - 16*Power(t2,5)) + 
                    Power(s2,3)*
                     (8*Power(t1,5) - 53*Power(t1,4)*t2 + 
                       63*Power(t1,3)*Power(t2,2) + 
                       23*Power(t1,2)*Power(t2,3) - 
                       19*t1*Power(t2,4) - 8*Power(t2,5))) + 
                 s1*Power(s2,2)*t2*
                  (Power(t1,2)*Power(t1 - t2,2)*t2*
                     (2*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) - 
                    2*Power(s2,4)*t1*
                     (Power(t1,2) - 3*t1*t2 + 3*Power(t2,2)) + 
                    Power(s2,3)*
                     (10*Power(t1,4) - 30*Power(t1,3)*t2 + 
                       27*Power(t1,2)*Power(t2,2) + 
                       2*t1*Power(t2,3) - 5*Power(t2,4)) + 
                    s2*t1*(8*Power(t1,5) - 24*Power(t1,4)*t2 + 
                       27*Power(t1,3)*Power(t2,2) - 
                       16*Power(t1,2)*Power(t2,3) + 
                       9*t1*Power(t2,4) - 4*Power(t2,5)) + 
                    Power(s2,2)*
                     (-16*Power(t1,5) + 48*Power(t1,4)*t2 - 
                       45*Power(t1,3)*Power(t2,2) + 
                       6*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) + 
                       2*Power(t2,5))) + 
                 Power(s1,3)*
                  (Power(t1,3)*Power(t1 - t2,3)*Power(t2,2) + 
                    6*Power(s2,6)*
                     (Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
                    s2*Power(t1,2)*Power(t1 - t2,2)*
                     (Power(t1,3) - 6*Power(t1,2)*t2 + 
                       13*t1*Power(t2,2) - 5*Power(t2,3)) - 
                    Power(s2,5)*
                     (6*Power(t1,3) + 9*Power(t1,2)*t2 - 
                       88*t1*Power(t2,2) + 52*Power(t2,3)) + 
                    Power(s2,4)*
                     (3*Power(t1,4) + 14*Power(t1,3)*t2 - 
                       105*Power(t1,2)*Power(t2,2) + 
                       39*t1*Power(t2,3) + 22*Power(t2,4)) + 
                    Power(s2,3)*
                     (Power(t1,5) - 30*Power(t1,4)*t2 + 
                       89*Power(t1,3)*Power(t2,2) - 
                       13*Power(t1,2)*Power(t2,3) - 
                       30*t1*Power(t2,4) - 2*Power(t2,5)) + 
                    Power(s2,2)*t1*
                     (-5*Power(t1,5) + 34*Power(t1,4)*t2 - 
                       56*Power(t1,3)*Power(t2,2) + 
                       21*Power(t1,2)*Power(t2,3) - t1*Power(t2,4) + 
                       4*Power(t2,5)))) + 
              Power(s,5)*(Power(s1,6)*
                  (6*Power(s2,2) + 4*s2*t1 - s2*t2 + Power(t2,2)) + 
                 Power(t1 - t2,3)*t2*
                  (-10*Power(s2,3)*(t1 - 2*t2) + 
                    Power(t1,2)*(t1 - t2)*t2 + 
                    Power(s2,2)*
                     (12*Power(t1,2) - 10*t1*t2 - 15*Power(t2,2)) + 
                    s2*t1*(-3*Power(t1,2) - 4*t1*t2 + 10*Power(t2,2))) \
+ Power(s1,5)*(10*Power(s2,3) + Power(s2,2)*(3*t1 - 29*t2) + 
                    t2*(8*Power(t1,2) - 6*t1*t2 - 3*Power(t2,2)) + 
                    s2*(33*Power(t1,2) - 6*t1*t2 + 17*Power(t2,2))) + 
                 Power(s1,4)*
                  (2*Power(s2,3)*(5*t1 - 23*t2) + 
                    Power(s2,2)*
                     (6*Power(t1,2) - 78*t1*t2 + 113*Power(t2,2)) + 
                    s2*(69*Power(t1,3) + 40*Power(t1,2)*t2 - 
                       65*t1*Power(t2,2) - 43*Power(t2,3)) + 
                    t2*(-10*Power(t1,3) - 16*Power(t1,2)*t2 + 
                       16*t1*Power(t2,2) + 3*Power(t2,3))) - 
                 Power(s1,3)*
                  (2*Power(s2,3)*
                     (15*Power(t1,2) + 99*t1*t2 - 55*Power(t2,2)) - 
                    3*Power(s2,2)*
                     (20*Power(t1,3) + 56*Power(t1,2)*t2 + 
                       33*t1*Power(t2,2) - 68*Power(t2,3)) + 
                    s2*(-49*Power(t1,4) + 97*Power(t1,3)*t2 + 
                       143*Power(t1,2)*Power(t2,2) - 
                       115*t1*Power(t2,3) - 49*Power(t2,4)) + 
                    t2*(3*Power(t1,4) + 4*Power(t1,3)*t2 - 
                       26*Power(t1,2)*Power(t2,2) + 
                       18*t1*Power(t2,3) + Power(t2,4))) + 
                 s1*(-(t1*Power(t1 - t2,3)*t2*
                       (3*Power(t1,2) - 5*t1*t2 - 4*Power(t2,2))) - 
                    s2*Power(t1 - t2,2)*
                     (6*Power(t1,4) + 17*Power(t1,3)*t2 + 
                       33*Power(t1,2)*Power(t2,2) - 
                       55*t1*Power(t2,3) - 10*Power(t2,4)) + 
                    Power(s2,3)*
                     (-20*Power(t1,4) - 26*Power(t1,3)*t2 + 
                       210*Power(t1,2)*Power(t2,2) - 
                       246*t1*Power(t2,3) + 80*Power(t2,4)) + 
                    Power(s2,2)*
                     (33*Power(t1,5) + 11*Power(t1,4)*t2 - 
                       167*Power(t1,3)*Power(t2,2) + 
                       68*Power(t1,2)*Power(t2,3) + 
                       150*t1*Power(t2,4) - 95*Power(t2,5))) - 
                 Power(s1,2)*
                  (2*t1*Power(t1 - t2,2)*t2*
                     (4*Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
                    2*Power(s2,3)*
                     (25*Power(t1,3) + 49*Power(t1,2)*t2 - 
                       160*t1*Power(t2,2) + 63*Power(t2,3)) + 
                    Power(s2,2)*
                     (-84*Power(t1,4) - 66*Power(t1,3)*t2 + 
                       205*Power(t1,2)*Power(t2,2) + 
                       163*t1*Power(t2,3) - 194*Power(t2,4)) + 
                    s2*(-3*Power(t1,5) + 44*Power(t1,4)*t2 + 
                       43*Power(t1,3)*Power(t2,2) - 
                       189*Power(t1,2)*Power(t2,3) + 
                       73*t1*Power(t2,4) + 32*Power(t2,5)))) - 
              Power(s,4)*(Power(s1,7)*(s2 + t1)*(6*s2 - t2) + 
                 Power(s1,6)*
                  (24*Power(s2,3) + Power(s2,2)*(13*t1 - 25*t2) + 
                    2*t1*t2*(-t1 + t2) + 
                    s2*(32*Power(t1,2) - 13*t1*t2 + 8*Power(t2,2))) - 
                 s2*Power(t1 - t2,3)*t2*
                  (5*Power(s2,3)*(t1 - 3*t2) + 
                    Power(t1,2)*t2*(-3*t1 + 4*t2) + 
                    s2*t1*(3*Power(t1,2) + 6*t1*t2 - 
                       20*Power(t2,2)) + 
                    Power(s2,2)*
                     (-8*Power(t1,2) + 10*t1*t2 + 20*Power(t2,2))) + 
                 Power(s1,5)*
                  (20*Power(s2,4) + Power(s2,3)*(33*t1 - 98*t2) + 
                    2*Power(t1,2)*t2*(-5*t1 + 4*t2) + 
                    Power(s2,2)*
                     (67*Power(t1,2) - 4*t1*t2 + 65*Power(t2,2)) + 
                    s2*(46*Power(t1,3) - 58*Power(t1,2)*t2 - 
                       19*t1*Power(t2,2) - 18*Power(t2,3))) + 
                 Power(s1,4)*
                  (Power(s2,4)*(40*t1 - 74*t2) + 
                    Power(s2,3)*t2*(-169*t1 + 218*t2) + 
                    Power(s2,2)*
                     (167*Power(t1,3) + 99*Power(t1,2)*t2 - 
                       167*t1*Power(t2,2) - 107*Power(t2,3)) - 
                    t1*t2*(6*Power(t1,3) - 24*Power(t1,2)*t2 + 
                       11*t1*Power(t2,2) + 2*Power(t2,3)) + 
                    s2*(7*Power(t1,4) - 173*Power(t1,3)*t2 + 
                       55*Power(t1,2)*Power(t2,2) + 
                       73*t1*Power(t2,3) + 16*Power(t2,4))) + 
                 Power(s1,3)*
                  (-(t1*Power(t1 - t2,3)*t2*(11*t1 + t2)) + 
                    2*Power(s2,4)*t2*(-131*t1 + 60*t2) + 
                    Power(s2,3)*
                     (42*Power(t1,3) + 317*Power(t1,2)*t2 + 
                       171*t1*Power(t2,2) - 302*Power(t2,3)) + 
                    Power(s2,2)*
                     (139*Power(t1,4) - 328*Power(t1,3)*t2 - 
                       303*Power(t1,2)*Power(t2,2) + 
                       278*t1*Power(t2,3) + 109*Power(t2,4)) - 
                    s2*(25*Power(t1,5) + 35*Power(t1,4)*t2 - 
                       163*Power(t1,3)*Power(t2,2) + 
                       2*Power(t1,2)*Power(t2,3) + 
                       83*t1*Power(t2,4) + 5*Power(t2,5))) - 
                 Power(s1,2)*
                  (Power(t1,2)*Power(t1 - t2,2)*t2*
                     (4*Power(t1,2) - 14*t1*t2 + 5*Power(t2,2)) + 
                    Power(s2,4)*
                     (40*Power(t1,3) + 127*Power(t1,2)*t2 - 
                       360*t1*Power(t2,2) + 109*Power(t2,3)) + 
                    Power(s2,3)*
                     (-96*Power(t1,4) - 197*Power(t1,3)*t2 + 
                       482*Power(t1,2)*Power(t2,2) + 
                       157*t1*Power(t2,3) - 258*Power(t2,4)) + 
                    s2*t1*(11*Power(t1,5) - 12*Power(t1,4)*t2 - 
                       39*Power(t1,3)*Power(t2,2) + 
                       31*Power(t1,2)*Power(t2,3) + 
                       61*t1*Power(t2,4) - 52*Power(t2,5)) + 
                    Power(s2,2)*
                     (-16*Power(t1,5) + 239*Power(t1,4)*t2 - 
                       201*Power(t1,3)*Power(t2,2) - 
                       268*Power(t1,2)*Power(t2,3) + 
                       160*t1*Power(t2,4) + 68*Power(t2,5))) + 
                 s1*(2*Power(t1,2)*Power(t1 - t2,4)*Power(t2,2) - 
                    2*Power(s2,4)*
                     (10*Power(t1,4) + 7*Power(t1,3)*t2 - 
                       90*Power(t1,2)*Power(t2,2) + 
                       107*t1*Power(t2,3) - 30*Power(t2,4)) - 
                    Power(s2,2)*Power(t1 - t2,2)*
                     (16*Power(t1,4) + 68*Power(t1,3)*t2 - 
                       3*Power(t1,2)*Power(t2,2) - 
                       100*t1*Power(t2,3) - 20*Power(t2,4)) + 
                    Power(s2,3)*
                     (45*Power(t1,5) + 17*Power(t1,4)*t2 - 
                       269*Power(t1,3)*Power(t2,2) + 
                       171*Power(t1,2)*Power(t2,3) + 
                       160*t1*Power(t2,4) - 120*Power(t2,5)) + 
                    s2*t1*(Power(t1,6) + 9*Power(t1,4)*Power(t2,2) - 
                       24*Power(t1,3)*Power(t2,3) + 
                       30*t1*Power(t2,5) - 16*Power(t2,6)))) + 
              Power(s,2)*(Power(s1,8)*s2*
                  (-5*Power(s2,2) + 2*s2*t1 + Power(t1,2)) - 
                 Power(s2,3)*(s2 - t1)*Power(t1 - t2,3)*Power(t2,2)*
                  (Power(s2,2) - t1*(t1 - 4*t2) - 6*s2*t2) + 
                 Power(s1,7)*
                  (-22*Power(s2,4) + s2*Power(t1,2)*(5*t1 - 3*t2) + 
                    Power(t1,3)*t2 - Power(s2,2)*t1*(2*t1 + 11*t2) + 
                    Power(s2,3)*(7*t1 + 23*t2)) + 
                 Power(s1,6)*
                  (-30*Power(s2,5) + s2*Power(t1,3)*(8*t1 - 11*t2) + 
                    Power(t1,3)*(3*t1 - 4*t2)*t2 + 
                    Power(s2,4)*(-23*t1 + 85*t2) + 
                    Power(s2,3)*
                     (17*Power(t1,2) - 32*t1*t2 - 46*Power(t2,2)) + 
                    Power(s2,2)*t1*
                     (-5*Power(t1,2) + 11*t1*t2 + 30*Power(t2,2))) + 
                 Power(s1,5)*
                  (-10*Power(s2,6) + Power(s2,5)*(-63*t1 + 116*t2) + 
                    Power(s2,4)*
                     (25*Power(t1,2) + 27*t1*t2 - 134*Power(t2,2)) + 
                    3*Power(t1,3)*t2*
                     (Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
                    Power(s2,2)*t1*
                     (15*Power(t1,3) + 35*Power(t1,2)*t2 - 
                       21*t1*Power(t2,2) - 44*Power(t2,3)) + 
                    2*s2*Power(t1,2)*
                     (2*Power(t1,3) - 6*Power(t1,2)*t2 - 
                       2*t1*Power(t2,2) + 5*Power(t2,3)) + 
                    Power(s2,3)*
                     (-22*Power(t1,3) - 26*Power(t1,2)*t2 + 
                       72*t1*Power(t2,2) + 50*Power(t2,3))) - 
                 Power(s1,4)*
                  (Power(s2,6)*(28*t1 - 31*t2) - 
                    Power(t1,3)*(t1 - 4*t2)*Power(t1 - t2,2)*t2 + 
                    Power(s2,5)*
                     (15*Power(t1,2) - 198*t1*t2 + 182*Power(t2,2)) + 
                    Power(s2,4)*
                     (3*Power(t1,3) + 212*Power(t1,2)*t2 - 
                       89*t1*Power(t2,2) - 119*Power(t2,3)) + 
                    Power(s2,2)*t1*
                     (-25*Power(t1,4) + 69*Power(t1,3)*t2 + 
                       24*Power(t1,2)*Power(t2,2) - 
                       20*t1*Power(t2,3) - 32*Power(t2,4)) + 
                    s2*Power(t1,2)*
                     (Power(t1,4) - 6*Power(t1,3)*t2 + 
                       22*Power(t1,2)*Power(t2,2) - 
                       34*t1*Power(t2,3) + 15*Power(t2,4)) + 
                    Power(s2,3)*
                     (23*Power(t1,4) - 241*Power(t1,3)*t2 + 
                       66*Power(t1,2)*Power(t2,2) + 
                       104*t1*Power(t2,3) + 29*Power(t2,4))) + 
                 Power(s1,3)*
                  (-(Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)) + 
                    Power(s2,6)*
                     (-24*Power(t1,2) + 95*t1*t2 - 34*Power(t2,2)) - 
                    s2*Power(t1,2)*Power(t1 - t2,2)*
                     (Power(t1,3) - 5*Power(t1,2)*t2 + 
                       19*t1*Power(t2,2) - 9*Power(t2,3)) + 
                    3*Power(s2,5)*
                     (11*Power(t1,3) - 37*Power(t1,2)*t2 - 
                       61*t1*Power(t2,2) + 52*Power(t2,3)) + 
                    Power(s2,4)*
                     (-59*Power(t1,4) + 121*Power(t1,3)*t2 + 
                       284*Power(t1,2)*Power(t2,2) - 
                       165*t1*Power(t2,3) - 76*Power(t2,4)) + 
                    Power(s2,2)*t1*
                     (7*Power(t1,5) - 61*Power(t1,4)*t2 + 
                       89*Power(t1,3)*Power(t2,2) - 
                       3*Power(t1,2)*Power(t2,3) - 
                       17*t1*Power(t2,4) - 9*Power(t2,5)) + 
                    Power(s2,3)*
                     (23*Power(t1,5) + 92*Power(t1,4)*t2 - 
                       358*Power(t1,3)*Power(t2,2) + 
                       92*Power(t1,2)*Power(t2,3) + 
                       101*t1*Power(t2,4) + 7*Power(t2,5))) + 
                 Power(s1,2)*s2*
                  (Power(t1,2)*Power(t1 - t2,2)*t2*
                     (Power(t1,3) - 6*Power(t1,2)*t2 + 
                       9*t1*Power(t2,2) - 2*Power(t2,3)) + 
                    Power(s2,5)*
                     (-4*Power(t1,3) + 57*Power(t1,2)*t2 - 
                       103*t1*Power(t2,2) + 16*Power(t2,3)) + 
                    Power(s2,4)*
                     (9*Power(t1,4) - 168*Power(t1,3)*t2 + 
                       262*Power(t1,2)*Power(t2,2) + 
                       55*t1*Power(t2,3) - 86*Power(t2,4)) + 
                    Power(s2,2)*t1*
                     (19*Power(t1,5) - 65*Power(t1,4)*t2 + 
                       9*Power(t1,3)*Power(t2,2) + 
                       84*Power(t1,2)*Power(t2,3) + 
                       21*t1*Power(t2,4) - 60*Power(t2,5)) + 
                    s2*Power(t1,2)*
                     (-2*Power(t1,5) - 8*Power(t1,4)*t2 + 
                       19*Power(t1,3)*Power(t2,2) + 
                       5*Power(t1,2)*Power(t2,3) - 
                       29*t1*Power(t2,4) + 15*Power(t2,5)) + 
                    Power(s2,3)*
                     (-26*Power(t1,5) + 233*Power(t1,4)*t2 - 
                       299*Power(t1,3)*Power(t2,2) - 
                       74*Power(t1,2)*Power(t2,3) + 
                       82*t1*Power(t2,4) + 38*Power(t2,5))) + 
                 s1*Power(s2,2)*
                  (Power(t1,2)*Power(t1 - t2,2)*t2*
                     (Power(t1,3) - 7*Power(t1,2)*t2 + 
                       14*t1*Power(t2,2) - 6*Power(t2,3)) + 
                    Power(s2,4)*
                     (2*Power(t1,4) + 7*Power(t1,3)*t2 - 
                       36*Power(t1,2)*Power(t2,2) + 
                       39*t1*Power(t2,3) - 4*Power(t2,4)) - 
                    Power(s2,3)*
                     (6*Power(t1,5) + 35*Power(t1,4)*t2 - 
                       140*Power(t1,3)*Power(t2,2) + 
                       119*Power(t1,2)*Power(t2,3) + 
                       24*t1*Power(t2,4) - 32*Power(t2,5)) + 
                    s2*t1*t2*
                     (-25*Power(t1,5) + 70*Power(t1,4)*t2 - 
                       71*Power(t1,3)*Power(t2,2) + 
                       44*Power(t1,2)*Power(t2,3) - 
                       34*t1*Power(t2,4) + 16*Power(t2,5)) + 
                    Power(s2,2)*
                     (4*Power(t1,6) + 58*Power(t1,5)*t2 - 
                       187*Power(t1,4)*Power(t2,2) + 
                       152*Power(t1,3)*Power(t2,3) + 
                       7*Power(t1,2)*Power(t2,4) - 20*t1*Power(t2,5) - 
                       10*Power(t2,6)))) + 
              Power(s,3)*(2*Power(s1,8)*s2*(s2 + t1) + 
                 Power(s1,7)*
                  (19*Power(s2,3) + Power(s2,2)*(4*t1 - 11*t2) + 
                    s2*t1*(7*t1 - 6*t2) + Power(t1,2)*t2) - 
                 Power(s2,2)*Power(t1 - t2,3)*t2*
                  (Power(s2,3)*(t1 - 6*t2) - 
                    3*Power(t1,2)*(t1 - 2*t2)*t2 + 
                    s2*t1*(Power(t1,2) + 4*t1*t2 - 20*Power(t2,2)) + 
                    Power(s2,2)*
                     (-2*Power(t1,2) + 5*t1*t2 + 15*Power(t2,2))) + 
                 Power(s1,6)*
                  (38*Power(s2,4) + Power(s2,3)*(22*t1 - 74*t2) - 
                    2*Power(t1,2)*Power(t2,2) - 
                    s2*Power(t1,2)*(t1 + 26*t2) + 
                    Power(s2,2)*(37*Power(t1,2) + 30*Power(t2,2))) + 
                 Power(s1,5)*
                  (20*Power(s2,5) + Power(s2,4)*(67*t1 - 148*t2) - 
                    Power(t1,2)*t2*
                     (4*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
                    Power(s2,3)*
                     (25*Power(t1,2) - 8*t1*t2 + 132*Power(t2,2)) + 
                    Power(s2,2)*
                     (78*Power(t1,3) - 87*Power(t1,2)*t2 - 
                       54*t1*Power(t2,2) - 44*Power(t2,3)) + 
                    s2*t1*(-23*Power(t1,3) - 29*Power(t1,2)*t2 + 
                       45*t1*Power(t2,2) + 16*Power(t2,3))) + 
                 Power(s1,4)*
                  (Power(s2,5)*(50*t1 - 66*t2) - 
                    3*Power(s2,4)*(79*t1 - 86*t2)*t2 + 
                    Power(s2,3)*
                     (119*Power(t1,3) + 218*Power(t1,2)*t2 - 
                       190*t1*Power(t2,2) - 150*Power(t2,3)) + 
                    Power(t1,2)*t2*
                     (-8*Power(t1,3) + 20*Power(t1,2)*t2 - 
                       18*t1*Power(t2,2) + 5*Power(t2,3)) + 
                    s2*t1*(-23*Power(t1,4) + 45*Power(t1,3)*t2 + 
                       54*Power(t1,2)*Power(t2,2) - 
                       43*t1*Power(t2,3) - 18*Power(t2,4)) + 
                    Power(s2,2)*
                     (23*Power(t1,4) - 361*Power(t1,3)*t2 + 
                       149*Power(t1,2)*Power(t2,2) + 
                       128*t1*Power(t2,3) + 32*Power(t2,4))) + 
                 Power(s1,3)*
                  (-(Power(t1,2)*Power(t1 - t2,2)*t2*
                       (3*Power(t1,2) - 10*t1*t2 + 4*Power(t2,2))) + 
                    Power(s2,5)*
                     (30*Power(t1,2) - 212*t1*t2 + 84*Power(t2,2)) + 
                    Power(s2,4)*
                     (-22*Power(t1,3) + 296*Power(t1,2)*t2 + 
                       217*t1*Power(t2,2) - 275*Power(t2,3)) + 
                    Power(s2,3)*
                     (142*Power(t1,4) - 339*Power(t1,3)*t2 - 
                       394*Power(t1,2)*Power(t2,2) + 
                       310*t1*Power(t2,3) + 125*Power(t2,4)) + 
                    s2*t1*(-4*Power(t1,5) + 22*Power(t1,4)*t2 - 
                       7*Power(t1,3)*Power(t2,2) - 
                       46*Power(t1,2)*Power(t2,3) + 
                       26*t1*Power(t2,4) + 6*Power(t2,5)) - 
                    Power(s2,2)*
                     (47*Power(t1,5) + 98*Power(t1,4)*t2 - 
                       436*Power(t1,3)*Power(t2,2) + 
                       103*Power(t1,2)*Power(t2,3) + 
                       138*t1*Power(t2,4) + 9*Power(t2,5))) + 
                 Power(s1,2)*
                  (Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                     (2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
                    Power(s2,5)*
                     (10*Power(t1,3) + 113*Power(t1,2)*t2 - 
                       255*t1*Power(t2,2) + 56*Power(t2,3)) + 
                    Power(s2,4)*
                     (34*Power(t1,4) + 273*Power(t1,3)*t2 - 
                       525*Power(t1,2)*Power(t2,2) - 
                       99*t1*Power(t2,3) + 197*Power(t2,4)) + 
                    Power(s2,3)*
                     (31*Power(t1,5) - 363*Power(t1,4)*t2 + 
                       430*Power(t1,3)*Power(t2,2) + 
                       188*Power(t1,2)*Power(t2,3) - 
                       164*t1*Power(t2,4) - 72*Power(t2,5)) + 
                    s2*Power(t1,2)*
                     (2*Power(t1,5) - 3*Power(t1,4)*t2 + 
                       19*Power(t1,3)*Power(t2,2) - 
                       50*Power(t1,2)*Power(t2,3) + 
                       47*t1*Power(t2,4) - 15*Power(t2,5)) + 
                    Power(s2,2)*t1*
                     (-26*Power(t1,5) + 65*Power(t1,4)*t2 + 
                       29*Power(t1,3)*Power(t2,2) - 
                       106*Power(t1,2)*Power(t2,3) - 
                       50*t1*Power(t2,4) + 84*Power(t2,5))) + 
                 s1*s2*(-(Power(t1,2)*Power(t1 - t2,2)*t2*
                       (Power(t1,3) - 7*Power(t1,2)*t2 + 
                        13*t1*Power(t2,2) - 6*Power(t2,3))) - 
                    2*Power(s2,4)*
                     (5*Power(t1,4) + 5*Power(t1,3)*t2 - 
                       51*Power(t1,2)*Power(t2,2) + 
                       59*t1*Power(t2,3) - 12*Power(t2,4)) + 
                    Power(s2,3)*
                     (27*Power(t1,5) + 40*Power(t1,4)*t2 - 
                       265*Power(t1,3)*Power(t2,2) + 
                       205*Power(t1,2)*Power(t2,3) + 
                       90*t1*Power(t2,4) - 85*Power(t2,5)) + 
                    s2*t1*(Power(t1,6) + 20*Power(t1,5)*t2 - 
                       51*Power(t1,4)*Power(t2,2) + 
                       40*Power(t1,3)*Power(t2,3) - 
                       34*Power(t1,2)*Power(t2,4) + 
                       48*t1*Power(t2,5) - 24*Power(t2,6)) + 
                    Power(s2,2)*
                     (-14*Power(t1,6) - 73*Power(t1,5)*t2 + 
                       253*Power(t1,4)*Power(t2,2) - 
                       145*Power(t1,3)*Power(t2,3) - 
                       93*Power(t1,2)*Power(t2,4) + 50*t1*Power(t2,5) + 
                       20*Power(t2,6))))))/
          (Power(s - s2,2)*Power(s1 + t1 - t2,3)) - 
         (s1*t1*(s - s2 + t1)*(s - s1 + t2)*
            (2*Power(s1,3)*Power(s2,2)*
               (2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*
               Power(s1 + t1 - t2,3)*t2*(s1*(s2 - t1) - s2*t2) + 
              2*Power(s,7)*s2*(t1 - t2)*t2*
               (2*Power(s1,3) + 6*Power(s1,2)*(t1 - t2) + 
                 6*s1*Power(t1 - t2,2) + Power(t1 - t2,2)*(2*t1 + t2)) - 
              2*Power(s,6)*(2*Power(s1,4)*s2*
                  (Power(t1,2) + 6*t1*t2 - 2*Power(t2,2) + 
                    s2*(t1 + 2*t2)) - 
                 Power(t1 - t2,3)*
                  (t1*(t1 - t2)*Power(t2,2) + 2*s2*t1*t2*(t1 + 2*t2) + 
                    Power(s2,2)*(Power(t1,2) - 5*t1*t2 - 3*Power(t2,2))\
) + 3*Power(s1,2)*(t1 - t2)*(t1*Power(t2,2)*(-t1 + t2) + 
                    Power(s2,2)*
                     (Power(t1,2) + 7*t1*t2 - 10*Power(t2,2)) + 
                    2*s2*(Power(t1,3) + 4*Power(t1,2)*t2 - 
                       7*t1*Power(t2,2) + 2*Power(t2,3))) + 
                 Power(s1,3)*
                  (t1*Power(t2,2)*(-t1 + t2) + 
                    Power(s2,2)*
                     (5*Power(t1,2) + 11*t1*t2 - 18*Power(t2,2)) + 
                    2*s2*(3*Power(t1,3) + 14*Power(t1,2)*t2 - 
                       23*t1*Power(t2,2) + 6*Power(t2,3))) - 
                 s1*(t1 - t2)*
                  (3*t1*Power(t1 - t2,2)*Power(t2,2) + 
                    Power(s2,2)*
                     (Power(t1,3) - 18*Power(t1,2)*t2 + 
                       42*t1*Power(t2,2) - 16*Power(t2,3)) - 
                    2*s2*(Power(t1,4) + Power(t1,3)*t2 - 
                       Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) + 
                       Power(t2,4)))) + 
              Power(s,5)*(4*Power(s1,5)*s2*
                  (-Power(s2,2) + 3*s2*t1 + 5*Power(t1,2) + 5*s2*t2 + 
                    15*t1*t2 + Power(t2,2)) + 
                 Power(t1 - t2,3)*
                  (2*Power(t1,2)*(t1 - t2)*Power(t2,2) + 
                    s2*t1*Power(t2,2)*(t1 + 6*t2) + 
                    2*Power(s2,2)*t1*
                     (Power(t1,2) - t1*t2 - 9*Power(t2,2)) + 
                    Power(s2,3)*
                     (-2*Power(t1,2) + 6*t1*t2 + 7*Power(t2,2))) + 
                 Power(s1,4)*
                  (2*t1*Power(t2,2)*(-4*t1 + 3*t2) + 
                    Power(s2,3)*(-4*t1 + 30*t2) + 
                    6*Power(s2,2)*
                     (5*Power(t1,2) + 11*t1*t2 - 15*Power(t2,2)) + 
                    2*s2*(30*Power(t1,3) + 46*Power(t1,2)*t2 - 
                       87*t1*Power(t2,2) - 12*Power(t2,3))) + 
                 2*s1*(-(t1*(t1 - 3*t2)*Power(t1 - t2,3)*
                       Power(t2,2)) - 
                    2*s2*Power(t1 - t2,2)*t2*
                     (7*Power(t1,3) + 11*Power(t1,2)*t2 - 
                       9*t1*Power(t2,2) - 3*Power(t2,3)) + 
                    Power(s2,3)*
                     (Power(t1,4) + 12*Power(t1,3)*t2 - 
                       63*Power(t1,2)*Power(t2,2) + 
                       56*t1*Power(t2,3) - 15*Power(t2,4)) - 
                    3*Power(s2,2)*t2*
                     (-7*Power(t1,4) + 9*Power(t1,3)*t2 - 
                       4*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                       Power(t2,4))) + 
                 2*Power(s1,3)*
                  (t1*Power(t2,2)*
                     (-11*Power(t1,2) + 20*t1*t2 - 9*Power(t2,2)) + 
                    5*Power(s2,3)*
                     (Power(t1,2) + 6*t1*t2 - 8*Power(t2,2)) + 
                    Power(s2,2)*
                     (10*Power(t1,3) + 65*Power(t1,2)*t2 - 
                       144*t1*Power(t2,2) + 75*Power(t2,3)) + 
                    s2*(30*Power(t1,4) - 12*Power(t1,3)*t2 - 
                       115*Power(t1,2)*Power(t2,2) + 
                       72*t1*Power(t2,3) + 24*Power(t2,4))) + 
                 Power(s1,2)*
                  (-18*t1*Power(t1 - t2,3)*Power(t2,2) + 
                    2*Power(s2,2)*t2*
                     (67*Power(t1,3) - 183*Power(t1,2)*t2 + 
                       156*t1*Power(t2,2) - 43*Power(t2,3)) + 
                    Power(s2,3)*
                     (14*Power(t1,3) + 42*Power(t1,2)*t2 - 
                       153*t1*Power(t2,2) + 91*Power(t2,3)) + 
                    s2*(20*Power(t1,5) - 84*Power(t1,4)*t2 + 
                       49*Power(t1,3)*Power(t2,2) + 
                       31*Power(t1,2)*Power(t2,3) + 30*t1*Power(t2,4) - 
                       46*Power(t2,5)))) + 
              Power(s,4)*(4*Power(s1,6)*s2*
                  (3*Power(s2,2) - 2*s2*(t1 + t2) - 
                    5*(2*Power(t1,2) + 4*t1*t2 + Power(t2,2))) + 
                 2*Power(s1,5)*
                  (3*Power(s2,4) + Power(s2,3)*(5*t1 - 37*t2) + 
                    3*t1*(2*t1 - t2)*Power(t2,2) + 
                    Power(s2,2)*
                     (-10*Power(t1,2) - 43*t1*t2 + 18*Power(t2,2)) + 
                    s2*(-60*Power(t1,3) - 24*Power(t1,2)*t2 + 
                       119*t1*Power(t2,2) + 46*Power(t2,3))) + 
                 s2*t2*(-(Power(t1,2)*(3*t1 - 4*t2)*Power(t1 - t2,3)*
                       t2) + 
                    Power(s2,3)*t2*
                     (-7*Power(t1,3) + 12*Power(t1,2)*t2 - 
                       15*t1*Power(t2,2) + 4*Power(t2,3)) + 
                    s2*t1*Power(t1 - t2,2)*
                     (4*Power(t1,3) - 9*Power(t1,2)*t2 - 
                       6*t1*Power(t2,2) + 8*Power(t2,3)) + 
                    Power(s2,2)*t1*
                     (-4*Power(t1,4) + 31*Power(t1,3)*t2 - 
                       57*Power(t1,2)*Power(t2,2) + 
                       43*t1*Power(t2,3) - 13*Power(t2,4))) + 
                 2*Power(s1,4)*
                  (Power(s2,4)*(7*t1 - 16*t2) + 
                    t1*Power(t2,2)*
                     (14*Power(t1,2) - 24*t1*t2 + 9*Power(t2,2)) + 
                    Power(s2,3)*
                     (-16*Power(t1,2) - 61*t1*t2 + 97*Power(t2,2)) - 
                    Power(s2,2)*
                     (10*Power(t1,3) + 114*Power(t1,2)*t2 - 
                       156*t1*Power(t2,2) + 15*Power(t2,3)) - 
                    2*s2*(30*Power(t1,4) - 54*Power(t1,3)*t2 - 
                       69*Power(t1,2)*Power(t2,2) + 
                       58*t1*Power(t2,3) + 39*Power(t2,4))) - 
                 Power(s1,2)*
                  (6*t1*Power(t1 - t2,2)*Power(t2,2)*
                     (2*Power(t1,2) - Power(t2,2)) + 
                    Power(s2,4)*
                     (6*Power(t1,3) + 36*Power(t1,2)*t2 - 
                       99*t1*Power(t2,2) + 54*Power(t2,3)) + 
                    Power(s2,3)*
                     (-4*Power(t1,4) + 74*Power(t1,3)*t2 - 
                       315*Power(t1,2)*Power(t2,2) + 
                       307*t1*Power(t2,3) - 110*Power(t2,4)) + 
                    Power(s2,2)*
                     (20*Power(t1,5) - 16*Power(t1,4)*t2 - 
                       57*Power(t1,3)*Power(t2,2) + 
                       94*Power(t1,2)*Power(t2,3) + 
                       70*t1*Power(t2,4) - 96*Power(t2,5)) + 
                    s2*t2*(-72*Power(t1,5) + 65*Power(t1,4)*t2 + 
                       223*Power(t1,3)*Power(t2,2) - 
                       324*Power(t1,2)*Power(t2,3) + 
                       76*t1*Power(t2,4) + 32*Power(t2,5))) + 
                 Power(s1,3)*
                  (Power(s2,4)*
                     (6*Power(t1,2) - 66*t1*t2 + 65*Power(t2,2)) + 
                    6*t1*Power(t2,2)*
                     (2*Power(t1,3) - 8*Power(t1,2)*t2 + 
                       9*t1*Power(t2,2) - 3*Power(t2,3)) - 
                    Power(s2,3)*
                     (36*Power(t1,3) + 70*Power(t1,2)*t2 - 
                       343*t1*Power(t2,2) + 240*Power(t2,3)) - 
                    Power(s2,2)*
                     (20*Power(t1,4) + 184*Power(t1,3)*t2 - 
                       541*Power(t1,2)*Power(t2,2) + 
                       252*t1*Power(t2,3) + 58*Power(t2,4)) + 
                    s2*(-40*Power(t1,5) + 256*Power(t1,4)*t2 - 
                       143*Power(t1,3)*Power(t2,2) - 
                       286*Power(t1,2)*Power(t2,3) + 
                       92*t1*Power(t2,4) + 116*Power(t2,5))) + 
                 s1*(-2*Power(t1,2)*(4*t1 - 3*t2)*Power(t1 - t2,3)*
                     Power(t2,2) - 
                    s2*t1*Power(t1 - t2,2)*Power(t2,2)*
                     (27*Power(t1,2) + 22*t1*t2 - 34*Power(t2,2)) + 
                    Power(s2,4)*
                     (-4*Power(t1,4) - 2*Power(t1,3)*t2 + 
                       51*Power(t1,2)*Power(t2,2) - 
                       32*t1*Power(t2,3) + 11*Power(t2,4)) + 
                    Power(s2,3)*
                     (10*Power(t1,5) - 56*Power(t1,4)*t2 + 
                       53*Power(t1,3)*Power(t2,2) - 
                       64*Power(t1,2)*Power(t2,3) + 
                       29*t1*Power(t2,4) - 2*Power(t2,5)) + 
                    Power(s2,2)*
                     (-8*Power(t1,6) + 54*Power(t1,5)*t2 + 
                       15*Power(t1,4)*Power(t2,2) - 
                       172*Power(t1,3)*Power(t2,3) + 
                       129*Power(t1,2)*Power(t2,4) + 
                       12*t1*Power(t2,5) - 30*Power(t2,6)))) + 
              s*s1*(2*Power(s2,3)*(s2 - t1)*t1*Power(t1 - t2,3)*
                  Power(t2,3) - 4*Power(s1,8)*s2*(s2 - t1)*(t1 + t2) - 
                 2*Power(s1,7)*s2*
                  (2*Power(s2,3) - Power(s2,2)*(t1 - 10*t2) + 
                    s2*(5*Power(t1,2) - 16*t1*t2 - 10*Power(t2,2)) + 
                    2*t1*(-3*Power(t1,2) + 3*t1*t2 + 7*Power(t2,2))) - 
                 2*Power(s1,6)*
                  (Power(s2,5) + 4*Power(s2,4)*t1 - 
                    Power(t1,3)*Power(t2,2) - 
                    Power(s2,3)*
                     (Power(t1,2) - 22*t1*t2 + 38*Power(t2,2)) + 
                    s2*t1*(-6*Power(t1,3) + 24*Power(t1,2)*t2 + 
                       9*t1*Power(t2,2) - 38*Power(t2,3)) + 
                    Power(s2,2)*
                     (2*Power(t1,3) - 45*Power(t1,2)*t2 + 
                       38*t1*Power(t2,2) + 22*Power(t2,3))) + 
                 s1*Power(s2,2)*Power(t2,2)*
                  (-(t1*Power(t1 - t2,2)*t2*
                       (9*Power(t1,2) - 8*t1*t2 + 2*Power(t2,2))) + 
                    3*Power(s2,3)*
                     (2*Power(t1,3) - 3*Power(t1,2)*t2 + 
                       4*t1*Power(t2,2) - Power(t2,3)) + 
                    s2*t1*(10*Power(t1,4) - 13*Power(t1,3)*t2 - 
                       6*Power(t1,2)*Power(t2,2) + 
                       17*t1*Power(t2,3) - 8*Power(t2,4)) + 
                    Power(s2,2)*
                     (-12*Power(t1,4) + 19*Power(t1,3)*t2 - 
                       18*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                       2*Power(t2,4))) + 
                 2*Power(s1,5)*
                  (-4*Power(s2,4)*(3*t1 - 4*t2)*t2 + 
                    3*Power(t1,3)*(t1 - t2)*Power(t2,2) + 
                    Power(s2,5)*(-3*t1 + 2*t2) + 
                    Power(s2,3)*
                     (-3*Power(t1,3) + 6*Power(t1,2)*t2 + 
                       55*t1*Power(t2,2) - 58*Power(t2,3)) + 
                    s2*t1*(2*Power(t1,4) - 22*Power(t1,3)*t2 + 
                       24*Power(t1,2)*Power(t2,2) + 
                       47*t1*Power(t2,3) - 50*Power(t2,4)) + 
                    Power(s2,2)*
                     (4*Power(t1,4) + 23*Power(t1,3)*t2 - 
                       88*Power(t1,2)*Power(t2,2) + 
                       39*t1*Power(t2,3) + 26*Power(t2,4))) + 
                 Power(s1,2)*s2*t2*
                  (Power(t1,2)*Power(t1 - t2,2)*t2*
                     (3*Power(t1,2) - 10*t1*t2 + 4*Power(t2,2)) + 
                    2*Power(s2,3)*t2*
                     (Power(t1,3) + 3*Power(t1,2)*t2 + 
                       12*t1*Power(t2,2) - 7*Power(t2,3)) + 
                    Power(s2,4)*
                     (-2*Power(t1,3) + 15*Power(t1,2)*t2 - 
                       30*t1*Power(t2,2) + 11*Power(t2,3)) - 
                    2*s2*t1*(2*Power(t1,5) - 14*Power(t1,4)*t2 + 
                       20*Power(t1,3)*Power(t2,2) - 
                       5*Power(t1,2)*Power(t2,3) - 7*t1*Power(t2,4) + 
                       4*Power(t2,5)) + 
                    2*Power(s2,2)*
                     (3*Power(t1,5) - 11*Power(t1,4)*t2 + 
                       8*Power(t1,3)*Power(t2,2) - 
                       9*Power(t1,2)*Power(t2,3) - t1*Power(t2,4) + 
                       4*Power(t2,5))) - 
                 Power(s1,4)*
                  (-6*Power(t1,3)*Power(t1 - t2,2)*Power(t2,2) + 
                    Power(s2,5)*
                     (6*Power(t1,2) - 6*t1*t2 - 3*Power(t2,2)) + 
                    Power(s2,4)*
                     (-8*Power(t1,3) + 48*Power(t1,2)*t2 - 
                       96*t1*Power(t2,2) + 58*Power(t2,3)) + 
                    s2*t1*t2*
                     (12*Power(t1,4) - 53*Power(t1,3)*t2 + 
                       6*Power(t1,2)*Power(t2,2) + 
                       102*t1*Power(t2,3) - 64*Power(t2,4)) + 
                    2*Power(s2,3)*
                     (5*Power(t1,4) - 41*Power(t1,3)*t2 + 
                       25*Power(t1,2)*Power(t2,2) + 
                       57*t1*Power(t2,3) - 46*Power(t2,4)) + 
                    Power(s2,2)*
                     (-8*Power(t1,5) + 34*Power(t1,4)*t2 + 
                       34*Power(t1,3)*Power(t2,2) - 
                       148*Power(t1,2)*Power(t2,3) + 
                       48*t1*Power(t2,4) + 32*Power(t2,5))) + 
                 Power(s1,3)*
                  (2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2) + 
                    2*s2*t1*Power(t1 - t2,2)*Power(t2,2)*
                     (12*Power(t1,2) - t1*t2 - 8*Power(t2,2)) + 
                    Power(s2,5)*
                     (-2*Power(t1,3) + 18*t1*Power(t2,2) - 
                       13*Power(t2,3)) + 
                    Power(s2,4)*
                     (4*Power(t1,4) - 24*Power(t1,3)*t2 + 
                       54*Power(t1,2)*Power(t2,2) - 
                       89*t1*Power(t2,3) + 42*Power(t2,4)) + 
                    Power(s2,3)*
                     (-4*Power(t1,5) + 52*Power(t1,4)*t2 - 
                       80*Power(t1,3)*Power(t2,2) + 
                       35*Power(t1,2)*Power(t2,3) + 
                       56*t1*Power(t2,4) - 40*Power(t2,5)) + 
                    Power(s2,2)*
                     (2*Power(t1,6) - 30*Power(t1,5)*t2 + 
                       50*Power(t1,4)*Power(t2,2) + 
                       11*Power(t1,3)*Power(t2,3) - 
                       78*Power(t1,2)*Power(t2,4) + 28*t1*Power(t2,5) + 
                       8*Power(t2,6)))) + 
              Power(s,2)*(-2*Power(s2,3)*(s2 - t1)*t1*Power(t1 - t2,3)*
                  Power(t2,3) + 
                 4*Power(s1,8)*s2*
                  (Power(s2,2) + 3*s2*t1 - 5*Power(t1,2) + 4*s2*t2 - 
                    6*t1*t2 - Power(t2,2)) + 
                 2*Power(s1,7)*
                  (7*Power(s2,4) - Power(s2,3)*(t1 - 7*t2) + 
                    Power(t1,2)*Power(t2,2) + 
                    Power(s2,2)*
                     (15*Power(t1,2) - 36*t1*t2 - 38*Power(t2,2)) + 
                    s2*(-30*Power(t1,3) + 20*Power(t1,2)*t2 + 
                       63*t1*Power(t2,2) + 10*Power(t2,3))) - 
                 2*s1*Power(s2,2)*Power(t2,2)*
                  (-(t1*Power(t1 - t2,2)*t2*
                       (8*Power(t1,2) - 6*t1*t2 + Power(t2,2))) + 
                    Power(s2,3)*
                     (2*Power(t1,3) + 3*Power(t1,2)*t2 + Power(t2,3)) \
+ s2*t1*(9*Power(t1,4) - 4*Power(t1,3)*t2 - 
                       24*Power(t1,2)*Power(t2,2) + 
                       30*t1*Power(t2,3) - 11*Power(t2,4)) - 
                    3*Power(s2,2)*
                     (4*Power(t1,4) - 5*Power(t1,3)*t2 + 
                       3*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                       Power(t2,4))) + 
                 2*Power(s1,6)*
                  (2*Power(s2,5) + 3*Power(s2,4)*(5*t1 - 7*t2) - 
                    Power(t1,2)*Power(t2,2)*(t1 + 2*t2) + 
                    Power(s2,3)*
                     (-8*Power(t1,2) + 31*t1*t2 - 39*Power(t2,2)) + 
                    Power(s2,2)*
                     (5*Power(t1,3) - 110*Power(t1,2)*t2 + 
                       79*t1*Power(t2,2) + 84*Power(t2,3)) - 
                    2*s2*(15*Power(t1,4) - 51*Power(t1,3)*t2 - 
                       26*Power(t1,2)*Power(t2,2) + 
                       69*t1*Power(t2,3) + 9*Power(t2,4))) + 
                 Power(s1,5)*
                  (4*Power(s2,5)*(3*t1 - 4*t2) + 
                    18*Power(t1,3)*Power(t2,2)*(-t1 + t2) + 
                    Power(s2,4)*
                     (6*Power(t1,2) - 40*t1*t2 + 53*Power(t2,2)) + 
                    Power(s2,3)*
                     (4*Power(t1,3) - 10*Power(t1,2)*t2 - 
                       129*t1*Power(t2,2) + 138*Power(t2,3)) - 
                    Power(s2,2)*
                     (30*Power(t1,4) + 86*Power(t1,3)*t2 - 
                       449*Power(t1,2)*Power(t2,2) + 
                       144*t1*Power(t2,3) + 208*Power(t2,4)) + 
                    s2*(-20*Power(t1,5) + 192*Power(t1,4)*t2 - 
                       157*Power(t1,3)*Power(t2,2) - 
                       376*Power(t1,2)*Power(t2,3) + 
                       322*t1*Power(t2,4) + 28*Power(t2,5))) + 
                 Power(s1,2)*s2*t2*
                  (-2*Power(s2,4)*
                     (2*Power(t1,3) - 9*Power(t1,2)*t2 + 
                       6*t1*Power(t2,2) - 5*Power(t2,3)) - 
                    t1*Power(t1 - t2,2)*t2*
                     (9*Power(t1,3) - 27*Power(t1,2)*t2 + 
                       14*t1*Power(t2,2) - 2*Power(t2,3)) + 
                    Power(s2,3)*
                     (8*Power(t1,4) - 13*Power(t1,3)*t2 - 
                       30*Power(t1,2)*Power(t2,2) - 
                       23*t1*Power(t2,3) + 16*Power(t2,4)) + 
                    Power(s2,2)*
                     (-16*Power(t1,5) + 87*Power(t1,4)*t2 - 
                       131*Power(t1,3)*Power(t2,2) + 
                       109*Power(t1,2)*Power(t2,3) + 
                       3*t1*Power(t2,4) - 28*Power(t2,5)) + 
                    s2*t1*(12*Power(t1,5) - 73*Power(t1,4)*t2 + 
                       72*Power(t1,3)*Power(t2,2) + 
                       63*Power(t1,2)*Power(t2,3) - 
                       122*t1*Power(t2,4) + 48*Power(t2,5))) + 
                 Power(s1,4)*
                  (-2*Power(t1,2)*(11*t1 - 2*t2)*Power(t1 - t2,2)*
                     Power(t2,2) + 
                    2*Power(s2,5)*
                     (6*Power(t1,2) - 18*t1*t2 + 13*Power(t2,2)) + 
                    Power(s2,4)*
                     (-22*Power(t1,3) + 54*Power(t1,2)*t2 + 
                       9*t1*Power(t2,2) - 32*Power(t2,3)) + 
                    Power(s2,3)*
                     (28*Power(t1,4) - 166*Power(t1,3)*t2 + 
                       159*Power(t1,2)*Power(t2,2) + 
                       133*t1*Power(t2,3) - 146*Power(t2,4)) - 
                    s2*t2*(-52*Power(t1,5) + 159*Power(t1,4)*t2 + 
                       55*Power(t1,3)*Power(t2,2) - 
                       374*Power(t1,2)*Power(t2,3) + 
                       198*t1*Power(t2,4) + 8*Power(t2,5)) + 
                    Power(s2,2)*
                     (-30*Power(t1,5) + 132*Power(t1,4)*t2 + 
                       71*Power(t1,3)*Power(t2,2) - 
                       434*Power(t1,2)*Power(t2,3) + 
                       94*t1*Power(t2,4) + 136*Power(t2,5))) + 
                 Power(s1,3)*
                  (-2*Power(t1,2)*Power(t1 - t2,3)*(4*t1 - t2)*
                     Power(t2,2) - 
                    s2*t1*Power(t1 - t2,2)*Power(t2,2)*
                     (73*Power(t1,2) + 8*t1*t2 - 48*Power(t2,2)) + 
                    Power(s2,5)*
                     (4*Power(t1,3) - 24*Power(t1,2)*t2 + 
                       36*t1*Power(t2,2) - 22*Power(t2,3)) + 
                    Power(s2,4)*
                     (-12*Power(t1,4) + 60*Power(t1,3)*t2 - 
                       9*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) - 
                       3*Power(t2,4)) + 
                    Power(s2,3)*
                     (14*Power(t1,5) - 124*Power(t1,4)*t2 + 
                       171*Power(t1,3)*Power(t2,2) - 
                       138*Power(t1,2)*Power(t2,3) - 
                       89*t1*Power(t2,4) + 96*Power(t2,5)) - 
                    Power(s2,2)*
                     (8*Power(t1,6) - 98*Power(t1,5)*t2 + 
                       97*Power(t1,4)*Power(t2,2) + 
                       172*Power(t1,3)*Power(t2,3) - 
                       331*Power(t1,2)*Power(t2,4) + 
                       98*t1*Power(t2,5) + 36*Power(t2,6)))) + 
              Power(s,3)*(-4*Power(s1,7)*s2*
                  (3*Power(s2,2) - 10*Power(t1,2) - 15*t1*t2 - 
                    4*Power(t2,2) + 2*s2*(t1 + 2*t2)) - 
                 2*Power(s1,6)*
                  (8*Power(s2,4) + Power(s2,3)*(3*t1 - 23*t2) + 
                    t1*(4*t1 - t2)*Power(t2,2) + 
                    Power(s2,2)*
                     (10*Power(t1,2) - 43*t1*t2 - 37*Power(t2,2)) + 
                    s2*(-60*Power(t1,3) + 14*Power(t1,2)*t2 + 
                       115*t1*Power(t2,2) + 38*Power(t2,3))) - 
                 Power(s2,2)*Power(t2,2)*
                  (Power(t1,2)*(7*t1 - 4*t2)*Power(t1 - t2,2)*t2 + 
                    3*Power(s2,2)*t1*
                     (4*Power(t1,3) - 5*Power(t1,2)*t2 + 
                       4*t1*Power(t2,2) - Power(t2,3)) + 
                    Power(s2,3)*
                     (-4*Power(t1,3) + 3*Power(t1,2)*t2 - 
                       6*t1*Power(t2,2) + Power(t2,3)) + 
                    s2*t1*(-8*Power(t1,4) + 3*Power(t1,3)*t2 + 
                       18*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                       6*Power(t2,4))) - 
                 2*Power(s1,5)*
                  (Power(s2,5) + Power(s2,4)*(18*t1 - 35*t2) - 
                    6*Power(s2,3)*
                     (3*Power(t1,2) + 3*t1*t2 - 8*Power(t2,2)) + 
                    3*t1*Power(t2,2)*
                     (2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
                    2*Power(s2,2)*t2*
                     (-69*Power(t1,2) + 55*t1*t2 + 46*Power(t2,2)) - 
                    6*s2*(10*Power(t1,4) - 27*Power(t1,3)*t2 - 
                       19*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) + 
                       11*Power(t2,4))) + 
                 Power(s1,4)*
                  (Power(s2,5)*(-6*t1 + 10*t2) - 
                    6*Power(s2,4)*
                     (2*Power(t1,2) - 20*t1*t2 + 23*Power(t2,2)) + 
                    6*t1*Power(t2,2)*
                     (2*Power(t1,3) - 3*t1*Power(t2,2) + Power(t2,3)) + 
                    Power(s2,3)*
                     (24*Power(t1,3) + 26*Power(t1,2)*t2 - 
                       147*t1*Power(t2,2) + 99*Power(t2,3)) + 
                    2*Power(s2,2)*
                     (20*Power(t1,4) + 70*Power(t1,3)*t2 - 
                       296*Power(t1,2)*Power(t2,2) + 
                       67*t1*Power(t2,3) + 134*Power(t2,4)) + 
                    s2*(40*Power(t1,5) - 324*Power(t1,4)*t2 + 
                       205*Power(t1,3)*Power(t2,2) + 
                       527*Power(t1,2)*Power(t2,3) - 
                       334*t1*Power(t2,4) - 100*Power(t2,5))) - 
                 s1*s2*t2*(-(t1*Power(t1 - t2,2)*t2*
                       (9*Power(t1,3) - 24*Power(t1,2)*t2 + 
                        14*t1*Power(t2,2) - 2*Power(t2,3))) + 
                    Power(s2,4)*
                     (-4*Power(t1,3) + 9*Power(t1,2)*t2 + Power(t2,3)) \
+ 6*Power(s2,3)*(Power(t1,4) - 2*Power(t1,3)*t2 - 
                       5*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) - 
                       Power(t2,4)) - 
                    2*Power(s2,2)*
                     (7*Power(t1,5) - 48*Power(t1,4)*t2 + 
                       72*Power(t1,3)*Power(t2,2) - 
                       37*Power(t1,2)*Power(t2,3) - 
                       12*t1*Power(t2,4) + 12*Power(t2,5)) + 
                    2*s2*t1*(6*Power(t1,5) - 31*Power(t1,4)*t2 + 
                       24*Power(t1,3)*Power(t2,2) + 
                       42*Power(t1,2)*Power(t2,3) - 65*t1*Power(t2,4) + 
                       24*Power(t2,5))) + 
                 Power(s1,3)*
                  (Power(s2,5)*
                     (-6*Power(t1,2) + 24*t1*t2 - 17*Power(t2,2)) + 
                    2*t1*Power(t1 - t2,2)*Power(t2,2)*
                     (14*Power(t1,2) - 4*t1*t2 - Power(t2,2)) + 
                    2*Power(s2,4)*
                     (10*Power(t1,3) + 12*Power(t1,2)*t2 - 
                       93*t1*Power(t2,2) + 68*Power(t2,3)) + 
                    2*Power(s2,3)*
                     (-12*Power(t1,4) + 71*Power(t1,3)*t2 - 
                       149*Power(t1,2)*Power(t2,2) + 
                       66*t1*Power(t2,3) + Power(t2,4)) + 
                    2*Power(s2,2)*
                     (20*Power(t1,5) - 68*Power(t1,4)*t2 - 
                       41*Power(t1,3)*Power(t2,2) + 
                       191*Power(t1,2)*Power(t2,3) + 
                       17*t1*Power(t2,4) - 100*Power(t2,5)) + 
                    s2*t2*(-88*Power(t1,5) + 163*Power(t1,4)*t2 + 
                       200*Power(t1,3)*Power(t2,2) - 
                       512*Power(t1,2)*Power(t2,3) + 
                       206*t1*Power(t2,4) + 28*Power(t2,5))) + 
                 Power(s1,2)*(6*Power(t1,2)*Power(t1 - t2,3)*
                     (2*t1 - t2)*Power(t2,2) + 
                    3*s2*t1*Power(t1 - t2,2)*Power(t2,2)*
                     (25*Power(t1,2) + 9*t1*t2 - 20*Power(t2,2)) + 
                    Power(s2,5)*
                     (-2*Power(t1,3) + 18*Power(t1,2)*t2 - 
                       24*t1*Power(t2,2) + 11*Power(t2,3)) + 
                    Power(s2,4)*
                     (12*Power(t1,4) - 32*Power(t1,3)*t2 - 
                       96*Power(t1,2)*Power(t2,2) + 
                       111*t1*Power(t2,3) - 58*Power(t2,4)) - 
                    3*Power(s2,3)*
                     (6*Power(t1,5) - 40*Power(t1,4)*t2 + 
                       45*Power(t1,3)*Power(t2,2) - 
                       44*Power(t1,2)*Power(t2,3) - 15*t1*Power(t2,4) + 
                       21*Power(t2,5)) + 
                    Power(s2,2)*
                     (12*Power(t1,6) - 114*Power(t1,5)*t2 + 
                       38*Power(t1,4)*Power(t2,2) + 
                       287*Power(t1,3)*Power(t2,3) - 
                       330*Power(t1,2)*Power(t2,4) + 40*t1*Power(t2,5) + 
                       58*Power(t2,6))))))/
          (Power(s - s1,2)*Power(s1 + t1 - t2,3)))*lg2(-s))/
     (s*Power(s1,2)*Power(s2,2)*Power(t1,2)*Power(s - s2 + t1,2)*
       Power(t2,2)*Power(s - s1 + t2,2)) + 
    (4*(2*Power(s1,5)*s2*(s2 - t1)*t1 + 
         Power(s1,4)*t1*(3*Power(s2,3) + Power(s2,2)*(t1 - 7*t2) + 
            Power(t1,2)*(-t1 + t2) + s2*t1*(-3*t1 + 8*t2)) + 
         Power(s1,3)*(Power(s2,4)*t2 + Power(s2,2)*t1*t2*(7*t1 + 5*t2) + 
            s2*Power(t1,2)*(-4*Power(t1,2) + 8*t1*t2 - 9*Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,2) - 16*t1*t2 + Power(t2,2)) - 
            Power(t1,3)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*(2*Power(s2,5)*t2 + 
            2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
            Power(s2,4)*t2*(-7*t1 + t2) + 
            Power(s2,3)*(4*Power(t1,3) - 7*Power(t1,2)*t2 + 
               17*t1*Power(t2,2) - Power(t2,3)) + 
            Power(s2,2)*t1*(-Power(t1,3) + 4*Power(t1,2)*t2 - 
               12*t1*Power(t2,2) + Power(t2,3)) + 
            s2*Power(t1,2)*(-3*Power(t1,3) + 9*Power(t1,2)*t2 - 
               10*t1*Power(t2,2) + 5*Power(t2,3))) - 
         Power(s2,2)*t2*(Power(s2,4)*(-2*t1 + t2) + 
            Power(s2,3)*(8*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(-12*Power(t1,3) + 18*Power(t1,2)*t2 - 
               8*t1*Power(t2,2) + Power(t2,3)) + 
            s2*(10*Power(t1,4) - 20*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
               Power(t2,4)) - 
            2*t1*(2*Power(t1,4) - 5*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + Power(t2,4)\
)) + s1*s2*(Power(s2,5)*t2 - Power(s2,4)*t2*(t1 + t2) - 
            Power(s2,3)*t2*(6*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
            2*Power(t1,2)*t2*
             (3*Power(t1,3) - 7*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s2,2)*(2*Power(t1,4) + 6*Power(t1,3)*t2 - 
               9*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
               Power(t2,4)) - 
            s2*t1*(2*Power(t1,4) + 5*Power(t1,3)*t2 - 
               10*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
               Power(t2,4))) + 
         Power(s,4)*(s1*t1*(2*Power(s2,2) + Power(t1 - t2,2) + 
               s2*(-t1 + t2)) + 
            (t1 - t2)*(t1*Power(t1 - t2,2) + Power(s2,2)*(2*t1 + t2) + 
               s2*(-Power(t1,2) + Power(t2,2)))) + 
         Power(s,3)*(-3*s2*t1*Power(t1 - t2,2)*(t1 + 2*t2) + 
            t1*Power(t1 - t2,3)*(t1 + 2*t2) + 
            Power(s2,3)*(-2*Power(t1,2) - 3*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,3) + 13*Power(t1,2)*t2 - 
               19*t1*Power(t2,2) + 4*Power(t2,3)) - 
            Power(s1,2)*t1*(7*Power(s2,2) + 3*Power(t1,2) - 5*t1*t2 + 
               2*Power(t2,2) + s2*(-4*t1 + t2)) + 
            s1*(-2*t1*(t1 - 2*t2)*Power(t1 - t2,2) - 
               Power(s2,3)*(2*t1 + t2) + 
               Power(s2,2)*(-5*Power(t1,2) + 14*t1*t2 + Power(t2,2)) + 
               s2*(Power(t1,3) - 4*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)))) + 
         Power(s,2)*(3*Power(s2,4)*(3*t1 - 2*t2)*t2 + 
            2*Power(t1,2)*Power(t1 - t2,3)*t2 + 
            Power(s2,3)*t2*(-31*Power(t1,2) + 36*t1*t2 - 
               6*Power(t2,2)) - 
            s2*Power(t1 - t2,2)*
             (3*Power(t1,3) + 10*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
               Power(t2,3)) + 
            Power(s2,2)*(4*Power(t1,4) + 20*Power(t1,3)*t2 - 
               36*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) - 
               Power(t2,4)) + 
            Power(s1,3)*t1*(10*Power(s2,2) + 3*Power(t1,2) - 4*t1*t2 + 
               Power(t2,2) - s2*(7*t1 + t2)) + 
            Power(s1,2)*(2*Power(s2,3)*(3*t1 + t2) + 
               Power(s2,2)*(6*Power(t1,2) - 32*t1*t2 + Power(t2,2)) - 
               3*t1*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) - 
               s2*(2*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) + Power(t2,3))) + 
            s1*(3*Power(s2,4)*t2 + 
               Power(s2,3)*(6*Power(t1,2) - 17*t1*t2 - 3*Power(t2,2)) - 
               t1*Power(t1 - t2,2)*(3*Power(t1,2) - 2*Power(t2,2)) - 
               2*Power(s2,2)*t2*
                (6*Power(t1,2) - 16*t1*t2 + 3*Power(t2,2)) + 
               2*s2*t1*(Power(t1,3) - 4*t1*Power(t2,2) + 3*Power(t2,3)))) \
- s*(Power(s1,4)*t1*(7*Power(s2,2) + t1*(t1 - t2) - s2*(6*t1 + t2)) + 
            Power(s1,3)*(-2*Power(t1,4) + 2*Power(t1,2)*Power(t2,2) + 
               Power(s2,3)*(7*t1 + t2) + 
               s2*Power(t1,2)*(-5*t1 + 17*t2) + 
               Power(s2,2)*(4*Power(t1,2) - 26*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(4*Power(s2,4)*t2 + 
               Power(s2,3)*(9*Power(t1,2) - 34*t1*t2 + 2*Power(t2,2)) + 
               2*Power(s2,2)*
                (Power(t1,3) + 3*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(t1,2)*(-3*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t1*(-5*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(3*Power(s2,5)*t2 + 
               2*Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2)*t2 - 
               Power(s2,4)*t2*(4*t1 + 3*t2) + 
               Power(s2,3)*(4*Power(t1,3) - 26*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*t1*(-6*Power(t1,4) + 5*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(3*Power(t1,4) + 16*Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            s2*(Power(s2,4)*(7*t1 - 4*t2)*t2 + 
               2*t1*Power(t1 - t2,2)*t2*
                (3*Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
               2*Power(s2,3)*t2*
                (12*Power(t1,2) - 13*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,4) + 26*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) - 
                  2*Power(t2,4)) - 
               s2*(2*Power(t1,5) + 15*Power(t1,4)*t2 - 
                  34*Power(t1,3)*Power(t2,2) + 
                  26*Power(t1,2)*Power(t2,3) - 11*t1*Power(t2,4) + 
                  2*Power(t2,5)))))*lg2(s/s1))/
     (s1*Power(s2,2)*t1*(s - s2 + t1)*(-s + s1 - t2)*(s1 + t1 - t2)*t2*
       (s2 - t1 + t2)) + (8*(2*Power(s2,3)*Power(s2 - t1,2)*Power(t1,2)*
          Power(t2,4) + 2*Power(s1,6)*s2*(s2 - t1)*t1*
          (-(t1*(t1 + t2)) + s2*(t1 + 2*t2)) + 
         Power(s,7)*t2*(Power(t1,2)*(t1 - t2) + 
            s2*(Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
         s1*Power(s2,2)*Power(t2,3)*
          (-2*Power(t1,3)*Power(t1 - t2,2) + Power(s2,4)*(-2*t1 + t2) + 
            Power(s2,2)*t1*t2*(4*t1 + t2) + 
            Power(s2,3)*(3*Power(t1,2) - Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,5)*(Power(s2,5)*(2*t1 - t2) - Power(t1,5)*t2 - 
            s2*Power(t1,3)*t2*(5*t1 + 6*t2) - 
            Power(s2,3)*t1*t2*(13*t1 + 8*t2) + 
            Power(s2,4)*(-3*Power(t1,2) + 7*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*
             (Power(t1,2) + 13*t1*t2 + 12*Power(t2,2))) + 
         Power(s1,2)*s2*Power(t2,2)*
          (Power(s2,5)*(4*t1 - 2*t2) + 4*Power(t1,4)*t2*(-t1 + t2) + 
            s2*Power(t1,3)*(3*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,4)*(-10*Power(t1,2) - 4*t1*t2 + 7*Power(t2,2)) + 
            Power(s2,3)*(9*Power(t1,3) + 6*Power(t1,2)*t2 - 
               4*t1*Power(t2,2) - 2*Power(t2,3)) - 
            2*Power(s2,2)*t1*(3*Power(t1,3) + t1*Power(t2,2) - 
               2*Power(t2,3))) + 
         Power(s1,3)*t2*(3*s2*Power(t1,4)*(2*t1 - t2)*t2 - 
            2*Power(t1,5)*Power(t2,2) + Power(s2,6)*(-3*t1 + t2) + 
            3*Power(s2,5)*(3*Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
            Power(s2,2)*Power(t1,3)*
             (-3*Power(t1,2) - 4*t1*t2 + 8*Power(t2,2)) + 
            2*Power(s2,3)*t1*
             (4*Power(t1,3) + Power(t1,2)*t2 - 2*t1*Power(t2,2) - 
               5*Power(t2,3)) + 
            Power(s2,4)*(-11*Power(t1,3) - 10*Power(t1,2)*t2 + 
               7*t1*Power(t2,2) + 8*Power(t2,3))) + 
         Power(s1,4)*(Power(s2,6)*t1 + 3*Power(t1,5)*Power(t2,2) - 
            2*s2*Power(t1,3)*t2*
             (Power(t1,2) - 2*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,5)*(-3*Power(t1,2) - 4*t1*t2 + 7*Power(t2,2)) + 
            Power(s2,4)*(4*Power(t1,3) + 6*Power(t1,2)*t2 - 
               11*t1*Power(t2,2) - 10*Power(t2,3)) + 
            Power(s2,2)*Power(t1,2)*
             (Power(t1,3) - 2*Power(t1,2)*t2 - 16*t1*Power(t2,2) - 
               6*Power(t2,3)) + 
            Power(s2,3)*t1*(-3*Power(t1,3) + 2*Power(t1,2)*t2 + 
               16*t1*Power(t2,2) + 10*Power(t2,3))) - 
         Power(s,6)*(-(Power(t1,2)*t2*
               (2*Power(t1,2) + t1*t2 - 3*Power(t2,2))) - 
            Power(s2,2)*(Power(t1,3) - 6*Power(t1,2)*t2 + 
               16*t1*Power(t2,2) - 5*Power(t2,3)) + 
            s2*t2*(2*Power(t1,3) + 3*Power(t1,2)*t2 + 
               6*t1*Power(t2,2) - 2*Power(t2,3)) + 
            s1*(Power(t1,2)*(5*t1 - 4*t2)*t2 + 
               2*Power(s2,2)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
               s2*(2*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s,5)*(Power(t1,2)*t2*
             (Power(t1,3) + 5*Power(t1,2)*t2 - 4*t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s2,3)*(-2*Power(t1,3) + 9*Power(t1,2)*t2 - 
               25*t1*Power(t2,2) + 10*Power(t2,3)) + 
            Power(s2,2)*(2*Power(t1,4) - Power(t1,3)*t2 + 
               9*Power(t1,2)*Power(t2,2) + 23*t1*Power(t2,3) - 
               9*Power(t2,4)) + 
            s2*t2*(-5*Power(t1,4) - 10*Power(t1,3)*t2 - 
               3*Power(t1,2)*Power(t2,2) + Power(t2,4)) + 
            Power(s1,2)*(2*Power(t1,2)*(5*t1 - 3*t2)*t2 + 
               Power(s2,3)*(-2*t1 + t2) + 
               Power(s2,2)*(6*Power(t1,2) + 9*t1*t2 - 8*Power(t2,2)) + 
               s2*(10*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  31*t1*Power(t2,2) + 9*Power(t2,3))) + 
            s1*(Power(s2,3)*(6*Power(t1,2) + 12*t1*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-10*Power(t1,2) - 4*t1*t2 + 9*Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,3) + 19*Power(t1,2)*t2 - 
                  65*t1*Power(t2,2) + 27*Power(t2,3)) + 
               s2*(-2*Power(t1,4) + 6*Power(t1,2)*Power(t2,2) + 
                  21*t1*Power(t2,3) - 8*Power(t2,4)))) - 
         s*(2*Power(s1,6)*s2*(s2 - t1)*t1*(t1 + t2) + 
            Power(s2,2)*Power(t2,3)*
             (s2*Power(t1,3)*(t1 - 8*t2) + Power(s2,3)*(t1 - t2)*t2 + 
               Power(s2,4)*(-t1 + t2) + 2*Power(t1,4)*(-t1 + t2) + 
               Power(s2,2)*t1*(2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) \
+ Power(s1,5)*(Power(s2,4)*(4*t1 - 2*t2) + 2*Power(t1,4)*t2 + 
               2*s2*Power(t1,3)*(5*t1 + 8*t2) + 
               Power(s2,2)*t1*
                (-17*Power(t1,2) - 38*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(3*Power(t1,2) + 22*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,4)*(Power(s2,5)*(8*t1 - 3*t2) - 
               5*Power(t1,4)*t2*(t1 + t2) + 
               s2*Power(t1,2)*t2*
                (-15*Power(t1,2) - 33*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,4)*(-16*Power(t1,2) + 9*t1*t2 + 
                  26*Power(t2,2)) + 
               2*Power(s2,2)*Power(t1,2)*
                (Power(t1,2) + 22*t1*t2 + 26*Power(t2,2)) + 
               Power(s2,3)*(6*Power(t1,3) - 30*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) - 20*Power(t2,3))) + 
            s1*s2*Power(t2,2)*
             (Power(s2,5)*(4*t1 - 3*t2) - 
               2*Power(t1,3)*t2*
                (4*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               Power(s2,4)*(-10*Power(t1,2) - 16*t1*t2 + 
                  13*Power(t2,2)) + 
               Power(s2,3)*(9*Power(t1,3) + 19*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*Power(t1,2)*
                (6*Power(t1,3) + 13*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-9*Power(t1,3) - 8*Power(t1,2)*t2 + 4*t1*Power(t2,2) + 
                  7*Power(t2,3))) + 
            Power(s1,2)*t2*(2*Power(t1,4)*Power(t2,2)*(-3*t1 + t2) + 
               Power(s2,6)*(-5*t1 + 2*t2) + 
               Power(s2,5)*(17*Power(t1,2) + 25*t1*t2 - 
                  28*Power(t2,2)) + 
               s2*Power(t1,3)*t2*
                (18*Power(t1,2) + 5*t1*t2 - 7*Power(t2,2)) - 
               Power(s2,4)*(24*Power(t1,3) + 40*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 35*Power(t2,3)) + 
               Power(s2,3)*(21*Power(t1,4) + 20*Power(t1,3)*t2 + 
                  20*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-9*Power(t1,4) - 22*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 13*t1*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(s1,3)*(2*Power(s2,6)*t1 + 
               Power(t1,4)*Power(t2,2)*(12*t1 + t2) + 
               Power(s2,5)*(-8*Power(t1,2) - 18*t1*t2 + 
                  19*Power(t2,2)) + 
               Power(s2,4)*(13*Power(t1,3) + 32*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 53*Power(t2,3)) - 
               2*s2*Power(t1,2)*t2*
                (4*Power(t1,3) + 3*Power(t1,2)*t2 - 10*t1*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,2)*t1*
                (4*Power(t1,4) - 28*Power(t1,2)*Power(t2,2) - 
                  t1*Power(t2,3) - 12*Power(t2,4)) + 
               Power(s2,3)*(-11*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  13*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) + 
                  20*Power(t2,4)))) + 
         Power(s,4)*(Power(t1,3)*Power(t2,2)*
             (3*Power(t1,2) + t1*t2 - 4*Power(t2,2)) + 
            Power(s2,4)*(Power(t1,3) - 4*Power(t1,2)*t2 + 
               19*t1*Power(t2,2) - 10*Power(t2,3)) - 
            2*Power(s2,3)*(Power(t1,4) + 3*Power(t1,2)*Power(t2,2) + 
               17*t1*Power(t2,3) - 8*Power(t2,4)) + 
            s2*t1*t2*(-2*Power(t1,4) - 18*Power(t1,3)*t2 + 
               t1*Power(t2,3) + 2*Power(t2,4)) + 
            Power(s2,2)*(Power(t1,5) + 6*Power(t1,4)*t2 + 
               13*Power(t1,3)*Power(t2,2) + 
               19*Power(t1,2)*Power(t2,3) + t1*Power(t2,4) - 
               4*Power(t2,5)) + 
            Power(s1,3)*(Power(s2,3)*(6*t1 - 3*t2) + 
               2*Power(t1,2)*t2*(-5*t1 + 2*t2) - 
               Power(s2,2)*(4*Power(t1,2) + t1*t2 - 12*Power(t2,2)) - 
               s2*(20*Power(t1,3) + 30*Power(t1,2)*t2 - 
                  31*t1*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*(5*t1 - 2*t2) + 
               Power(t1,2)*t2*
                (20*Power(t1,2) + 6*t1*t2 - 9*Power(t2,2)) + 
               Power(s2,3)*(-21*Power(t1,2) - 26*t1*t2 + 
                  29*Power(t2,2)) - 
               Power(s2,2)*(2*Power(t1,3) + 41*Power(t1,2)*t2 - 
                  97*t1*Power(t2,2) + 51*Power(t2,3)) + 
               2*s2*(5*Power(t1,4) + 11*Power(t1,3)*t2 - 
                  15*t1*Power(t2,3) + 5*Power(t2,4))) + 
            s1*(Power(s2,4)*(-6*Power(t1,2) - 14*t1*t2 + 
                  9*Power(t2,2)) + 
               Power(s2,3)*(12*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  95*t1*Power(t2,2) - 54*Power(t2,3)) + 
               Power(t1,2)*t2*
                (-5*Power(t1,3) - 20*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 4*Power(t2,3)) - 
               3*s2*t2*(-5*Power(t1,4) - 5*Power(t1,3)*t2 - 
                  6*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-7*Power(t1,4) + 8*Power(t1,3)*t2 - 
                  29*Power(t1,2)*Power(t2,2) - 67*t1*Power(t2,3) + 
                  34*Power(t2,4)))) + 
         Power(s,3)*(-(Power(s1,4)*
               (Power(s2,3)*(6*t1 - 3*t2) + 
                 Power(t1,2)*t2*(-5*t1 + t2) + 
                 Power(s2,2)*
                  (4*Power(t1,2) + 11*t1*t2 + 8*Power(t2,2)) - 
                 s2*(20*Power(t1,3) + 25*Power(t1,2)*t2 - 
                    17*t1*Power(t2,2) + 2*Power(t2,3)))) + 
            t2*(2*Power(t1,4)*(t1 - t2)*Power(t2,2) + 
               Power(s2,5)*t2*(-7*t1 + 5*t2) + 
               s2*Power(t1,3)*t2*
                (-6*Power(t1,2) - 11*t1*t2 + 8*Power(t2,2)) + 
               2*Power(s2,4)*
                (Power(t1,3) + Power(t1,2)*t2 + 12*t1*Power(t2,2) - 
                  7*Power(t2,3)) + 
               Power(s2,2)*t1*
                (3*Power(t1,4) + 14*Power(t1,3)*t2 + 
                  10*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
                  6*Power(t2,4)) - 
               Power(s2,3)*(5*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  17*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,5)*(-4*t1 + t2) + 
               Power(s2,4)*(22*Power(t1,2) + 34*t1*t2 - 
                  36*Power(t2,2)) - 
               Power(s2,3)*(24*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  110*t1*Power(t2,2) - 99*Power(t2,3)) + 
               2*Power(t1,2)*t2*
                (5*Power(t1,3) + 15*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) - Power(t2,3)) + 
               Power(s2,2)*(8*Power(t1,4) - 31*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 63*t1*Power(t2,3) - 
                  41*Power(t2,4)) + 
               s2*t2*(-10*Power(t1,4) + 21*Power(t1,3)*t2 - 
                  37*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,4)*(-14*t1 + 6*t2) + 
               Power(s2,3)*(25*Power(t1,2) + 2*t1*t2 - 
                  43*Power(t2,2)) + 
               Power(t1,2)*t2*
                (-20*Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(18*Power(t1,3) + 75*Power(t1,2)*t2 - 
                  65*t1*Power(t2,2) + 41*Power(t2,3)) - 
               s2*(20*Power(t1,4) + 48*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            s1*(Power(s2,5)*(2*Power(t1,2) + 8*t1*t2 - 5*Power(t2,2)) + 
               Power(t1,3)*Power(t2,2)*
                (-12*Power(t1,2) - 3*t1*t2 + 8*Power(t2,2)) - 
               Power(s2,4)*(7*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  67*t1*Power(t2,2) - 50*Power(t2,3)) + 
               Power(s2,3)*(9*Power(t1,4) + 10*Power(t1,3)*t2 + 
                  40*Power(t1,2)*Power(t2,2) + 83*t1*Power(t2,3) - 
                  55*Power(t2,4)) + 
               s2*t1*t2*(8*Power(t1,4) + 50*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) - 
                  5*Power(t2,4)) - 
               Power(s2,2)*(4*Power(t1,5) + 16*Power(t1,4)*t2 + 
                  30*Power(t1,3)*Power(t2,2) + 
                  58*Power(t1,2)*Power(t2,3) - 11*t1*Power(t2,4) - 
                  10*Power(t2,5)))) + 
         Power(s,2)*(Power(s1,5)*
             (Power(s2,3)*(2*t1 - t2) - Power(t1,3)*t2 + 
               Power(s2,2)*(6*Power(t1,2) + 9*t1*t2 + 2*Power(t2,2)) + 
               s2*t1*(-10*Power(t1,2) - 11*t1*t2 + 4*Power(t2,2))) + 
            s2*Power(t2,2)*(Power(s2,5)*(t1 - t2) + 
               2*Power(t1,4)*t2*(-2*t1 + t2) + 
               s2*Power(t1,3)*
                (3*Power(t1,2) + 11*t1*t2 - 8*Power(t2,2)) - 
               Power(s2,4)*(Power(t1,2) + 8*t1*t2 - 6*Power(t2,2)) + 
               Power(s2,3)*t2*
                (4*Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,2)*t1*
                (-3*Power(t1,3) - 4*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(s1,4)*(Power(s2,4)*(13*t1 - 6*t2) + 
               Power(t1,3)*t2*(10*t1 + t2) + 
               Power(s2,3)*(-9*Power(t1,2) + 30*t1*t2 + 
                  27*Power(t2,2)) + 
               s2*t1*(20*Power(t1,3) + 42*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 6*Power(t2,3)) - 
               Power(s2,2)*(27*Power(t1,3) + 79*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + 12*Power(t2,3))) + 
            s1*t2*(Power(s2,6)*(-2*t1 + t2) + 
               2*Power(t1,4)*Power(t2,2)*(-3*t1 + 2*t2) + 
               Power(s2,5)*(8*Power(t1,2) + 24*t1*t2 - 21*Power(t2,2)) - 
               Power(s2,4)*(15*Power(t1,3) + 31*Power(t1,2)*t2 + 
                  51*t1*Power(t2,2) - 41*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (18*Power(t1,3) + 19*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,3)*(18*Power(t1,4) + 22*Power(t1,3)*t2 + 
                  47*Power(t1,2)*Power(t2,2) - 13*t1*Power(t2,3) - 
                  12*Power(t2,4)) - 
               Power(s2,2)*t1*
                (9*Power(t1,4) + 32*Power(t1,3)*t2 + 
                  12*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) - 
                  11*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,5)*(10*t1 - 3*t2) - 
               2*Power(t1,3)*t2*
                (5*Power(t1,2) + 10*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,4)*(-29*Power(t1,2) - 18*t1*t2 + 
                  49*Power(t2,2)) + 
               Power(s2,3)*(20*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 75*Power(t2,3)) - 
               s2*t1*t2*(10*Power(t1,3) + 53*Power(t1,2)*t2 - 
                  32*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(-2*Power(t1,4) + 55*Power(t1,3)*t2 + 
                  57*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                  16*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,6)*t1 + 
               Power(t1,3)*Power(t2,2)*
                (18*Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,5)*(-7*Power(t1,2) - 22*t1*t2 + 17*Power(t2,2)) + 
               Power(s2,4)*(15*Power(t1,3) + 41*Power(t1,2)*t2 + 
                  65*t1*Power(t2,2) - 83*Power(t2,3)) - 
               Power(s2,3)*(15*Power(t1,4) + 18*Power(t1,3)*t2 + 
                  37*Power(t1,2)*Power(t2,2) + 44*t1*Power(t2,3) - 
                  59*Power(t2,4)) + 
               s2*t1*t2*(-12*Power(t1,4) - 42*Power(t1,3)*t2 + 
                  28*Power(t1,2)*Power(t2,2) - 17*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,2)*(6*Power(t1,5) + 12*Power(t1,4)*t2 + 
                  5*Power(t1,3)*Power(t2,2) + 
                  44*Power(t1,2)*Power(t2,3) - 22*t1*Power(t2,4) - 
                  6*Power(t2,5)))))*lg2(-s1))/
     ((s - s1)*s1*Power(s2,2)*Power(t1,2)*Power(s - s2 + t1,2)*Power(t2,2)*
       Power(s - s1 + t2,2)) - 
    (4*(Power(s2,3)*(s2 - 2*t1)*(t1 - t2)*Power(t2,3)*(s2 - t1 + t2) + 
         Power(s1,6)*s2*t1*(3*s2 - 3*t1 + 2*t2) - 
         Power(s1,5)*t1*(-5*Power(s2,3) + Power(t1,2)*(t1 - t2) + 
            8*Power(s2,2)*t2 + 
            4*s2*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*s2*(3*Power(s2,5)*t2 - 8*Power(s2,4)*t1*t2 + 
            Power(s2,2)*Power(t1,2)*
             (2*Power(t1,2) + 7*t1*t2 - 21*Power(t2,2)) - 
            Power(s2,3)*t2*(Power(t1,2) - 17*t1*t2 + Power(t2,2)) + 
            2*t1*Power(t1 - t2,2)*t2*
             (4*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) - 
            2*s2*Power(t1 - t2,2)*
             (Power(t1,3) + 5*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
               Power(t2,3))) + 
         Power(s1,4)*(Power(s2,4)*(t1 + t2) + 
            Power(s2,3)*(6*Power(t1,2) - 23*t1*t2 + Power(t2,2)) - 
            Power(s2,2)*t1*(Power(t1,2) - 17*t1*t2 + Power(t2,2)) - 
            Power(t1,3)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
            s2*t1*(-5*Power(t1,3) + 16*Power(t1,2)*t2 - 
               27*t1*Power(t2,2) + 12*Power(t2,3))) + 
         Power(s1,3)*(5*Power(s2,5)*t2 + 
            2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
            Power(s2,2)*Power(t2,2)*
             (-21*Power(t1,2) + 7*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,4)*(Power(t1,2) - 23*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,3)*(3*Power(t1,3) + 10*Power(t1,2)*t2 + 
               10*t1*Power(t2,2) + 3*Power(t2,3)) + 
            s2*t1*(-4*Power(t1,4) + 17*Power(t1,3)*t2 - 
               28*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
               10*Power(t2,4))) + 
         s1*Power(s2,2)*t2*(Power(s2,4)*(2*t1 - 3*t2) - 
            4*Power(s2,3)*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            2*t1*Power(t1 - t2,2)*
             (2*Power(t1,2) - 3*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(12*Power(t1,3) - 27*Power(t1,2)*t2 + 
               16*t1*Power(t2,2) - 5*Power(t2,3)) + 
            s2*(-10*Power(t1,4) + 25*Power(t1,3)*t2 - 
               28*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) - 
               4*Power(t2,4))) + 
         Power(s,4)*(s2*Power(t1 - t2,2)*t2*(s2 - t1 + t2) + 
            Power(s1,2)*(t1*Power(t1 - t2,2) + 
               2*Power(s2,2)*(t1 + t2) - 
               2*s2*(Power(t1,2) - Power(t2,2))) + 
            s1*(t1 - t2)*(t1*Power(t1 - t2,2) + 
               2*Power(s2,2)*(t1 + t2) - 2*s2*(Power(t1,2) - Power(t2,2))\
)) - Power(s,3)*(s2*(t1 - t2)*t2*
             (Power(s2,2)*(2*t1 - 3*t2) + 
               Power(t1 - t2,2)*(2*t1 + t2) - 
               2*s2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
            Power(s1,3)*(2*Power(s2,2)*(4*t1 + t2) + 
               t1*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
               s2*(-8*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(2*t1*(t1 - 2*t2)*Power(t1 - t2,2) + 
               2*Power(s2,3)*(t1 + 4*t2) + 
               4*Power(s2,2)*(Power(t1,2) - 7*t1*t2 + Power(t2,2)) - 
               s2*(5*Power(t1,3) - 23*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*(-(t1*Power(t1 - t2,3)*(t1 + 2*t2)) + 
               2*Power(s2,3)*(Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
               3*s2*Power(t1 - t2,2)*
                (Power(t1,2) + 4*t1*t2 + Power(t2,2)) - 
               Power(s2,2)*(4*Power(t1,3) + 14*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*t1*
             (13*Power(s2,2) - 13*s2*t1 + 3*Power(t1,2) + 8*s2*t2 - 
               4*t1*t2 + Power(t2,2)) + 
            Power(s1,3)*(8*Power(s2,3)*(t1 + t2) - 
               3*t1*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(3*Power(t1,2) - 49*t1*t2 + 7*Power(t2,2)) - 
               s2*(8*Power(t1,3) - 48*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + Power(t2,3))) + 
            s2*(t1 - t2)*t2*(Power(s2,3)*(t1 - 3*t2) - 
               3*Power(s2,2)*t1*(t1 - 2*t2) - 
               2*t1*Power(t1 - t2,2)*t2 + 
               s2*(2*Power(t1,3) - 2*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 3*Power(t2,3))) + 
            s1*(Power(s2,4)*(8*t1 - 13*t2)*t2 + 
               2*Power(t1,2)*Power(t1 - t2,3)*t2 - 
               s2*Power(t1 - t2,2)*
                (4*Power(t1,3) + 7*Power(t1,2)*t2 + 7*t1*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(s2,3)*(Power(t1,3) + 33*Power(t1,2)*t2 - 
                  48*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,4) + 26*Power(t1,3)*t2 - 
                  44*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,2)*(13*Power(s2,4)*t2 - 
               t1*Power(t1 - t2,2)*(3*Power(t1,2) - 2*Power(t2,2)) + 
               Power(s2,3)*(7*Power(t1,2) - 49*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,3) + 20*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*(Power(t1,4) + 13*Power(t1,3)*t2 - 
                  44*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) + 
                  4*Power(t2,4)))) - 
         s*(Power(s1,5)*t1*(10*Power(s2,2) - 10*s2*t1 + Power(t1,2) + 
               6*s2*t2 - t1*t2) + 
            Power(s1,4)*(-2*Power(t1,4) + 2*Power(t1,2)*Power(t2,2) + 
               Power(s2,3)*(11*t1 + t2) + 
               s2*t1*(-9*Power(t1,2) + 43*t1*t2 - 24*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 30*t1*t2 + Power(t2,2))) - 
            Power(s2,2)*(t1 - t2)*Power(t2,2)*
             (Power(s2,3) - 2*Power(s2,2)*(t1 + t2) + 
               2*t1*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) - 
               s2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(Power(s2,4)*(t1 + 11*t2) + 
               Power(s2,3)*(11*Power(t1,2) - 68*t1*t2 + 
                  11*Power(t2,2)) + 
               Power(t1,2)*(-3*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-3*Power(t1,3) + 47*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-7*Power(t1,4) + 22*Power(t1,3)*t2 - 
                  49*Power(t1,2)*Power(t2,2) + 28*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,2)*(10*Power(s2,5)*t2 + 
               2*Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2)*t2 + 
               Power(s2,4)*(Power(t1,2) - 30*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  47*t1*Power(t2,2) - 3*Power(t2,3)) + 
               4*Power(s2,2)*
                (Power(t1,4) + 5*Power(t1,3)*t2 - 
                  15*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               2*s2*(4*Power(t1,5) - 8*Power(t1,4)*t2 + 
                  10*Power(t1,3)*Power(t2,2) - 
                  14*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) + 
                  Power(t2,5))) + 
            s1*s2*(2*Power(s2,4)*(3*t1 - 5*t2)*t2 + 
               Power(s2,3)*t2*
                (-24*Power(t1,2) + 43*t1*t2 - 9*Power(t2,2)) + 
               8*t1*Power(t1 - t2,2)*t2*
                (Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,4) + 28*Power(t1,3)*t2 - 
                  49*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                  7*Power(t2,4)) - 
               2*s2*(Power(t1,5) + 7*Power(t1,4)*t2 - 
                  14*Power(t1,3)*Power(t2,2) + 
                  10*Power(t1,2)*Power(t2,3) - 8*t1*Power(t2,4) + 
                  4*Power(t2,5)))))*lg2(s/(s - s1 - s2)))/
     (Power(s1,2)*Power(s2,2)*t1*(s - s2 + t1)*(s1 + t1 - t2)*t2*
       (s - s1 + t2)*(s2 - t1 + t2)) - 
    (8*(2*Power(s1,5)*s2*(s2 - t1)*t1 + 
         Power(s1,4)*t1*(3*Power(s2,3) + Power(s2,2)*(t1 - 7*t2) + 
            Power(t1,2)*(-t1 + t2) + s2*t1*(-3*t1 + 8*t2)) + 
         Power(s1,3)*(Power(s2,4)*t2 + Power(s2,2)*t1*t2*(7*t1 + 5*t2) + 
            s2*Power(t1,2)*(-4*Power(t1,2) + 8*t1*t2 - 9*Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,2) - 16*t1*t2 + Power(t2,2)) - 
            Power(t1,3)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*(2*Power(s2,5)*t2 + 
            2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
            Power(s2,4)*t2*(-7*t1 + t2) + 
            Power(s2,3)*(4*Power(t1,3) - 7*Power(t1,2)*t2 + 
               17*t1*Power(t2,2) - Power(t2,3)) + 
            Power(s2,2)*t1*(-Power(t1,3) + 4*Power(t1,2)*t2 - 
               12*t1*Power(t2,2) + Power(t2,3)) + 
            s2*Power(t1,2)*(-3*Power(t1,3) + 9*Power(t1,2)*t2 - 
               10*t1*Power(t2,2) + 5*Power(t2,3))) - 
         Power(s2,2)*t2*(Power(s2,4)*(-2*t1 + t2) + 
            Power(s2,3)*(8*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(-12*Power(t1,3) + 18*Power(t1,2)*t2 - 
               8*t1*Power(t2,2) + Power(t2,3)) + 
            s2*(10*Power(t1,4) - 20*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
               Power(t2,4)) - 
            2*t1*(2*Power(t1,4) - 5*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + Power(t2,4)\
)) + s1*s2*(Power(s2,5)*t2 - Power(s2,4)*t2*(t1 + t2) - 
            Power(s2,3)*t2*(6*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
            2*Power(t1,2)*t2*
             (3*Power(t1,3) - 7*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s2,2)*(2*Power(t1,4) + 6*Power(t1,3)*t2 - 
               9*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
               Power(t2,4)) - 
            s2*t1*(2*Power(t1,4) + 5*Power(t1,3)*t2 - 
               10*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
               Power(t2,4))) + 
         Power(s,4)*(s1*t1*(2*Power(s2,2) + Power(t1 - t2,2) + 
               s2*(-t1 + t2)) + 
            (t1 - t2)*(t1*Power(t1 - t2,2) + Power(s2,2)*(2*t1 + t2) + 
               s2*(-Power(t1,2) + Power(t2,2)))) + 
         Power(s,3)*(-3*s2*t1*Power(t1 - t2,2)*(t1 + 2*t2) + 
            t1*Power(t1 - t2,3)*(t1 + 2*t2) + 
            Power(s2,3)*(-2*Power(t1,2) - 3*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,3) + 13*Power(t1,2)*t2 - 
               19*t1*Power(t2,2) + 4*Power(t2,3)) - 
            Power(s1,2)*t1*(7*Power(s2,2) + 3*Power(t1,2) - 5*t1*t2 + 
               2*Power(t2,2) + s2*(-4*t1 + t2)) + 
            s1*(-2*t1*(t1 - 2*t2)*Power(t1 - t2,2) - 
               Power(s2,3)*(2*t1 + t2) + 
               Power(s2,2)*(-5*Power(t1,2) + 14*t1*t2 + Power(t2,2)) + 
               s2*(Power(t1,3) - 4*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)))) + 
         Power(s,2)*(3*Power(s2,4)*(3*t1 - 2*t2)*t2 + 
            2*Power(t1,2)*Power(t1 - t2,3)*t2 + 
            Power(s2,3)*t2*(-31*Power(t1,2) + 36*t1*t2 - 
               6*Power(t2,2)) - 
            s2*Power(t1 - t2,2)*
             (3*Power(t1,3) + 10*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
               Power(t2,3)) + 
            Power(s2,2)*(4*Power(t1,4) + 20*Power(t1,3)*t2 - 
               36*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) - 
               Power(t2,4)) + 
            Power(s1,3)*t1*(10*Power(s2,2) + 3*Power(t1,2) - 4*t1*t2 + 
               Power(t2,2) - s2*(7*t1 + t2)) + 
            Power(s1,2)*(2*Power(s2,3)*(3*t1 + t2) + 
               Power(s2,2)*(6*Power(t1,2) - 32*t1*t2 + Power(t2,2)) - 
               3*t1*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) - 
               s2*(2*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) + Power(t2,3))) + 
            s1*(3*Power(s2,4)*t2 + 
               Power(s2,3)*(6*Power(t1,2) - 17*t1*t2 - 3*Power(t2,2)) - 
               t1*Power(t1 - t2,2)*(3*Power(t1,2) - 2*Power(t2,2)) - 
               2*Power(s2,2)*t2*
                (6*Power(t1,2) - 16*t1*t2 + 3*Power(t2,2)) + 
               2*s2*t1*(Power(t1,3) - 4*t1*Power(t2,2) + 3*Power(t2,3)))) \
- s*(Power(s1,4)*t1*(7*Power(s2,2) + t1*(t1 - t2) - s2*(6*t1 + t2)) + 
            Power(s1,3)*(-2*Power(t1,4) + 2*Power(t1,2)*Power(t2,2) + 
               Power(s2,3)*(7*t1 + t2) + 
               s2*Power(t1,2)*(-5*t1 + 17*t2) + 
               Power(s2,2)*(4*Power(t1,2) - 26*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(4*Power(s2,4)*t2 + 
               Power(s2,3)*(9*Power(t1,2) - 34*t1*t2 + 2*Power(t2,2)) + 
               2*Power(s2,2)*
                (Power(t1,3) + 3*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(t1,2)*(-3*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t1*(-5*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(3*Power(s2,5)*t2 + 
               2*Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2)*t2 - 
               Power(s2,4)*t2*(4*t1 + 3*t2) + 
               Power(s2,3)*(4*Power(t1,3) - 26*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*t1*(-6*Power(t1,4) + 5*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(3*Power(t1,4) + 16*Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            s2*(Power(s2,4)*(7*t1 - 4*t2)*t2 + 
               2*t1*Power(t1 - t2,2)*t2*
                (3*Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
               2*Power(s2,3)*t2*
                (12*Power(t1,2) - 13*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,4) + 26*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) - 
                  2*Power(t2,4)) - 
               s2*(2*Power(t1,5) + 15*Power(t1,4)*t2 - 
                  34*Power(t1,3)*Power(t2,2) + 
                  26*Power(t1,2)*Power(t2,3) - 11*t1*Power(t2,4) + 
                  2*Power(t2,5)))))*lg2(-(s/(s1*(s - s1 - s2)))))/
     (s1*Power(s2,2)*t1*(s - s2 + t1)*(-s + s1 - t2)*(s1 + t1 - t2)*t2*
       (s2 - t1 + t2)) + (4*(2*Power(s1,5)*s2*(s2 - t1)*t1 + 
         Power(s1,4)*t1*(3*Power(s2,3) + Power(s2,2)*(t1 - 7*t2) + 
            Power(t1,2)*(-t1 + t2) + s2*t1*(-3*t1 + 8*t2)) + 
         Power(s1,3)*(Power(s2,4)*t2 + Power(s2,2)*t1*t2*(7*t1 + 5*t2) + 
            s2*Power(t1,2)*(-4*Power(t1,2) + 8*t1*t2 - 9*Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,2) - 16*t1*t2 + Power(t2,2)) - 
            Power(t1,3)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*(2*Power(s2,5)*t2 + 
            2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
            Power(s2,4)*t2*(-7*t1 + t2) + 
            Power(s2,3)*(4*Power(t1,3) - 7*Power(t1,2)*t2 + 
               17*t1*Power(t2,2) - Power(t2,3)) + 
            Power(s2,2)*t1*(-Power(t1,3) + 4*Power(t1,2)*t2 - 
               12*t1*Power(t2,2) + Power(t2,3)) + 
            s2*Power(t1,2)*(-3*Power(t1,3) + 9*Power(t1,2)*t2 - 
               10*t1*Power(t2,2) + 5*Power(t2,3))) - 
         Power(s2,2)*t2*(Power(s2,4)*(-2*t1 + t2) + 
            Power(s2,3)*(8*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(-12*Power(t1,3) + 18*Power(t1,2)*t2 - 
               8*t1*Power(t2,2) + Power(t2,3)) + 
            s2*(10*Power(t1,4) - 20*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
               Power(t2,4)) - 
            2*t1*(2*Power(t1,4) - 5*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + Power(t2,4)\
)) + s1*s2*(Power(s2,5)*t2 - Power(s2,4)*t2*(t1 + t2) - 
            Power(s2,3)*t2*(6*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
            2*Power(t1,2)*t2*
             (3*Power(t1,3) - 7*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s2,2)*(2*Power(t1,4) + 6*Power(t1,3)*t2 - 
               9*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
               Power(t2,4)) - 
            s2*t1*(2*Power(t1,4) + 5*Power(t1,3)*t2 - 
               10*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
               Power(t2,4))) + 
         Power(s,4)*(s1*t1*(2*Power(s2,2) + Power(t1 - t2,2) + 
               s2*(-t1 + t2)) + 
            (t1 - t2)*(t1*Power(t1 - t2,2) + Power(s2,2)*(2*t1 + t2) + 
               s2*(-Power(t1,2) + Power(t2,2)))) + 
         Power(s,3)*(-3*s2*t1*Power(t1 - t2,2)*(t1 + 2*t2) + 
            t1*Power(t1 - t2,3)*(t1 + 2*t2) + 
            Power(s2,3)*(-2*Power(t1,2) - 3*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,3) + 13*Power(t1,2)*t2 - 
               19*t1*Power(t2,2) + 4*Power(t2,3)) - 
            Power(s1,2)*t1*(7*Power(s2,2) + 3*Power(t1,2) - 5*t1*t2 + 
               2*Power(t2,2) + s2*(-4*t1 + t2)) + 
            s1*(-2*t1*(t1 - 2*t2)*Power(t1 - t2,2) - 
               Power(s2,3)*(2*t1 + t2) + 
               Power(s2,2)*(-5*Power(t1,2) + 14*t1*t2 + Power(t2,2)) + 
               s2*(Power(t1,3) - 4*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)))) + 
         Power(s,2)*(3*Power(s2,4)*(3*t1 - 2*t2)*t2 + 
            2*Power(t1,2)*Power(t1 - t2,3)*t2 + 
            Power(s2,3)*t2*(-31*Power(t1,2) + 36*t1*t2 - 
               6*Power(t2,2)) - 
            s2*Power(t1 - t2,2)*
             (3*Power(t1,3) + 10*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
               Power(t2,3)) + 
            Power(s2,2)*(4*Power(t1,4) + 20*Power(t1,3)*t2 - 
               36*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) - 
               Power(t2,4)) + 
            Power(s1,3)*t1*(10*Power(s2,2) + 3*Power(t1,2) - 4*t1*t2 + 
               Power(t2,2) - s2*(7*t1 + t2)) + 
            Power(s1,2)*(2*Power(s2,3)*(3*t1 + t2) + 
               Power(s2,2)*(6*Power(t1,2) - 32*t1*t2 + Power(t2,2)) - 
               3*t1*t2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) - 
               s2*(2*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) + Power(t2,3))) + 
            s1*(3*Power(s2,4)*t2 + 
               Power(s2,3)*(6*Power(t1,2) - 17*t1*t2 - 3*Power(t2,2)) - 
               t1*Power(t1 - t2,2)*(3*Power(t1,2) - 2*Power(t2,2)) - 
               2*Power(s2,2)*t2*
                (6*Power(t1,2) - 16*t1*t2 + 3*Power(t2,2)) + 
               2*s2*t1*(Power(t1,3) - 4*t1*Power(t2,2) + 3*Power(t2,3)))) \
- s*(Power(s1,4)*t1*(7*Power(s2,2) + t1*(t1 - t2) - s2*(6*t1 + t2)) + 
            Power(s1,3)*(-2*Power(t1,4) + 2*Power(t1,2)*Power(t2,2) + 
               Power(s2,3)*(7*t1 + t2) + 
               s2*Power(t1,2)*(-5*t1 + 17*t2) + 
               Power(s2,2)*(4*Power(t1,2) - 26*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(4*Power(s2,4)*t2 + 
               Power(s2,3)*(9*Power(t1,2) - 34*t1*t2 + 2*Power(t2,2)) + 
               2*Power(s2,2)*
                (Power(t1,3) + 3*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(t1,2)*(-3*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t1*(-5*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  10*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(3*Power(s2,5)*t2 + 
               2*Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2)*t2 - 
               Power(s2,4)*t2*(4*t1 + 3*t2) + 
               Power(s2,3)*(4*Power(t1,3) - 26*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*t1*(-6*Power(t1,4) + 5*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(3*Power(t1,4) + 16*Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            s2*(Power(s2,4)*(7*t1 - 4*t2)*t2 + 
               2*t1*Power(t1 - t2,2)*t2*
                (3*Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
               2*Power(s2,3)*t2*
                (12*Power(t1,2) - 13*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,4) + 26*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) - 
                  2*Power(t2,4)) - 
               s2*(2*Power(t1,5) + 15*Power(t1,4)*t2 - 
                  34*Power(t1,3)*Power(t2,2) + 
                  26*Power(t1,2)*Power(t2,3) - 11*t1*Power(t2,4) + 
                  2*Power(t2,5)))))*lg2((s - s1 - s2)/s1))/
     (s1*Power(s2,2)*t1*(s - s2 + t1)*(-s + s1 - t2)*(s1 + t1 - t2)*t2*
       (s2 - t1 + t2)) + (4*(Power(s2,2)*(s2 - 2*t1)*(t1 - t2)*
          Power(t2,3)*(s2 - t1 + t2) + Power(s1,6)*t1*(s2 - t1 + 2*t2) - 
         Power(s1,5)*t1*(-2*Power(s2,2) + Power(t1,2) - 8*t1*t2 + 
            8*Power(t2,2) + s2*(t1 + t2)) + 
         Power(s1,4)*t1*(Power(s2,3) - Power(t1,3) + 
            Power(s2,2)*(t1 - 7*t2) + 8*Power(t1,2)*t2 - 
            18*t1*Power(t2,2) + 12*Power(t2,3) - 
            s2*(Power(t1,2) - 10*t1*t2 + 6*Power(t2,2))) - 
         s1*s2*Power(t2,2)*(2*Power(s2,4) + Power(s2,3)*(-8*t1 + 3*t2) + 
            Power(s2,2)*(9*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2)) + 
            2*t1*(2*Power(t1,3) - 6*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            s2*(-5*Power(t1,3) + 10*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
               3*Power(t2,3))) + 
         Power(s1,2)*t2*(2*Power(s2,5) + Power(s2,4)*(-7*t1 + t2) + 
            Power(s2,3)*t1*(5*t1 + 7*t2) + 
            Power(s2,2)*(Power(t1,3) - 12*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - Power(t2,3)) + 
            2*t1*(Power(t1,4) - 4*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
               2*Power(t2,4)) - 
            s2*(Power(t1,4) + 2*Power(t1,3)*t2 - 
               10*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
               2*Power(t2,4))) + 
         Power(s1,3)*(3*Power(s2,4)*t2 + 
            Power(s2,3)*(Power(t1,2) - 16*t1*t2 + 5*Power(t2,2)) - 
            Power(s2,2)*(Power(t1,3) - 17*Power(t1,2)*t2 + 
               7*t1*Power(t2,2) - 4*Power(t2,3)) + 
            s2*(Power(t1,4) - 4*Power(t1,3)*t2 - 
               9*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) + 
               2*Power(t2,4)) - 
            t1*(Power(t1,4) - 8*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
               10*Power(t2,4))) + 
         Power(s,4)*(Power(t1 - t2,2)*t2*(s2 - t1 + t2) - 
            Power(s1,2)*(Power(t1,2) + t1*t2 - 2*t2*(s2 + t2)) - 
            s1*(t1 - t2)*(Power(t1,2) - t2*(s2 + t2))) + 
         Power(s,3)*(-(Power(s1,3)*
               (-4*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2) + 
                 s2*(t1 + 2*t2))) + 
            Power(s1,2)*(4*Power(t1,3) - 7*Power(s2,2)*t2 - 
               19*Power(t1,2)*t2 + 13*t1*Power(t2,2) + 2*Power(t2,3) + 
               s2*(Power(t1,2) + 14*t1*t2 - 5*Power(t2,2))) - 
            (t1 - t2)*t2*(Power(s2,2)*(2*t1 - 3*t2) + 
               Power(t1 - t2,2)*(2*t1 + t2) - 
               2*s2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
            s1*(-3*Power(t1 - t2,2)*t2*(2*t1 + t2) + 
               Power(s2,2)*t2*(-t1 + 4*t2) + 
               s2*(2*Power(t1,3) + Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  Power(t2,3)))) + 
         Power(s,2)*(3*Power(s1,4)*t1*(s2 - 2*t1 + 3*t2) + 
            Power(s1,3)*(2*Power(s2,2)*(t1 + 3*t2) + 
               t1*(-6*Power(t1,2) + 36*t1*t2 - 31*Power(t2,2)) + 
               s2*(-3*Power(t1,2) - 17*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(-Power(t1,4) + 10*Power(s2,3)*t2 + 
               13*Power(t1,3)*t2 - 36*Power(t1,2)*Power(t2,2) + 
               20*t1*Power(t2,3) + 4*Power(t2,4) + 
               Power(s2,2)*(Power(t1,2) - 32*t1*t2 + 6*Power(t2,2)) - 
               2*s2*t1*(3*Power(t1,2) - 16*t1*t2 + 6*Power(t2,2))) + 
            (t1 - t2)*t2*(Power(s2,3)*(t1 - 3*t2) - 
               3*Power(s2,2)*t1*(t1 - 2*t2) - 
               2*t1*Power(t1 - t2,2)*t2 + 
               s2*(2*Power(t1,3) - 2*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 3*Power(t2,3))) - 
            s1*(Power(s2,3)*t2*(t1 + 7*t2) - 
               2*s2*t2*(3*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  Power(t2,3)) + 
               Power(s2,2)*(Power(t1,3) + 2*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1 - t2,2)*
                (Power(t1,3) - 3*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
                  3*Power(t2,3)))) - 
         s*(Power(s1,5)*t1*(3*s2 - 4*t1 + 7*t2) - 
            Power(s1,4)*t1*(-4*Power(s2,2) + 3*s2*t1 + 4*Power(t1,2) + 
               4*s2*t2 - 26*t1*t2 + 24*Power(t2,2)) - 
            s2*(t1 - t2)*Power(t2,2)*
             (Power(s2,3) - 2*Power(s2,2)*(t1 + t2) + 
               2*t1*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) - 
               s2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(-2*Power(t1,4) + 14*Power(t1,3)*t2 - 
               39*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) + 
               2*Power(t2,4) + Power(s2,3)*(t1 + 7*t2) + 
               Power(s2,2)*(2*Power(t1,2) - 34*t1*t2 + 9*Power(t2,2)) + 
               s2*(-5*Power(t1,3) + 41*Power(t1,2)*t2 - 
                  26*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*t2*(Power(s2,3)*(17*t1 - 5*t2)*t2 - 
               Power(s2,4)*(t1 + 6*t2) + 
               2*t1*Power(t1 - t2,2)*
                (Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) - 10*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*(Power(t1,4) - 6*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s1,2)*(-2*Power(t1,5) + 7*Power(s2,4)*t2 + 
               11*Power(t1,4)*t2 - 26*Power(t1,3)*Power(t2,2) + 
               34*Power(t1,2)*Power(t2,3) - 15*t1*Power(t2,4) - 
               2*Power(t2,5) + 
               Power(s2,3)*(Power(t1,2) - 26*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(-2*Power(t1,3) + 22*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(Power(t1,4) + 4*Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) + 
                  3*Power(t2,4)))))*lg2(s/s2))/
     (Power(s1,2)*s2*(-s + s2 - t1)*t1*(s1 + t1 - t2)*t2*(s - s1 + t2)*
       (s2 - t1 + t2)) - (8*(Power(s2,2)*(s2 - 2*t1)*(t1 - t2)*
          Power(t2,3)*(s2 - t1 + t2) + Power(s1,6)*t1*(s2 - t1 + 2*t2) - 
         Power(s1,5)*t1*(-2*Power(s2,2) + Power(t1,2) - 8*t1*t2 + 
            8*Power(t2,2) + s2*(t1 + t2)) + 
         Power(s1,4)*t1*(Power(s2,3) - Power(t1,3) + 
            Power(s2,2)*(t1 - 7*t2) + 8*Power(t1,2)*t2 - 
            18*t1*Power(t2,2) + 12*Power(t2,3) - 
            s2*(Power(t1,2) - 10*t1*t2 + 6*Power(t2,2))) - 
         s1*s2*Power(t2,2)*(2*Power(s2,4) + Power(s2,3)*(-8*t1 + 3*t2) + 
            Power(s2,2)*(9*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2)) + 
            2*t1*(2*Power(t1,3) - 6*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            s2*(-5*Power(t1,3) + 10*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
               3*Power(t2,3))) + 
         Power(s1,2)*t2*(2*Power(s2,5) + Power(s2,4)*(-7*t1 + t2) + 
            Power(s2,3)*t1*(5*t1 + 7*t2) + 
            Power(s2,2)*(Power(t1,3) - 12*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - Power(t2,3)) + 
            2*t1*(Power(t1,4) - 4*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
               2*Power(t2,4)) - 
            s2*(Power(t1,4) + 2*Power(t1,3)*t2 - 
               10*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
               2*Power(t2,4))) + 
         Power(s1,3)*(3*Power(s2,4)*t2 + 
            Power(s2,3)*(Power(t1,2) - 16*t1*t2 + 5*Power(t2,2)) - 
            Power(s2,2)*(Power(t1,3) - 17*Power(t1,2)*t2 + 
               7*t1*Power(t2,2) - 4*Power(t2,3)) + 
            s2*(Power(t1,4) - 4*Power(t1,3)*t2 - 
               9*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) + 
               2*Power(t2,4)) - 
            t1*(Power(t1,4) - 8*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
               10*Power(t2,4))) + 
         Power(s,4)*(Power(t1 - t2,2)*t2*(s2 - t1 + t2) - 
            Power(s1,2)*(Power(t1,2) + t1*t2 - 2*t2*(s2 + t2)) - 
            s1*(t1 - t2)*(Power(t1,2) - t2*(s2 + t2))) + 
         Power(s,3)*(-(Power(s1,3)*
               (-4*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2) + 
                 s2*(t1 + 2*t2))) + 
            Power(s1,2)*(4*Power(t1,3) - 7*Power(s2,2)*t2 - 
               19*Power(t1,2)*t2 + 13*t1*Power(t2,2) + 2*Power(t2,3) + 
               s2*(Power(t1,2) + 14*t1*t2 - 5*Power(t2,2))) - 
            (t1 - t2)*t2*(Power(s2,2)*(2*t1 - 3*t2) + 
               Power(t1 - t2,2)*(2*t1 + t2) - 
               2*s2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
            s1*(-3*Power(t1 - t2,2)*t2*(2*t1 + t2) + 
               Power(s2,2)*t2*(-t1 + 4*t2) + 
               s2*(2*Power(t1,3) + Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  Power(t2,3)))) + 
         Power(s,2)*(3*Power(s1,4)*t1*(s2 - 2*t1 + 3*t2) + 
            Power(s1,3)*(2*Power(s2,2)*(t1 + 3*t2) + 
               t1*(-6*Power(t1,2) + 36*t1*t2 - 31*Power(t2,2)) + 
               s2*(-3*Power(t1,2) - 17*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(-Power(t1,4) + 10*Power(s2,3)*t2 + 
               13*Power(t1,3)*t2 - 36*Power(t1,2)*Power(t2,2) + 
               20*t1*Power(t2,3) + 4*Power(t2,4) + 
               Power(s2,2)*(Power(t1,2) - 32*t1*t2 + 6*Power(t2,2)) - 
               2*s2*t1*(3*Power(t1,2) - 16*t1*t2 + 6*Power(t2,2))) + 
            (t1 - t2)*t2*(Power(s2,3)*(t1 - 3*t2) - 
               3*Power(s2,2)*t1*(t1 - 2*t2) - 
               2*t1*Power(t1 - t2,2)*t2 + 
               s2*(2*Power(t1,3) - 2*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 3*Power(t2,3))) - 
            s1*(Power(s2,3)*t2*(t1 + 7*t2) - 
               2*s2*t2*(3*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  Power(t2,3)) + 
               Power(s2,2)*(Power(t1,3) + 2*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1 - t2,2)*
                (Power(t1,3) - 3*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
                  3*Power(t2,3)))) - 
         s*(Power(s1,5)*t1*(3*s2 - 4*t1 + 7*t2) - 
            Power(s1,4)*t1*(-4*Power(s2,2) + 3*s2*t1 + 4*Power(t1,2) + 
               4*s2*t2 - 26*t1*t2 + 24*Power(t2,2)) - 
            s2*(t1 - t2)*Power(t2,2)*
             (Power(s2,3) - 2*Power(s2,2)*(t1 + t2) + 
               2*t1*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) - 
               s2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(-2*Power(t1,4) + 14*Power(t1,3)*t2 - 
               39*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) + 
               2*Power(t2,4) + Power(s2,3)*(t1 + 7*t2) + 
               Power(s2,2)*(2*Power(t1,2) - 34*t1*t2 + 9*Power(t2,2)) + 
               s2*(-5*Power(t1,3) + 41*Power(t1,2)*t2 - 
                  26*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*t2*(Power(s2,3)*(17*t1 - 5*t2)*t2 - 
               Power(s2,4)*(t1 + 6*t2) + 
               2*t1*Power(t1 - t2,2)*
                (Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) - 10*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*(Power(t1,4) - 6*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s1,2)*(-2*Power(t1,5) + 7*Power(s2,4)*t2 + 
               11*Power(t1,4)*t2 - 26*Power(t1,3)*Power(t2,2) + 
               34*Power(t1,2)*Power(t2,3) - 15*t1*Power(t2,4) - 
               2*Power(t2,5) + 
               Power(s2,3)*(Power(t1,2) - 26*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(-2*Power(t1,3) + 22*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(Power(t1,4) + 4*Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) + 
                  3*Power(t2,4)))))*lg2(-(s/((s - s1 - s2)*s2))))/
     (Power(s1,2)*s2*(-s + s2 - t1)*t1*(s1 + t1 - t2)*t2*(s - s1 + t2)*
       (s2 - t1 + t2)) + (4*(Power(s2,2)*(s2 - 2*t1)*(t1 - t2)*
          Power(t2,3)*(s2 - t1 + t2) + Power(s1,6)*t1*(s2 - t1 + 2*t2) - 
         Power(s1,5)*t1*(-2*Power(s2,2) + Power(t1,2) - 8*t1*t2 + 
            8*Power(t2,2) + s2*(t1 + t2)) + 
         Power(s1,4)*t1*(Power(s2,3) - Power(t1,3) + 
            Power(s2,2)*(t1 - 7*t2) + 8*Power(t1,2)*t2 - 
            18*t1*Power(t2,2) + 12*Power(t2,3) - 
            s2*(Power(t1,2) - 10*t1*t2 + 6*Power(t2,2))) - 
         s1*s2*Power(t2,2)*(2*Power(s2,4) + Power(s2,3)*(-8*t1 + 3*t2) + 
            Power(s2,2)*(9*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2)) + 
            2*t1*(2*Power(t1,3) - 6*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            s2*(-5*Power(t1,3) + 10*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
               3*Power(t2,3))) + 
         Power(s1,2)*t2*(2*Power(s2,5) + Power(s2,4)*(-7*t1 + t2) + 
            Power(s2,3)*t1*(5*t1 + 7*t2) + 
            Power(s2,2)*(Power(t1,3) - 12*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - Power(t2,3)) + 
            2*t1*(Power(t1,4) - 4*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
               2*Power(t2,4)) - 
            s2*(Power(t1,4) + 2*Power(t1,3)*t2 - 
               10*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
               2*Power(t2,4))) + 
         Power(s1,3)*(3*Power(s2,4)*t2 + 
            Power(s2,3)*(Power(t1,2) - 16*t1*t2 + 5*Power(t2,2)) - 
            Power(s2,2)*(Power(t1,3) - 17*Power(t1,2)*t2 + 
               7*t1*Power(t2,2) - 4*Power(t2,3)) + 
            s2*(Power(t1,4) - 4*Power(t1,3)*t2 - 
               9*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) + 
               2*Power(t2,4)) - 
            t1*(Power(t1,4) - 8*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
               10*Power(t2,4))) + 
         Power(s,4)*(Power(t1 - t2,2)*t2*(s2 - t1 + t2) - 
            Power(s1,2)*(Power(t1,2) + t1*t2 - 2*t2*(s2 + t2)) - 
            s1*(t1 - t2)*(Power(t1,2) - t2*(s2 + t2))) + 
         Power(s,3)*(-(Power(s1,3)*
               (-4*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2) + 
                 s2*(t1 + 2*t2))) + 
            Power(s1,2)*(4*Power(t1,3) - 7*Power(s2,2)*t2 - 
               19*Power(t1,2)*t2 + 13*t1*Power(t2,2) + 2*Power(t2,3) + 
               s2*(Power(t1,2) + 14*t1*t2 - 5*Power(t2,2))) - 
            (t1 - t2)*t2*(Power(s2,2)*(2*t1 - 3*t2) + 
               Power(t1 - t2,2)*(2*t1 + t2) - 
               2*s2*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
            s1*(-3*Power(t1 - t2,2)*t2*(2*t1 + t2) + 
               Power(s2,2)*t2*(-t1 + 4*t2) + 
               s2*(2*Power(t1,3) + Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  Power(t2,3)))) + 
         Power(s,2)*(3*Power(s1,4)*t1*(s2 - 2*t1 + 3*t2) + 
            Power(s1,3)*(2*Power(s2,2)*(t1 + 3*t2) + 
               t1*(-6*Power(t1,2) + 36*t1*t2 - 31*Power(t2,2)) + 
               s2*(-3*Power(t1,2) - 17*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(-Power(t1,4) + 10*Power(s2,3)*t2 + 
               13*Power(t1,3)*t2 - 36*Power(t1,2)*Power(t2,2) + 
               20*t1*Power(t2,3) + 4*Power(t2,4) + 
               Power(s2,2)*(Power(t1,2) - 32*t1*t2 + 6*Power(t2,2)) - 
               2*s2*t1*(3*Power(t1,2) - 16*t1*t2 + 6*Power(t2,2))) + 
            (t1 - t2)*t2*(Power(s2,3)*(t1 - 3*t2) - 
               3*Power(s2,2)*t1*(t1 - 2*t2) - 
               2*t1*Power(t1 - t2,2)*t2 + 
               s2*(2*Power(t1,3) - 2*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 3*Power(t2,3))) - 
            s1*(Power(s2,3)*t2*(t1 + 7*t2) - 
               2*s2*t2*(3*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  Power(t2,3)) + 
               Power(s2,2)*(Power(t1,3) + 2*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1 - t2,2)*
                (Power(t1,3) - 3*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
                  3*Power(t2,3)))) - 
         s*(Power(s1,5)*t1*(3*s2 - 4*t1 + 7*t2) - 
            Power(s1,4)*t1*(-4*Power(s2,2) + 3*s2*t1 + 4*Power(t1,2) + 
               4*s2*t2 - 26*t1*t2 + 24*Power(t2,2)) - 
            s2*(t1 - t2)*Power(t2,2)*
             (Power(s2,3) - 2*Power(s2,2)*(t1 + t2) + 
               2*t1*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) - 
               s2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(-2*Power(t1,4) + 14*Power(t1,3)*t2 - 
               39*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) + 
               2*Power(t2,4) + Power(s2,3)*(t1 + 7*t2) + 
               Power(s2,2)*(2*Power(t1,2) - 34*t1*t2 + 9*Power(t2,2)) + 
               s2*(-5*Power(t1,3) + 41*Power(t1,2)*t2 - 
                  26*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*t2*(Power(s2,3)*(17*t1 - 5*t2)*t2 - 
               Power(s2,4)*(t1 + 6*t2) + 
               2*t1*Power(t1 - t2,2)*
                (Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) - 10*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) - 5*Power(t2,3)) + 
               s2*(Power(t1,4) - 6*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s1,2)*(-2*Power(t1,5) + 7*Power(s2,4)*t2 + 
               11*Power(t1,4)*t2 - 26*Power(t1,3)*Power(t2,2) + 
               34*Power(t1,2)*Power(t2,3) - 15*t1*Power(t2,4) - 
               2*Power(t2,5) + 
               Power(s2,3)*(Power(t1,2) - 26*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(-2*Power(t1,3) + 22*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(Power(t1,4) + 4*Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) + 
                  3*Power(t2,4)))))*lg2((s - s1 - s2)/s2))/
     (Power(s1,2)*s2*(-s + s2 - t1)*t1*(s1 + t1 - t2)*t2*(s - s1 + t2)*
       (s2 - t1 + t2)) + (8*(-(Power(s2,3)*t1*
            (Power(s2,2) - 3*s2*t1 + 2*Power(t1,2))*Power(t2,5)) + 
         Power(s1,6)*s2*(s2 - t1)*
          (s2*t1*(t1 - 2*t2) - Power(t1,2)*(t1 - 2*t2) + Power(s2,2)*t2) \
+ s1*Power(s2,2)*Power(t2,3)*(-3*s2*Power(t1,2)*(t1 - 2*t2)*t2 + 
            4*Power(t1,3)*(t1 - t2)*t2 + 2*Power(s2,4)*(t1 + t2) - 
            Power(s2,3)*t1*(6*t1 + 5*t2) + 
            2*Power(s2,2)*t1*(2*Power(t1,2) + 2*t1*t2 - Power(t2,2))) + 
         Power(s,7)*t1*(Power(t2,2)*(-t1 + t2) + 
            s1*(Power(t1,2) - 4*t1*t2 + Power(t2,2))) - 
         Power(s1,5)*(Power(s2,5)*(t1 - 2*t2) - 
            2*Power(t1,4)*Power(t2,2) + 
            3*Power(s2,3)*t1*(4*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,4)*(-7*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*
             (-7*Power(t1,2) + 4*t1*t2 + 10*Power(t2,2)) + 
            s2*(Power(t1,5) - 3*Power(t1,3)*Power(t2,2))) + 
         Power(s1,2)*s2*Power(t2,2)*
          (-2*Power(t1,3)*Power(t1 - t2,2)*t2 - 
            2*Power(s2,5)*(3*t1 + 2*t2) + 
            Power(s2,2)*t1*t2*
             (8*Power(t1,2) - 4*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,4)*(12*Power(t1,2) + 13*t1*t2 + Power(t2,2)) + 
            s2*Power(t1,2)*t2*
             (-3*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*(-6*Power(t1,3) - 16*Power(t1,2)*t2 - 
               2*t1*Power(t2,2) + Power(t2,3))) + 
         Power(s1,4)*(-4*Power(t1,4)*Power(t2,3) + 
            s2*Power(t1,4)*t2*(t1 + 4*t2) + 
            Power(s2,5)*(4*Power(t1,2) + 7*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,3)*t1*(8*Power(t1,3) + 7*Power(t1,2)*t2 - 
               10*t1*Power(t2,2) - 11*Power(t2,3)) + 
            Power(s2,4)*(-10*Power(t1,3) - 11*Power(t1,2)*t2 + 
               6*t1*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,2)*Power(t1,2)*
             (-2*Power(t1,3) - 4*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
               9*Power(t2,3))) + 
         Power(s1,3)*t2*(2*Power(t1,4)*Power(t2,3) + 
            2*Power(s2,6)*(2*t1 + t2) - Power(s2,5)*t1*(8*t1 + 13*t2) + 
            s2*Power(t1,3)*t2*(3*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
            Power(s2,4)*(10*Power(t1,3) + 16*Power(t1,2)*t2 + 
               2*t1*Power(t2,2) - 3*Power(t2,3)) + 
            2*Power(s2,3)*t1*
             (-5*Power(t1,3) - 2*Power(t1,2)*t2 + t1*Power(t2,2) + 
               4*Power(t2,3)) + 
            Power(s2,2)*(4*Power(t1,5) - 2*Power(t1,4)*t2 - 
               6*Power(t1,2)*Power(t2,3))) + 
         Power(s,6)*(t1*Power(t2,2)*
             (4*s2*t1 - 3*Power(t1,2) - 5*s2*t2 + t1*t2 + 2*Power(t2,2)) \
+ Power(s1,2)*(-5*Power(t1,3) + 16*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               Power(t2,3) + 2*s2*(Power(t1,2) - 2*t1*t2 - Power(t2,2))) \
- s1*(t1*(-2*Power(t1,3) + 6*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s2*(5*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(s,5)*(t1*Power(t2,2)*
             (-2*Power(t1,3) - 4*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
               Power(t2,3) + Power(s2,2)*(-6*t1 + 10*t2) + 
               s2*(9*Power(t1,2) - 4*t1*t2 - 10*Power(t2,2))) + 
            Power(s1,3)*(10*Power(t1,3) + Power(s2,2)*(t1 - 2*t2) - 
               25*Power(t1,2)*t2 + 9*t1*Power(t2,2) - 2*Power(t2,3) + 
               s2*(-7*Power(t1,2) + 12*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(-9*Power(t1,4) + 23*Power(t1,3)*t2 + 
               9*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) + 
               2*Power(t2,4) + 
               Power(s2,2)*(-8*Power(t1,2) + 9*t1*t2 + 6*Power(t2,2)) + 
               s2*(27*Power(t1,3) - 65*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) - 3*Power(t2,3))) + 
            s1*(Power(s2,2)*(9*Power(t1,3) - 31*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 10*Power(t2,3)) + 
               t1*(Power(t1,4) - 3*Power(t1,2)*Power(t2,2) - 
                  10*t1*Power(t2,3) - 5*Power(t2,4)) + 
               s2*(-8*Power(t1,4) + 21*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 2*Power(t2,4)))) + 
         Power(s,4)*(Power(s1,4)*
             (-10*Power(t1,3) + 19*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
               Power(t2,3) + Power(s2,2)*(-2*t1 + 5*t2) + 
               s2*(9*Power(t1,2) - 14*t1*t2 - 6*Power(t2,2))) + 
            t1*Power(t2,2)*(2*Power(s2,3)*(2*t1 - 5*t2) + 
               t1*t2*(-4*Power(t1,2) + t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(-9*Power(t1,2) + 6*t1*t2 + 
                  20*Power(t2,2)) + 
               s2*(4*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  20*t1*Power(t2,2) - 5*Power(t2,3))) + 
            Power(s1,2)*(-4*Power(t1,5) + Power(t1,4)*t2 + 
               19*Power(t1,3)*Power(t2,2) + 
               13*Power(t1,2)*Power(t2,3) + 6*t1*Power(t2,4) + 
               Power(t2,5) + 
               Power(s2,3)*(12*Power(t1,2) - t1*t2 - 4*Power(t2,2)) - 
               Power(s2,2)*(51*Power(t1,3) - 97*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(34*Power(t1,4) - 67*Power(t1,3)*t2 - 
                  29*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) - 
                  7*Power(t2,4))) - 
            Power(s1,3)*(3*Power(s2,3)*(t1 - 2*t2) + 
               Power(s2,2)*(-29*Power(t1,2) + 26*t1*t2 + 
                  21*Power(t2,2)) + 
               s2*(54*Power(t1,3) - 95*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 12*Power(t2,3)) + 
               2*(-8*Power(t1,4) + 17*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + Power(t2,4))) + 
            s1*(-(Power(s2,3)*
                  (7*Power(t1,3) - 31*Power(t1,2)*t2 + 
                    30*t1*Power(t2,2) + 20*Power(t2,3))) - 
               3*s2*t1*(Power(t1,4) + Power(t1,3)*t2 - 
                  6*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               t1*t2*(2*Power(t1,4) + Power(t1,3)*t2 - 
                  18*t1*Power(t2,3) - 2*Power(t2,4)) + 
               2*Power(s2,2)*(5*Power(t1,4) - 15*Power(t1,3)*t2 + 
                  11*t1*Power(t2,3) + 5*Power(t2,4)))) - 
         s*(Power(s1,6)*(2*Power(s2,2) - 3*s2*t1 + Power(t1,2))*
             (Power(t1,2) + s2*t2 - t1*t2) + 
            Power(s2,2)*t1*Power(t2,4)*
             (2*Power(s2,3) + 2*Power(t1,2)*(t1 - 3*t2) - 
               5*Power(s2,2)*(t1 + t2) + s2*t1*(t1 + 12*t2)) - 
            Power(s1,5)*(Power(s2,4)*(3*t1 - 8*t2) + 
               Power(t1,4)*(t1 - t2) + 
               Power(s2,2)*t1*
                (28*Power(t1,2) - 25*t1*t2 - 17*Power(t2,2)) + 
               Power(s2,3)*(-19*Power(t1,2) + 18*t1*t2 + 
                  8*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-13*Power(t1,2) + 16*t1*t2 + 10*Power(t2,2))) - 
            s1*s2*Power(t2,2)*
             (2*Power(s2,5)*(t1 + t2) - 
               2*Power(s2,4)*t2*(8*t1 + 5*t2) + 
               s2*Power(t1,2)*t2*
                (7*Power(t1,2) - 5*t1*t2 - 18*Power(t2,2)) + 
               2*Power(t1,3)*t2*
                (Power(t1,2) - 3*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*t1*
                (-10*Power(t1,2) + 33*t1*t2 + 15*Power(t2,2)) + 
               2*Power(s2,2)*t1*
                (4*Power(t1,3) - 10*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  4*Power(t2,3))) - 
            Power(s1,4)*(s2 - t1)*
             (2*Power(s2,4)*(t1 - 2*t2) + 
               Power(t1,2)*t2*
                (2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(-24*Power(t1,2) - 13*t1*t2 + 
                  16*Power(t2,2)) + 
               Power(s2,2)*(29*Power(t1,3) - 18*Power(t1,2)*t2 - 
                  16*t1*Power(t2,2) - 13*Power(t2,3)) + 
               s2*t1*(-6*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) + 11*Power(t2,3))) + 
            Power(s1,2)*t2*(2*Power(t1,3)*(t1 - t2)*Power(t2,3) + 
               2*Power(s2,6)*(t1 + t2) + 
               Power(s2,5)*(2*Power(t1,2) - 38*t1*t2 - 
                  17*Power(t2,2)) + 
               2*Power(s2,4)*t2*
                (26*Power(t1,2) + 22*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (6*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*t1*
                (8*Power(t1,4) - 13*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 22*t1*Power(t2,3) - 
                  9*Power(t2,4)) - 
               Power(s2,3)*(12*Power(t1,4) + Power(t1,3)*t2 + 
                  28*Power(t1,2)*Power(t2,2) - 4*Power(t2,4))) + 
            Power(s1,3)*(Power(t1,3)*Power(t2,3)*(-8*t1 + t2) + 
               Power(s2,5)*(6*Power(t1,2) + 22*t1*t2 + 3*Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (7*Power(t1,3) + 4*Power(t1,2)*t2 - 8*t1*Power(t2,2) - 
                  9*Power(t2,3)) - 
               Power(s2,4)*(20*Power(t1,3) + 21*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,3)*(20*Power(t1,4) + 13*Power(t1,3)*t2 + 
                  13*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) - 
                  11*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-6*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  20*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) + 
                  21*Power(t2,4)))) + 
         Power(s,3)*(Power(s1,5)*
             (Power(t1,2)*(5*t1 - 7*t2) + Power(s2,2)*(t1 - 4*t2) + 
               s2*(-5*Power(t1,2) + 8*t1*t2 + 2*Power(t2,2))) - 
            t1*Power(t2,2)*(Power(s2,4)*(t1 - 5*t2) + 
               2*Power(t1,2)*(t1 - t2)*Power(t2,2) + 
               s2*t1*t2*(-8*Power(t1,2) + 3*t1*t2 + 12*Power(t2,2)) + 
               Power(s2,3)*(-3*Power(t1,2) + 4*t1*t2 + 
                  20*Power(t2,2)) + 
               2*Power(s2,2)*
                (Power(t1,3) + 6*Power(t1,2)*t2 - 15*t1*Power(t2,2) - 
                  5*Power(t2,3))) + 
            Power(s1,4)*(2*Power(s2,3)*(3*t1 - 7*t2) + 
               Power(s2,2)*(-36*Power(t1,2) + 34*t1*t2 + 
                  22*Power(t2,2)) + 
               s2*(50*Power(t1,3) - 67*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) - 7*Power(t2,3)) + 
               2*t1*(-7*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(3*Power(s2,4)*(t1 - 2*t2) + 
               Power(s2,3)*(-43*Power(t1,2) + 2*t1*t2 + 
                  25*Power(t2,2)) + 
               Power(s2,2)*(99*Power(t1,3) - 110*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) - 24*Power(t2,3)) + 
               t1*(6*Power(t1,4) - 3*Power(t1,3)*t2 - 
                  17*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               s2*(-55*Power(t1,4) + 83*Power(t1,3)*t2 + 
                  40*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
                  9*Power(t2,4))) + 
            s1*(Power(t1,2)*Power(t2,3)*
                (8*Power(t1,2) - 11*t1*t2 - 6*Power(t2,2)) + 
               Power(s2,4)*(2*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*t1*
                (2*Power(t1,4) + 5*Power(t1,3)*t2 - 
                  37*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               s2*t1*t2*(-5*Power(t1,4) + 8*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) + 50*t1*Power(t2,3) + 
                  8*Power(t2,4)) - 
               Power(s2,3)*(4*Power(t1,4) - 21*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) + 48*t1*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(s1,2)*(-(Power(s2,4)*
                  (8*Power(t1,2) + 11*t1*t2 + 4*Power(t2,2))) + 
               Power(s2,3)*(41*Power(t1,3) - 65*Power(t1,2)*t2 + 
                  75*t1*Power(t2,2) + 18*Power(t2,3)) + 
               t1*t2*(-6*Power(t1,4) + 3*Power(t1,3)*t2 + 
                  10*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,2)*(-41*Power(t1,4) + 63*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) - 31*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               s2*(10*Power(t1,5) + 11*Power(t1,4)*t2 - 
                  58*Power(t1,3)*Power(t2,2) - 
                  30*Power(t1,2)*Power(t2,3) - 16*t1*Power(t2,4) - 
                  4*Power(t2,5)))) + 
         Power(s,2)*(Power(s1,6)*(s2 - t1)*
             (Power(t1,2) + s2*t2 - t1*t2) - 
            Power(s1,5)*(Power(s2,3)*(3*t1 - 10*t2) + 
               s2*t1*(21*Power(t1,2) - 24*t1*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(-6*Power(t1,2) + 8*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-17*Power(t1,2) + 22*t1*t2 + 7*Power(t2,2))) \
+ s2*t1*Power(t2,3)*(-Power(s2,4) + 2*Power(t1,2)*(2*t1 - 3*t2)*t2 + 
               Power(s2,3)*(t1 + 10*t2) + 
               2*Power(s2,2)*
                (2*Power(t1,2) - 10*t1*t2 - 5*Power(t2,2)) + 
               s2*t1*(-4*Power(t1,2) + 3*t1*t2 + 18*Power(t2,2))) + 
            Power(s1,4)*(Power(s2,4)*(-6*t1 + 13*t2) + 
               Power(s2,3)*(49*Power(t1,2) - 18*t1*t2 - 
                  29*Power(t2,2)) + 
               Power(t1,3)*(-4*Power(t1,2) + 3*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*(41*Power(t1,3) - 51*Power(t1,2)*t2 - 
                  31*t1*Power(t2,2) - 15*Power(t2,3)) + 
               Power(s2,2)*(-83*Power(t1,3) + 65*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) + 15*Power(t2,3))) + 
            s1*t2*(2*Power(t1,3)*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,5)*(4*Power(t1,2) - 11*t1*t2 - 10*Power(t2,2)) - 
               Power(s2,3)*t1*
                (2*Power(t1,3) - 32*Power(t1,2)*t2 + 
                  53*t1*Power(t2,2) + 10*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (3*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,4)*(-6*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  42*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*t1*
                (4*Power(t1,4) - 17*Power(t1,3)*t2 + 
                  28*Power(t1,2)*Power(t2,2) - 42*t1*Power(t2,3) - 
                  12*Power(t2,4))) + 
            Power(s1,3)*(-(Power(s2,5)*(t1 - 2*t2)) + 
               3*Power(s2,4)*
                (9*Power(t1,2) + 10*t1*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*t2*
                (6*Power(t1,3) + Power(t1,2)*t2 - 4*t1*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(s2,3)*(-75*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*(59*Power(t1,4) - 44*Power(t1,3)*t2 - 
                  37*Power(t1,2)*Power(t2,2) - 18*t1*Power(t2,3) - 
                  15*Power(t2,4)) + 
               s2*t1*(-12*Power(t1,4) - 13*Power(t1,3)*t2 + 
                  47*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) + 
                  18*Power(t2,4))) + 
            Power(s1,2)*(Power(t1,2)*Power(t2,3)*
                (-8*Power(t1,2) + 11*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,5)*(2*Power(t1,2) + 9*t1*t2 + 6*Power(t2,2)) - 
               Power(s2,4)*(12*Power(t1,3) - 19*Power(t1,2)*t2 + 
                  79*t1*Power(t2,2) + 27*Power(t2,3)) + 
               s2*t1*t2*(11*Power(t1,4) - 12*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) - 32*t1*Power(t2,3) - 
                  9*Power(t2,4)) + 
               Power(s2,3)*(16*Power(t1,4) - 19*Power(t1,3)*t2 + 
                  57*Power(t1,2)*Power(t2,2) + 55*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,2)*(-6*Power(t1,5) - 22*Power(t1,4)*t2 + 
                  44*Power(t1,3)*Power(t2,2) + 
                  5*Power(t1,2)*Power(t2,3) + 12*t1*Power(t2,4) + 
                  6*Power(t2,5)))))*lg2(-s2))/
     (Power(s1,2)*(s - s2)*s2*Power(t1,2)*Power(s - s2 + t1,2)*Power(t2,2)*
       Power(s - s1 + t2,2)) - 
    (8*((s1*s2*(s - s1 + t2)*
            (-2*s1*s2*t1*t2*Power(s1*(s2 - t1) - s2*t2,3) + 
              2*Power(s,6)*t1*t2*
               (s1*(2*s2 + t1 - t2) + s2*(-t1 + t2)) - 
              2*s*s1*s2*t1*t2*
               (Power(s2,2)*Power(t2,2)*(-t1 + 2*t2) + 
                 Power(s1,2)*(s2 - t1)*
                  (t1*(-2*t1 + t2) + 2*s2*(t1 + t2)) + 
                 s1*s2*t2*(t1*(t1 + t2) - s2*(t1 + 4*t2))) + 
              Power(s,2)*s1*s2*t1*t2*
               (s2*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
                 s1*(s2*(-3*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) + 
                    t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)))) + 
              Power(s,3)*(2*Power(s1,4)*(s2 - t1)*t1*t2 + 
                 2*Power(s2,3)*t1*(-s2 + t1)*Power(t2,2) + 
                 Power(s1,3)*
                  (2*Power(t1,2)*Power(t2,2) - 
                    Power(s2,2)*(Power(t1,2) + Power(t2,2)) + 
                    s2*t1*(Power(t1,2) + Power(t2,2))) + 
                 s1*s2*t2*(2*Power(s2,3)*t1 - 
                    t1*Power(t1 - t2,2)*(t1 + t2) + 
                    Power(s2,2)*(Power(t1,2) + Power(t2,2)) - 
                    2*s2*t1*(Power(t1,2) + t1*t2 + Power(t2,2))) + 
                 Power(s1,2)*s2*
                  (s2*Power(t1 + t2,3) - 
                    Power(s2,2)*(Power(t1,2) + Power(t2,2)) - 
                    2*t1*t2*(Power(t1,2) + t1*t2 + Power(t2,2)))) + 
              Power(s,5)*(-2*Power(s1,2)*t1*(3*s2 + 3*t1 - 2*t2)*t2 + 
                 2*s2*t1*t2*(s2*(2*t1 - 3*t2) + t1*(-t1 + t2)) + 
                 s1*(-6*Power(s2,2)*t1*t2 + 
                    2*t1*(t1 - t2)*Power(t2,2) + 
                    s2*(Power(t1,3) + 5*Power(t1,2)*t2 + 
                       5*t1*Power(t2,2) + Power(t2,3)))) + 
              Power(s,4)*(2*Power(s1,3)*t1*(3*t1 - t2)*t2 + 
                 2*Power(s2,2)*t1*t2*
                  (-(s2*t1) + Power(t1,2) + 3*s2*t2 - 2*t1*t2) + 
                 Power(s1,2)*
                  (2*t1*Power(t2,2)*(-2*t1 + t2) + 
                    Power(s2,2)*
                     (Power(t1,2) + 6*t1*t2 + Power(t2,2)) - 
                    s2*(2*Power(t1,3) + 3*Power(t1,2)*t2 + 
                       8*t1*Power(t2,2) + Power(t2,3))) - 
                 s1*s2*(-4*t1*t2*(Power(t1,2) + Power(t2,2)) + 
                    s2*(Power(t1,3) + 8*Power(t1,2)*t2 + 
                       3*t1*Power(t2,2) + 2*Power(t2,3))))))/
          (Power(s,3)*(s - s2 + t1)) + 
         (s1*s2*t1*t2*(-2*Power(s,7)*(t1 - t2)*(s2*t1 - s1*t2) + 
              2*s1*s2*(s2 - t1)*(s1 - t2)*
               Power(s1*(s2 - t1) - s2*t2,3) - 
              2*s*s1*s2*(3*Power(s1,4)*Power(s2 - t1,3) + 
                 Power(s2,2)*Power(t2,3)*
                  (-3*Power(s2,2) + 2*s2*t1 + Power(t1,2) + 3*s2*t2 - 
                    2*t1*t2) + 
                 Power(s1,3)*Power(s2 - t1,2)*
                  (3*Power(s2,2) - 6*s2*(t1 + 2*t2) + 
                    t1*(3*t1 + 2*t2)) + 
                 s1*s2*Power(t2,2)*
                  (9*Power(s2,3) - Power(t1,2)*(t1 + t2) + 
                    4*s2*t1*(2*t1 + 3*t2) - 
                    4*Power(s2,2)*(4*t1 + 3*t2)) - 
                 Power(s1,2)*(s2 - t1)*t2*
                  (9*Power(s2,3) + Power(t1,2)*(-2*t1 + t2) + 
                    s2*t1*(10*t1 + 9*t2) - Power(s2,2)*(17*t1 + 18*t2))\
) + 2*Power(s,6)*(s2*t1*(2*s2*t1 - Power(t1,2) - 3*s2*t2 + 
                    Power(t2,2)) + 
                 Power(s1,2)*
                  (2*Power(s2,2) - s2*t1 + t2*(-3*t1 + 2*t2)) - 
                 s1*(Power(s2,2)*t2 - Power(t1,2)*t2 + Power(t2,3) + 
                    s2*(-3*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)))) + 
              Power(s,2)*s1*s2*
               (7*Power(s1,4)*Power(s2 - t1,2) + 
                 Power(s1,3)*(s2 - t1)*
                  (16*Power(s2,2) + t1*(16*t1 + 3*t2) - 
                    4*s2*(8*t1 + 7*t2)) + 
                 s2*Power(t2,2)*
                  (7*Power(s2,3) - Power(s2,2)*(3*t1 + 16*t2) - 
                    t1*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
                    s2*(-3*Power(t1,2) + 4*t1*t2 + 7*Power(t2,2))) - 
                 s1*t2*(14*Power(s2,4) - 
                    Power(s2,3)*(31*t1 + 48*t2) + 
                    Power(t1,2)*
                     (3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
                    5*s2*t1*
                     (3*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)) + 
                    Power(s2,2)*
                     (29*Power(t1,2) + 36*t1*t2 + 28*Power(t2,2))) + 
                 Power(s1,2)*
                  (7*Power(s2,4) - 4*Power(s2,3)*(7*t1 + 12*t2) + 
                    Power(t1,2)*
                     (7*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) - 
                    s2*t1*(28*Power(t1,2) + 36*t1*t2 + 
                       29*Power(t2,2)) + 
                    Power(s2,2)*
                     (42*Power(t1,2) + 80*t1*t2 + 42*Power(t2,2)))) - 
              2*Power(s,5)*(Power(s1,3)*
                  (5*Power(s2,2) + t2*(-3*t1 + t2) - s2*(4*t1 + t2)) + 
                 s2*t1*(Power(s2,2)*(t1 - 3*t2) + t1*(t1 - t2)*t2 - 
                    s2*(Power(t1,2) - 3*Power(t2,2))) + 
                 Power(s1,2)*
                  (5*Power(s2,3) + 3*Power(t1,2)*t2 - Power(t2,3) - 
                    5*Power(s2,2)*(t1 + t2) + 
                    s2*(4*Power(t1,2) - 3*t1*t2 + 7*Power(t2,2))) - 
                 s1*(t1*(t1 - t2)*Power(t2,2) + 
                    Power(s2,3)*(t1 + 4*t2) + 
                    Power(s2,2)*
                     (-7*Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
                    4*s2*(Power(t1,3) + Power(t2,3)))) + 
              Power(s,4)*(2*Power(s2,2)*t1*t2*
                  (-Power(s2,2) + t1*(t1 - 2*t2) + 3*s2*t2) + 
                 2*Power(s1,4)*
                  (5*Power(s2,2) - t1*t2 - s2*(5*t1 + t2)) + 
                 Power(s1,3)*
                  (20*Power(s2,3) + 6*Power(t1,2)*t2 - 
                    6*Power(s2,2)*(5*t1 + 4*t2) + 
                    s2*(13*Power(t1,2) + 9*Power(t2,2))) + 
                 2*Power(s1,2)*
                  (5*Power(s2,4) + t1*Power(t2,2)*(-2*t1 + t2) - 
                    3*Power(s2,3)*(4*t1 + 5*t2) + 
                    15*Power(s2,2)*(Power(t1,2) + Power(t2,2)) - 
                    s2*(7*Power(t1,3) - 4*Power(t1,2)*t2 + 
                       3*t1*Power(t2,2) + 4*Power(t2,3))) + 
                 s1*s2*(Power(t1,4) + 6*Power(t1,3)*t2 - 
                    6*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) + 
                    Power(t2,4) - 2*Power(s2,3)*(t1 + 5*t2) + 
                    Power(s2,2)*(9*Power(t1,2) + 13*Power(t2,2)) - 
                    2*s2*(4*Power(t1,3) + 3*Power(t1,2)*t2 - 
                       4*t1*Power(t2,2) + 7*Power(t2,3)))) - 
              Power(s,3)*(4*Power(s1,5)*s2*(s2 - t1) + 
                 2*Power(s2,3)*(s2 - t1)*t1*Power(t2,2) + 
                 2*Power(s1,4)*
                  (9*Power(s2,3) + Power(t1,2)*t2 + 
                    2*s2*t1*(4*t1 + t2) - Power(s2,2)*(17*t1 + 8*t2)) + 
                 Power(s1,3)*
                  (18*Power(s2,4) - 2*Power(t1,2)*Power(t2,2) - 
                    50*Power(s2,3)*(t1 + t2) + 
                    s2*t1*(-18*Power(t1,2) + 5*t1*t2 - 
                       7*Power(t2,2)) + 
                    Power(s2,2)*
                     (50*Power(t1,2) + 32*t1*t2 + 26*Power(t2,2))) + 
                 s1*s2*t2*(-4*Power(s2,4) - 
                    t1*Power(t1 - t2,2)*(t1 + t2) + 
                    4*Power(s2,3)*(t1 + 4*t2) + 
                    Power(s2,2)*
                     (-7*Power(t1,2) + 5*t1*t2 - 18*Power(t2,2)) + 
                    2*s2*(4*Power(t1,3) - 5*Power(t1,2)*t2 + 
                       2*t1*Power(t2,2) + 2*Power(t2,3))) + 
                 2*Power(s1,2)*s2*
                  (2*Power(s2,4) - Power(s2,3)*(8*t1 + 17*t2) + 
                    Power(s2,2)*
                     (13*Power(t1,2) + 16*t1*t2 + 25*Power(t2,2)) + 
                    t1*(2*Power(t1,3) + 2*Power(t1,2)*t2 - 
                       5*t1*Power(t2,2) + 4*Power(t2,3)) - 
                    s2*(9*Power(t1,3) + 4*Power(t1,2)*t2 + 
                       4*t1*Power(t2,2) + 9*Power(t2,3))))))/
          (Power(s,3)*Power(s - s2 + t1,2)) - 
         (s1*t2*(s - s1 + t2)*
            (-2*Power(s1,6)*s2*(s2 - t1)*t1*(s2 - t1 - 3*t2) + 
              2*Power(s,4)*s2*t1*
               (Power(s1,3) + 3*Power(s1,2)*(t1 - t2) + 
                 3*s1*Power(t1 - t2,2) + t1*Power(t1 - t2,2))*(t1 - t2) \
- Power(s1,5)*(2*Power(s2,3)*t1*(t1 - 7*t2) - Power(t1,4)*t2 + 
                 Power(s2,4)*(2*t1 + t2) + 
                 2*s2*Power(t1,2)*
                  (3*Power(t1,2) + 6*t1*t2 - 13*Power(t2,2)) + 
                 2*Power(s2,2)*t1*
                  (-5*Power(t1,2) + t1*t2 + 12*Power(t2,2))) + 
              Power(s2,2)*(s2 - t1)*Power(t2,2)*
               (Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) - 
                 2*s2*Power(t1,2)*
                  (2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
                 Power(s2,2)*
                  (4*Power(t1,3) - 5*Power(t1,2)*t2 + 
                    4*t1*Power(t2,2) - Power(t2,3))) - 
              Power(s1,4)*(Power(s2,5)*t2 + 
                 3*Power(t1,4)*t2*(-t1 + t2) + 
                 Power(s2,4)*
                  (6*Power(t1,2) - 3*t1*t2 - 4*Power(t2,2)) + 
                 Power(s2,3)*t1*
                  (-6*Power(t1,2) - 28*t1*t2 + 37*Power(t2,2)) + 
                 s2*Power(t1,2)*
                  (6*Power(t1,3) + 3*Power(t1,2)*t2 - 
                    53*t1*Power(t2,2) + 42*Power(t2,3)) + 
                 Power(s2,2)*
                  (-6*Power(t1,4) + 30*Power(t1,3)*t2 + 
                    9*Power(t1,2)*Power(t2,2) - 36*t1*Power(t2,3))) + 
              s1*s2*t2*(Power(t1,3)*Power(t1 - t2,2)*
                  (2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
                 s2*Power(t1,2)*t2*
                  (14*Power(t1,3) - 35*Power(t1,2)*t2 + 
                    30*t1*Power(t2,2) - 9*Power(t2,3)) - 
                 2*Power(s2,4)*
                  (2*Power(t1,3) - 5*Power(t1,2)*t2 + 
                    6*t1*Power(t2,2) - 2*Power(t2,3)) + 
                 Power(s2,3)*
                  (10*Power(t1,4) - 16*Power(t1,3)*t2 + 
                    13*Power(t1,2)*Power(t2,2) - Power(t2,4)) + 
                 Power(s2,2)*t1*
                  (-8*Power(t1,4) + 2*Power(t1,3)*t2 + 
                    17*Power(t1,2)*Power(t2,2) - 22*t1*Power(t2,3) + 
                    7*Power(t2,4))) + 
              Power(s1,3)*(3*Power(t1,4)*Power(t1 - t2,2)*t2 + 
                 4*Power(s2,5)*t2*(-t1 + t2) + 
                 Power(s2,4)*
                  (-6*Power(t1,3) + 13*Power(t1,2)*t2 + 
                    2*t1*Power(t2,2) - 6*Power(t2,3)) + 
                 Power(s2,3)*t1*
                  (10*Power(t1,3) + 12*Power(t1,2)*t2 - 
                    74*t1*Power(t2,2) + 47*Power(t2,3)) - 
                 Power(s2,2)*t1*
                  (2*Power(t1,4) + 32*Power(t1,3)*t2 - 
                    62*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                    24*Power(t2,4)) + 
                 s2*Power(t1,2)*
                  (-2*Power(t1,4) + 8*Power(t1,3)*t2 + 
                    24*Power(t1,2)*Power(t2,2) - 61*t1*Power(t2,3) + 
                    30*Power(t2,4))) - 
              Power(s1,2)*(-(Power(t1,4)*Power(t1 - t2,3)*t2) - 
                 s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                  (9*Power(t1,2) + 5*t1*t2 - 8*Power(t2,2)) + 
                 Power(s2,5)*t2*
                  (5*Power(t1,2) - 12*t1*t2 + 6*Power(t2,2)) + 
                 Power(s2,4)*
                  (2*Power(t1,4) - 11*Power(t1,3)*t2 + 
                    14*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) - 
                    4*Power(t2,4)) + 
                 Power(s2,3)*t1*
                  (-4*Power(t1,4) - 2*Power(t1,3)*t2 + 
                    41*Power(t1,2)*Power(t2,2) - 71*t1*Power(t2,3) + 
                    29*Power(t2,4)) + 
                 Power(s2,2)*t1*
                  (2*Power(t1,5) + 18*Power(t1,4)*t2 - 
                    67*Power(t1,3)*Power(t2,2) + 
                    73*Power(t1,2)*Power(t2,3) - 17*t1*Power(t2,4) - 
                    6*Power(t2,5))) + 
              Power(s,3)*(2*Power(s1,4)*s2*(s2 - 3*t1)*t1 + 
                 Power(s1,3)*
                  (-16*s2*Power(t1,2)*(t1 - t2) + 
                    Power(t1,2)*(t1 - t2)*t2 + 
                    Power(s2,2)*(2*Power(t1,2) + t1*t2 - Power(t2,2))) \
- 3*Power(s1,2)*(t1 - t2)*(4*s2*Power(t1,2)*(t1 - t2) + 
                    Power(t1,2)*t2*(-t1 + t2) + 
                    Power(s2,2)*
                     (2*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
                 Power(t1 - t2,3)*
                  (Power(t1,2)*(t1 - t2)*t2 + 
                    2*s2*Power(t1,2)*(t1 + t2) - 
                    Power(s2,2)*(4*Power(t1,2) - t1*t2 + Power(t2,2))) \
+ s1*(t1 - t2)*(3*Power(t1,2)*Power(t1 - t2,2)*t2 + 
                    4*s2*Power(t1,2)*t2*(-t1 + t2) + 
                    Power(s2,2)*
                     (-10*Power(t1,3) + 31*Power(t1,2)*t2 - 
                       18*t1*Power(t2,2) + 3*Power(t2,3)))) - 
              Power(s,2)*(2*Power(s1,5)*s2*t1*(2*s2 - 3*(t1 + t2)) - 
                 Power(t1 - t2,3)*
                  (-(s2*Power(t1,2)*(t1 - 3*t2)*t2) + 
                    Power(t1,3)*(t1 - t2)*t2 - 
                    Power(s2,2)*t1*
                     (2*Power(t1,2) + t1*t2 + Power(t2,2)) + 
                    Power(s2,3)*
                     (2*Power(t1,2) - 3*t1*t2 + 3*Power(t2,2))) + 
                 Power(s1,4)*
                  (Power(t1,2)*(2*t1 - t2)*t2 + 
                    Power(s2,3)*(4*t1 + t2) - 
                    Power(s2,2)*t2*(12*t1 + t2) + 
                    3*s2*t1*(-4*Power(t1,2) + t1*t2 + 8*Power(t2,2))) \
+ Power(s1,3)*(s2*t1*t2*(13*Power(t1,2) + 24*t1*t2 - 
                       36*Power(t2,2)) + 
                    Power(s2,3)*
                     (10*Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
                    Power(t1,2)*t2*
                     (5*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
                    Power(s2,2)*
                     (-22*Power(t1,3) + 3*Power(t1,2)*t2 + 
                       10*t1*Power(t2,2) + 3*Power(t2,3))) + 
                 Power(s1,2)*
                  (3*Power(t1,2)*Power(t1 - t2,3)*t2 + 
                    6*s2*t1*Power(t1 - t2,2)*
                     (2*Power(t1,2) + 3*t1*t2 + 4*Power(t2,2)) + 
                    2*Power(s2,3)*
                     (3*Power(t1,3) + 7*Power(t1,2)*t2 - 
                       15*t1*Power(t2,2) + 6*Power(t2,3)) + 
                    Power(s2,2)*
                     (-26*Power(t1,4) + 31*Power(t1,3)*t2 + 
                       Power(t1,2)*Power(t2,2) - t1*Power(t2,3) - 
                       3*Power(t2,4))) + 
                 s1*(-(Power(t1,2)*Power(t1 - t2,3)*t2*(t1 + t2)) + 
                    3*s2*t1*Power(t1 - t2,2)*
                     (2*Power(t1,3) + 5*Power(t1,2)*t2 - 
                       2*t1*Power(t2,2) - 2*Power(t2,3)) + 
                    Power(s2,3)*
                     (-2*Power(t1,4) + 35*Power(t1,3)*t2 - 
                       54*Power(t1,2)*Power(t2,2) + 
                       37*t1*Power(t2,3) - 10*Power(t2,4)) + 
                    Power(s2,2)*
                     (-6*Power(t1,5) - 13*Power(t1,4)*t2 + 
                       30*Power(t1,3)*Power(t2,2) - 
                       12*Power(t1,2)*Power(t2,3) + Power(t2,5)))) + 
              s*(2*Power(s1,6)*s2*t1*(s2 - t1 - 2*t2) + 
                 Power(s1,5)*
                  (Power(t1,3)*t2 + Power(s2,3)*(6*t1 + t2) + 
                    s2*t1*t2*(5*t1 + 18*t2) - 
                    Power(s2,2)*t1*(6*t1 + 19*t2)) + 
                 Power(s1,4)*
                  (Power(t1,3)*(t1 - 2*t2)*t2 + 
                    2*Power(s2,4)*(t1 + t2) + 
                    Power(s2,3)*
                     (12*Power(t1,2) - 15*t1*t2 - 5*Power(t2,2)) + 
                    Power(s2,2)*t1*
                     (-26*Power(t1,2) - 15*t1*t2 + 58*Power(t2,2)) + 
                    s2*t1*(12*Power(t1,3) + 19*Power(t1,2)*t2 - 
                       7*t1*Power(t2,2) - 30*Power(t2,3))) + 
                 Power(s1,2)*
                  (-(Power(t1,3)*(5*t1 - 2*t2)*Power(t1 - t2,2)*t2) + 
                    s2*t1*Power(t1 - t2,2)*
                     (6*Power(t1,3) + 11*Power(t1,2)*t2 - 
                       35*t1*Power(t2,2) - 6*Power(t2,3)) + 
                    Power(s2,4)*
                     (6*Power(t1,3) + 10*Power(t1,2)*t2 - 
                       30*t1*Power(t2,2) + 15*Power(t2,3)) + 
                    Power(s2,2)*t1*t2*
                     (15*Power(t1,3) + 24*Power(t1,2)*t2 - 
                       96*t1*Power(t2,2) + 52*Power(t2,3)) + 
                    Power(s2,3)*
                     (-12*Power(t1,4) - 7*Power(t1,3)*t2 + 
                       27*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
                       7*Power(t2,4))) + 
                 s2*t2*(-2*Power(t1,3)*Power(t1 - t2,4) + 
                    s2*Power(t1,2)*Power(t1 - t2,2)*
                     (2*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) - 
                    2*Power(s2,2)*t1*
                     (Power(t1,4) - 6*Power(t1,3)*t2 + 
                       9*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
                       Power(t2,4)) + 
                    Power(s2,3)*
                     (2*Power(t1,4) - 12*Power(t1,3)*t2 + 
                       17*Power(t1,2)*Power(t2,2) - 
                       12*t1*Power(t2,3) + 3*Power(t2,4))) + 
                 Power(s1,3)*
                  (3*Power(t1,4)*t2*(-t1 + t2) + 
                    Power(s2,4)*
                     (6*Power(t1,2) + 6*t1*t2 - 9*Power(t2,2)) + 
                    Power(s2,3)*t2*
                     (-27*Power(t1,2) + 8*t1*t2 + 9*Power(t2,2)) + 
                    Power(s2,2)*t1*
                     (-22*Power(t1,3) + 25*Power(t1,2)*t2 + 
                       84*t1*Power(t2,2) - 80*Power(t2,3)) + 
                    s2*t1*(16*Power(t1,4) + 7*Power(t1,3)*t2 - 
                       64*Power(t1,2)*Power(t2,2) + 
                       17*t1*Power(t2,3) + 22*Power(t2,4))) + 
                 s1*(-(Power(t1,3)*Power(t1 - t2,3)*(2*t1 - t2)*t2) - 
                    2*s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                     (4*Power(t1,2) + 4*t1*t2 - 5*Power(t2,2)) + 
                    Power(s2,4)*
                     (2*Power(t1,4) + 16*Power(t1,3)*t2 - 
                       33*Power(t1,2)*Power(t2,2) + 
                       34*t1*Power(t2,3) - 11*Power(t2,4)) + 
                    Power(s2,2)*t1*
                     (4*Power(t1,5) + 20*Power(t1,4)*t2 - 
                       36*Power(t1,3)*Power(t2,2) - 
                       5*Power(t1,2)*Power(t2,3) + 30*t1*Power(t2,4) - 
                       13*Power(t2,5)) + 
                    Power(s2,3)*
                     (-6*Power(t1,5) - 22*Power(t1,4)*t2 + 
                       38*Power(t1,3)*Power(t2,2) - 
                       22*Power(t1,2)*Power(t2,3) + 2*Power(t2,5))))))/
          ((s - s2 + t1)*Power(s1 + t1 - t2,3)) + 
         (s2*t1*(s - s1 + t2)*
            (-(Power(s2,2)*t1*Power(t2,4)*Power(s2 - t1 + t2,3)) + 
              2*Power(s,4)*s1*(t1 - t2)*t2*
               (Power(s2,3) - 3*Power(s2,2)*(t1 - t2) + 
                 3*s2*Power(t1 - t2,2) + Power(t1 - t2,2)*t2) + 
              Power(s1,5)*(s2 - t1)*t1*
               (Power(s2,3) - Power(t1,3) + 4*Power(t1,2)*t2 - 
                 5*t1*Power(t2,2) + 4*Power(t2,3) + 
                 Power(s2,2)*(-3*t1 + 4*t2) + 
                 s2*(3*Power(t1,2) - 8*t1*t2 + 5*Power(t2,2))) + 
              s1*s2*Power(t2,2)*
               (2*Power(s2,5)*(3*t1 + t2) + 
                 s2*t1*Power(t1 - t2,2)*
                  (8*Power(t1,2) - 5*t1*t2 - 9*Power(t2,2)) - 
                 t1*Power(t1 - t2,2)*t2*
                  (Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) + 
                 Power(s2,4)*
                  (-26*Power(t1,2) + 12*t1*t2 + 6*Power(t2,2)) + 
                 Power(s2,3)*
                  (42*Power(t1,3) - 53*Power(t1,2)*t2 + 
                    3*t1*Power(t2,2) + 6*Power(t2,3)) + 
                 Power(s2,2)*
                  (-30*Power(t1,4) + 61*Power(t1,3)*t2 - 
                    24*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
                    2*Power(t2,4))) + 
              Power(s1,4)*(s2 - t1)*
               (Power(s2,4)*(t1 + 2*t2) - 
                 Power(s2,3)*(3*Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
                 t1*t2*(Power(t1,3) - 6*Power(t1,2)*t2 + 
                    11*t1*Power(t2,2) - 8*Power(t2,3)) + 
                 Power(s2,2)*
                  (3*Power(t1,3) - 3*Power(t1,2)*t2 - 
                    7*t1*Power(t2,2) + 6*Power(t2,3)) + 
                 s2*(-Power(t1,4) + Power(t1,3)*t2 + 
                    7*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
                    2*Power(t2,4))) - 
              Power(s1,3)*t2*
               (-2*Power(s2,6) + 2*Power(s2,5)*(7*t1 - t2) + 
                 Power(s2,4)*
                  (-37*Power(t1,2) + 28*t1*t2 + 6*Power(t2,2)) - 
                 Power(t1,2)*t2*
                  (Power(t1,3) - 6*Power(t1,2)*t2 + 
                    11*t1*Power(t2,2) - 6*Power(t2,3)) + 
                 Power(s2,3)*
                  (47*Power(t1,3) - 74*Power(t1,2)*t2 + 
                    12*t1*Power(t2,2) + 10*Power(t2,3)) + 
                 s2*t1*(7*Power(t1,4) - 22*Power(t1,3)*t2 + 
                    17*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) - 
                    8*Power(t2,4)) + 
                 Power(s2,2)*
                  (-29*Power(t1,4) + 71*Power(t1,3)*t2 - 
                    41*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
                    4*Power(t2,4))) + 
              Power(s1,2)*t2*
               (-(Power(t1,2)*(t1 - 2*t2)*Power(t1 - t2,2)*
                    Power(t2,2)) - 2*Power(s2,6)*(3*t1 + 2*t2) + 
                 2*Power(s2,5)*
                  (12*Power(t1,2) + t1*t2 - 5*Power(t2,2)) + 
                 s2*Power(t1,2)*t2*
                  (9*Power(t1,3) - 30*Power(t1,2)*t2 + 
                    35*t1*Power(t2,2) - 14*Power(t2,3)) + 
                 Power(s2,4)*
                  (-36*Power(t1,3) + 9*Power(t1,2)*t2 + 
                    30*t1*Power(t2,2) - 6*Power(t2,3)) + 
                 Power(s2,3)*
                  (24*Power(t1,4) + Power(t1,3)*t2 - 
                    62*Power(t1,2)*Power(t2,2) + 32*t1*Power(t2,3) + 
                    2*Power(t2,4)) + 
                 Power(s2,2)*
                  (-6*Power(t1,5) - 17*Power(t1,4)*t2 + 
                    73*Power(t1,3)*Power(t2,2) - 
                    67*Power(t1,2)*Power(t2,3) + 18*t1*Power(t2,4) + 
                    2*Power(t2,5))) - 
              Power(s,3)*(-(t1*(t1 - t2)*Power(t2,2)*
                    Power(s2 - t1 + t2,3)) - 
                 2*s1*Power(t2,2)*
                  (3*Power(s2,4) - 8*Power(s2,3)*(t1 - t2) + 
                    6*Power(s2,2)*Power(t1 - t2,2) + 
                    2*s2*t1*Power(t1 - t2,2) + 
                    Power(t1 - t2,3)*(t1 + t2)) + 
                 Power(s1,2)*
                  (2*Power(s2,4)*t2 + 
                    Power(s2,3)*
                     (-Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
                    Power(t1 - t2,3)*
                     (Power(t1,2) - t1*t2 + 4*Power(t2,2)) + 
                    3*Power(s2,2)*
                     (Power(t1,3) - 6*Power(t1,2)*t2 + 
                       7*t1*Power(t2,2) - 2*Power(t2,3)) + 
                    s2*(-3*Power(t1,4) + 21*Power(t1,3)*t2 - 
                       49*Power(t1,2)*Power(t2,2) + 
                       41*t1*Power(t2,3) - 10*Power(t2,4)))) + 
              Power(s,2)*(-(t1*Power(t2,2)*Power(s2 - t1 + t2,3)*
                    (s2*(t1 - 2*t2) + t2*(-t1 + t2))) + 
                 s1*t2*(t1*Power(t1 - t2,3)*(3*t1 - t2)*t2 - 
                    6*Power(s2,5)*(t1 + t2) + 
                    3*Power(s2,4)*
                     (8*Power(t1,2) + t1*t2 - 4*Power(t2,2)) + 
                    6*Power(s2,2)*Power(t1 - t2,2)*
                     (4*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2)) + 
                    Power(s2,3)*t1*
                     (-36*Power(t1,2) + 24*t1*t2 + 13*Power(t2,2)) - 
                    3*s2*Power(t1 - t2,2)*
                     (2*Power(t1,3) + 2*Power(t1,2)*t2 - 
                       5*t1*Power(t2,2) - 2*Power(t2,3))) + 
                 Power(s1,3)*
                  (Power(s2,4)*(t1 + 4*t2) + 
                    Power(t1 - t2,3)*
                     (3*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
                    Power(s2,3)*
                     (-6*Power(t1,2) + t1*t2 + 10*Power(t2,2)) + 
                    2*Power(s2,2)*
                     (6*Power(t1,3) - 15*Power(t1,2)*t2 + 
                       7*t1*Power(t2,2) + 3*Power(t2,3)) + 
                    s2*(-10*Power(t1,4) + 37*Power(t1,3)*t2 - 
                       54*Power(t1,2)*Power(t2,2) + 
                       35*t1*Power(t2,3) - 2*Power(t2,4))) + 
                 Power(s1,2)*
                  (4*Power(s2,5)*t2 - Power(s2,4)*t1*(t1 + 12*t2) - 
                    Power(t1 - t2,3)*t2*
                     (Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
                    Power(s2,3)*
                     (3*Power(t1,3) + 10*Power(t1,2)*t2 + 
                       3*t1*Power(t2,2) - 22*Power(t2,3)) + 
                    Power(s2,2)*
                     (-3*Power(t1,4) - Power(t1,3)*t2 + 
                       Power(t1,2)*Power(t2,2) + 31*t1*Power(t2,3) - 
                       26*Power(t2,4)) + 
                    s2*(Power(t1,5) - 12*Power(t1,3)*Power(t2,2) + 
                       30*Power(t1,2)*Power(t2,3) - 
                       13*t1*Power(t2,4) - 6*Power(t2,5)))) - 
              s*(s2*t1*(s2 + t1 - 2*t2)*Power(t2,3)*
                  Power(s2 - t1 + t2,3) + 
                 Power(s1,4)*
                  (2*Power(s2,4)*(t1 + t2) + 
                    Power(s2,3)*
                     (-9*Power(t1,2) + 6*t1*t2 + 6*Power(t2,2)) + 
                    Power(s2,2)*
                     (15*Power(t1,3) - 30*Power(t1,2)*t2 + 
                       10*t1*Power(t2,2) + 6*Power(t2,3)) + 
                    t1*(3*Power(t1,4) - 12*Power(t1,3)*t2 + 
                       17*Power(t1,2)*Power(t2,2) - 
                       12*t1*Power(t2,3) + 2*Power(t2,4)) + 
                    s2*(-11*Power(t1,4) + 34*Power(t1,3)*t2 - 
                       33*Power(t1,2)*Power(t2,2) + 
                       16*t1*Power(t2,3) + 2*Power(t2,4))) + 
                 s1*t2*(-2*t1*Power(t1 - t2,4)*Power(t2,2) - 
                    2*Power(s2,6)*(2*t1 + t2) + 
                    Power(s2,5)*t1*(18*t1 + 5*t2) + 
                    2*s2*t1*Power(t1 - t2,2)*t2*
                     (5*Power(t1,2) - 4*t1*t2 - 4*Power(t2,2)) - 
                    Power(s2,2)*Power(t1 - t2,2)*
                     (6*Power(t1,3) + 35*Power(t1,2)*t2 - 
                       11*t1*Power(t2,2) - 6*Power(t2,3)) + 
                    Power(s2,4)*
                     (-30*Power(t1,3) - 7*Power(t1,2)*t2 + 
                       19*t1*Power(t2,2) + 12*Power(t2,3)) + 
                    Power(s2,3)*
                     (22*Power(t1,4) + 17*Power(t1,3)*t2 - 
                       64*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) + 
                       16*Power(t2,4))) + 
                 Power(s1,3)*
                  (Power(s2,5)*(t1 + 6*t2) + 
                    Power(s2,3)*t1*
                     (9*Power(t1,2) + 8*t1*t2 - 27*Power(t2,2)) + 
                    Power(s2,4)*
                     (-5*Power(t1,2) - 15*t1*t2 + 12*Power(t2,2)) + 
                    Power(s2,2)*
                     (-7*Power(t1,4) + 3*Power(t1,3)*t2 + 
                       27*Power(t1,2)*Power(t2,2) - 
                       7*t1*Power(t2,3) - 12*Power(t2,4)) - 
                    2*t1*t2*(Power(t1,4) - 5*Power(t1,3)*t2 + 
                       9*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                       Power(t2,4)) + 
                    2*s2*(Power(t1,5) - 11*Power(t1,3)*Power(t2,2) + 
                       19*Power(t1,2)*Power(t2,3) - 
                       11*t1*Power(t2,4) - 3*Power(t2,5))) + 
                 Power(s1,2)*t2*
                  (2*Power(s2,6) - Power(s2,5)*(19*t1 + 6*t2) + 
                    Power(s2,4)*
                     (58*Power(t1,2) - 15*t1*t2 - 26*Power(t2,2)) + 
                    t1*Power(t1 - t2,2)*t2*
                     (3*Power(t1,2) - 6*t1*t2 + 2*Power(t2,2)) + 
                    Power(s2,3)*
                     (-80*Power(t1,3) + 84*Power(t1,2)*t2 + 
                       25*t1*Power(t2,2) - 22*Power(t2,3)) + 
                    Power(s2,2)*t1*
                     (52*Power(t1,3) - 96*Power(t1,2)*t2 + 
                       24*t1*Power(t2,2) + 15*Power(t2,3)) + 
                    s2*(-13*Power(t1,5) + 30*Power(t1,4)*t2 - 
                       5*Power(t1,3)*Power(t2,2) - 
                       36*Power(t1,2)*Power(t2,3) + 20*t1*Power(t2,4) + 
                       4*Power(t2,5))))))/
          ((s - s2 + t1)*Power(s2 - t1 + t2,3)) + 
         (s1*t1*t2*(s - s1 + t2)*
            (Power(s,5)*t1*(-4*Power(s2,3) + 
                 5*Power(s2,2)*(t1 - t2) - 4*s2*Power(t1 - t2,2) + 
                 Power(t1 - t2,3))*(t1 - t2) + 
              2*Power(s1,4)*s2*Power(s2 - t1,2)*
               (Power(s2,3) + Power(s2,2)*(-2*t1 + 3*t2) + 
                 t2*(3*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
                 s2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
              Power(s2,2)*t1*Power(t2,2)*
               (2*Power(s2,5) - 
                 Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
                 2*s2*t1*Power(t1 - t2,2)*(3*t1 - t2) + 
                 Power(s2,4)*(-6*t1 + 4*t2) + 
                 2*Power(s2,3)*
                  (4*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
                 Power(s2,2)*
                  (-8*Power(t1,3) + 17*Power(t1,2)*t2 - 
                    12*t1*Power(t2,2) + 2*Power(t2,3))) + 
              s1*s2*t2*(-2*Power(s2,6)*(t1 - t2) + 
                 Power(t1,3)*Power(t1 - t2,2)*
                  (2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
                 2*Power(s2,5)*
                  (Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
                 2*Power(s2,4)*
                  (5*Power(t1,3) + 15*Power(t1,2)*t2 - 
                    22*t1*Power(t2,2) + 3*Power(t2,3)) + 
                 Power(s2,2)*t1*
                  (22*Power(t1,4) - 6*Power(t1,3)*t2 - 
                    53*Power(t1,2)*Power(t2,2) + 42*t1*Power(t2,3) - 
                    8*Power(t2,4)) + 
                 Power(s2,3)*
                  (-24*Power(t1,4) - 23*Power(t1,3)*t2 + 
                    84*Power(t1,2)*Power(t2,2) - 32*t1*Power(t2,3) + 
                    2*Power(t2,4)) + 
                 s2*Power(t1,2)*
                  (-10*Power(t1,4) + 19*Power(t1,3)*t2 - 
                    4*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
                    6*Power(t2,4))) + 
              Power(s1,3)*(s2 - t1)*
               (2*Power(s2,6) + Power(t1,3)*Power(t1 - t2,3) + 
                 Power(s2,5)*(-6*t1 + 2*t2) + 
                 Power(s2,4)*
                  (8*Power(t1,2) - 2*t1*t2 - 6*Power(t2,2)) - 
                 Power(s2,3)*
                  (9*Power(t1,3) + 8*Power(t1,2)*t2 - 
                    42*t1*Power(t2,2) + 10*Power(t2,3)) + 
                 Power(s2,2)*
                  (9*Power(t1,4) + 11*Power(t1,3)*t2 - 
                    66*Power(t1,2)*Power(t2,2) + 40*t1*Power(t2,3) - 
                    4*Power(t2,4)) + 
                 s2*t1*(-5*Power(t1,4) + 27*Power(t1,2)*Power(t2,2) - 
                    30*t1*Power(t2,3) + 10*Power(t2,4))) + 
              Power(s1,2)*(-4*Power(s2,7)*t2 + 
                 Power(t1,4)*Power(t1 - t2,3)*t2 + 
                 2*Power(s2,6)*
                  (Power(t1,2) + 9*t1*t2 - 5*Power(t2,2)) - 
                 2*Power(s2,5)*
                  (5*Power(t1,3) + 20*Power(t1,2)*t2 - 
                    25*t1*Power(t2,2) + 3*Power(t2,3)) + 
                 Power(s2,4)*
                  (20*Power(t1,4) + 49*Power(t1,3)*t2 - 
                    90*Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) + 
                    2*Power(t2,4)) + 
                 Power(s2,2)*t1*
                  (10*Power(t1,5) + 4*Power(t1,4)*t2 - 
                    7*Power(t1,3)*Power(t2,2) - 
                    45*Power(t1,2)*Power(t2,3) + 44*t1*Power(t2,4) - 
                    10*Power(t2,5)) - 
                 2*s2*Power(t1,2)*
                  (Power(t1,5) + Power(t1,3)*Power(t2,2) - 
                    9*Power(t1,2)*Power(t2,3) + 11*t1*Power(t2,4) - 
                    4*Power(t2,5)) + 
                 Power(s2,3)*
                  (-20*Power(t1,5) - 28*Power(t1,4)*t2 + 
                    62*Power(t1,3)*Power(t2,2) + 
                    24*Power(t1,2)*Power(t2,3) - 24*t1*Power(t2,4) + 
                    2*Power(t2,5))) - 
              Power(s,4)*(-(t1*(t1 - t2)*
                    (10*Power(s2,4) + Power(t1 - t2,3)*(2*t1 + t2) - 
                      2*s2*Power(t1 - t2,2)*(6*t1 + t2) + 
                      Power(s2,3)*(-22*t1 + 4*t2) + 
                      Power(s2,2)*
                       (22*Power(t1,2) - 25*t1*t2 + 3*Power(t2,2)))) + 
                 s1*(t1*(3*t1 - 2*t2)*Power(t1 - t2,3) + 
                    2*Power(s2,4)*(t1 + t2) - 
                    2*s2*Power(t1 - t2,3)*(6*t1 + t2) + 
                    Power(s2,3)*
                     (-12*Power(t1,2) + 16*t1*t2 + 6*Power(t2,2)) + 
                    Power(s2,2)*
                     (17*Power(t1,3) - 33*Power(t1,2)*t2 + 
                       10*t1*Power(t2,2) + 6*Power(t2,3)))) + 
              Power(s,3)*(t1*
                  (t1*Power(t1 - t2,4)*(t1 + 2*t2) + 
                    Power(s2,5)*(-10*t1 + 8*t2) + 
                    Power(s2,4)*
                     (30*Power(t1,2) - 20*t1*t2 - 13*Power(t2,2)) + 
                    Power(s2,2)*Power(t1 - t2,2)*
                     (27*Power(t1,2) + 6*t1*t2 - 13*Power(t2,2)) - 
                    2*s2*Power(t1 - t2,3)*
                     (5*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
                    Power(s2,3)*
                     (-38*Power(t1,3) + 42*Power(t1,2)*t2 + 
                       6*t1*Power(t2,2) - 10*Power(t2,3))) + 
                 s1*(2*Power(s2,5)*(t1 + 3*t2) - 
                    t1*Power(t1 - t2,3)*
                     (6*Power(t1,2) - 2*t1*t2 - Power(t2,2)) + 
                    Power(s2,4)*
                     (-26*Power(t1,2) + 12*t1*t2 + 16*Power(t2,2)) + 
                    Power(s2,2)*t1*
                     (-66*Power(t1,3) + 98*Power(t1,2)*t2 + 
                       3*t1*Power(t2,2) - 35*Power(t2,3)) + 
                    s2*Power(t1 - t2,2)*
                     (34*Power(t1,3) - 14*Power(t1,2)*t2 - 
                       13*t1*Power(t2,2) - 2*Power(t2,3)) + 
                    Power(s2,3)*
                     (62*Power(t1,3) - 52*Power(t1,2)*t2 - 
                       49*t1*Power(t2,2) + 12*Power(t2,3))) + 
                 Power(s1,2)*
                  (-2*Power(s2,5) + Power(s2,4)*(9*t1 - 2*t2) + 
                    t1*Power(t1 - t2,3)*(3*t1 - t2) + 
                    Power(s2,3)*
                     (-18*Power(t1,2) + 35*t1*t2 + 6*Power(t2,2)) + 
                    2*Power(s2,2)*
                     (10*Power(t1,3) - 27*Power(t1,2)*t2 + 
                       7*t1*Power(t2,2) + 5*Power(t2,3)) + 
                    s2*(-12*Power(t1,4) + 37*Power(t1,3)*t2 - 
                       30*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                       4*Power(t2,4)))) + 
              Power(s,2)*(t1*
                  (Power(s2,6)*(4*t1 - 2*t2) + 
                    Power(t1,2)*Power(t1 - t2,4)*t2 - 
                    2*Power(s2,5)*
                     (7*Power(t1,2) + 2*t1*t2 - 10*Power(t2,2)) - 
                    2*s2*t1*Power(t1 - t2,3)*
                     (Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)) + 
                    Power(s2,2)*Power(t1 - t2,2)*
                     (8*Power(t1,3) + 23*Power(t1,2)*t2 - 
                       36*t1*Power(t2,2) + 7*Power(t2,3)) + 
                    Power(s2,4)*
                     (20*Power(t1,3) + 16*Power(t1,2)*t2 - 
                       60*t1*Power(t2,2) + 17*Power(t2,3)) - 
                    2*Power(s2,3)*
                     (8*Power(t1,4) + 7*Power(t1,3)*t2 - 
                       44*Power(t1,2)*Power(t2,2) + 
                       40*t1*Power(t2,3) - 11*Power(t2,4))) + 
                 Power(s1,3)*
                  (4*Power(s2,5) - Power(t1,2)*Power(t1 - t2,3) + 
                    Power(s2,4)*(-13*t1 + 10*t2) + 
                    Power(s2,3)*
                     (16*Power(t1,2) - 41*t1*t2 + 6*Power(t2,2)) - 
                    Power(s2,2)*
                     (10*Power(t1,3) - 49*Power(t1,2)*t2 + 
                       21*t1*Power(t2,2) + 2*Power(t2,3)) + 
                    s2*(4*Power(t1,4) - 21*Power(t1,3)*t2 + 
                       18*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) - 
                       2*Power(t2,4))) - 
                 s1*(6*Power(s2,6)*t2 + 
                    Power(s2,4)*t1*
                     (70*Power(t1,2) + 8*t1*t2 - 95*Power(t2,2)) - 
                    4*Power(s2,5)*
                     (5*Power(t1,2) + 3*t1*t2 - 3*Power(t2,2)) + 
                    Power(t1,2)*Power(t1 - t2,3)*
                     (3*Power(t1,2) + 2*t1*t2 - 2*Power(t2,2)) - 
                    s2*t1*Power(t1 - t2,2)*
                     (26*Power(t1,3) + 6*Power(t1,2)*t2 - 
                       16*t1*Power(t2,2) - 7*Power(t2,3)) + 
                    Power(s2,3)*
                     (-100*Power(t1,4) + 32*Power(t1,3)*t2 + 
                       134*Power(t1,2)*Power(t2,2) - 
                       27*t1*Power(t2,3) - 12*Power(t2,4)) + 
                    Power(s2,2)*
                     (73*Power(t1,5) - 73*Power(t1,4)*t2 - 
                       55*Power(t1,3)*Power(t2,2) + 
                       46*Power(t1,2)*Power(t2,3) + 
                       15*t1*Power(t2,4) - 6*Power(t2,5))) + 
                 Power(s1,2)*
                  (4*Power(s2,6) - 16*Power(s2,5)*t1 + 
                    Power(t1,2)*Power(t1 - t2,3)*(6*t1 - t2) + 
                    Power(s2,4)*
                     (38*Power(t1,2) - 29*t1*t2 - 22*Power(t2,2)) + 
                    Power(s2,3)*
                     (-64*Power(t1,3) + 66*Power(t1,2)*t2 + 
                       86*t1*Power(t2,2) - 26*Power(t2,3)) + 
                    Power(s2,2)*
                     (64*Power(t1,4) - 84*Power(t1,3)*t2 - 
                       79*Power(t1,2)*Power(t2,2) + 
                       81*t1*Power(t2,3) - 6*Power(t2,4)) + 
                    2*s2*(-16*Power(t1,5) + 33*Power(t1,4)*t2 - 
                       4*Power(t1,3)*Power(t2,2) - 
                       23*Power(t1,2)*Power(t2,3) + 9*t1*Power(t2,4) + 
                       Power(t2,5)))) - 
              s*(2*Power(s1,4)*s2*
                  (Power(s2,4) - 3*Power(s2,3)*(t1 - t2) + 
                    3*Power(s2,2)*
                     (Power(t1,2) - 3*t1*t2 + Power(t2,2)) - 
                    t1*t2*(3*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
                    s2*(-Power(t1,3) + 9*Power(t1,2)*t2 - 
                       6*t1*Power(t2,2) + Power(t2,3))) + 
                 s2*t1*t2*(2*Power(t1,2)*Power(t1 - t2,4) + 
                    Power(s2,5)*(-8*t1 + 10*t2) + 
                    2*Power(s2,2)*Power(t1 - t2,2)*
                     (10*Power(t1,2) - 15*t1*t2 + 3*Power(t2,2)) + 
                    2*Power(s2,4)*
                     (13*Power(t1,2) - 22*t1*t2 + 6*Power(t2,2)) - 
                    s2*t1*Power(t1 - t2,2)*
                     (8*Power(t1,2) - 19*t1*t2 + 6*Power(t2,2)) + 
                    Power(s2,3)*
                     (-32*Power(t1,3) + 77*Power(t1,2)*t2 - 
                       58*t1*Power(t2,2) + 18*Power(t2,3))) + 
                 2*Power(s1,3)*
                  (3*Power(s2,6) + Power(s2,4)*t1*(17*t1 - 25*t2) + 
                    Power(t1,3)*Power(t1 - t2,3) + 
                    Power(s2,5)*(-11*t1 + 6*t2) + 
                    Power(s2,3)*
                     (-16*Power(t1,3) + 38*Power(t1,2)*t2 + 
                       9*t1*Power(t2,2) - 6*Power(t2,3)) + 
                    Power(s2,2)*
                     (11*Power(t1,4) - 28*Power(t1,3)*t2 - 
                       15*Power(t1,2)*Power(t2,2) + 
                       21*t1*Power(t2,3) - 3*Power(t2,4)) + 
                    s2*t1*(-5*Power(t1,4) + 12*Power(t1,3)*t2 + 
                       3*Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) + 
                       6*Power(t2,4))) + 
                 Power(s1,2)*
                  (2*Power(s2,7) - 
                    Power(t1,3)*Power(t1 - t2,3)*(3*t1 + t2) - 
                    2*Power(s2,6)*(4*t1 + 3*t2) + 
                    Power(s2,5)*
                     (24*Power(t1,2) + 20*t1*t2 - 26*Power(t2,2)) + 
                    s2*t1*Power(t1 - t2,2)*
                     (22*Power(t1,3) + 15*Power(t1,2)*t2 - 
                       12*t1*Power(t2,2) - 10*Power(t2,3)) - 
                    Power(s2,4)*
                     (57*Power(t1,3) + 38*Power(t1,2)*t2 - 
                       144*t1*Power(t2,2) + 22*Power(t2,3)) + 
                    Power(s2,3)*t1*
                     (80*Power(t1,3) + 27*Power(t1,2)*t2 - 
                       246*t1*Power(t2,2) + 84*Power(t2,3)) - 
                    2*Power(s2,2)*
                     (30*Power(t1,5) - 9*Power(t1,4)*t2 - 
                       77*Power(t1,3)*Power(t2,2) + 
                       46*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) - 
                       2*Power(t2,5))) + 
                 s1*(-2*Power(s2,7)*t2 + 
                    Power(t1,3)*Power(t1 - t2,3)*(2*t1 - t2)*t2 + 
                    6*Power(s2,6)*t1*(t1 + t2) - 
                    s2*Power(t1,2)*Power(t1 - t2,2)*
                     (4*Power(t1,3) + 12*Power(t1,2)*t2 - 
                       9*t1*Power(t2,2) - 4*Power(t2,3)) + 
                    Power(s2,5)*
                     (-24*Power(t1,3) - 24*Power(t1,2)*t2 + 
                       22*t1*Power(t2,2) + 12*Power(t2,3)) + 
                    2*Power(s2,4)*
                     (20*Power(t1,4) + 26*Power(t1,3)*t2 - 
                       34*Power(t1,2)*Power(t2,2) - 
                       29*t1*Power(t2,3) + 8*Power(t2,4)) + 
                    Power(s2,2)*t1*
                     (18*Power(t1,5) + 16*Power(t1,4)*t2 - 
                       63*Power(t1,3)*Power(t2,2) + 
                       3*Power(t1,2)*Power(t2,3) + 42*t1*Power(t2,4) - 
                       16*Power(t2,5)) + 
                    Power(s2,3)*
                     (-36*Power(t1,5) - 46*Power(t1,4)*t2 + 
                       87*Power(t1,3)*Power(t2,2) + 
                       60*Power(t1,2)*Power(t2,3) - 58*t1*Power(t2,4) + 
                       6*Power(t2,5))))))/
          (Power(s - s2 + t1,2)*Power(s2 - t1 + t2,3)) - 
         (s2*t1*t2*(Power(s,5)*
               (4*Power(s1,3) + 5*Power(s1,2)*(t1 - t2) + 
                 4*s1*Power(t1 - t2,2) + Power(t1 - t2,3))*(t1 - t2)*t2 \
+ Power(s2,2)*(s2 - t1)*Power(t1 - t2,3)*Power(t2,4) + 
              2*Power(s1,7)*(s2 - t1)*(Power(s2,2) - s2*t1 - t1*t2) + 
              2*Power(s1,6)*(Power(s2,4) + Power(s2,3)*(t1 - 4*t2) + 
                 Power(t1,2)*(2*t1 - 3*t2)*t2 + 
                 s2*t1*(3*Power(t1,2) - 7*t1*t2 + Power(t2,2)) + 
                 Power(s2,2)*(-5*Power(t1,2) + 9*t1*t2 + Power(t2,2))) + 
              2*Power(s1,5)*(Power(s2,4)*(3*t1 - 4*t2) + 
                 Power(t1,2)*t2*
                  (3*Power(t1,2) - 6*t1*t2 + 4*Power(t2,2)) + 
                 Power(s2,3)*
                  (-3*Power(t1,2) - 2*t1*t2 + 7*Power(t2,2)) + 
                 s2*t1*(3*Power(t1,3) - 22*Power(t1,2)*t2 + 
                    15*t1*Power(t2,2) + 5*Power(t2,3)) - 
                 Power(s2,2)*
                  (3*Power(t1,3) - 25*Power(t1,2)*t2 + 
                    20*t1*Power(t2,2) + 5*Power(t2,3))) + 
              Power(s1,4)*(6*Power(s2,4)*
                  (Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) + 
                 Power(t1,2)*t2*
                  (2*Power(t1,3) - 12*Power(t1,2)*t2 + 
                    17*t1*Power(t2,2) - 8*Power(t2,3)) - 
                 Power(s2,3)*
                  (10*Power(t1,3) - 48*Power(t1,2)*t2 + 
                    6*t1*Power(t2,2) + 17*Power(t2,3)) + 
                 s2*t1*(2*Power(t1,4) - 32*Power(t1,3)*t2 + 
                    84*Power(t1,2)*Power(t2,2) - 23*t1*Power(t2,3) - 
                    24*Power(t2,4)) + 
                 Power(s2,2)*
                  (2*Power(t1,4) + 6*Power(t1,3)*t2 - 
                    90*Power(t1,2)*Power(t2,2) + 49*t1*Power(t2,3) + 
                    20*Power(t2,4))) + 
              Power(s1,3)*(-2*Power(t1,2)*(t1 - 3*t2)*Power(t1 - t2,2)*
                  Power(t2,2) + 
                 2*Power(s2,4)*
                  (Power(t1,3) - 9*Power(t1,2)*t2 + 
                    18*t1*Power(t2,2) - 4*Power(t2,3)) + 
                 Power(s2,3)*
                  (-4*Power(t1,4) + 50*Power(t1,3)*t2 - 
                    108*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) + 
                    18*Power(t2,4)) + 
                 s2*t1*t2*(-8*Power(t1,4) + 42*Power(t1,3)*t2 - 
                    53*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                    22*Power(t2,4)) + 
                 2*Power(s2,2)*
                  (Power(t1,5) - 12*Power(t1,4)*t2 + 
                    12*Power(t1,3)*Power(t2,2) + 
                    31*Power(t1,2)*Power(t2,3) - 14*t1*Power(t2,4) - 
                    10*Power(t2,5))) + 
              s1*s2*Power(t2,2)*
               (t1*Power(t1 - t2,2)*t2*
                  (Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) + 
                 2*Power(s2,3)*t1*
                  (Power(t1,2) - 3*t1*t2 + 3*Power(t2,2)) + 
                 Power(s2,2)*
                  (-10*Power(t1,4) + 29*Power(t1,3)*t2 - 
                    24*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                    6*Power(t2,4)) + 
                 2*s2*(4*Power(t1,5) - 11*Power(t1,4)*t2 + 
                    9*Power(t1,3)*Power(t2,2) - 
                    Power(t1,2)*Power(t2,3) - Power(t2,5))) + 
              Power(s1,2)*t2*
               (Power(t1,2)*(t1 - 2*t2)*Power(t1 - t2,2)*Power(t2,2) + 
                 2*Power(s2,4)*
                  (-2*Power(t1,3) + 9*Power(t1,2)*t2 - 
                    12*t1*Power(t2,2) + Power(t2,3)) + 
                 Power(s2,3)*
                  (14*Power(t1,4) - 70*Power(t1,3)*t2 + 
                    93*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) - 
                    14*Power(t2,4)) + 
                 s2*t1*t2*(6*Power(t1,4) - 11*Power(t1,3)*t2 - 
                    4*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) - 
                    10*Power(t2,4)) + 
                 Power(s2,2)*
                  (-10*Power(t1,5) + 44*Power(t1,4)*t2 - 
                    45*Power(t1,3)*Power(t2,2) - 
                    7*Power(t1,2)*Power(t2,3) + 4*t1*Power(t2,4) + 
                    10*Power(t2,5))) - 
              Power(s,4)*(2*s1*(s2 - t2)*Power(t1 - t2,3)*(t1 + 6*t2) - 
                 Power(t1 - t2,3)*t2*
                  (-2*s2*t1 + Power(t1,2) + 3*s2*t2 + t1*t2 - 
                    2*Power(t2,2)) + 
                 2*Power(s1,4)*(5*(t1 - t2)*t2 + s2*(t1 + t2)) + 
                 2*Power(s1,3)*
                  (s2*(3*Power(t1,2) + 8*t1*t2 - 6*Power(t2,2)) + 
                    t2*(2*Power(t1,2) - 13*t1*t2 + 11*Power(t2,2))) + 
                 Power(s1,2)*(t1 - t2)*
                  (s2*(6*Power(t1,2) + 16*t1*t2 - 17*Power(t2,2)) + 
                    t2*(3*Power(t1,2) - 25*t1*t2 + 22*Power(t2,2)))) + 
              Power(s,3)*(-2*Power(s1,5)*
                  (Power(s2,2) - s2*(3*t1 + t2) + t2*(-4*t1 + 5*t2)) + 
                 Power(t1 - t2,3)*t2*
                  (Power(s2,2)*(t1 - 3*t2) - 
                    s2*(Power(t1,2) + 2*t1*t2 - 6*Power(t2,2)) - 
                    t2*(-2*Power(t1,2) + t1*t2 + Power(t2,2))) + 
                 Power(s1,4)*
                  (Power(s2,2)*(-2*t1 + 9*t2) + 
                    2*s2*(8*Power(t1,2) + 6*t1*t2 - 13*Power(t2,2)) + 
                    t2*(-13*Power(t1,2) - 20*t1*t2 + 30*Power(t2,2))) + 
                 Power(s1,2)*
                  (-(Power(t1 - t2,2)*t2*
                       (13*Power(t1,2) - 6*t1*t2 - 27*Power(t2,2))) + 
                    s2*t2*(-35*Power(t1,3) + 3*Power(t1,2)*t2 + 
                       98*t1*Power(t2,2) - 66*Power(t2,3)) + 
                    2*Power(s2,2)*
                     (5*Power(t1,3) + 7*Power(t1,2)*t2 - 
                       27*t1*Power(t2,2) + 10*Power(t2,3))) + 
                 Power(s1,3)*
                  (Power(s2,2)*
                     (6*Power(t1,2) + 35*t1*t2 - 18*Power(t2,2)) + 
                    2*t2*(-5*Power(t1,3) + 3*Power(t1,2)*t2 + 
                       21*t1*Power(t2,2) - 19*Power(t2,3)) + 
                    s2*(12*Power(t1,3) - 49*Power(t1,2)*t2 - 
                       52*t1*Power(t2,2) + 62*Power(t2,3))) - 
                 s1*(t1 - t2)*
                  (2*Power(t1 - t2,2)*t2*
                     (2*Power(t1,2) - 5*t1*t2 - 5*Power(t2,2)) - 
                    Power(s2,2)*
                     (4*Power(t1,3) + 5*Power(t1,2)*t2 - 
                       25*t1*Power(t2,2) + 12*Power(t2,3)) + 
                    s2*(2*Power(t1,4) + 11*Power(t1,3)*t2 + 
                       Power(t1,2)*Power(t2,2) - 48*t1*Power(t2,3) + 
                       34*Power(t2,4)))) + 
              Power(s,2)*(Power(s1,6)*
                  (4*Power(s2,2) - 6*s2*t1 - 2*(t1 - 2*t2)*t2) + 
                 Power(t1 - t2,3)*Power(t2,2)*
                  (Power(s2,3) + Power(s2,2)*(t1 - 6*t2) + 
                    t1*(t1 - t2)*t2 + 
                    s2*(-2*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2))) + 
                 2*Power(s1,5)*
                  (2*Power(s2,3) - 8*Power(s2,2)*t2 + 
                    t2*(10*Power(t1,2) - 2*t1*t2 - 7*Power(t2,2)) + 
                    s2*(-6*Power(t1,2) + 6*t1*t2 + 10*Power(t2,2))) + 
                 Power(s1,4)*
                  (Power(s2,3)*(10*t1 - 13*t2) + 
                    s2*t2*(95*Power(t1,2) - 8*t1*t2 - 
                       70*Power(t2,2)) + 
                    Power(s2,2)*
                     (-22*Power(t1,2) - 29*t1*t2 + 38*Power(t2,2)) + 
                    t2*(17*Power(t1,3) - 60*Power(t1,2)*t2 + 
                       16*t1*Power(t2,2) + 20*Power(t2,3))) + 
                 Power(s1,3)*
                  (Power(s2,3)*
                     (6*Power(t1,2) - 41*t1*t2 + 16*Power(t2,2)) + 
                    Power(s2,2)*
                     (-26*Power(t1,3) + 86*Power(t1,2)*t2 + 
                       66*t1*Power(t2,2) - 64*Power(t2,3)) - 
                    2*t2*(-11*Power(t1,4) + 40*Power(t1,3)*t2 - 
                       44*Power(t1,2)*Power(t2,2) + 
                       7*t1*Power(t2,3) + 8*Power(t2,4)) + 
                    s2*(12*Power(t1,4) + 27*Power(t1,3)*t2 - 
                       134*Power(t1,2)*Power(t2,2) - 
                       32*t1*Power(t2,3) + 100*Power(t2,4))) + 
                 Power(s1,2)*
                  (Power(t1 - t2,2)*t2*
                     (7*Power(t1,3) - 36*Power(t1,2)*t2 + 
                       23*t1*Power(t2,2) + 8*Power(t2,3)) - 
                    Power(s2,3)*
                     (2*Power(t1,3) + 21*Power(t1,2)*t2 - 
                       49*t1*Power(t2,2) + 10*Power(t2,3)) + 
                    Power(s2,2)*
                     (-6*Power(t1,4) + 81*Power(t1,3)*t2 - 
                       79*Power(t1,2)*Power(t2,2) - 
                       84*t1*Power(t2,3) + 64*Power(t2,4)) + 
                    s2*(6*Power(t1,5) - 15*Power(t1,4)*t2 - 
                       46*Power(t1,3)*Power(t2,2) + 
                       55*Power(t1,2)*Power(t2,3) + 
                       73*t1*Power(t2,4) - 73*Power(t2,5))) - 
                 s1*(2*Power(t1 - t2,3)*Power(t2,2)*
                     (3*Power(t1,2) - 5*t1*t2 - Power(t2,2)) + 
                    s2*Power(t1 - t2,2)*t2*
                     (7*Power(t1,3) + 16*Power(t1,2)*t2 - 
                       6*t1*Power(t2,2) - 26*Power(t2,3)) + 
                    Power(s2,3)*
                     (2*Power(t1,4) + Power(t1,3)*t2 - 
                       18*Power(t1,2)*Power(t2,2) + 
                       21*t1*Power(t2,3) - 4*Power(t2,4)) - 
                    2*Power(s2,2)*
                     (Power(t1,5) + 9*Power(t1,4)*t2 - 
                       23*Power(t1,3)*Power(t2,2) - 
                       4*Power(t1,2)*Power(t2,3) + 33*t1*Power(t2,4) - 
                       16*Power(t2,5)))) - 
              s*(2*Power(s1,7)*s2*(s2 - t1) + 
                 s2*Power(t1 - t2,3)*Power(t2,3)*
                  (-2*Power(s2,2) + t1*(t1 - 2*t2) + s2*(t1 + 3*t2)) + 
                 2*Power(s1,6)*
                  (3*Power(s2,3) + t1*(5*t1 - 4*t2)*t2 + 
                    3*s2*t2*(t1 + t2) - Power(s2,2)*(3*t1 + 4*t2)) + 
                 2*Power(s1,5)*
                  (Power(s2,4) + Power(s2,3)*(6*t1 - 11*t2) + 
                    Power(s2,2)*
                     (-13*Power(t1,2) + 10*t1*t2 + 12*Power(t2,2)) + 
                    t1*t2*(6*Power(t1,2) - 22*t1*t2 + 13*Power(t2,2)) + 
                    s2*(6*Power(t1,3) + 11*Power(t1,2)*t2 - 
                       12*t1*Power(t2,2) - 12*Power(t2,3))) + 
                 s1*t2*(2*t1*Power(t1 - t2,4)*Power(t2,2) - 
                    2*Power(s2,4)*t1*
                     (Power(t1,2) - 3*t1*t2 + 3*Power(t2,2)) - 
                    Power(s2,2)*Power(t1 - t2,2)*
                     (10*Power(t1,3) + 12*Power(t1,2)*t2 - 
                       15*t1*Power(t2,2) - 22*Power(t2,3)) + 
                    s2*Power(t1 - t2,2)*t2*
                     (4*Power(t1,3) + 9*Power(t1,2)*t2 - 
                       12*t1*Power(t2,2) - 4*Power(t2,3)) + 
                    2*Power(s2,3)*
                     (6*Power(t1,4) - 14*Power(t1,3)*t2 + 
                       3*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) - 
                       5*Power(t2,4))) + 
                 Power(s1,4)*
                  (6*Power(s2,4)*(t1 - t2) + 
                    Power(s2,3)*(-50*t1*t2 + 34*Power(t2,2)) + 
                    t1*t2*(18*Power(t1,3) - 58*Power(t1,2)*t2 + 
                       77*t1*Power(t2,2) - 32*Power(t2,3)) - 
                    Power(s2,2)*
                     (22*Power(t1,3) - 144*Power(t1,2)*t2 + 
                       38*t1*Power(t2,2) + 57*Power(t2,3)) + 
                    2*s2*(8*Power(t1,4) - 29*Power(t1,3)*t2 - 
                       34*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) + 
                       20*Power(t2,4))) + 
                 Power(s1,3)*
                  (6*Power(s2,4)*
                     (Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
                    2*t1*Power(t1 - t2,2)*t2*
                     (3*Power(t1,2) - 15*t1*t2 + 10*Power(t2,2)) - 
                    2*Power(s2,3)*
                     (6*Power(t1,3) - 9*Power(t1,2)*t2 - 
                       38*t1*Power(t2,2) + 16*Power(t2,3)) + 
                    Power(s2,2)*t2*
                     (84*Power(t1,3) - 246*Power(t1,2)*t2 + 
                       27*t1*Power(t2,2) + 80*Power(t2,3)) + 
                    s2*(6*Power(t1,5) - 58*Power(t1,4)*t2 + 
                       60*Power(t1,3)*Power(t2,2) + 
                       87*Power(t1,2)*Power(t2,3) - 46*t1*Power(t2,4) - 
                       36*Power(t2,5))) + 
                 Power(s1,2)*(-(t1*Power(t1 - t2,2)*Power(t2,2)*
                       (6*Power(t1,2) - 19*t1*t2 + 8*Power(t2,2))) + 
                    2*Power(s2,4)*
                     (Power(t1,3) - 6*Power(t1,2)*t2 + 
                       9*t1*Power(t2,2) - Power(t2,3)) + 
                    Power(s2,3)*
                     (-6*Power(t1,4) + 42*Power(t1,3)*t2 - 
                       30*Power(t1,2)*Power(t2,2) - 56*t1*Power(t2,3) + 
                       22*Power(t2,4)) + 
                    2*Power(s2,2)*
                     (2*Power(t1,5) - 3*Power(t1,4)*t2 - 
                       46*Power(t1,3)*Power(t2,2) + 
                       77*Power(t1,2)*Power(t2,3) + 9*t1*Power(t2,4) - 
                       30*Power(t2,5)) + 
                    s2*t2*(-16*Power(t1,5) + 42*Power(t1,4)*t2 + 
                       3*Power(t1,3)*Power(t2,2) - 
                       63*Power(t1,2)*Power(t2,3) + 16*t1*Power(t2,4) + 
                       18*Power(t2,5))))))/
          ((-s + s2 - t1)*Power(s1 + t1 - t2,3)))*lg2(-s + s1 + s2))/
     (Power(s1,2)*Power(s2,2)*Power(t1,2)*Power(t2,2)*Power(s - s1 + t2,2)) \
+ (4*(2*Power(s,5)*t1*(t1 - t2)*t2 + 
         s1*s2*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*t2*(s2 - t1 + t2)*
          (s1*(-s2 + t1) + s2*t2) - 
         Power(s,4)*t1*(2*t2*(s2*(t1 - 2*t2) + 2*t2*(-t1 + t2)) + 
            s1*(-Power(t1,2) + 6*t1*t2 - 7*Power(t2,2) + s2*(t1 + t2))) - 
         Power(s,3)*(t1*t2*(-(Power(s2,2)*(t1 - 3*t2)) + 
               2*s2*(t1 - 3*t2)*t2 + 3*Power(t2,2)*(-t1 + t2)) + 
            Power(s1,2)*t1*(Power(s2,2) + 3*Power(t1,2) - 7*t1*t2 + 
               10*Power(t2,2) - s2*(4*t1 + t2)) + 
            s1*t2*(Power(s2,2)*(-6*t1 + t2) + 
               s2*(3*Power(t1,2) + 4*t1*t2 + Power(t2,2)) - 
               t1*(Power(t1,2) - 10*t1*t2 + 13*Power(t2,2)))) + 
         s*(s2*t1*Power(t2,3)*(Power(s2,2) + Power(t2,2)) - 
            Power(s1,4)*t1*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               2*Power(t2,2) + s2*(-2*t1 + t2)) + 
            s1*t2*(Power(s2,4)*(t1 - 4*t2) - 2*s2*t1*Power(t2,3) - 
               t1*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,2)*t1*t2*(2*t1 + t2) - 
               Power(s2,3)*(Power(t1,2) - 2*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,3)*t2*(2*Power(s2,3) + Power(s2,2)*(3*t1 + t2) - 
               s2*(6*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               t1*(Power(t1,2) - 3*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(3*t1*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,4)*(t1 + 2*t2) + s2*Power(t1,2)*t2*(t1 + 2*t2) + 
               Power(s2,3)*(-2*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - 8*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 4*Power(t2,3)))) + 
         Power(s,2)*(t1*Power(t2,2)*
             (Power(s2,3) + Power(s2,2)*(t1 - 3*t2) + 3*s2*Power(t2,2) + 
               (t1 - t2)*Power(t2,2)) + 
            Power(s1,3)*t1*(2*Power(s2,2) + 3*Power(t1,2) - 4*t1*t2 + 
               7*Power(t2,2) + s2*(-5*t1 + t2)) + 
            Power(s1,2)*t2*(-Power(s2,3) + Power(s2,2)*(-11*t1 + t2) + 
               t1*(-2*Power(t1,2) + 9*t1*t2 - 15*Power(t2,2)) + 
               s2*(12*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
            s1*(3*t1*Power(t2,3)*(-2*t1 + 3*t2) + 
               Power(s2,3)*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) - 
               s2*t1*t2*(Power(t1,2) + t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-Power(t1,3) + 7*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 3*Power(t2,3)))))*
       lg2((s - s1 - s2)/(-s + s2 - t1)))/
     (s*s1*s2*t1*(s - s2 + t1)*Power(t2,2)*(s - s1 + t2)*(s2 - t1 + t2)) - 
    (8*(Power(s1,5)*Power(s2 - t1,2) + 
         2*Power(s,4)*t1*(s1 + t1)*(t1 - t2) + 
         Power(s1,4)*(s2 - t1)*
          (2*Power(s2,2) - t1*(t1 - 3*t2) - 3*s2*t2) + 
         s2*Power(t2,3)*(2*Power(s2,3) - 4*Power(s2,2)*t1 + 
            3*s2*Power(t1,2) + Power(t1,2)*(-2*t1 + t2)) + 
         s1*Power(t2,2)*(-4*Power(s2,4) + Power(s2,3)*(11*t1 + t2) + 
            Power(t1,2)*t2*(-3*t1 + 2*t2) + 
            s2*t1*(3*Power(t1,2) + 3*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*(-8*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
         Power(s1,3)*(Power(s2,3)*(2*t1 - 3*t2) + 
            2*Power(t1,2)*t2*(-2*t1 + 3*t2) + 
            s2*t1*(Power(t1,2) + 8*t1*t2 - 9*Power(t2,2)) + 
            Power(s2,2)*(-3*Power(t1,2) - 2*t1*t2 + 4*Power(t2,2))) + 
         Power(s1,2)*t2*(2*Power(s2,4) - 9*Power(s2,3)*t1 + 
            6*Power(t1,2)*(t1 - t2)*t2 + 
            Power(s2,2)*(9*Power(t1,2) + 9*t1*t2 - 3*Power(t2,2)) + 
            s2*t1*(-3*Power(t1,2) - 11*t1*t2 + 7*Power(t2,2))) - 
         s*(Power(s1,4)*(3*Power(s2,2) - 7*s2*t1 + 4*Power(t1,2) - 
               2*s2*t2) + Power(t2,2)*
             (3*s2*Power(t1,3) + 6*Power(s2,3)*t2 - 
               8*Power(s2,2)*t1*t2 + Power(t1,2)*t2*(-t1 + t2)) + 
            Power(s1,3)*(2*Power(s2,3) - 4*Power(s2,2)*(t1 + 2*t2) + 
               t1*(4*Power(t1,2) - 7*t1*t2 - 3*Power(t2,2)) + 
               s2*(-3*Power(t1,2) + 5*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3)*t1 + 
               Power(s2,2)*(-7*Power(t1,2) - 12*t1*t2 + 
                  4*Power(t2,2)) + 
               t1*t2*(-11*Power(t1,2) + 7*t1*t2 + 5*Power(t2,2)) + 
               s2*(4*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 5*Power(t2,3))) + 
            s1*t2*(-2*Power(s2,3)*(3*t1 + 4*t2) + 
               t1*t2*(9*Power(t1,2) - 4*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(10*Power(t1,2) + 19*t1*t2 + Power(t2,2)) - 
               s2*(7*Power(t1,3) + 6*Power(t1,2)*t2 + 5*t1*Power(t2,2) - 
                  2*Power(t2,3)))) + 
         Power(s,3)*(Power(s1,2)*
             (-6*Power(t1,2) + 5*t1*t2 - Power(t2,2) + 2*s2*(2*t1 + t2)) \
+ s1*(-6*Power(t1,3) + 7*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
               Power(t2,3) + 2*s2*t1*(t1 + 3*t2)) - 
            2*(2*Power(t1,2)*t2*(-t1 + t2) + 
               s2*(Power(t1,3) - Power(t1,2)*t2 + Power(t2,3)))) + 
         Power(s,2)*(Power(s1,3)*
             (2*Power(s2,2) - 9*s2*t1 + 7*Power(t1,2) - 4*s2*t2 - 
               3*t1*t2 + Power(t2,2)) - 
            Power(s1,2)*(-7*Power(t1,3) + 9*Power(t1,2)*t2 - 
               2*t1*Power(t2,2) + 2*Power(t2,3) + 
               2*Power(s2,2)*(t1 + 2*t2) + 
               s2*(4*Power(t1,2) + 6*t1*t2 - 5*Power(t2,2))) + 
            t2*(3*Power(t1,2)*(t1 - t2)*t2 + 6*Power(s2,2)*Power(t2,2) - 
               4*s2*t1*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
            s1*(-2*Power(s2,2)*
                (2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) + 
               s2*(5*Power(t1,3) + 4*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - Power(t2,3)) + 
               t2*(-11*Power(t1,3) + 6*Power(t1,2)*t2 + Power(t2,3)))))*
       lg2(-(s2/(s*(-s + s2 - t1)))))/
     (s1*s2*(-s + s2 - t1)*Power(t1,2)*(-s + s1 - t2)*(s1 + t1 - t2)*t2) - 
    (4*(Power(s,6)*(2*s1 + t1 - t2)*t2 + 
         s1*s2*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*(s1 - t2)*
          (s1 + t1 - t2)*(s1*(s2 - t1) - s2*t2) + 
         Power(s,5)*t2*(-4*Power(s1,2) - 3*s2*t1 + Power(t1,2) + 
            4*s2*t2 - Power(t2,2) + s1*(-7*s2 + 2*t1 + 3*t2)) + 
         Power(s,4)*t2*(2*Power(s1,3) + 3*Power(s2,2)*t1 - 
            2*s2*Power(t1,2) + Power(s1,2)*(11*s2 - 7*t1 - 2*t2) - 
            6*Power(s2,2)*t2 + Power(t1,2)*t2 + 4*s2*Power(t2,2) - 
            Power(t2,3) + s1*(9*Power(s2,2) - 7*s2*t1 + Power(t1,2) - 
               11*s2*t2 + 2*t1*t2 + 3*Power(t2,2))) - 
         s*(2*Power(s1,6)*s2 + 
            Power(s1,4)*s2*(5*Power(s2,2) + s2*(5*t1 - 14*t2) + 
               Power(t1 - 2*t2,2)) + 
            Power(s1,5)*s2*(5*s2 + 2*t1 - 5*t2) + 
            s2*(s2 - t1)*Power(t2,3)*(Power(s2,2) + Power(t2,2)) + 
            s1*t2*(t1*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,4)*(-3*t1 + t2) + s2*Power(t2,3)*(t1 + 2*t2) - 
               Power(s2,2)*t2*(2*Power(t1,2) + t1*t2 + 2*Power(t2,2)) - 
               Power(s2,3)*(Power(t1,2) - 9*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(3*Power(s2,4) + Power(s2,3)*(3*t1 - 13*t2) - 
               t1*t2*(Power(t1,2) + 2*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 15*t1*t2 + 13*Power(t2,2)) + 
               s2*(Power(t1,3) + Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*(3*t1 - 5*t2) - 
               t1*(t1 - 4*t2)*Power(t2,3) + 
               Power(s2,3)*(-2*Power(t1,2) - 11*t1*t2 + 
                  11*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 3*Power(t2,3)) - 
               s2*t2*(Power(t1,3) + 2*Power(t1,2)*t2 + 4*Power(t2,3)))) - 
         Power(s,3)*(2*Power(s1,4)*s2 + 4*Power(s1,3)*t1*(s2 - t2) + 
            t2*(Power(s2,3)*(t1 - 4*t2) - Power(t1,2)*Power(t2,2) + 
               Power(t2,4) - 
               Power(s2,2)*(Power(t1,2) - 6*Power(t2,2)) - 
               2*s2*t2*(-Power(t1,2) + t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3) + 11*Power(s2,2)*t2 + 
               s2*(3*Power(t1,2) - 22*t1*t2 - 3*Power(t2,2)) + 
               t2*(5*Power(t1,2) + 2*t1*t2 + 4*Power(t2,2))) + 
            s1*(Power(s2,3)*(2*t1 + 3*t2) - 
               Power(s2,2)*t2*(8*t1 + 15*t2) - 
               t2*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s2*(Power(t1,3) + Power(t1,2)*t2 + 11*t1*Power(t2,2) + 
                  5*Power(t2,3)))) + 
         Power(s,2)*(4*Power(s1,5)*s2 + 
            Power(s1,4)*s2*(4*s2 + 7*t1 - 9*t2) + 
            Power(t2,2)*(-Power(s2,4) + 4*Power(s2,3)*t2 + 
               t1*(t1 - t2)*Power(t2,2) + 2*s2*Power(t2,3) + 
               Power(s2,2)*(Power(t1,2) - 3*t1*t2 - Power(t2,2))) + 
            Power(s1,3)*(4*Power(s2,3) + Power(s2,2)*(6*t1 - 7*t2) + 
               3*Power(t1,2)*t2 + 2*Power(t2,3) + 
               s2*(5*Power(t1,2) - 18*t1*t2 + 7*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,4) + Power(s2,3)*(3*t1 - 2*t2) + 
               3*Power(s2,2)*(Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               s2*(2*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 3*Power(t2,3)) - 
               t2*(2*Power(t1,3) + 3*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*(Power(s2,4)*(2*t1 - t2) + 
               2*Power(t2,3)*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) - 
               Power(s2,3)*(Power(t1,2) + 5*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - 2*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + Power(t2,3)) - 
               s2*t2*(Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  7*Power(t2,3)))))*lg2((s - s1 - s2)/t1))/
     (s*s1*s2*t1*(s - s2 + t1)*(s1 + t1 - t2)*t2*Power(s - s1 + t2,2)) - 
    (8*(Power(s1,4)*t1*(-s2 + t1)*t2 + 
         s2*Power(t2,3)*(Power(s2,3) + s2*t1*(3*t1 - 2*t2) + 
            Power(s2,2)*(-4*t1 + t2) + Power(t1,2)*(-2*t1 + t2)) - 
         Power(s,5)*(t2*(-t1 + t2) + s2*(t1 + t2)) + 
         s1*Power(t2,2)*(-Power(s2,4) + 8*Power(s2,3)*t1 + 
            Power(t1,2)*t2*(-3*t1 + 2*t2) + 
            s2*t1*(3*Power(t1,2) + 2*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*(-8*Power(t1,2) + Power(t2,2))) + 
         Power(s1,3)*(-(Power(t1,3)*t2) + Power(s2,3)*(t1 + t2) + 
            s2*Power(t1,2)*(t1 + 4*t2) + 
            Power(s2,2)*(-2*Power(t1,2) - 4*t1*t2 + Power(t2,2))) - 
         Power(s1,2)*t2*(2*Power(s2,3)*(2*t1 + t2) + 
            Power(t1,2)*t2*(-3*t1 + 2*t2) + 
            s2*t1*(3*Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,2)*(-7*Power(t1,2) - 7*t1*t2 + 2*Power(t2,2))) + 
         Power(s,3)*(Power(s2,2)*(13*t1 - 3*t2)*t2 - 
            Power(s2,3)*(t1 + 3*t2) + 
            Power(s1,2)*(3*Power(s2,2) - 6*s2*t1 + 2*s2*t2 + 4*t1*t2 - 
               Power(t2,2)) + 
            t2*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
               Power(t2,3)) - 
            s2*(Power(t1,3) + 5*Power(t1,2)*t2 - t1*Power(t2,2) + 
               Power(t2,3)) + 
            s1*(2*Power(s2,3) + s2*(14*t1 - t2)*t2 + 
               Power(s2,2)*(-6*t1 + t2) + t1*t2*(-3*t1 + t2))) + 
         Power(s,4)*(2*t1*(t1 - t2)*t2 + s2*t2*(-8*t1 + 3*t2) + 
            Power(s2,2)*(2*t1 + 3*t2) + 
            s1*(-Power(s2,2) + 4*s2*t1 + t2*(-2*t1 + t2))) + 
         s*(2*Power(s1,3)*(Power(s2,3) - Power(s2,2)*t1 - 
               2*Power(t1,2)*t2 + s2*(5*t1 - t2)*t2) + 
            Power(s1,4)*(Power(s2,2) + t1*t2 + s2*(-t1 + t2)) - 
            Power(t2,2)*(3*Power(s2,2)*t2*(-3*t1 + t2) + 
               Power(t1,2)*t2*(-t1 + t2) + 3*Power(s2,3)*(t1 + t2) + 
               s2*t1*(3*Power(t1,2) + 5*t1*t2 - 4*Power(t2,2))) + 
            s1*t2*(Power(s2,4) + Power(s2,3)*(5*t1 + 3*t2) - 
               3*Power(s2,2)*t1*(3*t1 + 5*t2) - 
               t1*t2*(3*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               s2*(7*Power(t1,3) + 3*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4) + Power(s2,3)*(-2*t1 + t2) + 
               t1*t2*(2*Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) + 12*t1*t2 + 3*Power(t2,2)) - 
               3*s2*(Power(t1,3) + 2*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
                  Power(t2,3)))) + 
         Power(s,2)*(Power(s1,3)*
             (-3*Power(s2,2) + 4*s2*t1 - 2*s2*t2 - 4*t1*t2 + Power(t2,2)) \
- Power(s1,2)*(4*Power(s2,3) + 16*s2*t1*t2 + 
               Power(s2,2)*(-6*t1 + 3*t2) + 
               t2*(-4*Power(t1,2) - 2*t1*t2 + Power(t2,2))) - 
            s1*(Power(s2,4) - 2*Power(s2,3)*(t1 - t2) + 
               2*Power(t1,3)*t2 + t1*Power(t2,3) - Power(t2,4) - 
               3*s2*t1*(Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,2) + 17*t1*t2 + 2*Power(t2,2))) + 
            t2*(Power(s2,4) + 2*t1*(t1 - t2)*Power(t2,2) + 
               Power(s2,3)*(-6*t1 + t2) + 
               Power(s2,2)*(3*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  3*Power(t2,3)))))*lg2(-(s2/(s*t1))))/
     (s1*s2*t1*Power(s - s2 + t1,2)*(-s + s1 - t2)*t2*(s2 - t1 + t2)) - 
    (8*(Power(s2,3)*(s2 - t1)*Power(t2,4)*(s2 + t2)*
          (Power(s2,2) + s2*(t1 - t2) - t1*(2*t1 + t2)) - 
         Power(s1,6)*t1*(Power(s2,4) - 2*Power(s2,3)*(t1 - t2) + 
            s2*t1*(t1 - 3*t2)*t2 + Power(t1,2)*Power(t2,2) + 
            Power(s2,2)*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) + 
         Power(s,6)*(t1 - t2)*
          (s2*Power(t2,2)*(s2 + t2) + 
            s1*(Power(s2,2)*(t1 - t2) + s2*(t1 - t2)*t2 + t1*Power(t2,2))\
) + Power(s1,5)*(Power(s2,6) - 7*Power(s2,5)*t1 + 
            Power(t1,4)*Power(t2,2) + 
            s2*Power(t1,2)*t2*(Power(t1,2) - 6*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,4)*(12*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*
             (Power(t1,2) - 6*t1*t2 + 10*Power(t2,2)) + 
            Power(s2,3)*(-7*Power(t1,3) + 13*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + 2*Power(t2,3))) - 
         s1*Power(s2,2)*Power(t2,3)*
          (5*Power(s2,5) + Power(t1,3)*(2*t1 - t2)*t2 + 
            Power(s2,4)*(-5*t1 + t2) - 
            Power(s2,3)*(5*Power(t1,2) + 7*t1*t2 + 2*Power(t2,2)) + 
            s2*t1*(2*Power(t1,3) + Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s2,2)*(3*Power(t1,3) + 5*Power(t1,2)*t2 - 
               3*t1*Power(t2,2) + 2*Power(t2,3))) - 
         Power(s1,4)*(-3*Power(t1,3)*Power(t2,4) + 
            4*Power(s2,6)*(t1 + 2*t2) + 
            s2*Power(t1,2)*Power(t2,2)*
             (-2*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,5)*(-10*Power(t1,2) - 13*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*t1*t2*
             (-2*Power(t1,3) + 5*Power(t1,2)*t2 + 21*t1*Power(t2,2) - 
               5*Power(t2,3)) + 
            Power(s2,4)*(8*Power(t1,3) + 5*Power(t1,2)*t2 - 
               15*t1*Power(t2,2) + 5*Power(t2,3)) - 
            2*Power(s2,3)*(Power(t1,4) - Power(t1,3)*t2 - 
               6*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) - 
               2*Power(t2,4))) + 
         Power(s1,2)*s2*Power(t2,2)*
          (6*Power(s2,6) + Power(t1,2)*Power(t2,3)*(-3*t1 + 2*t2) - 
            Power(s2,5)*(6*t1 + 7*t2) + 
            Power(s2,4)*(-4*Power(t1,2) + 7*t1*t2 - 9*Power(t2,2)) + 
            Power(s2,3)*t1*(2*Power(t1,2) - t1*t2 + 12*Power(t2,2)) + 
            s2*t1*t2*(Power(t1,3) - 4*Power(t1,2)*t2 + t1*Power(t2,2) + 
               Power(t2,3)) + 
            2*Power(s2,2)*(Power(t1,4) + 2*Power(t1,3)*t2 + 
               2*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - Power(t2,4))) \
+ Power(s1,3)*t2*(-2*Power(s2,7) - 
            3*s2*Power(t1,3)*(t1 - 2*t2)*Power(t2,2) - 
            Power(t1,3)*Power(t2,3)*(t1 + 2*t2) + 
            Power(s2,6)*(4*t1 + 15*t2) + 
            Power(s2,5)*(-6*Power(t1,2) - 21*t1*t2 + 11*Power(t2,2)) + 
            Power(s2,2)*t1*t2*
             (-2*Power(t1,3) + 16*Power(t1,2)*t2 + 5*t1*Power(t2,2) - 
               4*Power(t2,3)) + 
            Power(s2,4)*(6*Power(t1,3) + 3*Power(t1,2)*t2 - 
               20*t1*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,3)*(-2*Power(t1,4) + 3*Power(t1,3)*t2 - 
               2*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
               4*Power(t2,4))) + 
         Power(s,5)*(Power(s1,2)*
             (2*Power(s2,3)*t1 - 5*Power(s2,2)*Power(t1 - t2,2) + 
               s2*t2*(-5*Power(t1,2) + 10*t1*t2 - 4*Power(t2,2)) - 
               Power(t2,2)*(5*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
            s2*t2*(s2 + t2)*(2*(t1 - t2)*Power(t2,2) + 
               s2*(Power(t1,2) - 5*t1*t2 + 5*Power(t2,2))) + 
            s1*(2*Power(t1,2)*(t1 - t2)*Power(t2,2) + 
               Power(s2,3)*(-4*Power(t1,2) + 8*t1*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - Power(t2,3)) + 
               2*s2*t2*(Power(t1,3) - 4*Power(t1,2)*t2 + 3*Power(t2,3)))) \
+ s*(Power(s1,6)*(Power(s2,3)*(t1 - t2) - 
               Power(s2,2)*Power(t1 - t2,2) - s2*t1*(t1 - 2*t2)*t2 - 
               Power(t1,2)*Power(t2,2)) + 
            Power(s2,2)*Power(t2,3)*
             (Power(s2,5) + Power(s2,4)*(2*t1 - 5*t2) + 
               Power(t1,2)*t2*(2*Power(t1,2) - t1*t2 - 2*Power(t2,2)) + 
               Power(s2,3)*(-5*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*t2*(3*Power(t1,2) - t1*t2 + 4*Power(t2,2)) + 
               s2*Power(t1,2)*(2*Power(t1,2) - t1*t2 + 6*Power(t2,2))) - 
            Power(s1,5)*(2*Power(s2,5) + 2*Power(s2,4)*(-6*t1 + t2) + 
               2*Power(t1,2)*Power(t2,2)*(-3*t1 + t2) + 
               2*s2*Power(t1,2)*t2*(-3*t1 + 10*t2) + 
               Power(s2,3)*(16*Power(t1,2) - 16*t1*t2 + 
                  5*Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + Power(t2,3))) - 
            s1*Power(s2,2)*Power(t2,2)*
             (4*Power(s2,5) + Power(s2,4)*(t1 - 20*t2) + 
               Power(s2,3)*(-13*Power(t1,2) + 11*t1*t2 - 
                  5*Power(t2,2)) + 
               t1*t2*(4*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,3) + 8*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 13*Power(t2,3)) + 
               s2*(5*Power(t1,4) + 5*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s1,3)*(-8*Power(t1,3)*Power(t2,4) + 
               2*Power(s2,6)*(3*t1 + 8*t2) - 
               Power(s2,5)*(20*Power(t1,2) + 16*t1*t2 + 
                  29*Power(t2,2)) + 
               Power(s2,4)*(20*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 11*Power(t2,3)) - 
               2*s2*t1*Power(t2,2)*
                (3*Power(t1,3) + 6*Power(t1,2)*t2 - 8*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(s2,2)*t2*
                (-6*Power(t1,4) + 17*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(s2,3)*(-6*Power(t1,4) + 10*Power(t1,3)*t2 + 
                  15*Power(t1,2)*Power(t2,2) - 17*t1*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,2)*t2*(Power(s2,7) + Power(s2,6)*(4*t1 - 34*t2) + 
               Power(t1,2)*Power(t2,3)*
                (2*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(s2,5)*(-4*Power(t1,2) + 33*t1*t2 + 
                  7*Power(t2,2)) + 
               s2*t1*Power(t2,2)*
                (6*Power(t1,3) - 14*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,4)*(-6*Power(t1,3) + 10*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,3)*(5*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(s2,2)*t2*
                (5*Power(t1,4) - 28*Power(t1,3)*t2 - 
                  12*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,4)*(-2*Power(s2,6) + 2*Power(s2,5)*(11*t1 + 8*t2) + 
               Power(s2,4)*(-43*Power(t1,2) + 8*t1*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*Power(t2,2)*
                (-4*Power(t1,2) + t1*t2 + 5*Power(t2,2)) + 
               2*s2*t1*t2*(-2*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,3)*(27*Power(t1,3) - 46*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,4) + 26*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) - 19*t1*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         Power(s,4)*(Power(s1,3)*
             (Power(s2,4) + 10*Power(s2,2)*Power(t1 - t2,2) + 
               Power(s2,3)*(-7*t1 + 2*t2) + 
               5*s2*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(t2,2)*(10*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2))) - 
            s2*t2*(s2 + t2)*(Power(t1 - t2,2)*t2*(t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) - 9*t1*t2 + 10*Power(t2,2)) + 
               s2*(-2*Power(t1,3) + Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
                  9*Power(t2,3))) - 
            Power(s1,2)*(Power(s2,4)*(6*t1 + 4*t2) + 
               Power(s2,3)*(-22*Power(t1,2) + 27*t1*t2 - 
                  17*Power(t2,2)) + 
               Power(s2,2)*(9*Power(t1,3) - 35*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + Power(t2,3)) + 
               Power(t2,2)*(9*Power(t1,3) - 8*Power(t1,2)*t2 - 
                  t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*t2*(9*Power(t1,3) - 35*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) + 14*Power(t2,3))) + 
            s1*(t1*Power(t1 - t2,2)*Power(t2,2)*(t1 + t2) + 
               Power(s2,4)*(5*Power(t1,2) - 14*t1*t2 + 19*Power(t2,2)) + 
               Power(s2,3)*(-6*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  8*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,4) - 8*Power(t1,3)*t2 + 
                  14*Power(t1,2)*Power(t2,2) - 20*Power(t2,4)) + 
               s2*t2*(Power(t1,4) - 8*Power(t1,3)*t2 + 
                  2*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) + 
                  7*Power(t2,4)))) + 
         Power(s,3)*(-(Power(s1,4)*
               (2*Power(s2,4) + 10*Power(s2,2)*Power(t1 - t2,2) + 
                 Power(s2,3)*(-9*t1 + 5*t2) + 
                 2*s2*t2*(5*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
                 Power(t2,2)*(10*Power(t1,2) - 7*t1*t2 + Power(t2,2)))) \
- 2*Power(s1,3)*(Power(s2,5) - Power(s2,4)*(11*t1 + 5*t2) + 
               Power(s2,3)*(22*Power(t1,2) - 21*t1*t2 + 
                  7*Power(t2,2)) + 
               Power(t2,2)*(-8*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - Power(t2,3)) - 
               Power(s2,2)*(8*Power(t1,3) - 30*Power(t1,2)*t2 + 
                  19*t1*Power(t2,2) + 3*Power(t2,3)) - 
               s2*t2*(8*Power(t1,3) - 30*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,5)*(6*t1 + 9*t2) + 
               Power(s2,4)*(-29*Power(t1,2) + 26*t1*t2 - 
                  41*Power(t2,2)) + 
               Power(s2,3)*(25*Power(t1,3) - 57*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) + 7*Power(t2,3)) - 
               2*Power(s2,2)*
                (2*Power(t1,4) - 15*Power(t1,3)*t2 + 
                  25*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) - 
                  15*Power(t2,4)) + 
               2*s2*t2*(-2*Power(t1,4) + 15*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 10*t1*Power(t2,3) - 
                  4*Power(t2,4)) - 
               Power(t2,2)*(4*Power(t1,4) - 3*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            s2*t2*(s2 + t2)*(2*Power(t1,2)*Power(t2,2)*(-t1 + t2) + 
               Power(s2,3)*(Power(t1,2) - 7*t1*t2 + 10*Power(t2,2)) - 
               2*Power(s2,2)*
                (Power(t1,3) - Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  8*Power(t2,3)) + 
               s2*(Power(t1,4) + 5*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            s1*(2*Power(t1,2)*Power(t2,4)*(-t1 + t2) + 
               Power(s2,5)*(-2*Power(t1,2) + 12*t1*t2 - 
                  25*Power(t2,2)) + 
               Power(s2,4)*(4*Power(t1,3) - 16*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-2*Power(t1,4) + 5*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  23*Power(t2,4)) + 
               2*s2*Power(t2,2)*
                (-Power(t1,4) + Power(t1,2)*Power(t2,2) - 
                  t1*Power(t2,3) + Power(t2,4)) + 
               Power(s2,3)*(-2*Power(t1,4) + 2*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  25*Power(t2,4)))) + 
         Power(s,2)*(Power(s1,5)*
             (Power(s2,4) + 5*Power(s2,2)*Power(t1 - t2,2) + 
               5*s2*t1*(t1 - 2*t2)*t2 + t1*(5*t1 - 2*t2)*Power(t2,2) + 
               Power(s2,3)*(-5*t1 + 4*t2)) + 
            s2*Power(t2,2)*(s2 + t2)*
             (Power(s2,4)*(2*t1 - 5*t2) + 
               Power(t1,2)*Power(t2,2)*(-t1 + t2) - 
               Power(s2,3)*(Power(t1,2) + 8*t1*t2 - 14*Power(t2,2)) + 
               s2*Power(t1,2)*
                (3*Power(t1,2) + 2*t1*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 6*Power(t2,3))) + 
            Power(s1,4)*(4*Power(s2,5) - Power(s2,4)*(27*t1 + 4*t2) + 
               t1*Power(t2,2)*
                (-14*Power(t1,2) + 8*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(40*Power(t1,2) - 37*t1*t2 + 9*Power(t2,2)) - 
               s2*t2*(14*Power(t1,3) - 50*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(14*Power(t1,3) - 50*Power(t1,2)*t2 + 
                  35*t1*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,3)*(Power(s2,6) - Power(s2,5)*(21*t1 + 26*t2) + 
               Power(s2,4)*(55*Power(t1,2) - 12*t1*t2 + 
                  18*Power(t2,2)) + 
               t1*Power(t2,2)*
                (6*Power(t1,3) - 3*Power(t1,2)*t2 - 11*t1*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(s2,3)*(39*Power(t1,3) - 72*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,2)*(6*Power(t1,4) - 42*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) + 36*t1*Power(t2,3) - 
                  19*Power(t2,4)) + 
               s2*t2*(6*Power(t1,4) - 42*Power(t1,3)*t2 - 
                  13*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) + 
                  Power(t2,4))) - 
            Power(s1,2)*(2*Power(s2,6)*(t1 + 3*t2) + 
               Power(s2,5)*(-12*Power(t1,2) + 13*t1*t2 - 
                  57*Power(t2,2)) + 
               t1*Power(t2,4)*
                (-7*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,4)*(16*Power(t1,3) - 27*Power(t1,2)*t2 + 
                  31*t1*Power(t2,2) + 2*Power(t2,3)) + 
               s2*t1*Power(t2,2)*
                (-6*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 9*Power(t2,3)) - 
               2*Power(s2,2)*t2*
                (3*Power(t1,4) - 9*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(s2,3)*(-6*Power(t1,4) + 12*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  23*Power(t2,4))) + 
            s1*t2*(-4*Power(s2,6)*(t1 - 4*t2) + 
               Power(t1,3)*Power(t2,3)*(-t1 + t2) + 
               2*Power(s2,5)*(3*Power(t1,2) + t1*t2 - 16*Power(t2,2)) + 
               Power(s2,4)*(2*Power(t1,3) - 24*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 15*Power(t2,3)) + 
               s2*t1*Power(t2,2)*
                (-3*Power(t1,3) + 10*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-4*Power(t1,4) + 9*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,3)*(-4*Power(t1,4) - 8*Power(t1,2)*Power(t2,2) + 
                  11*t1*Power(t2,3) + 27*Power(t2,4)))))*lg2(-t1))/
     (Power(s1,2)*Power(s2,2)*t1*Power(s - s2 + t1,2)*Power(t2,2)*
       Power(s - s1 + t2,2)*(s2 + t2)) + 
    (8*(2*Power(s,6)*t1*(t1 - t2)*t2*(s1*(s2 + t1) + s2*t2) - 
         Power(s2,3)*t1*Power(t2,4)*
          (-2*s2*Power(t1 + t2,2) + t1*t2*(2*t1 + t2) + 
            Power(s2,2)*(3*t1 + 2*t2)) + 
         Power(s1,7)*s2*(Power(s2,2)*t2 + Power(t1,2)*(t1 + t2) - 
            s2*t1*(t1 + 4*t2)) + 
         Power(s1,6)*(3*Power(s2,4)*t2 - 3*s2*Power(t1,2)*t2*(t1 + t2) - 
            Power(t1,3)*t2*(2*t1 + 3*t2) - 
            Power(s2,3)*(Power(t1,2) + 9*t1*t2 + 4*Power(t2,2)) + 
            Power(s2,2)*t1*(Power(t1,2) + 6*t1*t2 + 18*Power(t2,2))) + 
         s1*Power(s2,2)*Power(t2,3)*
          (Power(t1,3)*(2*t1 - t2)*t2 + 2*Power(s2,4)*(t1 + t2) - 
            2*Power(s2,3)*(2*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(3*Power(t1,3) - 2*t1*Power(t2,2)) + 
            s2*t1*(-2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
               2*Power(t2,3))) + 
         Power(s1,5)*(2*Power(s2,5)*t2 + 
            Power(t1,3)*Power(t2,2)*(5*t1 + 8*t2) + 
            Power(s2,4)*(Power(t1,2) - 2*t1*t2 - 8*Power(t2,2)) + 
            s2*Power(t1,2)*t2*
             (-4*Power(t1,2) - 5*t1*t2 + 5*Power(t2,2)) - 
            Power(s2,2)*t1*t2*
             (5*Power(t1,2) + 2*t1*t2 + 34*Power(t2,2)) + 
            Power(s2,3)*(-Power(t1,3) + 4*Power(t1,2)*t2 + 
               28*t1*Power(t2,2) + 7*Power(t2,3))) + 
         Power(s1,4)*(-(Power(t1,3)*Power(t2,3)*(4*t1 + 7*t2)) + 
            s2*Power(t1,2)*Power(t2,2)*
             (7*Power(t1,2) + 17*t1*t2 - 7*Power(t2,2)) + 
            Power(s2,5)*(Power(t1,2) + 6*t1*t2 - Power(t2,2)) + 
            Power(s2,3)*t2*(6*Power(t1,3) + 14*Power(t1,2)*t2 - 
               31*t1*Power(t2,2) - 7*Power(t2,3)) - 
            Power(s2,4)*(Power(t1,3) + 8*Power(t1,2)*t2 + 
               2*t1*Power(t2,2) - 7*Power(t2,3)) + 
            Power(s2,2)*t1*t2*
             (-7*Power(t1,3) + Power(t1,2)*t2 - 18*t1*Power(t2,2) + 
               35*Power(t2,3))) + 
         Power(s1,2)*s2*Power(t2,2)*
          (Power(t1,2)*(3*t1 - 2*t2)*Power(t2,3) - 
            2*Power(s2,5)*(3*t1 + 2*t2) + 
            Power(s2,4)*(16*Power(t1,2) + 15*t1*t2 + 7*Power(t2,2)) - 
            Power(s2,3)*t1*(11*Power(t1,2) + 23*t1*t2 + 
               12*Power(t2,2)) + 
            s2*t1*t2*(-7*Power(t1,3) + Power(t1,2)*t2 - 
               8*t1*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,2)*(4*Power(t1,4) + 8*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) - 7*t1*Power(t2,3) - 
               Power(t2,4))) + 
         Power(s1,3)*t2*(2*Power(s2,6)*(2*t1 + t2) + 
            Power(t1,3)*Power(t2,3)*(t1 + 2*t2) + 
            s2*Power(t1,2)*Power(t2,2)*
             (-3*Power(t1,2) - 13*t1*t2 + 6*Power(t2,2)) - 
            Power(s2,5)*(8*Power(t1,2) + 15*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,2)*t1*t2*
             (11*Power(t1,3) + 2*Power(t1,2)*t2 + 23*t1*Power(t2,2) - 
               20*Power(t2,3)) + 
            Power(s2,4)*(7*Power(t1,3) + 29*Power(t1,2)*t2 + 
               16*t1*Power(t2,2) - 2*Power(t2,3)) + 
            Power(s2,3)*(-4*Power(t1,4) - 15*Power(t1,3)*t2 - 
               34*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) + 
               4*Power(t2,4))) + 
         Power(s,5)*(s2*t1*t2*
             (t2*(Power(t1,2) + 5*t1*t2 - 6*Power(t2,2)) - 
               2*s2*(Power(t1,2) + 2*t1*t2 - 4*Power(t2,2))) + 
            s1*t2*(Power(t1,2)*
                (2*Power(t1,2) + 3*t1*t2 - 5*Power(t2,2)) + 
               2*s2*t1*(-2*Power(t1,2) + t1*t2 + Power(t2,2)) + 
               2*Power(s2,2)*(-2*Power(t1,2) + 5*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,2)*t2*(2*t1 + t2) + 
               2*t1*t2*(-5*Power(t1,2) + 3*t1*t2 + Power(t2,2)) - 
               s2*(Power(t1,3) + 11*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  Power(t2,3)))) + 
         Power(s,4)*(s2*t1*t2*
             (4*Power(s2,2)*(Power(t1,2) - 3*Power(t2,2)) + 
               3*Power(t2,2)*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) - 
               s2*(Power(t1,3) + 8*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
                  22*Power(t2,3))) + 
            Power(s1,3)*(2*Power(s2,3)*t2 + 
               2*t1*t2*(10*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2)) - 
               Power(s2,2)*(Power(t1,2) + 17*t1*t2 + 8*Power(t2,2)) + 
               s2*(5*Power(t1,3) + 25*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 3*Power(t2,3))) - 
            s1*t2*(Power(t1,2)*t2*
                (-5*Power(t1,2) + t1*t2 + 4*Power(t2,2)) + 
               2*Power(s2,3)*(Power(t1,2) + 10*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*(4*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 12*Power(t2,3)) - 
               Power(s2,2)*(14*Power(t1,3) + Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(-4*Power(s2,3)*t2*(2*t1 + t2) + 
               Power(s2,2)*(Power(t1,3) + 24*Power(t1,2)*t2 - 
                  27*t1*Power(t2,2) + 2*Power(t2,3)) - 
               s2*t2*(-13*Power(t1,3) + 14*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 3*Power(t2,3)) + 
               t1*t2*(-10*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(s,3)*(-(Power(s1,4)*
               (7*Power(s2,3)*t2 + 
                 2*t1*t2*(10*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) - 
                 Power(s2,2)*
                  (4*Power(t1,2) + 31*t1*t2 + 12*Power(t2,2)) + 
                 s2*(10*Power(t1,3) + 30*Power(t1,2)*t2 + 
                    t1*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(s1,3)*(-4*Power(s2,4)*t2 + 
               Power(s2,3)*(Power(t1,2) + 27*t1*t2 + 19*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,3) - 54*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 17*Power(t2,3)) + 
               t1*t2*(20*Power(t1,3) + 30*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) - 10*Power(t2,3)) + 
               s2*t2*(-12*Power(t1,3) + 13*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) + 6*Power(t2,3))) + 
            s2*t1*t2*(-(Power(t2,3)*
                  (-3*Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
               Power(s2,3)*(-2*Power(t1,2) + 4*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) + 11*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) - 30*Power(t2,3)) + 
               s2*t2*(-4*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,2)*(2*Power(s2,4)*t1*t2 + 
               Power(s2,2)*t2*
                (-25*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  17*t1*Power(t2,2) + 4*Power(t2,3)) + 
               t1*Power(t2,2)*
                (-20*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*(Power(t1,3) - 2*Power(t1,2)*t2 + 
                  47*t1*Power(t2,2) + 8*Power(t2,3)) + 
               s2*t2*(16*Power(t1,4) + 14*Power(t1,3)*t2 + 
                  7*Power(t1,2)*Power(t2,2) - 32*t1*Power(t2,3) - 
                  3*Power(t2,4))) + 
            s1*t2*(Power(t1,2)*Power(t2,2)*
                (4*Power(t1,2) - 3*t1*t2 - Power(t2,2)) + 
               4*Power(s2,4)*(2*Power(t1,2) + 5*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*t2*(-7*Power(t1,3) - 3*t1*Power(t2,2) + 
                  10*Power(t2,3)) - 
               Power(s2,3)*(21*Power(t1,3) + 9*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,2)*(10*Power(t1,4) + 21*Power(t1,3)*t2 + 
                  12*Power(t1,2)*Power(t2,2) - 26*t1*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(s,2)*(Power(s1,5)*
             (9*Power(s2,3)*t2 + 
               2*t1*t2*(5*Power(t1,2) + 3*t1*t2 - Power(t2,2)) - 
               Power(s2,2)*(6*Power(t1,2) + 31*t1*t2 + 8*Power(t2,2)) + 
               s2*(10*Power(t1,3) + 20*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + Power(t2,3))) + 
            s2*t1*Power(t2,2)*
             (t1*(t1 - t2)*Power(t2,3) - 2*Power(s2,4)*(t1 + t2) + 
               Power(s2,3)*(-4*Power(t1,2) + 14*t1*t2 + 
                  18*Power(t2,2)) + 
               Power(s2,2)*(3*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  16*t1*Power(t2,2) - 24*Power(t2,3)) + 
               s2*t2*(-5*Power(t1,3) - 2*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,4)*(11*Power(s2,4)*t2 - 
               3*Power(s2,3)*
                (Power(t1,2) + 13*t1*t2 + 10*Power(t2,2)) - 
               t1*t2*(20*Power(t1,3) + 30*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - 5*Power(t2,3)) - 
               s2*t2*(2*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(6*Power(t1,3) + 58*Power(t1,2)*t2 + 
                  57*t1*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,3)*(2*Power(s2,5)*t2 + 
               Power(s2,4)*(Power(t1,2) - 2*t1*t2 - 10*Power(t2,2)) + 
               t1*Power(t2,2)*
                (30*Power(t1,3) + 21*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,3)*(-3*Power(t1,3) + 14*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 18*Power(t2,3)) - 
               Power(s2,2)*t2*
                (-11*Power(t1,3) + Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) + 18*Power(t2,3)) + 
               s2*t2*(-24*Power(t1,4) - 22*Power(t1,3)*t2 - 
                  20*Power(t1,2)*Power(t2,2) + 56*t1*Power(t2,3) + 
                  3*Power(t2,4))) + 
            s1*t2*(Power(t1,3)*(t1 - t2)*Power(t2,3) - 
               2*Power(s2,5)*
                (2*Power(t1,2) + 5*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*t1*t2*
                (19*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 22*Power(t2,3)) + 
               s2*t1*Power(t2,2)*
                (-3*Power(t1,3) + Power(t1,2)*t2 + 2*Power(t2,3)) + 
               Power(s2,4)*(9*Power(t1,3) + 15*Power(t1,2)*t2 + 
                  28*t1*Power(t2,2) + 18*Power(t2,3)) - 
               Power(s2,3)*(6*Power(t1,4) + 29*Power(t1,3)*t2 + 
                  10*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,2)*(4*Power(s2,5)*t2*(t1 + t2) + 
               t1*Power(t2,3)*
                (-12*Power(t1,3) - Power(t1,2)*t2 + 7*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,3)*t2*
                (36*Power(t1,3) + 43*Power(t1,2)*t2 + 
                  57*t1*Power(t2,2) + 8*Power(t2,3)) - 
               Power(s2,4)*(Power(t1,3) + 20*Power(t1,2)*t2 + 
                  53*t1*Power(t2,2) + 22*Power(t2,3)) + 
               s2*Power(t2,2)*
                (21*Power(t1,4) + 8*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) - 26*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s2,2)*t2*
                (-24*Power(t1,4) - 17*Power(t1,3)*t2 - 
                  50*Power(t1,2)*Power(t2,2) + 35*t1*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         s*(-(Power(s1,6)*(5*Power(s2,3)*t2 + 
                 2*Power(t1,2)*t2*(t1 + t2) - 
                 Power(s2,2)*
                  (4*Power(t1,2) + 17*t1*t2 + 2*Power(t2,2)) + 
                 s2*t1*(5*Power(t1,2) + 7*t1*t2 + 4*Power(t2,2)))) + 
            Power(s2,2)*t1*Power(t2,3)*
             (-(Power(s2,3)*(5*t1 + 4*t2)) + 
               2*Power(s2,2)*t2*(7*t1 + 6*t2) + 
               t1*t2*(-2*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 3*Power(t1,2)*t2 - 9*t1*Power(t2,2) - 
                  6*Power(t2,3))) + 
            Power(s1,5)*(-10*Power(s2,4)*t2 + 
               Power(t1,2)*t2*
                (10*Power(t1,2) + 15*t1*t2 + 2*Power(t2,2)) + 
               s2*t1*t2*(8*Power(t1,2) + 5*t1*t2 + 18*Power(t2,2)) + 
               Power(s2,3)*(3*Power(t1,2) + 29*t1*t2 + 19*Power(t2,2)) - 
               Power(s2,2)*(4*Power(t1,3) + 30*Power(t1,2)*t2 + 
                  59*t1*Power(t2,2) + 7*Power(t2,3))) + 
            s1*Power(s2,2)*Power(t2,2)*
             (2*Power(s2,4)*(t1 + t2) - 
               Power(s2,3)*(7*Power(t1,2) + 14*t1*t2 + 10*Power(t2,2)) + 
               t1*t2*(12*Power(t1,3) - 5*Power(t1,2)*t2 - 
                  4*Power(t2,3)) + 
               2*Power(s2,2)*
                (5*Power(t1,3) + 5*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s2*t1*(-7*Power(t1,3) - 11*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 14*Power(t2,3))) + 
            Power(s1,3)*(Power(t1,2)*Power(t2,3)*
                (12*Power(t1,2) + 11*t1*t2 - 6*Power(t2,2)) - 
               Power(s2,5)*(Power(t1,2) + 12*t1*t2 + 3*Power(t2,2)) - 
               s2*t1*Power(t2,2)*
                (21*Power(t1,3) + 28*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  22*Power(t2,3)) + 
               2*Power(s2,4)*
                (Power(t1,3) + 10*Power(t1,2)*t2 + 17*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,3)*t2*
                (-25*Power(t1,3) - 48*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,2)*t2*
                (22*Power(t1,4) + 3*Power(t1,3)*t2 + 
                  62*Power(t1,2)*Power(t2,2) - 69*t1*Power(t2,3) - 
                  7*Power(t2,4))) + 
            Power(s1,2)*t2*(-2*Power(s2,6)*(t1 + t2) - 
               Power(t1,2)*Power(t2,3)*
                (2*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(s2,5)*(9*Power(t1,2) + 30*t1*t2 + 17*Power(t2,2)) + 
               3*s2*t1*Power(t2,2)*
                (2*Power(t1,3) + 3*Power(t1,2)*t2 - t1*Power(t2,2) - 
                  2*Power(t2,3)) - 
               Power(s2,4)*(14*Power(t1,3) + 55*Power(t1,2)*t2 + 
                  54*t1*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s2,3)*(9*Power(t1,4) + 33*Power(t1,3)*t2 + 
                  57*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-26*Power(t1,4) + 2*Power(t1,3)*t2 - 
                  40*Power(t1,2)*Power(t2,2) + 29*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,4)*(-4*Power(s2,5)*t2 - 
               2*Power(s2,4)*(Power(t1,2) - t1*t2 - 9*Power(t2,2)) + 
               Power(t1,2)*Power(t2,2)*
                (-20*Power(t1,2) - 23*t1*t2 + 4*Power(t2,2)) + 
               s2*t1*t2*(16*Power(t1,3) + 17*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 30*Power(t2,3)) + 
               Power(s2,3)*(3*Power(t1,3) - 14*Power(t1,2)*t2 - 
                  46*t1*Power(t2,2) - 25*Power(t2,3)) + 
               Power(s2,2)*(7*Power(t1,3)*t2 + 86*t1*Power(t2,3) + 
                  10*Power(t2,4)))))*lg2(s - s2 + t1))/
     (Power(s1,2)*Power(s2,2)*Power(t1,2)*(s - s2 + t1)*Power(t2,2)*
       Power(s - s1 + t2,2)*(s - s1 - s2 + t2)) - 
    (8*(2*Power(s1,4)*Power(s2 - t1,2)*t1 - 
         2*Power(s,4)*(t1 - t2)*t2*(s2 + t2) + 
         s2*(s2 - t1)*Power(t2,2)*
          (Power(s2,3) + s2*t1*(4*t1 - 3*t2) + 
            Power(s2,2)*(-2*t1 + t2) + Power(t1,2)*(-2*t1 + 3*t2)) + 
         Power(s1,3)*(s2 - t1)*
          (2*Power(s2,3) - Power(s2,2)*(t1 - 2*t2) + 4*Power(t1,2)*t2 - 
            s2*t1*(t1 + 7*t2)) + 
         Power(s1,2)*(Power(s2,5) + 3*Power(t1,3)*Power(t2,2) - 
            Power(s2,4)*(3*t1 + 2*t2) + 
            s2*Power(t1,2)*(Power(t1,2) - 5*t1*t2 - 8*Power(t2,2)) - 
            3*Power(s2,2)*t1*(Power(t1,2) - 3*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,3)*(4*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2))) + 
         s1*t2*(-2*Power(s2,5) + Power(s2,4)*(6*t1 - t2) + 
            Power(t1,3)*(t1 - 2*t2)*t2 + 
            Power(s2,2)*t1*(7*Power(t1,2) - 11*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,3)*(-9*Power(t1,2) + 8*t1*t2 + Power(t2,2)) + 
            s2*Power(t1,2)*(-2*Power(t1,2) + 3*t1*t2 + 3*Power(t2,2))) + 
         Power(s,3)*(4*t1*Power(t2,2)*(-t1 + t2) - 
            Power(s2,2)*(Power(t1,2) - 5*t1*t2 + 6*Power(t2,2)) + 
            s2*(Power(t1,3) - 4*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
               6*Power(t2,3)) + 
            2*s1*(-Power(t1,3) + t1*Power(t2,2) - Power(t2,3) + 
               s2*t2*(3*t1 + t2) + Power(s2,2)*(t1 + 2*t2))) + 
         Power(s,2)*(3*Power(t1,2)*Power(t2,2)*(-t1 + t2) + 
            Power(s2,3)*(Power(t1,2) - 3*t1*t2 + 7*Power(t2,2)) + 
            s2*t1*(Power(t1,3) + 6*t1*Power(t2,2) - 11*Power(t2,3)) + 
            Power(s2,2)*(-2*Power(t1,3) + 2*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + 7*Power(t2,3)) + 
            2*Power(s1,2)*(Power(s2,3) + 3*Power(t1,3) - 
               Power(s2,2)*(2*t1 + t2) - 
               s2*(2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) - 
            s1*(Power(s2,3)*(4*t1 + 9*t2) + 
               4*t1*t2*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-5*Power(t1,2) + 6*t1*t2 + 4*Power(t2,2)) + 
               s2*(Power(t1,3) - 12*Power(t1,2)*t2 - 4*t1*Power(t2,2) - 
                  5*Power(t2,3)))) - 
         s*(2*Power(s1,3)*(Power(s2,3) + 3*Power(t1,3) + 
               Power(s2,2)*t2 - s2*t1*(4*t1 + 3*t2)) + 
            t2*(4*Power(s2,4)*t2 + Power(t1,3)*(t1 - t2)*t2 + 
               Power(s2,2)*t1*
                (5*Power(t1,2) + 7*t1*t2 - 11*Power(t2,2)) + 
               Power(s2,3)*(-3*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               s2*Power(t1,2)*(-2*Power(t1,2) - 4*t1*t2 + 9*Power(t2,2))) \
+ Power(s1,2)*(3*Power(s2,4) - 8*Power(t1,3)*t2 - 
               4*Power(s2,3)*(2*t1 + t2) + 
               Power(s2,2)*(4*Power(t1,2) - 12*t1*t2 - 7*Power(t2,2)) + 
               s2*t1*(Power(t1,2) + 19*t1*t2 + 10*Power(t2,2))) + 
            s1*(3*Power(t1,2)*Power(t2,3) - Power(s2,4)*(2*t1 + 7*t2) + 
               Power(s2,3)*(5*Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)) + 
               s2*t1*(2*Power(t1,3) - 5*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(s2,2)*(-5*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 4*Power(t2,3)))))*
       lg2(-(s1/(s*(-s + s1 - t2)))))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*Power(t2,2)*(s2 - t1 + t2)) - 
    (4*(2*Power(s,5)*t1*(t1 - t2)*t2 + 
         s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*t1*(s1 + t1 - t2)*
          (s1*(s2 - t1) - s2*t2) + 
         Power(s,4)*t2*(4*Power(t1,2)*(t1 - t2) - 
            s2*(7*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
            s1*(2*t1*(-2*t1 + t2) + s2*(t1 + t2))) + 
         Power(s,3)*(Power(s1,2)*
             (s2*t1*(t1 - 6*t2) + Power(s2,2)*t2 + t1*(3*t1 - t2)*t2) + 
            t2*(3*Power(t1,3)*(t1 - t2) - 
               s2*t1*(13*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(10*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2))) + 
            s1*(2*Power(t1,2)*t2*(-3*t1 + t2) - 
               Power(s2,2)*t2*(t1 + 4*t2) + 
               s2*t1*(Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)))) + 
         Power(s,2)*(Power(s1,3)*
             (Power(s2,2)*t1 - Power(t1,2)*t2 - 
               s2*(3*Power(t1,2) - 6*t1*t2 + Power(t2,2))) + 
            s1*(-3*Power(t1,4)*t2 + Power(s2,3)*t2*(-t1 + 5*t2) + 
               Power(s2,2)*t1*
                (-2*Power(t1,2) + 5*t1*t2 - 12*Power(t2,2)) + 
               s2*t1*t2*(5*Power(t1,2) + t1*t2 + Power(t2,2))) + 
            t2*(Power(t1,4)*(t1 - t2) + 
               s2*(-9*Power(t1,4) + 6*Power(t1,3)*t2) + 
               Power(s2,3)*(-7*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*t1*(15*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2))\
) - Power(s1,2)*(Power(s2,2)*t1*(t1 - 11*t2) + 2*Power(s2,3)*t2 + 
               Power(t1,2)*t2*(-3*t1 + t2) + 
               s2*(3*Power(t1,3) + Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  Power(t2,3)))) - 
         s*(Power(s1,4)*s2*(t1*(-4*t1 + t2) + s2*(2*t1 + t2)) + 
            Power(s1,3)*(2*Power(s2,3)*t1 + Power(t1,3)*t2 + 
               Power(s2,2)*(-2*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) - 
               s2*t1*(4*Power(t1,2) - 2*t1*t2 + Power(t2,2))) - 
            s2*(s2 - t1)*t2*(Power(t1,3)*(2*t1 - t2) + 
               2*s2*Power(t1,2)*(-2*t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) - t1*t2 + Power(t2,2))) + 
            s1*(-2*s2*Power(t1,4)*t2 + Power(t1,5)*t2 + 
               Power(s2,2)*t1*Power(t2,2)*(2*t1 + t2) + 
               Power(s2,4)*t2*(-t1 + 2*t2) - 
               Power(s2,3)*t1*(Power(t1,2) - 4*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*s2*(-(Power(s2,3)*t2) + 
               Power(t1,2)*t2*(t1 + 2*t2) + Power(s2,2)*t1*(t1 + 3*t2) + 
               s2*(-4*Power(t1,3) + 4*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 
                  Power(t2,3)))))*lg2((s - s1 - s2)/(-s + s1 - t2)))/
     (s*s1*s2*Power(t1,2)*(s - s2 + t1)*(s1 + t1 - t2)*t2*(s - s1 + t2)) - 
    (4*(Power(s1,3)*Power(s2 - t1,2)*(s2 + t1) - 
         2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*(2*s2 - t1)*Power(t1 - t2,3) - 
         s*(3*Power(s2,2) - 4*s2*t1 + Power(t1,2))*Power(t1 - t2,3) + 
         s2*(Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*(2*Power(t1,3) - 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - Power(t2,3)) + 
            2*s2*t1*(-2*Power(t1,3) + 4*Power(t1,2)*t2 - 
               3*t1*Power(t2,2) + Power(t2,3))) - 
         Power(s1,2)*(-(s2*(s2 - t1)*
               (s2*(4*t1 - 3*t2) + t1*(-4*t1 + t2))) + 
            s*(Power(s2,2)*(t1 - 3*t2) + 3*Power(t1,2)*(t1 - t2) + 
               2*s2*t1*(-2*t1 + t2))) + 
         s1*(Power(t1,3)*Power(t1 - t2,2) + 
            Power(s2,2)*t1*(-5*Power(t1,2) + 14*t1*t2 - 5*Power(t2,2)) + 
            Power(s2,3)*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
            2*Power(s,2)*(t1 - t2)*(2*t1*(-t1 + t2) + s2*(t1 + 2*t2)) + 
            2*s*(Power(t1,2)*Power(t1 - t2,2) - 
               s2*t1*(2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)))))*
       lg2(-((s*(s1 + s2))/(s1 + t1 - t2))))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) - 
    (4*(2*Power(s1,2)*Power(s2,2)*(s2 - t1)*t1*(s1 + t1)*(s1 - t2)*
          (s1 + s2 - t2)*t2*Power(s2 - t1 + t2,3)*
          Power(s1*(-s2 + t1) + s2*t2,3) + 
         2*s*Power(s1,2)*Power(s2,2)*t1*(s1 + t1)*t2*
          Power(s2 - t1 + t2,3)*
          (-3*Power(s1,5)*Power(s2 - t1,3) - 
            Power(s1,4)*Power(s2 - t1,2)*
             (5*Power(s2,2) + t1*(-4*t1 + 7*t2) - s2*(t1 + 9*t2)) + 
            Power(s2,2)*Power(t2,3)*
             (3*Power(s2,3) - t1*(t1 - 2*t2)*t2 + 
               Power(s2,2)*(-4*t1 + t2) + 
               s2*(Power(t1,2) + t1*t2 - 3*Power(t2,2))) + 
            Power(s1,2)*t2*(9*Power(s2,5) + 
               Power(t1,3)*(2*t1 - t2)*t2 - 
               4*Power(s2,4)*(4*t1 + 3*t2) + 
               3*Power(s2,3)*(Power(t1,2) + 6*t1*t2 - 2*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-2*Power(t1,2) - 9*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*t1*(6*Power(t1,2) + t1*t2 + 3*Power(t2,2))) - 
            Power(s1,3)*(s2 - t1)*
             (3*Power(s2,4) + Power(t1,2)*t2*(-6*t1 + 5*t2) - 
               Power(s2,3)*(3*t1 + 14*t2) + 
               s2*t1*(3*Power(t1,2) + 9*t1*t2 - 13*Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,2) + 11*t1*t2 + 6*Power(t2,2))) \
+ s1*s2*Power(t2,2)*(-9*Power(s2,4) + Power(t1,2)*t2*(t1 + t2) + 
               2*Power(s2,3)*(7*t1 + t2) - 
               s2*t1*(Power(t1,2) - t1*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,2) - 5*t1*t2 + 9*Power(t2,2)))) - 
         2*Power(s,10)*t2*(-(s2*Power(t1,2)*(t1 - t2)*t2*
               Power(s2 - t1 + t2,3)) + 
            Power(s1,2)*(2*Power(t1,2)*Power(t1 - t2,4) + 
               Power(s2,4)*(-Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
               s2*Power(t1 - t2,3)*
                (3*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (5*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(-2*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 3*Power(t2,3))) + 
            s1*t1*(2*Power(t1,2)*Power(t1 - t2,4) + 
               Power(s2,4)*(-Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
               s2*Power(t1 - t2,3)*
                (3*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (5*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(-2*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 3*Power(t2,3)))) + 
         Power(s,2)*Power(s1,2)*Power(s2,2)*t1*(s1 + t1)*t2*
          Power(s2 - t1 + t2,3)*
          (14*Power(s1,5)*Power(s2 - t1,2) + 
            2*Power(s1,4)*(s2 - t1)*
             (23*Power(s2,2) - 33*s2*t1 + 10*Power(t1,2) - 29*s2*t2 + 
               13*t1*t2) + Power(s1,3)*
             (46*Power(s2,4) - 2*Power(s2,3)*(61*t1 + 74*t2) + 
               Power(t1,2)*(-13*Power(t1,2) + 36*t1*t2 + 
                  9*Power(t2,2)) - 
               2*s2*t1*(2*Power(t1,2) + 68*t1*t2 + 45*Power(t2,2)) + 
               Power(s2,2)*(93*Power(t1,2) + 248*t1*t2 + 83*Power(t2,2))\
) + s1*t2*(-28*Power(s2,5) + 2*Power(s2,4)*(28*t1 + 57*t2) + 
               s2*Power(t1,2)*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               Power(t1,2)*t2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               2*Power(s2,3)*
                (17*Power(t1,2) + 69*t1*t2 + 29*Power(t2,2)) + 
               Power(s2,2)*(3*Power(t1,3) + 37*Power(t1,2)*t2 + 
                  47*t1*Power(t2,2) - 5*Power(t2,3))) + 
            s2*Power(t2,2)*(14*Power(s2,4) - 
               2*Power(s2,3)*(6*t1 + 17*t2) + 
               Power(s2,2)*(-3*Power(t1,2) + 34*t1*t2 + Power(t2,2)) - 
               t1*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               s2*(Power(t1,3) - 3*Power(t1,2)*t2 - 7*t1*Power(t2,2) + 
                  7*Power(t2,3))) + 
            Power(s1,2)*(14*Power(s2,5) - 
               2*Power(s2,4)*(22*t1 + 63*t2) + 
               2*Power(t1,2)*t2*
                (7*Power(t1,2) - 10*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(39*Power(t1,2) + 226*t1*t2 + 
                  159*Power(t2,2)) + 
               s2*t1*(-7*Power(t1,3) + 7*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) + 37*Power(t2,3)) - 
               Power(s2,2)*(2*Power(t1,3) + 121*Power(t1,2)*t2 + 
                  176*t1*Power(t2,2) + 41*Power(t2,3)))) - 
         2*Power(s,9)*(-(s2*Power(t1,2)*t2*Power(s2 - t1 + t2,3)*
               (3*(t1 - t2)*Power(t2,2) + 
                 s2*(Power(t1,2) - 6*t1*t2 + 6*Power(t2,2)))) + 
            s1*t1*t2*(Power(t1,2)*Power(t1 - t2,4)*(4*t1 + 5*t2) + 
               3*Power(s2,5)*(Power(t1,2) + 4*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,4)*(4*Power(t1,3) - 44*Power(t1,2)*t2 + 
                  43*t1*Power(t2,2) - 15*Power(t2,3)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (20*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 3*Power(t2,3)) - 
               s2*Power(t1 - t2,3)*
                (15*Power(t1,3) - 11*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,3)*(-17*Power(t1,4) + 52*Power(t1,3)*t2 - 
                  56*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) - 
                  9*Power(t2,4))) + 
            Power(s1,3)*(2*Power(s2,5)*Power(t2,2) - 
               2*t1*Power(t1 - t2,3)*t2*
                (5*Power(t1,2) - 3*t1*t2 - Power(t2,2)) + 
               Power(s2,4)*(Power(t1,3) + 4*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(-3*Power(t1,4) + 10*Power(t1,3)*t2 - 
                  41*Power(t1,2)*Power(t2,2) + 34*t1*Power(t2,3) - 
                  9*Power(t2,4)) - 
               s2*Power(t1 - t2,2)*
                (Power(t1,4) - 15*Power(t1,3)*t2 + 
                  16*Power(t1,2)*Power(t2,2) - 10*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*(3*Power(t1,5) - 24*Power(t1,4)*t2 + 
                  75*Power(t1,3)*Power(t2,2) - 
                  92*Power(t1,2)*Power(t2,3) + 51*t1*Power(t2,4) - 
                  13*Power(t2,5))) + 
            Power(s1,2)*(2*Power(s2,5)*t2*
                (Power(t1,2) + 6*t1*t2 - 3*Power(t2,2)) - 
               Power(t1,2)*Power(t1 - t2,3)*t2*
                (6*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,4)*(Power(t1,4) + 11*Power(t1,3)*t2 - 
                  42*Power(t1,2)*Power(t2,2) + 43*t1*Power(t2,3) - 
                  15*Power(t2,4)) - 
               s2*Power(t1 - t2,2)*
                (Power(t1,5) - 5*Power(t1,3)*Power(t2,2) - 
                  10*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) - 
                  3*Power(t2,5)) - 
               Power(s2,3)*(3*Power(t1,5) + 10*Power(t1,4)*t2 - 
                  29*Power(t1,3)*Power(t2,2) + 
                  46*Power(t1,2)*Power(t2,3) - 30*t1*Power(t2,4) + 
                  9*Power(t2,5)) + 
               Power(s2,2)*(3*Power(t1,6) - 3*Power(t1,5)*t2 + 
                  19*Power(t1,4)*Power(t2,2) - 
                  45*Power(t1,3)*Power(t2,3) + 
                  44*Power(t1,2)*Power(t2,4) - 21*t1*Power(t2,5) + 
                  3*Power(t2,6)))) + 
         Power(s,8)*(-2*s2*Power(t1,2)*t2*Power(s2 - t1 + t2,3)*
             (Power(s2,2)*(3*Power(t1,2) - 14*t1*t2 + 15*Power(t2,2)) + 
               s2*(-2*Power(t1,3) + 15*t1*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t2*(Power(t1,3) - Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  3*Power(t2,3))) + 
            s1*t1*t2*(6*Power(s2,6)*
                (Power(t1,2) + 10*t1*t2 - 5*Power(t2,2)) - 
               4*Power(t1,2)*Power(t1 - t2,4)*
                (Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) + 
               2*Power(s2,5)*
                (4*Power(t1,3) - 95*Power(t1,2)*t2 + 
                  77*t1*Power(t2,2) - 29*Power(t2,3)) + 
               2*Power(s2,4)*t1*
                (-23*Power(t1,3) + 78*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 24*Power(t2,3)) - 
               2*Power(s2,2)*Power(t1 - t2,2)*
                (35*Power(t1,4) + 4*Power(t1,3)*t2 - 
                  37*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               s2*Power(t1 - t2,3)*
                (32*Power(t1,4) + 34*Power(t1,3)*t2 - 
                  29*Power(t1,2)*Power(t2,2) - 36*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,3)*(74*Power(t1,5) - 74*Power(t1,4)*t2 - 
                  297*Power(t1,3)*Power(t2,2) + 
                  513*Power(t1,2)*Power(t2,3) - 264*t1*Power(t2,4) + 
                  48*Power(t2,5))) + 
            Power(s1,4)*(-2*Power(s2,6)*t2 - 
               2*Power(s2,5)*(Power(t1,2) - 6*t1*t2 - 5*Power(t2,2)) - 
               4*t1*Power(t1 - t2,3)*t2*
                (10*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2)) + 
               8*Power(s2,4)*
                (2*Power(t1,3) - 2*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,3)*(-36*Power(t1,4) + 69*Power(t1,3)*t2 - 
                  249*Power(t1,2)*Power(t2,2) + 144*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               2*Power(s2,2)*
                (16*Power(t1,5) - 54*Power(t1,4)*t2 + 
                  170*Power(t1,3)*Power(t2,2) - 
                  214*Power(t1,2)*Power(t2,3) + 102*t1*Power(t2,4) - 
                  19*Power(t2,5)) + 
               s2*(-10*Power(t1,6) + 79*Power(t1,5)*t2 - 
                  173*Power(t1,4)*Power(t2,2) + 
                  181*Power(t1,3)*Power(t2,3) - 
                  135*Power(t1,2)*Power(t2,4) + 76*t1*Power(t2,5) - 
                  18*Power(t2,6))) + 
            Power(s1,3)*(18*Power(s2,6)*Power(t2,2) + 
               2*t1*Power(t1 - t2,3)*Power(t2,2)*
                (13*Power(t1,2) - 9*t1*t2 - 5*Power(t2,2)) + 
               2*Power(s2,5)*
                (Power(t1,3) + 21*Power(t1,2)*t2 + 27*t1*Power(t2,2) - 
                  6*Power(t2,3)) + 
               2*Power(s2,4)*
                (Power(t1,4) - 13*Power(t1,3)*t2 - 
                  135*Power(t1,2)*Power(t2,2) + 162*t1*Power(t2,3) - 
                  60*Power(t2,4)) - 
               Power(s2,3)*(18*Power(t1,5) + 23*Power(t1,4)*t2 - 
                  157*Power(t1,3)*Power(t2,2) + 
                  342*Power(t1,2)*Power(t2,3) - 334*t1*Power(t2,4) + 
                  108*Power(t2,5)) + 
               Power(s2,2)*(22*Power(t1,6) + 58*Power(t1,5)*t2 + 
                  6*Power(t1,4)*Power(t2,2) - 
                  346*Power(t1,3)*Power(t2,3) + 
                  334*Power(t1,2)*Power(t2,4) - 78*t1*Power(t2,5) + 
                  6*Power(t2,6)) - 
               s2*(8*Power(t1,7) + 61*Power(t1,6)*t2 - 
                  205*Power(t1,5)*Power(t2,2) + 
                  125*Power(t1,4)*Power(t2,3) + 
                  173*Power(t1,3)*Power(t2,4) - 
                  270*Power(t1,2)*Power(t2,5) + 132*t1*Power(t2,6) - 
                  24*Power(t2,7))) + 
            Power(s1,2)*(-2*Power(s2,6)*t2*
                (Power(t1,2) - 30*t1*t2 + 15*Power(t2,2)) + 
               2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (18*Power(t1,3) + Power(t1,2)*t2 - 9*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               2*Power(s2,5)*
                (2*Power(t1,4) + 44*Power(t1,3)*t2 - 
                  76*Power(t1,2)*Power(t2,2) + 77*t1*Power(t2,3) - 
                  29*Power(t2,4)) - 
               2*Power(s2,4)*t1*
                (7*Power(t1,4) + 73*Power(t1,3)*t2 - 
                  18*Power(t1,2)*Power(t2,2) - 51*t1*Power(t2,3) + 
                  24*Power(t2,4)) + 
               s2*Power(t1 - t2,2)*
                (2*Power(t1,6) - 104*Power(t1,5)*t2 + 
                  72*Power(t1,4)*Power(t2,2) + 
                  3*Power(t1,3)*Power(t2,3) - 
                  43*Power(t1,2)*Power(t2,4) + 42*t1*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(s2,3)*(18*Power(t1,6) + 52*Power(t1,5)*t2 + 
                  8*Power(t1,4)*Power(t2,2) - 
                  285*Power(t1,3)*Power(t2,3) + 
                  503*Power(t1,2)*Power(t2,4) - 264*t1*Power(t2,5) + 
                  48*Power(t2,6)) - 
               2*Power(s2,2)*(5*Power(t1,7) - 38*Power(t1,6)*t2 + 
                  5*Power(t1,5)*Power(t2,2) + 
                  150*Power(t1,4)*Power(t2,3) - 
                  118*Power(t1,3)*Power(t2,4) - 
                  31*Power(t1,2)*Power(t2,5) + 34*t1*Power(t2,6) - 
                  7*Power(t2,7)))) + 
         Power(s,3)*(-2*Power(s2,3)*(s2 - t1)*Power(t1,2)*(s2 - t2)*
             Power(t2,4)*Power(s2 - t1 + t2,3)*
             (Power(s2,2) + s2*(t1 - t2) - t1*(2*t1 + t2)) + 
            Power(s1,8)*s2*(s2 - t1)*t1*
             (2*Power(s2,4)*(t1 - 4*t2) + 
               Power(s2,3)*(-8*Power(t1,2) + 27*t1*t2 - 
                  24*Power(t2,2)) + 
               3*Power(s2,2)*
                (4*Power(t1,3) - 11*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) - 8*Power(t2,3)) + 
               s2*(-8*Power(t1,4) + 17*Power(t1,3)*t2 - 
                  36*Power(t1,2)*Power(t2,2) + 29*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               t1*(2*Power(t1,4) - 3*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s1,7)*(2*Power(s2,8)*t2 + 
               2*Power(t1,4)*Power(t1 - t2,3)*t2*(2*t1 + 3*t2) + 
               2*Power(s2,7)*(Power(t1,2) - 35*t1*t2 - Power(t2,2)) - 
               Power(s2,6)*(8*Power(t1,3) - 293*Power(t1,2)*t2 + 
                  148*t1*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,5)*(10*Power(t1,4) - 503*Power(t1,3)*t2 + 
                  578*Power(t1,2)*Power(t2,2) - 54*t1*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(s2,4)*t2*
                (416*Power(t1,4) - 801*Power(t1,3)*t2 + 
                  275*Power(t1,2)*Power(t2,2) + 56*t1*Power(t2,3) - 
                  8*Power(t2,4)) - 
               Power(s2,3)*t1*
                (10*Power(t1,5) + 156*Power(t1,4)*t2 - 
                  451*Power(t1,3)*Power(t2,2) + 
                  275*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) - 
                  32*Power(t2,5)) - 
               s2*Power(t1,3)*
                (2*Power(t1,5) + 7*Power(t1,4)*t2 + 
                  7*Power(t1,3)*Power(t2,2) - 
                  53*Power(t1,2)*Power(t2,3) + 31*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,2)*Power(t1,2)*
                (8*Power(t1,5) + 21*Power(t1,4)*t2 - 
                  65*Power(t1,3)*Power(t2,2) + 
                  25*Power(t1,2)*Power(t2,3) - 3*t1*Power(t2,4) + 
                  12*Power(t2,5))) + 
            Power(s1,6)*(2*Power(s2,9)*t2 + 
               4*Power(t1,4)*Power(t1 - t2,3)*t2*
                (Power(t1,2) - t1*t2 - 4*Power(t2,2)) - 
               2*Power(s2,8)*(Power(t1,2) + 54*t1*t2 + 9*Power(t2,2)) + 
               2*Power(s2,7)*
                (6*Power(t1,3) + 222*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) - 19*Power(t2,3)) + 
               Power(s2,6)*(-30*Power(t1,4) - 702*Power(t1,3)*t2 + 
                  249*Power(t1,2)*Power(t2,2) + 281*t1*Power(t2,3) + 
                  14*Power(t2,4)) + 
               s2*Power(t1,3)*t2*
                (-2*Power(t1,5) + 19*Power(t1,4)*t2 + 
                  32*Power(t1,3)*Power(t2,2) - 
                  97*Power(t1,2)*Power(t2,3) + 22*t1*Power(t2,4) + 
                  26*Power(t2,5)) + 
               Power(s2,5)*(40*Power(t1,5) + 472*Power(t1,4)*t2 - 
                  68*Power(t1,3)*Power(t2,2) - 
                  801*Power(t1,2)*Power(t2,3) + 225*t1*Power(t2,4) + 
                  60*Power(t2,5)) + 
               Power(s2,2)*Power(t1,2)*
                (-2*Power(t1,6) + 2*Power(t1,5)*t2 - 
                  160*Power(t1,4)*Power(t2,2) + 
                  316*Power(t1,3)*Power(t2,3) - 
                  261*Power(t1,2)*Power(t2,4) + 158*t1*Power(t2,5) - 
                  63*Power(t2,6)) + 
               Power(s2,3)*t1*
                (12*Power(t1,6) - 36*Power(t1,5)*t2 + 
                  535*Power(t1,4)*Power(t2,2) - 
                  1113*Power(t1,3)*Power(t2,3) + 
                  737*Power(t1,2)*Power(t2,4) - 69*t1*Power(t2,5) - 
                  55*Power(t2,6)) - 
               Power(s2,4)*(30*Power(t1,6) + 76*Power(t1,5)*t2 + 
                  491*Power(t1,4)*Power(t2,2) - 
                  1315*Power(t1,3)*Power(t2,3) + 
                  650*Power(t1,2)*Power(t2,4) + 53*t1*Power(t2,5) - 
                  28*Power(t2,6))) + 
            s1*Power(s2,2)*t1*Power(t2,3)*
             (Power(s2,8)*(6*t1 - 2*t2) - 
               2*Power(s2,7)*(8*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               Power(t1,3)*Power(t1 - t2,2)*t2*
                (4*Power(t1,3) - 6*Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(s2,6)*(-6*Power(t1,3) + 32*Power(t1,2)*t2 - 
                  24*t1*Power(t2,2) + 4*Power(t2,3)) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*
                (4*Power(t1,4) - 2*Power(t1,3)*t2 - 
                  14*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               2*Power(s2,5)*
                (30*Power(t1,4) - 47*Power(t1,3)*t2 + 
                  34*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,4)*(-70*Power(t1,5) + 58*Power(t1,4)*t2 + 
                  6*Power(t1,3)*Power(t2,2) - 
                  35*Power(t1,2)*Power(t2,3) + 22*t1*Power(t2,4) - 
                  2*Power(t2,5)) + 
               Power(s2,2)*t1*
                (6*Power(t1,6) - 60*Power(t1,5)*t2 + 
                  92*Power(t1,4)*Power(t2,2) - 
                  37*Power(t1,3)*Power(t2,3) - 
                  7*Power(t1,2)*Power(t2,4) + 9*t1*Power(t2,5) - 
                  4*Power(t2,6)) + 
               Power(s2,3)*(24*Power(t1,6) + 48*Power(t1,5)*t2 - 
                  132*Power(t1,4)*Power(t2,2) + 
                  103*Power(t1,3)*Power(t2,3) - 
                  49*Power(t1,2)*Power(t2,4) + 12*t1*Power(t2,5) - 
                  2*Power(t2,6))) - 
            Power(s1,2)*s2*Power(t2,2)*
             (Power(s2,9)*(-8*Power(t1,2) - 6*t1*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*Power(t1 - t2,2)*Power(t2,3)*
                (2*Power(t1,2) - 7*t1*t2 + 6*Power(t2,2)) + 
               2*Power(s2,8)*
                (23*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,7)*(-119*Power(t1,4) + 66*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) + 24*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,6)*(171*Power(t1,5) - 199*Power(t1,4)*t2 + 
                  27*Power(t1,3)*Power(t2,2) - 
                  39*Power(t1,2)*Power(t2,3) + 16*t1*Power(t2,4) - 
                  4*Power(t2,5)) + 
               s2*Power(t1,3)*t2*
                (7*Power(t1,6) - 28*Power(t1,5)*t2 + 
                  8*Power(t1,4)*Power(t2,2) + 
                  67*Power(t1,3)*Power(t2,3) - 
                  91*Power(t1,2)*Power(t2,4) + 49*t1*Power(t2,5) - 
                  12*Power(t2,6)) + 
               Power(s2,5)*(-134*Power(t1,6) + 182*Power(t1,5)*t2 - 
                  35*Power(t1,4)*Power(t2,2) + 
                  35*Power(t1,3)*Power(t2,3) - 
                  26*Power(t1,2)*Power(t2,4) - 22*t1*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s2,4)*(44*Power(t1,7) - 13*Power(t1,6)*t2 + 
                  2*Power(t1,5)*Power(t2,2) - 
                  90*Power(t1,4)*Power(t2,3) + 
                  105*Power(t1,3)*Power(t2,4) - 
                  26*Power(t1,2)*Power(t2,5) - 12*t1*Power(t2,6) + 
                  2*Power(t2,7)) + 
               Power(s2,2)*Power(t1,2)*
                (-5*Power(t1,7) + 8*Power(t1,6)*t2 + 
                  65*Power(t1,5)*Power(t2,2) - 
                  88*Power(t1,4)*Power(t2,3) - 
                  11*Power(t1,3)*Power(t2,4) + 
                  35*Power(t1,2)*Power(t2,5) - 4*t1*Power(t2,6) + 
                  3*Power(t2,7)) + 
               Power(s2,3)*t1*
                (5*Power(t1,7) - 53*Power(t1,6)*t2 - 
                  30*Power(t1,5)*Power(t2,2) + 
                  146*Power(t1,4)*Power(t2,3) - 
                  117*Power(t1,3)*Power(t2,4) + 
                  55*Power(t1,2)*Power(t2,5) - 17*t1*Power(t2,6) + 
                  4*Power(t2,7))) + 
            Power(s1,5)*(-2*Power(t1,4)*Power(t1 - t2,3)*Power(t2,2)*
                (5*Power(t1,2) + 4*t1*t2 - 7*Power(t2,2)) - 
               2*Power(s2,9)*(Power(t1,2) + 29*t1*t2 + 8*Power(t2,2)) + 
               2*Power(s2,8)*
                (4*Power(t1,3) + 110*Power(t1,2)*t2 + 
                  54*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,6)*t2*
                (-64*Power(t1,4) + 614*Power(t1,3)*t2 - 
                  582*Power(t1,2)*Power(t2,2) - 115*t1*Power(t2,3) + 
                  50*Power(t2,4)) + 
               Power(s2,7)*(-10*Power(t1,4) - 245*Power(t1,3)*t2 - 
                  320*Power(t1,2)*Power(t2,2) + 247*t1*Power(t2,3) + 
                  90*Power(t2,4)) + 
               Power(s2,5)*(10*Power(t1,6) + 344*Power(t1,5)*t2 - 
                  723*Power(t1,4)*Power(t2,2) + 
                  162*Power(t1,3)*Power(t2,3) + 
                  484*Power(t1,2)*Power(t2,4) - 207*t1*Power(t2,5) - 
                  50*Power(t2,6)) + 
               2*s2*Power(t1,3)*t2*
                (4*Power(t1,6) + 4*Power(t1,5)*t2 - 
                  14*Power(t1,4)*Power(t2,2) - 
                  15*Power(t1,3)*Power(t2,3) + 
                  13*Power(t1,2)*Power(t2,4) + 28*t1*Power(t2,5) - 
                  18*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (-36*Power(t1,6) - 3*Power(t1,5)*t2 + 
                  328*Power(t1,4)*Power(t2,2) - 
                  592*Power(t1,3)*Power(t2,3) + 
                  505*Power(t1,2)*Power(t2,4) - 273*t1*Power(t2,5) + 
                  77*Power(t2,6)) + 
               Power(s2,4)*(-8*Power(t1,7) - 288*Power(t1,6)*t2 + 
                  423*Power(t1,5)*Power(t2,2) + 
                  722*Power(t1,4)*Power(t2,3) - 
                  1330*Power(t1,3)*Power(t2,4) + 
                  537*Power(t1,2)*Power(t2,5) + 31*t1*Power(t2,6) - 
                  36*Power(t2,7)) + 
               Power(s2,3)*t1*
                (2*Power(t1,7) + 119*Power(t1,6)*t2 - 
                  81*Power(t1,5)*Power(t2,2) - 
                  881*Power(t1,4)*Power(t2,3) + 
                  1485*Power(t1,3)*Power(t2,4) - 
                  756*Power(t1,2)*Power(t2,5) + 30*t1*Power(t2,6) + 
                  42*Power(t2,7))) - 
            Power(s1,4)*(2*Power(s2,10)*t2*(6*t1 + t2) - 
               4*Power(t1,4)*Power(t1 - t2,3)*Power(t2,3)*
                (2*Power(t1,2) + 3*t1*t2 - Power(t2,2)) + 
               2*Power(s2,9)*
                (Power(t1,3) - 10*Power(t1,2)*t2 - 30*t1*Power(t2,2) - 
                  12*Power(t2,3)) - 
               Power(s2,8)*(10*Power(t1,4) + 97*Power(t1,3)*t2 - 
                  166*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
                  30*Power(t2,4)) + 
               Power(s2,7)*(20*Power(t1,5) + 375*Power(t1,4)*t2 - 
                  220*Power(t1,3)*Power(t2,2) - 
                  157*Power(t1,2)*Power(t2,3) + 160*t1*Power(t2,4) + 
                  54*Power(t2,5)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (14*Power(t1,6) + 6*Power(t1,5)*t2 - 
                  44*Power(t1,4)*Power(t2,2) + 
                  41*Power(t1,3)*Power(t2,3) - 
                  78*Power(t1,2)*Power(t2,4) + 87*t1*Power(t2,5) - 
                  24*Power(t2,6)) + 
               Power(s2,6)*(-20*Power(t1,6) - 549*Power(t1,5)*t2 + 
                  273*Power(t1,4)*Power(t2,2) + 
                  592*Power(t1,3)*Power(t2,3) - 
                  463*Power(t1,2)*Power(t2,4) + 31*t1*Power(t2,5) + 
                  72*Power(t2,6)) + 
               Power(s2,5)*(10*Power(t1,7) + 414*Power(t1,6)*t2 - 
                  279*Power(t1,5)*Power(t2,2) - 
                  567*Power(t1,4)*Power(t2,3) + 
                  238*Power(t1,3)*Power(t2,4) + 
                  130*Power(t1,2)*Power(t2,5) - 43*t1*Power(t2,6) - 
                  6*Power(t2,7)) + 
               Power(s2,3)*t1*t2*
                (23*Power(t1,7) + 69*Power(t1,6)*t2 + 
                  132*Power(t1,5)*Power(t2,2) - 
                  989*Power(t1,4)*Power(t2,3) + 
                  1257*Power(t1,3)*Power(t2,4) - 
                  533*Power(t1,2)*Power(t2,5) + 27*t1*Power(t2,6) + 
                  7*Power(t2,7)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (Power(t1,7) - 64*Power(t1,6)*t2 - 
                  48*Power(t1,5)*Power(t2,2) + 
                  447*Power(t1,4)*Power(t2,3) - 
                  673*Power(t1,3)*Power(t2,4) + 
                  526*Power(t1,2)*Power(t2,5) - 238*t1*Power(t2,6) + 
                  43*Power(t2,7)) + 
               Power(s2,4)*(-2*Power(t1,8) - 159*Power(t1,7)*t2 + 
                  99*Power(t1,6)*Power(t2,2) + 
                  75*Power(t1,5)*Power(t2,3) + 
                  669*Power(t1,4)*Power(t2,4) - 
                  912*Power(t1,3)*Power(t2,5) + 
                  284*Power(t1,2)*Power(t2,6) + 22*t1*Power(t2,7) - 
                  20*Power(t2,8))) + 
            Power(s1,3)*t2*(-2*Power(t1,5)*Power(t1 - t2,3)*Power(t2,3)*
                (t1 + 2*t2) + 
               4*Power(s2,10)*(-3*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               Power(s2,9)*(76*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,8)*(-205*Power(t1,4) - 71*Power(t1,3)*t2 + 
                  51*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) - 
                  28*Power(t2,4)) + 
               Power(s2,7)*t1*
                (300*Power(t1,4) + 103*Power(t1,3)*t2 - 
                  181*Power(t1,2)*Power(t2,2) - 165*t1*Power(t2,3) + 
                  45*Power(t2,4)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (6*Power(t1,6) + 4*Power(t1,5)*t2 - 
                  37*Power(t1,4)*Power(t2,2) + 
                  65*Power(t1,3)*Power(t2,3) - 
                  73*Power(t1,2)*Power(t2,4) + 41*t1*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(s2,6)*(-250*Power(t1,6) - 98*Power(t1,5)*t2 + 
                  317*Power(t1,4)*Power(t2,2) + 
                  435*Power(t1,3)*Power(t2,3) - 
                  348*Power(t1,2)*Power(t2,4) + t1*Power(t2,5) + 
                  28*Power(t2,6)) + 
               Power(s2,3)*t1*t2*
                (29*Power(t1,7) + 170*Power(t1,5)*Power(t2,2) - 
                  516*Power(t1,4)*Power(t2,3) + 
                  480*Power(t1,3)*Power(t2,4) - 
                  177*Power(t1,2)*Power(t2,5) + 30*t1*Power(t2,6) - 
                  4*Power(t2,7)) + 
               Power(s2,5)*(112*Power(t1,7) + 126*Power(t1,6)*t2 - 
                  357*Power(t1,5)*Power(t2,2) - 
                  335*Power(t1,4)*Power(t2,3) + 
                  547*Power(t1,3)*Power(t2,4) - 
                  153*Power(t1,2)*Power(t2,5) + 7*t1*Power(t2,6) + 
                  8*Power(t2,7)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (2*Power(t1,7) - 38*Power(t1,6)*t2 - 
                  61*Power(t1,5)*Power(t2,2) + 
                  322*Power(t1,4)*Power(t2,3) - 
                  409*Power(t1,3)*Power(t2,4) + 
                  267*Power(t1,2)*Power(t2,5) - 98*t1*Power(t2,6) + 
                  11*Power(t2,7)) + 
               Power(s2,4)*(-21*Power(t1,8) - 105*Power(t1,7)*t2 + 
                  202*Power(t1,6)*Power(t2,2) - 
                  57*Power(t1,5)*Power(t2,3) + 
                  24*Power(t1,4)*Power(t2,4) - 
                  89*Power(t1,3)*Power(t2,5) + 
                  38*Power(t1,2)*Power(t2,6) + 16*t1*Power(t2,7) - 
                  4*Power(t2,8)))) + 
         Power(s,7)*(2*s2*Power(t1,2)*t2*Power(s2 - t1 + t2,3)*
             (Power(s2,3)*(3*Power(t1,2) - 16*t1*t2 + 20*Power(t2,2)) + 
               Power(s2,2)*(-4*Power(t1,3) + Power(t1,2)*t2 + 
                  29*t1*Power(t2,2) - 35*Power(t2,3)) + 
               Power(t2,2)*(-3*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  t1*Power(t2,2) - Power(t2,3)) + 
               s2*(Power(t1,4) + 8*Power(t1,3)*t2 - 
                  9*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  14*Power(t2,4))) - 
            s1*t1*t2*(2*Power(s2,7)*
                (Power(t1,2) + 40*t1*t2 - 20*Power(t2,2)) + 
               2*Power(t1,2)*Power(t1 - t2,4)*t2*
                (5*Power(t1,2) + 8*t1*t2 + Power(t2,2)) + 
               2*Power(s2,6)*
                (4*Power(t1,3) - 107*Power(t1,2)*t2 + 
                  55*t1*Power(t2,2) - 25*Power(t2,3)) + 
               Power(s2,5)*(-34*Power(t1,4) + 100*Power(t1,3)*t2 + 
                  354*Power(t1,2)*Power(t2,2) - 316*t1*Power(t2,3) + 
                  62*Power(t2,4)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (36*Power(t1,5) + 14*Power(t1,4)*t2 - 
                  58*Power(t1,3)*Power(t2,2) - 
                  115*Power(t1,2)*Power(t2,3) + 126*t1*Power(t2,4) - 
                  22*Power(t2,5)) - 
               s2*Power(t1 - t2,3)*
                (8*Power(t1,5) + 44*Power(t1,4)*t2 - 
                  21*Power(t1,3)*Power(t2,2) - 
                  43*Power(t1,2)*Power(t2,3) - 22*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,4)*(56*Power(t1,5) + 52*Power(t1,4)*t2 - 
                  778*Power(t1,3)*Power(t2,2) + 
                  887*Power(t1,2)*Power(t2,3) - 416*t1*Power(t2,4) + 
                  88*Power(t2,5)) + 
               Power(s2,3)*(-60*Power(t1,6) + 50*Power(t1,5)*t2 + 
                  253*Power(t1,4)*Power(t2,2) - 
                  164*Power(t1,3)*Power(t2,3) - 
                  199*Power(t1,2)*Power(t2,4) + 128*t1*Power(t2,5) - 
                  8*Power(t2,6))) + 
            Power(s1,5)*(6*Power(s2,6)*t2 + 
               Power(s2,5)*(8*Power(t1,2) - 36*t1*t2 - 6*Power(t2,2)) + 
               4*t1*Power(t1 - t2,3)*t2*
                (10*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) - 
               Power(s2,4)*(44*Power(t1,3) - 79*Power(t1,2)*t2 + 
                  100*t1*Power(t2,2) + 40*Power(t2,3)) + 
               Power(s2,3)*(84*Power(t1,4) - 136*Power(t1,3)*t2 + 
                  421*Power(t1,2)*Power(t2,2) - 204*t1*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,2)*(-68*Power(t1,5) + 145*Power(t1,4)*t2 - 
                  452*Power(t1,3)*Power(t2,2) + 
                  585*Power(t1,2)*Power(t2,3) - 240*t1*Power(t2,4) + 
                  18*Power(t2,5)) + 
               s2*(20*Power(t1,6) - 96*Power(t1,5)*t2 + 
                  181*Power(t1,4)*Power(t2,2) - 
                  184*Power(t1,3)*Power(t2,3) + 
                  151*Power(t1,2)*Power(t2,4) - 88*t1*Power(t2,5) + 
                  14*Power(t2,6))) + 
            Power(s1,4)*(6*Power(s2,7)*t2 + 
               Power(s2,6)*(4*Power(t1,2) - 46*t1*t2 - 
                  54*Power(t2,2)) - 
               2*t1*Power(t1 - t2,3)*t2*
                (20*Power(t1,3) + 22*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) - 10*Power(t2,3)) - 
               2*Power(s2,5)*
                (12*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  56*t1*Power(t2,2) + 37*Power(t2,3)) + 
               Power(s2,4)*(38*Power(t1,4) + 83*Power(t1,3)*t2 + 
                  555*Power(t1,2)*Power(t2,2) - 490*t1*Power(t2,3) + 
                  132*Power(t2,4)) - 
               Power(s2,3)*(10*Power(t1,5) + 45*Power(t1,4)*t2 + 
                  249*Power(t1,3)*Power(t2,2) - 
                  523*Power(t1,2)*Power(t2,3) + 582*t1*Power(t2,4) - 
                  210*Power(t2,5)) + 
               Power(s2,2)*(-18*Power(t1,6) - 129*Power(t1,5)*t2 - 
                  17*Power(t1,4)*Power(t2,2) + 
                  783*Power(t1,3)*Power(t2,3) - 
                  729*Power(t1,2)*Power(t2,4) + 68*t1*Power(t2,5) + 
                  34*Power(t2,6)) + 
               s2*(10*Power(t1,7) + 167*Power(t1,6)*t2 - 
                  365*Power(t1,5)*Power(t2,2) + 
                  18*Power(t1,4)*Power(t2,3) + 
                  481*Power(t1,3)*Power(t2,4) - 
                  489*Power(t1,2)*Power(t2,5) + 206*t1*Power(t2,6) - 
                  30*Power(t2,7))) + 
            Power(s1,3)*(-2*Power(s2,7)*t2*(t1 + 16*t2) + 
               2*Power(s2,6)*
                (2*Power(t1,3) - 37*Power(t1,2)*t2 - 
                  65*t1*Power(t2,2) + 35*Power(t2,3)) - 
               2*t1*Power(t1 - t2,3)*t2*
                (30*Power(t1,4) - 9*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,5)*(-30*Power(t1,4) + 76*Power(t1,3)*t2 + 
                  398*Power(t1,2)*Power(t2,2) - 636*t1*Power(t2,3) + 
                  278*Power(t2,4)) + 
               Power(s2,4)*(76*Power(t1,5) + 110*Power(t1,4)*t2 - 
                  25*Power(t1,3)*Power(t2,2) + 
                  31*Power(t1,2)*Power(t2,3) - 292*t1*Power(t2,4) + 
                  112*Power(t2,5)) - 
               Power(s2,3)*(88*Power(t1,6) + 199*Power(t1,5)*t2 + 
                  16*Power(t1,4)*Power(t2,2) - 
                  831*Power(t1,3)*Power(t2,3) + 
                  1191*Power(t1,2)*Power(t2,4) - 658*t1*Power(t2,5) + 
                  152*Power(t2,6)) + 
               Power(s2,2)*(48*Power(t1,7) + 26*Power(t1,6)*t2 - 
                  235*Power(t1,5)*Power(t2,2) + 
                  577*Power(t1,4)*Power(t2,3) - 
                  489*Power(t1,3)*Power(t2,4) - 
                  147*Power(t1,2)*Power(t2,5) + 294*t1*Power(t2,6) - 
                  70*Power(t2,7)) + 
               s2*(-10*Power(t1,8) + 125*Power(t1,7)*t2 - 
                  288*Power(t1,6)*Power(t2,2) + 
                  230*Power(t1,5)*Power(t2,3) + 
                  57*Power(t1,4)*Power(t2,4) - 
                  355*Power(t1,3)*Power(t2,5) + 
                  363*Power(t1,2)*Power(t2,6) - 140*t1*Power(t2,7) + 
                  18*Power(t2,8))) + 
            Power(s1,2)*(8*Power(s2,7)*t2*
                (Power(t1,2) - 10*t1*t2 + 5*Power(t2,2)) + 
               2*Power(s2,6)*t2*
                (-78*Power(t1,3) + 46*Power(t1,2)*t2 - 
                  55*t1*Power(t2,2) + 25*Power(t2,3)) + 
               2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (10*Power(t1,4) + 30*Power(t1,3)*t2 - 
                  11*Power(t1,2)*Power(t2,2) - 10*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               2*Power(s2,5)*
                (Power(t1,5) + 197*Power(t1,4)*t2 + 
                  69*Power(t1,3)*Power(t2,2) - 
                  254*Power(t1,2)*Power(t2,3) + 158*t1*Power(t2,4) - 
                  31*Power(t2,5)) + 
               Power(s2,4)*(-6*Power(t1,6) - 358*Power(t1,5)*t2 + 
                  84*Power(t1,4)*Power(t2,2) + 
                  325*Power(t1,3)*Power(t2,3) - 
                  621*Power(t1,2)*Power(t2,4) + 416*t1*Power(t2,5) - 
                  88*Power(t2,6)) - 
               s2*Power(t1 - t2,2)*t2*
                (130*Power(t1,6) + 32*Power(t1,5)*t2 - 
                  71*Power(t1,4)*Power(t2,2) - 
                  51*Power(t1,3)*Power(t2,3) + 
                  59*Power(t1,2)*Power(t2,4) - 24*t1*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s2,3)*(6*Power(t1,7) + 16*Power(t1,6)*t2 - 
                  256*Power(t1,5)*Power(t2,2) + 
                  893*Power(t1,4)*Power(t2,3) - 
                  961*Power(t1,3)*Power(t2,4) + 
                  559*Power(t1,2)*Power(t2,5) - 128*t1*Power(t2,6) + 
                  8*Power(t2,7)) + 
               Power(s2,2)*(-2*Power(t1,8) + 206*Power(t1,7)*t2 - 
                  238*Power(t1,6)*Power(t2,2) - 
                  123*Power(t1,5)*Power(t2,3) + 
                  63*Power(t1,4)*Power(t2,4) - 
                  131*Power(t1,3)*Power(t2,5) + 
                  373*Power(t1,2)*Power(t2,6) - 170*t1*Power(t2,7) + 
                  22*Power(t2,8)))) + 
         Power(s,6)*(-2*s2*Power(t1,2)*t2*Power(s2 - t1 + t2,3)*
             (3*Power(t1,2)*(t1 - t2)*Power(t2,3) + 
               Power(s2,4)*(Power(t1,2) - 9*t1*t2 + 15*Power(t2,2)) + 
               Power(s2,3)*(-2*Power(t1,3) + 2*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 40*Power(t2,3)) + 
               s2*t2*(-4*Power(t1,4) - 9*Power(t1,3)*t2 + 
                  16*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,2)*(Power(t1,4) + 11*Power(t1,3)*t2 - 
                  20*Power(t1,2)*Power(t2,2) - 18*t1*Power(t2,3) + 
                  26*Power(t2,4))) + 
            Power(s1,6)*(-6*Power(s2,6)*t2 - 
               4*t1*Power(t1 - t2,3)*t2*
                (5*Power(t1,2) + 3*t1*t2 - Power(t2,2)) - 
               2*Power(s2,5)*(6*Power(t1,2) - 18*t1*t2 + Power(t2,2)) + 
               Power(s2,4)*(56*Power(t1,3) - 91*Power(t1,2)*t2 + 
                  100*t1*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,3)*(-96*Power(t1,4) + 134*Power(t1,3)*t2 - 
                  349*Power(t1,2)*Power(t2,2) + 158*t1*Power(t2,3) + 
                  30*Power(t2,4)) + 
               Power(s2,2)*(72*Power(t1,5) - 117*Power(t1,4)*t2 + 
                  336*Power(t1,3)*Power(t2,2) - 
                  427*Power(t1,2)*Power(t2,3) + 156*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               s2*(-20*Power(t1,6) + 64*Power(t1,5)*t2 - 
                  115*Power(t1,4)*Power(t2,2) + 
                  126*Power(t1,3)*Power(t2,3) - 
                  103*Power(t1,2)*Power(t2,4) + 58*t1*Power(t2,5) - 
                  4*Power(t2,6))) + 
            Power(s1,5)*(-16*Power(s2,7)*t2 + 
               Power(s2,6)*(-14*Power(t1,2) + 128*t1*t2 + 
                  54*Power(t2,2)) + 
               2*t1*Power(t1 - t2,3)*t2*
                (30*Power(t1,3) + 28*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) - 5*Power(t2,3)) + 
               2*Power(s2,5)*
                (35*Power(t1,3) - 114*Power(t1,2)*t2 + 
                  121*t1*Power(t2,2) + 80*Power(t2,3)) + 
               Power(s2,4)*(-126*Power(t1,4) + 119*Power(t1,3)*t2 - 
                  951*Power(t1,2)*Power(t2,2) + 446*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,3)*(98*Power(t1,5) - 24*Power(t1,4)*t2 + 
                  673*Power(t1,3)*Power(t2,2) - 
                  784*Power(t1,2)*Power(t2,3) + 492*t1*Power(t2,4) - 
                  156*Power(t2,5)) + 
               s2*t2*(-188*Power(t1,6) + 281*Power(t1,5)*t2 + 
                  190*Power(t1,4)*Power(t2,2) - 
                  549*Power(t1,3)*Power(t2,3) + 
                  418*Power(t1,2)*Power(t2,4) - 166*t1*Power(t2,5) + 
                  12*Power(t2,6)) - 
               Power(s2,2)*(28*Power(t1,6) - 149*Power(t1,5)*t2 + 
                  125*Power(t1,4)*Power(t2,2) + 
                  495*Power(t1,3)*Power(t2,3) - 
                  575*Power(t1,2)*Power(t2,4) + 16*t1*Power(t2,5) + 
                  62*Power(t2,6))) + 
            s1*t1*t2*(30*Power(s2,8)*(2*t1 - t2)*t2 - 
               4*Power(t1,3)*Power(t1 - t2,4)*Power(t2,2)*(2*t1 + t2) + 
               2*Power(s2,7)*
                (2*Power(t1,3) - 70*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) - 5*Power(t2,3)) - 
               2*Power(s2,6)*
                (6*Power(t1,4) + 2*Power(t1,3)*t2 - 
                  265*Power(t1,2)*Power(t2,2) + 212*t1*Power(t2,3) - 
                  49*Power(t2,4)) + 
               s2*t1*Power(t1 - t2,3)*t2*
                (14*Power(t1,4) - 10*Power(t1,3)*t2 - 
                  31*Power(t1,2)*Power(t2,2) - 27*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,5)*(16*Power(t1,5) + 174*Power(t1,4)*t2 - 
                  833*Power(t1,3)*Power(t2,2) + 
                  633*Power(t1,2)*Power(t2,3) - 210*t1*Power(t2,4) + 
                  62*Power(t2,5)) + 
               Power(s2,4)*(-16*Power(t1,6) - 44*Power(t1,5)*t2 + 
                  189*Power(t1,4)*Power(t2,2) + 
                  417*Power(t1,3)*Power(t2,3) - 
                  798*Power(t1,2)*Power(t2,4) + 408*t1*Power(t2,5) - 
                  52*Power(t2,6)) - 
               Power(s2,2)*Power(t1 - t2,2)*
                (4*Power(t1,6) + 4*Power(t1,5)*t2 - 
                  75*Power(t1,4)*Power(t2,2) - 
                  63*Power(t1,3)*Power(t2,3) - 
                  18*Power(t1,2)*Power(t2,4) + 80*t1*Power(t2,5) - 
                  8*Power(t2,6)) + 
               Power(s2,3)*(12*Power(t1,7) - 64*Power(t1,6)*t2 + 
                  135*Power(t1,5)*Power(t2,2) - 
                  542*Power(t1,4)*Power(t2,3) + 
                  803*Power(t1,3)*Power(t2,4) - 
                  480*Power(t1,2)*Power(t2,5) + 164*t1*Power(t2,6) - 
                  28*Power(t2,7))) + 
            Power(s1,3)*(2*Power(s2,8)*t2*(3*t1 + 14*t2) - 
               2*Power(s2,7)*
                (2*Power(t1,3) - 26*Power(t1,2)*t2 - 
                  78*t1*Power(t2,2) + 61*Power(t2,3)) + 
               2*Power(s2,6)*
                (9*Power(t1,4) + 24*Power(t1,3)*t2 - 
                  164*Power(t1,2)*Power(t2,2) + 293*t1*Power(t2,3) - 
                  143*Power(t2,4)) - 
               2*t1*Power(t1 - t2,3)*t2*
                (20*Power(t1,5) + 40*Power(t1,4)*t2 - 
                  5*Power(t1,3)*Power(t2,2) - 
                  11*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               Power(s2,5)*(-38*Power(t1,5) - 624*Power(t1,4)*t2 + 
                  76*Power(t1,3)*Power(t2,2) + 
                  423*Power(t1,2)*Power(t2,3) - 261*t1*Power(t2,4) + 
                  80*Power(t2,5)) + 
               Power(s2,4)*(46*Power(t1,6) + 990*Power(t1,5)*t2 - 
                  693*Power(t1,4)*Power(t2,2) - 
                  381*Power(t1,3)*Power(t2,3) + 
                  1283*Power(t1,2)*Power(t2,4) - 1207*t1*Power(t2,5) + 
                  320*Power(t2,6)) - 
               Power(s2,3)*(30*Power(t1,7) + 470*Power(t1,6)*t2 - 
                  1219*Power(t1,5)*Power(t2,2) + 
                  2199*Power(t1,4)*Power(t2,3) - 
                  1935*Power(t1,3)*Power(t2,4) + 
                  685*Power(t1,2)*Power(t2,5) + 87*t1*Power(t2,6) - 
                  38*Power(t2,7)) + 
               Power(s2,2)*(8*Power(t1,8) - 162*Power(t1,7)*t2 - 
                  153*Power(t1,6)*Power(t2,2) + 
                  1225*Power(t1,5)*Power(t2,3) - 
                  1246*Power(t1,4)*Power(t2,4) + 
                  680*Power(t1,3)*Power(t2,5) - 
                  663*Power(t1,2)*Power(t2,6) + 369*t1*Power(t2,7) - 
                  62*Power(t2,8)) + 
               s2*t2*(200*Power(t1,8) - 331*Power(t1,7)*t2 + 
                  12*Power(t1,6)*Power(t2,2) + 
                  131*Power(t1,5)*Power(t2,3) + 
                  48*Power(t1,4)*Power(t2,4) - 
                  182*Power(t1,3)*Power(t2,5) + 
                  170*Power(t1,2)*Power(t2,6) - 52*t1*Power(t2,7) + 
                  4*Power(t2,8))) - 
            Power(s1,4)*(6*Power(s2,8)*t2 - 
               2*Power(s2,7)*t2*(35*t1 + 51*t2) + 
               2*Power(s2,6)*
                (5*Power(t1,3) - 18*Power(t1,2)*t2 - 
                  54*t1*Power(t2,2) - 21*Power(t2,3)) - 
               2*t1*Power(t1 - t2,3)*t2*
                (20*Power(t1,4) - 26*Power(t1,3)*t2 - 
                  17*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,5)*(-62*Power(t1,4) + 435*Power(t1,3)*t2 + 
                  397*Power(t1,2)*Power(t2,2) - 736*t1*Power(t2,3) + 
                  398*Power(t2,4)) + 
               Power(s2,4)*(146*Power(t1,5) - 503*Power(t1,4)*t2 + 
                  606*Power(t1,3)*Power(t2,2) - 
                  209*Power(t1,2)*Power(t2,3) - 682*t1*Power(t2,4) + 
                  348*Power(t2,5)) + 
               Power(s2,3)*(-166*Power(t1,6) + 75*Power(t1,5)*t2 - 
                  988*Power(t1,4)*Power(t2,2) + 
                  2287*Power(t1,3)*Power(t2,3) - 
                  1725*Power(t1,2)*Power(t2,4) + 590*t1*Power(t2,5) - 
                  108*Power(t2,6)) + 
               Power(s2,2)*(92*Power(t1,7) + 113*Power(t1,6)*t2 + 
                  86*Power(t1,5)*Power(t2,2) - 
                  638*Power(t1,4)*Power(t2,3) + 
                  350*Power(t1,3)*Power(t2,4) - 
                  271*Power(t1,2)*Power(t2,5) + 410*t1*Power(t2,6) - 
                  112*Power(t2,7)) + 
               s2*(-20*Power(t1,8) + 20*Power(t1,7)*t2 - 
                  109*Power(t1,6)*Power(t2,2) + 
                  184*Power(t1,5)*Power(t2,3) + 
                  101*Power(t1,4)*Power(t2,4) - 
                  466*Power(t1,3)*Power(t2,5) + 
                  442*Power(t1,2)*Power(t2,6) - 156*t1*Power(t2,7) + 
                  12*Power(t2,8))) - 
            Power(s1,2)*(2*Power(s2,8)*t2*
                (Power(t1,2) - 30*t1*t2 + 15*Power(t2,2)) - 
               2*Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (20*Power(t1,4) + 13*Power(t1,3)*t2 - 
                  9*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               2*Power(s2,7)*
                (2*Power(t1,4) - 52*Power(t1,3)*t2 - 
                  14*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,6)*(-14*Power(t1,5) + 342*Power(t1,4)*t2 + 
                  412*Power(t1,3)*Power(t2,2) - 
                  550*Power(t1,2)*Power(t2,3) + 424*t1*Power(t2,4) - 
                  98*Power(t2,5)) + 
               Power(s2,5)*(18*Power(t1,6) - 407*Power(t1,5)*t2 - 
                  321*Power(t1,4)*Power(t2,2) + 
                  64*Power(t1,3)*Power(t2,3) + 
                  14*Power(t1,2)*Power(t2,4) + 210*t1*Power(t2,5) - 
                  62*Power(t2,6)) + 
               s2*t1*Power(t1 - t2,2)*t2*
                (32*Power(t1,6) + 144*Power(t1,5)*t2 - 
                  40*Power(t1,4)*Power(t2,2) - 
                  81*Power(t1,3)*Power(t2,3) + 
                  8*Power(t1,2)*Power(t2,4) + 29*t1*Power(t2,5) - 
                  4*Power(t2,6)) + 
               Power(s2,4)*(-10*Power(t1,7) + 125*Power(t1,6)*t2 - 
                  14*Power(t1,5)*Power(t2,2) + 
                  947*Power(t1,4)*Power(t2,3) - 
                  1562*Power(t1,3)*Power(t2,4) + 
                  1207*Power(t1,2)*Power(t2,5) - 408*t1*Power(t2,6) + 
                  52*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-133*Power(t1,8) + 130*Power(t1,7)*t2 - 
                  216*Power(t1,6)*Power(t2,2) + 
                  798*Power(t1,5)*Power(t2,3) - 
                  806*Power(t1,4)*Power(t2,4) + 
                  478*Power(t1,3)*Power(t2,5) - 
                  339*Power(t1,2)*Power(t2,6) + 96*t1*Power(t2,7) - 
                  8*Power(t2,8)) + 
               Power(s2,3)*(2*Power(t1,8) + 143*Power(t1,7)*t2 - 
                  159*Power(t1,6)*Power(t2,2) - 
                  21*Power(t1,5)*Power(t2,3) - 
                  292*Power(t1,4)*Power(t2,4) + 
                  170*Power(t1,3)*Power(t2,5) + 
                  185*Power(t1,2)*Power(t2,6) - 164*t1*Power(t2,7) + 
                  28*Power(t2,8)))) + 
         Power(s,5)*(-2*s2*Power(t1,2)*Power(t2,2)*
             Power(s2 - t1 + t2,3)*
             (2*Power(s2,5)*(t1 - 3*t2) + 
               Power(t1,2)*(t1 - t2)*Power(t2,3) - 
               Power(s2,4)*(Power(t1,2) + 12*t1*t2 - 25*Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (-5*Power(t1,2) - 2*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,3)*(-4*Power(t1,3) + 17*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) - 24*Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,4) + 6*Power(t1,3)*t2 - 
                  26*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,7)*(2*Power(s2,6)*t2 + 
               4*Power(t1,2)*Power(t1 - t2,3)*t2*(t1 + t2) + 
               2*Power(s2,5)*(4*Power(t1,2) - 6*t1*t2 + Power(t2,2)) - 
               Power(s2,4)*(34*Power(t1,3) - 37*Power(t1,2)*t2 + 
                  34*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,3)*(54*Power(t1,4) - 60*Power(t1,3)*t2 + 
                  117*Power(t1,2)*Power(t2,2) - 48*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               s2*t1*(10*Power(t1,5) - 22*Power(t1,4)*t2 + 
                  41*Power(t1,3)*Power(t2,2) - 
                  46*Power(t1,2)*Power(t2,3) + 27*t1*Power(t2,4) - 
                  16*Power(t2,5)) - 
               Power(s2,2)*(38*Power(t1,5) - 51*Power(t1,4)*t2 + 
                  118*Power(t1,3)*Power(t2,2) - 
                  131*Power(t1,2)*Power(t2,3) + 42*t1*Power(t2,4) + 
                  4*Power(t2,5))) + 
            Power(s1,6)*(14*Power(s2,7)*t2 - 
               2*Power(t1,3)*Power(t1 - t2,3)*t2*(18*t1 + 19*t2) + 
               18*Power(s2,6)*(Power(t1,2) - 7*t1*t2 - Power(t2,2)) - 
               Power(s2,5)*(86*Power(t1,3) - 319*Power(t1,2)*t2 + 
                  268*t1*Power(t2,2) + 110*Power(t2,3)) + 
               Power(s2,4)*(160*Power(t1,4) - 345*Power(t1,3)*t2 + 
                  951*Power(t1,2)*Power(t2,2) - 300*t1*Power(t2,3) - 
                  82*Power(t2,4)) + 
               Power(s2,3)*(-144*Power(t1,5) + 201*Power(t1,4)*t2 - 
                  931*Power(t1,3)*Power(t2,2) + 
                  852*Power(t1,2)*Power(t2,3) - 246*t1*Power(t2,4) + 
                  24*Power(t2,5)) + 
               Power(s2,2)*(62*Power(t1,6) - 133*Power(t1,5)*t2 + 
                  317*Power(t1,4)*Power(t2,2) - 
                  219*Power(t1,3)*Power(t2,3) + 
                  3*Power(t1,2)*Power(t2,4) - 34*t1*Power(t2,5) + 
                  28*Power(t2,6)) + 
               s2*t1*(-10*Power(t1,6) + 106*Power(t1,5)*t2 - 
                  121*Power(t1,4)*Power(t2,2) - 
                  121*Power(t1,3)*Power(t2,3) + 
                  203*Power(t1,2)*Power(t2,4) - 99*t1*Power(t2,5) + 
                  54*Power(t2,6))) + 
            s1*t1*Power(t2,2)*
             (-2*Power(t1,4)*Power(t1 - t2,4)*Power(t2,2) + 
               12*Power(s2,9)*(-2*t1 + t2) + 
               2*Power(s2,8)*
                (27*Power(t1,2) + 31*t1*t2 - 7*Power(t2,2)) + 
               Power(s2,7)*(20*Power(t1,3) - 358*Power(t1,2)*t2 + 
                  252*t1*Power(t2,2) - 66*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (6*Power(t1,3) - 30*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) - 7*Power(t2,3)) - 
               Power(s2,6)*(106*Power(t1,4) - 360*Power(t1,3)*t2 + 
                  121*Power(t1,2)*Power(t2,2) + 66*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (10*Power(t1,5) + 24*Power(t1,4)*t2 + 
                  32*Power(t1,3)*Power(t2,2) + 
                  45*Power(t1,2)*Power(t2,3) - 39*t1*Power(t2,4) - 
                  12*Power(t2,5)) + 
               Power(s2,5)*(8*Power(t1,5) + 234*Power(t1,4)*t2 - 
                  831*Power(t1,3)*Power(t2,2) + 
                  839*Power(t1,2)*Power(t2,3) - 376*t1*Power(t2,4) + 
                  58*Power(t2,5)) + 
               Power(s2,4)*(106*Power(t1,6) - 482*Power(t1,5)*t2 + 
                  943*Power(t1,4)*Power(t2,2) - 
                  713*Power(t1,3)*Power(t2,3) + 
                  196*Power(t1,2)*Power(t2,4) - 12*t1*Power(t2,5) + 
                  12*Power(t2,6)) - 
               Power(s2,3)*(68*Power(t1,7) - 162*Power(t1,6)*t2 + 
                  173*Power(t1,5)*Power(t2,2) + 
                  78*Power(t1,4)*Power(t2,3) - 
                  351*Power(t1,3)*Power(t2,4) + 
                  302*Power(t1,2)*Power(t2,5) - 120*t1*Power(t2,6) + 
                  12*Power(t2,7))) + 
            Power(s1,5)*(14*Power(s2,8)*t2 + 
               Power(s2,6)*t2*
                (287*Power(t1,2) - 124*t1*t2 - 186*Power(t2,2)) + 
               2*Power(s2,7)*
                (Power(t1,2) - 84*t1*t2 - 53*Power(t2,2)) + 
               2*Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (29*Power(t1,2) + 24*t1*t2 - 9*Power(t2,2)) + 
               Power(s2,5)*(-40*Power(t1,4) + 121*Power(t1,3)*t2 + 
                  554*Power(t1,2)*Power(t2,2) - 224*t1*Power(t2,3) + 
                  162*Power(t2,4)) + 
               Power(s2,4)*(120*Power(t1,5) - 584*Power(t1,4)*t2 + 
                  396*Power(t1,3)*Power(t2,2) - 
                  403*Power(t1,2)*Power(t2,3) - 370*t1*Power(t2,4) + 
                  332*Power(t2,5)) + 
               Power(s2,3)*(-150*Power(t1,6) + 353*Power(t1,5)*t2 - 
                  1281*Power(t1,4)*Power(t2,2) + 
                  2512*Power(t1,3)*Power(t2,3) - 
                  1578*Power(t1,2)*Power(t2,4) + 212*t1*Power(t2,5) + 
                  48*Power(t2,6)) + 
               Power(s2,2)*(88*Power(t1,7) + 37*Power(t1,6)*t2 + 
                  530*Power(t1,5)*Power(t2,2) - 
                  1755*Power(t1,4)*Power(t2,3) + 
                  1486*Power(t1,3)*Power(t2,4) - 
                  542*Power(t1,2)*Power(t2,5) + 250*t1*Power(t2,6) - 
                  56*Power(t2,7)) - 
               s2*t1*(20*Power(t1,7) + 60*Power(t1,6)*t2 + 
                  27*Power(t1,5)*Power(t2,2) - 
                  323*Power(t1,4)*Power(t2,3) + 
                  241*Power(t1,3)*Power(t2,4) - 
                  13*Power(t1,2)*Power(t2,5) - 82*t1*Power(t2,6) + 
                  64*Power(t2,7))) - 
            Power(s1,2)*(4*Power(s2,9)*t2*
                (Power(t1,2) + 6*t1*t2 - 3*Power(t2,2)) - 
               2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,3)*
                (12*Power(t1,3) + 2*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) - 3*Power(t2,3)) - 
               2*Power(s2,8)*
                (Power(t1,4) - 3*Power(t1,3)*t2 - 
                  20*Power(t1,2)*Power(t2,2) + 31*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (42*Power(t1,5) + 4*Power(t1,4)*t2 + 
                  49*Power(t1,3)*Power(t2,2) - 
                  101*Power(t1,2)*Power(t2,3) + 55*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               2*Power(s2,7)*
                (4*Power(t1,5) - 21*Power(t1,4)*t2 - 
                  161*Power(t1,3)*Power(t2,2) + 
                  92*Power(t1,2)*Power(t2,3) - 126*t1*Power(t2,4) + 
                  33*Power(t2,5)) + 
               Power(s2,6)*(-12*Power(t1,6) + 22*Power(t1,5)*t2 + 
                  469*Power(t1,4)*Power(t2,2) + 
                  503*Power(t1,3)*Power(t2,3) - 
                  496*Power(t1,2)*Power(t2,4) + 66*t1*Power(t2,5) + 
                  6*Power(t2,6)) + 
               Power(s2,5)*(8*Power(t1,7) + 89*Power(t1,6)*t2 - 
                  364*Power(t1,5)*Power(t2,2) - 
                  1094*Power(t1,4)*Power(t2,3) + 
                  1599*Power(t1,3)*Power(t2,4) - 
                  896*Power(t1,2)*Power(t2,5) + 376*t1*Power(t2,6) - 
                  58*Power(t2,7)) + 
               Power(s2,4)*(-2*Power(t1,8) - 153*Power(t1,7)*t2 + 
                  331*Power(t1,6)*Power(t2,2) + 
                  60*Power(t1,5)*Power(t2,3) + 
                  30*Power(t1,4)*Power(t2,4) - 
                  346*Power(t1,3)*Power(t2,5) + 
                  279*Power(t1,2)*Power(t2,6) + 12*t1*Power(t2,7) - 
                  12*Power(t2,8)) - 
               Power(s2,2)*t1*t2*
                (23*Power(t1,8) + 18*Power(t1,7)*t2 + 
                  4*Power(t1,6)*Power(t2,2) + 
                  23*Power(t1,5)*Power(t2,3) - 
                  348*Power(t1,4)*Power(t2,4) + 
                  362*Power(t1,3)*Power(t2,5) - 
                  155*Power(t1,2)*Power(t2,6) + 85*t1*Power(t2,7) - 
                  12*Power(t2,8)) + 
               Power(s2,3)*t2*
                (97*Power(t1,8) - 202*Power(t1,7)*t2 + 
                  529*Power(t1,6)*Power(t2,2) - 
                  972*Power(t1,5)*Power(t2,3) + 
                  755*Power(t1,4)*Power(t2,4) - 
                  502*Power(t1,3)*Power(t2,5) + 
                  371*Power(t1,2)*Power(t2,6) - 120*t1*Power(t2,7) + 
                  12*Power(t2,8))) - 
            Power(s1,3)*(6*Power(s2,9)*t2*(t1 + 2*t2) + 
               2*Power(s2,8)*
                (Power(t1,3) + 11*Power(t1,2)*t2 + 40*t1*Power(t2,2) - 
                  48*Power(t2,3)) + 
               2*Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (30*Power(t1,4) + 21*Power(t1,3)*t2 - 
                  4*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
                  3*Power(t2,4)) - 
               2*Power(s2,7)*
                (8*Power(t1,4) - 29*Power(t1,3)*t2 + 
                  37*Power(t1,2)*Power(t2,2) - 98*t1*Power(t2,3) + 
                  60*Power(t2,4)) + 
               Power(s2,6)*(42*Power(t1,5) - 551*Power(t1,4)*t2 - 
                  248*Power(t1,3)*Power(t2,2) + 
                  584*Power(t1,2)*Power(t2,3) - 625*t1*Power(t2,4) + 
                  222*Power(t2,5)) + 
               Power(s2,5)*(-50*Power(t1,6) + 1001*Power(t1,5)*t2 - 
                  34*Power(t1,4)*Power(t2,2) - 
                  354*Power(t1,3)*Power(t2,3) + 
                  759*Power(t1,2)*Power(t2,4) - 931*t1*Power(t2,5) + 
                  258*Power(t2,6)) + 
               Power(s2,4)*(28*Power(t1,7) - 677*Power(t1,6)*t2 + 
                  635*Power(t1,5)*Power(t2,2) - 
                  1272*Power(t1,4)*Power(t2,3) + 
                  1407*Power(t1,3)*Power(t2,4) - 
                  806*Power(t1,2)*Power(t2,5) + 317*t1*Power(t2,6) - 
                  66*Power(t2,7)) + 
               Power(s2,3)*(-6*Power(t1,8) + 43*Power(t1,7)*t2 - 
                  346*Power(t1,6)*Power(t2,2) + 
                  622*Power(t1,5)*Power(t2,3) + 
                  172*Power(t1,4)*Power(t2,4) - 
                  363*Power(t1,3)*Power(t2,5) - 
                  268*Power(t1,2)*Power(t2,6) + 389*t1*Power(t2,7) - 
                  78*Power(t2,8)) + 
               s2*t1*t2*(-48*Power(t1,8) - 100*Power(t1,7)*t2 + 
                  201*Power(t1,6)*Power(t2,2) + 
                  325*Power(t1,5)*Power(t2,3) - 
                  753*Power(t1,4)*Power(t2,4) + 
                  582*Power(t1,3)*Power(t2,5) - 
                  226*Power(t1,2)*Power(t2,6) + 15*t1*Power(t2,7) + 
                  4*Power(t2,8)) + 
               Power(s2,2)*t2*
                (146*Power(t1,8) + 15*Power(t1,7)*t2 + 
                  239*Power(t1,6)*Power(t2,2) - 
                  1679*Power(t1,5)*Power(t2,3) + 
                  1911*Power(t1,4)*Power(t2,4) - 
                  878*Power(t1,3)*Power(t2,5) + 
                  358*Power(t1,2)*Power(t2,6) - 108*t1*Power(t2,7) + 
                  12*Power(t2,8))) + 
            Power(s1,4)*(2*Power(s2,9)*t2 - 
               2*Power(s2,8)*
                (2*Power(t1,2) + 31*t1*t2 + 42*Power(t2,2)) + 
               2*Power(s2,7)*
                (13*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 33*Power(t2,3)) + 
               2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (20*Power(t1,4) + 20*Power(t1,3)*t2 - 
                  9*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,6)*(-72*Power(t1,4) + 248*Power(t1,3)*t2 + 
                  197*Power(t1,2)*Power(t2,2) - 607*t1*Power(t2,3) + 
                  462*Power(t2,4)) + 
               Power(s2,5)*(112*Power(t1,5) - 102*Power(t1,4)*t2 + 
                  342*Power(t1,3)*Power(t2,2) - 
                  180*Power(t1,2)*Power(t2,3) - 359*t1*Power(t2,4) + 
                  164*Power(t2,5)) - 
               Power(s2,4)*(104*Power(t1,6) + 482*Power(t1,5)*t2 + 
                  109*Power(t1,4)*Power(t2,2) - 
                  661*Power(t1,3)*Power(t2,3) + 
                  710*Power(t1,2)*Power(t2,4) - 907*t1*Power(t2,5) + 
                  318*Power(t2,6)) + 
               Power(s2,3)*(54*Power(t1,7) + 486*Power(t1,6)*t2 - 
                  779*Power(t1,5)*Power(t2,2) + 
                  1757*Power(t1,4)*Power(t2,3) - 
                  2255*Power(t1,3)*Power(t2,4) + 
                  740*Power(t1,2)*Power(t2,5) + 341*t1*Power(t2,6) - 
                  128*Power(t2,7)) + 
               s2*t1*t2*(-140*Power(t1,7) + 245*Power(t1,6)*t2 + 
                  163*Power(t1,5)*Power(t2,2) - 
                  653*Power(t1,4)*Power(t2,3) + 
                  681*Power(t1,3)*Power(t2,4) - 
                  346*Power(t1,2)*Power(t2,5) + 8*t1*Power(t2,6) + 
                  30*Power(t2,7)) + 
               Power(s2,2)*(-12*Power(t1,8) - 8*Power(t1,7)*t2 + 
                  312*Power(t1,6)*Power(t2,2) - 
                  1760*Power(t1,5)*Power(t2,3) + 
                  2817*Power(t1,4)*Power(t2,4) - 
                  1848*Power(t1,3)*Power(t2,5) + 
                  703*Power(t1,2)*Power(t2,6) - 270*t1*Power(t2,7) + 
                  44*Power(t2,8)))) - 
         Power(s,4)*(2*Power(s2,2)*Power(t1,2)*Power(t2,3)*
             Power(s2 - t1 + t2,3)*
             (Power(s2,5) + 2*Power(s2,4)*(t1 - 4*t2) + 
               s2*Power(t1,2)*
                (2*Power(t1,2) - 3*t1*t2 - 11*Power(t2,2)) + 
               Power(s2,2)*t2*(16*Power(t1,2) + t1*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*t2*(-2*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(-5*Power(t1,2) - 3*t1*t2 + 11*Power(t2,2))) + 
            Power(s1,8)*s2*Power(t1,2)*
             (2*Power(s2,4) + 2*Power(t1,4) - 3*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) - 
               2*Power(t2,4) + Power(s2,3)*(-8*t1 + 3*t2) + 
               3*Power(s2,2)*(4*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               s2*(-8*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,7)*(2*Power(s2,6)*t1*(5*t1 - 26*t2) + 
               4*Power(s2,7)*t2 - 
               2*Power(t1,3)*Power(t1 - t2,3)*t2*(4*t1 + 5*t2) - 
               4*Power(s2,5)*
                (12*Power(t1,3) - 43*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,4)*(92*Power(t1,4) - 249*Power(t1,3)*t2 + 
                  451*Power(t1,2)*Power(t2,2) - 132*t1*Power(t2,3) - 
                  32*Power(t2,4)) - 
               s2*Power(t1,2)*
                (8*Power(t1,5) - 29*Power(t1,4)*t2 + 
                  36*Power(t1,3)*Power(t2,2) + 
                  Power(t1,2)*Power(t2,3) + 2*t1*Power(t2,4) - 
                  28*Power(t2,5)) + 
               Power(s2,2)*t1*
                (42*Power(t1,5) - 79*Power(t1,4)*t2 + 
                  217*Power(t1,3)*Power(t2,2) - 
                  261*Power(t1,2)*Power(t2,3) + 125*t1*Power(t2,4) - 
                  24*Power(t2,5)) - 
               Power(s2,3)*(88*Power(t1,5) - 183*Power(t1,4)*t2 + 
                  514*Power(t1,3)*Power(t2,2) - 
                  412*Power(t1,2)*Power(t2,3) + 76*t1*Power(t2,4) + 
                  12*Power(t2,5))) + 
            Power(s1,6)*(10*Power(s2,8)*t2 + 
               2*Power(s2,7)*
                (2*Power(t1,2) - 78*t1*t2 - 19*Power(t2,2)) + 
               2*Power(t1,3)*Power(t1 - t2,3)*t2*
                (6*Power(t1,2) + 15*t1*t2 + 10*Power(t2,2)) - 
               2*Power(s2,6)*
                (7*Power(t1,3) - 235*Power(t1,2)*t2 + 
                  96*t1*Power(t2,2) + 61*Power(t2,3)) + 
               Power(s2,5)*(6*Power(t1,4) - 519*Power(t1,3)*t2 + 
                  789*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) - 
                  38*Power(t2,4)) + 
               Power(s2,4)*(36*Power(t1,5) + 151*Power(t1,4)*t2 - 
                  658*Power(t1,3)*Power(t2,2) + 72*t1*Power(t2,4) + 
                  88*Power(t2,5)) + 
               Power(s2,3)*(-64*Power(t1,6) + 79*Power(t1,5)*t2 - 
                  128*Power(t1,4)*Power(t2,2) + 
                  746*Power(t1,3)*Power(t2,3) - 
                  649*Power(t1,2)*Power(t2,4) + 66*t1*Power(t2,5) + 
                  52*Power(t2,6)) + 
               Power(s2,2)*t1*
                (42*Power(t1,6) - 7*Power(t1,5)*t2 + 
                  256*Power(t1,4)*Power(t2,2) - 
                  873*Power(t1,3)*Power(t2,3) + 
                  840*Power(t1,2)*Power(t2,4) - 308*t1*Power(t2,5) + 
                  60*Power(t2,6)) - 
               s2*Power(t1,2)*
                (10*Power(t1,6) + 40*Power(t1,5)*t2 + 
                  23*Power(t1,4)*Power(t2,2) - 
                  253*Power(t1,3)*Power(t2,3) + 
                  263*Power(t1,2)*Power(t2,4) - 169*t1*Power(t2,5) + 
                  92*Power(t2,6))) + 
            s1*s2*t1*Power(t2,2)*
             (Power(t1,3)*Power(t1 - t2,3)*Power(t2,3)*(8*t1 + t2) + 
               Power(s2,9)*(-4*t1 + 2*t2) + 
               2*Power(s2,8)*
                (5*Power(t1,2) + 17*t1*t2 - 5*Power(t2,2)) - 
               4*Power(s2,7)*t2*
                (30*Power(t1,2) - 16*t1*t2 + 5*Power(t2,2)) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (16*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,6)*(-10*Power(t1,4) + 36*Power(t1,3)*t2 + 
                  74*Power(t1,2)*Power(t2,2) - 96*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,5)*(-20*Power(t1,5) + 266*Power(t1,4)*t2 - 
                  482*Power(t1,3)*Power(t2,2) + 
                  388*Power(t1,2)*Power(t2,3) - 140*t1*Power(t2,4) + 
                  26*Power(t2,5)) + 
               Power(s2,4)*(54*Power(t1,6) - 350*Power(t1,5)*t2 + 
                  458*Power(t1,4)*Power(t2,2) - 
                  177*Power(t1,3)*Power(t2,3) - 
                  37*Power(t1,2)*Power(t2,4) + 58*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,2)*t1*
                (10*Power(t1,7) + 24*Power(t1,6)*t2 - 
                  154*Power(t1,5)*Power(t2,2) + 
                  259*Power(t1,4)*Power(t2,3) - 
                  192*Power(t1,3)*Power(t2,4) + 
                  58*Power(t1,2)*Power(t2,5) + 7*t1*Power(t2,6) - 
                  12*Power(t2,7)) + 
               Power(s2,3)*(-40*Power(t1,7) + 124*Power(t1,6)*t2 + 
                  12*Power(t1,5)*Power(t2,2) - 
                  331*Power(t1,4)*Power(t2,3) + 
                  373*Power(t1,3)*Power(t2,4) - 
                  202*Power(t1,2)*Power(t2,5) + 64*t1*Power(t2,6) - 
                  8*Power(t2,7))) + 
            Power(s1,5)*(4*Power(s2,9)*t2 - 
               2*Power(s2,8)*
                (3*Power(t1,2) + 60*t1*t2 + 37*Power(t2,2)) + 
               Power(s2,7)*(34*Power(t1,3) + 310*Power(t1,2)*t2 + 
                  58*t1*Power(t2,2) - 56*Power(t2,3)) + 
               2*Power(t1,3)*Power(t1 - t2,3)*t2*
                (10*Power(t1,3) - 17*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,6)*(-84*Power(t1,4) - 144*Power(t1,3)*t2 + 
                  18*Power(t1,2)*Power(t2,2) + 92*t1*Power(t2,3) + 
                  236*Power(t2,4)) + 
               Power(s2,5)*(116*Power(t1,5) - 254*Power(t1,4)*t2 + 
                  572*Power(t1,3)*Power(t2,2) - 
                  697*Power(t1,2)*Power(t2,3) - 243*t1*Power(t2,4) + 
                  244*Power(t2,5)) + 
               Power(s2,4)*(-94*Power(t1,6) + 222*Power(t1,5)*t2 - 
                  1105*Power(t1,4)*Power(t2,2) + 
                  1478*Power(t1,3)*Power(t2,3) - 
                  408*Power(t1,2)*Power(t2,4) + 19*t1*Power(t2,5) - 
                  50*Power(t2,6)) + 
               s2*Power(t1,2)*t2*
                (-40*Power(t1,6) + 99*Power(t1,5)*t2 + 
                  171*Power(t1,4)*Power(t2,2) - 
                  597*Power(t1,3)*Power(t2,3) + 
                  619*Power(t1,2)*Power(t2,4) - 386*t1*Power(t2,5) + 
                  124*Power(t2,6)) + 
               Power(s2,3)*(42*Power(t1,7) + 36*Power(t1,6)*t2 + 
                  555*Power(t1,5)*Power(t2,2) - 
                  578*Power(t1,4)*Power(t2,3) - 
                  371*Power(t1,3)*Power(t2,4) + 
                  353*Power(t1,2)*Power(t2,5) + 125*t1*Power(t2,6) - 
                  80*Power(t2,7)) - 
               Power(s2,2)*t1*
                (8*Power(t1,7) + 34*Power(t1,6)*t2 + 
                  63*Power(t1,5)*Power(t2,2) + 
                  436*Power(t1,4)*Power(t2,3) - 
                  1269*Power(t1,3)*Power(t2,4) + 
                  977*Power(t1,2)*Power(t2,5) - 266*t1*Power(t2,6) + 
                  51*Power(t2,7))) + 
            Power(s1,2)*t2*(-2*Power(t1,4)*Power(t1 - t2,3)*Power(t2,3)*
                (2*Power(t1,2) + t1*t2 - 2*Power(t2,2)) - 
               2*Power(s2,10)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
               2*Power(s2,9)*
                (6*Power(t1,3) + 2*Power(t1,2)*t2 + 17*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,8)*(-42*Power(t1,4) + 12*Power(t1,3)*t2 + 
                  18*Power(t1,2)*Power(t2,2) + 64*t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               s2*Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                (12*Power(t1,4) - 14*Power(t1,3)*t2 + 
                  20*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
                  17*Power(t2,4)) + 
               Power(s2,7)*(93*Power(t1,5) + 3*Power(t1,4)*t2 - 
                  525*Power(t1,3)*Power(t2,2) + 
                  265*Power(t1,2)*Power(t2,3) - 96*t1*Power(t2,4) + 
                  12*Power(t2,5)) + 
               Power(s2,6)*(-122*Power(t1,6) - 49*Power(t1,5)*t2 + 
                  984*Power(t1,4)*Power(t2,2) - 
                  725*Power(t1,3)*Power(t2,3) + 
                  222*Power(t1,2)*Power(t2,4) - 140*t1*Power(t2,5) + 
                  26*Power(t2,6)) + 
               Power(s2,5)*(90*Power(t1,7) + 35*Power(t1,6)*t2 - 
                  538*Power(t1,5)*Power(t2,2) + 
                  218*Power(t1,4)*Power(t2,3) + 
                  335*Power(t1,3)*Power(t2,4) - 
                  268*Power(t1,2)*Power(t2,5) + 58*t1*Power(t2,6) - 
                  2*Power(t2,7)) - 
               Power(s2,2)*Power(t1,2)*t2*
                (2*Power(t1,7) - Power(t1,6)*t2 + 
                  120*Power(t1,5)*Power(t2,2) - 
                  300*Power(t1,4)*Power(t2,3) + 
                  264*Power(t1,3)*Power(t2,4) - 
                  134*Power(t1,2)*Power(t2,5) + 48*t1*Power(t2,6) + 
                  Power(t2,7)) + 
               Power(s2,3)*t1*
                (5*Power(t1,8) - 6*Power(t1,7)*t2 + 
                  71*Power(t1,6)*Power(t2,2) + 
                  128*Power(t1,5)*Power(t2,3) - 
                  502*Power(t1,4)*Power(t2,4) + 
                  400*Power(t1,3)*Power(t2,5) - 
                  161*Power(t1,2)*Power(t2,6) + 81*t1*Power(t2,7) - 
                  12*Power(t2,8)) + 
               Power(s2,4)*(-34*Power(t1,8) + 7*Power(t1,7)*t2 - 
                  59*Power(t1,6)*Power(t2,2) + 
                  222*Power(t1,5)*Power(t2,3) - 
                  279*Power(t1,4)*Power(t2,4) + 
                  249*Power(t1,3)*Power(t2,5) - 
                  129*Power(t1,2)*Power(t2,6) + 64*t1*Power(t2,7) - 
                  8*Power(t2,8))) + 
            Power(s1,3)*(-2*Power(s2,10)*t2*(t1 + t2) - 
               2*Power(s2,9)*
                (Power(t1,3) + 11*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  17*Power(t2,3)) + 
               2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,3)*
                (12*Power(t1,3) + 10*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + 2*Power(t2,3)) + 
               2*Power(s2,8)*
                (6*Power(t1,4) + 48*Power(t1,3)*t2 - 
                  33*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,7)*(28*Power(t1,5) + 141*Power(t1,4)*t2 - 
                  381*Power(t1,3)*Power(t2,2) + 
                  279*Power(t1,2)*Power(t2,3) - 337*t1*Power(t2,4) + 
                  140*Power(t2,5)) + 
               2*Power(s2,6)*
                (16*Power(t1,6) + 65*Power(t1,5)*t2 - 
                  386*Power(t1,4)*Power(t2,2) + 
                  99*Power(t1,3)*Power(t2,3) - 
                  134*Power(t1,2)*Power(t2,4) + 134*t1*Power(t2,5) - 
                  37*Power(t2,6)) - 
               Power(s2,5)*(18*Power(t1,7) + 155*Power(t1,6)*t2 - 
                  856*Power(t1,5)*Power(t2,2) + 
                  24*Power(t1,4)*Power(t2,3) + 
                  273*Power(t1,3)*Power(t2,4) - 
                  183*Power(t1,2)*Power(t2,5) + 301*t1*Power(t2,6) - 
                  86*Power(t2,7)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-42*Power(t1,7) + 28*Power(t1,6)*t2 - 
                  22*Power(t1,5)*Power(t2,2) + 
                  227*Power(t1,4)*Power(t2,3) - 
                  360*Power(t1,3)*Power(t2,4) + 
                  271*Power(t1,2)*Power(t2,5) - 118*t1*Power(t2,6) + 
                  16*Power(t2,7)) + 
               Power(s2,3)*t2*
                (-102*Power(t1,8) + 241*Power(t1,7)*t2 - 
                  954*Power(t1,6)*Power(t2,2) + 
                  1864*Power(t1,5)*Power(t2,3) - 
                  1483*Power(t1,4)*Power(t2,4) + 
                  573*Power(t1,3)*Power(t2,5) - 
                  204*Power(t1,2)*Power(t2,6) + 90*t1*Power(t2,7) - 
                  12*Power(t2,8)) + 
               Power(s2,2)*t1*t2*
                (23*Power(t1,8) + 27*Power(t1,7)*t2 + 
                  279*Power(t1,6)*Power(t2,2) - 
                  688*Power(t1,5)*Power(t2,3) + 
                  347*Power(t1,4)*Power(t2,4) + 
                  2*Power(t1,3)*Power(t2,5) + 
                  16*Power(t1,2)*Power(t2,6) + 11*t1*Power(t2,7) - 
                  3*Power(t2,8)) + 
               Power(s2,4)*(4*Power(t1,8) + 173*Power(t1,7)*t2 - 
                  621*Power(t1,6)*Power(t2,2) + 
                  674*Power(t1,5)*Power(t2,3) - 
                  911*Power(t1,4)*Power(t2,4) + 
                  743*Power(t1,3)*Power(t2,5) - 
                  105*Power(t1,2)*Power(t2,6) - 159*t1*Power(t2,7) + 
                  42*Power(t2,8))) + 
            Power(s1,4)*(-2*Power(s2,9)*
                (Power(t1,2) + 19*t1*t2 + 14*Power(t2,2)) - 
               2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)*
                (20*Power(t1,3) + 15*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,8)*(6*Power(t1,3) + 80*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) + 82*Power(t2,3)) + 
               2*Power(s2,7)*(Power(t1,4) - 14*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) - 102*t1*Power(t2,3) + 
                  109*Power(t2,4)) + 
               Power(s2,6)*(-28*Power(t1,5) + 106*Power(t1,4)*t2 + 
                  35*Power(t1,3)*Power(t2,2) + 
                  62*Power(t1,2)*Power(t2,3) - 57*t1*Power(t2,4) - 
                  66*Power(t2,5)) + 
               Power(s2,5)*(42*Power(t1,6) - 408*Power(t1,5)*t2 + 
                  234*Power(t1,4)*Power(t2,2) - 
                  577*Power(t1,3)*Power(t2,3) + 
                  124*Power(t1,2)*Power(t2,4) + 555*t1*Power(t2,5) - 
                  266*Power(t2,6)) + 
               s2*Power(t1,2)*t2*
                (32*Power(t1,7) + 50*Power(t1,6)*t2 - 
                  70*Power(t1,5)*Power(t2,2) - 
                  348*Power(t1,4)*Power(t2,3) + 
                  671*Power(t1,3)*Power(t2,4) - 
                  578*Power(t1,2)*Power(t2,5) + 325*t1*Power(t2,6) - 
                  74*Power(t2,7)) + 
               Power(s2,4)*(-26*Power(t1,7) + 434*Power(t1,6)*t2 - 
                  752*Power(t1,5)*Power(t2,2) + 
                  1987*Power(t1,4)*Power(t2,3) - 
                  1771*Power(t1,3)*Power(t2,4) + 
                  284*Power(t1,2)*Power(t2,5) + 135*t1*Power(t2,6) - 
                  40*Power(t2,7)) + 
               Power(s2,2)*t1*t2*
                (-68*Power(t1,7) - 143*Power(t1,6)*t2 + 
                  433*Power(t1,5)*Power(t2,2) + 
                  144*Power(t1,4)*Power(t2,3) - 
                  711*Power(t1,3)*Power(t2,4) + 
                  421*Power(t1,2)*Power(t2,5) - 98*t1*Power(t2,6) + 
                  18*Power(t2,7)) + 
               Power(s2,3)*(6*Power(t1,8) - 110*Power(t1,7)*t2 + 
                  604*Power(t1,6)*Power(t2,2) - 
                  1803*Power(t1,5)*Power(t2,3) + 
                  1668*Power(t1,4)*Power(t2,4) - 
                  373*Power(t1,3)*Power(t2,5) + 
                  34*Power(t1,2)*Power(t2,6) - 193*t1*Power(t2,7) + 
                  52*Power(t2,8)))))*lg2(s1 + t1 - t2))/
     (Power(s,3)*Power(s1,2)*Power(s2,2)*Power(t1,2)*(s1 + t1)*
       Power(s - s2 + t1,2)*Power(t2,2)*Power(s - s1 + t2,2)*
       (s - s1 - s2 + t2)*Power(s2 - t1 + t2,3)) - 
    (8*(Power(s,5)*Power(t1 - t2,2) + 
         s1*s2*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*t2*
          (s1*(s2 - t1) - s2*t2) + 
         Power(s,4)*((t1 - t2)*
             (-3*s2*t1 + Power(t1,2) + 4*s2*t2 - 2*t1*t2) + 
            2*s1*(s2*t1 - 2*Power(t1,2) + 3*t1*t2 - Power(t2,2))) + 
         Power(s,3)*(Power(s2,2)*
             (2*Power(t1,2) - 9*t1*t2 + 7*Power(t2,2)) + 
            t2*(-Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
               Power(t2,3)) + 
            s2*(-2*Power(t1,3) + 6*Power(t1,2)*t2 - 7*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,2) + 6*Power(t1,2) - 7*t1*t2 + 
               2*Power(t2,2) - s2*(5*t1 + t2)) - 
            s1*(3*Power(t1,3) - 6*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
               2*Power(t2,3) + Power(s2,2)*(4*t1 + 3*t2) + 
               s2*(-12*Power(t1,2) + 17*t1*t2 - 3*Power(t2,2)))) - 
         s*(-(s2*(s2 - t1)*Power(t2,2)*
               (2*Power(s2,2) + 2*s2*t2 + Power(t2,2))) + 
            Power(s1,4)*(t1*(-t1 + t2) + s2*(t1 + t2)) + 
            Power(s1,3)*(-Power(s2,3) + Power(t1,3) + 
               Power(s2,2)*(6*t1 + 4*t2) - 
               2*s2*(3*Power(t1,2) - t1*t2 + Power(t2,2))) + 
            s1*t2*(2*Power(s2,4) + Power(s2,3)*(2*t1 - 7*t2) + 
               Power(s2,2)*(t1 - 3*t2)*t2 - 
               t1*(t1 - 2*t2)*Power(t2,2) - 
               s2*t2*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(4*Power(s2,3)*(t1 + 2*t2) + 
               t1*t2*(Power(t1,2) - 2*Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               s2*(2*Power(t1,3) - Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  2*Power(t2,3)))) - 
         Power(s,2)*(Power(s1,3)*
             (Power(s2,2) + Power(-2*t1 + t2,2) - 2*s2*(2*t1 + t2)) + 
            Power(s1,2)*(Power(s2,3) - 3*Power(t1,3) + 
               3*Power(t1,2)*t2 - 2*Power(t2,3) - 
               Power(s2,2)*(10*t1 + 7*t2) + 
               s2*(15*Power(t1,2) - 13*t1*t2 + 2*Power(t2,2))) + 
            t2*(t1*(t1 - t2)*Power(t2,2) + Power(s2,3)*(-4*t1 + 6*t2) + 
               s2*t2*(3*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2))) + 
            s1*(-(Power(s2,3)*(2*t1 + 5*t2)) + 
               2*Power(s2,2)*(4*Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
               s2*(-4*Power(t1,3) + 7*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)) + 
               t2*(-2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)))))*lg2(-(t1/(s2*(-s2 + t1 - t2)))))/
     (s*s1*s2*t1*(s - s2 + t1)*Power(t2,2)*(s - s1 + t2)) + 
    (8*(-(s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*(s2 - t1)*
            (s1*(s2 - t1) - s2*t2)) + 
         Power(s,4)*(Power(s2,2)*(t1 - t2) + 
            t2*(-3*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) - 
            s2*(Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
         Power(s,3)*(Power(s2,3)*(-2*t1 + 3*t2) - 
            t1*t2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,2) - 13*t1*t2 + 6*Power(t2,2)) + 
            s2*t2*(12*Power(t1,2) - 13*t1*t2 + 6*Power(t2,2)) + 
            s1*(Power(s2,3) + Power(s2,2)*(-5*t1 + t2) + 
               s2*(5*Power(t1,2) - 15*t1*t2 + 2*Power(t2,2)) + 
               t2*(8*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)))) - 
         Power(s,2)*(-(Power(s2,4)*(t1 - 3*t2)) + 
            Power(t1,3)*(t1 - t2)*t2 + 
            Power(s1,2)*(3*Power(s2,3) - 11*Power(s2,2)*t1 + 
               s2*t1*(9*t1 - 16*t2) + t1*(7*t1 - 4*t2)*t2) + 
            s2*t1*t2*(-9*Power(t1,2) + 7*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,3)*(Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,2)*t2*(15*Power(t1,2) - 11*t1*t2 + 6*Power(t2,2)) + 
            s1*(2*Power(s2,4) + Power(s2,3)*(-8*t1 + t2) + 
               Power(t1,2)*t2*(-5*t1 + 3*t2) + 
               Power(s2,2)*(6*Power(t1,2) - 20*t1*t2 + 4*Power(t2,2)) + 
               s2*t2*(16*Power(t1,2) - 9*t1*t2 + 4*Power(t2,2)))) + 
         s*(Power(s1,3)*(4*Power(s2,3) - 11*Power(s2,2)*t1 + 
               s2*t1*(7*t1 - 6*t2) + 2*Power(t1,2)*t2) + 
            Power(s1,2)*(4*Power(s2,4) + 6*Power(s2,2)*t1*(t1 - t2) + 
               5*s2*Power(t1,2)*t2 - 2*Power(t1,3)*t2 - 
               2*Power(s2,3)*(5*t1 + t2)) + 
            s2*t2*(Power(s2,4) + Power(t1,3)*(2*t1 - t2) + 
               Power(s2,3)*(-3*t1 + 2*t2) - 
               s2*t1*(6*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(6*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) + 
            s1*(Power(s2,5) + 2*Power(s2,3)*Power(t1 - t2,2) + 
               Power(t1,4)*t2 + s2*Power(t1,2)*t2*(-6*t1 + t2) - 
               Power(s2,4)*(3*t1 + t2) + 
               2*Power(s2,2)*(4*Power(t1,2)*t2 + Power(t2,3)))))*
       lg2(-((-s + s1 - t2)/(s1*(-s2 + t1 - t2)))))/
     (s*s1*s2*t1*Power(s - s2 + t1,2)*t2*(s - s1 + t2)) + 
    (8*(2*Power(s,4)*Power(t1 - t2,2)*t2 + 
         Power(s2,2)*(t1 - t2)*Power(t2,2)*
          (-(t1*(2*t1 + t2)) + s2*(3*t1 + 2*t2)) + 
         Power(s1,4)*(2*Power(s2,3) + t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-5*t1 + 2*t2) + 
            s2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
         Power(s1,3)*(2*Power(s2,4) - 4*Power(s2,3)*t1 + 
            Power(s2,2)*(Power(t1,2) - 2*t1*t2 - Power(t2,2)) - 
            t1*t2*(Power(t1,2) + t1*t2 - Power(t2,2)) + 
            s2*(Power(t1,3) + 5*t1*Power(t2,2))) + 
         s1*s2*t2*(-2*Power(s2,3)*t1 + 
            Power(t1,2)*(2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(5*Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)) - 
            s2*(5*Power(t1,3) - 4*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
               2*Power(t2,3))) + 
         Power(s1,2)*(2*Power(s2,4)*(t1 - t2) + 
            Power(t1,3)*Power(t2,2) + 
            Power(s2,3)*(-6*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*(6*Power(t1,3) - 8*Power(t1,2)*t2 + 
               11*t1*Power(t2,2) + Power(t2,3)) - 
            s2*(2*Power(t1,4) - 4*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) + Power(t2,4))) \
+ Power(s,3)*(Power(t1 - t2,2)*t2*(t1 + 2*t2) - 
            2*s2*(Power(t1,3) - 4*t1*Power(t2,2) + 3*Power(t2,3)) + 
            s1*(-2*s2*t1*(t1 - 3*t2) + 
               t2*(-7*Power(t1,2) + 11*t1*t2 - 4*Power(t2,2)))) - 
         Power(s,2)*(Power(s1,2)*
             (4*Power(s2,2)*t1 + s2*t1*(-7*t1 + 16*t2) + 
               t2*(-9*Power(t1,2) + 11*t1*t2 - 3*Power(t2,2))) + 
            (t1 - t2)*(t1*Power(t2,2)*(-t1 + t2) + 
               Power(s2,2)*(-2*Power(t1,2) + 2*t1*t2 + 6*Power(t2,2)) + 
               s2*(Power(t1,3) + 3*Power(t1,2)*t2 - t1*Power(t2,2) - 
                  6*Power(t2,3))) + 
            s1*(2*Power(s2,2)*t1*(t1 + 5*t2) + 
               s2*(-6*Power(t1,3) + 2*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 10*Power(t2,3)) + 
               t2*(3*Power(t1,3) + Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  2*Power(t2,3)))) + 
         s*(-(Power(s1,3)*(2*Power(s2,3) + Power(s2,2)*(-9*t1 + 2*t2) + 
                 s2*(8*Power(t1,2) - 15*t1*t2 + Power(t2,2)) + 
                 t2*(5*Power(t1,2) - 5*t1*t2 + Power(t2,2)))) + 
            s2*(t1 - t2)*t2*(2*Power(s2,2)*(t1 + t2) + 
               s2*(2*Power(t1,2) - 5*t1*t2 - 6*Power(t2,2)) + 
               t1*(-2*Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3)*(t1 + t2) + 
               2*Power(s2,2)*(Power(t1,2) + 4*t1*t2 + Power(t2,2)) + 
               t2*(3*Power(t1,3) + 2*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3)) - 
               s2*(5*Power(t1,3) - 3*Power(t1,2)*t2 + 6*t1*Power(t2,2) + 
                  4*Power(t2,3))) + 
            s1*(2*Power(t1,2)*Power(t2,2)*(-t1 + t2) + 
               2*Power(s2,3)*t1*(2*t1 + 3*t2) - 
               Power(s2,2)*(7*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 8*Power(t2,3)) + 
               s2*(3*Power(t1,4) - Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  4*Power(t2,4)))))*
       lg2(-((-s + s1 - t2)/((-s + s2 - t1)*(-s2 + t1 - t2)))))/
     (Power(s1,2)*s2*(-s + s2 - t1)*t1*(s1 + t1 - t2)*t2*(s - s1 + t2)) - 
    (4*(Power(s,6)*t1*(2*s1 + 3*t1 - 3*t2)*t2 + 
         s1*s2*(Power(s1,2) - Power(s2,2))*t1*(s1 - t2)*(s1 + t1 - t2)*
          (s1*(-s2 + t1) + s2*t2) - 
         Power(s,5)*t2*(4*Power(s1,2)*t1 + 
            s1*(6*s2*t1 + 4*Power(t1,2) - s2*t2 - 7*t1*t2) + 
            s2*(10*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
            t1*(-5*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2))) + 
         Power(s,4)*(2*Power(s1,3)*t1*t2 + 
            Power(s1,2)*(Power(s2,2)*t2 - 5*t1*Power(t2,2) + 
               s2*(Power(t1,2) + 4*t1*t2 - Power(t2,2))) + 
            t2*(Power(s2,2)*(13*Power(t1,2) - 13*t1*t2 + 
                  3*Power(t2,2)) + 
               t1*(3*Power(t1,3) + 2*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) - Power(t2,3)) - 
               s2*(15*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + Power(t2,3))) + 
            s1*(4*Power(s2,2)*(2*t1 - t2)*t2 + 
               t1*t2*(-9*Power(t1,2) + 4*t1*t2 + 5*Power(t2,2)) + 
               s2*(Power(t1,3) + 4*Power(t1,2)*t2 - 13*t1*Power(t2,2) + 
                  2*Power(t2,3)))) - 
         Power(s,3)*(2*Power(s1,4)*s2*t1 + 
            Power(s1,3)*(-(t1*Power(t2,2)) + Power(s2,2)*(-t1 + t2) + 
               s2*(8*Power(t1,2) - 12*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3)*(t1 + t2) + 
               Power(s2,2)*(Power(t1,2) - t1*t2 - 5*Power(t2,2)) + 
               t1*t2*(-4*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2)) + 
               s2*(7*Power(t1,3) - 18*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - Power(t2,3))) + 
            t2*(-Power(t1,5) - 2*Power(t1,4)*t2 + 
               2*Power(t1,3)*Power(t2,2) + t1*Power(t2,4) + 
               Power(s2,3)*(8*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(9*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) - Power(t2,3)) - 
               Power(s2,2)*(16*Power(t1,3) + Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 3*Power(t2,3))) + 
            s1*(Power(s2,3)*(2*Power(t1,2) + 4*t1*t2 - 5*Power(t2,2)) + 
               s2*Power(t1,2)*
                (Power(t1,2) - 18*t1*t2 + 16*Power(t2,2)) + 
               t1*t2*(5*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,2)*(2*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) + 7*Power(t2,3)))) - 
         s*(2*Power(s1,6)*s2*t1 + 
            Power(s1,5)*s2*(s2*t1 + 8*Power(t1,2) - s2*t2 - 6*t1*t2) + 
            s2*(-s2 + t1)*Power(t2,2)*
             (2*Power(t1,4) - Power(t1,3)*t2 - t1*Power(t2,3) + 
               2*s2*Power(t1,2)*(-2*t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2))) + 
            Power(s1,4)*(Power(s2,3)*t1 - Power(t1,3)*t2 + 
               Power(s2,2)*(7*Power(t1,2) - 13*t1*t2 + 3*Power(t2,2)) + 
               s2*t1*(7*Power(t1,2) - 12*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,3)*(Power(s2,3)*t1*(t1 - 10*t2) + 
               Power(s2,4)*(2*t1 + t2) + 
               s2*Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
               Power(t1,2)*t2*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(7*Power(t1,3) - 21*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 3*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*t1*t2*(-13*t1 + 18*t2) + 
               Power(s2,4)*(2*Power(t1,2) - 2*t1*t2 - 3*Power(t2,2)) - 
               Power(t1,2)*t2*
                (Power(t1,3) + t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*t1*t2*(Power(t1,3) - Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,4) - 5*Power(t1,3)*t2 + 
                  13*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            s1*t2*(Power(s2,4)*t2*(-2*t1 + 3*t2) - 
               2*Power(s2,3)*t1*
                (4*Power(t1,2) - 8*t1*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*t2*
                (Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*t1*
                (6*Power(t1,3) - 5*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) + 
               s2*t1*(-2*Power(t1,4) - Power(t1,3)*t2 + t1*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(s,2)*(4*Power(s1,5)*s2*t1 + 
            Power(s1,4)*(Power(s2,2)*(t1 - t2) + Power(t1,2)*t2 + 
               s2*(14*Power(t1,2) - 16*t1*t2 + Power(t2,2))) + 
            Power(s1,3)*(-(Power(t1,3)*t2) + 2*t1*Power(t2,3) + 
               2*Power(s2,3)*(t1 + t2) + 
               Power(s2,2)*(9*Power(t1,2) - 22*t1*t2 + 2*Power(t2,2)) + 
               s2*(12*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) - 2*Power(t2,3))) + 
            s1*(2*Power(s2,4)*(Power(t1,2) - Power(t2,2)) + 
               Power(s2,3)*t2*
                (-2*Power(t1,2) - 5*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*t1*
                (Power(t1,3) - 19*Power(t1,2)*t2 + 29*t1*Power(t2,2) - 
                  14*Power(t2,3)) + 
               2*s2*t1*t2*(5*Power(t1,3) - Power(t1,2)*t2 - 
                  3*Power(t2,3)) - 
               2*t1*t2*(Power(t1,4) + Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) - 
                  Power(t2,4))) + 
            Power(s1,2)*(Power(s2,4)*(2*t1 + t2) + 
               Power(s2,3)*(2*Power(t1,2) - 4*t1*t2 - 7*Power(t2,2)) + 
               t1*t2*(Power(t1,3) + 3*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,2)*(9*Power(t1,3) - 34*Power(t1,2)*t2 + 
                  34*t1*Power(t2,2) - Power(t2,3)) + 
               s2*(2*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            t2*(Power(s2,4)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               Power(t1,2)*t2*
                (Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(s2,2)*t1*
                (6*Power(t1,3) + 13*Power(t1,2)*t2 - 12*t1*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s2,3)*(6*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 3*Power(t2,3)) - 
               2*s2*(Power(t1,5) + 4*Power(t1,4)*t2 - 
                  3*Power(t1,3)*Power(t2,2) - t1*Power(t2,4)))))*
       lg2((-s2 + t1 - t2)/(s - s1 - s2)))/
     (s*s1*s2*Power(t1,2)*(s - s2 + t1)*(s1 + t1 - t2)*t2*
       Power(s - s1 + t2,2)) - 
    (4*(2*Power(s,3)*s1*(2*Power(s1,2) + 3*s1*(t1 - t2) + 
            4*Power(t1 - t2,2)) - 
         4*Power(s1,4)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
            Power(t2,2) + s2*(-2*t1 + t2)) + 
         Power(s1,3)*(-3*Power(s2,3) + Power(t1,3) + 8*Power(t1,2)*t2 - 
            18*t1*Power(t2,2) + 12*Power(t2,3) + 
            Power(s2,2)*(7*t1 + 8*t2) + 
            s2*(-5*Power(t1,2) - 16*t1*t2 + 10*Power(t2,2))) + 
         Power(s1,2)*(-12*Power(t1 - t2,2)*Power(t2,2) - 
            8*Power(s2,2)*t1*(t1 + 2*t2) + Power(s2,3)*(4*t1 + 9*t2) + 
            s2*(4*Power(t1,3) + 7*Power(t1,2)*t2 + 4*t1*Power(t2,2) - 
               6*Power(t2,3))) + 
         s2*(Power(t1 - t2,2)*
             (2*Power(t1,3) - Power(t1,2)*t2 + 2*Power(t2,3)) + 
            Power(s2,2)*(2*Power(t1,3) - 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) + 3*Power(t2,3)) - 
            2*s2*(2*Power(t1,4) - 4*Power(t1,3)*t2 + 
               3*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 2*Power(t2,4)\
)) + s1*(Power(s2,3)*(3*Power(t1,2) - 8*t1*t2 - 9*Power(t2,2)) + 
            Power(s2,2)*(-5*Power(t1,3) + 14*Power(t1,2)*t2 + 
               11*t1*Power(t2,2) - 8*Power(t2,3)) + 
            Power(t1 - t2,2)*
             (Power(t1,3) - 2*t1*Power(t2,2) + 4*Power(t2,3)) + 
            s2*(Power(t1,4) - 4*Power(t1,3)*t2 - 
               3*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) - 
               2*Power(t2,4))) - 
         2*Power(s,2)*(2*Power(s1,4) + Power(s1,3)*(3*s2 + t1 - 4*t2) - 
            s1*(t1 - t2)*(-2*s2*t1 + Power(t1,2) + 7*s2*t2 + 3*t1*t2 - 
               4*Power(t2,2)) + 
            Power(t1 - t2,2)*(t1*(t1 - t2) - s2*(2*t1 + t2)) + 
            Power(s1,2)*(6*Power(t1 - t2,2) - s2*(2*t1 + 9*t2))) + 
         s*(4*Power(s1,4)*(s2 - t1) + 
            8*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               Power(t2,2) - s2*(2*t1 + t2)) - 
            (t1 - t2)*(Power(s2,2)*
                (3*Power(t1,2) - 6*t1*t2 - 5*Power(t2,2)) + 
               Power(t1 - t2,2)*(Power(t1,2) - 2*Power(t2,2)) - 
               4*s2*(Power(t1,3) - 2*Power(t1,2)*t2 + Power(t2,3))) + 
            2*s1*(Power(t1 - t2,2)*(Power(t1,2) + 6*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) + 3*t1*t2 + 9*Power(t2,2)) + 
               s2*(-2*Power(t1,3) - 3*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  4*Power(t2,3))) - 
            Power(s1,2)*(-2*s2*t1*(4*t1 + 5*t2) + 
               Power(s2,2)*(5*t1 + 21*t2) + 
               3*(Power(t1,3) + 3*Power(t1,2)*t2 - 10*t1*Power(t2,2) + 
                  6*Power(t2,3)))))*lg2((s*(-s2 + t1 - t2))/(s1 + s2)))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) + 
    (4*(Power(s1,3)*Power(s2 - t1,2)*(s2 + t1) - 
         2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*(2*s2 - t1)*Power(t1 - t2,3) - 
         s*(3*Power(s2,2) - 4*s2*t1 + Power(t1,2))*Power(t1 - t2,3) + 
         s2*(Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*(2*Power(t1,3) - 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - Power(t2,3)) + 
            2*s2*t1*(-2*Power(t1,3) + 4*Power(t1,2)*t2 - 
               3*t1*Power(t2,2) + Power(t2,3))) - 
         Power(s1,2)*(-(s2*(s2 - t1)*
               (s2*(4*t1 - 3*t2) + t1*(-4*t1 + t2))) + 
            s*(Power(s2,2)*(t1 - 3*t2) + 3*Power(t1,2)*(t1 - t2) + 
               2*s2*t1*(-2*t1 + t2))) + 
         s1*(Power(t1,3)*Power(t1 - t2,2) + 
            Power(s2,2)*t1*(-5*Power(t1,2) + 14*t1*t2 - 5*Power(t2,2)) + 
            Power(s2,3)*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
            s2*Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) - 
            2*Power(s,2)*(t1 - t2)*(2*t1*(-t1 + t2) + s2*(t1 + 2*t2)) + 
            2*s*(Power(t1,2)*Power(t1 - t2,2) - 
               s2*t1*(2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) + 5*t1*t2 - 3*Power(t2,2)))))*
       lg2(-((Power(s,2)*(-s2 + t1 - t2))/(s1 + s2))))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) - 
    (4*(Power(s,6)*(2*s1 + t1 - t2)*t2 + 
         s1*s2*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*(s1 - t2)*
          (s1 + t1 - t2)*(s1*(s2 - t1) - s2*t2) + 
         Power(s,5)*t2*(-4*Power(s1,2) - 3*s2*t1 + Power(t1,2) + 
            4*s2*t2 - Power(t2,2) + s1*(-7*s2 + 2*t1 + 3*t2)) + 
         Power(s,4)*t2*(2*Power(s1,3) + 3*Power(s2,2)*t1 - 
            2*s2*Power(t1,2) + Power(s1,2)*(11*s2 - 7*t1 - 2*t2) - 
            6*Power(s2,2)*t2 + Power(t1,2)*t2 + 4*s2*Power(t2,2) - 
            Power(t2,3) + s1*(9*Power(s2,2) - 7*s2*t1 + Power(t1,2) - 
               11*s2*t2 + 2*t1*t2 + 3*Power(t2,2))) - 
         s*(2*Power(s1,6)*s2 + 
            Power(s1,4)*s2*(5*Power(s2,2) + s2*(5*t1 - 14*t2) + 
               Power(t1 - 2*t2,2)) + 
            Power(s1,5)*s2*(5*s2 + 2*t1 - 5*t2) + 
            s2*(s2 - t1)*Power(t2,3)*(Power(s2,2) + Power(t2,2)) + 
            s1*t2*(t1*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,4)*(-3*t1 + t2) + s2*Power(t2,3)*(t1 + 2*t2) - 
               Power(s2,2)*t2*(2*Power(t1,2) + t1*t2 + 2*Power(t2,2)) - 
               Power(s2,3)*(Power(t1,2) - 9*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(3*Power(s2,4) + Power(s2,3)*(3*t1 - 13*t2) - 
               t1*t2*(Power(t1,2) + 2*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 15*t1*t2 + 13*Power(t2,2)) + 
               s2*(Power(t1,3) + Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*(3*t1 - 5*t2) - 
               t1*(t1 - 4*t2)*Power(t2,3) + 
               Power(s2,3)*(-2*Power(t1,2) - 11*t1*t2 + 
                  11*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 3*Power(t2,3)) - 
               s2*t2*(Power(t1,3) + 2*Power(t1,2)*t2 + 4*Power(t2,3)))) - 
         Power(s,3)*(2*Power(s1,4)*s2 + 4*Power(s1,3)*t1*(s2 - t2) + 
            t2*(Power(s2,3)*(t1 - 4*t2) - Power(t1,2)*Power(t2,2) + 
               Power(t2,4) - 
               Power(s2,2)*(Power(t1,2) - 6*Power(t2,2)) - 
               2*s2*t2*(-Power(t1,2) + t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3) + 11*Power(s2,2)*t2 + 
               s2*(3*Power(t1,2) - 22*t1*t2 - 3*Power(t2,2)) + 
               t2*(5*Power(t1,2) + 2*t1*t2 + 4*Power(t2,2))) + 
            s1*(Power(s2,3)*(2*t1 + 3*t2) - 
               Power(s2,2)*t2*(8*t1 + 15*t2) - 
               t2*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s2*(Power(t1,3) + Power(t1,2)*t2 + 11*t1*Power(t2,2) + 
                  5*Power(t2,3)))) + 
         Power(s,2)*(4*Power(s1,5)*s2 + 
            Power(s1,4)*s2*(4*s2 + 7*t1 - 9*t2) + 
            Power(t2,2)*(-Power(s2,4) + 4*Power(s2,3)*t2 + 
               t1*(t1 - t2)*Power(t2,2) + 2*s2*Power(t2,3) + 
               Power(s2,2)*(Power(t1,2) - 3*t1*t2 - Power(t2,2))) + 
            Power(s1,3)*(4*Power(s2,3) + Power(s2,2)*(6*t1 - 7*t2) + 
               3*Power(t1,2)*t2 + 2*Power(t2,3) + 
               s2*(5*Power(t1,2) - 18*t1*t2 + 7*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,4) + Power(s2,3)*(3*t1 - 2*t2) + 
               3*Power(s2,2)*(Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               s2*(2*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 3*Power(t2,3)) - 
               t2*(2*Power(t1,3) + 3*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*(Power(s2,4)*(2*t1 - t2) + 
               2*Power(t2,3)*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) - 
               Power(s2,3)*(Power(t1,2) + 5*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - 2*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + Power(t2,3)) - 
               s2*t2*(Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  7*Power(t2,3)))))*lg2((-s2 + t1 - t2)/t1))/
     (s*s1*s2*t1*(s - s2 + t1)*(s1 + t1 - t2)*t2*Power(s - s1 + t2,2)) + 
    (8*(Power(s,6)*(2*s1 + t1 - t2)*t2 + 
         s1*s2*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*(s1 - t2)*
          (s1 + t1 - t2)*(s1*(s2 - t1) - s2*t2) + 
         Power(s,5)*t2*(-4*Power(s1,2) - 3*s2*t1 + Power(t1,2) + 
            4*s2*t2 - Power(t2,2) + s1*(-7*s2 + 2*t1 + 3*t2)) + 
         Power(s,4)*t2*(2*Power(s1,3) + 3*Power(s2,2)*t1 - 
            2*s2*Power(t1,2) + Power(s1,2)*(11*s2 - 7*t1 - 2*t2) - 
            6*Power(s2,2)*t2 + Power(t1,2)*t2 + 4*s2*Power(t2,2) - 
            Power(t2,3) + s1*(9*Power(s2,2) - 7*s2*t1 + Power(t1,2) - 
               11*s2*t2 + 2*t1*t2 + 3*Power(t2,2))) - 
         s*(2*Power(s1,6)*s2 + 
            Power(s1,4)*s2*(5*Power(s2,2) + s2*(5*t1 - 14*t2) + 
               Power(t1 - 2*t2,2)) + 
            Power(s1,5)*s2*(5*s2 + 2*t1 - 5*t2) + 
            s2*(s2 - t1)*Power(t2,3)*(Power(s2,2) + Power(t2,2)) + 
            s1*t2*(t1*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,4)*(-3*t1 + t2) + s2*Power(t2,3)*(t1 + 2*t2) - 
               Power(s2,2)*t2*(2*Power(t1,2) + t1*t2 + 2*Power(t2,2)) - 
               Power(s2,3)*(Power(t1,2) - 9*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(3*Power(s2,4) + Power(s2,3)*(3*t1 - 13*t2) - 
               t1*t2*(Power(t1,2) + 2*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) - 15*t1*t2 + 13*Power(t2,2)) + 
               s2*(Power(t1,3) + Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*(3*t1 - 5*t2) - 
               t1*(t1 - 4*t2)*Power(t2,3) + 
               Power(s2,3)*(-2*Power(t1,2) - 11*t1*t2 + 
                  11*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) - 3*Power(t2,3)) - 
               s2*t2*(Power(t1,3) + 2*Power(t1,2)*t2 + 4*Power(t2,3)))) - 
         Power(s,3)*(2*Power(s1,4)*s2 + 4*Power(s1,3)*t1*(s2 - t2) + 
            t2*(Power(s2,3)*(t1 - 4*t2) - Power(t1,2)*Power(t2,2) + 
               Power(t2,4) - 
               Power(s2,2)*(Power(t1,2) - 6*Power(t2,2)) - 
               2*s2*t2*(-Power(t1,2) + t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,3) + 11*Power(s2,2)*t2 + 
               s2*(3*Power(t1,2) - 22*t1*t2 - 3*Power(t2,2)) + 
               t2*(5*Power(t1,2) + 2*t1*t2 + 4*Power(t2,2))) + 
            s1*(Power(s2,3)*(2*t1 + 3*t2) - 
               Power(s2,2)*t2*(8*t1 + 15*t2) - 
               t2*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s2*(Power(t1,3) + Power(t1,2)*t2 + 11*t1*Power(t2,2) + 
                  5*Power(t2,3)))) + 
         Power(s,2)*(4*Power(s1,5)*s2 + 
            Power(s1,4)*s2*(4*s2 + 7*t1 - 9*t2) + 
            Power(t2,2)*(-Power(s2,4) + 4*Power(s2,3)*t2 + 
               t1*(t1 - t2)*Power(t2,2) + 2*s2*Power(t2,3) + 
               Power(s2,2)*(Power(t1,2) - 3*t1*t2 - Power(t2,2))) + 
            Power(s1,3)*(4*Power(s2,3) + Power(s2,2)*(6*t1 - 7*t2) + 
               3*Power(t1,2)*t2 + 2*Power(t2,3) + 
               s2*(5*Power(t1,2) - 18*t1*t2 + 7*Power(t2,2))) + 
            Power(s1,2)*(2*Power(s2,4) + Power(s2,3)*(3*t1 - 2*t2) + 
               3*Power(s2,2)*(Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               s2*(2*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  12*t1*Power(t2,2) + 3*Power(t2,3)) - 
               t2*(2*Power(t1,3) + 3*t1*Power(t2,2) + 4*Power(t2,3))) + 
            s1*(Power(s2,4)*(2*t1 - t2) + 
               2*Power(t2,3)*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) - 
               Power(s2,3)*(Power(t1,2) + 5*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - 2*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + Power(t2,3)) - 
               s2*t2*(Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  7*Power(t2,3)))))*
       lg2(-((-s2 + t1 - t2)/((s - s1 - s2)*t1))))/
     (s*s1*s2*t1*(s - s2 + t1)*(s1 + t1 - t2)*t2*Power(s - s1 + t2,2)) - 
    (4*(2*Power(s,5)*t1*(t1 - t2)*t2 + 
         s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*t1*(s1 + t1 - t2)*
          (s1*(s2 - t1) - s2*t2) + 
         Power(s,4)*t2*(4*Power(t1,2)*(t1 - t2) - 
            s2*(7*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
            s1*(2*t1*(-2*t1 + t2) + s2*(t1 + t2))) + 
         Power(s,3)*(Power(s1,2)*
             (s2*t1*(t1 - 6*t2) + Power(s2,2)*t2 + t1*(3*t1 - t2)*t2) + 
            t2*(3*Power(t1,3)*(t1 - t2) - 
               s2*t1*(13*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(10*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2))) + 
            s1*(2*Power(t1,2)*t2*(-3*t1 + t2) - 
               Power(s2,2)*t2*(t1 + 4*t2) + 
               s2*t1*(Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)))) + 
         Power(s,2)*(Power(s1,3)*
             (Power(s2,2)*t1 - Power(t1,2)*t2 - 
               s2*(3*Power(t1,2) - 6*t1*t2 + Power(t2,2))) + 
            s1*(-3*Power(t1,4)*t2 + Power(s2,3)*t2*(-t1 + 5*t2) + 
               Power(s2,2)*t1*
                (-2*Power(t1,2) + 5*t1*t2 - 12*Power(t2,2)) + 
               s2*t1*t2*(5*Power(t1,2) + t1*t2 + Power(t2,2))) + 
            t2*(Power(t1,4)*(t1 - t2) + 
               s2*(-9*Power(t1,4) + 6*Power(t1,3)*t2) + 
               Power(s2,3)*(-7*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*t1*(15*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2))\
) - Power(s1,2)*(Power(s2,2)*t1*(t1 - 11*t2) + 2*Power(s2,3)*t2 + 
               Power(t1,2)*t2*(-3*t1 + t2) + 
               s2*(3*Power(t1,3) + Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  Power(t2,3)))) - 
         s*(Power(s1,4)*s2*(t1*(-4*t1 + t2) + s2*(2*t1 + t2)) + 
            Power(s1,3)*(2*Power(s2,3)*t1 + Power(t1,3)*t2 + 
               Power(s2,2)*(-2*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) - 
               s2*t1*(4*Power(t1,2) - 2*t1*t2 + Power(t2,2))) - 
            s2*(s2 - t1)*t2*(Power(t1,3)*(2*t1 - t2) + 
               2*s2*Power(t1,2)*(-2*t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) - t1*t2 + Power(t2,2))) + 
            s1*(-2*s2*Power(t1,4)*t2 + Power(t1,5)*t2 + 
               Power(s2,2)*t1*Power(t2,2)*(2*t1 + t2) + 
               Power(s2,4)*t2*(-t1 + 2*t2) - 
               Power(s2,3)*t1*(Power(t1,2) - 4*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*s2*(-(Power(s2,3)*t2) + 
               Power(t1,2)*t2*(t1 + 2*t2) + Power(s2,2)*t1*(t1 + 3*t2) + 
               s2*(-4*Power(t1,3) + 4*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 
                  Power(t2,3)))))*lg2((-s2 + t1 - t2)/(-s + s1 - t2)))/
     (s*s1*s2*Power(t1,2)*(s - s2 + t1)*(s1 + t1 - t2)*t2*(s - s1 + t2)) + 
    (8*(2*Power(s,5)*t1*(t1 - t2)*t2 + 
         s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*t1*(s1 + t1 - t2)*
          (s1*(s2 - t1) - s2*t2) + 
         Power(s,4)*t2*(4*Power(t1,2)*(t1 - t2) - 
            s2*(7*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
            s1*(2*t1*(-2*t1 + t2) + s2*(t1 + t2))) + 
         Power(s,3)*(Power(s1,2)*
             (s2*t1*(t1 - 6*t2) + Power(s2,2)*t2 + t1*(3*t1 - t2)*t2) + 
            t2*(3*Power(t1,3)*(t1 - t2) - 
               s2*t1*(13*Power(t1,2) - 10*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(10*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2))) + 
            s1*(2*Power(t1,2)*t2*(-3*t1 + t2) - 
               Power(s2,2)*t2*(t1 + 4*t2) + 
               s2*t1*(Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)))) + 
         Power(s,2)*(Power(s1,3)*
             (Power(s2,2)*t1 - Power(t1,2)*t2 - 
               s2*(3*Power(t1,2) - 6*t1*t2 + Power(t2,2))) + 
            s1*(-3*Power(t1,4)*t2 + Power(s2,3)*t2*(-t1 + 5*t2) + 
               Power(s2,2)*t1*
                (-2*Power(t1,2) + 5*t1*t2 - 12*Power(t2,2)) + 
               s2*t1*t2*(5*Power(t1,2) + t1*t2 + Power(t2,2))) + 
            t2*(Power(t1,4)*(t1 - t2) + 
               s2*(-9*Power(t1,4) + 6*Power(t1,3)*t2) + 
               Power(s2,3)*(-7*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*t1*(15*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2))\
) - Power(s1,2)*(Power(s2,2)*t1*(t1 - 11*t2) + 2*Power(s2,3)*t2 + 
               Power(t1,2)*t2*(-3*t1 + t2) + 
               s2*(3*Power(t1,3) + Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  Power(t2,3)))) - 
         s*(Power(s1,4)*s2*(t1*(-4*t1 + t2) + s2*(2*t1 + t2)) + 
            Power(s1,3)*(2*Power(s2,3)*t1 + Power(t1,3)*t2 + 
               Power(s2,2)*(-2*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) - 
               s2*t1*(4*Power(t1,2) - 2*t1*t2 + Power(t2,2))) - 
            s2*(s2 - t1)*t2*(Power(t1,3)*(2*t1 - t2) + 
               2*s2*Power(t1,2)*(-2*t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) - t1*t2 + Power(t2,2))) + 
            s1*(-2*s2*Power(t1,4)*t2 + Power(t1,5)*t2 + 
               Power(s2,2)*t1*Power(t2,2)*(2*t1 + t2) + 
               Power(s2,4)*t2*(-t1 + 2*t2) - 
               Power(s2,3)*t1*(Power(t1,2) - 4*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*s2*(-(Power(s2,3)*t2) + 
               Power(t1,2)*t2*(t1 + 2*t2) + Power(s2,2)*t1*(t1 + 3*t2) + 
               s2*(-4*Power(t1,3) + 4*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 
                  Power(t2,3)))))*
       lg2(-((-s2 + t1 - t2)/((s - s1 - s2)*(-s + s1 - t2)))))/
     (s*s1*s2*Power(t1,2)*(s - s2 + t1)*(s1 + t1 - t2)*t2*(s - s1 + t2)) - 
    (4*(Power(s,3)*(2*Power(s1,2) + s1*(t1 - t2) + 3*Power(t1 - t2,2)) - 
         2*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
            Power(t2,2) + s2*(-2*t1 + t2)) + 
         s1*(-(Power(s2,3)*(t1 - 6*t2)) + 
            s2*t1*(Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)) - 
            Power(t1 - t2,2)*(Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*(-3*Power(s2,3) + Power(t1,3) - 5*t1*Power(t2,2) + 
            4*Power(t2,3) + Power(s2,2)*(7*t1 + 2*t2) + 
            s2*(-5*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) - 
         s2*(Power(t1 - t2,2)*(2*Power(t1,2) + t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,2) - t1*t2 + 3*Power(t2,2)) + 
            s2*(-4*Power(t1,3) + 4*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
               2*Power(t2,3))) + 
         Power(s,2)*(-2*Power(s1,3) + Power(s1,2)*(-3*s2 + t1 + 2*t2) + 
            s1*(5*s2*t1 - 7*Power(t1,2) + 6*s2*t2 + 11*t1*t2 - 
               4*Power(t2,2)) + 
            (t1 - t2)*(2*t1*(t1 - t2) + s2*(-4*t1 + 7*t2))) + 
         s*(2*Power(s1,3)*(s2 - t1) + 
            Power(t1 - t2,2)*(Power(t1,2) + Power(t2,2)) + 
            Power(s2,2)*(3*Power(t1,2) - 6*t1*t2 + 7*Power(t2,2)) + 
            s2*(-4*Power(t1,3) + 8*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            2*Power(s1,2)*(2*Power(s2,2) + 3*Power(t1,2) - 3*t1*t2 + 
               2*Power(t2,2) - s2*(5*t1 + t2)) - 
            s1*(3*Power(t1,3) - 3*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
               5*Power(t2,3) + Power(s2,2)*(5*t1 + 11*t2) + 
               s2*(-8*Power(t1,2) + 2*Power(t2,2)))))*
       lg2((s*(-s2 + t1 - t2))/(s1 + t1 - t2)))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,2)) - 
    (4*(-(Power(s,3)*(2*Power(s1,3) + 3*Power(s1,2)*(t1 - t2) + 
              4*s1*Power(t1 - t2,2) - 3*Power(t1 - t2,3))) + 
         2*Power(s1,4)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
            Power(t2,2) + s2*(-2*t1 + t2)) - 
         Power(s1,3)*(2*Power(t1,3) + 4*Power(t1,2)*t2 - 
            9*t1*Power(t2,2) + 6*Power(t2,3) + 
            2*Power(s2,2)*(t1 + 2*t2) + 
            s2*(-4*Power(t1,2) - 8*t1*t2 + 5*Power(t2,2))) + 
         Power(s1,2)*(-8*Power(s2,3)*t1 + 
            6*Power(t1 - t2,2)*Power(t2,2) + 
            2*Power(s2,2)*t1*(8*t1 + t2) - 
            s2*(8*Power(t1,3) + 2*Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
               3*Power(t2,3))) + 
         s1*(2*Power(s2,3)*t1*(-3*t1 + 8*t2) - 
            Power(t1 - t2,2)*
             (2*Power(t1,3) - t1*Power(t2,2) + 2*Power(t2,3)) + 
            2*Power(s2,2)*(5*Power(t1,3) - 14*Power(t1,2)*t2 + 
               t1*Power(t2,2) + 2*Power(t2,3)) + 
            s2*(-2*Power(t1,4) + 8*Power(t1,3)*t2 - 
               3*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + Power(t2,4)\
)) - s2*(2*Power(s2,2)*t1*(2*Power(t1,2) - 3*t1*t2 + 4*Power(t2,2)) + 
            Power(t1 - t2,2)*
             (4*Power(t1,3) - 2*Power(t1,2)*t2 + Power(t2,3)) + 
            2*s2*(-4*Power(t1,4) + 8*Power(t1,3)*t2 - 
               6*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + Power(t2,4))) \
+ Power(s,2)*(2*Power(s1,4) + Power(s1,3)*(3*s2 + t1 - 4*t2) + 
            s1*(t1 - t2)*(5*s2*t1 - 7*Power(t1,2) - s2*t2 + 3*t1*t2 + 
               4*Power(t2,2)) + 
            Power(t1 - t2,2)*(4*t1*(t1 - t2) + s2*(-8*t1 + 5*t2)) + 
            Power(s1,2)*(6*Power(t1 - t2,2) - s2*(2*t1 + 9*t2))) + 
         s*(-2*Power(s1,4)*(s2 - t1) - 
            4*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               Power(t2,2) - s2*(2*t1 + t2)) + 
            Power(s1,2)*(6*Power(t1,3) - 15*t1*Power(t2,2) + 
               9*Power(t2,3) - 2*s2*t1*(5*t1 + t2) + 
               Power(s2,2)*(4*t1 + 6*t2)) + 
            (t1 - t2)*(Power(t1 - t2,2)*(2*Power(t1,2) - Power(t2,2)) + 
               2*Power(s2,2)*(3*Power(t1,2) - 6*t1*t2 + Power(t2,2)) - 
               2*s2*(4*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + Power(t2,3))) - 
            2*s1*(Power(s2,2)*t1*(2*t1 + 9*t2) + 
               Power(t1 - t2,2)*(2*Power(t1,2) + 3*Power(t2,2)) + 
               s2*(-4*Power(t1,3) - 3*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  2*Power(t2,3)))))*
       lg2((s*(-s2 + t1 - t2))/(-s + s1 + t1 - t2)))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) + 
    (8*(Power(s,3)*(2*Power(s1,2) + s1*(t1 - t2) + 3*Power(t1 - t2,2)) - 
         2*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
            Power(t2,2) + s2*(-2*t1 + t2)) + 
         s1*(-(Power(s2,3)*(t1 - 6*t2)) + 
            s2*t1*(Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) + 
            Power(s2,2)*(Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)) - 
            Power(t1 - t2,2)*(Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
         Power(s1,2)*(-3*Power(s2,3) + Power(t1,3) - 5*t1*Power(t2,2) + 
            4*Power(t2,3) + Power(s2,2)*(7*t1 + 2*t2) + 
            s2*(-5*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) - 
         s2*(Power(t1 - t2,2)*(2*Power(t1,2) + t1*t2 + Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,2) - t1*t2 + 3*Power(t2,2)) + 
            s2*(-4*Power(t1,3) + 4*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
               2*Power(t2,3))) + 
         Power(s,2)*(-2*Power(s1,3) + Power(s1,2)*(-3*s2 + t1 + 2*t2) + 
            s1*(5*s2*t1 - 7*Power(t1,2) + 6*s2*t2 + 11*t1*t2 - 
               4*Power(t2,2)) + 
            (t1 - t2)*(2*t1*(t1 - t2) + s2*(-4*t1 + 7*t2))) + 
         s*(2*Power(s1,3)*(s2 - t1) + 
            Power(t1 - t2,2)*(Power(t1,2) + Power(t2,2)) + 
            Power(s2,2)*(3*Power(t1,2) - 6*t1*t2 + 7*Power(t2,2)) + 
            s2*(-4*Power(t1,3) + 8*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            2*Power(s1,2)*(2*Power(s2,2) + 3*Power(t1,2) - 3*t1*t2 + 
               2*Power(t2,2) - s2*(5*t1 + t2)) - 
            s1*(3*Power(t1,3) - 3*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
               5*Power(t2,3) + Power(s2,2)*(5*t1 + 11*t2) + 
               s2*(-8*Power(t1,2) + 2*Power(t2,2)))))*
       lg2(-((s*(s - s1 - s2)*(-s2 + t1 - t2))/
           ((s1 + s2)*(-s + s1 + t1 - t2)))))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,2)) + 
    (4*(2*Power(s1*(s2 - t1) - s2*t2,3) + 
         2*Power(s,4)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) + 
         Power(s,3)*(-2*Power(s1,3) - 2*Power(s2,3) - 
            4*s2*Power(t1 - t2,2) + Power(t1 - t2,2)*(t1 + t2) + 
            Power(s2,2)*(3*t1 + t2) + Power(s1,2)*(-4*s2 + t1 + 3*t2) - 
            4*s1*(Power(s2,2) + Power(t1 - t2,2) - s2*(t1 + t2))) - 
         2*s*(2*Power(s1,3)*Power(s2 - t1,2) + 
            Power(s2,2)*(2*s2 + t1 - 2*t2)*Power(t2,2) + 
            Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) - 4*s2*t1 + 2*Power(t1,2) - 6*s2*t2 - t1*t2) \
- s1*s2*t2*(4*Power(s2,2) + t1*(t1 + t2) - s2*(5*t1 + 6*t2))) + 
         Power(s,2)*(3*Power(s1,3)*(s2 - t1) + 
            Power(s1,2)*(6*Power(s2,2) - 12*s2*t1 + 6*Power(t1,2) - 
               9*s2*t2 - 2*t1*t2) - 
            s2*t2*(3*Power(s2,2) + Power(t1,2) + 2*s2*(t1 - 3*t2) - 
               4*t1*t2 + 3*Power(t2,2)) + 
            s1*(3*Power(s2,3) - 3*Power(s2,2)*(3*t1 + 4*t2) - 
               t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(9*Power(t1,2) - 6*t1*t2 + 9*Power(t2,2)))))*
       lg2(((-s + s1 + t1 - t2)*(-s2 + t1 - t2))/s))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (4*(-(Power(s,3)*(2*Power(s1,3) + 3*Power(s1,2)*(t1 - t2) + 
              4*s1*Power(t1 - t2,2) + Power(t1 - t2,3))) - 
         s2*(2*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
          Power(t2,3) + 2*Power(s1,4)*
          (Power(s2,2) + Power(t1,2) - t1*t2 + Power(t2,2) + 
            s2*(-2*t1 + t2)) + 
         Power(s1,3)*(2*Power(s2,3) - 4*Power(s2,2)*(t1 + t2) + 
            t2*(-4*Power(t1,2) + 9*t1*t2 - 6*Power(t2,2)) + 
            s2*(2*Power(t1,2) + 8*t1*t2 - 5*Power(t2,2))) + 
         s1*Power(t2,2)*(6*Power(s2,3) + (t1 - 2*t2)*Power(t1 - t2,2) + 
            Power(s2,2)*(-8*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
         Power(s1,2)*t2*(-6*Power(s2,3) + 10*Power(s2,2)*t1 + 
            6*Power(t1 - t2,2)*t2 + 
            s2*(-4*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) + 
         Power(s,2)*(2*Power(s1,4) + Power(s1,3)*(3*s2 + t1 - 4*t2) - 
            3*s2*Power(t1 - t2,2)*t2 + 
            s1*(t1 - t2)*(Power(t1,2) + s2*(t1 - 9*t2) - 5*t1*t2 + 
               4*Power(t2,2)) + 
            Power(s1,2)*(6*Power(t1 - t2,2) - s2*(2*t1 + 9*t2))) + 
         s*(-2*Power(s1,4)*(s2 - t1) - 
            (4*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
             (t1 - t2)*Power(t2,2) - 
            4*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               Power(t2,2) - s2*(2*t1 + t2)) + 
            2*s1*t2*(Power(s2,2)*(t1 - 6*t2) - 3*Power(t1 - t2,2)*t2 + 
               s2*(Power(t1,2) + t1*t2 - 2*Power(t2,2))) + 
            Power(s1,2)*(-2*s2*t1*(t1 + 3*t2) + 
               2*Power(s2,2)*(t1 + 6*t2) + 
               3*t2*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)))))*
       lg2(-(((-s + s1 + t1 - t2)*(-s2 + t1 - t2))/(s1 + t1 - t2))))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) + 
    (4*(-(Power(s,3)*(2*Power(s1,3) + 3*Power(s1,2)*(t1 - t2) + 
              4*s1*Power(t1 - t2,2) + Power(t1 - t2,3))) - 
         s2*(2*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
          Power(t2,3) + 2*Power(s1,4)*
          (Power(s2,2) + Power(t1,2) - t1*t2 + Power(t2,2) + 
            s2*(-2*t1 + t2)) + 
         Power(s1,3)*(2*Power(s2,3) - 4*Power(s2,2)*(t1 + t2) + 
            t2*(-4*Power(t1,2) + 9*t1*t2 - 6*Power(t2,2)) + 
            s2*(2*Power(t1,2) + 8*t1*t2 - 5*Power(t2,2))) + 
         s1*Power(t2,2)*(6*Power(s2,3) + (t1 - 2*t2)*Power(t1 - t2,2) + 
            Power(s2,2)*(-8*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
         Power(s1,2)*t2*(-6*Power(s2,3) + 10*Power(s2,2)*t1 + 
            6*Power(t1 - t2,2)*t2 + 
            s2*(-4*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) + 
         Power(s,2)*(2*Power(s1,4) + Power(s1,3)*(3*s2 + t1 - 4*t2) - 
            3*s2*Power(t1 - t2,2)*t2 + 
            s1*(t1 - t2)*(Power(t1,2) + s2*(t1 - 9*t2) - 5*t1*t2 + 
               4*Power(t2,2)) + 
            Power(s1,2)*(6*Power(t1 - t2,2) - s2*(2*t1 + 9*t2))) + 
         s*(-2*Power(s1,4)*(s2 - t1) - 
            (4*Power(s2,2) - 2*s2*(t1 - t2) + Power(t1 - t2,2))*
             (t1 - t2)*Power(t2,2) - 
            4*Power(s1,3)*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               Power(t2,2) - s2*(2*t1 + t2)) + 
            2*s1*t2*(Power(s2,2)*(t1 - 6*t2) - 3*Power(t1 - t2,2)*t2 + 
               s2*(Power(t1,2) + t1*t2 - 2*Power(t2,2))) + 
            Power(s1,2)*(-2*s2*t1*(t1 + 3*t2) + 
               2*Power(s2,2)*(t1 + 6*t2) + 
               3*t2*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)))))*
       lg2(-((s*Power(-s2 + t1 - t2,2))/(-s + s1 + t1 - t2))))/
     (s1*s2*t1*(-s + s1 - t2)*Power(s1 + t1 - t2,3)) - 
    (8*(Power(s1,4)*Power(t1,2)*(-s2 + t1) - 
         Power(s,5)*(t1*(t1 - t2) + s1*(t1 + t2)) + 
         Power(s1,3)*(Power(t1,3)*(t1 - 4*t2) + 8*s2*Power(t1,2)*t2 + 
            Power(s2,3)*(t1 + t2) - 2*Power(s2,2)*t1*(t1 + 2*t2)) + 
         s2*t1*Power(t2,2)*(Power(s2,3) + Power(t1,2)*(2*t1 - 3*t2) - 
            Power(s2,2)*t2 + s2*t1*(-2*t1 + 3*t2)) + 
         s1*t2*(-(Power(s2,4)*t1) + Power(t1,3)*(t1 - 2*t2)*t2 + 
            Power(s2,3)*t2*(4*t1 + t2) + 
            Power(s2,2)*t1*(3*Power(t1,2) - 5*t1*t2 - 3*Power(t2,2)) + 
            s2*Power(t1,2)*(-2*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*(Power(t1,3)*t2*(-2*t1 + 3*t2) + 
            Power(s2,3)*(Power(t1,2) - 4*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*t1*(-2*Power(t1,2) + 7*t1*t2 + 7*Power(t2,2)) + 
            s2*(Power(t1,4) - 8*Power(t1,2)*Power(t2,2))) + 
         Power(s,4)*(Power(s1,2)*(-s2 + 3*t1 + 2*t2) + 
            s1*(3*Power(t1,2) + 4*s2*t2 - 8*t1*t2) + 
            t1*(s2*(t1 - 2*t2) + 2*t2*(-t1 + t2))) + 
         Power(s,3)*(Power(s1,3)*(2*s2 - 3*t1 - t2) + 
            t1*(-Power(t1,3) - Power(s2,2)*(t1 - 4*t2) + 
               Power(t1,2)*t2 + s2*(t1 - 3*t2)*t2 - t1*Power(t2,2) + 
               Power(t2,3)) - 
            s1*(Power(t1,3) + s2*t1*(t1 - 14*t2) - 
               2*Power(s2,2)*(t1 - 3*t2) - Power(t1,2)*t2 + 
               5*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s1,2)*(3*Power(s2,2) + s2*(t1 - 6*t2) + 
               t1*(-3*t1 + 13*t2))) + 
         Power(s,2)*(Power(s1,4)*(-s2 + t1) + 
            Power(s1,3)*(-4*Power(s2,2) - 2*s2*t1 + Power(t1,2) + 
               2*s2*t2 - 6*t1*t2) + 
            Power(s1,2)*(-3*Power(s2,3) - 3*Power(s2,2)*(t1 - 2*t2) - 
               s2*(2*Power(t1,2) + 17*t1*t2 + 2*Power(t2,2)) + 
               t1*(3*Power(t1,2) + 4*t1*t2 + 3*Power(t2,2))) + 
            s1*(-2*Power(s2,3)*(t1 - 2*t2) - 16*Power(s2,2)*t1*t2 + 
               3*s2*t2*(2*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               t1*(3*Power(t1,3) - 6*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  4*Power(t2,3))) + 
            t1*(Power(s2,3)*(t1 - 4*t2) + 2*Power(t1,2)*t2*(-t1 + t2) + 
               Power(s2,2)*(-Power(t1,2) + 2*t1*t2 + 4*Power(t2,2)) + 
               s2*(Power(t1,3) - Power(t1,2)*t2 - 2*Power(t2,3)))) + 
         s*(Power(s1,4)*s2*(s2 + t1) + 
            Power(s1,3)*(2*Power(s2,3) + Power(s2,2)*(t1 - 2*t2) - 
               3*Power(t1,2)*(t1 + t2) + s2*t1*(3*t1 + 5*t2)) + 
            t1*t2*(Power(s2,4) - 4*Power(s2,3)*t2 + 
               Power(t1,2)*t2*(-t1 + t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(-Power(t1,2) + 2*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,4) - 3*Power(t1,3)*(t1 - 3*t2) - 
               2*Power(s2,3)*t2 - 3*s2*t1*t2*(5*t1 + 3*t2) + 
               Power(s2,2)*(3*Power(t1,2) + 12*t1*t2 + 4*Power(t2,2))) + 
            s1*(-2*Power(s2,3)*t1*(t1 - 5*t2) + Power(s2,4)*(t1 - t2) + 
               Power(t1,2)*t2*(4*Power(t1,2) - 5*t1*t2 - 3*Power(t2,2)) + 
               3*Power(s2,2)*(Power(t1,3) - 3*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) - Power(t2,3)) + 
               s2*t1*(-2*Power(t1,3) + Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 7*Power(t2,3)))))*lg2(-(s1/(s*t2))))/
     (s1*s2*(-s + s2 - t1)*t1*(s1 + t1 - t2)*t2*Power(s - s1 + t2,2)) - 
    (4*(Power(s,6)*t1*(2*s2 - t1 + t2) + 
         s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*(s2 - t1)*
          (s2 - t1 + t2)*(s1*(s2 - t1) - s2*t2) + 
         Power(s,5)*t1*(-4*Power(s2,2) + 3*s2*t1 - Power(t1,2) + 
            s1*(-7*s2 + 4*t1 - 3*t2) + 2*s2*t2 + Power(t2,2)) + 
         Power(s,4)*t1*(2*Power(s2,3) - Power(t1,3) + t1*Power(t2,2) + 
            3*Power(s1,2)*(3*s2 - 2*t1 + t2) - 
            Power(s2,2)*(2*t1 + 7*t2) + 
            s1*(11*Power(s2,2) - 11*s2*t1 + 4*Power(t1,2) - 7*s2*t2 - 
               2*Power(t2,2)) + 
            s2*(3*Power(t1,2) + 2*t1*t2 + Power(t2,2))) - 
         Power(s,3)*(Power(s1,2)*t1*
             (11*Power(s2,2) - 15*s2*t1 + 6*Power(t1,2) - 8*s2*t2 - 
               Power(t2,2)) + 
            Power(s1,3)*(2*Power(s2,2) + t1*(-4*t1 + t2) + 
               s2*(3*t1 + 2*t2)) + 
            t1*(Power(t1,4) - 4*Power(s2,3)*t2 - 
               Power(t1,2)*Power(t2,2) + 
               Power(s2,2)*(4*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2)) - 
               s2*(5*Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3))) + 
            s1*(2*Power(s2,4) + 4*Power(s2,3)*t2 - 
               2*Power(t1,2)*(Power(t1,2) + t1*t2 - Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,2) - 22*t1*t2 + 3*Power(t2,2)) + 
               s2*(5*Power(t1,3) + 11*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)))) - 
         s*(Power(s1,4)*(s2 - t1)*
             (3*Power(s2,2) - 2*s2*t1 - Power(t1,2) + 3*s2*t2) - 
            s2*t1*t2*(Power(t1,3)*(2*t1 - t2) + 
               s2*Power(t1,2)*(-4*t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) + Power(t2,2))) + 
            Power(s1,3)*(5*Power(s2,4) - Power(t1,3)*t2 + 
               Power(s2,3)*(-13*t1 + 3*t2) + 
               Power(s2,2)*(11*Power(t1,2) - 11*t1*t2 - 
                  2*Power(t2,2)) - 
               s2*t1*(3*Power(t1,2) - 9*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(5*Power(s2,5) + Power(t1,5) + 
               Power(s2,4)*(-14*t1 + 5*t2) + 
               Power(s2,3)*(13*Power(t1,2) - 15*t1*t2 + Power(t2,2)) - 
               s2*Power(t1,2)*(2*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  t1*Power(t2,2) + Power(t2,3))) + 
            s1*(s2 - t1)*(2*Power(s2,5) - 2*s2*Power(t1,4) + 
               Power(s2,3)*Power(t1 - t2,2) + Power(t1,4)*t2 + 
               Power(s2,4)*(-3*t1 + 2*t2) + 
               Power(s2,2)*(2*Power(t1,3) + 2*t1*Power(t2,2) + 
                  Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*
             (2*Power(s2,2) - s2*t1 - Power(t1,2) + 2*s2*t2) + 
            Power(s1,3)*(4*Power(s2,3) + 4*Power(t1,3) + 
               Power(s2,2)*(-2*t1 + 3*t2) - 
               s2*(6*Power(t1,2) + 5*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(4*Power(s2,4) + Power(s2,3)*(-7*t1 + 6*t2) + 
               3*Power(s2,2)*(Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               Power(t1,2)*(-Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               s2*(Power(t1,3) + 17*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
                  Power(t2,3))) + 
            t1*(Power(t1,3)*t2*(-t1 + t2) + 
               2*s2*Power(t1,2)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,2) + 3*Power(t2,2)) - 
               Power(s2,2)*(4*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  2*Power(t2,3))) + 
            s1*(4*Power(s2,5) + 2*Power(t1,5) + 
               Power(s2,4)*(-9*t1 + 7*t2) + 
               Power(s2,3)*(7*Power(t1,2) - 18*t1*t2 + 5*Power(t2,2)) - 
               s2*t1*(7*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 2*Power(t2,3)))))*
       lg2((s - s1 - s2)/t2))/
     (s*s1*s2*t1*Power(s - s2 + t1,2)*t2*(s - s1 + t2)*(s2 - t1 + t2)) - 
    (8*(-(Power(s1,5)*Power(s2 - t1,2)) - 
         Power(s1,4)*s2*(s2 - t1)*(2*s2 - 3*t1 - t2) - 
         Power(s2,2)*t1*t2*(Power(s2,3) - t1*Power(t2,2) + 
            Power(s2,2)*(-2*t1 + t2) + 
            s2*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
         Power(s1,3)*(-Power(s2,4) + 4*Power(s2,3)*(t1 + t2) + 
            s2*t1*(-3*Power(t1,2) + 4*t1*t2 - 4*Power(t2,2)) + 
            Power(s2,2)*(-Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
            Power(t1,2)*(Power(t1,2) - t1*t2 + 3*Power(t2,2))) + 
         Power(s1,2)*(-Power(s2,5) + 2*Power(s2,4)*t1 + 
            Power(s2,3)*(3*Power(t1,2) - 6*t1*t2 - 4*Power(t2,2)) + 
            Power(t1,2)*t2*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
            s2*t1*(2*Power(t1,3) - 5*Power(t1,2)*t2 + 
               2*t1*Power(t2,2) + Power(t2,3)) - 
            Power(s2,2)*(6*Power(t1,3) - 11*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) + Power(t2,3))) + 
         s1*s2*(Power(s2,4)*(-t1 + t2) + 
            Power(s2,3)*(3*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
            t1*Power(t2,2)*(2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            s2*t1*(Power(t1,3) - Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(s2,2)*(-3*Power(t1,3) + 6*Power(t1,2)*t2 - 
               3*t1*Power(t2,2) + 2*Power(t2,3))) + 
         Power(s,4)*(t1*(t1 - t2)*(s2 - t1 + t2) + 
            s1*(t1*(-t1 + t2) + s2*(t1 + t2))) + 
         Power(s,3)*(Power(s1,2)*
             (Power(s2,2) + Power(-2*t1 + t2,2) - 2*s2*(2*t1 + t2)) - 
            s1*(-3*Power(t1,3) - 2*s2*t1*t2 + 6*Power(t1,2)*t2 - 
               4*t1*Power(t2,2) + Power(t2,3) + 
               2*Power(s2,2)*(t1 + 2*t2)) + 
            t1*(4*s2*Power(t1 - t2,2) - Power(t1 - t2,3) + 
               Power(s2,2)*(-3*t1 + 4*t2))) + 
         Power(s,2)*(Power(s1,3)*
             (-2*Power(s2,2) - 6*Power(t1,2) + 5*t1*t2 - Power(t2,2) + 
               s2*(7*t1 + t2)) + 
            s1*(3*Power(t1,4) - 7*Power(t1,3)*t2 + 
               5*Power(t1,2)*Power(t2,2) - Power(t2,4) + 
               2*Power(s2,3)*(t1 + 2*t2) + 
               s2*t1*(-11*Power(t1,2) + 22*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,2)*(6*Power(t1,2) - 8*t1*t2 - 4*Power(t2,2))) + 
            Power(s1,2)*(-Power(s2,3) - 3*Power(t1,3) + 
               6*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 2*Power(t2,3) + 
               Power(s2,2)*(9*t1 + 10*t2) + 
               s2*(-6*Power(t1,2) + t1*t2 + Power(t2,2))) + 
            t1*(3*Power(s2,3)*(t1 - 2*t2) + t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(-5*Power(t1,2) + 12*t1*t2 - 6*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 6*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  3*Power(t2,3)))) + 
         s*(2*Power(s1,4)*(Power(s2,2) - 3*s2*t1 + t1*(2*t1 - t2)) + 
            Power(s1,3)*(2*Power(s2,3) - Power(s2,2)*(12*t1 + 7*t2) + 
               s2*(8*Power(t1,2) - t1*t2 - Power(t2,2)) + 
               t1*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            s2*t1*(-(Power(s2,3)*(t1 - 4*t2)) + 
               2*t1*(t1 - t2)*Power(t2,2) + 
               2*Power(s2,2)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
               s2*(Power(t1,3) - 4*Power(t1,2)*t2 + 5*t1*Power(t2,2) - 
                  3*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4) - 7*Power(s2,3)*(t1 + t2) - 
               2*Power(s2,2)*(Power(t1,2) - 5*t1*t2 - 2*Power(t2,2)) + 
               2*s2*t1*(5*Power(t1,2) - 9*t1*t2 + 6*Power(t2,2)) + 
               t1*(-3*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(-2*Power(s2,4)*t2 + 
               Power(s2,3)*(-7*Power(t1,2) + 10*t1*t2 + 3*Power(t2,2)) + 
               t1*t2*(-2*Power(t1,3) + Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*(11*Power(t1,3) - 20*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - Power(t2,3)) + 
               s2*(-4*Power(t1,4) + 11*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + Power(t2,4)\
))))*lg2(-(t1/((-s2 + t1 - t2)*t2))))/
     (s1*Power(s2,2)*t1*(s - s2 + t1)*(-s + s1 - t2)*(s1 + t1 - t2)*t2) - 
    (8*(Power(s1,7)*t1*(-2*Power(s2,3) + 6*Power(s2,2)*t1 - 
            5*s2*Power(t1,2) + Power(t1,3)) - 
         Power(s2,3)*Power(t1,2)*Power(t2,3)*
          (Power(s2,3) - 3*s2*Power(t1,2) - Power(s2,2)*t2 + 
            Power(t1,2)*(2*t1 + t2)) + 
         Power(s1,6)*s2*(Power(s2,4) - Power(t1,3)*(t1 - 5*t2) - 
            4*Power(s2,3)*(2*t1 + t2) + Power(s2,2)*t1*(15*t1 + 4*t2) - 
            s2*Power(t1,2)*(7*t1 + 6*t2)) - 
         Power(s,6)*(t1 - t2)*
          (s2*Power(t1,2)*t2 + 
            Power(s1,2)*(-(s2*t1) + Power(t1,2) + s2*t2) + 
            s1*t1*(-(s2*t1) + Power(t1,2) + s2*t2)) + 
         s1*Power(s2,2)*t1*Power(t2,2)*
          (Power(t1,4)*(2*t1 - 3*t2) + Power(s2,4)*(3*t1 - t2) + 
            3*s2*Power(t1,2)*(2*t1 - t2)*t2 + 
            Power(s2,3)*(-3*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*t1*(-2*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) - 
         Power(s1,5)*(Power(t1,6) + 7*Power(s2,5)*t2 + 
            3*Power(t1,4)*Power(t2,2) + 
            Power(s2,4)*(3*Power(t1,2) - 13*t1*t2 - 10*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*
             (9*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) - 
            s2*Power(t1,3)*(2*Power(t1,2) + 7*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,3)*t1*(-11*Power(t1,2) + 21*t1*t2 + 6*Power(t2,2))) \
+ Power(s1,2)*s2*t2*(Power(t1,4)*(t1 - 2*t2)*Power(t2,2) + 
            Power(s2,4)*t2*(10*Power(t1,2) - 6*t1*t2 + Power(t2,2)) - 
            Power(s2,5)*(2*Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*
             (-4*Power(t1,3) + 5*Power(t1,2)*t2 + 16*t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            s2*Power(t1,3)*(Power(t1,3) + Power(t1,2)*t2 - 
               4*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*t1*(5*Power(t1,3) - 21*Power(t1,2)*t2 - 
               5*t1*Power(t2,2) + 2*Power(t2,3))) - 
         Power(s1,4)*(Power(s2,6)*t2 + 
            2*Power(t1,4)*(t1 - t2)*Power(t2,2) + 
            Power(s2,5)*(-3*Power(t1,2) + 8*t1*t2 - 12*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*t2*
             (-12*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
            s2*Power(t1,3)*(2*Power(t1,3) - 3*Power(t1,2)*t2 + 
               5*t1*Power(t2,2) + 3*Power(t2,3)) - 
            Power(s2,3)*t1*(4*Power(t1,3) - 20*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) + 6*Power(t2,3)) + 
            Power(s2,4)*(5*Power(t1,3) - 15*Power(t1,2)*t2 + 
               5*t1*Power(t2,2) + 8*Power(t2,3))) + 
         Power(s1,3)*(2*Power(s2,6)*t2*(-t1 + t2) + 
            Power(t1,5)*Power(t2,2)*(t1 + 2*t2) + 
            Power(s2,5)*(2*Power(t1,3) - 9*Power(t1,2)*t2 + 
               13*t1*Power(t2,2) - 7*Power(t2,3)) + 
            s2*Power(t1,3)*t2*
             (2*Power(t1,3) - 8*Power(t1,2)*t2 - t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s2,3)*t1*(4*Power(t1,4) - 12*Power(t1,3)*t2 - 
               2*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
               2*Power(t2,4)) - 
            2*Power(s2,4)*(2*Power(t1,4) - 10*Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - Power(t2,4)) \
+ 2*Power(s2,2)*Power(t1,2)*(-Power(t1,4) + Power(t1,3)*t2 + 
               2*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + Power(t2,4)\
)) + Power(s,5)*(Power(s1,3)*
             (2*Power(s2,2)*t2 + 
               s2*(-7*Power(t1,2) + 8*t1*t2 - 4*Power(t2,2)) + 
               t1*(5*Power(t1,2) - 5*t1*t2 + Power(t2,2))) - 
            s2*Power(t1,2)*(2*(t1 - t2)*Power(t2,2) + 
               s2*(Power(t1,2) - 5*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,2)*(-5*Power(s2,2)*Power(t1 - t2,2) + 
               Power(t1,2)*(3*Power(t1,2) - 3*t1*t2 + Power(t2,2)) - 
               s2*(Power(t1,3) - 3*Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            s1*t1*(2*Power(t1,3)*(-t1 + t2) + 
               Power(s2,2)*(-4*Power(t1,2) + 10*t1*t2 - 5*Power(t2,2)) + 
               2*s2*(3*Power(t1,3) - 4*t1*Power(t2,2) + Power(t2,3)))) + 
         Power(s,4)*(Power(s1,4)*
             (Power(s2,3) - 2*Power(s2,2)*(2*t1 + 3*t2) + 
               t1*(-10*Power(t1,2) + 9*t1*t2 - 2*Power(t2,2)) + 
               s2*(19*Power(t1,2) - 14*t1*t2 + 5*Power(t2,2))) + 
            s2*Power(t1,2)*(Power(t1 - t2,2)*t2*(t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) - 9*t1*t2 + 10*Power(t2,2)) + 
               s2*(-2*Power(t1,3) + Power(t1,2)*t2 + 8*t1*Power(t2,2) - 
                  9*Power(t2,3))) + 
            Power(s1,3)*(Power(s2,3)*(2*t1 - 7*t2) + 
               Power(s2,2)*(17*Power(t1,2) - 27*t1*t2 + 
                  22*Power(t2,2)) + 
               s2*(-8*Power(t1,3) + 17*t1*Power(t2,2) - 
                  6*Power(t2,3)) + 
               t1*(-Power(t1,3) + Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  2*Power(t2,3))) + 
            s1*t1*(-(Power(t1,2)*Power(t1 - t2,2)*(t1 + t2)) + 
               5*Power(s2,3)*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(14*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  35*t1*Power(t2,2) + 9*Power(t2,3)) + 
               s2*(7*Power(t1,4) - 2*Power(t1,3)*t2 + 
                  2*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,2)*(10*Power(s2,3)*Power(t1 - t2,2) + 
               Power(t1,2)*(8*Power(t1,3) - 7*Power(t1,2)*t2 + 
                  Power(t2,3)) - 
               Power(s2,2)*(Power(t1,3) + 20*Power(t1,2)*t2 - 
                  35*t1*Power(t2,2) + 9*Power(t2,3)) + 
               s2*(-20*Power(t1,4) + 14*Power(t1,2)*Power(t2,2) - 
                  8*t1*Power(t2,3) + Power(t2,4)))) + 
         s*(Power(s1,7)*t1*(Power(s2,2) - 4*s2*t1 + Power(t1,2)) + 
            Power(s1,6)*(-2*Power(s2,4) + s2*Power(t1,2)*(20*t1 - t2) + 
               Power(t1,3)*(-5*t1 + 2*t2) + 
               2*Power(s2,3)*(8*t1 + 3*t2) + 
               Power(s2,2)*(-34*Power(t1,2) + 4*t1*t2)) + 
            Power(s2,2)*Power(t1,2)*Power(t2,2)*
             (-Power(s2,4) - 2*Power(s2,3)*(t1 - 3*t2) - 
               8*s2*Power(t1,2)*t2 + 
               Power(s2,2)*(5*Power(t1,2) + t1*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-2*Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
            Power(s1,5)*(-2*Power(s2,5) + 
               2*Power(s2,4)*(8*t1 + 11*t2) + 
               Power(t1,3)*(-2*Power(t1,2) + t1*t2 - 5*Power(t2,2)) + 
               Power(s2,2)*t1*
                (7*Power(t1,2) + 33*t1*t2 - 4*Power(t2,2)) + 
               s2*Power(t1,2)*
                (5*Power(t1,2) - 11*t1*t2 + 13*Power(t2,2)) - 
               Power(s2,3)*(29*Power(t1,2) + 16*t1*t2 + 20*Power(t2,2))) \
+ s1*Power(s2,2)*t1*t2*(Power(s2,4)*(2*t1 - t2) + 
               Power(s2,3)*(-20*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) - 4*Power(t2,3)) + 
               2*s2*t1*(Power(t1,3) + 8*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,2)*(2*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,4)*(-2*Power(s2,5)*(t1 - 6*t2) + 
               Power(s2,4)*(2*Power(t1,2) + 8*t1*t2 - 43*Power(t2,2)) + 
               Power(t1,4)*(4*Power(t1,2) - t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*t1*
                (17*Power(t1,3) + 14*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 6*Power(t2,3)) - 
               s2*Power(t1,2)*
                (13*Power(t1,3) + 17*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,3)*(-11*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,3)*(Power(s2,6)*(-t1 + t2) + 
               Power(s2,5)*(-5*Power(t1,2) + 16*t1*t2 - 
                  16*Power(t2,2)) + 
               Power(t1,3)*Power(t2,2)*
                (6*Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
               Power(s2,4)*(8*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  46*t1*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,3)*(3*Power(t1,4) - 17*Power(t1,3)*t2 + 
                  15*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s2*Power(t1,2)*
                (6*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-12*Power(t1,4) + Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  5*Power(t2,4))) - 
            Power(s1,2)*(Power(s2,6)*Power(t1 - t2,2) + 
               Power(t1,4)*Power(t2,2)*
                (2*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(s2,5)*(Power(t1,3) - 15*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*Power(t1,3)*t2*
                (4*Power(t1,3) - 15*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (-3*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  12*Power(t1,2)*Power(t2,2) + 28*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,4)*(-6*Power(t1,4) + 19*Power(t1,3)*t2 + 
                  39*Power(t1,2)*Power(t2,2) - 26*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,3)*t1*
                (7*Power(t1,4) + 8*Power(t1,3)*t2 - 
                  58*Power(t1,2)*Power(t2,2) - 17*t1*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         Power(s,2)*(Power(s1,6)*
             (Power(s2,3) + 4*s2*t1*(4*t1 - t2) - 
               2*Power(s2,2)*(3*t1 + t2) + Power(t1,2)*(-5*t1 + 2*t2)) + 
            Power(s1,5)*(4*Power(s2,4) - Power(s2,3)*(26*t1 + 21*t2) + 
               Power(t1,2)*(9*Power(t1,2) - 6*t1*t2 - Power(t2,2)) + 
               2*s2*t1*(-16*Power(t1,2) + t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(57*Power(t1,2) - 13*t1*t2 + 12*Power(t2,2))) \
+ s2*Power(t1,2)*t2*(Power(t1,2)*(t1 - t2)*Power(t2,2) + 
               Power(s2,4)*(-2*t1 + 5*t2) + 
               Power(s2,3)*(Power(t1,2) + 8*t1*t2 - 14*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-3*Power(t1,2) - 2*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,3) - 11*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,4)*(Power(s2,5) - Power(s2,4)*(4*t1 + 27*t2) + 
               Power(s2,3)*(18*Power(t1,2) - 12*t1*t2 + 
                  55*Power(t2,2)) + 
               Power(t1,2)*(8*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*t1*(-15*Power(t1,3) + 4*Power(t1,2)*t2 - 
                  24*t1*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,2)*(2*Power(t1,3) + 31*Power(t1,2)*t2 - 
                  27*t1*Power(t2,2) + 16*Power(t2,3))) + 
            s1*t1*(Power(t1,4)*(t1 - t2)*Power(t2,2) + 
               5*Power(s2,5)*t2*(-2*t1 + t2) + 
               s2*Power(t1,2)*t2*
                (2*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*t1*t2*
                (-9*Power(t1,3) - 17*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) + 6*Power(t2,3)) - 
               Power(s2,4)*(Power(t1,3) + 4*Power(t1,2)*t2 - 
                  50*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,3)*(Power(t1,4) + 26*Power(t1,3)*t2 - 
                  13*Power(t1,2)*Power(t2,2) - 42*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,2)*(5*Power(s2,5)*Power(t1 - t2,2) + 
               Power(t1,3)*Power(t2,2)*
                (-6*Power(t1,2) + t1*t2 + 3*Power(t2,2)) - 
               Power(s2,4)*(2*Power(t1,3) + 35*Power(t1,2)*t2 - 
                  50*t1*Power(t2,2) + 14*Power(t2,3)) + 
               s2*Power(t1,2)*
                (-6*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               2*Power(s2,2)*t1*
                (11*Power(t1,4) + 3*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,3)*(-19*Power(t1,4) + 36*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) - 42*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,5)*(4*t1 - 5*t2) + 
               Power(s2,4)*(9*Power(t1,2) - 37*t1*t2 + 40*Power(t2,2)) - 
               Power(s2,3)*(11*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  72*t1*Power(t2,2) + 39*Power(t2,3)) - 
               Power(s2,2)*(23*Power(t1,4) + 5*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(t1,2)*(-6*Power(t1,4) + 3*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(27*Power(t1,5) + 11*Power(t1,4)*t2 - 
                  8*Power(t1,3)*Power(t2,2) - 4*t1*Power(t2,4)))) + 
         Power(s,3)*(Power(s1,5)*
             (-2*Power(s2,3) + Power(s2,2)*(9*t1 + 6*t2) + 
               s2*(-25*Power(t1,2) + 12*t1*t2 - 2*Power(t2,2)) + 
               t1*(10*Power(t1,2) - 7*t1*t2 + Power(t2,2))) + 
            Power(s1,4)*(-2*Power(s2,4) + 2*Power(s2,3)*(5*t1 + 11*t2) + 
               Power(s2,2)*(-41*Power(t1,2) + 26*t1*t2 - 
                  29*Power(t2,2)) + 
               t1*(-6*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 2*Power(t2,3)) + 
               s2*(25*Power(t1,3) - 2*Power(t1,2)*t2 - 
                  16*t1*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(-10*Power(s2,4)*Power(t1 - t2,2) + 
               Power(t1,2)*Power(t1 - t2,2)*
                (4*Power(t1,2) + 5*t1*t2 + Power(t2,2)) + 
               2*Power(s2,3)*
                (3*Power(t1,3) + 19*Power(t1,2)*t2 - 
                  30*t1*Power(t2,2) + 8*Power(t2,3)) + 
               s2*t1*(-23*Power(t1,4) + Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               2*Power(s2,2)*(15*Power(t1,4) - 7*Power(t1,3)*t2 - 
                  25*Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s1,3)*(Power(s2,4)*(-5*t1 + 9*t2) - 
               2*Power(s2,3)*
                (7*Power(t1,2) - 21*t1*t2 + 22*Power(t2,2)) + 
               Power(s2,2)*(7*Power(t1,3) + 14*Power(t1,2)*t2 - 
                  57*t1*Power(t2,2) + 25*Power(t2,3)) + 
               s2*(25*Power(t1,4) + 3*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               t1*(-12*Power(t1,4) + 9*Power(t1,3)*t2 - 
                  5*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  Power(t2,4))) - 
            s2*Power(t1,2)*(2*Power(t1,2)*Power(t2,2)*(-t1 + t2) + 
               Power(s2,3)*(Power(t1,2) - 7*t1*t2 + 10*Power(t2,2)) - 
               2*Power(s2,2)*
                (Power(t1,3) - Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  8*Power(t2,3)) + 
               s2*(Power(t1,4) + 5*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            2*s1*t1*(Power(t1,3)*(t1 - t2)*Power(t2,2) - 
               Power(s2,4)*(Power(t1,2) - 10*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(5*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  30*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,4) - 10*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*(Power(t1,5) - Power(t1,4)*t2 + 
                  Power(t1,3)*Power(t2,2) - t1*Power(t2,4)))))*lg2(-t2))/
     (Power(s1,2)*Power(s2,2)*Power(t1,2)*(s1 + t1)*Power(s - s2 + t1,2)*t2*
       Power(s - s1 + t2,2)) + 
    (8*(-2*Power(s,6)*t1*(t1 - t2)*t2*(s1*(s2 + t1) + s2*t2) + 
         2*Power(s1,6)*s2*(s2 - t1)*t1*
          (-(t1*(t1 + t2)) + s2*(t1 + 2*t2)) - 
         Power(s2,3)*Power(s2 - t1,2)*t1*Power(t2,3)*
          (-(t1*(2*t1 + t2)) + s2*(3*t1 + 2*t2)) + 
         s1*Power(s2,2)*(s2 - t1)*Power(t2,2)*
          (Power(t1,4)*(2*t1 - 3*t2) + Power(s2,4)*(t1 + t2) - 
            2*Power(s2,3)*t1*(t1 + t2) + 
            Power(s2,2)*t1*(3*Power(t1,2) - 7*t1*t2 - 4*Power(t2,2)) + 
            s2*Power(t1,2)*(-4*Power(t1,2) + 10*t1*t2 + 3*Power(t2,2))) + 
         Power(s1,5)*(2*Power(s2,5)*t1 - Power(t1,4)*t2*(2*t1 + 3*t2) + 
            Power(s2,4)*(-Power(t1,2) + 6*t1*t2 + Power(t2,2)) - 
            2*s2*Power(t1,3)*(Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)) - 
            Power(s2,3)*t1*(6*Power(t1,2) + 15*t1*t2 + 8*Power(t2,2)) + 
            Power(s2,2)*Power(t1,2)*
             (7*Power(t1,2) + 15*t1*t2 + 16*Power(t2,2))) + 
         Power(s1,2)*s2*t2*(-(Power(t1,4)*(t1 - 2*t2)*Power(t2,2)) - 
            Power(s2,6)*(4*t1 + t2) + 
            Power(s2,5)*(18*Power(t1,2) + 6*t1*t2 + Power(t2,2)) - 
            Power(s2,4)*t1*(34*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,3)*t1*(35*Power(t1,3) - 18*Power(t1,2)*t2 + 
               t1*Power(t2,2) - 7*Power(t2,3)) + 
            s2*Power(t1,3)*(5*Power(t1,3) - 8*Power(t1,2)*t2 + 
               t1*Power(t2,2) - 7*Power(t2,3)) + 
            Power(s2,2)*Power(t1,2)*
             (-20*Power(t1,3) + 23*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
               11*Power(t2,3))) + 
         Power(s1,4)*(3*Power(s2,6)*t1 + 
            2*Power(t1,4)*t2*Power(t1 + t2,2) + 
            Power(s2,5)*(-8*Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
            Power(s2,2)*Power(t1,2)*t2*
             (12*Power(t1,2) + 23*t1*t2 + 11*Power(t2,2)) + 
            Power(s2,4)*(7*Power(t1,3) - 2*Power(t1,2)*t2 - 
               8*t1*Power(t2,2) - Power(t2,3)) + 
            Power(s2,3)*t1*(-2*Power(t1,3) + 16*Power(t1,2)*t2 + 
               29*t1*Power(t2,2) + 7*Power(t2,3)) + 
            s2*(-2*Power(t1,5)*t2 + 3*Power(t1,3)*Power(t2,3))) + 
         Power(s1,3)*(Power(s2,7)*t1 - 
            Power(t1,5)*Power(t2,2)*(t1 + 2*t2) - 
            Power(s2,6)*(4*Power(t1,2) + 9*t1*t2 + Power(t2,2)) + 
            s2*Power(t1,3)*t2*
             (2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s2,5)*(7*Power(t1,3) + 28*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - Power(t2,3)) + 
            Power(s2,4)*t1*(-7*Power(t1,3) - 31*Power(t1,2)*t2 + 
               14*t1*Power(t2,2) + 6*Power(t2,3)) + 
            Power(s2,3)*t1*(4*Power(t1,4) + 17*Power(t1,3)*t2 - 
               34*Power(t1,2)*Power(t2,2) - 15*t1*Power(t2,3) - 
               4*Power(t2,4)) + 
            Power(s2,2)*Power(t1,2)*
             (-Power(t1,4) - 7*Power(t1,3)*t2 + 
               18*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) + 
               4*Power(t2,4))) + 
         Power(s,5)*(2*Power(s1,2)*t1*
             (Power(s2,2)*(t1 + 2*t2) + 
               s2*(Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) - 
               t2*(-4*Power(t1,2) + 2*t1*t2 + Power(t2,2))) + 
            s2*t1*t2*(2*s2*(Power(t1,2) + 3*t1*t2 - 5*Power(t2,2)) + 
               t2*(-5*Power(t1,2) + 3*t1*t2 + 2*Power(t2,2))) + 
            s1*(2*s2*t1*t2*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*t2*(-6*Power(t1,2) + 5*t1*t2 + Power(t2,2)) - 
               Power(s2,2)*(Power(t1,3) - 9*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + Power(t2,3)))) + 
         Power(s,4)*(-2*Power(s1,3)*t1*
             (-Power(s2,3) + 6*Power(t1,2)*t2 - 2*Power(t2,3) + 
               2*Power(s2,2)*(t1 + 2*t2) + 
               s2*(4*Power(t1,2) + 10*t1*t2 + Power(t2,2))) + 
            s1*(3*Power(t1,3)*t2*
                (-2*Power(t1,2) + t1*t2 + Power(t2,2)) - 
               Power(s2,2)*t1*
                (3*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) - 13*Power(t2,3)) + 
               s2*t1*t2*(12*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(s2,3)*(3*Power(t1,3) - 11*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) + 5*Power(t2,3))) - 
            s2*t1*t2*(Power(s2,2)*
                (6*Power(t1,2) + 4*t1*t2 - 20*Power(t2,2)) + 
               t1*t2*(4*Power(t1,2) + t1*t2 - 5*Power(t2,2)) + 
               s2*(-5*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s1,2)*(-(Power(s2,3)*
                  (8*Power(t1,2) + 17*t1*t2 + Power(t2,2))) + 
               t1*t2*(22*Power(t1,3) - 6*Power(t1,2)*t2 - 
                  8*t1*Power(t2,2) - Power(t2,3)) + 
               Power(s2,2)*(2*Power(t1,3) - 27*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t1*(4*Power(t1,3) + 2*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 14*Power(t2,3)))) + 
         Power(s,3)*(2*Power(s1,4)*t1*
             (-2*Power(s2,3) + Power(s2,2)*t2 + 
               t2*(4*Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
               2*s2*(3*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) + 
            s2*t1*t2*(Power(s2,3)*
                (6*Power(t1,2) - 4*t1*t2 - 20*Power(t2,2)) - 
               Power(t1,2)*t2*(Power(t1,2) + 3*t1*t2 - 4*Power(t2,2)) + 
               s2*t1*(4*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) - 20*Power(t2,3)) + 
               Power(s2,2)*(-10*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,3)*(-7*Power(s2,4)*t1 + 
               Power(s2,3)*(19*Power(t1,2) + 27*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(8*Power(t1,3) + 47*Power(t1,2)*t2 - 
                  2*t1*Power(t2,2) + Power(t2,3)) + 
               t1*t2*(-30*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + Power(t2,3)) - 
               s2*t1*(14*Power(t1,3) + 20*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 21*Power(t2,3))) - 
            s1*(Power(t1,4)*t2*
                (2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) - 
               Power(s2,3)*t1*
                (6*Power(t1,3) + 26*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - 12*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (-10*Power(t1,3) + 3*Power(t1,2)*t2 + 7*Power(t2,3)) + 
               Power(s2,4)*(3*Power(t1,3) + Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,2)*t1*
                (3*Power(t1,4) + 32*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) - 
                  16*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,4)*
                (12*Power(t1,2) + 31*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*t1*
                (4*Power(t1,3) - 17*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
                  25*Power(t2,3)) + 
               Power(s2,3)*(-17*Power(t1,3) + Power(t1,2)*t2 - 
                  54*t1*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,2)*t2*
                (20*Power(t1,3) + 2*Power(t1,2)*t2 - 9*t1*Power(t2,2) - 
                  4*Power(t2,3)) + 
               s2*t1*(2*Power(t1,4) - 26*Power(t1,3)*t2 + 
                  12*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) + 
                  10*Power(t2,4)))) + 
         Power(s,2)*(-2*Power(s1,5)*t1*
             (-Power(s2,3) - 2*Power(s2,2)*(t1 + t2) + 
               t1*t2*(t1 + t2) + 
               s2*(4*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,4)*(11*Power(s2,4)*t1 + 
               2*Power(t1,2)*t2*
                (9*Power(t1,2) + 7*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,3)*(-10*Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
               Power(s2,2)*(22*Power(t1,3) + 53*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t1*(18*Power(t1,3) + 28*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) + 9*Power(t2,3))) - 
            s2*t1*t2*(Power(t1,3)*(t1 - t2)*Power(t2,2) + 
               2*Power(s2,4)*(Power(t1,2) - 3*t1*t2 - 5*Power(t2,2)) + 
               Power(s2,2)*t1*
                (4*Power(t1,3) + 12*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) - 30*Power(t2,3)) + 
               s2*Power(t1,2)*
                (-Power(t1,3) - 7*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(s2,3)*(-5*Power(t1,3) + Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,2)*(-(Power(s2,5)*
                  (8*Power(t1,2) + 31*t1*t2 + 6*Power(t2,2))) - 
               Power(s2,3)*t1*
                (18*Power(t1,3) + 45*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(t1,3)*t2*
                (6*Power(t1,3) + 6*Power(t1,2)*t2 - 2*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,4)*(20*Power(t1,3) + 57*Power(t1,2)*t2 + 
                  58*t1*Power(t2,2) + 6*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (-22*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) + 19*Power(t2,3)) + 
               Power(s2,2)*t1*
                (6*Power(t1,4) + 35*Power(t1,3)*t2 - 
                  50*Power(t1,2)*Power(t2,2) - 17*t1*Power(t2,3) - 
                  24*Power(t2,4))) + 
            Power(s1,3)*(9*Power(s2,5)*t1 - 
               3*Power(s2,4)*
                (10*Power(t1,2) + 13*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(18*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,2)*t2*
                (-24*Power(t1,3) - 16*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*t1*
                (8*Power(t1,3) + 57*Power(t1,2)*t2 + 
                  43*t1*Power(t2,2) + 36*Power(t2,3)) - 
               s2*t1*(6*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  10*Power(t1,2)*Power(t2,2) + 29*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            s1*(Power(t1,5)*Power(t2,2)*(-t1 + t2) + 
               s2*Power(t1,3)*t2*
                (2*Power(t1,3) + t1*Power(t2,2) - 3*Power(t2,3)) - 
               Power(s2,4)*t1*
                (3*Power(t1,3) + 41*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,5)*(Power(t1,3) + 9*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,3)*t1*
                (3*Power(t1,4) + 56*Power(t1,3)*t2 - 
                  20*Power(t1,2)*Power(t2,2) - 22*t1*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (-Power(t1,4) - 26*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) + 
                  21*Power(t2,4)))) + 
         s*(-2*Power(s1,6)*s2*(s2 - t1)*t1*(t1 + t2) - 
            Power(s2,2)*(s2 - t1)*t1*Power(t2,2)*
             (2*Power(s2,3)*(t1 + t2) - Power(s2,2)*t2*(13*t1 + 10*t2) + 
               Power(t1,2)*(2*Power(t1,2) - t1*t2 - 2*Power(t2,2)) + 
               2*s2*t1*(-2*Power(t1,2) + 5*t1*t2 + 5*Power(t2,2))) - 
            Power(s1,5)*(4*Power(s2,4)*t1 + 
               Power(t1,3)*t2*(4*t1 + 5*t2) + 
               Power(s2,3)*(3*Power(t1,2) + 12*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*
                (10*Power(t1,2) + 14*t1*t2 + 7*Power(t2,2)) - 
               Power(s2,2)*t1*(17*Power(t1,2) + 30*t1*t2 + 9*Power(t2,2))\
) + Power(s1,4)*(-10*Power(s2,5)*t1 + 2*Power(t1,4)*t2*(6*t1 + 7*t2) + 
               2*Power(s2,4)*(9*Power(t1,2) + t1*t2 - Power(t2,2)) + 
               2*Power(s2,3)*
                (Power(t1,3) + 17*Power(t1,2)*t2 + 10*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               2*s2*Power(t1,2)*
                (3*Power(t1,3) + 3*Power(t1,2)*t2 + 5*t1*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(s2,2)*t1*
                (16*Power(t1,3) + 54*Power(t1,2)*t2 + 
                  55*t1*Power(t2,2) + 14*Power(t2,3))) + 
            s1*Power(s2,2)*t2*
             (-(Power(s2,4)*(4*Power(t1,2) + 7*t1*t2 + 5*Power(t2,2))) + 
               Power(s2,3)*t1*
                (18*Power(t1,2) + 5*t1*t2 + 8*Power(t2,2)) + 
               s2*Power(t1,2)*
                (22*Power(t1,3) - Power(t1,2)*t2 - 28*t1*Power(t2,2) - 
                  21*Power(t2,3)) - 
               3*Power(t1,3)*
                (2*Power(t1,3) + Power(t1,2)*t2 - 3*t1*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-30*Power(t1,3) + 6*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 16*Power(t2,3))) + 
            Power(s1,3)*(-5*Power(s2,6)*t1 + 
               Power(s2,5)*(19*Power(t1,2) + 29*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*t1*
                (14*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  48*t1*Power(t2,2) - 25*Power(t2,3)) + 
               s2*Power(t1,2)*t2*
                (14*Power(t1,3) + Power(t1,2)*t2 - 11*t1*Power(t2,2) - 
                  7*Power(t2,3)) - 
               Power(s2,4)*(25*Power(t1,3) + 46*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) - 3*Power(t2,3)) - 
               Power(t1,3)*t2*
                (6*Power(t1,3) + 9*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-3*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  57*Power(t1,2)*Power(t2,2) + 33*t1*Power(t2,3) + 
                  9*Power(t2,4))) + 
            Power(s1,2)*(Power(t1,4)*Power(t2,2)*
                (2*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(s2,6)*(2*Power(t1,2) + 17*t1*t2 + 4*Power(t2,2)) - 
               Power(s2,5)*(7*Power(t1,3) + 59*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) + 4*Power(t2,3)) + 
               s2*Power(t1,3)*t2*
                (-4*Power(t1,3) - 5*t1*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,4)*(10*Power(t1,4) + 86*Power(t1,3)*t2 + 
                  7*t1*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (2*Power(t1,4) + 29*Power(t1,3)*t2 - 
                  40*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) - 
                  26*Power(t2,4)) + 
               Power(s2,3)*t1*
                (-7*Power(t1,4) - 69*Power(t1,3)*t2 + 
                  62*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  22*Power(t2,4)))))*lg2(s - s1 + t2))/
     (Power(s1,2)*Power(s2,2)*Power(t1,2)*Power(s - s2 + t1,2)*
       (s - s1 - s2 + t1)*Power(t2,2)*(s - s1 + t2)) - 
    (8*(s1*s2*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*(s1 - t2)*
          (s1*(s2 - t1) - s2*t2) + 
         Power(s,4)*(Power(s1,2)*(t1 - t2) + 
            s1*(2*Power(t1,2) - 5*t1*t2 + Power(t2,2)) + 
            t1*(2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2))) - 
         Power(s,3)*(Power(s1,3)*(s2 + 3*t1 - 2*t2) + 
            Power(s1,2)*(6*Power(t1,2) + s2*(t1 - 5*t2) - 13*t1*t2 + 
               2*Power(t2,2)) + 
            t1*(-(t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
               s2*(2*Power(t1,2) - 9*t1*t2 + 8*Power(t2,2))) + 
            s1*(s2*(2*Power(t1,2) - 15*t1*t2 + 5*Power(t2,2)) + 
               t1*(6*Power(t1,2) - 13*t1*t2 + 12*Power(t2,2)))) + 
         Power(s,2)*(Power(s1,4)*(2*s2 + 3*t1 - t2) + 
            Power(s1,3)*(3*Power(s2,2) + 6*Power(t1,2) + 
               s2*(t1 - 8*t2) - 11*t1*t2 + Power(t2,2)) + 
            t1*t2*(s2*(3*t1 - 5*t2)*t2 + Power(t2,2)*(-t1 + t2) + 
               Power(s2,2)*(-4*t1 + 7*t2)) + 
            Power(s1,2)*(-11*Power(s2,2)*t2 + 
               s2*(4*Power(t1,2) - 20*t1*t2 + 6*Power(t2,2)) + 
               t1*(6*Power(t1,2) - 11*t1*t2 + 15*Power(t2,2))) + 
            s1*(Power(s2,2)*t2*(-16*t1 + 9*t2) + 
               t1*t2*(-2*Power(t1,2) + 7*t1*t2 - 9*Power(t2,2)) + 
               s2*t1*(4*Power(t1,2) - 9*t1*t2 + 16*Power(t2,2)))) - 
         s*(Power(s1,5)*(s2 + t1) + 
            s2*t1*Power(t2,2)*(2*Power(s2,2) - 2*s2*t2 + Power(t2,2)) + 
            Power(s1,4)*(4*Power(s2,2) + t1*(2*t1 - 3*t2) - 
               s2*(t1 + 3*t2)) + 
            s1*t2*(5*Power(s2,2)*t1*t2 + s2*t1*(t1 - 6*t2)*t2 - 
               t1*(t1 - 2*t2)*Power(t2,2) + Power(s2,3)*(-6*t1 + 7*t2)) + 
            Power(s1,3)*(4*Power(s2,3) + 2*s2*Power(t1 - t2,2) - 
               2*Power(s2,2)*(t1 + 5*t2) + 
               t1*(2*Power(t1,2) - 3*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(-11*Power(s2,3)*t2 + 
               6*Power(s2,2)*t2*(-t1 + t2) - 
               t1*t2*(Power(t1,2) - 3*t1*t2 + 6*Power(t2,2)) + 
               2*s2*(Power(t1,3) + 4*t1*Power(t2,2)))))*
       lg2(-((-s + s2 - t1)/(s2*(-s1 - t1 + t2)))))/
     (s*s1*s2*t1*(s - s2 + t1)*t2*Power(s - s1 + t2,2)) + 
    (8*(2*Power(s,4)*t1*Power(t1 - t2,2) + 
         2*Power(s1,4)*s2*(s2 - t1)*(s2 + t2) + 
         Power(s2,2)*t1*t2*(t1*Power(t2,2) + Power(s2,2)*(-t1 + t2) + 
            s2*(Power(t1,2) - t1*t2 - Power(t2,2))) + 
         Power(s1,3)*(2*Power(s2,4) - 4*Power(s2,3)*t2 + 
            Power(s2,2)*(-2*Power(t1,2) + 5*t1*t2 - 6*Power(t2,2)) - 
            Power(t1,2)*(2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
            s2*t1*(2*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) + 
         Power(s1,2)*(Power(s2,4)*(2*t1 - 5*t2) + 
            Power(t1,2)*t2*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
            Power(s2,3)*(-Power(t1,2) - 2*t1*t2 + Power(t2,2)) - 
            s2*t1*(2*Power(t1,3) + 3*Power(t1,2)*t2 - 
               4*t1*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,2)*(Power(t1,3) + 11*Power(t1,2)*t2 - 
               8*t1*Power(t2,2) + 6*Power(t2,3))) + 
         s1*s2*(t1*Power(t2,2)*
             (2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,3)*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*(5*Power(t1,2)*t2 + Power(t2,3)) - 
            s2*(Power(t1,4) - Power(t1,3)*t2 + 
               6*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
               2*Power(t2,4))) + 
         Power(s,3)*(-(t1*(t1 - t2)*
               (4*s2*t1 - 2*Power(t1,2) - 7*s2*t2 + t1*t2 + Power(t2,2))\
) - 2*s1*(3*Power(t1,3) - 3*s2*t1*t2 - 4*Power(t1,2)*t2 + 
               Power(t2,2)*(s2 + t2))) + 
         Power(s,2)*(2*Power(s1,2)*
             (3*Power(t1,3) - 2*Power(t1,2)*t2 - t1*t2*(5*s2 + 2*t2) + 
               t2*(-2*Power(s2,2) - s2*t2 + Power(t2,2))) - 
            s1*(6*Power(t1,4) - 5*Power(t1,3)*t2 + 
               Power(s2,2)*(16*t1 - 7*t2)*t2 - 
               4*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
               Power(t2,4) + 
               s2*(-10*Power(t1,3) + 7*Power(t1,2)*t2 + 
                  2*t1*Power(t2,2) - 6*Power(t2,3))) + 
            t1*(t1*Power(t1 - t2,2)*t2 + 
               Power(s2,2)*(3*Power(t1,2) - 11*t1*t2 + 9*Power(t2,2)) - 
               s2*(2*Power(t1,3) - 6*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  3*Power(t2,3)))) + 
         s*(2*Power(s1,3)*(-Power(s2,3) - Power(t1,3) + t1*Power(t2,2) + 
               Power(s2,2)*(t1 + t2) + s2*t2*(3*t1 + 2*t2)) + 
            s2*t1*(2*t1*(t1 - t2)*Power(t2,2) - 
               Power(s2,2)*(Power(t1,2) - 5*t1*t2 + 5*Power(t2,2)) + 
               s2*(Power(t1,3) - 4*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
                  3*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*(-2*t1 + 9*t2) + 
               2*Power(s2,2)*(Power(t1,2) + 4*t1*t2 + Power(t2,2)) + 
               t1*(6*Power(t1,3) - Power(t1,2)*t2 - 7*t1*Power(t2,2) + 
                  2*Power(t2,3)) - 
               s2*(8*Power(t1,3) + 6*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 
                  7*Power(t2,3))) - 
            s1*(Power(s2,3)*(Power(t1,2) - 15*t1*t2 + 8*Power(t2,2)) + 
               t1*t2*(2*Power(t1,3) - Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,3) + 6*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(-4*Power(t1,4) + 3*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 3*Power(t2,4)\
))))*lg2(-((-s + s2 - t1)/((-s + s1 - t2)*(-s1 - t1 + t2)))))/
     (s1*Power(s2,2)*t1*(s - s2 + t1)*(-s + s1 - t2)*t2*(s2 - t1 + t2)) - 
    (8*(Power(s,5)*Power(t1 - t2,2) + 
         s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*t1*
          (s1*(s2 - t1) - s2*t2) + 
         Power(s,4)*(s1*(-4*Power(t1,2) + 7*t1*t2 + (2*s2 - 3*t2)*t2) + 
            (t1 - t2)*(-2*s2*t1 + 4*s2*t2 + 2*t1*t2 - Power(t2,2))) + 
         Power(s,3)*(Power(s1,2)*
             (Power(s2,2) - 3*s2*t1 + 7*Power(t1,2) - 4*s2*t2 - 
               9*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,2) - 7*t1*t2 + 6*Power(t2,2)) + 
            t1*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
               Power(t2,3)) - 
            s2*(2*Power(t1,3) + 2*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               3*Power(t2,3)) - 
            s1*(-2*Power(t1,3) + 7*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               2*Power(t2,3) + Power(s2,2)*(t1 + 5*t2) + 
               s2*(-3*Power(t1,2) + 17*t1*t2 - 12*Power(t2,2)))) - 
         Power(s,2)*(Power(s2,3)*Power(t1 - 2*t2,2) + 
            Power(t1,3)*t2*(-t1 + t2) + 
            Power(s1,3)*(Power(s2,2) - 5*s2*t1 + 6*Power(t1,2) - 
               2*s2*t2 - 4*t1*t2) + 
            Power(s2,2)*(-2*Power(t1,3) + 3*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            s2*t1*(2*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) - 
               2*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,3) - Power(s2,2)*(7*t1 + 10*t2) + 
               t1*(4*Power(t1,2) - 7*t1*t2 + 4*Power(t2,2)) + 
               2*s2*(3*Power(t1,2) - 6*t1*t2 + 4*Power(t2,2))) + 
            s1*(-2*Power(s2,3)*(t1 + 2*t2) + 
               Power(t1,2)*(2*Power(t1,2) - 3*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,2) - 13*t1*t2 + 15*Power(t2,2)) + 
               s2*(Power(t1,3) + Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  4*Power(t2,3)))) - 
         s*(2*Power(s1,4)*(s2 - t1)*t1 + 
            Power(s1,3)*(-Power(s2,3) + 2*Power(t1,2)*(-t1 + t2) + 
               4*Power(s2,2)*(2*t1 + t2) + s2*t1*(-7*t1 + 2*t2)) + 
            Power(s1,2)*(-(Power(t1,3)*(t1 - 2*t2)) + 
               s2*Power(t1,2)*(-3*t1 + t2) + Power(s2,3)*(4*t1 + 6*t2) + 
               Power(s2,2)*(-2*Power(t1,2) + t1*t2 - 6*Power(t2,2))) + 
            s2*t2*(Power(s2,3)*(t1 - t2) + Power(t1,3)*(2*t1 - t2) + 
               Power(s2,2)*Power(t2,2) + 
               s2*(-2*Power(t1,3) + t1*Power(t2,2))) + 
            s1*(Power(t1,4)*t2 + Power(s2,4)*(t1 + t2) - 
               s2*Power(t1,2)*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               2*Power(s2,3)*(Power(t1,2) - t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(2*Power(t1,3) - 4*Power(t1,2)*t2 - 
                  t1*Power(t2,2) + 2*Power(t2,3)))))*
       lg2(-(t2/(s1*(-s1 - t1 + t2)))))/
     (s*s1*s2*Power(t1,2)*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (8*(-(Power(s1,5)*(Power(s2,2) + t1*t2 + s2*(-t1 + t2))) + 
         Power(s2,2)*Power(t2,2)*
          (-Power(s2,3) + s2*(3*Power(t1,2) - t1*t2 + Power(t2,2)) + 
            t1*(-2*Power(t1,2) + t1*t2 + Power(t2,2))) + 
         Power(s1,4)*(-Power(s2,3) + 2*Power(s2,2)*t2 - 
            t1*(t1 - 2*t2)*t2 + 
            s2*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2))) + 
         s1*s2*t2*(2*Power(s2,4) - Power(s2,3)*(t1 + 3*t2) + 
            Power(s2,2)*(-4*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) + 
            Power(t1,2)*(2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
            s2*(Power(t1,3) + 2*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
               2*Power(t2,3))) - 
         Power(s1,3)*(2*Power(s2,4) - 4*Power(s2,3)*(t1 + t2) + 
            Power(s2,2)*(4*Power(t1,2) + 6*t1*t2 - 3*Power(t2,2)) + 
            t1*t2*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            s2*(-2*Power(t1,3) + 3*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
               3*Power(t2,3))) - 
         Power(s1,2)*(Power(s2,5) - Power(t1,3)*Power(t2,2) - 
            Power(s2,4)*(t1 + 5*t2) + 
            Power(s2,3)*(-Power(t1,2) + 6*t1*t2 + Power(t2,2)) + 
            s2*t2*(-2*Power(t1,3) + 3*Power(t1,2)*t2 + t1*Power(t2,2) - 
               Power(t2,3)) + 
            Power(s2,2)*(Power(t1,3) + 3*Power(t1,2)*t2 - 
               11*t1*Power(t2,2) + 6*Power(t2,3))) + 
         Power(s,4)*((t1 - t2)*t2*(s2 - t1 + t2) + 
            s1*(t2*(-t1 + t2) + s2*(t1 + t2))) + 
         Power(s,3)*(Power(s2,2)*Power(t1 - 2*t2,2) + 
            Power(t1 - t2,3)*t2 - 
            s2*(Power(t1,3) - 4*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            Power(s1,2)*(Power(s2,2) + (4*t1 - 3*t2)*t2 - 
               2*s2*(2*t1 + t2)) + 
            s1*(2*s2*t1*t2 + 4*Power(t1 - t2,2)*t2 - 
               2*Power(s2,2)*(t1 + 2*t2))) + 
         Power(s,2)*(t1*Power(t1 - t2,2)*Power(t2,2) - 
            Power(s2,3)*(Power(t1,2) - 5*t1*t2 + 6*Power(t2,2)) + 
            Power(s2,2)*(2*Power(t1,3) - 5*Power(t1,2)*t2 + 
               6*t1*Power(t2,2) - 3*Power(t2,3)) - 
            s2*(Power(t1,4) - 5*Power(t1,2)*Power(t2,2) + 
               7*t1*Power(t2,3) - 3*Power(t2,4)) + 
            Power(s1,3)*(-Power(s2,2) + 3*t2*(-2*t1 + t2) + 
               2*s2*(2*t1 + t2)) + 
            Power(s1,2)*(-2*Power(s2,3) + Power(s2,2)*(10*t1 + 9*t2) + 
               t2*(-6*Power(t1,2) + 12*t1*t2 - 5*Power(t2,2)) + 
               s2*(-4*Power(t1,2) - 8*t1*t2 + 6*Power(t2,2))) + 
            s1*(Power(s2,3)*(t1 + 7*t2) + 
               s2*t2*(-8*Power(t1,2) + 22*t1*t2 - 11*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,2) + t1*t2 - 6*Power(t2,2)) + 
               t2*(-3*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 2*Power(t2,3)))) + 
         s*(Power(s1,4)*(Power(s2,2) - 2*s2*t1 + (4*t1 - t2)*t2) + 
            Power(s1,3)*(2*Power(s2,3) - 7*Power(s2,2)*(t1 + t2) + 
               s2*(3*Power(t1,2) + 10*t1*t2 - 7*Power(t2,2)) + 
               2*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
            s2*t2*(-2*Power(s2,3)*(t1 - 2*t2) + 
               Power(s2,2)*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               s2*(2*Power(t1,3) - 5*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - 3*Power(t2,3)) + 
               t1*(-2*Power(t1,3) + 3*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            Power(s1,2)*(2*Power(s2,4) - Power(s2,3)*(7*t1 + 12*t2) + 
               2*Power(s2,2)*(2*Power(t1,2) + 5*t1*t2 - Power(t2,2)) - 
               s2*(Power(t1,3) - 5*Power(t1,2)*t2 + 20*t1*Power(t2,2) - 
                  11*Power(t2,3)) - 
               t2*(-3*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3))) - 
            s1*(6*Power(s2,4)*t2 + 2*Power(t1,2)*(t1 - t2)*Power(t2,2) + 
               Power(s2,3)*(Power(t1,2) + t1*t2 - 8*Power(t2,2)) - 
               2*Power(s2,2)*t2*
                (6*Power(t1,2) - 9*t1*t2 + 5*Power(t2,2)) + 
               s2*(-Power(t1,4) + 3*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
                  4*Power(t2,4)))))*lg2(-(t2/(t1*(-s1 - t1 + t2)))))/
     (Power(s1,2)*s2*(-s + s2 - t1)*t1*t2*(s - s1 + t2)*(s2 - t1 + t2)) - 
    (4*(Power(s,6)*t1*t2*(2*s2 - 3*t1 + 3*t2) + 
         s1*s2*(Power(s1,2) - Power(s2,2))*(s2 - t1)*t2*(s2 - t1 + t2)*
          (s1*(s2 - t1) - s2*t2) - 
         Power(s,5)*t1*(t2*(4*Power(s2,2) - 7*s2*t1 + 3*Power(t1,2) + 
               4*s2*t2 + 2*t1*t2 - 5*Power(t2,2)) + 
            s1*(-(s2*t1) + Power(t1,2) + 6*s2*t2 - 10*t1*t2 + 
               10*Power(t2,2))) + 
         Power(s,4)*(Power(s1,2)*t1*
             (Power(s2,2) - 4*s2*t1 + 3*Power(t1,2) + 8*s2*t2 - 
               13*t1*t2 + 13*Power(t2,2)) + 
            t1*t2*(2*Power(s2,3) - 5*Power(s2,2)*t1 - Power(t1,3) - 
               4*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 3*Power(t2,3) + 
               s2*(5*Power(t1,2) + 4*t1*t2 - 9*Power(t2,2))) + 
            s1*(Power(s2,2)*(-Power(t1,2) + 4*t1*t2 + Power(t2,2)) + 
               s2*(2*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + Power(t2,3)) - 
               t1*(Power(t1,3) - 9*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 
                  15*Power(t2,3)))) - 
         Power(s,3)*(Power(s1,3)*
             (2*Power(s2,2)*(t1 + t2) + 
               s2*(-5*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2)) + 
               t1*(3*Power(t1,2) - 8*t1*t2 + 8*Power(t2,2))) + 
            s1*(2*Power(s2,4)*t2 + 
               s2*Power(t2,2)*
                (16*Power(t1,2) - 18*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 12*t1*t2 + 8*Power(t2,2)) - 
               t1*t2*(Power(t1,3) + 12*Power(t1,2)*t2 - 
                  9*t1*Power(t2,2) - 9*Power(t2,3)) - 
               Power(s2,2)*(Power(t1,3) - 13*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) - 7*Power(t2,3))) + 
            t1*t2*(-(Power(s2,3)*t1) + Power(t1,4) + 
               2*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) - 
               Power(t2,4) + 
               Power(s2,2)*(5*Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
               s2*(-5*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*(t1 - t2) + 
               Power(s2,2)*(-5*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               s2*(7*Power(t1,3) - 9*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 2*Power(t2,3)) - 
               t1*(3*Power(t1,3) - 11*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  16*Power(t2,3)))) - 
         s*(Power(s1,4)*(s2 - t1)*
             (Power(s2,2)*(t1 + 2*t2) - 
               2*s2*(Power(t1,2) - Power(t2,2)) + 
               t1*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,3)*t2*(Power(s2,4) + 
               Power(s2,2)*t1*(18*t1 - 13*t2) + 
               Power(s2,3)*(-10*t1 + t2) - 
               2*s2*t1*(5*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 6*Power(t2,2))) - 
            s2*t1*Power(t2,2)*
             (2*Power(t1,4) + Power(s2,3)*t2 - Power(t1,3)*t2 - 
               t1*Power(t2,3) + 
               Power(s2,2)*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               s2*(-4*Power(t1,3) + Power(t1,2)*t2 + Power(t2,3))) + 
            s1*t2*(2*Power(s2,6) + Power(s2,5)*(-6*t1 + 8*t2) + 
               Power(s2,3)*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               Power(s2,4)*(6*Power(t1,2) - 12*t1*t2 + 7*Power(t2,2)) - 
               Power(t1,2)*t2*
                (Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-4*Power(t1,3) + 2*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3)) + 
               s2*t1*(2*Power(t1,4) + Power(t1,3)*t2 - t1*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,5)*(-t1 + t2) + 
               Power(s2,4)*(3*Power(t1,2) - 13*t1*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*t2*
                (Power(t1,3) + 3*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*t1*t2*(-Power(t1,3) + Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,3)*(-3*Power(t1,3) + 24*Power(t1,2)*t2 - 
                  21*t1*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(Power(t1,4) - 12*Power(t1,3)*t2 + 
                  13*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) + 
                  Power(t2,4)))) + 
         Power(s,2)*(Power(s1,4)*
             (Power(s2,2)*(t1 + 2*t2) - 
               2*s2*(Power(t1,2) - Power(t2,2)) + 
               t1*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,3)*(2*Power(s2,3)*(t1 + t2) + 
               s2*t1*(8*Power(t1,2) - 5*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(-7*Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
               t1*(3*Power(t1,3) - 7*Power(t1,2)*t2 + 4*t1*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,4)*(-t1 + t2) + 
               Power(s2,3)*(2*Power(t1,2) - 22*t1*t2 + 9*Power(t2,2)) - 
               Power(s2,2)*(Power(t1,3) - 34*Power(t1,2)*t2 + 
                  34*t1*Power(t2,2) - 9*Power(t2,3)) + 
               s2*t2*(-14*Power(t1,3) + 29*Power(t1,2)*t2 - 
                  19*t1*Power(t2,2) + Power(t2,3)) + 
               t1*t2*(Power(t1,3) - 12*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 6*Power(t2,3))) + 
            t1*t2*(Power(s2,4)*t2 + 
               Power(s2,3)*(2*Power(t1,2) - Power(t2,2)) + 
               t1*t2*(-Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(-4*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + Power(t2,3)) + 
               2*s2*(Power(t1,4) + 2*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) - t1*Power(t2,3) - Power(t2,4))\
) + s1*(4*Power(s2,5)*t2 + Power(s2,4)*
                (Power(t1,2) - 16*t1*t2 + 14*Power(t2,2)) - 
               2*s2*t1*t2*(3*Power(t1,3) + t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,3)*(-2*Power(t1,3) + 21*Power(t1,2)*t2 - 
                  22*t1*Power(t2,2) + 12*Power(t2,3)) + 
               2*t1*t2*(Power(t1,4) + 3*Power(t1,2)*Power(t2,2) - 
                  4*t1*Power(t2,3) - Power(t2,4)) + 
               Power(s2,2)*(Power(t1,4) - 5*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  2*Power(t2,4)))))*lg2((-s1 - t1 + t2)/(s - s1 - s2)))/
     (s*s1*s2*t1*Power(s - s2 + t1,2)*Power(t2,2)*(s - s1 + t2)*
       (s2 - t1 + t2)) + (4*(Power(s,3)*
          (2*Power(s2,3) - 3*Power(s2,2)*(t1 - t2) + 
            4*s2*Power(t1 - t2,2) + 3*Power(t1 - t2,3)) + 
         2*Power(s1,3)*t2*(4*Power(s2,2) - 8*s2*t1 + 4*Power(t1,2) + 
            3*s2*t2 - 3*t1*t2 + 2*Power(t2,2)) + 
         s2*(-6*s2*Power(t1,2)*Power(t1 - t2,2) - 
            2*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(t1 - t2,2)*
             (2*Power(t1,3) - Power(t1,2)*t2 + 2*Power(t2,3)) + 
            Power(s2,2)*(6*Power(t1,3) - 9*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) + 2*Power(t2,3))) - 
         2*Power(s1,2)*(Power(s2,4) - Power(t1,4) - Power(t1,3)*t2 + 
            6*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
            4*Power(t2,4) - Power(s2,3)*(2*t1 + t2) + 
            Power(s2,2)*t2*(t1 + 8*t2) + 
            s2*(2*Power(t1,3) + Power(t1,2)*t2 - 14*t1*Power(t2,2) + 
               5*Power(t2,3))) + 
         s1*(-2*Power(s2,4)*(t1 - 2*t2) + 
            Power(s2,3)*(5*Power(t1,2) - 8*t1*t2 - 4*Power(t2,2)) + 
            Power(t1 - t2,2)*
             (Power(t1,3) - 2*t1*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,2)*(-3*Power(t1,3) + 2*Power(t1,2)*t2 + 
               2*t1*Power(t2,2) + 8*Power(t2,3)) + 
            s2*(-Power(t1,4) + 4*Power(t1,3)*t2 + 
               3*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
               2*Power(t2,4))) - 
         Power(s,2)*(2*Power(s2,4) + 6*Power(s2,2)*Power(t1 - t2,2) - 
            4*Power(t1 - t2,3)*t2 + Power(s2,3)*(-4*t1 + t2) - 
            s2*Power(t1 - t2,2)*(4*t1 + 7*t2) + 
            s1*(3*Power(s2,3) + (5*t1 - 8*t2)*Power(t1 - t2,2) - 
               Power(s2,2)*(9*t1 + 2*t2) + 
               s2*(Power(t1,2) - 6*t1*t2 + 5*Power(t2,2)))) + 
         s*(-2*Power(s2,4)*t2 - 
            Power(t1 - t2,3)*(Power(t1,2) - 2*Power(t2,2)) + 
            4*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            2*s2*Power(t1 - t2,2)*(3*Power(t1,2) + 2*Power(t2,2)) - 
            3*Power(s2,2)*(3*Power(t1,3) - 5*Power(t1,2)*t2 + 
               2*Power(t2,3)) + 
            2*Power(s1,2)*(2*Power(s2,3) + Power(t1,3) - 
               7*Power(t1,2)*t2 + 9*t1*Power(t2,2) - 3*Power(t2,3) - 
               Power(s2,2)*(3*t1 + 2*t2) + s2*t2*(9*t1 + 2*t2)) + 
            2*s1*(Power(s2,4) - 2*Power(s2,3)*(t1 + 2*t2) + 
               Power(s2,2)*t2*(t1 + 5*t2) - 
               Power(t1 - t2,2)*(Power(t1,2) + 4*t1*t2 - 4*Power(t2,2)) + 
               s2*(2*Power(t1,3) + 5*Power(t1,2)*t2 - 3*t1*Power(t2,2) - 
                  4*Power(t2,3)))))*lg2((s*(-s1 - t1 + t2))/(s1 + s2)))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) + 
    (4*(2*Power(s1,3)*Power(s2 - t1,3) - 
         Power(s,3)*(2*Power(s2,3) - 3*Power(s2,2)*(t1 - t2) + 
            4*s2*Power(t1 - t2,2) - Power(t1 - t2,3)) + 
         2*Power(s1,2)*Power(s2 - t1,2)*
          (Power(s2,2) - 2*s2*t2 + t1*(-t1 + t2)) + 
         s2*(6*s2*Power(t1,2)*Power(t1 - t2,2) - 
            Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*t1*(-6*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
            2*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
         s1*(s2 - t1)*(2*Power(s2,3)*(t1 - 2*t2) + 
            Power(t1,2)*Power(t1 - t2,2) + 2*s2*t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-3*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2))) + 
         s*(-6*s2*Power(t1,2)*Power(t1 - t2,2) + 
            Power(t1,2)*Power(t1 - t2,3) + 2*Power(s2,4)*t2 - 
            4*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            3*Power(s2,2)*t1*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) - 
            2*Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) + 2*t1*(t1 - t2) - s2*(4*t1 + t2)) - 
            2*s1*(Power(s2,4) - Power(t1,2)*Power(t1 - t2,2) + 
               Power(s2,2)*t2*(3*t1 + t2) - 2*Power(s2,3)*(t1 + 2*t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 - Power(t2,2)))) + 
         Power(s,2)*(s2*(2*Power(s2,3) + 6*s2*Power(t1 - t2,2) - 
               Power(t1 - t2,2)*(4*t1 - t2) + Power(s2,2)*(-4*t1 + t2)) + 
            s1*(3*Power(s2,3) - 3*t1*Power(t1 - t2,2) - 
               Power(s2,2)*(9*t1 + 2*t2) + 
               s2*(9*Power(t1,2) - 10*t1*t2 + Power(t2,2)))))*
       lg2(-((Power(s,2)*(-s1 - t1 + t2))/(s1 + s2))))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) + 
    (4*(2*Power(s,5)*t1*(t1 - t2)*t2 + 
         s1*s2*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*t2*(s2 - t1 + t2)*
          (s1*(-s2 + t1) + s2*t2) - 
         Power(s,4)*t1*(2*t2*(s2*(t1 - 2*t2) + 2*t2*(-t1 + t2)) + 
            s1*(-Power(t1,2) + 6*t1*t2 - 7*Power(t2,2) + s2*(t1 + t2))) - 
         Power(s,3)*(t1*t2*(-(Power(s2,2)*(t1 - 3*t2)) + 
               2*s2*(t1 - 3*t2)*t2 + 3*Power(t2,2)*(-t1 + t2)) + 
            Power(s1,2)*t1*(Power(s2,2) + 3*Power(t1,2) - 7*t1*t2 + 
               10*Power(t2,2) - s2*(4*t1 + t2)) + 
            s1*t2*(Power(s2,2)*(-6*t1 + t2) + 
               s2*(3*Power(t1,2) + 4*t1*t2 + Power(t2,2)) - 
               t1*(Power(t1,2) - 10*t1*t2 + 13*Power(t2,2)))) + 
         s*(s2*t1*Power(t2,3)*(Power(s2,2) + Power(t2,2)) - 
            Power(s1,4)*t1*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               2*Power(t2,2) + s2*(-2*t1 + t2)) + 
            s1*t2*(Power(s2,4)*(t1 - 4*t2) - 2*s2*t1*Power(t2,3) - 
               t1*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,2)*t1*t2*(2*t1 + t2) - 
               Power(s2,3)*(Power(t1,2) - 2*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,3)*t2*(2*Power(s2,3) + Power(s2,2)*(3*t1 + t2) - 
               s2*(6*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               t1*(Power(t1,2) - 3*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(3*t1*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,4)*(t1 + 2*t2) + s2*Power(t1,2)*t2*(t1 + 2*t2) + 
               Power(s2,3)*(-2*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - 8*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 4*Power(t2,3)))) + 
         Power(s,2)*(t1*Power(t2,2)*
             (Power(s2,3) + Power(s2,2)*(t1 - 3*t2) + 3*s2*Power(t2,2) + 
               (t1 - t2)*Power(t2,2)) + 
            Power(s1,3)*t1*(2*Power(s2,2) + 3*Power(t1,2) - 4*t1*t2 + 
               7*Power(t2,2) + s2*(-5*t1 + t2)) + 
            Power(s1,2)*t2*(-Power(s2,3) + Power(s2,2)*(-11*t1 + t2) + 
               t1*(-2*Power(t1,2) + 9*t1*t2 - 15*Power(t2,2)) + 
               s2*(12*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
            s1*(3*t1*Power(t2,3)*(-2*t1 + 3*t2) + 
               Power(s2,3)*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) - 
               s2*t1*t2*(Power(t1,2) + t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-Power(t1,3) + 7*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 3*Power(t2,3)))))*
       lg2((-s1 - t1 + t2)/(-s + s2 - t1)))/
     (s*s1*s2*t1*(s - s2 + t1)*Power(t2,2)*(s - s1 + t2)*(s2 - t1 + t2)) - 
    (8*(2*Power(s,5)*t1*(t1 - t2)*t2 + 
         s1*s2*(Power(s1,2) + 2*s1*s2 + 2*Power(s2,2))*t2*(s2 - t1 + t2)*
          (s1*(-s2 + t1) + s2*t2) - 
         Power(s,4)*t1*(2*t2*(s2*(t1 - 2*t2) + 2*t2*(-t1 + t2)) + 
            s1*(-Power(t1,2) + 6*t1*t2 - 7*Power(t2,2) + s2*(t1 + t2))) - 
         Power(s,3)*(t1*t2*(-(Power(s2,2)*(t1 - 3*t2)) + 
               2*s2*(t1 - 3*t2)*t2 + 3*Power(t2,2)*(-t1 + t2)) + 
            Power(s1,2)*t1*(Power(s2,2) + 3*Power(t1,2) - 7*t1*t2 + 
               10*Power(t2,2) - s2*(4*t1 + t2)) + 
            s1*t2*(Power(s2,2)*(-6*t1 + t2) + 
               s2*(3*Power(t1,2) + 4*t1*t2 + Power(t2,2)) - 
               t1*(Power(t1,2) - 10*t1*t2 + 13*Power(t2,2)))) + 
         s*(s2*t1*Power(t2,3)*(Power(s2,2) + Power(t2,2)) - 
            Power(s1,4)*t1*(Power(s2,2) + Power(t1,2) - t1*t2 + 
               2*Power(t2,2) + s2*(-2*t1 + t2)) + 
            s1*t2*(Power(s2,4)*(t1 - 4*t2) - 2*s2*t1*Power(t2,3) - 
               t1*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,2)*t1*t2*(2*t1 + t2) - 
               Power(s2,3)*(Power(t1,2) - 2*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,3)*t2*(2*Power(s2,3) + Power(s2,2)*(3*t1 + t2) - 
               s2*(6*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               t1*(Power(t1,2) - 3*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,2)*(3*t1*(t1 - 2*t2)*Power(t2,3) + 
               Power(s2,4)*(t1 + 2*t2) + s2*Power(t1,2)*t2*(t1 + 2*t2) + 
               Power(s2,3)*(-2*Power(t1,2) + 5*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(Power(t1,3) - 8*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 4*Power(t2,3)))) + 
         Power(s,2)*(t1*Power(t2,2)*
             (Power(s2,3) + Power(s2,2)*(t1 - 3*t2) + 3*s2*Power(t2,2) + 
               (t1 - t2)*Power(t2,2)) + 
            Power(s1,3)*t1*(2*Power(s2,2) + 3*Power(t1,2) - 4*t1*t2 + 
               7*Power(t2,2) + s2*(-5*t1 + t2)) + 
            Power(s1,2)*t2*(-Power(s2,3) + Power(s2,2)*(-11*t1 + t2) + 
               t1*(-2*Power(t1,2) + 9*t1*t2 - 15*Power(t2,2)) + 
               s2*(12*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2))) + 
            s1*(3*t1*Power(t2,3)*(-2*t1 + 3*t2) + 
               Power(s2,3)*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) - 
               s2*t1*t2*(Power(t1,2) + t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-Power(t1,3) + 7*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 3*Power(t2,3)))))*
       lg2(-((-s1 - t1 + t2)/((s - s1 - s2)*(-s + s2 - t1)))))/
     (s*s1*s2*t1*(s - s2 + t1)*Power(t2,2)*(s - s1 + t2)*(s2 - t1 + t2)) - 
    (4*(-4*(s1 + s2)*Power(s1*(s2 - t1) - s2*t2,2) + 
         2*Power(s,3)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) - 
         Power(s,2)*(s1 + s2)*
          (2*Power(s1,2) + 2*Power(s2,2) + s1*(2*s2 - t1 - 3*t2) + 
            4*Power(t1 - t2,2) - s2*(3*t1 + t2)) + 
         s*(s1 + s2)*(3*Power(s1,2)*(s2 - t1) + 
            s2*t2*(-3*s2 - 2*t1 + 6*t2) + 
            s1*(3*Power(s2,2) + 2*t1*(3*t1 - t2) - 9*s2*(t1 + t2))))*
       lg2(-(((-s2 + t1 - t2)*(-s1 - t1 + t2))/s)))/
     (Power(s,2)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) + 
    (4*(6*Power(s1*(s2 - t1) - s2*t2,3) + 
         2*Power(s,4)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) + 
         Power(s,3)*(-2*Power(s1,3) - 2*Power(s2,3) - 
            4*s2*Power(t1 - t2,2) + 3*Power(t1 - t2,2)*(t1 + t2) + 
            Power(s2,2)*(3*t1 + t2) + Power(s1,2)*(-4*s2 + t1 + 3*t2) - 
            4*s1*(Power(s2,2) + Power(t1 - t2,2) - s2*(t1 + t2))) - 
         2*s*(2*Power(s1,3)*Power(s2 - t1,2) + 
            Power(s2,2)*(2*s2 + 3*t1 - 6*t2)*Power(t2,2) + 
            Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) - 8*s2*t1 + 6*Power(t1,2) - 10*s2*t2 - 
               3*t1*t2) + s1*s2*t2*
             (-4*Power(s2,2) - 3*t1*(t1 + t2) + 7*s2*(t1 + 2*t2))) + 
         Power(s,2)*(3*Power(s1,3)*(s2 - t1) + 
            Power(s1,2)*(6*Power(s2,2) - 12*s2*t1 + 6*Power(t1,2) - 
               9*s2*t2 - 2*t1*t2) - 
            s2*t2*(3*Power(s2,2) + 2*s2*(t1 - 3*t2) + 
               3*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
            s1*(3*Power(s2,3) - 3*Power(s2,2)*(3*t1 + 4*t2) - 
               3*t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               5*s2*(3*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)))))*
       lg2(((-s2 + t1 - t2)*(-s1 - t1 + t2))/(-s + s1 + t1 - t2)))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (4*(2*Power(s1*(s2 - t1) - s2*t2,3) + 
         2*Power(s,4)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) + 
         Power(s,3)*(-2*Power(s1,3) - 2*Power(s2,3) - 
            4*s2*Power(t1 - t2,2) + Power(t1 - t2,2)*(t1 + t2) + 
            Power(s2,2)*(3*t1 + t2) + Power(s1,2)*(-4*s2 + t1 + 3*t2) - 
            4*s1*(Power(s2,2) + Power(t1 - t2,2) - s2*(t1 + t2))) - 
         2*s*(2*Power(s1,3)*Power(s2 - t1,2) + 
            Power(s2,2)*(2*s2 + t1 - 2*t2)*Power(t2,2) + 
            Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) - 4*s2*t1 + 2*Power(t1,2) - 6*s2*t2 - t1*t2) \
- s1*s2*t2*(4*Power(s2,2) + t1*(t1 + t2) - s2*(5*t1 + 6*t2))) + 
         Power(s,2)*(3*Power(s1,3)*(s2 - t1) + 
            Power(s1,2)*(6*Power(s2,2) - 12*s2*t1 + 6*Power(t1,2) - 
               9*s2*t2 - 2*t1*t2) - 
            s2*t2*(3*Power(s2,2) + Power(t1,2) + 2*s2*(t1 - 3*t2) - 
               4*t1*t2 + 3*Power(t2,2)) + 
            s1*(3*Power(s2,3) - 3*Power(s2,2)*(3*t1 + 4*t2) - 
               t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(9*Power(t1,2) - 6*t1*t2 + 9*Power(t2,2)))))*
       lg2(-((Power(-s2 + t1 - t2,2)*(-s1 - t1 + t2))/(-s + s1 + t1 - t2)))\
)/(Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (4*(Power(s,6)*t1*(2*s2 - t1 + t2) + 
         s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*(s2 - t1)*
          (s2 - t1 + t2)*(s1*(s2 - t1) - s2*t2) + 
         Power(s,5)*t1*(-4*Power(s2,2) + 3*s2*t1 - Power(t1,2) + 
            s1*(-7*s2 + 4*t1 - 3*t2) + 2*s2*t2 + Power(t2,2)) + 
         Power(s,4)*t1*(2*Power(s2,3) - Power(t1,3) + t1*Power(t2,2) + 
            3*Power(s1,2)*(3*s2 - 2*t1 + t2) - 
            Power(s2,2)*(2*t1 + 7*t2) + 
            s1*(11*Power(s2,2) - 11*s2*t1 + 4*Power(t1,2) - 7*s2*t2 - 
               2*Power(t2,2)) + 
            s2*(3*Power(t1,2) + 2*t1*t2 + Power(t2,2))) - 
         Power(s,3)*(Power(s1,2)*t1*
             (11*Power(s2,2) - 15*s2*t1 + 6*Power(t1,2) - 8*s2*t2 - 
               Power(t2,2)) + 
            Power(s1,3)*(2*Power(s2,2) + t1*(-4*t1 + t2) + 
               s2*(3*t1 + 2*t2)) + 
            t1*(Power(t1,4) - 4*Power(s2,3)*t2 - 
               Power(t1,2)*Power(t2,2) + 
               Power(s2,2)*(4*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2)) - 
               s2*(5*Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3))) + 
            s1*(2*Power(s2,4) + 4*Power(s2,3)*t2 - 
               2*Power(t1,2)*(Power(t1,2) + t1*t2 - Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,2) - 22*t1*t2 + 3*Power(t2,2)) + 
               s2*(5*Power(t1,3) + 11*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)))) - 
         s*(Power(s1,4)*(s2 - t1)*
             (3*Power(s2,2) - 2*s2*t1 - Power(t1,2) + 3*s2*t2) - 
            s2*t1*t2*(Power(t1,3)*(2*t1 - t2) + 
               s2*Power(t1,2)*(-4*t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) + Power(t2,2))) + 
            Power(s1,3)*(5*Power(s2,4) - Power(t1,3)*t2 + 
               Power(s2,3)*(-13*t1 + 3*t2) + 
               Power(s2,2)*(11*Power(t1,2) - 11*t1*t2 - 
                  2*Power(t2,2)) - 
               s2*t1*(3*Power(t1,2) - 9*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(5*Power(s2,5) + Power(t1,5) + 
               Power(s2,4)*(-14*t1 + 5*t2) + 
               Power(s2,3)*(13*Power(t1,2) - 15*t1*t2 + Power(t2,2)) - 
               s2*Power(t1,2)*(2*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  t1*Power(t2,2) + Power(t2,3))) + 
            s1*(s2 - t1)*(2*Power(s2,5) - 2*s2*Power(t1,4) + 
               Power(s2,3)*Power(t1 - t2,2) + Power(t1,4)*t2 + 
               Power(s2,4)*(-3*t1 + 2*t2) + 
               Power(s2,2)*(2*Power(t1,3) + 2*t1*Power(t2,2) + 
                  Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*
             (2*Power(s2,2) - s2*t1 - Power(t1,2) + 2*s2*t2) + 
            Power(s1,3)*(4*Power(s2,3) + 4*Power(t1,3) + 
               Power(s2,2)*(-2*t1 + 3*t2) - 
               s2*(6*Power(t1,2) + 5*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(4*Power(s2,4) + Power(s2,3)*(-7*t1 + 6*t2) + 
               3*Power(s2,2)*(Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               Power(t1,2)*(-Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               s2*(Power(t1,3) + 17*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
                  Power(t2,3))) + 
            t1*(Power(t1,3)*t2*(-t1 + t2) + 
               2*s2*Power(t1,2)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,2) + 3*Power(t2,2)) - 
               Power(s2,2)*(4*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  2*Power(t2,3))) + 
            s1*(4*Power(s2,5) + 2*Power(t1,5) + 
               Power(s2,4)*(-9*t1 + 7*t2) + 
               Power(s2,3)*(7*Power(t1,2) - 18*t1*t2 + 5*Power(t2,2)) - 
               s2*t1*(7*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 2*Power(t2,3)))))*
       lg2((-s1 - t1 + t2)/t2))/
     (s*s1*s2*t1*Power(s - s2 + t1,2)*t2*(s - s1 + t2)*(s2 - t1 + t2)) + 
    (8*(Power(s,6)*t1*(2*s2 - t1 + t2) + 
         s1*s2*(2*Power(s1,2) + 2*s1*s2 + Power(s2,2))*(s2 - t1)*
          (s2 - t1 + t2)*(s1*(s2 - t1) - s2*t2) + 
         Power(s,5)*t1*(-4*Power(s2,2) + 3*s2*t1 - Power(t1,2) + 
            s1*(-7*s2 + 4*t1 - 3*t2) + 2*s2*t2 + Power(t2,2)) + 
         Power(s,4)*t1*(2*Power(s2,3) - Power(t1,3) + t1*Power(t2,2) + 
            3*Power(s1,2)*(3*s2 - 2*t1 + t2) - 
            Power(s2,2)*(2*t1 + 7*t2) + 
            s1*(11*Power(s2,2) - 11*s2*t1 + 4*Power(t1,2) - 7*s2*t2 - 
               2*Power(t2,2)) + 
            s2*(3*Power(t1,2) + 2*t1*t2 + Power(t2,2))) - 
         Power(s,3)*(Power(s1,2)*t1*
             (11*Power(s2,2) - 15*s2*t1 + 6*Power(t1,2) - 8*s2*t2 - 
               Power(t2,2)) + 
            Power(s1,3)*(2*Power(s2,2) + t1*(-4*t1 + t2) + 
               s2*(3*t1 + 2*t2)) + 
            t1*(Power(t1,4) - 4*Power(s2,3)*t2 - 
               Power(t1,2)*Power(t2,2) + 
               Power(s2,2)*(4*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2)) - 
               s2*(5*Power(t1,3) + Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3))) + 
            s1*(2*Power(s2,4) + 4*Power(s2,3)*t2 - 
               2*Power(t1,2)*(Power(t1,2) + t1*t2 - Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,2) - 22*t1*t2 + 3*Power(t2,2)) + 
               s2*(5*Power(t1,3) + 11*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)))) - 
         s*(Power(s1,4)*(s2 - t1)*
             (3*Power(s2,2) - 2*s2*t1 - Power(t1,2) + 3*s2*t2) - 
            s2*t1*t2*(Power(t1,3)*(2*t1 - t2) + 
               s2*Power(t1,2)*(-4*t1 + t2) + 
               Power(s2,2)*(2*Power(t1,2) + Power(t2,2))) + 
            Power(s1,3)*(5*Power(s2,4) - Power(t1,3)*t2 + 
               Power(s2,3)*(-13*t1 + 3*t2) + 
               Power(s2,2)*(11*Power(t1,2) - 11*t1*t2 - 
                  2*Power(t2,2)) - 
               s2*t1*(3*Power(t1,2) - 9*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(5*Power(s2,5) + Power(t1,5) + 
               Power(s2,4)*(-14*t1 + 5*t2) + 
               Power(s2,3)*(13*Power(t1,2) - 15*t1*t2 + Power(t2,2)) - 
               s2*Power(t1,2)*(2*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-3*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  t1*Power(t2,2) + Power(t2,3))) + 
            s1*(s2 - t1)*(2*Power(s2,5) - 2*s2*Power(t1,4) + 
               Power(s2,3)*Power(t1 - t2,2) + Power(t1,4)*t2 + 
               Power(s2,4)*(-3*t1 + 2*t2) + 
               Power(s2,2)*(2*Power(t1,3) + 2*t1*Power(t2,2) + 
                  Power(t2,3)))) + 
         Power(s,2)*(Power(s1,4)*
             (2*Power(s2,2) - s2*t1 - Power(t1,2) + 2*s2*t2) + 
            Power(s1,3)*(4*Power(s2,3) + 4*Power(t1,3) + 
               Power(s2,2)*(-2*t1 + 3*t2) - 
               s2*(6*Power(t1,2) + 5*t1*t2 + Power(t2,2))) + 
            Power(s1,2)*(4*Power(s2,4) + Power(s2,3)*(-7*t1 + 6*t2) + 
               3*Power(s2,2)*(Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               Power(t1,2)*(-Power(t1,2) - 3*t1*t2 + Power(t2,2)) + 
               s2*(Power(t1,3) + 17*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
                  Power(t2,3))) + 
            t1*(Power(t1,3)*t2*(-t1 + t2) + 
               2*s2*Power(t1,2)*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,2) + 3*Power(t2,2)) - 
               Power(s2,2)*(4*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  2*Power(t2,3))) + 
            s1*(4*Power(s2,5) + 2*Power(t1,5) + 
               Power(s2,4)*(-9*t1 + 7*t2) + 
               Power(s2,3)*(7*Power(t1,2) - 18*t1*t2 + 5*Power(t2,2)) - 
               s2*t1*(7*Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,2)*(3*Power(t1,3) + 12*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 2*Power(t2,3)))))*
       lg2(-((-s1 - t1 + t2)/((s - s1 - s2)*t2))))/
     (s*s1*s2*t1*Power(s - s2 + t1,2)*t2*(s - s1 + t2)*(s2 - t1 + t2)) - 
    (4*(2*Power(s1,3)*Power(s2 - t1,3) - 
         Power(s,3)*(2*Power(s2,3) - 3*Power(s2,2)*(t1 - t2) + 
            4*s2*Power(t1 - t2,2) - Power(t1 - t2,3)) + 
         2*Power(s1,2)*Power(s2 - t1,2)*
          (Power(s2,2) - 2*s2*t2 + t1*(-t1 + t2)) + 
         s2*(6*s2*Power(t1,2)*Power(t1 - t2,2) - 
            Power(t1,2)*Power(t1 - t2,2)*(2*t1 - t2) + 
            Power(s2,2)*t1*(-6*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
            2*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2))) + 
         s1*(s2 - t1)*(2*Power(s2,3)*(t1 - 2*t2) + 
            Power(t1,2)*Power(t1 - t2,2) + 2*s2*t1*(t1 - t2)*t2 + 
            Power(s2,2)*(-3*Power(t1,2) + 4*t1*t2 + 2*Power(t2,2))) + 
         s*(-6*s2*Power(t1,2)*Power(t1 - t2,2) + 
            Power(t1,2)*Power(t1 - t2,3) + 2*Power(s2,4)*t2 - 
            4*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            3*Power(s2,2)*t1*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) - 
            2*Power(s1,2)*(s2 - t1)*
             (2*Power(s2,2) + 2*t1*(t1 - t2) - s2*(4*t1 + t2)) - 
            2*s1*(Power(s2,4) - Power(t1,2)*Power(t1 - t2,2) + 
               Power(s2,2)*t2*(3*t1 + t2) - 2*Power(s2,3)*(t1 + 2*t2) + 
               s2*t1*(2*Power(t1,2) - t1*t2 - Power(t2,2)))) + 
         Power(s,2)*(s2*(2*Power(s2,3) + 6*s2*Power(t1 - t2,2) - 
               Power(t1 - t2,2)*(4*t1 - t2) + Power(s2,2)*(-4*t1 + t2)) + 
            s1*(3*Power(s2,3) - 3*t1*Power(t1 - t2,2) - 
               Power(s2,2)*(9*t1 + 2*t2) + 
               s2*(9*Power(t1,2) - 10*t1*t2 + Power(t2,2)))))*
       lg2(-((s*(s1 + s2))/(s2 - t1 + t2))))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) - 
    (4*(-(Power(s1,3)*(3*Power(s2,2) + 3*Power(t1,2) - t1*t2 + 
              2*Power(t2,2) + s2*(-6*t1 + t2))) + 
         Power(s,3)*(2*Power(s2,2) + 3*Power(t1 - t2,2) + 
            s2*(-t1 + t2)) + Power(s,2)*
          (-2*Power(s2,3) + 2*Power(t1 - t2,2)*t2 + 
            Power(s2,2)*(2*t1 + t2) + 
            s2*(-4*Power(t1,2) + 11*t1*t2 - 7*Power(t2,2)) + 
            s1*(-3*Power(s2,2) + 6*s2*t1 - 7*Power(t1,2) + 5*s2*t2 + 
               11*t1*t2 - 4*Power(t2,2))) + 
         Power(s1,2)*(-2*Power(s2,3) - 2*Power(t1,3) + 
            2*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 4*Power(t2,3) + 
            Power(s2,2)*(2*t1 + 7*t2) + 
            s2*(2*Power(t1,2) - 9*t1*t2 + Power(t2,2))) - 
         s1*(2*Power(s2,3)*(t1 - 2*t2) - 
            s2*t2*(-3*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
            Power(t1 - t2,2)*(Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(-3*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) - 
         s2*(2*Power(s2,2)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(t1 - t2,2)*(2*Power(t1,2) + t1*t2 + Power(t2,2)) - 
            s2*(4*Power(t1,3) - 5*Power(t1,2)*t2 + Power(t2,3))) + 
         s*(-2*Power(s2,3)*t2 + 
            Power(t1 - t2,2)*(Power(t1,2) + Power(t2,2)) + 
            Power(s1,2)*(4*Power(s2,2) - 11*s2*t1 + 7*Power(t1,2) - 
               5*s2*t2 - 6*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*(4*Power(t1,2) - 6*t1*t2 + 6*Power(t2,2)) + 
            s2*(-5*Power(t1,3) + 5*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            2*s1*(Power(s2,3) + Power(t1,3) - 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - 2*Power(t2,3) - 
               Power(s2,2)*(t1 + 5*t2) - s2*(Power(t1,2) - 4*Power(t2,2)))\
))*lg2((s*(-s1 - t1 + t2))/(s2 - t1 + t2)))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,2)) + 
    (4*(2*Power(s1,2)*Power(s2,2)*(s2 - t1)*(s1 + s2 - t1)*t1*(s1 - t2)*
          Power(s1 + t1 - t2,3)*t2*(s2 + t2)*
          Power(s1*(-s2 + t1) + s2*t2,3) + 
         Power(s,10)*t1*(4*s2*Power(t1 - t2,4)*Power(t2,2)*(s2 + t2) + 
            6*Power(s1,2)*Power(t1 - t2,3)*
             (Power(s2,2)*(t1 - t2) + s2*(t1 - t2)*t2 + t1*Power(t2,2)) \
+ 2*Power(s1,4)*(t1*(t1 - t2)*Power(t2,2) + 
               Power(s2,2)*(Power(t1,2) - 2*t1*t2 - 2*Power(t2,2)) + 
               s2*t2*(Power(t1,2) - 2*t1*t2 - 2*Power(t2,2))) + 
            Power(s1,3)*(t1 - t2)*
             (6*t1*(t1 - t2)*Power(t2,2) + 
               Power(s2,2)*(6*Power(t1,2) - 12*t1*t2 + Power(t2,2)) + 
               s2*t2*(6*Power(t1,2) - 12*t1*t2 + Power(t2,2))) + 
            s1*Power(t1 - t2,3)*
             (2*t1*(t1 - t2)*Power(t2,2) + 
               Power(s2,2)*(2*Power(t1,2) - 4*t1*t2 + 7*Power(t2,2)) + 
               s2*t2*(2*Power(t1,2) - 4*t1*t2 + 7*Power(t2,2)))) - 
         2*s*Power(s1,2)*Power(s2,2)*t1*Power(s1 + t1 - t2,3)*t2*
          (s2 + t2)*(-7*Power(s1,5)*Power(s2 - t1,3) + 
            Power(s2,2)*(s2 - t1)*Power(t2,3)*
             (7*Power(s2,2) - 6*s2*t1 - Power(t1,2) - 4*s2*t2 + 2*t1*t2) \
- Power(s1,4)*Power(s2 - t1,2)*
             (15*Power(s2,2) - 26*s2*t1 + 11*Power(t1,2) - 24*s2*t2 + 
               6*t1*t2) - s1*s2*Power(t2,2)*
             (21*Power(s2,4) + Power(t1,3)*(t1 + t2) - 
               3*Power(s2,3)*(19*t1 + 9*t2) - 
               s2*t1*(17*Power(t1,2) + 19*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(52*Power(t1,2) + 45*t1*t2 + 3*Power(t2,2))) \
+ Power(s1,2)*t2*(21*Power(s2,5) + Power(t1,4)*(-2*t1 + t2) - 
               3*Power(s2,4)*(25*t1 + 19*t2) + 
               s2*Power(t1,2)*
                (18*Power(t1,2) + 19*t1*t2 + Power(t2,2)) - 
               Power(s2,2)*t1*
                (63*Power(t1,2) + 91*t1*t2 + 16*Power(t2,2)) + 
               Power(s2,3)*(101*Power(t1,2) + 128*t1*t2 + 
                  16*Power(t2,2))) - 
            Power(s1,3)*(s2 - t1)*
             (7*Power(s2,4) - Power(s2,3)*(24*t1 + 49*t2) + 
               Power(t1,2)*(3*Power(t1,2) + 9*t1*t2 - Power(t2,2)) + 
               6*Power(s2,2)*
                (5*Power(t1,2) + 16*t1*t2 + 5*Power(t2,2)) - 
               s2*t1*(16*Power(t1,2) + 56*t1*t2 + 17*Power(t2,2)))) - 
         Power(s,2)*Power(s1,2)*Power(s2,2)*t1*Power(s1 + t1 - t2,3)*t2*
          (s2 + t2)*(21*Power(s1,5)*Power(s2 - t1,2) + 
            Power(s1,4)*(s2 - t1)*
             (79*Power(s2,2) + t1*(71*t1 + 13*t2) - 
               2*s2*(75*t1 + 38*t2)) + 
            s2*Power(t2,2)*(21*Power(s2,4) - 
               2*Power(s2,3)*(17*t1 + 25*t2) + 
               2*s2*t1*(3*Power(t1,2) - 5*t1*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(6*Power(t1,2) + 64*t1*t2 + 13*Power(t2,2))) \
+ Power(s1,3)*(79*Power(s2,4) - 4*Power(s2,3)*(73*t1 + 62*t2) + 
               Power(t1,2)*(49*Power(t1,2) + 41*t1*t2 - 
                  7*Power(t2,2)) - 
               s2*t1*(232*Power(t1,2) + 262*t1*t2 + 71*Power(t2,2)) + 
               Power(s2,2)*(396*Power(t1,2) + 469*t1*t2 + 
                  96*Power(t2,2))) + 
            s1*t2*(-42*Power(s2,5) + Power(s2,4)*(131*t1 + 179*t2) + 
               Power(t1,3)*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               Power(s2,3)*(160*Power(t1,2) + 289*t1*t2 + 
                  116*Power(t2,2)) - 
               s2*t1*(30*Power(t1,3) - 19*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(98*Power(t1,3) + 95*Power(t1,2)*t2 + 
                  123*t1*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s1,2)*(21*Power(s2,5) - Power(s2,4)*(97*t1 + 208*t2) + 
               Power(s2,3)*(172*Power(t1,2) + 517*t1*t2 + 
                  272*Power(t2,2)) - 
               Power(t1,2)*(7*Power(t1,3) + 23*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t1*(55*Power(t1,3) + 163*Power(t1,2)*t2 + 
                  78*t1*Power(t2,2) + 27*Power(t2,3)) - 
               Power(s2,2)*(144*Power(t1,3) + 449*Power(t1,2)*t2 + 
                  349*t1*Power(t2,2) + 48*Power(t2,3)))) + 
         Power(s,9)*(-2*s2*t1*Power(t1 - t2,3)*t2*
             (s2*t2*(-3*Power(t1,2) + 7*t1*t2 - 6*Power(t2,2)) + 
               2*Power(s2,2)*(Power(t1,2) + 3*t1*t2 - 5*Power(t2,2)) + 
               Power(t2,2)*(-5*Power(t1,2) + t1*t2 + 4*Power(t2,2))) + 
            2*Power(s1,5)*t1*
             (2*Power(s2,3)*t1 - 
               Power(t2,2)*(6*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               6*s2*t2*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,2) + 12*t1*t2 + 5*Power(t2,2))) \
+ Power(s1,4)*(-6*t1*Power(t1 - t2,2)*(5*t1 - t2)*Power(t2,2) + 
               Power(s2,3)*(2*Power(t1,3) + 10*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 2*Power(t2,3)) - 
               s2*t1*t2*(30*Power(t1,3) - 86*Power(t1,2)*t2 + 
                  87*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(-30*Power(t1,4) + 86*Power(t1,3)*t2 - 
                  83*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,2)*(t1 - t2)*
             (2*t1*Power(t1 - t2,2)*Power(t2,2)*
                (3*Power(t1,2) + 6*t1*t2 - Power(t2,2)) + 
               s2*t1*t2*(6*Power(t1,4) - 36*Power(t1,3)*t2 + 
                  19*Power(t1,2)*Power(t2,2) + 45*t1*Power(t2,3) - 
                  34*Power(t2,4)) + 
               Power(s2,3)*(-26*Power(t1,4) + 76*Power(t1,3)*t2 - 
                  93*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,2)*(6*Power(t1,5) - 36*Power(t1,4)*t2 + 
                  35*Power(t1,3)*Power(t2,2) - 
                  12*Power(t1,2)*Power(t2,3) - 11*t1*Power(t2,4) - 
                  6*Power(t2,5))) + 
            s1*Power(t1 - t2,2)*
             (6*Power(t1,3)*Power(t1 - t2,2)*Power(t2,2) + 
               s2*t1*t2*(6*Power(t1,4) - 28*Power(t1,3)*t2 + 
                  27*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) - 
                  31*Power(t2,4)) + 
               Power(s2,3)*(-10*Power(t1,4) + 20*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) + 34*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,2)*(6*Power(t1,5) - 28*Power(t1,4)*t2 + 
                  25*Power(t1,3)*Power(t2,2) - 
                  Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) - 
                  2*Power(t2,5))) - 
            Power(s1,3)*(6*t1*Power(t1 - t2,2)*Power(t2,2)*
                (3*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               Power(s2,3)*(18*Power(t1,4) - 68*Power(t1,3)*t2 + 
                  75*Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*t1*t2*(18*Power(t1,4) - 60*Power(t1,3)*t2 + 
                  107*Power(t1,2)*Power(t2,2) - 82*t1*Power(t2,3) + 
                  17*Power(t2,4)) + 
               Power(s2,2)*(18*Power(t1,5) - 60*Power(t1,4)*t2 + 
                  87*Power(t1,3)*Power(t2,2) - 
                  43*Power(t1,2)*Power(t2,3) + 21*t1*Power(t2,4) + 
                  6*Power(t2,5)))) + 
         Power(s,8)*(2*Power(s1,6)*t1*
             (Power(s2,4) - 9*Power(s2,3)*t1 + 
               Power(s2,2)*(15*Power(t1,2) - 30*t1*t2 - 
                  2*Power(t2,2)) - 
               3*s2*t2*(-5*Power(t1,2) + 10*t1*t2 + 2*Power(t2,2)) + 
               Power(t2,2)*(15*Power(t1,2) - 14*t1*t2 + 3*Power(t2,2))) \
+ 2*s2*t1*Power(t1 - t2,3)*t2*
             (Power(s2,3)*(6*Power(t1,2) + 4*t1*t2 - 20*Power(t2,2)) + 
               Power(s2,2)*t1*
                (-5*Power(t1,2) - 9*t1*t2 + 13*Power(t2,2)) - 
               2*Power(t2,2)*
                (-2*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t2*(-Power(t1,3) - 9*Power(t1,2)*t2 + 
                  t1*Power(t2,2) + 18*Power(t2,3))) - 
            Power(s1,4)*(s2*t1*Power(t2,2)*
                (-48*Power(t1,3) + 14*Power(t1,2)*t2 + 
                  149*t1*Power(t2,2) - 19*Power(t2,3)) + 
               2*t1*Power(t2,3)*
                (42*Power(t1,3) - 92*Power(t1,2)*t2 + 
                  65*t1*Power(t2,2) - 15*Power(t2,3)) + 
               Power(s2,4)*(24*Power(t1,3) + 40*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 16*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-48*Power(t1,4) + 110*Power(t1,3)*t2 + 
                  14*Power(t1,2)*Power(t2,2) - 177*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,3)*(-120*Power(t1,4) + 324*Power(t1,3)*t2 - 
                  285*Power(t1,2)*Power(t2,2) - 65*t1*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(s1,5)*(2*Power(s2,4)*
                (5*Power(t1,2) + 6*t1*t2 - Power(t2,2)) + 
               2*Power(s2,3)*
                (-6*Power(t1,3) + 27*Power(t1,2)*t2 + 
                  33*t1*Power(t2,2) + Power(t2,3)) - 
               s2*t1*t2*(58*Power(t1,3) - 154*Power(t1,2)*t2 + 
                  203*t1*Power(t2,2) + 7*Power(t2,3)) + 
               2*t1*Power(t2,2)*
                (-29*Power(t1,3) + 72*Power(t1,2)*t2 - 
                  51*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,2)*(-58*Power(t1,4) + 154*Power(t1,3)*t2 - 
                  165*Power(t1,2)*Power(t2,2) + 97*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,3)*(-2*t1*Power(t1 - t2,2)*Power(t2,2)*
                (24*Power(t1,3) - 16*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,4)*(8*Power(t1,4) - 144*Power(t1,3)*t2 + 
                  249*Power(t1,2)*Power(t2,2) - 24*t1*Power(t2,3) + 
                  36*Power(t2,4)) + 
               s2*t1*t2*(-48*Power(t1,5) + 264*Power(t1,4)*t2 - 
                  474*Power(t1,3)*Power(t2,2) + 
                  227*Power(t1,2)*Power(t2,3) + 79*t1*Power(t2,4) - 
                  48*Power(t2,5)) + 
               Power(s2,3)*(108*Power(t1,5) - 334*Power(t1,4)*t2 + 
                  304*Power(t1,3)*Power(t2,2) - 
                  107*Power(t1,2)*Power(t2,3) + 2*t1*Power(t2,4) + 
                  18*Power(t2,5)) + 
               Power(s2,2)*(-48*Power(t1,6) + 264*Power(t1,5)*t2 - 
                  464*Power(t1,4)*Power(t2,2) + 
                  177*Power(t1,3)*Power(t2,3) + 
                  47*Power(t1,2)*Power(t2,4) - 92*t1*Power(t2,5) - 
                  18*Power(t2,6))) + 
            s1*(t1 - t2)*(2*Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (3*Power(t1,2) - Power(t2,2)) + 
               s2*t1*Power(t1 - t2,2)*t2*
                (6*Power(t1,4) - 36*Power(t1,3)*t2 - 
                  19*Power(t1,2)*Power(t2,2) + 39*t1*Power(t2,3) + 
                  31*Power(t2,4)) + 
               Power(s2,4)*(18*Power(t1,5) - 58*Power(t1,4)*t2 + 
                  95*Power(t1,3)*Power(t2,2) - 
                  131*Power(t1,2)*Power(t2,3) + 74*t1*Power(t2,4) - 
                  10*Power(t2,5)) + 
               Power(s2,3)*(-24*Power(t1,6) + 108*Power(t1,5)*t2 - 
                  190*Power(t1,4)*Power(t2,2) + 
                  69*Power(t1,3)*Power(t2,3) + 
                  103*Power(t1,2)*Power(t2,4) - 70*t1*Power(t2,5) - 
                  8*Power(t2,6)) + 
               Power(s2,2)*(6*Power(t1,7) - 48*Power(t1,6)*t2 + 
                  95*Power(t1,5)*Power(t2,2) - 
                  89*Power(t1,4)*Power(t2,3) - 
                  30*Power(t1,3)*Power(t2,4) + 
                  177*Power(t1,2)*Power(t2,5) - 113*t1*Power(t2,6) + 
                  2*Power(t2,7))) + 
            Power(s1,2)*(-2*t1*Power(t1 - t2,3)*Power(t2,2)*
                (7*Power(t1,3) - 15*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  2*Power(t2,3)) - 
               s2*t1*Power(t1 - t2,2)*t2*
                (14*Power(t1,4) - 40*Power(t1,3)*t2 + 
                  106*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) - 
                  61*Power(t2,4)) + 
               Power(s2,4)*(38*Power(t1,5) - 204*Power(t1,4)*t2 + 
                  405*Power(t1,3)*Power(t2,2) - 
                  300*Power(t1,2)*Power(t2,3) + 75*t1*Power(t2,4) - 
                  32*Power(t2,5)) + 
               Power(s2,3)*(-6*Power(t1,6) + 78*Power(t1,5)*t2 - 
                  275*Power(t1,4)*Power(t2,2) + 
                  231*Power(t1,3)*Power(t2,3) + 
                  49*Power(t1,2)*Power(t2,4) - 73*t1*Power(t2,5) - 
                  22*Power(t2,6)) + 
               Power(s2,2)*(-14*Power(t1,7) + 68*Power(t1,6)*t2 - 
                  94*Power(t1,5)*Power(t2,2) - 
                  132*Power(t1,4)*Power(t2,3) + 
                  205*Power(t1,3)*Power(t2,4) + 
                  24*Power(t1,2)*Power(t2,5) - 67*t1*Power(t2,6) + 
                  10*Power(t2,7)))) + 
         Power(s,7)*(2*Power(s1,7)*t1*
             (-3*Power(s2,4) + 2*Power(s2,3)*(8*t1 + t2) + 
               Power(t2,2)*(-20*Power(t1,2) + 16*t1*t2 - 
                  3*Power(t2,2)) - 
               2*Power(s2,2)*
                (10*Power(t1,2) - 20*t1*t2 + Power(t2,2)) + 
               2*s2*t2*(-10*Power(t1,2) + 20*t1*t2 + Power(t2,2))) + 
            Power(s1,6)*(-6*Power(s2,5)*t1 + 
               Power(s2,4)*(54*Power(t1,2) + 46*t1*t2 - 
                  4*Power(t2,2)) + 
               Power(s2,3)*(-70*Power(t1,3) + 138*Power(t1,2)*t2 + 
                  87*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*t1*t2*(-50*Power(t1,3) + 110*Power(t1,2)*t2 - 
                  231*t1*Power(t2,2) + 2*Power(t2,3)) + 
               2*t1*Power(t2,2)*
                (-25*Power(t1,3) + 79*Power(t1,2)*t2 - 
                  58*t1*Power(t2,2) + 13*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-50*Power(t1,3) + 110*Power(t1,2)*t2 - 
                  101*t1*Power(t2,2) + 163*Power(t2,3))) - 
            2*s2*t1*Power(t1 - t2,3)*t2*
             (Power(s2,4)*(6*Power(t1,2) - 4*t1*t2 - 20*Power(t2,2)) - 
               t1*Power(t2,2)*
                (Power(t1,3) + 7*Power(t1,2)*t2 - 3*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,3)*(-10*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) + 20*Power(t2,3)) + 
               s2*t2*(3*Power(t1,4) + 10*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) - 30*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s2,2)*(4*Power(t1,4) + 7*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) + 
                  30*Power(t2,4))) + 
            Power(s1,5)*(Power(s2,5)*
                (6*Power(t1,2) + 38*t1*t2 - 8*Power(t2,2)) + 
               Power(s2,4)*(74*Power(t1,3) + 110*Power(t1,2)*t2 + 
                  41*t1*Power(t2,2) + 24*Power(t2,3)) + 
               2*t1*Power(t2,2)*
                (31*Power(t1,4) - 12*Power(t1,3)*t2 - 
                  72*Power(t1,2)*Power(t2,2) + 73*t1*Power(t2,3) - 
                  22*Power(t2,4)) + 
               s2*t1*t2*(62*Power(t1,4) - 316*Power(t1,3)*t2 + 
                  379*Power(t1,2)*Power(t2,2) + 115*t1*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s2,3)*(-278*Power(t1,4) + 650*Power(t1,3)*t2 - 
                  494*Power(t1,2)*Power(t2,2) - 71*t1*Power(t2,3) + 
                  30*Power(t2,4)) + 
               Power(s2,2)*(62*Power(t1,5) - 316*Power(t1,4)*t2 + 
                  547*Power(t1,3)*Power(t2,2) - 
                  217*Power(t1,2)*Power(t2,3) - 416*t1*Power(t2,4) - 
                  2*Power(t2,5))) + 
            Power(s1,4)*(Power(s2,5)*
                (40*Power(t1,3) + 106*Power(t1,2)*t2 - 
                  61*t1*Power(t2,2) + 44*Power(t2,3)) - 
               Power(s2,4)*(132*Power(t1,4) - 484*Power(t1,3)*t2 + 
                  601*Power(t1,2)*Power(t2,2) + 193*t1*Power(t2,3) + 
                  38*Power(t2,4)) + 
               Power(s2,3)*(-112*Power(t1,5) + 306*Power(t1,4)*t2 + 
                  8*Power(t1,3)*Power(t2,2) + 
                  62*Power(t1,2)*Power(t2,3) - 171*t1*Power(t2,4) - 
                  76*Power(t2,5)) + 
               2*t1*Power(t2,2)*
                (44*Power(t1,5) - 144*Power(t1,4)*t2 + 
                  153*Power(t1,3)*Power(t2,2) - 
                  40*Power(t1,2)*Power(t2,3) - 31*t1*Power(t2,4) + 
                  18*Power(t2,5)) + 
               s2*t1*t2*(88*Power(t1,5) - 416*Power(t1,4)*t2 + 
                  840*Power(t1,3)*Power(t2,2) - 
                  729*Power(t1,2)*Power(t2,3) + 44*t1*Power(t2,4) + 
                  18*Power(t2,5)) + 
               Power(s2,2)*(88*Power(t1,6) - 416*Power(t1,5)*t2 + 
                  588*Power(t1,4)*Power(t2,2) - 
                  231*Power(t1,3)*Power(t2,3) - 
                  3*Power(t1,2)*Power(t2,4) + 387*t1*Power(t2,5) + 
                  6*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,5)*
                (24*Power(t1,4) + 210*Power(t1,3)*t2 - 
                  449*Power(t1,2)*Power(t2,2) + 82*t1*Power(t2,3) - 
                  84*Power(t2,4)) - 
               2*t1*Power(t1 - t2,2)*Power(t2,2)*
                (4*Power(t1,4) + 28*Power(t1,3)*t2 - 
                  46*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,4)*(-210*Power(t1,5) + 576*Power(t1,4)*t2 - 
                  408*Power(t1,3)*Power(t2,2) + 
                  219*Power(t1,2)*Power(t2,3) + 113*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               s2*t1*t2*(-8*Power(t1,6) + 128*Power(t1,5)*t2 - 
                  136*Power(t1,4)*Power(t2,2) - 
                  227*Power(t1,3)*Power(t2,3) + 
                  209*Power(t1,2)*Power(t2,4) + 63*t1*Power(t2,5) - 
                  29*Power(t2,6)) + 
               Power(s2,3)*(152*Power(t1,6) - 650*Power(t1,5)*t2 + 
                  1013*Power(t1,4)*Power(t2,2) - 
                  564*Power(t1,3)*Power(t2,3) - 
                  36*Power(t1,2)*Power(t2,4) + 265*t1*Power(t2,5) + 
                  88*Power(t2,6)) - 
               Power(s2,2)*(8*Power(t1,7) - 128*Power(t1,6)*t2 + 
                  488*Power(t1,5)*Power(t2,2) - 
                  726*Power(t1,4)*Power(t2,3) + 
                  779*Power(t1,3)*Power(t2,4) - 
                  219*Power(t1,2)*Power(t2,5) + 41*t1*Power(t2,6) + 
                  6*Power(t2,7))) + 
            Power(s1,2)*(-2*t1*Power(t1 - t2,3)*Power(t2,2)*
                (11*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  8*t1*Power(t2,3) + Power(t2,4)) - 
               s2*t1*Power(t1 - t2,2)*t2*
                (22*Power(t1,5) - 126*Power(t1,4)*t2 + 
                  151*Power(t1,3)*Power(t2,2) + 
                  81*Power(t1,2)*Power(t2,3) + 9*t1*Power(t2,4) - 
                  26*Power(t2,5)) + 
               Power(s2,5)*(-18*Power(t1,5) + 242*Power(t1,4)*t2 - 
                  575*Power(t1,3)*Power(t2,2) + 
                  458*Power(t1,2)*Power(t2,3) - 107*t1*Power(t2,4) + 
                  68*Power(t2,5)) + 
               Power(s2,4)*(-34*Power(t1,6) - 70*Power(t1,5)*t2 + 
                  657*Power(t1,4)*Power(t2,2) - 
                  705*Power(t1,3)*Power(t2,3) - 
                  17*Power(t1,2)*Power(t2,4) + 143*t1*Power(t2,5) + 
                  18*Power(t2,6)) + 
               Power(s2,3)*(70*Power(t1,7) - 292*Power(t1,6)*t2 + 
                  237*Power(t1,5)*Power(t2,2) + 
                  308*Power(t1,4)*Power(t2,3) - 
                  505*Power(t1,3)*Power(t2,4) + 
                  186*Power(t1,2)*Power(t2,5) - 32*t1*Power(t2,6) - 
                  48*Power(t2,7)) + 
               Power(s2,2)*(-22*Power(t1,8) + 170*Power(t1,7)*t2 - 
                  407*Power(t1,6)*Power(t2,2) + 
                  272*Power(t1,5)*Power(t2,3) - 
                  183*Power(t1,4)*Power(t2,4) + 
                  140*Power(t1,3)*Power(t2,5) + 
                  226*Power(t1,2)*Power(t2,6) - 198*t1*Power(t2,7) + 
                  2*Power(t2,8))) + 
            s1*(2*Power(t1,3)*Power(t1 - t2,4)*Power(t2,2)*
                (Power(t1,2) - 3*Power(t2,2)) + 
               s2*t1*Power(t1 - t2,3)*t2*
                (2*Power(t1,5) - 22*Power(t1,4)*t2 - 
                  33*Power(t1,3)*Power(t2,2) - 
                  11*Power(t1,2)*Power(t2,3) + 41*t1*Power(t2,4) + 
                  7*Power(t2,5)) + 
               Power(s2,5)*(-14*Power(t1,6) + 88*Power(t1,5)*t2 - 
                  175*Power(t1,4)*Power(t2,2) + 
                  244*Power(t1,3)*Power(t2,3) - 
                  221*Power(t1,2)*Power(t2,4) + 96*t1*Power(t2,5) - 
                  20*Power(t2,6)) + 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (2*Power(t1,6) - 24*Power(t1,5)*t2 + 
                  69*Power(t1,4)*Power(t2,2) - 
                  95*Power(t1,3)*Power(t2,3) - 
                  78*Power(t1,2)*Power(t2,4) + 62*t1*Power(t2,5) + 
                  129*Power(t2,6)) + 
               Power(s2,4)*(30*Power(t1,7) - 206*Power(t1,6)*t2 + 
                  547*Power(t1,5)*Power(t2,2) - 
                  639*Power(t1,4)*Power(t2,3) + 
                  111*Power(t1,3)*Power(t2,4) + 
                  345*Power(t1,2)*Power(t2,5) - 180*t1*Power(t2,6) - 
                  10*Power(t2,7)) + 
               Power(s2,3)*(-18*Power(t1,8) + 140*Power(t1,7)*t2 - 
                  407*Power(t1,6)*Power(t2,2) + 
                  507*Power(t1,5)*Power(t2,3) - 
                  219*Power(t1,4)*Power(t2,4) - 
                  213*Power(t1,3)*Power(t2,5) + 
                  340*Power(t1,2)*Power(t2,6) - 140*t1*Power(t2,7) + 
                  10*Power(t2,8)))) + 
         Power(s,4)*(2*Power(s1,10)*t1*
             (s2*t1*(t1 - 2*t2)*t2 + Power(t1,2)*Power(t2,2) - 
               Power(s2,3)*(t1 + 2*t2) + 
               Power(s2,2)*(Power(t1,2) - 2*t1*t2 - 2*Power(t2,2))) + 
            2*Power(s2,2)*(s2 - t1)*t1*Power(t1 - t2,3)*Power(t2,3)*
             (Power(s2,4)*(5*t1 + 4*t2) + 
               Power(t1,2)*t2*(2*Power(t1,2) - t1*t2 - 2*Power(t2,2)) - 
               Power(s2,3)*(5*Power(t1,2) + 11*t1*t2 + 6*Power(t2,2)) - 
               2*Power(s2,2)*
                (Power(t1,3) - 3*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s2*t1*(2*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 10*Power(t2,3))) + 
            Power(s1,9)*(4*Power(s2,5)*t1 - 
               2*Power(t1,3)*Power(t2,2)*(5*t1 + t2) - 
               2*Power(s2,4)*
                (14*Power(t1,2) + 28*t1*t2 + Power(t2,2)) + 
               2*s2*Power(t1,2)*t2*
                (-5*Power(t1,2) + 17*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(34*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-10*Power(t1,3) + 34*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 23*Power(t2,3))) + 
            Power(s1,8)*(10*Power(s2,6)*t1 - 
               2*Power(s2,5)*
                (37*Power(t1,2) + 81*t1*t2 + 3*Power(t2,2)) - 
               2*Power(t1,3)*Power(t2,2)*
                (10*Power(t1,2) - 21*t1*t2 + 8*Power(t2,2)) - 
               4*s2*Power(t1,3)*t2*
                (5*Power(t1,2) - 16*t1*t2 + 30*Power(t2,2)) + 
               Power(s2,4)*(82*Power(t1,3) + 39*Power(t1,2)*t2 + 
                  138*t1*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-20*Power(t1,4) + 64*Power(t1,3)*t2 + 
                  26*Power(t1,2)*Power(t2,2) - 15*t1*Power(t2,3) - 
                  70*Power(t2,4)) + 
               2*Power(s2,3)*
                (Power(t1,4) + 14*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) + 84*t1*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,7)*(4*Power(s2,7)*t1 - 
               2*Power(s2,6)*
                (19*Power(t1,2) + 99*t1*t2 - 2*Power(t2,2)) + 
               2*Power(t1,3)*Power(t2,2)*
                (6*Power(t1,3) + 10*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,5)*(-56*Power(t1,3) + 40*Power(t1,2)*t2 + 
                  482*t1*Power(t2,2) + 34*Power(t2,3)) + 
               2*s2*Power(t1,2)*t2*
                (6*Power(t1,4) - 48*Power(t1,3)*t2 + 
                  40*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,4)*(218*Power(t1,4) - 165*Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) - 73*t1*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(s2,3)*(140*Power(t1,5) - 334*Power(t1,4)*t2 + 
                  298*Power(t1,3)*Power(t2,2) - 
                  453*Power(t1,2)*Power(t2,3) + 360*t1*Power(t2,4) + 
                  28*Power(t2,5)) + 
               Power(s2,2)*t1*
                (12*Power(t1,5) - 96*Power(t1,4)*t2 + 
                  268*Power(t1,3)*Power(t2,2) - 
                  587*Power(t1,2)*Power(t2,3) + 44*t1*Power(t2,4) + 
                  133*Power(t2,5))) - 
            Power(s1,6)*(10*Power(s2,7)*(7*t1 - t2)*t2 + 
               Power(s2,6)*(122*Power(t1,3) + 261*Power(t1,2)*t2 - 
                  620*t1*Power(t2,2) + 14*Power(t2,3)) + 
               2*Power(t1,3)*Power(t2,2)*
                (-13*Power(t1,4) + 34*Power(t1,3)*t2 - 
                  40*Power(t1,2)*Power(t2,2) + 16*t1*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,5)*(-236*Power(t1,4) - 203*Power(t1,3)*t2 - 
                  206*Power(t1,2)*Power(t2,2) + 469*t1*Power(t2,3) + 
                  84*Power(t2,4)) + 
               2*s2*Power(t1,2)*t2*
                (-13*Power(t1,5) + 70*Power(t1,4)*t2 - 
                  186*Power(t1,3)*Power(t2,2) + 
                  242*Power(t1,2)*Power(t2,3) - 138*t1*Power(t2,4) + 
                  18*Power(t2,5)) + 
               Power(s2,4)*(66*Power(t1,5) + 138*Power(t1,4)*t2 - 
                  87*Power(t1,3)*Power(t2,2) - 
                  32*Power(t1,2)*Power(t2,3) - 4*t1*Power(t2,4) + 
                  28*Power(t2,5)) + 
               Power(s2,3)*(74*Power(t1,6) - 307*Power(t1,5)*t2 + 
                  400*Power(t1,4)*Power(t2,2) - 
                  273*Power(t1,3)*Power(t2,3) + 
                  1069*Power(t1,2)*Power(t2,4) - 491*t1*Power(t2,5) - 
                  32*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-26*Power(t1,6) + 140*Power(t1,5)*t2 - 
                  245*Power(t1,4)*Power(t2,2) + 
                  778*Power(t1,3)*Power(t2,3) - 
                  1155*Power(t1,2)*Power(t2,4) + 102*t1*Power(t2,5) + 
                  152*Power(t2,6))) - 
            Power(s1,5)*(2*Power(s2,8)*(t1 - t2)*t2 + 
               Power(s2,7)*(24*Power(t1,3) + 179*Power(t1,2)*t2 - 
                  227*t1*Power(t2,2) + 48*Power(t2,3)) + 
               Power(s2,6)*(38*Power(t1,4) - 63*Power(t1,3)*t2 - 
                  1046*Power(t1,2)*Power(t2,2) + 738*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               2*Power(t1,3)*Power(t2,2)*
                (Power(t1,5) + 9*Power(t1,4)*t2 - 
                  36*Power(t1,3)*Power(t2,2) + 
                  73*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) + 
                  Power(t2,5)) - 
               Power(s2,5)*(244*Power(t1,5) - 209*Power(t1,4)*t2 - 
                  825*Power(t1,3)*Power(t2,2) + 
                  90*Power(t1,2)*Power(t2,3) + 125*t1*Power(t2,4) + 
                  116*Power(t2,5)) + 
               s2*Power(t1,2)*t2*
                (2*Power(t1,6) - 58*Power(t1,5)*t2 + 
                  24*Power(t1,4)*Power(t2,2) + 
                  150*Power(t1,3)*Power(t2,3) - 
                  423*Power(t1,2)*Power(t2,4) + 349*t1*Power(t2,5) - 
                  78*Power(t2,6)) + 
               Power(s2,4)*(266*Power(t1,6) - 490*Power(t1,5)*t2 - 
                  399*Power(t1,4)*Power(t2,2) + 
                  662*Power(t1,3)*Power(t2,3) + 
                  138*Power(t1,2)*Power(t2,4) + 103*t1*Power(t2,5) - 
                  42*Power(t2,6)) + 
               Power(s2,2)*t1*
                (2*Power(t1,7) - 58*Power(t1,6)*t2 + 
                  215*Power(t1,5)*Power(t2,2) - 
                  113*Power(t1,4)*Power(t2,3) - 
                  403*Power(t1,3)*Power(t2,4) + 
                  723*Power(t1,2)*Power(t2,5) - 71*t1*Power(t2,6) - 
                  97*Power(t2,7)) + 
               Power(s2,3)*(-86*Power(t1,7) + 261*Power(t1,6)*t2 + 
                  131*Power(t1,5)*Power(t2,2) - 
                  188*Power(t1,4)*Power(t2,3) + 
                  134*Power(t1,3)*Power(t2,4) - 
                  1282*Power(t1,2)*Power(t2,5) + 498*t1*Power(t2,6) + 
                  18*Power(t2,7))) + 
            s1*s2*Power(t2,2)*
             (-(Power(t1,4)*Power(t1 - t2,3)*Power(t2,3)*(8*t1 + t2)) + 
               2*Power(s2,7)*t2*
                (-4*Power(t1,3) + 5*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3)) + 
               s2*Power(t1,3)*Power(t1 - t2,2)*t2*
                (16*Power(t1,4) - 34*Power(t1,3)*t2 + 
                  29*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               2*Power(s2,6)*
                (9*Power(t1,5) + 4*Power(t1,4)*t2 - 
                  2*Power(t1,3)*Power(t2,2) - 
                  26*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) - 
                  4*Power(t2,5)) + 
               Power(s2,4)*t1*
                (104*Power(t1,6) - 382*Power(t1,5)*t2 + 
                  722*Power(t1,4)*Power(t2,2) - 
                  752*Power(t1,3)*Power(t2,3) + 
                  245*Power(t1,2)*Power(t2,4) + 124*t1*Power(t2,5) - 
                  51*Power(t2,6)) - 
               Power(s2,5)*(72*Power(t1,6) - 158*Power(t1,5)*t2 + 
                  302*Power(t1,4)*Power(t2,2) - 
                  323*Power(t1,3)*Power(t2,3) + 
                  42*Power(t1,2)*Power(t2,4) + 49*t1*Power(t2,5) + 
                  10*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (14*Power(t1,7) - 116*Power(t1,6)*t2 + 
                  322*Power(t1,5)*Power(t2,2) - 
                  478*Power(t1,4)*Power(t2,3) + 
                  306*Power(t1,3)*Power(t2,4) - 
                  7*Power(t1,2)*Power(t2,5) - 8*t1*Power(t2,6) - 
                  33*Power(t2,7)) + 
               Power(s2,3)*t1*
                (-64*Power(t1,7) + 324*Power(t1,6)*t2 - 
                  684*Power(t1,5)*Power(t2,2) + 
                  858*Power(t1,4)*Power(t2,3) - 
                  457*Power(t1,3)*Power(t2,4) - 
                  96*Power(t1,2)*Power(t2,5) + 83*t1*Power(t2,6) + 
                  28*Power(t2,7))) + 
            Power(s1,3)*(-2*Power(t1,3)*Power(t1 - t2,2)*Power(t2,4)*
                (5*Power(t1,3) - 11*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               2*Power(s2,8)*t2*
                (-3*Power(t1,3) + 11*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 6*Power(t2,3)) - 
               Power(s2,7)*(12*Power(t1,5) + 73*Power(t1,4)*t2 - 
                  447*Power(t1,3)*Power(t2,2) + 
                  679*Power(t1,2)*Power(t2,3) - 201*t1*Power(t2,4) + 
                  88*Power(t2,5)) + 
               Power(s2,6)*(52*Power(t1,6) + 87*Power(t1,5)*t2 - 
                  710*Power(t1,4)*Power(t2,2) + 
                  973*Power(t1,3)*Power(t2,3) + 
                  66*Power(t1,2)*Power(t2,4) - 35*t1*Power(t2,5) - 
                  64*Power(t2,6)) + 
               s2*Power(t1,2)*Power(t2,2)*
                (-12*Power(t1,7) + 14*Power(t1,6)*t2 + 
                  50*Power(t1,5)*Power(t2,2) - 
                  181*Power(t1,4)*Power(t2,3) + 
                  264*Power(t1,3)*Power(t2,4) - 
                  196*Power(t1,2)*Power(t2,5) + 47*t1*Power(t2,6) + 
                  14*Power(t2,7)) + 
               Power(s2,5)*(-80*Power(t1,7) + 77*Power(t1,6)*t2 + 
                  529*Power(t1,5)*Power(t2,2) - 
                  1073*Power(t1,4)*Power(t2,3) - 
                  48*Power(t1,3)*Power(t2,4) + 
                  434*Power(t1,2)*Power(t2,5) + 112*t1*Power(t2,6) + 
                  42*Power(t2,7)) + 
               Power(s2,3)*t1*
                (-12*Power(t1,8) + 72*Power(t1,7)*t2 - 
                  94*Power(t1,6)*Power(t2,2) + 
                  219*Power(t1,5)*Power(t2,3) - 
                  862*Power(t1,4)*Power(t2,4) + 
                  1685*Power(t1,3)*Power(t2,5) - 
                  941*Power(t1,2)*Power(t2,6) + 285*t1*Power(t2,7) - 
                  141*Power(t2,8)) + 
               Power(s2,2)*t1*t2*
                (-12*Power(t1,8) + 70*Power(t1,7)*t2 - 
                  107*Power(t1,6)*Power(t2,2) + 
                  259*Power(t1,5)*Power(t2,3) - 
                  519*Power(t1,4)*Power(t2,4) + 
                  218*Power(t1,3)*Power(t2,5) + 
                  98*Power(t1,2)*Power(t2,6) - 40*t1*Power(t2,7) + 
                  3*Power(t2,8)) + 
               Power(s2,4)*(52*Power(t1,8) - 145*Power(t1,7)*t2 - 
                  216*Power(t1,6)*Power(t2,2) + 
                  425*Power(t1,5)*Power(t2,3) + 
                  719*Power(t1,4)*Power(t2,4) - 
                  1450*Power(t1,3)*Power(t2,5) + 
                  190*Power(t1,2)*Power(t2,6) + 64*t1*Power(t2,7) + 
                  6*Power(t2,8))) + 
            Power(s1,2)*t2*(2*Power(t1,4)*Power(t1 - t2,3)*Power(t2,3)*
                (2*Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               s2*Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                (12*Power(t1,4) - 20*Power(t1,3)*t2 + t1*Power(t2,3) - 
                  23*Power(t2,4)) - 
               2*Power(s2,8)*
                (Power(t1,4) - 7*Power(t1,3)*t2 + 
                  13*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,7)*(-17*Power(t1,5) + 112*Power(t1,4)*t2 - 
                  260*Power(t1,3)*Power(t2,2) + 
                  300*Power(t1,2)*Power(t2,3) - 77*t1*Power(t2,4) + 
                  42*Power(t2,5)) + 
               Power(s2,6)*(48*Power(t1,6) - 297*Power(t1,5)*t2 + 
                  847*Power(t1,4)*Power(t2,2) - 
                  1038*Power(t1,3)*Power(t2,3) + 
                  247*Power(t1,2)*Power(t2,4) + 41*t1*Power(t2,5) + 
                  42*Power(t2,6)) + 
               Power(s2,4)*t1*
                (10*Power(t1,7) - 116*Power(t1,6)*t2 + 
                  567*Power(t1,5)*Power(t2,2) - 
                  1091*Power(t1,4)*Power(t2,3) + 
                  604*Power(t1,3)*Power(t2,4) + 
                  364*Power(t1,2)*Power(t2,5) - 119*t1*Power(t2,6) - 
                  95*Power(t2,7)) - 
               Power(s2,2)*Power(t1,2)*t2*
                (Power(t1,7) + 34*Power(t1,6)*t2 - 
                  109*Power(t1,5)*Power(t2,2) + 
                  244*Power(t1,4)*Power(t2,3) - 
                  332*Power(t1,3)*Power(t2,4) + 
                  170*Power(t1,2)*Power(t2,5) + 2*t1*Power(t2,6) - 
                  10*Power(t2,7)) - 
               Power(s2,5)*(38*Power(t1,7) - 270*Power(t1,6)*t2 + 
                  1077*Power(t1,5)*Power(t2,2) - 
                  1644*Power(t1,4)*Power(t2,3) + 
                  720*Power(t1,3)*Power(t2,4) + 
                  120*Power(t1,2)*Power(t2,5) + 17*t1*Power(t2,6) + 
                  8*Power(t2,7)) + 
               Power(s2,3)*t1*
                (-Power(t1,8) + 18*Power(t1,7)*t2 - 
                  51*Power(t1,6)*Power(t2,2) + 
                  168*Power(t1,5)*Power(t2,3) + 
                  97*Power(t1,4)*Power(t2,4) - 
                  644*Power(t1,3)*Power(t2,5) + 
                  341*Power(t1,2)*Power(t2,6) + 14*t1*Power(t2,7) + 
                  24*Power(t2,8))) - 
            Power(s1,4)*(2*Power(s2,8)*t2*
                (3*Power(t1,2) - 4*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,7)*(32*Power(t1,4) + 165*Power(t1,3)*t2 - 
                  596*Power(t1,2)*Power(t2,2) + 305*t1*Power(t2,3) - 
                  92*Power(t2,4)) - 
               Power(s2,6)*(88*Power(t1,5) + 165*Power(t1,4)*t2 - 
                  165*Power(t1,3)*Power(t2,2) - 
                  1012*Power(t1,2)*Power(t2,3) + 337*t1*Power(t2,4) + 
                  36*Power(t2,5)) + 
               2*Power(t1,3)*Power(t2,2)*
                (4*Power(t1,6) - 13*Power(t1,5)*t2 + 
                  26*Power(t1,4)*Power(t2,2) - 
                  13*Power(t1,3)*Power(t2,3) - 
                  29*Power(t1,2)*Power(t2,4) + 31*t1*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(s2,5)*(50*Power(t1,6) + 95*Power(t1,5)*t2 - 
                  40*Power(t1,4)*Power(t2,2) - 
                  1175*Power(t1,3)*Power(t2,3) + 
                  640*Power(t1,2)*Power(t2,4) + 44*t1*Power(t2,5) + 
                  94*Power(t2,6)) + 
               Power(s2,4)*(40*Power(t1,7) - 213*Power(t1,6)*t2 + 
                  189*Power(t1,5)*Power(t2,2) + 
                  1367*Power(t1,4)*Power(t2,3) - 
                  1750*Power(t1,3)*Power(t2,4) + 
                  52*Power(t1,2)*Power(t2,5) - 93*t1*Power(t2,6) + 
                  26*Power(t2,7)) + 
               s2*Power(t1,2)*t2*
                (8*Power(t1,7) - 64*Power(t1,6)*t2 + 
                  210*Power(t1,5)*Power(t2,2) - 
                  364*Power(t1,4)*Power(t2,3) + 
                  350*Power(t1,3)*Power(t2,4) - 
                  75*Power(t1,2)*Power(t2,5) - 101*t1*Power(t2,6) + 
                  56*Power(t2,7)) - 
               Power(s2,3)*(42*Power(t1,8) - 178*Power(t1,7)*t2 + 
                  44*Power(t1,6)*Power(t2,2) + 
                  600*Power(t1,5)*Power(t2,3) - 
                  1117*Power(t1,4)*Power(t2,4) + 
                  681*Power(t1,3)*Power(t2,5) - 
                  859*Power(t1,2)*Power(t2,6) + 349*t1*Power(t2,7) + 
                  4*Power(t2,8)) + 
               Power(s2,2)*t1*
                (8*Power(t1,8) - 64*Power(t1,7)*t2 + 
                  156*Power(t1,6)*Power(t2,2) - 
                  311*Power(t1,5)*Power(t2,3) + 
                  82*Power(t1,4)*Power(t2,4) - 
                  30*Power(t1,3)*Power(t2,5) + 
                  9*Power(t1,2)*Power(t2,6) - 23*t1*Power(t2,7) + 
                  30*Power(t2,8)))) + 
         Power(s,3)*(2*Power(s2,3)*Power(s2 - t1,2)*t1*Power(t1 - t2,3)*
             Power(t2,4)*(s2 + t2)*
             (-(t1*(2*t1 + t2)) + s2*(3*t1 + 2*t2)) + 
            Power(s1,10)*(s2 - t1)*t1*
             (-2*s2*t1*(t1 - 2*t2)*t2 - 2*Power(t1,2)*Power(t2,2) + 
               Power(s2,3)*(2*t1 + 17*t2) + 
               Power(s2,2)*(-2*Power(t1,2) + 4*t1*t2 + 17*Power(t2,2))) \
+ Power(s1,9)*(-2*Power(s2,6)*t1 + 
               2*Power(t1,4)*(t1 - 3*t2)*Power(t2,2) + 
               Power(s2,5)*(16*Power(t1,2) + 87*t1*t2 + 
                  2*Power(t2,2)) + 
               2*s2*Power(t1,3)*t2*
                (Power(t1,2) - 2*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,3)*t1*
                (8*Power(t1,3) + 15*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) - 108*Power(t2,3)) - 
               Power(s2,4)*(24*Power(t1,3) + 100*Power(t1,2)*t2 + 
                  23*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,2)*Power(t1,2)*
                (2*Power(t1,3) - 4*Power(t1,2)*t2 + 19*t1*Power(t2,2) + 
                  67*Power(t2,3))) - 
            Power(s1,8)*(2*Power(s2,7)*t1 + 4*Power(t1,6)*Power(t2,2) - 
               2*Power(s2,6)*(9*Power(t1,2) + 80*t1*t2 + Power(t2,2)) + 
               2*s2*Power(t1,3)*t2*
                (2*Power(t1,3) - 12*Power(t1,2)*t2 + 
                  16*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,5)*(10*Power(t1,3) + 178*Power(t1,2)*t2 + 
                  355*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,3)*t1*
                (-28*Power(t1,4) + 16*Power(t1,3)*t2 + 
                  58*Power(t1,2)*Power(t2,2) - 195*t1*Power(t2,3) - 
                  299*Power(t2,4)) + 
               2*Power(s2,4)*
                (15*Power(t1,4) - 7*Power(t1,3)*t2 - 
                  130*Power(t1,2)*Power(t2,2) + 95*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*Power(t1,2)*
                (4*Power(t1,4) - 24*Power(t1,3)*t2 - 
                  4*Power(t1,2)*Power(t2,2) - 44*t1*Power(t2,3) + 
                  159*Power(t2,4))) + 
            Power(s1,7)*(Power(s2,7)*
                (2*Power(t1,2) + 99*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,6)*(38*Power(t1,3) + 89*Power(t1,2)*t2 - 
                  709*t1*Power(t2,2) - 12*Power(t2,3)) - 
               4*Power(t1,4)*Power(t2,2)*
                (Power(t1,3) - 3*Power(t1,2)*t2 + 4*t1*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,3)*t1*t2*
                (-106*Power(t1,4) + 408*Power(t1,3)*t2 + 
                  122*Power(t1,2)*Power(t2,2) - 337*t1*Power(t2,3) - 
                  462*Power(t2,4)) + 
               Power(s2,5)*(-90*Power(t1,4) - 437*Power(t1,3)*t2 + 
                  549*Power(t1,2)*Power(t2,2) + 483*t1*Power(t2,3) + 
                  10*Power(t2,4)) - 
               2*s2*Power(t1,3)*t2*
                (2*Power(t1,4) - 8*Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) - 46*t1*Power(t2,3) + 
                  29*Power(t2,4)) + 
               Power(s2,4)*(54*Power(t1,5) + 343*Power(t1,4)*t2 - 
                  381*Power(t1,3)*Power(t2,2) - 
                  312*Power(t1,2)*Power(t2,3) + 745*t1*Power(t2,4) + 
                  20*Power(t2,5)) + 
               Power(s2,2)*Power(t1,2)*
                (-4*Power(t1,5) + 16*Power(t1,4)*t2 - 
                  98*Power(t1,3)*Power(t2,2) + 
                  85*Power(t1,2)*Power(t2,3) - 222*t1*Power(t2,4) + 
                  219*Power(t2,5))) + 
            s1*Power(s2,2)*Power(t2,3)*
             (Power(t1,4)*Power(t1 - t2,2)*t2*
                (4*Power(t1,3) - 6*Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               2*Power(s2,6)*t2*
                (-4*Power(t1,3) + 5*Power(t1,2)*t2 - t1*Power(t2,2) + 
                  Power(t2,3)) + 
               2*Power(s2,5)*t2*
                (25*Power(t1,4) - 37*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,4)*t1*
                (6*Power(t1,5) + 70*Power(t1,4)*t2 - 
                  148*Power(t1,3)*Power(t2,2) + 
                  47*Power(t1,2)*Power(t2,3) + 30*t1*Power(t2,4) - 
                  5*Power(t2,5)) + 
               Power(s2,3)*t1*
                (16*Power(t1,6) + 6*Power(t1,5)*t2 - 
                  98*Power(t1,4)*Power(t2,2) + 
                  63*Power(t1,3)*Power(t2,3) + 
                  43*Power(t1,2)*Power(t2,4) - 19*t1*Power(t2,5) - 
                  7*Power(t2,6)) + 
               s2*Power(t1,3)*
                (4*Power(t1,6) - 26*Power(t1,5)*t2 + 
                  46*Power(t1,4)*Power(t2,2) - 
                  47*Power(t1,3)*Power(t2,3) + 
                  39*Power(t1,2)*Power(t2,4) - 13*t1*Power(t2,5) - 
                  3*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*
                (-14*Power(t1,6) + 44*Power(t1,5)*t2 - 
                  18*Power(t1,4)*Power(t2,2) + 
                  7*Power(t1,3)*Power(t2,3) - 
                  53*Power(t1,2)*Power(t2,4) + 21*t1*Power(t2,5) + 
                  11*Power(t2,6))) + 
            Power(s1,6)*(Power(s2,8)*(13*t1 - 2*t2)*t2 + 
               Power(s2,7)*(18*Power(t1,3) + 213*Power(t1,2)*t2 - 
                  422*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,5)*t1*
                (-50*Power(t1,4) + 203*Power(t1,3)*t2 + 
                  970*Power(t1,2)*Power(t2,2) - 947*t1*Power(t2,3) - 
                  87*Power(t2,4)) + 
               2*Power(t1,4)*Power(t2,2)*
                (Power(t1,4) - Power(t1,2)*Power(t2,2) + 
                  12*t1*Power(t2,3) - 15*Power(t2,4)) + 
               Power(s2,6)*(-14*Power(t1,4) - 431*Power(t1,3)*t2 - 
                  486*Power(t1,2)*Power(t2,2) + 1274*t1*Power(t2,3) + 
                  30*Power(t2,4)) + 
               s2*Power(t1,3)*t2*
                (2*Power(t1,5) - 22*Power(t1,4)*t2 + 
                  32*Power(t1,3)*Power(t2,2) - 
                  10*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) + 
                  62*Power(t2,5)) + 
               Power(s2,2)*Power(t1,2)*
                (2*Power(t1,6) - 22*Power(t1,5)*t2 - 
                  53*Power(t1,4)*Power(t2,2) + 
                  173*Power(t1,3)*Power(t2,3) - 
                  173*Power(t1,2)*Power(t2,4) + 264*t1*Power(t2,5) - 
                  171*Power(t2,6)) + 
               Power(s2,4)*(72*Power(t1,6) + 47*Power(t1,5)*t2 - 
                  684*Power(t1,4)*Power(t2,2) + 
                  992*Power(t1,3)*Power(t2,3) + 
                  537*Power(t1,2)*Power(t2,4) - 1233*t1*Power(t2,5) - 
                  20*Power(t2,6)) + 
               Power(s2,3)*t1*
                (-28*Power(t1,6) - 25*Power(t1,5)*t2 + 
                  506*Power(t1,4)*Power(t2,2) - 
                  891*Power(t1,3)*Power(t2,3) - 
                  365*Power(t1,2)*Power(t2,4) + 356*t1*Power(t2,5) + 
                  423*Power(t2,6))) + 
            Power(s1,5)*(Power(s2,8)*t2*
                (39*Power(t1,2) - 54*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,7)*(22*Power(t1,4) + 75*Power(t1,3)*t2 - 
                  820*Power(t1,2)*Power(t2,2) + 732*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               2*Power(t1,4)*Power(t2,2)*
                (Power(t1,5) - 3*Power(t1,4)*t2 + 
                  9*Power(t1,3)*Power(t2,2) - 
                  13*Power(t1,2)*Power(t2,3) - 3*t1*Power(t2,4) + 
                  9*Power(t2,5)) - 
               Power(s2,6)*(60*Power(t1,5) + 389*Power(t1,4)*t2 - 
                  1113*Power(t1,3)*Power(t2,2) - 
                  623*Power(t1,2)*Power(t2,3) + 1147*t1*Power(t2,4) + 
                  40*Power(t2,5)) + 
               s2*Power(t1,3)*t2*
                (2*Power(t1,6) - 12*Power(t1,5)*t2 + 
                  50*Power(t1,4)*Power(t2,2) - 
                  96*Power(t1,3)*Power(t2,3) + 
                  131*Power(t1,2)*Power(t2,4) - 61*t1*Power(t2,5) - 
                  12*Power(t2,6)) + 
               Power(s2,5)*(50*Power(t1,6) + 476*Power(t1,5)*t2 - 
                  915*Power(t1,4)*Power(t2,2) - 
                  423*Power(t1,3)*Power(t2,3) + 
                  1162*Power(t1,2)*Power(t2,4) - 434*t1*Power(t2,5) - 
                  10*Power(t2,6)) - 
               Power(s2,4)*(6*Power(t1,7) + 238*Power(t1,6)*t2 - 
                  752*Power(t1,5)*Power(t2,2) + 
                  230*Power(t1,4)*Power(t2,3) + 
                  631*Power(t1,3)*Power(t2,4) + 
                  893*Power(t1,2)*Power(t2,5) - 1135*t1*Power(t2,6) - 
                  10*Power(t2,7)) + 
               Power(s2,2)*Power(t1,2)*
                (2*Power(t1,7) - 12*Power(t1,6)*t2 + 
                  29*Power(t1,5)*Power(t2,2) - 
                  23*Power(t1,4)*Power(t2,3) - 
                  63*Power(t1,3)*Power(t2,4) + 
                  63*Power(t1,2)*Power(t2,5) - 71*t1*Power(t2,6) + 
                  57*Power(t2,7)) - 
               Power(s2,3)*t1*
                (8*Power(t1,7) - 47*Power(t1,6)*t2 + 
                  177*Power(t1,5)*Power(t2,2) + 
                  166*Power(t1,4)*Power(t2,3) - 
                  208*Power(t1,3)*Power(t2,4) - 
                  775*Power(t1,2)*Power(t2,5) + 354*t1*Power(t2,6) + 
                  224*Power(t2,7))) + 
            Power(s1,2)*s2*Power(t2,2)*
             (-(Power(t1,4)*Power(t1 - t2,2)*Power(t2,3)*
                  (2*Power(t1,2) - 7*t1*t2 + 6*Power(t2,2))) + 
               Power(s2,7)*(-13*Power(t1,4) + 55*Power(t1,3)*t2 - 
                  69*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s2,6)*(9*Power(t1,5) - 45*Power(t1,4)*t2 + 
                  19*Power(t1,3)*Power(t2,2) + 
                  109*Power(t1,2)*Power(t2,3) - 46*t1*Power(t2,4) - 
                  8*Power(t2,5)) + 
               Power(s2,5)*(30*Power(t1,6) - 43*Power(t1,5)*t2 + 
                  45*Power(t1,4)*Power(t2,2) - 
                  177*Power(t1,3)*Power(t2,3) + 
                  115*Power(t1,2)*Power(t2,4) + 18*t1*Power(t2,5) + 
                  2*Power(t2,6)) + 
               s2*Power(t1,3)*t2*
                (-9*Power(t1,6) + 42*Power(t1,5)*t2 - 
                  84*Power(t1,4)*Power(t2,2) + 
                  67*Power(t1,3)*Power(t2,3) + 
                  9*Power(t1,2)*Power(t2,4) - 37*t1*Power(t2,5) + 
                  12*Power(t2,6)) + 
               Power(s2,4)*t1*
                (-42*Power(t1,6) + 110*Power(t1,5)*t2 - 
                  165*Power(t1,4)*Power(t2,2) + 
                  274*Power(t1,3)*Power(t2,3) - 
                  196*Power(t1,2)*Power(t2,4) - 78*t1*Power(t2,5) + 
                  63*Power(t2,6)) + 
               Power(s2,3)*t1*
                (23*Power(t1,7) - 137*Power(t1,6)*t2 + 
                  308*Power(t1,5)*Power(t2,2) - 
                  432*Power(t1,4)*Power(t2,3) + 
                  378*Power(t1,3)*Power(t2,4) - 
                  75*Power(t1,2)*Power(t2,5) - 21*t1*Power(t2,6) - 
                  18*Power(t2,7)) + 
               Power(s2,2)*(-7*Power(t1,9) + 69*Power(t1,8)*t2 - 
                  194*Power(t1,7)*Power(t2,2) + 
                  325*Power(t1,6)*Power(t2,3) - 
                  331*Power(t1,5)*Power(t2,4) + 
                  128*Power(t1,4)*Power(t2,5) + 
                  6*Power(t1,2)*Power(t2,7))) + 
            Power(s1,3)*t2*(-2*Power(t1,5)*Power(t1 - t2,3)*
                Power(t2,3)*(t1 + 2*t2) - 
               s2*Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                (6*Power(t1,4) - 12*Power(t1,3)*t2 + 
                  15*Power(t1,2)*Power(t2,2) - 5*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,8)*(13*Power(t1,4) - 86*Power(t1,3)*t2 + 
                  147*Power(t1,2)*Power(t2,2) - 64*t1*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,7)*(-54*Power(t1,5) + 80*Power(t1,4)*t2 + 
                  271*Power(t1,3)*Power(t2,2) - 
                  654*Power(t1,2)*Power(t2,3) + 255*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               Power(s2,6)*(100*Power(t1,6) - 50*Power(t1,5)*t2 - 
                  526*Power(t1,4)*Power(t2,2) + 
                  892*Power(t1,3)*Power(t2,3) - 
                  214*Power(t1,2)*Power(t2,4) - 131*t1*Power(t2,5) - 
                  12*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (-4*Power(t1,7) + 13*Power(t1,6)*t2 + 
                  43*Power(t1,5)*Power(t2,2) - 
                  98*Power(t1,4)*Power(t2,3) - 
                  9*Power(t1,3)*Power(t2,4) + 
                  57*Power(t1,2)*Power(t2,5) + 2*t1*Power(t2,6) - 
                  7*Power(t2,7)) + 
               Power(s2,5)*(-94*Power(t1,7) + 156*Power(t1,6)*t2 + 
                  361*Power(t1,5)*Power(t2,2) - 
                  1083*Power(t1,4)*Power(t2,3) + 
                  517*Power(t1,3)*Power(t2,4) + 
                  456*Power(t1,2)*Power(t2,5) - 249*t1*Power(t2,6) - 
                  2*Power(t2,7)) + 
               Power(s2,4)*t1*
                (39*Power(t1,7) - 116*Power(t1,6)*t2 - 
                  316*Power(t1,5)*Power(t2,2) + 
                  1179*Power(t1,4)*Power(t2,3) - 
                  1110*Power(t1,3)*Power(t2,4) + 
                  265*Power(t1,2)*Power(t2,5) - 194*t1*Power(t2,6) + 
                  167*Power(t2,7)) + 
               Power(s2,3)*t1*
                (-4*Power(t1,8) + 20*Power(t1,7)*t2 + 
                  96*Power(t1,6)*Power(t2,2) - 
                  501*Power(t1,5)*Power(t2,3) + 
                  828*Power(t1,4)*Power(t2,4) - 
                  554*Power(t1,3)*Power(t2,5) + 
                  239*Power(t1,2)*Power(t2,6) - 88*t1*Power(t2,7) - 
                  6*Power(t2,8))) + 
            Power(s1,4)*(2*Power(t1,4)*Power(t1 - t2,2)*Power(t2,4)*
                (Power(t1,2) - 8*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,8)*t2*
                (39*Power(t1,3) - 127*Power(t1,2)*t2 + 
                  86*t1*Power(t2,2) - 20*Power(t2,3)) + 
               Power(s2,7)*t1*
                (8*Power(t1,4) - 93*Power(t1,3)*t2 - 
                  315*Power(t1,2)*Power(t2,2) + 1136*t1*Power(t2,3) - 
                  622*Power(t2,4)) + 
               s2*Power(t1,3)*Power(t2,2)*
                (4*Power(t1,6) - 10*Power(t1,5)*t2 + 
                  8*Power(t1,4)*Power(t2,2) + 
                  27*Power(t1,3)*Power(t2,3) - 
                  81*Power(t1,2)*Power(t2,4) + 65*t1*Power(t2,5) - 
                  14*Power(t2,6)) + 
               Power(s2,6)*(-28*Power(t1,6) + 71*Power(t1,5)*t2 + 
                  768*Power(t1,4)*Power(t2,2) - 
                  1380*Power(t1,3)*Power(t2,3) - 
                  131*Power(t1,2)*Power(t2,4) + 536*t1*Power(t2,5) + 
                  30*Power(t2,6)) + 
               Power(s2,2)*Power(t1,2)*t2*
                (4*Power(t1,7) + 4*Power(t1,6)*t2 - 
                  60*Power(t1,5)*Power(t2,2) + 
                  140*Power(t1,4)*Power(t2,3) - 
                  42*Power(t1,3)*Power(t2,4) + 
                  4*Power(t1,2)*Power(t2,5) - 42*t1*Power(t2,6) + 
                  7*Power(t2,7)) + 
               Power(s2,5)*(36*Power(t1,7) + 7*Power(t1,6)*t2 - 
                  721*Power(t1,5)*Power(t2,2) + 
                  1556*Power(t1,4)*Power(t2,3) - 
                  442*Power(t1,3)*Power(t2,4) - 
                  971*Power(t1,2)*Power(t2,5) + 499*t1*Power(t2,6) + 
                  8*Power(t2,7)) - 
               Power(s2,4)*(20*Power(t1,8) + 34*Power(t1,7)*t2 - 
                  583*Power(t1,6)*Power(t2,2) + 
                  1705*Power(t1,5)*Power(t2,3) - 
                  1374*Power(t1,4)*Power(t2,4) + 
                  173*Power(t1,3)*Power(t2,5) - 
                  710*Power(t1,2)*Power(t2,6) + 600*t1*Power(t2,7) + 
                  2*Power(t2,8)) + 
               Power(s2,3)*t1*
                (4*Power(t1,8) + 6*Power(t1,7)*t2 - 
                  210*Power(t1,6)*Power(t2,2) + 
                  617*Power(t1,5)*Power(t2,3) - 
                  814*Power(t1,4)*Power(t2,4) + 
                  720*Power(t1,3)*Power(t2,5) - 
                  721*Power(t1,2)*Power(t2,6) + 260*t1*Power(t2,7) + 
                  61*Power(t2,8)))) + 
         Power(s,6)*(2*Power(s1,8)*t1*
             (3*Power(s2,4) + 15*s2*t1*(t1 - 2*t2)*t2 - 
               2*Power(s2,3)*(7*t1 + 3*t2) + 
               Power(s2,2)*(15*Power(t1,2) - 30*t1*t2 - 
                  2*Power(t2,2)) + 
               Power(t2,2)*(15*Power(t1,2) - 9*t1*t2 + Power(t2,2))) + 
            Power(s1,7)*(16*Power(s2,5)*t1 - 
               6*Power(s2,4)*t1*(17*t1 + 13*t2) + 
               2*t1*Power(t2,2)*
                (5*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  32*t1*Power(t2,2) - 5*Power(t2,3)) + 
               2*s2*t1*t2*(5*Power(t1,3) + 5*Power(t1,2)*t2 + 
                  73*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,3)*(122*Power(t1,3) - 173*Power(t1,2)*t2 - 
                  39*t1*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(10*Power(t1,4) + 10*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) - 83*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,6)*(6*Power(s2,6)*t1 - 
               2*Power(s2,5)*
                (27*Power(t1,2) + 68*t1*t2 - 7*Power(t2,2)) - 
               2*Power(s2,4)*
                (21*Power(t1,3) + 64*Power(t1,2)*t2 + 
                  25*t1*Power(t2,2) - 5*Power(t2,3)) + 
               Power(s2,3)*(286*Power(t1,4) - 605*Power(t1,3)*t2 + 
                  442*Power(t1,2)*Power(t2,2) - 95*t1*Power(t2,3) - 
                  18*Power(t2,4)) - 
               2*s2*t1*t2*(49*Power(t1,4) - 212*Power(t1,3)*t2 + 
                  278*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               2*t1*Power(t2,2)*
                (-49*Power(t1,4) + 66*Power(t1,3)*t2 + 
                  7*Power(t1,2)*Power(t2,2) - 34*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,2)*(-98*Power(t1,5) + 424*Power(t1,4)*t2 - 
                  595*Power(t1,3)*Power(t2,2) + 
                  540*Power(t1,2)*Power(t2,3) + 293*t1*Power(t2,4) - 
                  14*Power(t2,5))) - 
            2*s2*t1*Power(t1 - t2,3)*t2*
             (-2*Power(s2,5)*(Power(t1,2) - 3*t1*t2 - 5*Power(t2,2)) - 
               2*Power(t1,2)*Power(t2,3)*
                (Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
               Power(s2,4)*(5*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  28*t1*Power(t2,2) - 30*Power(t2,3)) + 
               s2*t1*t2*(Power(t1,4) + 9*Power(t1,3)*t2 + 
                  9*Power(t1,2)*Power(t2,2) - 13*t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,3)*(-4*Power(t1,4) - 17*Power(t1,3)*t2 + 
                  17*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,2)*(Power(t1,5) + 7*Power(t1,4)*t2 - 
                  11*Power(t1,3)*Power(t2,2) - 
                  5*Power(t1,2)*Power(t2,3) + 40*t1*Power(t2,4) + 
                  20*Power(t2,5))) + 
            Power(s1,5)*(2*Power(s2,6)*
                (Power(t1,2) - 21*t1*t2 + 6*Power(t2,2)) - 
               5*Power(s2,5)*
                (32*Power(t1,3) + 51*Power(t1,2)*t2 - 
                  39*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,4)*(398*Power(t1,4) - 760*Power(t1,3)*t2 + 
                  588*Power(t1,2)*Power(t2,2) + 460*t1*Power(t2,3) - 
                  62*Power(t2,4)) - 
               2*t1*Power(t2,2)*
                (31*Power(t1,5) - 138*Power(t1,4)*t2 + 
                  193*Power(t1,3)*Power(t2,2) - 
                  102*Power(t1,2)*Power(t2,3) + 4*t1*Power(t2,4) + 
                  10*Power(t2,5)) + 
               s2*t1*t2*(-62*Power(t1,5) + 210*Power(t1,4)*t2 - 
                  587*Power(t1,3)*Power(t2,2) + 
                  831*Power(t1,2)*Power(t2,3) - 167*t1*Power(t2,4) + 
                  11*Power(t2,5)) + 
               Power(s2,3)*(-80*Power(t1,5) + 252*Power(t1,4)*t2 - 
                  496*Power(t1,3)*Power(t2,2) - 
                  99*Power(t1,2)*Power(t2,3) + 705*t1*Power(t2,4) + 
                  38*Power(t2,5)) + 
               Power(s2,2)*(-62*Power(t1,6) + 210*Power(t1,5)*t2 + 
                  51*Power(t1,4)*Power(t2,2) + 
                  13*Power(t1,3)*Power(t2,3) - 
                  541*Power(t1,2)*Power(t2,4) - 351*t1*Power(t2,5) + 
                  18*Power(t2,6))) + 
            Power(s1,4)*(-(Power(s2,6)*
                  (26*Power(t1,3) + 118*Power(t1,2)*t2 - 
                    91*t1*Power(t2,2) + 56*Power(t2,3))) + 
               Power(s2,5)*(-8*Power(t1,4) - 437*Power(t1,3)*t2 + 
                  1041*Power(t1,2)*Power(t2,2) + 2*t1*Power(t2,3) + 
                  126*Power(t2,4)) + 
               Power(s2,4)*(348*Power(t1,5) - 714*Power(t1,4)*t2 - 
                  298*Power(t1,3)*Power(t2,2) + 
                  388*Power(t1,2)*Power(t2,3) - 443*t1*Power(t2,4) + 
                  146*Power(t2,5)) + 
               Power(s2,3)*(-320*Power(t1,6) + 1214*Power(t1,5)*t2 - 
                  1101*Power(t1,4)*Power(t2,2) + 
                  209*Power(t1,3)*Power(t2,3) + 
                  512*Power(t1,2)*Power(t2,4) - 1022*t1*Power(t2,5) - 
                  46*Power(t2,6)) + 
               s2*t1*t2*(52*Power(t1,6) - 408*Power(t1,5)*t2 + 
                  736*Power(t1,4)*Power(t2,2) - 
                  399*Power(t1,3)*Power(t2,3) - 
                  123*Power(t1,2)*Power(t2,4) + 9*t1*Power(t2,5) - 
                  17*Power(t2,6)) + 
               2*t1*Power(t2,2)*
                (26*Power(t1,6) - 42*Power(t1,5)*t2 - 
                  37*Power(t1,4)*Power(t2,2) + 
                  140*Power(t1,3)*Power(t2,3) - 
                  123*Power(t1,2)*Power(t2,4) + 31*t1*Power(t2,5) + 
                  5*Power(t2,6)) + 
               Power(s2,2)*(52*Power(t1,7) - 408*Power(t1,6)*t2 + 
                  1152*Power(t1,5)*Power(t2,2) - 
                  1330*Power(t1,4)*Power(t2,3) + 
                  939*Power(t1,3)*Power(t2,4) + 
                  96*Power(t1,2)*Power(t2,5) + 121*t1*Power(t2,6) - 
                  10*Power(t2,7))) + 
            Power(s1,2)*(-2*Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (4*Power(t1,4) - 3*Power(t1,3)*t2 - 
                  7*Power(t1,2)*Power(t2,2) + 9*t1*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s2,6)*(4*Power(t1,5) + 162*Power(t1,4)*t2 - 
                  445*Power(t1,3)*Power(t2,2) + 
                  394*Power(t1,2)*Power(t2,3) - 91*t1*Power(t2,4) + 
                  72*Power(t2,5)) - 
               s2*t1*Power(t1 - t2,2)*t2*
                (8*Power(t1,6) - 80*Power(t1,5)*t2 + 
                  44*Power(t1,4)*Power(t2,2) + 
                  79*Power(t1,3)*Power(t2,3) + 
                  91*Power(t1,2)*Power(t2,4) + 23*t1*Power(t2,5) - 
                  Power(t2,6)) + 
               Power(s2,5)*(62*Power(t1,6) + 27*Power(t1,5)*t2 - 
                  567*Power(t1,4)*Power(t2,2) + 
                  575*Power(t1,3)*Power(t2,3) + 
                  165*Power(t1,2)*Power(t2,4) - 152*t1*Power(t2,5) + 
                  28*Power(t2,6)) + 
               Power(s2,4)*(-112*Power(t1,7) + 398*Power(t1,6)*t2 - 
                  328*Power(t1,5)*Power(t2,2) + 
                  370*Power(t1,4)*Power(t2,3) - 
                  629*Power(t1,3)*Power(t2,4) + 
                  178*Power(t1,2)*Power(t2,5) + 141*t1*Power(t2,6) + 
                  92*Power(t2,7)) + 
               Power(s2,2)*t1*
                (-8*Power(t1,8) + 96*Power(t1,7)*t2 - 
                  359*Power(t1,6)*Power(t2,2) + 
                  588*Power(t1,5)*Power(t2,3) - 
                  900*Power(t1,4)*Power(t2,4) + 
                  838*Power(t1,3)*Power(t2,5) - 
                  227*Power(t1,2)*Power(t2,6) + 82*t1*Power(t2,7) - 
                  110*Power(t2,8)) + 
               Power(s2,3)*(62*Power(t1,8) - 363*Power(t1,7)*t2 + 
                  725*Power(t1,6)*Power(t2,2) - 
                  832*Power(t1,5)*Power(t2,3) + 
                  1303*Power(t1,4)*Power(t2,4) - 
                  1324*Power(t1,3)*Power(t2,5) + 
                  120*Power(t1,2)*Power(t2,6) + 193*t1*Power(t2,7) - 
                  8*Power(t2,8))) + 
            Power(s1,3)*(Power(s2,6)*
                (-30*Power(t1,4) - 176*Power(t1,3)*t2 + 
                  408*Power(t1,2)*Power(t2,2) - 107*t1*Power(t2,3) + 
                  96*Power(t2,4)) + 
               Power(s2,5)*(156*Power(t1,5) - 467*Power(t1,4)*t2 + 
                  617*Power(t1,3)*Power(t2,2) - 
                  739*Power(t1,2)*Power(t2,3) - 70*t1*Power(t2,4) - 
                  98*Power(t2,5)) + 
               2*t1*Power(t1 - t2,2)*Power(t2,2)*
                (14*Power(t1,5) - 35*Power(t1,4)*t2 + 
                  37*Power(t1,3)*Power(t2,2) + 
                  4*Power(t1,2)*Power(t2,3) - 22*t1*Power(t2,4) - 
                  Power(t2,5)) - 
               2*Power(s2,4)*
                (54*Power(t1,6) - 279*Power(t1,5)*t2 + 
                  694*Power(t1,4)*Power(t2,2) - 
                  1042*Power(t1,3)*Power(t2,3) + 
                  456*Power(t1,2)*Power(t2,4) + 13*t1*Power(t2,5) + 
                  83*Power(t2,6)) + 
               s2*t1*t2*(28*Power(t1,7) - 164*Power(t1,6)*t2 + 
                  529*Power(t1,5)*Power(t2,2) - 
                  828*Power(t1,4)*Power(t2,3) + 
                  522*Power(t1,3)*Power(t2,4) - 
                  202*Power(t1,2)*Power(t2,5) + 110*t1*Power(t2,6) + 
                  5*Power(t2,7)) + 
               Power(s2,3)*(-38*Power(t1,7) + 101*Power(t1,6)*t2 + 
                  457*Power(t1,5)*Power(t2,2) - 
                  1615*Power(t1,4)*Power(t2,3) + 
                  2261*Power(t1,3)*Power(t2,4) - 
                  1034*Power(t1,2)*Power(t2,5) + 436*t1*Power(t2,6) + 
                  30*Power(t2,7)) + 
               Power(s2,2)*(28*Power(t1,8) - 164*Power(t1,7)*t2 + 
                  248*Power(t1,6)*Power(t2,2) - 
                  51*Power(t1,5)*Power(t2,3) - 
                  304*Power(t1,4)*Power(t2,4) + 
                  28*Power(t1,3)*Power(t2,5) - 
                  129*Power(t1,2)*Power(t2,6) + 106*t1*Power(t2,7) + 
                  2*Power(t2,8))) + 
            s1*(-6*Power(t1,4)*Power(t1 - t2,4)*Power(t2,4) - 
               s2*Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (4*Power(t1,4) + 22*Power(t1,3)*t2 + 
                  21*Power(t1,2)*Power(t2,2) + 13*t1*Power(t2,3) - 
                  11*Power(t2,4)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*t2*
                (4*Power(t1,6) - 34*Power(t1,5)*t2 + 
                  21*Power(t1,4)*Power(t2,2) + 
                  110*Power(t1,3)*Power(t2,3) - 
                  6*Power(t1,2)*Power(t2,4) - 147*t1*Power(t2,5) - 
                  28*Power(t2,6)) + 
               Power(s2,6)*(4*Power(t1,6) - 58*Power(t1,5)*t2 + 
                  122*Power(t1,4)*Power(t2,2) - 
                  171*Power(t1,3)*Power(t2,3) + 
                  148*Power(t1,2)*Power(t2,4) - 59*t1*Power(t2,5) + 
                  20*Power(t2,6)) + 
               Power(s2,5)*t1*
                (-12*Power(t1,6) + 166*Power(t1,5)*t2 - 
                  480*Power(t1,4)*Power(t2,2) + 
                  700*Power(t1,3)*Power(t2,3) - 
                  289*Power(t1,2)*Power(t2,4) - 292*t1*Power(t2,5) + 
                  205*Power(t2,6)) + 
               2*Power(s2,4)*
                (6*Power(t1,8) - 78*Power(t1,7)*t2 + 
                  257*Power(t1,6)*Power(t2,2) - 
                  331*Power(t1,5)*Power(t2,3) + 
                  114*Power(t1,4)*Power(t2,4) + 
                  122*Power(t1,3)*Power(t2,5) - 
                  99*Power(t1,2)*Power(t2,6) + 15*t1*Power(t2,7) - 
                  10*Power(t2,8)) - 
               2*Power(s2,3)*t1*
                (2*Power(t1,8) - 26*Power(t1,7)*t2 + 
                  102*Power(t1,6)*Power(t2,2) - 
                  153*Power(t1,5)*Power(t2,3) + 
                  69*Power(t1,4)*Power(t2,4) + 
                  122*Power(t1,3)*Power(t2,5) - 
                  72*Power(t1,2)*Power(t2,6) - 147*t1*Power(t2,7) + 
                  103*Power(t2,8)))) + 
         Power(s,5)*(-2*Power(s1,9)*t1*
             (Power(s2,4) + 6*s2*t1*(t1 - 2*t2)*t2 + 
               2*t1*(3*t1 - t2)*Power(t2,2) - 6*Power(s2,3)*(t1 + t2) + 
               Power(s2,2)*(6*Power(t1,2) - 12*t1*t2 - 5*Power(t2,2))) + 
            Power(s1,8)*(-14*Power(s2,5)*t1 + 
               2*s2*Power(t1,2)*t2*
                (7*Power(t1,2) - 31*t1*t2 - 27*Power(t2,2)) + 
               2*Power(t1,2)*Power(t2,2)*
                (7*Power(t1,2) + 12*t1*t2 - 7*Power(t2,2)) + 
               Power(s2,4)*(84*Power(t1,2) + 83*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(-96*Power(t1,3) + 85*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(14*Power(t1,4) - 62*Power(t1,3)*t2 + 
                  45*Power(t1,2)*Power(t2,2) - 24*t1*Power(t2,3) - 
                  2*Power(t2,4))) + 
            2*s2*t1*Power(t1 - t2,3)*Power(t2,2)*
             (Power(t1,3)*(t1 - t2)*Power(t2,3) + 
               2*Power(s2,6)*(t1 + t2) - 
               Power(s2,5)*t2*(19*t1 + 18*t2) + 
               Power(s2,4)*t1*
                (-9*Power(t1,2) + 24*t1*t2 + 29*Power(t2,2)) + 
               s2*Power(t1,2)*t2*
                (-3*Power(t1,3) - 5*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-3*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  4*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s2,3)*(10*Power(t1,4) - 8*Power(t1,3)*t2 - 
                  9*Power(t1,2)*Power(t2,2) + 20*t1*Power(t2,3) + 
                  20*Power(t2,4))) + 
            Power(s1,7)*(-14*Power(s2,6)*t1 + 
               2*Power(s2,5)*(53*Power(t1,2) + 97*t1*t2 - Power(t2,2)) + 
               2*Power(t1,2)*Power(t2,2)*
                (33*Power(t1,3) - 57*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) + 5*Power(t2,3)) - 
               Power(s2,4)*(66*Power(t1,3) - 77*Power(t1,2)*t2 + 
                  82*t1*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,3)*(-120*Power(t1,4) + 189*Power(t1,3)*t2 - 
                  97*Power(t1,2)*Power(t2,2) + 39*t1*Power(t2,3) - 
                  16*Power(t2,4)) + 
               2*s2*t1*t2*(33*Power(t1,4) - 126*Power(t1,3)*t2 + 
                  183*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(66*Power(t1,5) - 252*Power(t1,4)*t2 + 
                  185*Power(t1,3)*Power(t2,2) - 
                  380*Power(t1,2)*Power(t2,3) + 31*t1*Power(t2,4) + 
                  8*Power(t2,5))) + 
            Power(s1,6)*(-2*Power(s2,7)*t1 + 
               Power(s2,5)*t1*
                (186*Power(t1,2) + 161*t1*t2 - 336*Power(t2,2)) + 
               3*Power(s2,6)*
                (6*Power(t1,2) + 49*t1*t2 - 6*Power(t2,2)) + 
               2*Power(t1,2)*Power(t2,2)*
                (3*Power(t1,4) - 61*Power(t1,3)*t2 + 
                  109*Power(t1,2)*Power(t2,2) - 75*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,4)*(-462*Power(t1,4) + 624*Power(t1,3)*t2 - 
                  437*Power(t1,2)*Power(t2,2) - 124*t1*Power(t2,3) + 
                  72*Power(t2,4)) + 
               s2*t1*t2*(6*Power(t1,5) + 66*Power(t1,4)*t2 + 
                  93*Power(t1,3)*Power(t2,2) - 
                  358*Power(t1,2)*Power(t2,3) + 99*t1*Power(t2,4) - 
                  8*Power(t2,5)) + 
               Power(s2,3)*(222*Power(t1,5) - 638*Power(t1,4)*t2 + 
                  686*Power(t1,3)*Power(t2,2) - 
                  400*Power(t1,2)*Power(t2,3) - 443*t1*Power(t2,4) + 
                  42*Power(t2,5)) + 
               Power(s2,2)*(6*Power(t1,6) + 66*Power(t1,5)*t2 - 
                  537*Power(t1,4)*Power(t2,2) + 
                  590*Power(t1,3)*Power(t2,3) + 
                  587*Power(t1,2)*Power(t2,4) - 72*t1*Power(t2,5) - 
                  12*Power(t2,6))) + 
            Power(s1,5)*(-2*Power(s2,7)*
                (Power(t1,2) - 9*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,6)*(110*Power(t1,3) + 315*Power(t1,2)*t2 - 
                  353*t1*Power(t2,2) + 86*Power(t2,3)) + 
               Power(s2,5)*(-162*Power(t1,4) + 201*Power(t1,3)*t2 - 
                  761*Power(t1,2)*Power(t2,2) - 88*t1*Power(t2,3) + 
                  40*Power(t2,4)) + 
               Power(s2,4)*(-164*Power(t1,5) + 404*Power(t1,4)*t2 + 
                  137*Power(t1,3)*Power(t2,2) + 
                  41*Power(t1,2)*Power(t2,3) - 76*t1*Power(t2,4) - 
                  112*Power(t2,5)) - 
               2*Power(t1,2)*Power(t2,2)*
                (29*Power(t1,5) - 66*Power(t1,4)*t2 + 
                  42*Power(t1,3)*Power(t2,2) + 
                  42*Power(t1,2)*Power(t2,3) - 70*t1*Power(t2,4) + 
                  20*Power(t2,5)) + 
               Power(s2,3)*(258*Power(t1,6) - 962*Power(t1,5)*t2 + 
                  746*Power(t1,4)*Power(t2,2) - 
                  490*Power(t1,3)*Power(t2,3) + 
                  495*Power(t1,2)*Power(t2,4) + 785*t1*Power(t2,5) - 
                  50*Power(t2,6)) + 
               s2*t1*t2*(-58*Power(t1,6) + 376*Power(t1,5)*t2 - 
                  799*Power(t1,4)*Power(t2,2) + 
                  849*Power(t1,3)*Power(t2,3) - 
                  281*Power(t1,2)*Power(t2,4) + 33*t1*Power(t2,5) + 
                  12*Power(t2,6)) + 
               Power(s2,2)*(-58*Power(t1,7) + 376*Power(t1,6)*t2 - 
                  887*Power(t1,5)*Power(t2,2) + 
                  1559*Power(t1,4)*Power(t2,3) - 
                  1257*Power(t1,3)*Power(t2,4) - 
                  431*Power(t1,2)*Power(t2,5) + 136*t1*Power(t2,6) + 
                  8*Power(t2,7))) + 
            s1*t2*(-2*Power(t1,5)*Power(t1 - t2,4)*Power(t2,3) - 
               s2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2)*
                (6*Power(t1,3) + 2*Power(t1,2)*t2 + 31*t1*Power(t2,2) - 
                  3*Power(t2,3)) + 
               2*Power(s2,7)*
                (8*Power(t1,5) - 18*Power(t1,4)*t2 + 
                  32*Power(t1,3)*Power(t2,2) - 
                  29*Power(t1,2)*Power(t2,3) + 9*t1*Power(t2,4) - 
                  5*Power(t2,5)) + 
               Power(s2,2)*Power(t1,2)*Power(t1 - t2,2)*t2*
                (4*Power(t1,5) + 46*Power(t1,4)*t2 - 
                  131*Power(t1,3)*Power(t2,2) + 
                  80*Power(t1,2)*Power(t2,3) + 13*t1*Power(t2,4) + 
                  33*Power(t2,5)) + 
               Power(s2,6)*(-54*Power(t1,6) + 136*Power(t1,5)*t2 - 
                  274*Power(t1,4)*Power(t2,2) + 
                  161*Power(t1,3)*Power(t2,3) + 
                  146*Power(t1,2)*Power(t2,4) - 113*t1*Power(t2,5) + 
                  10*Power(t2,6)) + 
               Power(s2,5)*(64*Power(t1,7) - 140*Power(t1,6)*t2 + 
                  100*Power(t1,5)*Power(t2,2) + 
                  228*Power(t1,4)*Power(t2,3) - 
                  423*Power(t1,3)*Power(t2,4) + 
                  92*Power(t1,2)*Power(t2,5) + 65*t1*Power(t2,6) + 
                  20*Power(t2,7)) + 
               2*Power(s2,4)*t1*
                (-15*Power(t1,7) + 17*Power(t1,6)*t2 + 
                  126*Power(t1,5)*Power(t2,2) - 
                  357*Power(t1,4)*Power(t2,3) + 
                  428*Power(t1,3)*Power(t2,4) - 
                  159*Power(t1,2)*Power(t2,5) - 123*t1*Power(t2,6) + 
                  77*Power(t2,7)) + 
               Power(s2,3)*t1*
                (4*Power(t1,8) + 2*Power(t1,7)*t2 - 
                  182*Power(t1,6)*Power(t2,2) + 
                  614*Power(t1,5)*Power(t2,3) - 
                  946*Power(t1,4)*Power(t2,4) + 
                  491*Power(t1,3)*Power(t2,5) + 
                  192*Power(t1,2)*Power(t2,6) - 133*t1*Power(t2,7) - 
                  42*Power(t2,8))) + 
            Power(s1,4)*(Power(s2,7)*
                (6*Power(t1,3) + 52*Power(t1,2)*t2 - 
                  48*t1*Power(t2,2) + 34*Power(t2,3)) + 
               Power(s2,6)*(82*Power(t1,4) + 315*Power(t1,3)*t2 - 
                  1107*Power(t1,2)*Power(t2,2) + 330*t1*Power(t2,3) - 
                  160*Power(t2,4)) + 
               Power(s2,5)*(-332*Power(t1,5) + 339*Power(t1,4)*t2 + 
                  603*Power(t1,3)*Power(t2,2) - 
                  93*Power(t1,2)*Power(t2,3) + 516*t1*Power(t2,4) - 
                  120*Power(t2,5)) - 
               2*Power(t1,2)*Power(t2,2)*
                (6*Power(t1,6) - 39*Power(t1,5)*t2 + 
                  104*Power(t1,4)*Power(t2,2) - 
                  135*Power(t1,3)*Power(t2,3) + 
                  63*Power(t1,2)*Power(t2,4) + 14*t1*Power(t2,5) - 
                  13*Power(t2,6)) + 
               Power(s2,4)*(318*Power(t1,6) - 859*Power(t1,5)*t2 + 
                  198*Power(t1,4)*Power(t2,2) - 
                  273*Power(t1,3)*Power(t2,3) - 
                  8*Power(t1,2)*Power(t2,4) + 593*t1*Power(t2,5) + 
                  104*Power(t2,6)) - 
               s2*t1*t2*(12*Power(t1,7) - 12*Power(t1,6)*t2 + 
                  229*Power(t1,5)*Power(t2,2) - 
                  691*Power(t1,4)*Power(t2,3) + 
                  905*Power(t1,3)*Power(t2,4) - 
                  537*Power(t1,2)*Power(t2,5) + 167*t1*Power(t2,6) + 
                  8*Power(t2,7)) + 
               Power(s2,3)*(-66*Power(t1,7) + 283*Power(t1,6)*t2 - 
                  448*Power(t1,5)*Power(t2,2) + 
                  836*Power(t1,4)*Power(t2,3) - 
                  1071*Power(t1,3)*Power(t2,4) + 
                  100*Power(t1,2)*Power(t2,5) - 477*t1*Power(t2,6) + 
                  28*Power(t2,7)) + 
               Power(s2,2)*(-12*Power(t1,8) + 12*Power(t1,7)*t2 + 
                  212*Power(t1,6)*Power(t2,2) - 
                  58*Power(t1,5)*Power(t2,3) - 
                  22*Power(t1,4)*Power(t2,4) + 
                  113*Power(t1,3)*Power(t2,5) + 
                  329*Power(t1,2)*Power(t2,6) - 136*t1*Power(t2,7) - 
                  2*Power(t2,8))) + 
            Power(s1,3)*(2*Power(s2,7)*
                (5*Power(t1,4) + 33*Power(t1,3)*t2 - 
                  84*Power(t1,2)*Power(t2,2) + 30*t1*Power(t2,3) - 
                  27*Power(t2,4)) + 
               2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (6*Power(t1,5) - 9*Power(t1,4)*t2 + 
                  4*Power(t1,3)*Power(t2,2) + 
                  26*Power(t1,2)*Power(t2,3) - 18*t1*Power(t2,4) - 
                  3*Power(t2,5)) + 
               Power(s2,6)*(-24*Power(t1,5) + 219*Power(t1,4)*t2 - 
                  769*Power(t1,3)*Power(t2,2) + 
                  1103*Power(t1,2)*Power(t2,3) - 160*t1*Power(t2,4) + 
                  144*Power(t2,5)) + 
               Power(s2,5)*(-48*Power(t1,6) - 187*Power(t1,5)*t2 + 
                  1361*Power(t1,4)*Power(t2,2) - 
                  2633*Power(t1,3)*Power(t2,3) + 
                  1149*Power(t1,2)*Power(t2,4) - 245*t1*Power(t2,5) + 
                  150*Power(t2,6)) + 
               Power(s2,4)*(128*Power(t1,7) - 351*Power(t1,6)*t2 - 
                  593*Power(t1,5)*Power(t2,2) + 
                  2509*Power(t1,4)*Power(t2,3) - 
                  2189*Power(t1,3)*Power(t2,4) + 
                  610*Power(t1,2)*Power(t2,5) - 462*t1*Power(t2,6) - 
                  54*Power(t2,7)) + 
               s2*t1*t2*(12*Power(t1,8) - 120*Power(t1,7)*t2 + 
                  325*Power(t1,6)*Power(t2,2) - 
                  365*Power(t1,5)*Power(t2,3) + 
                  103*Power(t1,4)*Power(t2,4) + 
                  114*Power(t1,3)*Power(t2,5) - 
                  176*Power(t1,2)*Power(t2,6) + 105*t1*Power(t2,7) + 
                  2*Power(t2,8)) - 
               Power(s2,3)*(78*Power(t1,8) - 385*Power(t1,7)*t2 + 
                  309*Power(t1,6)*Power(t2,2) + 
                  459*Power(t1,5)*Power(t2,3) - 
                  747*Power(t1,4)*Power(t2,4) - 
                  492*Power(t1,3)*Power(t2,5) + 
                  213*Power(t1,2)*Power(t2,6) + 28*t1*Power(t2,7) + 
                  6*Power(t2,8)) + 
               Power(s2,2)*t1*
                (12*Power(t1,8) - 120*Power(t1,7)*t2 + 
                  390*Power(t1,6)*Power(t2,2) - 
                  547*Power(t1,5)*Power(t2,3) + 
                  562*Power(t1,4)*Power(t2,4) - 
                  900*Power(t1,3)*Power(t2,5) + 
                  631*Power(t1,2)*Power(t2,6) - 218*t1*Power(t2,7) + 
                  71*Power(t2,8))) + 
            Power(s1,2)*(2*Power(t1,3)*Power(t1 - t2,4)*Power(t2,4)*
                (7*t1 + 5*t2) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (12*Power(t1,5) + 28*Power(t1,4)*t2 - 
                  55*Power(t1,3)*Power(t2,2) - 
                  31*Power(t1,2)*Power(t2,3) - 47*t1*Power(t2,4) - 
                  18*Power(t2,5)) + 
               2*Power(s2,7)*(2*Power(t1,5) + 24*Power(t1,4)*t2 - 
                  78*Power(t1,3)*Power(t2,2) + 
                  84*Power(t1,2)*Power(t2,3) - 21*t1*Power(t2,4) + 
                  19*Power(t2,5)) + 
               Power(s2,6)*(-28*Power(t1,6) + 18*Power(t1,5)*t2 + 
                  39*Power(t1,4)*Power(t2,2) + 
                  115*Power(t1,3)*Power(t2,3) - 
                  405*Power(t1,2)*Power(t2,4) + 127*t1*Power(t2,5) - 
                  62*Power(t2,6)) + 
               Power(s2,5)*(56*Power(t1,7) - 228*Power(t1,6)*t2 + 
                  509*Power(t1,5)*Power(t2,2) - 
                  1364*Power(t1,4)*Power(t2,3) + 
                  1841*Power(t1,3)*Power(t2,4) - 
                  596*Power(t1,2)*Power(t2,5) - 92*t1*Power(t2,6) - 
                  88*Power(t2,7)) + 
               Power(s2,2)*t1*t2*
                (12*Power(t1,8) - 90*Power(t1,7)*t2 + 
                  204*Power(t1,6)*Power(t2,2) - 
                  416*Power(t1,5)*Power(t2,3) + 
                  387*Power(t1,4)*Power(t2,4) + 
                  19*Power(t1,3)*Power(t2,5) - 
                  111*Power(t1,2)*Power(t2,6) + 11*t1*Power(t2,7) - 
                  16*Power(t2,8)) + 
               Power(s2,4)*(-44*Power(t1,8) + 252*Power(t1,7)*t2 - 
                  709*Power(t1,6)*Power(t2,2) + 
                  1871*Power(t1,5)*Power(t2,3) - 
                  2950*Power(t1,4)*Power(t2,4) + 
                  2011*Power(t1,3)*Power(t2,5) - 
                  201*Power(t1,2)*Power(t2,6) - 44*t1*Power(t2,7) + 
                  12*Power(t2,8)) + 
               Power(s2,3)*t1*
                (12*Power(t1,8) - 102*Power(t1,7)*t2 + 
                  377*Power(t1,6)*Power(t2,2) - 
                  970*Power(t1,5)*Power(t2,3) + 
                  2025*Power(t1,4)*Power(t2,4) - 
                  1895*Power(t1,3)*Power(t2,5) + 
                  225*Power(t1,2)*Power(t2,6) + 91*t1*Power(t2,7) + 
                  141*Power(t2,8)))))*lg2(s2 - t1 + t2))/
     (Power(s,3)*Power(s1,2)*Power(s2,2)*Power(t1,2)*Power(s - s2 + t1,2)*
       (s - s1 - s2 + t1)*Power(s1 + t1 - t2,3)*Power(t2,2)*
       Power(s - s1 + t2,2)*(s2 + t2)) - 
    (4*(2*Power(s,3)*s2*(2*Power(s2,2) - 3*s2*(t1 - t2) + 
            4*Power(t1 - t2,2)) + 
         Power(s1,3)*(-3*Power(s2,3) + 3*Power(t1,3) + 
            4*Power(t1,2)*t2 - 3*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s2,2)*(9*t1 + 4*t2) + 
            s2*(-9*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) + 
         s2*(-12*s2*Power(t1,2)*Power(t1 - t2,2) - 
            4*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(t1 - t2,2)*
             (4*Power(t1,3) - 2*Power(t1,2)*t2 + Power(t2,3)) + 
            Power(s2,2)*(12*Power(t1,3) - 18*Power(t1,2)*t2 + 
               8*t1*Power(t2,2) + Power(t2,3))) - 
         Power(s1,2)*(4*Power(s2,4) - 4*Power(t1,4) + 2*Power(t1,3)*t2 + 
            6*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
            4*Power(t2,4) + 8*Power(s2,2)*t2*(2*t1 + t2) - 
            Power(s2,3)*(8*t1 + 7*t2) + 
            s2*(8*Power(t1,3) - 11*Power(t1,2)*t2 - 14*t1*Power(t2,2) + 
               5*Power(t2,3))) + 
         s1*(-4*Power(s2,4)*(t1 - 2*t2) + 
            Power(s2,3)*(10*Power(t1,2) - 16*t1*t2 - 5*Power(t2,2)) + 
            Power(t1 - t2,2)*
             (2*Power(t1,3) - t1*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,2)*(-6*Power(t1,3) + 4*Power(t1,2)*t2 + 
               7*t1*Power(t2,2) + 4*Power(t2,3)) + 
            s2*(-2*Power(t1,4) + 8*Power(t1,3)*t2 - 
               3*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + Power(t2,4)\
)) - 2*Power(s,2)*(2*Power(s2,4) + 6*Power(s2,2)*Power(t1 - t2,2) - 
            Power(t1 - t2,3)*t2 + Power(s2,3)*(-4*t1 + t2) - 
            s2*Power(t1 - t2,2)*(4*t1 + t2) + 
            s1*(3*Power(s2,3) - Power(t1 - t2,2)*(t1 + 2*t2) - 
               Power(s2,2)*(9*t1 + 2*t2) + 
               s2*(7*Power(t1,2) - 9*t1*t2 + 2*Power(t2,2)))) + 
         s*(-4*Power(s2,4)*t2 - 
            Power(t1 - t2,3)*(2*Power(t1,2) - Power(t2,2)) + 
            2*s2*Power(t1 - t2,2)*(6*Power(t1,2) + Power(t2,2)) + 
            8*Power(s2,3)*(Power(t1,2) - t1*t2 + Power(t2,2)) - 
            3*Power(s2,2)*(6*Power(t1,3) - 10*Power(t1,2)*t2 + 
               3*t1*Power(t2,2) + Power(t2,3)) + 
            Power(s1,2)*(8*Power(s2,3) - 5*Power(t1,3) - 
               Power(t1,2)*t2 + 9*t1*Power(t2,2) - 3*Power(t2,3) - 
               Power(s2,2)*(21*t1 + 5*t2) + 
               2*s2*(9*Power(t1,2) + 3*t1*t2 + Power(t2,2))) + 
            2*s1*(2*Power(s2,4) - 4*Power(s2,3)*(t1 + 2*t2) + 
               Power(s2,2)*t2*(5*t1 + 4*t2) - 
               2*Power(t1 - t2,2)*(Power(t1,2) + t1*t2 - Power(t2,2)) + 
               s2*(4*Power(t1,3) + Power(t1,2)*t2 - 3*t1*Power(t2,2) - 
                  2*Power(t2,3)))))*
       lg2((s*(-s1 - t1 + t2))/(-s + s2 - t1 + t2)))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) + 
    (8*(-(Power(s1,3)*(3*Power(s2,2) + 3*Power(t1,2) - t1*t2 + 
              2*Power(t2,2) + s2*(-6*t1 + t2))) + 
         Power(s,3)*(2*Power(s2,2) + 3*Power(t1 - t2,2) + 
            s2*(-t1 + t2)) + Power(s,2)*
          (-2*Power(s2,3) + 2*Power(t1 - t2,2)*t2 + 
            Power(s2,2)*(2*t1 + t2) + 
            s2*(-4*Power(t1,2) + 11*t1*t2 - 7*Power(t2,2)) + 
            s1*(-3*Power(s2,2) + 6*s2*t1 - 7*Power(t1,2) + 5*s2*t2 + 
               11*t1*t2 - 4*Power(t2,2))) + 
         Power(s1,2)*(-2*Power(s2,3) - 2*Power(t1,3) + 
            2*Power(t1,2)*t2 - 4*t1*Power(t2,2) + 4*Power(t2,3) + 
            Power(s2,2)*(2*t1 + 7*t2) + 
            s2*(2*Power(t1,2) - 9*t1*t2 + Power(t2,2))) - 
         s1*(2*Power(s2,3)*(t1 - 2*t2) - 
            s2*t2*(-3*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
            Power(t1 - t2,2)*(Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(-3*Power(t1,2) + 2*t1*t2 + 5*Power(t2,2))) - 
         s2*(2*Power(s2,2)*(Power(t1,2) - t1*t2 + Power(t2,2)) + 
            Power(t1 - t2,2)*(2*Power(t1,2) + t1*t2 + Power(t2,2)) - 
            s2*(4*Power(t1,3) - 5*Power(t1,2)*t2 + Power(t2,3))) + 
         s*(-2*Power(s2,3)*t2 + 
            Power(t1 - t2,2)*(Power(t1,2) + Power(t2,2)) + 
            Power(s1,2)*(4*Power(s2,2) - 11*s2*t1 + 7*Power(t1,2) - 
               5*s2*t2 - 6*t1*t2 + 3*Power(t2,2)) + 
            Power(s2,2)*(4*Power(t1,2) - 6*t1*t2 + 6*Power(t2,2)) + 
            s2*(-5*Power(t1,3) + 5*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
               3*Power(t2,3)) + 
            2*s1*(Power(s2,3) + Power(t1,3) - 3*Power(t1,2)*t2 + 
               4*t1*Power(t2,2) - 2*Power(t2,3) - 
               Power(s2,2)*(t1 + 5*t2) - s2*(Power(t1,2) - 4*Power(t2,2)))\
))*lg2(-((s*(s - s1 - s2)*(-s1 - t1 + t2))/
           ((s1 + s2)*(-s + s2 - t1 + t2)))))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,2)) - 
    (4*(6*Power(s1*(s2 - t1) - s2*t2,3) + 
         4*Power(s,4)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) + 
         Power(s,3)*(-4*Power(s1,3) - 4*Power(s2,3) - 
            8*s2*Power(t1 - t2,2) + 3*Power(t1 - t2,2)*(t1 + t2) + 
            2*Power(s2,2)*(3*t1 + t2) + 
            Power(s1,2)*(-8*s2 + 2*t1 + 6*t2) - 
            8*s1*(Power(s2,2) + Power(t1 - t2,2) - s2*(t1 + t2))) - 
         2*s*(4*Power(s1,3)*Power(s2 - t1,2) + 
            Power(s2,2)*(4*s2 + 3*t1 - 6*t2)*Power(t2,2) + 
            Power(s1,2)*(s2 - t1)*
             (4*Power(s2,2) + 3*t1*(2*t1 - t2) - 2*s2*(5*t1 + 7*t2)) + 
            s1*s2*t2*(-8*Power(s2,2) - 3*t1*(t1 + t2) + 
               s2*(11*t1 + 16*t2))) + 
         Power(s,2)*(6*Power(s1,3)*(s2 - t1) + 
            2*Power(s1,2)*(6*Power(s2,2) - 12*s2*t1 + 6*Power(t1,2) - 
               9*s2*t2 - 2*t1*t2) - 
            s2*t2*(6*Power(s2,2) + 4*s2*(t1 - 3*t2) + 
               3*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2))) + 
            s1*(6*Power(s2,3) - 6*Power(s2,2)*(3*t1 + 4*t2) - 
               3*t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               7*s2*(3*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)))))*
       lg2(((-s2 + t1 - t2)*(-s1 - t1 + t2))/(-s + s2 - t1 + t2)))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) + 
    (8*(-4*(s1 + s2)*Power(s1*(s2 - t1) - s2*t2,2) + 
         2*Power(s,3)*(Power(s1,2) + Power(s2,2) + s1*(t1 - t2) + 
            Power(t1 - t2,2) + s2*(-t1 + t2)) - 
         Power(s,2)*(s1 + s2)*
          (2*Power(s1,2) + 2*Power(s2,2) + s1*(2*s2 - t1 - 3*t2) + 
            4*Power(t1 - t2,2) - s2*(3*t1 + t2)) + 
         s*(s1 + s2)*(3*Power(s1,2)*(s2 - t1) + 
            s2*t2*(-3*s2 - 2*t1 + 6*t2) + 
            s1*(3*Power(s2,2) + 2*t1*(3*t1 - t2) - 9*s2*(t1 + t2))))*
       lg2(-(((s - s1 - s2)*(-s2 + t1 - t2)*(-s1 - t1 + t2))/
           ((-s + s1 + t1 - t2)*(-s + s2 - t1 + t2)))))/
     (Power(s,2)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) + 
    (4*(2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*Power(t1 - t2,2)*(2*s2 + t1 - t2)*t2 + 
         s*(3*Power(s2,2) + 2*s2*(t1 - t2) + Power(t1 - t2,2))*(t1 - t2)*
          Power(t2,2) + s2*(Power(s2,2) + Power(t1 - t2,2))*Power(t2,3) + 
         Power(s1,3)*(Power(s2,3) - Power(t1,3) + 4*Power(t1,2)*t2 - 
            3*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s2,2)*(-3*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) + 
         s1*(2*Power(s,2)*(t1 - t2)*
             (-2*Power(t1 - t2,2) + s2*(2*t1 + t2)) + 
            Power(t2,2)*(-Power(s2,3) - Power(s2,2)*(t1 - 4*t2) - 
               (t1 - 2*t2)*Power(t1 - t2,2) + 
               s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) - 
            2*s*t2*(Power(s2,2)*(t1 - 2*t2) + 2*Power(t1 - t2,3) + 
               s2*(-3*Power(t1,2) + t1*t2 + 2*Power(t2,2)))) + 
         Power(s1,2)*(s*(3*Power(t1 - t2,3) + Power(s2,2)*(3*t1 - t2) + 
               2*s2*(-3*Power(t1,2) + 5*t1*t2 + Power(t2,2))) - 
            t2*(Power(s2,3) - 2*Power(t1,3) - 4*Power(s2,2)*(t1 - 2*t2) + 
               6*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 4*Power(t2,3) + 
               s2*(5*Power(t1,2) - 14*t1*t2 + 5*Power(t2,2)))))*
       lg2(-((s*Power(-s1 - t1 + t2,2))/(-s + s2 - t1 + t2))))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) + 
    (4*(2*Power(s1,3)*Power(s2 - t1,3) - 2*Power(s2,3)*Power(t2,3) + 
         Power(s,3)*Power(t1 - t2,2)*(t1 + t2) + 
         2*s*Power(s2,2)*Power(t2,2)*(-t1 + 2*t2) - 
         Power(s,2)*s2*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
         2*Power(s1,2)*(s2 - t1)*
          (3*s2*(-s2 + t1)*t2 + s*(t1*(-2*t1 + t2) + 2*s2*(t1 + t2))) + 
         s1*(6*Power(s2,2)*(s2 - t1)*Power(t2,2) + 
            2*s*s2*t2*(t1*(t1 + t2) - s2*(t1 + 4*t2)) + 
            Power(s,2)*(-(t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
               s2*(3*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)))))*
       lg2(-(((-s2 + t1 - t2)*Power(-s1 - t1 + t2,2))/(-s + s2 - t1 + t2)))\
)/(Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (4*(2*Power(s1,3)*Power(s2 - t1,3) - 2*Power(s2,3)*Power(t2,3) + 
         Power(s,3)*Power(t1 - t2,2)*(t1 + t2) + 
         2*s*Power(s2,2)*Power(t2,2)*(-t1 + 2*t2) - 
         Power(s,2)*s2*t2*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
         2*Power(s1,2)*(s2 - t1)*
          (3*s2*(-s2 + t1)*t2 + s*(t1*(-2*t1 + t2) + 2*s2*(t1 + t2))) + 
         s1*(6*Power(s2,2)*(s2 - t1)*Power(t2,2) + 
            2*s*s2*t2*(t1*(t1 + t2) - s2*(t1 + 4*t2)) + 
            Power(s,2)*(-(t1*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
               s2*(3*Power(t1,2) - 2*t1*t2 + 3*Power(t2,2)))))*
       lg2(((-s1 - t1 + t2)*(-s + s2 - t1 + t2))/s))/
     (Power(s,3)*t1*(s - s2 + t1)*t2*(s - s1 + t2)) - 
    (4*(2*Power(s,3)*Power(t1 - t2,3) + 
         2*Power(s,2)*Power(t1 - t2,2)*(2*s2 + t1 - t2)*t2 + 
         s*(3*Power(s2,2) + 2*s2*(t1 - t2) + Power(t1 - t2,2))*(t1 - t2)*
          Power(t2,2) + s2*(Power(s2,2) + Power(t1 - t2,2))*Power(t2,3) + 
         Power(s1,3)*(Power(s2,3) - Power(t1,3) + 4*Power(t1,2)*t2 - 
            3*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s2,2)*(-3*t1 + 4*t2) + 
            s2*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2))) + 
         s1*(2*Power(s,2)*(t1 - t2)*
             (-2*Power(t1 - t2,2) + s2*(2*t1 + t2)) + 
            Power(t2,2)*(-Power(s2,3) - Power(s2,2)*(t1 - 4*t2) - 
               (t1 - 2*t2)*Power(t1 - t2,2) + 
               s2*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2))) - 
            2*s*t2*(Power(s2,2)*(t1 - 2*t2) + 2*Power(t1 - t2,3) + 
               s2*(-3*Power(t1,2) + t1*t2 + 2*Power(t2,2)))) + 
         Power(s1,2)*(s*(3*Power(t1 - t2,3) + Power(s2,2)*(3*t1 - t2) + 
               2*s2*(-3*Power(t1,2) + 5*t1*t2 + Power(t2,2))) - 
            t2*(Power(s2,3) - 2*Power(t1,3) - 4*Power(s2,2)*(t1 - 2*t2) + 
               6*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 4*Power(t2,3) + 
               s2*(5*Power(t1,2) - 14*t1*t2 + 5*Power(t2,2)))))*
       lg2(-(((-s1 - t1 + t2)*(-s + s2 - t1 + t2))/(s2 - t1 + t2))))/
     (s1*s2*(-s + s2 - t1)*t2*Power(s2 - t1 + t2,3)) + 
    (16*(Power(s,5)*Power(t1 - t2,2) - 
         s1*s2*(s1 + s2)*Power(s1*(s2 - t1) - s2*t2,2) + 
         Power(s,4)*(Power(t1 - t2,2)*(t1 + t2) - 
            s2*(Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
            s1*(-3*Power(t1,2) + 8*t1*t2 - Power(t2,2) + 3*s2*(t1 + t2))) \
+ s*(Power(s1,4)*s2*(s2 - t1) + 
            Power(s2,2)*Power(t2,2)*
             (s2*(4*t1 - t2) + 2*t1*(-2*t1 + t2)) + 
            Power(s1,3)*(4*Power(s2,3) - Power(t1,2)*(t1 - 4*t2) + 
               2*s2*t1*(3*t1 - 2*t2) - 3*Power(s2,2)*(3*t1 + t2)) + 
            s1*s2*t2*(-Power(s2,3) - 4*Power(t1,2)*t2 + 
               s2*t1*(4*t1 + t2) + Power(s2,2)*(-4*t1 + 6*t2)) + 
            Power(s1,2)*(Power(s2,4) + 2*Power(t1,2)*(t1 - 2*t2)*t2 - 
               3*Power(s2,3)*(t1 + 3*t2) + s2*t1*t2*(t1 + 4*t2) + 
               2*Power(s2,2)*(Power(t1,2) + t1*t2 + Power(t2,2)))) + 
         Power(s,3)*(Power(s1,2)*
             (2*Power(s2,2) - 7*s2*t1 + 3*Power(t1,2) - 4*s2*t2 - 
               10*t1*t2) + t2*
             (2*t1*Power(t1 - t2,2) + Power(s2,2)*(-10*t1 + 3*t2) + 
               s2*(5*Power(t1,2) + 6*t1*t2 - 3*Power(t2,2))) + 
            s1*(-(Power(s2,2)*(4*t1 + 7*t2)) + 
               2*s2*(3*Power(t1,2) - 8*t1*t2 + 3*Power(t2,2)) + 
               t1*(-3*Power(t1,2) + 6*t1*t2 + 5*Power(t2,2)))) - 
         Power(s,2)*(Power(s1,3)*
             (3*Power(s2,2) + t1*(t1 - 4*t2) - s2*(5*t1 + t2)) + 
            s2*t2*(Power(s2,2)*(-4*t1 + t2) + 4*t1*t2*(-2*t1 + t2) + 
               s2*(4*Power(t1,2) + 9*t1*t2 - 3*Power(t2,2))) + 
            Power(s1,2)*(3*Power(s2,3) - 11*Power(s2,2)*(t1 + t2) + 
               s2*(10*Power(t1,2) - 12*t1*t2 + Power(t2,2)) + 
               t1*(-3*Power(t1,2) + 9*t1*t2 + 4*Power(t2,2))) + 
            s1*(4*Power(t1,2)*(t1 - 2*t2)*t2 + 6*s2*t1*t2*(t1 + t2) - 
               Power(s2,3)*(t1 + 5*t2) + 
               Power(s2,2)*(Power(t1,2) - 12*t1*t2 + 10*Power(t2,2)))))*
       Log(-s))/((s - s1)*s1*(s - s2)*s2*t1*(s - s2 + t1)*t2*(s - s1 + t2)) \
+ (16*((-(Power(s1,3)*s2*(2*s1 + s2)*t2*(s2 - t1 + t2)*
               Power(s1*(s2 - t1) - s2*t2,2)) + 
            2*Power(s,8)*t1*(s2*t1 + t2*(-t1 + t2)) + 
            Power(s,7)*(s1*(-3*Power(s2,2)*(t1 - t2) + 
                  t2*(11*Power(t1,2) - 14*t1*t2 + 3*Power(t2,2)) + 
                  s2*(-5*Power(t1,2) - 5*t1*t2 + 6*Power(t2,2))) + 
               t1*(-4*Power(s2,2)*t1 + 
                  3*s2*(Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
                  t2*(Power(t1,2) - 8*t1*t2 + 7*Power(t2,2)))) - 
            Power(s,6)*(Power(s1,2)*
                (3*Power(s2,3) - 2*Power(t1,3) - 
                  2*Power(s2,2)*(7*t1 - 8*t2) + 22*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) + 7*Power(t2,3) + 
                  s2*(Power(t1,2) - 16*t1*t2 + 20*Power(t2,2))) + 
               s1*(-3*Power(s2,3)*(t1 - 2*t2) + 
                  Power(s2,2)*
                   (-14*Power(t1,2) + 8*t1*t2 + 7*Power(t2,2)) + 
                  s2*(14*Power(t1,3) + 5*Power(t1,2)*t2 - 
                     8*t1*Power(t2,2) - 5*Power(t2,3)) + 
                  2*t2*(Power(t1,3) - 15*Power(t1,2)*t2 + 
                     17*t1*Power(t2,2) - 3*Power(t2,3))) - 
               t1*(2*Power(s2,3)*t1 - 2*Power(s2,2)*t1*(t1 + 5*t2) + 
                  s2*t2*(7*Power(t1,2) + 11*t1*t2 - 12*Power(t2,2)) + 
                  t2*(Power(t1,3) - 9*t1*Power(t2,2) + 8*Power(t2,3)))) \
- s*Power(s1,2)*(Power(s1,4)*(s2 - t1)*
                (Power(s2,3) + s2*t1*(t1 - 4*t2) + 
                  t1*(3*t1 - t2)*t2 + Power(s2,2)*(-2*t1 + t2)) + 
               s1*Power(t2,2)*
                (3*Power(s2,5) + Power(s2,4)*(-3*t1 + t2) + 
                  3*Power(s2,3)*t2*(t1 + t2) + 
                  Power(s2,2)*Power(t2,2)*(-11*t1 + 5*t2) + 
                  s2*t1*t2*(-3*Power(t1,2) + 14*t1*t2 - 
                     15*Power(t2,2)) + 
                  Power(t1,2)*t2*(Power(t1,2) - Power(t2,2))) + 
               s2*Power(t2,3)*
                (3*Power(s2,4) + t1*Power(t1 - t2,2)*t2 + 
                  Power(s2,3)*(-7*t1 + 6*t2) + 
                  s2*t1*(-2*Power(t1,2) + 8*t1*t2 - 7*Power(t2,2)) + 
                  Power(s2,2)*
                   (6*Power(t1,2) - 13*t1*t2 + 3*Power(t2,2))) + 
               Power(s1,3)*(Power(s2,5) + 
                  Power(t1,2)*(7*t1 - 4*t2)*Power(t2,2) - 
                  Power(s2,4)*(3*t1 + 13*t2) + 
                  Power(s2,3)*
                   (3*Power(t1,2) + 32*t1*t2 - 13*Power(t2,2)) + 
                  s2*t1*t2*(6*Power(t1,2) - 25*t1*t2 + 
                     4*Power(t2,2)) + 
                  Power(s2,2)*
                   (-Power(t1,3) - 25*Power(t1,2)*t2 + 
                     31*t1*Power(t2,2) + Power(t2,3))) + 
               Power(s1,2)*t2*
                (-7*Power(s2,5) + 
                  Power(t1,2)*Power(t2,2)*(-3*t1 + 2*t2) + 
                  Power(s2,4)*(19*t1 + 5*t2) + 
                  Power(s2,3)*
                   (-19*Power(t1,2) + t1*t2 + 6*Power(t2,2)) + 
                  Power(s2,2)*
                   (9*Power(t1,3) - 13*Power(t1,2)*t2 + 
                     10*t1*Power(t2,2) - 6*Power(t2,3)) + 
                  s2*t1*(-2*Power(t1,3) + 7*Power(t1,2)*t2 - 
                     9*t1*Power(t2,2) + 15*Power(t2,3)))) - 
            Power(s,2)*s1*(Power(s1,5)*(s2 - t1)*
                (Power(s2,2) + 2*Power(t1,2) + s2*(-3*t1 + t2)) + 
               s2*Power(t2,3)*
                (-(s2*t1*(t1 - 2*t2)*t2) + t1*(t1 - t2)*Power(t2,2) + 
                  Power(s2,3)*(4*t1 + t2) + 
                  Power(s2,2)*(-4*Power(t1,2) + 2*t1*t2 + Power(t2,2))\
) + Power(s1,4)*(-7*Power(s2,4) + Power(s2,3)*(17*t1 - 2*t2) + 
                  Power(t1,2)*(16*t1 - 5*t2)*t2 + 
                  Power(s2,2)*
                   (-13*Power(t1,2) + 25*t1*t2 + 6*Power(t2,2)) + 
                  s2*(3*Power(t1,3) - 39*Power(t1,2)*t2 - 
                     t1*Power(t2,2) + Power(t2,3))) + 
               s1*Power(t2,2)*
                (7*Power(s2,5) + Power(s2,4)*(-19*t1 + 12*t2) + 
                  Power(s2,3)*
                   (14*Power(t1,2) - 20*t1*t2 + Power(t2,2)) + 
                  s2*t1*t2*(7*Power(t1,2) - 19*t1*t2 + 
                     19*Power(t2,2)) - 
                  t1*t2*(3*Power(t1,3) - 2*Power(t1,2)*t2 - 
                     2*t1*Power(t2,2) + Power(t2,3)) - 
                  Power(s2,2)*
                   (2*Power(t1,3) - 7*Power(t1,2)*t2 + 
                     5*t1*Power(t2,2) + 4*Power(t2,3))) + 
               Power(s1,2)*t2*
                (6*Power(s2,5) + Power(s2,4)*(-18*t1 + t2) + 
                  2*Power(t1,2)*t2*
                   (Power(t1,2) + 3*t1*t2 - 3*Power(t2,2)) + 
                  Power(s2,3)*
                   (27*Power(t1,2) - 28*t1*t2 + 17*Power(t2,2)) + 
                  Power(s2,2)*
                   (-21*Power(t1,3) + 45*Power(t1,2)*t2 - 
                     56*t1*Power(t2,2) + 20*Power(t2,3)) + 
                  s2*(6*Power(t1,4) - 20*Power(t1,3)*t2 + 
                     47*Power(t1,2)*Power(t2,2) - 67*t1*Power(t2,3) - 
                     2*Power(t2,4))) + 
               Power(s1,3)*(-5*Power(s2,5) + 
                  Power(s2,4)*(15*t1 + 19*t2) + 
                  t1*Power(t2,2)*
                   (-21*Power(t1,2) + 14*t1*t2 - 3*Power(t2,2)) + 
                  Power(s2,3)*
                   (-17*Power(t1,2) - 34*t1*t2 + 9*Power(t2,2)) + 
                  Power(s2,2)*
                   (9*Power(t1,3) + 19*Power(t1,2)*t2 - 
                     33*t1*Power(t2,2) - 12*Power(t2,3)) + 
                  s2*(-2*Power(t1,4) - 4*Power(t1,3)*t2 + 
                     45*Power(t1,2)*Power(t2,2) + 18*t1*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            Power(s,4)*(Power(s1,4)*
                (12*Power(t1,3) + Power(s2,2)*(18*t1 - 7*t2) - 
                  8*Power(t1,2)*t2 + 9*t1*Power(t2,2) - Power(t2,3) + 
                  s2*(-28*Power(t1,2) + 3*t1*t2 - 8*Power(t2,2))) + 
               t1*Power(t2,2)*
                (t1*Power(t1 - t2,2)*t2 + 2*Power(s2,3)*(3*t1 + t2) + 
                  3*s2*t2*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
                  Power(s2,2)*
                   (-6*Power(t1,2) - 4*t1*t2 + 6*Power(t2,2))) + 
               Power(s1,3)*(2*Power(s2,4) - 
                  Power(s2,3)*(7*t1 + 11*t2) + 
                  Power(s2,2)*
                   (25*Power(t1,2) - 47*t1*t2 + 14*Power(t2,2)) + 
                  t2*(-22*Power(t1,3) + 37*Power(t1,2)*t2 - 
                     40*t1*Power(t2,2) + 8*Power(t2,3)) + 
                  s2*(-19*Power(t1,3) + 62*Power(t1,2)*t2 + 
                     6*t1*Power(t2,2) + 35*Power(t2,3))) + 
               Power(s1,2)*(Power(s2,4)*(-4*t1 + 7*t2) + 
                  Power(s2,3)*
                   (19*Power(t1,2) - 52*t1*t2 + 36*Power(t2,2)) + 
                  Power(s2,2)*
                   (-21*Power(t1,3) + 22*Power(t1,2)*t2 - 
                     20*t1*Power(t2,2) + 30*Power(t2,3)) + 
                  t2*(3*Power(t1,4) + 7*Power(t1,3)*t2 - 
                     32*Power(t1,2)*Power(t2,2) + 
                     32*t1*Power(t2,3) - 6*Power(t2,4)) + 
                  s2*(6*Power(t1,4) + 15*Power(t1,3)*t2 + 
                     6*Power(t1,2)*Power(t2,2) - 77*t1*Power(t2,3) - 
                     5*Power(t2,4))) - 
               s1*t2*(Power(s2,4)*(6*t1 - 5*t2) + 
                  Power(s2,3)*
                   (6*Power(t1,2) - 19*t1*t2 - 9*Power(t2,2)) + 
                  Power(s2,2)*
                   (-14*Power(t1,3) + 15*Power(t1,2)*t2 + 
                     6*t1*Power(t2,2) + 3*Power(t2,3)) + 
                  2*t1*t2*(3*Power(t1,3) - 3*Power(t1,2)*t2 - 
                     4*t1*Power(t2,2) + 4*Power(t2,3)) + 
                  s2*(2*Power(t1,4) + 4*Power(t1,3)*t2 + 
                     18*Power(t1,2)*Power(t2,2) - 39*t1*Power(t2,3) + 
                     7*Power(t2,4)))) + 
            Power(s,5)*(Power(s1,3)*
                (4*Power(s2,3) - 8*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) + 5*Power(t2,3) + 
                  Power(s2,2)*(-22*t1 + 21*t2) + 
                  s2*(18*Power(t1,2) - 15*t1*t2 + 22*Power(t2,2))) + 
               Power(s1,2)*(3*Power(s2,4) + 
                  Power(s2,3)*(-8*t1 + 13*t2) + 
                  Power(s2,2)*
                   (-22*Power(t1,2) + 31*t1*t2 - 7*Power(t2,2)) + 
                  s2*(24*Power(t1,3) - 23*Power(t1,2)*t2 - 
                     3*t1*Power(t2,2) - 30*Power(t2,3)) + 
                  t2*(7*Power(t1,3) - 46*Power(t1,2)*t2 + 
                     57*t1*Power(t2,2) - 13*Power(t2,3))) + 
               t1*t2*(Power(s2,3)*(6*t1 + t2) + 
                  s2*t2*(6*Power(t1,2) + 11*t1*t2 - 15*Power(t2,2)) + 
                  Power(s2,2)*
                   (-6*Power(t1,2) - 9*t1*t2 + 3*Power(t2,2)) + 
                  t2*(2*Power(t1,3) - 3*Power(t1,2)*t2 - 
                     2*t1*Power(t2,2) + 3*Power(t2,3))) + 
               s1*(3*Power(s2,4)*t2 + 
                  Power(s2,3)*
                   (-8*Power(t1,2) + 19*t1*t2 - 4*Power(t2,2)) + 
                  Power(s2,2)*
                   (10*Power(t1,3) + 5*Power(t1,2)*t2 - 
                     3*t1*Power(t2,2) - 18*Power(t2,3)) + 
                  3*t2*(-Power(t1,4) + 9*Power(t1,2)*Power(t2,2) - 
                     9*t1*Power(t2,3) + Power(t2,4)) - 
                  s2*(2*Power(t1,4) + 19*Power(t1,3)*t2 + 
                     11*Power(t1,2)*Power(t2,2) - 43*t1*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(s,3)*(-(Power(s1,5)*t1*
                  (11*Power(s2,2) - 19*s2*t1 + 8*Power(t1,2) - t1*t2 + 
                    Power(t2,2))) + 
               s2*t1*Power(t2,3)*
                (t1*(t1 - t2)*t2 + Power(s2,2)*(2*t1 + t2) - 
                  s2*(2*Power(t1,2) + t1*t2 - 3*Power(t2,2))) + 
               s1*Power(t2,2)*
                (Power(s2,4)*(-10*t1 + t2) + 
                  Power(s2,3)*
                   (6*Power(t1,2) + t1*t2 + 6*Power(t2,2)) + 
                  s2*t1*t2*(Power(t1,2) - 8*t1*t2 + 10*Power(t2,2)) - 
                  t1*t2*(3*Power(t1,3) - 4*Power(t1,2)*t2 + 
                     Power(t2,3)) + 
                  Power(s2,2)*
                   (4*Power(t1,3) - 5*Power(t1,2)*t2 - 
                     10*t1*Power(t2,2) + 5*Power(t2,3))) - 
               Power(s1,4)*(11*Power(s2,4) - 
                  Power(s2,3)*(26*t1 + 3*t2) + 
                  Power(s2,2)*
                   (23*Power(t1,2) - 44*t1*t2 - 6*Power(t2,2)) + 
                  t2*(-29*Power(t1,3) + 17*Power(t1,2)*t2 - 
                     10*t1*Power(t2,2) + Power(t2,3)) + 
                  s2*(-8*Power(t1,3) + 72*Power(t1,2)*t2 + 
                     10*t1*Power(t2,2) + 9*Power(t2,3))) + 
               Power(s1,2)*t2*
                (-4*Power(s2,5) + Power(s2,4)*(8*t1 - 2*t2) + 
                  Power(s2,3)*
                   (11*Power(t1,2) - 37*t1*t2 + 22*Power(t2,2)) + 
                  t1*t2*(6*Power(t1,3) - 10*t1*Power(t2,2) + 
                     5*Power(t2,3)) + 
                  Power(s2,2)*
                   (-21*Power(t1,3) + 45*Power(t1,2)*t2 - 
                     39*t1*Power(t2,2) + 25*Power(t2,3)) + 
                  s2*(6*Power(t1,4) - 15*Power(t1,3)*t2 + 
                     45*Power(t1,2)*Power(t2,2) - 76*t1*Power(t2,3) + 
                     5*Power(t2,4))) + 
               Power(s1,3)*(-4*Power(s2,5) + 
                  2*Power(s2,4)*(8*t1 + t2) + 
                  Power(s2,3)*
                   (-27*Power(t1,2) + 21*t1*t2 - 26*Power(t2,2)) + 
                  Power(s2,2)*
                   (21*Power(t1,3) - 17*Power(t1,2)*t2 + 
                     13*t1*Power(t2,2) - 19*Power(t2,3)) + 
                  t2*(-Power(t1,4) - 21*Power(t1,3)*t2 + 
                     24*Power(t1,2)*Power(t2,2) - 16*t1*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  s2*(-6*Power(t1,4) - 3*Power(t1,3)*t2 + 
                     16*Power(t1,2)*Power(t2,2) + 68*t1*Power(t2,3) + 
                     16*Power(t2,4)))))/(Power(s + t2,2)*(s2 - t1 + t2)) \
- (Power(s1,3)*s2*(2*s1 + s2)*(s1 - t2)*(s1 + t1 - t2)*
             Power(s1*(s2 - t1) - s2*t2,2) + 
            Power(s,7)*(s1 - t2)*t2*(s1*(t1 - 3*t2) + 2*t2*(-t1 + t2)) + 
            Power(s,6)*((t1 - t2)*Power(t2,2)*
                (Power(t1,2) - 7*s2*t2 + t1*t2 + 2*Power(t2,2)) + 
               Power(s1,3)*(-2*s2*t1 + Power(t1,2) - 3*s2*t2 - 
                  8*t1*t2 + 12*Power(t2,2)) + 
               Power(s1,2)*(-2*s2*Power(t1,2) + Power(t1,3) - 
                  3*s2*t1*t2 - 5*Power(t1,2)*t2 + 15*s2*Power(t2,2) + 
                  16*t1*Power(t2,2) - 23*Power(t2,3)) + 
               s1*t2*(2*s2*Power(t1,2) - 2*Power(t1,3) + 12*s2*t1*t2 + 
                  3*Power(t1,2)*t2 - 19*s2*Power(t2,2) - 
                  10*t1*Power(t2,2) + 13*Power(t2,3))) + 
            Power(s,5)*(Power(s1,4)*
                (-2*Power(s2,2) + 10*s2*t1 - 6*Power(t1,2) + 
                  11*s2*t2 + 22*t1*t2 - 15*Power(t2,2)) + 
               Power(s1,3)*(-5*Power(t1,3) + 11*Power(s2,2)*t2 + 
                  24*Power(t1,2)*t2 - 37*t1*Power(t2,2) + 
                  32*Power(t2,3) + 
                  s2*(9*Power(t1,2) + 8*t1*t2 - 59*Power(t2,2))) + 
               Power(s1,2)*(Power(t1,4) + 8*Power(t1,3)*t2 - 
                  22*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) - 
                  22*Power(t2,4) + 
                  Power(s2,2)*
                   (2*Power(t1,2) + 11*t1*t2 - 26*Power(t2,2)) - 
                  s2*(Power(t1,3) + 10*Power(t1,2)*t2 + 
                     41*t1*Power(t2,2) - 86*Power(t2,3))) + 
               s1*t2*(-2*Power(t1,4) - 3*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) + 
                  5*Power(t2,4) + 
                  Power(s2,2)*
                   (-2*Power(t1,2) - 21*t1*t2 + 26*Power(t2,2)) + 
                  s2*(2*Power(t1,3) + 7*Power(t1,2)*t2 + 
                     24*t1*Power(t2,2) - 44*Power(t2,3))) + 
               Power(t2,2)*(Power(s2,2)*(10*t1 - 9*t2)*t2 - 
                  s2*(Power(t1,3) + 5*Power(t1,2)*t2 - 6*Power(t2,3)) + 
                  t1*(Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
                     Power(t2,3)))) + 
            s*Power(s1,2)*(-2*Power(s1,6)*Power(s2 - t1,2) - 
               Power(s1,5)*(s2 - t1)*
                (4*Power(s2,2) - 11*s2*t1 - Power(t1,2) - 14*s2*t2 + 
                  7*t1*t2) + 
               Power(s1,4)*(Power(s2,4) + 
                  Power(s2,3)*(-4*t1 + 18*t2) + 
                  2*Power(s2,2)*
                   (9*Power(t1,2) - 23*t1*t2 - 19*Power(t2,2)) + 
                  Power(t1,2)*
                   (Power(t1,2) + 2*t1*t2 - 9*Power(t2,2)) - 
                  8*s2*t1*(2*Power(t1,2) - 2*t1*t2 - 7*Power(t2,2))) + 
               s2*Power(t2,3)*
                (-(Power(s2,3)*(t1 - 2*t2)) - t1*Power(t1 - t2,2)*t2 + 
                  6*Power(s2,2)*t2*(-t1 + t2) + 
                  s2*(-2*Power(t1,3) + 11*Power(t1,2)*t2 - 
                     11*t1*Power(t2,2) + Power(t2,3))) + 
               s1*Power(t2,2)*
                (Power(s2,4)*(3*t1 - 7*t2) - Power(t1,4)*t2 + 
                  2*Power(s2,3)*(11*t1 - 12*t2)*t2 + 
                  Power(t1,2)*Power(t2,3) + 
                  s2*t1*t2*(5*Power(t1,2) + 4*t1*t2 - 
                     11*Power(t2,2)) + 
                  Power(s2,2)*
                   (6*Power(t1,3) - 41*Power(t1,2)*t2 + 
                     38*t1*Power(t2,2) + 5*Power(t2,3))) - 
               Power(s1,2)*t2*
                (3*Power(s2,4)*(t1 - 3*t2) + 
                  10*Power(s2,3)*(3*t1 - 4*t2)*t2 + 
                  3*Power(t1,2)*t2*
                   (-Power(t1,2) + t1*t2 + Power(t2,2)) + 
                  s2*t1*(-2*Power(t1,3) + 16*Power(t1,2)*t2 + 
                     18*t1*Power(t2,2) - 51*Power(t2,3)) + 
                  Power(s2,2)*
                   (5*Power(t1,3) - 65*Power(t1,2)*t2 + 
                     59*t1*Power(t2,2) + 29*Power(t2,3))) + 
               Power(s1,3)*(Power(s2,4)*(t1 - 5*t2) + 
                  18*Power(s2,3)*(t1 - 2*t2)*t2 + 
                  Power(t1,2)*t2*
                   (-3*Power(t1,2) + 2*t1*t2 + 6*Power(t2,2)) + 
                  s2*t1*(-2*Power(t1,3) + 27*Power(t1,2)*t2 + 
                     6*t1*Power(t2,2) - 78*Power(t2,3)) + 
                  Power(s2,2)*
                   (Power(t1,3) - 53*Power(t1,2)*t2 + 
                     63*t1*Power(t2,2) + 49*Power(t2,3)))) + 
            Power(s,4)*(Power(s1,5)*
                (3*Power(s2,2) - 22*s2*t1 + 14*Power(t1,2) - 
                  17*s2*t2 - 28*t1*t2 + 6*Power(t2,2)) + 
               Power(t2,3)*(Power(t1,2)*Power(t1 - t2,2) + 
                  Power(s2,3)*(-7*t1 + 5*t2) + 
                  Power(s2,2)*
                   (9*Power(t1,2) - 3*t1*t2 - 6*Power(t2,2)) + 
                  s2*t1*(-3*Power(t1,2) + t1*t2 + 2*Power(t2,2))) + 
               Power(s1,4)*(2*Power(s2,3) + 10*Power(t1,3) - 
                  49*Power(t1,2)*t2 + 53*t1*Power(t2,2) - 
                  14*Power(t2,3) - Power(s2,2)*(7*t1 + 30*t2) + 
                  s2*(-11*Power(t1,2) + t1*t2 + 88*Power(t2,2))) + 
               Power(s1,3)*(Power(s2,3)*(2*t1 - 9*t2) - 
                  4*Power(s2,2)*
                   (3*Power(t1,2) + 6*t1*t2 - 23*Power(t2,2)) + 
                  s2*(13*Power(t1,3) + 6*Power(t1,2)*t2 + 
                     47*t1*Power(t2,2) - 135*Power(t2,3)) - 
                  2*(2*Power(t1,4) + 7*Power(t1,3)*t2 - 
                     31*Power(t1,2)*Power(t2,2) + 17*t1*Power(t2,3) - 
                     5*Power(t2,4))) - 
               Power(s1,2)*(Power(s2,3)*(11*t1 - 17*t2)*t2 + 
                  Power(s2,2)*
                   (2*Power(t1,3) - 21*Power(t1,2)*t2 - 
                     62*t1*Power(t2,2) + 110*Power(t2,3)) + 
                  s2*(-2*Power(t1,4) + 23*Power(t1,3)*t2 + 
                     Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                     78*Power(t2,4)) + 
                  t2*(-9*Power(t1,4) - Power(t1,3)*t2 + 
                     24*Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                     Power(t2,4))) - 
               s1*t2*(Power(s2,3)*t2*(-16*t1 + 15*t2) + 
                  Power(s2,2)*
                   (-2*Power(t1,3) + 18*Power(t1,2)*t2 + 
                     28*t1*Power(t2,2) - 51*Power(t2,3)) + 
                  t2*(6*Power(t1,4) - 6*Power(t1,3)*t2 + 
                     Power(t1,2)*Power(t2,2) - 2*t1*Power(t2,3) + 
                     Power(t2,4)) + 
                  s2*(2*Power(t1,4) - 14*Power(t1,3)*t2 - 
                     3*Power(t1,2)*Power(t2,2) + 8*t1*Power(t2,3) + 
                     14*Power(t2,4)))) + 
            Power(s,2)*s1*(Power(s1,6)*
                (3*Power(s2,2) + t1*(9*t1 - 4*t2) - 4*s2*(4*t1 + t2)) + 
               s2*Power(t2,3)*
                (t1*Power(t2,2)*(-t1 + t2) + 
                  Power(s2,3)*(-4*t1 + 2*t2) + 
                  4*Power(s2,2)*(Power(t1,2) - Power(t2,2)) - 
                  s2*t2*(Power(t1,2) - 4*t1*t2 + Power(t2,2))) + 
               Power(s1,5)*(Power(s2,3) - 
                  Power(s2,2)*(25*t1 + 31*t2) + 
                  5*t1*(Power(t1,2) - 6*t1*t2 + 3*Power(t2,2)) + 
                  s2*(15*Power(t1,2) + 50*t1*t2 + 22*Power(t2,2))) - 
               s1*Power(t2,2)*
                (Power(s2,4)*(-6*t1 + 4*t2) + 
                  Power(s2,3)*
                   (4*Power(t1,2) + 21*t1*t2 - 23*Power(t2,2)) - 
                  t1*t2*(3*Power(t1,3) - 2*Power(t1,2)*t2 - 
                     2*t1*Power(t2,2) + Power(t2,3)) + 
                  s2*t2*(9*Power(t1,3) + Power(t1,2)*t2 - 
                     15*t1*Power(t2,2) + 2*Power(t2,3)) + 
                  4*Power(s2,2)*
                   (Power(t1,3) - 8*Power(t1,2)*t2 + 
                     4*t1*Power(t2,2) + 4*Power(t2,3))) - 
               Power(s1,4)*(2*Power(s2,4) + 
                  Power(s2,3)*(-5*t1 + 12*t2) + 
                  Power(s2,2)*
                   (35*Power(t1,2) - 46*t1*t2 - 98*Power(t2,2)) + 
                  t1*(4*Power(t1,3) + 8*Power(t1,2)*t2 - 
                     45*t1*Power(t2,2) + 20*Power(t2,3)) + 
                  s2*(-37*Power(t1,3) + 28*Power(t1,2)*t2 + 
                     90*t1*Power(t2,2) + 39*Power(t2,3))) + 
               Power(s1,2)*t2*
                (Power(s2,3)*
                   (-4*Power(t1,2) + 47*t1*t2 - 44*Power(t2,2)) + 
                  t1*t2*(-10*Power(t1,3) + 10*Power(t1,2)*t2 + 
                     9*t1*Power(t2,2) - 4*Power(t2,3)) + 
                  Power(s2,2)*
                   (11*Power(t1,3) - 84*Power(t1,2)*t2 + 
                     18*t1*Power(t2,2) + 80*Power(t2,3)) + 
                  s2*(-6*Power(t1,4) + 42*Power(t1,3)*t2 + 
                     8*Power(t1,2)*Power(t2,2) - 68*t1*Power(t2,3) - 
                     3*Power(t2,4))) + 
               Power(s1,3)*(-2*Power(s2,4)*(t1 - 2*t2) + 
                  Power(s2,3)*
                   (4*Power(t1,2) - 31*t1*t2 + 36*Power(t2,2)) + 
                  t1*t2*(11*Power(t1,3) - 6*Power(t1,2)*t2 - 
                     31*t1*Power(t2,2) + 12*Power(t2,3)) - 
                  Power(s2,2)*
                   (7*Power(t1,3) - 88*Power(t1,2)*t2 + 
                     27*t1*Power(t2,2) + 133*Power(t2,3)) + 
                  s2*(6*Power(t1,4) - 67*Power(t1,3)*t2 + 
                     7*Power(t1,2)*Power(t2,2) + 108*t1*Power(t2,3) + 
                     26*Power(t2,4)))) + 
            Power(s,3)*(Power(s1,6)*
                (-2*Power(s2,2) + 13*s2*(2*t1 + t2) + 
                  t1*(-16*t1 + 17*t2)) + 
               s2*(s2 - t1)*Power(t2,3)*
                (Power(s2,2)*(2*t1 - t2) + t1*t2*(-t1 + t2) + 
                  s2*(-2*Power(t1,2) + t1*t2 + 2*Power(t2,2))) - 
               Power(s1,5)*(Power(s2,3) - 
                  3*Power(s2,2)*(7*t1 + 12*t2) + 
                  t1*(10*Power(t1,2) - 53*t1*t2 + 44*Power(t2,2)) + 
                  s2*(3*Power(t1,2) + 35*t1*t2 + 66*Power(t2,2))) + 
               s1*Power(t2,2)*
                (Power(s2,4)*(-4*t1 + 3*t2) + 
                  2*Power(s2,3)*
                   (3*Power(t1,2) + 9*t1*t2 - 11*Power(t2,2)) - 
                  t1*t2*(3*Power(t1,3) - 4*Power(t1,2)*t2 + 
                     Power(t2,3)) + 
                  s2*t2*(7*Power(t1,3) - 4*Power(t1,2)*t2 - 
                     6*t1*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(s2,2)*
                   (-2*Power(t1,3) - 16*Power(t1,2)*t2 + 
                     9*t1*Power(t2,2) + 13*Power(t2,3))) + 
               Power(s1,4)*(6*Power(t1,4) - 3*Power(s2,3)*(t1 - 5*t2) + 
                  14*Power(t1,3)*t2 - 79*Power(t1,2)*Power(t2,2) + 
                  43*t1*Power(t2,3) + Power(t2,4) + 
                  Power(s2,2)*
                   (30*Power(t1,2) + t1*t2 - 122*Power(t2,2)) + 
                  s2*(-35*Power(t1,3) + 18*Power(t1,2)*t2 + 
                     16*t1*Power(t2,2) + 107*Power(t2,3))) + 
               Power(s1,2)*t2*
                (Power(s2,4)*(2*t1 - 3*t2) + 
                  Power(s2,3)*(-45*t1*t2 + 51*Power(t2,2)) + 
                  Power(s2,2)*
                   (-7*Power(t1,3) + 48*Power(t1,2)*t2 + 
                     23*t1*Power(t2,2) - 81*Power(t2,3)) + 
                  t2*(12*Power(t1,4) - 12*Power(t1,3)*t2 - 
                     6*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                     Power(t2,4)) + 
                  s2*(6*Power(t1,4) - 39*Power(t1,3)*t2 + 
                     12*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) + 
                     11*Power(t2,4))) + 
               Power(s1,3)*(Power(s2,4)*t2 + 
                  Power(s2,3)*
                   (-2*Power(t1,2) + 28*t1*t2 - 45*Power(t2,2)) + 
                  Power(s2,2)*
                   (7*Power(t1,3) - 60*Power(t1,2)*t2 - 
                     53*t1*Power(t2,2) + 156*Power(t2,3)) + 
                  t2*(-15*Power(t1,4) + 5*Power(t1,3)*t2 + 
                     46*Power(t1,2)*Power(t2,2) - 18*t1*Power(t2,3) - 
                     2*Power(t2,4)) - 
                  s2*(6*Power(t1,4) - 63*Power(t1,3)*t2 + 
                     21*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) + 
                     67*Power(t2,4)))))/(Power(s1 - t2,2)*(s1 + t1 - t2)))*
       Log(-s1))/(s*Power(s - s1,2)*s1*s2*t1*(s - s2 + t1)*t2*(s - s1 + t2)) \
+ (16*((-(s1*Power(s2,3)*(s1 + 2*s2)*t1*(s1 + t1 - t2)*
               Power(s1*(s2 - t1) - s2*t2,2)) + 
            2*Power(s,8)*t2*(Power(t1,2) + s1*t2 - t1*t2) + 
            Power(s,7)*(Power(s1,2)*(3*s2*(t1 - t2) - 4*Power(t2,2)) + 
               t1*(t1 - t2)*(3*s2*t1 - 11*s2*t2 + 7*t1*t2 - 
                  Power(t2,2)) + 
               s1*(s2*(6*Power(t1,2) - 5*t1*t2 - 5*Power(t2,2)) + 
                  3*t2*(-Power(t1,2) + 2*t1*t2 + Power(t2,2)))) - 
            s*Power(s2,2)*(Power(s1,5)*
                (Power(s2,3) - 7*Power(s2,2)*t1 + 3*s2*Power(t1,2) + 
                  3*Power(t1,3)) + 
               s2*t1*Power(t2,2)*
                (-Power(t1,4) + Power(s2,3)*(t1 - 3*t2) + 
                  s2*Power(t1,2)*(2*t1 - 3*t2) + 
                  Power(t1,2)*Power(t2,2) + 
                  Power(s2,2)*t1*(-4*t1 + 7*t2)) + 
               Power(s1,4)*(Power(s2,4) + Power(t1,3)*(6*t1 - 7*t2) + 
                  s2*Power(t1,2)*(t1 - 3*t2) - 
                  Power(s2,3)*(13*t1 + 3*t2) + 
                  Power(s2,2)*t1*(5*t1 + 19*t2)) + 
               Power(s1,3)*(Power(s2,4)*(t1 - 3*t2) + 
                  3*s2*Power(t1,3)*(t1 + t2) + 
                  Power(s2,2)*t1*
                   (6*Power(t1,2) + t1*t2 - 19*Power(t2,2)) + 
                  Power(s2,3)*
                   (-13*Power(t1,2) + 32*t1*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (3*Power(t1,2) - 13*t1*t2 + 6*Power(t2,2))) + 
               s1*t2*(Power(t1,4)*Power(t1 - t2,2) + 
                  s2*Power(t1,3)*
                   (-15*Power(t1,2) + 14*t1*t2 - 3*Power(t2,2)) - 
                  Power(s2,4)*(Power(t1,2) - 7*t1*t2 + Power(t2,2)) + 
                  Power(s2,3)*t1*
                   (4*Power(t1,2) - 25*t1*t2 + 6*Power(t2,2)) + 
                  Power(s2,2)*t1*
                   (15*Power(t1,3) - 9*Power(t1,2)*t2 + 
                     7*t1*Power(t2,2) - 2*Power(t2,3))) + 
               Power(s1,2)*(s2*Power(t1,4)*(5*t1 - 11*t2) + 
                  Power(s2,4)*t2*(-5*t1 + 3*t2) + 
                  Power(t1,3)*t2*
                   (-7*Power(t1,2) + 8*t1*t2 - 2*Power(t2,2)) + 
                  Power(s2,3)*
                   (Power(t1,3) + 31*Power(t1,2)*t2 - 
                     25*t1*Power(t2,2) - Power(t2,3)) + 
                  Power(s2,2)*t1*
                   (-6*Power(t1,3) + 10*Power(t1,2)*t2 - 
                     13*t1*Power(t2,2) + 9*Power(t2,3)))) - 
            Power(s,6)*(Power(s1,3)*
                (3*Power(s2,2) + 6*s2*t1 - 3*s2*t2 - 2*Power(t2,2)) + 
               Power(s2,2)*(7*Power(t1,3) - 29*Power(t1,2)*t2 + 
                  22*t1*Power(t2,2) - 2*Power(t2,3)) - 
               t1*t2*(8*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  Power(t2,3)) + 
               2*s2*t1*(-3*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(2*Power(s2,2)*(8*t1 - 7*t2) + 
                  2*Power(t2,2)*(5*t1 + t2) + 
                  s2*(7*Power(t1,2) + 8*t1*t2 - 14*Power(t2,2))) + 
               s1*(t1*t2*(12*Power(t1,2) - 11*t1*t2 - 7*Power(t2,2)) + 
                  Power(s2,2)*
                   (20*Power(t1,2) - 16*t1*t2 + Power(t2,2)) + 
                  s2*(-5*Power(t1,3) - 8*Power(t1,2)*t2 + 
                     5*t1*Power(t2,2) + 14*Power(t2,3)))) + 
            Power(s,5)*(3*Power(s1,4)*s2*(s2 + t1) + 
               Power(s2,3)*(5*Power(t1,3) - 25*Power(t1,2)*t2 + 
                  20*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(t1,2)*t2*
                (3*Power(t1,3) - 2*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-13*Power(t1,3) + 57*Power(t1,2)*t2 - 
                  46*t1*Power(t2,2) + 7*Power(t2,3)) + 
               3*s2*(Power(t1,5) - 9*Power(t1,4)*t2 + 
                  9*Power(t1,3)*Power(t2,2) - t1*Power(t2,4)) + 
               Power(s1,3)*(4*Power(s2,3) + 
                  Power(s2,2)*(13*t1 - 8*t2) + t1*t2*(t1 + 6*t2) + 
                  s2*(-4*Power(t1,2) + 19*t1*t2 - 8*Power(t2,2))) + 
               Power(s1,2)*(Power(s2,3)*(21*t1 - 22*t2) + 
                  Power(s2,2)*
                   (-7*Power(t1,2) + 31*t1*t2 - 22*Power(t2,2)) + 
                  3*t1*t2*(Power(t1,2) - 3*t1*t2 - 2*Power(t2,2)) + 
                  s2*(-18*Power(t1,3) - 3*Power(t1,2)*t2 + 
                     5*t1*Power(t2,2) + 10*Power(t2,3))) + 
               s1*(Power(t1,2)*t2*
                   (-15*Power(t1,2) + 11*t1*t2 + 6*Power(t2,2)) + 
                  Power(s2,3)*
                   (22*Power(t1,2) - 15*t1*t2 + 18*Power(t2,2)) - 
                  Power(s2,2)*
                   (30*Power(t1,3) + 3*Power(t1,2)*t2 + 
                     23*t1*Power(t2,2) - 24*Power(t2,3)) - 
                  s2*(8*Power(t1,4) - 43*Power(t1,3)*t2 + 
                     11*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) + 
                     2*Power(t2,4)))) - 
            Power(s,2)*s2*(Power(s1,5)*s2*
                (-5*Power(s2,2) + 6*s2*t1 + 7*Power(t1,2)) + 
               Power(s1,4)*(-7*Power(s2,4) + 
                  s2*Power(t1,2)*(12*t1 - 19*t2) + 
                  Power(s2,2)*t1*(t1 - 18*t2) + 
                  Power(t1,3)*(t1 + 4*t2) + 
                  Power(s2,3)*(19*t1 + 15*t2)) + 
               Power(s1,3)*(Power(s2,5) + 
                  Power(s2,4)*(-2*t1 + 17*t2) + 
                  Power(s2,3)*
                   (9*Power(t1,2) - 34*t1*t2 - 17*Power(t2,2)) + 
                  Power(t1,3)*
                   (Power(t1,2) + 2*t1*t2 - 4*Power(t2,2)) + 
                  s2*Power(t1,2)*
                   (Power(t1,2) - 20*t1*t2 + 14*Power(t2,2)) + 
                  Power(s2,2)*t1*
                   (17*Power(t1,2) - 28*t1*t2 + 27*Power(t2,2))) - 
               s2*t2*(Power(s2,3)*t1*(5*t1 - 16*t2)*t2 + 
                  2*Power(s2,4)*Power(t2,2) + 
                  2*s2*Power(t1,2)*t2*
                   (3*Power(t1,2) - 3*t1*t2 - Power(t2,2)) + 
                  Power(s2,2)*Power(t1,2)*
                   (3*Power(t1,2) - 14*t1*t2 + 21*Power(t2,2)) + 
                  Power(t1,3)*
                   (Power(t1,3) - 2*Power(t1,2)*t2 - 
                     2*t1*Power(t2,2) + 3*Power(t2,3))) + 
               Power(s1,2)*(Power(s2,5)*(t1 - 4*t2) + 
                  Power(t1,4)*(2*t1 - t2)*t2 + 
                  Power(s2,4)*
                   (6*Power(t1,2) + 25*t1*t2 - 13*Power(t2,2)) + 
                  Power(s2,2)*t1*
                   (20*Power(t1,3) - 56*Power(t1,2)*t2 + 
                     45*t1*Power(t2,2) - 21*Power(t2,3)) - 
                  s2*Power(t1,2)*
                   (4*Power(t1,3) + 5*Power(t1,2)*t2 - 
                     7*t1*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(s2,3)*
                   (-12*Power(t1,3) - 33*Power(t1,2)*t2 + 
                     19*t1*Power(t2,2) + 9*Power(t2,3))) + 
               s1*(Power(t1,5)*t2*(-t1 + t2) + 
                  Power(s2,5)*t2*(-t1 + 5*t2) + 
                  s2*Power(t1,3)*t2*
                   (19*Power(t1,2) - 19*t1*t2 + 7*Power(t2,2)) + 
                  Power(s2,4)*
                   (Power(t1,3) - Power(t1,2)*t2 - 
                     39*t1*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(s2,3)*
                   (3*Power(t1,4) + 18*Power(t1,3)*t2 + 
                     45*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) - 
                     2*Power(t2,4)) + 
                  Power(s2,2)*t1*
                   (-2*Power(t1,4) - 67*Power(t1,3)*t2 + 
                     47*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            Power(s,3)*(-4*Power(s1,5)*Power(s2,2)*(s2 + t1) + 
               Power(s1,4)*s2*
                (-11*Power(s2,3) + Power(t1,2)*(t1 - 10*t2) - 
                  2*s2*t1*(t1 - 4*t2) + 2*Power(s2,2)*(t1 + 8*t2)) + 
               Power(s1,3)*(Power(t1,3)*t2*(t1 + 2*t2) + 
                  Power(s2,4)*(3*t1 + 26*t2) + 
                  Power(s2,3)*
                   (-26*Power(t1,2) + 21*t1*t2 - 27*Power(t2,2)) + 
                  s2*Power(t1,2)*
                   (6*Power(t1,2) + t1*t2 + 6*Power(t2,2)) + 
                  Power(s2,2)*t1*
                   (22*Power(t1,2) - 37*t1*t2 + 11*Power(t2,2))) - 
               Power(s1,2)*(11*Power(s2,5)*t2 + 
                  Power(t1,3)*t2*
                   (-3*Power(t1,2) + t1*t2 + 2*Power(t2,2)) + 
                  Power(s2,4)*
                   (-6*Power(t1,2) - 44*t1*t2 + 23*Power(t2,2)) + 
                  Power(s2,3)*
                   (19*Power(t1,3) - 13*Power(t1,2)*t2 + 
                     17*t1*Power(t2,2) - 21*Power(t2,3)) + 
                  s2*Power(t1,2)*
                   (-5*Power(t1,3) + 10*Power(t1,2)*t2 + 
                     5*t1*Power(t2,2) - 4*Power(t2,3)) + 
                  Power(s2,2)*t1*
                   (-25*Power(t1,3) + 39*Power(t1,2)*t2 - 
                     45*t1*Power(t2,2) + 21*Power(t2,3))) - 
               s2*(Power(s2,4)*t2*
                   (Power(t1,2) - t1*t2 + 8*Power(t2,2)) + 
                  Power(s2,3)*t1*
                   (Power(t1,3) - 10*Power(t1,2)*t2 + 
                     17*t1*Power(t2,2) - 29*Power(t2,3)) + 
                  s2*Power(t1,2)*t2*
                   (-5*Power(t1,3) + 10*Power(t1,2)*t2 - 
                     6*Power(t2,3)) + 
                  Power(t1,3)*t2*
                   (Power(t1,3) - 4*t1*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(s2,2)*t1*
                   (-3*Power(t1,4) + 16*Power(t1,3)*t2 - 
                     24*Power(t1,2)*Power(t2,2) + 21*t1*Power(t2,3) + 
                     Power(t2,4))) + 
               s1*(19*Power(s2,5)*Power(t2,2) + 
                  Power(t1,4)*Power(t2,2)*(-t1 + t2) + 
                  s2*Power(t1,3)*t2*
                   (10*Power(t1,2) - 8*t1*t2 + Power(t2,2)) - 
                  Power(s2,4)*
                   (9*Power(t1,3) + 10*Power(t1,2)*t2 + 
                     72*t1*Power(t2,2) - 8*Power(t2,3)) + 
                  Power(s2,3)*
                   (16*Power(t1,4) + 68*Power(t1,3)*t2 + 
                     16*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) - 
                     6*Power(t2,4)) + 
                  Power(s2,2)*t1*
                   (5*Power(t1,4) - 76*Power(t1,3)*t2 + 
                     45*Power(t1,2)*Power(t2,2) - 15*t1*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            Power(s,4)*(Power(t1,3)*Power(t1 - t2,2)*Power(t2,2) + 
               Power(s1,4)*s2*
                (2*Power(s2,2) + 7*s2*t1 + 5*Power(t1,2) - 4*s2*t2 - 
                  6*t1*t2) + 
               Power(s2,3)*t1*
                (8*Power(t1,3) - 40*Power(t1,2)*t2 + 
                  37*t1*Power(t2,2) - 22*Power(t2,3)) - 
               Power(s2,4)*(Power(t1,3) - 9*Power(t1,2)*t2 + 
                  8*t1*Power(t2,2) - 12*Power(t2,3)) + 
               2*s2*Power(t1,2)*t2*
                (-4*Power(t1,3) + 4*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,2)*t1*
                (-6*Power(t1,4) + 32*Power(t1,3)*t2 - 
                  32*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s1,3)*(2*Power(t1,2)*t2*(t1 + 3*t2) - 
                  Power(s2,3)*(11*t1 + 7*t2) + 
                  s2*t1*(9*Power(t1,2) + 19*t1*t2 - 6*Power(t2,2)) + 
                  Power(s2,2)*
                   (36*Power(t1,2) - 52*t1*t2 + 19*Power(t2,2))) - 
               Power(s1,2)*(Power(s2,4)*(7*t1 - 18*t2) + 
                  Power(s2,3)*
                   (-14*Power(t1,2) + 47*t1*t2 - 25*Power(t2,2)) + 
                  2*Power(t1,2)*t2*
                   (-3*Power(t1,2) + 2*t1*t2 + 3*Power(t2,2)) + 
                  s2*t1*(3*Power(t1,3) + 6*Power(t1,2)*t2 + 
                     15*t1*Power(t2,2) - 14*Power(t2,3)) + 
                  Power(s2,2)*
                   (-30*Power(t1,3) + 20*Power(t1,2)*t2 - 
                     22*t1*Power(t2,2) + 21*Power(t2,3))) + 
               s1*(Power(s2,4)*
                   (-8*Power(t1,2) + 3*t1*t2 - 28*Power(t2,2)) + 
                  3*Power(t1,3)*t2*
                   (-2*Power(t1,2) + t1*t2 + Power(t2,2)) + 
                  Power(s2,3)*
                   (35*Power(t1,3) + 6*Power(t1,2)*t2 + 
                     62*t1*Power(t2,2) - 19*Power(t2,3)) - 
                  s2*t1*(7*Power(t1,4) - 39*Power(t1,3)*t2 + 
                     18*Power(t1,2)*Power(t2,2) + 4*t1*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  Power(s2,2)*
                   (-5*Power(t1,4) - 77*Power(t1,3)*t2 + 
                     6*Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) + 
                     6*Power(t2,4)))))/(Power(s + t1,2)*(s1 + t1 - t2)) + 
         (-(s1*Power(s2,3)*(s1 + 2*s2)*(s2 - t1)*(s2 - t1 + t2)*
               Power(s1*(s2 - t1) - s2*t2,2)) + 
            Power(s,7)*(s2 - t1)*t1*(s2*(3*t1 - t2) + 2*t1*(-t1 + t2)) + 
            Power(s,6)*(2*Power(t1,5) - Power(t1,4)*t2 - 
               Power(t1,2)*Power(t2,3) - 
               Power(s2,3)*(12*Power(t1,2) - 8*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(23*Power(t1,3) - 16*Power(t1,2)*t2 + 
                  5*t1*Power(t2,2) - Power(t2,3)) + 
               s2*t1*(-13*Power(t1,3) + 10*Power(t1,2)*t2 - 
                  3*t1*Power(t2,2) + 2*Power(t2,3)) + 
               s1*(s2 - t1)*(7*Power(t1,2)*(t1 - t2) + 
                  Power(s2,2)*(3*t1 + 2*t2) + 
                  s2*(-12*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)))) + 
            Power(s,5)*(Power(s2,4)*
                (15*Power(t1,2) - 22*t1*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*t2*
                (Power(t1,3) - Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(s2,3)*(-32*Power(t1,3) + 37*Power(t1,2)*t2 - 
                  24*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(22*Power(t1,4) - 19*Power(t1,3)*t2 + 
                  22*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               s2*t1*(-5*Power(t1,4) + Power(t1,3)*t2 - 
                  3*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s1,2)*(s2 - t1)*
                (2*Power(s2,3) - 9*Power(s2,2)*t1 + 
                  Power(t1,2)*(-9*t1 + 10*t2) + 
                  s2*(17*Power(t1,2) - 11*t1*t2 - 2*Power(t2,2))) + 
               s1*(-(Power(s2,4)*(11*t1 + 10*t2)) + 
                  Power(s2,3)*
                   (59*Power(t1,2) - 8*t1*t2 - 9*Power(t2,2)) + 
                  s2*t1*(44*Power(t1,3) - 24*Power(t1,2)*t2 - 
                     7*t1*Power(t2,2) - 2*Power(t2,3)) + 
                  Power(t1,2)*
                   (-6*Power(t1,3) + 5*t1*Power(t2,2) + Power(t2,3)) + 
                  Power(s2,2)*
                   (-86*Power(t1,3) + 41*Power(t1,2)*t2 + 
                     10*t1*Power(t2,2) + Power(t2,3)))) - 
            s*Power(s2,2)*(Power(s1,4)*Power(s2 - t1,3)*
                (s2 - 2*t1 + t2) - 
               2*Power(s1,3)*Power(s2 - t1,3)*
                (2*Power(s2,2) + 3*t1*(t1 - t2) + s2*(-3*t1 + 2*t2)) - 
               s2*(s2 - t1)*Power(t2,2)*
                (2*Power(s2,4) + Power(t1,4) - 
                  Power(t1,2)*Power(t2,2) + Power(s2,3)*(-5*t1 + t2) + 
                  Power(s2,2)*(4*Power(t1,2) - t1*t2 - Power(t2,2)) + 
                  s2*t1*(-2*Power(t1,2) - 3*t1*t2 + 2*Power(t2,2))) - 
               s1*t2*(-4*Power(s2,6) + Power(t1,4)*Power(t1 - t2,2) + 
                  Power(s2,5)*(21*t1 + 10*t2) + 
                  s2*Power(t1,3)*
                   (11*Power(t1,2) - 4*t1*t2 - 5*Power(t2,2)) - 
                  8*Power(s2,4)*
                   (7*Power(t1,2) + 2*t1*t2 - 2*Power(t2,2)) + 
                  Power(s2,2)*t1*
                   (-51*Power(t1,3) + 18*Power(t1,2)*t2 + 
                     16*t1*Power(t2,2) - 2*Power(t2,3)) + 
                  Power(s2,3)*
                   (78*Power(t1,3) - 6*Power(t1,2)*t2 - 
                     27*t1*Power(t2,2) + 2*Power(t2,3))) - 
               Power(s1,2)*(s2 - t1)*
                (2*Power(s2,5) - 3*Power(s2,4)*(4*t1 + 5*t2) + 
                  Power(s2,3)*
                   (26*Power(t1,2) + 31*t1*t2 - 18*Power(t2,2)) + 
                  Power(t1,2)*
                   (Power(t1,3) - 11*Power(t1,2)*t2 + 
                     11*t1*Power(t2,2) - 2*Power(t2,3)) - 
                  Power(s2,2)*
                   (23*Power(t1,3) + 32*Power(t1,2)*t2 - 
                     35*t1*Power(t2,2) + Power(t2,3)) + 
                  s2*t1*(6*Power(t1,3) + 27*Power(t1,2)*t2 - 
                     30*t1*Power(t2,2) + 4*Power(t2,3)))) + 
            Power(s,4)*(-(Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)) - 
               Power(s1,3)*Power(s2 - t1,2)*
                (2*Power(s2,2) - 5*s2*t1 + 5*Power(t1,2) + 2*s2*t2 - 
                  7*t1*t2) - 
               2*Power(s2,5)*
                (3*Power(t1,2) - 14*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,4)*(14*Power(t1,3) - 53*Power(t1,2)*t2 + 
                  49*t1*Power(t2,2) - 10*Power(t2,3)) + 
               Power(s2,2)*t1*
                (Power(t1,4) - 6*Power(t1,3)*t2 + 
                  24*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) - 
                  9*Power(t2,4)) + 
               Power(s2,3)*(-10*Power(t1,4) + 34*Power(t1,3)*t2 - 
                  62*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s2*Power(t1,2)*
                (Power(t1,4) - 2*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 6*t1*Power(t2,3) + 
                  6*Power(t2,4)) - 
               Power(s1,2)*(s2 - t1)*
                (3*Power(s2,4) - Power(s2,3)*(27*t1 + 7*t2) + 
                  Power(s2,2)*
                   (65*Power(t1,2) - 31*t1*t2 - 12*Power(t2,2)) + 
                  3*Power(t1,2)*
                   (2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
                  s2*(-45*Power(t1,3) + 31*Power(t1,2)*t2 + 
                     9*t1*Power(t2,2) - 2*Power(t2,3))) + 
               s1*(Power(s2,5)*(17*t1 + 22*t2) - 
                  Power(s2,4)*
                   (88*Power(t1,2) + t1*t2 - 11*Power(t2,2)) - 
                  Power(t1,3)*t2*
                   (2*Power(t1,2) + t1*t2 - 3*Power(t2,2)) + 
                  Power(s2,3)*
                   (135*Power(t1,3) - 47*Power(t1,2)*t2 - 
                     6*t1*Power(t2,2) - 13*Power(t2,3)) + 
                  Power(s2,2)*
                   (-78*Power(t1,4) + 22*Power(t1,3)*t2 + 
                     Power(t1,2)*Power(t2,2) + 23*t1*Power(t2,3) - 
                     2*Power(t2,4)) + 
                  s2*t1*(14*Power(t1,4) + 8*Power(t1,3)*t2 - 
                     3*Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) + 
                     2*Power(t2,4)))) + 
            Power(s,2)*s2*(2*Power(s1,4)*Power(s2 - t1,2)*
                (Power(s2,2) - t1*(t1 - 2*t2) + s2*t2) - 
               Power(s1,3)*Power(s2 - t1,2)*
                (Power(s2,3) - 4*Power(t1,3) + 4*t1*Power(t2,2) + 
                  5*Power(s2,2)*(-2*t1 + t2) + 
                  s2*(15*Power(t1,2) - 21*t1*t2 + 4*Power(t2,2))) - 
               Power(s1,2)*(s2 - t1)*
                (3*Power(s2,5) - Power(s2,4)*(28*t1 + 25*t2) + 
                  7*Power(s2,3)*
                   (10*Power(t1,2) + 3*t1*t2 - 5*Power(t2,2)) + 
                  Power(t1,3)*(Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
                  s2*t1*(17*Power(t1,3) + 12*Power(t1,2)*t2 - 
                     31*t1*Power(t2,2) + 4*Power(t2,3)) - 
                  Power(s2,2)*
                   (63*Power(t1,3) + 6*Power(t1,2)*t2 - 
                     53*t1*Power(t2,2) + 7*Power(t2,3))) + 
               s2*t2*(Power(s2,5)*(4*t1 - 9*t2) - 
                  5*Power(s2,4)*
                   (3*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
                  Power(s2,2)*t1*
                   (-12*Power(t1,3) + 31*Power(t1,2)*t2 + 
                     6*t1*Power(t2,2) - 11*Power(t2,3)) - 
                  Power(t1,3)*
                   (Power(t1,3) - 2*Power(t1,2)*t2 - 
                     2*t1*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(s2,3)*
                   (20*Power(t1,3) - 45*Power(t1,2)*t2 + 
                     8*t1*Power(t2,2) + 4*Power(t2,3)) + 
                  s2*Power(t1,2)*
                   (4*Power(t1,3) - 9*Power(t1,2)*t2 - 
                     10*t1*Power(t2,2) + 10*Power(t2,3))) + 
               s1*(Power(t1,5)*t2*(-t1 + t2) + 
                  4*Power(s2,6)*(t1 + 4*t2) - 
                  Power(s2,5)*
                   (22*Power(t1,2) + 50*t1*t2 + 15*Power(t2,2)) + 
                  Power(s2,4)*
                   (39*Power(t1,3) + 90*Power(t1,2)*t2 + 
                     28*t1*Power(t2,2) - 37*Power(t2,3)) + 
                  s2*Power(t1,3)*
                   (2*Power(t1,3) - 15*Power(t1,2)*t2 + 
                     t1*Power(t2,2) + 9*Power(t2,3)) - 
                  Power(s2,3)*
                   (26*Power(t1,4) + 108*Power(t1,3)*t2 + 
                     7*Power(t1,2)*Power(t2,2) - 67*t1*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(s2,2)*t1*
                   (3*Power(t1,4) + 68*Power(t1,3)*t2 - 
                     8*Power(t1,2)*Power(t2,2) - 42*t1*Power(t2,3) + 
                     6*Power(t2,4)))) - 
            Power(s,3)*(Power(s1,4)*Power(s2 - t1,2)*t1*
                (s2 - t1 + 2*t2) - 
               Power(s1,3)*Power(s2 - t1,2)*
                (Power(s2,3) + Power(s2,2)*(-13*t1 + 3*t2) - 
                  2*t1*(Power(t1,2) + t1*t2 - 2*Power(t2,2)) + 
                  2*s2*(9*Power(t1,2) - 11*t1*t2 + Power(t2,2))) - 
               Power(s1,2)*(s2 - t1)*
                (2*Power(s2,5) - Power(s2,4)*(34*t1 + 21*t2) + 
                  Power(s2,3)*
                   (88*Power(t1,2) - 22*t1*t2 - 30*Power(t2,2)) + 
                  s2*Power(t1,2)*
                   (13*Power(t1,2) + 8*t1*t2 - 18*Power(t2,2)) - 
                  Power(t1,2)*t2*
                   (Power(t1,2) + 2*t1*t2 - 2*Power(t2,2)) + 
                  Power(s2,2)*
                   (-68*Power(t1,3) + 31*Power(t1,2)*t2 + 
                     30*t1*Power(t2,2) - 7*Power(t2,3))) + 
               s1*(Power(t1,4)*Power(t2,2)*(-t1 + t2) + 
                  13*Power(s2,6)*(t1 + 2*t2) - 
                  Power(s2,5)*
                   (66*Power(t1,2) + 35*t1*t2 + 3*Power(t2,2)) + 
                  Power(s2,4)*
                   (107*Power(t1,3) + 16*Power(t1,2)*t2 + 
                     18*t1*Power(t2,2) - 35*Power(t2,3)) + 
                  s2*Power(t1,3)*
                   (2*Power(t1,3) - 6*Power(t1,2)*t2 - 
                     4*t1*Power(t2,2) + 7*Power(t2,3)) - 
                  Power(s2,3)*
                   (67*Power(t1,4) + 25*Power(t1,3)*t2 + 
                     21*Power(t1,2)*Power(t2,2) - 63*t1*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(s2,2)*t1*
                   (11*Power(t1,4) + 25*Power(t1,3)*t2 + 
                     12*Power(t1,2)*Power(t2,2) - 39*t1*Power(t2,3) + 
                     6*Power(t2,4))) + 
               s2*(Power(s2,5)*(17*t1 - 16*t2)*t2 + 
                  Power(s2,4)*t2*
                   (-44*Power(t1,2) + 53*t1*t2 - 10*Power(t2,2)) - 
                  Power(t1,3)*t2*
                   (Power(t1,3) - 4*t1*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(s2,2)*t1*
                   (-2*Power(t1,4) - 18*Power(t1,3)*t2 + 
                     46*Power(t1,2)*Power(t2,2) + 5*t1*Power(t2,3) - 
                     15*Power(t2,4)) + 
                  Power(s2,3)*
                   (Power(t1,4) + 43*Power(t1,3)*t2 - 
                     79*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  s2*Power(t1,2)*
                   (Power(t1,4) + 3*Power(t1,3)*t2 - 
                     6*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                     12*Power(t2,4)))))/(Power(s2 - t1,2)*(s2 - t1 + t2)))*
       Log(-s2))/(s*s1*Power(s - s2,2)*s2*t1*(s - s2 + t1)*t2*(s - s1 + t2)) \
+ (32*(s - s1 - s2)*(-2*Power(s1,3)*(s2 + t1) + 2*Power(s,3)*(t1 + t2) - 
         Power(s,2)*(-2*Power(t1 + t2,2) + s1*(s2 + 5*t1 + t2) + 
            s2*(t1 + 5*t2)) + 
         Power(s1,2)*(Power(s2,2) - 6*s2*t1 + t1*(t1 + 6*t2)) + 
         t2*(-2*Power(s2,3) + Power(s2,2)*(6*t1 + t2) - 
            2*s2*(3*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
            4*t1*(Power(t1,2) - t1*t2 + Power(t2,2))) - 
         2*s1*(Power(s2,3) + 3*Power(s2,2)*t2 - 3*s2*t1*t2 + 
            t1*(Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) + 
         s*(Power(s1,2)*(s2 + 5*t1 + t2) + Power(s2,2)*(t1 + 5*t2) - 
            s2*(Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) + 
            2*(Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
               Power(t2,3)) + 
            s1*(Power(s2,2) - 3*Power(t1,2) - 4*t1*t2 - Power(t2,2) + 
               6*s2*(t1 + t2))))*Log(-s + s1 + s2))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
    (16*(-(s1*s2*(s1 + 2*s2)*(s2 - t1)*Power(t1 - t2,2)*(s1 + t1 - t2)*
            (s2 + t2)*(s2 - t1 + t2)*Power(s1*(-s2 + t1) + s2*t2,2)) - 
         Power(s,5)*(s2 - t1)*Power(t1 - t2,2)*(s2 + t2)*
          ((t1 - t2)*(t1*Power(t1 - t2,2) - Power(s2,2)*t2 - 
               s2*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2))) + 
            s1*(-2*s2*Power(t1 - t2,2) - 2*Power(s2,2)*t2 + 
               t1*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)))) + 
         Power(s,4)*Power(t1 - t2,2)*
          (Power(t1,2)*Power(t1 - t2,3)*t2*(t1 + t2) + 
            Power(s2,5)*(2*Power(t1,2) - 5*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,4)*(-8*Power(t1,3) + 23*Power(t1,2)*t2 - 
               26*t1*Power(t2,2) + 9*Power(t2,3)) + 
            s2*t1*t2*(-5*Power(t1,4) + 11*Power(t1,3)*t2 - 
               9*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*(9*Power(t1,4) - 31*Power(t1,3)*t2 + 
               47*Power(t1,2)*Power(t2,2) - 30*t1*Power(t2,3) + 
               3*Power(t2,4)) - 
            Power(s2,2)*(3*Power(t1,5) - 18*Power(t1,4)*t2 + 
               34*Power(t1,3)*Power(t2,2) - 
               29*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) + 
               Power(t2,5)) + 
            2*Power(s1,2)*(s2 - t1)*
             (Power(s2,3)*(t1 - 2*t2) + 
               Power(s2,2)*(-4*Power(t1,2) + 7*t1*t2 - 4*Power(t2,2)) + 
               t1*t2*(2*Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               s2*(2*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 2*Power(t2,3))) + 
            s1*(Power(s2,5)*(2*t1 - 7*t2) + 
               Power(s2,4)*(-6*Power(t1,2) + 25*t1*t2 - 
                  11*Power(t2,2)) - 
               Power(s2,3)*(Power(t1,3) + 18*Power(t1,2)*t2 - 
                  27*t1*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*t2*
                (-3*Power(t1,3) + 7*Power(t1,2)*t2 - 9*t1*Power(t2,2) + 
                  5*Power(t2,3)) + 
               s2*t1*(-4*Power(t1,4) + 15*Power(t1,3)*t2 - 
                  18*Power(t1,2)*Power(t2,2) + 15*t1*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(s2,2)*(9*Power(t1,4) - 11*Power(t1,3)*t2 - 
                  3*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
                  3*Power(t2,4)))) - 
         Power(s,3)*(t1 - t2)*
          (Power(t1,3)*Power(t1 - t2,4)*Power(t2,2) + 
            Power(s2,6)*(2*Power(t1,3) - 9*Power(t1,2)*t2 + 
               17*t1*Power(t2,2) - 9*Power(t2,3)) + 
            Power(s2,5)*(-8*Power(t1,4) + 38*Power(t1,3)*t2 - 
               78*Power(t1,2)*Power(t2,2) + 62*t1*Power(t2,3) - 
               15*Power(t2,4)) + 
            s2*Power(t1,2)*Power(t1 - t2,2)*
             (Power(t1,4) + 3*Power(t1,3)*t2 - 
               11*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,4)*(12*Power(t1,5) - 57*Power(t1,4)*t2 + 
               131*Power(t1,3)*Power(t2,2) - 
               141*Power(t1,2)*Power(t2,3) + 57*t1*Power(t2,4) - 
               3*Power(t2,5)) - 
            Power(s2,2)*t1*t2*
             (15*Power(t1,5) - 59*Power(t1,4)*t2 + 
               98*Power(t1,3)*Power(t2,2) - 
               68*Power(t1,2)*Power(t2,3) + 8*t1*Power(t2,4) + 
               6*Power(t2,5)) + 
            Power(s2,3)*(-7*Power(t1,6) + 42*Power(t1,5)*t2 - 
               116*Power(t1,4)*Power(t2,2) + 
               156*Power(t1,3)*Power(t2,3) - 
               83*Power(t1,2)*Power(t2,4) + 6*t1*Power(t2,5) + 
               3*Power(t2,6)) - 
            Power(s1,3)*(s2 - t1)*
             (Power(s2,4)*(4*t1 - 2*t2) + 
               Power(s2,3)*(-11*Power(t1,2) + 18*t1*t2 - 
                  6*Power(t2,2)) + 
               Power(s2,2)*(14*Power(t1,3) - 39*Power(t1,2)*t2 + 
                  28*t1*Power(t2,2) - 6*Power(t2,3)) + 
               t1*t2*(-6*Power(t1,3) + 14*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-6*Power(t1,4) + 28*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) + 18*t1*Power(t2,3) - 
                  2*Power(t2,4))) - 
            Power(s1,2)*(2*Power(s2,6)*t1 + 
               Power(s2,5)*(-3*Power(t1,2) + 19*t1*t2 - 
                  10*Power(t2,2)) + 
               Power(s2,4)*(Power(t1,3) - 61*Power(t1,2)*t2 + 
                  69*t1*Power(t2,2) - 17*Power(t2,3)) + 
               Power(s2,3)*(6*Power(t1,4) + 43*Power(t1,3)*t2 - 
                  120*Power(t1,2)*Power(t2,2) + 67*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(t1,2)*t2*
                (4*Power(t1,4) - 14*Power(t1,3)*t2 + 
                  16*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s2*t1*(5*Power(t1,5) - 23*Power(t1,4)*t2 + 
                  27*Power(t1,3)*Power(t2,2) - 
                  8*Power(t1,2)*Power(t2,3) + 9*t1*Power(t2,4) - 
                  8*Power(t2,5)) + 
               Power(s2,2)*(-11*Power(t1,5) + 17*Power(t1,4)*t2 + 
                  47*Power(t1,3)*Power(t2,2) - 
                  57*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) + 
                  3*Power(t2,5))) + 
            s1*(Power(s2,6)*t2*(-11*t1 + 10*t2) - 
               Power(s2,5)*(Power(t1,3) - 28*Power(t1,2)*t2 + 
                  35*t1*Power(t2,2) - 7*Power(t2,3)) + 
               Power(t1,2)*Power(t1 - t2,2)*t2*
                (2*Power(t1,3) + 3*Power(t1,2)*t2 - 2*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,4)*(-4*Power(t1,4) + 7*Power(t1,3)*t2 - 
                  3*Power(t1,2)*Power(t2,2) + 18*t1*Power(t2,3) - 
                  15*Power(t2,4)) + 
               s2*t1*Power(t1 - t2,2)*
                (2*Power(t1,4) - 6*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 15*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(s2,3)*(12*Power(t1,5) - 49*Power(t1,4)*t2 + 
                  87*Power(t1,3)*Power(t2,2) - 
                  110*Power(t1,2)*Power(t2,3) + 72*t1*Power(t2,4) - 
                  11*Power(t2,5)) + 
               Power(s2,2)*(-9*Power(t1,6) + 34*Power(t1,5)*t2 - 
                  75*Power(t1,4)*Power(t2,2) + 
                  111*Power(t1,3)*Power(t2,3) - 
                  92*Power(t1,2)*Power(t2,4) + 28*t1*Power(t2,5) + 
                  Power(t2,6)))) + 
         Power(s,2)*(-2*Power(s1,4)*Power(s2 - t1,2)*(t1 - t2)*
             (Power(s2,3)*(3*t1 - 2*t2) + 
               Power(s2,2)*(-4*Power(t1,2) + 9*t1*t2 - 4*Power(t2,2)) + 
               t1*t2*(2*Power(t1,2) - 4*t1*t2 + Power(t2,2)) + 
               s2*(2*Power(t1,3) - 8*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 2*Power(t2,3))) - 
            s2*(t1 - t2)*t2*(Power(t1,3)*Power(t1 - t2,2)*t2*
                (3*Power(t1,2) - 2*t1*t2 - 2*Power(t2,2)) + 
               Power(s2,6)*(3*Power(t1,2) - 12*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,5)*(-12*Power(t1,3) + 51*Power(t1,2)*t2 - 
                  47*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,4)*(16*Power(t1,4) - 82*Power(t1,3)*t2 + 
                  109*Power(t1,2)*Power(t2,2) - 43*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*Power(t1,2)*
                (4*Power(t1,5) - 14*Power(t1,4)*t2 + 
                  3*Power(t1,3)*Power(t2,2) + 
                  16*Power(t1,2)*Power(t2,3) - 7*t1*Power(t2,4) - 
                  2*Power(t2,5)) - 
               Power(s2,3)*(4*Power(t1,5) - 58*Power(t1,4)*t2 + 
                  123*Power(t1,3)*Power(t2,2) - 
                  71*Power(t1,2)*Power(t2,3) + 2*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,2)*t1*
                (-7*Power(t1,5) - 4*Power(t1,4)*t2 + 
                  58*Power(t1,3)*Power(t2,2) - 
                  59*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) + 
                  6*Power(t2,5))) + 
            Power(s1,3)*(Power(s2,6)*
                (-7*Power(t1,2) + 10*t1*t2 - 3*Power(t2,2)) + 
               Power(s2,5)*(17*Power(t1,3) - 42*Power(t1,2)*t2 + 
                  32*t1*Power(t2,2) - 6*Power(t2,3)) + 
               Power(s2,4)*(-13*Power(t1,4) + 70*Power(t1,3)*t2 - 
                  95*Power(t1,2)*Power(t2,2) + 41*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,3)*t1*
                (Power(t1,4) - 40*Power(t1,3)*t2 + 
                  107*Power(t1,2)*Power(t2,2) - 100*t1*Power(t2,3) + 
                  30*Power(t2,4)) + 
               Power(t1,2)*t2*
                (-3*Power(t1,5) + 15*Power(t1,4)*t2 - 
                  19*Power(t1,3)*Power(t2,2) + 
                  2*Power(t1,2)*Power(t2,3) + 7*t1*Power(t2,4) - 
                  2*Power(t2,5)) + 
               Power(s2,2)*t1*
                (4*Power(t1,5) - 13*Power(t1,4)*t2 - 
                  21*Power(t1,3)*Power(t2,2) + 
                  74*Power(t1,2)*Power(t2,3) - 57*t1*Power(t2,4) + 
                  13*Power(t2,5)) + 
               s2*t1*(-2*Power(t1,6) + 18*Power(t1,5)*t2 - 
                  35*Power(t1,4)*Power(t2,2) + 
                  10*Power(t1,3)*Power(t2,3) + 
                  28*Power(t1,2)*Power(t2,4) - 20*t1*Power(t2,5) + 
                  2*Power(t2,6))) - 
            s1*(Power(s2,7)*Power(t1 - t2,2)*(2*t1 + 7*t2) - 
               Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (2*Power(t1,3) - 3*t1*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,6)*(-7*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,5)*(14*Power(t1,5) - 68*Power(t1,4)*t2 + 
                  136*Power(t1,3)*Power(t2,2) - 
                  158*Power(t1,2)*Power(t2,3) + 102*t1*Power(t2,4) - 
                  27*Power(t2,5)) - 
               s2*t1*Power(t1 - t2,3)*
                (2*Power(t1,5) + 4*Power(t1,4)*t2 - 
                  20*Power(t1,3)*Power(t2,2) + 
                  2*Power(t1,2)*Power(t2,3) + 13*t1*Power(t2,4) - 
                  2*Power(t2,5)) + 
               Power(s2,4)*(-17*Power(t1,6) + 140*Power(t1,5)*t2 - 
                  356*Power(t1,4)*Power(t2,2) + 
                  466*Power(t1,3)*Power(t2,3) - 
                  353*Power(t1,2)*Power(t2,4) + 136*t1*Power(t2,5) - 
                  17*Power(t2,6)) + 
               Power(s2,3)*t1*
                (8*Power(t1,6) - 108*Power(t1,5)*t2 + 
                  354*Power(t1,4)*Power(t2,2) - 
                  535*Power(t1,3)*Power(t2,3) + 
                  439*Power(t1,2)*Power(t2,4) - 190*t1*Power(t2,5) + 
                  33*Power(t2,6)) + 
               Power(s2,2)*t1*
                (2*Power(t1,7) + 27*Power(t1,6)*t2 - 
                  157*Power(t1,5)*Power(t2,2) + 
                  290*Power(t1,4)*Power(t2,3) - 
                  240*Power(t1,3)*Power(t2,4) + 
                  76*Power(t1,2)*Power(t2,5) + 13*t1*Power(t2,6) - 
                  11*Power(t2,7))) + 
            Power(s1,2)*(-2*Power(s2,7)*t1*(t1 - t2) + 
               Power(s2,6)*t2*
                (-16*Power(t1,2) + 26*t1*t2 - 11*Power(t2,2)) + 
               Power(s2,5)*(9*Power(t1,4) + 49*Power(t1,3)*t2 - 
                  118*Power(t1,2)*Power(t2,2) + 79*t1*Power(t2,3) - 
                  18*Power(t2,4)) + 
               Power(t1,2)*Power(t1 - t2,2)*t2*
                (Power(t1,4) + 6*Power(t1,3)*t2 - 
                  3*Power(t1,2)*Power(t2,2) - 10*t1*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s2,4)*(16*Power(t1,5) + 38*Power(t1,4)*t2 - 
                  156*Power(t1,3)*Power(t2,2) + 
                  142*Power(t1,2)*Power(t2,3) - 44*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,3)*(19*Power(t1,6) - 22*Power(t1,5)*t2 - 
                  52*Power(t1,4)*Power(t2,2) + 
                  35*Power(t1,3)*Power(t2,3) + 
                  56*Power(t1,2)*Power(t2,4) - 41*t1*Power(t2,5) + 
                  4*Power(t2,6)) - 
               2*Power(s2,2)*t1*
                (7*Power(t1,6) - 18*Power(t1,5)*t2 + 
                  8*Power(t1,4)*Power(t2,2) - 
                  28*Power(t1,3)*Power(t2,3) + 
                  81*Power(t1,2)*Power(t2,4) - 67*t1*Power(t2,5) + 
                  17*Power(t2,6)) + 
               s2*t1*(4*Power(t1,7) - 12*Power(t1,6)*t2 - 
                  4*Power(t1,4)*Power(t2,3) + 
                  79*Power(t1,3)*Power(t2,4) - 
                  110*Power(t1,2)*Power(t2,5) + 47*t1*Power(t2,6) - 
                  4*Power(t2,7)))) + 
         s*(Power(s1,5)*Power(s2 - t1,2)*(t1 - t2)*
             (2*Power(s2,3)*(t1 - t2) + Power(t1,2)*(t1 - 2*t2)*t2 + 
               Power(s2,2)*(-3*Power(t1,2) + 6*t1*t2 - 4*Power(t2,2)) + 
               s2*(Power(t1,3) - 5*Power(t1,2)*t2 + 4*t1*Power(t2,2) - 
                  2*Power(t2,3))) + 
            Power(s1,4)*Power(s2 - t1,2)*
             (Power(s2,4)*(7*Power(t1,2) - 12*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-9*Power(t1,3) + 27*Power(t1,2)*t2 - 
                  26*t1*Power(t2,2) + 7*Power(t2,3)) + 
               t1*t2*(Power(t1,4) - 5*Power(t1,3)*t2 + 
                  6*Power(t1,2)*Power(t2,2) - 2*Power(t2,4)) + 
               Power(s2,2)*(2*Power(t1,4) - 14*Power(t1,3)*t2 + 
                  20*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) - 
                  Power(t2,4)) - 
               s2*t2*(2*Power(t1,4) - 2*Power(t1,3)*t2 + 
                  Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  3*Power(t2,4))) - 
            s2*(s2 - t1)*Power(t2,2)*
             (2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2) - 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,6)*(3*Power(t1,2) - 5*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,4)*t1*
                (7*Power(t1,3) - 24*Power(t1,2)*t2 + 
                  26*t1*Power(t2,2) - 9*Power(t2,3)) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*
                (3*Power(t1,3) - 5*Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               Power(s2,5)*(-9*Power(t1,3) + 20*Power(t1,2)*t2 - 
                  14*t1*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,3)*(2*Power(t1,5) + 2*Power(t1,4)*t2 - 
                  10*Power(t1,3)*Power(t2,2) + 
                  6*Power(t1,2)*Power(t2,3) + t1*Power(t2,4) - 
                  Power(t2,5))) + 
            Power(s1,3)*(s2 - t1)*
             (Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*(3*t1 + 4*t2) + 
               Power(s2,6)*(3*Power(t1,2) - 4*t1*t2 + Power(t2,2)) - 
               Power(s2,5)*(Power(t1,3) - 2*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,4)*(-10*Power(t1,4) + 12*Power(t1,3)*t2 - 
                  13*Power(t1,2)*Power(t2,2) + 24*t1*Power(t2,3) - 
                  11*Power(t2,4)) + 
               Power(s2,3)*(13*Power(t1,5) - 24*Power(t1,4)*t2 + 
                  6*Power(t1,3)*Power(t2,2) - Power(t1,2)*Power(t2,3) + 
                  10*t1*Power(t2,4) - 3*Power(t2,5)) + 
               s2*t1*(2*Power(t1,6) - 6*Power(t1,5)*t2 - 
                  Power(t1,4)*Power(t2,2) + 
                  12*Power(t1,3)*Power(t2,3) - 
                  7*Power(t1,2)*Power(t2,4) - 2*t1*Power(t2,5) + 
                  Power(t2,6)) + 
               Power(s2,2)*(-7*Power(t1,6) + 20*Power(t1,5)*t2 - 
                  2*Power(t1,4)*Power(t2,2) - 
                  24*Power(t1,3)*Power(t2,3) + 
                  14*Power(t1,2)*Power(t2,4) - 5*t1*Power(t2,5) + 
                  2*Power(t2,6))) - 
            s1*t2*(-2*Power(t1,4)*Power(t1 - t2,3)*Power(t2,3) - 
               Power(s2,8)*(Power(t1,2) - 3*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,7)*t2*(Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) - 
               s2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (3*Power(t1,3) - 3*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,6)*(16*Power(t1,4) - 60*Power(t1,3)*t2 + 
                  91*Power(t1,2)*Power(t2,2) - 60*t1*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s2,5)*(-31*Power(t1,5) + 129*Power(t1,4)*t2 - 
                  226*Power(t1,3)*Power(t2,2) + 
                  198*Power(t1,2)*Power(t2,3) - 83*t1*Power(t2,4) + 
                  13*Power(t2,5)) + 
               Power(s2,4)*(15*Power(t1,6) - 82*Power(t1,5)*t2 + 
                  182*Power(t1,4)*Power(t2,2) - 
                  199*Power(t1,3)*Power(t2,3) + 
                  105*Power(t1,2)*Power(t2,4) - 26*t1*Power(t2,5) + 
                  3*Power(t2,6)) + 
               Power(s2,3)*t1*
                (5*Power(t1,6) - 9*Power(t1,5)*t2 - 
                  3*Power(t1,4)*Power(t2,2) + 
                  10*Power(t1,3)*Power(t2,3) + 
                  12*Power(t1,2)*Power(t2,4) - 21*t1*Power(t2,5) + 
                  7*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-4*Power(t1,7) + 21*Power(t1,6)*t2 - 
                  50*Power(t1,5)*Power(t2,2) + 
                  73*Power(t1,4)*Power(t2,3) - 
                  78*Power(t1,3)*Power(t2,4) + 
                  56*Power(t1,2)*Power(t2,5) - 20*t1*Power(t2,6) + 
                  2*Power(t2,7))) + 
            Power(s1,2)*(-(Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                  (Power(t1,3) + Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
                    2*Power(t2,3))) + 
               Power(s2,7)*(3*Power(t1,3) - 7*t1*Power(t2,2) + 
                  5*Power(t2,3)) - 
               Power(s2,6)*(11*Power(t1,4) + Power(t1,3)*t2 - 
                  37*Power(t1,2)*Power(t2,2) + 43*t1*Power(t2,3) - 
                  15*Power(t2,4)) + 
               Power(s2,5)*(16*Power(t1,5) - 22*Power(t1,4)*t2 - 
                  10*Power(t1,3)*Power(t2,2) + 
                  54*Power(t1,2)*Power(t2,3) - 51*t1*Power(t2,4) + 
                  14*Power(t2,5)) + 
               Power(s2,4)*(-11*Power(t1,6) + 63*Power(t1,5)*t2 - 
                  133*Power(t1,4)*Power(t2,2) + 
                  136*Power(t1,3)*Power(t2,3) - 
                  57*Power(t1,2)*Power(t2,4) + 2*t1*Power(t2,5) + 
                  3*Power(t2,6)) + 
               s2*Power(t1,3)*
                (-Power(t1,6) + 2*Power(t1,5)*t2 + 
                  12*Power(t1,4)*Power(t2,2) - 
                  41*Power(t1,3)*Power(t2,3) + 
                  51*Power(t1,2)*Power(t2,4) - 29*t1*Power(t2,5) + 
                  6*Power(t2,6)) + 
               Power(s2,3)*(2*Power(t1,7) - 54*Power(t1,6)*t2 + 
                  191*Power(t1,5)*Power(t2,2) - 
                  318*Power(t1,4)*Power(t2,3) + 
                  268*Power(t1,3)*Power(t2,4) - 
                  110*Power(t1,2)*Power(t2,5) + 20*t1*Power(t2,6) - 
                  Power(t2,7)) + 
               Power(s2,2)*t1*
                (2*Power(t1,7) + 12*Power(t1,6)*t2 - 
                  89*Power(t1,5)*Power(t2,2) + 
                  207*Power(t1,4)*Power(t2,3) - 
                  233*Power(t1,3)*Power(t2,4) + 
                  135*Power(t1,2)*Power(t2,5) - 37*t1*Power(t2,6) + 
                  3*Power(t2,7)))))*Log(-t1))/
     (s*s1*s2*Power(s2 - t1,2)*t1*(s - s2 + t1)*Power(t1 - t2,2)*
       (s1 + t1 - t2)*t2*(s - s1 + t2)*(s2 + t2)*(s2 - t1 + t2)) - 
    (16*(s1*s2*(s1 + 2*s2)*t1*(s1 + s2 - t2)*(s1 + t1 - t2)*
          (s2 - t1 + t2)*Power(Power(s1,2)*(s2 - t1) + 
            s2*t2*(s2 - t1 + t2) - 
            s1*(Power(s2,2) - 2*s2*t1 + Power(t1,2) + 2*s2*t2 - t1*t2),2) \
+ Power(s,8)*(t1 - t2)*(s1*(Power(s2,2)*t1 - 3*s2*Power(t1 - t2,2) + 
               (4*t1 - 3*t2)*Power(t1 - t2,2)) + 
            Power(s1,2)*(2*Power(t1,2) - 3*t1*t2 + t2*(s2 + t2)) + 
            (t1 - t2)*(Power(s2,2)*(t1 - 2*t2) + 2*Power(t1 - t2,3) + 
               s2*(-3*Power(t1,2) + 7*t1*t2 - 4*Power(t2,2)))) + 
         Power(s,7)*(Power(s1,3)*
             (-(Power(s2,2)*t1) - 8*Power(t1,3) + 20*Power(t1,2)*t2 - 
               13*t1*Power(t2,2) + Power(t2,3) + 
               s2*(3*Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
            s1*(s2*Power(t1 - t2,3)*(2*t1 - t2) + 
               Power(t1 - t2,3)*
                (4*Power(t1,2) + 15*t1*t2 - 10*Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - t1*t2 - Power(t2,2)) + 
               Power(s2,2)*(-6*Power(t1,3) + 7*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 10*Power(t2,3))) + 
            (t1 - t2)*(-(s2*t1*(11*t1 - 20*t2)*Power(t1 - t2,2)) + 
               2*Power(t1 - t2,4)*(3*t1 + 2*t2) + 
               Power(s2,3)*(Power(t1,2) + 10*t1*t2 - 8*Power(t2,2)) + 
               4*Power(s2,2)*
                (Power(t1,3) - 8*Power(t1,2)*t2 + 10*t1*Power(t2,2) - 
                  3*Power(t2,3))) + 
            Power(s1,2)*(Power(s2,3)*t2 - 
               Power(t1 - t2,2)*
                (10*Power(t1,2) - 29*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(-11*Power(t1,2) + 4*t1*t2 + 7*Power(t2,2)) + 
               s2*(16*Power(t1,3) - 28*Power(t1,2)*t2 + 
                  13*t1*Power(t2,2) - Power(t2,3)))) + 
         Power(s,6)*(Power(t1 - t2,5)*
             (6*Power(t1,2) + 11*t1*t2 + 2*Power(t2,2)) - 
            s2*Power(t1 - t2,3)*
             (17*Power(t1,3) - 42*Power(t1,2)*t2 - 16*t1*Power(t2,2) + 
               8*Power(t2,3)) + 
            Power(s2,2)*Power(t1 - t2,2)*
             (11*Power(t1,3) - 107*Power(t1,2)*t2 + 35*t1*Power(t2,2) + 
               8*Power(t2,3)) - 
            Power(s2,4)*(3*Power(t1,3) + 11*Power(t1,2)*t2 - 
               26*t1*Power(t2,2) + 14*Power(t2,3)) + 
            Power(s2,3)*(3*Power(t1,4) + 68*Power(t1,3)*t2 - 
               139*Power(t1,2)*Power(t2,2) + 84*t1*Power(t2,3) - 
               16*Power(t2,4)) - 
            Power(s1,4)*(Power(s2,3) - 13*Power(t1,3) + 
               33*Power(t1,2)*t2 - 24*t1*Power(t2,2) + 2*Power(t2,3) + 
               4*Power(s2,2)*(-3*t1 + t2) + 
               s2*(18*Power(t1,2) - 16*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,3)*(Power(s2,4) + 3*Power(t1,4) - 
               57*Power(t1,3)*t2 + 138*Power(t1,2)*Power(t2,2) - 
               95*t1*Power(t2,3) + 11*Power(t2,4) + 
               6*Power(s2,3)*(-3*t1 + t2) + 
               Power(s2,2)*(66*Power(t1,2) - 77*t1*t2 + 
                  14*Power(t2,2)) + 
               s2*(-52*Power(t1,3) + 114*Power(t1,2)*t2 - 
                  80*t1*Power(t2,2) + 20*Power(t2,3))) + 
            Power(s1,2)*(2*Power(s2,4)*(5*t1 - 3*t2) + 
               Power(s2,3)*(-39*Power(t1,2) + 60*t1*t2 - 
                  32*Power(t2,2)) + 
               Power(s2,2)*(51*Power(t1,3) - 176*Power(t1,2)*t2 + 
                  150*t1*Power(t2,2) - 25*Power(t2,3)) - 
               Power(t1 - t2,2)*
                (27*Power(t1,3) - 17*Power(t1,2)*t2 - 
                  81*t1*Power(t2,2) + 18*Power(t2,3)) + 
               s2*(-4*Power(t1,4) + 81*Power(t1,3)*t2 - 
                  162*Power(t1,2)*Power(t2,2) + 102*t1*Power(t2,3) - 
                  17*Power(t2,4))) + 
            s1*(s2*Power(t1 - t2,3)*
                (13*Power(t1,2) + 10*t1*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(6*Power(t1,2) + 2*t1*t2 + 7*Power(t2,2)) - 
               Power(t1 - t2,3)*
                (11*Power(t1,3) - 43*Power(t1,2)*t2 - 
                  12*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,3)*(-19*Power(t1,3) + 29*Power(t1,2)*t2 - 
                  40*t1*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s2,2)*(8*Power(t1,4) - 62*Power(t1,3)*t2 + 
                  109*Power(t1,2)*Power(t2,2) - 59*t1*Power(t2,3) + 
                  4*Power(t2,4)))) + 
         Power(s,5)*(t1*Power(t1 - t2,5)*
             (2*Power(t1,2) + 10*t1*t2 + 5*Power(t2,2)) + 
            Power(s2,5)*t2*(11*Power(t1,2) - 19*t1*t2 + 
               16*Power(t2,2)) + 
            Power(s2,2)*Power(t1 - t2,2)*
             (20*Power(t1,4) - 182*Power(t1,3)*t2 + 
               24*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
               4*Power(t2,4)) - 
            s2*Power(t1 - t2,3)*
             (14*Power(t1,4) - 43*Power(t1,3)*t2 - 
               50*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(s2,4)*(-2*Power(t1,4) - 85*Power(t1,3)*t2 + 
               156*Power(t1,2)*Power(t2,2) - 98*t1*Power(t2,3) + 
               20*Power(t2,4)) - 
            Power(s2,3)*(6*Power(t1,5) - 211*Power(t1,4)*t2 + 
               400*Power(t1,3)*Power(t2,2) - 
               249*Power(t1,2)*Power(t2,3) + 50*t1*Power(t2,4) + 
               4*Power(t2,5)) + 
            Power(s1,5)*(4*Power(s2,3) - 11*Power(t1,3) + 
               27*Power(t1,2)*t2 - 26*t1*Power(t2,2) + 4*Power(t2,3) + 
               5*Power(s2,2)*(-6*t1 + t2) + 
               s2*(35*Power(t1,2) - 29*t1*t2 + 5*Power(t2,2))) + 
            Power(s1,4)*(-4*Power(s2,4) + 13*Power(t1,4) + 
               Power(s2,3)*(38*t1 - 5*t2) + 16*Power(t1,3)*t2 - 
               123*Power(t1,2)*Power(t2,2) + 121*t1*Power(t2,3) - 
               18*Power(t2,4) - 
               3*Power(s2,2)*
                (35*Power(t1,2) - 45*t1*t2 + 4*Power(t2,2)) + 
               s2*(65*Power(t1,3) - 185*Power(t1,2)*t2 + 
                  122*t1*Power(t2,2) - 29*Power(t2,3))) + 
            Power(s1,3)*(37*Power(t1,5) + Power(s2,4)*(5*t1 - 6*t2) - 
               149*Power(t1,4)*t2 + 116*Power(t1,3)*Power(t2,2) + 
               159*Power(t1,2)*Power(t2,3) - 193*t1*Power(t2,4) + 
               30*Power(t2,5) - 
               2*Power(s2,3)*
                (5*Power(t1,2) - 3*t1*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*(21*Power(t1,3) + 130*Power(t1,2)*t2 - 
                  120*t1*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(-39*Power(t1,4) - 60*Power(t1,3)*t2 + 
                  202*Power(t1,2)*Power(t2,2) - 153*t1*Power(t2,3) + 
                  47*Power(t2,4))) + 
            Power(s1,2)*(Power(s2,5)*(-18*t1 + 11*t2) + 
               2*Power(s2,4)*
                (45*Power(t1,2) - 61*t1*t2 + 32*Power(t2,2)) + 
               Power(s2,3)*(-156*Power(t1,3) + 330*Power(t1,2)*t2 - 
                  309*t1*Power(t2,2) + 65*Power(t2,3)) + 
               Power(s2,2)*(137*Power(t1,4) - 338*Power(t1,3)*t2 + 
                  307*Power(t1,2)*Power(t2,2) - 117*t1*Power(t2,3) + 
                  11*Power(t2,4)) - 
               Power(t1 - t2,2)*
                (7*Power(t1,4) + 77*Power(t1,3)*t2 - 
                  145*Power(t1,2)*Power(t2,2) - 78*t1*Power(t2,3) + 
                  22*Power(t2,4)) + 
               s2*(-42*Power(t1,5) + 149*Power(t1,4)*t2 - 
                  187*Power(t1,3)*Power(t2,2) + 
                  60*Power(t1,2)*Power(t2,3) + 43*t1*Power(t2,4) - 
                  23*Power(t2,5))) - 
            s1*(Power(s2,5)*(18*Power(t1,2) + 19*Power(t2,2)) - 
               s2*Power(t1 - t2,3)*
                (13*Power(t1,3) + 2*Power(t1,2)*t2 - 6*t1*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s2,4)*(-79*Power(t1,3) + 81*Power(t1,2)*t2 - 
                  126*t1*Power(t2,2) + 63*Power(t2,3)) + 
               Power(t1 - t2,3)*
                (18*Power(t1,4) - 21*Power(t1,3)*t2 - 
                  66*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,3)*(118*Power(t1,4) - 232*Power(t1,3)*t2 + 
                  332*Power(t1,2)*Power(t2,2) - 258*t1*Power(t2,3) + 
                  37*Power(t2,4)) + 
               Power(s2,2)*(-61*Power(t1,5) + 190*Power(t1,4)*t2 - 
                  268*Power(t1,3)*Power(t2,2) + 
                  225*Power(t1,2)*Power(t2,3) - 89*t1*Power(t2,4) + 
                  3*Power(t2,5)))) - 
         Power(s,4)*(-(Power(t1,2)*Power(t1 - t2,5)*t2*(3*t1 + 4*t2)) - 
            Power(s2,6)*(Power(t1,3) - 6*Power(t1,2)*t2 + 
               7*t1*Power(t2,2) - 14*Power(t2,3)) - 
            Power(s2,2)*t1*Power(t1 - t2,2)*
             (17*Power(t1,4) - 143*Power(t1,3)*t2 - 
               33*Power(t1,2)*Power(t2,2) + 26*t1*Power(t2,3) - 
               10*Power(t2,4)) + 
            s2*t1*Power(t1 - t2,3)*
             (6*Power(t1,4) - 20*Power(t1,3)*t2 - 
               55*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
               6*Power(t2,4)) + 
            Power(s2,5)*(2*Power(t1,4) - 48*Power(t1,3)*t2 + 
               87*Power(t1,2)*Power(t2,2) - 94*t1*Power(t2,3) + 
               24*Power(t2,4)) + 
            Power(s2,4)*(-5*Power(t1,5) + 167*Power(t1,4)*t2 - 
               303*Power(t1,3)*Power(t2,2) + 
               251*Power(t1,2)*Power(t2,3) - 100*t1*Power(t2,4) + 
               6*Power(t2,5)) + 
            Power(s2,3)*(15*Power(t1,6) - 260*Power(t1,5)*t2 + 
               456*Power(t1,4)*Power(t2,2) - 
               285*Power(t1,3)*Power(t2,3) + 
               75*Power(t1,2)*Power(t2,4) + 3*t1*Power(t2,5) - 
               4*Power(t2,6)) + 
            Power(s1,6)*(9*Power(s2,3) - 5*Power(t1,3) + 
               10*Power(t1,2)*t2 - 13*t1*Power(t2,2) + 2*Power(t2,3) + 
               Power(s2,2)*(-33*t1 + 4*t2) + 
               s2*(29*Power(t1,2) - 20*t1*t2 - 3*Power(t2,2))) + 
            Power(s1,5)*(-10*Power(s2,4) + 17*Power(t1,4) + 
               Power(s2,3)*(37*t1 - 28*t2) - 20*Power(t1,3)*t2 - 
               26*Power(t1,2)*Power(t2,2) + 61*t1*Power(t2,3) - 
               10*Power(t2,4) + 
               Power(s2,2)*(-50*Power(t1,2) + 115*t1*t2 + 
                  6*Power(t2,2)) + 
               s2*(10*Power(t1,3) - 104*Power(t1,2)*t2 + 
                  39*t1*Power(t2,2) + 14*Power(t2,3))) + 
            Power(s1,4)*(-2*Power(s2,5) + 18*Power(t1,5) - 
               125*Power(t1,4)*t2 + 229*Power(t1,3)*Power(t2,2) - 
               52*Power(t1,2)*Power(t2,3) - 106*t1*Power(t2,4) + 
               20*Power(t2,5) + Power(s2,4)*(29*t1 + 27*t2) + 
               Power(s2,3)*(-109*Power(t1,2) - 10*t1*t2 + 
                  4*Power(t2,2)) + 
               4*Power(s2,2)*
                (48*Power(t1,3) - 30*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) - 19*Power(t2,3)) + 
               s2*(-124*Power(t1,4) + 197*Power(t1,3)*t2 - 
                  98*Power(t1,2)*Power(t2,2) + 96*t1*Power(t2,3) - 
                  31*Power(t2,4))) + 
            Power(s1,3)*(3*Power(s2,6) - 28*Power(t1,6) + 
               22*Power(t1,5)*t2 + 258*Power(t1,4)*Power(t2,2) - 
               516*Power(t1,3)*Power(t2,3) + 
               202*Power(t1,2)*Power(t2,4) + 82*t1*Power(t2,5) - 
               20*Power(t2,6) - Power(s2,5)*(29*t1 + 12*t2) + 
               Power(s2,4)*(117*Power(t1,2) - 23*t1*t2 - 
                  33*Power(t2,2)) + 
               Power(s2,3)*(-224*Power(t1,3) + 305*Power(t1,2)*t2 - 
                  160*t1*Power(t2,2) + 74*Power(t2,3)) + 
               Power(s2,2)*(238*Power(t1,4) - 695*Power(t1,3)*t2 + 
                  645*Power(t1,2)*Power(t2,2) - 420*t1*Power(t2,3) + 
                  151*Power(t2,4)) + 
               s2*(-81*Power(t1,5) + 425*Power(t1,4)*t2 - 
                  668*Power(t1,3)*Power(t2,2) + 
                  604*Power(t1,2)*Power(t2,3) - 307*t1*Power(t2,4) + 
                  39*Power(t2,5))) + 
            Power(s1,2)*(Power(s2,6)*(-6*t1 + 11*t2) + 
               Power(s2,5)*(36*Power(t1,2) - 93*t1*t2 + 
                  72*Power(t2,2)) + 
               Power(s2,4)*(-73*Power(t1,3) + 220*Power(t1,2)*t2 - 
                  306*t1*Power(t2,2) + 51*Power(t2,3)) + 
               Power(s2,3)*(56*Power(t1,4) - 152*Power(t1,3)*t2 + 
                  250*Power(t1,2)*Power(t2,2) + 83*t1*Power(t2,3) - 
                  92*Power(t2,4)) - 
               Power(t1 - t2,2)*
                (17*Power(t1,5) - 77*Power(t1,4)*t2 - 
                  2*Power(t1,3)*Power(t2,2) + 
                  210*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) - 
                  10*Power(t2,5)) - 
               Power(s2,2)*(7*Power(t1,5) + 155*Power(t1,4)*t2 - 
                  345*Power(t1,3)*Power(t2,2) + 
                  550*Power(t1,2)*Power(t2,3) - 485*t1*Power(t2,4) + 
                  118*Power(t2,5)) + 
               s2*(6*Power(t1,6) + 85*Power(t1,5)*t2 - 
                  289*Power(t1,4)*Power(t2,2) + 
                  544*Power(t1,3)*Power(t2,3) - 
                  584*Power(t1,2)*Power(t2,4) + 264*t1*Power(t2,5) - 
                  26*Power(t2,6))) + 
            s1*(-2*Power(s2,6)*
                (5*Power(t1,2) + t1*t2 + 13*Power(t2,2)) + 
               Power(s2,5)*(65*Power(t1,3) - 25*Power(t1,2)*t2 + 
                  175*t1*Power(t2,2) - 79*Power(t2,3)) + 
               Power(s2,4)*(-156*Power(t1,4) + 152*Power(t1,3)*t2 - 
                  421*Power(t1,2)*Power(t2,2) + 368*t1*Power(t2,3) - 
                  40*Power(t2,4)) - 
               s2*Power(t1 - t2,3)*
                (12*Power(t1,4) - 42*Power(t1,3)*t2 - 
                  29*Power(t1,2)*Power(t2,2) - 45*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(t1 - t2,3)*
                (7*Power(t1,5) + 12*Power(t1,4)*t2 - 
                  54*Power(t1,3)*Power(t2,2) - 
                  43*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,3)*(158*Power(t1,5) - 218*Power(t1,4)*t2 + 
                  420*Power(t1,3)*Power(t2,2) - 
                  449*Power(t1,2)*Power(t2,3) + 40*t1*Power(t2,4) + 
                  37*Power(t2,5)) + 
               Power(s2,2)*(-53*Power(t1,6) + 31*Power(t1,5)*t2 + 
                  13*Power(t1,4)*Power(t2,2) + 
                  19*Power(t1,3)*Power(t2,3) + 
                  130*Power(t1,2)*Power(t2,4) - 173*t1*Power(t2,5) + 
                  33*Power(t2,6)))) + 
         s*(Power(s1,8)*s2*(Power(s2,2) - 5*s2*t1 + 2*Power(t1,2))*
             (s2 - t1 + t2) - 
            Power(s1,7)*(3*Power(s2,4)*(t1 + 2*t2) + 
               Power(t1,3)*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(2*Power(t1,2) - 31*t1*t2 + 6*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-4*Power(t1,2) - 6*t1*t2 + 9*Power(t2,2)) - 
               2*Power(s2,2)*t1*
                (Power(t1,2) - 21*t1*t2 + 14*Power(t2,2))) - 
            Power(s1,6)*(2*Power(s2,6) + Power(s2,5)*(-19*t1 + 3*t2) + 
               Power(s2,4)*(53*Power(t1,2) - 37*t1*t2 - 
                  14*Power(t2,2)) + 
               s2*Power(t1,2)*
                (-21*Power(t1,3) + 39*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,3)*(-56*Power(t1,3) + 23*Power(t1,2)*t2 + 
                  63*t1*Power(t2,2) - 15*Power(t2,3)) + 
               Power(t1,3)*(Power(t1,3) - 5*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) - 10*Power(t2,3)) + 
               Power(s2,2)*t1*
                (40*Power(t1,3) - 3*Power(t1,2)*t2 - 
                  117*t1*Power(t2,2) + 67*Power(t2,3))) + 
            Power(s1,5)*(Power(s2,6)*(4*t1 + 9*t2) + 
               Power(s2,5)*(10*Power(t1,2) - 77*t1*t2 + 
                  14*Power(t2,2)) - 
               Power(s2,4)*(46*Power(t1,3) - 234*Power(t1,2)*t2 + 
                  123*t1*Power(t2,2) + 15*Power(t2,3)) + 
               s2*Power(t1,2)*
                (18*Power(t1,4) - 78*Power(t1,3)*t2 + 
                  61*Power(t1,2)*Power(t2,2) + 31*t1*Power(t2,3) - 
                  24*Power(t2,4)) + 
               Power(t1,3)*(Power(t1,4) - 7*Power(t1,2)*Power(t2,2) + 
                  22*t1*Power(t2,3) - 20*Power(t2,4)) + 
               Power(s2,3)*(46*Power(t1,4) - 211*Power(t1,3)*t2 + 
                  98*Power(t1,2)*Power(t2,2) + 64*t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-33*Power(t1,4) + 103*Power(t1,3)*t2 + 
                  52*Power(t1,2)*Power(t2,2) - 210*t1*Power(t2,3) + 
                  90*Power(t2,4))) + 
            s2*t1*Power(t2,2)*
             (Power(s2,7)*(t1 - 3*t2) + 
               2*Power(t1,3)*Power(t1 - t2,3)*Power(t2,2) + 
               Power(s2,6)*(-7*Power(t1,2) + 20*t1*t2 - 
                  6*Power(t2,2)) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*t2*
                (Power(t1,2) - 13*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,5)*t1*
                (21*Power(t1,2) - 55*t1*t2 + 25*Power(t2,2)) + 
               Power(s2,4)*(-28*Power(t1,4) + 74*Power(t1,3)*t2 - 
                  34*Power(t1,2)*Power(t2,2) - 13*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,2)*t1*
                (-3*Power(t1,5) + 5*Power(t1,4)*t2 + 
                  30*Power(t1,3)*Power(t2,2) - 
                  66*Power(t1,2)*Power(t2,3) + 41*t1*Power(t2,4) - 
                  7*Power(t2,5)) + 
               Power(s2,3)*(16*Power(t1,5) - 42*Power(t1,4)*t2 + 
                  48*Power(t1,2)*Power(t2,3) - 26*t1*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(s1,4)*(Power(s2,8) + Power(s2,7)*(-15*t1 + 2*t2) + 
               Power(s2,6)*(56*Power(t1,2) - 31*t1*t2 - 
                  15*Power(t2,2)) - 
               Power(s2,5)*(84*Power(t1,3) + 10*Power(t1,2)*t2 - 
                  125*t1*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,4)*(74*Power(t1,4) + 162*Power(t1,3)*t2 - 
                  444*Power(t1,2)*Power(t2,2) + 189*t1*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*t1*
                (24*Power(t1,5) - 12*Power(t1,4)*t2 + 
                  31*Power(t1,3)*Power(t2,2) - 
                  244*Power(t1,2)*Power(t2,3) + 262*t1*Power(t2,4) - 
                  75*Power(t2,5)) - 
               Power(s2,3)*(58*Power(t1,5) + 91*Power(t1,4)*t2 - 
                  336*Power(t1,3)*Power(t2,2) + 
                  144*Power(t1,2)*Power(t2,3) + 36*t1*Power(t2,4) - 
                  15*Power(t2,5)) + 
               Power(t1,3)*(Power(t1,5) - 6*Power(t1,4)*t2 + 
                  14*Power(t1,3)*Power(t2,2) - 
                  8*Power(t1,2)*Power(t2,3) - 20*t1*Power(t2,4) + 
                  20*Power(t2,5)) + 
               s2*Power(t1,2)*
                (Power(t1,5) - 24*Power(t1,4)*t2 + 
                  37*Power(t1,3)*Power(t2,2) + 
                  57*Power(t1,2)*Power(t2,3) - 98*t1*Power(t2,4) + 
                  26*Power(t2,5))) + 
            Power(s1,3)*(Power(s2,8)*(t1 - 3*t2) - 
               2*Power(s2,7)*
                (7*Power(t1,2) - 21*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,6)*(40*Power(t1,3) - 149*Power(t1,2)*t2 + 
                  58*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,5)*(-49*Power(t1,4) + 152*Power(t1,3)*t2 + 
                  38*Power(t1,2)*Power(t2,2) - 113*t1*Power(t2,3) + 
                  24*Power(t2,4)) + 
               Power(t1,3)*t2*
                (-2*Power(t1,5) + 12*Power(t1,4)*t2 - 
                  31*Power(t1,3)*Power(t2,2) + 
                  28*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) - 
                  10*Power(t2,5)) + 
               Power(s2,4)*(46*Power(t1,5) - 23*Power(t1,4)*t2 - 
                  412*Power(t1,3)*Power(t2,2) + 
                  500*Power(t1,2)*Power(t2,3) - 157*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               s2*Power(t1,2)*
                (-3*Power(t1,6) + 14*Power(t1,5)*t2 - 
                  31*Power(t1,4)*Power(t2,2) + 
                  109*Power(t1,3)*Power(t2,3) - 
                  199*Power(t1,2)*Power(t2,4) + 130*t1*Power(t2,5) - 
                  21*Power(t2,6)) + 
               Power(s2,3)*(-46*Power(t1,6) + 57*Power(t1,5)*t2 + 
                  263*Power(t1,4)*Power(t2,2) - 
                  387*Power(t1,3)*Power(t2,3) + 
                  110*Power(t1,2)*Power(t2,4) + 15*t1*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(s2,2)*t1*
                (25*Power(t1,6) - 90*Power(t1,5)*t2 + 
                  114*Power(t1,4)*Power(t2,2) - 
                  220*Power(t1,3)*Power(t2,3) + 
                  351*Power(t1,2)*Power(t2,4) - 213*t1*Power(t2,5) + 
                  40*Power(t2,6))) + 
            Power(s1,2)*(Power(s2,8)*t2*(-5*t1 + 3*t2) + 
               Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                (Power(t1,3) - 7*Power(t1,2)*t2 + 9*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,7)*(Power(t1,3) + 45*Power(t1,2)*t2 - 
                  45*t1*Power(t2,2) + 6*Power(t2,3)) - 
               Power(s2,6)*(10*Power(t1,4) + 110*Power(t1,3)*t2 - 
                  157*Power(t1,2)*Power(t2,2) + 39*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s2,5)*(26*Power(t1,5) + 77*Power(t1,4)*t2 - 
                  88*Power(t1,3)*Power(t2,2) - 
                  92*Power(t1,2)*Power(t2,3) + 70*t1*Power(t2,4) - 
                  11*Power(t2,5)) + 
               Power(s2,4)*(-25*Power(t1,6) + 15*Power(t1,5)*t2 - 
                  212*Power(t1,4)*Power(t2,2) + 
                  543*Power(t1,3)*Power(t2,3) - 
                  362*Power(t1,2)*Power(t2,4) + 75*t1*Power(t2,5) - 
                  4*Power(t2,6)) + 
               Power(s2,2)*t1*
                (3*Power(t1,7) - 31*Power(t1,6)*t2 + 
                  45*Power(t1,5)*Power(t2,2) - 
                  22*Power(t1,4)*Power(t2,3) + 
                  118*Power(t1,3)*Power(t2,4) - 
                  198*Power(t1,2)*Power(t2,5) + 98*t1*Power(t2,6) - 
                  13*Power(t2,7)) - 
               s2*Power(t1,2)*
                (Power(t1,7) - 8*Power(t1,6)*t2 + 
                  20*Power(t1,5)*Power(t2,2) - 
                  46*Power(t1,4)*Power(t2,3) + 
                  118*Power(t1,3)*Power(t2,4) - 
                  152*Power(t1,2)*Power(t2,5) + 77*t1*Power(t2,6) - 
                  10*Power(t2,7)) + 
               Power(s2,3)*(6*Power(t1,7) + Power(t1,6)*t2 + 
                  165*Power(t1,5)*Power(t2,2) - 
                  478*Power(t1,4)*Power(t2,3) + 
                  361*Power(t1,3)*Power(t2,4) - 
                  60*Power(t1,2)*Power(t2,5) - 7*t1*Power(t2,6) + 
                  Power(t2,7))) + 
            s1*t2*(2*Power(t1,4)*Power(t1 - t2,3)*Power(t2,3) - 
               Power(s2,8)*(Power(t1,2) - 7*t1*t2 + Power(t2,2)) - 
               s2*Power(t1,2)*Power(t1 - t2,3)*t2*
                (3*Power(t1,3) + 5*Power(t1,2)*t2 + 11*t1*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s2,7)*(7*Power(t1,3) - 51*Power(t1,2)*t2 + 
                  24*t1*Power(t2,2) - 2*Power(t2,3)) + 
               Power(s2,6)*t1*
                (-11*Power(t1,3) + 130*Power(t1,2)*t2 - 
                  89*t1*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,5)*(-8*Power(t1,5) - 117*Power(t1,4)*t2 + 
                  60*Power(t1,3)*Power(t2,2) + 
                  67*Power(t1,2)*Power(t2,3) - 30*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,4)*(30*Power(t1,6) - 5*Power(t1,5)*t2 + 
                  141*Power(t1,4)*Power(t2,2) - 
                  293*Power(t1,3)*Power(t2,3) + 
                  151*Power(t1,2)*Power(t2,4) - 21*t1*Power(t2,5) + 
                  Power(t2,6)) + 
               Power(s2,3)*t1*
                (-21*Power(t1,6) + 39*Power(t1,5)*t2 - 
                  171*Power(t1,4)*Power(t2,2) + 
                  320*Power(t1,3)*Power(t2,3) - 
                  196*Power(t1,2)*Power(t2,4) + 28*t1*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s2,2)*t1*
                (4*Power(t1,7) + 26*Power(t1,5)*Power(t2,2) - 
                  75*Power(t1,4)*Power(t2,3) + 
                  35*Power(t1,3)*Power(t2,4) + 
                  27*Power(t1,2)*Power(t2,5) - 19*t1*Power(t2,6) + 
                  2*Power(t2,7)))) + 
         Power(s,3)*(Power(t1,3)*Power(t1 - t2,5)*Power(t2,2) + 
            Power(s2,7)*t2*(Power(t1,2) - t1*t2 + 8*Power(t2,2)) - 
            s2*Power(t1,2)*Power(t1 - t2,3)*
             (Power(t1,4) - 3*Power(t1,3)*t2 - 
               24*Power(t1,2)*Power(t2,2) - 17*t1*Power(t2,3) - 
               2*Power(t2,4)) + 
            Power(s2,6)*(Power(t1,4) - 9*Power(t1,3)*t2 + 
               22*Power(t1,2)*Power(t2,2) - 63*t1*Power(t2,3) + 
               16*Power(t2,4)) + 
            Power(s2,5)*(-2*Power(t1,5) + 50*Power(t1,4)*t2 - 
               119*Power(t1,3)*Power(t2,2) + 
               198*Power(t1,2)*Power(t2,3) - 92*t1*Power(t2,4) + 
               4*Power(t2,5)) + 
            Power(s2,2)*t1*Power(t1 - t2,2)*
             (5*Power(t1,5) - 46*Power(t1,4)*t2 - 
               45*Power(t1,3)*Power(t2,2) - 3*Power(t1,2)*Power(t2,3) - 
               15*t1*Power(t2,4) + 6*Power(t2,5)) + 
            Power(s2,4)*(4*Power(t1,6) - 118*Power(t1,5)*t2 + 
               246*Power(t1,4)*Power(t2,2) - 
               309*Power(t1,3)*Power(t2,3) + 
               174*Power(t1,2)*Power(t2,4) - 3*t1*Power(t2,5) - 
               8*Power(t2,6)) - 
            Power(s2,3)*(7*Power(t1,7) - 126*Power(t1,6)*t2 + 
               213*Power(t1,5)*Power(t2,2) - 
               175*Power(t1,4)*Power(t2,3) + 
               88*Power(t1,3)*Power(t2,4) + 
               22*Power(t1,2)*Power(t2,5) - 33*t1*Power(t2,6) + 
               4*Power(t2,7)) + 
            Power(s1,7)*(9*Power(s2,3) + 6*Power(s2,2)*(-3*t1 + t2) + 
               s2*(10*Power(t1,2) - 6*t1*t2 - 3*Power(t2,2)) - 
               t1*(Power(t1,2) - t1*t2 + 2*Power(t2,2))) + 
            Power(s1,6)*(-5*Power(s2,4) + Power(s2,3)*(11*t1 - 47*t2) + 
               Power(s2,2)*(16*Power(t1,2) + 77*t1*t2 - 
                  24*Power(t2,2)) + 
               t1*(9*Power(t1,3) - 15*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) + 6*Power(t2,3)) + 
               s2*(-31*Power(t1,3) - 9*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) + 18*Power(t2,3))) - 
            Power(s1,5)*(14*Power(s2,5) - 18*Power(s2,4)*(3*t1 + t2) + 
               2*Power(s2,3)*
                (59*Power(t1,2) + 22*t1*t2 - 55*Power(t2,2)) + 
               Power(t1,2)*t2*
                (41*Power(t1,2) - 123*t1*t2 + 112*Power(t2,2)) + 
               Power(s2,2)*(-181*Power(t1,3) + 139*Power(t1,2)*t2 + 
                  139*t1*Power(t2,2) - 32*Power(t2,3)) + 
               s2*(105*Power(t1,4) - 243*Power(t1,3)*t2 + 
                  112*Power(t1,2)*Power(t2,2) - 77*t1*Power(t2,3) + 
                  46*Power(t2,4))) + 
            Power(s1,4)*(9*Power(s2,6) + Power(s2,5)*(-36*t1 + 43*t2) + 
               Power(s2,4)*(49*Power(t1,2) - 101*t1*t2 - 
                  54*Power(t2,2)) + 
               Power(s2,3)*(-36*Power(t1,3) + 107*Power(t1,2)*t2 + 
                  240*t1*Power(t2,2) - 161*Power(t2,3)) + 
               Power(s2,2)*(49*Power(t1,4) - 310*Power(t1,3)*t2 + 
                  9*Power(t1,2)*Power(t2,2) + 201*t1*Power(t2,3) - 
                  9*Power(t2,4)) + 
               t1*(-22*Power(t1,5) + 64*Power(t1,4)*t2 + 
                  21*Power(t1,3)*Power(t2,2) - 
                  275*Power(t1,2)*Power(t2,3) + 246*t1*Power(t2,4) - 
                  20*Power(t2,5)) + 
               s2*(-18*Power(t1,5) + 259*Power(t1,4)*t2 - 
                  365*Power(t1,3)*Power(t2,2) + 
                  240*Power(t1,2)*Power(t2,3) - 208*t1*Power(t2,4) + 
                  64*Power(t2,5))) + 
            Power(s1,3)*(Power(s2,7) - Power(s2,6)*(12*t1 + 29*t2) + 
               Power(s2,5)*(75*Power(t1,2) + 96*t1*t2 - 
                  41*Power(t2,2)) + 
               Power(s2,4)*(-216*Power(t1,3) + 25*Power(t1,2)*t2 - 
                  82*t1*Power(t2,2) + 116*Power(t2,3)) + 
               Power(s2,3)*(299*Power(t1,4) - 426*Power(t1,3)*t2 + 
                  629*Power(t1,2)*Power(t2,2) - 625*t1*Power(t2,3) + 
                  163*Power(t2,4)) - 
               Power(s2,2)*(201*Power(t1,5) - 434*Power(t1,4)*t2 + 
                  602*Power(t1,3)*Power(t2,2) - 
                  741*Power(t1,2)*Power(t2,3) + 273*t1*Power(t2,4) + 
                  15*Power(t2,5)) + 
               s2*(53*Power(t1,6) - 135*Power(t1,5)*t2 + 
                  144*Power(t1,4)*Power(t2,2) - 
                  140*Power(t1,3)*Power(t2,3) - 
                  106*Power(t1,2)*Power(t2,4) + 222*t1*Power(t2,5) - 
                  51*Power(t2,6)) + 
               t1*(-3*Power(t1,6) + 73*Power(t1,5)*t2 - 
                  235*Power(t1,4)*Power(t2,2) + 
                  139*Power(t1,3)*Power(t2,3) + 
                  247*Power(t1,2)*Power(t2,4) - 251*t1*Power(t2,5) + 
                  30*Power(t2,6))) - 
            s1*(Power(s2,7)*t2*(t1 + 18*t2) - 
               Power(s2,6)*(14*Power(t1,3) + 11*Power(t1,2)*t2 + 
                  139*t1*Power(t2,2) - 50*Power(t2,3)) + 
               t1*Power(t1 - t2,3)*t2*
                (8*Power(t1,4) - 8*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) - 10*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,5)*(62*Power(t1,4) - Power(t1,3)*t2 + 
                  397*Power(t1,2)*Power(t2,2) - 281*t1*Power(t2,3) + 
                  10*Power(t2,4)) + 
               Power(s2,4)*(-97*Power(t1,5) + 3*Power(t1,4)*t2 - 
                  480*Power(t1,3)*Power(t2,2) + 
                  499*Power(t1,2)*Power(t2,3) + 57*t1*Power(t2,4) - 
                  57*Power(t2,5)) - 
               s2*Power(t1 - t2,3)*
                (9*Power(t1,5) - 41*Power(t1,4)*t2 - 
                  33*Power(t1,3)*Power(t2,2) - 
                  89*Power(t1,2)*Power(t2,3) - 7*t1*Power(t2,4) + 
                  4*Power(t2,5)) + 
               Power(s2,3)*(57*Power(t1,6) + 103*Power(t1,5)*t2 + 
                  44*Power(t1,4)*Power(t2,2) - 
                  113*Power(t1,3)*Power(t2,3) - 
                  353*Power(t1,2)*Power(t2,4) + 285*t1*Power(t2,5) - 
                  36*Power(t2,6)) + 
               Power(s2,2)*(Power(t1,7) - 172*Power(t1,6)*t2 + 
                  314*Power(t1,5)*Power(t2,2) - 
                  335*Power(t1,4)*Power(t2,3) + 
                  530*Power(t1,3)*Power(t2,4) - 
                  423*Power(t1,2)*Power(t2,5) + 82*t1*Power(t2,6) + 
                  3*Power(t2,7))) + 
            Power(s1,2)*(Power(s2,7)*(t1 + 9*t2) + 
               Power(s2,6)*(-8*Power(t1,2) - 69*t1*t2 + 
                  54*Power(t2,2)) + 
               Power(s2,5)*(37*Power(t1,3) + 171*Power(t1,2)*t2 - 
                  255*t1*Power(t2,2) + 18*Power(t2,3)) + 
               Power(s2,4)*(-113*Power(t1,4) - 60*Power(t1,3)*t2 + 
                  276*Power(t1,2)*Power(t2,2) + 187*t1*Power(t2,3) - 
                  124*Power(t2,4)) + 
               Power(s2,3)*(165*Power(t1,5) - 340*Power(t1,4)*t2 + 
                  417*Power(t1,3)*Power(t2,2) - 
                  946*Power(t1,2)*Power(t2,3) + 670*t1*Power(t2,4) - 
                  106*Power(t2,5)) + 
               t1*Power(t1 - t2,2)*
                (9*Power(t1,5) - 6*Power(t1,4)*t2 - 
                  79*Power(t1,3)*Power(t2,2) + 
                  107*Power(t1,2)*Power(t2,3) + 85*t1*Power(t2,4) - 
                  18*Power(t2,5)) + 
               Power(s2,2)*(-109*Power(t1,6) + 439*Power(t1,5)*t2 - 
                  780*Power(t1,4)*Power(t2,2) + 
                  1232*Power(t1,3)*Power(t2,3) - 
                  1023*Power(t1,2)*Power(t2,4) + 228*t1*Power(t2,5) + 
                  13*Power(t2,6)) + 
               s2*(17*Power(t1,7) - 117*Power(t1,6)*t2 + 
                  278*Power(t1,5)*Power(t2,2) - 
                  529*Power(t1,4)*Power(t2,3) + 
                  513*Power(t1,3)*Power(t2,4) - 
                  77*Power(t1,2)*Power(t2,5) - 107*t1*Power(t2,6) + 
                  22*Power(t2,7)))) + 
         Power(s,2)*(-(Power(s1,8)*s2*(3*s2 - t1)*(s2 - t1 + t2)) - 
            Power(s1,7)*(3*Power(s2,4) - 3*Power(s2,3)*(4*t1 + 5*t2) + 
               Power(s2,2)*(24*Power(t1,2) + 17*t1*t2 - 
                  18*Power(t2,2)) + 
               2*Power(t1,2)*(Power(t1,2) - t1*t2 + 2*Power(t2,2)) + 
               s2*t1*(-17*Power(t1,2) + 7*t1*t2 + 8*Power(t2,2))) + 
            Power(s1,6)*(10*Power(s2,5) + Power(s2,4)*(-35*t1 + 26*t2) + 
               Power(s2,3)*(61*Power(t1,2) - 70*t1*t2 - 
                  31*Power(t2,2)) + 
               Power(s2,2)*(-61*Power(t1,3) + 113*Power(t1,2)*t2 + 
                  48*t1*Power(t2,2) - 47*Power(t2,3)) + 
               2*s2*t1*(11*Power(t1,3) - 40*Power(t1,2)*t2 + 
                  4*t1*Power(t2,2) + 15*Power(t2,3)) + 
               Power(t1,2)*(3*Power(t1,3) - 5*t1*Power(t2,2) + 
                  18*Power(t2,3))) + 
            Power(s1,5)*(2*Power(s2,6) - Power(s2,5)*(13*t1 + 41*t2) + 
               Power(s2,4)*(42*Power(t1,2) + 143*t1*t2 - 
                  76*Power(t2,2)) + 
               Power(s2,3)*(-69*Power(t1,3) - 183*Power(t1,2)*t2 + 
                  153*t1*Power(t2,2) + 37*Power(t2,3)) + 
               s2*t1*(-41*Power(t1,4) + 25*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) + 109*t1*Power(t2,3) - 
                  66*Power(t2,4)) + 
               Power(t1,2)*(7*Power(t1,4) - 34*Power(t1,3)*t2 + 
                  64*Power(t1,2)*Power(t2,2) - 25*t1*Power(t2,3) - 
                  30*Power(t2,4)) + 
               Power(s2,2)*(72*Power(t1,4) + 87*Power(t1,3)*t2 - 
                  119*Power(t1,2)*Power(t2,2) - 127*t1*Power(t2,3) + 
                  70*Power(t2,4))) + 
            Power(s1,4)*(-7*Power(s2,7) + Power(s2,6)*(36*t1 - 13*t2) + 
               Power(s2,5)*(-98*Power(t1,2) + 34*t1*t2 + 
                  70*Power(t2,2)) + 
               4*Power(s2,4)*
                (43*Power(t1,3) - 13*Power(t1,2)*t2 - 
                  74*t1*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s2,3)*(-193*Power(t1,4) + 145*Power(t1,3)*t2 + 
                  352*Power(t1,2)*Power(t2,2) - 180*t1*Power(t2,3) - 
                  33*Power(t2,4)) + 
               Power(s2,2)*(151*Power(t1,5) - 314*Power(t1,4)*t2 + 
                  49*Power(t1,3)*Power(t2,2) - 
                  98*Power(t1,2)*Power(t2,3) + 225*t1*Power(t2,4) - 
                  65*Power(t2,5)) + 
               Power(t1,2)*(-3*Power(t1,5) - 18*Power(t1,4)*t2 + 
                  117*Power(t1,3)*Power(t2,2) - 
                  212*Power(t1,2)*Power(t2,3) + 102*t1*Power(t2,4) + 
                  20*Power(t2,5)) + 
               s2*t1*(-58*Power(t1,5) + 230*Power(t1,4)*t2 - 
                  295*Power(t1,3)*Power(t2,2) + 
                  358*Power(t1,2)*Power(t2,3) - 333*t1*Power(t2,4) + 
                  89*Power(t2,5))) - 
            s2*t2*(2*Power(s2,7)*Power(t2,2) - 
               3*Power(t1,3)*Power(t1 - t2,3)*t2*
                (Power(t1,2) + 2*t1*t2 + 2*Power(t2,2)) + 
               Power(s2,6)*t2*
                (4*Power(t1,2) - 23*t1*t2 + 4*Power(t2,2)) + 
               Power(s2,5)*t1*
                (Power(t1,3) - 30*Power(t1,2)*t2 + 98*t1*Power(t2,2) - 
                  39*Power(t2,3)) + 
               s2*Power(t1,2)*Power(t1 - t2,2)*
                (4*Power(t1,4) + 11*Power(t1,3)*t2 + 
                  11*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                  13*Power(t2,4)) + 
               Power(s2,4)*(-13*Power(t1,5) + 81*Power(t1,4)*t2 - 
                  202*Power(t1,3)*Power(t2,2) + 
                  114*Power(t1,2)*Power(t2,3) + t1*Power(t2,4) - 
                  4*Power(t2,5)) + 
               Power(s2,3)*(27*Power(t1,6) - 100*Power(t1,5)*t2 + 
                  209*Power(t1,4)*Power(t2,2) - 
                  124*Power(t1,3)*Power(t2,3) - 
                  31*Power(t1,2)*Power(t2,4) + 27*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,2)*t1*
                (-19*Power(t1,6) + 45*Power(t1,5)*t2 - 
                  83*Power(t1,4)*Power(t2,2) + 
                  37*Power(t1,3)*Power(t2,3) + 
                  74*Power(t1,2)*Power(t2,4) - 64*t1*Power(t2,5) + 
                  10*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,8) - 5*Power(s2,7)*(t1 - 4*t2) + 
               Power(s2,6)*(Power(t1,2) - 97*t1*t2 + 21*Power(t2,2)) + 
               Power(s2,5)*(22*Power(t1,3) + 168*Power(t1,2)*t2 + 
                  21*t1*Power(t2,2) - 70*Power(t2,3)) - 
               Power(s2,4)*(39*Power(t1,4) + 110*Power(t1,3)*t2 + 
                  327*Power(t1,2)*Power(t2,2) - 421*t1*Power(t2,3) + 
                  85*Power(t2,4)) + 
               Power(t1,3)*(-5*Power(t1,5) + 21*Power(t1,4)*t2 - 
                  153*Power(t1,2)*Power(t2,3) + 273*t1*Power(t2,4) - 
                  136*Power(t2,5)) + 
               Power(s2,3)*(24*Power(t1,5) + 29*Power(t1,4)*t2 + 
                  483*Power(t1,3)*Power(t2,2) - 
                  685*Power(t1,2)*Power(t2,3) + 153*t1*Power(t2,4) + 
                  25*Power(t2,5)) + 
               s2*t1*(-15*Power(t1,6) + 127*Power(t1,5)*t2 - 
                  272*Power(t1,4)*Power(t2,2) + 
                  400*Power(t1,3)*Power(t2,3) - 
                  565*Power(t1,2)*Power(t2,4) + 391*t1*Power(t2,5) - 
                  72*Power(t2,6)) + 
               Power(s2,2)*(16*Power(t1,6) - 146*Power(t1,5)*t2 + 
                  29*Power(t1,4)*Power(t2,2) + 
                  100*Power(t1,3)*Power(t2,3) + 
                  225*Power(t1,2)*Power(t2,4) - 222*t1*Power(t2,5) + 
                  38*Power(t2,6))) + 
            Power(s1,2)*(Power(s2,8)*(t1 - 4*t2) + 
               Power(s2,7)*(3*Power(t1,2) + 39*t1*t2 - 23*Power(t2,2)) - 
               Power(s2,6)*(31*Power(t1,3) + 119*Power(t1,2)*t2 - 
                  138*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(t1,2)*Power(t1 - t2,2)*t2*
                (7*Power(t1,4) - 19*Power(t1,3)*t2 - 
                  14*Power(t1,2)*Power(t2,2) + 67*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,5)*(82*Power(t1,4) + 145*Power(t1,3)*t2 - 
                  232*Power(t1,2)*Power(t2,2) - 82*t1*Power(t2,3) + 
                  48*Power(t2,4)) - 
               Power(s2,4)*(121*Power(t1,5) + Power(t1,4)*t2 + 
                  35*Power(t1,3)*Power(t2,2) - 
                  608*Power(t1,2)*Power(t2,3) + 375*t1*Power(t2,4) - 
                  40*Power(t2,5)) + 
               Power(s2,3)*(104*Power(t1,6) - 174*Power(t1,5)*t2 + 
                  421*Power(t1,4)*Power(t2,2) - 
                  1081*Power(t1,3)*Power(t2,3) + 
                  789*Power(t1,2)*Power(t2,4) - 110*t1*Power(t2,5) - 
                  13*Power(t2,6)) + 
               s2*t1*t2*(15*Power(t1,6) - 42*Power(t1,5)*t2 + 
                  18*Power(t1,4)*Power(t2,2) - 
                  129*Power(t1,3)*Power(t2,3) + 
                  316*Power(t1,2)*Power(t2,4) - 210*t1*Power(t2,5) + 
                  32*Power(t2,6)) - 
               Power(s2,2)*(38*Power(t1,7) - 95*Power(t1,6)*t2 + 
                  226*Power(t1,5)*Power(t2,2) - 
                  568*Power(t1,4)*Power(t2,3) + 
                  409*Power(t1,3)*Power(t2,4) + 
                  88*Power(t1,2)*Power(t2,5) - 111*t1*Power(t2,6) + 
                  13*Power(t2,7))) + 
            s1*(Power(s2,8)*t2*(-t1 + 5*t2) - 
               Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (2*Power(t1,3) - 8*Power(t1,2)*t2 - 11*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,7)*(Power(t1,3) + Power(t1,2)*t2 - 
                  57*t1*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,6)*(2*Power(t1,4) + 11*Power(t1,3)*t2 + 
                  216*Power(t1,2)*Power(t2,2) - 116*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*t1*Power(t1 - t2,3)*
                (2*Power(t1,5) - 8*Power(t1,4)*t2 - 
                  19*Power(t1,3)*Power(t2,2) - 
                  45*Power(t1,2)*Power(t2,3) - 25*t1*Power(t2,4) + 
                  6*Power(t2,5)) - 
               Power(s2,5)*(15*Power(t1,5) + 31*Power(t1,4)*t2 + 
                  364*Power(t1,3)*Power(t2,2) - 
                  276*Power(t1,2)*Power(t2,3) - 41*t1*Power(t2,4) + 
                  21*Power(t2,5)) + 
               Power(s2,4)*(16*Power(t1,6) + 75*Power(t1,5)*t2 + 
                  242*Power(t1,4)*Power(t2,2) - 
                  160*Power(t1,3)*Power(t2,3) - 
                  302*Power(t1,2)*Power(t2,4) + 169*t1*Power(t2,5) - 
                  12*Power(t2,6)) + 
               Power(s2,3)*(2*Power(t1,7) - 128*Power(t1,6)*t2 + 
                  76*Power(t1,5)*Power(t2,2) - 
                  192*Power(t1,4)*Power(t2,3) + 
                  591*Power(t1,3)*Power(t2,4) - 
                  398*Power(t1,2)*Power(t2,5) + 52*t1*Power(t2,6) + 
                  3*Power(t2,7)) + 
               Power(s2,2)*(-8*Power(t1,8) + 87*Power(t1,7)*t2 - 
                  133*Power(t1,6)*Power(t2,2) + 
                  204*Power(t1,5)*Power(t2,3) - 
                  390*Power(t1,4)*Power(t2,4) + 
                  282*Power(t1,3)*Power(t2,5) - 
                  22*Power(t1,2)*Power(t2,6) - 22*t1*Power(t2,7) + 
                  2*Power(t2,8)))))*Log(s - s2 + t1))/
     (s*s1*s2*t1*Power(s + t1,2)*(s - s2 + t1)*(s1 + t1 - t2)*
       Power(s1 - s2 + t1 - t2,2)*t2*(s - s1 + t2)*(s - s1 - s2 + t2)*
       (s2 - t1 + t2)) - (16*(Power(s1,5)*Power(s2 - t1,2) + 
         Power(s2,3)*t1*(s2 - t2)*Power(t2,2) + 
         Power(s,4)*s1*t2*(-t1 + t2) + 
         Power(s1,4)*(Power(t1,2)*(t1 - t2) + 2*s2*t1*(t1 + t2) - 
            Power(s2,2)*(3*t1 + 5*t2)) + 
         s1*Power(s2,2)*t2*(-(Power(s2,2)*t1) - Power(t1,3) + 
            Power(t1,2)*t2 - 4*t1*Power(t2,2) + Power(t2,3) + 
            s2*(2*Power(t1,2) + 4*t1*t2 - Power(t2,2))) - 
         Power(s1,3)*(Power(t1,3)*t2 + Power(s2,3)*(3*t1 + t2) + 
            Power(s2,2)*(2*Power(t1,2) + 3*t1*t2 - 8*Power(t2,2)) - 
            s2*t1*(5*Power(t1,2) - 3*t1*t2 + Power(t2,2))) + 
         Power(s1,2)*s2*(Power(s2,2)*(-3*Power(t1,2) + 2*Power(t2,2)) + 
            s2*(2*Power(t1,3) + 10*t1*Power(t2,2) - 5*Power(t2,3)) + 
            t1*(Power(t1,3) - 4*Power(t1,2)*t2 + t1*Power(t2,2) - 
               Power(t2,3))) + 
         Power(s,3)*(s2*t1*(t1 - t2)*t2 + 
            Power(s1,2)*(4*s2*t1 - Power(t1,2) + s2*t2 + 5*t1*t2 - 
               3*Power(t2,2)) + 
            s1*(4*s2*Power(t1,2) - Power(t1,3) + 2*s2*t1*t2 + 
               2*Power(t1,2)*t2 - 3*s2*Power(t2,2) - 3*t1*Power(t2,2) + 
               2*Power(t2,3))) - 
         Power(s,2)*(Power(s1,3)*
             (10*s2*t1 - 3*Power(t1,2) + 4*s2*t2 + 7*t1*t2 - 
               2*Power(t2,2)) + 
            s2*t1*(-Power(t1,3) + 2*Power(t1,2)*t2 + 
               2*t1*(s2 - t2)*t2 + Power(t2,2)*(-3*s2 + t2)) + 
            Power(s1,2)*(-3*Power(t1,3) + 5*Power(t1,2)*t2 - 
               7*t1*Power(t2,2) + 3*Power(t2,3) + 
               2*Power(s2,2)*(4*t1 + t2) + 
               2*s2*(3*Power(t1,2) + 3*t1*t2 - 5*Power(t2,2))) + 
            s1*(Power(s2,2)*(8*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2)) + 
               t2*(Power(t1,3) - 2*Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
                  Power(t2,3)) - 
               5*s2*(Power(t1,3) - Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
                  Power(t2,3)))) + 
         s*(Power(s2,2)*t1*t2*
             (Power(t1,2) + s2*(t1 - 3*t2) - 2*t1*t2 + 2*Power(t2,2)) + 
            Power(s1,4)*(-Power(s2,2) + 3*t1*(-t1 + t2) + 
               s2*(8*t1 + 3*t2)) + 
            Power(s1,3)*(s2*(2*t1 - 9*t2)*t2 + 
               Power(s2,2)*(11*t1 + 6*t2) + 
               t1*(-3*Power(t1,2) + 4*t1*t2 - 4*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,3)*(4*t1 + t2) + 
               Power(s2,2)*(10*Power(t1,2) + 2*t1*t2 - 9*Power(t2,2)) + 
               t1*t2*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               s2*(-10*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 8*Power(t2,3))) + 
            s1*s2*(Power(s2,2)*(4*Power(t1,2) + 2*t1*t2 - Power(t2,2)) + 
               s2*(-2*Power(t1,3) - 11*t1*Power(t2,2) + 4*Power(t2,3)) - 
               2*(Power(t1,4) - 3*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  Power(t2,4)))))*Log(s1 + t1 - t2))/
     (s1*s2*(-s + s2 - t1)*t1*(s1 + t1)*(-s + s1 - t2)*(-s + s1 + s2 - t2)*
       t2) - (16*(-(s1*s2*(2*s1 + s2)*(s1 + t1)*(s1 - t2)*
            Power(t1 - t2,2)*(s1 + t1 - t2)*(s2 - t1 + t2)*
            Power(s1*(-s2 + t1) + s2*t2,2)) + 
         Power(s,5)*(s1 + t1)*(s1 - t2)*Power(t1 - t2,2)*
          (Power(s1,2)*t1*(2*s2 - t1 + t2) - 
            s1*(t1 - t2)*(-2*s2*t1 + Power(t1,2) + 2*s2*t2 - 2*t1*t2 + 
               2*Power(t2,2)) + 
            (t1 - t2)*t2*(Power(t1 - t2,2) + s2*(-2*t1 + t2))) - 
         Power(s,4)*Power(t1 - t2,2)*
          (Power(s1,5)*(7*s2*t1 - 5*Power(t1,2) - 2*s2*t2 + 5*t1*t2 - 
               2*Power(t2,2)) + 
            Power(s1,4)*(-9*Power(t1,3) + Power(s2,2)*(4*t1 - 2*t2) + 
               26*Power(t1,2)*t2 - 23*t1*Power(t2,2) + 8*Power(t2,3) + 
               s2*(11*Power(t1,2) - 25*t1*t2 + 6*Power(t2,2))) + 
            Power(s1,3)*(-3*Power(t1,4) + 30*Power(t1,3)*t2 - 
               47*Power(t1,2)*Power(t2,2) + 31*t1*Power(t2,3) - 
               9*Power(t2,4) + 
               2*Power(s2,2)*
                (4*Power(t1,2) - 9*t1*t2 + 5*Power(t2,2)) + 
               s2*(Power(t1,3) - 27*Power(t1,2)*t2 + 
                  18*t1*Power(t2,2) + Power(t2,3))) + 
            t1*Power(t2,2)*(Power(t1 - t2,3)*(t1 + t2) + 
               Power(s2,2)*(6*Power(t1,2) - 8*t1*t2 + 4*Power(t2,2)) + 
               s2*(-5*Power(t1,3) + 9*Power(t1,2)*t2 - 
                  7*t1*Power(t2,2) + 3*Power(t2,3))) - 
            s1*(t1 - t2)*t2*(2*Power(s2,2)*
                (5*Power(t1,2) - 8*t1*t2 + 2*Power(t2,2)) + 
               s2*(-8*Power(t1,3) + 7*Power(t1,2)*t2 - 
                  11*t1*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(2*Power(t1,3) + 3*Power(t1,2)*t2 - 
                  6*t1*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*(Power(t1,5) + 7*Power(t1,4)*t2 - 
               29*Power(t1,3)*Power(t2,2) + 34*Power(t1,2)*Power(t2,3) - 
               18*t1*Power(t2,4) + 3*Power(t2,5) + 
               2*Power(s2,2)*
                (2*Power(t1,3) - 13*Power(t1,2)*t2 + 
                  15*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*(-3*Power(t1,4) + 4*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) - 
                  9*Power(t2,4)))) + 
         Power(s,3)*(t1 - t2)*
          (Power(s1,6)*(-9*Power(t1,3) + s2*t1*(10*t1 - 11*t2) - 
               2*Power(s2,2)*t2 + 17*Power(t1,2)*t2 - 
               9*t1*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s1,5)*(-15*Power(t1,4) + 2*Power(s2,3)*(t1 - 2*t2) + 
               62*Power(t1,3)*t2 - 78*Power(t1,2)*Power(t2,2) + 
               38*t1*Power(t2,3) - 8*Power(t2,4) + 
               Power(s2,2)*(10*Power(t1,2) - 19*t1*t2 + 
                  3*Power(t2,2)) + 
               s2*(7*Power(t1,3) - 35*Power(t1,2)*t2 + 
                  28*t1*Power(t2,2) - Power(t2,3))) + 
            Power(s1,4)*(-3*Power(t1,5) + 57*Power(t1,4)*t2 - 
               141*Power(t1,3)*Power(t2,2) + 
               131*Power(t1,2)*Power(t2,3) - 57*t1*Power(t2,4) + 
               12*Power(t2,5) + 
               Power(s2,3)*(6*Power(t1,2) - 20*t1*t2 + 
                  15*Power(t2,2)) + 
               Power(s2,2)*(17*Power(t1,3) - 69*Power(t1,2)*t2 + 
                  61*t1*Power(t2,2) - Power(t2,3)) + 
               s2*(-15*Power(t1,4) + 18*Power(t1,3)*t2 - 
                  3*Power(t1,2)*Power(t2,2) + 7*t1*Power(t2,3) - 
                  4*Power(t2,4))) + 
            t1*Power(t2,2)*(t1*Power(t1 - t2,4)*t2 + 
               Power(s2,3)*(4*Power(t1,3) - 11*Power(t1,2)*t2 + 
                  14*t1*Power(t2,2) - 6*Power(t2,3)) + 
               s2*Power(t1 - t2,2)*
                (Power(t1,3) - 2*Power(t1,2)*t2 + 3*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*(-5*Power(t1,4) + 11*Power(t1,3)*t2 - 
                  16*Power(t1,2)*Power(t2,2) + 14*t1*Power(t2,3) - 
                  4*Power(t2,4))) + 
            s1*t2*(Power(s2,3)*
                (-6*Power(t1,4) + 29*Power(t1,3)*t2 - 
                  53*Power(t1,2)*Power(t2,2) + 34*t1*Power(t2,3) - 
                  6*Power(t2,4)) - 
               s2*Power(t1 - t2,2)*
                (2*Power(t1,4) + 15*Power(t1,3)*t2 - 
                  Power(t1,2)*Power(t2,2) + 6*t1*Power(t2,3) - 
                  2*Power(t2,4)) + 
               Power(t1 - t2,2)*t2*
                (Power(t1,4) + 10*Power(t1,3)*t2 - 
                  11*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(8*Power(t1,5) - 9*Power(t1,4)*t2 + 
                  8*Power(t1,3)*Power(t2,2) - 
                  27*Power(t1,2)*Power(t2,3) + 23*t1*Power(t2,4) - 
                  5*Power(t2,5))) + 
            Power(s1,3)*(3*Power(t1,6) + 6*Power(t1,5)*t2 - 
               83*Power(t1,4)*Power(t2,2) + 
               156*Power(t1,3)*Power(t2,3) - 
               116*Power(t1,2)*Power(t2,4) + 42*t1*Power(t2,5) - 
               7*Power(t2,6) + 
               Power(s2,3)*(6*Power(t1,3) - 34*Power(t1,2)*t2 + 
                  57*t1*Power(t2,2) - 25*Power(t2,3)) + 
               Power(s2,2)*(4*Power(t1,4) - 67*Power(t1,3)*t2 + 
                  120*Power(t1,2)*Power(t2,2) - 43*t1*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s2*(-11*Power(t1,5) + 72*Power(t1,4)*t2 - 
                  110*Power(t1,3)*Power(t2,2) + 
                  87*Power(t1,2)*Power(t2,3) - 49*t1*Power(t2,4) + 
                  12*Power(t2,5))) + 
            Power(s1,2)*(Power(s2,3)*
                (2*Power(t1,4) - 24*Power(t1,3)*t2 + 
                  67*Power(t1,2)*Power(t2,2) - 67*t1*Power(t2,3) + 
                  20*Power(t2,4)) - 
               Power(s2,2)*(3*Power(t1,5) + 7*Power(t1,4)*t2 - 
                  57*Power(t1,3)*Power(t2,2) + 
                  47*Power(t1,2)*Power(t2,3) + 17*t1*Power(t2,4) - 
                  11*Power(t2,5)) - 
               t1*t2*(6*Power(t1,5) + 8*Power(t1,4)*t2 - 
                  68*Power(t1,3)*Power(t2,2) + 
                  98*Power(t1,2)*Power(t2,3) - 59*t1*Power(t2,4) + 
                  15*Power(t2,5)) + 
               s2*(Power(t1,6) + 28*Power(t1,5)*t2 - 
                  92*Power(t1,4)*Power(t2,2) + 
                  111*Power(t1,3)*Power(t2,3) - 
                  75*Power(t1,2)*Power(t2,4) + 34*t1*Power(t2,5) - 
                  9*Power(t2,6)))) + 
         s*(Power(s1,8)*t1*(t1 - t2)*
             (2*s2*t1 - 2*Power(t1,2) - s2*t2 + 3*t1*t2) + 
            Power(s1,7)*(-(s2*Power(t1,2)*
                  (3*Power(t1,2) - 5*t1*t2 + Power(t2,2))) + 
               Power(s2,3)*(Power(t1,2) - 4*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(5*Power(t1,3) - 7*Power(t1,2)*t2 + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-3*Power(t1,3) + 16*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) + 12*Power(t2,3))) + 
            Power(s1,6)*(4*Power(t1,2)*(3*t1 - 4*t2)*Power(t1 - t2,2)*
                t2 + Power(s2,4)*
                (5*Power(t1,2) - 12*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,3)*(-5*Power(t1,3) + 3*Power(t1,2)*t2 + 
                  6*t1*Power(t2,2) - 4*Power(t2,3)) + 
               s2*t1*(-15*Power(t1,4) + 60*Power(t1,3)*t2 - 
                  91*Power(t1,2)*Power(t2,2) + 60*t1*Power(t2,3) - 
                  16*Power(t2,4)) + 
               Power(s2,2)*(15*Power(t1,4) - 43*Power(t1,3)*t2 + 
                  37*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) - 
                  11*Power(t2,4))) - 
            s2*t1*(t1 - t2)*Power(t2,3)*
             (2*Power(t1,3)*Power(t1 - t2,2)*t2 + 
               Power(s2,4)*t2*(-2*t1 + t2) - 
               Power(s2,2)*t1*Power(t1 - t2,2)*(4*t1 + 3*t2) + 
               Power(s2,3)*(2*Power(t1,3) + 2*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3)) + 
               s2*(2*Power(t1,5) - 7*Power(t1,4)*t2 + 
                  6*Power(t1,3)*Power(t2,2) - t1*Power(t2,4))) + 
            Power(s1,5)*(2*Power(s2,5)*Power(t1 - t2,2) + 
               Power(s2,4)*(7*Power(t1,3) - 36*Power(t1,2)*t2 + 
                  51*t1*Power(t2,2) - 23*Power(t2,3)) + 
               Power(s2,3)*(-11*Power(t1,4) + 29*Power(t1,3)*t2 - 
                  17*Power(t1,2)*Power(t2,2) + 10*t1*Power(t2,3) - 
                  9*Power(t2,4)) + 
               Power(t1,2)*(Power(t1,5) - Power(t1,4)*t2 - 
                  15*Power(t1,3)*Power(t2,2) + 
                  36*Power(t1,2)*Power(t2,3) - 26*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               Power(s2,2)*(14*Power(t1,5) - 51*Power(t1,4)*t2 + 
                  54*Power(t1,3)*Power(t2,2) - 
                  10*Power(t1,2)*Power(t2,3) - 22*t1*Power(t2,4) + 
                  16*Power(t2,5)) + 
               s2*t1*(-13*Power(t1,5) + 83*Power(t1,4)*t2 - 
                  198*Power(t1,3)*Power(t2,2) + 
                  226*Power(t1,2)*Power(t2,3) - 129*t1*Power(t2,4) + 
                  31*Power(t2,5))) + 
            Power(s1,4)*(Power(s2,5)*
                (4*Power(t1,3) - 14*Power(t1,2)*t2 + 
                  17*t1*Power(t2,2) - 7*Power(t2,3)) - 
               Power(s2,4)*(Power(t1,4) + 23*Power(t1,3)*t2 - 
                  77*Power(t1,2)*Power(t2,2) + 80*t1*Power(t2,3) - 
                  27*Power(t2,4)) + 
               Power(t1,2)*t2*
                (-2*Power(t1,5) + 5*Power(t1,4)*t2 - 
                  4*Power(t1,3)*Power(t2,2) + 
                  5*Power(t1,2)*Power(t2,3) - 9*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               Power(s2,3)*(-3*Power(t1,5) + 21*Power(t1,4)*t2 - 
                  25*Power(t1,3)*Power(t2,2) + 
                  19*Power(t1,2)*Power(t2,3) - 36*t1*Power(t2,4) + 
                  23*Power(t2,5)) + 
               s2*t1*(-3*Power(t1,6) + 26*Power(t1,5)*t2 - 
                  105*Power(t1,4)*Power(t2,2) + 
                  199*Power(t1,3)*Power(t2,3) - 
                  182*Power(t1,2)*Power(t2,4) + 82*t1*Power(t2,5) - 
                  15*Power(t2,6)) + 
               Power(s2,2)*(3*Power(t1,6) + 2*Power(t1,5)*t2 - 
                  57*Power(t1,4)*Power(t2,2) + 
                  136*Power(t1,3)*Power(t2,3) - 
                  133*Power(t1,2)*Power(t2,4) + 63*t1*Power(t2,5) - 
                  11*Power(t2,6))) + 
            s1*Power(t2,2)*(-2*Power(t1,4)*Power(t1 - t2,3)*
                Power(t2,2) + 
               Power(s2,5)*Power(t1 - t2,2)*
                (2*Power(t1,2) - 6*t1*t2 + Power(t2,2)) + 
               s2*Power(t1,2)*Power(t1 - t2,3)*
                (2*Power(t1,3) - 3*Power(t1,2)*t2 + 3*t1*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(s2,4)*t1*
                (Power(t1,4) + 3*Power(t1,3)*t2 - 
                  13*Power(t1,2)*Power(t2,2) + 12*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,3)*(-5*Power(t1,6) + 11*Power(t1,5)*t2 + 
                  4*Power(t1,4)*Power(t2,2) - 
                  17*Power(t1,3)*Power(t2,3) + 
                  4*Power(t1,2)*Power(t2,4) + 6*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,2)*t2*
                (6*Power(t1,6) - 29*Power(t1,5)*t2 + 
                  51*Power(t1,4)*Power(t2,2) - 
                  41*Power(t1,3)*Power(t2,3) + 
                  12*Power(t1,2)*Power(t2,4) + 2*t1*Power(t2,5) - 
                  Power(t2,6))) + 
            Power(s1,3)*(Power(s2,5)*Power(t1 - t2,2)*
                (2*Power(t1,2) - 10*t1*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*Power(t1 - t2,2)*Power(t2,2)*
                (2*Power(t1,3) - 3*Power(t1,2)*t2 + 6*t1*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(s2,4)*(-3*Power(t1,5) + 5*Power(t1,4)*t2 + 
                  24*Power(t1,3)*Power(t2,2) - 
                  64*Power(t1,2)*Power(t2,3) + 53*t1*Power(t2,4) - 
                  13*Power(t2,5)) + 
               Power(s2,3)*(2*Power(t1,6) - 2*Power(t1,5)*t2 + 
                  4*Power(t1,4)*Power(t2,2) - 
                  23*Power(t1,3)*Power(t2,3) - 
                  8*Power(t1,2)*Power(t2,4) + 44*t1*Power(t2,5) - 
                  20*Power(t2,6)) + 
               s2*t1*t2*(-7*Power(t1,6) + 21*Power(t1,5)*t2 - 
                  12*Power(t1,4)*Power(t2,2) - 
                  10*Power(t1,3)*Power(t2,3) + 
                  3*Power(t1,2)*Power(t2,4) + 9*t1*Power(t2,5) - 
                  5*Power(t2,6)) - 
               Power(s2,2)*(Power(t1,7) - 20*Power(t1,6)*t2 + 
                  110*Power(t1,5)*Power(t2,2) - 
                  268*Power(t1,4)*Power(t2,3) + 
                  318*Power(t1,3)*Power(t2,4) - 
                  191*Power(t1,2)*Power(t2,5) + 54*t1*Power(t2,6) - 
                  2*Power(t2,7))) + 
            Power(s1,2)*t2*(Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                (Power(t1,2) - t1*t2 - Power(t2,2)) + 
               Power(s2,5)*(-4*Power(t1,4) + 18*Power(t1,3)*t2 - 
                  31*Power(t1,2)*Power(t2,2) + 22*t1*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(s2,4)*(4*Power(t1,5) - 7*Power(t1,4)*t2 - 
                  Power(t1,3)*Power(t2,2) + 
                  11*Power(t1,2)*Power(t2,3) - 9*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,3)*(-Power(t1,6) + 3*Power(t1,5)*t2 - 
                  21*Power(t1,4)*Power(t2,2) + 
                  36*Power(t1,3)*Power(t2,3) + 
                  Power(t1,2)*Power(t2,4) - 26*t1*Power(t2,5) + 
                  9*Power(t2,6)) + 
               Power(s2,2)*(3*Power(t1,7) - 37*Power(t1,6)*t2 + 
                  135*Power(t1,5)*Power(t2,2) - 
                  233*Power(t1,4)*Power(t2,3) + 
                  207*Power(t1,3)*Power(t2,4) - 
                  89*Power(t1,2)*Power(t2,5) + 12*t1*Power(t2,6) + 
                  2*Power(t2,7)) + 
               s2*t1*(-2*Power(t1,7) + 20*Power(t1,6)*t2 - 
                  56*Power(t1,5)*Power(t2,2) + 
                  78*Power(t1,4)*Power(t2,3) - 
                  73*Power(t1,3)*Power(t2,4) + 
                  50*Power(t1,2)*Power(t2,5) - 21*t1*Power(t2,6) + 
                  4*Power(t2,7)))) + 
         Power(s,2)*(Power(s1,7)*(t1 - t2)*
             (2*Power(s2,2)*t2 + 
               s2*(-7*Power(t1,2) + 5*t1*t2 + 2*Power(t2,2)) + 
               t1*(7*Power(t1,2) - 12*t1*t2 + 3*Power(t2,2))) + 
            Power(s1,6)*(Power(s2,2)*t1*
                (-11*Power(t1,2) + 26*t1*t2 - 16*Power(t2,2)) + 
               Power(s2,3)*(-3*Power(t1,2) + 10*t1*t2 - 7*Power(t2,2)) + 
               s2*(3*Power(t1,4) + 4*Power(t1,3)*t2 - 
                  11*Power(t1,2)*Power(t2,2) - 4*t1*Power(t2,3) + 
                  7*Power(t2,4)) + 
               t1*(11*Power(t1,4) - 58*Power(t1,3)*t2 + 
                  98*Power(t1,2)*Power(t2,2) - 63*t1*Power(t2,3) + 
                  12*Power(t2,4))) + 
            Power(s1,5)*(-2*Power(s2,4)*
                (2*Power(t1,2) - 5*t1*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(-6*Power(t1,3) + 32*Power(t1,2)*t2 - 
                  42*t1*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,2)*(-18*Power(t1,4) + 79*Power(t1,3)*t2 - 
                  118*Power(t1,2)*Power(t2,2) + 49*t1*Power(t2,3) + 
                  9*Power(t2,4)) + 
               t1*(Power(t1,5) - 44*Power(t1,4)*t2 + 
                  152*Power(t1,3)*Power(t2,2) - 
                  191*Power(t1,2)*Power(t2,3) + 98*t1*Power(t2,4) - 
                  16*Power(t2,5)) + 
               s2*(27*Power(t1,5) - 102*Power(t1,4)*t2 + 
                  158*Power(t1,3)*Power(t2,2) - 
                  136*Power(t1,2)*Power(t2,3) + 68*t1*Power(t2,4) - 
                  14*Power(t2,5))) + 
            s2*t1*(t1 - t2)*Power(t2,2)*
             (2*Power(s2,3)*t2*(Power(t1,2) - 4*t1*t2 + 2*Power(t2,2)) - 
               t1*Power(t1 - t2,2)*
                (2*Power(t1,3) - 3*Power(t1,2)*t2 + 2*Power(t2,3)) + 
               Power(s2,2)*(-2*Power(t1,4) + 5*Power(t1,3)*t2 + 
                  7*Power(t1,2)*Power(t2,2) - 12*t1*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(4*Power(t1,5) - 14*Power(t1,4)*t2 + 
                  7*Power(t1,3)*Power(t2,2) + 
                  9*Power(t1,2)*Power(t2,3) - 5*t1*Power(t2,4) - 
                  Power(t2,5))) - 
            Power(s1,2)*(t1 - t2)*t2*
             (-2*Power(s2,4)*
                (5*Power(t1,3) - 22*Power(t1,2)*t2 + 
                  27*t1*Power(t2,2) - 8*Power(t2,3)) + 
               Power(s2,3)*(-13*Power(t1,4) + 44*Power(t1,3)*t2 - 
                  30*Power(t1,2)*Power(t2,2) - 9*t1*Power(t2,3) + 
                  4*Power(t2,4)) + 
               2*Power(s2,2)*
                (17*Power(t1,5) - 50*Power(t1,4)*t2 + 
                  31*Power(t1,3)*Power(t2,2) + 
                  3*Power(t1,2)*Power(t2,3) + 11*t1*Power(t2,4) - 
                  7*Power(t2,5)) + 
               t1*t2*(2*Power(t1,5) + 7*Power(t1,4)*t2 - 
                  16*Power(t1,3)*Power(t2,2) - 
                  3*Power(t1,2)*Power(t2,3) + 14*t1*Power(t2,4) - 
                  4*Power(t2,5)) + 
               s2*(-11*Power(t1,6) + 2*Power(t1,5)*t2 + 
                  78*Power(t1,4)*Power(t2,2) - 
                  162*Power(t1,3)*Power(t2,3) + 
                  128*Power(t1,2)*Power(t2,4) - 29*t1*Power(t2,5) - 
                  2*Power(t2,6))) + 
            Power(s1,4)*(Power(s2,4)*
                (-8*Power(t1,3) + 34*Power(t1,2)*t2 - 
                  46*t1*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,3)*(-3*Power(t1,4) + 41*Power(t1,3)*t2 - 
                  95*Power(t1,2)*Power(t2,2) + 70*t1*Power(t2,3) - 
                  13*Power(t2,4)) - 
               Power(s2,2)*(3*Power(t1,5) - 44*Power(t1,4)*t2 + 
                  142*Power(t1,3)*Power(t2,2) - 
                  156*Power(t1,2)*Power(t2,3) + 38*t1*Power(t2,4) + 
                  16*Power(t2,5)) + 
               t1*(-3*Power(t1,6) + Power(t1,5)*t2 + 
                  73*Power(t1,4)*Power(t2,2) - 
                  194*Power(t1,3)*Power(t2,3) + 
                  181*Power(t1,2)*Power(t2,4) - 62*t1*Power(t2,5) + 
                  4*Power(t2,6)) + 
               s2*(17*Power(t1,6) - 136*Power(t1,5)*t2 + 
                  353*Power(t1,4)*Power(t2,2) - 
                  466*Power(t1,3)*Power(t2,3) + 
                  356*Power(t1,2)*Power(t2,4) - 140*t1*Power(t2,5) + 
                  17*Power(t2,6))) + 
            Power(s1,3)*(Power(s2,4)*
                (-4*Power(t1,4) + 34*Power(t1,3)*t2 - 
                  86*Power(t1,2)*Power(t2,2) + 82*t1*Power(t2,3) - 
                  26*Power(t2,4)) + 
               Power(s2,3)*t2*
                (30*Power(t1,4) - 100*Power(t1,3)*t2 + 
                  107*Power(t1,2)*Power(t2,2) - 40*t1*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*t2*(-33*Power(t1,6) + 190*Power(t1,5)*t2 - 
                  439*Power(t1,4)*Power(t2,2) + 
                  535*Power(t1,3)*Power(t2,3) - 
                  354*Power(t1,2)*Power(t2,4) + 108*t1*Power(t2,5) - 
                  8*Power(t2,6)) + 
               t1*t2*(6*Power(t1,6) + Power(t1,5)*t2 - 
                  66*Power(t1,4)*Power(t2,2) + 
                  117*Power(t1,3)*Power(t2,3) - 
                  62*Power(t1,2)*Power(t2,4) - 3*t1*Power(t2,5) + 
                  7*Power(t2,6)) + 
               Power(s2,2)*(4*Power(t1,6) - 41*Power(t1,5)*t2 + 
                  56*Power(t1,4)*Power(t2,2) + 
                  35*Power(t1,3)*Power(t2,3) - 
                  52*Power(t1,2)*Power(t2,4) - 22*t1*Power(t2,5) + 
                  19*Power(t2,6))) + 
            s1*t2*(-(Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                  (2*Power(t1,2) + 2*t1*t2 - 3*Power(t2,2))) - 
               2*Power(s2,4)*t2*
                (4*Power(t1,4) - 19*Power(t1,3)*t2 + 
                  27*Power(t1,2)*Power(t2,2) - 14*t1*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*Power(t1 - t2,3)*
                (2*Power(t1,5) - 13*Power(t1,4)*t2 - 
                  2*Power(t1,3)*Power(t2,2) + 
                  20*Power(t1,2)*Power(t2,3) - 4*t1*Power(t2,4) - 
                  2*Power(t2,5)) + 
               Power(s2,3)*(2*Power(t1,6) - 20*Power(t1,5)*t2 + 
                  28*Power(t1,4)*Power(t2,2) + 
                  10*Power(t1,3)*Power(t2,3) - 
                  35*Power(t1,2)*Power(t2,4) + 18*t1*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,2)*(-4*Power(t1,7) + 47*Power(t1,6)*t2 - 
                  110*Power(t1,5)*Power(t2,2) + 
                  79*Power(t1,4)*Power(t2,3) - 
                  4*Power(t1,3)*Power(t2,4) - 12*t1*Power(t2,6) + 
                  4*Power(t2,7)))))*Log(-t2))/
     (s*s1*s2*t1*(s1 + t1)*(s - s2 + t1)*Power(s1 - t2,2)*Power(t1 - t2,2)*
       (s1 + t1 - t2)*t2*(s - s1 + t2)*(s2 - t1 + t2)) + 
    (16*(-(s1*s2*(2*s1 + s2)*(s1 + s2 - t1)*(s1 + t1 - t2)*t2*
            (s2 - t1 + t2)*Power(Power(s1,2)*(s2 - t1) + 
              s2*t2*(s2 - t1 + t2) - 
              s1*(Power(s2,2) - 2*s2*t1 + Power(t1,2) + 2*s2*t2 - t1*t2),
             2)) + Power(s,8)*(t1 - t2)*
          (s1*(Power(s2,2)*t1 - 3*s2*Power(t1 - t2,2) + 
               (4*t1 - 3*t2)*Power(t1 - t2,2)) + 
            Power(s1,2)*(2*Power(t1,2) - 3*t1*t2 + t2*(s2 + t2)) + 
            (t1 - t2)*(Power(s2,2)*(t1 - 2*t2) + 2*Power(t1 - t2,3) + 
               s2*(-3*Power(t1,2) + 7*t1*t2 - 4*Power(t2,2)))) - 
         Power(s,7)*(Power(s1,3)*
             (Power(s2,2)*t1 + 8*Power(t1,3) - 18*Power(t1,2)*t2 + 
               9*t1*Power(t2,2) + Power(t2,3) - 
               s2*(Power(t1,2) + t1*t2 - Power(t2,2))) + 
            s1*(s2*(t1 - 2*t2)*Power(t1 - t2,3) - 
               (20*t1 - 11*t2)*Power(t1 - t2,3)*t2 + 
               Power(s2,3)*(Power(t1,2) - 3*t1*t2 + 3*Power(t2,2)) - 
               Power(s2,2)*(Power(t1,3) - 13*Power(t1,2)*t2 + 
                  28*t1*Power(t2,2) - 16*Power(t2,3))) + 
            Power(s1,2)*(-(Power(s2,3)*t2) + 
               Power(s2,2)*(7*Power(t1,2) + 4*t1*t2 - 11*Power(t2,2)) + 
               4*Power(t1 - t2,2)*
                (3*Power(t1,2) - 7*t1*t2 + Power(t2,2)) + 
               s2*(-10*Power(t1,3) + 9*Power(t1,2)*t2 + 
                  7*t1*Power(t2,2) - 6*Power(t2,3))) + 
            (t1 - t2)*(-2*Power(t1 - t2,4)*(2*t1 + 3*t2) + 
               s2*Power(t1 - t2,2)*
                (10*Power(t1,2) - 15*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,3)*(Power(t1,2) - 12*t1*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(-7*Power(t1,3) + 36*Power(t1,2)*t2 - 
                  39*t1*Power(t2,2) + 10*Power(t2,3)))) - 
         Power(s,6)*(-(Power(t1 - t2,5)*
               (2*Power(t1,2) + 11*t1*t2 + 6*Power(t2,2))) + 
            s2*Power(t1 - t2,3)*
             (11*Power(t1,3) - 12*Power(t1,2)*t2 - 43*t1*Power(t2,2) + 
               11*Power(t2,3)) + 
            Power(s2,4)*(-2*Power(t1,3) + 24*Power(t1,2)*t2 - 
               33*t1*Power(t2,2) + 13*Power(t2,3)) - 
            Power(s2,2)*Power(t1 - t2,2)*
             (18*Power(t1,3) - 81*Power(t1,2)*t2 - 17*t1*Power(t2,2) + 
               27*Power(t2,3)) + 
            Power(s2,3)*(11*Power(t1,4) - 95*Power(t1,3)*t2 + 
               138*Power(t1,2)*Power(t2,2) - 57*t1*Power(t2,3) + 
               3*Power(t2,4)) + 
            Power(s1,4)*(Power(s2,3) - 14*Power(t1,3) + 
               26*Power(t1,2)*t2 - 11*t1*Power(t2,2) - 3*Power(t2,3) + 
               Power(s2,2)*(-6*t1 + 10*t2) + 
               s2*(7*Power(t1,2) + 2*t1*t2 + 6*Power(t2,2))) - 
            Power(s1,3)*(Power(s2,4) + 16*Power(t1,4) - 
               6*Power(s2,3)*(t1 - 3*t2) - 84*Power(t1,3)*t2 + 
               139*Power(t1,2)*Power(t2,2) - 68*t1*Power(t2,3) - 
               3*Power(t2,4) + 
               Power(s2,2)*(32*Power(t1,2) - 60*t1*t2 + 
                  39*Power(t2,2)) + 
               s2*(-28*Power(t1,3) + 40*Power(t1,2)*t2 - 
                  29*t1*Power(t2,2) + 19*Power(t2,3))) - 
            s1*(s2*Power(t1 - t2,3)*
                (6*Power(t1,2) + 10*t1*t2 + 13*Power(t2,2)) + 
               Power(s2,4)*(5*Power(t1,2) - 16*t1*t2 + 
                  18*Power(t2,2)) - 
               Power(t1 - t2,3)*
                (8*Power(t1,3) - 16*Power(t1,2)*t2 - 
                  42*t1*Power(t2,2) + 17*Power(t2,3)) + 
               Power(s2,3)*(-20*Power(t1,3) + 80*Power(t1,2)*t2 - 
                  114*t1*Power(t2,2) + 52*Power(t2,3)) + 
               Power(s2,2)*(17*Power(t1,4) - 102*Power(t1,3)*t2 + 
                  162*Power(t1,2)*Power(t2,2) - 81*t1*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,2)*(-4*Power(s2,4)*(t1 - 3*t2) + 
               Power(s2,3)*(14*Power(t1,2) - 77*t1*t2 + 
                  66*Power(t2,2)) + 
               Power(t1 - t2,2)*
                (8*Power(t1,3) + 35*Power(t1,2)*t2 - 
                  107*t1*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,2)*(-25*Power(t1,3) + 150*Power(t1,2)*t2 - 
                  176*t1*Power(t2,2) + 51*Power(t2,3)) + 
               s2*(4*Power(t1,4) - 59*Power(t1,3)*t2 + 
                  109*Power(t1,2)*Power(t2,2) - 62*t1*Power(t2,3) + 
                  8*Power(t2,4)))) + 
         Power(s,5)*(Power(t1 - t2,5)*t2*
             (5*Power(t1,2) + 10*t1*t2 + 2*Power(t2,2)) + 
            Power(s2,5)*(-4*Power(t1,3) + 26*Power(t1,2)*t2 - 
               27*t1*Power(t2,2) + 11*Power(t2,3)) + 
            Power(s2,4)*(18*Power(t1,4) - 121*Power(t1,3)*t2 + 
               123*Power(t1,2)*Power(t2,2) - 16*t1*Power(t2,3) - 
               13*Power(t2,4)) + 
            Power(s2,2)*Power(t1 - t2,2)*
             (22*Power(t1,4) - 78*Power(t1,3)*t2 - 
               145*Power(t1,2)*Power(t2,2) + 77*t1*Power(t2,3) + 
               7*Power(t2,4)) - 
            s2*Power(t1 - t2,3)*
             (6*Power(t1,4) - Power(t1,3)*t2 - 
               66*Power(t1,2)*Power(t2,2) - 21*t1*Power(t2,3) + 
               18*Power(t2,4)) - 
            Power(s2,3)*(30*Power(t1,5) - 193*Power(t1,4)*t2 + 
               159*Power(t1,3)*Power(t2,2) + 
               116*Power(t1,2)*Power(t2,3) - 149*t1*Power(t2,4) + 
               37*Power(t2,5)) + 
            Power(s1,5)*(Power(s2,2)*(-11*t1 + 18*t2) + 
               t1*(-16*Power(t1,2) + 19*t1*t2 - 11*Power(t2,2)) + 
               s2*(19*Power(t1,2) + 18*Power(t2,2))) + 
            Power(s1,4)*(4*Power(s2,4) - 20*Power(t1,4) + 
               Power(s2,3)*(6*t1 - 5*t2) + 98*Power(t1,3)*t2 - 
               156*Power(t1,2)*Power(t2,2) + 85*t1*Power(t2,3) + 
               2*Power(t2,4) - 
               2*Power(s2,2)*
                (32*Power(t1,2) - 61*t1*t2 + 45*Power(t2,2)) + 
               s2*(63*Power(t1,3) - 126*Power(t1,2)*t2 + 
                  81*t1*Power(t2,2) - 79*Power(t2,3))) + 
            Power(s1,3)*(-4*Power(s2,5) + 4*Power(t1,5) + 
               Power(s2,4)*(5*t1 - 38*t2) + 50*Power(t1,4)*t2 - 
               249*Power(t1,3)*Power(t2,2) + 
               400*Power(t1,2)*Power(t2,3) - 211*t1*Power(t2,4) + 
               6*Power(t2,5) + 
               2*Power(s2,3)*
                (10*Power(t1,2) - 3*t1*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-65*Power(t1,3) + 309*Power(t1,2)*t2 - 
                  330*t1*Power(t2,2) + 156*Power(t2,3)) + 
               s2*(37*Power(t1,4) - 258*Power(t1,3)*t2 + 
                  332*Power(t1,2)*Power(t2,2) - 232*t1*Power(t2,3) + 
                  118*Power(t2,4))) + 
            Power(s1,2)*(-5*Power(s2,5)*(t1 - 6*t2) + 
               3*Power(s2,4)*
                (4*Power(t1,2) - 45*t1*t2 + 35*Power(t2,2)) - 
               Power(s2,3)*(3*Power(t1,3) - 120*Power(t1,2)*t2 + 
                  130*t1*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s2,2)*(-11*Power(t1,4) + 117*Power(t1,3)*t2 - 
                  307*Power(t1,2)*Power(t2,2) + 338*t1*Power(t2,3) - 
                  137*Power(t2,4)) + 
               Power(t1 - t2,2)*
                (4*Power(t1,4) - 25*Power(t1,3)*t2 - 
                  24*Power(t1,2)*Power(t2,2) + 182*t1*Power(t2,3) - 
                  20*Power(t2,4)) + 
               s2*(3*Power(t1,5) - 89*Power(t1,4)*t2 + 
                  225*Power(t1,3)*Power(t2,2) - 
                  268*Power(t1,2)*Power(t2,3) + 190*t1*Power(t2,4) - 
                  61*Power(t2,5))) + 
            s1*(Power(s2,5)*(-5*Power(t1,2) + 29*t1*t2 - 
                  35*Power(t2,2)) + 
               Power(s2,4)*(29*Power(t1,3) - 122*Power(t1,2)*t2 + 
                  185*t1*Power(t2,2) - 65*Power(t2,3)) + 
               s2*Power(t1 - t2,3)*
                (4*Power(t1,3) - 6*Power(t1,2)*t2 + 2*t1*Power(t2,2) + 
                  13*Power(t2,3)) - 
               Power(t1 - t2,3)*
                (4*Power(t1,4) + 11*Power(t1,3)*t2 - 
                  50*Power(t1,2)*Power(t2,2) - 43*t1*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(s2,3)*(-47*Power(t1,4) + 153*Power(t1,3)*t2 - 
                  202*Power(t1,2)*Power(t2,2) + 60*t1*Power(t2,3) + 
                  39*Power(t2,4)) + 
               Power(s2,2)*(23*Power(t1,5) - 43*Power(t1,4)*t2 - 
                  60*Power(t1,3)*Power(t2,2) + 
                  187*Power(t1,2)*Power(t2,3) - 149*t1*Power(t2,4) + 
                  42*Power(t2,5)))) + 
         Power(s,4)*(t1*Power(t1 - t2,5)*Power(t2,2)*(4*t1 + 3*t2) + 
            Power(s2,6)*(2*Power(t1,3) - 13*Power(t1,2)*t2 + 
               10*t1*Power(t2,2) - 5*Power(t2,3)) + 
            Power(s2,5)*(-10*Power(t1,4) + 61*Power(t1,3)*t2 - 
               26*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
               17*Power(t2,4)) + 
            Power(s2,2)*Power(t1 - t2,2)*
             (10*Power(t1,5) - 5*Power(t1,4)*t2 - 
               210*Power(t1,3)*Power(t2,2) + 
               2*Power(t1,2)*Power(t2,3) + 77*t1*Power(t2,4) - 
               17*Power(t2,5)) - 
            s2*Power(t1 - t2,3)*
             (2*Power(t1,5) + 5*Power(t1,4)*t2 - 
               43*Power(t1,3)*Power(t2,2) - 
               54*Power(t1,2)*Power(t2,3) + 12*t1*Power(t2,4) + 
               7*Power(t2,5)) + 
            Power(s2,4)*(20*Power(t1,5) - 106*Power(t1,4)*t2 - 
               52*Power(t1,3)*Power(t2,2) + 
               229*Power(t1,2)*Power(t2,3) - 125*t1*Power(t2,4) + 
               18*Power(t2,5)) + 
            Power(s2,3)*(-20*Power(t1,6) + 82*Power(t1,5)*t2 + 
               202*Power(t1,4)*Power(t2,2) - 
               516*Power(t1,3)*Power(t2,3) + 
               258*Power(t1,2)*Power(t2,4) + 22*t1*Power(t2,5) - 
               28*Power(t2,6)) + 
            Power(s1,6)*(3*Power(s2,3) + 14*Power(t1,3) + 
               Power(s2,2)*(11*t1 - 6*t2) - 7*Power(t1,2)*t2 + 
               6*t1*Power(t2,2) - Power(t2,3) - 
               2*s2*(13*Power(t1,2) + t1*t2 + 5*Power(t2,2))) + 
            Power(s1,5)*(-2*Power(s2,4) + 24*Power(t1,4) - 
               94*Power(t1,3)*t2 + 87*Power(t1,2)*Power(t2,2) - 
               48*t1*Power(t2,3) + 2*Power(t2,4) - 
               Power(s2,3)*(12*t1 + 29*t2) + 
               Power(s2,2)*(72*Power(t1,2) - 93*t1*t2 + 
                  36*Power(t2,2)) + 
               s2*(-79*Power(t1,3) + 175*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) + 65*Power(t2,3))) + 
            Power(s1,4)*(-10*Power(s2,5) + 6*Power(t1,5) - 
               100*Power(t1,4)*t2 + 251*Power(t1,3)*Power(t2,2) - 
               303*Power(t1,2)*Power(t2,3) + 167*t1*Power(t2,4) - 
               5*Power(t2,5) + Power(s2,4)*(27*t1 + 29*t2) + 
               Power(s2,3)*(-33*Power(t1,2) - 23*t1*t2 + 
                  117*Power(t2,2)) + 
               Power(s2,2)*(51*Power(t1,3) - 306*Power(t1,2)*t2 + 
                  220*t1*Power(t2,2) - 73*Power(t2,3)) + 
               s2*(-40*Power(t1,4) + 368*Power(t1,3)*t2 - 
                  421*Power(t1,2)*Power(t2,2) + 152*t1*Power(t2,3) - 
                  156*Power(t2,4))) + 
            Power(s1,3)*(9*Power(s2,6) - 4*Power(t1,6) + 
               3*Power(t1,5)*t2 + 75*Power(t1,4)*Power(t2,2) - 
               285*Power(t1,3)*Power(t2,3) + 
               456*Power(t1,2)*Power(t2,4) - 260*t1*Power(t2,5) + 
               15*Power(t2,6) + Power(s2,5)*(-28*t1 + 37*t2) + 
               Power(s2,4)*(4*Power(t1,2) - 10*t1*t2 - 
                  109*Power(t2,2)) + 
               Power(s2,3)*(74*Power(t1,3) - 160*Power(t1,2)*t2 + 
                  305*t1*Power(t2,2) - 224*Power(t2,3)) + 
               Power(s2,2)*(-92*Power(t1,4) + 83*Power(t1,3)*t2 + 
                  250*Power(t1,2)*Power(t2,2) - 152*t1*Power(t2,3) + 
                  56*Power(t2,4)) + 
               s2*(37*Power(t1,5) + 40*Power(t1,4)*t2 - 
                  449*Power(t1,3)*Power(t2,2) + 
                  420*Power(t1,2)*Power(t2,3) - 218*t1*Power(t2,4) + 
                  158*Power(t2,5))) + 
            Power(s1,2)*(Power(s2,6)*(4*t1 - 33*t2) + 
               Power(s2,5)*(6*Power(t1,2) + 115*t1*t2 - 
                  50*Power(t2,2)) - 
               4*Power(s2,4)*
                (19*Power(t1,3) - 4*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) - 48*Power(t2,3)) + 
               Power(t1 - t2,2)*t2*
                (10*Power(t1,4) - 26*Power(t1,3)*t2 + 
                  33*Power(t1,2)*Power(t2,2) + 143*t1*Power(t2,3) - 
                  17*Power(t2,4)) + 
               Power(s2,3)*(151*Power(t1,4) - 420*Power(t1,3)*t2 + 
                  645*Power(t1,2)*Power(t2,2) - 695*t1*Power(t2,3) + 
                  238*Power(t2,4)) - 
               Power(s2,2)*(118*Power(t1,5) - 485*Power(t1,4)*t2 + 
                  550*Power(t1,3)*Power(t2,2) - 
                  345*Power(t1,2)*Power(t2,3) + 155*t1*Power(t2,4) + 
                  7*Power(t2,5)) + 
               s2*(33*Power(t1,6) - 173*Power(t1,5)*t2 + 
                  130*Power(t1,4)*Power(t2,2) + 
                  19*Power(t1,3)*Power(t2,3) + 
                  13*Power(t1,2)*Power(t2,4) + 31*t1*Power(t2,5) - 
                  53*Power(t2,6))) + 
            s1*(Power(s2,6)*(-3*Power(t1,2) - 20*t1*t2 + 
                  29*Power(t2,2)) + 
               Power(s2,5)*(14*Power(t1,3) + 39*Power(t1,2)*t2 - 
                  104*t1*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,4)*(-31*Power(t1,4) + 96*Power(t1,3)*t2 - 
                  98*Power(t1,2)*Power(t2,2) + 197*t1*Power(t2,3) - 
                  124*Power(t2,4)) - 
               Power(t1 - t2,3)*t2*
                (6*Power(t1,4) - 8*Power(t1,3)*t2 - 
                  55*Power(t1,2)*Power(t2,2) - 20*t1*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*Power(t1 - t2,3)*
                (7*Power(t1,4) - 45*Power(t1,3)*t2 - 
                  29*Power(t1,2)*Power(t2,2) - 42*t1*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,3)*(39*Power(t1,5) - 307*Power(t1,4)*t2 + 
                  604*Power(t1,3)*Power(t2,2) - 
                  668*Power(t1,2)*Power(t2,3) + 425*t1*Power(t2,4) - 
                  81*Power(t2,5)) + 
               Power(s2,2)*(-26*Power(t1,6) + 264*Power(t1,5)*t2 - 
                  584*Power(t1,4)*Power(t2,2) + 
                  544*Power(t1,3)*Power(t2,3) - 
                  289*Power(t1,2)*Power(t2,4) + 85*t1*Power(t2,5) + 
                  6*Power(t2,6)))) + 
         Power(s,3)*(-(Power(s1,7)*
               (Power(s2,3) + Power(s2,2)*(9*t1 + t2) - 
                 s2*t1*(18*t1 + t2) + 
                 t1*(8*Power(t1,2) - t1*t2 + Power(t2,2)))) - 
            Power(s1,6)*(9*Power(s2,4) + 16*Power(t1,4) - 
               63*Power(t1,3)*t2 + 22*Power(t1,2)*Power(t2,2) - 
               9*t1*Power(t2,3) + Power(t2,4) - 
               Power(s2,3)*(29*t1 + 12*t2) + 
               Power(s2,2)*(54*Power(t1,2) - 69*t1*t2 - 
                  8*Power(t2,2)) + 
               s2*(-50*Power(t1,3) + 139*Power(t1,2)*t2 + 
                  11*t1*Power(t2,2) + 14*Power(t2,3))) + 
            Power(s1,5)*(14*Power(s2,5) - 4*Power(t1,5) + 
               92*Power(t1,4)*t2 - 198*Power(t1,3)*Power(t2,2) + 
               119*Power(t1,2)*Power(t2,3) - 50*t1*Power(t2,4) + 
               2*Power(t2,5) + Power(s2,4)*(-43*t1 + 36*t2) + 
               Power(s2,3)*(41*Power(t1,2) - 96*t1*t2 - 
                  75*Power(t2,2)) - 
               Power(s2,2)*(18*Power(t1,3) - 255*Power(t1,2)*t2 + 
                  171*t1*Power(t2,2) + 37*Power(t2,3)) + 
               s2*(10*Power(t1,4) - 281*Power(t1,3)*t2 + 
                  397*Power(t1,2)*Power(t2,2) - t1*Power(t2,3) + 
                  62*Power(t2,4))) + 
            Power(s1,4)*(5*Power(s2,6) + 8*Power(t1,6) + 
               3*Power(t1,5)*t2 - 174*Power(t1,4)*Power(t2,2) + 
               309*Power(t1,3)*Power(t2,3) - 
               246*Power(t1,2)*Power(t2,4) + 118*t1*Power(t2,5) - 
               4*Power(t2,6) - 18*Power(s2,5)*(t1 + 3*t2) + 
               Power(s2,4)*(54*Power(t1,2) + 101*t1*t2 - 
                  49*Power(t2,2)) + 
               Power(s2,3)*(-116*Power(t1,3) + 82*Power(t1,2)*t2 - 
                  25*t1*Power(t2,2) + 216*Power(t2,3)) + 
               Power(s2,2)*(124*Power(t1,4) - 187*Power(t1,3)*t2 - 
                  276*Power(t1,2)*Power(t2,2) + 60*t1*Power(t2,3) + 
                  113*Power(t2,4)) + 
               s2*(-57*Power(t1,5) + 57*Power(t1,4)*t2 + 
                  499*Power(t1,3)*Power(t2,2) - 
                  480*Power(t1,2)*Power(t2,3) + 3*t1*Power(t2,4) - 
                  97*Power(t2,5))) + 
            t2*(Power(t1,2)*Power(t1 - t2,5)*Power(t2,2) + 
               Power(s2,7)*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,5)*t1*t2*
                (112*Power(t1,2) - 123*t1*t2 + 41*Power(t2,2)) - 
               Power(s2,6)*(6*Power(t1,3) + 17*Power(t1,2)*t2 - 
                  15*t1*Power(t2,2) + 9*Power(t2,3)) - 
               s2*t1*Power(t1 - t2,3)*
                (4*Power(t1,4) - 10*Power(t1,3)*t2 - 
                  39*Power(t1,2)*Power(t2,2) - 8*t1*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(s2,2)*Power(t1 - t2,2)*
                (18*Power(t1,5) - 85*Power(t1,4)*t2 - 
                  107*Power(t1,3)*Power(t2,2) + 
                  79*Power(t1,2)*Power(t2,3) + 6*t1*Power(t2,4) - 
                  9*Power(t2,5)) + 
               Power(s2,4)*(20*Power(t1,5) - 246*Power(t1,4)*t2 + 
                  275*Power(t1,3)*Power(t2,2) - 
                  21*Power(t1,2)*Power(t2,3) - 64*t1*Power(t2,4) + 
                  22*Power(t2,5)) + 
               Power(s2,3)*(-30*Power(t1,6) + 251*Power(t1,5)*t2 - 
                  247*Power(t1,4)*Power(t2,2) - 
                  139*Power(t1,3)*Power(t2,3) + 
                  235*Power(t1,2)*Power(t2,4) - 73*t1*Power(t2,5) + 
                  3*Power(t2,6))) + 
            Power(s1,3)*(-9*Power(s2,7) + 4*Power(t1,7) + 
               Power(s2,6)*(47*t1 - 11*t2) - 33*Power(t1,6)*t2 + 
               22*Power(t1,5)*Power(t2,2) + 
               88*Power(t1,4)*Power(t2,3) - 
               175*Power(t1,3)*Power(t2,4) + 
               213*Power(t1,2)*Power(t2,5) - 126*t1*Power(t2,6) + 
               7*Power(t2,7) + 
               Power(s2,5)*(-110*Power(t1,2) + 44*t1*t2 + 
                  118*Power(t2,2)) + 
               Power(s2,4)*(161*Power(t1,3) - 240*Power(t1,2)*t2 - 
                  107*t1*Power(t2,2) + 36*Power(t2,3)) + 
               Power(s2,3)*(-163*Power(t1,4) + 625*Power(t1,3)*t2 - 
                  629*Power(t1,2)*Power(t2,2) + 426*t1*Power(t2,3) - 
                  299*Power(t2,4)) + 
               Power(s2,2)*(106*Power(t1,5) - 670*Power(t1,4)*t2 + 
                  946*Power(t1,3)*Power(t2,2) - 
                  417*Power(t1,2)*Power(t2,3) + 340*t1*Power(t2,4) - 
                  165*Power(t2,5)) + 
               s2*(-36*Power(t1,6) + 285*Power(t1,5)*t2 - 
                  353*Power(t1,4)*Power(t2,2) - 
                  113*Power(t1,3)*Power(t2,3) + 
                  44*Power(t1,2)*Power(t2,4) + 103*t1*Power(t2,5) + 
                  57*Power(t2,6))) + 
            s1*(Power(s2,7)*
                (3*Power(t1,2) + 6*t1*t2 - 10*Power(t2,2)) + 
               Power(s2,6)*(-18*Power(t1,3) - 3*Power(t1,2)*t2 + 
                  9*t1*Power(t2,2) + 31*Power(t2,3)) + 
               Power(t1 - t2,3)*Power(t2,2)*
                (2*Power(t1,4) + 17*Power(t1,3)*t2 + 
                  24*Power(t1,2)*Power(t2,2) + 3*t1*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s2,5)*(46*Power(t1,4) - 77*Power(t1,3)*t2 + 
                  112*Power(t1,2)*Power(t2,2) - 243*t1*Power(t2,3) + 
                  105*Power(t2,4)) + 
               s2*Power(t1 - t2,3)*
                (4*Power(t1,5) - 7*Power(t1,4)*t2 - 
                  89*Power(t1,3)*Power(t2,2) - 
                  33*Power(t1,2)*Power(t2,3) - 41*t1*Power(t2,4) + 
                  9*Power(t2,5)) + 
               Power(s2,4)*(-64*Power(t1,5) + 208*Power(t1,4)*t2 - 
                  240*Power(t1,3)*Power(t2,2) + 
                  365*Power(t1,2)*Power(t2,3) - 259*t1*Power(t2,4) + 
                  18*Power(t2,5)) + 
               Power(s2,3)*(51*Power(t1,6) - 222*Power(t1,5)*t2 + 
                  106*Power(t1,4)*Power(t2,2) + 
                  140*Power(t1,3)*Power(t2,3) - 
                  144*Power(t1,2)*Power(t2,4) + 135*t1*Power(t2,5) - 
                  53*Power(t2,6)) + 
               Power(s2,2)*(-22*Power(t1,7) + 107*Power(t1,6)*t2 + 
                  77*Power(t1,5)*Power(t2,2) - 
                  513*Power(t1,4)*Power(t2,3) + 
                  529*Power(t1,3)*Power(t2,4) - 
                  278*Power(t1,2)*Power(t2,5) + 117*t1*Power(t2,6) - 
                  17*Power(t2,7))) + 
            Power(s1,2)*(-6*Power(s2,7)*(t1 - 3*t2) + 
               Power(s2,6)*(24*Power(t1,2) - 77*t1*t2 - 
                  16*Power(t2,2)) + 
               Power(s2,5)*(-32*Power(t1,3) + 139*Power(t1,2)*t2 + 
                  139*t1*Power(t2,2) - 181*Power(t2,3)) + 
               Power(s2,4)*(9*Power(t1,4) - 201*Power(t1,3)*t2 - 
                  9*Power(t1,2)*Power(t2,2) + 310*t1*Power(t2,3) - 
                  49*Power(t2,4)) - 
               Power(t1 - t2,2)*t2*
                (6*Power(t1,5) - 15*Power(t1,4)*t2 - 
                  3*Power(t1,3)*Power(t2,2) - 
                  45*Power(t1,2)*Power(t2,3) - 46*t1*Power(t2,4) + 
                  5*Power(t2,5)) + 
               Power(s2,3)*(15*Power(t1,5) + 273*Power(t1,4)*t2 - 
                  741*Power(t1,3)*Power(t2,2) + 
                  602*Power(t1,2)*Power(t2,3) - 434*t1*Power(t2,4) + 
                  201*Power(t2,5)) + 
               Power(s2,2)*(-13*Power(t1,6) - 228*Power(t1,5)*t2 + 
                  1023*Power(t1,4)*Power(t2,2) - 
                  1232*Power(t1,3)*Power(t2,3) + 
                  780*Power(t1,2)*Power(t2,4) - 439*t1*Power(t2,5) + 
                  109*Power(t2,6)) + 
               s2*(3*Power(t1,7) + 82*Power(t1,6)*t2 - 
                  423*Power(t1,5)*Power(t2,2) + 
                  530*Power(t1,4)*Power(t2,3) - 
                  335*Power(t1,3)*Power(t2,4) + 
                  314*Power(t1,2)*Power(t2,5) - 172*t1*Power(t2,6) + 
                  Power(t2,7)))) - 
         s*(Power(s1,8)*(s2 - t1)*
             (Power(s2,3) + s2*t1*(t1 - 4*t2) + t1*(3*t1 - t2)*t2 + 
               Power(s2,2)*(-2*t1 + t2)) + 
            Power(s1,7)*(Power(s2,4)*(2*t1 - 15*t2) + 
               Power(t1,2)*t2*
                (-6*Power(t1,2) + 20*t1*t2 - 7*Power(t2,2)) - 
               2*Power(s2,3)*
                (3*Power(t1,2) - 21*t1*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(6*Power(t1,3) - 45*Power(t1,2)*t2 + 
                  45*t1*Power(t2,2) + Power(t2,3)) + 
               s2*t1*(-2*Power(t1,3) + 24*Power(t1,2)*t2 - 
                  51*t1*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s1,6)*(-2*Power(s2,6) + Power(s2,5)*(9*t1 + 4*t2) + 
               Power(t1,2)*Power(t2,2)*
                (25*Power(t1,2) - 55*t1*t2 + 21*Power(t2,2)) + 
               Power(s2,4)*(-15*Power(t1,2) - 31*t1*t2 + 
                  56*Power(t2,2)) + 
               s2*t1*t2*(8*Power(t1,3) - 89*Power(t1,2)*t2 + 
                  130*t1*Power(t2,2) - 11*Power(t2,3)) + 
               Power(s2,3)*(11*Power(t1,3) + 58*Power(t1,2)*t2 - 
                  149*t1*Power(t2,2) + 40*Power(t2,3)) - 
               Power(s2,2)*(3*Power(t1,4) + 39*Power(t1,3)*t2 - 
                  157*Power(t1,2)*Power(t2,2) + 110*t1*Power(t2,3) + 
                  10*Power(t2,4))) - 
            s2*(s2 - t1)*Power(t2,3)*
             (-2*Power(t1,3)*Power(t1 - t2,3)*t2 + 
               Power(s2,5)*(2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               s2*t1*Power(t1 - t2,2)*
                (2*Power(t1,3) + 7*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,4)*(-8*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  4*t1*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(12*Power(t1,4) - 14*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) + t1*Power(t2,3) - 
                  Power(t2,4)) - 
               Power(s2,2)*(8*Power(t1,5) - 6*Power(t1,4)*t2 - 
                  11*Power(t1,3)*Power(t2,2) + 
                  13*Power(t1,2)*Power(t2,3) - 5*t1*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(s1,5)*(Power(s2,6)*(-3*t1 + 19*t2) + 
               Power(s2,5)*(14*Power(t1,2) - 77*t1*t2 + 
                  10*Power(t2,2)) - 
               Power(s2,4)*(26*Power(t1,3) - 125*Power(t1,2)*t2 + 
                  10*t1*Power(t2,2) + 84*Power(t2,3)) + 
               Power(s2,3)*(24*Power(t1,4) - 113*Power(t1,3)*t2 + 
                  38*Power(t1,2)*Power(t2,2) + 152*t1*Power(t2,3) - 
                  49*Power(t2,4)) + 
               Power(t1,2)*t2*
                (6*Power(t1,4) - 13*Power(t1,3)*t2 - 
                  34*Power(t1,2)*Power(t2,2) + 74*t1*Power(t2,3) - 
                  28*Power(t2,4)) + 
               s2*t1*(2*Power(t1,5) - 30*Power(t1,4)*t2 + 
                  67*Power(t1,3)*Power(t2,2) + 
                  60*Power(t1,2)*Power(t2,3) - 117*t1*Power(t2,4) - 
                  8*Power(t2,5)) + 
               Power(s2,2)*(-11*Power(t1,5) + 70*Power(t1,4)*t2 - 
                  92*Power(t1,3)*Power(t2,2) - 
                  88*Power(t1,2)*Power(t2,3) + 77*t1*Power(t2,4) + 
                  26*Power(t2,5))) + 
            Power(s1,4)*(Power(s2,8) - 3*Power(s2,7)*(2*t1 + t2) + 
               Power(s2,6)*(14*Power(t1,2) + 37*t1*t2 - 
                  53*Power(t2,2)) - 
               Power(s2,5)*(15*Power(t1,3) + 123*Power(t1,2)*t2 - 
                  234*t1*Power(t2,2) + 46*Power(t2,3)) + 
               Power(s2,4)*(5*Power(t1,4) + 189*Power(t1,3)*t2 - 
                  444*Power(t1,2)*Power(t2,2) + 162*t1*Power(t2,3) + 
                  74*Power(t2,4)) + 
               Power(t1,2)*t2*
                (3*Power(t1,5) - 26*Power(t1,4)*t2 + 
                  48*Power(t1,3)*Power(t2,2) - 42*t1*Power(t2,4) + 
                  16*Power(t2,5)) + 
               Power(s2,3)*(4*Power(t1,5) - 157*Power(t1,4)*t2 + 
                  500*Power(t1,3)*Power(t2,2) - 
                  412*Power(t1,2)*Power(t2,3) - 23*t1*Power(t2,4) + 
                  46*Power(t2,5)) + 
               Power(s2,2)*(-4*Power(t1,6) + 75*Power(t1,5)*t2 - 
                  362*Power(t1,4)*Power(t2,2) + 
                  543*Power(t1,3)*Power(t2,3) - 
                  212*Power(t1,2)*Power(t2,4) + 15*t1*Power(t2,5) - 
                  25*Power(t2,6)) + 
               s2*t1*(Power(t1,6) - 21*Power(t1,5)*t2 + 
                  151*Power(t1,4)*Power(t2,2) - 
                  293*Power(t1,3)*Power(t2,3) + 
                  141*Power(t1,2)*Power(t2,4) - 5*t1*Power(t2,5) + 
                  30*Power(t2,6))) + 
            s1*Power(t2,2)*(2*Power(s2,8)*(t1 - t2) - 
               2*Power(t1,4)*Power(t1 - t2,3)*Power(t2,2) + 
               Power(s2,7)*(-9*Power(t1,2) + 6*t1*t2 + 4*Power(t2,2)) - 
               s2*Power(t1,2)*Power(t1 - t2,3)*
                (2*Power(t1,3) - 11*Power(t1,2)*t2 - 
                  5*t1*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s2,6)*(18*Power(t1,3) - 7*Power(t1,2)*t2 - 
                  39*t1*Power(t2,2) + 21*Power(t2,3)) + 
               Power(s2,5)*(-24*Power(t1,4) + 31*Power(t1,3)*t2 + 
                  61*Power(t1,2)*Power(t2,2) - 78*t1*Power(t2,3) + 
                  18*Power(t2,4)) + 
               Power(s2,4)*(26*Power(t1,5) - 98*Power(t1,4)*t2 + 
                  57*Power(t1,3)*Power(t2,2) + 
                  37*Power(t1,2)*Power(t2,3) - 24*t1*Power(t2,4) + 
                  Power(t2,5)) + 
               Power(s2,3)*(-21*Power(t1,6) + 130*Power(t1,5)*t2 - 
                  199*Power(t1,4)*Power(t2,2) + 
                  109*Power(t1,3)*Power(t2,3) - 
                  31*Power(t1,2)*Power(t2,4) + 14*t1*Power(t2,5) - 
                  3*Power(t2,6)) + 
               Power(s2,2)*(10*Power(t1,7) - 77*Power(t1,6)*t2 + 
                  152*Power(t1,5)*Power(t2,2) - 
                  118*Power(t1,4)*Power(t2,3) + 
                  46*Power(t1,3)*Power(t2,4) - 
                  20*Power(t1,2)*Power(t2,5) + 8*t1*Power(t2,6) - 
                  Power(t2,7))) + 
            Power(s1,3)*(Power(s2,8)*(t1 - 6*t2) + 
               Power(s2,7)*(-6*Power(t1,2) + 31*t1*t2 - 
                  2*Power(t2,2)) + 
               Power(s2,6)*(15*Power(t1,3) - 63*Power(t1,2)*t2 - 
                  23*t1*Power(t2,2) + 56*Power(t2,3)) + 
               Power(s2,5)*(-20*Power(t1,4) + 64*Power(t1,3)*t2 + 
                  98*Power(t1,2)*Power(t2,2) - 211*t1*Power(t2,3) + 
                  46*Power(t2,4)) + 
               Power(s2,4)*(15*Power(t1,5) - 36*Power(t1,4)*t2 - 
                  144*Power(t1,3)*Power(t2,2) + 
                  336*Power(t1,2)*Power(t2,3) - 91*t1*Power(t2,4) - 
                  58*Power(t2,5)) + 
               Power(t1,2)*Power(t2,2)*
                (-7*Power(t1,5) + 41*Power(t1,4)*t2 - 
                  66*Power(t1,3)*Power(t2,2) + 
                  30*Power(t1,2)*Power(t2,3) + 5*t1*Power(t2,4) - 
                  3*Power(t2,5)) + 
               Power(s2,3)*(-6*Power(t1,6) + 15*Power(t1,5)*t2 + 
                  110*Power(t1,4)*Power(t2,2) - 
                  387*Power(t1,3)*Power(t2,3) + 
                  263*Power(t1,2)*Power(t2,4) + 57*t1*Power(t2,5) - 
                  46*Power(t2,6)) + 
               s2*t1*t2*(2*Power(t1,6) + 28*Power(t1,5)*t2 - 
                  196*Power(t1,4)*Power(t2,2) + 
                  320*Power(t1,3)*Power(t2,3) - 
                  171*Power(t1,2)*Power(t2,4) + 39*t1*Power(t2,5) - 
                  21*Power(t2,6)) + 
               Power(s2,2)*(Power(t1,7) - 7*Power(t1,6)*t2 - 
                  60*Power(t1,5)*Power(t2,2) + 
                  361*Power(t1,4)*Power(t2,3) - 
                  478*Power(t1,3)*Power(t2,4) + 
                  165*Power(t1,2)*Power(t2,5) + t1*Power(t2,6) + 
                  6*Power(t2,7))) + 
            Power(s1,2)*t2*(Power(s2,8)*(-5*t1 + 7*t2) + 
               2*Power(s2,7)*(14*Power(t1,2) - 21*t1*t2 + Power(t2,2)) + 
               Power(t1,3)*Power(t1 - t2,2)*Power(t2,2)*
                (7*Power(t1,2) - 13*t1*t2 + Power(t2,2)) + 
               Power(s2,6)*(-67*Power(t1,3) + 117*Power(t1,2)*t2 + 
                  3*t1*Power(t2,2) - 40*Power(t2,3)) + 
               Power(s2,5)*(90*Power(t1,4) - 210*Power(t1,3)*t2 + 
                  52*Power(t1,2)*Power(t2,2) + 103*t1*Power(t2,3) - 
                  33*Power(t2,4)) + 
               Power(s2,4)*(-75*Power(t1,5) + 262*Power(t1,4)*t2 - 
                  244*Power(t1,3)*Power(t2,2) + 
                  31*Power(t1,2)*Power(t2,3) - 12*t1*Power(t2,4) + 
                  24*Power(t2,5)) + 
               Power(s2,3)*(40*Power(t1,6) - 213*Power(t1,5)*t2 + 
                  351*Power(t1,4)*Power(t2,2) - 
                  220*Power(t1,3)*Power(t2,3) + 
                  114*Power(t1,2)*Power(t2,4) - 90*t1*Power(t2,5) + 
                  25*Power(t2,6)) + 
               Power(s2,2)*(-13*Power(t1,7) + 98*Power(t1,6)*t2 - 
                  198*Power(t1,5)*Power(t2,2) + 
                  118*Power(t1,4)*Power(t2,3) - 
                  22*Power(t1,3)*Power(t2,4) + 
                  45*Power(t1,2)*Power(t2,5) - 31*t1*Power(t2,6) + 
                  3*Power(t2,7)) + 
               s2*(2*Power(t1,8) - 19*Power(t1,7)*t2 + 
                  27*Power(t1,6)*Power(t2,2) + 
                  35*Power(t1,5)*Power(t2,3) - 
                  75*Power(t1,4)*Power(t2,4) + 
                  26*Power(t1,3)*Power(t2,5) + 4*t1*Power(t2,7)))) - 
         Power(s,2)*(Power(s1,8)*(s2 - t1)*
             (Power(s2,2) + 2*Power(t1,2) + s2*(-3*t1 + t2)) + 
            Power(s1,7)*(-7*Power(s2,4) + 5*Power(s2,3)*(4*t1 - t2) + 
               Power(t1,2)*(-4*Power(t1,2) + 23*t1*t2 - 4*Power(t2,2)) + 
               Power(s2,2)*(-23*Power(t1,2) + 39*t1*t2 + 
                  3*Power(t2,2)) + 
               s2*(14*Power(t1,3) - 57*Power(t1,2)*t2 + t1*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(s1,6)*(2*Power(s2,5) + Power(s2,4)*(-13*t1 + 36*t2) + 
               Power(s2,3)*(21*Power(t1,2) - 97*t1*t2 + Power(t2,2)) + 
               t1*t2*(39*Power(t1,3) - 98*Power(t1,2)*t2 + 
                  30*t1*Power(t2,2) - Power(t2,3)) - 
               Power(s2,2)*(11*Power(t1,3) - 138*Power(t1,2)*t2 + 
                  119*t1*Power(t2,2) + 31*Power(t2,3)) + 
               s2*(Power(t1,4) - 116*Power(t1,3)*t2 + 
                  216*Power(t1,2)*Power(t2,2) + 11*t1*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s2*Power(t2,2)*(2*Power(s2,6)*
                (2*Power(t1,2) - t1*t2 + Power(t2,2)) + 
               Power(s2,5)*(-18*Power(t1,3) + 5*Power(t1,2)*t2 - 
                  3*Power(t2,3)) - 
               Power(t1,2)*Power(t1 - t2,3)*
                (2*Power(t1,3) - 11*Power(t1,2)*t2 - 8*t1*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s2*t1*Power(t1 - t2,2)*
                (6*Power(t1,4) - 67*Power(t1,3)*t2 + 
                  14*Power(t1,2)*Power(t2,2) + 19*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(s2,4)*(30*Power(t1,4) + 25*Power(t1,3)*t2 - 
                  64*Power(t1,2)*Power(t2,2) + 34*t1*Power(t2,3) - 
                  7*Power(t2,4)) + 
               Power(s2,3)*(-20*Power(t1,5) - 102*Power(t1,4)*t2 + 
                  212*Power(t1,3)*Power(t2,2) - 
                  117*Power(t1,2)*Power(t2,3) + 18*t1*Power(t2,4) + 
                  3*Power(t2,5)) + 
               Power(s2,2)*t2*
                (136*Power(t1,5) - 273*Power(t1,4)*t2 + 
                  153*Power(t1,3)*Power(t2,2) - 21*t1*Power(t2,4) + 
                  5*Power(t2,5))) + 
            Power(s1,5)*(10*Power(s2,6) - Power(s2,5)*(41*t1 + 13*t2) + 
               Power(s2,4)*(70*Power(t1,2) + 34*t1*t2 - 
                  98*Power(t2,2)) + 
               Power(s2,3)*(-70*Power(t1,3) + 21*Power(t1,2)*t2 + 
                  168*t1*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s2,2)*(48*Power(t1,4) - 82*Power(t1,3)*t2 - 
                  232*Power(t1,2)*Power(t2,2) + 145*t1*Power(t2,3) + 
                  82*Power(t2,4)) + 
               t1*(4*Power(t1,5) - Power(t1,4)*t2 - 
                  114*Power(t1,3)*Power(t2,2) + 
                  202*Power(t1,2)*Power(t2,3) - 81*t1*Power(t2,4) + 
                  13*Power(t2,5)) - 
               s2*(21*Power(t1,5) - 41*Power(t1,4)*t2 - 
                  276*Power(t1,3)*Power(t2,2) + 
                  364*Power(t1,2)*Power(t2,3) + 31*t1*Power(t2,4) + 
                  15*Power(t2,5))) + 
            s1*t2*(Power(s2,8)*(t1 - t2) - 
               3*Power(t1,2)*Power(t1 - t2,3)*Power(t2,2)*
                (2*Power(t1,2) + 2*t1*t2 + Power(t2,2)) + 
               Power(s2,7)*(-8*Power(t1,2) - 7*t1*t2 + 17*Power(t2,2)) + 
               Power(s2,6)*(30*Power(t1,3) + 8*Power(t1,2)*t2 - 
                  80*t1*Power(t2,2) + 22*Power(t2,3)) + 
               Power(s2,5)*(-66*Power(t1,4) + 109*Power(t1,3)*t2 + 
                  8*Power(t1,2)*Power(t2,2) + 25*t1*Power(t2,3) - 
                  41*Power(t2,4)) + 
               Power(s2,4)*(89*Power(t1,5) - 333*Power(t1,4)*t2 + 
                  358*Power(t1,3)*Power(t2,2) - 
                  295*Power(t1,2)*Power(t2,3) + 230*t1*Power(t2,4) - 
                  58*Power(t2,5)) - 
               s2*Power(t1 - t2,3)*
                (6*Power(t1,5) - 25*Power(t1,4)*t2 - 
                  45*Power(t1,3)*Power(t2,2) - 
                  19*Power(t1,2)*Power(t2,3) - 8*t1*Power(t2,4) + 
                  2*Power(t2,5)) + 
               Power(s2,3)*(-72*Power(t1,6) + 391*Power(t1,5)*t2 - 
                  565*Power(t1,4)*Power(t2,2) + 
                  400*Power(t1,3)*Power(t2,3) - 
                  272*Power(t1,2)*Power(t2,4) + 127*t1*Power(t2,5) - 
                  15*Power(t2,6)) + 
               Power(s2,2)*t1*
                (32*Power(t1,6) - 210*Power(t1,5)*t2 + 
                  316*Power(t1,4)*Power(t2,2) - 
                  129*Power(t1,3)*Power(t2,3) + 
                  18*Power(t1,2)*Power(t2,4) - 42*t1*Power(t2,5) + 
                  15*Power(t2,6))) - 
            Power(s1,4)*(3*Power(s2,7) + Power(s2,6)*(-26*t1 + 35*t2) + 
               Power(s2,5)*(76*Power(t1,2) - 143*t1*t2 - 
                  42*Power(t2,2)) - 
               4*Power(s2,4)*
                (27*Power(t1,3) - 74*Power(t1,2)*t2 - 
                  13*t1*Power(t2,2) + 43*Power(t2,3)) + 
               Power(s2,3)*(85*Power(t1,4) - 421*Power(t1,3)*t2 + 
                  327*Power(t1,2)*Power(t2,2) + 110*t1*Power(t2,3) + 
                  39*Power(t2,4)) + 
               Power(s2,2)*(-40*Power(t1,5) + 375*Power(t1,4)*t2 - 
                  608*Power(t1,3)*Power(t2,2) + 
                  35*Power(t1,2)*Power(t2,3) + t1*Power(t2,4) + 
                  121*Power(t2,5)) + 
               s2*(12*Power(t1,6) - 169*Power(t1,5)*t2 + 
                  302*Power(t1,4)*Power(t2,2) + 
                  160*Power(t1,3)*Power(t2,3) - 
                  242*Power(t1,2)*Power(t2,4) - 75*t1*Power(t2,5) - 
                  16*Power(t2,6)) + 
               t1*(-2*Power(t1,6) + 27*Power(t1,5)*t2 - 
                  31*Power(t1,4)*Power(t2,2) - 
                  124*Power(t1,3)*Power(t2,3) + 
                  209*Power(t1,2)*Power(t2,4) - 100*t1*Power(t2,5) + 
                  27*Power(t2,6))) + 
            Power(s1,3)*(-3*Power(s2,8) + 3*Power(s2,7)*(5*t1 + 4*t2) + 
               Power(s2,6)*(-31*Power(t1,2) - 70*t1*t2 + 
                  61*Power(t2,2)) + 
               Power(s2,5)*(37*Power(t1,3) + 153*Power(t1,2)*t2 - 
                  183*t1*Power(t2,2) - 69*Power(t2,3)) + 
               Power(s2,4)*(-33*Power(t1,4) - 180*Power(t1,3)*t2 + 
                  352*Power(t1,2)*Power(t2,2) + 145*t1*Power(t2,3) - 
                  193*Power(t2,4)) + 
               Power(s2,3)*(25*Power(t1,5) + 153*Power(t1,4)*t2 - 
                  685*Power(t1,3)*Power(t2,2) + 
                  483*Power(t1,2)*Power(t2,3) + 29*t1*Power(t2,4) + 
                  24*Power(t2,5)) + 
               t1*t2*(-10*Power(t1,6) + 64*Power(t1,5)*t2 - 
                  74*Power(t1,4)*Power(t2,2) - 
                  37*Power(t1,3)*Power(t2,3) + 
                  83*Power(t1,2)*Power(t2,4) - 45*t1*Power(t2,5) + 
                  19*Power(t2,6)) + 
               Power(s2,2)*(-13*Power(t1,6) - 110*Power(t1,5)*t2 + 
                  789*Power(t1,4)*Power(t2,2) - 
                  1081*Power(t1,3)*Power(t2,3) + 
                  421*Power(t1,2)*Power(t2,4) - 174*t1*Power(t2,5) + 
                  104*Power(t2,6)) + 
               s2*(3*Power(t1,7) + 52*Power(t1,6)*t2 - 
                  398*Power(t1,5)*Power(t2,2) + 
                  591*Power(t1,4)*Power(t2,3) - 
                  192*Power(t1,3)*Power(t2,4) + 
                  76*Power(t1,2)*Power(t2,5) - 128*t1*Power(t2,6) + 
                  2*Power(t2,7))) + 
            Power(s1,2)*(Power(s2,8)*(-3*t1 + 4*t2) + 
               Power(s2,7)*(18*Power(t1,2) - 17*t1*t2 - 24*Power(t2,2)) + 
               Power(s2,6)*(-47*Power(t1,3) + 48*Power(t1,2)*t2 + 
                  113*t1*Power(t2,2) - 61*Power(t2,3)) + 
               t1*Power(t1 - t2,2)*Power(t2,2)*
                (13*Power(t1,4) - 22*Power(t1,3)*t2 - 
                  11*Power(t1,2)*Power(t2,2) - 11*t1*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,5)*(70*Power(t1,4) - 127*Power(t1,3)*t2 - 
                  119*Power(t1,2)*Power(t2,2) + 87*t1*Power(t2,3) + 
                  72*Power(t2,4)) + 
               Power(s2,4)*(-65*Power(t1,5) + 225*Power(t1,4)*t2 - 
                  98*Power(t1,3)*Power(t2,2) + 
                  49*Power(t1,2)*Power(t2,3) - 314*t1*Power(t2,4) + 
                  151*Power(t2,5)) + 
               Power(s2,3)*(38*Power(t1,6) - 222*Power(t1,5)*t2 + 
                  225*Power(t1,4)*Power(t2,2) + 
                  100*Power(t1,3)*Power(t2,3) + 
                  29*Power(t1,2)*Power(t2,4) - 146*t1*Power(t2,5) + 
                  16*Power(t2,6)) - 
               Power(s2,2)*(13*Power(t1,7) - 111*Power(t1,6)*t2 + 
                  88*Power(t1,5)*Power(t2,2) + 
                  409*Power(t1,4)*Power(t2,3) - 
                  568*Power(t1,3)*Power(t2,4) + 
                  226*Power(t1,2)*Power(t2,5) - 95*t1*Power(t2,6) + 
                  38*Power(t2,7)) + 
               s2*(2*Power(t1,8) - 22*Power(t1,7)*t2 - 
                  22*Power(t1,6)*Power(t2,2) + 
                  282*Power(t1,5)*Power(t2,3) - 
                  390*Power(t1,4)*Power(t2,4) + 
                  204*Power(t1,3)*Power(t2,5) - 
                  133*Power(t1,2)*Power(t2,6) + 87*t1*Power(t2,7) - 
                  8*Power(t2,8)))))*Log(s - s1 + t2))/
     (s*s1*s2*t1*(s - s2 + t1)*(s - s1 - s2 + t1)*(s1 + t1 - t2)*
       Power(s1 - s2 + t1 - t2,2)*t2*Power(s + t2,2)*(s - s1 + t2)*
       (s2 - t1 + t2)) - (16*(Power(s,4)*s2*t1*(t1 - t2) + 
         Power(s1,4)*t1*(-s2 + t1)*t2 + 
         Power(s2,3)*(s2 - t1)*Power(t2,2)*(s2 + t2) - 
         Power(s1,3)*(Power(t1,3)*t2 + Power(s2,3)*(t1 + 3*t2) + 
            s2*t1*(Power(t1,2) - 4*t1*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*(-2*Power(t1,2) + 3*Power(t2,2))) + 
         s1*Power(s2,2)*t2*(-2*Power(s2,3) - Power(t1,3) + 
            Power(t1,2)*t2 - 4*t1*Power(t2,2) + Power(t2,3) + 
            2*Power(s2,2)*(t1 + t2) + 
            s2*(Power(t1,2) - 3*t1*t2 + 5*Power(t2,2))) + 
         Power(s1,2)*s2*(Power(s2,4) - Power(s2,3)*(5*t1 + 3*t2) + 
            Power(s2,2)*(8*Power(t1,2) - 3*t1*t2 - 2*Power(t2,2)) + 
            t1*(Power(t1,3) - 4*Power(t1,2)*t2 + t1*Power(t2,2) - 
               Power(t2,3)) + 
            s2*(-5*Power(t1,3) + 10*Power(t1,2)*t2 + 2*Power(t2,3))) + 
         Power(s,3)*(-(s2*(-2*Power(t1,3) + 3*Power(t1,2)*t2 - 
                 2*t1*Power(t2,2) + Power(t2,3) + 
                 s2*(3*Power(t1,2) - 5*t1*t2 + Power(t2,2)))) + 
            s1*(t1*t2*(-t1 + t2) + Power(s2,2)*(t1 + 4*t2) + 
               s2*(-3*Power(t1,2) + 2*t1*t2 + 4*Power(t2,2)))) - 
         Power(s,2)*(Power(s1,2)*
             (t1*t2*(-3*t1 + 2*t2) + 2*Power(s2,2)*(t1 + 4*t2) + 
               s2*(-3*Power(t1,2) + 2*t1*t2 + 8*Power(t2,2))) + 
            s1*(2*Power(s2,3)*(2*t1 + 5*t2) + 
               Power(s2,2)*(-10*Power(t1,2) + 6*t1*t2 + 6*Power(t2,2)) + 
               5*s2*(Power(t1,3) - 2*Power(t1,2)*t2 + t1*Power(t2,2) - 
                  Power(t2,3)) + 
               t2*(Power(t1,3) - 2*Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
                  Power(t2,3))) - 
            s2*(Power(s2,2)*(2*Power(t1,2) - 7*t1*t2 + 3*Power(t2,2)) + 
               t1*(Power(t1,3) - 2*Power(t1,2)*t2 + 2*t1*Power(t2,2) - 
                  Power(t2,3)) + 
               s2*(-3*Power(t1,3) + 7*Power(t1,2)*t2 - 5*t1*Power(t2,2) + 
                  3*Power(t2,3)))) + 
         s*(Power(s2,2)*t2*(3*Power(s2,2)*(t1 - t2) + 
               s2*(-4*Power(t1,2) + 4*t1*t2 - 3*Power(t2,2)) + 
               t1*(Power(t1,2) - 2*t1*t2 + 2*Power(t2,2))) + 
            Power(s1,3)*(t1*t2*(-3*t1 + t2) + Power(s2,2)*(t1 + 4*t2) + 
               s2*(-Power(t1,2) + 2*t1*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(-Power(s2,4) + Power(s2,3)*(6*t1 + 11*t2) + 
               t1*t2*(2*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
               Power(s2,2)*(-9*Power(t1,2) + 2*t1*t2 + 10*Power(t2,2)) + 
               s2*(4*Power(t1,3) - 11*Power(t1,2)*t2 - 2*Power(t2,3))) + 
            s1*s2*(Power(s2,2)*t1*(-9*t1 + 2*t2) + 
               Power(s2,3)*(3*t1 + 8*t2) + 
               s2*(8*Power(t1,3) - 15*Power(t1,2)*t2 + 7*t1*Power(t2,2) - 
                  10*Power(t2,3)) - 
               2*(Power(t1,4) - 3*Power(t1,3)*t2 + 
                  3*Power(t1,2)*Power(t2,2) - 3*t1*Power(t2,3) + 
                  Power(t2,4)))))*Log(s2 - t1 + t2))/
     (s1*s2*(-s + s2 - t1)*(-s + s1 + s2 - t1)*t1*(-s + s1 - t2)*t2*
       (s2 + t2)));
       
       return a;
};
