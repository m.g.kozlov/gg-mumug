int
logl_c(const long double zr, const long double zi,  long double * lnr, long double * theta)
{
    if(zr != 0.0 || zi != 0.0) {
    const long double ax = fabsl(zr);
    const long double ay = fabsl(zi);
    const long double min = fminl(ax, ay);
    const long double max = fmaxl(ax, ay);
    
    (* lnr) = logl(max) + 0.5L * logl(1.0L + (min/max)*(min/max));
    (* theta) = atan2l(zi, zr);
    
    return 1;
  }
  else {
    return 0;
  }
}
