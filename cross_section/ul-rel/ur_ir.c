#include "../h/nlo_functions.h"
#include "../h/dilog.h"

#define Pi       3.141592653589793238462643383279502884L
#define Power pq



long double ur_ir(double *vars)
{
    long double s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    long double omega=vars[5];
    long double aG=1/Power(4.*Pi,2);
    long double a=aG*((32*Power(Pi,2)*(-s + s1 + s2)*
       (Power(s1,3)*(s2 + t1) + 3*Power(s,2)*(s2 - 2*t1)*t2 - 
         3*Power(s1,2)*t1*(s + t2) - Power(s,3)*(t1 + t2) + 
         (s2 - 2*t1)*t2*(Power(s2,2) - s2*t1 + Power(t1,2) + 
            Power(t2,2)) - s*(Power(t1,3) + 3*Power(s2,2)*t2 + 
            3*Power(t1,2)*t2 + Power(t2,3) + 3*t1*t2*(-2*s2 + t2)) + 
         s1*(Power(s2,3) + t1*(3*Power(s,2) + Power(t1,2) + 6*s*t2 + 
               3*Power(t2,2)))))/
     (3.*s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) + 
    (32*(s - s1 - s2)*(-2*Power(s1,3)*(s2 + t1) + 2*Power(s,3)*(t1 + t2) - 
         Power(s,2)*(-2*Power(t1 + t2,2) + s1*(s2 + 5*t1 + t2) + 
            s2*(t1 + 5*t2)) + Power(s1,2)*
          (Power(s2,2) - 6*s2*t1 + t1*(t1 + 6*t2)) + 
         t2*(-2*Power(s2,3) + Power(s2,2)*(6*t1 + t2) - 
            2*s2*(3*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
            4*t1*(Power(t1,2) - t1*t2 + Power(t2,2))) - 
         2*s1*(Power(s2,3) + 3*Power(s2,2)*t2 - 3*s2*t1*t2 + 
            t1*(Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) + 
         s*(Power(s1,2)*(s2 + 5*t1 + t2) + Power(s2,2)*(t1 + 5*t2) - 
            s2*(Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) + 
            2*(Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
               Power(t2,3)) + s1*
             (Power(s2,2) - 3*Power(t1,2) - 4*t1*t2 - Power(t2,2) + 
               6*s2*(t1 + t2)))))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) + 
    (16*(-s + s1 + s2)*(Power(s1,3)*(s2 + t1) + 
         3*Power(s,2)*(s2 - 2*t1)*t2 - 3*Power(s1,2)*t1*(s + t2) - 
         Power(s,3)*(t1 + t2) + 
         (s2 - 2*t1)*t2*(Power(s2,2) - s2*t1 + Power(t1,2) + 
            Power(t2,2)) - s*(Power(t1,3) + 3*Power(s2,2)*t2 + 
            3*Power(t1,2)*t2 + Power(t2,3) + 3*t1*t2*(-2*s2 + t2)) + 
         s1*(Power(s2,3) + t1*
             (3*Power(s,2) + Power(t1,2) + 6*s*t2 + 3*Power(t2,2))))*
       lg2(s - s1 - s2))/(s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
    (32*(-s + s1 + s2)*(Power(s1,3)*(s2 + t1) + 
         3*Power(s,2)*(s2 - 2*t1)*t2 - 3*Power(s1,2)*t1*(s + t2) - 
         Power(s,3)*(t1 + t2) + 
         (s2 - 2*t1)*t2*(Power(s2,2) - s2*t1 + Power(t1,2) + 
            Power(t2,2)) - s*(Power(t1,3) + 3*Power(s2,2)*t2 + 
            3*Power(t1,2)*t2 + Power(t2,3) + 3*t1*t2*(-2*s2 + t2)) + 
         s1*(Power(s2,3) + t1*
             (3*Power(s,2) + Power(t1,2) + 6*s*t2 + 3*Power(t2,2))))*
       Log(s - s1 - s2))/(s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) + 
    Log(4)*((32*(-s + s1 + s2)*
          (Power(s1,3)*(s2 + t1) + 3*Power(s,2)*(s2 - 2*t1)*t2 - 
            3*Power(s1,2)*t1*(s + t2) - Power(s,3)*(t1 + t2) + 
            (s2 - 2*t1)*t2*(Power(s2,2) - s2*t1 + Power(t1,2) + 
               Power(t2,2)) - 
            s*(Power(t1,3) + 3*Power(s2,2)*t2 + 3*Power(t1,2)*t2 + 
               Power(t2,3) + 3*t1*t2*(-2*s2 + t2)) + 
            s1*(Power(s2,3) + 
               t1*(3*Power(s,2) + Power(t1,2) + 6*s*t2 + 3*Power(t2,2)))))/
        (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
       (32*(-s + s1 + s2)*(Power(s1,3)*(s2 + t1) + 
            3*Power(s,2)*(s2 - 2*t1)*t2 - 3*Power(s1,2)*t1*(s + t2) - 
            Power(s,3)*(t1 + t2) + 
            (s2 - 2*t1)*t2*(Power(s2,2) - s2*t1 + Power(t1,2) + 
               Power(t2,2)) - 
            s*(Power(t1,3) + 3*Power(s2,2)*t2 + 3*Power(t1,2)*t2 + 
               Power(t2,3) + 3*t1*t2*(-2*s2 + t2)) + 
            s1*(Power(s2,3) + 
               t1*(3*Power(s,2) + Power(t1,2) + 6*s*t2 + 3*Power(t2,2))))*
          Log(s - s1 - s2))/(s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2)) - 
    (32*(s - s1 - s2)*(-2*Power(s1,3)*(s2 + t1) + 
         2*Power(s,3)*(t1 + t2) - 
         Power(s,2)*(-2*Power(t1 + t2,2) + s1*(s2 + 5*t1 + t2) + 
            s2*(t1 + 5*t2)) + 
         Power(s1,2)*(Power(s2,2) - 6*s2*t1 + t1*(t1 + 6*t2)) + 
         t2*(-2*Power(s2,3) + Power(s2,2)*(6*t1 + t2) - 
            2*s2*(3*Power(t1,2) - 2*t1*t2 + Power(t2,2)) + 
            4*t1*(Power(t1,2) - t1*t2 + Power(t2,2))) - 
         2*s1*(Power(s2,3) + 3*Power(s2,2)*t2 - 3*s2*t1*t2 + 
            t1*(Power(t1,2) - 2*t1*t2 + 3*Power(t2,2))) + 
         s*(Power(s1,2)*(s2 + 5*t1 + t2) + Power(s2,2)*(t1 + 5*t2) - 
            s2*(Power(t1,2) + 4*t1*t2 + 3*Power(t2,2)) + 
            2*(Power(t1,3) + Power(t1,2)*t2 + t1*Power(t2,2) + 
               Power(t2,3)) + 
            s1*(Power(s2,2) - 3*Power(t1,2) - 4*t1*t2 - Power(t2,2) + 
               6*s2*(t1 + t2))))*Log(-s + s1 + s2))/
     (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) + 
    Log(omega)*((64*(-s + s1 + s2)*
          (Power(s1,3)*(s2 + t1) + 3*Power(s,2)*(s2 - 2*t1)*t2 - 
            3*Power(s1,2)*t1*(s + t2) - Power(s,3)*(t1 + t2) + 
            (s2 - 2*t1)*t2*(Power(s2,2) - s2*t1 + Power(t1,2) + 
               Power(t2,2)) - s*
             (Power(t1,3) + 3*Power(s2,2)*t2 + 3*Power(t1,2)*t2 + 
               Power(t2,3) + 3*t1*t2*(-2*s2 + t2)) + 
            s1*(Power(s2,3) + t1*
                (3*Power(s,2) + Power(t1,2) + 6*s*t2 + 3*Power(t2,2)))))/
        (s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2) - 
       (64*(-s + s1 + s2)*(Power(s1,3)*(s2 + t1) + 
            3*Power(s,2)*(s2 - 2*t1)*t2 - 3*Power(s1,2)*t1*(s + t2) - 
            Power(s,3)*(t1 + t2) + 
            (s2 - 2*t1)*t2*(Power(s2,2) - s2*t1 + Power(t1,2) + 
               Power(t2,2)) - s*
             (Power(t1,3) + 3*Power(s2,2)*t2 + 3*Power(t1,2)*t2 + 
               Power(t2,3) + 3*t1*t2*(-2*s2 + t2)) + 
            s1*(Power(s2,3) + t1*
                (3*Power(s,2) + Power(t1,2) + 6*s*t2 + 3*Power(t2,2))))*
          Log(-s + s1 + s2))/(s1*s2*(-s + s2 - t1)*t1*(-s + s1 - t2)*t2)));
          
          return a;
};
