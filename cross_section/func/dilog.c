#include <float.h>
#include <math.h>
#include <tgmath.h>
//#include <stdio.h>


#define Pi       3.141592653589793238462643383279502884L /* pi */
#define  MY_LDBL_EPSILON    0.000000000000000000000000001L



#include "logc.c"
#include "clausen.c"

/* Evaluate series for real dilog(x)
 * Sum[ x^k / k^2, {k,1,Infinity}]
 *
 * Converges rapidly for |x| < 1/2.
 */
long double 
dilog_series_1(long double x)
{
  const int kmax = 1000;
  long double sum  = x;
  long double term = x;
  int k;
  for(k=2; k<kmax; k++) {
    const long double rk = (k-1.0L)/k;
    term *= x;
    term *= rk*rk;
    sum += term;
    if(fabsl(term/sum) < MY_LDBL_EPSILON) break;
  }
  return sum;
}


/* Compute the associated series
 *
 *   sum_{k=1}{infty} r^k / (k^2 (k+1))
 *
 * This is a series which appears in the one-step accelerated
 * method, which splits out one elementary function from the
 * full definition of Li_2(x). See below.
 */
long double
series_2(long double r)
{
  static const int kmax = 100;
  long double rk = r;
  long double sum = 0.5L * r;
  int k;
  for(k=2; k<13; k++)
  {
    long double ds;
    rk *= r;
    ds = rk/(k*k*(k+1.0L));
    sum += ds;
  }
  for(; k<kmax; k++)
  {
    long double ds;
    rk *= r;
    ds = rk/(k*k*(k+1.0L));
    sum += ds;
    if(fabsl(ds/sum) < 0.5*MY_LDBL_EPSILON) break;
  }

  
  return sum;
}


/* Compute Li_2(x) using the accelerated series representation.
 *
 * Li_2(x) = 1 + (1-x)ln(1-x)/x + series_2(x)
 *
 * assumes: -1 < x < 1
 */
long double
dilog_series_2(long double x)
{
  long double val=series_2(x);
  long double t;
  if(x > 0.01)
    t = (1.0L - x) * logl(1.0L-x) / x;
  else
  {
    static const long double c3 = 1.0L/3.0L;
    static const long double c4 = 1.0L/4.0L;
    static const long double c5 = 1.0L/5.0L;
    static const long double c6 = 1.0L/6.0L;
    static const long double c7 = 1.0L/7.0L;
    static const long double c8 = 1.0L/8.0L;
    const long double t68 = c6 + x*(c7 + x*c8);
    const long double t38 = c3 + x *(c4 + x *(c5 + x * t68));
    t = (x - 1.0L) * (1.0L + x*(0.5L + x*t38));
  }
  val +=1.0L+t;
  return val;
}


/* Calculates Li_2(x) for real x. Assumes x >= 0.0.
 */
long double
dilog_xge0(const long double x)
{
  long double val;
  if(x > 2.0) {
    const long double log_x = logl(x);
    const long double t1 = Pi*Pi/3.0L;
    const long double t2 = dilog_series_2(1.0L/x);
    const long double t3 = 0.5L*log_x*log_x;
    val  = t1 - t2 - t3;
    return val;
  }
  else if(x > 1.01) {
    const long double log_x    = logl(x);
    const long double log_term = log_x * (logl(1.0L-1.0L/x) + 0.5L*log_x);
    const long double t1 = Pi*Pi/6.0;
    const long double t2 = dilog_series_2(1.0L - 1.0L/x);
    const long double t3 = log_term;
    val  = t1 + t2 - t3;
    return val;
  }
  else if(x > 1.0) {
    /* series around x = 1.0 */
    const long double eps = x - 1.0L;
    const long double lne = logl(eps);
    const long double c0 = Pi*Pi/6.0L;
    const long double c1 =   1.0L - lne;
    const long double c2 = -(1.0L - 2.0L*lne)/4.0L;
    const long double c3 =  (1.0L - 3.0L*lne)/9.0L;
    const long double c4 = -(1.0L - 4.0L*lne)/16.0L;
    const long double c5 =  (1.0L - 5.0L*lne)/25.0L;
    const long double c6 = -(1.0L - 6.0L*lne)/36.0L;
    const long double c7 =  (1.0L - 7.0L*lne)/49.0L;
    const long double c8 = -(1.0L - 8.0L*lne)/64.0L;
    val = c0+eps*(c1+eps*(c2+eps*(c3+eps*(c4+eps*(c5+eps*(c6+eps*(c7+eps*c8)))))));
    return val;
  }
  else if(x == 1.0) {
    val = Pi*Pi/6.0L;
    return val;
  }
  else if(x > 0.5) {
    const long double log_x = logl(x);
    const long double t1 = Pi*Pi/6.0L;
    const long double t2 = dilog_series_2(1.0L-x);
    const long double t3 = log_x*logl(1.0L-x);
    val  = t1 - t2 - t3;
    return val;
  }
  else if(x > 0.25) {
    return dilog_series_2(x);
  }
  else if(x > 0.0) {
    return dilog_series_1(x);
  }
  else {
    /* x == 0.0 */
    val = 0.0L;
    return val;
  }
};




long double
dilog(long double x)
{
  if(x >= 0.0) {
    return dilog_xge0(x);
  }
  else {
    long double d1 = dilog_xge0( -x);
    long double d2 = dilog_xge0(x*x);
    return -d1 + 0.5L * d2;
  }
};

/*
############ COMPLEX DILOG ###############
 */


/* Evaluate the series representation for Li2(z):
 *
 *   Li2(z) = Sum[ |z|^k / k^2 Exp[i k arg(z)], {k,1,Infinity}]
 *   |z|    = r
 *   arg(z) = theta
 *   
 * Assumes 0 < r < 1.
 * It is used only for small r.
 */
static
int
dilogc_series_1(
  const long double r,
  const long double x,
  const long double y,
  long double * real_result,
  long double * imag_result
  )
{
  const long double cos_theta = x/r;
  const long double sin_theta = y/r;
  const long double alpha = 1.0L - cos_theta;
  const long double beta  = sin_theta;
  long double ck = cos_theta;
  long double sk = sin_theta;
  long double rk = r;
  long double real_sum = r*ck;
  long double imag_sum = r*sk;
  const int kmax = 100 + (int)(22.0L/(-logl(r))); 
  int k;
  for(k=2; k<kmax; k++) {
    long double dr, di;
    long double ck_tmp = ck;
    ck = ck - (alpha*ck + beta*sk);
    sk = sk - (alpha*sk - beta*ck_tmp);
    rk *= r;
    dr = rk/((long double)k*k) * ck;
    di = rk/((long double)k*k) * sk;
    real_sum += dr;
    imag_sum += di;
    if(fabsl((dr*dr + di*di)/(real_sum*real_sum + imag_sum*imag_sum)) < MY_LDBL_EPSILON*MY_LDBL_EPSILON) break;
  }

  (* real_result) = real_sum;
  (* imag_result) = imag_sum;
  

  return 1;
};



/* Compute
 *
 *   sum_{k=1}{infty} z^k / (k^2 (k+1))
 *
 * This is a series which appears in the one-step accelerated
 * method, which splits out one elementary function from the
 * full definition of Li_2.
 */
static int
series_2_c(
  long double r,
  long double x,
  long double y,
  long double * sum_re,
  long double * sum_im
  )
{
  const long double cos_theta = x/r;
  const long double sin_theta = y/r;
  const long double alpha = 1.0L - cos_theta;
  const long double beta  = sin_theta;
  long double ck = cos_theta;
  long double sk = sin_theta;
  long double rk = r;
  long double real_sum = 0.5L * r*ck;
  long double imag_sum = 0.5L * r*sk;
  const int kmax = 100 + (int)(18.0L/(-logl(r))); /* tuned for double-precision */
  int k;
  for(k=2; k<kmax; k++)
  {
    long double dr, di;
    const long double ck_tmp = ck;
    ck = ck - (alpha*ck + beta*sk);
    sk = sk - (alpha*sk - beta*ck_tmp);
    rk *= r;
    dr = rk/((long double)k*k*(k+1.0L)) * ck;
    di = rk/((long double)k*k*(k+1.0L)) * sk;
    real_sum += dr;
    imag_sum += di;
    if(fabs((dr*dr + di*di)/(real_sum*real_sum + imag_sum*imag_sum)) < MY_LDBL_EPSILON*MY_LDBL_EPSILON) break;
  }

  (* sum_re) = real_sum;
  (* sum_im) = imag_sum;
  

  return 1;
};


/* Compute Li_2(z) using the one-step accelerated series.
 *
 * Li_2(z) = 1 + (1-z)ln(1-z)/z + series_2_c(z)
 *
 * z = r exp(i theta)
 * assumes: r < 1
 * assumes: r > epsilon, so that we take no special care with log(1-z)
 */
static
int
dilogc_series_2(
  const long double r,
  const long double x,
  const long double y,
  long double * real_dl,
  long double * imag_dl
  )
{
  if(r == 0.0)
  {
    (* real_dl) = 0.0L;
    (* imag_dl) = 0.0L;
    return 1;
  }
  else
  {
    long double sum_re;
    long double sum_im;
    series_2_c(r, x, y, &sum_re, &sum_im);

    /* t = ln(1-z)/z */
    long double ln_omz_r;
    long double ln_omz_theta;
    
    logl_c(1.0L-x, -y, &ln_omz_r, &ln_omz_theta);
    
    const long double t_x = ( ln_omz_r * x + ln_omz_theta * y)/(r*r);
    const long double t_y = (-ln_omz_r * y + ln_omz_theta * x)/(r*r);

    /* r = (1-z) ln(1-z)/z */
    const long double r_x = (1.0L - x) * t_x + y * t_y;
    const long double r_y = (1.0L - x) * t_y - y * t_x;

    (* real_dl) = sum_re + r_x + 1.0L;
    (* imag_dl) = sum_im + r_y;
    
    return 1;
  }
};



/* Evaluate a series for Li_2(z) when |z| is near 1.
 * This is uniformly good away from z=1.
 *
 *   Li_2(z) = Sum[ a^n/n! H_n(theta), {n, 0, Infinity}]
 *
 * where
 *   H_n(theta) = Sum[ e^(i m theta) m^n / m^2, {m, 1, Infinity}]
 *   a = ln(r)
 *
 *  H_0(t) = Gl_2(t) + i Cl_2(t)
 *  H_1(t) = 1/2 ln(2(1-c)) + I atan2(-s, 1-c)
 *  H_2(t) = -1/2 + I/2 s/(1-c)
 *  H_3(t) = -1/2 /(1-c)
 *  H_4(t) = -I/2 s/(1-c)^2
 *  H_5(t) = 1/2 (2 + c)/(1-c)^2
 *  H_6(t) = I/2 s/(1-c)^5 (8(1-c) - s^2 (3 + c))
 */
static
int
dilogc_series_3(
  const long double r,
  const long double x,
  const long double y,
  long double * real_result,
  long double * imag_result
  )
{
  const long double theta = atan2l(y, x);
  const long double cos_theta = x/r;
  const long double sin_theta = y/r;
  const long double a = logl(r);
  const long double omc = 1.0L - cos_theta;
  const long double omc2 = omc*omc;
  long double H_re[7];
  long double H_im[7];
  long double an, nfact;
  long double sum_re, sum_im;
  long double Him0;
  int n;

  H_re[0] = Pi*Pi/6.0L + 0.25L*(theta*theta - 2.0L*Pi*fabsl(theta));
  /*!!!!!!!!!*/
  Him0 = clausen(theta);
  
  
  H_im[0] = Him0;

  H_re[1] = -0.5L*logl(2.0L*omc);
  H_im[1] = -atan2l(-sin_theta, omc);

  H_re[2] = -0.5L;
  H_im[2] = 0.5L * sin_theta/omc;

  H_re[3] = -0.5L/omc;
  H_im[3] = 0.0L;

  H_re[4] = 0.0L;
  H_im[4] = -0.5L*sin_theta/omc2;

  H_re[5] = 0.5L * (2.0L + cos_theta)/omc2;
  H_im[5] = 0.0L;

  H_re[6] = 0.0L;
  H_im[6] = 0.5L * sin_theta/(omc2*omc2*omc) * (8.0L*omc - sin_theta*sin_theta*(3.0L + cos_theta));

  sum_re = H_re[0];
  sum_im = H_im[0];
  an = 1.0L;
  nfact = 1.0L;
  for(n=1; n<=6; n++) {
    double t;
    an *= a;
    nfact *= n;
    t = an/nfact;
    sum_re += t * H_re[n];
    sum_im += t * H_im[n];
  }

  (* real_result) = sum_re;
  (* imag_result) = sum_im;
  

  return 1;
};



/* Calculate complex dilogarithm Li_2(z) in the fundamental region,
 * which we take to be the intersection of the unit disk with the
 * half-space x < MAGIC_SPLIT_VALUE. It turns out that 0.732 is a
 * nice choice for MAGIC_SPLIT_VALUE since then points mapped out
 * of the x > MAGIC_SPLIT_VALUE region and into another part of the
 * unit disk are bounded in radius by MAGIC_SPLIT_VALUE itself.
 *
 * If |z| < 0.98 we use a direct series summation. Otherwise z is very
 * near the unit circle, and the series_2 expansion is used; see above.
 * Because the fundamental region is bounded away from z = 1, this
 * works well.
 */
static
int
dilogc_fundamental(long double r,long double x,long double y, long double * real_dl, long double * imag_dl)
{
  if(r > 0.98)  
    return dilogc_series_3(r, x, y, real_dl, imag_dl);
  else if(r > 0.25)
    return dilogc_series_2(r, x, y, real_dl, imag_dl);
  else
    return dilogc_series_1(r, x, y, real_dl, imag_dl);
};


/* Compute Li_2(z) for z in the unit disk, |z| < 1. If z is outside
 * the fundamental region, which means that it is too close to z = 1,
 * then it is reflected into the fundamental region using the identity
 *
 *   Li2(z) = -Li2(1-z) + zeta(2) - ln(z) ln(1-z).
 */
static
int
dilogc_unitdisk(long double x,long double y, long double * real_dl, long double * imag_dl)
{
  static const long double MAGIC_SPLIT_VALUE = 0.732L;
  static const long double zeta2 = Pi*Pi/6.0L;
  const long double r = hypotl(x, y);

  if(x > MAGIC_SPLIT_VALUE)
  {
    /* Reflect away from z = 1 if we are too close. The magic value
     * insures that the reflected value of the radius satisfies the
     * related inequality r_tmp < MAGIC_SPLIT_VALUE.
     */
    const long double x_tmp = 1.0L - x;
    const long double y_tmp =     - y;
    const long double r_tmp = hypotl(x_tmp, y_tmp);
    /* const double cos_theta_tmp = x_tmp/r_tmp; */
    /* const double sin_theta_tmp = y_tmp/r_tmp; */

    long double result_re_tmp;
    long double result_im_tmp;

    const int stat_dilog = dilogc_fundamental(r_tmp, x_tmp, y_tmp, &result_re_tmp, &result_im_tmp);

    const long double lnz    =  logl(r);               /*  log(|z|)   */
    const long double lnomz  =  logl(r_tmp);           /*  log(|1-z|) */
    const long double argz   =  atan2l(y, x);          /*  arg(z) assuming principal branch */
    const long double argomz =  atan2l(y_tmp, x_tmp);  /*  arg(1-z)   */
    (* real_dl)  = -result_re_tmp + zeta2 - lnz*lnomz + argz*argomz;
    (* imag_dl)  = -result_im_tmp - argz*lnomz - argomz*lnz;
    
    return stat_dilog;
  }
  else
  {
    return dilogc_fundamental(r, x, y, real_dl, imag_dl);
  }
};



int
dilogc(
  const long double x,
  const long double y,
  long double * real_dl,
  long double * imag_dl
  )
{
  const long double zeta2 = Pi*Pi/6.0L;
  const long double r2 = x*x + y*y;

  if(y == 0.0)
  {
    if(x >= 1.0)
    {
      (* imag_dl) = -Pi * logl(x);
    }
    else
    {
      (* imag_dl) = 0.0L;
    }
    (* real_dl) = dilog(x);
    return 1;
  }
  else if(fabsl(r2 - 1.0L) < MY_LDBL_EPSILON)
  {
    /* Lewin A.2.4.1 and A.2.4.2 */

    const long double theta = atan2l(y, x);
    const long double term1 = theta*theta/4.0L;
    const long double term2 = Pi*fabsl(theta)/2.0L;
    (* real_dl) = zeta2 + term1 - term2;
    /*!!!!!!!!!!!!!*/
    (* imag_dl) = clausen(theta);
    return 1;
  }
  else if(r2 < 1.0)
  {
    return dilogc_unitdisk(x, y, real_dl, imag_dl);
  }
  else
  {
    /* Reduce argument to unit disk. */
    const long double r = sqrtl(r2);
    const long double x_tmp =  x/r2;
    const long double y_tmp = -y/r2;
    /* const double r_tmp = 1.0/r; */
    long double result_re_tmp, result_im_tmp;

    const int stat_dilog =
      dilogc_unitdisk(x_tmp, y_tmp, &result_re_tmp, &result_im_tmp);

    /* Unwind the inversion.
     *
     *  Li_2(z) + Li_2(1/z) = -zeta(2) - 1/2 ln(-z)^2
     */
    const long double theta = atan2l(y, x);
    const long double theta_abs = fabsl(theta);
    const long double theta_sgn = ( theta < 0.0 ? -1.0L : 1.0L );
    const long double ln_minusz_re = logl(r);
    const long double ln_minusz_im = theta_sgn * (theta_abs - Pi);
    const long double lmz2_re = ln_minusz_re*ln_minusz_re - ln_minusz_im*ln_minusz_im;
    const long double lmz2_im = 2.0L*ln_minusz_re*ln_minusz_im;
    (* real_dl) = -result_re_tmp - 0.5L * lmz2_re - zeta2;
    
    (* imag_dl) = -result_im_tmp - 0.5L * lmz2_im;
    return stat_dilog;
  }
};

















