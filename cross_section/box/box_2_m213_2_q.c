#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m213_2_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(2*Power(s,2)*s1*t2*(Power(s1,2)*
             (2 - t1*(-5 + t2) - 9*t2 + 3*Power(t2,2)) + 
            2*s1*(-2 + t1*(3 - 7*t2) + Power(t1,2)*(-1 + t2) - t2 + 
               10*Power(t2,2) - 3*Power(t2,3)) + 
            t2*(4 - 2*Power(t1,2)*(-1 + t2) - 11*Power(t2,2) + 
               3*Power(t2,3) + t1*(-6 + 9*t2 + Power(t2,2)))) - 
         s1*(s1 - t2)*t2*(8 + Power(s1,3)*(4 + 5*s2 - 9*t1)*(-1 + t2) - 
            29*t2 + 54*s2*t2 - 4*Power(s2,2)*t2 + 67*Power(t2,2) - 
            61*s2*Power(t2,2) + 10*Power(s2,2)*Power(t2,2) - 
            47*Power(t2,3) + 8*s2*Power(t2,3) + 
            2*Power(s2,2)*Power(t2,3) + Power(t2,4) - s2*Power(t2,4) + 
            Power(t1,2)*(8 + 5*t2 - 5*Power(t2,2)) - 
            t1*(16 + 2*(4 + 9*s2)*t2 + (20 - 9*s2)*Power(t2,2) + 
               (-44 + 7*s2)*Power(t2,3)) + 
            Power(s1,2)*(-7 + Power(t1,2)*(21 - 13*t2) + 22*t2 - 
               15*Power(t2,2) + Power(s2,2)*(2 + 6*t2) + 
               2*t1*(-4 - 7*t2 + 11*Power(t2,2)) + 
               s2*(11 - 4*t2 - 7*Power(t2,2) + t1*(-19 + 3*t2))) + 
            s1*(5 - 36*t2 + 21*Power(t2,2) + 10*Power(t2,3) - 
               4*Power(s2,2)*(-1 + 3*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-29 + 8*t2 + 5*Power(t2,2)) - 
               t1*(-56 + 20*t2 + 23*Power(t2,2) + 13*Power(t2,3)) + 
               s2*(-54 + 50*t2 + Power(t2,2) + 3*Power(t2,3) + 
                  2*t1*(9 + 5*t2 + 2*Power(t2,2))))) + 
         s*(Power(s1,4)*(2 + (-22 + 2*s2 - t1)*t2 + 
               (25 + 6*s2 - 7*t1)*Power(t2,2) - 5*Power(t2,3)) - 
            2*Power(t2,2)*(1 - 2*t2 + Power(t2,3)) + 
            s1*t2*(-10 + (-30 + 8*s2)*t2 + (50 - 8*s2)*Power(t2,2) + 
               (-1 + 24*s2)*Power(t2,3) - 2*(5 + 4*s2)*Power(t2,4) + 
               Power(t2,5) + Power(t1,2)*
                (-10 + 10*t2 + 15*Power(t2,2) - 7*Power(t2,3)) + 
               t1*(24 + (14 - 8*s2)*t2 + (-91 + 2*s2)*Power(t2,2) + 
                  (27 - 2*s2)*Power(t2,3) + 10*Power(t2,4))) + 
            Power(s1,3)*(Power(t1,2)*(25 - 17*t2)*t2 + 
               t1*(2 - (23 + 14*s2)*t2 + (1 + 6*s2)*Power(t2,2) + 
                  20*Power(t2,3)) + 
               (-1 + t2)*t2*(32 - 45*t2 + 11*Power(t2,2) - 
                  4*s2*(2 + 5*t2))) + 
            Power(s1,2)*(2*Power(t1,2)*
                (-1 + t2 - 14*Power(t2,2) + 6*Power(t2,3)) + 
               t1*t2*(-40 + 90*t2 - 3*Power(t2,2) - 23*Power(t2,3) - 
                  4*s2*(-2 - 3*t2 + Power(t2,2))) - 
               t2*(-38 + 8*t2 + 64*Power(t2,2) - 41*Power(t2,3) + 
                  7*Power(t2,4) + s2*(8 + 38*Power(t2,2) - 22*Power(t2,3)))\
))))/(s*(-1 + s1)*s1*(-1 + t1)*Power(s1 - t2,2)*Power(-1 + t2,2)*t2) - 
    (8*(Power(s,5)*(-1 + t2)*t2*((-2 + t2)*t2 - t1*(2 + t2)) + 
         4*(-1 + t2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          (Power(s1,2)*t2 + t2*(-1 + 2*t2) - 
            s1*(-1 + 2*t2 + Power(t2,2))) + 
         Power(s,4)*(Power(t1,2)*
             (-2 + t2 + 2*Power(t2,2) - Power(t2,3)) + 
            t2*(2 + (1 + s2)*t2 + (36 + s2)*Power(t2,2) - 
               2*(9 + s2)*Power(t2,3) + Power(t2,4)) + 
            t1*t2*(s2*Power(-1 + t2,2) + 
               2*(-3 + 5*t2 - 2*Power(t2,2) + Power(t2,3))) + 
            s1*(t1*(2 - 7*t2 + 8*Power(t2,2) + 3*Power(t2,3)) + 
               t2*(2 - 9*t2 + 11*Power(t2,2) - 2*Power(t2,3) + 
                  s2*(2 - 3*t2 + Power(t2,2))))) - 
         Power(s,3)*(Power(t1,2)*
             (-6 + 12*t2 + 3*Power(t2,2) - 5*Power(t2,3)) + 
            t1*(4 + 10*t2 + (5 - 14*s2)*Power(t2,2) + 
               (-33 + 5*s2)*Power(t2,3) + (23 + s2)*Power(t2,4) - 
               3*Power(t2,5)) + 
            Power(s1,2)*(-(t2*
                  (-4 + 9*t2 - 10*Power(t2,2) + Power(t2,3))) + 
               s2*(-2 + 9*t2 - 11*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(4 - 16*t2 + 15*Power(t2,2) + 3*Power(t2,3))) + 
            t2*(8 + 31*t2 + 76*Power(t2,2) - 92*Power(t2,3) + 
               18*Power(t2,4) - Power(t2,5) + 
               Power(s2,2)*(t2 - Power(t2,3)) + 
               s2*(-5 + 4*t2 + 28*Power(t2,2) - 14*Power(t2,3) + 
                  Power(t2,4))) + 
            s1*(-2 - 11*t2 - 57*Power(t2,2) + 97*Power(t2,3) - 
               23*Power(t2,4) + Power(s2,2)*t2*(-1 + Power(t2,2)) + 
               Power(t1,2)*(-10 + 9*Power(t2,2) - 3*Power(t2,3)) + 
               t1*t2*(-22 + 58*t2 - 37*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(t2*(5 - 34*t2 + 22*Power(t2,2) - 5*Power(t2,3)) + 
                  t1*(3 + 7*t2 - 4*Power(t2,2) + 2*Power(t2,3))))) + 
         Power(s,2)*(-2 + 8*t1 - 6*Power(t1,2) - 7*t2 - 3*s2*t2 + 
            8*t1*t2 - 3*s2*t1*t2 + 25*Power(t1,2)*t2 + 14*Power(t2,2) + 
            53*s2*Power(t2,2) - 3*Power(s2,2)*Power(t2,2) - 
            57*t1*Power(t2,2) - 21*s2*t1*Power(t2,2) - 
            14*Power(t1,2)*Power(t2,2) + 43*Power(t2,3) + 
            57*s2*Power(t2,3) - Power(s2,2)*Power(t2,3) - 
            38*t1*Power(t2,3) + 25*s2*t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) - 107*Power(t2,4) - 
            74*s2*Power(t2,4) + 53*t1*Power(t2,4) - s2*t1*Power(t2,4) + 
            Power(t1,2)*Power(t2,4) + 64*Power(t2,5) + 
            12*s2*Power(t2,5) - 20*t1*Power(t2,5) - 5*Power(t2,6) - 
            s2*Power(t2,6) + 2*t1*Power(t2,6) + 
            Power(s1,3)*(2*t2*(-1 + Power(t2,2)) + 
               s2*(-4 + 9*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               t1*(4 - 11*t2 + 10*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*(8 + 8*t2 - 62*Power(t2,2) + 48*Power(t2,3) - 
               2*Power(t2,4) + 
               Power(s2,2)*(-1 - 3*t2 - 2*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(t1,2)*(14 + 2*t2 - 15*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-3 - 69*t2 + 106*Power(t2,2) - 49*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s2*(-(t2*(-74 + 81*t2 - 20*Power(t2,2) + Power(t2,3))) + 
                  2*t1*(5 + 2*t2 - 4*Power(t2,2) + Power(t2,3)))) + 
            s1*(-2 - 37*t2 - 20*Power(t2,2) + 156*Power(t2,3) - 
               98*Power(t2,4) + Power(t2,5) + 
               2*Power(s2,2)*t2*(2 + 2*t2 + Power(t2,2) - Power(t2,3)) - 
               Power(t1,2)*(16 - 21*t2 - 3*Power(t2,2) + 
                  7*Power(t2,3) + Power(t2,4)) + 
               t1*(-8 + 82*t2 + 73*Power(t2,2) - 142*Power(t2,3) + 
                  49*Power(t2,4) - 2*Power(t2,5)) + 
               s2*(1 - 41*t2 - 151*Power(t2,2) + 166*Power(t2,3) - 
                  28*Power(t2,4) + Power(t2,5) + 
                  t1*(5 - t2 - 5*Power(t2,2) - 11*Power(t2,3) + 
                     4*Power(t2,4))))) + 
         s*(2 - 4*t1 + 2*Power(t1,2) + 4*t2 - 2*s2*t2 + 14*t1*t2 + 
            2*s2*t1*t2 - 18*Power(t1,2)*t2 + 8*Power(t2,2) - 
            45*s2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) + 
            5*t1*Power(t2,2) + 17*s2*t1*Power(t2,2) + 
            27*Power(t1,2)*Power(t2,2) - 50*Power(t2,3) + 
            47*s2*Power(t2,3) - 11*Power(s2,2)*Power(t2,3) - 
            9*t1*Power(t2,3) - 33*s2*t1*Power(t2,3) - 
            12*Power(t1,2)*Power(t2,3) + 56*Power(t2,4) + 
            47*s2*Power(t2,4) + 2*Power(s2,2)*Power(t2,4) - 
            33*t1*Power(t2,4) + 23*s2*t1*Power(t2,4) - 
            3*Power(t1,2)*Power(t2,4) - 26*Power(t2,5) - 
            49*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) + 
            31*t1*Power(t2,5) - s2*t1*Power(t2,5) + 6*Power(t2,6) + 
            2*s2*Power(t2,6) - 4*t1*Power(t2,6) + 
            2*Power(s1,4)*(s2 - t1)*(-1 + Power(t2,2)) + 
            Power(s1,2)*(Power(-1 + t2,2)*(-7 + 12*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(4 - 17*t2 - 2*Power(t2,2) + 3*Power(t2,3)) + 
               t1*(53 - 35*t2 - 113*Power(t2,2) + 111*Power(t2,3) - 
                  16*Power(t2,4)) + 
               Power(t1,2)*(-6 - 28*t2 + 19*Power(t2,2) + 
                  2*Power(t2,3) + Power(t2,4)) - 
               s2*(27 + 83*t2 - 243*Power(t2,2) + 121*Power(t2,3) + 
                  12*Power(t2,4) + 
                  t1*(1 - 61*t2 + 39*Power(t2,2) - 3*Power(t2,3)))) + 
            Power(s1,3)*(-6*Power(-1 + t2,2)*(1 + t2) + 
               t1*Power(-1 + t2,2)*(-21 + 13*t2) + 
               Power(s2,2)*(3 + 2*t2 - Power(t2,2)) + 
               Power(t1,2)*(10 + 5*t2 - 12*Power(t2,2) + Power(t2,3)) + 
               s2*(Power(-1 + t2,2)*(31 + 5*t2) - 
                  t1*(13 + 7*t2 - 13*Power(t2,2) + Power(t2,3)))) + 
            s1*(-(Power(s2,2)*t2*
                  (8 - 25*t2 + 2*Power(t2,2) + 3*Power(t2,3))) + 
               t1*Power(-1 + t2,2)*
                (2 - 82*t2 - 69*Power(t2,2) + 5*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (12 + 11*t2 - 4*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(10 - 7*t2 + 14*Power(t2,2) - 7*Power(t2,3) + 
                  2*Power(t2,4)) + 
               s2*(Power(-1 + t2,2)*
                   (2 + 76*t2 + 155*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-2 - 16*t2 - 15*Power(t2,2) + 23*Power(t2,3) - 
                     15*Power(t2,4) + Power(t2,5))))))*B1(s,t2,s1))/
     (s*(-1 + s1)*(-1 + t1)*(-1 + t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (16*(-2*(-1 + s1)*Power(s1,2)*Power(s1 - t2,2)*(1 + (-2 + s1)*t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,5)*s1*(s1 - t2)*t2*
          (Power(s1,2)*(-2 + t1 + t2) + 
            t2*(2 - (-2 + t1)*t2 + Power(t2,2)) + 
            2*s1*(1 + Power(t1,2) + t2 - Power(t2,2) - t1*(3 + t2))) + 
         Power(s,4)*(Power(s1,5)*(-2 + t1 + (3 + s2)*t2 - 5*t1*t2) + 
            Power(t2,3)*(-1 + t2 + Power(t2,2)) + 
            Power(s1,3)*t2*(-18 - 25*t2 + 15*Power(t2,2) - 
               5*Power(t2,3) + Power(t1,2)*(-5 + 7*t2) + 
               t1*(46 + 9*t2 - 8*Power(t2,2)) + 
               s2*(2 - 4*t1 + 2*Power(t2,2))) + 
            s1*Power(t2,2)*(4 + (11 - 2*s2)*t2 + 2*Power(t2,2) + 
               (-2 + s2)*Power(t2,3) - Power(t2,4) + 
               t1*(-2 + 2*t2 - 4*Power(t2,2) + Power(t2,3))) + 
            Power(s1,2)*t2*(3 + 4*t2 + 2*(-1 + s2)*Power(t2,2) - 
               2*(1 + s2)*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,2)*(6 + 5*t2 - 3*Power(t2,2)) - 
               t1*(10 + (42 - 4*s2)*t2 + 2*(-5 + s2)*Power(t2,2) + 
                  Power(t2,3))) + 
            Power(s1,4)*(-10*Power(t1,2)*t2 + 
               t1*(-6 + 2*(-2 + s2)*t2 + 13*Power(t2,2)) + 
               2*(1 - (-10 + s2)*t2 - (7 + s2)*Power(t2,2) + Power(t2,3))\
)) + Power(s,3)*(Power(t2,3)*(3 - 4*t2 - 2*Power(t2,2) + Power(t2,3)) + 
            Power(s1,6)*((5 - 2*t2)*t2 + s2*(1 + t2) + t1*(-4 + 9*t2)) + 
            s1*Power(t2,2)*(-13 + (-13 + 6*s2)*t2 + 
               (20 - 6*s2)*Power(t2,2) + (11 - 4*s2)*Power(t2,3) + 
               (-1 + s2)*Power(t2,4) + 
               t1*(6 - 8*t2 + 8*Power(t2,2) - 3*Power(t2,3))) + 
            Power(s1,3)*(3 + (43 - 6*s2)*t2 + (54 - 8*s2)*Power(t2,2) + 
               (17 - 23*s2)*Power(t2,3) + (-9 + s2)*Power(t2,4) + 
               3*Power(t2,5) + 
               Power(t1,2)*(6 - 16*t2 + 5*Power(t2,2) + 
                  6*Power(t2,3)) + 
               2*t1*(-5 + (-45 + 8*s2)*t2 - 15*(-2 + s2)*Power(t2,2) + 
                  (10 + 3*s2)*Power(t2,3))) - 
            Power(s1,2)*t2*(5 + 2*(11 + s2)*t2 + 
               (25 - 12*s2)*Power(t2,2) - 7*(-3 + 2*s2)*Power(t2,3) + 
               4*(-2 + s2)*Power(t2,4) + Power(t2,5) + 
               Power(t1,2)*(18 - t2 - 8*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-28 + 4*(-23 + 3*s2)*t2 + (74 - 14*s2)*Power(t2,2) + 
                  2*(-1 + s2)*Power(t2,3) + Power(t2,4))) - 
            Power(s1,5)*(-16 + Power(t1,2)*(1 - 20*t2) + 28*t2 + 
               8*Power(t2,2) - 5*Power(t2,3) + 
               t1*(2 - 45*t2 + 18*Power(t2,2)) + 
               s2*(2 + t2 + 6*Power(t2,2) + t1*(2 + 6*t2))) + 
            Power(s1,4)*(-16 - 51*t2 + 32*Power(t2,2) + 5*Power(t2,3) - 
               5*Power(t2,4) + Power(t1,2)*(3 - 12*Power(t2,2)) + 
               2*t1*(15 - 8*t2 - 42*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(2 + 4*t2 + 13*Power(t2,2) + 7*Power(t2,3) + 
                  2*t1*(-2 + 9*t2 + Power(t2,2))))) + 
         Power(s,2)*(Power(s1,7)*
             (4 + t1*(5 - 7*t2) + s2*(2 - 5*t2) - 7*t2) + 
            Power(t2,3)*(-3 + 5*t2 + Power(t2,2) - 2*Power(t2,3)) + 
            s1*Power(t2,2)*(14 + (5 - 6*s2)*t2 + 
               4*(-10 + 3*s2)*Power(t2,2) + (-5 + s2)*Power(t2,3) - 
               3*(-3 + s2)*Power(t2,4) + 
               t1*(-6 + 10*t2 - 8*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s1,2)*t2*(1 + 4*(5 + s2)*t2 + 
               (35 - 22*s2)*Power(t2,2) + 
               (-21 + 29*s2 - 6*Power(s2,2))*Power(t2,3) + 
               (-20 + 9*s2 + Power(s2,2))*Power(t2,4) + 
               (7 + s2)*Power(t2,5) + 
               Power(t1,2)*(18 - 7*t2 - 11*Power(t2,2) + 
                  4*Power(t2,3)) - 
               t1*(26 + (78 - 12*s2)*t2 + 2*(-61 + 13*s2)*Power(t2,2) + 
                  (17 - 12*s2)*Power(t2,3) + 2*(3 + s2)*Power(t2,4) + 
                  2*Power(t2,5))) + 
            Power(s1,3)*(-6 + (-64 + 6*s2)*t2 + 
               5*(-15 + 4*s2)*Power(t2,2) + 
               (96 - 93*s2 + 20*Power(s2,2))*Power(t2,3) + 
               (68 - 26*s2)*Power(t2,4) - 6*(4 + s2)*Power(t2,5) + 
               2*Power(t2,6) + 
               Power(t1,2)*(-12 + 11*t2 - 21*Power(t2,2) + 
                  8*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(20 + (118 - 20*s2)*t2 + (-85 + 56*s2)*Power(t2,2) + 
                  (22 - 46*s2)*Power(t2,3) + (4 + 6*s2)*Power(t2,4) + 
                  5*Power(t2,5))) + 
            Power(s1,6)*(-10 + Power(t1,2)*(2 - 20*t2) + 10*t2 + 
               23*Power(t2,2) - Power(t2,3) - Power(s2,2)*(2 + 3*t2) + 
               t1*(20 - 48*t2 + 9*Power(t2,2)) + 
               s2*(4 - 3*t2 + 20*Power(t2,2) + 2*t1*(3 + 5*t2))) + 
            Power(s1,5)*(-24 + 4*(-1 - 7*s2 + 3*Power(s2,2))*t2 + 
               (16 - 21*s2 + 8*Power(s2,2))*Power(t2,2) - 
               (33 + 29*s2)*Power(t2,3) + 4*Power(t2,4) + 
               Power(t1,2)*(-6 + 25*t2 + 15*Power(t2,2)) + 
               t1*(1 - t2 + 109*Power(t2,2) + 2*Power(t2,3) - 
                  2*s2*(-8 + 23*t2 + 7*Power(t2,2)))) - 
            Power(s1,4)*(-22 - 103*t2 + 42*Power(t2,2) + 
               93*Power(t2,3) - 34*Power(t2,4) + 5*Power(t2,5) + 
               6*Power(s2,2)*Power(t2,2)*(4 + t2) + 
               Power(t1,2)*(16 - 38*t2 + 27*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(26 + 30*t2 + 51*Power(t2,2) + 52*Power(t2,3) + 
                  7*Power(t2,4)) - 
               s2*(-4 - 10*t2 + 87*Power(t2,2) + 42*Power(t2,3) + 
                  19*Power(t2,4) + t1*(8 - 46*t2 + 74*Power(t2,2))))) + 
         s*(s1 - t2)*(Power(s1,7)*(-1 + t2)*(2 + 3*s2 + 2*t1 + t2) - 
            Power(t2,2)*(1 - 2*t2 + Power(t2,3)) + 
            s1*(-1 + t2)*t2*(-4 + (-7 + 2*s2)*t2 + 
               (17 - 4*s2)*Power(t2,2) + (11 - 2*s2)*Power(t2,3) + 
               t1*(2 - 2*t2 + Power(t2,2))) + 
            Power(s1,2)*(3 + 3*t2 + 2*s2*Power(t2,2) + 
               (-10 + 19*s2 - 4*Power(s2,2))*Power(t2,3) + 
               (21 - 35*s2 + 5*Power(s2,2))*Power(t2,4) + 
               (-17 + 2*s2)*Power(t2,5) + 
               Power(t1,2)*(6 - t2 - 10*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-10 + (-22 + 4*s2)*t2 + (60 - 18*s2)*Power(t2,2) + 
                  3*(-5 + 6*s2)*Power(t2,3) - (3 + 2*s2)*Power(t2,4) + 
                  2*Power(t2,5))) + 
            Power(s1,6)*(5*Power(s2,2)*t2 + Power(t1,2)*(-1 + 10*t2) + 
               t1*(-8 + 11*t2 + Power(t2,2)) - 
               2*(-7 + 3*t2 + 3*Power(t2,2) + Power(t2,3)) - 
               s2*(-6 + t2 + 9*Power(t2,2) + 2*t1*(2 + 5*t2))) + 
            Power(s1,5)*(-26 + 9*t2 + 8*Power(t2,2) + 8*Power(t2,3) + 
               Power(t2,4) + Power(s2,2)*(8 - 13*t2 - 11*Power(t2,2)) + 
               Power(t1,2)*(9 - 31*t2 - 2*Power(t2,2)) - 
               t1*(-44 + 27*t2 + 37*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-25 + 13*t2 + 25*Power(t2,2) + 11*Power(t2,3) + 
                  2*t1*(-5 + 18*t2 + 7*Power(t2,2)))) + 
            Power(s1,4)*(47 - 54*t2 + 2*Power(t2,2) + 13*Power(t2,3) - 
               8*Power(t2,4) + 
               Power(s2,2)*t2*(-20 + 31*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(-1 + 15*t2 + 2*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-50 - 27*t2 + 119*Power(t2,2) + 3*Power(t2,3) + 
                  3*Power(t2,4)) - 
               s2*(2 - 89*t2 + 99*Power(t2,2) + 29*Power(t2,3) + 
                  7*Power(t2,4) + 
                  2*t1*(1 - 7*t2 + 23*Power(t2,2) + Power(t2,3)))) + 
            Power(s1,3)*(-22 - 27*t2 + 121*Power(t2,2) - 91*Power(t2,3) + 
               14*Power(t2,4) + 5*Power(t2,5) - 
               Power(s2,2)*Power(t2,2)*(-16 + 23*t2 + Power(t2,2)) + 
               Power(t1,2)*(-13 + 7*t2 + 10*Power(t2,2) - 
                  4*Power(t2,3)) + 
               t1*(46 + t2 - 75*Power(t2,2) - 13*Power(t2,3) + 
                  3*Power(t2,4) - 2*Power(t2,5)) + 
               s2*(2 - 6*t2 - 81*Power(t2,2) + 117*Power(t2,3) + 
                  6*Power(t2,4) + 2*Power(t2,5) - 
                  2*t1*(2 - 10*t2 + 11*Power(t2,2) - 8*Power(t2,3) + 
                     Power(t2,4))))))*R1q(s1))/
     (s*(-1 + s1)*s1*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + t1)*Power(s1 - t2,3)*(-1 + t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (16*(2*Power(s1 - t2,2)*(-1 + t2)*Power(t2,2)*(1 + (-2 + s1)*t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,3)*(s1 - t2)*Power(t2,2)*
          (2*Power(s1,2)*(1 + 2*t1 - t2 - 3*Power(t2,2) + Power(t2,3)) + 
            t2*(-4 - Power(t1,2)*Power(-1 + t2,2) + 5*t2 + 
               4*Power(t2,2) - 11*Power(t2,3) + 2*Power(t2,4) + 
               t1*(3 + t2 - 3*Power(t2,2) + 3*Power(t2,3))) - 
            s1*(Power(t1,2)*Power(-1 + t2,2) + 
               t1*(-3 + 15*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               t2*(1 + 2*t2 - 15*Power(t2,2) + 4*Power(t2,3)))) + 
         Power(s,2)*t2*(-(Power(s1,4)*
               (-1 + t2 - 4*s2*t2 + (11 + 2*s2)*Power(t2,2) - 
                 (13 + 2*s2)*Power(t2,3) + 2*Power(t2,4) + 
                 2*t1*(-2 + 3*t2 + Power(t2,2)))) - 
            Power(t2,2)*(6 - 4*(-2 + s2)*t2 + 
               (-35 + 9*s2)*Power(t2,2) - 2*(-5 + s2)*Power(t2,3) + 
               (28 + 6*s2)*Power(t2,4) - (20 + s2)*Power(t2,5) + 
               3*Power(t2,6) + 
               Power(t1,2)*(3 - 5*t2 + 4*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-10 + 2*(5 + s2)*t2 + (2 - 7*s2)*Power(t2,2) + 
                  3*(-1 + s2)*Power(t2,3) - (11 + 2*s2)*Power(t2,4) + 
                  4*Power(t2,5))) + 
            Power(s1,3)*(Power(t1,2)*
                (1 + 5*t2 - 6*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(2 - 3*(7 + s2)*t2 + (22 + s2)*Power(t2,2) - 
                  2*(2 + s2)*Power(t2,3) + 5*Power(t2,4)) + 
               t2*(-7 + 10*t2 + 35*Power(t2,2) - 46*Power(t2,3) + 
                  8*Power(t2,4) + 
                  s2*(1 - 12*t2 + 16*Power(t2,2) - 9*Power(t2,3)))) + 
            Power(s1,2)*(-(Power(t1,2)*
                  (-1 + 6*t2 + Power(t2,2) + 5*Power(t2,3) + 
                    Power(t2,4))) + 
               t2*(-3 + (-1 + s2)*t2 + 2*(13 + s2)*Power(t2,2) - 
                  4*(20 + 7*s2)*Power(t2,3) + 
                  (71 + 13*s2)*Power(t2,4) - 13*Power(t2,5)) + 
               t1*t2*(-10 + 62*t2 - 61*Power(t2,2) + 33*Power(t2,3) - 
                  12*Power(t2,4) + 
                  s2*(2 + t2 + 7*Power(t2,2) + 2*Power(t2,3)))) + 
            s1*t2*(Power(t1,2)*
                (-4 + 12*t2 + Power(t2,3) + 3*Power(t2,4)) + 
               t1*(2 - 6*t2 - (43 + 5*s2)*Power(t2,2) + 
                  (66 - 5*s2)*Power(t2,3) - 2*(25 + s2)*Power(t2,4) + 
                  11*Power(t2,5)) + 
               t2*(23 - 28*t2 - 37*Power(t2,2) + 90*Power(t2,3) - 
                  58*Power(t2,4) + 10*Power(t2,5) + 
                  s2*(-4 + 7*t2 + 4*Power(t2,2) + 20*Power(t2,3) - 
                     7*Power(t2,4))))) - 
         s*(s1 - t2)*(Power(s1,4)*(-1 + t2)*
             (-1 + (3 + 4*s2 - 2*t1)*t2 - 
               (7 + 2*s2 + 2*t1)*Power(t2,2) + (5 + 2*s2)*Power(t2,3)) + 
            Power(t2,2)*(Power(s2,2)*Power(t2,3)*
                (2 - 7*t2 + Power(t2,2)) - 
               Power(t1,2)*(-3 + 7*t2 - 9*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(-1 + t2,2)*
                (6 + 12*t2 - 23*Power(t2,2) - 12*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s2*t2*(-4 + 17*t2 - 17*Power(t2,2) + 14*Power(t2,3) - 
                  13*Power(t2,4) + 3*Power(t2,5)) + 
               t1*(-10 + (19 + 2*s2)*t2 - (1 + 13*s2)*Power(t2,2) + 
                  3*(-7 + 6*s2)*Power(t2,3) + (4 + s2)*Power(t2,4) + 
                  12*Power(t2,5) - 3*Power(t2,6))) + 
            Power(s1,3)*(Power(t1,2)*t2*
                (-1 + 6*t2 - 6*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(1 - (2 + s2)*t2 + (5 - 2*s2)*Power(t2,2) + 
                  (-4 + s2)*Power(t2,3) - 2*(2 + 3*s2)*Power(t2,4) + 
                  4*Power(t2,5)) + 
               t2*(Power(s2,2)*t2*(2 - t2 + 3*Power(t2,2)) - 
                  s2*Power(-1 + t2,2)*(1 - 7*t2 + 7*Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (2 + t2 - 12*Power(t2,2) + Power(t2,3)))) - 
            Power(s1,2)*(Power(t1,2)*
                (1 - 8*t2 + 16*Power(t2,2) - 15*Power(t2,3) + 
                  18*Power(t2,4)) + 
               t2*(1 - (11 + 3*s2)*t2 + 
                  (37 - s2 + 2*Power(s2,2))*Power(t2,2) + 
                  (-53 - 24*s2 + 5*Power(s2,2))*Power(t2,3) + 
                  (36 + 39*s2 + 5*Power(s2,2))*Power(t2,4) - 
                  (12 + 11*s2)*Power(t2,5) + 2*Power(t2,6)) + 
               t1*t2*(-3 + 29*t2 - 29*Power(t2,2) + 12*Power(t2,3) - 
                  14*Power(t2,4) + 5*Power(t2,5) + 
                  s2*(2 - 5*t2 + 2*Power(t2,2) - 15*Power(t2,3) - 
                     8*Power(t2,4)))) + 
            s1*t2*(Power(t1,2)*
                (4 - 15*t2 + 4*Power(t2,2) + 23*Power(t2,3) - 
                  9*Power(t2,4) + 5*Power(t2,5)) + 
               t1*(-2 + 5*t2 + (40 + 9*s2)*Power(t2,2) - 
                  (87 + 14*s2)*Power(t2,3) + (80 - 17*s2)*Power(t2,4) - 
                  2*(20 + s2)*Power(t2,5) + 4*Power(t2,6)) + 
               t2*(Power(s2,2)*Power(t2,2)*(-2 + 13*t2 + Power(t2,2)) + 
                  Power(-1 + t2,2)*
                   (-13 - 3*t2 + 41*Power(t2,2) - 6*Power(t2,3) + 
                     Power(t2,4)) + 
                  s2*(4 - 19*t2 + 11*Power(t2,2) - 22*Power(t2,3) + 
                     35*Power(t2,4) - 9*Power(t2,5))))))*R1q(t2))/
     (s*(-1 + s1)*(-1 + t1)*Power(s1 - t2,3)*Power(-1 + t2,3)*t2*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(3*Power(s,5)*t2*(-2 + t1 + t2) + 
         Power(s,4)*(t1*t2*(-27 - 3*s2 + t2) + Power(t1,2)*(-4 + 5*t2) + 
            t2*(33 + 6*s2 - 9*t2 + 4*Power(t2,2)) - 
            s1*(6 - 13*t2 - 3*s2*t2 + Power(t2,2) + t1*(-3 + 10*t2))) + 
         Power(s,3)*(Power(t1,2)*(16 - 14*t2 + 3*Power(t2,2)) + 
            t1*(-8 + (41 + 2*s2)*t2 + (-38 + 4*s2)*Power(t2,2) + 
               2*Power(t2,3)) + 
            Power(s1,2)*(4 + 6*t2 - 7*Power(t2,2) + s2*(3 + t2) + 
               t1*(-7 + 8*t2)) - 
            t2*(70 + 3*Power(s2,2)*(-2 + t2) - 22*t2 + Power(t2,3) + 
               s2*(15 - 8*t2 + 3*Power(t2,2))) - 
            s1*(-27 + 15*t2 + 3*Power(s2,2)*t2 + 13*Power(t2,2) + 
               Power(t2,3) + Power(t1,2)*(-19 + 20*t2) + 
               s2*(-6 + t1*(11 - 17*t2) + 16*t2 + 4*Power(t2,2)) - 
               2*t1*(-8 + 29*t2 + 6*Power(t2,2)))) - 
         Power(-1 + s1,2)*(4 + 2*Power(s1,3)*(1 + s2 - 2*t1)*(-1 + t2) - 
            11*t2 + 25*s2*t2 - 2*Power(s2,2)*t2 + 27*Power(t2,2) - 
            31*s2*Power(t2,2) + 5*Power(s2,2)*Power(t2,2) - 
            21*Power(t2,3) + 7*s2*Power(t2,3) + 
            Power(s2,2)*Power(t2,3) + Power(t2,4) - s2*Power(t2,4) + 
            Power(t1,2)*(4 + t2 - Power(t2,2)) - 
            t1*(8 + (6 + 7*s2)*t2 - 2*(-1 + s2)*Power(t2,2) + 
               (-16 + 3*s2)*Power(t2,3)) + 
            Power(s1,2)*(-2 + Power(t1,2)*(9 - 5*t2) + 8*t2 - 
               6*Power(t2,2) + Power(s2,2)*(1 + 3*t2) + 
               s2*(1 - 8*t1 + 2*t2 - 3*Power(t2,2)) + 
               t1*(-1 - 8*t2 + 9*Power(t2,2))) + 
            s1*(-1 - 13*t2 + 11*Power(t2,2) + 3*Power(t2,3) + 
               Power(s2,2)*(2 - 6*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(-13 + 4*t2 + Power(t2,2)) - 
               t1*(-30 + 21*t2 + 4*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(-25 + 30*t2 - 7*Power(t2,2) + 2*Power(t2,3) + 
                  t1*(7 + 6*t2 + 3*Power(t2,2))))) + 
         Power(s,2)*(-4 + 26*t2 + 3*s2*t2 - 10*Power(s2,2)*t2 - 
            76*Power(t2,2) - 15*s2*Power(t2,2) + 
            9*Power(s2,2)*Power(t2,2) - 15*Power(t2,3) - 
            s2*Power(t2,3) - Power(s2,2)*Power(t2,3) + Power(t2,4) + 
            s2*Power(t2,4) + Power(t1,2)*(-24 + 12*t2 - 5*Power(t2,2)) + 
            Power(s1,3)*(8 + t1 + s2*(4 - 13*t2) - 21*t2 + 6*t1*t2 + 
               5*Power(t2,2)) + 
            t1*(24 + (-7 + 12*s2)*t2 + (79 - 10*s2)*Power(t2,2) + 
               (-16 + 3*s2)*Power(t2,3)) + 
            Power(s1,2)*(12 + Power(s2,2)*(-7 + t2) - 39*t2 + 
               47*Power(t2,2) - 4*Power(t2,3) + 
               5*Power(t1,2)*(-7 + 6*t2) + 
               t1*(17 - 23*t2 - 36*Power(t2,2)) + 
               s2*(1 + t1*(34 - 25*t2) + 18*t2 + 15*Power(t2,2))) + 
            s1*(-35 + 34*t2 - 12*Power(t2,2) - 9*Power(t2,3) + 
               2*Power(t2,4) + 
               Power(t1,2)*(-17 + 14*t2 - 7*Power(t2,2)) + 
               Power(s2,2)*(6 - 2*t2 + 4*Power(t2,2)) + 
               t1*(13 + 51*t2 + 62*Power(t2,2) + 5*Power(t2,3)) - 
               s2*(17 + 85*t2 - 11*Power(t2,2) + 2*Power(t2,3) + 
                  t1*(-15 + 6*t2 + 7*Power(t2,2))))) + 
         s*(8 + 6*t2 + 31*s2*t2 + 2*Power(s2,2)*t2 + 35*Power(t2,2) - 
            8*s2*Power(t2,2) - Power(s2,2)*Power(t2,2) - 34*Power(t2,3) + 
            15*s2*Power(t2,3) + 2*Power(s2,2)*Power(t2,3) + Power(t2,4) - 
            2*s2*Power(t2,4) + Power(t1,2)*(16 - 2*t2 + Power(t2,2)) - 
            2*t1*(12 + (8 + 9*s2)*t2 + (22 - 4*s2)*Power(t2,2) + 
               3*(-5 + s2)*Power(t2,3)) + 
            Power(s1,4)*(-8 + t1*(7 - 11*t2) + 10*t2 + s2*(-9 + 11*t2)) - 
            Power(s1,2)*(-16 + 80*t2 - 77*Power(t2,2) - 10*Power(t2,3) + 
               Power(t2,4) + Power(t1,2)*(22 - 14*t2 - 5*Power(t2,2)) + 
               Power(s2,2)*(-4 + 14*t2 + 5*Power(t2,2)) + 
               t1*(-73 + 93*t2 + 54*Power(t2,2) + 12*Power(t2,3)) + 
               s2*(99 - 154*t2 + 4*Power(t2,2) - 7*Power(t2,3) - 
                  6*t1*(4 - t2 + Power(t2,2)))) + 
            Power(s1,3)*(-1 + Power(t1,2)*(29 - 20*t2) + 29*t2 - 
               35*Power(t2,2) + Power(t2,3) + Power(s2,2)*(4 + 5*t2) + 
               t1*(-2 - 8*t2 + 32*Power(t2,2)) + 
               s2*(t1*(-31 + 11*t2) - 2*(-9 + 9*t2 + 7*Power(t2,2)))) + 
            s1*(-27 - 5*t2 + 43*Power(t2,2) - 49*Power(t2,3) + 
               4*Power(t2,4) + Power(t1,2)*(-23 + 8*t2 - 6*Power(t2,2)) + 
               Power(s2,2)*(-4 + 3*t2 + 2*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(46 - 28*t2 + 78*Power(t2,2) + 26*Power(t2,3)) + 
               s2*(t1*(3 + 9*t2 + 6*Power(t2,2) - 6*Power(t2,3)) - 
                  2*(-1 - 13*t2 + 61*Power(t2,2) - 7*Power(t2,3) + 
                     Power(t2,4))))))*R2q(s))/
     (s*(-1 + s1)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + t1)*(-1 + t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(Power(s,7)*t2*(t1*(2 + t2) + t2*(-2 + 3*t2)) + 
         Power(s,6)*(Power(t1,2)*(-2 - t2 + Power(t2,2)) + 
            t1*t2*(-12 + s2 - 7*t2 - s2*t2 + 2*Power(t2,2)) + 
            t2*(2 + (21 + s2)*t2 - 2*(7 + s2)*Power(t2,2) + 
               7*Power(t2,3)) + 
            s1*(t2*(-6 + 2*s2 + 11*t2 + 3*s2*t2 - 11*Power(t2,2)) + 
               t1*(2 - 7*t2 - 6*Power(t2,2)))) + 
         Power(s,5)*(Power(t1,2)*(12 + t2 - 6*Power(t2,2)) + 
            t1*(-4 + (10 - 3*s2)*t2 + (20 + 3*s2)*Power(t2,2) + 
               (-18 + s2)*Power(t2,3) + 5*Power(t2,4)) + 
            t2*(-14 - 90*t2 - Power(s2,2)*(-3 + t2)*t2 + 
               35*Power(t2,2) - 26*Power(t2,3) + 3*Power(t2,4) + 
               s2*(5 + 2*t2 + 9*Power(t2,2) - 7*Power(t2,3))) + 
            Power(s1,2)*(3*t1*(-2 + t2 + 5*Power(t2,2)) - 
               s2*(-2 + t2 + 11*Power(t2,2)) + 
               2*(-2 + 7*t2 - 7*Power(t2,2) + 7*Power(t2,3))) + 
            s1*(2 + (45 - 11*s2 + Power(s2,2))*t2 - 
               (68 + 5*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (32 + 13*s2)*Power(t2,3) - 21*Power(t2,4) + 
               Power(t1,2)*(12 + 7*t2 - 6*Power(t2,2)) + 
               t1*(-6 + 19*t2 + 31*Power(t2,2) - 5*Power(t2,3) + 
                  s2*(-3 - 11*t2 + 9*Power(t2,2))))) + 
         Power(s,4)*(-2 + 21*t2 - 18*s2*t2 + 141*Power(t2,2) + 
            16*s2*Power(t2,2) - 16*Power(s2,2)*Power(t2,2) - 
            107*Power(t2,3) - 4*s2*Power(t2,3) + 
            9*Power(s2,2)*Power(t2,3) + 19*Power(t2,4) + 
            28*s2*Power(t2,4) - 10*Power(t2,5) - 3*s2*Power(t2,5) - 
            Power(t1,2)*(30 - 10*t2 - 9*Power(t2,2) + Power(t2,3)) + 
            t1*(20 + 46*t2 + (-57 + s2)*Power(t2,2) + 
               (57 - 8*s2)*Power(t2,3) - 27*Power(t2,4) + 2*Power(t2,5)) \
+ 2*Power(s1,3)*(2 + t1 + 2*t2 + 10*t1*t2 - 3*Power(t2,2) - 
               10*t1*Power(t2,2) - 3*Power(t2,3) + 
               s2*(-1 - 5*t2 + 7*Power(t2,2))) + 
            Power(s1,2)*(22 - 79*t2 + 28*Power(t2,2) - 4*Power(t2,3) + 
               21*Power(t2,4) + t1*(-7 + 12*t2 - 46*Power(t2,2)) + 
               3*Power(t1,2)*(-10 - 7*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(-1 - 13*t2 + 11*Power(t2,2)) + 
               s2*(-6 + 40*t2 + 5*Power(t2,2) - 26*Power(t2,3) + 
                  t1*(13 + 45*t2 - 27*Power(t2,2)))) + 
            s1*(-8 - 128*t2 + 231*Power(t2,2) - 68*Power(t2,3) + 
               36*Power(t2,4) - 5*Power(t2,5) + 
               Power(s2,2)*t2*(9 + 4*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(-40 + 3*t2 + 20*Power(t2,2) + Power(t2,3)) + 
               t1*(2 + 105*t2 - 54*Power(t2,2) + 46*Power(t2,3) - 
                  13*Power(t2,4)) + 
               s2*(1 - 16*t2 - 108*Power(t2,2) - 23*Power(t2,3) + 
                  24*Power(t2,4) + 
                  t1*(14 + 10*t2 - 25*Power(t2,2) + Power(t2,3))))) - 
         Power(-1 + s1,3)*(-2 + 4*t1 - 2*Power(t1,2) + 
            2*Power(s1,4)*(s2 - t1)*(-1 + t2) - 10*t2 + 2*s2*t2 + 
            6*t1*t2 - 2*s2*t1*t2 + 4*Power(t1,2)*t2 + 28*Power(t2,2) - 
            23*s2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) - 
            7*t1*Power(t2,2) + 3*s2*t1*Power(t2,2) - 
            5*Power(t1,2)*Power(t2,2) - 18*Power(t2,3) + 
            22*s2*Power(t2,3) - 7*Power(s2,2)*Power(t2,3) - 
            2*t1*Power(t2,3) + 6*s2*t1*Power(t2,3) - 
            Power(t1,2)*Power(t2,3) + 2*Power(t2,4) - s2*Power(t2,4) - 
            Power(s2,2)*Power(t2,4) - t1*Power(t2,4) + 
            s2*t1*Power(t2,4) + 
            Power(s1,3)*(2*Power(-1 + t2,2) + Power(s2,2)*(-1 + 5*t2) + 
               Power(t1,2)*(2 + 3*t2 - Power(t2,2)) + 
               t1*(-3 + 2*t2 + Power(t2,2)) + 
               s2*(1 + 2*t2 - 3*Power(t2,2) + 
                  t1*(-1 - 8*t2 + Power(t2,2)))) + 
            Power(s1,2)*(-(Power(-1 + t2,2)*(1 + t2)) + 
               Power(s2,2)*(4 - 5*t2 - 11*Power(t2,2)) - 
               Power(t1,2)*(2 + 10*t2 - Power(t2,2) + Power(t2,3)) + 
               t1*(19 - 14*t2 - 9*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-15 + 8*t2 + 9*Power(t2,2) - 2*Power(t2,3) + 
                  t1*(-5 + 20*t2 + 9*Power(t2,2)))) + 
            s1*(-(Power(-1 + t2,2)*(-16 - t2 + 3*Power(t2,2))) + 
               Power(s2,2)*t2*(-8 + 13*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(2 + 3*t2 + 5*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-18 - 4*t2 + 27*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*(2 - 38*t2 + 31*Power(t2,2) + 8*Power(t2,3) - 
                  3*Power(t2,4) + 
                  t1*(-2 - 2*t2 + 25*Power(t2,2) + 2*Power(t2,3) + 
                     Power(t2,4))))) + 
         Power(s,2)*(-12 + 40*t1 - 30*Power(t1,2) - 49*t2 - 8*s2*t2 + 
            104*t1*t2 - 15*s2*t1*t2 + 35*Power(t1,2)*t2 - 
            34*Power(t2,2) + 42*s2*Power(t2,2) - 
            12*Power(s2,2)*Power(t2,2) - 113*t1*Power(t2,2) + 
            21*s2*t1*Power(t2,2) - 21*Power(t1,2)*Power(t2,2) + 
            13*Power(t2,3) - 124*s2*Power(t2,3) + 
            30*Power(s2,2)*Power(t2,3) + 91*t1*Power(t2,3) + 
            2*s2*t1*Power(t2,3) - 6*Power(t1,2)*Power(t2,3) + 
            6*Power(t2,4) + 27*s2*Power(t2,4) - 
            15*Power(s2,2)*Power(t2,4) - 28*t1*Power(t2,4) + 
            7*s2*t1*Power(t2,4) + 26*Power(t2,5) - 15*s2*Power(t2,5) + 
            10*t1*Power(t2,5) + 
            Power(s1,5)*(-12 + 26*t2 - 13*Power(t2,2) + Power(t2,3) - 
               s2*(-8 + 14*t2 + Power(t2,2)) - 
               3*t1*(6 - 11*t2 + 2*Power(t2,2))) + 
            Power(s1,4)*(8 + 21*t2 - 69*Power(t2,2) + 26*Power(t2,3) + 
               Power(s2,2)*(2 - 38*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(-30 - 31*t2 + 15*Power(t2,2)) - 
               2*t1*(5 + 14*t2 - Power(t2,2) + 5*Power(t2,3)) - 
               s2*(27 - 80*t2 + Power(t2,2) + 4*Power(t2,3) + 
                  t1*(-14 - 86*t2 + 27*Power(t2,2)))) + 
            Power(s1,3)*(5 - 87*t2 + 120*Power(t2,2) + 25*Power(t2,3) - 
               16*Power(t2,4) + Power(t2,5) + 
               Power(s2,2)*(-11 + 17*t2 + 65*Power(t2,2) - 
                  17*Power(t2,3)) + 
               Power(t1,2)*(58*t2 + 6*Power(t2,3)) + 
               t1*(-84 + 209*t2 + 52*Power(t2,2) + 2*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(-17 + 78*t2 - 294*Power(t2,2) + 33*Power(t2,3) + 
                  8*Power(t2,4) + 
                  t1*(16 - 84*t2 - 57*Power(t2,2) + 11*Power(t2,3)))) + 
            Power(s1,2)*(33 - 90*t2 + 165*Power(t2,2) - 
               245*Power(t2,3) + 19*Power(t2,4) + 6*Power(t2,5) - 
               2*Power(t1,2)*
                (2 - 14*t2 + 17*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-23 + 58*t2 - 25*Power(t2,2) - 
                  42*Power(t2,3) + 8*Power(t2,4)) - 
               t1*(82 - 98*t2 + 167*Power(t2,2) - 21*Power(t2,3) + 
                  22*Power(t2,4) + 2*Power(t2,5)) + 
               s2*(63 + 70*t2 - 191*Power(t2,2) + 254*Power(t2,3) - 
                  5*Power(t2,4) - Power(t2,5) + 
                  t1*(10 - 49*t2 + 48*Power(t2,2) + 20*Power(t2,3) - 
                     5*Power(t2,4)))) + 
            s1*(34 + 27*t2 + 71*Power(t2,2) - 44*Power(t2,3) + 
               47*Power(t2,4) - 9*Power(t2,5) + 
               2*Power(t1,2)*t2*(19 - 12*t2 + Power(t2,2)) + 
               Power(s2,2)*t2*
                (43 - 101*t2 + 45*Power(t2,2) + 7*Power(t2,3)) + 
               t1*(-102 + 144*t2 - 152*Power(t2,2) + 24*Power(t2,3) - 
                  15*Power(t2,4) + 8*Power(t2,5)) - 
               s2*(3 + 134*t2 - 173*Power(t2,2) - 49*Power(t2,3) + 
                  38*Power(t2,4) + 8*Power(t2,5) + 
                  t1*(-24 + 34*t2 - 15*Power(t2,2) + Power(t2,3) + 
                     2*Power(t2,4))))) + 
         Power(s,3)*(8 - 40*t1 + 40*Power(t1,2) + 11*t2 + 22*s2*t2 - 
            114*t1*t2 + 10*s2*t1*t2 - 30*Power(t1,2)*t2 - 
            47*Power(t2,2) - 63*s2*Power(t2,2) + 
            26*Power(s2,2)*Power(t2,2) + 113*t1*Power(t2,2) - 
            14*s2*t1*Power(t2,2) + 4*Power(t1,2)*Power(t2,2) + 
            152*Power(t2,3) + 50*s2*Power(t2,3) - 
            30*Power(s2,2)*Power(t2,3) - 101*t1*Power(t2,3) + 
            12*s2*t1*Power(t2,3) + 4*Power(t1,2)*Power(t2,3) + 
            13*Power(t2,4) - 25*s2*Power(t2,4) + 
            3*Power(s2,2)*Power(t2,4) + 42*t1*Power(t2,4) - 
            3*s2*t1*Power(t2,4) + 2*Power(t2,5) + 13*s2*Power(t2,5) - 
            10*t1*Power(t2,5) - 
            Power(s1,4)*(-8 + 32*t2 - 22*Power(t2,2) + Power(t2,3) + 
               t1*(-12 + 40*t2 - 15*Power(t2,2)) + 
               2*s2*(2 - 9*t2 + 3*Power(t2,2))) + 
            Power(s1,3)*(-16 + 13*t2 + Power(s2,2)*(34 - 15*t2)*t2 + 
               74*Power(t2,2) - 36*Power(t2,3) - 7*Power(t2,4) + 
               Power(t1,2)*(40 + 34*t2 - 20*Power(t2,2)) + 
               t1*(32 - 16*t2 + 23*Power(t2,2) + 10*Power(t2,3)) + 
               s2*(15 - 78*t2 + 2*Power(t2,2) + 20*Power(t2,3) + 
                  t1*(-20 - 87*t2 + 38*Power(t2,2)))) + 
            Power(s1,2)*(-49 + 152*t2 - 218*Power(t2,2) + 
               9*Power(t2,3) + 5*Power(t2,4) + Power(t2,5) - 
               2*Power(t1,2)*
                (-20 + 13*t2 + 10*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s2,2)*(11 - 9*t2 - 40*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(80 - 249*t2 + 11*Power(t2,2) - 36*Power(t2,3) + 
                  9*Power(t2,4)) - 
               s2*(18 + 96*t2 - 292*Power(t2,2) + Power(t2,3) + 
                  26*Power(t2,4) + 
                  t1*(30 - 13*t2 - 57*Power(t2,2) + 9*Power(t2,3)))) + 
            s1*(2 + 162*t2 - 257*Power(t2,2) + 212*Power(t2,3) - 
               26*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,2)*(40 - 42*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*t2*
                (-41 + 43*t2 + 7*Power(t2,2) - 4*Power(t2,3)) + 
               t1*(40 - 261*t2 + 162*Power(t2,2) - 77*Power(t2,3) + 
                  45*Power(t2,4) - 2*Power(t2,5)) + 
               s2*(-1 + 108*t2 + 115*Power(t2,2) - 141*Power(t2,3) - 
                  41*Power(t2,4) + 5*Power(t2,5) + 
                  t1*(-26 + 28*t2 - 5*Power(t2,2) + Power(t2,3) + 
                     3*Power(t2,4))))) + 
         s*(8 - 20*t1 + 12*Power(t1,2) + 39*t2 - 3*s2*t2 - 42*t1*t2 + 
            9*s2*t1*t2 - 19*Power(t1,2)*t2 - 17*Power(t2,2) + 
            25*s2*Power(t2,2) - 5*Power(s2,2)*Power(t2,2) + 
            50*t1*Power(t2,2) - 13*s2*t1*Power(t2,2) + 
            18*Power(t1,2)*Power(t2,2) - 64*Power(t2,3) + 
            49*s2*Power(t2,3) - Power(s2,2)*Power(t2,3) - 
            29*t1*Power(t2,3) - 13*s2*t1*Power(t2,3) + 
            4*Power(t1,2)*Power(t2,3) + 31*Power(t2,4) - 
            38*s2*Power(t2,4) + 13*Power(s2,2)*Power(t2,4) + 
            9*t1*Power(t2,4) - 5*s2*t1*Power(t2,4) + 3*Power(t2,5) + 
            s2*Power(t2,5) - 2*t1*Power(t2,5) + 
            Power(s1,6)*(t1*(10 - 13*t2 + Power(t2,2)) + 
               2*(2 - 3*t2 + Power(t2,2)) + s2*(-6 + 7*t2 + Power(t2,2))) \
+ Power(s1,3)*(-28 - 3*t2 - 29*Power(t2,2) + 72*Power(t2,3) - 
               9*Power(t2,4) - 3*Power(t2,5) + 
               Power(s2,2)*t2*
                (7 + 42*t2 + 29*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,2)*(8 + 30*t2 + 20*Power(t2,2) + 
                  8*Power(t2,3)) - 
               s2*(-58 + (20 + 57*t1)*t2 + (127 + 69*t1)*Power(t2,2) + 
                  5*(23 + 3*t1)*Power(t2,3) - (13 + t1)*Power(t2,4) + 
                  Power(t2,5)) + 
               t1*(-68 + 109*t2 + 155*Power(t2,2) - 11*Power(t2,3) + 
                  5*Power(t2,4) + 2*Power(t2,5))) - 
            Power(s1,5)*(6 + 6*t2 - 16*Power(t2,2) + 4*Power(t2,3) + 
               Power(s2,2)*(2 - 21*t2 + 2*Power(t2,2)) + 
               3*Power(t1,2)*(-4 - 5*t2 + 2*Power(t2,2)) + 
               t1*(18 - 33*t2 + 2*Power(t2,2) - 3*Power(t2,3)) + 
               s2*(-25 + 35*t2 + 5*Power(t2,2) + Power(t2,3) + 
                  t1*(5 + 42*t2 - 9*Power(t2,2)))) + 
            Power(s1,4)*(5 + 34*t2 - 42*Power(t2,2) + 2*Power(t2,3) + 
               Power(t2,4) - Power(t1,2)*
                (20 + 55*t2 - 10*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(9 - 33*t2 - 43*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(72 - 117*t2 - 41*Power(t2,2) + 10*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*(-66 + 29*t2 + 134*Power(t2,2) - 20*Power(t2,3) + 
                  Power(t2,4) + 
                  t1*(-2 + 108*t2 + 28*Power(t2,2) - 4*Power(t2,3)))) + 
            Power(s1,2)*(85 - 23*t2 - 43*Power(t2,2) + 10*Power(t2,3) - 
               42*Power(t2,4) + 13*Power(t2,5) + 
               Power(t1,2)*(8 + 26*t2 - 28*Power(t2,2)) + 
               Power(s2,2)*(9 - 55*t2 + 48*Power(t2,2) - 
                  21*Power(t2,3) - 7*Power(t2,4)) - 
               t1*(70 - 8*t2 + 134*Power(t2,2) + 33*Power(t2,3) + 
                  3*Power(t2,4) + 6*Power(t2,5)) + 
               s2*(-48 + 131*t2 + 28*Power(t2,2) + 79*Power(t2,3) + 
                  49*Power(t2,4) - Power(t2,5) + 
                  t1*(18 - 17*t2 + 5*Power(t2,2) + 13*Power(t2,3) + 
                     Power(t2,4)))) + 
            s1*(-52 - 99*t2 + 209*Power(t2,2) - 80*Power(t2,3) + 
               35*Power(t2,4) - 13*Power(t2,5) - 
               Power(t1,2)*(20 - 3*t2 + 14*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-4 + 56*t2 - 77*Power(t2,2) + 14*Power(t2,3)) + 
               t1*(94 + 22*t2 - 29*Power(t2,2) + 60*Power(t2,3) - 
                  9*Power(t2,4) + 6*Power(t2,5)) + 
               s2*(5 + 19*t2 - 248*Power(t2,2) + 136*Power(t2,3) - 
                  57*Power(t2,4) + Power(t2,5) + 
                  t1*(-11 - t2 + 40*Power(t2,2) + 19*Power(t2,3) + 
                     3*Power(t2,4))))))*T2q(s1,s))/
     (s*(-1 + s1)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + t1)*(-1 + t2)*Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(Power(s,4)*(s1 - t2)*t2*
          (Power(s1,2)*(t1*(-2 + t2) - t2*(2 + t2)) + 
            2*s1*t2*(2 + Power(t1,2) + 3*t2 + Power(t2,2) - 
               t1*(1 + 2*t2)) + 
            t2*(4 + 2*Power(t1,2) + 2*t2 - 2*Power(t2,2) - Power(t2,3) + 
               t1*(-6 - 4*t2 + Power(t2,2)))) + 
         Power(s,3)*(Power(s1,2)*t2*
             (8 + 2*(-4 + s2)*t2 - 3*(7 + 5*s2)*Power(t2,2) - 
               3*(4 + 3*s2)*Power(t2,3) + 9*Power(t2,4) + 
               Power(t1,2)*(-2 - 23*t2 + 7*Power(t2,2)) + 
               t1*(-12 + (18 + s2)*t2 + 3*(11 + s2)*Power(t2,2) - 
                  15*Power(t2,3))) + 
            Power(t2,2)*(4 + (34 - 4*s2)*t2 + 2*(2 + s2)*Power(t2,2) - 
               (19 + 3*s2)*Power(t2,3) - (7 + 2*s2)*Power(t2,4) + 
               Power(t2,5) - 
               Power(t1,2)*(-4 - 4*t2 + 5*Power(t2,2) + Power(t2,3)) + 
               t1*(-8 + 4*(-10 + s2)*t2 - (-4 + s2)*Power(t2,2) + 
                  (18 + s2)*Power(t2,3) + 2*Power(t2,4))) - 
            Power(s1,4)*(t1*(2 - 5*t2 + 3*Power(t2,2)) + 
               t2*(2 + t2 - 2*Power(t2,2) + s2*(2 + t2))) + 
            Power(s1,3)*(Power(t1,2)*(2 + 5*t2 - 7*Power(t2,2)) + 
               t2*(6 + (17 + 7*s2)*t2 + (4 + 5*s2)*Power(t2,2) - 
                  7*Power(t2,3)) + 
               t1*t2*(s2*(-1 + t2) + t2*(-25 + 13*t2))) + 
            s1*Power(t2,2)*(-30 - 14*t2 + 21*Power(t2,2) + 
               16*Power(t2,3) - 5*Power(t2,4) + 
               Power(t1,2)*(8 + 11*t2 - 3*Power(t2,2)) + 
               t1*(28 + 4*t2 - 23*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(4 - 4*t2 + 13*Power(t2,2) + 7*Power(t2,3) + 
                  t1*(-4 + t2 - 5*Power(t2,2))))) - 
         Power(s,2)*(s1 - t2)*
          (Power(s1,4)*(t1*(-2 + 2*t2 - 3*Power(t2,2)) + 
               s2*(2 + t2 - 2*Power(t2,2)) + t2*(2 - 5*t2 + Power(t2,2))\
) + Power(s1,2)*(-4 + (-10 + s2)*t2 + 
               (-11 + 51*s2 - 3*Power(s2,2))*Power(t2,2) + 
               (17 + 16*s2 + 3*Power(s2,2))*Power(t2,3) - 
               (30 + 13*s2)*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,2)*(4 + 10*t2 - 29*Power(t2,2)) + 
               t1*(2 + 2*(-28 + 5*s2)*t2 - (19 + 2*s2)*Power(t2,2) + 
                  (28 + 3*s2)*Power(t2,3) - 2*Power(t2,4))) + 
            t2*(-8 + (-52 + 8*s2)*t2 + (10 - 3*s2)*Power(t2,2) + 
               (21 + 15*s2 - Power(s2,2))*Power(t2,3) + 
               (-11 + 5*s2 + Power(s2,2))*Power(t2,4) - 
               (11 + s2)*Power(t2,5) + Power(t2,6) + 
               Power(t1,2)*(-8 + 4*t2 + 6*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(16 + (50 - 8*s2)*t2 + 8*(-7 + s2)*Power(t2,2) - 
                  (21 + 2*s2)*Power(t2,3) - (-10 + s2)*Power(t2,4) + 
                  3*Power(t2,5))) + 
            Power(s1,3)*(-2 + (-13 - 13*s2 + Power(s2,2))*t2 - 
               (-12 + 13*s2 + Power(s2,2))*Power(t2,2) + 
               (17 + 9*s2)*Power(t2,3) - 2*Power(t2,4) + 
               Power(t1,2)*(4 + 12*t2 - 9*Power(t2,2)) + 
               t1*(6 + 32*t2 - 32*Power(t2,2) + 7*Power(t2,3) + 
                  s2*(-3 - 8*t2 + 4*Power(t2,2)))) + 
            s1*t2*(36 + 54*t2 - 9*Power(t2,2) - 
               3*Power(s2,2)*(-1 + t2)*Power(t2,2) - 32*Power(t2,3) + 
               29*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,2)*(-28 + 32*t2 + 8*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(-12 + 2*t2 + 34*Power(t2,2) + 16*Power(t2,3) - 
                  5*Power(t2,4)) + 
               s2*(-8 + 2*t2 - 55*Power(t2,2) - 9*Power(t2,3) + 
                  7*Power(t2,4) + 
                  t1*(8 - 15*t2 + 12*Power(t2,2) - 6*Power(t2,3))))) - 
         s*Power(s1 - t2,2)*(-4 + 8*t1 - 4*Power(t1,2) - 24*t2 + 
            4*s2*t2 + 20*t1*t2 - 4*s2*t1*t2 + 4*Power(t1,2)*t2 + 
            3*Power(t2,2) + s2*Power(t2,2) - 34*t1*Power(t2,2) + 
            5*s2*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) + 
            35*Power(t2,3) - 20*s2*Power(t2,3) + 
            3*Power(s2,2)*Power(t2,3) + 19*t1*Power(t2,3) - 
            2*s2*t1*Power(t2,3) - 5*Power(t1,2)*Power(t2,3) - 
            8*Power(t2,4) - 13*s2*Power(t2,4) + 
            2*Power(s2,2)*Power(t2,4) + 21*t1*Power(t2,4) - 
            s2*t1*Power(t2,4) + Power(t1,2)*Power(t2,4) - 
            5*Power(t2,5) - 7*s2*Power(t2,5) + 2*t1*Power(t2,5) + 
            3*Power(t2,6) + s2*Power(t2,6) - 2*t1*Power(t2,6) + 
            Power(s1,4)*(2*(-1 + t2)*t2 + s2*(2 - 5*t2 + Power(t2,2)) + 
               t1*(-2 + 3*t2 + Power(t2,2))) + 
            Power(s1,3)*(Power(s2,2)*(-1 - 6*t2 + 2*Power(t2,2)) + 
               t1*(11 - 28*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-4 - 10*t2 + 5*Power(t2,2)) - 
               2*(3 - 9*t2 + 4*Power(t2,2) + 2*Power(t2,3)) - 
               2*s2*(1 + 3*t2 - 11*Power(t2,2) + Power(t2,3) + 
                  t1*(-2 - 7*t2 + 2*Power(t2,2)))) + 
            Power(s1,2)*(-8 - 11*t2 + 29*Power(t2,2) - 11*Power(t2,3) + 
               Power(t2,4) + 
               Power(s2,2)*t2*(5 + 14*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(4 + 3*t2 + 4*Power(t2,2) + 4*Power(t2,3)) - 
               t1*(26 - 55*t2 - 27*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(-1 + 6*t2 - 29*Power(t2,2) - 30*Power(t2,3) + 
                  2*Power(t2,4) + 
                  t1*(7 - 32*t2 - 7*Power(t2,2) + 2*Power(t2,3)))) + 
            s1*(12 + 53*t2 - 50*Power(t2,2) - 51*Power(t2,3) + 
               38*Power(t2,4) - 2*Power(t2,5) + 
               Power(s2,2)*Power(t2,2)*(-7 - 10*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-16 + 43*t2 - 26*Power(t2,2) - 
                  7*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(4 - 36*t2 - 21*Power(t2,2) + 6*Power(t2,3) - 
                  31*Power(t2,4) + 2*Power(t2,5)) + 
               2*s2*(-2 + 8*Power(t2,2) + 23*Power(t2,3) + 
                  10*Power(t2,4) - Power(t2,5) + 
                  t1*(2 - 6*t2 + 15*Power(t2,2) - 3*Power(t2,3) + 
                     Power(t2,4))))) - 
         Power(s1 - t2,3)*(-2 + 4*t1 - 2*Power(t1,2) + 
            2*Power(s1,4)*(s2 - t1)*(-1 + t2) - 10*t2 + 2*s2*t2 + 
            6*t1*t2 - 2*s2*t1*t2 + 4*Power(t1,2)*t2 + 28*Power(t2,2) - 
            23*s2*Power(t2,2) + 4*Power(s2,2)*Power(t2,2) - 
            7*t1*Power(t2,2) + 3*s2*t1*Power(t2,2) - 
            5*Power(t1,2)*Power(t2,2) - 18*Power(t2,3) + 
            22*s2*Power(t2,3) - 7*Power(s2,2)*Power(t2,3) - 
            2*t1*Power(t2,3) + 6*s2*t1*Power(t2,3) - 
            Power(t1,2)*Power(t2,3) + 2*Power(t2,4) - s2*Power(t2,4) - 
            Power(s2,2)*Power(t2,4) - t1*Power(t2,4) + 
            s2*t1*Power(t2,4) + 
            Power(s1,3)*(2*Power(-1 + t2,2) + Power(s2,2)*(-1 + 5*t2) + 
               Power(t1,2)*(2 + 3*t2 - Power(t2,2)) + 
               t1*(-3 + 2*t2 + Power(t2,2)) + 
               s2*(1 + 2*t2 - 3*Power(t2,2) + 
                  t1*(-1 - 8*t2 + Power(t2,2)))) + 
            Power(s1,2)*(-(Power(-1 + t2,2)*(1 + t2)) + 
               Power(s2,2)*(4 - 5*t2 - 11*Power(t2,2)) - 
               Power(t1,2)*(2 + 10*t2 - Power(t2,2) + Power(t2,3)) + 
               t1*(19 - 14*t2 - 9*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-15 + 8*t2 + 9*Power(t2,2) - 2*Power(t2,3) + 
                  t1*(-5 + 20*t2 + 9*Power(t2,2)))) + 
            s1*(-(Power(-1 + t2,2)*(-16 - t2 + 3*Power(t2,2))) + 
               Power(s2,2)*t2*(-8 + 13*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(2 + 3*t2 + 5*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-18 - 4*t2 + 27*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*(2 - 38*t2 + 31*Power(t2,2) + 8*Power(t2,3) - 
                  3*Power(t2,4) + 
                  t1*(-2 - 2*t2 + 25*Power(t2,2) + 2*Power(t2,3) + 
                     Power(t2,4))))))*T3q(t2,s1))/
     (s*(-1 + s1)*(-1 + t1)*Power(s1 - t2,2)*(-1 + t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(Power(s,4)*t2*(t1*(2 + t2 + 6*Power(t2,2) - Power(t2,3)) + 
            t2*(10 - 25*t2 + 6*Power(t2,2) + Power(t2,3))) + 
         Power(s,3)*(Power(t1,2)*
             (-2 + 3*t2 + 5*Power(t2,2) + 3*Power(t2,3) - Power(t2,4)) + 
            t2*(2 - (37 + 3*s2)*t2 + (85 + 24*s2)*Power(t2,2) - 
               3*(18 + s2)*Power(t2,3) + (3 - 2*s2)*Power(t2,4) + 
               Power(t2,5)) + 
            t1*t2*(s2*(1 - 7*t2 - 3*Power(t2,2) + Power(t2,3)) + 
               2*(-3 - 2*t2 - 10*Power(t2,2) + 6*Power(t2,3) + 
                  Power(t2,4))) + 
            s1*(t1*(2 + 3*t2 + 5*Power(t2,2) - 21*Power(t2,3) + 
                  3*Power(t2,4)) + 
               t2*(18 - 75*t2 + 66*Power(t2,2) - 7*Power(t2,3) - 
                  2*Power(t2,4) + 
                  s2*(2 - 5*t2 + 10*Power(t2,2) + Power(t2,3))))) + 
         s*(-1 + t2)*(2 - 8*t1 + 6*Power(t1,2) + 7*t2 + 3*s2*t2 - 
            16*t1*t2 + 3*s2*t1*t2 - 13*Power(t1,2)*t2 - 22*Power(t2,2) + 
            53*s2*Power(t2,2) - 13*Power(s2,2)*Power(t2,2) + 
            17*t1*Power(t2,2) + 5*s2*t1*Power(t2,2) + 
            10*Power(t1,2)*Power(t2,2) + 19*Power(t2,3) - 
            91*s2*Power(t2,3) + 27*Power(s2,2)*Power(t2,3) + 
            22*t1*Power(t2,3) - 37*s2*t1*Power(t2,3) + 
            12*Power(t1,2)*Power(t2,3) - 13*Power(t2,4) + 
            32*s2*Power(t2,4) + 2*Power(s2,2)*Power(t2,4) - 
            11*t1*Power(t2,4) - 3*s2*t1*Power(t2,4) + 
            Power(t1,2)*Power(t2,4) + 10*Power(t2,5) + 
            4*s2*Power(t2,5) - 6*t1*Power(t2,5) - 3*Power(t2,6) - 
            s2*Power(t2,6) + 2*t1*Power(t2,6) + 
            Power(s1,3)*(-1 + t2)*
             (-12 + (10 + 13*s2)*t2 + (2 + s2)*Power(t2,2) + 
               t1*(-4 - 11*t2 + Power(t2,2))) + 
            Power(s1,2)*(-4*Power(-1 + t2,2)*
                (-2 + 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(2 + 6*t2 + 11*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(s2,2)*(1 + t2 + 12*Power(t2,2) + 2*Power(t2,3)) - 
               s2*(-4 + 12*(2 + t1)*t2 + 11*(-3 + 2*t1)*Power(t2,2) - 
                  2*(-6 + t1)*Power(t2,3) + Power(t2,4)) + 
               t1*(11 - 11*t2 - 6*Power(t2,2) + 3*Power(t2,3) + 
                  3*Power(t2,4))) - 
            s1*(2*Power(s2,2)*t2*
                (-6 + 14*t2 + 7*Power(t2,2) + Power(t2,3)) - 
               Power(-1 + t2,2)*
                (2 - 3*t2 + 14*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-4 + 13*t2 + 17*Power(t2,2) + 
                  5*Power(t2,3) + Power(t2,4)) + 
               t1*(-16 + 6*t2 + 29*Power(t2,2) - 16*Power(t2,3) - 
                  5*Power(t2,4) + 2*Power(t2,5)) + 
               s2*(1 + 57*t2 - 103*Power(t2,2) + 36*Power(t2,3) + 
                  10*Power(t2,4) - Power(t2,5) + 
                  t1*(5 + 5*t2 - 61*Power(t2,2) - 9*Power(t2,3) - 
                     4*Power(t2,4))))) + 
         Power(-1 + t2,2)*(2 - 4*t1 + 2*Power(t1,2) - 
            2*Power(s1,4)*(s2 - t1)*(-1 + t2) + 10*t2 - 2*s2*t2 - 
            6*t1*t2 + 2*s2*t1*t2 - 4*Power(t1,2)*t2 - 28*Power(t2,2) + 
            23*s2*Power(t2,2) - 4*Power(s2,2)*Power(t2,2) + 
            7*t1*Power(t2,2) - 3*s2*t1*Power(t2,2) + 
            5*Power(t1,2)*Power(t2,2) + 18*Power(t2,3) - 
            22*s2*Power(t2,3) + 7*Power(s2,2)*Power(t2,3) + 
            2*t1*Power(t2,3) - 6*s2*t1*Power(t2,3) + 
            Power(t1,2)*Power(t2,3) - 2*Power(t2,4) + s2*Power(t2,4) + 
            Power(s2,2)*Power(t2,4) + t1*Power(t2,4) - 
            s2*t1*Power(t2,4) + 
            Power(s1,2)*(Power(-1 + t2,2)*(1 + t2) + 
               Power(s2,2)*(-4 + 5*t2 + 11*Power(t2,2)) + 
               t1*(-19 + 14*t2 + 9*Power(t2,2) - 4*Power(t2,3)) + 
               Power(t1,2)*(2 + 10*t2 - Power(t2,2) + Power(t2,3)) + 
               s2*(15 - 8*t2 - 9*Power(t2,2) + 2*Power(t2,3) + 
                  t1*(5 - 20*t2 - 9*Power(t2,2)))) - 
            Power(s1,3)*(2*Power(-1 + t2,2) + Power(s2,2)*(-1 + 5*t2) + 
               Power(t1,2)*(2 + 3*t2 - Power(t2,2)) + 
               t1*(-3 + 2*t2 + Power(t2,2)) + 
               s2*(1 + 2*t2 - 3*Power(t2,2) + 
                  t1*(-1 - 8*t2 + Power(t2,2)))) - 
            s1*(-(Power(-1 + t2,2)*(-16 - t2 + 3*Power(t2,2))) + 
               Power(s2,2)*t2*(-8 + 13*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(2 + 3*t2 + 5*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-18 - 4*t2 + 27*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4)) - 
               s2*(2 - 38*t2 + 31*Power(t2,2) + 8*Power(t2,3) - 
                  3*Power(t2,4) + 
                  t1*(-2 - 2*t2 + 25*Power(t2,2) + 2*Power(t2,3) + 
                     Power(t2,4))))) - 
         Power(s,2)*(-(Power(t1,2)*
               (6 - 14*t2 + 3*Power(t2,2) + 10*Power(t2,3) + 
                 3*Power(t2,4))) + 
            t1*(4 + 6*t2 - (5 + 16*s2)*Power(t2,2) + 
               (-26 + 29*s2)*Power(t2,3) + 2*(12 + s2)*Power(t2,4) + 
               s2*Power(t2,5) - 3*Power(t2,6)) + 
            Power(s1,2)*(-1 + t2)*
             (8 - 52*t2 + 45*Power(t2,2) - Power(t2,4) + 
               s2*(2 - 5*t2 + 21*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(4 - 4*t2 - 23*Power(t2,2) + 3*Power(t2,3))) + 
            t2*(Power(s2,2)*t2*(9 - 17*t2 + Power(t2,2) - Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-8 + 17*t2 - 27*Power(t2,2) - 5*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(-5 - 39*t2 + 92*Power(t2,2) - 50*Power(t2,3) + 
                  Power(t2,4) + Power(t2,5))) + 
            s1*(Power(s2,2)*t2*(-1 + t2 + 7*Power(t2,2) + Power(t2,3)) - 
               Power(-1 + t2,2)*
                (2 - 39*t2 + 72*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-6 + 5*Power(t2,2) + 12*Power(t2,3) - 
                  3*Power(t2,4)) + 
               t1*t2*(18 + 4*t2 - 41*Power(t2,2) + 14*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s2*(t2*(13 - 61*t2 + 62*Power(t2,2) - 9*Power(t2,3) - 
                     5*Power(t2,4)) + 
                  t1*(3 + 10*t2 - 15*Power(t2,2) - 16*Power(t2,3) + 
                     2*Power(t2,4))))))*T4q(-1 + t2))/
     (s*(-1 + s1)*(-1 + t1)*Power(-1 + t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(Power(s,5)*t2*(-(t1*(-2 + t2)) + t2*(2 + t2)) - 
         2*Power(s1 - t2,2)*Power(-1 + t2,2)*
          (-7 + Power(s1,2)*(s2 - t1) + 4*t1 + 3*Power(t1,2) + 6*t2 - 
            12*t1*t2 + Power(t2,2) + 
            s2*(4 + t1*(-4 + t2) + 5*t2 - Power(t2,2)) + 
            3*s1*(1 + s2*(-3 + t1) - Power(t1,2) - t2 + t1*(2 + t2))) + 
         Power(s,4)*(-(Power(t1,2)*(2 + t2 + Power(t2,2))) + 
            t1*t2*(-6 + s2 + 6*t2 + s2*t2 + 2*Power(t2,2)) + 
            t2*(2 - 3*(1 + s2)*t2 - (7 + 2*s2)*Power(t2,2) + 
               Power(t2,3)) + 
            s1*(t1*(2 - 5*t2 + 3*Power(t2,2)) + 
               t2*(2 + t2 - 2*Power(t2,2) + s2*(2 + t2)))) + 
         Power(s,3)*(Power(t1,2)*(6 - 2*t2 - 3*Power(t2,2)) + 
            t1*(-4 - 14*t2 + (3 - 2*s2)*Power(t2,2) - 
               (8 + s2)*Power(t2,3) + 3*Power(t2,4)) + 
            Power(s1,2)*(t1*(-2 + 2*t2 - 3*Power(t2,2)) + 
               s2*(2 + t2 - 2*Power(t2,2)) + t2*(2 - 5*t2 + Power(t2,2))) \
+ t2*(-8 - 23*t2 + Power(s2,2)*(-1 + t2)*t2 + 29*Power(t2,2) - 
               13*Power(t2,3) + Power(t2,4) + 
               s2*(5 + 15*t2 + 5*Power(t2,2) - Power(t2,3))) + 
            s1*(2 + 5*t2 - Power(s2,2)*(-1 + t2)*t2 - 20*Power(t2,2) + 
               11*Power(t2,3) + t1*t2*(14 + 6*t2 - 5*Power(t2,2)) + 
               3*Power(t1,2)*(2 + Power(t2,2)) - 
               s2*(t2*(13 + 5*t2 - 5*Power(t2,2)) + 
                  t1*(3 + 4*t2 + 2*Power(t2,2))))) + 
         Power(s,2)*(-2 - 9*t2 - 3*s2*t2 + 49*Power(t2,2) + 
            4*s2*Power(t2,2) - 3*Power(s2,2)*Power(t2,2) - 
            78*Power(t2,3) + 9*s2*Power(t2,3) - 
            2*Power(s2,2)*Power(t2,3) + 47*Power(t2,4) + 
            9*s2*Power(t2,4) - 7*Power(t2,5) - s2*Power(t2,5) + 
            Power(t1,2)*(-6 + 7*t2 + Power(t2,2) - 5*Power(t2,3)) + 
            t1*(8 - 3*(-8 + s2)*t2 + 3*(-29 + 6*s2)*Power(t2,2) + 
               (45 - 7*s2)*Power(t2,3) - 10*Power(t2,4) + 2*Power(t2,5)) \
+ Power(s1,3)*(2*(-1 + t2)*t2 + s2*(2 - 5*t2 + Power(t2,2)) + 
               t1*(-2 + 3*t2 + Power(t2,2))) + 
            Power(s1,2)*(Power(t1,2)*(-8 + 2*t2 - 3*Power(t2,2)) + 
               Power(s2,2)*(-1 - 6*t2 + 2*Power(t2,2)) + 
               6*(1 - 4*t2 + 3*Power(t2,2)) + 
               t1*(-1 + 12*t2 - 22*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(-4 + 4*t2 + 9*Power(t2,2) - Power(t2,3) + 
                  2*t1*(3 + 3*t2 + Power(t2,2)))) + 
            s1*(-2 - 31*t2 + 81*Power(t2,2) - 49*Power(t2,3) + 
               Power(t2,4) - 2*Power(s2,2)*t2*(-2 - 4*t2 + Power(t2,2)) - 
               Power(t1,2)*(4 - 5*t2 - 12*Power(t2,2) + Power(t2,3)) + 
               t1*(-16 + 66*t2 - 43*Power(t2,2) + 19*Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*(1 - 10*t2 + 3*Power(t2,2) - 19*Power(t2,3) + 
                  Power(t2,4) + 
                  t1*(5 - 14*t2 - 17*Power(t2,2) + 4*Power(t2,3))))) + 
         s*(2 - 4*t1 + 2*Power(t1,2) + 2*Power(s1,4)*(s2 - t1)*(-1 + t2) + 
            10*t2 - 2*s2*t2 - 6*t1*t2 + 2*s2*t1*t2 - 4*Power(t1,2)*t2 - 
            60*Power(t2,2) + 5*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) + 71*t1*Power(t2,2) - 
            25*s2*t1*Power(t2,2) + 9*Power(t1,2)*Power(t2,2) + 
            92*Power(t2,3) + 14*s2*Power(t2,3) - 
            7*Power(s2,2)*Power(t2,3) - 122*t1*Power(t2,3) + 
            38*s2*t1*Power(t2,3) - 9*Power(t1,2)*Power(t2,3) - 
            56*Power(t2,4) - 21*s2*Power(t2,4) - Power(s2,2)*Power(t2,4) + 
            61*t1*Power(t2,4) - 7*s2*t1*Power(t2,4) - 
            2*Power(t1,2)*Power(t2,4) + 14*Power(t2,5) + 
            4*s2*Power(t2,5) - 2*Power(t2,6) + 
            Power(s1,2)*(Power(s2,2)*(4 - 5*t2 - 11*Power(t2,2)) + 
               Power(-1 + t2,2)*(-3 + 11*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-6 + 12*t2 - 19*Power(t2,2) + Power(t2,3)) + 
               t1*(29 - 74*t2 + 41*Power(t2,2) + 4*Power(t2,3)) - 
               s2*(19 + t1 - 52*t2 + 27*Power(t2,2) - 25*t1*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(s1,3)*(5*t1*Power(-1 + t2,2) - 
               2*Power(-1 + t2,2)*(3 + t2) + Power(s2,2)*(-1 + 5*t2) + 
               Power(t1,2)*(4 - t2 + Power(t2,2)) - 
               s2*(-3*Power(-1 + t2,2) + t1*(3 + 4*t2 + Power(t2,2)))) + 
            s1*(Power(s2,2)*t2*(-8 + 13*t2 + 7*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-16 + 35*t2 - 13*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-2 + t2 - 3*Power(t2,2) + 16*Power(t2,3)) + 
               t1*(18 - 108*t2 + 183*Power(t2,2) - 82*Power(t2,3) - 
                  11*Power(t2,4)) + 
               s2*(2 + 14*t2 - 69*Power(t2,2) + 56*Power(t2,3) - 
                  3*Power(t2,4) + 
                  t1*(-2 + 26*t2 - 35*Power(t2,2) - 14*Power(t2,3) + 
                     Power(t2,4))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + t1)*(-1 + t2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)));
   return a;
};
