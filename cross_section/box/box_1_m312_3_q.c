#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m312_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(Power(s,2)*(1 - 2*Power(s2,3) + Power(s2,2)*(9 + t1 - t2) + 
            s2*(-20 - 5*t1 + t2)) - 
         s2*(-1 + t1)*(-7 - 2*Power(s2,3) + 41*t1 - 18*Power(t1,2) + 
            s1*(-1 + s2)*(-2 + s2 + t1) + Power(s2,2)*(6 - 2*t1 - 3*t2) - 
            9*t2 + 6*t1*t2 + s2*
             (-41 + 17*t1 + 6*Power(t1,2) + 12*t2 - 6*t1*t2)) + 
         s*(-1 + 2*Power(s2,4) + t1 + Power(s2,3)*(-4 + s1 + t2) + 
            Power(s2,2)*(22 - t1 - 3*Power(t1,2) + s1*(5 + t1) - 13*t2 + 
               3*t1*t2) - s2*(-37 + 64*t1 - 11*Power(t1,2) + s1*(6 + t1) - 
               12*t2 + 3*t1*t2))))/
     (s*Power(-1 + s2,2)*s2*(-s + s2 - t1)*(-1 + s + t1)) + 
    (8*(-(Power(s2,4)*(-1 + t1)*(-5 + 3*s1 - 2*t2)) + 
         Power(s2,5)*(s1 - t2) - 
         Power(s2,2)*(-6 - 44*t1 + 3*Power(t1,3) + 
            s1*(6 - 16*t1 + 7*Power(t1,2) + Power(t1,3)) + 
            Power(t1,2)*(47 - 6*t2) + 8*t2) + 
         Power(s,3)*(-2*(-3 + s2)*Power(t1,2) + 
            t1*(-5 + Power(s2,2) + 2*s2*(-4 + t2) - 2*t2) + 
            (-1 + Power(s2,2))*t2) + 
         Power(s2,3)*(-27 + 33*t1 + s1*(-6 - t1 + 3*Power(t1,2)) + 
            5*t2 - Power(t1,2)*(6 + t2)) + 
         s2*(4*Power(t1,4) + s1*
             (8 - 4*t1 - 6*Power(t1,2) + 5*Power(t1,3)) + 
            t1*(80 - 12*t2) + Power(t1,3)*(31 - 4*t2) + 14*(-5 + t2) - 
            Power(t1,2)*(45 + t2)) + 
         2*(4 - 6*Power(t1,4) + Power(t1,2)*(-31 + 5*s1 - 2*t2) - 
            2*Power(t1,3)*(-5 + s1 - t2) - 4*t2 + t1*(23 - 4*s1 + 5*t2)) \
+ Power(s,2)*(-1 + 12*Power(t1,3) - 
            s2*(4 + s1 - 4*s1*t1 + 4*Power(t1,3) + 
               Power(t1,2)*(23 - 4*t2) + t1*(-39 + t2) - 6*t2) + 
            Power(s2,3)*(s1 - 2*t1 - 3*t2) - t2 + 
            t1*(39 - 3*s1 + 3*t2) - Power(t1,2)*(27 + 4*t2) + 
            Power(s2,2)*(-7 + 6*Power(t1,2) - 2*t2 - 
               t1*(-20 + s1 + 2*t2))) - 
         s*(2 - 6*Power(t1,4) + Power(t1,2)*(-55 + 7*s1 - 8*t2) + 
            Power(s2,4)*(2*s1 - t1 - 3*t2) - 10*t2 + 
            2*Power(t1,3)*(17 + t2) + t1*(87 - 6*s1 + t2) + 
            Power(s2,3)*(s1*(3 - 4*t1) + 4*Power(t1,2) - 4*(4 + t2) + 
               t1*(17 + 2*t2)) + 
            Power(s2,2)*(-11 - 5*Power(t1,3) + 
               s1*(-11 + 6*t1 + 2*Power(t1,2)) - 3*t1*(-23 + t2) + 
               6*t2 + Power(t1,2)*(-22 + 3*t2)) + 
            s2*(-43 + 76*t1 + 2*Power(t1,4) + 
               s1*(6 + 4*t1 - 9*Power(t1,2)) + Power(t1,3)*(11 - 2*t2) + 
               11*t2 + Power(t1,2)*(-79 + 5*t2))))*B1(s,1 - s + s2 - t1,s2)\
)/(Power(-1 + s2,2)*(-s + s2 - t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) \
+ (16*(Power(s,4)*(1 - s2 + 7*Power(s2,2) + Power(s2,3)) + 
         Power(s,3)*(-2*Power(s2,4) + 2*(-2 + t1) + 
            s2*(2 + Power(t1,2) - t1*(-2 + t2)) + 
            Power(s2,3)*(-16 - 4*s1 + 3*t1 + t2) + 
            Power(s2,2)*(-28 + 4*s1 + t1 - Power(t1,2) - t2 + t1*t2)) + 
         s*(-2*(2 - 3*t1 + Power(t1,2)) + 
            Power(s2,5)*(-6 - 5*s1 + 2*t1 + 5*t2) + 
            Power(s2,4)*(15 - 9*Power(t1,2) + s1*(-13 + 3*t1) + 7*t2 + 
               3*t1*(2 + t2)) + 
            Power(s2,2)*(-60 - 8*Power(t1,2) - 7*Power(t1,3) + 
               s1*(12 - 4*t1 + Power(t1,2) + Power(t1,3)) - 11*t2 + 
               t1*(79 + t2)) + 
            s2*(6 - s1*Power(t1,3) - 3*t1*t2 + 
               2*Power(t1,2)*(-3 + s1 + t2)) - 
            Power(s2,3)*(-29 - 3*Power(t1,3) + 
               s1*(-6 - t1 + 3*Power(t1,2)) + t2 + t1*(57 + t2) + 
               Power(t1,2)*(-13 + 2*t2))) + 
         (-1 + s2)*(-Power(-1 + t1,2) + 2*Power(s2,5)*(s1 - t2) + 
            Power(s2,4)*(-5 + s1 + 2*t1 + s1*t1 + 3*Power(t1,2) - t2 - 
               t1*t2) - s2*(-1 + t1)*
             (2 + (-1 + s1)*Power(t1,2) - t1*(1 + t2)) + 
            Power(s2,3)*(13 - 2*Power(t1,3) + 
               s1*(-5 - 7*t1 + 2*Power(t1,2)) + t1*(-12 + t2) + 8*t2 + 
               Power(t1,2)*(1 + t2)) + 
            Power(s2,2)*(7*Power(t1,3) + 
               s1*(4 + 3*Power(t1,2) - Power(t1,3)) - 5*(3 + t2) + 
               t1*(29 + t2) - Power(t1,2)*(21 + 2*t2))) + 
         Power(s,2)*(6 + Power(s2,5) - 6*t1 + Power(t1,2) + 
            Power(s2,4)*(19 + 7*s1 - 5*t1 - 4*t2) + 
            Power(s2,2)*(64 - 36*t1 - Power(t1,3) + 
               s1*(-12 + 4*t1 + Power(t1,2)) + Power(t1,2)*(-14 + t2) + 
               7*t2) + s2*(-4 + Power(t1,3) - 
               Power(t1,2)*(-2 + s1 + t2) + t1*(-7 + 3*t2)) - 
            Power(s2,3)*(26 - 7*Power(t1,2) + s1*(-5 + 4*t1) + 3*t2 + 
               t1*(2 + 3*t2))))*R1q(s2))/
     (Power(-1 + s2,2)*s2*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*(-s + s2 - t1)*Power(-1 + s + t1,2)) - 
    (32*(2*Power(s,4) + Power(s,3)*(-8 + s1 - 4*s2 - s1*s2 + 4*t1) + 
         Power(s2,4)*(s1 - t2) - 
         (-1 + t1)*(1 + 2*(-3 + s1)*t1 + 3*Power(t1,3) - 
            Power(t1,2)*(-2 + t2) - t2) + 
         Power(s2,3)*(-3 - s1*(-2 + t1) + Power(t1,2) - 2*t2 + 
            t1*(2 + t2)) + Power(s2,2)*
          (-2*Power(t1,3) - s1*(1 + 4*t1) + Power(t1,2)*(-4 + t2) + 
            2*t2 + 2*t1*(3 + t2)) + 
         s2*(-5*Power(t1,2) + Power(t1,4) + 
            s1*(-2 + 3*t1 + 2*Power(t1,2)) - Power(t1,3)*(-5 + t2) + 
            2*(2 + t2) - t1*(5 + 4*t2)) + 
         s*(-10 + 23*t1 - 7*Power(t1,2) - 6*Power(t1,3) - 
            s1*(-1 + s2)*(2 + 2*Power(s2,2) - 2*s2*(-3 + t1) - 5*t1 + 
               Power(t1,2)) + 2*Power(s2,3)*(-1 + t2) + t2 - t1*t2 + 
            2*Power(t1,2)*t2 + 
            Power(s2,2)*(5 + t1 - 2*Power(t1,2) + 2*t2) + 
            s2*(2*Power(t1,3) + Power(t1,2)*(5 - 2*t2) + t1*(-4 + t2) - 
               5*(1 + t2))) + 
         Power(s,2)*(15 + s1*(-1 + s2)*(3 + 2*s2 - 2*t1) - 16*t1 - 
            Power(t1,2) - Power(s2,2)*(-5 + t2) + t1*t2 + 
            s2*(Power(t1,2) + t2 - t1*(4 + t2))))*R1q(1 - s + s2 - t1))/
     (Power(-1 + s2,2)*Power(-s + s2 - t1,2)*Power(-1 + s + t1,2)) + 
    (16*(-4*Power(s,4) + Power(s,3)*
          (-2 + 2*s1*(-1 + s2) - 2*Power(s2,2) - t1 + s2*(8 + t1 - t2) + 
            t2) + Power(-1 + s2,2)*
          (4 - s1*Power(-1 + s2,2) + Power(s2,3) - 18*t1 + 
            6*Power(t1,2) + s2*
             (17 - 2*Power(t1,2) + 2*t1*(-3 + t2) - 6*t2) + 
            2*Power(s2,2)*(-1 + t2) + 4*t2 - 2*t1*t2) + 
         s*(-19 - 4*Power(s2,4) + 29*t1 - 12*Power(t1,2) + 
            s1*Power(-1 + s2,2)*(2 + s2 + t1) + 
            Power(s2,3)*(8 + 2*t1 - 5*t2) - 7*t2 + 4*t1*t2 + 
            s2*(-18 + 64*t1 - 8*Power(t1,2) + t2) + 
            Power(s2,2)*(-47 + t1 + 4*Power(t1,2) + 11*t2 - 4*t1*t2)) + 
         Power(s,2)*(15 + 5*Power(s2,3) - 14*t1 + 6*Power(t1,2) - 
            s1*(-1 + s2)*(1 + 2*s2 + t1) + 2*t2 - 2*t1*t2 + 
            Power(s2,2)*(-12 - 3*t1 + 4*t2) + 
            s2*(68 + t1 - 2*Power(t1,2) - 6*t2 + 2*t1*t2)))*R2q(s))/
     (s*Power(-1 + s2,2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*(-s + s2 - t1)) + 
    (8*(Power(-1 + s2,4)*(-4 + 2*s2 + Power(s2,2) + 2*t1 - s2*t1)*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) + 
         Power(s,5)*((7 + 4*s2 + Power(s2,2))*t1 - 4*Power(t1,2) + 
            (-1 + Power(s2,2))*t2) + 
         Power(s,3)*(-1 + 6*(-8 + s1)*Power(t1,2) - 
            2*(-10 + s1)*Power(t1,3) + t1*(16 + s1 - 8*t2) + 10*t2 + 
            Power(s2,4)*(-4*s1 + 6*t1 + 10*t2) + 
            Power(s2,3)*(-2 - 9*Power(t1,2) + s1*(-6 + 11*t1) + 12*t2 - 
               4*t1*t2) + s2*
             (-5 + 162*t1 - 84*Power(t1,2) + 2*Power(t1,3) + 
               s1*(-5 + 3*t1 + 3*Power(t1,2) + 2*Power(t1,3)) - 12*t2 + 
               4*t1*t2) + Power(s2,2)*
             (-64 + 80*t1 - 11*Power(t1,2) + 2*Power(t1,3) - 
               3*s1*(-5 + 5*t1 + 3*Power(t1,2)) - 20*t2 + 8*t1*t2)) + 
         Power(s,4)*(-1 - 2*(-14 + s1)*Power(t1,2) - 4*Power(t1,3) + 
            Power(s2,3)*(s1 - 4*t1 - 5*t2) + t1*(-22 - 3*s1 + 2*t2) + 
            s2*(8 - 67*t1 + 13*Power(t1,2) + 
               s1*(-1 + 6*t1 + 2*Power(t1,2)) + 10*t2 - 3*t1*t2) + 
            Power(s2,2)*(5 + 3*Power(t1,2) - 5*t2 + t1*(-7 - 3*s1 + t2))) \
- s*Power(-1 + s2,2)*(11 + (8 - 6*s1)*Power(t1,2) - 
            2*(-2 + s1)*Power(t1,3) + Power(s2,4)*(4*s1 - t1 - 5*t2) - 
            15*t2 + t1*(-17 + 13*s1 + 8*t2) + 
            Power(s2,2)*(4 + Power(t1,2) - 2*Power(t1,3) + 
               s1*(-9 - 7*t1 + 7*Power(t1,2)) + 20*t2 - 8*t1*t2) - 
            s2*(-3 + 4*Power(t1,2) - 2*Power(t1,3) + 
               s1*(5 + t1 - 7*Power(t1,2) + 2*Power(t1,3)) - 6*t2 + 
               4*t1*(3 + t2)) + 
            Power(s2,3)*(-2 + s1*(10 - 9*t1) + 3*Power(t1,2) - 6*t2 + 
               t1*(2 + 4*t2))) + 
         Power(s,2)*(9 + (18 - 8*s1)*Power(t1,2) + 
            4*(-5 + s1)*Power(t1,3) + t1*(11*s1 + 12*(-1 + t2)) + 
            2*Power(s2,5)*(3*s1 - 2*t1 - 5*t2) - 20*t2 + 
            Power(s2,4)*(-4 + 9*Power(t1,2) - 5*s1*(-2 + 3*t1) - 6*t2 + 
               6*t1*(1 + t2)) + 
            Power(s2,2)*(77 + 23*Power(t1,2) + 4*Power(t1,3) + 
               s1*(8 + 8*t1 - 6*Power(t1,2) - 4*Power(t1,3)) - 14*t2 + 
               2*t1*(-67 + 3*t2)) + 
            s2*(s1*(5 - 16*t1 + Power(t1,2)) - 
               2*(8 - 56*Power(t1,2) + 6*Power(t1,3) - 8*t2 + 
                  5*t1*(6 + t2))) + 
            Power(s2,3)*(s1*(-29 + 12*t1 + 13*Power(t1,2)) - 
               2*(-15 + Power(t1,2) + 2*Power(t1,3) - 17*t2 + 
                  t1*(10 + 7*t2)))))*T2q(s2,s))/
     (s*Power(-1 + s2,2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*(-s + s2 - t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))\
) + (8*(-((-1 + s2)*Power(-1 + t1,2)*
            (-4 + 2*s2 + Power(s2,2) + 2*t1 - s2*t1)*
            (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2)) + 
         Power(s,4)*(2*(-3 + s2)*Power(t1,2) - 
            t1*(11 + Power(s2,2) + 2*s2*(-4 + t2) - 2*t2) + t2 - 
            Power(s2,2)*t2) + 
         Power(s,3)*(1 - 18*Power(t1,3) + 
            s2*(-12 + s1 + 4*s1*t1 + 6*Power(t1,3) + 
               Power(t1,2)*(33 - 6*t2) + 3*t1*(-14 + t2) - 5*t2) + 
            t1*(33 - 5*s1 - 2*t2) + t2 + Power(s2,3)*(-s1 + t1 + 2*t2) + 
            Power(s2,2)*(7 - 5*Power(t1,2) + t1*(-12 + s1 - t2) + 
               2*t2) + Power(t1,2)*(-8 + 6*t2)) + 
         Power(s,2)*(2 - 18*Power(t1,4) + 
            Power(t1,2)*(44 - 10*s1 - 9*t2) + Power(s2,4)*(s1 - t2) - 
            9*t2 + t1*(-47 + 18*s1 + 4*t2) + Power(t1,3)*(19 + 6*t2) + 
            Power(s2,3)*(-9 + s1*(3 - 4*t1) + 2*Power(t1,2) - 4*t2 + 
               t1*(7 + 5*t2)) + 
            s2*(6*Power(t1,4) + s1*(-2 - 8*t1 + 11*Power(t1,2)) + 
               t1*(59 - 15*t2) - 6*Power(t1,3)*(-7 + t2) + 
               14*(1 + t2) + Power(t1,2)*(-121 + 6*t2)) - 
            Power(s2,2)*(31 + 9*Power(t1,3) + 
               s1*(2 + 6*t1 + Power(t1,2)) - 3*Power(t1,2)*(-11 + t2) - 
               t1*(73 + 6*t2))) + 
         s*(-1 + t1)*(7 - 6*Power(t1,4) + 
            Power(t1,2)*(21 - 7*s1 - 4*t2) + 2*Power(s2,4)*(s1 - t2) - 
            11*t2 + 2*Power(t1,3)*(5 + t2) + t1*(-32 + 17*s1 + 3*t2) + 
            s2*(6 + 2*Power(t1,4) + s1*(-9 - 11*t1 + 10*Power(t1,2)) + 
               t1*(38 - 12*t2) + Power(t1,3)*(19 - 2*t2) + 
               Power(t1,2)*(-65 + t2) + 23*t2) - 
            Power(s2,2)*(23 + 5*Power(t1,3) + 
               s1*(-2 + 7*t1 + 3*Power(t1,2)) + 
               Power(t1,2)*(23 - 3*t2) + 4*t2 - 3*t1*(17 + 3*t2)) + 
            Power(s2,3)*(s1*(5 + t1) + 
               3*(5*t1 + Power(t1,2) - 2*(3 + t2)))))*
       T3q(s2,1 - s + s2 - t1))/
     (s*Power(-1 + s2,2)*(-s + s2 - t1)*(-1 + s + t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) - 
    (8*((-1 + s2)*(Power(s2,3) - 2*Power(s2,2)*(-1 + t1) - 
            2*(-2 + t1)*t1 + s2*(-4 + Power(t1,2)))*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) + 
         Power(s,3)*(-2*(-3 + s2)*Power(t1,2) + 
            t1*(7 + Power(s2,2) + 2*s2*(-2 + t2) - 2*t2) + 
            (-1 + Power(s2,2))*t2) + 
         Power(s,2)*(-1 + 12*Power(t1,3) - 
            s2*(-8 + s1 + 4*Power(t1,3) + Power(t1,2)*(13 - 4*t2) + 
               3*t1*(-1 + t2) - 6*t2) + Power(s2,3)*(s1 - 2*t1 - 3*t2) - 
            3*t2 - Power(t1,2)*(13 + 4*t2) + t1*(17 + s1 + 5*t2) + 
            Power(s2,2)*(-3 + 6*Power(t1,2) - t1*(-6 + s1 + 2*t2))) - 
         s*(4 - 6*Power(t1,4) + Power(t1,2)*(-43 + s1 - 6*t2) + 
            Power(s2,4)*(2*s1 - t1 - 3*t2) - 4*t2 + 
            2*Power(t1,3)*(10 + t2) + t1*(31 - 4*s1 + 9*t2) + 
            Power(s2,2)*(7 - 5*Power(t1,3) + 
               s1*(-7 + 2*t1 + 2*Power(t1,2)) + t1*(7 - 9*t2) + 
               3*Power(t1,2)*(-2 + t2) + 12*t2) + 
            Power(s2,3)*(-4 + s1 - 4*s1*t1 + 4*Power(t1,2) + 
               t1*(3 + 2*t2)) + 
            s2*(2*Power(t1,4) + s1*(4 + 6*t1 - 3*Power(t1,2)) + 
               Power(t1,3)*(9 - 2*t2) - 2*t1*(-16 + t2) - 5*(7 + t2) + 
               Power(t1,2)*(-19 + 3*t2))))*T4q(1 - s + s2 - t1))/
     (s*Power(-1 + s2,2)*(-s + s2 - t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) - 
    (8*(Power(s,3)*(2*(-3 + s2)*Power(t1,2) - 
            t1*(3 + Power(s2,2) + 2*s2*(-4 + t2) - 2*t2) + t2 - 
            Power(s2,2)*t2) - 4*(s2 - t1)*(-1 + t1)*
          (-1 + 6*Power(t1,2) + t1*(-5 + s1 - 2*t2) + 
            Power(s2,2)*(-2 + s1 + 2*t1 - t2) + t2 - 
            s2*(-7 + s1 + 5*t1 + s1*t1 + 2*Power(t1,2) - 2*t1*t2)) + 
         Power(s,2)*(1 - 6*Power(t1,3) + 3*t2 + 2*Power(t1,2)*(16 + t2) + 
            Power(s2,3)*(-s1 + t1 + 2*t2) - t1*(17 + s1 + 6*t2) + 
            Power(s2,2)*(7 - 3*Power(t1,2) + t1*(-10 + s1 + t2)) + 
            s2*(-4 + s1 + 2*Power(t1,3) + Power(t1,2)*(7 - 2*t2) - 5*t2 + 
               t1*(-26 + 5*t2))) + 
         s*(Power(s2,4)*(s1 - t2) + 
            Power(s2,3)*(-5 + s1 - 2*s1*t1 + t1*(5 + t2)) + 
            Power(s2,2)*(-9 + 11*Power(t1,2) + 
               s1*(-6 + 6*t1 + Power(t1,2)) + 7*t2 - 2*t1*(1 + 4*t2)) + 
            2*(2 + 28*Power(t1,3) + Power(t1,2)*(-37 + 3*s1 - 8*t2) - 
               2*t2 + t1*(7 - 2*s1 + 9*t2)) - 
            s2*(16*Power(t1,3) + s1*(-4 + 7*Power(t1,2)) + 
               Power(t1,2)*(57 - 16*t2) + 2*(15 + t2) + t1*(-103 + 11*t2)))\
)*T5q(s))/(s*Power(-1 + s2,2)*(-s + s2 - t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))));
   return a;
};
