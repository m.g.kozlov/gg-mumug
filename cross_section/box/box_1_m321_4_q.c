#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m321_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(8*Power(s,4)*s2*(s2 - t1)*(-1 + t1)*t1 + 
         Power(s,3)*t1*(-2*(-1 + t1)*t1 - 8*Power(s2,3)*(-3 + 2*t1) + 
            Power(s2,2)*(-6 - 8*s1*(-1 + t1) - 47*t1 + 37*Power(t1,2) - 
               11*t2 + 11*t1*t2) + 
            s2*(10 - 15*Power(t1,3) + Power(t1,2)*(11 + 8*s1 - 5*t2) - 
               6*t2 + t1*(2 - 8*s1 + 11*t2))) - 
         Power(s,2)*(2*Power(t1,2)*(3 - 5*t1 + 2*Power(t1,2)) + 
            8*Power(s2,4)*t1*(1 + s1 - t2) + 
            Power(s2,3)*(2 + 19*Power(t1,3) + t1*(-18 + 19*s1 - 22*t2) + 
               Power(t1,2)*(-43 - 27*s1 + 30*t2)) + 
            s2*t1*(12 + 9*Power(t1,4) - 25*t2 + 5*Power(t2,2) + 
               Power(t1,3)*(-8 - 11*s1 + 5*t2) + 
               t1*(50 + 4*s1*(-5 + t2) + 17*t2 - 7*Power(t2,2)) + 
               Power(t1,2)*(-87 + s1*(39 - 4*t2) - 5*t2 + 2*Power(t2,2))) \
+ Power(s2,2)*(-28*Power(t1,4) + Power(t1,3)*(43 + 30*s1 - 27*t2) + 
               t2*(1 + t2) + t1*
                (-42 - 4*s1*(-5 + t2) - 17*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(83 + 51*t2 - 8*Power(t2,2) + s1*(-58 + 4*t2)))\
) - s2*(s2 - t1)*t1*(3*(-1 + s1)*Power(t1,4) - 4*Power(-1 + t2,2) + 
            4*Power(s2,3)*(-1 + 2*Power(s1,2) + t1 + 
               s1*(-3 + 3*t1 - 4*t2) + 4*t2 - 4*t1*t2 + 2*Power(t2,2)) + 
            Power(t1,3)*(-5 + 5*Power(s1,2) + 36*t2 - 3*s1*(11 + t2)) + 
            Power(t1,2)*(-17 - 9*Power(s1,2) + 22*t2 - 9*Power(t2,2) + 
               s1*(7 + 6*t2)) + 
            t1*(29 - 4*Power(s1,2) - 66*t2 + 5*Power(t2,2) + 
               s1*(23 + 13*t2)) - 
            Power(s2,2)*(Power(s1,2)*(17 + 7*t1) + 
               Power(t1,2)*(8 - 25*t2) + t2*(37 + 3*t2) + 
               s1*(-33 + 16*t1 + 17*Power(t1,2) - 24*t2 - 24*t1*t2) + 
               t1*(-8 - 12*t2 + 21*Power(t2,2))) - 
            s2*(25 + Power(s1,2)*(-4 - 26*t1 + 6*Power(t1,2)) - 58*t2 + 
               Power(t2,2) + Power(t1,3)*(-7 + 9*t2) + 
               Power(t1,2)*(3 + 56*t2 - 9*Power(t2,2)) - 
               t1*(21 + 7*t2 + 16*Power(t2,2)) + 
               s1*(23 - 2*Power(t1,3) + 13*t2 + 10*t1*(4 + 3*t2) + 
                  Power(t1,2)*(-61 + 5*t2)))) + 
         s*(-2*Power(t1,2)*(-1 + 4*t1 - 4*Power(t1,2) + Power(t1,3)) + 
            8*Power(s2,5)*t1*(-1 + s1 + t1 - t2) + 
            Power(s2,2)*(1 + 8*Power(t1,5) + t1*(-28 - 6*s1*(-1 + t2)) + 
               2*t2 - Power(t2,2) - Power(t1,4)*(36 + 3*s1 + 7*t2) + 
               Power(t1,3)*(-156 + s1*(87 - 22*t2) - 35*t2 + 
                  22*Power(t2,2)) + 
               Power(t1,2)*(211 + 8*Power(s1,2) + 16*t2 - 
                  13*Power(t2,2) + 6*s1*(-11 + 2*t2))) + 
            s2*t1*(-9 - 3*Power(t1,5) + 10*t2 - 5*Power(t2,2) + 
               Power(t1,4)*(11 + 4*s1 + 2*t2) + 
               t1*(38 + 7*s1*(-1 + t2) - 15*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-126 - 4*Power(s1,2) + s1*(37 - 10*t2) + 
                  5*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(89 + 4*Power(s1,2) + 14*t2 - 3*Power(t2,2) + 
                  s1*(-50 + 3*t2))) + 
            Power(s2,4)*(-14*Power(t1,3) + 2*(2 + t2) + 
               Power(t1,2)*(8 - 11*s1 + 7*t2) + 
               t1*(2 + 8*Power(s1,2) + 15*t2 + 8*Power(t2,2) - 
                  s1*(13 + 16*t2))) + 
            Power(s2,3)*(4 + Power(t1,4) - 4*Power(s1,2)*t1*(1 + 3*t1) + 
               3*t2 + 3*Power(t2,2) + Power(t1,3)*(25 + 6*t2) + 
               Power(t1,2)*(61 + 18*t2 - 33*Power(t2,2)) + 
               7*t1*(-13 - 5*t2 + 2*Power(t2,2)) + 
               s1*(1 + 2*Power(t1,3) + t1*(29 - 2*t2) - t2 + 
                  Power(t1,2)*(-24 + 35*t2))))))/
     (s*(-1 + s2)*s2*Power(s2 - t1,2)*(-s + s2 - t1)*(-1 + t1)*t1*
       (s - s1 + t2)) + (8*(8*Power(s,6)*Power(t1,2) + 
         2*(-1 + t1)*(2*Power(s2,3) + t1 - 3*Power(t1,2) - 
            3*Power(s2,2)*(1 + t1) + s2*(-1 + 6*t1 + Power(t1,2)))*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,5)*(-((-24 + s1)*Power(t1,3)) + Power(t2,2) - 
            t1*t2*(3 - s1 + t2) + 
            s2*t1*(16 - Power(t1,2) + t1*(-29 + t2) + t2) + 
            Power(t1,2)*(-44 + s1*(-5 + t2) + 7*t2)) + 
         Power(s,4)*(-2*(-10 + s1)*Power(t1,4) - (-2 + t2)*t2 + 
            Power(s2,2)*(8 + 4*Power(t1,3) + 
               Power(t1,2)*(37 + s1 - 3*t2) + t1*(-43 + s1 - 2*t2) + t2) \
+ Power(t1,3)*(-78 + Power(s1,2) + 2*s1*(-10 + t2) + 18*t2) - 
            Power(t1,2)*(-75 + 3*Power(s1,2) + s1*(-39 + t2) + 33*t2) - 
            t1*(1 + s1 - 7*t2 + 3*s1*t2 - 7*Power(t2,2)) + 
            s2*(-3*Power(t1,4) + (-5 + 3*s1 - 8*t2)*t2 + 
               Power(t1,3)*(-57 + 4*s1 + 3*t2) + 
               Power(t1,2)*(160 + 14*s1 + Power(s1,2) - 26*t2 - 
                  4*s1*t2) + t1*
                (-70 - 12*s1 + Power(s1,2) + 30*t2 - 5*s1*t2 + 
                  6*Power(t2,2)))) - 
         Power(s,3)*(-1 + (-7 + s1)*Power(t1,5) + 2*Power(t2,2) - 
            Power(t1,4)*(-17 + 2*Power(s1,2) + s1*(-21 + t2) + 11*t2) + 
            Power(t1,3)*(-28 + 7*Power(s1,2) + 5*s1*(-19 + t2) + 
               30*t2 - 4*Power(t2,2)) + 
            t1*(-5 + s1*(4 - 5*t2) + 2*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*(5*Power(t1,3) + s1*(-1 + t1 + 2*Power(t1,2)) - 
               3*Power(t1,2)*(-6 + t2) + 2*(7 + t2) - t1*(37 + t2)) + 
            Power(t1,2)*(44 - 9*Power(s1,2) - 19*t2 - Power(t2,2) + 
               s1*(83 + t2)) + 
            Power(s2,2)*(25 - 6*Power(t1,4) + 
               Power(s1,2)*(-2 + 3*t1 + 3*Power(t1,2)) + 
               Power(t1,2)*(167 - 45*t2) - 31*t2 - 23*Power(t2,2) + 
               Power(t1,3)*(-37 + 5*t2) + 
               t1*(-145 + 73*t2 + 15*Power(t2,2)) + 
               s1*(9 + 7*Power(t1,3) + Power(t1,2)*(11 - 5*t2) + 
                  17*t2 - t1*(29 + 12*t2))) + 
            s2*(3 + 27*Power(t1,4) + Power(t1,5) + 
               Power(s1,2)*t1*(9 - 10*t1 + Power(t1,2)) + 8*t2 - 
               4*Power(t2,2) + 
               Power(t1,2)*(188 - 125*t2 - 6*Power(t2,2)) - 
               Power(t1,3)*(135 - 57*t2 + Power(t2,2)) + 
               t1*(-76 + 66*t2 + 27*Power(t2,2)) - 
               s1*(1 - 140*Power(t1,2) + 8*Power(t1,4) + 
                  Power(t1,3)*(38 - 7*t2) + t1*(83 + 15*t2)))) + 
         Power(s,2)*(1 + 2*Power(t1,6) - 4*t2 + 3*Power(t2,2) + 
            Power(t1,5)*(8 - 10*s1 + Power(s1,2) + 6*t2) + 
            Power(s2,4)*(-1 + t1)*
             (-6 + s1 + 4*t1 + s1*t1 + 2*Power(t1,2) - t2 - t1*t2) + 
            t1*(-9 - 5*s1*(-1 + t2) + 21*t2 - 11*Power(t2,2)) + 
            Power(t1,4)*(-60 + 54*s1 - 5*Power(s1,2) + 22*t2 - s1*t2 + 
               2*Power(t2,2)) + 
            Power(t1,3)*(69 + 6*Power(s1,2) - 79*t2 - 10*Power(t2,2) + 
               s1*(-83 + 3*t2)) + 
            Power(t1,2)*(-11 - 8*Power(s1,2) + 18*t2 + 18*Power(t2,2) + 
               s1*(50 + 7*t2)) - 
            s2*(3 + 8*Power(t1,5) + 
               2*Power(s1,2)*t1*
                (-5 + 10*t1 - 8*Power(t1,2) + 2*Power(t1,3)) + 6*t2 - 
               8*Power(t2,2) + Power(t1,4)*(1 + 35*t2) + 
               Power(t1,3)*(-66 - 55*t2 + 4*Power(t2,2)) + 
               3*t1*(2 + 13*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(48 - 25*t2 + 8*Power(t2,2)) - 
               s1*(-1 + 227*Power(t1,2) + 2*Power(t1,5) - 
                  Power(t1,4)*(-26 + t2) + t2 + t1*(-79 + 8*t2) + 
                  Power(t1,3)*(-175 + 12*t2))) + 
            Power(s2,3)*(23 - 3*Power(t1,4) + 
               2*Power(s1,2)*(-4 + 2*t1 + Power(t1,2)) + 
               Power(t1,2)*(52 - 35*t2) + Power(t1,3)*(-4 + t2) - 
               49*t2 - 30*Power(t2,2) + 
               t1*(-68 + 83*t2 + 20*Power(t2,2)) + 
               s1*(21 + 5*Power(t1,3) + 34*t2 - 
                  Power(t1,2)*(5 + 2*t2) - t1*(21 + 20*t2))) + 
            Power(s2,2)*(19 + Power(t1,5) + 
               Power(s1,2)*(-2 + 22*t1 - 15*Power(t1,2) + Power(t1,3)) + 
               14*t2 + 5*Power(t2,2) + Power(t1,4)*(7 + t2) + 
               Power(t1,2)*(66 - 175*t2 - 14*Power(t2,2)) + 
               Power(t1,3)*(-46 + 63*t2 - 2*Power(t2,2)) + 
               t1*(-47 + 113*t2 + 41*Power(t2,2)) + 
               s1*(27 - 8*Power(t1,4) - 13*t2 + 
                  Power(t1,3)*(-13 + 5*t2) + Power(t1,2)*(145 + 7*t2) - 
                  t1*(167 + 35*t2)))) - 
         s*(Power(t1,5)*(7 - 13*s1 - 6*t2) + 
            2*Power(t1,6)*(-1 + s1 - 2*t2) + Power(-1 + t2,2) - 
            t1*(-1 + t2)*(-13 + 2*s1 + 10*t2) + 
            Power(t1,2)*(-4 - 3*Power(s1,2) - 31*t2 + 25*Power(t2,2) + 
               3*s1*(3 + 4*t2)) + 
            Power(t1,4)*(-35 + 5*Power(s1,2) + 89*t2 + s1*(-23 + 8*t2)) - 
            Power(t1,3)*(-46 + 6*Power(s1,2) + 69*t2 + 20*Power(t2,2) + 
               s1*(-23 + 10*t2)) + 
            s2*(11 + Power(s1,2)*t1*
                (6 + 20*t1 - 3*Power(t1,2) - 8*Power(t1,3) + 
                  Power(t1,4)) - 19*t2 + 8*Power(t2,2) - 
               Power(t1,4)*(43 + 3*t2) + Power(t1,5)*(11 + 18*t2) + 
               t1*(33 + 18*t2 - 31*Power(t2,2)) + 
               Power(t1,3)*(108 - 220*t2 + 3*Power(t2,2)) + 
               2*Power(t1,2)*(-60 + 103*t2 + 18*Power(t2,2)) - 
               2*s1*(1 + 5*Power(t1,5) - 2*Power(t1,3)*(-2 + t2) - t2 + 
                  Power(t1,4)*(-39 + 2*t2) + 3*t1*(5 + 2*t2) + 
                  Power(t1,2)*(14 + 11*t2))) + 
            Power(s2,3)*(1 + Power(s1,2)*
                (8 + 27*t1 - 20*Power(t1,2) + Power(t1,3)) + 17*t2 + 
               18*Power(t2,2) + Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(12 + 24*t2 - Power(t2,2)) - 
               2*Power(t1,2)*(13 + 53*t2 + 7*Power(t2,2)) + 
               t1*(12 + 64*t2 + 13*Power(t2,2)) - 
               2*s1*(-5 - 4*Power(t1,3) + Power(t1,4) + 17*t2 + 
                  4*t1*(11 + 3*t2) - Power(t1,2)*(36 + 13*t2))) + 
            Power(s2,4)*(2*Power(s1,2)*(-5 + 3*t1) + 
               s1*(11 - 7*Power(t1,2) + Power(t1,3) + 28*t2 - 
                  5*t1*(1 + 4*t2)) + 
               t2*(-9*Power(t1,2) - Power(t1,3) - 9*(3 + 2*t2) + 
                  t1*(37 + 14*t2))) + 
            Power(s2,2)*(-28 - Power(t1,5) - 
               Power(s1,2)*(3 + 22*t1 + 19*Power(t1,2) - 
                  22*Power(t1,3) + 2*Power(t1,4)) + 11*t2 + 
               7*Power(t2,2) - Power(t1,4)*(22 + 27*t2) + 
               t1*(73 - 154*t2 - 34*Power(t2,2)) + 
               2*Power(t1,3)*(32 + 37*t2 + Power(t2,2)) + 
               Power(t1,2)*(-86 + 96*t2 + Power(t2,2)) + 
               s1*(21 + 7*Power(t1,4) + Power(t1,5) - 
                  2*Power(t1,3)*(66 + t2) - 4*Power(t1,2)*(-27 + 4*t2) + 
                  t1*(-5 + 66*t2)))))*B1(s,t1,s2))/
     (s*(-1 + s2)*(-s + s2 - t1)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(s - s1 + t2)) + 
    (8*(4*Power(s,7)*s2*t1*(Power(s2,2) - Power(t1,2)) - 
         2*(-1 + s2)*Power(s2,2)*Power(s2 - t1,2)*
          (-1 + 2*Power(s2,2) + 3*t1 - s2*(3 + t1))*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,6)*(Power(s2,4)*(4 - 14*t1) + 2*Power(t1,3) + 
            s2*Power(t1,2)*(-8 - 10*Power(t1,2) + 
               t1*(11 + 4*s1 - 2*t2) + t2) + 
            Power(s2,2)*t1*(-6 + 25*Power(t1,2) + t1*(-17 + t2) + 
               5*t2) + Power(s2,3)*t1*(-4 - 4*s1 + 5*t1 + 7*t2)) + 
         Power(s,5)*(6*(-2 + t1)*Power(t1,3) + 
            2*Power(s2,5)*(-5 + 11*t1) + 
            Power(s2,4)*(-30*Power(t1,2) + s1*(-4 + 13*t1) - 
               31*t1*(1 + t2) + t2*(7 + 2*t2)) + 
            s2*Power(t1,2)*(47 - 8*Power(t1,3) + 
               2*Power(t1,2)*(13 + 5*s1 - 2*t2) - 8*t2 + 
               t1*(-8 - 25*s1 + 6*t2)) + 
            Power(s2,3)*(-6 - 40*Power(t1,3) + 5*t2 + 
               Power(t1,2)*(111 + 13*t2) + 
               t1*(-24 + 19*s1 - 44*t2 - 4*s1*t2 + 2*Power(t2,2))) + 
            Power(s2,2)*t1*(13 + 44*Power(t1,3) - 33*t2 + 
               6*Power(t2,2) + Power(t1,2)*(-96 - 23*s1 + 10*t2) + 
               t1*(80 - 17*t2 + 2*Power(t2,2) + 2*s1*(5 + 2*t2)))) + 
         Power(s,4)*(-4*Power(s2,6)*(-3 + 7*t1) + 
            2*Power(t1,3)*(13 - 14*t1 + 3*Power(t1,2)) + 
            s2*Power(t1,2)*(-101 - 2*Power(t1,4) + 
               t1*(8 + 61*s1 - 13*t2) + 
               Power(t1,3)*(17 + 8*s1 - 2*t2) + 18*t2 + 
               Power(t1,2)*(13 - 58*s1 + 7*t2)) + 
            Power(s2,5)*(90*Power(t1,2) + s1*(9 - 20*t1 + 4*t2) + 
               t1*(79 + 48*t2) - 2*(21 + 14*t2 + 6*Power(t2,2))) + 
            Power(s2,4)*(-17 - 40*Power(t1,3) - 29*t2 + 2*Power(t2,2) - 
               Power(t1,2)*(265 + 48*t2) + 
               t1*(180 + 103*t2 - 4*Power(t2,2)) + 
               s1*(15 + 10*Power(t1,2) - 4*t2 + t1*(-23 + 8*t2))) + 
            Power(s2,2)*t1*(18 + 19*Power(t1,4) + 53*t2 - 
               12*Power(t2,2) + Power(t1,3)*(-98 - 38*s1 + 13*t2) + 
               Power(t1,2)*(85 - 48*t2 + 2*Power(t2,2) + 
                  s1*(135 + 8*t2)) + 
               t1*(-114 + 52*t2 + 8*Power(t2,2) - s1*(51 + 20*t2))) + 
            Power(s2,3)*(15 - 33*Power(t1,4) + 
               Power(t1,3)*(237 + 40*s1 - 5*t2) - 29*t2 + 
               6*Power(t2,2) - 
               Power(t1,2)*(278 - 20*t2 + 4*Power(t2,2) + 
                  s1*(63 + 20*t2)) + 
               t1*(97 + 80*t2 - 34*Power(t2,2) + s1*(-25 + 24*t2)))) + 
         s*(s2 - t1)*(6*Power(s2,8)*(-1 + t1) - 
            2*Power(t1,2)*(-1 + 4*t1 - 4*Power(t1,2) + Power(t1,3)) + 
            Power(s2,7)*(18 + 10*Power(s1,2) - 18*Power(t1,2) + 
               s1*(-17 + 9*t1 - 28*t2) + 5*t2 + 3*t1*t2 + 18*Power(t2,2)\
) - s2*(-1 + t1)*t1*(-4 + Power(t1,2)*(71 - 21*s1 - 6*t2) + 
               t1*(-13 + 10*s1 - 3*t2) + 
               2*Power(t1,3)*(-9 + 2*s1 + 2*t2)) + 
            Power(s2,6)*(27 + 18*Power(t1,3) - 
               2*Power(s1,2)*(7 + 13*t1) - 16*t2 - 50*Power(t2,2) + 
               Power(t1,2)*(31 + 4*t2) - 
               2*t1*(38 + 22*t2 + 7*Power(t2,2)) + 
               s1*(11 - 25*Power(t1,2) + 44*t2 + 10*t1*(7 + 6*t2))) + 
            Power(s2,3)*(-21 + 2*Power(s1,2)*Power(t1,2)*(-2 + 7*t1) + 
               Power(t1,3)*(78 - 46*t2) + Power(t1,4)*(41 - 18*t2) + 
               11*t2 - 18*Power(t2,2) + 
               t1*(46 + 75*t2 - 6*Power(t2,2)) + 
               2*Power(t1,2)*(-72 + 41*t2 + 5*Power(t2,2)) + 
               s1*(2 - 15*Power(t1,4) + 2*Power(t1,5) + 
                  t1*(3 - 32*t2) + 8*t2 + 2*Power(t1,2)*(37 + 6*t2) + 
                  2*Power(t1,3)*(-85 + 8*t2))) + 
            Power(s2,4)*(15 + 23*Power(t1,4) - 
               2*Power(s1,2)*t1*(-4 + 21*t1 + 3*Power(t1,2)) - 44*t2 + 
               20*Power(t2,2) + 2*Power(t1,3)*(-53 + 28*t2) - 
               2*Power(t1,2)*(53 + 20*t2 + 11*Power(t2,2)) - 
               2*t1*(-87 + 74*t2 + 11*Power(t2,2)) + 
               s1*(-5 - 11*Power(t1,4) + 4*t2 + 
                  4*Power(t1,3)*(12 + t2) + 4*t1*(-26 + 3*t2) + 
                  4*Power(t1,2)*(62 + 11*t2))) + 
            Power(s2,2)*(8 + Power(t1,5)*(6*s1 + 4*(-5 + t2)) - 12*t2 + 
               6*Power(t2,2) + 
               Power(t1,4)*(-13 + s1*(37 - 8*t2) + 16*t2) - 
               2*Power(t1,3)*(-71 + 10*s1*(2 + t2)) - 
               4*t1*(-4 - 9*t2 + Power(t2,2) + s1*(3 + 2*t2)) + 
               Power(t1,2)*(-133 - 68*t2 + 6*Power(t2,2) + 
                  s1*(33 + 28*t2))) + 
            Power(s2,5)*(-69 - 6*Power(t1,4) + 
               Power(s1,2)*(-4 + 42*t1 + 22*Power(t1,2)) + 32*t2 + 
               24*Power(t2,2) - Power(t1,3)*(48 + 7*t2) + 
               Power(t1,2)*(153 - 23*t2 + 6*Power(t2,2)) + 
               2*t1*(-15 + 71*t2 + 23*Power(t2,2)) + 
               s1*(45 + 25*Power(t1,3) - 4*t2 - 
                  4*Power(t1,2)*(23 + 9*t2) - 2*t1*(61 + 48*t2)))) + 
         Power(s,3)*(16*Power(s2,7)*(-1 + 2*t1) + 
            2*Power(t1,3)*(-13 + 23*t1 - 10*Power(t1,2) + Power(t1,3)) + 
            s2*Power(t1,2)*(99 + 2*(1 + s1)*Power(t1,4) + 
               Power(t1,3)*(27 - 39*s1 - 2*t2) - 16*t2 + 
               Power(t1,2)*(-130 + 117*s1 + t2) + 
               t1*(-7 - 73*s1 + 19*t2)) + 
            Power(s2,6)*(70 + 2*Power(s1,2) - 134*Power(t1,2) + 40*t2 + 
               28*Power(t2,2) - 7*t1*(5 + 4*t2) + 
               s1*(26*t1 - 5*(3 + 4*t2))) + 
            Power(s2,5)*(39 - 4*Power(s1,2)*t1 + 160*Power(t1,3) + 
               15*t2 - 18*Power(t2,2) + Power(t1,2)*(226 + 54*t2) - 
               t1*(266 + 103*t2 + 10*Power(t2,2)) + 
               s1*(1 - 54*Power(t1,2) + 12*t2 + 4*t1*(6 + 7*t2))) - 
            Power(s2,2)*t1*(63 + Power(t1,4)*(19 + 15*s1 - 4*t2) + 
               15*t2 + Power(t1,2)*
                (-217 - 131*t2 - 2*Power(t2,2) + 9*s1*(31 + 4*t2)) + 
               Power(t1,3)*(77 + 2*Power(s1,2) + 8*t2 - 
                  s1*(175 + 4*t2)) + 
               t1*(5 + 72*t2 + 22*Power(t2,2) - 2*s1*(47 + 18*t2))) + 
            Power(s2,3)*(-4 + 4*Power(t1,5) + 
               Power(t1,4)*(99 + 22*s1 - 7*t2) + 31*t2 - 
               6*Power(t2,2) + 
               Power(t1,3)*(-27 + 4*Power(s1,2) + 81*t2 - 
                  6*Power(t2,2) - s1*(259 + 16*t2)) + 
               Power(t1,2)*(-47 - 166*t2 - 52*Power(t2,2) + 
                  4*s1*(58 + 23*t2)) + 
               t1*(-103 + 61*t2 + 36*Power(t2,2) - s1*(5 + 52*t2))) + 
            Power(s2,4)*(39 - 62*Power(t1,4) + 40*t2 - 32*Power(t2,2) - 
               Power(t1,3)*(257 + 23*t2) + 
               Power(t1,2)*(301 - 56*t2 + 6*Power(t2,2)) + 
               t1*(-65 + 19*t2 + 74*Power(t2,2)) + 
               s1*(19*Power(t1,3) + 16*(-1 + t2) + 
                  2*Power(t1,2)*(57 + 2*t2) - t1*(71 + 68*t2)))) - 
         Power(s,2)*(2*Power(s2,8)*(-8 + 11*t1) + 
            2*Power(t1,3)*(-6 + 16*t1 - 11*Power(t1,2) + 2*Power(t1,3)) + 
            Power(s2,7)*(46 + 8*Power(s1,2) - 93*Power(t1,2) + 
               3*s1*(-9 + 8*t1 - 12*t2) - t1*(-15 + t2) + 24*t2 + 
               32*Power(t2,2)) + 
            s2*Power(t1,2)*(43 + Power(t1,3)*(115 - 56*s1 - 14*t2) - 
               5*t2 + 2*Power(t1,4)*(-5 + 3*s1 + 2*t2) + 
               Power(t1,2)*(-185 + 100*s1 + 7*t2) + 
               t1*(1 - 43*s1 + 13*t2)) + 
            Power(s2,5)*(-75 - 85*Power(t1,4) + 
               16*Power(s1,2)*t1*(1 + t1) - 23*t2 - 38*Power(t2,2) - 
               Power(t1,3)*(172 + 31*t2) + 
               Power(t1,2)*(292 - 59*t2 + 16*Power(t2,2)) + 
               2*t1*(110 + 96*t2 + 47*Power(t2,2)) + 
               s1*(29 + 90*Power(t1,3) + 32*t2 - 5*t1*(41 + 16*t2) - 
                  3*Power(t1,2)*(23 + 24*t2))) + 
            Power(s2,2)*t1*(-50 - 2*Power(t1,5) + 
               Power(t1,4)*(33 - 50*s1 + 2*Power(s1,2) - 12*t2) + 
               22*t2 - 12*Power(t2,2) + 
               Power(t1,3)*(-368 - 4*Power(s1,2) + 3*t2 + 
                  2*s1*(115 + 6*t2)) + 
               t1*(-91 - 71*t2 - 10*Power(t2,2) + s1*(75 + 28*t2)) + 
               Power(t1,2)*(490 + 126*t2 + 4*Power(t2,2) - 
                  s1*(235 + 52*t2))) - 
            Power(s2,6)*(5 - 137*Power(t1,3) + Power(s1,2)*(4 + 22*t1) + 
               41*t2 + 48*Power(t2,2) - Power(t1,2)*(86 + 21*t2) + 
               t1*(206 + 80*t2 + 34*Power(t2,2)) + 
               s1*(-23 + 78*Power(t1,2) - 24*t2 - t1*(99 + 92*t2))) + 
            Power(s2,4)*(23 + 4*Power(s1,2)*(-6 + t1)*Power(t1,2) + 
               21*Power(t1,5) - 71*t2 + Power(t1,4)*(98 + 9*t2) + 
               Power(t1,3)*(-63 + 134*t2 - 8*Power(t2,2)) + 
               t1*(202 + 226*t2 + 42*Power(t2,2)) - 
               Power(t1,2)*(633 + 170*t2 + 78*Power(t2,2)) + 
               s1*(-3 - 42*Power(t1,4) + 20*t2 + 
                  Power(t1,3)*(-89 + 12*t2) + 
                  Power(t1,2)*(467 + 92*t2) - t1*(109 + 100*t2))) + 
            Power(s2,3)*(13 - 2*Power(t1,6) - 5*t2 + 6*Power(t2,2) + 
               Power(t1,5)*(-9 + 6*s1 + 2*t2) + 
               Power(t1,4)*(-86 - 8*Power(s1,2) - 23*t2 + 
                  2*s1*(65 + 2*t2)) + 
               Power(t1,3)*(655 + 16*Power(s1,2) + 54*t2 + 
                  20*Power(t2,2) - 3*s1*(153 + 16*t2)) + 
               t1*(47 + 105*t2 + 22*Power(t2,2) - s1*(29 + 48*t2)) + 
               Power(t1,2)*(5*s1*(43 + 24*t2) - 
                  2*(205 + 168*t2 + 4*Power(t2,2))))))*R1q(s2))/
     (s*(-1 + s2)*s2*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       Power(s2 - t1,3)*(-s + s2 - t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(s - s1 + t2)) - 
    (8*(8*Power(s,5)*(s2 - t1)*Power(-1 + t1,2)*Power(t1,3) - 
         2*Power(s2 - t1,2)*(-1 + t1)*Power(t1,2)*
          (-1 + 2*Power(s2,2) + 3*t1 - s2*(3 + t1))*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2) + 
         Power(s,4)*Power(t1,2)*
          (4*Power(s2,3)*(1 - 4*t1 + Power(t1,2)) + 
            2*Power(s2,2)*(6 - 13*Power(t1,3) + t2 + 
               Power(t1,2)*(44 + t2) - t1*(25 + 2*t2)) + 
            s2*(47*Power(t1,4) + Power(t1,2)*(89 + 16*s1 - 17*t2) + 
               t2 + t1*(-21 - 8*s1 + 7*t2) + 
               Power(t1,3)*(-139 - 8*s1 + 9*t2)) + 
            t1*(-12 - 19*Power(t1,4) + t1*(27 + 8*s1 - 15*t2) + 
               Power(t1,3)*(49 + 8*s1 - 5*t2) + 5*t2 + 
               Power(t1,2)*(-37 - 16*s1 + 15*t2))) + 
         Power(s,3)*t1*(-4*Power(s2,4)*
             (-1 + Power(t1,3) + t1*(7 + t2) + 
               Power(t1,2)*(-9 - 2*s1 + t2)) + 
            Power(t1,2)*(-14*Power(t1,5) + 
               Power(t1,4)*(27 + 19*s1 - 13*t2) + 
               t1*(-24 - 31*s1 + 62*t2 + 2*s1*t2 - 7*Power(t2,2)) + 
               3*(11 - 9*t2 + Power(t2,2)) + 
               Power(t1,3)*(54 - 75*s1 + 36*t2 + Power(t2,2)) + 
               Power(t1,2)*(-52 + 79*s1 - 50*t2 - 2*s1*t2 + 
                  3*Power(t2,2))) - 
            Power(s2,2)*(62*Power(t1,5) - 
               3*Power(t1,4)*(96 + 17*s1 - 12*t2) + Power(t2,2) + 
               Power(t1,3)*(224 + 83*s1 - 46*t2 + 8*s1*t2 - 
                  5*Power(t2,2)) + 
               t1*(2 + 11*s1 - 4*t2 + 2*s1*t2 - Power(t2,2)) + 
               Power(t1,2)*(-96 - 43*s1 + 14*t2 - 10*s1*t2 + 
                  5*Power(t2,2))) + 
            2*Power(s2,3)*(1 + 11*Power(t1,4) + t2 + Power(t2,2) + 
               Power(t1,3)*(-79 - 14*s1 + 5*t2) - 
               t1*(23 + s1 + 6*t2 + s1*t2 + Power(t2,2)) + 
               Power(t1,2)*(66 + 8*t2 + s1*(7 + t2))) + 
            s2*t1*(-15 + 58*Power(t1,5) + 3*t2 + 4*Power(t2,2) + 
               Power(t1,4)*(-193 - 50*s1 + 43*t2) + 
               t1*(6 + 42*s1 - 44*t2 - 2*Power(t2,2)) - 
               2*Power(t1,2)*
                (10 - 50*t2 + Power(t2,2) + 3*s1*(20 + t2)) + 
               2*Power(t1,3)*(42 - 59*t2 + 3*s1*(24 + t2)))) - 
         Power(s,2)*(4*Power(s2,5)*t1*
             (2 + Power(t1,3) + Power(t1,2)*(4 + 4*s1 - 2*t2) + t2 - 
               t1*(7 + 2*s1 + t2)) - 
            2*Power(s2,4)*(-1 + 5*Power(t1,5) + 
               Power(t1,4)*(42 + 31*s1 - 16*t2) + 
               t1*(-3 + s1*(-1 + t2) - 4*t2 - 4*Power(t2,2)) - 
               Power(t1,3)*(54 + 3*Power(s1,2) - 2*s1*(-13 + t2) + 
                  t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(11 - Power(s1,2) + 5*t2 + 2*Power(t2,2) + 
                  s1*(12 + 5*t2))) + 
            Power(t1,3)*(3*Power(t1,6) - 16*(-1 + t2) + 
               Power(t1,5)*(7 - 14*s1 + 10*t2) - 
               Power(t1,4)*(166 + s1*(-78 + t2) + 8*t2 + Power(t2,2)) + 
               t1*(33 + 58*t2 - 5*Power(t2,2) + s1*(-36 + 5*t2)) + 
               Power(t1,3)*(336 + 2*Power(s1,2) + 12*t2 - 
                  5*Power(t2,2) + s1*(-142 + 7*t2)) - 
               Power(t1,2)*(229 + 2*Power(s1,2) + 72*t2 - 
                  11*Power(t2,2) + s1*(-130 + 11*t2))) + 
            s2*Power(t1,2)*(-24 - 23*Power(t1,6) + 
               Power(t1,5)*(63 + 56*s1 - 46*t2) + 29*t2 - 
               3*Power(t2,2) + 
               t1*(-77 - 126*t2 + 5*Power(t2,2) + s1*(66 + t2)) + 
               Power(t1,4)*(296 - 6*Power(s1,2) + 115*t2 - 
                  s1*(228 + 5*t2)) + 
               Power(t1,2)*(443 + 6*Power(s1,2) + 234*t2 - 
                  11*Power(t2,2) - s1*(274 + 9*t2)) + 
               Power(t1,3)*(-678 - 8*Power(s1,2) - 166*t2 + 
                  Power(t2,2) + s1*(340 + 29*t2))) + 
            Power(s2,2)*t1*(35*Power(t1,6) + 
               Power(t1,5)*(-193 - 100*s1 + 72*t2) - 
               2*(-1 + t2 + 2*Power(t2,2)) - 
               Power(t1,2)*(241 + 6*Power(s1,2) + 184*t2 + 
                  15*Power(t2,2) - 11*s1*(16 + 3*t2)) + 
               Power(t1,4)*(-76 + 18*Power(s1,2) - 168*t2 - 
                  3*Power(t2,2) + 3*s1*(86 + 3*t2)) + 
               t1*(67 + 52*t2 + 21*Power(t2,2) - s1*(32 + 9*t2)) + 
               Power(t1,3)*(406 + 12*Power(s1,2) + 214*t2 + 
                  25*Power(t2,2) - s1*(286 + 81*t2))) + 
            Power(s2,3)*(-9*Power(t1,6) + 
               Power(t1,5)*(191 + 104*s1 - 60*t2) + t2*(1 + t2) + 
               Power(t1,4)*(-128 - 18*Power(s1,2) + s1*(-152 + t2) + 
                  51*t2 + 6*Power(t2,2)) + 
               t1*(-13 - 8*t2 - 9*Power(t2,2) + s1*(2 + 3*t2)) - 
               Power(t1,3)*(62 + 8*Power(s1,2) + 30*t2 + 
                  29*Power(t2,2) - s1*(112 + 55*t2)) + 
               Power(t1,2)*(2*Power(s1,2) - s1*(34 + 11*t2) + 
                  7*(3 + 2*t2 + Power(t2,2))))) + 
         s*(s2 - t1)*(4*Power(s2,5)*(-1 + t1)*t1*
             (2*s1*t1 + (1 + t1)*(-1 + t1 - t2)) - 
            2*Power(s2,4)*(-2 + 5*Power(t1,5) + 
               Power(t1,4)*(13*s1 - 8*t2) - t2 + 
               t1*(11 + s1*(-2 + t2) + 4*t2 - 3*Power(t2,2)) + 
               Power(t1,2)*(-6 + 5*s1 + Power(s1,2) - t2 + 
                  3*Power(t2,2)) - 
               Power(t1,3)*(8 + 5*Power(s1,2) + s1*(16 - 7*t2) - 6*t2 + 
                  4*Power(t2,2))) + 
            Power(t1,2)*(5 - 6*t2 + 3*Power(t2,2) + 
               Power(t1,6)*(3 - 3*s1 + 2*t2) + 
               Power(t1,5)*(-64 - s1*(-23 + t2) + 10*t2) + 
               t1*(-52 + 13*s1 + 16*t2 - 3*s1*t2 - 10*Power(t2,2)) + 
               Power(t1,4)*(227 + 10*Power(s1,2) + 2*s1*(-30 + t2) - 
                  44*t2 - Power(t2,2)) + 
               Power(t1,3)*(-316 - 4*Power(s1,2) + 30*t2 + 
                  6*Power(t2,2) - 4*s1*(-25 + 7*t2)) + 
               Power(t1,2)*(197 + 2*Power(s1,2) - 8*t2 + 
                  10*Power(t2,2) + s1*(-73 + 14*t2))) + 
            Power(s2,3)*(4 + 6*Power(t1,6) - 
               2*Power(s1,2)*(t1 + 2*Power(t1,3) + 13*Power(t1,4)) + 
               Power(t1,5)*(22 - 19*t2) + 3*t2 + 3*Power(t2,2) + 
               Power(t1,4)*(2 + 5*t2 - Power(t2,2)) - 
               6*Power(t1,3)*(23 - t2 + 6*Power(t2,2)) + 
               4*Power(t1,2)*(39 + 13*t2 + 6*Power(t2,2)) - 
               t1*(52 + 47*t2 + 22*Power(t2,2)) + 
               s1*(1 + 37*Power(t1,5) - t2 - 2*Power(t1,2)*(4 + 7*t2) + 
                  t1*(13 + 14*t2) + 2*Power(t1,3)*(5 + 21*t2) + 
                  Power(t1,4)*(-53 + 23*t2))) + 
            s2*t1*(-2*Power(t1,7) + Power(t1,6)*(14 + 15*s1 - 13*t2) + 
               4*(-2 + t2)*t2 - 
               Power(t1,2)*(342 + 6*Power(s1,2) + 161*t2 + 
                  4*Power(t2,2) - 5*s1*(27 + 2*t2)) + 
               t1*(58 + 49*t2 - 5*Power(t2,2) - s1*(17 + 3*t2)) + 
               Power(t1,5)*(114 - 6*Power(s1,2) + 11*t2 + Power(t2,2) - 
                  s1*(59 + 3*t2)) + 
               2*Power(t1,3)*
                (323 + 4*Power(s1,2) + 60*t2 - 11*Power(t2,2) + 
                  s1*(-88 + 7*t2)) + 
               2*Power(t1,4)*(-244 - 14*Power(s1,2) + t2 - 
                  3*Power(t2,2) + s1*(51 + 23*t2))) + 
            Power(s2,2)*(1 + 2*Power(t1,7) + 2*t2 - Power(t2,2) + 
               Power(t1,6)*(-35 - 31*s1 + 18*t2) + 
               Power(t1,5)*(-54 + 65*s1 + 22*Power(s1,2) - 34*t2 - 
                  5*s1*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(213 - 75*s1 + 6*Power(s1,2) + 174*t2 - 
                  38*s1*t2 + 36*Power(t2,2)) - 
               2*Power(t1,3)*(226 + 2*Power(s1,2) + 109*t2 + 
                  Power(t2,2) - 5*s1*(8 + 3*t2)) + 
               Power(t1,4)*(357 + 24*Power(s1,2) + 82*t2 + 
                  23*Power(t2,2) - 6*s1*(7 + 15*t2)) + 
               t1*(s1*(3 + 7*t2) - 2*(16 + 12*t2 + 5*Power(t2,2))))))*
       R1q(t1))/(s*(-1 + s2)*Power(s2 - t1,3)*(-s + s2 - t1)*
       Power(-1 + t1,2)*t1*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*
       (s - s1 + t2)) - (8*(6*Power(s,6)*t1 + 
         Power(s,5)*(s2*(6 - 18*t1) + 11*Power(t1,2) + 2*Power(t2,2) - 
            t1*(38 + 6*s1 + t2)) + 
         Power(s,4)*(12*Power(s2,2)*(-1 + t1) + 6*Power(t1,3) - 
            4*(-1 + t2)*t2 - 3*Power(t1,2)*(15 + 3*s1 + t2) + 
            t1*(25 + 46*s1 - 21*t2 + 3*Power(t2,2)) + 
            s2*(-32 - 20*Power(t1,2) - 5*t2 - 12*Power(t2,2) + 
               5*t1*(21 + t2) + s1*(-6 + 15*t1 + 4*t2))) + 
         Power(s,2)*(-((1 + s1)*Power(t1,4)) - 
            6*Power(s2,4)*(-2 + 3*t1) + 2*t2*(-4 + 5*t2) + 
            t1*(-82 + 6*Power(s1,2) + s1*(12 - 8*t2) - 17*t2 - 
               2*Power(t2,2)) + 
            Power(t1,3)*(-13 - 2*Power(s1,2) + s1*(22 + t2)) + 
            Power(t1,2)*(90 + 6*Power(s1,2) + 35*t2 + Power(t2,2) - 
               s1*(54 + 7*t2)) + 
            Power(s2,3)*(-50 - 8*Power(s1,2) + 53*t1 + 16*Power(t1,2) - 
               32*t2 + 28*t1*t2 - 32*Power(t2,2) + 
               s1*(21 - 26*t1 + 36*t2)) + 
            Power(s2,2)*(129 + Power(s1,2)*(6 - 4*t1) + 2*Power(t1,3) + 
               s1*(-99 + 18*Power(t1,2) + t1*(118 - 33*t2) - 13*t2) + 
               96*t2 + 9*Power(t2,2) - 5*Power(t1,2)*(10 + 7*t2) + 
               3*t1*(-37 - 25*t2 + 12*Power(t2,2))) + 
            s2*(63 - 2*Power(t1,4) + 
               2*Power(s1,2)*(-1 + 2*t1 + 5*Power(t1,2)) + 33*t2 + 
               9*Power(t2,2) + Power(t1,3)*(4 + 7*t2) + 
               Power(t1,2)*(133 + 63*t2 - 5*Power(t2,2)) + 
               t1*(-239 + 17*t2 + 6*Power(t2,2)) + 
               s1*(-109*Power(t1,2) + 3*Power(t1,3) + t1*(71 - 16*t2) - 
                  8*(3 + t2)))) + 
         Power(s,3)*(12*Power(s2,3)*t1 + Power(t1,4) - 
            2*Power(t1,3)*(6 + 2*s1 + t2) + 
            t1*(78 - 68*s1 - 2*Power(s1,2) + 55*t2 - 4*Power(t2,2)) - 
            2*(-1 + 2*t2 + Power(t2,2)) + 
            Power(t1,2)*(-20 - 2*Power(s1,2) - 16*t2 + Power(t2,2) + 
               s1*(58 + t2)) + 
            Power(s2,2)*(58 + 2*Power(s1,2) + s1*(5 - 20*t2) + 20*t2 + 
               28*Power(t2,2) - t1*(105 + 16*t2)) + 
            s2*(-11 + 2*Power(s1,2)*t1 - 8*Power(t1,3) - 38*t2 + 
               7*Power(t2,2) + Power(t1,2)*(69 + 17*t2) + 
               t1*(60 + 72*t2 - 18*Power(t2,2)) + 
               s1*(44 + 8*Power(t1,2) + t1*(-131 + 11*t2)))) - 
         Power(-1 + s2,2)*((-1 + s1)*Power(t1,4) - 2*Power(-1 + t2,2) + 
            Power(t1,2)*(-13 + s1 - 4*Power(s1,2) + 18*t2 + s1*t2 - 
               3*Power(t2,2)) + 
            Power(s2,3)*(-2 + 4*Power(s1,2) + s1*(-5 + 5*t1 - 8*t2) + 
               t1*(2 - 7*t2) + 7*t2 + 4*Power(t2,2)) + 
            Power(t1,3)*(-1 + 2*Power(s1,2) + 12*t2 - s1*(12 + t2)) + 
            t1*(17 - 2*Power(s1,2) - 34*t2 + Power(t2,2) + 
               2*s1*(5 + 4*t2)) + 
            s2*(-15 - 2*Power(s1,2)*(-1 - 6*t1 + Power(t1,2)) + 
               Power(t1,3)*(2 - 3*t2) + 30*t2 + Power(t2,2) + 
               Power(t1,2)*(-1 - 19*t2 + 3*Power(t2,2)) + 
               2*t1*(7 - 4*t2 + 4*Power(t2,2)) + 
               s1*(Power(t1,3) + Power(t1,2)*(21 - 2*t2) - 
                  2*(5 + 4*t2) - 2*t1*(6 + 7*t2))) + 
            Power(s2,2)*(1 - 4*Power(s1,2)*(2 + t1) - 14*t2 - 
               3*Power(t2,2) + Power(t1,2)*(-3 + 10*t2) + 
               t1*(2 + 4*t2 - 9*Power(t2,2)) + 
               s1*(11 - 7*Power(t1,2) + 13*t2 + t1*(-4 + 11*t2)))) + 
         s*(-4 + 6*Power(s2,5)*(-1 + t1) + (-1 + 2*s1)*Power(t1,4) + 
            12*t2 - 8*Power(t2,2) + 
            Power(t1,3)*(-6 + 4*Power(s1,2) + 22*t2 - 2*s1*(13 + t2)) - 
            Power(t1,2)*(5 + 8*Power(s1,2) + 6*t2 + 5*Power(t2,2) - 
               s1*(2 + 7*t2)) + 
            t1*(32 - 6*Power(s1,2) - 50*t2 + 4*Power(t2,2) + 
               2*s1*(13 + 8*t2)) + 
            Power(s2,4)*(22 + 10*Power(s1,2) - 7*Power(t1,2) + 
               s1*(-25 + 22*t1 - 28*t2) + 24*t2 + 18*Power(t2,2) - 
               t1*(13 + 23*t2)) + 
            s2*(-11 - 4*Power(t1,4) + 
               2*Power(s1,2)*(2 + 3*t1 + Power(t1,2) + 2*Power(t1,3)) + 
               8*t2 + Power(t2,2) + 2*Power(t1,3)*(6 + 11*t2) + 
               Power(t1,2)*(-19 + 89*t2 - 2*Power(t2,2)) + 
               2*t1*(-6 - 41*t2 + Power(t2,2)) + 
               s1*(2*Power(t1,4) + t1*(57 - 9*t2) - 
                  8*Power(t1,2)*(2 + t2) - 4*(5 + t2) - 
                  2*Power(t1,3)*(10 + t2))) + 
            Power(s2,2)*(Power(t1,4) + 
               Power(s1,2)*(8 + 10*t1 - 10*Power(t1,2)) - 
               2*Power(t1,3)*(1 + 4*t2) + 
               2*(-8 + 73*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-52 - 78*t2 + 7*Power(t2,2)) + 
               t1*(91 - 67*t2 + 24*Power(t2,2)) + 
               s1*(-75 + 2*Power(t1,3) + Power(t1,2)*(82 - 3*t2) - 
                  22*t2 - 10*t1*(3 + 2*t2))) + 
            Power(s2,3)*(-49 - 2*Power(s1,2)*(11 + t1) - 82*t2 - 
               23*Power(t2,2) + Power(t1,2)*(23 + 31*t2) + 
               t1*(20 + 42*t2 - 30*Power(t2,2)) + 
               s1*(88 - 24*Power(t1,2) + 42*t2 + t1*(-47 + 33*t2)))))*R2q(s)\
)/(s*(-1 + s2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-s + s2 - t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))*(s - s1 + t2)) \
+ (8*(4*Power(s,8)*Power(t1,2) + 
         Power(s,7)*(10*Power(t1,3) + (-3 + s1)*t1*t2 + Power(t2,2) + 
            s2*t1*(8 - 21*t1 + t2) + Power(t1,2)*(-28 - 5*s1 + 2*t2)) + 
         Power(s,6)*(8*Power(t1,4) - 2*Power(t1,3)*(25 + 6*s1 - 2*t2) + 
            2*(1 - 2*t2)*t2 + 
            Power(s2,2)*(4 + 43*Power(t1,2) + t1*(-35 + s1 - 6*t2) + 
               t2) + Power(t1,2)*
             (40 + 43*s1 + Power(s1,2) - 32*t2 + 2*s1*t2) - 
            t1*(1 + s1 - 18*t2 + 6*s1*t2 - 4*Power(t2,2)) + 
            s2*(-41*Power(t1,3) + Power(t1,2)*(127 + 25*s1 - 9*t2) + 
               (-5 + 3*s1 - 9*t2)*t2 + 
               t1*(-46 - 12*s1 + Power(s1,2) + 21*t2 - 7*s1*t2))) - 
         Power(-1 + s2,3)*(2*(-1 + s1)*Power(t1,5) + 
            Power(t1,4)*(-3 + s1*(5 - 2*t2)) + Power(-1 + t2,2) - 
            t1*(-1 + t2)*(-10 + 2*s1 + 3*t2) + 
            Power(t1,2)*(10 - 7*s1 + Power(s1,2) - 8*t2 + 
               4*Power(t2,2)) + 
            Power(t1,3)*(4 + 3*Power(s1,2) - 3*t2 + 2*Power(t2,2) - 
               2*s1*(1 + 2*t2)) + 
            Power(s2,4)*(4*Power(s1,2) + 
               s1*(-3 + 2*t1 + Power(t1,2) - 8*t2) + 
               t2*(3 - 2*t1 - Power(t1,2) + 4*t2)) + 
            Power(s2,2)*(-Power(t1,4) + 
               Power(s1,2)*(1 + 11*t1 + 14*Power(t1,2) - 
                  2*Power(t1,3)) - (-7 + t2)*t2 - 
               5*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(13 + t2 + 8*Power(t2,2)) + 
               t1*(-7 - 3*t2 + 17*Power(t2,2)) + 
               s1*(-5 + 12*Power(t1,3) + Power(t1,4) + t1*(8 - 26*t2) - 
                  2*t2 - 4*Power(t1,2)*(4 + 5*t2))) + 
            s2*(9 + Power(s1,2)*t1*
                (-2 - 10*t1 - 5*Power(t1,2) + Power(t1,3)) - 11*t2 + 
               2*Power(t2,2) + Power(t1,4)*(5 + 2*t2) + 
               Power(t1,2)*(2 + t2 - 11*Power(t2,2)) + 
               t1*(-12 + 5*t2 - 5*Power(t2,2)) + 
               Power(t1,3)*(-4 + 3*t2 - 2*Power(t2,2)) + 
               2*s1*(-1 - 4*Power(t1,4) + t2 + 4*Power(t1,3)*t2 + 
                  t1*(6 + t2) + Power(t1,2)*(-1 + 10*t2))) + 
            Power(s2,3)*(3 + Power(s1,2)*(-4 - 13*t1 + Power(t1,2)) + 
               t2 - 6*Power(t2,2) + Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(1 + 7*t2 - Power(t2,2)) - 
               t1*(5 + 9*t2 + 9*Power(t2,2)) - 
               2*s1*(2 + 4*Power(t1,2) + Power(t1,3) - 5*t2 - 
                  t1*(7 + 11*t2)))) + 
         Power(s,3)*(-1 + 2*Power(t1,5)*(-9 + 5*s1 - 2*t2) + 12*t2 - 
            15*Power(t2,2) + Power(t1,4)*
             (212 - 76*s1 - 7*Power(s1,2) + 44*t2) + 
            Power(s2,5)*(-4 + s1*(6 - 10*t1) - 4*t1 + 11*Power(t1,2) - 
               10*t2 + 15*t1*t2) + 
            Power(t1,2)*(253 - 47*Power(s1,2) + s1*(95 - 26*t2) + 
               118*t2 + 8*Power(t2,2)) + 
            t1*(59 - 91*t2 + 2*Power(t2,2) + 7*s1*(-4 + 5*t2)) + 
            Power(t1,3)*(37*Power(s1,2) + 6*s1*(2 + 3*t2) - 
               4*(119 + 24*t2 + 2*Power(t2,2))) + 
            s2*(2*Power(t1,5) + 
               Power(s1,2)*t1*(40 - 74*t1 + 23*Power(t1,2)) + 
               2*Power(t1,4)*(54 + 35*t2) - 
               2*Power(t1,3)*(383 + 71*t2 + 4*Power(t2,2)) - 
               2*(9 - 20*t2 + 7*Power(t2,2)) - 
               t1*(411 + 199*t2 + 14*Power(t2,2)) + 
               Power(t1,2)*(1205 + 140*t2 + 22*Power(t2,2)) + 
               2*s1*(4 - 57*Power(t1,4) + 2*Power(t1,5) - 4*t2 + 
                  Power(t1,3)*(190 + 3*t2) + 3*t1*(3 + 8*t2) - 
                  Power(t1,2)*(119 + 12*t2))) + 
            Power(s2,3)*(60 + 81*Power(t1,3) - 8*Power(t1,4) + 
               Power(s1,2)*(10 - 75*t1 + 36*Power(t1,2)) + 38*t2 + 
               10*Power(t2,2) + 
               Power(t1,2)*(134 + 330*t2 - 10*Power(t2,2)) - 
               2*t1*(127 + 217*t2 + 65*Power(t2,2)) + 
               2*s1*(-50 + 11*Power(t1,3) - 3*t2 + 77*t1*(3 + t2) - 
                  Power(t1,2)*(151 + 13*t2))) + 
            Power(s2,2)*(128 + 2*Power(t1,5) + 
               Power(s1,2)*(-5 + 15*t1 + 19*Power(t1,2) - 
                  16*Power(t1,3)) + 58*t2 - 2*Power(t2,2) + 
               Power(t1,4)*(-29 + 6*t2) - Power(t1,3)*(263 + 238*t2) - 
               2*t1*(376 + 103*t2 + 17*Power(t2,2)) + 
               Power(t1,2)*(883 + 338*t2 + 44*Power(t2,2)) - 
               2*s1*(13*Power(t1,4) + 3*(9 + t2) - 
                  3*Power(t1,3)*(55 + 2*t2) + 
                  5*Power(t1,2)*(67 + 4*t2) - t1*(184 + 19*t2))) + 
            Power(s2,4)*(-14 - 4*Power(t1,3) - 
               4*Power(s1,2)*(-7 + 4*t1) + t1*(76 - 125*t2) + 122*t2 + 
               85*Power(t2,2) - 2*Power(t1,2)*(32 + 5*t2) + 
               s1*(15*Power(t1,2) - 8*(6 + 13*t2) + t1*(32 + 25*t2)))) + 
         Power(s,5)*(1 + 2*Power(t1,5) - 6*t2 + 4*Power(t2,2) + 
            Power(t1,4)*(-27 - 9*s1 + 2*t2) - 
            t1*(-9 + s1 + 42*t2 - 17*s1*t2 + 15*Power(t2,2)) + 
            Power(t1,3)*(2 + 2*Power(s1,2) - 47*t2 + s1*(82 + t2)) + 
            Power(t1,2)*(94 - 10*Power(s1,2) + 111*t2 + 4*Power(t2,2) - 
               2*s1*(57 + 4*t2)) + 
            Power(s2,3)*(-14 + s1 - 5*s1*t1 - 40*Power(t1,2) - 5*t2 + 
               t1*(56 + 15*t2)) + 
            Power(s2,2)*(-17 + Power(s1,2)*(2 - 6*t1) + 
               61*Power(t1,3) + t1*(145 - 67*t2) + 31*t2 + 
               34*Power(t2,2) + Power(t1,2)*(-221 + 14*t2) + 
               s1*(-9 - 44*Power(t1,2) - 22*t2 + t1*(47 + 20*t2))) + 
            s2*(-3 - 4*Power(s1,2)*t1 - 25*Power(t1,4) + 5*t2 + 
               23*Power(t2,2) - 7*Power(t1,3)*(-21 + 2*t2) + 
               t1*(24 - 131*t2 - 29*Power(t2,2)) - 
               Power(t1,2)*(86 - 154*t2 + Power(t2,2)) + 
               s1*(1 + 46*Power(t1,3) - 9*t2 - 
                  Power(t1,2)*(204 + 11*t2) + t1*(101 + 36*t2)))) + 
         Power(s,4)*(-2 - 2*(2 + s1)*Power(t1,5) + 
            Power(t1,4)*(-41 + 51*s1 + Power(s1,2) - 24*t2) + 2*t2 + 
            5*Power(t2,2) + t1*
             (-31 + s1*(14 - 30*t2) + 65*t2 + 17*Power(t2,2)) + 
            Power(t1,3)*(310 - 16*Power(s1,2) + 111*t2 + 
               2*Power(t2,2) - 2*s1*(77 + 2*t2)) + 
            2*Power(s2,4)*(8 + 5*Power(t1,2) + s1*(-2 + 5*t1) + 5*t2 - 
               t1*(17 + 10*t2)) + 
            Power(t1,2)*(-309 + 32*Power(s1,2) - 158*t2 - 
               12*Power(t2,2) + s1*(79 + 18*t2)) + 
            Power(s2,3)*(32 - 34*Power(t1,3) + 
               2*Power(s1,2)*(-6 + 7*t1) - 5*Power(t1,2)*(-36 + t2) - 
               85*t2 - 70*Power(t2,2) + 6*t1*(-27 + 20*t2) + 
               s1*(33 + 25*Power(t1,2) + 66*t2 - 2*t1*(32 + 15*t2))) + 
            s2*(5 - 4*Power(t1,5) + 4*Power(s1,2)*t1*(-2 + 5*t1) - 
               6*Power(t1,4)*(-8 + t2) - 9*t2 - 11*Power(t2,2) + 
               2*Power(t1,3)*(67 + 82*t2) - 
               Power(t1,2)*(581 + 335*t2 + 19*Power(t2,2)) + 
               t1*(230 + 226*t2 + 56*Power(t2,2)) + 
               s1*(-4 + 27*Power(t1,4) + 10*t2 - 
                  6*Power(t1,3)*(45 + t2) + 
                  3*Power(t1,2)*(157 + 8*t2) - 2*t1*(90 + 31*t2))) + 
            Power(s2,2)*(-1 + 26*Power(t1,4) + 
               Power(s1,2)*(-4 + 28*t1 - 15*Power(t1,2)) - 38*t2 - 
               42*Power(t2,2) + 3*Power(t1,3)*(-52 + 5*t2) + 
               Power(t1,2)*(-21 - 308*t2 + 5*Power(t2,2)) + 
               t1*(101 + 347*t2 + 85*Power(t2,2)) + 
               s1*(43 - 60*Power(t1,3) + 30*t2 + 
                  Power(t1,2)*(359 + 24*t2) - 2*t1*(175 + 48*t2)))) + 
         Power(s,2)*(5 - 66*t1 + 25*s1*t1 - 11*Power(t1,2) - 
            163*s1*Power(t1,2) + 34*Power(s1,2)*Power(t1,2) + 
            122*Power(t1,3) + 172*s1*Power(t1,3) - 
            31*Power(s1,2)*Power(t1,3) - 136*Power(t1,4) - 
            2*s1*Power(t1,4) + 13*Power(s1,2)*Power(t1,4) + 
            46*Power(t1,5) - 12*s1*Power(t1,5) - 18*t2 + 96*t1*t2 - 
            26*s1*t1*t2 - 66*Power(t1,2)*t2 + 20*s1*Power(t1,2)*t2 + 
            40*Power(t1,3)*t2 - 32*s1*Power(t1,3)*t2 - 
            48*Power(t1,4)*t2 + 2*s1*Power(t1,4)*t2 + 4*Power(t1,5)*t2 + 
            14*Power(t2,2) - 18*t1*Power(t2,2) + 
            8*Power(t1,2)*Power(t2,2) + 12*Power(t1,3)*Power(t2,2) + 
            Power(s2,6)*(-4 - 9*Power(t1,2) + s1*(-4 + 5*t1) + 
               t1*(13 - 6*t2) + 5*t2) + 
            s2*(33 - 180*Power(t1,4) + 
               Power(s1,2)*t1*
                (-47 + 77*t1 - 58*Power(t1,2) + 7*Power(t1,3)) - 
               53*t2 + 13*Power(t2,2) + 2*Power(t1,5)*(5 + 6*t2) + 
               t1*(134 + 99*t2 - 20*Power(t2,2)) + 
               Power(t1,2)*(-540 + 35*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*(631 - 60*t2 + 8*Power(t2,2)) - 
               s1*(10 + 10*Power(t1,5) + Power(t1,2)*(271 - 40*t2) + 
                  2*Power(t1,4)*(-48 + t2) - 9*t2 + 
                  Power(t1,3)*(68 + 6*t2) + t1*(-170 + 11*t2))) - 
            Power(s2,2)*(102 + 
               Power(s1,2)*(-15 + 63*t1 - 67*Power(t1,2) + 
                  Power(t1,3) + 4*Power(t1,4)) + 41*t2 - 
               6*Power(t2,2) + Power(t1,4)*(77 + 78*t2) + 
               Power(t1,3)*(-359 + 6*t2 - 12*Power(t2,2)) - 
               2*Power(t1,2)*(-433 + 80*t2 + 7*Power(t2,2)) + 
               t1*(-630 + 54*t2 + 20*Power(t2,2)) + 
               s1*(242*Power(t1,3) - 84*Power(t1,4) + 2*Power(t1,5) + 
                  2*(13 + t2) - 7*t1*(7 + 2*t2) + 
                  2*Power(t1,2)*(-98 + 11*t2))) + 
            Power(s2,5)*(11*Power(t1,3) + Power(s1,2)*(-32 + 9*t1) + 
               Power(t1,2)*(5 + 13*t2) - t2*(91 + 61*t2) + 
               t1*(-16 + 73*t2) + 
               s1*(36 - 25*Power(t1,2) + 91*t2 - t1*(2 + 11*t2))) + 
            Power(s2,4)*(-2*Power(t1,4) + 
               Power(s1,2)*(4 + 95*t1 - 33*Power(t1,2)) - 
               Power(t1,3)*(33 + 10*t2) + 
               2*Power(t1,2)*(-37 - 101*t2 + 5*Power(t2,2)) + 
               4*(-9 + 8*t2 + 13*Power(t2,2)) + 
               t1*(145 + 264*t2 + 110*Power(t2,2)) + 
               s1*(56 + 14*Power(t1,3) - 76*t2 + 
                  Power(t1,2)*(139 + 14*t2) - 3*t1*(99 + 52*t2))) + 
            Power(s2,3)*(-208 + 
               13*Power(s1,2)*
                (1 - 2*t1 - 5*Power(t1,2) + 2*Power(t1,3)) + 
               Power(t1,4)*(19 - 2*t2) - 42*t2 + 8*Power(t2,2) + 
               2*Power(t1,3)*(79 + 90*t2) - 
               4*t1*(-112 + 30*t2 + 21*Power(t2,2)) - 
               Power(t1,2)*(409 + 116*t2 + 58*Power(t2,2)) + 
               2*s1*(26 + 3*Power(t1,4) - 3*t2 - 
                  Power(t1,3)*(98 + 5*t2) + Power(t1,2)*(206 + 30*t2) + 
                  t1*(-91 + 55*t2)))) + 
         s*(-4 + 40*t1 - 11*s1*t1 - 53*Power(t1,2) + 72*s1*Power(t1,2) - 
            11*Power(s1,2)*Power(t1,2) + 74*Power(t1,3) - 
            98*s1*Power(t1,3) + 5*Power(s1,2)*Power(t1,3) - 
            57*Power(t1,4) + 35*s1*Power(t1,4) - 
            7*Power(s1,2)*Power(t1,4) - 2*s1*Power(t1,5) + 10*t2 - 
            56*t1*t2 + 11*s1*t1*t2 + 33*Power(t1,2)*t2 - 
            6*s1*Power(t1,2)*t2 - 9*Power(t1,3)*t2 + 
            21*s1*Power(t1,3)*t2 + 34*Power(t1,4)*t2 - 8*Power(t1,5)*t2 - 
            6*Power(t2,2) + 13*t1*Power(t2,2) - 
            12*Power(t1,2)*Power(t2,2) - 8*Power(t1,3)*Power(t2,2) + 
            Power(s2,7)*(-1 + t1)*(-2 - s1 + 2*t1 + t2) + 
            Power(s2,6)*(-1 - 2*Power(s1,2)*(-9 + t1) - 3*Power(t1,3) + 
               t1*(3 - 21*t2) + Power(t1,2)*(1 - 6*t2) + 31*t2 + 
               24*Power(t2,2) + 
               s1*(-15 + t1 + 10*Power(t1,2) - 42*t2 + 2*t1*t2)) + 
            Power(s2,2)*(-6 + 
               Power(s1,2)*(-9 + 13*t1 - 68*Power(t1,2) + 
                  33*Power(t1,3) - 3*Power(t1,4)) - 21*t2 + 
               8*Power(t2,2) - 8*Power(t1,5)*(1 + t2) - 
               Power(t1,4)*(45 + 34*t2) + 
               t1*(158 + 53*t2 - 22*Power(t2,2)) + 
               4*Power(t1,3)*(-28 + 17*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(13 + 66*t2 + 12*Power(t2,2)) + 
               s1*(41 + 30*Power(t1,4) + 2*Power(t1,5) + 
                  Power(t1,3)*(2 - 16*t2) + 8*t2 + 15*t1*(-11 + 2*t2) + 
                  Power(t1,2)*(-34 + 6*t2))) - 
            s2*(23 + 5*Power(s1,2)*t1*
                (-4 + 2*t1 - 9*Power(t1,2) + 2*Power(t1,3)) - 27*t2 + 
               Power(t2,2) - 2*Power(t1,5)*(3 + 8*t2) + 
               Power(t1,4)*(-63 + 34*t2) + 
               Power(t1,2)*(178 + 10*t2 - 27*Power(t2,2)) + 
               Power(t1,3)*(-101 + 56*t2 - 8*Power(t2,2)) - 
               t1*(31 + 17*t2 + 3*Power(t2,2)) + 
               s1*(4*Power(t1,4) + 7*(-1 + t2) + 
                  2*Power(t1,3)*(37 + 7*t2) + t1*(103 + 12*t2) + 
                  Power(t1,2)*(-214 + 49*t2))) + 
            Power(s2,4)*(53 - 12*Power(t1,4) + 
               Power(s1,2)*(9 + 69*t1 + 47*Power(t1,2) - 
                  14*Power(t1,3)) + 52*t2 + 30*Power(t2,2) - 
               Power(t1,3)*(39 + 67*t2) + 
               Power(t1,2)*(39 - 21*t2 + 40*Power(t2,2)) + 
               t1*(-41 + 160*t2 + 121*Power(t2,2)) + 
               s1*(3*Power(t1,4) - 54*(1 + t2) + 
                  3*Power(t1,3)*(24 + t2) - 8*Power(t1,2)*(11 + 7*t2) - 
                  3*t1*(19 + 65*t2))) + 
            Power(s2,3)*(-36 + 2*Power(t1,5) + 
               Power(s1,2)*(-8 + 21*t1 - 66*Power(t1,2) - 
                  5*Power(t1,3) + 4*Power(t1,4)) - 51*t2 + 
               2*Power(t2,2) + 34*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(31 + 58*t2 - 8*Power(t2,2)) - 
               2*Power(t1,2)*(-36 + 65*t2 + 31*Power(t2,2)) - 
               t1*(103 + 87*t2 + 66*Power(t2,2)) + 
               s1*(47 - 32*Power(t1,4) + 6*Power(t1,3)*(-3 + t2) + 
                  4*Power(t1,2)*(16 + 27*t2) + t1*(115 + 74*t2))) + 
            Power(s2,5)*(-1 + Power(t1,4) + 
               Power(s1,2)*(-26 - 57*t1 + 12*Power(t1,2)) - 47*t2 - 
               57*Power(t2,2) + 6*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(8 + 68*t2 - 5*Power(t2,2)) - 
               t1*(20 + 67*t2 + 49*Power(t2,2)) + 
               s1*(5 - 12*Power(t1,3) + 95*t2 - Power(t1,2)*(46 + 3*t2) + 
                  t1*(93 + 90*t2)))))*T2q(s2,s))/
     (s*(-1 + s2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*
       (-s + s2 - t1)*Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*
       (s - s1 + t2)) + (8*(-4*Power(s,6)*(1 + s2)*(s2 - t1)*
          Power(t1,2) + 2*Power(s,5)*t1*
          (Power(s2,3)*(-4 + 8*t1) + 
            s2*t1*(8 - 2*s1*(-1 + t1) + 7*Power(t1,2) + t1*(-16 + t2) - 
               4*t2) + t1*(2 + (3 - 2*s1)*t1 + 6*Power(t1,2) - t2) - 
            2*Power(s2,2)*(2 + 8*Power(t1,2) + t1*(-6 - s1 + t2))) + 
         Power(s,4)*(Power(s2,4)*(-4 - 25*Power(t1,2) + t1*(24 + t2)) + 
            Power(s2,3)*(-4 + 77*Power(t1,3) + Power(t2,2) + 
               Power(t1,2)*(-68 - 13*s1 + 11*t2) + 
               t1*(20 - 11*t2 + s1*(8 + t2))) + 
            Power(t1,2)*((12 + s1)*Power(t1,3) - 
               t1*(60 + 2*s1*(-9 + t2) - 12*t2 + Power(t2,2)) - 
               2*(7 - 6*t2 + Power(t2,2)) + 
               Power(t1,2)*(34 + 3*t2 - s1*(10 + t2))) - 
            Power(s2,2)*t1*(-12 + 71*Power(t1,3) + 16*t2 + 
               3*Power(t2,2) + Power(t1,2)*(-84 + 17*t2) + 
               t1*(20 - 45*t2 + 2*Power(t2,2)) + 
               s1*(-8 - 27*Power(t1,2) + t1*(36 + t2))) + 
            s2*t1*(8 + 19*Power(t1,4) - 4*t2 + 
               Power(t1,3)*(-52 - 15*s1 + 5*t2) + 
               t1*(28 + 2*s1*(-13 + t2) + 36*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(-36 - 29*t2 + s1*(38 + t2)))) + 
         Power(s2 - t1,3)*(2*(-1 + s1)*Power(t1,5) + 
            Power(t1,4)*(-3 + s1*(5 - 2*t2)) + Power(-1 + t2,2) - 
            t1*(-1 + t2)*(-10 + 2*s1 + 3*t2) + 
            Power(t1,2)*(10 - 7*s1 + Power(s1,2) - 8*t2 + 
               4*Power(t2,2)) + 
            Power(t1,3)*(4 + 3*Power(s1,2) - 3*t2 + 2*Power(t2,2) - 
               2*s1*(1 + 2*t2)) + 
            Power(s2,4)*(4*Power(s1,2) + 
               s1*(-3 + 2*t1 + Power(t1,2) - 8*t2) + 
               t2*(3 - 2*t1 - Power(t1,2) + 4*t2)) + 
            Power(s2,2)*(-Power(t1,4) + 
               Power(s1,2)*(1 + 11*t1 + 14*Power(t1,2) - 
                  2*Power(t1,3)) - (-7 + t2)*t2 - 
               5*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(13 + t2 + 8*Power(t2,2)) + 
               t1*(-7 - 3*t2 + 17*Power(t2,2)) + 
               s1*(-5 + 12*Power(t1,3) + Power(t1,4) + t1*(8 - 26*t2) - 
                  2*t2 - 4*Power(t1,2)*(4 + 5*t2))) + 
            s2*(9 + Power(s1,2)*t1*
                (-2 - 10*t1 - 5*Power(t1,2) + Power(t1,3)) - 11*t2 + 
               2*Power(t2,2) + Power(t1,4)*(5 + 2*t2) + 
               Power(t1,2)*(2 + t2 - 11*Power(t2,2)) + 
               t1*(-12 + 5*t2 - 5*Power(t2,2)) + 
               Power(t1,3)*(-4 + 3*t2 - 2*Power(t2,2)) + 
               2*s1*(-1 - 4*Power(t1,4) + t2 + 4*Power(t1,3)*t2 + 
                  t1*(6 + t2) + Power(t1,2)*(-1 + 10*t2))) + 
            Power(s2,3)*(3 + Power(s1,2)*(-4 - 13*t1 + Power(t1,2)) + 
               t2 - 6*Power(t2,2) + Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(1 + 7*t2 - Power(t2,2)) - 
               t1*(5 + 9*t2 + 9*Power(t2,2)) - 
               2*s1*(2 + 4*Power(t1,2) + Power(t1,3) - 5*t2 - 
                  t1*(7 + 11*t2)))) - 
         Power(s,2)*(s2 - t1)*
          (Power(s2,5)*(s1*(-1 + 2*t1) + (-2 + 3*t1)*(-3 + 3*t1 - t2)) + 
            s2*(3*Power(t1,6) - 2*Power(t1,5)*(13 + 7*s1 - t2) + 
               Power(t1,3)*(353 - 5*Power(s1,2) + s1*(-56 + t2) + 
                  8*t2 - 5*Power(t2,2)) + 2*(3 - 4*t2 + Power(t2,2)) + 
               Power(t1,2)*(-375 + 8*Power(s1,2) + s1*(51 - 28*t2) + 
                  102*t2 + 22*Power(t2,2)) - 
               Power(t1,4)*(91 + Power(s1,2) + 53*t2 + Power(t2,2) - 
                  3*s1*(16 + t2)) + 
               2*t1*(41 - 10*t2 - 4*Power(t2,2) + 2*s1*(-7 + 3*t2))) + 
            t1*((9 + s1)*Power(t1,5) + 
               Power(t1,4)*(8 - 2*Power(s1,2) - s1*(-2 + t2) + 15*t2) + 
               2*(-1 + Power(t2,2)) - 
               Power(t1,2)*(-202 + 4*Power(s1,2) + s1*(22 - 15*t2) + 
                  88*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(-122 + 5*Power(s1,2) + 29*t2 - 
                  2*Power(t2,2) + s1*(-7 + 2*t2)) + 
               t1*(-55 + 24*t2 + 4*Power(t2,2) - 2*s1*(-9 + 5*t2))) + 
            Power(s2,4)*(-11 - 38*Power(t1,3) + 
               Power(s1,2)*(-2 + 3*t1) - 23*t2 - 13*Power(t2,2) + 
               Power(t1,2)*(51 + 7*t2) + t1*(-2 + 22*t2) + 
               s1*(9 + Power(t1,2) + 13*t2 - t1*(12 + 5*t2))) + 
            Power(s2,2)*(-21 - 26*Power(t1,5) + 
               Power(s1,2)*t1*(-4 - 7*t1 + 11*Power(t1,2)) + 
               Power(t1,4)*(52 - 9*t2) - 16*t2 + 10*Power(t2,2) + 
               t1*(176 + 42*t2 - 43*Power(t2,2)) + 
               Power(t1,3)*(172 + 89*t2 - 4*Power(t2,2)) - 
               Power(t1,2)*(345 + 128*t2 + Power(t2,2)) + 
               s1*(30*Power(t1,4) - 2*(-5 + t2) - 
                  4*Power(t1,3)*(29 + 2*t2) + 5*t1*(-8 + 3*t2) + 
                  Power(t1,2)*(130 + 17*t2))) + 
            Power(s2,3)*(-29 + Power(s1,2)*(9 - 11*t1)*t1 + 
               52*Power(t1,4) - 4*t2 + Power(t1,3)*(-71 + 3*t2) + 
               Power(t1,2)*(-87 - 87*t2 + 11*Power(t2,2)) + 
               t1*(135 + 94*t2 + 31*Power(t2,2)) + 
               s1*(11 - 20*Power(t1,3) - 2*t2 + 
                  Power(t1,2)*(79 + 11*t2) - t1*(76 + 33*t2)))) + 
         s*Power(s2 - t1,2)*(2*Power(t1,6) - 
            Power(t1,5)*(-2 + 2*s1 + Power(s1,2) - 8*t2) + 
            Power(s2,5)*(-1 + t1)*(-2 + s1 + 2*t1 - t2) + 
            t1*(4*s1 + 3*(-5 + t2))*(-1 + t2) - 2*Power(-1 + t2,2) + 
            Power(t1,2)*(-80 + 2*Power(s1,2) + s1*(7 - 13*t2) + 63*t2) + 
            Power(t1,4)*(6 + 4*Power(s1,2) - 12*t2 + s1*(-10 + 3*t2)) + 
            Power(t1,3)*(57 - 4*Power(s1,2) - 49*t2 + s1*(13 + 4*t2)) + 
            Power(s2,4)*(-5 + 2*Power(s1,2)*(-3 + t1) - 7*Power(t1,3) - 
               17*t2 - 12*Power(t2,2) + Power(t1,2)*(13 + 6*t2) + 
               t1*(-1 + 7*t2) + 
               s1*(9 - 2*Power(t1,2) + 18*t2 - t1*(3 + 2*t2))) - 
            s2*(13 + 2*Power(t1,5) + 
               Power(s1,2)*t1*
                (4 - 8*t1 - 2*Power(t1,2) + Power(t1,3)) - 14*t2 + 
               Power(t2,2) + Power(t1,4)*(29 + 31*t2) - 
               Power(t1,3)*(47 + 36*t2 + 4*Power(t2,2)) + 
               t1*(-125 + 87*t2 + 4*Power(t2,2)) - 
               2*Power(t1,2)*(-64 + 50*t2 + 7*Power(t2,2)) + 
               s1*(2*Power(t1,5) + t1*(6 - 18*t2) + 
                  Power(t1,4)*(-30 + t2) + 4*(-1 + t2) + 
                  Power(t1,3)*(41 + 10*t2) + Power(t1,2)*(17 + 21*t2))) \
+ Power(s2,2)*(-29 - 3*Power(t1,5) + 
               Power(s1,2)*(2 - 4*t1 - 22*Power(t1,2) + 
                  7*Power(t1,3)) + Power(t1,4)*(10 - 3*t2) - 8*t2 + 
               20*Power(t2,2) + Power(t1,3)*(63 + 39*t2) + 
               t1*(64 - 15*t2 - 45*Power(t2,2)) - 
               Power(t1,2)*(105 + 69*t2 + 16*Power(t2,2)) + 
               s1*(-1 + 4*Power(t1,4) - 5*t2 - 
                  Power(t1,3)*(57 + 2*t2) + t1*(-3 + 28*t2) + 
                  Power(t1,2)*(113 + 37*t2))) + 
            Power(s2,3)*(-17 + Power(s1,2)*(22 - 7*t1)*t1 + 
               8*Power(t1,4) + 12*t2 + 7*Power(t2,2) - 
               Power(t1,3)*(19 + 2*t2) + 
               Power(t1,2)*(-31 - 36*t2 + 6*Power(t2,2)) + 
               t1*(59 + 58*t2 + 26*Power(t2,2)) - 
               s1*(-7 + Power(t1,3) + 11*t2 - Power(t1,2)*(33 + 5*t2) + 
                  t1*(71 + 48*t2)))) + 
         Power(s,3)*(Power(s2,4)*
             (Power(s1,2)*t1 - 85*Power(t1,3) + 
               Power(t1,2)*(93 - 4*t2) + 14*t1*(-1 + 2*t2) - 
               3*t2*(3 + 2*t2) + 
               s1*(4 + 11*Power(t1,2) + 3*t2 - 4*t1*(5 + t2))) + 
            Power(s2,5)*(8 + 20*Power(t1,2) + t2 + 
               t1*(s1 - 3*(9 + t2))) + 
            Power(s2,2)*(4 - 71*Power(t1,5) + 
               Power(t1,4)*(101 + 51*s1 - 18*t2) - 2*t2 + 
               t1*(46 + 4*s1*(-8 + t2) + 46*t2 - 13*Power(t2,2)) + 
               Power(t1,2)*(-253 + 111*s1 + 2*Power(s1,2) - 134*t2 + 
                  6*Power(t2,2)) + 
               2*Power(t1,3)*(102 + 2*Power(s1,2) + 59*t2 - 
                  3*Power(t2,2) - s1*(67 + 3*t2))) + 
            s2*t1*(13*Power(t1,5) + Power(t1,4)*(-48 - 22*s1 + 3*t2) - 
               4*(5 - 5*t2 + Power(t2,2)) + 
               t1*(-142 + s1*(56 - 12*t2) - 8*t2 + 13*Power(t2,2)) + 
               Power(t1,3)*(-174 - 49*t2 + 4*s1*(15 + t2)) + 
               Power(t1,2)*(391 - 4*Power(s1,2) + 24*t2 - 
                  10*Power(t2,2) + s1*(-99 + 8*t2))) - 
            Power(s2,3)*(4*Power(s1,2)*Power(t1,2) - 123*Power(t1,4) + 
               Power(t1,3)*(131 - 22*t2) + t2*(6 + t2) + 
               Power(t1,2)*(68 + 110*t2 - 8*Power(t2,2)) - 
               t1*(55 + 66*t2 + 18*Power(t2,2)) + 
               s1*(-4 + 43*Power(t1,3) - 8*Power(t1,2)*(12 + t2) + 
                  t1*(45 + 8*t2))) + 
            Power(t1,2)*(2*(6 + s1)*Power(t1,4) + 
               Power(t1,2)*(-187 + 2*Power(s1,2) + s1*(29 - 3*t2) + 
                  41*t2 - 2*Power(t2,2)) + 2*(7 - 7*t2 + Power(t2,2)) - 
               Power(t1,3)*(-46 + Power(s1,2) - 8*t2 + 2*s1*(1 + t2)) + 
               t1*(4*s1*(-7 + 2*t2) - 5*(-18 + 4*t2 + Power(t2,2))))))*
       T3q(s2,t1))/
     (s*(-1 + s2)*Power(s2 - t1,2)*(-s + s2 - t1)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(s - s1 + t2)) - 
    (8*(4*Power(s,5)*Power(t1,2)*(1 + t1) + 
         Power(s,4)*(-((-18 + s1)*Power(t1,4)) + 
            t1*(-3 + s1 - 2*t2)*t2 + Power(t2,2) + 
            Power(t1,3)*(-22 - t2 + s1*(2 + t2)) + 
            Power(t1,2)*(28 + 12*t2 + Power(t2,2) - s1*(9 + 2*t2)) + 
            s2*t1*(8 - Power(t1,3) + Power(t1,2)*(-18 + t2) + t2 + 
               t1*(3 - 8*s1 + 6*t2))) - 
         Power(-1 + t1,2)*(2*(-1 + s1)*Power(t1,5) + 
            Power(t1,4)*(-3 + s1*(5 - 2*t2)) + Power(-1 + t2,2) - 
            t1*(-1 + t2)*(-10 + 2*s1 + 3*t2) + 
            Power(t1,2)*(10 - 7*s1 + Power(s1,2) - 8*t2 + 
               4*Power(t2,2)) + 
            Power(t1,3)*(4 + 3*Power(s1,2) - 3*t2 + 2*Power(t2,2) - 
               2*s1*(1 + 2*t2)) + 
            Power(s2,4)*(4*Power(s1,2) + 
               s1*(-3 + 2*t1 + Power(t1,2) - 8*t2) + 
               t2*(3 - 2*t1 - Power(t1,2) + 4*t2)) + 
            Power(s2,2)*(-Power(t1,4) + 
               Power(s1,2)*(1 + 11*t1 + 14*Power(t1,2) - 
                  2*Power(t1,3)) - (-7 + t2)*t2 - 
               5*Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(13 + t2 + 8*Power(t2,2)) + 
               t1*(-7 - 3*t2 + 17*Power(t2,2)) + 
               s1*(-5 + 12*Power(t1,3) + Power(t1,4) + t1*(8 - 26*t2) - 
                  2*t2 - 4*Power(t1,2)*(4 + 5*t2))) + 
            s2*(9 + Power(s1,2)*t1*
                (-2 - 10*t1 - 5*Power(t1,2) + Power(t1,3)) - 11*t2 + 
               2*Power(t2,2) + Power(t1,4)*(5 + 2*t2) + 
               Power(t1,2)*(2 + t2 - 11*Power(t2,2)) + 
               t1*(-12 + 5*t2 - 5*Power(t2,2)) + 
               Power(t1,3)*(-4 + 3*t2 - 2*Power(t2,2)) + 
               2*s1*(-1 - 4*Power(t1,4) + t2 + 4*Power(t1,3)*t2 + 
                  t1*(6 + t2) + Power(t1,2)*(-1 + 10*t2))) + 
            Power(s2,3)*(3 + Power(s1,2)*(-4 - 13*t1 + Power(t1,2)) + 
               t2 - 6*Power(t2,2) + Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(1 + 7*t2 - Power(t2,2)) - 
               t1*(5 + 9*t2 + 9*Power(t2,2)) - 
               2*s1*(2 + 4*Power(t1,2) + Power(t1,3) - 5*t2 - 
                  t1*(7 + 11*t2)))) + 
         Power(s,2)*(1 - (-9 + s1)*Power(t1,6) + 
            Power(t1,5)*(-6 + 2*Power(s1,2) + s1*(-18 + t2) - 5*t2) - 
            2*Power(t2,2) + Power(t1,3)*
             (-54 + 14*Power(s1,2) + 2*s1*(-55 + t2) + 27*t2 - 
               13*Power(t2,2)) + 
            Power(t1,4)*(21 - 13*Power(s1,2) + 19*t2 + 2*Power(t2,2) + 
               6*s1*(9 + t2)) + 
            Power(t1,2)*(25 - 11*Power(s1,2) - 35*t2 + 4*Power(t2,2) + 
               s1*(79 + 2*t2)) + 
            t1*(4 - 6*t2 + Power(t2,2) + s1*(-4 + 5*t2)) - 
            Power(s2,3)*(-1 + t1)*
             (2 + 5*Power(t1,3) + s1*(-7 + 21*t1 + 2*Power(t1,2)) + 
               Power(t1,2)*(16 - 3*t2) + 6*t2 - t1*(23 + 19*t2)) - 
            Power(s2,2)*(-39 - 6*Power(t1,5) + 
               Power(s1,2)*(-2 - 9*t1 + 16*Power(t1,2) + 
                  3*Power(t1,3)) - 23*t2 - 13*Power(t2,2) + 
               Power(t1,4)*(-33 + 5*t2) + 2*Power(t1,3)*(59 + 7*t2) + 
               2*t1*(56 + 23*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-152 - 42*t2 + 11*Power(t2,2)) + 
               s1*(7*Power(t1,4) + t1*(-24 + t2) + 13*(1 + t2) - 
                  5*Power(t1,2)*(-8 + 5*t2) - Power(t1,3)*(36 + 5*t2))) \
- s2*(3 + 30*Power(t1,5) + Power(t1,6) + 
               Power(s1,2)*t1*
                (-7 + 21*t1 - 31*Power(t1,2) + Power(t1,3)) + 4*t2 - 
               2*Power(t2,2) + 
               Power(t1,2)*(-200 + 13*t2 - 23*Power(t2,2)) + 
               Power(t1,3)*(159 + 8*t2 - 7*Power(t2,2)) - 
               Power(t1,4)*(80 + 3*t2 + Power(t2,2)) + 
               t1*(87 - 22*t2 + 17*Power(t2,2)) + 
               s1*(-1 - 8*Power(t1,5) + t1*(44 - 3*t2) + 
                  Power(t1,2)*(-89 + t2) + Power(t1,4)*(-4 + 7*t2) + 
                  Power(t1,3)*(58 + 27*t2)))) + 
         s*(-1 + t1)*(-1 + 2*Power(t1,6) + 
            Power(s2,4)*(-1 + t1)*
             (-6 + 2*Power(t1,2) + s1*(7 + t1) - t1*(-4 + t2) - 7*t2) + 
            Power(t1,5)*(8 - 12*s1 + Power(s1,2) - 4*t2) + 4*t2 - 
            3*Power(t2,2) + Power(t1,4)*
             (-28 + 10*s1 - 7*Power(s1,2) + 20*t2 + 9*s1*t2) + 
            t1*(11 + 5*s1*(-1 + t2) - 21*t2 + 7*Power(t2,2)) - 
            Power(t1,2)*(25 + 8*Power(s1,2) + 26*t2 + 8*Power(t2,2) - 
               s1*(64 + 5*t2)) + 
            Power(t1,3)*(33 - 2*Power(s1,2) + 27*t2 - 12*Power(t2,2) + 
               s1*(-57 + 13*t2)) + 
            Power(s2,3)*(5 - 3*Power(t1,4) + 
               2*Power(s1,2)*(-1 + 8*t1 + Power(t1,2)) + 
               Power(t1,3)*(-8 + t2) + 3*t2 + 4*Power(t2,2) + 
               5*Power(t1,2)*(6 + t2) + 
               3*t1*(-8 - 3*t2 + 4*Power(t2,2)) + 
               s1*(3 + 5*Power(t1,3) + t1*(3 - 28*t2) - 2*t2 - 
                  Power(t1,2)*(11 + 2*t2))) + 
            s2*(1 - 8*Power(t1,5) + 
               2*Power(s1,2)*t1*
                (7 + 3*t1 + 16*Power(t1,2) - 2*Power(t1,3)) + 6*t2 - 
               4*Power(t2,2) + Power(t1,4)*(-7 + 3*t2) + 
               3*Power(t1,3)*(16 - 5*t2 + 2*Power(t2,2)) + 
               t1*(8 + 79*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-42 - 73*t2 + 34*Power(t2,2)) + 
               s1*(1 + 2*Power(t1,5) + Power(t1,2)*(125 - 38*t2) - 
                  Power(t1,4)*(-26 + t2) - t2 - 11*t1*(9 + 2*t2) - 
                  Power(t1,3)*(55 + 34*t2))) + 
            Power(s2,2)*(Power(t1,5) + 
               Power(s1,2)*(-6 - 2*t1 - 41*Power(t1,2) + Power(t1,3)) + 
               Power(t1,4)*(11 + t2) + 
               t1*(-5 + 49*t2 - 23*Power(t2,2)) + 
               Power(t1,2)*(8 - 13*t2 - 18*Power(t2,2)) + 
               Power(t1,3)*(-20 + 3*t2 - 2*Power(t2,2)) - 
               5*(-1 + 8*t2 + Power(t2,2)) + 
               s1*(37 - 8*Power(t1,4) + 15*t2 + 
                  Power(t1,3)*(-11 + 5*t2) + t1*(-65 + 21*t2) + 
                  Power(t1,2)*(47 + 55*t2)))) + 
         Power(s,3)*(-2*(-10 + s1)*Power(t1,5) + 
            Power(t1,4)*(-44 + Power(s1,2) + 2*s1*(-4 + t2)) - 
            (-2 + t2)*t2 + Power(t1,2)*
             (-74 + 5*Power(s1,2) + 2*s1*(-9 + t2) - 8*t2 - 
               7*Power(t2,2)) - 
            t1*(1 + s1 - 5*t2 + 3*s1*t2 - 6*Power(t2,2)) + 
            Power(t1,3)*(99 - 6*Power(s1,2) - s1*(-13 + t2) + 17*t2 + 
               2*Power(t2,2)) + 
            Power(s2,2)*(4 + 4*Power(t1,4) + 
               Power(t1,3)*(25 + s1 - 3*t2) + t2 + 
               t1*(1 - 15*s1 + 11*t2) + Power(t1,2)*(22*s1 - 17*(2 + t2))\
) + s2*(-3*Power(t1,5) + (-5 + 3*s1 - 6*t2)*t2 + 
               Power(t1,4)*(-46 + 4*s1 + 3*t2) + 
               t1*(66 + Power(s1,2) + 33*t2 + 12*Power(t2,2) - 
                  10*s1*(2 + t2)) + 
               Power(t1,3)*(103 + Power(s1,2) + 11*t2 - 
                  2*s1*(11 + 2*t2)) + 
               Power(t1,2)*(6*Power(s1,2) - 5*s1*(-6 + t2) + 
                  2*(-60 - 17*t2 + Power(t2,2))))))*T4q(t1))/
     (s*(-1 + s2)*(-s + s2 - t1)*(-1 + t1)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(s - s1 + t2)) + 
    (8*(Power(s,5)*(s1*t1*(t1 - t2) + s2*t1*(t1 - t2) + (3*t1 - t2)*t2) + 
         2*Power(s2 - t1,2)*Power(-1 + t1,2)*
          (-5 + Power(s1,2)*(s2 - t1) + 4*t1 + Power(t1,2) + 2*t2 - 
            2*Power(s2,2)*t2 - 12*t1*t2 + 3*Power(t2,2) + 
            s1*(3 + 2*Power(s2,2) - Power(t1,2) - s2*(11 + t1 - 2*t2) - 
               3*t2 + t1*(8 + t2)) + 
            s2*(2 + 9*t2 - 3*Power(t2,2) + t1*(-2 + 3*t2))) + 
         Power(s,4)*((8 + 2*s1 + 3*s2)*Power(t1,3) - 
            Power(t1,2)*(18 + Power(s1,2) + 4*Power(s2,2) - 14*t2 + 
               2*s1*(-2 + 2*s2 + t2) + s2*(4 + 3*t2)) + 
            t2*(-2 - Power(s2,2) + t2 + s2*(5 - 3*s1 + 6*t2)) + 
            t1*(1 - Power(s1,2)*s2 - 9*t2 - 4*Power(t2,2) + 
               3*Power(s2,2)*(1 + t2) - s2*(2 + 11*t2) + 
               s1*(1 - Power(s2,2) + 3*t2 + 4*s2*(1 + t2)))) + 
         Power(s,3)*(-1 + (11 + s1)*Power(t1,4) - 
            Power(t1,3)*(56 + 2*Power(s1,2) + s1*(-2 + t2) - 27*t2) + 
            2*Power(t2,2) + Power(s2,3)*
             (2 - 7*t1 + 5*Power(t1,2) + s1*(-1 + 2*t1) + 2*t2 - 3*t1*t2) \
+ t1*(-6 + 4*s1 + 6*t2 - 5*s1*t2 + 3*Power(t2,2)) - 
            Power(t1,2)*(-48 + Power(s1,2) + 47*t2 + 4*Power(t2,2) - 
               s1*(15 + 4*t2)) + 
            Power(s2,2)*(-3 - 6*Power(t1,3) + Power(s1,2)*(-2 + 3*t1) - 
               17*t2 - 13*Power(t2,2) + Power(t1,2)*(13 + 5*t2) + 
               2*t1*(-2 + 7*t2) + 
               s1*(5 + 7*Power(t1,2) + 13*t2 - 5*t1*(2 + t2))) + 
            s2*(3 - 16*Power(t1,3) + Power(t1,4) + 
               Power(s1,2)*t1*(1 + t1) + 4*t2 - 2*Power(t2,2) - 
               Power(t1,2)*(-55 + 35*t2 + Power(t2,2)) + 
               t1*(-31 + 42*t2 + 17*Power(t2,2)) - 
               s1*(1 + 8*Power(t1,3) - 7*Power(t1,2)*t2 + 3*t1*(2 + 5*t2))\
)) + s*(-4*(3 + s1)*Power(t1,5) + 2*Power(t1,6) + Power(-1 + t2,2) - 
            t1*(-1 + t2)*(-10 + 2*s1 + 3*t2) + 
            Power(t1,3)*(-34 + 5*Power(s1,2) + 93*t2 + 4*Power(t2,2) - 
               4*s1*(15 + 2*t2)) + 
            Power(t1,4)*(27 - 6*Power(s1,2) - 56*t2 + 2*Power(t2,2) + 
               s1*(51 + 2*t2)) + 
            Power(s2,4)*(-4*Power(s1,2) - 
               t2*(5 - 6*t1 + Power(t1,2) + 4*t2) + 
               s1*(5 - 6*t1 + Power(t1,2) + 8*t2)) + 
            Power(t1,2)*(26 - 3*Power(s1,2) - 48*t2 - 8*Power(t2,2) + 
               s1*(11 + 16*t2)) + 
            s2*(9 - 4*Power(t1,5) + 
               Power(s1,2)*t1*
                (6 - 4*t1 + 13*Power(t1,2) + Power(t1,3)) - 11*t2 + 
               2*Power(t2,2) + 5*Power(t1,4)*(5 + 2*t2) + 
               Power(t1,3)*(-44 + 99*t2 - 12*Power(t2,2)) + 
               t1*(-24 + 77*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(38 - 175*t2 + 19*Power(t2,2)) - 
               2*s1*(1 + 48*Power(t1,3) + 2*Power(t1,4) - t2 + 
                  9*t1*(2 + t2) + Power(t1,2)*(-69 + 8*t2))) + 
            Power(s2,3)*(7 + Power(s1,2)*(6 + 9*t1 + Power(t1,2)) - 
               3*t2 + 4*Power(t2,2) + Power(t1,3)*(1 + t2) - 
               Power(t1,2)*(-5 + 5*t2 + Power(t2,2)) + 
               t1*(-13 + 7*t2 + 13*Power(t2,2)) - 
               2*s1*(2 + Power(t1,3) + 5*t2 + t1*(-3 + 11*t2))) + 
            Power(s2,2)*(-4 + Power(t1,4) - 
               Power(s1,2)*(3 + 7*t1 + 12*Power(t1,2) + 2*Power(t1,3)) - 
               25*t2 - Power(t2,2) - Power(t1,3)*(19 + 9*t2) + 
               t1*(-9 + 81*t2 - 25*Power(t2,2)) + 
               Power(t1,2)*(31 - 47*t2 + 2*Power(t2,2)) + 
               s1*(25 + 14*Power(t1,3) + Power(t1,4) + 2*t2 + 
                  2*Power(t1,2)*(17 + 6*t2) + t1*(-74 + 34*t2)))) - 
         Power(s,2)*(1 - 6*Power(t1,5) + 
            Power(t1,4)*(42 + 2*s1 + Power(s1,2) - 14*t2) + 
            Power(s2,4)*(-1 + t1)*(-2 + s1 + 2*t1 - t2) - 4*t2 + 
            3*Power(t2,2) + t1*
             (-10 - 5*s1*(-1 + t2) + 17*t2 - 4*Power(t2,2)) + 
            Power(t1,2)*(43 - 4*Power(s1,2) - 59*t2 - 4*Power(t2,2) + 
               s1*(27 + 10*t2)) + 
            Power(t1,3)*(4*Power(s1,2) + s1*(-58 + t2) - 
               2*(35 - 42*t2 + Power(t2,2))) + 
            Power(s2,3)*(-1 + 2*Power(s1,2)*(-3 + t1) - 3*Power(t1,3) - 
               17*t2 - 12*Power(t2,2) + Power(t1,2)*(9 + t2) + 
               t1*(-5 + 12*t2) + 
               s1*(11 + 5*Power(t1,2) + 18*t2 - 2*t1*(6 + t2))) - 
            s2*(1 - 8*Power(t1,4) + 
               2*Power(s1,2)*t1*(-1 + 3*t1 + 2*Power(t1,2)) + 
               Power(t1,3)*(71 - 31*t2) + 6*t2 - 4*Power(t2,2) + 
               t1*(53 - 47*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-117 + 124*t2 + 8*Power(t2,2)) - 
               s1*(-1 + 2*Power(t1,4) - Power(t1,3)*(-4 + t2) + t2 + 
                  t1*(-26 + 3*t2) + Power(t1,2)*(73 + 13*t2))) + 
            Power(s2,2)*(Power(t1,4) + 
               Power(s1,2)*(2 + 8*t1 + Power(t1,2)) + 
               Power(t1,3)*(-8 + t2) + 3*(7 + 2*t2 + Power(t2,2)) - 
               2*Power(t1,2)*(-17 + 15*t2 + Power(t2,2)) + 
               t1*(-48 + 55*t2 + 26*Power(t2,2)) - 
               s1*(5 + 8*Power(t1,3) + 9*t2 - 5*Power(t1,2)*(1 + t2) + 
                  t1*(24 + 34*t2)))))*T5q(s))/
     (s*(-1 + s2)*(-s + s2 - t1)*
       Power(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2),2)*(s - s1 + t2)));
   return a;
};
