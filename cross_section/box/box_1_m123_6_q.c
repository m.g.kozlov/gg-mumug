#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m123_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(Power(s,5)*(-1 + s1)*s1*(4 + 5*t1 - 9*t2) + 
         Power(s,4)*(-2*(-1 + s2 - t1 + t2) + 
            Power(s1,3)*(-6 + 5*s2 - 18*t1 + 19*t2) + 
            s1*(5 - 6*t1 - 36*t2 - 9*t1*t2 + 25*Power(t2,2) + 
               s2*(22 - 2*t1 + t2)) + 
            Power(s1,2)*(-1 + 30*t1 + 11*t2 + 9*t1*t2 - 25*Power(t2,2) + 
               s2*(-25 - 6*t1 + 7*t2))) + 
         Power(s,3)*(-2*(-1 + s2 - t1 + t2)*(-4 + 3*t2) - 
            Power(s1,4)*(2 + 9*s2 - 21*t1 + 10*t2) + 
            Power(s1,3)*(37 - 6*Power(s2,2) + 4*s2*(9 + 4*t1) - 47*t2 + 
               28*Power(t2,2) - t1*(29 + 35*t2)) + 
            s1*(45 + 4*Power(t1,2)*(-2 + t2) + 47*t2 - 85*Power(t2,2) + 
               25*Power(t2,3) - 2*Power(s2,2)*(2 + 5*t2) + 
               t1*(28 - 37*t2 - 5*Power(t2,2)) + 
               s2*(-104 - 2*t1*(-12 + t2) + 73*t2 + 9*Power(t2,2))) + 
            Power(s1,2)*(-64 + 36*t2 + 23*Power(t2,2) - 25*Power(t2,3) - 
               4*Power(t1,2)*(2 + t2) + 2*Power(s2,2)*(9 + t2) + 
               t1*(-60 + 122*t2 + 5*Power(t2,2)) + 
               s2*(53 + t1*(8 - 22*t2) - 99*t2 + 15*Power(t2,2)))) + 
         2*Power(-1 + t2,2)*(Power(s1,5)*(2 + 6*s2 - 8*t1) + 
            (-1 + s2 - t1 + t2)*(-1 + t2 + Power(t2,2)) + 
            Power(s1,4)*(-4 + 5*Power(s2,2) - 2*Power(t1,2) + 
               15*t1*(2 + t2) - s2*(30 + 5*t1 + 9*t2)) + 
            Power(s1,3)*(7 + 14*t2 - 3*Power(t2,2) + 
               2*Power(t1,2)*(6 + t2) - 2*Power(s2,2)*(1 + 4*t2) + 
               t1*(5 - 63*t2 - 7*Power(t2,2)) + 
               s2*(-12 + 11*t1*(-1 + t2) + 53*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(-2 - 19*t2 + 4*Power(t2,2) + Power(t2,3) - 
               2*Power(t1,2)*(-5 + 7*t2) + 
               Power(s2,2)*(-9 + 11*t2 + 3*Power(t2,2)) + 
               8*t1*(-3 + t2 + 4*Power(t2,2)) + 
               s2*(40 - 19*t2 - 20*Power(t2,2) + Power(t2,3) + 
                  t1*(-3 + 6*t2 - 6*Power(t2,2)))) + 
            s1*(-12 + 4*Power(t1,2)*(-1 + t2) + 3*t2 + 19*Power(t2,2) - 
               10*Power(t2,3) + t1*(12 + t2 - 8*Power(t2,2)) + 
               Power(s2,2)*(-2 + 9*t2 - 7*Power(t2,2)) + 
               s2*(13 - 34*t2 + 17*Power(t2,2) - Power(t2,3) + 
                  t1*(3 - 9*t2 + 6*Power(t2,2))))) + 
         Power(s,2)*(4*Power(s1,5)*(1 + s2 - 2*t1) - 
            2*(-1 + s2 - t1 + t2)*(6 - 9*t2 + 2*Power(t2,2)) + 
            2*Power(s1,4)*(-5 + 3*Power(s2,2) - 4*t2 + 5*Power(t2,2) - 
               s2*(4 + 5*t1 + 5*t2) + t1*(-4 + 19*t2)) + 
            Power(s1,3)*(-28 + Power(s2,2)*(4 - 24*t2) + 131*t2 - 
               70*Power(t2,2) + 3*Power(t2,3) + Power(t1,2)*(4 + 8*t2) - 
               2*t1*(-70 + 78*t2 + 19*Power(t2,2)) + 
               s2*(-145 + 158*t2 - 3*Power(t2,2) + t1*(-30 + 46*t2))) + 
            Power(s1,2)*(93 - 173*t2 + 24*Power(t2,2) + 35*Power(t2,3) - 
               11*Power(t2,4) + 
               Power(t1,2)*(52 - 40*t2 - 8*Power(t2,2)) + 
               2*Power(s2,2)*(-23 + 25*t2 + 5*Power(t2,2)) + 
               3*t1*(-16 - 51*t2 + 70*Power(t2,2) + Power(t2,3)) + 
               s2*(67 + 105*t2 - 159*Power(t2,2) + 11*Power(t2,3) + 
                  t1*(-28 + 44*t2 - 38*Power(t2,2)))) + 
            s1*(-103 + 8*Power(t1,2)*Power(-1 + t2,2) + 32*t2 + 
               166*Power(t2,2) - 106*Power(t2,3) + 11*Power(t2,4) + 
               Power(s2,2)*(4 + 30*t2 - 34*Power(t2,2)) + 
               t1*(16 + 49*t2 - 56*Power(t2,2) - 3*Power(t2,3)) + 
               s2*(158 - 279*t2 + 102*Power(t2,2) + 13*Power(t2,3) + 
                  2*t1*(-18 + 11*t2 + 7*Power(t2,2))))) + 
         s*(-1 + t2)*(4*Power(s1,5)*(5 + s2 - 3*t1 - 3*t2) + 
            2*(-1 + s2 - t1 + t2)*(-4 + 5*t2 + Power(t2,2)) + 
            Power(s1,4)*(-32 + 16*Power(s2,2) + 61*t1 - 4*Power(t1,2) - 
               4*t2 + 47*t1*t2 + 20*Power(t2,2) - s2*(65 + 20*t1 + 19*t2)) \
+ Power(s1,3)*(5 + 118*t2 - 45*Power(t2,2) - 6*Power(t2,3) + 
               4*Power(t1,2)*(7 + 3*t2) - 2*Power(s2,2)*(3 + 17*t2) - 
               7*t1*(-13 + 36*t2 + 5*Power(t2,2)) + 
               2*s2*(-58 + 109*t2 + 3*Power(t2,2) + 2*t1*(-9 + 13*t2))) + 
            Power(s1,2)*(40 - 131*t2 + 10*Power(t2,2) + 19*Power(t2,3) - 
               2*Power(t2,4) - 
               4*Power(t1,2)*(-16 + 16*t2 + Power(t2,2)) + 
               2*Power(s2,2)*(-23 + 28*t2 + 7*Power(t2,2)) + 
               t1*(-109 - 21*t2 + 184*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(163 - 28*t2 - 124*Power(t2,2) + 5*Power(t2,3) + 
                  t1*(-32 + 38*t2 - 34*Power(t2,2)))) + 
            s1*(-81 + 15*t2 + 135*Power(t2,2) - 71*Power(t2,3) + 
               2*Power(t2,4) + Power(s2,2)*(-4 + 42*t2 - 38*Power(t2,2)) + 
               4*Power(t1,2)*(-2 + t2 + Power(t2,2)) + 
               t1*(57 + 4*t2 - 43*Power(t2,2) - 2*Power(t2,3)) + 
               s2*(102 - 213*t2 + 92*Power(t2,2) + 3*Power(t2,3) + 
                  2*t1*(-4 - 9*t2 + 13*Power(t2,2)))))))/
     (Power(-1 + s1,2)*s1*(-s + s1 - t2)*(1 - s + s1 - t2)*(-1 + t2)*
       Power(-1 + s + t2,2)*(-1 + s2 - t1 + t2)) - 
    (8*(21 - 9*s2 - 10*Power(s2,2) + 2*Power(s2,3) - 2*t1 + 24*s2*t1 - 
         9*Power(s2,2)*t1 + 2*Power(s2,3)*t1 - 16*Power(t1,2) + 
         17*s2*Power(t1,2) - 5*Power(s2,2)*Power(t1,2) - 10*Power(t1,3) + 
         4*s2*Power(t1,3) - Power(t1,4) - 68*t2 + 8*s2*t2 + 
         37*Power(s2,2)*t2 - 2*Power(s2,3)*t2 - 2*Power(s2,4)*t2 + 
         42*t1*t2 - 98*s2*t1*t2 + 7*Power(s2,2)*t1*t2 + 
         Power(s2,3)*t1*t2 + 69*Power(t1,2)*t2 - 19*s2*Power(t1,2)*t2 + 
         7*Power(s2,2)*Power(t1,2)*t2 + 14*Power(t1,3)*t2 - 
         9*s2*Power(t1,3)*t2 + 3*Power(t1,4)*t2 + 78*Power(t2,2) + 
         10*s2*Power(t2,2) - 28*Power(s2,2)*Power(t2,2) + 
         4*Power(s2,4)*Power(t2,2) - 96*t1*Power(t2,2) + 
         90*s2*t1*Power(t2,2) + Power(s2,2)*t1*Power(t2,2) - 
         11*Power(s2,3)*t1*Power(t2,2) - 74*Power(t1,2)*Power(t2,2) + 
         5*s2*Power(t1,2)*Power(t2,2) + 
         10*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         6*Power(t1,3)*Power(t2,2) - 3*s2*Power(t1,3)*Power(t2,2) - 
         36*Power(t2,3) - 6*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) + 
         2*Power(s2,3)*Power(t2,3) + 74*t1*Power(t2,3) - 
         14*s2*t1*Power(t2,3) - 5*Power(s2,2)*t1*Power(t2,3) + 
         21*Power(t1,2)*Power(t2,3) + 3*s2*Power(t1,2)*Power(t2,3) + 
         5*Power(t2,4) - 5*s2*Power(t2,4) - 18*t1*Power(t2,4) - 
         2*s2*t1*Power(t2,4) + 2*s2*Power(t2,5) + 
         2*Power(s1,6)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) - 
         2*Power(s,4)*(-1 + s1)*(t1 - t2)*
          (1 - s2 + t1 - t2 - s1*(-1 + s2 - 3*t1 + 3*t2)) + 
         Power(s1,5)*(s2 - t1)*
          (Power(s2,3) + Power(t1,3) + 4*Power(-1 + t2,2) - 
            5*Power(t1,2)*(1 + t2) - Power(s2,2)*(7 + t1 + 3*t2) + 
            t1*(2 - 6*t2 + 4*Power(t2,2)) - 
            s2*(2 + Power(t1,2) - 6*t2 + 4*Power(t2,2) - 4*t1*(3 + 2*t2))\
) + Power(s1,4)*(Power(s2,5) + 2*Power(t1,4) + 2*Power(-1 + t2,3) - 
            2*Power(s2,4)*(-2 + 2*t1 + t2) + 
            2*t1*Power(-1 + t2,2)*(1 + 4*t2) - 
            Power(t1,3)*(-4 + 9*t2 + Power(t2,2)) + 
            Power(t1,2)*(30 - 25*t2 - 6*Power(t2,2) + Power(t2,3)) - 
            Power(s2,2)*(-33 + 4*Power(t1,3) + 32*t2 + Power(t2,2) + 
               2*Power(t1,2)*(-11 + 5*t2) + 
               t1*(-44 + 71*t2 - 9*Power(t2,2))) + 
            Power(s2,3)*(6*Power(t1,2) + 8*t1*(-2 + t2) - 
               3*(6 - 9*t2 + Power(t2,2))) + 
            s2*(Power(t1,4) + 4*Power(t1,3)*(-3 + t2) - 
               2*Power(-1 + t2,2)*(1 + 4*t2) + 
               Power(t1,2)*(-30 + 53*t2 - 5*Power(t2,2)) + 
               t1*(-65 + 63*t2 + Power(t2,2) + Power(t2,3)))) + 
         s1*(-3*Power(t1,5) + 2*Power(s2,5)*t2 + 
            Power(t1,4)*(-9 + 8*t2) + 
            Power(t1,3)*(-37 + 93*t2 - 38*Power(t2,2)) - 
            Power(-1 + t2,3)*(-2 - 3*t2 + 7*Power(t2,2)) + 
            2*t1*Power(-1 + t2,2)*(30 - t2 + 7*Power(t2,2)) + 
            Power(t1,2)*(-17 + 84*t2 - 93*Power(t2,2) + 
               26*Power(t2,3)) + 
            Power(s2,4)*(-4 + 3*t2 + 2*Power(t2,2) - t1*(2 + 9*t2)) + 
            Power(s2,3)*(17 - 31*t2 - 8*Power(t2,2) + 4*Power(t2,3) + 
               3*Power(t1,2)*(3 + 5*t2) + 
               t1*(19 - 15*t2 - 6*Power(t2,2))) - 
            Power(s2,2)*(3 - 39*t2 + 34*Power(t2,2) + 13*Power(t2,3) - 
               11*Power(t2,4) + Power(t1,3)*(15 + 11*t2) + 
               Power(t1,2)*(35 - 29*t2 - 6*Power(t2,2)) + 
               t1*(76 - 166*t2 + 29*Power(t2,2) + 7*Power(t2,3))) + 
            s2*(Power(t1,4)*(11 + 3*t2) + 
               Power(t1,3)*(29 - 25*t2 - 2*Power(t2,2)) + 
               3*Power(-1 + t2,2)*
                (-8 - 13*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               3*Power(t1,2)*
                (32 - 76*t2 + 25*Power(t2,2) + Power(t2,3)) + 
               t1*(18 - 121*t2 + 133*Power(t2,2) - 23*Power(t2,3) - 
                  7*Power(t2,4)))) + 
         Power(s1,2)*(-2*Power(t1,5) - Power(s2,5)*(-2 + t2) + 
            4*Power(t1,4)*(-5 + 3*t2) + 
            Power(-1 + t2,3)*(-19 + 8*t2 + Power(t2,2)) + 
            4*t1*Power(-1 + t2,2)*(3 - 7*t2 + 3*Power(t2,2)) + 
            Power(t1,3)*(-22 + 25*t2 + 7*Power(t2,2)) + 
            Power(t1,2)*(57 - 48*t2 + 21*Power(t2,2) - 30*Power(t2,3)) + 
            Power(s2,4)*(-4 - 5*t2 + Power(t2,2) + 2*t1*(-5 + 2*t2)) - 
            Power(s2,3)*(-34 + 41*t2 + 11*Power(t2,2) - 8*Power(t2,3) + 
               Power(t1,2)*(-20 + 6*t2) + t1*(-36 + 3*t2 + Power(t2,2))) \
+ Power(s2,2)*(-11 + 4*Power(t1,3)*(-5 + t2) + 85*t2 - 38*Power(t2,2) - 
               39*Power(t2,3) + 3*Power(t2,4) - 
               Power(t1,2)*(80 - 33*t2 + Power(t2,2)) + 
               t1*(-95 + 122*t2 + 14*Power(t2,2) - 11*Power(t2,3))) + 
            s2*(-(Power(t1,4)*(-10 + t2)) + 
               Power(t1,3)*(68 - 37*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-12 + 25*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(83 - 106*t2 - 10*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-37 - 63*t2 + 41*Power(t2,2) + 63*Power(t2,3) - 
                  4*Power(t2,4)))) - 
         Power(s1,3)*(-5*Power(t1,5) + Power(s2,5)*(3 + t2) + 
            2*Power(-1 + t2,3)*(1 + 2*t2) + Power(t1,4)*(11 + 15*t2) + 
            2*t1*Power(-1 + t2,2)*(-23 + 4*t2 + Power(t2,2)) - 
            4*Power(t1,3)*(4 + t2 + 4*Power(t2,2)) + 
            Power(t1,2)*(-20 + 54*t2 - 38*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s2,4)*(23 + 6*t2 + Power(t2,2) - t1*(16 + 5*t2)) + 
            Power(s2,3)*(-10 + 24*t2 + 22*Power(t2,2) + 
               Power(t1,2)*(35 + 9*t2) - t1*(83 + 29*t2 + 4*Power(t2,2))\
) + Power(s2,2)*(-31 + 79*t2 - 55*Power(t2,2) + 7*Power(t2,3) - 
               Power(t1,3)*(39 + 7*t2) + 
               Power(t1,2)*(108 + 55*t2 + 5*Power(t2,2)) + 
               t1*(7 - 57*t2 - 59*Power(t2,2) + Power(t2,3))) + 
            s2*(2*Power(t1,4)*(11 + t2) + 
               Power(-1 + t2,2)*(45 - 10*t2 + Power(t2,2)) - 
               Power(t1,3)*(59 + 47*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(19 + 37*t2 + 53*Power(t2,2) - Power(t2,3)) + 
               t1*(54 - 143*t2 + 105*Power(t2,2) - 17*Power(t2,3) + 
                  Power(t2,4)))) + 
         Power(s,3)*(-6 + 6*s2 + 13*t1 - 15*s2*t1 - 4*Power(s2,2)*t1 + 
            14*Power(t1,2) + 5*s2*Power(t1,2) - Power(t1,3) - 3*t2 + 
            5*s2*t2 + 4*Power(s2,2)*t2 - 17*t1*t2 - 11*s2*t1*t2 + 
            4*Power(t1,2)*t2 + 3*Power(t2,2) + 6*s2*Power(t2,2) - 
            5*t1*Power(t2,2) + 2*Power(t2,3) + 
            Power(s1,3)*(-4 + Power(t1,3) + Power(t1,2)*(21 - 2*t2) + 
               8*Power(t2,2) + Power(s2,2)*(2 + t1 + t2) + 
               t1*(20 - 29*t2 + Power(t2,2)) + 
               s2*(2 - 2*Power(t1,2) + t1*(-19 + t2) - 3*t2 + 
                  Power(t2,2))) + 
            Power(s1,2)*(6 - 11*Power(t1,3) - 
               10*Power(s2,2)*(t1 - t2) - 35*t2 + 19*Power(t2,2) - 
               2*Power(t2,3) + 4*Power(t1,2)*(3 + 5*t2) + 
               t1*(9 - 31*t2 - 7*Power(t2,2)) + 
               s2*(-6 + 21*Power(t1,2) + 35*t2 + 24*Power(t2,2) - 
                  9*t1*(1 + 5*t2))) + 
            s1*(4 + 11*Power(t1,3) + Power(s2,2)*(-2 + 9*t1 - 11*t2) + 
               42*t2 - 34*Power(t2,2) - Power(t1,2)*(51 + 22*t2) + 
               t1*(-46 + 85*t2 + 11*Power(t2,2)) - 
               s2*(2 + 20*Power(t1,2) + 45*t2 + 27*Power(t2,2) - 
                  t1*(51 + 47*t2)))) + 
         Power(s,2)*(17 - 9*s2 - 8*Power(s2,2) - 43*t1 + 52*s2*t1 + 
            6*Power(s2,2)*t1 + 2*Power(s2,3)*t1 - 44*Power(t1,2) - 
            s2*Power(t1,2) - 5*Power(s2,2)*Power(t1,2) - 5*Power(t1,3) + 
            4*s2*Power(t1,3) - Power(t1,4) - 26*t2 + 10*s2*t2 + 
            3*Power(s2,2)*t2 - 4*Power(s2,3)*t2 + 97*t1*t2 - 
            59*s2*t1*t2 - 4*Power(s2,2)*t1*t2 + 64*Power(t1,2)*t2 + 
            13*s2*Power(t1,2)*t2 - 5*Power(t1,3)*t2 - 15*Power(t2,2) + 
            17*s2*Power(t2,2) + 14*Power(s2,2)*Power(t2,2) - 
            79*t1*Power(t2,2) - 39*s2*t1*Power(t2,2) + 
            17*Power(t1,2)*Power(t2,2) + 20*Power(t2,3) + 
            22*s2*Power(t2,3) - 15*t1*Power(t2,3) + 4*Power(t2,4) + 
            Power(s1,4)*(2 + Power(s2,3) - 3*Power(t1,3) - t2 - 
               Power(t2,3) - Power(s2,2)*(7 + 5*t1 + 3*t2) + 
               Power(t1,2)*(-26 + 4*t2) + t1*(-25 + 21*t2) + 
               s2*(7*Power(t1,2) - t1*(-33 + t2) + (9 - 5*t2)*t2)) - 
            s1*(20 - 12*Power(t1,4) + Power(s2,3)*(4 + 9*t1 - 16*t2) + 
               3*t2 - 80*Power(t2,2) + 51*Power(t2,3) + 6*Power(t2,4) + 
               10*Power(t1,3)*(5 + 2*t2) + 
               Power(t1,2)*(-37 + 16*t2 + 2*Power(t2,2)) - 
               t1*(95 - 140*t2 + 117*Power(t2,2) + 16*Power(t2,3)) - 
               Power(s2,2)*(10 + 30*Power(t1,2) + 24*t2 + 
                  17*Power(t2,2) - 2*t1*(25 + 26*t2)) + 
               s2*(-18 + 33*Power(t1,3) + 27*t2 + 50*Power(t2,2) + 
                  29*Power(t2,3) - 8*Power(t1,2)*(13 + 7*t2) + 
                  t1*(54 + 22*t2 - 6*Power(t2,2)))) + 
            Power(s1,3)*(Power(t1,3)*(19 + 2*t2) - 
               Power(s2,3)*(10 + 2*t1 + 3*t2) + 
               Power(t1,2)*(1 - 7*t2 - 4*Power(t2,2)) + 
               2*(-15 + 33*t2 - 19*Power(t2,2) + Power(t2,3)) + 
               2*t1*(-20 + 50*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-24 + 4*Power(t1,2) + t2 - 3*Power(t2,2) + 
                  t1*(43 + 4*t2)) + 
               s2*(76 - 2*Power(t1,3) - 98*t2 - 26*Power(t2,2) - 
                  Power(t1,2)*(52 + 3*t2) + 
                  t1*(9 + 22*t2 + 5*Power(t2,2)))) + 
            Power(s1,2)*(35 - 9*Power(t1,4) + 
               Power(s2,3)*(9 + 11*t1 - 15*t2) - 54*t2 - 5*Power(t2,2) + 
               24*Power(t2,3) + Power(t1,3)*(45 + 19*t2) + 
               Power(t1,2)*(54 - 63*t2 - 11*Power(t2,2)) + 
               t1*(35 - 122*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(41 - 31*Power(t1,2) - 31*t2 - 
                  30*Power(t2,2) + 8*t1*(3 + 7*t2)) + 
               s2*(-97 + 29*Power(t1,3) + 136*t2 + 44*Power(t2,2) + 
                  9*Power(t2,3) - 6*Power(t1,2)*(13 + 10*t2) + 
                  2*t1*(-41 + 50*t2 + 11*Power(t2,2))))) + 
         s*(-32 + 10*s2 + 20*Power(s2,2) - 2*Power(s2,3) + 32*t1 - 
            63*s2*t1 + 7*Power(s2,2)*t1 - 4*Power(s2,3)*t1 + 
            46*Power(t1,2) - 21*s2*Power(t1,2) + 
            10*Power(s2,2)*Power(t1,2) + 16*Power(t1,3) - 
            8*s2*Power(t1,3) + 2*Power(t1,4) + 81*t2 - 11*s2*t2 - 
            36*Power(s2,2)*t2 + 2*Power(s2,4)*t2 - 129*t1*t2 + 
            148*s2*t1*t2 + 16*Power(s2,2)*t1*t2 - Power(s2,3)*t1*t2 - 
            125*Power(t1,2)*t2 - 14*s2*Power(t1,2)*t2 - 
            7*Power(s2,2)*Power(t1,2)*t2 - 2*Power(t1,3)*t2 + 
            9*s2*Power(t1,3)*t2 - 3*Power(t1,4)*t2 - 53*Power(t2,2) - 
            2*s2*Power(t2,2) - 8*Power(s2,2)*Power(t2,2) - 
            10*Power(s2,3)*Power(t2,2) + 162*t1*Power(t2,2) - 
            25*s2*t1*Power(t2,2) + 19*Power(s2,2)*t1*Power(t2,2) + 
            50*Power(t1,2)*Power(t2,2) - 13*s2*Power(t1,2)*Power(t2,2) + 
            4*Power(t1,3)*Power(t2,2) - 7*Power(t2,3) - 
            9*s2*Power(t2,3) - 2*Power(s2,2)*Power(t2,3) - 
            59*t1*Power(t2,3) - 8*s2*t1*Power(t2,3) + 
            3*Power(t1,2)*Power(t2,3) + 9*Power(t2,4) + 
            12*s2*Power(t2,4) - 6*t1*Power(t2,4) + 2*Power(t2,5) + 
            Power(s1,4)*(-2*Power(s2,4) + Power(t1,4) - 
               (-3 + t2)*Power(-1 + t2,2) - 2*Power(t1,3)*(5 + 4*t2) + 
               Power(s2,3)*(3 + 7*t1 + 5*t2) + 
               Power(t1,2)*(-14 - 25*t2 + 11*Power(t2,2)) + 
               t1*(28 - 50*t2 + 26*Power(t2,2) - 4*Power(t2,3)) - 
               Power(s2,2)*(-9 + 7*Power(t1,2) + 45*t2 - 
                  8*Power(t2,2) + 2*t1*(10 + 7*t2)) + 
               s2*(-25 + Power(t1,3) + 41*t2 - 17*Power(t2,2) + 
                  Power(t2,3) + Power(t1,2)*(27 + 17*t2) + 
                  t1*(7 + 66*t2 - 17*Power(t2,2)))) + 
            Power(s1,5)*(-2*Power(s2,3) + Power(s2,2)*(9 + 8*t1 + t2) + 
               t1*(7 + 4*Power(t1,2) - 5*t1*(-3 + t2) - 8*t2 + 
                  Power(t2,2)) + 
               s2*(-10*Power(t1,2) + 4*t1*(-6 + t2) + 
                  3*(-1 + Power(t2,2)))) + 
            Power(s1,3)*(10*Power(t1,4) + Power(s2,4)*(11 + t1 + 3*t2) + 
               Power(t1,3)*(-46 - 20*t2 + Power(t2,2)) + 
               2*Power(-1 + t2,2)*(8 - 7*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-71 + 126*t2 + 33*Power(t2,2) - 
                  2*Power(t2,3)) + 
               t1*(-67 + 85*t2 + 8*Power(t2,2) - 27*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(55 - 3*Power(t1,2) + 11*t2 + 
                  3*Power(t2,2) - 2*t1*(22 + 5*t2)) + 
               Power(s2,2)*(-103 + 3*Power(t1,3) + 140*t2 + 
                  49*Power(t2,2) + Power(t1,2)*(65 + 11*t2) - 
                  t1*(157 + 36*t2 + 10*Power(t2,2))) - 
               s2*(-37 + Power(t1,4) + 36*t2 + 15*Power(t2,2) - 
                  14*Power(t2,3) + Power(t1,3)*(42 + 4*t2) - 
                  Power(t1,2)*(148 + 45*t2 + 6*Power(t2,2)) + 
                  t1*(-187 + 295*t2 + 63*Power(t2,2) + Power(t2,3)))) + 
            s1*(3*Power(t1,5) + Power(s2,4)*(2 + 2*t1 - 7*t2) - 
               Power(t1,4)*(10 + t2) + 
               Power(t1,3)*(61 - 49*t2 - 9*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (-39 - 7*t2 + 33*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(31 - 11*t2 + 22*Power(t2,2) + 
                  6*Power(t2,3)) + 
               2*t1*(-38 + 81*t2 - 77*Power(t2,2) + 32*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,3)*(-11 - 9*Power(t1,2) - 18*t2 + 
                  4*Power(t2,2) + t1*(3 + 25*t2)) + 
               Power(s2,2)*(10 + 15*Power(t1,3) + 12*t2 + Power(t2,2) + 
                  25*Power(t2,3) - 2*Power(t1,2)*(11 + 15*t2) + 
                  t1*(98 - 31*t2 - 14*Power(t2,2))) - 
               s2*(28 + 11*Power(t1,4) - 75*t2 + 9*Power(t2,2) + 
                  33*Power(t2,3) + 5*Power(t2,4) - 
                  Power(t1,3)*(27 + 13*t2) + 
                  Power(t1,2)*(148 - 98*t2 - 19*Power(t2,2)) + 
                  2*t1*(17 + 23*Power(t2,2) + 8*Power(t2,3)))) + 
            Power(s1,2)*(-3*Power(t1,5) + Power(t1,4)*(11 + 6*t2) + 
               Power(s2,4)*(-9 - 3*t1 + 8*t2) + 
               Power(t1,3)*(-33 + 35*t2) + 
               Power(s2,3)*(-31 + 12*Power(t1,2) + t1*(10 - 26*t2) + 
                  6*t2 + 11*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (44 - 45*t2 - 14*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(95 - 112*t2 + 68*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(-8 + 160*t2 - 163*Power(t2,2) + 6*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s2,2)*(7 - 18*Power(t1,3) - 8*t2 - 42*Power(t2,2) - 
                  15*Power(t2,3) + 2*Power(t1,2)*(9 + 17*t2) + 
                  t1*(36 + 5*t2 - 11*Power(t2,2))) + 
               s2*(61 + 12*Power(t1,4) + Power(t1,2)*(28 - 46*t2) - 
                  233*t2 + 152*Power(t2,2) + 23*Power(t2,3) - 
                  3*Power(t2,4) - 2*Power(t1,3)*(15 + 11*t2) + 
                  t1*(67 - 71*t2 + 107*Power(t2,2) + 13*Power(t2,3))))))*
       B1(1 - s2 + t1 - t2,s1,1 - s + s1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*
       (-1 + t2)*(-1 + s2 - t1 + t2)) - 
    (16*(Power(s,5)*(-1 + s1)*
          (1 - s2 + t1 - t2 + s1*(3 + 4*t1 - 2*t2)*(-1 + s2 - t1 + t2) - 
            Power(s1,2)*(-1 + s2 - t1 + t2)*(7 + 2*t1 + 2*t2) + 
            Power(s1,3)*(-5 - 7*t1 + s2*(5 + 2*t1) + 5*t2 - 2*t1*t2 + 
               2*Power(t2,2))) + 
         Power(s,4)*(-(Power(s1,6)*(-1 + s2 - t1 + t2)) + 
            (-1 + s2 - t1 + t2)*(-5 + 4*t2) + 
            Power(s1,5)*(2*Power(s2,2) + Power(t1,2) - 5*s2*(2 + t1) + 
               7*t1*(1 + t2) - 2*(-4 + t2 + 4*Power(t2,2))) + 
            Power(s1,4)*(-10 + Power(t1,3) - Power(s2,2)*(13 + 2*t1) - 
               22*t2 + 33*Power(t2,2) + 5*Power(t2,3) - 
               Power(t1,2)*(3 + 5*t2) - t1*(2 + 38*t2 + Power(t2,2)) + 
               s2*(23 + Power(t1,2) + 11*t2 - 5*Power(t2,2) + 
                  3*t1*(5 + 4*t2))) - 
            s1*(15 + Power(t1,2)*(22 - 15*t2) - 40*t2 + 
               32*Power(t2,2) - 7*Power(t2,3) + 
               Power(s2,2)*(1 + 4*t2) + 
               t1*(37 - 62*t2 + 22*Power(t2,2)) + 
               s2*(-16 + 22*t2 - 3*Power(t2,2) + t1*(-23 + 11*t2))) + 
            Power(s1,2)*(32 - 2*Power(t1,3) - 
               20*Power(t1,2)*(-1 + t2) - 64*t2 + 34*Power(t2,2) + 
               Power(s2,2)*(1 - 4*t1 + 6*t2) + 
               t1*(52 - 78*t2 + 22*Power(t2,2)) + 
               s2*(-33 + 6*Power(t1,2) + 27*t2 + 6*Power(t2,2) + 
                  t1*(-17 + 12*t2))) + 
            Power(s1,3)*(-21 + Power(t1,3) + 62*t2 + 10*Power(t1,2)*t2 - 
               35*Power(t2,2) - 12*Power(t2,3) + 
               Power(s2,2)*(11 + 2*t1 + 2*t2) + 
               t1*(-30 + 59*t2 + Power(t2,2)) - 
               s2*(-10 + 3*Power(t1,2) + 28*t2 + t1*(8 + 21*t2)))) + 
         Power(s,3)*(Power(s1,7)*(-1 + s2 - t1 + t2) + 
            (-1 + s2 - t1 + t2)*(10 - 16*t2 + 5*Power(t2,2)) + 
            Power(s1,6)*(-2 - 2*Power(t1,2) - 7*t2 + 9*Power(t2,2) + 
               s2*(4 + 2*t1 + 3*t2) - t1*(2 + 5*t2)) - 
            Power(s1,2)*(52 - 137*t2 + 98*Power(t2,2) - 
               12*Power(t2,3) - Power(t2,4) + Power(s2,3)*(2 + 4*t2) + 
               Power(t1,3)*(-11 + 7*t2) + 
               Power(t1,2)*(15 - 31*t2 + 21*Power(t2,2)) + 
               t1*(66 - 160*t2 + 90*Power(t2,2) - 27*Power(t2,3)) + 
               s2*(-55 + Power(t1,2)*(32 - 18*t2) + 71*t2 + 
                  4*t1*(-3 + t2)*t2 + 37*Power(t2,2) - 22*Power(t2,3)) \
+ Power(s2,2)*(1 + 19*t2 - 17*Power(t2,2) + t1*(-23 + 7*t2))) + 
            Power(s1,5)*(-2*Power(s2,3) - 3*Power(t1,3) + 
               Power(s2,2)*(4 + 5*t1 + 7*t2) + 
               Power(t1,2)*(2 + 19*t2) + 
               t1*(3 + 24*t2 - 2*Power(t2,2)) - 
               7*t2*(-5 + 3*t2 + 2*Power(t2,2)) - 
               s2*(8 + 36*t2 - 19*Power(t2,2) + t1*(2 + 30*t2))) + 
            Power(s1,3)*(36 + 2*Power(s2,3) - 106*t2 + 
               111*Power(t2,2) - 27*Power(t2,3) - 14*Power(t2,4) + 
               Power(t1,3)*(-6 + 5*t2) + 
               Power(s2,2)*(-44 - 10*t1 + 40*t2 + 9*t1*t2 + 
                  2*Power(t2,2)) + 
               2*Power(t1,2)*(-10 + 5*t2 + 6*Power(t2,2)) + 
               t1*(20 - 95*t2 + 59*Power(t2,2) - 3*Power(t2,3)) + 
               s2*(4 - 14*Power(t1,2)*(-1 + t2) - 8*t2 + 
                  15*Power(t2,2) + 8*Power(t2,3) + 
                  t1*(68 - 38*t2 - 30*Power(t2,2)))) + 
            s1*(21 - 85*t2 + 103*Power(t2,2) - 48*Power(t2,3) + 
               9*Power(t2,4) + 
               Power(s2,2)*(4 + 14*t2 - 15*Power(t2,2)) + 
               Power(t1,2)*(48 - 64*t2 + 21*Power(t2,2)) + 
               t1*(69 - 176*t2 + 124*Power(t2,2) - 30*Power(t2,3)) - 
               s2*(25 - 54*t2 + 10*Power(t2,2) + 6*Power(t2,3) + 
                  t1*(52 - 50*t2 + 6*Power(t2,2)))) + 
            Power(s1,4)*(4 + 6*Power(s2,3) - 13*t2 - 47*Power(t2,2) + 
               52*Power(t2,3) + 4*Power(t2,4) + 2*Power(t1,3)*(3 + t2) + 
               Power(t1,2)*(15 - 32*t2 - 12*Power(t2,2)) + 
               t1*(3 + 12*t2 - 38*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(25 - 46*t2 + 4*Power(t2,2) - 
                  2*t1*(1 + 5*t2)) + 
               s2*(-29 + 94*t2 - 36*Power(t2,2) - 12*Power(t2,3) + 
                  2*Power(t1,2)*(-5 + 4*t2) + 
                  2*t1*(-24 + 39*t2 + 8*Power(t2,2))))) + 
         s*(-1 + t2)*(-((-1 + s2 - t1 + t2)*
               (5 - 11*t2 + 4*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,7)*(4*Power(s2,2) + 4*Power(t1,2) - 
               9*t1*(-1 + t2) + Power(-1 + t2,2) + s2*(-9 - 8*t1 + 9*t2)\
) + Power(s1,6)*(-2*Power(s2,3) + 2*Power(t1,3) + 
               Power(t1,2)*(7 - 13*t2) - 3*Power(-1 + t2,2) + 
               Power(s2,2)*(-7 + 6*t1 + t2) + 
               s2*(26 - 6*Power(t1,2) - 11*t2 + 12*t1*t2 - 
                  15*Power(t2,2)) + t1*(-24 + 7*t2 + 17*Power(t2,2))) - 
            Power(s1,5)*(6*Power(s2,3)*(-2 + t2) + 
               Power(t1,3)*(1 + t2) + Power(-1 + t2,2)*(-5 + 2*t2) + 
               Power(t1,2)*(31 + 6*t2 - 11*Power(t2,2)) + 
               Power(s2,2)*(5 + t1*(37 - 23*t2) + 4*t2 + 
                  17*Power(t2,2)) + 
               t1*(-37 + 11*t2 + 16*Power(t2,2) + 10*Power(t2,3)) + 
               s2*(34 - 8*t2 - 13*Power(t2,2) - 13*Power(t2,3) + 
                  2*Power(t1,2)*(-13 + 8*t2) - 
                  2*t1*(20 + t2 + 5*Power(t2,2)))) - 
            Power(s1,4)*(-2*Power(t1,3)*(-6 + t2) + 
               2*Power(s2,3)*(12 - 12*t2 + Power(t2,2)) - 
               Power(-1 + t2,2)*(2 - 26*t2 + 3*Power(t2,2)) + 
               2*Power(t1,2)*
                (-4 - 34*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               t1*(54 - 22*t2 - 31*Power(t2,2) + Power(t2,3) - 
                  2*Power(t2,4)) + 
               s2*(-50 + 58*t2 - 53*Power(t2,2) + 43*Power(t2,3) + 
                  2*Power(t2,4) + 
                  2*t1*t2*(59 + 6*t2 + 3*Power(t2,2)) - 
                  2*Power(t1,2)*(-6 + 10*t2 + 5*Power(t2,2))) + 
               Power(s2,2)*(6 - 44*t2 - 24*Power(t2,2) - 
                  6*Power(t2,3) + t1*(-48 + 46*t2 + 8*Power(t2,2)))) - 
            s1*(Power(t1,2)*(28 - 44*t2 + 21*Power(t2,2) - 
                  3*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-3 + 18*t2 + 17*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(4 + 14*t2 - 29*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(31 - 93*t2 + 66*Power(t2,2) - 8*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s2*(-7 + 11*t2 + 42*Power(t2,2) - 58*Power(t2,3) + 
                  12*Power(t2,4) + 
                  t1*(-32 + 30*t2 + 8*Power(t2,2) - 10*Power(t2,3)))) + 
            Power(s1,2)*(Power(s2,3)*(6 + 12*t2 - 14*Power(t2,2)) + 
               Power(t1,3)*(-17 + 12*t2 - 3*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (36 - 45*t2 - 69*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-11 + 44*t2 - 16*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(18 - 48*t2 - 40*Power(t2,2) + 69*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(1 + 10*t2 + 10*Power(t2,2) - 
                  3*Power(t2,3) + t1*(-37 + 4*t2 + 17*Power(t2,2))) + 
               s2*(-39 + Power(t1,2)*(48 - 28*t2) + 80*t2 + 
                  52*Power(t2,2) - 105*Power(t2,3) + 12*Power(t2,4) + 
                  t1*(26 - 94*t2 + 38*Power(t2,2) - 6*Power(t2,3)))) + 
            Power(s1,3)*(Power(t1,3)*(4 + 11*t2 + 3*Power(t2,2)) + 
               Power(s2,3)*(-4 - 6*t2 + 4*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (31 - 77*t2 - 23*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(1 + 9*t2 + 44*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(24 + 69*t2 - 106*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(53 - 93*t2 - 18*Power(t2,2) + 
                  2*Power(t2,3) + t1*(4 + 27*t2 - Power(t2,2))) - 
               s2*(32 - 12*t2 + 25*Power(t2,2) - 39*Power(t2,3) - 
                  6*Power(t2,4) + 
                  Power(t1,2)*(4 + 32*t2 + 6*Power(t2,2)) + 
                  2*t1*(29 - 54*t2 - 34*Power(t2,2) + 3*Power(t2,3))))) + 
         Power(-1 + t2,2)*(-(Power(s1,7)*(s2 - t1)*
               (-1 + 3*s2 - 3*t1 + t2)) - 
            (-1 + s2 - t1 + t2)*(1 - 2*t2 + Power(t2,3)) - 
            Power(s1,6)*(2*Power(s2,3) + 5*t1*(-1 + t2) + 
               Power(-1 + t2,2) - 2*Power(t1,2)*(9 + t2) - 
               4*Power(s2,2)*(4 + t1 + t2) + 
               s2*(5 + 2*Power(t1,2) - 5*t2 + t1*(34 + 6*t2))) + 
            Power(s1,5)*(4*Power(t1,3) - Power(s2,3)*(-5 + t2) + 
               8*Power(-1 + t2,2) - Power(t1,2)*(22 + 25*t2) - 
               Power(s2,2)*(19 + t1*(10 - 6*t2) + 21*t2 + 
                  7*Power(t2,2)) + t1*(10 - 21*t2 + 11*Power(t2,2)) + 
               s2*(-8 + Power(t1,2)*(1 - 5*t2) + 17*t2 - 
                  9*Power(t2,2) + t1*(43 + 42*t2 + 9*Power(t2,2)))) + 
            Power(s1,3)*(-29*Power(t1,2)*Power(t2,2) + 
               Power(t1,3)*(4 + 8*t2) + 
               Power(s2,3)*(-3 - 3*t2 + 2*Power(t2,2)) + 
               Power(-1 + t2,2)*(-8 + 31*t2 + 7*Power(t2,2)) + 
               t1*(12 + 23*t2 - 49*Power(t2,2) + 14*Power(t2,3)) + 
               Power(s2,2)*(19 - 32*t2 - 18*Power(t2,2) + 
                  2*Power(t2,3) + t1*(7 + 16*t2 - 3*Power(t2,2))) + 
               s2*(-16 + 3*t2 + 11*Power(t2,2) + 2*Power(t2,4) + 
                  Power(t1,2)*(-8 - 21*t2 + Power(t2,2)) + 
                  t1*(-20 + 33*t2 + 48*Power(t2,2) - 3*Power(t2,3)))) + 
            Power(s1,2)*(Power(t1,3)*(-5 + t2) + 
               Power(s2,3)*(2 + 5*t2 - 5*Power(t2,2)) - 
               Power(t1,2)*(2 - 11*t2 + Power(t2,2)) - 
               Power(-1 + t2,2)*(-12 + 15*t2 + 22*Power(t2,2)) + 
               t1*(7 - 17*t2 - 12*Power(t2,2) + 22*Power(t2,3)) - 
               Power(s2,2)*(-1 + t2 - 12*Power(t2,2) + 4*Power(t2,3) + 
                  t1*(11 + 5*t2 - 8*Power(t2,2))) + 
               s2*(-13 + 30*t2 + 5*Power(t2,2) - 23*Power(t2,3) + 
                  Power(t2,4) - 
                  Power(t1,2)*(-14 + t2 + 3*Power(t2,2)) + 
                  t1*(5 - 20*t2 - 3*Power(t2,2) + 2*Power(t2,3)))) - 
            Power(s1,4)*(Power(t1,3)*(11 + t2) + 
               Power(-1 + t2,2)*(8 + 13*t2) + 
               Power(t1,2)*(1 - 45*t2 - 8*Power(t2,2)) + 
               Power(s2,3)*(6 - 7*t2 + Power(t2,2)) + 
               t1*(32 - 38*t2 - Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(1 - 34*t2 - 18*Power(t2,2) - Power(t2,3) + 
                  t1*(-6 + 17*t2 + Power(t2,2))) - 
               s2*(29 - 41*t2 + 14*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t1,2)*(11 + 11*t2 + 2*Power(t2,2)) - 
                  t1*(-1 + 78*t2 + 25*Power(t2,2) + 2*Power(t2,3)))) + 
            s1*(2*Power(-1 + t2,2)*t2*(1 + 4*t2) + 
               Power(t1,2)*(-6 + 7*t2 - 2*Power(t2,2)) - 
               3*t1*(2 - 5*t2 + Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,2)*(1 + 4*t2 - 7*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(1 + t2 - 17*Power(t2,2) + 18*Power(t2,3) - 
                  3*Power(t2,4) + 
                  t1*(7 - 3*t2 - 5*Power(t2,2) + 3*Power(t2,3))))) + 
         Power(s,2)*((-1 + s2 - t1 + t2)*
             (-10 + 24*t2 - 15*Power(t2,2) + Power(t2,3)) + 
            Power(s1,7)*(Power(s2,2) + Power(t1,2) + t1*(-1 + t2) - 
               4*Power(-1 + t2,2) - s2*(-1 + 2*t1 + t2)) + 
            Power(s1,6)*(2*Power(t1,3) + Power(t1,2)*(3 - 17*t2) + 
               Power(s2,2)*(-11 + 2*t1 - 3*t2) + 
               Power(-1 + t2,2)*(11 + 10*t2) + 
               t1*(-4 - 7*t2 + 11*Power(t2,2)) + 
               s2*(4 - 4*Power(t1,2) + 7*t2 - 11*Power(t2,2) + 
                  4*t1*(2 + 5*t2))) - 
            Power(s1,5)*(Power(t1,3)*(2 + 4*t2) + 
               Power(s2,3)*(-9 + 7*t2) + 
               Power(t1,2)*(12 - 4*t2 - 29*Power(t2,2)) + 
               Power(s2,2)*(-8 + t1*(32 - 22*t2) - 18*t2 + 
                  5*Power(t2,2)) + 
               Power(-1 + t2,2)*(11 + 35*t2 + 6*Power(t2,2)) + 
               t1*(-17 + 4*t2 - 6*Power(t2,2) + 19*Power(t2,3)) + 
               s2*(8 + t2 + 23*Power(t2,2) - 32*Power(t2,3) + 
                  Power(t1,2)*(-25 + 11*t2) + 
                  t1*(-4 + 22*t2 + 24*Power(t2,2)))) + 
            Power(s1,4)*(-(Power(s2,3)*(24 - 23*t2 + Power(t2,2))) + 
               Power(t1,3)*(-8 + 9*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*
                (9 + 8*t2 + 29*Power(t2,2) + Power(t2,3)) - 
               Power(t1,2)*(15 - 69*t2 + 41*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(-40 + 14*t2 + 22*Power(t2,2) - 3*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,2)*(-29 + 55*t2 - 31*Power(t2,2) + 
                  9*Power(t2,3) + t1*(46 - 25*t2 - 15*Power(t2,2))) + 
               s2*(46 - 114*t2 + 158*Power(t2,2) - 81*Power(t2,3) - 
                  9*Power(t2,4) + 
                  Power(t1,2)*(-14 - 7*t2 + 15*Power(t2,2)) + 
                  2*t1*(27 - 71*t2 + 39*Power(t2,2) + Power(t2,3)))) - 
            s1*(Power(t1,2)*(52 - 102*t2 + 64*Power(t2,2) - 
                  13*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-13 + 50*t2 - 6*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,2)*(6 + 18*t2 - 44*Power(t2,2) + 
                  21*Power(t2,3)) + 
               t1*(65 - 217*t2 + 222*Power(t2,2) - 88*Power(t2,3) + 
                  18*Power(t2,4)) + 
               s2*(-19 + 51*t2 + 6*Power(t2,2) - 54*Power(t2,3) + 
                  16*Power(t2,4) - 
                  2*t1*(29 - 42*t2 + 10*Power(t2,2) + 4*Power(t2,3)))) + 
            Power(s1,3)*(Power(s2,3)*(-3 - t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(5 - 6*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(23 - 43*t2 - 3*Power(t2,2) + 
                  2*Power(t2,3)) - 
               4*Power(-1 + t2,2)*
                (11 - 19*t2 - Power(t2,2) + 2*Power(t2,3)) + 
               t1*(18 + 76*t2 - 106*Power(t2,2) + 13*Power(t2,3) - 
                  Power(t2,4)) + 
               Power(s2,2)*(73 - 125*t2 + 31*Power(t2,2) + 
                  t1*(5 - 4*t2 + 9*Power(t2,2))) - 
               s2*(30 - 47*t2 + 95*Power(t2,2) - 68*Power(t2,3) - 
                  10*Power(t2,4) + 
                  Power(t1,2)*(7 - 11*t2 + 18*Power(t2,2)) + 
                  2*t1*(53 - 87*t2 + 5*Power(t2,2) + 8*Power(t2,3)))) + 
            Power(s1,2)*(Power(s2,3)*(6 + 9*t2 - 13*Power(t2,2)) + 
               Power(t1,3)*(-21 + 25*t2 - 8*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (54 - 71*t2 - 58*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,2)*(8 - 29*t2 + 5*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(41 - 129*t2 + 58*Power(t2,2) + 18*Power(t2,3) + 
                  12*Power(t2,4)) + 
               Power(s2,2)*(t1*(-45 + 31*t2 + 6*Power(t2,2)) + 
                  t2*(25 - 27*t2 + 12*Power(t2,2))) + 
               s2*(-58 + 113*t2 + 64*Power(t2,2) - 146*Power(t2,3) + 
                  27*Power(t2,4) + 
                  5*Power(t1,2)*(12 - 13*t2 + 3*Power(t2,2)) - 
                  2*t1*(-16 + 57*t2 - 40*Power(t2,2) + 9*Power(t2,3))))))*
       R1q(s1))/(Power(-1 + s1,3)*s1*(-s + s1 - t2)*
       (1 - s + s*s1 - s1*s2 + s1*t1 - t2)*(-1 + t2)*Power(-1 + s + t2,3)*
       (-1 + s2 - t1 + t2)) - (16*
       (Power(s,8)*(Power(s1,2)*(-1 + s2 - t1 + t2) - 
            (-1 + s2 - t1 + t2)*(2 + 3*t1 + 2*t2) + 
            s1*(-1 + s2 - 4*t1 + 3*s2*t1 - Power(t1,2) - t2 + 2*s2*t2 - 
               3*t1*t2 + 4*Power(t2,2))) - 
         Power(s,7)*(-2 + 10*t1 + 12*Power(t1,2) - 2*Power(t1,3) + 
            9*t2 - 32*t1*t2 - 21*Power(t1,2)*t2 - 15*Power(t2,2) + 
            13*t1*Power(t2,2) + 10*Power(t2,3) + 
            5*Power(s1,3)*(-1 + s2 - t1 + t2) - 
            Power(s2,2)*(4 + 2*t1 + 5*t2) + 
            Power(s1,2)*(-9 - 4*Power(t1,2) - 2*t2 + 19*Power(t2,2) + 
               s2*(9 + 12*t1 + 7*t2) - t1*(17 + 15*t2)) + 
            s2*(6 + 4*Power(t1,2) - 6*t2 + 5*Power(t2,2) + 
               t1*(-6 + 26*t2)) + 
            s1*(19 - 20*t2 + 10*Power(t2,2) - 15*Power(t2,3) + 
               20*Power(t1,2)*(1 + t2) + Power(s2,2)*(7 + 5*t1 + 7*t2) - 
               5*t1*(-5 - t2 + Power(t2,2)) - 
               s2*(26 + 5*Power(t1,2) - 5*t2 - 8*Power(t2,2) + 
                  t1*(22 + 38*t2)))) + 
         Power(s,6)*(-34 - 8*t1 + 16*Power(t1,2) - 25*Power(t1,3) + 
            Power(t1,4) + Power(s2,3)*(t1 - 4*t2) + 87*t2 - 80*t1*t2 - 
            55*Power(t1,2)*t2 + 11*Power(t1,3)*t2 - 103*Power(t2,2) + 
            117*t1*Power(t2,2) + 60*Power(t1,2)*Power(t2,2) + 
            68*Power(t2,3) - 52*t1*Power(t2,3) - 20*Power(t2,4) + 
            10*Power(s1,4)*(-1 + s2 - t1 + t2) + 
            Power(s1,3)*(-25 + Power(s2,2) - 7*Power(t1,2) - 3*t2 + 
               40*Power(t2,2) + s2*(26 + 18*t1 + 11*t2) - 
               t1*(30 + 31*t2)) + 
            s2*(52 - Power(t1,3) + Power(t1,2)*(46 - 30*t2) - 48*t2 + 
               18*Power(t2,2) + 5*Power(t2,3) + 
               t1*(8 + 60*t2 - 79*Power(t2,2))) - 
            Power(s2,2)*(Power(t1,2) + t1*(21 - 23*t2) + 
               3*(6 + 5*t2 - 7*Power(t2,2))) + 
            Power(s1,2)*(59 - 2*Power(s2,3) - 2*Power(t1,3) - 74*t2 + 
               62*Power(t2,2) - 65*Power(t2,3) + 
               Power(s2,2)*(32 + 21*t1 + 34*t2) + 
               Power(t1,2)*(41 + 97*t2) + 
               t1*(68 + 23*t2 - 30*Power(t2,2)) - 
               s2*(93 + 17*Power(t1,2) - 13*t2 - 55*Power(t2,2) + 
                  6*t1*(9 + 28*t2))) + 
            s1*(27 + 2*Power(t1,4) - 58*t2 - 22*Power(t1,3)*t2 + 
               28*Power(t2,2) - 9*Power(t2,3) + 22*Power(t2,4) + 
               Power(s2,3)*(5 + t1 + 9*t2) + 
               Power(t1,2)*(67 - 78*t2 - 45*Power(t2,2)) + 
               Power(s2,2)*(-4 + t1*(6 - 69*t2) + t2 - 2*Power(t2,2)) + 
               t1*(58 - 84*t2 - 18*Power(t2,2) + 43*Power(t2,3)) + 
               s2*(-26 - 3*Power(t1,3) + 37*t2 + 15*Power(t2,2) - 
                  45*Power(t2,3) + Power(t1,2)*(-11 + 82*t2) + 
                  t1*(-58 + 58*t2 + 71*Power(t2,2))))) + 
         Power(s,5)*(85 + 9*t1 - 8*Power(t1,2) + 110*Power(t1,3) - 
            10*Power(t1,4) + Power(s2,4)*(-2 + t2) - 198*t2 + 
            186*t1*t2 - 2*Power(t1,2)*t2 - 106*Power(t1,3)*t2 + 
            5*Power(t1,4)*t2 + 229*Power(t2,2) - 387*t1*Power(t2,2) - 
            113*Power(t1,2)*Power(t2,2) + 25*Power(t1,3)*Power(t2,2) - 
            212*Power(t2,3) + 288*t1*Power(t2,3) + 
            90*Power(t1,2)*Power(t2,3) + 116*Power(t2,4) - 
            100*t1*Power(t2,4) - 20*Power(t2,5) - 
            10*Power(s1,5)*(-1 + s2 - t1 + t2) + 
            Power(s2,3)*(-8 + 14*t2 - 19*Power(t2,2) + 4*t1*(1 + t2)) + 
            Power(s1,4)*(27 - 2*Power(s2,2) + 8*Power(t1,2) + 15*t2 - 
               50*Power(t2,2) - s2*(33 + 14*t1 + 12*t2) + 
               t1*(27 + 34*t2)) + 
            Power(s2,2)*(36 + 22*t2 - 117*Power(t2,2) + 
               30*Power(t2,3) - 6*Power(t1,2)*(2 + t2) + 
               t1*(102 - 134*t2 + 87*Power(t2,2))) - 
            s2*(113 + 4*Power(t1,3)*(-5 + t2) - 72*t2 - 
               81*Power(t2,2) + 66*Power(t2,3) - 30*Power(t2,4) + 
               Power(t1,2)*(204 - 226*t2 + 93*Power(t2,2)) + 
               2*t1*(11 + 4*t2 - 100*Power(t2,2) + 54*Power(t2,3))) + 
            Power(s1,3)*(-89 + 7*Power(s2,3) + 6*Power(t1,3) + 113*t2 - 
               125*Power(t2,2) + 119*Power(t2,3) - 
               Power(s2,2)*(69 + 35*t1 + 64*t2) - 
               Power(t1,2)*(28 + 193*t2) + 
               t1*(-115 - 53*t2 + 80*Power(t2,2)) + 
               s2*(161 + 22*Power(t1,2) + 23*t2 - 132*Power(t2,2) + 
                  t1*(70 + 302*t2))) - 
            Power(s1,2)*(70 + 10*Power(t1,4) + 
               Power(t1,3)*(17 - 90*t2) - 193*t2 + 149*Power(t2,2) - 
               88*Power(t2,3) + 76*Power(t2,4) + 
               2*Power(s2,3)*(5 + 24*t2) + 
               Power(t1,2)*(100 - 14*t2 - 183*Power(t2,2)) + 
               Power(s2,2)*(-34 + 10*Power(t1,2) + t1*(57 - 280*t2) + 
                  44*t2 + 9*Power(t2,2)) + 
               t1*(63 - 15*t2 - 216*Power(t2,2) + 187*Power(t2,3)) - 
               s2*(48 + 20*Power(t1,3) + Power(t1,2)*(84 - 322*t2) - 
                  71*t2 - 145*Power(t2,2) + 215*Power(t2,3) + 
                  t1*(62 + 84*t2 - 238*Power(t2,2)))) + 
            s1*(30 + Power(t1,5) + Power(s2,4)*(3 + t1 - 5*t2) - 
               152*t2 - 3*Power(t1,4)*t2 + 260*Power(t2,2) - 
               175*Power(t2,3) + 25*Power(t2,4) + 16*Power(t2,5) + 
               Power(t1,3)*(21 + 66*t2 - 68*Power(t2,2)) - 
               3*Power(t1,2)*
                (19 - 118*t2 + 85*Power(t2,2) + 6*Power(t2,3)) + 
               t1*(7 + 78*t2 - 85*Power(t2,2) - 11*Power(t2,3) + 
                  72*Power(t2,4)) - 
               Power(s2,3)*(14 + 4*Power(t1,2) + 13*t2 - 
                  14*Power(t2,2) - 9*t1*(-3 + 4*t2)) + 
               Power(s2,2)*(41 + 6*Power(t1,3) + 
                  Power(t1,2)*(45 - 60*t2) + 23*t2 - 17*Power(t2,2) + 
                  45*Power(t2,3) + t1*(38 + 152*t2 - 165*Power(t2,2))) - 
               s2*(62 + 4*Power(t1,4) + Power(t1,3)*(21 - 32*t2) - 
                  141*t2 + 130*Power(t2,2) - 52*Power(t2,3) + 
                  70*Power(t2,4) + 
                  Power(t1,2)*(45 + 205*t2 - 219*Power(t2,2)) + 
                  t1*(-3 + 358*t2 - 245*Power(t2,2) + 2*Power(t2,3))))) + 
         Power(s,4)*(-113 + 93*t1 + 24*Power(t1,2) - 239*Power(t1,3) + 
            31*Power(t1,4) + 117*t2 - 713*t1*t2 + 72*Power(t1,2)*t2 + 
            387*Power(t1,3)*t2 - 35*Power(t1,4)*t2 + 248*Power(t2,2) + 
            1429*t1*Power(t2,2) - 31*Power(t1,2)*Power(t2,2) - 
            199*Power(t1,3)*Power(t2,2) + 10*Power(t1,4)*Power(t2,2) - 
            342*Power(t2,3) - 1158*t1*Power(t2,3) - 
            118*Power(t1,2)*Power(t2,3) + 30*Power(t1,3)*Power(t2,3) + 
            16*Power(t2,4) + 443*t1*Power(t2,4) + 
            75*Power(t1,2)*Power(t2,4) + 84*Power(t2,5) - 
            105*t1*Power(t2,5) - 10*Power(t2,6) + 
            5*Power(s1,6)*(-1 + s2 - t1 + t2) + 
            Power(s2,4)*(8 - 9*t2 + 5*Power(t2,2)) + 
            Power(s1,5)*(-6 - 7*Power(t1,2) - 32*t2 + 40*Power(t2,2) + 
               s2*(18 + 9*t1 + 10*t2) - t1*(11 + 21*t2)) + 
            Power(s2,3)*(32 - 52*t2 + 72*Power(t2,2) - 35*Power(t2,3) + 
               t1*(-25 + 2*t2 + 5*Power(t2,2))) - 
            Power(s2,2)*(36 + 52*t2 - 318*Power(t2,2) + 
               220*Power(t2,3) - 10*Power(t2,4) + 
               3*Power(t1,2)*(-19 + 4*t2 + 5*Power(t2,2)) + 
               t1*(243 - 431*t2 + 403*Power(t2,2) - 160*Power(t2,3))) + 
            s2*(117 + 183*t2 - 936*Power(t2,2) + 859*Power(t2,3) - 
               252*Power(t2,4) + 40*Power(t2,5) + 
               Power(t1,3)*(-71 + 54*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(450 - 766*t2 + 530*Power(t2,2) - 
                  155*Power(t2,3)) + 
               t1*(-38 + 70*t2 - 287*Power(t2,2) + 268*Power(t2,3) - 
                  55*Power(t2,4))) - 
            Power(s1,4)*(-58 + 10*Power(s2,3) + 6*Power(t1,3) + 
               Power(t1,2)*(15 - 202*t2) + 78*t2 - 129*Power(t2,2) + 
               115*Power(t2,3) - Power(s2,2)*(71 + 31*t1 + 67*t2) + 
               t1*(-85 - 109*t2 + 121*Power(t2,2)) + 
               s2*(121 + 15*Power(t1,2) + 102*t2 - 162*Power(t2,2) + 
                  t1*(39 + 292*t2))) + 
            Power(s1,3)*(97 - 2*Power(s2,4) + 17*Power(t1,4) + 
               Power(t1,3)*(35 - 143*t2) - 262*t2 + 194*Power(t2,2) - 
               126*Power(t2,3) + 103*Power(t2,4) + 
               Power(s2,3)*(13 + 82*t2) + 
               Power(t1,2)*(11 + 199*t2 - 282*Power(t2,2)) + 
               Power(s2,2)*(-79 + 23*Power(t1,2) + t1*(93 - 420*t2) + 
                  36*t2 + 48*Power(t2,2)) + 
               t1*(20 + 107*t2 - 468*Power(t2,2) + 335*Power(t2,3)) + 
               s2*(-51 - 38*Power(t1,3) + 56*t2 + 372*Power(t2,2) - 
                  383*Power(t2,3) + Power(t1,2)*(-141 + 481*t2) + 
                  t1*(69 - 286*t2 + 290*Power(t2,2)))) + 
            Power(s1,2)*(-147 + Power(s2,5) - 4*Power(t1,5) + 542*t2 - 
               689*Power(t2,2) + 341*Power(t2,3) - 10*Power(t2,4) - 
               39*Power(t2,5) + Power(t1,4)*(3 + 7*t2) + 
               Power(s2,4)*(-6 - 8*t1 + 17*t2) + 
               Power(s2,3)*(-14 + 22*Power(t1,2) + t1*(59 - 108*t2) + 
                  139*t2 - 64*Power(t2,2)) + 
               Power(t1,3)*(87 - 337*t2 + 232*Power(t2,2)) + 
               Power(t1,2)*(121 - 574*t2 + 290*Power(t2,2) + 
                  42*Power(t2,3)) + 
               t1*(-230 + 625*t2 - 619*Power(t2,2) + 404*Power(t2,3) - 
                  238*Power(t2,4)) + 
               s2*(306 + 17*Power(t1,4) + Power(t1,3)*(41 - 88*t2) - 
                  830*t2 + 688*Power(t2,2) - 354*Power(t2,3) + 
                  252*Power(t2,4) + 
                  Power(t1,2)*(-231 + 1025*t2 - 713*Power(t2,2)) + 
                  t1*t2*(454 - 297*t2 + 122*Power(t2,2))) + 
               Power(s2,2)*(-130 - 28*Power(t1,3) + 143*t2 + 
                  36*Power(t2,2) - 209*Power(t2,3) + 
                  Power(t1,2)*(-97 + 172*t2) + 
                  t1*(158 - 827*t2 + 545*Power(t2,2)))) + 
            s1*(13 + Power(s2,5)*(-2 + t2) + 141*t2 - 722*Power(t2,2) + 
               1040*Power(t2,3) - 530*Power(t2,4) + 52*Power(t2,5) + 
               6*Power(t2,6) + Power(t1,5)*(-1 + 2*t2) + 
               Power(t1,4)*(-16 + 35*t2 - 22*Power(t2,2)) + 
               Power(s2,4)*(-15 + t1*(7 - 2*t2) + 24*t2 - 
                  10*Power(t2,2)) + 
               Power(t1,3)*(-114 + 5*t2 + 159*Power(t2,2) - 
                  72*Power(t2,3)) + 
               3*Power(t1,2)*t2*
                (-171 + 336*t2 - 164*Power(t2,2) + 11*Power(t2,3)) + 
               t1*(46 - 301*t2 + 327*Power(t2,2) - 162*Power(t2,3) + 
                  72*Power(t2,4) + 53*Power(t2,5)) - 
               Power(s2,3)*(-44 + 2*t2 + 29*Power(t2,2) + 
                  15*Power(t2,3) + 2*Power(t1,2)*(4 + t2) + 
                  t1*(-106 + 197*t2 - 97*Power(t2,2))) + 
               Power(s2,2)*(-10 - 301*t2 + 351*Power(t2,2) - 
                  86*Power(t2,3) + 80*Power(t2,4) + 
                  Power(t1,3)*(2 + 8*t2) - 
                  3*Power(t1,2)*(61 - 119*t2 + 62*Power(t2,2)) + 
                  t1*(-212 + 44*t2 + 277*Power(t2,2) - 127*Power(t2,3))) \
+ s2*(-36 + Power(t1,4)*(2 - 7*t2) + 157*t2 + 80*Power(t2,2) - 
                  207*Power(t2,3) + 21*Power(t2,4) - 50*Power(t2,5) + 
                  Power(t1,3)*(108 - 219*t2 + 121*Power(t2,2)) + 
                  Power(t1,2)*
                   (282 - 47*t2 - 407*Power(t2,2) + 214*Power(t2,3)) + 
                  t1*(35 + 739*t2 - 1294*Power(t2,2) + 553*Power(t2,3) - 
                     103*Power(t2,4))))) - 
         Power(s,3)*(-100 + 232*t1 + 81*Power(t1,2) - 289*Power(t1,3) + 
            44*Power(t1,4) - 135*t2 - 1594*t1*t2 - 134*Power(t1,2)*t2 + 
            694*Power(t1,3)*t2 - 74*Power(t1,4)*t2 + 1514*Power(t2,2) + 
            3744*t1*Power(t2,2) + 62*Power(t1,2)*Power(t2,2) - 
            620*Power(t1,3)*Power(t2,2) + 40*Power(t1,4)*Power(t2,2) - 
            2767*Power(t2,3) - 3946*t1*Power(t2,3) + 
            236*Power(t1,3)*Power(t2,3) - 10*Power(t1,4)*Power(t2,3) + 
            2026*Power(t2,4) + 1896*t1*Power(t2,4) + 
            22*Power(t1,2)*Power(t2,4) - 20*Power(t1,3)*Power(t2,4) - 
            529*Power(t2,5) - 392*t1*Power(t2,5) - 
            33*Power(t1,2)*Power(t2,5) - 11*Power(t2,6) + 
            61*t1*Power(t2,6) + 2*Power(t2,7) + 
            Power(s1,7)*(-1 + s2 - t1 + t2) - 
            2*Power(s2,4)*(-6 + 9*t2 - 8*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s1,6)*(8 - 2*Power(s2,2) - 4*Power(t1,2) - 27*t2 - 
               7*t1*t2 + 19*Power(t2,2) + s2*(2 + 6*t1 + 5*t2)) + 
            Power(s2,3)*(51 - 126*t2 + 192*Power(t2,2) - 
               148*Power(t2,3) + 30*Power(t2,4) + 
               8*t1*(-5 + t2 + 4*Power(t2,2))) + 
            Power(s2,2)*(-7 - 150*t2 + 574*Power(t2,2) - 
               584*Power(t2,3) + 150*Power(t2,4) + 15*Power(t2,5) + 
               4*Power(t1,2)*
                (22 - 9*t2 - 18*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(-311 + 786*t2 - 1004*Power(t2,2) + 692*Power(t2,3) - 
                  160*Power(t2,4))) + 
            s2*(56 + 656*t2 - 2682*Power(t2,2) + 3478*Power(t2,3) - 
               1796*Power(t2,4) + 310*Power(t2,5) - 23*Power(t2,6) - 
               8*Power(t1,3)*(13 - 15*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(549 - 1354*t2 + 1432*Power(t2,2) - 
                  780*Power(t2,3) + 150*Power(t2,4)) - 
               2*t1*(82 - 282*t2 + 448*Power(t2,2) - 292*Power(t2,3) + 
                  31*Power(t2,4) + 11*Power(t2,5))) - 
            Power(s1,5)*(-1 + 7*Power(s2,3) + 2*Power(t1,3) + 10*t2 - 
               66*Power(t2,2) + 57*Power(t2,3) - 
               9*Power(t1,2)*(-4 + 13*t2) - 
               Power(s2,2)*(24 + 16*t1 + 47*t2) + 
               t1*(7 - 138*t2 + 106*Power(t2,2)) + 
               s2*(4 + 7*Power(t1,2) + 138*t2 - 117*Power(t2,2) + 
                  8*t1*(-2 + 21*t2))) + 
            Power(s1,4)*(70 - 3*Power(s2,4) + 12*Power(t1,4) + 
               Power(t1,3)*(40 - 111*t2) - 140*t2 + 68*Power(t2,2) - 
               61*Power(t2,3) + 63*Power(t2,4) + 
               Power(s2,3)*(3 + t1 + 68*t2) + 
               Power(t1,2)*(-68 + 216*t2 - 191*Power(t2,2)) + 
               Power(s2,2)*(-43 + 19*Power(t1,2) + t1*(86 - 307*t2) - 
                  68*t2 + 80*Power(t2,2)) + 
               t1*(4 + 139*t2 - 467*Power(t2,2) + 307*Power(t2,3)) + 
               s2*(-57 - 29*Power(t1,3) + 6*t2 + 396*Power(t2,2) - 
                  328*Power(t2,3) + Power(t1,2)*(-129 + 350*t2) + 
                  t1*(111 - 164*t2 + 127*Power(t2,2)))) + 
            Power(s1,3)*(-189 + Power(s2,5) - 5*Power(t1,5) + 562*t2 - 
               607*Power(t2,2) + 273*Power(t2,3) - 7*Power(t2,4) - 
               32*Power(t2,5) + 5*Power(t1,4)*(3 + t2) + 
               Power(s2,4)*(-2 - 9*t1 + 19*t2) + 
               Power(s2,3)*(-47 + 26*Power(t1,2) + t1*(31 - 106*t2) + 
                  190*t2 - 66*Power(t2,2)) + 
               Power(t1,3)*(161 - 485*t2 + 278*Power(t2,2)) + 
               Power(t1,2)*(86 - 253*t2 + 73*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(-370 + 979*t2 - 949*Power(t2,2) + 619*Power(t2,3) - 
                  298*Power(t2,4)) + 
               Power(s2,2)*(-213 - 34*Power(t1,3) + 299*t2 + 
                  160*Power(t2,2) - 332*Power(t2,3) + 
                  Power(t1,2)*(-41 + 160*t2) + 
                  t1*(333 - 1121*t2 + 592*Power(t2,2))) + 
               s2*(452 + 21*Power(t1,4) - 1064*t2 + 835*Power(t2,2) - 
                  524*Power(t2,3) + 320*Power(t2,4) - 
                  3*Power(t1,3)*(1 + 26*t2) - 
                  3*Power(t1,2)*(149 - 472*t2 + 268*Power(t2,2)) + 
                  t1*(93 + 28*t2 - 259*Power(t2,2) + 306*Power(t2,3)))) \
- Power(s1,2)*(166 + Power(t1,5)*(4 - 7*t2) - 1161*t2 + 
               3*Power(s2,5)*t2 + 2640*Power(t2,2) - 2559*Power(t2,3) + 
               984*Power(t2,4) - 61*Power(t2,5) - 9*Power(t2,6) + 
               Power(s2,4)*(39 + t1*(4 - 19*t2) - 64*t2 + 
                  30*Power(t2,2)) + 
               Power(t1,4)*(59 - 102*t2 + 58*Power(t2,2)) + 
               Power(t1,3)*(-132 + 552*t2 - 605*Power(t2,2) + 
                  178*Power(t2,3)) + 
               Power(t1,2)*(-283 + 1551*t2 - 1956*Power(t2,2) + 
                  765*Power(t2,3) - 102*Power(t2,4)) + 
               t1*(124 - 679*t2 + 1248*Power(t2,2) - 794*Power(t2,3) + 
                  188*Power(t2,4) - 118*Power(t2,5)) + 
               Power(s2,3)*(86 - 390*t2 + 281*Power(t2,2) + 
                  42*Power(t2,3) + 2*Power(t1,2)*(-8 + 23*t2) + 
                  t1*(-252 + 470*t2 - 248*Power(t2,2))) + 
               Power(s2,2)*(-163 + 699*t2 - 628*Power(t2,2) + 
                  339*Power(t2,3) - 274*Power(t2,4) - 
                  6*Power(t1,3)*(-4 + 9*t2) + 
                  Power(t1,2)*(446 - 850*t2 + 464*Power(t2,2)) + 
                  t1*(-256 + 1352*t2 - 1415*Power(t2,2) + 
                     274*Power(t2,3))) + 
               s2*(-130 + 915*t2 - 1720*Power(t2,2) + 
                  1040*Power(t2,3) - 196*Power(t2,4) + 
                  122*Power(t2,5) + Power(t1,4)*(-16 + 31*t2) + 
                  Power(t1,3)*(-292 + 546*t2 - 304*Power(t2,2)) + 
                  Power(t1,2)*
                   (302 - 1514*t2 + 1739*Power(t2,2) - 494*Power(t2,3)) \
+ 2*t1*(211 - 1119*t2 + 1320*Power(t2,2) - 574*Power(t2,3) + 
                     188*Power(t2,4)))) - 
            s1*(-235 + 1080*t2 - 1419*Power(t2,2) + 22*Power(t2,3) + 
               1122*Power(t2,4) - 610*Power(t2,5) + 39*Power(t2,6) + 
               Power(t2,7) - 2*Power(t1,5)*(1 + t2) + 
               2*Power(s2,5)*(3 - 3*t2 + Power(t2,2)) + 
               Power(t1,4)*(34 - 96*t2 + 99*Power(t2,2) - 
                  28*Power(t2,3)) + 
               Power(t1,3)*(213 - 260*t2 - 38*Power(t2,2) + 
                  120*Power(t2,3) - 28*Power(t2,4)) + 
               Power(t1,2)*(-93 + 855*t2 - 1919*Power(t2,2) + 
                  1588*Power(t2,3) - 484*Power(t2,4) + 36*Power(t2,5)) + 
               t1*(-423 + 2081*t2 - 3192*Power(t2,2) + 
                  1838*Power(t2,3) - 433*Power(t2,4) + 
                  123*Power(t2,5) + 19*Power(t2,6)) + 
               Power(s2,4)*(30 - 64*t2 + 41*Power(t2,2) + 
                  t1*(-26 + 22*t2 - 8*Power(t2,2))) + 
               Power(s2,3)*(-95 + 96*t2 + 40*Power(t2,2) - 
                  6*Power(t2,3) - 40*Power(t2,4) + 
                  4*Power(t1,2)*(11 - 7*t2 + 3*Power(t2,2)) + 
                  2*t1*(-92 + 234*t2 - 201*Power(t2,2) + 44*Power(t2,3))\
) + Power(s2,2)*(-124 + 1058*t2 - 1807*Power(t2,2) + 902*Power(t2,3) - 
                  99*Power(t2,4) + 55*Power(t2,5) - 
                  4*Power(t1,3)*(9 - 3*t2 + 2*Power(t2,2)) - 
                  12*Power(t1,2)*
                   (-26 + 70*t2 - 65*Power(t2,2) + 17*Power(t2,3)) + 
                  t1*(408 - 612*t2 + 132*Power(t2,2) + 92*Power(t2,3) - 
                     3*Power(t2,4))) + 
               s2*(426 - 2189*t2 + 3291*Power(t2,2) - 1636*Power(t2,3) + 
                  148*Power(t2,4) - 35*Power(t2,5) - 18*Power(t2,6) + 
                  2*Power(t1,4)*(7 + t2 + Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-96 + 266*t2 - 259*Power(t2,2) + 72*Power(t2,3)) + 
                  Power(t1,2)*
                   (-526 + 776*t2 - 134*Power(t2,2) - 206*Power(t2,3) + 
                     71*Power(t2,4)) + 
                  t1*(210 - 1808*t2 + 3496*Power(t2,2) - 
                     2320*Power(t2,3) + 548*Power(t2,4) - 94*Power(t2,5))\
))) - Power(-1 + t2,2)*(-(Power(s1,7)*(s2 - t1)*
               (-1 + 3*s2 - 3*t1 + t2)) + 
            Power(s1,6)*(-8*Power(s2,3) + 6*Power(t1,3) - 
               Power(-1 + t2,2) + 4*Power(t1,2)*(4 + t2) + 
               2*Power(s2,2)*(7 + 11*t1 + 3*t2) - 
               t1*(-3 + t2 + 2*Power(t2,2)) + 
               s2*(-3 - 20*Power(t1,2) + t2 + 2*Power(t2,2) - 
                  10*t1*(3 + t2))) + 
            Power(s1,5)*(-7*Power(s2,4) - 3*Power(t1,4) + 
               2*Power(-1 + t2,2)*(3 + t2) - 6*Power(t1,3)*(1 + 2*t2) + 
               Power(s2,3)*(9 + 24*t1 + 17*t2) + 
               Power(t1,2)*(24 - 73*t2 + 2*Power(t2,2)) + 
               t1*(33 - 66*t2 + 32*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(-25 + 30*Power(t1,2) + 65*t2 + 
                  7*Power(t2,2) + 14*t1*(2 + 3*t2)) + 
               s2*(-31 + 16*Power(t1,3) + 62*t2 - 30*Power(t2,2) - 
                  Power(t2,3) + Power(t1,2)*(25 + 37*t2) + 
                  t1*(-47 + 134*t2 + 7*Power(t2,2)))) - 
            Power(s1,4)*(2*Power(s2,5) + 
               Power(s2,4)*(5 - 8*t1 - 15*t2) - 6*Power(t1,4)*t2 + 
               Power(t1,3)*(60 - 45*t2 - 7*Power(t2,2)) + 
               Power(-1 + t2,2)*(-17 + 37*t2 + Power(t2,2)) + 
               Power(t1,2)*(113 - 92*t2 - 78*Power(t2,2) + 
                  5*Power(t2,3)) + 
               t1*(26 + 28*t2 - 115*Power(t2,2) + 61*Power(t2,3)) + 
               Power(s2,3)*(-81 + 12*Power(t1,2) + 71*t2 + 
                  14*Power(t2,2) + t1*(-11 + 47*t2)) - 
               Power(s2,2)*(-120 + 8*Power(t1,3) + 108*t2 + 
                  55*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t1,2)*(-7 + 55*t2) + 
                  t1*(-223 + 197*t2 + 26*Power(t2,2))) + 
               s2*(-25 + 2*Power(t1,4) - 19*t2 + 94*Power(t2,2) - 
                  50*Power(t2,3) + Power(t1,3)*(-1 + 29*t2) + 
                  Power(t1,2)*(-202 + 171*t2 + 19*Power(t2,2)) + 
                  t1*(-236 + 211*t2 + 120*Power(t2,2) + 9*Power(t2,3)))) \
+ Power(s1,3)*(5*Power(s2,5)*(-1 + t2) + Power(t1,5)*(3 + t2) + 
               Power(s2,4)*(18 + 23*t1 - 21*t2 - 19*t1*t2 - 
                  9*Power(t2,2)) - 
               2*Power(t1,4)*(-9 + 14*t2 + 3*Power(t2,2)) + 
               Power(-1 + t2,2)*(-42 + 13*t2 + 59*Power(t2,2)) + 
               Power(t1,3)*(-87 + 149*t2 - 31*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,2)*(-267 + 681*t2 - 422*Power(t2,2) - 
                  23*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(-213 + 593*t2 - 473*Power(t2,2) + 52*Power(t2,3) + 
                  41*Power(t2,4)) + 
               Power(s2,3)*(93 - 203*t2 + 76*Power(t2,2) + 
                  10*Power(t2,3) + Power(t1,2)*(-42 + 26*t2) + 
                  t1*(-79 + 109*t2 + 22*Power(t2,2))) - 
               Power(s2,2)*(280 - 725*t2 + 475*Power(t2,2) - 
                  7*Power(t2,3) + 6*Power(t2,4) + 
                  2*Power(t1,3)*(-19 + 7*t2) + 
                  Power(t1,2)*(-122 + 183*t2 + 23*Power(t2,2)) + 
                  t1*(261 - 532*t2 + 177*Power(t2,2) + 14*Power(t2,3))) \
+ s2*(218 + Power(t1,4)*(-17 + t2) - 611*t2 + 513*Power(t2,2) - 
                  98*Power(t2,3) - 22*Power(t2,4) + 
                  Power(t1,3)*(-79 + 123*t2 + 16*Power(t2,2)) + 
                  Power(t1,2)*
                   (255 - 478*t2 + 132*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(542 - 1397*t2 + 898*Power(t2,2) + 7*Power(t2,3) + 
                     8*Power(t2,4)))) - 
            (-1 + t2)*(3 + 42*t2 - 198*Power(t2,2) + 319*Power(t2,3) - 
               228*Power(t2,4) + 62*Power(t2,5) + 
               Power(s2,4)*t2*(-3 + 2*t2 + Power(t2,2)) + 
               Power(t1,4)*(-1 - 4*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(12 - 32*t2 + 44*Power(t2,2) - 
                  25*Power(t2,3)) + 
               Power(t1,2)*(-14 + 52*t2 - 53*Power(t2,2) - 
                  7*Power(t2,3) + 24*Power(t2,4)) - 
               t1*(24 - 158*t2 + 348*Power(t2,2) - 323*Power(t2,3) + 
                  106*Power(t2,4) + 4*Power(t2,5)) - 
               Power(s2,3)*(3 - 14*t2 + 30*Power(t2,2) - 
                  19*Power(t2,3) - Power(t2,4) + 
                  t1*(1 - 19*t2 + 17*Power(t2,2) + Power(t2,3))) + 
               Power(s2,2)*(-5 + 27*t2 - 34*Power(t2,2) - 
                  4*Power(t2,3) + 18*Power(t2,4) - 
                  Power(t1,2)*
                   (-1 + 33*t2 - 33*Power(t2,2) + Power(t2,3)) + 
                  t1*(14 - 52*t2 + 104*Power(t2,2) - 71*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(5 - 92*t2 + 264*Power(t2,2) - 279*Power(t2,3) + 
                  101*Power(t2,4) + 2*Power(t2,5) + 
                  Power(t1,3)*
                   (1 + 21*t2 - 23*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-23 + 70*t2 - 118*Power(t2,2) + 77*Power(t2,3) - 
                     3*Power(t2,4)) + 
                  t1*(25 - 99*t2 + 109*Power(t2,2) + 5*Power(t2,3) - 
                     46*Power(t2,4) + 2*Power(t2,5)))) - 
            Power(s1,2)*(Power(t1,5)*(-4 + 7*t2 + Power(t2,2)) + 
               Power(s2,5)*(3 - 9*t2 + 4*Power(t2,2)) - 
               Power(t1,4)*(26 - 64*t2 + 49*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (107 - 277*t2 + 159*Power(t2,2) + 36*Power(t2,3)) + 
               Power(t1,2)*(209 - 793*t2 + 973*Power(t2,2) - 
                  394*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,3)*(38 - 101*t2 + 49*Power(t2,2) + 
                  26*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(256 - 1104*t2 + 1647*Power(t2,2) - 
                  971*Power(t2,3) + 162*Power(t2,4) + 10*Power(t2,5)) - 
               Power(s2,4)*(23 - 58*t2 + 44*Power(t2,2) + 
                  Power(t2,3) + t1*(16 - 43*t2 + 15*Power(t2,2))) + 
               Power(s2,3)*(-19 + 87*t2 - 88*Power(t2,2) + 
                  2*Power(t2,3) + 6*Power(t2,4) + 
                  Power(t1,2)*(34 - 82*t2 + 20*Power(t2,2)) + 
                  t1*(99 - 256*t2 + 205*Power(t2,2) - 4*Power(t2,3))) - 
               Power(s2,2)*(-188 + 768*t2 - 998*Power(t2,2) + 
                  434*Power(t2,3) - 9*Power(t2,4) + Power(t2,5) + 
                  2*Power(t1,3)*(18 - 39*t2 + 5*Power(t2,2)) + 
                  Power(t1,2)*
                   (155 - 402*t2 + 327*Power(t2,2) - 8*Power(t2,3)) + 
                  t1*(-69 + 237*t2 - 169*Power(t2,2) - 
                     48*Power(t2,3) + 11*Power(t2,4))) + 
               s2*(-258 + Power(t1,4)*(19 - 37*t2) + 1119*t2 - 
                  1688*Power(t2,2) + 1028*Power(t2,3) - 
                  203*Power(t2,4) + 2*Power(t2,5) + 
                  Power(t1,3)*(105 - 268*t2 + 215*Power(t2,2)) + 
                  Power(t1,2)*
                   (-88 + 251*t2 - 130*Power(t2,2) - 76*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  t1*(-392 + 1536*t2 - 1929*Power(t2,2) + 
                     800*Power(t2,3) - Power(t2,4) + 2*Power(t2,5)))) + 
            s1*(Power(s2,5)*t2*(3 - 4*t2 + Power(t2,2)) + 
               Power(t1,5)*(1 - 6*t2 + 5*Power(t2,2)) + 
               Power(t1,4)*(9 - 37*t2 + 51*Power(t2,2) - 
                  24*Power(t2,3)) + 
               2*Power(-1 + t2,2)*
                (-26 + 137*t2 - 204*Power(t2,2) + 94*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,3)*(13 - 30*t2 + 53*Power(t2,2) - 
                  55*Power(t2,3) + 21*Power(t2,4)) - 
               Power(t1,2)*(73 - 345*t2 + 563*Power(t2,2) - 
                  368*Power(t2,3) + 76*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(-122 + 722*t2 - 1546*Power(t2,2) + 
                  1481*Power(t2,3) - 593*Power(t2,4) + 58*Power(t2,5)) + 
               Power(s2,4)*(6 - 26*t2 + 36*Power(t2,2) - 
                  17*Power(t2,3) + 
                  t1*(1 - 18*t2 + 21*Power(t2,2) - 4*Power(t2,3))) + 
               Power(s2,3)*(-13 + 39*t2 - 57*Power(t2,2) + 
                  39*Power(t2,3) - 11*Power(t2,4) + Power(t2,5) + 
                  Power(t1,2)*
                   (-4 + 42*t2 - 44*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(-30 + 127*t2 - 177*Power(t2,2) + 87*Power(t2,3) - 
                     3*Power(t2,4))) + 
               Power(s2,2)*(-40 + 249*t2 - 477*Power(t2,2) + 
                  354*Power(t2,3) - 85*Power(t2,4) - 2*Power(t2,5) + 
                  Power(t1,3)*
                   (6 - 48*t2 + 46*Power(t2,2) - 4*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (17 - 71*t2 + 99*Power(t2,2) - 49*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  t1*(33 - 105*t2 + 195*Power(t2,2) - 175*Power(t2,3) + 
                     61*Power(t2,4) - 3*Power(t2,5))) + 
               s2*(97 - 639*t2 + 1452*Power(t2,2) - 1451*Power(t2,3) + 
                  610*Power(t2,4) - 71*Power(t2,5) + 2*Power(t2,6) + 
                  Power(t1,4)*
                   (-4 + 27*t2 - 24*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,3)*
                   (-36 + 149*t2 - 207*Power(t2,2) + 101*Power(t2,3) - 
                     3*Power(t2,4)) + 
                  Power(t1,2)*
                   (-33 + 96*t2 - 191*Power(t2,2) + 191*Power(t2,3) - 
                     71*Power(t2,4) + 2*Power(t2,5)) + 
                  t1*(120 - 611*t2 + 1042*Power(t2,2) - 
                     696*Power(t2,3) + 136*Power(t2,4) + 11*Power(t2,5)))\
)) + Power(s,2)*(-(Power(s1,7)*
               (Power(s2,2) + Power(t1,2) + t1*(-1 + t2) - 
                 4*Power(-1 + t2,2) - s2*(-1 + 2*t1 + t2))) + 
            Power(s1,6)*(-2*Power(s2,3) + 7*Power(t1,2)*(-3 + 5*t2) - 
               Power(-1 + t2,2)*(11 + 10*t2) + 
               Power(s2,2)*(-7 + 4*t1 + 21*t2) + 
               t1*(-34 + 83*t2 - 49*Power(t2,2)) + 
               s2*(34 - 2*Power(t1,2) + t1*(28 - 56*t2) - 83*t2 + 
                  49*Power(t2,2))) + 
            Power(s1,5)*(-Power(s2,4) + 3*Power(t1,4) + 
               Power(t1,3)*(26 - 42*t2) + Power(s2,3)*(-11 + 31*t2) + 
               Power(t1,2)*(-22 + 42*t2 - 41*Power(t2,2)) + 
               Power(-1 + t2,2)*(15 + 25*t2 + 12*Power(t2,2)) + 
               t1*(16 + 59*t2 - 215*Power(t2,2) + 140*Power(t2,3)) + 
               Power(s2,2)*(6*Power(t1,2) + t1*(60 - 116*t2) + 
                  7*(4 - 16*t2 + 9*Power(t2,2))) - 
               s2*(41 + 8*Power(t1,3) + Power(t1,2)*(75 - 127*t2) + 
                  6*t2 - 184*Power(t2,2) + 137*Power(t2,3) + 
                  t1*(6 - 70*t2 + 22*Power(t2,2)))) + 
            Power(s1,4)*(-2*Power(t1,5) + Power(t1,4)*(11 + t2) + 
               Power(s2,4)*(2 - 2*t1 + 6*t2) + 
               Power(t1,3)*(110 - 273*t2 + 129*Power(t2,2)) + 
               Power(t1,2)*(1 + 19*t2 + 17*Power(t2,2) - 
                  41*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (37 - 3*t2 + 5*Power(t2,2) + 8*Power(t2,3)) + 
               t1*(-159 + 359*t2 - 323*Power(t2,2) + 282*Power(t2,3) - 
                  159*Power(t2,4)) + 
               Power(s2,3)*(-33 + 8*Power(t1,2) + 79*t2 - 
                  12*Power(t2,2) - t1*(5 + 31*t2)) + 
               Power(s2,2)*(-120 - 12*Power(t1,3) + 114*t2 + 
                  250*Power(t2,2) - 248*Power(t2,3) + 
                  15*Power(t1,2)*(1 + 3*t2) + 
                  t1*(230 - 563*t2 + 231*Power(t2,2))) + 
               s2*(170 + 8*Power(t1,4) - 311*t2 + 197*Power(t2,2) - 
                  218*Power(t2,3) + 162*Power(t2,4) - 
                  Power(t1,3)*(23 + 21*t2) + 
                  Power(t1,2)*(-307 + 757*t2 - 348*Power(t2,2)) + 
                  t1*(97 - 79*t2 - 309*Power(t2,2) + 299*Power(t2,3)))) \
+ Power(s1,3)*(Power(s2,5)*(3 - 5*t2) + Power(t1,5)*(-5 + 7*t2) + 
               Power(t1,4)*(-32 + 69*t2 - 45*Power(t2,2)) + 
               Power(t1,3)*(236 - 716*t2 + 606*Power(t2,2) - 
                  124*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-264 + 712*t2 - 489*Power(t2,2) + 10*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(t1,2)*(402 - 1301*t2 + 1227*Power(t2,2) - 
                  410*Power(t2,3) + 103*Power(t2,4)) + 
               t1*(-203 + 959*t2 - 1447*Power(t2,2) + 
                  814*Power(t2,3) - 209*Power(t2,4) + 86*Power(t2,5)) + 
               Power(s2,4)*(-17 + 26*t2 - 13*Power(t2,2) + 
                  t1*(-17 + 27*t2)) + 
               Power(s2,3)*(-206 + Power(t1,2)*(38 - 58*t2) + 555*t2 - 
                  279*Power(t2,2) - 76*Power(t2,3) + 
                  t1*(137 - 267*t2 + 150*Power(t2,2))) + 
               Power(s2,2)*(211 - 534*t2 + 428*Power(t2,2) - 
                  369*Power(t2,3) + 285*Power(t2,4) + 
                  Power(t1,3)*(-42 + 62*t2) - 
                  3*Power(t1,2)*(85 - 175*t2 + 102*Power(t2,2)) - 
                  2*t1*(-324 + 976*t2 - 714*Power(t2,2) + 
                     55*Power(t2,3))) + 
               s2*(291 + Power(t1,4)*(23 - 33*t2) - 1258*t2 + 
                  1728*Power(t2,2) - 839*Power(t2,3) + 
                  172*Power(t2,4) - 94*Power(t2,5) + 
                  Power(t1,3)*(167 - 353*t2 + 214*Power(t2,2)) + 
                  Power(t1,2)*
                   (-678 + 2113*t2 - 1755*Power(t2,2) + 
                     310*Power(t2,3)) + 
                  t1*(-631 + 1937*t2 - 1829*Power(t2,2) + 
                     893*Power(t2,3) - 412*Power(t2,4)))) + 
            (-1 + t2)*(60 + 303*t2 - 1819*Power(t2,2) + 
               3109*Power(t2,3) - 2292*Power(t2,4) + 655*Power(t2,5) - 
               16*Power(t2,6) + 
               Power(t1,4)*(-31 + 31*t2 - 5*Power(t2,2) + 
                  5*Power(t2,3)) + 
               2*Power(s2,4)*
                (-4 + t2 - 2*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(200 - 464*t2 + 446*Power(t2,2) - 
                  192*Power(t2,3) + 7*Power(t2,4)) + 
               Power(t1,2)*(-107 + 314*t2 - 315*Power(t2,2) + 
                  17*Power(t2,3) + 91*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(-238 + 1570*t2 - 3566*Power(t2,2) + 
                  3555*Power(t2,3) - 1463*Power(t2,4) + 
                  157*Power(t2,5) - 18*Power(t2,6)) + 
               Power(s2,3)*(-41 + 114*t2 - 202*Power(t2,2) + 
                  142*Power(t2,3) - 10*Power(t2,4) + 
                  t1*(25 + 53*t2 - 73*Power(t2,2) - 5*Power(t2,3))) - 
               Power(s2,2)*(21 - 208*t2 + 488*Power(t2,2) - 
                  328*Power(t2,3) + 6*Power(t2,4) + 15*Power(t2,5) + 
                  3*Power(t1,2)*
                   (19 + 27*t2 - 51*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-222 + 572*t2 - 850*Power(t2,2) + 
                     596*Power(t2,3) - 87*Power(t2,4))) + 
               s2*(2 - 798*t2 + 2697*Power(t2,2) - 3219*Power(t2,3) + 
                  1477*Power(t2,4) - 161*Power(t2,5) + 5*Power(t2,6) + 
                  Power(t1,3)*
                   (71 - 5*t2 - 71*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,2)*
                   (-381 + 922*t2 - 1094*Power(t2,2) + 
                     646*Power(t2,3) - 84*Power(t2,4)) + 
                  t1*(206 - 774*t2 + 1061*Power(t2,2) - 
                     387*Power(t2,3) - 157*Power(t2,4) + 39*Power(t2,5))\
)) + Power(s1,2)*(-(Power(t1,5)*(-5 + t2)*t2) + 
               Power(s2,5)*t2*(-9 + 7*t2) + 
               Power(s2,4)*(-73 + (165 + 41*t1)*t2 - 
                  (112 + 29*t1)*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,4)*(-103 + 251*t2 - 216*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(t1,3)*(145 - 610*t2 + 776*Power(t2,2) - 
                  325*Power(t2,3) + 32*Power(t2,4)) + 
               Power(t1,2)*(599 - 2910*t2 + 4566*Power(t2,2) - 
                  2794*Power(t2,3) + 588*Power(t2,4) - 59*Power(t2,5)) \
- Power(-1 + t2,2)*(-154 - 110*t2 + 967*Power(t2,2) - 
                  802*Power(t2,3) + 25*Power(t2,4) + Power(t2,5)) - 
               t1*(-515 + 1978*t2 - 2381*Power(t2,2) + 
                  860*Power(t2,3) + 20*Power(t2,4) + 15*Power(t2,5) + 
                  23*Power(t2,6)) + 
               Power(s2,3)*(-125 + 640*t2 - 806*Power(t2,2) + 
                  175*Power(t2,3) + 100*Power(t2,4) + 
                  2*Power(t1,2)*t2*(-37 + 23*t2) + 
                  t1*(386 - 974*t2 + 816*Power(t2,2) - 176*Power(t2,3))\
) + Power(s2,2)*(624 - 2743*t2 + 2*Power(t1,3)*(33 - 17*t2)*t2 + 
                  3762*Power(t2,2) - 1850*Power(t2,3) + 
                  325*Power(t2,4) - 128*Power(t2,5) + 
                  4*Power(t1,2)*
                   (-164 + 426*t2 - 378*Power(t2,2) + 95*Power(t2,3)) \
+ t1*(308 - 1618*t2 + 2190*Power(t2,2) - 747*Power(t2,3) - 
                     83*Power(t2,4))) + 
               s2*(-596 + 2136*t2 - 2342*Power(t2,2) + 
                  632*Power(t2,3) + 133*Power(t2,4) + 13*Power(t2,5) + 
                  24*Power(t2,6) + Power(t1,4)*t2*(-29 + 11*t2) - 
                  2*Power(t1,3)*
                   (-223 + 573*t2 - 512*Power(t2,2) + 132*Power(t2,3)) \
+ Power(t1,2)*(-328 + 1588*t2 - 2160*Power(t2,2) + 897*Power(t2,3) - 
                     49*Power(t2,4)) + 
                  2*t1*(-587 + 2747*t2 - 4087*Power(t2,2) + 
                     2317*Power(t2,3) - 482*Power(t2,4) + 
                     102*Power(t2,5)))) + 
            s1*(6*Power(s2,5)*(-1 + t2) - 
               2*Power(t1,5)*(-1 - 3*t2 + 3*Power(t2,2) + Power(t2,3)) + 
               Power(t1,4)*(-39 + 148*t2 - 213*Power(t2,2) + 
                  117*Power(t2,3) - 12*Power(t2,4)) + 
               Power(t1,3)*(-181 + 375*t2 - 230*Power(t2,2) + 
                  30*Power(t2,3) + 6*Power(t2,4) - 2*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (349 - 1582*t2 + 1967*Power(t2,2) - 467*Power(t2,3) - 
                  317*Power(t2,4) + 14*Power(t2,5)) + 
               Power(t1,2)*(279 - 1594*t2 + 3321*Power(t2,2) - 
                  3143*Power(t2,3) + 1365*Power(t2,4) - 
                  240*Power(t2,5) + 13*Power(t2,6)) + 
               t1*(690 - 4066*t2 + 8643*Power(t2,2) - 
                  8264*Power(t2,3) + 3458*Power(t2,4) - 
                  538*Power(t2,5) + 74*Power(t2,6) + 3*Power(t2,7)) + 
               Power(s2,4)*(-33 + 108*t2 - 123*Power(t2,2) + 
                  39*Power(t2,3) + 10*Power(t2,4) - 
                  2*t1*(-13 + 9*t2 + 3*Power(t2,2) + Power(t2,3))) + 
               Power(s2,3)*(102 - 212*t2 + 98*Power(t2,2) + 
                  28*Power(t2,3) + 11*Power(t2,4) - 25*Power(t2,5) + 
                  4*Power(t1,2)*
                   (-11 + 3*t2 + 6*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(183 - 652*t2 + 852*Power(t2,2) - 
                     414*Power(t2,3) + 27*Power(t2,4))) + 
               Power(s2,2)*(216 - 1623*t2 + 3590*Power(t2,2) - 
                  3151*Power(t2,3) + 1004*Power(t2,4) - 
                  53*Power(t2,5) + 18*Power(t2,6) - 
                  12*Power(t1,3)*
                   (-3 - t2 + 3*Power(t2,2) + Power(t2,3)) - 
                  6*Power(t1,2)*
                   (51 - 188*t2 + 258*Power(t2,2) - 138*Power(t2,3) + 
                     16*Power(t2,4)) + 
                  t1*(-365 + 904*t2 - 846*Power(t2,2) + 
                     404*Power(t2,3) - 136*Power(t2,4) + 33*Power(t2,5))\
) + s2*(-626 + 4007*t2 - 8884*Power(t2,2) + 8672*Power(t2,3) - 
                  3600*Power(t2,4) + 473*Power(t2,5) - 39*Power(t2,6) - 
                  3*Power(t2,7) + 
                  2*Power(t1,4)*
                   (-7 - 9*t2 + 12*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,3)*
                   (195 - 732*t2 + 1032*Power(t2,2) - 570*Power(t2,3) + 
                     71*Power(t2,4)) + 
                  Power(t1,2)*
                   (444 - 1067*t2 + 978*Power(t2,2) - 462*Power(t2,3) + 
                     119*Power(t2,4) - 6*Power(t2,5)) + 
                  t1*(-520 + 3216*t2 - 6701*Power(t2,2) + 
                     5924*Power(t2,3) - 2134*Power(t2,4) + 
                     248*Power(t2,5) - 35*Power(t2,6))))) - 
         s*(-1 + t2)*(Power(s1,7)*
             (4*Power(s2,2) + 4*Power(t1,2) - 9*t1*(-1 + t2) + 
               Power(-1 + t2,2) + s2*(-9 - 8*t1 + 9*t2)) + 
            Power(s1,6)*(6*Power(s2,3) - 6*Power(t1,3) - 
               Power(-1 + t2,2)*(1 + 2*t2) + 3*Power(t1,2)*(-5 + 3*t2) + 
               Power(s2,2)*(-29 - 18*t1 + 23*t2) + 
               s2*(20 + 18*Power(t1,2) + t1*(44 - 32*t2) + t2 - 
                  21*Power(t2,2)) + t1*(-18 - 5*t2 + 23*Power(t2,2))) + 
            Power(s1,5)*(Power(t1,3)*(-22 + 8*t2) + 
               Power(s2,3)*(1 + 17*t2) + 
               Power(t1,2)*(17 - 12*t2 - 31*Power(t2,2)) + 
               Power(-1 + t2,2)*(-18 + 20*t2 + Power(t2,2)) - 
               t1*(1 - 49*t2 + 22*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,2)*(21 + 34*t2 - 81*Power(t2,2) - 
                  2*t1*(18 + 7*t2)) + 
               s2*(10 + Power(t1,2)*(57 - 11*t2) - 70*t2 + 
                  37*Power(t2,2) + 23*Power(t2,3) + 
                  2*t1*(-17 - 15*t2 + 58*Power(t2,2)))) + 
            Power(s1,4)*(-2*Power(s2,5) + 2*Power(t1,5) + 
               Power(t1,4)*(1 - 7*t2) + 
               2*Power(s2,4)*(-5 + 5*t1 + 6*t2) + 
               Power(t1,3)*(-112 + 155*t2 - 5*Power(t2,2)) - 
               Power(-1 + t2,2)*(-73 + 62*t2 + 32*Power(t2,2)) + 
               Power(t1,2)*(-127 + 113*t2 + 49*Power(t2,2) + 
                  33*Power(t2,3)) + 
               t1*(95 - 280*t2 + 203*Power(t2,2) - 35*Power(t2,3) + 
                  17*Power(t2,4)) - 
               Power(s2,3)*(20*Power(t1,2) + 17*t1*(-1 + t2) + 
                  5*(-23 + 21*t2 + 12*Power(t2,2))) + 
               Power(s2,2)*(20*Power(t1,3) - 3*Power(t1,2)*(1 + 3*t2) + 
                  t1*(-362 + 429*t2 + 71*Power(t2,2)) + 
                  2*(-32 + t2 + 11*Power(t2,2) + 54*Power(t2,3))) - 
               s2*(118 + 10*Power(t1,4) + Power(t1,3)*(5 - 21*t2) - 
                  302*t2 + 179*Power(t2,2) - 13*Power(t2,3) + 
                  18*Power(t2,4) + 
                  Power(t1,2)*(-359 + 479*t2 + 6*Power(t2,2)) + 
                  t1*(-207 + 163*t2 + 23*Power(t2,2) + 157*Power(t2,3)))) \
+ Power(s1,3)*(-(Power(t1,5)*(-3 + t2)) + Power(s2,5)*(-7 + 9*t2) + 
               Power(s2,4)*(29 + t1*(31 - 37*t2) - 30*t2 - 
                  13*Power(t2,2)) + 
               Power(t1,4)*(42 - 73*t2 + 17*Power(t2,2)) - 
               Power(t1,3)*(215 - 474*t2 + 213*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (45 - 293*t2 + 301*Power(t2,2) + 14*Power(t2,3)) + 
               Power(t1,2)*(-565 + 1583*t2 - 1143*Power(t2,2) + 
                  86*Power(t2,3) - 17*Power(t2,4)) - 
               t1*(260 - 572*t2 + 216*Power(t2,2) + 148*Power(t2,3) - 
                  59*Power(t2,4) + 7*Power(t2,5)) + 
               Power(s2,3)*(244 - 580*t2 + 229*Power(t2,2) + 
                  77*Power(t2,3) + Power(t1,2)*(-54 + 58*t2) + 
                  t1*(-161 + 239*t2 - 22*Power(t2,2))) - 
               Power(s2,2)*(563 - 1474*t2 + 966*Power(t2,2) - 
                  71*Power(t2,3) + 72*Power(t2,4) + 
                  Power(t1,3)*(-46 + 42*t2) + 
                  Power(t1,2)*(-277 + 461*t2 - 100*Power(t2,2)) + 
                  t1*(670 - 1603*t2 + 720*Power(t2,2) + 111*Power(t2,3))\
) + s2*(248 - 511*t2 + 167*Power(t2,2) + 113*Power(t2,3) - 
                  26*Power(t2,4) + 9*Power(t2,5) + 
                  Power(t1,4)*(-19 + 13*t2) + 
                  Power(t1,3)*(-187 + 325*t2 - 82*Power(t2,2)) + 
                  Power(t1,2)*
                   (641 - 1497*t2 + 704*Power(t2,2) + 38*Power(t2,3)) + 
                  t1*(1121 - 3067*t2 + 2169*Power(t2,2) - 
                     219*Power(t2,3) + 108*Power(t2,4)))) + 
            Power(s1,2)*(Power(s2,5)*(-4 + 17*t2 - 9*Power(t2,2)) + 
               Power(t1,5)*(4 - 9*t2 - 3*Power(t2,2)) + 
               Power(t1,4)*(77 - 185*t2 + 146*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-277 + 569*t2 - 67*Power(t2,2) - 303*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(t1,3)*(-119 + 389*t2 - 328*Power(t2,2) + 
                  22*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,2)*(-591 + 2458*t2 - 3330*Power(t2,2) + 
                  1608*Power(t2,3) - 132*Power(t2,4) + 5*Power(t2,5)) + 
               t1*(-686 + 2909*t2 - 4204*Power(t2,2) + 
                  2347*Power(t2,3) - 369*Power(t2,4) + Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s2,4)*(63 - 149*t2 + 102*Power(t2,2) + 
                  6*Power(t2,3) + t1*(20 - 77*t2 + 33*Power(t2,2))) - 
               Power(s2,3)*(-84 + 408*t2 - 477*Power(t2,2) + 
                  79*Power(t2,3) + 46*Power(t2,4) + 
                  2*Power(t1,2)*(20 - 69*t2 + 21*Power(t2,2)) + 
                  t1*(292 - 734*t2 + 578*Power(t2,2) - 40*Power(t2,3))) \
+ Power(s2,2)*(-585 + 2486*t2 - 3334*Power(t2,2) + 1518*Power(t2,3) - 
                  88*Power(t2,4) + 21*Power(t2,5) + 
                  2*Power(t1,3)*(20 - 61*t2 + 9*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (-236 + 603*t2 - 498*Power(t2,2) + 53*Power(t2,3)) + 
                  t1*(-243 + 1023*t2 - 1064*Power(t2,2) + 
                     114*Power(t2,3) + 82*Power(t2,4))) + 
               s2*(731 - 3060*t2 + 4394*Power(t2,2) - 2479*Power(t2,3) + 
                  442*Power(t2,4) - 26*Power(t2,5) - 2*Power(t2,6) + 
                  Power(t1,4)*(-20 + 53*t2 + 3*Power(t2,2)) + 
                  Power(t1,3)*
                   (-320 + 806*t2 - 666*Power(t2,2) + 68*Power(t2,3)) + 
                  Power(t1,2)*
                   (278 - 1004*t2 + 915*Power(t2,2) - 57*Power(t2,3) - 
                     40*Power(t2,4)) + 
                  t1*(1148 - 4830*t2 + 6506*Power(t2,2) - 
                     3048*Power(t2,3) + 222*Power(t2,4) - 34*Power(t2,5))\
)) - (-1 + t2)*(21 + 186*t2 - 959*Power(t2,2) + 1589*Power(t2,3) - 
               1159*Power(t2,4) + 328*Power(t2,5) - 6*Power(t2,6) + 
               Power(t1,4)*(-10 - 3*t2 + 12*Power(t2,2) + Power(t2,3)) + 
               Power(s2,4)*(-2 - 7*t2 + 4*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(75 - 180*t2 + 205*Power(t2,2) - 
                  104*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(-63 + 219*t2 - 233*Power(t2,2) + 
                  83*Power(t2,4)) + 
               t1*(-119 + 782*t2 - 1749*Power(t2,2) + 
                  1682*Power(t2,3) - 617*Power(t2,4) + 20*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,3)*(-17 + 60*t2 - 121*Power(t2,2) + 
                  80*Power(t2,3) + Power(t2,4) - 
                  4*t1*(-1 - 15*t2 + 15*Power(t2,2) + Power(t2,3))) + 
               Power(s2,2)*(-19 + 123*t2 - 209*Power(t2,2) + 
                  68*Power(t2,3) + 47*Power(t2,4) - 4*Power(t2,5) - 
                  6*Power(t1,2)*
                   (2 + 17*t2 - 20*Power(t2,2) + Power(t2,3)) + 
                  t1*(85 - 252*t2 + 447*Power(t2,2) - 312*Power(t2,3) + 
                     23*Power(t2,4))) + 
               s2*(15 - 432*t2 + 1331*Power(t2,2) - 1494*Power(t2,3) + 
                  613*Power(t2,4) - 30*Power(t2,5) + 
                  4*Power(t1,3)*
                   (5 + 13*t2 - 19*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-143 + 372*t2 - 531*Power(t2,2) + 336*Power(t2,3) - 
                     25*Power(t2,4)) + 
                  2*t1*(58 - 227*t2 + 281*Power(t2,2) - 48*Power(t2,3) - 
                     78*Power(t2,4) + 8*Power(t2,5)))) + 
            s1*(2*Power(s2,5)*(1 + 2*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               Power(t1,5)*(1 - 13*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               Power(t1,4)*(28 - 111*t2 + 156*Power(t2,2) - 
                  76*Power(t2,3) + Power(t2,4)) + 
               Power(t1,3)*(74 - 146*t2 + 141*Power(t2,2) - 
                  107*Power(t2,3) + 42*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (216 - 1084*t2 + 1537*Power(t2,2) - 619*Power(t2,3) - 
                  85*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(-471 + 2795*t2 - 5997*Power(t2,2) + 
                  5772*Power(t2,3) - 2364*Power(t2,4) + 
                  282*Power(t2,5) - 17*Power(t2,6)) + 
               Power(t1,2)*(-248 + 1239*t2 - 2210*Power(t2,2) + 
                  1695*Power(t2,3) - 523*Power(t2,4) + 47*Power(t2,5) - 
                  2*Power(t2,6)) - 
               Power(s2,4)*(-21 + 80*t2 - 103*Power(t2,2) + 
                  41*Power(t2,3) + 5*Power(t2,4) + 
                  t1*(7 + 29*t2 - 43*Power(t2,2) + 7*Power(t2,3))) + 
               Power(s2,3)*(-55 + 132*t2 - 123*Power(t2,2) + 
                  49*Power(t2,3) - 13*Power(t2,4) + 6*Power(t2,5) + 
                  Power(t1,2)*
                   (8 + 76*t2 - 92*Power(t2,2) + 8*Power(t2,3)) + 
                  t1*(-109 + 423*t2 - 573*Power(t2,2) + 
                     271*Power(t2,3) - 4*Power(t2,4))) - 
               Power(s2,2)*(152 - 1026*t2 + 2117*Power(t2,2) - 
                  1731*Power(t2,3) + 497*Power(t2,4) - 10*Power(t2,5) + 
                  3*Power(t2,6) + 
                  2*Power(t1,3)*
                   (1 + 47*t2 - 49*Power(t2,2) + Power(t2,3)) - 
                  3*Power(t1,2)*
                   (61 - 239*t2 + 331*Power(t2,2) - 165*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(-163 + 427*t2 - 565*Power(t2,2) + 
                     427*Power(t2,3) - 151*Power(t2,4) + 13*Power(t2,5))) \
+ s2*(394 - 2589*t2 + 5874*Power(t2,2) - 5874*Power(t2,3) + 
                  2497*Power(t2,4) - 318*Power(t2,5) + 16*Power(t2,6) - 
                  2*Power(t1,4)*
                   (1 - 28*t2 + 26*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,3)*
                   (-123 + 485*t2 - 679*Power(t2,2) + 341*Power(t2,3) - 
                     16*Power(t2,4)) + 
                  Power(t1,2)*
                   (-182 + 441*t2 - 583*Power(t2,2) + 485*Power(t2,3) - 
                     180*Power(t2,4) + 7*Power(t2,5)) + 
                  t1*(425 - 2311*t2 + 4282*Power(t2,2) - 
                     3266*Power(t2,3) + 895*Power(t2,4) - 
                     27*Power(t2,5) + 6*Power(t2,6))))))*
       R1q(1 - s + s1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*(1 - s + s1 - t2)*
       (1 - s + s*s1 - s1*s2 + s1*t1 - t2)*(-1 + t2)*Power(-1 + s + t2,3)*
       (-1 + s2 - t1 + t2)*(-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2) + 2*s2 - 2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 
         2*s*t1 - 2*s1*t1 - 2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(-6 - 19*s2 + 38*Power(s2,2) - 7*Power(s2,3) - 6*Power(s2,4) - 
         72*t1 + 9*s2*t1 + 36*Power(s2,2)*t1 + 15*Power(s2,3)*t1 - 
         44*Power(t1,2) - 53*s2*Power(t1,2) - 
         10*Power(s2,2)*Power(t1,2) + 24*Power(t1,3) - s2*Power(t1,3) + 
         2*Power(t1,4) + Power(s1,5)*(s2 - t1)*(4 + 3*s2 - 5*t1 - 2*t2) + 
         2*Power(s,5)*(-1 + s1)*(1 + t1 - 2*t2) + 96*t2 + 24*s2*t2 - 
         123*Power(s2,2)*t2 + 6*Power(s2,3)*t2 + 9*Power(s2,4)*t2 + 
         264*t1*t2 + 67*s2*t1*t2 - 62*Power(s2,2)*t1*t2 - 
         13*Power(s2,3)*t1*t2 + 46*Power(t1,2)*t2 + 
         104*s2*Power(t1,2)*t2 - 7*Power(s2,2)*Power(t1,2)*t2 - 
         48*Power(t1,3)*t2 + 17*s2*Power(t1,3)*t2 - 6*Power(t1,4)*t2 - 
         290*Power(t2,2) + 25*s2*Power(t2,2) + 
         129*Power(s2,2)*Power(t2,2) - 5*Power(s2,3)*Power(t2,2) - 
         7*Power(s2,4)*Power(t2,2) - 306*t1*Power(t2,2) - 
         164*s2*t1*Power(t2,2) + 56*Power(s2,2)*t1*Power(t2,2) + 
         14*Power(s2,3)*t1*Power(t2,2) + 40*Power(t1,2)*Power(t2,2) - 
         83*s2*Power(t1,2)*Power(t2,2) - 
         7*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         32*Power(t1,3)*Power(t2,2) + 300*Power(t2,3) - 
         26*s2*Power(t2,3) - 48*Power(s2,2)*Power(t2,3) - 
         2*Power(s2,3)*Power(t2,3) + 106*t1*Power(t2,3) + 
         100*s2*t1*Power(t2,3) - 6*Power(s2,2)*t1*Power(t2,3) - 
         46*Power(t1,2)*Power(t2,3) + 8*s2*Power(t1,2)*Power(t2,3) - 
         100*Power(t2,4) - 4*s2*Power(t2,4) + 8*t1*Power(t2,4) - 
         4*s2*t1*Power(t2,4) - 
         Power(s,4)*(Power(s1,2)*(4 + 7*t1 - 11*t2) + 
            s2*(-8 - 9*t1 + 7*t2) + 
            2*(5*Power(t1,2) + t1*(6 - 7*t2) + (3 - 2*t2)*t2) + 
            s1*(-6 - 23*t1 - 8*Power(t1,2) + s2*(10 + 11*t1 - 11*t2) + 
               11*t2 + 12*t1*t2 + 4*Power(t2,2))) + 
         Power(s1,4)*(9*Power(s2,3) - 6*Power(t1,3) + t1*(40 - 34*t2) - 
            3*Power(t1,2)*(1 + 4*t2) - Power(s2,2)*(26*t1 + 7*t2) - 
            2*(2 - 3*t2 + Power(t2,2)) + 
            s2*(-38 + 23*Power(t1,2) + 32*t2 + t1*(3 + 19*t2))) + 
         Power(s1,3)*(35 + 3*Power(s2,4) + Power(t1,4) + 
            Power(s2,3)*(10 - 6*t1 - 25*t2) - 64*t2 + 29*Power(t2,2) + 
            2*Power(t1,3)*(1 + 5*t2) + 
            Power(t1,2)*(-31 + 44*t2 + 4*Power(t2,2)) + 
            t1*(-53 - 23*t2 + 54*Power(t2,2)) + 
            Power(s2,2)*(-73 + 4*Power(t1,2) + 72*t2 - 10*Power(t2,2) + 
               3*t1*(-9 + 25*t2)) + 
            s2*(50 - 2*Power(t1,3) + Power(t1,2)*(15 - 60*t2) + 21*t2 - 
               49*Power(t2,2) + 2*t1*(55 - 63*t2 + 5*Power(t2,2)))) + 
         Power(s,3)*(Power(s1,3)*(-2 + s2 + 9*t1 - 8*t2) - 
            Power(s2,2)*(8 + 4*t1 + t2) + 
            s2*(-17 + 12*Power(t1,2) - t1*(-6 + t2) + 44*t2 - 
               Power(t2,2)) + 
            Power(s1,2)*(25 - 5*Power(s2,2) - 27*Power(t1,2) + 
               s2*(15 + 40*t1 - 22*t2) - 15*t2 + 8*Power(t2,2) + 
               t1*(-46 + 27*t2)) + 
            s1*(-39 + 8*Power(t1,3) + Power(t1,2)*(56 - 6*t2) + 
               Power(s2,2)*(21 + 13*t1 - 6*t2) + 32*t2 - 
               5*Power(t2,2) - 2*Power(t2,3) + 
               t1*(48 - 11*t2 - 12*Power(t2,2)) - 
               s2*(5 + 21*Power(t1,2) + t1*(78 - 5*t2) + 10*t2 - 
                  12*Power(t2,2))) + 
            2*(7 - 4*Power(t1,3) - 2*t2 - 4*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,2)*(1 + t2) + 
               t1*(1 - 24*t2 + 7*Power(t2,2)))) + 
         Power(s1,2)*(-50 - 3*Power(s2,5) + 11*Power(t1,4) + 
            Power(s2,4)*(18 + 18*t1 - 23*t2) + 36*t2 + 56*Power(t2,2) - 
            42*Power(t2,3) + Power(t1,2)*
             (-111 + 188*t2 - 26*Power(t2,2)) - 
            2*Power(t1,3)*(13 + 19*t2 + 2*Power(t2,2)) - 
            Power(s2,3)*(45 + 36*Power(t1,2) + t1*(77 - 81*t2) - 
               48*t2 + 10*Power(t2,2)) - 
            2*t1*(48 - 175*t2 + 100*Power(t2,2) + 10*Power(t2,3)) + 
            Power(s2,2)*(-50 + 30*Power(t1,3) + 
               Power(t1,2)*(111 - 93*t2) + 216*t2 - 77*Power(t2,2) + 
               2*Power(t2,3) + 4*t1*(17 - 35*t2 + 3*Power(t2,2))) + 
            s2*(131 - 9*Power(t1,4) - 410*t2 + 243*Power(t2,2) + 
               2*Power(t2,3) + 7*Power(t1,3)*(-9 + 5*t2) + 
               Power(t1,2)*(3 + 130*t2 + 2*Power(t2,2)) - 
               2*t1*(-80 + 205*t2 - 56*Power(t2,2) + 2*Power(t2,3)))) + 
         s1*(-67 + 6*Power(t1,5) + Power(t1,4)*(18 - 32*t2) - 
            3*Power(s2,5)*(-2 + t2) + 350*t2 - 449*Power(t2,2) + 
            150*Power(t2,3) + 16*Power(t2,4) + 
            Power(s2,4)*(10 - 21*t1 - 2*t2 + 9*t1*t2) + 
            Power(t1,3)*(-41 - 7*t2 + 46*Power(t2,2)) - 
            Power(t1,2)*(95 - 190*t2 + 81*Power(t2,2) + 4*Power(t2,3)) + 
            t1*(-109 + 375*t2 - 398*Power(t2,2) + 116*Power(t2,3)) + 
            Power(s2,3)*(-56 + Power(t1,2)*(21 - 9*t2) + 126*t2 - 
               59*Power(t2,2) + 2*Power(t2,3) + 
               t1*(-58 + 52*t2 - 8*Power(t2,2))) + 
            Power(s2,2)*(61 - 92*t2 + 29*Power(t2,2) - 6*Power(t2,3) + 
               3*Power(t1,3)*(1 + t2) + 
               2*Power(t1,2)*(52 - 65*t2 + 8*Power(t2,2)) + 
               t1*(88 - 294*t2 + 186*Power(t2,2) - 6*Power(t2,3))) + 
            s2*(46 - 15*Power(t1,4) - 311*t2 + 423*Power(t2,2) - 
               146*Power(t2,3) + 4*Power(t2,4) - 
               2*Power(t1,3)*(37 - 56*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(9 + 175*t2 - 173*Power(t2,2) + 
                  4*Power(t2,3)) + 
               2*t1*(15 - 40*t2 + 17*Power(t2,2) + 7*Power(t2,3)))) - 
         Power(s,2)*(Power(s2,3)*(4 + 3*t1 - 7*t2) + 
            Power(s1,4)*(-8 + 2*s2 + 5*t1 + t2) - 
            Power(s1,3)*(-64 + 11*Power(s2,2) + 35*Power(t1,2) + 
               t1*(23 - 14*t2) + 49*t2 - 4*Power(t2,2) + 
               s2*(10 - 53*t1 + 7*t2)) + 
            Power(s2,2)*(-48 - 2*Power(t1,2) + 37*t2 + 17*Power(t2,2) + 
               t1*(-34 + 8*t2)) + 
            s2*(49 - 5*Power(t1,3) + 83*t2 - 75*Power(t2,2) + 
               10*Power(t2,3) + Power(t1,2)*(58 + 7*t2) + 
               t1*(32 - 89*t2 - 30*Power(t2,2))) + 
            2*(-3 + 2*Power(t1,4) - 45*t2 + 31*Power(t2,2) + 
               4*Power(t2,3) - 2*Power(t1,3)*(7 + 2*t2) + 
               Power(t1,2)*(5 + 27*t2 + 7*Power(t2,2)) + 
               t1*(4 - 34*t2 + 20*Power(t2,2) - 9*Power(t2,3))) + 
            Power(s1,2)*(-112 - 7*Power(s2,3) + 21*Power(t1,3) + 
               45*t2 + 26*Power(t2,2) - 2*Power(t2,3) + 
               Power(t1,2)*(89 + t2) + 
               Power(s2,2)*(22 + 45*t1 + 12*t2) + 
               t1*(-13 + 68*t2 - 20*Power(t2,2)) - 
               s2*(-108 + 59*Power(t1,2) + 109*t2 - 26*Power(t2,2) + 
                  3*t1*(38 + 9*t2))) + 
            s1*(57 - 2*Power(t1,4) + 127*t2 - 141*Power(t2,2) + 
               14*Power(t2,3) - Power(t1,3)*(15 + 2*t2) + 
               Power(s2,3)*(6 + t1 + 8*t2) + 
               Power(t1,2)*(-44 - 53*t2 + 6*Power(t2,2)) + 
               t1*(38 - 19*t2 - 14*Power(t2,2) + 6*Power(t2,3)) - 
               Power(s2,2)*(-60 + 4*Power(t1,2) + 67*t2 - 
                  12*Power(t2,2) + t1*(12 + 35*t2)) + 
               s2*(-170 + 5*Power(t1,3) + 50*t2 + 45*Power(t2,2) - 
                  6*Power(t2,3) + Power(t1,2)*(21 + 29*t2) + 
                  t1*(-23 + 138*t2 - 16*Power(t2,2))))) + 
         s*(-3*Power(s2,4)*(-2 + t2) + 
            Power(s1,5)*(-4 + s2 + t1 + 2*t2) + 
            Power(s2,3)*(11 - 2*t2 + 21*Power(t2,2) - t1*(12 + 5*t2)) + 
            Power(s1,4)*(35 - 9*Power(s2,2) - 21*Power(t1,2) - 
               3*t1*(-2 + t2) - 27*t2 + s2*(-19 + 32*t1 + 6*t2)) + 
            Power(s2,2)*(-82 + 151*t2 - 38*Power(t2,2) + 8*Power(t2,3) + 
               Power(t1,2)*(8 + 19*t2) + 
               t1*(-66 + 36*t2 - 58*Power(t2,2))) + 
            s2*(77 - 26*t2 - 135*Power(t2,2) + 80*Power(t2,3) - 
               Power(t1,3)*(4 + 11*t2) + 
               Power(t1,2)*(99 - 54*t2 + 37*Power(t2,2)) + 
               t1*(16 - 149*t2 + 72*Power(t2,2) - 12*Power(t2,3))) + 
            2*(-6 + Power(t1,4) - 74*t2 + 170*Power(t2,2) - 
               88*Power(t2,3) + 6*Power(t2,4) + 
               2*Power(t1,3)*(-11 + 5*t2) - 
               Power(t1,2)*(-29 - 5*t2 + 15*Power(t2,2) + Power(t2,3)) + 
               t1*(46 - 133*t2 + 104*Power(t2,2) - 26*Power(t2,3) + 
                  2*Power(t2,4))) + 
            Power(s1,2)*(-89 + Power(s2,4) - 3*Power(t1,4) + 389*t2 - 
               240*Power(t2,2) - 4*Power(t2,3) - 
               Power(t1,3)*(4 + 11*t2) + 
               Power(s2,3)*(-19 - 6*t1 + 46*t2) + 
               Power(t1,2)*(2 - 139*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(134 + 52*t1 + 6*Power(t1,2) - 168*t2 - 
                  135*t1*t2 + 28*Power(t2,2)) + 
               t1*(4 + 101*t2 - 52*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-88 + 2*Power(t1,3) - 120*t2 + 87*Power(t2,2) - 
                  4*Power(t2,3) + Power(t1,2)*(-29 + 100*t2) + 
                  t1*(-146 + 331*t2 - 32*Power(t2,2)))) + 
            Power(s1,3)*(-63 - 15*Power(s2,3) + 19*Power(t1,3) + 2*t2 + 
               33*Power(t2,2) + 4*Power(t1,2)*(11 + 5*t2) + 
               Power(s2,2)*(1 + 56*t1 + 26*t2) + 
               t1*(-85 + 97*t2 - 8*Power(t2,2)) - 
               s2*(60*Power(t1,2) + t1*(46 + 53*t2) - 
                  2*(68 - 59*t2 + 7*Power(t2,2)))) + 
            s1*(123 + 6*Power(t1,4) - 224*t2 - 83*Power(t2,2) + 
               138*Power(t2,3) - 4*Power(t2,4) + 
               Power(s2,4)*(-13 - 3*t1 + 10*t2) + 
               t1*t2*(-95 + 70*t2 - 26*Power(t2,2)) + 
               Power(t1,3)*(26 - 35*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(57 + 9*Power(t1,2) + t1*(56 - 37*t2) - 
                  44*t2 + 4*Power(t2,2)) - 
               Power(t1,2)*(47 + 42*t2 - 113*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(s2,2)*(58 + 9*Power(t1,3) + 
                  Power(t1,2)*(67 - 44*t2) + 132*t2 - 109*Power(t2,2) + 
                  6*Power(t2,3) + t1*(103 - 69*t2 - 4*Power(t2,2))) + 
               s2*(-111 + 3*Power(t1,4) + Power(t1,3)*(18 - 17*t2) + 
                  426*t2 - 226*Power(t2,2) + 20*Power(t2,3) - 
                  10*Power(t1,2)*(-2 - t2 + Power(t2,2)) + 
                  t1*(106 + 173*t2 - 232*Power(t2,2) + 12*Power(t2,3))))))*
       R2q(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       (-1 + t2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(39 - 41*s2 - 9*Power(s2,2) + 3*Power(s2,3) + 6*Power(s2,4) + 
         2*Power(s2,5) + 2*Power(s1,8)*Power(s2 - t1,2) + 20*t1 + 
         44*s2*t1 - 31*Power(s2,2)*t1 - 8*Power(s2,3)*t1 - 
         7*Power(s2,4)*t1 - 2*Power(s2,5)*t1 - 41*Power(t1,2) + 
         72*s2*Power(t1,2) - 17*Power(s2,2)*Power(t1,2) + 
         Power(s2,3)*Power(t1,2) + 9*Power(s2,4)*Power(t1,2) - 
         44*Power(t1,3) + 34*s2*Power(t1,3) + 
         19*Power(s2,2)*Power(t1,3) - 16*Power(s2,3)*Power(t1,3) - 
         15*Power(t1,4) - 23*s2*Power(t1,4) + 
         14*Power(s2,2)*Power(t1,4) + 8*Power(t1,5) - 6*s2*Power(t1,5) + 
         Power(t1,6) + 2*Power(s,7)*(-1 + s1)*(t1 - t2) - 235*t2 + 
         170*s2*t2 + 118*Power(s2,2)*t2 - 28*Power(s2,3)*t2 - 
         41*Power(s2,4)*t2 - 2*Power(s2,5)*t2 + 2*Power(s2,6)*t2 - 
         23*t1*t2 - 429*s2*t1*t2 + 182*Power(s2,2)*t1*t2 + 
         138*Power(s2,3)*t1*t2 + 9*Power(s2,4)*t1*t2 - 
         5*Power(s2,5)*t1*t2 + 337*Power(t1,2)*t2 - 
         330*s2*Power(t1,2)*t2 - 172*Power(s2,2)*Power(t1,2)*t2 - 
         6*Power(s2,3)*Power(t1,2)*t2 - 3*Power(s2,4)*Power(t1,2)*t2 + 
         176*Power(t1,3)*t2 + 94*s2*Power(t1,3)*t2 - 
         16*Power(s2,2)*Power(t1,3)*t2 + 22*Power(s2,3)*Power(t1,3)*t2 - 
         19*Power(t1,4)*t2 + 24*s2*Power(t1,4)*t2 - 
         28*Power(s2,2)*Power(t1,4)*t2 - 9*Power(t1,5)*t2 + 
         15*s2*Power(t1,5)*t2 - 3*Power(t1,6)*t2 + 503*Power(t2,2) - 
         143*s2*Power(t2,2) - 299*Power(s2,2)*Power(t2,2) - 
         11*Power(s2,3)*Power(t2,2) + 60*Power(s2,4)*Power(t2,2) - 
         2*Power(s2,5)*Power(t2,2) - 4*Power(s2,6)*Power(t2,2) - 
         246*t1*Power(t2,2) + 1020*s2*t1*Power(t2,2) - 
         96*Power(s2,2)*t1*Power(t2,2) - 215*Power(s2,3)*t1*Power(t2,2) + 
         18*Power(s2,4)*t1*Power(t2,2) + 19*Power(s2,5)*t1*Power(t2,2) - 
         757*Power(t1,2)*Power(t2,2) + 254*s2*Power(t1,2)*Power(t2,2) + 
         304*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         51*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         36*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         147*Power(t1,3)*Power(t2,2) - 203*s2*Power(t1,3)*Power(t2,2) + 
         65*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         34*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         54*Power(t1,4)*Power(t2,2) - 39*s2*Power(t1,4)*Power(t2,2) - 
         16*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         9*Power(t1,5)*Power(t2,2) + 3*s2*Power(t1,5)*Power(t2,2) - 
         421*Power(t2,3) - 146*s2*Power(t2,3) + 
         236*Power(s2,2)*Power(t2,3) + 68*Power(s2,3)*Power(t2,3) - 
         35*Power(s2,4)*Power(t2,3) - 6*Power(s2,5)*Power(t2,3) + 
         613*t1*Power(t2,3) - 839*s2*t1*Power(t2,3) - 
         133*Power(s2,2)*t1*Power(t2,3) + 
         131*Power(s2,3)*t1*Power(t2,3) + 20*Power(s2,4)*t1*Power(t2,3) + 
         619*Power(t1,2)*Power(t2,3) + 76*s2*Power(t1,2)*Power(t2,3) - 
         189*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         24*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         11*Power(t1,3)*Power(t2,3) + 125*s2*Power(t1,3)*Power(t2,3) + 
         12*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         32*Power(t1,4)*Power(t2,3) - 2*s2*Power(t1,4)*Power(t2,3) + 
         54*Power(t2,4) + 232*s2*Power(t2,4) - 
         36*Power(s2,2)*Power(t2,4) - 40*Power(s2,3)*Power(t2,4) - 
         2*Power(s2,4)*Power(t2,4) - 482*t1*Power(t2,4) + 
         188*s2*t1*Power(t2,4) + 104*Power(s2,2)*t1*Power(t2,4) + 
         2*Power(s2,3)*t1*Power(t2,4) - 150*Power(t1,2)*Power(t2,4) - 
         98*s2*Power(t1,2)*Power(t2,4) + 
         2*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         34*Power(t1,3)*Power(t2,4) - 2*s2*Power(t1,3)*Power(t2,4) + 
         84*Power(t2,5) - 72*s2*Power(t2,5) - 
         12*Power(s2,2)*Power(t2,5) + 118*t1*Power(t2,5) + 
         20*s2*t1*Power(t2,5) - 2*Power(s2,2)*t1*Power(t2,5) - 
         10*Power(t1,2)*Power(t2,5) + 2*s2*Power(t1,2)*Power(t2,5) - 
         24*Power(t2,6) + Power(s1,7)*(s2 - t1)*
          (-4 + 7*Power(s2,2) + 10*t1 + 5*Power(t1,2) + 4*t2 + 4*t1*t2 - 
            2*s2*(5 + 6*t1 + 2*t2)) + 
         Power(s,6)*(2 + 5*t1 - 5*Power(t1,2) + 
            2*s2*(-2 + 3*t1 - 5*t2) + t2 + 3*t1*t2 + 2*Power(t2,2) - 
            Power(s1,2)*(-Power(t1,2) - 12*t2 + t1*(8 + t2) + 
               s2*(2 + t1 + t2)) + 
            s1*(-2 + 2*Power(t1,2) - 15*t2 - 4*Power(t2,2) + 
               t1*(5 + 2*t2) + s2*(6 - 7*t1 + 13*t2))) + 
         Power(s1,6)*(10*Power(s2,4) + 3*Power(t1,4) + 
            2*Power(-1 + t2,2) + 2*Power(t1,3)*(-1 + 6*t2) - 
            Power(s2,3)*(11 + 33*t1 + 15*t2) + 
            Power(t1,2)*(-16 + 51*t2 + Power(t2,2)) + 
            4*t1*(-5 + 3*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(-19 + 39*Power(t1,2) + 55*t2 + 
               2*t1*(9 + 22*t2)) - 
            s2*(19*Power(t1,3) + Power(t1,2)*(5 + 41*t2) - 
               t1*(37 - 110*t2 + Power(t2,2)) + 
               4*(-5 + 3*t2 + 2*Power(t2,2)))) + 
         Power(s1,5)*(4*Power(s2,5) + Power(t1,5) + 
            Power(s2,4)*(15 - 11*t1 - 30*t2) - 
            12*Power(t1,4)*(-1 + t2) - 2*Power(-1 + t2,2)*(5 + 2*t2) - 
            3*Power(t1,3)*(-21 + 11*t2 + Power(t2,2)) - 
            6*Power(t1,2)*(-5 + 3*t2 + 11*Power(t2,2)) - 
            2*t1*(9 - 54*t2 + 44*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(-94 + 8*Power(t1,2) + 113*t2 - 4*Power(t2,2) + 
               t1*(-76 + 113*t2)) + 
            Power(s2,2)*(62 + 2*Power(t1,3) + 
               Power(t1,2)*(119 - 148*t2) - 54*t2 - 62*Power(t2,2) + 
               t1*(267 - 281*t2 + 11*Power(t2,2))) - 
            s2*(-17 + 4*Power(t1,4) + Power(t1,3)*(70 - 77*t2) + 
               109*t2 - 93*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(236 - 201*t2 + 4*Power(t2,2)) + 
               t1*(97 - 81*t2 - 125*Power(t2,2) + Power(t2,3)))) + 
         Power(s1,4)*(-4*Power(s2,6) - Power(t1,6) + 
            Power(s2,5)*(25 + 29*t1 - 32*t2) + 
            Power(t1,5)*(-19 + 4*t2) + 
            Power(-1 + t2,2)*(-5 + 44*t2 + Power(t2,2)) + 
            Power(t1,4)*(18 + 7*t2 + 3*Power(t2,2)) - 
            Power(s2,4)*(69 + 77*Power(t1,2) + t1*(136 - 145*t2) - 
               69*t2 + 8*Power(t2,2)) + 
            Power(t1,3)*(82 - 213*t2 + 65*Power(t2,2)) + 
            2*Power(t1,2)*(86 - 151*t2 + 95*Power(t2,2) + 
               6*Power(t2,3)) + 
            t1*(79 - 175*t2 - Power(t2,2) + 97*Power(t2,3)) + 
            Power(s2,3)*(98*Power(t1,3) + Power(t1,2)*(277 - 247*t2) + 
               t1*(193 - 196*t2 + 23*Power(t2,2)) - 
               2*(13 - 78*t2 + 72*Power(t2,2))) - 
            Power(s2,2)*(-194 + 62*Power(t1,4) + 
               Power(t1,3)*(265 - 191*t2) + 437*t2 - 324*Power(t2,2) + 
               9*Power(t2,3) + 
               Power(t1,2)*(161 - 192*t2 + 19*Power(t2,2)) + 
               t1*(-157 + 578*t2 - 386*Power(t2,2) + 3*Power(t2,3))) + 
            s2*(-91 + 17*Power(t1,5) + Power(t1,4)*(118 - 61*t2) + 
               214*t2 - 40*Power(t2,2) - 84*Power(t2,3) + Power(t2,4) + 
               Power(t1,3)*(19 - 72*t2 + Power(t2,2)) + 
               Power(t1,2)*(-213 + 635*t2 - 307*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-381 + 784*t2 - 559*Power(t2,2) + 12*Power(t2,3)))) - 
         Power(s1,3)*(3*Power(s2,7) - 6*Power(t1,6) + 
            Power(s2,6)*(-7 - 22*t1 + 14*t2) + 
            Power(t1,5)*(99 - 33*t2 + Power(t2,2)) + 
            Power(s2,5)*(-22 + 65*Power(t1,2) + t1*(42 - 71*t2) + 
               9*t2 + 4*Power(t2,2)) + 
            Power(-1 + t2,2)*(-40 + 65*t2 + 43*Power(t2,2)) + 
            Power(t1,4)*(90 - 280*t2 + 66*Power(t2,2)) + 
            Power(t1,3)*(-120 + 58*t2 + 85*Power(t2,2) - 
               6*Power(t2,3)) + 
            Power(t1,2)*(-176 + 645*t2 - 556*Power(t2,2) + 
               137*Power(t2,3)) + 
            3*t1*(-49 + 151*t2 - 206*Power(t2,2) + 99*Power(t2,3) + 
               5*Power(t2,4)) + 
            Power(s2,4)*(89 - 100*Power(t1,3) - 293*t2 + 
               140*Power(t2,2) + 8*Power(t1,2)*(-13 + 18*t2) + 
               t1*(226 - 110*t2 - 9*Power(t2,2))) + 
            Power(s2,2)*(-21 - 38*Power(t1,5) + 299*t2 - 
               452*Power(t2,2) + 227*Power(t2,3) - 3*Power(t2,4) + 
               Power(t1,4)*(-99 + 74*t2) + 
               Power(t1,3)*(799 - 357*t2 + 8*Power(t2,2)) + 
               t1*(185 - 582*t2 + 652*Power(t2,2) - 32*Power(t2,3)) + 
               Power(t1,2)*(651 - 1917*t2 + 660*Power(t2,2) - 
                  6*Power(t2,3))) + 
            Power(s2,3)*(-133 + 85*Power(t1,4) + 293*t2 - 
               274*Power(t2,2) + 11*Power(t2,3) - 
               2*Power(t1,3)*(-68 + 73*t2) + 
               Power(t1,2)*(-645 + 309*t2 + 2*Power(t2,2)) + 
               t1*(-414 + 1258*t2 - 507*Power(t2,2) + 3*Power(t2,3))) + 
            s2*(146 + 7*Power(t1,6) + Power(t1,5)*(38 - 15*t2) - 
               481*t2 + 700*Power(t2,2) - 373*Power(t2,3) + 
               8*Power(t2,4) + 
               Power(t1,4)*(-457 + 182*t2 - 6*Power(t2,2)) + 
               Power(t1,3)*(-416 + 1232*t2 - 359*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(68 + 231*t2 - 463*Power(t2,2) + 
                  27*Power(t2,3)) + 
               t1*(182 - 917*t2 + 1005*Power(t2,2) - 379*Power(t2,3) + 
                  9*Power(t2,4)))) - 
         s1*(-3*Power(t1,7) + 2*Power(s2,7)*t2 + 
            2*Power(t1,6)*(-2 + 7*t2) + 
            Power(t1,5)*(-1 + 61*t2 - 62*Power(t2,2)) + 
            Power(-1 + t2,2)*
             (-14 + 145*t2 - 67*Power(t2,2) + 46*Power(t2,3)) + 
            Power(t1,4)*(106 - 243*t2 + 32*Power(t2,2) + 
               69*Power(t2,3)) + 
            Power(t1,3)*(187 - 836*t2 + 1074*Power(t2,2) - 
               353*Power(t2,3) - 19*Power(t2,4)) + 
            Power(t1,2)*(-32 - 280*t2 + 1053*Power(t2,2) - 
               1060*Power(t2,3) + 297*Power(t2,4)) + 
            t1*(-127 + 511*t2 - 594*Power(t2,2) + 61*Power(t2,3) + 
               209*Power(t2,4) - 60*Power(t2,5)) + 
            Power(s2,6)*(4 - 3*t2 + 8*Power(t2,2) - t1*(2 + 13*t2)) + 
            Power(s2,4)*(-1 + 42*t2 - 160*Power(t2,2) + 
               87*Power(t2,3) + Power(t2,4) - 
               5*Power(t1,3)*(7 + 10*t2) + 
               t1*(-69 + 270*t2 - 223*Power(t2,2)) + 
               4*Power(t1,2)*(4 + 19*t2 + 12*Power(t2,2))) + 
            Power(s2,5)*(15 - 51*t2 + 39*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(13 + 35*t2) - 
               t1*(15 + 8*t2 + 32*Power(t2,2))) + 
            Power(s2,3)*(-32 + 253*t2 - 358*Power(t2,2) + 
               60*Power(t2,3) + 26*Power(t2,4) + 
               10*Power(t1,4)*(5 + 4*t2) - 
               2*Power(t1,3)*(-3 + 82*t2 + 16*Power(t2,2)) + 
               Power(t1,2)*(118 - 565*t2 + 497*Power(t2,2) - 
                  12*Power(t2,3)) + 
               t1*(-104 + 154*t2 + 384*Power(t2,2) - 306*Power(t2,3) + 
                  Power(t2,4))) + 
            s2*(71 + Power(t1,5)*(17 - 76*t2) - 356*t2 + 
               413*Power(t2,2) + 52*Power(t2,3) - 232*Power(t2,4) + 
               52*Power(t2,5) + Power(t1,6)*(17 + 3*t2) + 
               Power(t1,4)*(27 - 300*t2 + 292*Power(t2,2) - 
                  6*Power(t2,3)) + 
               t1*(85 + 336*t2 - 1622*Power(t2,2) + 1784*Power(t2,3) - 
                  539*Power(t2,4)) + 
               Power(t1,3)*(-318 + 724*t2 - 270*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(t1,2)*(-403 + 1957*t2 - 2601*Power(t2,2) + 
                  834*Power(t2,3) + 56*Power(t2,4))) - 
            Power(s2,2)*(43 + 76*t2 - 569*Power(t2,2) + 
               698*Power(t2,3) - 220*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,5)*(40 + 17*t2) + 
               Power(t1,4)*(24 - 161*t2 - 8*Power(t2,2)) + 
               Power(t1,3)*(90 - 585*t2 + 543*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-317 + 677*t2 + 256*Power(t2,2) - 
                  420*Power(t2,3) + 5*Power(t2,4)) + 
               t1*(-248 + 1374*t2 - 1885*Power(t2,2) + 541*Power(t2,3) + 
                  63*Power(t2,4)))) + 
         Power(s1,2)*(5*Power(t1,7) + Power(t1,6)*(9 - 30*t2) - 
            Power(s2,7)*(-2 + t2) + 
            Power(t1,5)*(-73 + 37*t2 + 35*Power(t2,2)) + 
            Power(-1 + t2,2)*
             (36 - 85*t2 + 149*Power(t2,2) + 6*Power(t2,3)) - 
            Power(t1,4)*(228 - 548*t2 + 235*Power(t2,2) + 
               8*Power(t2,3)) + 
            Power(t1,3)*(-37 + 603*t2 - 839*Power(t2,2) + 
               241*Power(t2,3)) + 
            Power(t1,2)*(179 - 389*t2 + 41*Power(t2,2) + 
               202*Power(t2,3) - 45*Power(t2,4)) + 
            t1*(101 - 616*t2 + 899*Power(t2,2) - 514*Power(t2,3) + 
               130*Power(t2,4)) + 
            Power(s2,6)*(12 - 10*t2 + t1*(-8 + 3*t2)) + 
            Power(s2,5)*(-11 + 5*Power(t1,2) + 84*t2 - 58*Power(t2,2) + 
               t1*(-83 + 90*t2 - 4*Power(t2,2))) - 
            Power(s2,4)*(41 - 144*t2 + 32*Power(t2,2) + 
               13*Power(t2,3) + 5*Power(t1,3)*(-5 + 2*t2) + 
               Power(t1,2)*(-221 + 290*t2 - 16*Power(t2,2)) + 
               t1*(31 + 281*t2 - 256*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(62 + 15*Power(t1,4)*(-4 + t2) - 507*t2 + 
               698*Power(t2,2) - 250*Power(t2,3) - 3*Power(t2,4) + 
               Power(t1,3)*(-294 + 460*t2 - 24*Power(t2,2)) + 
               Power(t1,2)*(232 + 302*t2 - 455*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(401 - 1101*t2 + 417*Power(t2,2) + 40*Power(t2,3))) - 
            Power(s2,2)*(36 - 103*t2 + 398*Power(t2,2) - 
               338*Power(t2,3) + 19*Power(t2,4) + 
               Power(t1,5)*(-58 + 9*t2) - 
               2*Power(t1,4)*(103 - 195*t2 + 8*Power(t2,2)) + 
               t1*(213 - 1780*t2 + 2373*Power(t2,2) - 
                  774*Power(t2,3)) + 
               Power(t1,3)*(400 + 60*t2 - 409*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(907 - 2318*t2 + 973*Power(t2,2) + 
                  49*Power(t2,3))) + 
            s2*(-24 + 346*t2 - 597*Power(t2,2) + 418*Power(t2,3) - 
               149*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,6)*(-27 + 2*t2) + 
               Power(t1,5)*(-71 + 170*t2 - 4*Power(t2,2)) + 
               Power(t1,4)*(283 - 82*t2 - 187*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,3)*(775 - 1909*t2 + 823*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(t1,2)*(188 - 1876*t2 + 2514*Power(t2,2) - 
                  765*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(-124 + 209*t2 + 468*Power(t2,2) - 607*Power(t2,3) + 
                  78*Power(t2,4)))) + 
         Power(s,5)*(-1 - 6*s2 + 12*Power(s2,2) - 15*t1 - 19*s2*t1 - 
            8*Power(s2,2)*t1 + 10*Power(t1,2) + 14*s2*Power(t1,2) - 
            6*Power(t1,3) + 4*t2 + 6*s2*t2 + 18*Power(s2,2)*t2 + 
            2*t1*t2 - 21*s2*t1*t2 + 7*Power(t1,2)*t2 + 16*Power(t2,2) - 
            12*s2*Power(t2,2) - t1*Power(t2,2) + 
            Power(s1,3)*(2 - Power(s2,2) - 6*Power(t1,2) - 31*t2 + 
               Power(t2,2) + t1*(7 + 4*t2) + s2*(10 + 7*t1 + 7*t2)) + 
            s1*(-7 - 7*Power(t1,3) + 
               Power(s2,2)*(-26 + 14*t1 - 33*t2) - 16*t2 - 
               39*Power(t2,2) + 14*Power(t1,2)*(1 + t2) - 
               t1*(7 + 24*t2 + 7*Power(t2,2)) + 
               s2*(34 - 7*Power(t1,2) + 65*t2 + 27*Power(t2,2) + 
                  5*t1*(-1 + 2*t2))) + 
            Power(s1,2)*(4 + 3*Power(t1,3) + 46*t2 + 21*Power(t2,2) - 
               Power(t1,2)*(7 + t2) + Power(s2,2)*(13 + t1 + 6*t2) - 
               2*t1*(-5 - 4*t2 + Power(t2,2)) - 
               s2*(34 + 4*Power(t1,2) + 72*t2 + t1*(-15 + 7*t2)))) + 
         Power(s,4)*(-29 - 30*t1 - 49*Power(t1,2) + 12*Power(t1,3) - 
            2*Power(t1,4) + 4*Power(s2,3)*(-2 + t1 - 3*t2) + 65*t2 - 
            36*t1*t2 - 42*Power(t1,2)*t2 + 4*Power(t1,3)*t2 + 
            67*t1*Power(t2,2) + 6*Power(t1,2)*Power(t2,2) + 
            13*Power(t2,3) - 6*t1*Power(t2,3) - 2*Power(t2,4) + 
            Power(s1,4)*(-6 + 5*Power(s2,2) + 16*Power(t1,2) - 
               7*t1*(-2 + t2) + 41*t2 - 3*Power(t2,2) - 
               3*s2*(7 + 7*t1 + 6*t2)) + 
            s2*(45 + 8*Power(t1,3) - 54*t2 - 42*Power(t2,2) - 
               6*Power(t2,3) - Power(t1,2)*(29 + 14*t2) + 
               t1*(73 + 82*t2 - 23*Power(t2,2))) + 
            Power(s2,2)*(-10*Power(t1,2) + t1*(25 + 22*t2) + 
               4*(-7 - 7*t2 + 6*Power(t2,2))) + 
            Power(s1,3)*(17 + Power(s2,3) - 14*Power(t1,3) - 93*t2 - 
               31*Power(t2,2) - Power(t1,2)*(1 + 4*t2) - 
               Power(s2,2)*(37 + 12*t1 + 42*t2) + 
               t1*(-31 - 85*t2 + 13*Power(t2,2)) + 
               s2*(39 + 25*Power(t1,2) + 206*t2 - 8*Power(t2,2) + 
                  t1*(7 + 50*t2))) - 
            s1*(-79 + 20*Power(t1,4) + 131*t2 + 55*Power(t2,2) + 
               29*Power(t2,3) + Power(t2,4) - 
               Power(t1,3)*(58 + 33*t2) + 
               Power(t1,2)*(-75 + 56*t2 + 27*Power(t2,2)) + 
               t1*(-114 + 65*t2 + 68*Power(t2,2) - 15*Power(t2,3)) + 
               2*Power(s2,3)*(9*t1 - 4*(4 + 5*t2)) + 
               Power(s2,2)*(5 - 16*Power(t1,2) + 107*t2 + 
                  76*Power(t2,2) + 2*t1*(-4 + 9*t2)) + 
               s2*(90 - 22*Power(t1,3) - 123*t2 - 167*Power(t2,2) + 
                  2*Power(t2,3) + Power(t1,2)*(98 + 55*t2) + 
                  t1*(48 - 92*t2 - 95*Power(t2,2)))) + 
            Power(s1,2)*(-58 + 3*Power(t1,4) + 
               Power(s2,3)*(-22 + 6*t1 - 15*t2) + 98*t2 + 
               125*Power(t2,2) + Power(t1,3)*(-2 + 3*t2) + 
               Power(t1,2)*(-4 + 8*t2 - 5*Power(t2,2)) - 
               t1*(40 - 110*t2 - 44*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(62 - 9*Power(t1,2) + 181*t2 + 
                  t1*(-28 + 41*t2)) - 
               s2*(-24 + 254*t2 + 117*Power(t2,2) + 
                  Power(t1,2)*(-52 + 29*t2) + 
                  t1*(23 + 154*t2 - 4*Power(t2,2))))) + 
         Power(s,3)*(66 + 196*t1 + 178*Power(t1,2) + 18*Power(t1,3) + 
            8*Power(t1,4) + 2*Power(t1,5) + 
            2*Power(s2,4)*(-2 + t1 - t2) - 220*t2 - 197*t1*t2 - 
            184*Power(t1,2)*t2 - 69*Power(t1,3)*t2 + 2*Power(t1,4)*t2 + 
            193*Power(t2,2) - 36*t1*Power(t2,2) + 
            26*Power(t1,2)*Power(t2,2) - 4*Power(t1,3)*Power(t2,2) + 
            12*Power(t2,3) + 79*t1*Power(t2,3) + 
            14*Power(t1,2)*Power(t2,3) - 4*Power(t2,4) - 
            14*t1*Power(t2,4) + 
            Power(s2,3)*(-8*Power(t1,2) + t1*(7 + 12*t2) + 
               8*(4 + 5*t2 - 2*Power(t2,2))) + 
            Power(s1,5)*(-11*Power(s2,2) - 24*Power(t1,2) + 
               t1*(-34 + 7*t2) + s2*(23 + 35*t1 + 22*t2) + 
               3*(2 - 9*t2 + Power(t2,2))) + 
            Power(s2,2)*(11 + 12*Power(t1,3) + 
               Power(t1,2)*(6 - 16*t2) + 36*t2 + 8*Power(t2,2) + 
               24*Power(t2,3) + t1*(-36 - 155*t2 + 24*Power(t2,2))) - 
            s2*(90 + 8*Power(t1,4) + Power(t1,3)*(17 - 4*t2) - 82*t2 + 
               28*Power(t2,2) + 60*Power(t2,3) - 8*Power(t2,4) + 
               2*Power(t1,2)*(7 - 92*t2 + 2*Power(t2,2)) + 
               t1*(199 - 142*t2 + 13*Power(t2,2) + 30*Power(t2,3))) + 
            Power(s1,4)*(-41 - 11*Power(s2,3) + 29*Power(t1,3) + 
               102*t2 + 14*Power(t2,2) + Power(t1,2)*(41 + 19*t2) + 
               Power(s2,2)*(55 + 51*t1 + 86*t2) + 
               t1*(29 + 175*t2 - 24*Power(t2,2)) - 
               s2*(-6 + 69*Power(t1,2) + 295*t2 - 17*Power(t2,2) + 
                  t1*(79 + 105*t2))) + 
            Power(s1,3)*(117 + 6*Power(s2,4) - 9*Power(t1,4) + 
               Power(t1,3)*(46 - 27*t2) - 85*t2 - 210*Power(t2,2) + 
               14*Power(t2,3) + Power(s2,3)*(24 - 28*t1 + 98*t2) + 
               Power(s2,2)*(29 + 29*Power(t1,2) + t1*(87 - 245*t2) - 
                  466*t2 + 22*Power(t2,2)) + 
               Power(t1,2)*(68 - 198*t2 + 23*Power(t2,2)) + 
               t1*(136 - 298*t2 - 90*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(-182 + 2*Power(t1,3) + 449*t2 + 210*Power(t2,2) + 
                  Power(t1,2)*(-157 + 174*t2) + 
                  t1*(-123 + 629*t2 - 48*Power(t2,2)))) + 
            Power(s1,2)*(-137 + Power(t1,5) + 
               Power(s2,3)*(10 + 41*Power(t1,2) + t1*(82 - 74*t2) - 
                  221*t2) + 83*t2 + 228*Power(t2,2) + 45*Power(t2,3) + 
               3*Power(t2,4) + Power(t1,4)*(-4 + 5*t2) + 
               Power(s2,4)*(6 - 14*t1 + 20*t2) - 
               3*Power(t1,3)*(9 + 14*t2 + Power(t2,2)) + 
               t1*(-310 + 143*t2 + 433*Power(t2,2) - 52*Power(t2,3)) - 
               Power(t1,2)*(247 + 87*t2 - 155*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,2)*(-264 - 39*Power(t1,3) + 
                  93*Power(t1,2)*(-2 + t2) + 261*t2 + 
                  283*Power(t2,2) + 4*t1*(-19 + 78*t2 + Power(t2,2))) + 
               s2*(313 + 11*Power(t1,4) + Power(t1,3)*(102 - 44*t2) - 
                  184*t2 - 668*Power(t2,2) + 13*Power(t2,3) - 
                  Power(t1,2)*(-93 + 49*t2 + Power(t2,2)) + 
                  t1*(463 - 63*t2 - 412*Power(t2,2) + 4*Power(t2,3)))) + 
            s1*(-27 - 24*Power(t1,5) + 
               2*Power(s2,4)*(-2 + 5*t1 - 10*t2) + 180*t2 - 
               241*Power(t2,2) - 80*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,4)*(83 + 49*t2) + 
               Power(t1,3)*(-46 + 5*t2 - 55*Power(t2,2)) + 
               Power(t1,2)*(-76 + 415*t2 - 127*Power(t2,2) + 
                  29*Power(t2,3)) + 
               t1*(-94 + 283*t2 - 279*Power(t2,2) - 57*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,3)*(67 + 6*Power(t1,2) - 64*t2 - 
                  114*Power(t2,2) + t1*(46 + 39*t2)) + 
               Power(s2,2)*(-42*Power(t1,3) + 
                  187*Power(t1,2)*(1 + t2) + 
                  t1*(17 - 17*t2 - 275*Power(t2,2)) + 
                  4*(44 + 4*t2 - 63*Power(t2,2) + 2*Power(t2,3))) + 
               s2*(-42 + 62*Power(t1,4) - 19*t2 + 275*Power(t2,2) + 
                  146*Power(t2,3) + 4*Power(t2,4) - 
                  Power(t1,3)*(220 + 177*t2) + 
                  4*Power(t1,2)*(24 - 13*t2 + 54*Power(t2,2)) - 
                  t1*(76 + 414*t2 - 290*Power(t2,2) + 45*Power(t2,3))))) \
+ Power(s,2)*(-12 - 235*t1 - 277*Power(t1,2) - 82*Power(t1,3) - 
            28*Power(t1,4) + Power(t1,5) + Power(t1,6) + 41*t2 + 
            592*t1*t2 + 761*Power(t1,2)*t2 + 241*Power(t1,3)*t2 - 
            38*Power(t1,4)*t2 + 7*Power(t1,5)*t2 - 205*Power(t2,2) - 
            589*t1*Power(t2,2) - 602*Power(t1,2)*Power(t2,2) - 
            32*Power(t1,3)*Power(t2,2) - 20*Power(t1,4)*Power(t2,2) + 
            212*Power(t2,3) + 197*t1*Power(t2,3) + 
            51*Power(t1,2)*Power(t2,3) + 26*Power(t1,3)*Power(t2,3) - 
            8*Power(t2,4) + 40*t1*Power(t2,4) - 
            12*Power(t1,2)*Power(t2,4) - 12*Power(t2,5) - 
            2*t1*Power(t2,5) + Power(s2,5)*(4 - 2*t1 + 6*t2) + 
            Power(s2,4)*(14 + 9*Power(t1,2) - 23*t2 - 6*Power(t2,2) - 
               t1*(22 + 21*t2)) + 
            Power(s2,3)*(-47 - 16*Power(t1,3) - 32*t2 + 
               44*Power(t2,2) - 36*Power(t2,3) + 
               Power(t1,2)*(41 + 20*t2) + 
               t1*(-31 + 95*t2 + 44*Power(t2,2))) + 
            Power(s2,2)*(-7 + 14*Power(t1,4) - 4*t2 - 76*Power(t2,2) + 
               46*Power(t2,3) - 12*Power(t2,4) + 
               Power(t1,3)*(-31 + 6*t2) - 
               Power(t1,2)*(8 + 159*t2 + 90*Power(t2,2)) + 
               t1*(28 + 301*t2 - 141*Power(t2,2) + 98*Power(t2,3))) + 
            s2*(48 - 6*Power(t1,5) + Power(t1,4)*(7 - 18*t2) + 26*t2 + 
               19*Power(t2,2) - 84*Power(t2,3) - 32*Power(t2,4) + 
               Power(t1,3)*(53 + 125*t2 + 72*Power(t2,2)) + 
               Power(t1,2)*(101 - 510*t2 + 129*Power(t2,2) - 
                  88*Power(t2,3)) + 
               t1*(293 - 771*t2 + 667*Power(t2,2) - 83*Power(t2,3) + 
                  30*Power(t2,4))) + 
            Power(s1,6)*(-2 + 13*Power(s2,2) + 21*Power(t1,2) + 
               t1*(26 - 4*t2) + 7*t2 - Power(t2,2) - 
               s2*(34*t1 + 13*(1 + t2))) + 
            Power(s1,5)*(25 + 26*Power(s2,3) - 33*Power(t1,3) - 46*t2 + 
               Power(t2,2) - Power(t1,2)*(71 + 26*t2) - 
               Power(s2,2)*(61 + 89*t1 + 74*t2) + 
               t1*(-14 - 141*t2 + 17*Power(t2,2)) + 
               s2*(-27 + 96*Power(t1,2) + 199*t2 - 10*Power(t2,2) + 
                  12*t1*(11 + 8*t2))) + 
            Power(s1,4)*(3*Power(s2,4) + 12*Power(t1,4) + 
               Power(t1,3)*(-50 + 54*t2) - 
               2*Power(s2,3)*(4 + 5*t1 + 75*t2) + 
               Power(t1,2)*(-153 + 346*t2 - 27*Power(t2,2)) + 
               t1*(-171 + 370*t2 + 82*Power(t2,2) - 3*Power(t2,3)) - 
               2*(25 + 16*t2 - 78*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,2)*(-150 + 23*Power(t1,2) + 557*t2 - 
                  33*Power(t2,2) + 8*t1*(-11 + 47*t2)) + 
               s2*(207 - 28*Power(t1,3) + Power(t1,2)*(146 - 280*t2) - 
                  388*t2 - 173*Power(t2,2) + 
                  t1*(315 - 906*t2 + 71*Power(t2,2)))) + 
            Power(s1,3)*(28 - 14*Power(s2,5) + 
               2*Power(s2,4)*(15 + 43*t1 - 56*t2) + 
               Power(t1,4)*(87 - 26*t2) + 190*t2 - 294*Power(t2,2) - 
               31*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,3)*(51 - 109*t2 + 10*Power(t2,2)) + 
               Power(t1,2)*(239 + 273*t2 - 270*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(219 + 174*t2 - 849*Power(t2,2) + 67*Power(t2,3)) - 
               Power(s2,3)*(169 + 174*Power(t1,2) - 429*t2 + 
                  28*Power(t2,2) - 5*t1*(-57 + 79*t2)) + 
               Power(s2,2)*(391 + 146*Power(t1,3) + 
                  Power(t1,2)*(567 - 480*t2) - 162*t2 - 
                  467*Power(t2,2) + t1*(451 - 916*t2 + 66*Power(t2,2))) \
- s2*(196 + 44*Power(t1,4) + Power(t1,3)*(399 - 223*t2) + 286*t2 - 
                  1054*Power(t2,2) + 39*Power(t2,3) + 
                  Power(t1,2)*(333 - 596*t2 + 48*Power(t2,2)) + 
                  t1*(624 + 127*t2 - 710*Power(t2,2) + 9*Power(t2,3)))) \
+ Power(s1,2)*(176 + Power(s2,5)*(14 + 11*t1 - 15*t2) - 471*t2 + 
               107*Power(t2,2) + 353*Power(t2,3) - 49*Power(t2,4) + 
               Power(t1,5)*(9 + 2*t2) + 
               Power(t1,4)*(78 - 142*t2 + Power(t2,2)) + 
               Power(t1,2)*(34 - 657*t2 + 457*Power(t2,2) - 
                  88*Power(t2,3)) + 
               Power(t1,3)*(104 - 492*t2 + 261*Power(t2,2) - 
                  3*Power(t2,3)) + 
               t1*(235 - 382*t2 + 264*Power(t2,2) + 272*Power(t2,3)) + 
               Power(s2,4)*(-48 - 44*Power(t1,2) + 117*t2 + 
                  t1*(-108 + 61*t2)) + 
               Power(s2,3)*(213 + 66*Power(t1,3) + 
                  Power(t1,2)*(231 - 95*t2) + 158*t2 - 
                  357*Power(t2,2) - 2*t1*(-7 + 52*t2 + 8*Power(t2,2))) \
- Power(s2,2)*(51 + 44*Power(t1,4) + Power(t1,3)*(185 - 69*t2) + 
                  515*t2 - 917*Power(t2,2) + 39*Power(t2,3) + 
                  Power(t1,2)*(-194 + 285*t2 - 33*Power(t2,2)) + 
                  t1*(224 + 999*t2 - 948*Power(t2,2) + 6*Power(t2,3))) \
+ s2*(-292 + 11*Power(t1,5) + Power(t1,4)*(39 - 22*t2) + 555*t2 - 
                  415*Power(t2,2) - 384*Power(t2,3) - 9*Power(t2,4) - 
                  2*Power(t1,3)*(119 - 207*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (-93 + 1333*t2 - 852*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(15 + 1126*t2 - 1303*Power(t2,2) + 144*Power(t2,3))\
)) + s1*(-195 - 14*Power(t1,6) + Power(s2,5)*(-14 + t1 - 3*t2) + 
               533*t2 - 160*Power(t2,2) - 288*Power(t2,3) + 
               48*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,5)*(54 + 33*t2) + 
               Power(t1,4)*(-131 + 25*t2 - 37*Power(t2,2)) + 
               Power(t1,3)*(-188 + 135*t2 + 48*Power(t2,2) + 
                  13*Power(t2,3)) + 
               Power(t1,2)*(-168 - 256*t2 + 730*Power(t2,2) - 
                  244*Power(t2,3) + 5*Power(t2,4)) + 
               t1*(-266 + 38*t2 + 662*Power(t2,2) - 682*Power(t2,3) + 
                  83*Power(t2,4)) + 
               Power(s2,4)*(15 - 18*Power(t1,2) + 11*t2 - 
                  96*Power(t2,2) + t1*(69 + 95*t2)) + 
               Power(s2,3)*(62*Power(t1,3) - 
                  3*Power(t1,2)*(59 + 100*t2) + 
                  t1*(159 - 106*t2 + 325*Power(t2,2)) + 
                  3*(11 - 62*t2 + 40*Power(t2,2) - 4*Power(t2,3))) + 
               Power(s2,2)*(-263 - 88*Power(t1,4) + 209*t2 + 
                  151*Power(t2,2) - 292*Power(t2,3) - 6*Power(t2,4) + 
                  Power(t1,3)*(257 + 360*t2) + 
                  Power(t1,2)*(-494 + 204*t2 - 399*Power(t2,2)) + 
                  t1*(-251 + 440*t2 - 81*Power(t2,2) + 45*Power(t2,3))) \
+ s2*(379 + 57*Power(t1,5) - 526*t2 - 150*Power(t2,2) + 
                  588*Power(t2,3) - 38*Power(t2,4) - 
                  Power(t1,4)*(189 + 185*t2) + 
                  Power(t1,3)*(451 - 134*t2 + 207*Power(t2,2)) - 
                  Power(t1,2)*
                   (-406 + 389*t2 + 87*Power(t2,2) + 46*Power(t2,3)) + 
                  t1*(400 + 122*t2 - 889*Power(t2,2) + 484*Power(t2,3) - 
                     3*Power(t2,4))))) + 
         s*(-65 + 61*t1 + 184*Power(t1,2) + 102*Power(t1,3) + 
            35*Power(t1,4) - 11*Power(t1,5) - 2*Power(t1,6) + 342*t2 - 
            2*Power(s2,6)*t2 - 341*t1*t2 - 891*Power(t1,2)*t2 - 
            334*Power(t1,3)*t2 + 50*Power(t1,4)*t2 - 5*Power(t1,5)*t2 + 
            3*Power(t1,6)*t2 - 477*Power(t2,2) + 885*t1*Power(t2,2) + 
            1273*Power(t1,2)*Power(t2,2) + 156*Power(t1,3)*Power(t2,2) - 
            18*Power(t1,4)*Power(t2,2) - 7*Power(t1,5)*Power(t2,2) + 
            80*Power(t2,3) - 907*t1*Power(t2,3) - 
            558*Power(t1,2)*Power(t2,3) - 27*Power(t1,3)*Power(t2,3) + 
            6*Power(t1,4)*Power(t2,3) + 160*Power(t2,4) + 
            300*t1*Power(t2,4) + 68*Power(t1,2)*Power(t2,4) - 
            40*Power(t2,5) - 20*t1*Power(t2,5) - 
            2*Power(t1,2)*Power(t2,5) + 
            Power(s1,7)*(-8*Power(s2,2) + t1*(-7 - 10*t1 + t2) + 
               3*s2*(1 + 6*t1 + t2)) + 
            Power(s2,4)*(-18 + 3*Power(t1,2)*(-6 + t2) + 64*t2 - 
               24*Power(t2,2) + 24*Power(t2,3) + 
               t1*(27 - 19*t2 - 63*Power(t2,2))) + 
            Power(s2,5)*(t1*(4 + 5*t2) + 2*(-3 + t2 + 6*Power(t2,2))) + 
            Power(s2,3)*(20 + Power(t1,3)*(32 - 22*t2) + 14*t2 + 
               4*Power(t2,2) + 36*Power(t2,3) + 8*Power(t2,4) + 
               2*Power(t1,2)*(-17 + 25*t2 + 62*Power(t2,2)) + 
               t1*(36 - 234*t2 + 69*Power(t2,2) - 82*Power(t2,3))) + 
            Power(s2,2)*(21 + 28*Power(t1,4)*(-1 + t2) - 152*t2 + 
               279*Power(t2,2) - 156*Power(t2,3) + 76*Power(t2,4) - 
               2*Power(t1,3)*t2*(28 + 57*t2) + 
               Power(t1,2)*(17 + 326*t2 - 84*Power(t2,2) + 
                  98*Power(t2,3)) + 
               t1*(22 - 296*t2 + 152*Power(t2,2) - 127*Power(t2,3) - 
                  18*Power(t2,4))) + 
            s2*(-3*Power(t1,5)*(-4 + 5*t2) + 
               4*Power(t1,4)*(6 + 7*t2 + 12*Power(t2,2)) - 
               Power(t1,3)*(70 + 206*t2 - 57*Power(t2,2) + 
                  46*Power(t2,3)) + 
               2*Power(t1,2)*
                (-72 + 308*t2 - 156*Power(t2,2) + 59*Power(t2,3) + 
                  5*Power(t2,4)) + 
               t1*(-198 + 1021*t2 - 1533*Power(t2,2) + 
                  708*Power(t2,3) - 144*Power(t2,4) + 4*Power(t2,5)) + 
               2*(24 - 110*t2 + 55*Power(t2,2) + 108*Power(t2,3) - 
                  78*Power(t2,4) + 12*Power(t2,5))) - 
            Power(s1,6)*(3 + 23*Power(s2,3) - 20*Power(t1,3) - 4*t2 + 
               Power(t2,2) - 4*Power(s2,2)*(10 + 17*t1 + 7*t2) - 
               2*Power(t1,2)*(23 + 8*t2) + 
               t1*(1 - 45*t2 + 4*Power(t2,2)) + 
               s2*(-14 + 65*Power(t1,2) + 55*t2 - Power(t2,2) + 
                  t1*(88 + 42*t2))) + 
            Power(s1,5)*(-6 - 19*Power(s2,4) - 9*Power(t1,4) + 
               Power(t1,3)*(15 - 42*t2) + 38*t2 - 36*Power(t2,2) + 
               4*Power(t2,3) + Power(s2,3)*(17 + 65*t1 + 82*t2) + 
               Power(t1,2)*(96 - 221*t2 + 8*Power(t2,2)) + 
               t1*(97 - 173*t2 - 37*Power(t2,2) + Power(t2,3)) - 
               Power(s2,2)*(-104 + 82*Power(t1,2) + 294*t2 - 
                  11*Power(t2,2) + 3*t1*(1 + 72*t2)) + 
               s2*(-99 + 45*Power(t1,3) + 150*t2 + 61*Power(t2,2) + 
                  Power(t1,2)*(-29 + 176*t2) + 
                  t1*(-207 + 531*t2 - 28*Power(t2,2)))) + 
            Power(s1,4)*(59 + 7*Power(s2,5) - 2*Power(t1,5) - 187*t2 + 
               108*Power(t2,2) + 21*Power(t2,3) - Power(t2,4) + 
               Power(t1,4)*(-69 + 32*t2) + 
               Power(s2,4)*(-51 - 49*t1 + 114*t2) + 
               Power(t1,3)*(-151 + 139*t2 - 3*Power(t2,2)) + 
               Power(s2,3)*(245 + 107*Power(t1,2) + t1*(291 - 409*t2) - 
                  372*t2 + 27*Power(t2,2)) + 
               t1*(22 - 318*t2 + 520*Power(t2,2) - 30*Power(t2,3)) - 
               Power(t1,2)*(131 + 61*t2 - 211*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,2)*(-214 - 97*Power(t1,3) + 117*t2 + 
                  303*Power(t2,2) + Power(t1,2)*(-498 + 508*t2) + 
                  t1*(-702 + 913*t2 - 70*Power(t2,2))) + 
               s2*(-59 + 34*Power(t1,4) + Power(t1,3)*(327 - 245*t2) + 
                  421*t2 - 583*Power(t2,2) + 27*Power(t2,3) + 
                  Power(t1,2)*(608 - 680*t2 + 46*Power(t2,2)) + 
                  t1*(367 - 93*t2 - 502*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(s1,3)*(-22 + 11*Power(s2,6) + Power(t1,6) + 
               Power(t1,5)*(45 - 7*t2) - 10*t2 + 328*Power(t2,2) - 
               318*Power(t2,3) + 22*Power(t2,4) + 
               Power(s2,5)*(-34 - 75*t1 + 63*t2) - 
               2*Power(t1,4)*(94 - 22*t2 + Power(t2,2)) + 
               Power(s2,4)*(67 + 191*Power(t1,2) + t1*(226 - 275*t2) - 
                  129*t2 + 17*Power(t2,2)) + 
               Power(t1,3)*(-222 + 838*t2 - 241*Power(t2,2) + 
                  3*Power(t2,3)) + 
               5*Power(t1,2)*
                (-23 + 154*t2 - 146*Power(t2,2) + 7*Power(t2,3)) + 
               t1*(-83 + 193*t2 - 77*Power(t2,2) - 240*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,3)*(-71 - 234*Power(t1,3) - 503*t2 + 
                  428*Power(t2,2) + Power(t1,2)*(-519 + 454*t2) + 
                  t1*(5 + 262*t2 - 40*Power(t2,2))) + 
               s2*(186 - 35*Power(t1,5) - 419*t2 + 147*Power(t2,2) + 
                  290*Power(t2,3) - 6*Power(t2,4) + 
                  Power(t1,4)*(-259 + 107*t2) + 
                  Power(t1,3)*(515 - 84*t2 - 2*Power(t2,2)) + 
                  t1*(316 - 1755*t2 + 1900*Power(t2,2) - 
                     99*Power(t2,3)) + 
                  Power(t1,2)*
                   (433 - 2339*t2 + 940*Power(t2,2) - 12*Power(t2,3))) + 
               Power(s2,2)*(-189 + 141*Power(t1,4) + 
                  Power(t1,3)*(541 - 342*t2) + 937*t2 - 
                  1106*Power(t2,2) + 36*Power(t2,3) + 
                  3*Power(t1,2)*(-133 - 31*t2 + 9*Power(t2,2)) + 
                  t1*(-140 + 2004*t2 - 1127*Power(t2,2) + 9*Power(t2,3)))\
) + Power(s1,2)*(11 + 15*Power(t1,6) + 
               Power(s2,5)*(-2 + 15*Power(t1,2) + t1*(55 - 23*t2) - 
                  7*t2) - 61*t2 - 174*Power(t2,2) + 159*Power(t2,3) + 
               71*Power(t2,4) - 6*Power(t2,5) + 
               Power(s2,6)*(-11 - 3*t1 + 6*t2) + 
               Power(t1,5)*(70 - 118*t2 + Power(t2,2)) - 
               Power(t1,4)*(-77 + 190*t2 - 164*Power(t2,2) + 
                  Power(t2,3)) - 
               2*Power(t1,3)*
                (56 - 197*t2 + 121*Power(t2,2) + 22*Power(t2,3)) + 
               t1*(186 + 22*t2 - 957*Power(t2,2) + 925*Power(t2,3) - 
                  100*Power(t2,4)) + 
               Power(t1,2)*(149 + 877*t2 - 1499*Power(t2,2) + 
                  552*Power(t2,3) - 3*Power(t2,4)) + 
               Power(s2,4)*(28 - 30*Power(t1,3) - 295*t2 + 
                  228*Power(t2,2) + Power(t1,2)*(-95 + 32*t2) + 
                  2*t1*(76 - 76*t2 + 7*Power(t2,2))) + 
               Power(s2,2)*(403 - 15*Power(t1,5) + 80*t2 - 
                  811*Power(t2,2) + 589*Power(t2,3) + 9*Power(t2,4) + 
                  Power(t1,4)*(35 + 2*t2) + 
                  Power(t1,3)*(650 - 866*t2 + 45*Power(t2,2)) + 
                  t1*(369 - 326*t2 + 369*Power(t2,2) - 
                     132*Power(t2,3)) + 
                  Power(t1,2)*
                   (545 - 1691*t2 + 1152*Power(t2,2) - 9*Power(t2,3))) + 
               Power(s2,3)*(-209 + 30*Power(t1,4) + 
                  Power(t1,3)*(50 - 18*t2) + 343*t2 - 342*Power(t2,2) + 
                  39*Power(t2,3) + 
                  Power(t1,2)*(-514 + 616*t2 - 43*Power(t2,2)) + 
                  t1*(-276 + 1193*t2 - 836*Power(t2,2) + 4*Power(t2,3))) \
+ s2*(-175 + 3*Power(t1,6) + Power(t1,5)*(-49 + t2) + 10*t2 + 
                  988*Power(t2,2) - 967*Power(t2,3) + 68*Power(t2,4) + 
                  Power(t1,4)*(-356 + 527*t2 - 17*Power(t2,2)) + 
                  Power(t1,3)*
                   (-374 + 983*t2 - 708*Power(t2,2) + 6*Power(t2,3)) + 
                  Power(t1,2)*
                   (-48 - 411*t2 + 215*Power(t2,2) + 137*Power(t2,3)) - 
                  t1*(577 + 898*t2 - 2247*Power(t2,2) + 1118*Power(t2,3))\
)) + s1*(138 - 3*Power(t1,7) - 440*t2 + 500*Power(t2,2) + 
               74*Power(t2,3) - 340*Power(t2,4) + 68*Power(t2,5) + 
               Power(t1,6)*(17 + 7*t2) + Power(s2,6)*(6 - 2*t1 + 7*t2) - 
               Power(t1,5)*(23 + 11*t2 + 6*Power(t2,2)) - 
               Power(t1,4)*(-160 + 294*t2 - 148*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(119 + 23*t2 - 550*Power(t2,2) + 739*Power(t2,3) - 
                  303*Power(t2,4)) + 
               Power(t1,3)*(355 - 884*t2 + 758*Power(t2,2) - 
                  225*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(133 - 401*t2 + 254*Power(t2,2) - 
                  113*Power(t2,3) + 66*Power(t2,4)) + 
               Power(s2,5)*(29 + 13*Power(t1,2) - 21*t2 + 
                  43*Power(t2,2) - 3*t1*(11 + 21*t2)) + 
               Power(s2,4)*(-29 - 35*Power(t1,3) + 28*t2 + 
                  43*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t1,2)*(89 + 189*t2) + 
                  t1*(-140 + 47*t2 - 170*Power(t2,2))) + 
               Power(s2,3)*(-15 + 50*Power(t1,4) + 217*t2 - 
                  467*Power(t2,2) + 262*Power(t2,3) + 4*Power(t2,4) - 
                  2*Power(t1,3)*(73 + 133*t2) + 
                  Power(t1,2)*(269 - 4*t2 + 258*Power(t2,2)) - 
                  t1*(134 - 353*t2 + 364*Power(t2,2) + 15*Power(t2,3))) + 
               s2*(-216 + 17*Power(t1,6) + 274*t2 + 56*Power(t2,2) - 
                  390*Power(t2,3) + 236*Power(t2,4) + 12*Power(t2,5) - 
                  7*Power(t1,5)*(11 + 9*t2) + 
                  Power(t1,4)*(122 + 49*t2 + 59*Power(t2,2)) + 
                  Power(t1,3)*
                   (-512 + 997*t2 - 574*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-203 + 546*t2 - 518*Power(t2,2) + 
                     485*Power(t2,3) - 146*Power(t2,4)) + 
                  Power(t1,2)*
                   (-755 + 2101*t2 - 2059*Power(t2,2) + 
                     696*Power(t2,3) - 10*Power(t2,4))) + 
               Power(s2,2)*(87 - 40*Power(t1,5) - 177*t2 + 
                  281*Power(t2,2) - 352*Power(t2,3) + 58*Power(t2,4) + 
                  9*Power(t1,4)*(16 + 21*t2) - 
                  Power(t1,3)*(257 + 60*t2 + 184*Power(t2,2)) + 
                  Power(t1,2)*
                   (515 - 1084*t2 + 747*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(415 - 1434*t2 + 1768*Power(t2,2) - 
                     733*Power(t2,3) + 3*Power(t2,4))))))*
       T2q(1 - s + s1 - t2,1 - s2 + t1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*
       (-1 + t2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(2*Power(s,7)*(-1 + s1)*(t1 - t2) + 
         Power(s,6)*(-2 - Power(t1,2) + t1*(11 + 2*s2 - 9*t2) - 9*t2 - 
            2*s2*t2 + 10*Power(t2,2) - 
            s1*(-6 + t1 + s2*(2 + 5*t1 - 3*t2) + t2 - 14*t1*t2 + 
               14*Power(t2,2)) + 
            Power(s1,2)*(-Power(t1,2) - t1*(8 + t2) + s2*(2 + t1 + t2) + 
               2*(-2 + 4*t2 + Power(t2,2)))) + 
         Power(s,5)*(5 + 6*s2 - 45*t1 - 14*s2*t1 + 2*Power(s2,2)*t1 + 
            11*Power(t1,2) - 3*s2*Power(t1,2) + Power(t1,3) + 34*t2 + 
            5*s2*t2 - 2*Power(s2,2)*t2 + 40*t1*t2 + 16*s2*t1*t2 - 
            7*Power(t1,2)*t2 - 43*Power(t2,2) - 12*s2*Power(t2,2) - 
            14*t1*Power(t2,2) + 20*Power(t2,3) + 
            Power(s1,3)*(10 + Power(s2,2) + 3*Power(t1,2) - 7*t2 - 
               7*Power(t2,2) + 3*t1*(3 + t2) - 2*s2*(3 + 2*t1 + t2)) + 
            s1*(-9 - 5*Power(t1,3) + 5*t2 + 15*t1*(-1 + t2)*t2 + 
               14*Power(t2,2) - 30*Power(t2,3) + 
               Power(s2,2)*(2 + t1 + 2*t2) + 
               4*Power(t1,2)*(-2 + 5*t2) + 
               2*s2*(-5 + 2*Power(t1,2) + t1*(11 - 23*t2) + 3*t2 + 
                  16*Power(t2,2))) + 
            Power(s1,2)*(-8 + Power(t1,2)*(5 - 11*t2) - 23*t2 + 
               35*Power(t2,2) + 4*Power(t2,3) - 
               Power(s2,2)*(5 + 2*t1 + 3*t2) + 
               t1*(25 - 38*t2 + 7*Power(t2,2)) + 
               s2*(14 + 2*Power(t1,2) - 15*t2 - 5*Power(t2,2) + 
                  6*t1*(1 + 2*t2)))) + 
         Power(s,4)*(-1 + 145*t1 - 37*Power(t1,2) - 5*Power(t1,3) - 
            116*t2 + 2*Power(s2,3)*t2 - 173*t1*t2 + 47*Power(t1,2)*t2 + 
            6*Power(t1,3)*t2 + 189*Power(t2,2) + 58*t1*Power(t2,2) - 
            24*Power(t1,2)*Power(t2,2) - 90*Power(t2,3) + 
            18*Power(t2,4) + Power(s2,2)*
             (2 - 10*t1 - 2*t2 + 7*t1*t2 - 8*Power(t2,2)) - 
            Power(s1,4)*(4 + 2*Power(s2,2) + 4*Power(t1,2) + 4*t2 - 
               8*Power(t2,2) + t1*(5 + t2) - s2*(5 + 6*t1 + t2)) + 
            s2*(-42 - 15*Power(t1,2)*(-1 + t2) + 31*t2 + 
               13*Power(t2,2) - 32*Power(t2,3) + 
               t1*(35 - 65*t2 + 52*Power(t2,2))) - 
            Power(s1,3)*(-5 + 2*Power(s2,3) + Power(t1,3) - 11*t2 + 
               3*Power(t2,2) + 13*Power(t2,3) - 
               Power(s2,2)*(4 + 5*t1 + 4*t2) - 
               4*Power(t1,2)*(-4 + 7*t2) + 
               t1*(1 - 18*t2 + 19*Power(t2,2)) + 
               s2*(29 + 2*Power(t1,2) - 18*t2 - 13*Power(t2,2) + 
                  10*t1*(-1 + 3*t2))) + 
            s1*(28 - 3*Power(t1,4) + Power(s2,3)*(2 + 2*t1 - 5*t2) + 
               Power(t1,3)*(18 - 4*t2) + 5*t2 - 39*Power(t2,2) + 
               33*Power(t2,3) - 27*Power(t2,4) - 
               Power(s2,2)*(3 + 7*Power(t1,2) + t1*(12 - 34*t2) + 
                  3*t2 + 17*Power(t2,2)) + 
               Power(t1,2)*(48 - 101*t2 + 50*Power(t2,2)) - 
               t1*(11 + 58*t2 - 35*Power(t2,2) + 16*Power(t2,3)) + 
               s2*(31 + 8*Power(t1,3) - 50*t2 - 7*Power(t2,2) + 
                  76*Power(t2,3) - Power(t1,2)*(8 + 25*t2) + 
                  t1*(-53 + 158*t2 - 79*Power(t2,2)))) + 
            Power(s1,2)*(-23 + Power(t1,3)*(11 - 6*t2) + 61*t2 - 
               107*Power(t2,2) + 67*Power(t2,3) + 2*Power(t2,4) + 
               Power(s2,3)*(1 + t1 + 3*t2) + 
               Power(t1,2)*(22 + 7*t2 - 16*Power(t2,2)) + 
               Power(s2,2)*(2 - 2*Power(t1,2) + t1*(13 - 20*t2) + 
                  8*t2 + 3*Power(t2,2)) + 
               t1*(-76 + 143*t2 - 75*Power(t2,2) + 20*Power(t2,3)) + 
               s2*(26 + Power(t1,3) + 36*t2 - 61*Power(t2,2) - 
                  13*Power(t2,3) + Power(t1,2)*(-25 + 23*t2) + 
                  t1*(-49 + 19*t2 + 4*Power(t2,2))))) + 
         Power(s,3)*(2*Power(s1,5)*
             (Power(s2,2) - 2*s2*t1 + Power(t1,2) - 2*Power(-1 + t2,2)) \
+ Power(s1,4)*(Power(s2,3) + Power(t1,3) + Power(t1,2)*(14 - 22*t2) + 
               4*Power(-1 + t2,2)*(-1 + 2*t2) - 
               Power(s2,2)*(4 + t1 + 4*t2) + 
               t1*(-9 - 16*t2 + 25*Power(t2,2)) - 
               s2*(Power(t1,2) + t1*(10 - 26*t2) + 
                  17*(-1 + Power(t2,2)))) + 
            Power(s1,3)*(Power(s2,4) + Power(s2,3)*(4 - 3*t1 - 3*t2) + 
               3*Power(t1,3)*(-4 + 3*t2) - 
               Power(-1 + t2,2)*(-3 + 24*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(46 - 95*t2 + 31*Power(t2,2)) + 
               t1*(69 - 119*t2 + 93*Power(t2,2) - 43*Power(t2,3)) + 
               Power(s2,2)*(45 + 3*Power(t1,2) - 51*t2 - 
                  12*Power(t2,2) + t1*(-28 + 23*t2)) - 
               s2*(19 + Power(t1,3) - t2 + 7*Power(t2,2) - 
                  25*Power(t2,3) + Power(t1,2)*(-36 + 29*t2) + 
                  t1*(83 - 130*t2 + 11*Power(t2,2)))) + 
            s1*(Power(t1,4)*(12 - 9*t2) + 2*Power(s2,4)*t2 + 
               Power(t1,3)*(-27 + 8*t2 + 18*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (121 + 45*t2 - 9*Power(t2,2) + 11*Power(t2,3)) + 
               Power(t1,2)*(-146 + 257*t2 - 173*Power(t2,2) + 
                  30*Power(t2,3)) - 
               2*t1*(10 - 109*t2 + 141*Power(t2,2) - 56*Power(t2,3) + 
                  14*Power(t2,4)) - 
               Power(s2,3)*(2 - 12*t2 + 7*Power(t2,2) + t1*(8 + t2)) + 
               Power(s2,2)*(-42 + Power(t1,2)*(28 - 13*t2) + 36*t2 + 
                  35*Power(t2,2) - 61*Power(t2,3) + 
                  7*t1*(3 - 16*t2 + 12*Power(t2,2))) + 
               s2*(29 - 52*t2 - 38*Power(t2,2) - 6*Power(t2,3) + 
                  67*Power(t2,4) + Power(t1,3)*(-32 + 21*t2) + 
                  Power(t1,2)*(8 + 92*t2 - 95*Power(t2,2)) + 
                  t1*(160 - 281*t2 + 198*Power(t2,2) - 13*Power(t2,3)))) \
+ 2*(Power(s2,3)*t2*(-4 + 5*t2) + 
               2*Power(-1 + t2,2)*t2*(54 - 24*t2 + Power(t2,2)) + 
               Power(t1,3)*(5 - 12*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(25 - 50*t2 + 46*Power(t2,2) - 
                  20*Power(t2,3)) + 
               2*t1*(-70 + 139*t2 - 84*Power(t2,2) + 9*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,2)*(-6 + 19*t2 - 9*Power(t2,2) - 
                  3*Power(t2,3) + t1*(10 - 14*t2 + Power(t2,2))) + 
               s2*(52 - 99*t2 + 65*Power(t2,2) + 3*Power(t2,3) - 
                  21*Power(t2,4) - 
                  3*Power(t1,2)*(5 - 10*t2 + 4*Power(t2,2)) + 
                  2*t1*(-7 + 18*t2 - 31*Power(t2,2) + 19*Power(t2,3)))) \
- Power(s1,2)*(Power(s2,4)*(-2 + t2) + Power(t1,4)*(-3 + 2*t2) + 
               Power(s2,3)*(3 + t1*(13 - 9*t2) + t2 - Power(t2,2)) + 
               Power(t1,3)*(18 - 29*t2 + 10*Power(t2,2)) - 
               Power(-1 + t2,2)*(101 - 56*t2 + 59*Power(t2,2)) + 
               2*Power(t1,2)*
                (55 - 78*t2 + 10*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-52 + 185*t2 - 168*Power(t2,2) + 51*Power(t2,3) - 
                  16*Power(t2,4)) + 
               Power(s2,2)*(23 + 17*t2 - 47*Power(t2,2) - 
                  15*Power(t2,3) + Power(t1,2)*(-23 + 17*t2) + 
                  t1*(8 - 39*t2 + 24*Power(t2,2))) + 
               s2*(75 + Power(t1,3)*(15 - 11*t2) - 97*t2 - 
                  86*Power(t2,2) + 101*Power(t2,3) + 7*Power(t2,4) + 
                  Power(t1,2)*(-29 + 67*t2 - 33*Power(t2,2)) + 
                  t1*(-173 + 235*t2 - 45*Power(t2,2) + 27*Power(t2,3))))) \
+ Power(-1 + t2,2)*(2*Power(s1,5)*Power(s2 - t1,2)*
             (-1 + 2*s2 - 2*t1 + t2) + 
            Power(s1,4)*(s2 - t1)*
             (2*Power(s2,3) + 2*Power(t1,3) + 4*Power(-1 + t2,2) + 
               4*t1*(-1 + t2)*t2 - Power(s2,2)*(13 + 2*t1 + 7*t2) - 
               Power(t1,2)*(11 + 9*t2) - 
               2*s2*(Power(t1,2) + 2*(-1 + t2)*t2 - 4*t1*(3 + 2*t2))) + 
            (-1 + t2)*(Power(t1,3)*(1 - 3*t2) + 
               2*Power(s2,3)*t2*(-1 + 2*t2) - 
               Power(t1,2)*(5 - 8*t2 + Power(t2,2)) - 
               Power(-1 + t2,2)*(-3 - 29*t2 + 8*Power(t2,2)) + 
               t1*(-39 + 86*t2 - 57*Power(t2,2) + 10*Power(t2,3)) - 
               Power(s2,2)*(6 - 14*t2 + 12*Power(t2,2) - 
                  6*Power(t2,3) + t1*(-2 + t2 + 7*Power(t2,2))) + 
               s2*(18 - 45*t2 + 40*Power(t2,2) - 15*Power(t2,3) + 
                  2*Power(t2,4) + 
                  3*Power(t1,2)*(-1 + 2*t2 + Power(t2,2)) + 
                  t1*(13 - 24*t2 + 11*Power(t2,2) - 4*Power(t2,3)))) + 
            s1*(-1 + t2)*(3*Power(t1,4) + 2*Power(s2,4)*t2 + 
               Power(t1,3)*(-19 + 6*t2) + 
               Power(t1,2)*(-70 + 98*t2 - 20*Power(t2,2)) + 
               Power(-1 + t2,2)*(-31 + 16*t2 + Power(t2,2)) + 
               t1*(-37 + 125*t2 - 99*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s2,3)*(12 - 13*t2 + 8*Power(t2,2) - 
                  t1*(2 + 7*t2)) + 
               Power(s2,2)*(-43 + 80*t2 - 31*Power(t2,2) + 
                  2*Power(t2,3) + Power(t1,2)*(7 + 8*t2) + 
                  t1*(-41 + 26*t2 - 12*Power(t2,2))) + 
               s2*(38 - 121*t2 + 89*Power(t2,2) - 7*Power(t2,3) + 
                  Power(t2,4) - Power(t1,3)*(8 + 3*t2) + 
                  Power(t1,2)*(48 - 19*t2 + 4*Power(t2,2)) + 
                  t1*(107 - 166*t2 + 45*Power(t2,2) - 2*Power(t2,3)))) + 
            Power(s1,3)*(Power(s2,4)*(7 - 3*t2) + 2*Power(-1 + t2,3) + 
               2*Power(t1,4)*(1 + t2) + 
               4*t1*Power(-1 + t2,2)*(3 + 2*t2) + 
               Power(t1,3)*(31 - 15*t2 - 4*Power(t2,2)) + 
               Power(t1,2)*(54 - 49*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(-48 + 35*t2 + Power(t2,2) + 
                  t1*(-23 + 7*t2)) - 
               Power(s2,2)*(-57 + 3*Power(t1,2)*(-9 + t2) + 56*t2 + 
                  Power(t2,2) + t1*(-129 + 89*t2 + 4*Power(t2,2))) + 
               s2*(-4*Power(-1 + t2,2)*(3 + 2*t2) - 
                  Power(t1,3)*(13 + 3*t2) + 
                  Power(t1,2)*(-112 + 69*t2 + 7*Power(t2,2)) + 
                  t1*(-113 + 111*t2 + Power(t2,2) + Power(t2,3)))) + 
            Power(s1,2)*(Power(t1,4)*(11 - 5*t2) - 
               4*Power(-1 + t2,3)*(2 + t2) + 
               Power(s2,4)*(6 - 5*t2 + Power(t2,2)) - 
               2*t1*Power(-1 + t2,2)*(-38 + 10*t2 + Power(t2,2)) + 
               Power(t1,3)*(31 - 51*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(74 - 149*t2 + 74*Power(t2,2) + 
                  Power(t2,3)) - 
               Power(s2,3)*(32 - 63*t2 + 19*Power(t2,2) + 
                  2*t1*(14 - 9*t2 + Power(t2,2))) + 
               Power(s2,2)*(87 - 174*t2 + 85*Power(t2,2) + 
                  2*Power(t2,3) + 
                  Power(t1,2)*(49 - 26*t2 + Power(t2,2)) + 
                  t1*(91 - 169*t2 + 42*Power(t2,2))) + 
               s2*(2*Power(t1,3)*(-19 + 9*t2) + 
                  Power(t1,2)*(-90 + 157*t2 - 31*Power(t2,2)) - 
                  Power(-1 + t2,2)*(75 - 22*t2 + Power(t2,2)) + 
                  t1*(-162 + 327*t2 - 165*Power(t2,2) + Power(t2,3) - 
                     Power(t2,4))))) - 
         s*(-1 + t2)*(6*Power(s1,5)*Power(s2 - t1,2)*(-1 + t2) - 
            Power(s1,4)*(2*Power(s2,4) - 
               Power(s2,3)*(21 + 4*t1 + 11*t2) + 
               Power(s2,2)*(3*t1*(23 + 9*t2) + 10*(-1 + Power(t2,2))) + 
               t1*(-2*Power(t1,3) + (-11 + t2)*Power(-1 + t2,2) + 
                  Power(t1,2)*(27 + 5*t2) + 
                  4*t1*(-3 + t2 + 2*Power(t2,2))) + 
               s2*(4*Power(t1,3) + Power(-1 + t2,2)*(7 + 3*t2) - 
                  3*Power(t1,2)*(25 + 7*t2) - 
                  2*t1*(-11 + 2*t2 + 9*Power(t2,2)))) - 
            (-1 + t2)*(2*Power(s2,3)*t2*(-4 + 7*t2) + 
               Power(t1,3)*(5 - 14*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-5 + 9*t2 + 11*Power(t2,2) - 
                  9*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-11 - 124*t2 + 41*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-169 + 354*t2 - 225*Power(t2,2) + 32*Power(t2,3) + 
                  8*Power(t2,4)) - 
               2*Power(s2,2)*
                (10 - 26*t2 + 21*Power(t2,2) - 8*Power(t2,3) + 
                  t1*(-5 + 4*t2 + 10*Power(t2,2))) + 
               s2*(74 - 171*t2 + 150*Power(t2,2) - 51*Power(t2,3) - 
                  2*Power(t2,4) + 
                  3*Power(t1,2)*(-5 + 10*t2 + Power(t2,2)) + 
                  2*t1*(17 - 34*t2 + 9*Power(t2,2) + 2*Power(t2,3)))) + 
            Power(s1,3)*(Power(t1,4)*(2 - 6*t2) + 
               (-13 + t2)*Power(-1 + t2,3) + Power(s2,4)*(-7 + 3*t2) + 
               Power(s2,3)*(60 + t1*(19 - 3*t2) - 57*t2 - 
                  11*Power(t2,2)) + 
               2*t1*Power(-1 + t2,2)*(13 - 10*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-40 + 43*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-65 + 42*t2 + 21*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s2,2)*(-38 - 9*t2 + 42*Power(t2,2) + 
                  5*Power(t2,3) - 3*Power(t1,2)*(5 + 3*t2) + 
                  t1*(-168 + 173*t2 + 19*Power(t2,2))) + 
               s2*(Power(t1,3)*(1 + 15*t2) + 
                  Power(t1,2)*(148 - 159*t2 - 13*Power(t2,2)) - 
                  Power(-1 + t2,2)*(27 - 18*t2 + Power(t2,2)) - 
                  3*t1*(-37 + 19*t2 + 13*Power(t2,2) + 5*Power(t2,3)))) \
+ s1*(-1 + t2)*(3*Power(t1,4)*(-4 + t2) - 6*Power(s2,4)*t2 + 
               Power(t1,3)*(44 - 6*t2 - 11*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (122 - 30*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(206 - 263*t2 + 55*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(94 - 341*t2 + 279*Power(t2,2) - 29*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,3)*(-26 + 28*t2 - 19*Power(t2,2) + 
                  t1*(8 + 19*t2)) + 
               Power(s2,2)*(120 - 190*t2 + 61*Power(t2,2) + 
                  17*Power(t2,3) - Power(t1,2)*(28 + 17*t2) + 
                  t1*(82 - 26*t2 + 5*Power(t2,2))) + 
               s2*(-113 + 326*t2 - 196*Power(t2,2) - 16*Power(t2,3) - 
                  Power(t2,4) + Power(t1,3)*(32 + t2) + 
                  Power(t1,2)*(-100 + 4*t2 + 25*Power(t2,2)) - 
                  t1*(298 - 401*t2 + 96*Power(t2,2) + 23*Power(t2,3)))) \
- Power(s1,2)*(Power(t1,4)*(11 - 3*t2 - 2*Power(t2,2)) + 
               Power(s2,4)*(6 - 5*t2 + Power(t2,2)) + 
               Power(-1 + t2,3)*(17 - 25*t2 + 4*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (167 - 6*t2 - 8*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(36 - 47*t2 + Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(165 - 312*t2 + 130*Power(t2,2) + 
                  18*Power(t2,3) - Power(t2,4)) + 
               Power(s2,3)*(-53 + 88*t2 - 22*Power(t2,2) - 
                  5*Power(t2,3) + t1*(-25 + 10*t2 + 3*Power(t2,2))) + 
               Power(s2,2)*(182 - 306*t2 + 68*Power(t2,2) + 
                  56*Power(t2,3) + 
                  Power(t1,2)*(43 - 8*t2 - 11*Power(t2,2)) + 
                  t1*(128 - 197*t2 + 35*Power(t2,2) + 10*Power(t2,3))) + 
               s2*(Power(t1,3)*(-35 + 6*t2 + 9*Power(t2,2)) - 
                  Power(-1 + t2,2)*(139 + 5*t2 + 10*Power(t2,2)) - 
                  Power(t1,2)*
                   (111 - 156*t2 + 14*Power(t2,2) + 7*Power(t2,3)) - 
                  t1*(357 - 654*t2 + 246*Power(t2,2) + 46*Power(t2,3) + 
                     5*Power(t2,4))))) + 
         Power(s,2)*(6*Power(s1,5)*(s2 - t1)*(-1 + t2)*
             (-2 + s2 - t1 + 2*t2) - 
            Power(s1,4)*(-1 + t2)*
             (3*Power(s2,3) + 3*Power(t1,3) - 4*Power(-1 + t2,2) + 
               Power(t1,2)*(-58 + 6*t2) - 
               Power(s2,2)*(40 + 3*t1 + 12*t2) + 
               t1*(-29 + 56*t2 - 27*Power(t2,2)) + 
               s2*(17 - 3*Power(t1,2) - 32*t2 + 15*Power(t2,2) + 
                  t1*(98 + 6*t2))) + 
            Power(s1,3)*(Power(s2,4)*(-1 + t2) + 
               4*Power(t1,4)*(-1 + t2) - Power(-1 + t2,4)*(17 + t2) + 
               Power(t1,3)*(34 - 51*t2 + 9*Power(t2,2)) + 
               Power(s2,3)*(-26 - 7*t1*(-1 + t2) + 25*t2 + 
                  9*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(79 - 50*t2 + 25*Power(t2,2)) + 
               Power(t1,2)*(14 + 61*t2 - 78*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s2,2)*(-33 + 15*Power(t1,2)*(-1 + t2) + 132*t2 - 
                  79*Power(t2,2) - 20*Power(t2,3) + 
                  t1*(98 - 125*t2 + 3*Power(t2,2))) + 
               s2*(-13*Power(t1,3)*(-1 + t2) + 
                  Power(t1,2)*(-106 + 151*t2 - 21*Power(t2,2)) + 
                  Power(-1 + t2,2)*(57 - 14*t2 + 11*Power(t2,2)) + 
                  t1*(7 - 157*t2 + 121*Power(t2,2) + 29*Power(t2,3)))) + 
            s1*(-1 + t2)*(-9*Power(t1,4)*(-2 + t2) + 6*Power(s2,4)*t2 + 
               Power(t1,3)*(-39 - 4*t2 + 28*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (187 + 2*t2 - 16*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(242 - 309*t2 + 105*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(-87 + 381*t2 - 357*Power(t2,2) + 69*Power(t2,3) - 
                  6*Power(t2,4)) + 
               Power(s2,3)*(14 - 10*t2 + 9*Power(t2,2) - 
                  3*t1*(4 + 5*t2)) + 
               Power(s2,2)*(-120 + 144*t2 - 11*Power(t2,2) - 
                  61*Power(t2,3) + 3*Power(t1,2)*(14 + t2) + 
                  t1*(-31 - 68*t2 + 58*Power(t2,2))) + 
               s2*(123 - 303*t2 + 107*Power(t2,2) + 53*Power(t2,3) + 
                  20*Power(t2,4) + 3*Power(t1,3)*(-16 + 5*t2) + 
                  Power(t1,2)*(56 + 82*t2 - 95*Power(t2,2)) + 
                  t1*(315 - 380*t2 + 111*Power(t2,2) + 50*Power(t2,3)))) \
+ (-1 + t2)*(6*Power(s2,3)*t2*(-2 + 3*t2) + 
               2*Power(t1,3)*(5 - 13*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(23 - 41*t2 + 55*Power(t2,2) - 
                  31*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-10 - 223*t2 + 85*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(-301 + 602*t2 - 364*Power(t2,2) + 38*Power(t2,3) + 
                  25*Power(t2,4)) - 
               2*Power(s2,2)*
                (12 - 35*t2 + 25*Power(t2,2) - 5*Power(t2,3) + 
                  t1*(-10 + 11*t2 + 8*Power(t2,2))) - 
               2*s2*(3*Power(t1,2)*(5 - 10*t2 + 2*Power(t2,2)) + 
                  t1*(-8 + 17*t2 + 20*Power(t2,2) - 23*Power(t2,3)) + 
                  2*(-31 + 65*t2 - 53*Power(t2,2) + 13*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            Power(s1,2)*(Power(t1,4)*(-3 + 7*t2 - 4*Power(t2,2)) - 
               Power(s2,4)*(2 - 3*t2 + Power(t2,2)) + 
               Power(-1 + t2,3)*(87 - 50*t2 + 23*Power(t2,2)) + 
               Power(t1,3)*(16 - 15*t2 + 9*Power(t2,2) - 2*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (110 + 52*t2 - 5*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(187 - 382*t2 + 198*Power(t2,2) - 
                  4*Power(t2,3) + Power(t2,4)) + 
               Power(s2,3)*(-23 + 22*t2 - 7*Power(t2,3) + 
                  t1*(15 - 28*t2 + 13*Power(t2,2))) + 
               Power(s2,2)*(133 - 27*Power(t1,2)*Power(-1 + t2,2) - 
                  166*t2 - 64*Power(t2,2) + 88*Power(t2,3) + 
                  9*Power(t2,4) + 
                  t1*(46 - 35*t2 + 9*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(Power(t1,3)*(17 - 36*t2 + 19*Power(t2,2)) - 
                  Power(-1 + t2,2)*(43 + 53*t2 + 66*Power(t2,2)) + 
                  Power(t1,2)*
                   (-39 + 28*t2 - 18*Power(t2,2) + 5*Power(t2,3)) - 
                  2*t1*(175 - 326*t2 + 133*Power(t2,2) + 6*Power(t2,3) + 
                     12*Power(t2,4))))))*T3q(1 - s + s1 - t2,s1))/
     ((-1 + s1)*(-s + s1 - t2)*Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*
       (-1 + t2)*Power(-1 + s + t2,2)*(-1 + s2 - t1 + t2)) - 
    (8*(-29 + 4*s2 + 10*Power(s2,2) + 2*Power(s1,7)*Power(s2 - t1,2) + 
         13*t1 - 21*s2*t1 + 2*Power(s2,2)*t1 + 7*Power(t1,2) - 
         3*s2*Power(t1,2) + Power(t1,3) - 
         2*Power(s,4)*Power(-1 + s1,3)*(t1 - t2) + 35*t2 + 19*s2*t2 - 
         12*Power(s2,2)*t2 - 2*Power(s2,3)*t2 - 48*t1*t2 + 30*s2*t1*t2 - 
         Power(s2,2)*t1*t2 - 10*Power(t1,2)*t2 + 6*s2*Power(t1,2)*t2 - 
         3*Power(t1,3)*t2 + 25*Power(t2,2) - 30*s2*Power(t2,2) - 
         6*Power(s2,2)*Power(t2,2) + 4*Power(s2,3)*Power(t2,2) + 
         43*t1*Power(t2,2) - 3*s2*t1*Power(t2,2) - 
         7*Power(s2,2)*t1*Power(t2,2) + 5*Power(t1,2)*Power(t2,2) + 
         3*s2*Power(t1,2)*Power(t2,2) - 39*Power(t2,3) + s2*Power(t2,3) + 
         10*Power(s2,2)*Power(t2,3) - 8*t1*Power(t2,3) - 
         10*s2*t1*Power(t2,3) + 8*Power(t2,4) + 6*s2*Power(t2,4) + 
         Power(s1,6)*(s2 - t1)*
          (Power(s2,2) - Power(t1,2) + 4*(-1 + t2) - 4*s2*(2 + t2) + 
            4*t1*(2 + t2)) + Power(s,3)*Power(-1 + s1,2)*
          (14 + 5*t1 - Power(t1,2) - 3*t2 + 11*t1*t2 - 10*Power(t2,2) - 
            4*s2*(3 + t2) + Power(s1,2)*
             (-Power(t1,2) - 6*t2 + t1*(2 + t2) + s2*(2 + t1 + t2)) + 
            s1*(-14 - 8*Power(t1,2) + s2*(10 + 13*t1 - 11*t2) + 23*t2 + 
               t1*(-21 + 8*t2))) + 
         Power(s1,5)*(Power(s2,4) - 13*Power(t1,3) + 
            Power(s2,3)*(2 - 3*t1 - 3*t2) + 2*Power(-1 + t2,2) + 
            Power(t1,2)*t2*(23 + t2) + 8*t1*(-2 + t2 + Power(t2,2)) + 
            Power(s2,2)*(-3 + 3*Power(t1,2) + 27*t2 + t1*(-19 + 8*t2)) - 
            s2*(Power(t1,3) + 5*Power(t1,2)*(-6 + t2) - 
               t1*(5 - 54*t2 + Power(t2,2)) + 8*(-2 + t2 + Power(t2,2)))) \
+ Power(s1,3)*(13*Power(t1,4) + 
            Power(s2,3)*(24 + t1*(71 - 11*t2) - 22*t2) + 
            Power(s2,4)*(-25 + 6*t2) - 6*Power(t1,3)*(2 + 7*t2) + 
            Power(-1 + t2,2)*(-1 + 28*t2 + Power(t2,2)) + 
            Power(t1,2)*(72 - 43*t2 + 13*Power(t2,2)) + 
            t1*(15 - 75*t2 + 41*Power(t2,2) + 19*Power(t2,3)) + 
            Power(s2,2)*(68 - 77*t2 + 49*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(-54 + 4*t2) + 
               2*t1*(-26 - 5*t2 + 2*Power(t2,2))) + 
            s2*(-28 + Power(t1,3)*(-5 + t2) + 113*t2 - 77*Power(t2,2) - 
               9*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(40 + 74*t2 - 4*Power(t2,2)) - 
               2*t1*(73 - 67*t2 + 36*Power(t2,2)))) - 
         Power(s1,4)*(-5*Power(t1,4) + 
            Power(s2,3)*(29 - 4*t1*(-8 + t2) - 27*t2) + 
            Power(s2,4)*(-6 + t2) - 2*Power(t1,3)*(14 + t2) + 
            3*Power(t1,2)*(6 + t2 + 3*Power(t2,2)) + 
            4*(2 - 3*t2 + Power(t2,3)) + 
            2*t1*(2 - 28*t2 + 25*Power(t2,2) + Power(t2,3)) + 
            Power(s2,2)*(-11 + 35*t2 + 6*Power(t2,2) + 
               Power(t1,2)*(-51 + 5*t2) + t1*(-94 + 60*t2)) + 
            s2*(-3 - 2*Power(t1,3)*(-15 + t2) - 
               31*Power(t1,2)*(-3 + t2) + 57*t2 - 55*Power(t2,2) + 
               Power(t2,3) + t1*
                (-4 - 43*t2 - 14*Power(t2,2) + Power(t2,3)))) + 
         s1*(3*Power(t1,3) + 3*Power(t1,4) + 2*Power(s2,4)*t2 + 
            2*Power(t1,2)*(17 - 35*t2 + 8*Power(t2,2)) + 
            Power(-1 + t2,2)*(13 + 36*t2 + 17*Power(t2,2)) + 
            t1*(-67 + 37*t2 + 71*Power(t2,2) - 41*Power(t2,3)) + 
            Power(s2,3)*(-20 + 11*t2 + 8*Power(t2,2) - t1*(2 + 7*t2)) + 
            Power(s2,2)*(-33 + 18*t2 - 9*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,2)*(7 + 8*t2) + t1*(55 - 32*t2 - 18*Power(t2,2))\
) + s2*(94 - 143*t2 + 53*Power(t2,2) + 3*Power(t2,3) - 7*Power(t2,4) - 
               Power(t1,3)*(8 + 3*t2) + 
               Power(t1,2)*(-38 + 21*t2 + 10*Power(t2,2)) + 
               t1*(-5 + 66*t2 - 23*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(s1,2)*(-5*Power(t1,4) + Power(t1,3)*(40 - 21*t2) + 
            Power(s2,4)*(10 + t2) - 
            5*Power(-1 + t2,2)*(-3 + 9*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(-49 - 29*t2 + 70*Power(t2,2)) + 
            t1*(55 - 38*t2 + 15*Power(t2,2) - 32*Power(t2,3)) + 
            Power(s2,3)*(54 - 43*t2 + 4*Power(t2,2) - 2*t1*(17 + t2)) + 
            Power(s2,2)*(-95 + 131*t2 - 36*Power(t2,2) - 
               8*Power(t2,3) + Power(t1,2)*(33 + t2) + 
               t1*(-79 + 63*t2 + 5*Power(t2,2))) + 
            s2*(-53 - 4*Power(t1,3) + 40*t2 - 25*Power(t2,2) + 
               38*Power(t2,3) + Power(t1,2)*(-15 + t2 - 9*Power(t2,2)) + 
               t1*(151 - 115*t2 - 29*Power(t2,2) + 9*Power(t2,3)))) + 
         Power(s,2)*(-1 + s1)*
          (41 + 6*t1 - 6*Power(t1,2) - Power(t1,3) - 29*t2 + 8*t1*t2 + 
            2*Power(t1,2)*t2 - 8*Power(t2,2) - 13*t1*Power(t2,2) + 
            12*Power(t2,3) - 2*Power(s2,2)*(4 + t1 + 2*t2) + 
            s2*(-28 + 3*Power(t1,2) + 5*t2 + 2*Power(t2,2) + 
               4*t1*(4 + t2)) + 
            Power(s1,4)*(-2 + Power(s2,2) + 3*Power(t1,2) - 
               t1*(-5 + t2) + 7*t2 - Power(t2,2) - 4*s2*(1 + t1 + t2)) + 
            Power(s1,3)*(Power(t1,2)*(21 - 2*t2) - 
               Power(s2,2)*(2*t1 + 3*t2) + 
               2*t1*(11 - 18*t2 + Power(t2,2)) + 
               4*(6 - 10*t2 + Power(t2,2)) + 
               s2*(-14 + 2*Power(t1,2) + 42*t2 + t1*(-23 + 3*t2))) - 
            Power(s1,2)*(35 + 9*Power(t1,3) + 
               Power(s2,2)*(45 + 21*t1 - 23*t2) + 
               Power(t1,2)*(48 - 10*t2) - 33*t2 - 30*Power(t2,2) + 
               t1*(58 - 31*t2 + Power(t2,2)) + 
               s2*(-80 - 30*Power(t1,2) + 91*t2 + Power(t2,2) + 
                  6*t1*(-17 + 5*t2))) + 
            s1*(-28 + 6*Power(t1,3) + 49*t2 - 49*Power(t2,2) - 
               8*Power(t2,3) + 2*Power(t1,2)*(3 + t2) + 
               Power(s2,2)*(52 + 5*t1 + 4*t2) + t1*(5 + 46*t2) - 
               s2*(34 + 11*Power(t1,2) - 8*t2 - 23*Power(t2,2) + 
                  t1*(51 + 25*t2)))) + 
         s*(56 - 18*s2 - 18*Power(s2,2) - 16*t1 + 37*s2*t1 - 
            4*Power(s2,2)*t1 - 12*Power(t1,2) + 6*s2*Power(t1,2) - 
            2*Power(t1,3) - 41*t2 - 8*s2*t2 + 2*Power(s2,2)*t2 + 
            2*Power(s2,3)*t2 + 40*t1*t2 - 13*s2*t1*t2 + 
            Power(s2,2)*t1*t2 + 5*Power(t1,2)*t2 - 6*s2*Power(t1,2)*t2 + 
            3*Power(t1,3)*t2 - 24*Power(t2,2) + 2*s2*Power(t2,2) + 
            8*Power(s2,2)*Power(t2,2) - 12*t1*Power(t2,2) - 
            2*s2*t1*Power(t2,2) - 7*Power(t1,2)*Power(t2,2) + 
            15*Power(t2,3) + 2*s2*Power(t2,3) + 10*t1*Power(t2,3) - 
            6*Power(t2,4) + Power(s1,6)*
             (-2*Power(s2,2) + t1*(-7 - 4*t1 + t2) + 
               3*s2*(1 + 2*t1 + t2)) - 
            Power(s1,5)*(3 + 2*Power(s2,3) + Power(t1,3) + 
               Power(t1,2)*(3 - 7*t2) - 4*t2 + Power(t2,2) - 
               Power(s2,2)*(5 + 5*t1 + 7*t2) + 
               t1*(-4 - 34*t2 + 4*Power(t2,2)) + 
               s2*(-5 + 2*Power(t1,2) + 40*t2 - Power(t2,2) + 
                  4*t1*(1 + 3*t2))) - 
            Power(s1,2)*(27 - 6*Power(t1,4) + 
               5*Power(s2,3)*(15 + t1 - t2) - 157*t2 + 81*Power(t2,2) + 
               49*Power(t2,3) + Power(t1,3)*(25 + 7*t2) - 
               Power(t1,2)*(29 + 102*t2 + 8*Power(t2,2)) + 
               t1*(135 - 90*t2 + 30*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s2,2)*(-1 + 16*Power(t1,2) + 12*t2 - 
                  18*Power(t2,2) + t1*(141 + 7*t2)) + 
               s2*(-188 + 17*Power(t1,3) + 247*t2 - 125*Power(t2,2) - 
                  16*Power(t2,3) + Power(t1,2)*(41 + 5*t2) + 
                  6*t1*(5 + 17*t2))) + 
            Power(s1,4)*(-3 + 27*Power(t1,3) + 
               Power(s2,2)*(43 - 2*Power(t1,2) + t1*(51 - 8*t2) - 
                  64*t2) + 30*t2 - 31*Power(t2,2) + 4*Power(t2,3) + 
               Power(s2,3)*(-7 + t1 + 3*t2) - 
               Power(t1,2)*(-67 + 33*t2 + Power(t2,2)) + 
               t1*(26 - 95*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               s2*(-43 + Power(t1,3) + 114*t2 + 7*Power(t2,2) + 
                  Power(t1,2)*(-71 + 5*t2) + 
                  t1*(-113 + 103*t2 - 2*Power(t2,2)))) + 
            s1*(-61 - 3*Power(t1,4) + t2 + 18*Power(t2,2) + 
               35*Power(t2,3) + 7*Power(t2,4) + Power(t1,3)*(11 + t2) - 
               Power(t1,2)*(8 + 3*t2) - 
               t1*(-93 + 39*t2 + 55*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(2*t1 + 3*(6 + t2)) - 
               Power(s2,2)*(-93 + 7*Power(t1,2) + 49*t2 + 
                  7*Power(t2,2) + t1*(37 + 7*t2)) + 
               s2*(-121 + 8*Power(t1,3) + 166*t2 - 15*Power(t2,2) - 
                  24*Power(t2,3) + Power(t1,2)*(8 + 3*t2) + 
                  t1*(-83 + 30*t2 + 27*Power(t2,2)))) + 
            Power(s1,3)*(38 - 3*Power(t1,4) + 
               Power(s2,3)*(66 + 10*t1 - 21*t2) + 
               3*Power(t1,3)*(-6 + t2) - 143*t2 + 103*Power(t2,2) + 
               3*Power(t2,3) - Power(t2,4) - Power(t1,2)*(85 + 54*t2) + 
               t1*(27 + t2 + 87*Power(t2,2) + Power(t2,3)) + 
               Power(s2,2)*(-120 - 23*Power(t1,2) + 116*t2 + 
                  Power(t2,2) + 3*t1*(-60 + 13*t2)) + 
               s2*(16*Power(t1,3) - 3*Power(t1,2)*(-44 + 7*t2) + 
                  t1*(211 - 70*t2 + Power(t2,2)) - 
                  2*(7 + 6*t2 + 44*Power(t2,2) + Power(t2,3))))))*T4q(s1))/
     (Power(-1 + s1,2)*(-s + s1 - t2)*
       Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*(-1 + t2)*
       (-1 + s2 - t1 + t2)) + (8*
       (11 - 13*s2 - 8*Power(s2,2) + 2*Power(s2,3) + 20*t1 + 4*s2*t1 - 
         11*Power(s2,2)*t1 + 2*Power(s2,3)*t1 + 2*Power(t1,2) + 
         17*s2*Power(t1,2) - 5*Power(s2,2)*Power(t1,2) - 8*Power(t1,3) + 
         4*s2*Power(t1,3) - Power(t1,4) - 48*t2 + 16*s2*t2 + 
         39*Power(s2,2)*t2 - 2*Power(s2,4)*t2 - 34*t1*t2 - 56*s2*t1*t2 + 
         15*Power(s2,2)*t1*t2 + Power(s2,3)*t1*t2 + 21*Power(t1,2)*t2 - 
         29*s2*Power(t1,2)*t2 + 7*Power(s2,2)*Power(t1,2)*t2 + 
         14*Power(t1,3)*t2 - 9*s2*Power(t1,3)*t2 + 3*Power(t1,4)*t2 + 
         82*Power(t2,2) + 6*s2*Power(t2,2) - 36*Power(s2,2)*Power(t2,2) - 
         8*Power(s2,3)*Power(t2,2) + 4*Power(s2,4)*Power(t2,2) + 
         4*t1*Power(t2,2) + 72*s2*t1*Power(t2,2) + 
         3*Power(s2,2)*t1*Power(t2,2) - 11*Power(s2,3)*t1*Power(t2,2) - 
         36*Power(t1,2)*Power(t2,2) + 13*s2*Power(t1,2)*Power(t2,2) + 
         10*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         8*Power(t1,3)*Power(t2,2) - 3*s2*Power(t1,3)*Power(t2,2) - 
         68*Power(t2,3) - 6*s2*Power(t2,3) - Power(s2,2)*Power(t2,3) + 
         8*Power(s2,3)*Power(t2,3) + 14*t1*Power(t2,3) - 
         16*s2*t1*Power(t2,3) - 13*Power(s2,2)*t1*Power(t2,3) + 
         13*Power(t1,2)*Power(t2,3) + 5*s2*Power(t1,2)*Power(t2,3) + 
         27*Power(t2,4) - 5*s2*Power(t2,4) + 6*Power(s2,2)*Power(t2,4) - 
         4*t1*Power(t2,4) - 4*s2*t1*Power(t2,4) - 4*Power(t2,5) + 
         2*s2*Power(t2,5) + 2*Power(s1,5)*Power(s2 - t1,2)*
          (-1 + s2 - t1 + t2) + 
         2*Power(s,4)*(-1 + s1)*(t1 - t2)*(-2 + s1 + s2 - t1 + t2) + 
         Power(s1,4)*(s2 - t1)*
          (Power(s2,3) + Power(t1,3) + 4*Power(-1 + t2,2) + 
            4*t1*(-1 + t2)*t2 - Power(s2,2)*(9 + t1 + 3*t2) - 
            Power(t1,2)*(7 + 5*t2) + 
            s2*(-Power(t1,2) - 4*(-1 + t2)*t2 + 8*t1*(2 + t2))) + 
         Power(s,3)*(-12 + 2*Power(s1,3)*(2 + s2 - 3*t1) + t1 - 
            2*Power(t1,2) + Power(t1,3) + 2*Power(s2,2)*(t1 - t2) + 
            13*t2 + 17*t1*t2 - 15*Power(t2,2) - 3*t1*Power(t2,2) + 
            2*Power(t2,3) + s2*
             (6 + t1 - 3*Power(t1,2) - 9*t2 + 3*t1*t2) - 
            s1*(-26 + Power(s2,2)*(2 + 5*t1 - 3*t2) + 7*t2 - 
               13*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,2)*(13 + 4*t2) + t1*(29 - 8*Power(t2,2)) + 
               s2*(6 - 5*Power(t1,2) + 4*t1*(-5 + t2) + Power(t2,2))) + 
            Power(s1,2)*(Power(t1,3) + Power(t1,2)*(15 - 2*t2) + 
               Power(s2,2)*(2 + t1 + t2) + 
               t1*(32 - 17*t2 + Power(t2,2)) + 
               2*(-9 - 2*t2 + Power(t2,2)) + 
               s2*(-2 - 2*Power(t1,2) + t1*(-17 + t2) + 5*t2 + 
                  Power(t2,2)))) + 
         Power(s1,3)*(Power(s2,5) + 3*Power(t1,4) + 
            Power(s2,4)*(3 - 4*t1 - 2*t2) + 2*Power(-1 + t2,3) + 
            2*t1*Power(-1 + t2,2)*(3 + 4*t2) - 
            Power(t1,3)*(-1 + 14*t2 + Power(t2,2)) + 
            Power(t1,2)*(30 - 29*t2 - 2*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(-9 + 6*Power(t1,2) + 26*t2 - 3*Power(t2,2) + 
               2*t1*(-7 + 4*t2)) + 
            Power(s2,2)*(-4*Power(t1,3) + Power(t1,2)*(22 - 10*t2) + 
               3*(11 - 12*t2 + Power(t2,2)) + 
               t1*(23 - 74*t2 + 9*Power(t2,2))) + 
            s2*(Power(t1,4) + 2*Power(t1,3)*(-7 + 2*t2) - 
               2*Power(-1 + t2,2)*(3 + 4*t2) + 
               Power(t1,2)*(-15 + 62*t2 - 5*Power(t2,2)) + 
               t1*(-65 + 71*t2 - 7*Power(t2,2) + Power(t2,3)))) + 
         s1*(-3*Power(t1,5) + 2*Power(s2,5)*t2 + 
            Power(t1,4)*(-8 + 9*t2) + 
            Power(t1,3)*(3 + 33*t2 - 20*Power(t2,2)) + 
            Power(-1 + t2,3)*(-15 + 12*t2 + Power(t2,2)) + 
            2*t1*Power(-1 + t2,2)*(16 - 30*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(41 - 47*t2 - Power(t2,2) + 7*Power(t2,3)) + 
            Power(s2,4)*(-4 + t2 + 6*Power(t2,2) - t1*(2 + 9*t2)) + 
            Power(s2,3)*(17 - 51*t2 + 12*Power(t2,2) + 6*Power(t2,3) + 
               3*Power(t1,2)*(3 + 5*t2) + 
               t1*(21 - 16*t2 - 15*Power(t2,2))) - 
            Power(s2,2)*(-29 + 24*t2 + 8*Power(t2,2) - 3*Power(t2,4) + 
               Power(t1,3)*(15 + 11*t2) - 
               2*Power(t1,2)*(-19 + 19*t2 + 6*Power(t2,2)) + 
               t1*(39 - 155*t2 + 60*Power(t2,2) + 8*Power(t2,3))) + 
            s2*(Power(t1,4)*(11 + 3*t2) + 
               Power(t1,3)*(29 - 32*t2 - 3*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-29 + 51*t2 - Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(19 - 137*t2 + 68*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-64 + 53*t2 + 27*Power(t2,2) - 13*Power(t2,3) - 
                  3*Power(t2,4)))) - 
         Power(s1,2)*(Power(t1,5) + Power(t1,4)*(4 - 7*t2) + 
            Power(s2,5)*(-2 + t2) + 4*Power(-1 + t2,3)*(1 + t2) + 
            Power(s2,4)*(10 - 5*t1*(-2 + t2) - 12*t2 + Power(t2,2)) + 
            2*t1*Power(-1 + t2,2)*(-20 + 8*t2 + Power(t2,2)) + 
            Power(t1,3)*(-23 + 14*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(-26 + 85*t2 - 60*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(19 - 10*t2 - 3*Power(t2,2) + 
               Power(t1,2)*(-19 + 9*t2) + 
               t1*(-39 + 49*t2 - 4*Power(t2,2))) + 
            Power(s2,2)*(-26 + Power(t1,3)*(17 - 7*t2) + 87*t2 - 
               64*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,2)*(52 - 69*t2 + 5*Power(t2,2)) + 
               t1*(-58 + 29*t2 + 10*Power(t2,2) + Power(t2,3))) + 
            s2*(Power(t1,4)*(-7 + 2*t2) + 
               Power(t1,3)*(-27 + 39*t2 - 2*Power(t2,2)) + 
               Power(-1 + t2,2)*(39 - 18*t2 + Power(t2,2)) - 
               Power(t1,2)*(-62 + 33*t2 + 10*Power(t2,2) + Power(t2,3)) + 
               t1*(53 - 176*t2 + 130*Power(t2,2) - 8*Power(t2,3) + 
                  Power(t2,4)))) - 
         Power(s,2)*(-19 + 4*Power(s1,4)*(s2 - t1) + 7*t1 - 
            6*Power(t1,2) + 7*Power(t1,3) + Power(t1,4) - 
            2*Power(s2,3)*(t1 - t2) + 60*t2 + 19*t1*t2 - 
            20*Power(t1,2)*t2 - 3*Power(t1,3)*t2 - 59*Power(t2,2) - 
            5*t1*Power(t2,2) + 3*Power(t1,2)*Power(t2,2) + 
            18*Power(t2,3) - t1*Power(t2,3) + 
            Power(s2,2)*(6 + 5*Power(t1,2) + t1*(8 - 12*t2) - 13*t2 + 
               4*Power(t2,2)) + 
            s2*(9 - 4*Power(t1,3) - 42*t2 + t1*(37 - 11*t2)*t2 + 
               11*Power(t2,2) + 2*Power(t2,3) + Power(t1,2)*(-15 + 13*t2)\
) + Power(s1,3)*(-6 - Power(s2,3) + 3*Power(t1,3) + t1*(23 - 11*t2) - 
               4*Power(t1,2)*(-7 + t2) + 5*t2 + Power(t2,3) + 
               Power(s2,2)*(9 + 5*t1 + 3*t2) + 
               s2*(-10 - 7*Power(t1,2) + t1*(-37 + t2) - 7*t2 + 
                  5*Power(t2,2))) + 
            Power(s1,2)*(14 - 15*t2 + 8*Power(t2,2) - 7*Power(t2,3) - 
               2*Power(t1,3)*(3 + t2) + Power(s2,3)*(5 + 2*t1 + 3*t2) + 
               Power(t1,2)*(-27 - 25*t2 + 4*Power(t2,2)) + 
               t1*(35 - 63*t2 + 38*Power(t2,2) - 2*Power(t2,3)) - 
               Power(s2,2)*(-3 + 4*Power(t1,2) + 10*t2 - 
                  3*Power(t2,2) + 4*t1*(6 + t2)) + 
               s2*(-22 + 2*Power(t1,3) + 27*t2 - 13*Power(t2,2) + 
                  Power(t1,2)*(25 + 3*t2) + 
                  t1*(30 + 27*t2 - 5*Power(t2,2)))) + 
            s1*(9 - 5*Power(t1,4) - 43*t2 + 43*Power(t2,2) - 
               9*Power(t2,3) - Power(s2,3)*(2 + t1 + 2*t2) + 
               Power(t1,3)*(-5 + 17*t2) + 
               Power(t1,2)*(1 + 54*t2 - 19*Power(t2,2)) + 
               t1*(-70 + 67*t2 - 40*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(-24 - 3*Power(t1,2) + 21*t2 - 
                  13*Power(t2,2) + 4*t1*(1 + 6*t2)) + 
               s2*(25 + 9*Power(t1,3) + Power(t1,2)*(3 - 39*t2) + 11*t2 + 
                  11*Power(t2,2) - 11*Power(t2,3) + 
                  t1*(24 - 85*t2 + 41*Power(t2,2))))) - 
         s*(18 - 14*s2 - 16*Power(s2,2) + 2*Power(s2,3) + 16*t1 + 
            7*s2*t1 - 17*Power(s2,2)*t1 + 4*Power(s2,3)*t1 + 
            6*Power(t1,2) + 29*s2*Power(t1,2) - 
            10*Power(s2,2)*Power(t1,2) - 14*Power(t1,3) + 
            8*s2*Power(t1,3) - 2*Power(t1,4) - 81*t2 + 33*s2*t2 + 
            42*Power(s2,2)*t2 + 4*Power(s2,3)*t2 - 2*Power(s2,4)*t2 - 
            25*t1*t2 - 70*s2*t1*t2 + 8*Power(s2,2)*t1*t2 + 
            Power(s2,3)*t1*t2 + 29*Power(t1,2)*t2 - 22*s2*Power(t1,2)*t2 + 
            7*Power(s2,2)*Power(t1,2)*t2 + 10*Power(t1,3)*t2 - 
            9*s2*Power(t1,3)*t2 + 3*Power(t1,4)*t2 + 125*Power(t2,2) - 
            36*s2*Power(t2,2) - 12*Power(s2,2)*Power(t2,2) + 
            2*Power(s2,3)*Power(t2,2) + 12*t1*Power(t2,2) + 
            45*s2*t1*Power(t2,2) - 21*Power(s2,2)*t1*Power(t2,2) - 
            26*Power(t1,2)*Power(t2,2) + 29*s2*Power(t1,2)*Power(t2,2) - 
            10*Power(t1,3)*Power(t2,2) - 77*Power(t2,3) + 
            11*s2*Power(t2,3) + 8*Power(s2,2)*Power(t2,3) + 
            5*t1*Power(t2,3) - 26*s2*t1*Power(t2,3) + 
            13*Power(t1,2)*Power(t2,3) + 13*Power(t2,4) + 
            6*s2*Power(t2,4) - 8*t1*Power(t2,4) + 2*Power(t2,5) + 
            Power(s1,4)*(2*Power(s2,3) - Power(s2,2)*(13 + 8*t1 + t2) + 
               s2*(3 + 10*Power(t1,2) - 4*t1*(-8 + t2) - 3*Power(t2,2)) - 
               t1*(7 + 4*Power(t1,2) + t1*(19 - 5*t2) - 8*t2 + 
                  Power(t2,2))) + 
            Power(s1,3)*(2*Power(s2,4) - Power(t1,4) + 
               (-3 + t2)*Power(-1 + t2,2) + 2*Power(t1,3)*(5 + 4*t2) - 
               Power(s2,3)*(5 + 7*t1 + 5*t2) + 
               Power(t1,2)*(23 + 30*t2 - 11*Power(t2,2)) + 
               t1*(-29 + 50*t2 - 25*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(10 + 7*Power(t1,2) + 40*t2 - 8*Power(t2,2) + 
                  2*t1*(12 + 7*t2)) - 
               s2*(-30 + Power(t1,3) + 49*t2 - 20*Power(t2,2) + 
                  Power(t2,3) + Power(t1,2)*(29 + 17*t2) + 
                  t1*(35 + 66*t2 - 17*Power(t2,2)))) - 
            Power(s1,2)*(-5*Power(t1,4) + Power(s2,4)*(1 + t1 + 3*t2) - 
               Power(s2,3)*(-22 + t1 + 3*Power(t1,2) + 22*t2 + 
                  10*t1*t2 - 3*Power(t2,2)) + 
               Power(t1,3)*(-24 + 32*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*(17 - 13*t2 + 4*Power(t2,2)) - 
               2*Power(t1,2)*(20 - 41*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               t1*(-36 + 7*t2 + 43*Power(t2,2) - 15*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(-9 + 3*Power(t1,3) + 42*t2 - 9*Power(t2,2) + 
                  Power(t1,2)*(-6 + 11*t2) + 
                  t1*(-73 + 86*t2 - 10*Power(t2,2))) + 
               s2*(23 - Power(t1,4) + Power(t1,3)*(11 - 4*t2) + 11*t2 - 
                  39*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t1,2)*(75 - 96*t2 + 6*Power(t2,2)) - 
                  t1*(-56 + 141*t2 - 38*Power(t2,2) + Power(t2,3)))) + 
            s1*(-3*Power(t1,5) + 2*Power(t1,4)*(2 + 5*t2) + 
               Power(s2,4)*(-2 - 2*t1 + 5*t2) + 
               Power(t1,3)*(23 - 5*t2 - 13*Power(t2,2)) + 
               Power(s2,3)*(17 + 9*Power(t1,2) + t1*(9 - 34*t2) - 18*t2 + 
                  16*Power(t2,2)) + 
               Power(-1 + t2,2)*(11 - 2*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(19 - 108*t2 + 46*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(32 - 181*t2 + 194*Power(t2,2) - 41*Power(t2,3) - 
                  4*Power(t2,4)) + 
               Power(s2,2)*(34 - 15*Power(t1,3) - 94*t2 + 
                  13*Power(t2,2) + 13*Power(t2,3) + 
                  Power(t1,2)*(-8 + 63*t2) + 
                  t1*(-29 + 55*t2 - 51*Power(t2,2))) + 
               s2*(-22 + 11*Power(t1,4) + 128*t2 - 119*Power(t2,2) + 
                  10*Power(t2,3) + 3*Power(t2,4) - 
                  Power(t1,3)*(3 + 44*t2) + 
                  Power(t1,2)*(-11 - 32*t2 + 48*Power(t2,2)) - 
                  t1*(43 - 186*t2 + 57*Power(t2,2) + 18*Power(t2,3))))))*
       T5q(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-s + s1 - t2)*Power(1 - s + s*s1 - s1*s2 + s1*t1 - t2,2)*
       (-1 + t2)*(-1 + s2 - t1 + t2)));
   return a;
};
