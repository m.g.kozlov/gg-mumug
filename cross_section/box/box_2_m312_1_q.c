#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m312_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(7 - 17*s2 + 6*Power(s2,2) + 27*Power(s2,3) + 5*Power(s2,4) + 
         4*Power(s2,5) - 8*Power(s1,4)*(-1 + s2)*s2*(1 + s2 - t1)*
          (-1 + t1) - 23*t1 + 52*s2*t1 - 35*Power(s2,2)*t1 - 
         59*Power(s2,3)*t1 - 23*Power(s2,4)*t1 - 8*Power(s2,5)*t1 + 
         24*Power(t1,2) - 64*s2*Power(t1,2) + 42*Power(s2,2)*Power(t1,2) + 
         51*Power(s2,3)*Power(t1,2) + 31*Power(s2,4)*Power(t1,2) + 
         4*Power(s2,5)*Power(t1,2) - 4*Power(t1,3) + 50*s2*Power(t1,3) - 
         8*Power(s2,2)*Power(t1,3) - 33*Power(s2,3)*Power(t1,3) - 
         13*Power(s2,4)*Power(t1,3) - 7*Power(t1,4) - 31*s2*Power(t1,4) + 
         14*Power(s2,3)*Power(t1,4) + 3*Power(t1,5) + 10*s2*Power(t1,5) - 
         5*Power(s2,2)*Power(t1,5) + 6*t2 + 12*s2*t2 - 14*Power(s2,2)*t2 + 
         7*Power(s2,3)*t2 + 20*Power(s2,4)*t2 + Power(s2,5)*t2 - 
         14*t1*t2 - 20*s2*t1*t2 + 55*Power(s2,2)*t1*t2 - 
         25*Power(s2,3)*t1*t2 - 42*Power(s2,4)*t1*t2 - 
         2*Power(s2,5)*t1*t2 + 8*Power(t1,2)*t2 - 10*s2*Power(t1,2)*t2 - 
         85*Power(s2,2)*Power(t1,2)*t2 + 30*Power(s2,3)*Power(t1,2)*t2 + 
         24*Power(s2,4)*Power(t1,2)*t2 + Power(s2,5)*Power(t1,2)*t2 + 
         2*Power(t1,3)*t2 + 40*s2*Power(t1,3)*t2 + 
         61*Power(s2,2)*Power(t1,3)*t2 - 13*Power(s2,3)*Power(t1,3)*t2 - 
         2*Power(s2,4)*Power(t1,3)*t2 - 2*Power(t1,4)*t2 - 
         30*s2*Power(t1,4)*t2 - 17*Power(s2,2)*Power(t1,4)*t2 + 
         Power(s2,3)*Power(t1,4)*t2 + 8*s2*Power(t1,5)*t2 - Power(t2,2) + 
         42*s2*Power(t2,2) - 29*Power(s2,2)*Power(t2,2) - 
         54*Power(s2,3)*Power(t2,2) + 18*Power(s2,4)*Power(t2,2) + 
         8*Power(s2,5)*Power(t2,2) + t1*Power(t2,2) - 
         61*s2*t1*Power(t2,2) + 114*Power(s2,2)*t1*Power(t2,2) + 
         42*Power(s2,3)*t1*Power(t2,2) - 32*Power(s2,4)*t1*Power(t2,2) - 
         8*Power(s2,5)*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) - 
         8*s2*Power(t1,2)*Power(t2,2) - 
         97*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         18*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         14*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         Power(t1,3)*Power(t2,2) + 35*s2*Power(t1,3)*Power(t2,2) + 
         12*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         6*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         8*s2*Power(t1,4)*Power(t2,2) - 23*s2*Power(t2,3) + 
         3*Power(s2,2)*Power(t2,3) + 15*Power(s2,3)*Power(t2,3) - 
         Power(s2,4)*Power(t2,3) + 6*Power(s2,5)*Power(t2,3) + 
         50*s2*t1*Power(t2,3) - 25*Power(s2,2)*t1*Power(t2,3) - 
         20*Power(s2,3)*t1*Power(t2,3) - 5*Power(s2,4)*t1*Power(t2,3) - 
         27*s2*Power(t1,2)*Power(t2,3) + 
         28*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         4*Power(s,5)*s2*(1 + 4*t1 - 3*t2 + s2*(-1 - 2*t1 + t2) + 
            2*s1*(-1 + s2 - t1 + t2)) + 
         Power(s1,3)*s2*(-6*Power(s2,4) + Power(s2,3)*(1 + 5*t1) + 
            (-1 + t1)*(-4 + 5*t1 + 3*Power(t1,2) - 24*t2 + 24*t1*t2) + 
            s2*(-11 + 5*Power(t1,3) + 6*t1*(5 + 4*t2) - 
               6*Power(t1,2)*(5 + 4*t2)) - 
            2*Power(s2,2)*(2 + 2*Power(t1,2) + 12*t2 - t1*(7 + 12*t2))) + 
         Power(s,4)*(Power(s2,3)*(18 + 13*t1 - 5*t2) + 
            8*Power(s1,2)*s2*(-1 + t1 - t2) - 2*(2 + t1)*(-1 + t1 - t2) + 
            2*s2*(-13 + 7*t1 + 26*Power(t1,2) - 4*t2 - 12*t1*t2 - 
               6*Power(t2,2)) - 
            Power(s2,2)*(4 + 57*t1 + 18*Power(t1,2) - 41*t2 + 2*t1*t2 - 
               4*Power(t2,2)) - 
            s1*(26*Power(s2,3) + 2*(2 + t1) + 
               Power(s2,2)*(8 - 73*t1 + 29*t2) + 
               s2*(-54 + 47*t1 + 32*Power(t1,2) - 5*t2 - 24*t1*t2 - 
                  8*Power(t2,2)))) - 
         Power(s1,2)*(Power(-1 + t1,2)*(2 - t1 + Power(t1,2)) + 
            2*Power(s2,5)*(-4 + 4*t1 - 9*t2) + 
            Power(s2,4)*(-5 - Power(t1,2) + 3*t2 + 3*t1*(2 + 5*t2)) - 
            s2*(-1 + t1)*(-70 + 8*Power(t1,4) + 35*t2 + 24*Power(t2,2) - 
               4*Power(t1,3)*(1 + 2*t2) + Power(t1,2)*(-33 + 14*t2) + 
               t1*(95 - 53*t2 - 24*Power(t2,2))) + 
            Power(s2,3)*(42 - 26*Power(t1,3) - 11*t2 - 24*Power(t2,2) + 
               Power(t1,2)*(34 + 5*t2) + 
               t1*(-50 + 24*t2 + 24*Power(t2,2))) + 
            Power(s2,2)*(-25 + 27*Power(t1,4) - 9*t2 - 
               10*Power(t1,3)*(4 + t2) - 
               2*Power(t1,2)*(-9 + 16*t2 + 12*Power(t2,2)) + 
               t1*(20 + 33*t2 + 24*Power(t2,2)))) - 
         s1*(Power(-1 + t1,2)*
             (5 + t1 + Power(t1,3) - 3*t2 - Power(t1,2)*(5 + t2)) + 
            Power(s2,5)*(5 + 5*Power(t1,2) + 16*t2 + 18*Power(t2,2) - 
               2*t1*(5 + 8*t2)) + 
            Power(s2,4)*(19 - 13*Power(t1,3) + 27*t2 - 3*Power(t2,2) + 
               Power(t1,2)*(45 + 19*t2) - 
               t1*(51 + 46*t2 + 15*Power(t2,2))) + 
            s2*(-1 + t1)*(-16 - 103*t2 + 54*Power(t2,2) + 
               8*Power(t2,3) + 2*Power(t1,4)*(9 + 4*t2) - 
               Power(t1,3)*(12 + 25*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-36 + 29*t2 + 17*Power(t2,2)) + 
               t1*(46 + 83*t2 - 75*Power(t2,2) - 8*Power(t2,3))) + 
            Power(s2,2)*(12 - 3*Power(t1,5) - 16*t2 + Power(t2,2) - 
               2*Power(t1,4)*(19 + 11*t2) + 
               Power(t1,3)*(80 + 53*t2 + 15*Power(t2,2)) + 
               2*Power(t1,2)*
                (-11 - 72*t2 + 15*Power(t2,2) + 4*Power(t2,3)) - 
               t1*(29 - 129*t2 + 28*Power(t2,2) + 8*Power(t2,3))) + 
            Power(s2,3)*(23 + 11*Power(t1,4) - 95*t2 + 22*Power(t2,2) + 
               8*Power(t2,3) + Power(t1,3)*(-15 + 11*t2) + 
               Power(t1,2)*(20 + 3*t2 - 10*Power(t2,2)) - 
               t1*(39 - 81*t2 + 30*Power(t2,2) + 8*Power(t2,3)))) - 
         Power(s,3)*(18 + 8*Power(s1,3)*s2*(-3 + 2*s2) - 21*t1 - 
            2*Power(t1,2) + 5*Power(t1,3) + 17*t2 - 3*t1*t2 - 
            4*Power(t1,2)*t2 - Power(t2,2) - t1*Power(t2,2) + 
            Power(s2,3)*(33 - 19*Power(t1,2) + 3*t1*(-27 + t2) + 16*t2 - 
               2*Power(t2,2)) + Power(s2,4)*(5*t1 + 2*(9 + t2)) + 
            Power(s1,2)*(-2 - 19*Power(s2,3) + 
               Power(s2,2)*(-37 + 50*t1 - 53*t2) + 
               s2*(58 + 14*t1 - 32*Power(t1,2) + 21*t2 + 32*t1*t2)) + 
            Power(s2,2)*(-13 + 11*Power(t1,3) + 107*t2 - 
               62*Power(t2,2) - Power(t2,3) + 
               Power(t1,2)*(149 + 19*t2) - 
               t1*(71 + 81*t2 + 5*Power(t2,2))) + 
            s2*(-72 - 64*Power(t1,3) + Power(t1,2)*(-40 + t2) - 86*t2 + 
               25*Power(t2,2) + Power(t2,3) + 
               t1*(144 + 33*t2 + 38*Power(t2,2))) + 
            s1*(-16 - 25*Power(s2,4) + t1 + 5*Power(t1,2) + 
               Power(s2,3)*(-43 + 112*t1) + 3*t2 + t1*t2 + 
               Power(s2,2)*(5 - 159*Power(t1,2) + 44*t2 + 
                  38*Power(t2,2) + t1*(8 + 35*t2)) + 
               s2*(103 + 48*Power(t1,3) + Power(t1,2)*(98 - 16*t2) - 
                  71*t2 + 2*Power(t2,2) - 
                  t1*(161 + 20*t2 + 32*Power(t2,2))))) + 
         Power(s,2)*(31 + 8*Power(s1,4)*(-1 + s2)*s2 - 57*t1 + 
            19*Power(t1,2) + 11*Power(t1,3) - 4*Power(t1,4) + 28*t2 - 
            24*t1*t2 - 4*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - 
            3*Power(t2,2) - t1*Power(t2,2) + 2*Power(t1,2)*Power(t2,2) + 
            Power(s2,5)*(4 + 3*t2) - 
            Power(s2,4)*(-35 + 4*Power(t1,2) - 13*t1*(-3 + t2) + 21*t2 + 
               16*Power(t2,2)) + 
            Power(s1,3)*s2*(-34 + 11*Power(s2,2) + 51*t1 + 24*t2 - 
               3*s2*(3 + 9*t1 + 8*t2)) + 
            Power(s2,3)*(32 + 4*Power(t1,3) + 74*t2 - 38*Power(t2,2) + 
               4*Power(t2,3) + Power(t1,2)*(117 + 5*t2) - 
               t1*(117 + 4*t2 + 7*Power(t2,2))) + 
            s2*(-90 + 36*Power(t1,4) - 87*t2 + 110*Power(t2,2) - 
               31*Power(t2,3) + 5*Power(t1,3)*(11 + 6*t2) - 
               2*Power(t1,2)*(107 + 29*t2 + 24*Power(t2,2)) + 
               t1*(213 + 117*t2 - 16*Power(t2,2) - 2*Power(t2,3))) + 
            Power(s2,2)*(12 + 83*t2 - 125*Power(t2,2) + 27*Power(t2,3) - 
               2*Power(t1,3)*(71 + 8*t2) + 
               Power(t1,2)*(130 + 9*t2 - 2*Power(t2,2)) + 
               2*t1*(-16 - 63*t2 + 72*Power(t2,2) + Power(t2,3))) - 
            Power(s1,2)*(6 + 29*Power(s2,4) - 5*t1 + Power(t1,2) + 
               Power(s2,3)*(52 - 75*t1 + 30*t2) + 
               s2*(-192 - 48*Power(t1,3) - t2 + 24*Power(t2,2) + 
                  16*Power(t1,2)*(1 + 3*t2) + 2*t1*(89 + 10*t2)) + 
               Power(s2,2)*(17 + 127*Power(t1,2) + 11*t2 - 
                  24*Power(t2,2) - 2*t1*(69 + 58*t2))) - 
            s1*(25 + 7*Power(s2,5) + 5*Power(t1,3) + 
               Power(s2,4)*(24 - 44*t1 - 41*t2) + 
               Power(t1,2)*(-13 + t2) - 9*t2 + t1*(-15 + 4*t2) + 
               Power(s2,3)*(55 + 148*Power(t1,2) - 47*t2 - 
                  15*Power(t2,2) + t1*(-84 + 17*t2)) + 
               s2*(-59 + 32*Power(t1,4) + 256*t2 - 64*Power(t2,2) - 
                  8*Power(t2,3) + Power(t1,3)*(105 + 16*t2) - 
                  2*Power(t1,2)*(84 + 29*t2 + 24*Power(t2,2)) + 
                  t1*(92 - 122*t2 + 29*Power(t2,2))) + 
               Power(s2,2)*(28 - 134*Power(t1,3) - 119*t2 + 
                  7*Power(t2,2) + 8*Power(t2,3) - 
                  3*Power(t1,2)*(21 + 13*t2) + 
                  t1*(83 + 173*t2 + 91*Power(t2,2))))) + 
         s*(-8*Power(s1,4)*(-1 + s2)*s2*(2 + s2 - 2*t1) + 
            Power(s2,5)*(-8 - 8*t1*(-1 + t2) + 8*t2 + 10*Power(t2,2)) + 
            Power(s1,3)*s2*(6 + 11*Power(s2,3) - 32*t1 + 30*Power(t1,2) - 
               48*t2 + 48*t1*t2 + Power(s2,2)*(-1 + 7*t1 + 24*t2) - 
               3*s2*(-8 + 13*t1 + 2*Power(t1,2) - 8*t2 + 16*t1*t2)) - 
            (-1 + t1)*(-9*Power(t1,3) + Power(t1,4) + 
               3*(-8 - 7*t2 + Power(t2,2)) - 
               Power(t1,2)*(5 - 5*t2 + Power(t2,2)) + 
               t1*(37 + 12*t2 + 2*Power(t2,2))) + 
            Power(s2,4)*(-10 + Power(t1,3) + 7*Power(t1,2)*(-4 + t2) + 
               9*t2 - 20*Power(t2,2) - 11*Power(t2,3) + 
               t1*(37 - 16*t2 + 10*Power(t2,2))) + 
            Power(s2,3)*(-44 - 2*Power(t1,4) - 72*t2 + 72*Power(t2,2) - 
               25*Power(t2,3) + Power(t1,3)*(63 + 4*t2) + 
               Power(t1,2)*(-92 + 4*t2 - 15*Power(t2,2)) + 
               t1*(75 + 64*t2 - 15*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s2,2)*(-35 + Power(t1,5) - 13*t2 + 106*Power(t2,2) - 
               19*Power(t2,3) - 3*Power(t1,4)*(17 + t2) - 
               3*Power(t1,3)*(-18 + 16*t2 + Power(t2,2)) + 
               Power(t1,2)*(-58 + 50*t2 + 94*Power(t2,2) + Power(t2,3)) + 
               t1*(89 + 14*t2 - 235*Power(t2,2) + 55*Power(t2,3))) + 
            s2*(57 + 8*Power(t1,5) + 9*t2 - 115*Power(t2,2) + 
               55*Power(t2,3) + Power(t1,4)*(35 + 27*t2) - 
               Power(t1,3)*(122 + 63*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(164 + 66*t2 + 44*Power(t2,2) - Power(t2,3)) - 
               t1*(142 + 39*t2 - 97*Power(t2,2) + 58*Power(t2,3))) + 
            Power(s1,2)*(10*Power(s2,5) - 
               2*(-3 + 5*t1 - 3*Power(t1,2) + Power(t1,3)) + 
               Power(s2,4)*(6 - 16*t1 - 33*t2) + 
               Power(s2,3)*(57 + 82*Power(t1,2) + t2 - 24*Power(t2,2) - 
                  t1*(97 + 35*t2)) + 
               s2*(-196 + 32*Power(t1,4) + 63*t2 + 48*Power(t2,2) - 
                  2*Power(t1,3)*(11 + 16*t2) + 
                  Power(t1,2)*(-149 + 23*t2) + 
                  t1*(331 - 66*t2 - 48*Power(t2,2))) + 
               Power(s2,2)*(-104*Power(t1,3) + 
                  Power(t1,2)*(141 + 73*t2) - 
                  3*(9 + 5*t2 + 8*Power(t2,2)) + 
                  3*t1*(-16 + 7*t2 + 16*Power(t2,2)))) + 
            s1*(-20*Power(s2,5)*t2 + 
               Power(s2,4)*(12 + 38*Power(t1,2) + 22*t2 + 
                  33*Power(t2,2) - 2*t1*(25 + t2)) - 
               (-1 + t1)*(3*Power(t1,3) - 9*(-2 + t2) - 
                  Power(t1,2)*(14 + t2) + t1*(-3 + 2*t2)) + 
               Power(s2,3)*(73 - 73*Power(t1,3) + 
                  Power(t1,2)*(47 - 28*t2) - 106*t2 + 25*Power(t2,2) + 
                  8*Power(t2,3) + t1*(-47 + 50*t2 + 25*Power(t2,2))) + 
               Power(s2,2)*(51 + 43*Power(t1,4) - 98*t2 + 
                  10*Power(t2,2) + 8*Power(t2,3) + 
                  Power(t1,3)*(101 + 67*t2) - 
                  2*Power(t1,2)*(83 + 91*t2 + 34*Power(t2,2)) - 
                  t1*(29 - 289*t2 + 37*Power(t2,2) + 16*Power(t2,3))) + 
               s2*(14 - 8*Power(t1,5) + 275*t2 - 124*Power(t2,2) - 
                  16*Power(t2,3) - 8*Power(t1,4)*(8 + 3*t2) + 
                  Power(t1,2)*(44 - 3*t2 - 52*Power(t2,2)) + 
                  Power(t1,3)*(91 + 76*t2 + 32*Power(t2,2)) + 
                  t1*(-77 - 316*t2 + 156*Power(t2,2) + 16*Power(t2,3)))))))/
     ((-1 + s1)*(-1 + s2)*s2*(-s + s2 - t1)*(1 - s + s2 - t1)*
       Power(-1 + s + t1,2)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)) - 
    (8*(32 - 7*s2 - 17*Power(s2,2) + 8*Power(s1,6)*Power(s2,2) - 114*t1 + 
         24*s2*t1 + 54*Power(s2,2)*t1 + 146*Power(t1,2) - 
         30*s2*Power(t1,2) - 60*Power(s2,2)*Power(t1,2) - 
         74*Power(t1,3) + 16*s2*Power(t1,3) + 
         26*Power(s2,2)*Power(t1,3) + 6*Power(t1,4) - 3*s2*Power(t1,4) - 
         3*Power(s2,2)*Power(t1,4) + 4*Power(t1,5) - 
         4*Power(s,5)*(-1 + s2)*Power(t1 - t2,2) + 7*t2 + 76*s2*t2 - 
         48*Power(s2,2)*t2 - 42*Power(s2,3)*t2 - Power(s2,4)*t2 - 
         23*t1*t2 - 208*s2*t1*t2 + 117*Power(s2,2)*t1*t2 + 
         92*Power(s2,3)*t1*t2 + 2*Power(s2,4)*t1*t2 + 24*Power(t1,2)*t2 + 
         170*s2*Power(t1,2)*t2 - 91*Power(s2,2)*Power(t1,2)*t2 - 
         58*Power(s2,3)*Power(t1,2)*t2 - Power(s2,4)*Power(t1,2)*t2 - 
         7*Power(t1,3)*t2 - 20*s2*Power(t1,3)*t2 + 
         23*Power(s2,2)*Power(t1,3)*t2 + 8*Power(s2,3)*Power(t1,3)*t2 - 
         Power(t1,4)*t2 - 18*s2*Power(t1,4)*t2 - 
         Power(s2,2)*Power(t1,4)*t2 + 3*Power(t2,2) + 3*s2*Power(t2,2) + 
         57*Power(s2,2)*Power(t2,2) - 58*Power(s2,3)*Power(t2,2) - 
         28*Power(s2,4)*Power(t2,2) - Power(s2,5)*Power(t2,2) - 
         13*t1*Power(t2,2) - 7*s2*t1*Power(t2,2) - 
         113*Power(s2,2)*t1*Power(t2,2) + 84*Power(s2,3)*t1*Power(t2,2) + 
         34*Power(s2,4)*t1*Power(t2,2) + Power(s2,5)*t1*Power(t2,2) + 
         12*Power(t1,2)*Power(t2,2) - 11*s2*Power(t1,2)*Power(t2,2) + 
         31*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         28*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         6*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         3*Power(t1,3)*Power(t2,2) + 14*s2*Power(t1,3)*Power(t2,2) + 
         25*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         2*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         Power(t1,4)*Power(t2,2) + s2*Power(t1,4)*Power(t2,2) + 
         7*Power(t2,3) - 6*s2*Power(t2,3) + 4*Power(s2,2)*Power(t2,3) + 
         17*Power(s2,3)*Power(t2,3) - 20*Power(s2,4)*Power(t2,3) - 
         4*Power(s2,5)*Power(t2,3) - 18*t1*Power(t2,3) + 
         9*s2*t1*Power(t2,3) + 15*Power(s2,2)*t1*Power(t2,3) - 
         19*Power(s2,3)*t1*Power(t2,3) + 10*Power(s2,4)*t1*Power(t2,3) + 
         Power(s2,5)*t1*Power(t2,3) + 13*Power(t1,2)*Power(t2,3) - 
         5*s2*Power(t1,2)*Power(t2,3) - 
         29*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         12*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         3*Power(t1,3)*Power(t2,3) - 5*s2*Power(t1,3)*Power(t2,3) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,3) - 3*Power(t2,4) + 
         17*s2*Power(t2,4) - 17*Power(s2,2)*Power(t2,4) + 
         5*Power(s2,3)*Power(t2,4) + 3*Power(s2,4)*Power(t2,4) - 
         Power(s2,5)*Power(t2,4) + t1*Power(t2,4) - 
         23*s2*t1*Power(t2,4) + 16*Power(s2,2)*t1*Power(t2,4) + 
         15*Power(s2,3)*t1*Power(t2,4) + Power(s2,4)*t1*Power(t2,4) + 
         3*Power(t1,2)*Power(t2,4) + 8*s2*Power(t1,2)*Power(t2,4) + 
         6*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         Power(s2,3)*Power(t1,2)*Power(t2,4) + 2*Power(t2,5) + 
         9*Power(s2,2)*Power(t2,5) - 9*Power(s2,3)*Power(t2,5) - 
         t1*Power(t2,5) - 5*s2*t1*Power(t2,5) - 
         6*Power(s2,2)*t1*Power(t2,5) - 2*Power(s2,3)*t1*Power(t2,5) + 
         s2*Power(t2,6) + 2*Power(s2,2)*Power(t2,6) + 
         Power(s2,3)*Power(t2,6) + 
         Power(s1,5)*(Power(s2,4) - Power(t1,2) + 
            s2*(-16 + 2*Power(t1,2) - t1*(-18 + t2)) + 
            Power(s2,2)*(9 + Power(t1,2) - t1*(-10 + t2) - 43*t2) + 
            Power(s2,3)*(6 - 2*t1 + t2)) + 
         Power(s1,4)*(8 + Power(s2,5) + 4*Power(t1,3) + 3*t1*(-6 + t2) - 
            2*Power(s2,4)*t2 + Power(t1,2)*(7 + 2*t2) + 
            Power(s2,3)*(30 - 3*Power(t1,2) - 34*t2 - 3*Power(t2,2) + 
               t1*(2 + 7*t2)) + 
            Power(s2,2)*(-41 + 2*Power(t1,3) + Power(t1,2)*(4 - 5*t2) - 
               26*t2 + 94*Power(t2,2) + 4*t1*(13 - 12*t2 + Power(t2,2))) \
+ s2*(-18 + 2*Power(t1,3) + Power(t1,2)*(14 - 9*t2) + 68*t2 + 
               Power(t2,2) + t1*(2 - 80*t2 + 4*Power(t2,2)))) + 
         Power(s,4)*(6 + 8*t1 - 11*Power(t1,2) + 3*Power(t1,3) + 8*t2 + 
            22*t1*t2 + 3*Power(t1,2)*t2 - 11*Power(t2,2) - 
            15*t1*Power(t2,2) + 9*Power(t2,3) + 
            Power(s1,2)*(-1 + s2)*
             (-6 + 2*Power(s2,2) - t1 + t2 + s2*(4 - t1 + t2)) + 
            Power(s2,3)*(2 + Power(t1,2) + 3*t2 + Power(t2,2) - 
               t1*(3 + 2*t2)) - 
            s2*(10 + 6*Power(t1,3) + 31*t2 + 3*Power(t2,2) + 
               6*Power(t2,3) - Power(t1,2)*(5 + 6*t2) + 
               t1*(1 + 2*t2 - 6*Power(t2,2))) + 
            Power(s2,2)*(2 - Power(t1,3) + 20*t2 + 17*Power(t2,2) + 
               Power(t2,3) + 3*Power(t1,2)*(3 + t2) - 
               t1*(4 + 26*t2 + 3*Power(t2,2))) + 
            s1*(-12 - 17*t1 - 16*Power(t1,2) + 
               Power(s2,3)*(-4 + 3*t1 - 3*t2) + t2 + 24*t1*t2 - 
               8*Power(t2,2) - 
               Power(s2,2)*(4 + 3*t1 + 2*Power(t1,2) + 13*t2 - 
                  4*t1*t2 + 2*Power(t2,2)) + 
               s2*(20 + 17*t1 + 14*Power(t1,2) + 15*t2 - 20*t1*t2 + 
                  6*Power(t2,2)))) + 
         Power(s1,3)*(9 + Power(t1,4) + Power(t1,3)*(3 - 10*t2) + 
            Power(s2,4)*(18 - 3*Power(t1,2) + t1*(-4 + t2) - 5*t2) + 
            Power(s2,5)*(1 + 2*t1 - 2*t2) - 25*t2 - 32*Power(t1,2)*t2 - 
            2*Power(t2,2) + t1*(-12 + 63*t2 - 8*Power(t2,2)) + 
            Power(s2,3)*(-41 - 99*t2 + 6*Power(t1,2)*t2 + 
               75*Power(t2,2) + 2*Power(t2,3) + 
               t1*(55 - 15*t2 - 7*Power(t2,2))) + 
            Power(s2,2)*(-79 + Power(t1,4) + 142*t2 + 15*Power(t2,2) - 
               106*Power(t2,3) - Power(t1,3)*(11 + 5*t2) + 
               3*Power(t1,2)*(9 - 8*t2 + 3*Power(t2,2)) + 
               t1*(74 - 169*t2 + 90*Power(t2,2) - 6*Power(t2,3))) + 
            s2*(64 + 4*Power(t1,4) + Power(t1,3)*(10 - 7*t2) + 27*t2 - 
               108*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,2)*(74 - 64*t2 + 15*Power(t2,2)) + 
               t1*(-145 + 42*t2 + 137*Power(t2,2) - 6*Power(t2,3)))) + 
         Power(s1,2)*(-29 - 2*Power(t1,4)*(-4 + t2) - 2*t2 + 
            23*Power(t2,2) + 6*Power(t2,3) + 
            Power(t1,3)*(27 - 21*t2 + 8*Power(t2,2)) + 
            Power(t1,2)*(-101 + 46*t2 + 46*Power(t2,2) - 
               2*Power(t2,3)) + 
            t1*(95 - 24*t2 - 71*Power(t2,2) + 6*Power(t2,3)) + 
            Power(s2,5)*(Power(t1,2) - 6*t2 - t1*(1 + 3*t2)) + 
            Power(s2,4)*(-19 - 2*Power(t1,3) - 57*t2 + 13*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,2)*(7 + 4*t2) + 
               t1*(14 + 20*t2 - Power(t2,2))) + 
            Power(s2,3)*(-46 - 15*Power(t1,3) + Power(t1,4) + 101*t2 + 
               113*Power(t2,2) - 81*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(15 - 10*t2 - 2*Power(t2,2)) - 
               t1*(-45 + 133*t2 - 39*Power(t2,2) + Power(t2,3))) - 
            s2*(-64 + 135*t2 - 17*Power(t2,2) - 76*Power(t2,3) - 
               6*Power(t2,4) + Power(t1,4)*(8 + 5*t2) + 
               Power(t1,3)*(-53 + 43*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(1 + 127*t2 - 94*Power(t2,2) + 
                  11*Power(t2,3)) + 
               t1*(108 - 289*t2 + 113*Power(t2,2) + 111*Power(t2,3) - 
                  4*Power(t2,4))) + 
            Power(s2,2)*(114 - Power(t1,4)*(-1 + t2) + 165*t2 - 
               178*Power(t2,2) + 21*Power(t2,3) + 64*Power(t2,4) + 
               Power(t1,3)*(11 + 10*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(113 - 63*t2 + 42*Power(t2,2) - 
                  7*Power(t2,3)) + 
               t1*(-239 - 147*t2 + 198*Power(t2,2) - 82*Power(t2,3) + 
                  4*Power(t2,4)))) + 
         s1*(-16 + 27*t2 - 14*Power(t2,2) - 3*Power(t2,3) - 
            6*Power(t2,4) + Power(t1,4)*(19 - 13*t2 + Power(t2,2)) + 
            Power(s2,5)*t2*(1 - Power(t1,2) + 9*t2 + 2*Power(t2,2)) - 
            Power(t1,3)*(38 + 13*t2 - 21*Power(t2,2) + 2*Power(t2,3)) + 
            t1*(32 - 81*t2 + 54*Power(t2,2) + 25*Power(t2,3)) + 
            Power(t1,2)*(3 + 80*t2 - 59*Power(t2,2) - 24*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,4)*(2*Power(t1,2)*(-1 + t2) + 
               Power(t1,3)*(1 + t2) - 
               t2*(-48 - 59*t2 + 11*Power(t2,2) + Power(t2,3)) - 
               t1*(-1 + 51*t2 + 26*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(35 - Power(t1,4) + 107*t2 - 77*Power(t2,2) - 
               49*Power(t2,3) + 43*Power(t2,4) - 3*Power(t2,5) + 
               Power(t1,3)*(2 + 7*t2) + 
               Power(t1,2)*(34 + 25*t2 + 22*Power(t2,2) - 
                  2*Power(t2,3)) + 
               t1*(-70 - 139*t2 + 97*Power(t2,2) - 41*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s2*(-105 - 71*t2 + 77*Power(t2,2) - 43*Power(t2,3) - 
               20*Power(t2,4) - 4*Power(t2,5) + 
               Power(t1,4)*(15 + t2 + Power(t2,2)) + 
               Power(t1,3)*(58 - 45*t2 + 38*Power(t2,2) - 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-266 - 18*t2 + 58*Power(t2,2) - 
                  52*Power(t2,3) + 3*Power(t2,4)) + 
               t1*(298 + 133*t2 - 153*Power(t2,2) + 92*Power(t2,3) + 
                  41*Power(t2,4) - Power(t2,5))) - 
            Power(s2,2)*(-34 + 9*Power(t1,4) + 172*t2 + 90*Power(t2,2) - 
               94*Power(t2,3) + 28*Power(t2,4) + 19*Power(t2,5) + 
               Power(t1,3)*(-21 + 38*t2 - 3*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-19 + 144*t2 - 65*Power(t2,2) + 
                  28*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(65 - 354*t2 - 58*Power(t2,2) + 97*Power(t2,3) - 
                  36*Power(t2,4) + Power(t2,5)))) - 
         Power(s,3)*(28 - 43*t1 + 4*Power(t1,2) + 13*Power(t1,3) + 
            35*t2 - 66*t1*t2 - 4*Power(t1,2)*t2 - 5*Power(t1,3)*t2 - 
            2*Power(t2,2) - 31*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) + 22*Power(t2,3) + 
            9*t1*Power(t2,3) - 7*Power(t2,4) + 
            Power(s2,4)*(4 + 2*Power(t1,2) + 7*t2 + 3*Power(t2,2) - 
               t1*(6 + 5*t2)) + 
            s2*(-47 - 10*Power(t1,3) + 3*Power(t1,4) - 59*t2 + 
               21*Power(t2,2) + 6*Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,2)*(-45 + 2*t2 - 6*Power(t2,2)) + 
               t1*(97 + 152*t2 + 2*Power(t2,2))) + 
            Power(s2,3)*(-1 - 3*Power(t1,3) + 34*t2 + 32*Power(t2,2) + 
               3*Power(t2,3) + 9*Power(t1,2)*(2 + t2) - 
               t1*(12 + 46*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(16 + Power(t1,4) - 19*t2 - 46*Power(t2,2) - 
               25*Power(t2,3) - 3*Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(21 + 11*t2 + 3*Power(t2,2)) - 
               t1*(34 + 43*t2 - 20*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(14 + 5*Power(s2,3) + 2*t1 - t2 + 
               s2*(-37 - t1 + t2) + Power(s2,2)*(-3*t1 + 2*(9 + t2))) + 
            Power(s1,2)*(5 + 5*Power(s2,4) - 28*Power(t1,2) + 
               Power(s2,3)*(-3 + 4*t1 - 6*t2) - 22*t2 - 5*Power(t2,2) + 
               t1*(-33 + 29*t2) + 
               s2*(27 + 17*Power(t1,2) + t1*(19 - 14*t2) + 83*t2 + 
                  Power(t2,2)) - 
               Power(s2,2)*(34 + 5*Power(t1,2) + 61*t2 + 
                  4*Power(t2,2) - t1*(16 + 9*t2))) + 
            s1*(-51 + 10*Power(t1,3) + Power(s2,4)*(-9 + 7*t1 - 8*t2) + 
               7*t2 - 14*Power(t2,2) + 13*Power(t2,3) + 
               Power(t1,2)*(14 + 17*t2) + 
               t1*(63 + 64*t2 - 40*Power(t2,2)) + 
               Power(s2,3)*(3 - 4*Power(t1,2) - 28*t2 + 6*t1*t2 - 
                  2*Power(t2,2)) - 
               s2*(-69 + 19*Power(t1,3) + Power(t1,2)*(42 - 9*t2) + 
                  65*t2 + 52*Power(t2,2) + 5*Power(t2,3) + 
                  t1*(99 + 26*t2 - 15*Power(t2,2))) + 
               Power(s2,2)*(-Power(t1,3) + 4*Power(t1,2)*(12 + t2) + 
                  t1*(23 - 60*t2 - 5*Power(t2,2)) + 
                  2*(-6 + 50*t2 + 34*Power(t2,2) + Power(t2,3))))) + 
         Power(s,2)*(84 - 185*t1 + 128*Power(t1,2) - 17*Power(t1,3) - 
            10*Power(t1,4) + 73*t2 - 161*t1*t2 + 88*Power(t1,2)*t2 + 
            11*Power(t1,3)*t2 - Power(t1,4)*t2 + 21*Power(t2,2) + 
            t1*Power(t2,2) - 10*Power(t1,2)*Power(t2,2) + 
            5*Power(t1,3)*Power(t2,2) + 24*Power(t2,3) + 
            27*t1*Power(t2,3) - 5*Power(t1,2)*Power(t2,3) - 
            18*Power(t2,4) - t1*Power(t2,4) + 2*Power(t2,5) + 
            Power(s1,4)*(8 + 4*Power(s2,3) + t1 + 
               Power(s2,2)*(37 - 3*t1 + t2) + s2*(-43 - 2*t1 + t2)) + 
            Power(s2,5)*(2 + Power(t1,2) + 5*t2 + 3*Power(t2,2) - 
               t1*(3 + 4*t2)) + 
            Power(s2,4)*(2 - 2*Power(t1,3) + 22*t2 + 25*Power(t2,2) + 
               3*Power(t2,3) + 4*Power(t1,2)*(3 + 2*t2) - 
               t1*(12 + 32*t2 + 9*Power(t2,2))) + 
            Power(s2,3)*(12 + Power(t1,4) + 60*t2 - 27*Power(t2,2) - 
               43*Power(t2,3) - 2*Power(t2,4) - 
               Power(t1,3)*(14 + 3*t2) + 
               Power(t1,2)*(31 + 23*t2 + Power(t2,2)) + 
               t1*(-30 - 88*t2 + 22*Power(t2,2) + 3*Power(t2,3))) - 
            Power(s2,2)*(-1 + Power(t1,4)*(-2 + t2) + 198*t2 + 
               92*Power(t2,2) - 34*Power(t2,3) - 15*Power(t2,4) + 
               2*Power(t2,5) + 
               Power(t1,3)*(-11 + 10*t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(13 + 53*t2 - 5*Power(t2,2) + 
                  9*Power(t2,3)) + 
               t1*(1 - 254*t2 - 116*Power(t2,2) + 12*Power(t2,3) - 
                  7*Power(t2,4))) + 
            s2*(-99 + Power(t1,4)*(7 - 4*t2) + 26*t2 + 54*Power(t2,2) - 
               32*Power(t2,3) - 9*Power(t2,4) - 2*Power(t2,5) + 
               2*Power(t1,3)*(18 + 2*t2 + 5*Power(t2,2)) - 
               Power(t1,2)*(185 + 96*t2 + 14*Power(t2,2) + 
                  10*Power(t2,3)) + 
               t1*(241 + 77*t2 - 100*Power(t2,2) + 12*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s1,3)*(35 + 9*Power(s2,4) - 25*Power(t1,2) + 
               Power(s2,3)*(16 - 3*t1 - 3*t2) - 26*t2 - 2*Power(t2,2) + 
               t1*(-39 + 16*t2) + 
               s2*(-42 + 10*Power(t1,2) + t1*(23 - 3*t2) + 139*t2 - 
                  Power(t2,2)) - 
               Power(s2,2)*(38 + 3*Power(t1,2) + 132*t2 + Power(t2,2) - 
                  t1*(49 + 5*t2))) + 
            Power(s1,2)*(-29 + 4*Power(s2,5) + 15*Power(t1,3) + 
               Power(s2,4)*(-5 + 10*t1 - 14*t2) - 39*t2 + 
               10*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,2)*(8 + 27*t2) + 
               t1*(44 + 113*t2 - 36*Power(t2,2)) - 
               2*Power(s2,3)*
                (-1 + 7*Power(t1,2) + 44*t2 + 4*Power(t2,2) - 
                  t1*(9 + 10*t2)) - 
               s2*(-214 + 18*Power(t1,3) - 40*t2 + 158*Power(t2,2) + 
                  3*Power(t2,3) + Power(t1,2)*(37 + 9*t2) + 
                  t1*(220 + 41*t2 - 18*Power(t2,2))) + 
               Power(s2,2)*(-162 + 3*Power(t1,3) + 
                  Power(t1,2)*(73 - 6*t2) + 127*t2 + 168*Power(t2,2) - 
                  3*Power(t2,3) + 2*t1*(55 - 61*t2 + 3*Power(t2,2)))) + 
            s1*(-104 + Power(t1,4) + Power(s2,5)*(-6 + 5*t1 - 7*t2) + 
               25*t2 - 20*Power(t2,2) + 26*Power(t2,3) - 6*Power(t2,4) - 
               9*Power(t1,3)*(3 + 2*t2) - 
               Power(s2,4)*(-3*t1 + Power(t1,2) + (21 - 2*t2)*t2) + 
               Power(t1,2)*(-50 + 6*t2 + 3*Power(t2,2)) + 
               t1*(170 - 68*t2 - 101*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,3)*(-46 - 6*Power(t1,3) + 37*t2 + 
                  115*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t1,2)*(39 + 17*t2) + 
                  t1*(21 - 56*t2 - 20*Power(t2,2))) + 
               s2*(-18 + 10*Power(t1,4) + Power(t1,3)*(36 - 5*t2) - 
                  298*t2 + 34*Power(t2,2) + 71*Power(t2,3) + 
                  5*Power(t2,4) + 
                  Power(t1,2)*(-25 + 55*t2 + 9*Power(t2,2)) + 
                  t1*(-10 + 359*t2 + 6*Power(t2,2) - 19*Power(t2,3))) + 
               Power(s2,2)*(162 + 3*Power(t1,4) + 270*t2 - 
                  123*Power(t2,2) - 88*Power(t2,3) + 5*Power(t2,4) - 
                  Power(t1,3)*(21 + 11*t2) + 
                  3*Power(t1,2)*(17 - 16*t2 + 6*Power(t2,2)) - 
                  t1*(187 + 269*t2 - 85*Power(t2,2) + 15*Power(t2,3))))) \
- s*(93 + Power(s1,5)*s2*(-16 + Power(s2,2) - s2*(-29 + t1) - t1) - 
            259*t1 + 247*Power(t1,2) - 85*Power(t1,3) + 4*Power(t1,5) + 
            49*t2 - 122*t1*t2 + 102*Power(t1,2)*t2 - 19*Power(t1,3)*t2 - 
            10*Power(t1,4)*t2 + 15*Power(t2,2) - 10*t1*Power(t2,2) - 
            22*Power(t1,2)*Power(t2,2) + 15*Power(t1,3)*Power(t2,2) + 
            Power(t1,4)*Power(t2,2) + 17*Power(t2,3) - 9*t1*Power(t2,3) - 
            12*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) - 
            14*Power(t2,4) - t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) + 
            4*Power(t2,5) - t1*Power(t2,5) + 
            Power(s2,6)*t2*(1 - t1 + t2) + 
            Power(s1,4)*(16 + 5*Power(s2,4) - 10*Power(t1,2) + 
               Power(s2,3)*(20 - 6*t1 + t2) + t1*(-19 + 3*t2) + 
               Power(s2,2)*(3 + Power(t1,2) - t1*(-40 + t2) - 127*t2 + 
                  Power(t2,2)) + 
               s2*(-60 + 5*Power(t1,2) + t1*(39 - 2*t2) + 70*t2 + 
                  Power(t2,2))) + 
            Power(s2,5)*(1 + 4*t2 + 6*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(1 + 2*t2) - t1*(2 + 6*t2 + 3*Power(t2,2))) + 
            Power(s2,4)*(-(Power(t1,3)*(2 + t2)) + 
               Power(t1,2)*(4 + 9*t2) + 
               t2*(24 + t2 - 24*Power(t2,2) - 3*Power(t2,3)) + 
               t1*(-2 - 32*t2 + 4*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,3)*(13 + Power(t1,4) - 28*t2 - 138*Power(t2,2) - 
               27*Power(t2,3) + 22*Power(t2,4) - 2*Power(t2,5) + 
               2*Power(t1,3)*(-1 + 2*t2 + Power(t2,2)) - 
               Power(t1,2)*(-14 + 28*t2 + 11*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-26 + 52*t2 + 151*Power(t2,2) - 4*Power(t2,3) + 
                  6*Power(t2,4))) + 
            s2*(-60 + 150*t2 + 46*Power(t2,2) - 37*Power(t2,3) + 
               13*Power(t2,4) - Power(t2,5) + Power(t2,6) + 
               Power(t1,4)*(1 - 14*t2 + Power(t2,2)) + 
               Power(t1,3)*(62 + 30*t2 + 35*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-187 + 86*t2 + 8*Power(t2,2) - 
                  37*Power(t2,3) + 6*Power(t2,4)) + 
               t1*(184 - 252*t2 - 77*Power(t2,2) + 76*Power(t2,3) + 
                  17*Power(t2,4) - 4*Power(t2,5))) - 
            Power(s2,2)*(31 + 202*t2 - 35*Power(t2,2) - 44*Power(t2,3) + 
               38*Power(t2,4) + 9*Power(t2,5) - Power(t2,6) + 
               2*Power(t1,4)*(3 + t2) + 
               Power(t1,3)*(-39 - 6*t2 - 18*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(91 + 215*t2 + 23*Power(t2,2) + 
                  31*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(-89 - 413*t2 + 15*Power(t2,2) + 15*Power(t2,3) - 
                  24*Power(t2,4) + 3*Power(t2,5))) + 
            Power(s1,3)*(31 + 4*Power(s2,5) + 12*Power(t1,3) + 
               Power(s2,4)*(5 + 3*t1 - 8*t2) - 50*t2 - 4*Power(t2,2) + 
               3*Power(t1,2)*(2 + 5*t2) + 
               t1*(-42 + 86*t2 - 8*Power(t2,2)) + 
               Power(s2,3)*(47 - 12*Power(t1,2) - 91*t2 - 
                  7*Power(t2,2) + t1*(16 + 19*t2)) - 
               s2*(-57 + 3*Power(t1,3) - 154*t2 + 113*Power(t2,2) + 
                  4*Power(t2,3) + 3*Power(t1,2)*(-8 + 7*t2) + 
                  t1*(119 + 115*t2 - 16*Power(t2,2))) + 
               Power(s2,2)*(-196 + 5*Power(t1,3) + 
                  Power(t1,2)*(38 - 12*t2) + 29*t2 + 216*Power(t2,2) - 
                  4*Power(t2,3) + 6*t1*(24 - 23*t2 + 2*Power(t2,2)))) + 
            Power(s1,2)*(-61 + Power(s2,6) + 2*Power(t1,4) + 
               Power(s2,5)*(-2 + 5*t1 - 7*t2) - 23*t2 + 38*Power(t2,2) + 
               12*Power(t2,3) - Power(t1,3)*(13 + 23*t2) + 
               Power(t1,2)*(-85 - 4*t2 + 3*Power(t2,2)) + 
               t1*(156 + 29*t2 - 116*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,4)*(7 - 6*Power(t1,2) - 40*t2 - 2*Power(t2,2) + 
                  4*t1*(1 + t2)) + 
               Power(s2,3)*(-133 - 3*Power(t1,3) - 125*t2 + 
                  144*Power(t2,2) + 5*Power(t2,3) + 
                  2*Power(t1,2)*(12 + 7*t2) - 
                  2*t1*(-58 + 14*t2 + 7*Power(t2,2))) + 
               s2*(253 + 11*Power(t1,4) + Power(t1,3)*(56 - 12*t2) - 
                  161*t2 - 115*Power(t2,2) + 79*Power(t2,3) + 
                  6*Power(t2,4) + 
                  Power(t1,2)*(67 - 59*t2 + 33*Power(t2,2)) + 
                  t1*(-374 + 312*t2 + 130*Power(t2,2) - 26*Power(t2,3))) \
+ Power(s2,2)*(47 + 3*Power(t1,4) + 454*t2 - 105*Power(t2,2) - 
                  176*Power(t2,3) + 6*Power(t2,4) - 
                  13*Power(t1,3)*(2 + t2) + 
                  Power(t1,2)*(72 - 89*t2 + 24*Power(t2,2)) - 
                  t1*(81 + 337*t2 - 180*Power(t2,2) + 20*Power(t2,3)))) + 
            s1*(-77 + Power(t1,4)*(14 - 3*t2) + 
               Power(s2,6)*(-1 + t1 - 2*t2) + 54*t2 - 25*Power(t2,2) + 
               10*Power(t2,3) - 12*Power(t2,4) + 
               Power(s2,5)*(-1 + Power(t1,2) - 4*t2 - 2*t1*t2 + 
                  2*Power(t2,2)) + 
               Power(t1,3)*(-25 + 2*t2 + 14*Power(t2,2)) + 
               Power(t1,2)*(-54 + 107*t2 + 10*Power(t2,2) - 
                  11*Power(t2,3)) + 
               2*t1*(71 - 79*t2 + 11*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,4)*(-11 - 4*Power(t1,3) - 6*t2 + 
                  59*Power(t2,2) + 8*Power(t2,3) + 
                  2*Power(t1,2)*(7 + 4*t2) + 
                  t1*(1 - 12*t2 - 11*Power(t2,2))) + 
               Power(s2,3)*(20 + 2*Power(t1,4) + 283*t2 + 
                  105*Power(t2,2) - 95*Power(t2,3) + 2*Power(t2,4) - 
                  3*Power(t1,3)*(8 + t2) + 
                  Power(t1,2)*(54 + 7*t2 + 4*Power(t2,2)) - 
                  t1*(52 + 295*t2 - 16*Power(t2,2) + 5*Power(t2,3))) - 
               s2*(192 + 318*t2 - 141*Power(t2,2) - 8*Power(t2,3) + 
                  19*Power(t2,4) + 4*Power(t2,5) + 
                  Power(t1,4)*(1 + 9*t2) + 
                  Power(t1,3)*(-52 + 85*t2 - 19*Power(t2,2)) + 
                  Power(t1,2)*
                   (247 + 115*t2 - 72*Power(t2,2) + 23*Power(t2,3)) + 
                  t1*(-388 - 501*t2 + 269*Power(t2,2) + 71*Power(t2,3) - 
                     17*Power(t2,4))) + 
               Power(s2,2)*(188 + Power(t1,4)*(3 - 2*t2) - 81*t2 - 
                  302*Power(t2,2) + 111*Power(t2,3) + 67*Power(t2,4) - 
                  4*Power(t2,5) + 
                  Power(t1,3)*(33 - 6*t2 + 9*Power(t2,2)) + 
                  Power(t1,2)*
                   (120 - 23*t2 + 82*Power(t2,2) - 16*Power(t2,3)) + 
                  t1*(-344 + 82*t2 + 208*Power(t2,2) - 106*Power(t2,3) + 
                     13*Power(t2,4))))))*
       B1(1 - s1 - t1 + t2,s2,1 - s + s2 - t1))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)) - 
    (8*(-7 + 77*s2 - 77*Power(s2,2) + 17*Power(s2,3) + 8*Power(s2,4) - 
         2*Power(s2,5) - 8*Power(s1,5)*Power(-1 + s2,2)*Power(s2,3)*
          (-1 + t1) + 30*t1 - 328*s2*t1 + 322*Power(s2,2)*t1 - 
         74*Power(s2,3)*t1 - 30*Power(s2,4)*t1 + 8*Power(s2,5)*t1 - 
         47*Power(t1,2) + 523*s2*Power(t1,2) - 
         511*Power(s2,2)*Power(t1,2) + 127*Power(s2,3)*Power(t1,2) + 
         40*Power(s2,4)*Power(t1,2) - 12*Power(s2,5)*Power(t1,2) + 
         28*Power(t1,3) - 352*s2*Power(t1,3) + 
         364*Power(s2,2)*Power(t1,3) - 108*Power(s2,3)*Power(t1,3) - 
         20*Power(s2,4)*Power(t1,3) + 8*Power(s2,5)*Power(t1,3) + 
         3*Power(t1,4) + 43*s2*Power(t1,4) - 91*Power(s2,2)*Power(t1,4) + 
         47*Power(s2,3)*Power(t1,4) - 2*Power(s2,5)*Power(t1,4) - 
         10*Power(t1,5) + 56*s2*Power(t1,5) - 
         14*Power(s2,2)*Power(t1,5) - 10*Power(s2,3)*Power(t1,5) + 
         2*Power(s2,4)*Power(t1,5) + 3*Power(t1,6) - 19*s2*Power(t1,6) + 
         7*Power(s2,2)*Power(t1,6) + Power(s2,3)*Power(t1,6) - 6*t2 + 
         57*s2*t2 + 39*Power(s2,2)*t2 - 123*Power(s2,3)*t2 + 
         75*Power(s2,4)*t2 - 8*Power(s2,5)*t2 - 2*Power(s2,6)*t2 + 
         20*t1*t2 - 170*s2*t1*t2 - 144*Power(s2,2)*t1*t2 + 
         368*Power(s2,3)*t1*t2 - 208*Power(s2,4)*t1*t2 + 
         24*Power(s2,5)*t1*t2 + 6*Power(s2,6)*t1*t2 - 22*Power(t1,2)*t2 + 
         144*s2*Power(t1,2)*t2 + 190*Power(s2,2)*Power(t1,2)*t2 - 
         370*Power(s2,3)*Power(t1,2)*t2 + 
         176*Power(s2,4)*Power(t1,2)*t2 - 24*Power(s2,5)*Power(t1,2)*t2 - 
         6*Power(s2,6)*Power(t1,2)*t2 + 6*Power(t1,3)*t2 + 
         24*s2*Power(t1,3)*t2 - 94*Power(s2,2)*Power(t1,3)*t2 + 
         132*Power(s2,3)*Power(t1,3)*t2 - 30*Power(s2,4)*Power(t1,3)*t2 + 
         8*Power(s2,5)*Power(t1,3)*t2 + 2*Power(s2,6)*Power(t1,3)*t2 + 
         4*Power(t1,4)*t2 - 91*s2*Power(t1,4)*t2 - 
         3*Power(s2,2)*Power(t1,4)*t2 - 11*Power(s2,3)*Power(t1,4)*t2 - 
         11*Power(s2,4)*Power(t1,4)*t2 - 2*Power(t1,5)*t2 + 
         42*s2*Power(t1,5)*t2 + 14*Power(s2,2)*Power(t1,5)*t2 + 
         4*Power(s2,3)*Power(t1,5)*t2 - 2*Power(s2,4)*Power(t1,5)*t2 - 
         6*s2*Power(t1,6)*t2 - 2*Power(s2,2)*Power(t1,6)*t2 + 
         Power(t2,2) - 20*s2*Power(t2,2) + 101*Power(s2,2)*Power(t2,2) - 
         103*Power(s2,3)*Power(t2,2) - 2*Power(s2,4)*Power(t2,2) + 
         55*Power(s2,5)*Power(t2,2) - 24*Power(s2,6)*Power(t2,2) - 
         2*t1*Power(t2,2) + 60*s2*t1*Power(t2,2) - 
         190*Power(s2,2)*t1*Power(t2,2) + 
         162*Power(s2,3)*t1*Power(t2,2) + 14*Power(s2,4)*t1*Power(t2,2) - 
         82*Power(s2,5)*t1*Power(t2,2) + 46*Power(s2,6)*t1*Power(t2,2) - 
         62*s2*Power(t1,2)*Power(t2,2) + 
         36*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         6*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         36*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         20*Power(s2,6)*Power(t1,2)*Power(t2,2) + 
         2*Power(t1,3)*Power(t2,2) + 28*s2*Power(t1,3)*Power(t2,2) + 
         102*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         58*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         38*Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         26*Power(s2,5)*Power(t1,3)*Power(t2,2) - 
         2*Power(s2,6)*Power(t1,3)*Power(t2,2) - 
         Power(t1,4)*Power(t2,2) - 10*s2*Power(t1,4)*Power(t2,2) - 
         57*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         14*Power(s2,4)*Power(t1,4)*Power(t2,2) + 
         Power(s2,5)*Power(t1,4)*Power(t2,2) + 
         4*s2*Power(t1,5)*Power(t2,2) + 
         8*Power(s2,2)*Power(t1,5)*Power(t2,2) + 
         4*Power(s2,3)*Power(t1,5)*Power(t2,2) - 
         30*Power(s2,2)*Power(t2,3) + 75*Power(s2,3)*Power(t2,3) - 
         93*Power(s2,4)*Power(t2,3) + 55*Power(s2,5)*Power(t2,3) - 
         7*Power(s2,6)*Power(t2,3) - 8*Power(s2,7)*Power(t2,3) - 
         s2*t1*Power(t2,3) + 70*Power(s2,2)*t1*Power(t2,3) - 
         108*Power(s2,3)*t1*Power(t2,3) + 82*Power(s2,4)*t1*Power(t2,3) - 
         31*Power(s2,5)*t1*Power(t2,3) + 12*Power(s2,6)*t1*Power(t2,3) + 
         8*Power(s2,7)*t1*Power(t2,3) + 4*s2*Power(t1,2)*Power(t2,3) - 
         44*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         15*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         9*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         27*Power(s2,5)*Power(t1,2)*Power(t2,3) - 
         5*Power(s2,6)*Power(t1,2)*Power(t2,3) - 
         5*s2*Power(t1,3)*Power(t2,3) + 
         8*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         22*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         4*Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         3*Power(s2,5)*Power(t1,3)*Power(t2,3) + 
         2*s2*Power(t1,4)*Power(t2,3) - 
         4*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         4*Power(s2,3)*Power(t1,4)*Power(t2,3) - 
         2*Power(s2,4)*Power(t1,4)*Power(t2,3) + 
         Power(s2,2)*Power(t2,4) - 18*Power(s2,3)*Power(t2,4) + 
         36*Power(s2,4)*Power(t2,4) - 24*Power(s2,5)*Power(t2,4) + 
         11*Power(s2,6)*Power(t2,4) - 6*Power(s2,7)*Power(t2,4) - 
         5*Power(s2,2)*t1*Power(t2,4) + 24*Power(s2,3)*t1*Power(t2,4) - 
         44*Power(s2,4)*t1*Power(t2,4) + 24*Power(s2,5)*t1*Power(t2,4) + 
         Power(s2,6)*t1*Power(t2,4) + 
         6*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         6*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         2*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
         2*Power(s2,4)*Power(t1,3)*Power(t2,4) + 
         4*Power(s,6)*(-1 + s2)*s2*
          (1 - Power(t1,2) + Power(s2,2)*(-1 + t1 - t2) + t2 + t1*t2 + 
            s1*(-1 + Power(s2,2) - t1 - s2*t1 + 2*s2*t2) + 
            s2*(t1*(1 + t2) - t2*(2 + t2))) + 
         Power(s1,4)*Power(s2,2)*
          (-16 - 6*Power(s2,5) + 33*t1 - 19*Power(t1,2) + 
            2*Power(t1,3) + Power(s2,4)*(11 + t1) - 
            Power(s2,3)*(-1 + t1)*(-21 + 13*t1 - 32*t2) + 
            s2*(23 + 49*Power(t1,2) - 12*Power(t1,3) - 32*t2 + 
               t1*(-54 + 32*t2)) + 
            Power(s2,2)*(17 + 7*Power(t1,2) + 2*Power(t1,3) + 64*t2 - 
               2*t1*(19 + 32*t2))) - 
         2*Power(s,5)*(5*Power(s2,5)*(-1 + t1 - t2) + 
            (2 + t1)*(-1 + t1 - t2) - 
            Power(s2,4)*(-8*t1 + 5*Power(t1,2) + t2 - 7*t1*t2 + 
               2*Power(t2,2)) + 
            2*Power(s1,2)*s2*
             (2 + Power(s2,3) + t1 - s2*(7 + t1 + 2*t2) + 
               Power(s2,2)*(4 - 2*t1 + 4*t2)) + 
            Power(s2,2)*(-18 + 7*Power(t1,3) - 3*t2 - Power(t2,2) - 
               3*Power(t2,3) + Power(t1,2)*(11 + 3*t2) + 
               t1*(3 - 30*t2 - 7*Power(t2,2))) + 
            s2*(3 - 7*Power(t1,3) + 5*t2 + 2*Power(t2,2) + 
               Power(t1,2)*(5 + 4*t2) + t1*(-1 + 3*t2 + 3*Power(t2,2))) \
+ Power(s2,3)*(22 + 10*t2 + 5*Power(t2,2) + 3*Power(t2,3) - 
               Power(t1,2)*(8 + 7*t2) + t1*(-20 + 13*t2 + 4*Power(t2,2))\
) + s1*(2 + 5*Power(s2,5) + t1 + Power(s2,4)*(-2 - 10*t1 + 3*t2) - 
               s2*(7 + 5*Power(t1,2) + 6*t2 + 5*t1*t2) + 
               Power(s2,3)*(-30 + 18*t1 + 8*Power(t1,2) - 12*t2 - 
                  7*t1*t2 - 11*Power(t2,2)) + 
               Power(s2,2)*(32 - t1 - 7*Power(t1,2) + 7*t2 + 20*t1*t2 + 
                  7*Power(t2,2)))) + 
         Power(s1,3)*s2*(-(Power(-1 + t1,2)*
               (-6 + 8*t1 - 3*Power(t1,2) + 2*Power(t1,3))) - 
            8*Power(s2,6)*(-1 + t1 - 3*t2) + 
            Power(s2,5)*(7 + 5*Power(t1,2) - 44*t2 - 4*t1*(3 + t2)) - 
            s2*(-1 + t1)*(-5 + 2*Power(t1,4) + t1*(5 - 57*t2) + 51*t2 - 
               Power(t1,3)*(7 + 2*t2) + Power(t1,2)*(-5 + 12*t2)) - 
            Power(s2,4)*(-1 + t1)*
             (-58 + 18*Power(t1,2) + 87*t2 + 48*Power(t2,2) - 
               3*t1*(12 + 13*t2)) + 
            Power(s2,2)*(-108 - 4*Power(t1,5) - 45*t2 + 
               48*Power(t2,2) + 6*Power(t1,3)*(17 + 2*t2) + 
               Power(t1,4)*(-7 + 6*t2) - Power(t1,2)*(247 + 111*t2) - 
               6*t1*(-44 - 19*t2 + 8*Power(t2,2))) + 
            Power(s2,3)*(110 + 15*Power(t1,4) - 81*t2 - 96*Power(t2,2) + 
               Power(t1,2)*(61 + 3*t2) - 2*Power(t1,3)*(25 + 7*t2) + 
               4*t1*(-34 + 35*t2 + 24*Power(t2,2)))) + 
         Power(s1,2)*(-(Power(-1 + t1,3)*(2 - t1 + Power(t1,2))) - 
            12*Power(s2,7)*t2*(2 - 2*t1 + 3*t2) - 
            s2*Power(-1 + t1,2)*
             (7 + 2*Power(t1,4) + 14*t2 - 4*Power(t1,3)*(5 + t2) + 
               Power(t1,2)*(25 + 6*t2) - t1*(16 + 19*t2)) + 
            Power(s2,5)*(-1 + t1)*
             (-53 + 5*Power(t1,3) - 175*t2 + 135*Power(t2,2) + 
               32*Power(t2,3) + 7*Power(t1,2)*(3 + 5*t2) + 
               t1*(27 - 88*t2 - 39*Power(t2,2))) + 
            Power(s2,6)*(-20 - 6*Power(t1,3) - 21*t2 + 66*Power(t2,2) - 
               Power(t1,2)*(8 + 15*t2) + t1*(34 + 36*t2 + 6*Power(t2,2))\
) - Power(s2,2)*(-1 + t1)*(166 + 6*Power(t1,5) - 30*t2 - 
               53*Power(t2,2) - 10*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-34 + 46*t2 + 4*Power(t2,2)) - 
               2*Power(t1,2)*(-80 + 21*t2 + 8*Power(t2,2)) + 
               t1*(-288 + 66*t2 + 59*Power(t2,2))) + 
            Power(s2,3)*(-102 + 273*t2 + 3*Power(t2,2) - 
               32*Power(t2,3) + 2*Power(t1,5)*(9 + 5*t2) - 
               6*Power(t1,4)*(3 + 2*t2 + 2*Power(t2,2)) + 
               2*Power(t1,3)*(-43 - 55*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(52 + 405*t2 + 75*Power(t2,2)) + 
               2*t1*(68 - 283*t2 - 21*Power(t2,2) + 16*Power(t2,3))) - 
            Power(s2,4)*(20 + Power(t1,5) + 311*t2 - 147*Power(t2,2) - 
               64*Power(t2,3) + Power(t1,4)*(44 + 30*t2) - 
               8*Power(t1,3)*(19 + 12*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(188 + 101*t2 + 33*Power(t2,2)) + 
               t1*(-101 - 346*t2 + 210*Power(t2,2) + 64*Power(t2,3)))) + 
         s1*(24*Power(s2,7)*Power(t2,2)*(1 - t1 + t2) - 
            Power(-1 + t1,3)*
             (5 + t1 + Power(t1,3) - 3*t2 - Power(t1,2)*(5 + t2)) + 
            s2*Power(-1 + t1,2)*
             (-76 + 29*t2 + 8*Power(t2,2) + Power(t1,4)*(15 + 2*t2) - 
               2*Power(t1,3)*(24 + 13*t2 + Power(t2,2)) + 
               Power(t1,2)*(15 + 33*t2 + Power(t2,2)) - 
               2*t1*(-47 + 21*t2 + 5*Power(t2,2))) + 
            Power(s2,6)*(2 + 44*t2 + 21*Power(t2,2) - 44*Power(t2,3) + 
               Power(t1,3)*(-2 + 8*t2) + 
               Power(t1,2)*(6 + 28*t2 + 15*Power(t2,2)) - 
               2*t1*(3 + 40*t2 + 18*Power(t2,2) + 2*Power(t2,3))) - 
            Power(s2,5)*(-1 + t1)*
             (4 - 110*t2 - 172*Power(t2,2) + 93*Power(t2,3) + 
               8*Power(t2,4) + Power(t1,3)*(4 + 8*t2) + 
               Power(t1,2)*(-4 + 42*t2 + 20*Power(t2,2)) - 
               t1*(4 - 60*t2 + 76*Power(t2,2) + 13*Power(t2,3))) + 
            Power(s2,2)*(-1 + t1)*
             (64 + 257*t2 - 65*Power(t2,2) - 17*Power(t2,3) + 
               Power(t1,5)*(5 + 6*t2) + 
               Power(t1,4)*(7 - 26*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(241 + 53*t2 - 51*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,3)*(-109 + 49*t2 + 43*Power(t2,2) + 
                  2*Power(t2,3)) + 
               t1*(-208 - 339*t2 + 111*Power(t2,2) + 15*Power(t2,3))) - 
            Power(s2,3)*(-144 + 3*Power(t1,6) - 207*t2 + 
               240*Power(t2,2) - 37*Power(t2,3) - 8*Power(t2,4) + 
               2*Power(t1,5)*(6 + 11*t2 + 3*Power(t2,2)) + 
               2*Power(t1,3)*
                (162 - 68*t2 + 7*Power(t2,2) + 6*Power(t2,3)) - 
               Power(t1,4)*(94 + 19*t2 + 23*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(t1,2)*(-573 + 34*t2 + 173*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(472 + 306*t2 - 410*Power(t2,2) + 42*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(s2,4)*(-71 + 18*t2 + 294*Power(t2,2) - 
               119*Power(t2,3) - 16*Power(t2,4) + 
               Power(t1,5)*(8 + 3*t2) + 
               Power(t1,4)*(-9 + 46*t2 + 17*Power(t2,2)) - 
               2*Power(t1,3)*
                (-25 + 81*t2 + 25*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(-176 + 192*t2 + 31*Power(t2,2) + 
                  29*Power(t2,3)) + 
               t1*(198 - 97*t2 - 292*Power(t2,2) + 152*Power(t2,3) + 
                  16*Power(t2,4)))) + 
         Power(s,4)*(-22 + 27*t1 + 2*Power(t1,2) - 7*Power(t1,3) - 
            4*Power(s1,3)*s2*
             (-1 + Power(s2,3) + s2*(7 + t1) + 
               Power(s2,2)*(-9 + t1 - 2*t2)) + 
            6*Power(s2,6)*(-1 + t1 - t2) - 21*t2 + 5*t1*t2 + 
            6*Power(t1,2)*t2 + Power(t2,2) + t1*Power(t2,2) - 
            Power(s2,5)*(18 + 17*Power(t1,2) + 13*t2 + Power(t2,2) - 
               t1*(37 + 22*t2)) + 
            Power(s2,4)*(22 + 9*Power(t1,3) + 15*t2 + 13*Power(t2,2) + 
               8*Power(t2,3) - 2*Power(t1,2)*(25 + 14*t2) + 
               t1*(17 + 3*t2 + 11*Power(t2,2))) + 
            s2*(128 + 18*Power(t1,4) + 2*Power(t1,3)*(-13 + t2) + 
               135*t2 + 7*Power(t2,2) + 
               Power(t1,2)*(29 - 20*t2 - 18*Power(t2,2)) - 
               t1*(149 + 20*t2 - 6*Power(t2,2) + 2*Power(t2,3))) + 
            2*Power(s2,3)*(77 + 54*t2 + 8*Power(t2,2) + 3*Power(t2,3) - 
               Power(t2,4) + 3*Power(t1,3)*(7 + 3*t2) + 
               Power(t1,2)*(54 - 5*t2 + Power(t2,2)) - 
               t1*(153 + 28*t2 + 39*Power(t2,2) + 9*Power(t2,3))) - 
            2*Power(s2,2)*(133 + 9*Power(t1,4) + 101*t2 + 
               10*Power(t2,2) + 11*Power(t2,3) - Power(t2,4) + 
               5*Power(t1,3)*(5 + 2*t2) + 
               Power(t1,2)*(8 - 54*t2 - 8*Power(t2,2)) - 
               t1*(176 - 13*t2 + 22*Power(t2,2) + 10*Power(t2,3))) + 
            2*Power(s1,2)*(1 + 5*Power(s2,5) + 
               Power(s2,4)*(14 - 20*t1 + 15*t2) - 
               s2*(-17 + 6*Power(t1,2) + 5*t2 + t1*t2) + 
               Power(s2,3)*(-30 + 16*Power(t1,2) - 20*t2 - 
                  9*Power(t2,2) - 3*t1*(3 + 8*t2)) + 
               Power(s2,2)*(-19 + 2*Power(t1,2) + 10*t2 + Power(t2,2) + 
                  t1*(29 + 21*t2))) + 
            s1*(20 + 6*Power(s2,6) - 7*Power(t1,2) + 
               Power(s2,5)*(12 - 25*t1 - 7*t2) - 3*t2 - t1*(3 + t2) + 
               Power(s2,4)*(-58 + 43*Power(t1,2) - 27*t2 - 
                  34*Power(t2,2) + t1*(29 + 13*t2)) + 
               s2*(-166 + 6*Power(t1,3) - 39*t2 + 30*Power(t1,2)*t2 + 
                  6*Power(t2,2) + t1*(63 - 8*t2 + 4*Power(t2,2))) + 
               2*Power(s2,2)*
                (164 + 25*Power(t1,3) + Power(t1,2)*(8 - 34*t2) + 
                  21*t2 + 15*Power(t2,2) - 2*Power(t2,3) - 
                  t1*(129 + 18*t2 + 29*Power(t2,2))) - 
               2*Power(s2,3)*(59 + 12*Power(t1,3) - 5*t2 + Power(t2,2) - 
                  6*Power(t2,3) + Power(t1,2)*(66 + 9*t2) - 
                  t1*(109 + 56*t2 + 35*Power(t2,2))))) + 
         Power(s,3)*(49 + 4*Power(s1,4)*Power(s2,2)*
             (1 - 4*s2 + Power(s2,2)) - 96*t1 + 38*Power(t1,2) + 
            18*Power(t1,3) - 9*Power(t1,4) + 45*t2 - 44*t1*t2 - 
            5*Power(t1,2)*t2 + 6*Power(t1,3)*t2 - 4*Power(t2,2) - 
            t1*Power(t2,2) + 3*Power(t1,2)*Power(t2,2) + 
            Power(s2,6)*(18 + 6*Power(t1,2) + 33*t2 + 13*Power(t2,2) - 
               t1*(24 + 25*t2)) - 
            Power(s2,5)*(5 + 9*Power(t1,3) + 42*t2 + 3*Power(t2,2) + 
               6*Power(t2,3) - Power(t1,2)*(65 + 33*t2) + 
               t1*(51 + 27*t2 + 2*Power(t2,2))) + 
            s2*(10*Power(t1,5) + Power(t1,4)*(-23 + 14*t2) + 
               Power(t1,3)*(9 - 55*t2 - 18*Power(t2,2)) + 
               5*(-77 - 72*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(-338 + 61*t2 + 31*Power(t2,2) - 
                  6*Power(t2,3)) + 
               t1*(727 + 304*t2 - 6*Power(t2,2) + 7*Power(t2,3))) + 
            Power(s2,4)*(-23 + 4*Power(t1,4) + 
               Power(t1,2)*(11 - 51*t2) - 63*t2 - 152*Power(t2,2) - 
               6*Power(t2,3) + 2*Power(t2,4) - 
               Power(t1,3)*(70 + 31*t2) + 
               t1*(78 + 231*t2 + 51*Power(t2,2) + 25*Power(t2,3))) + 
            Power(s2,3)*(-274 - 62*t2 + 236*Power(t2,2) + 
               14*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,4)*(37 + 10*t2) + 
               Power(t1,3)*(142 + 59*t2 + 14*Power(t2,2)) - 
               Power(t1,2)*(565 + 86*t2 + 131*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(660 - 41*t2 + 82*Power(t2,2) - 11*Power(t2,3) - 
                  6*Power(t2,4))) + 
            Power(s2,2)*(628 - 10*Power(t1,5) + 417*t2 - 
               123*Power(t2,2) + 30*Power(t2,3) - 8*Power(t2,4) - 
               3*Power(t1,4)*(19 + 8*t2) + 
               Power(t1,3)*(22 + 85*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(711 - 64*t2 + 113*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-1294 - 318*t2 - 156*Power(t2,2) - 53*Power(t2,3) + 
                  6*Power(t2,4))) - 
            2*Power(s1,3)*s2*
             (9 + 3*Power(s2,4) - 7*t1 + Power(t1,2) + 
               s2*(7*Power(t1,2) - t1*(-34 + t2) + 7*(-9 + t2)) + 
               Power(s2,3)*(-7 - 3*t1 + 10*t2) + 
               Power(s2,2)*(50 + 8*Power(t1,2) - 9*t2 - 
                  3*t1*(16 + 5*t2))) + 
            Power(s1,2)*(-8 + 9*Power(s2,6) + 7*t1 - Power(t1,2) + 
               Power(s2,5)*(-37 + 34*t1 + 2*t2) + 
               s2*(-45 - 14*Power(t1,3) + t1*(24 - 29*t2) + 
                  Power(t1,2)*(57 - 2*t2) + 44*t2) + 
               Power(s2,4)*(-114 + 149*t1 - 100*Power(t1,2) - 78*t2 + 
                  57*t1*t2 + 30*Power(t2,2)) + 
               Power(s2,2)*(-239 - 18*Power(t1,3) - 182*t2 + 
                  8*Power(t2,2) + Power(t1,2)*(31 + 88*t2) + 
                  t1*(96 + 7*t2 + 2*Power(t2,2))) + 
               Power(s2,3)*(434 + 48*Power(t1,3) + 
                  Power(t1,2)*(69 - 38*t2) + 214*t2 + 18*Power(t2,2) - 
                  t1*(406 + 147*t2 + 66*Power(t2,2)))) + 
            s1*(-41 - 10*Power(t1,3) + 
               Power(s2,6)*(-33 + 25*t1 - 22*t2) + t1*(32 - 6*t2) + 
               Power(t1,2)*(17 - 2*t2) + 12*t2 + 
               Power(s2,5)*(56 - 41*Power(t1,2) + 36*t2 + 
                  10*Power(t2,2) - 7*t1*(-3 + 4*t2)) + 
               s2*(448 - 2*Power(t1,4) + 12*t2 - 26*Power(t2,2) + 
                  8*Power(t1,3)*(5 + 4*t2) + 
                  t1*(-461 - 2*t2 + 8*Power(t2,2)) + 
                  Power(t1,2)*(11 - 96*t2 + 10*Power(t2,2))) + 
               Power(s2,4)*(131 + 42*Power(t1,3) + 264*t2 + 
                  70*Power(t2,2) - 16*Power(t2,3) + 
                  Power(t1,2)*(149 + 90*t2) - 
                  4*t1*(102 + 47*t2 + 22*Power(t2,2))) + 
               Power(s2,2)*(-509 + 66*Power(t1,4) + 
                  Power(t1,3)*(6 - 38*t2) + 344*t2 + 26*Power(t2,2) + 
                  10*Power(t2,3) - 
                  2*Power(t1,2)*(162 + 25*t2 + 49*Power(t2,2)) + 
                  t1*(665 + 36*t2 + 114*Power(t2,2) - 10*Power(t2,3))) - 
               2*Power(s2,3)*(34 + 8*Power(t1,4) + 299*t2 + 
                  64*Power(t2,2) + 13*Power(t2,3) + 
                  Power(t1,3)*(103 + 29*t2) - 
                  Power(t1,2)*(118 + 61*t2 + 36*Power(t2,2)) - 
                  t1*(87 + 94*t2 + 31*Power(t2,2) + 21*Power(t2,3))))) + 
         Power(s,2)*(2*Power(s1,4)*Power(s2,2)*
             (-12 + 7*Power(s2,3) + s2*(43 - 22*t1) + 5*t1 + 
               Power(s2,2)*(-26 + 5*t1)) - 
            2*Power(s2,7)*t2*(7 - 7*t1 + 6*t2) + 
            Power(s2,6)*(-24 + 13*t2 + 57*Power(t2,2) + 
               20*Power(t2,3) - 3*Power(t1,2)*(8 + 7*t2) + 
               t1*(48 + 8*t2 - 17*Power(t2,2))) - 
            (-1 + t1)*(-55 + 5*Power(t1,4) - 49*t2 + 6*Power(t2,2) - 
               2*Power(t1,3)*(10 + t2) - 
               3*Power(t1,2)*(8 - 3*t2 + Power(t2,2)) + 
               t1*(94 + 36*t2 + 3*Power(t2,2))) - 
            Power(s2,5)*(-39 + 2*Power(t1,4) - 17*t2 + 68*Power(t2,2) - 
               33*Power(t2,3) + Power(t2,4) - 
               Power(t1,3)*(47 + 26*t2) + 
               Power(t1,2)*(49 - 21*t2 - 12*Power(t2,2)) + 
               t1*(35 + 64*t2 + 40*Power(t2,2) + 11*Power(t2,3))) + 
            s2*(507 + 2*Power(t1,6) + 432*t2 - 75*Power(t2,2) + 
               2*Power(t1,5)*(-4 + 5*t2) - 
               2*Power(t1,4)*(28 + 30*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-201 + 172*t2 + 32*Power(t2,2) - 
                  6*Power(t2,3)) - 
               t1*(1343 + 670*t2 - 82*Power(t2,2) + 9*Power(t2,3)) + 
               Power(t1,2)*(1099 + 116*t2 - 31*Power(t2,2) + 
                  16*Power(t2,3))) + 
            Power(s2,4)*(3 + Power(t1,5) + 152*t2 + 213*Power(t2,2) - 
               165*Power(t2,3) - 5*Power(t2,4) - 
               Power(t1,4)*(37 + 14*t2) - 
               Power(t1,3)*(30 + 97*t2 + 17*Power(t2,2)) + 
               Power(t1,2)*(170 + 326*t2 + 43*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*(-107 - 367*t2 - 53*Power(t2,2) - 8*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s2,3)*(242 - 199*t2 - 467*Power(t2,2) + 
               127*Power(t2,3) - 5*Power(t2,4) + 
               2*Power(t1,5)*(6 + t2) + 
               2*Power(t1,4)*(45 + 32*t2 + 5*Power(t2,2)) - 
               2*Power(t1,3)*
                (195 + 21*t2 + 32*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(704 - 369*t2 + 69*Power(t2,2) - 
                  44*Power(t2,3) - 6*Power(t2,4)) + 
               2*t1*(-329 + 272*t2 + 135*Power(t2,2) + 38*Power(t2,3) + 
                  6*Power(t2,4))) - 
            Power(s2,2)*(688 + 2*Power(t1,6) + 272*t2 - 
               354*Power(t2,2) + 63*Power(t2,3) - 11*Power(t2,4) + 
               4*Power(t1,5)*(8 + 3*t2) + 
               4*Power(t1,4)*(-15 - 6*t2 + Power(t2,2)) - 
               2*Power(t1,3)*
                (249 - 16*t2 + 55*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(1726 + 46*t2 + 279*Power(t2,2) + 
                  44*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(-1890 - 338*t2 + 119*Power(t2,2) - 48*Power(t2,3) + 
                  18*Power(t2,4))) - 
            Power(s1,3)*s2*(-30 + 20*Power(s2,5) + 48*t1 - 
               23*Power(t1,2) + 6*Power(t1,3) + 
               Power(s2,4)*(8 - 8*t1 + 41*t2) + 
               Power(s2,2)*(86 + 24*Power(t1,3) + t1*(46 - 48*t2) + 
                  217*t2 - 7*Power(t1,2)*(11 + 6*t2)) + 
               s2*(18*Power(t1,3) + Power(t1,2)*(43 - 6*t2) - 
                  79*(-2 + t2) + 6*t1*(-40 + 7*t2)) + 
               Power(s2,3)*(-146 - 39*Power(t1,2) - 179*t2 + 
                  2*t1*(53 + 27*t2))) + 
            Power(s1,2)*(-12*Power(s2,7) - 
               3*(-4 + 7*t1 - 4*Power(t1,2) + Power(t1,3)) + 
               Power(s2,6)*(69 - 29*t1 + 60*t2) + 
               Power(s2,5)*(-47 + 75*Power(t1,2) + 61*t2 + 
                  39*Power(t2,2) - t1*(124 + 39*t2)) + 
               s2*(13 - 10*Power(t1,4) - 72*t2 + 
                  2*Power(t1,3)*(53 + 3*t2) - 
                  Power(t1,2)*(109 + 42*t2) + t1*(2 + 111*t2)) + 
               Power(s2,2)*(621 - 38*Power(t1,4) + 197*t2 - 
                  75*Power(t2,2) + 4*Power(t1,3)*(4 + 23*t2) - 
                  6*Power(t1,2)*(-55 + 17*t2 + Power(t2,2)) + 
                  3*t1*(-289 - 92*t2 + 12*Power(t2,2))) + 
               Power(s2,3)*(32*Power(t1,4) + 
                  2*Power(t1,3)*(73 + 7*t2) + 
                  6*t1*(153 + 40*t2 + 8*Power(t2,2)) - 
                  6*Power(t1,2)*(106 + 31*t2 + 15*Power(t2,2)) + 
                  3*(-214 + 81*t2 + 57*Power(t2,2))) + 
               Power(s2,4)*(130 - 97*Power(t1,3) + 
                  Power(t1,2)*(112 - 6*t2) - 409*t2 - 207*Power(t2,2) + 
                  t1*(41 + 108*t2 + 84*Power(t2,2)))) - 
            s1*((-1 + t1)*(43 - 27*Power(t1,2) + 8*Power(t1,3) + 
                  6*t1*(-3 + t2) - 18*t2) + 
               2*Power(s2,7)*(-7 + 7*t1 - 12*t2) + 
               Power(s2,6)*(13 - 21*Power(t1,2) + t1*(8 - 46*t2) + 
                  126*t2 + 60*Power(t2,2)) + 
               Power(s2,5)*(42 + 29*Power(t1,3) - 115*t2 + 
                  86*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t1,2)*(40 + 87*t2) - 
                  t1*(111 + 164*t2 + 42*Power(t2,2))) + 
               s2*(550 + 2*Power(t1,5) - 74*t2 - 42*Power(t2,2) - 
                  Power(t1,4)*(75 + 16*t2) + 
                  Power(t1,3)*(177 + 150*t2 - 6*Power(t2,2)) + 
                  Power(t1,2)*(301 - 176*t2 - 3*Power(t2,2)) + 
                  t1*(-955 + 120*t2 + 54*Power(t2,2))) - 
               Power(s2,4)*(-187 + 15*Power(t1,4) - 361*t2 + 
                  428*Power(t2,2) + 85*Power(t2,3) + 
                  5*Power(t1,3)*(43 + 24*t2) - 
                  Power(t1,2)*(600 + 185*t2 + 57*Power(t2,2)) + 
                  t1*(557 + 54*t2 + 6*Power(t2,2) - 46*Power(t2,3))) + 
               Power(s2,2)*(-245 - 38*Power(t1,5) + 
                  Power(t1,4)*(3 - 14*t2) + 925*t2 - 24*Power(t2,2) - 
                  9*Power(t2,3) + 
                  2*Power(t1,3)*(59 + 40*t2 + 43*Power(t2,2)) + 
                  Power(t1,2)*
                   (-152 + 9*t2 - 189*Power(t2,2) + 6*Power(t2,3)) - 
                  2*t1*(-157 + 438*t2 - 6*Power(t2,2) + 7*Power(t2,3))) \
+ Power(s2,3)*(-394 + 4*Power(t1,5) - 1057*t2 + 284*Power(t2,2) + 
                  35*Power(t2,3) + Power(t1,4)*(143 + 46*t2) - 
                  2*Power(t1,3)*(13 - 9*t2 + 8*Power(t2,2)) + 
                  2*t1*(557 + 514*t2 + 135*Power(t2,2) + 
                     32*Power(t2,3)) - 
                  Power(t1,2)*
                   (841 + 399*t2 + 153*Power(t2,2) + 54*Power(t2,3))))) + 
         s*(-8*Power(s1,5)*Power(-1 + s2,2)*Power(s2,3) - 
            2*Power(s2,7)*t2*(-1 - Power(t1,2) + t1*(2 - 9*t2) + 9*t2 + 
               8*Power(t2,2)) + 
            Power(s1,4)*Power(s2,2)*
             (36 + 7*Power(s2,4) - 43*t1 + 8*Power(t1,2) + 
               Power(s2,2)*(13 - 45*t1 + 8*Power(t1,2) - 64*t2) + 
               Power(s2,3)*(1 + t1 + 32*t2) + 
               s2*(-81 + 135*t1 - 40*Power(t1,2) + 32*t2)) + 
            Power(s2,6)*t2*(-44 - 8*Power(t1,3) - 4*t2 + 
               41*Power(t2,2) + 7*Power(t2,3) - 
               14*Power(t1,2)*(2 + t2) + t1*(80 + 18*t2 - 3*Power(t2,2))) \
- Power(-1 + t1,2)*(-31 - 12*Power(t1,3) + Power(t1,4) - 27*t2 + 
               4*Power(t2,2) - Power(t1,2)*(4 - 7*t2 + Power(t2,2)) + 
               t1*(46 + 14*t2 + 3*Power(t2,2))) + 
            s2*(-1 + t1)*(317 + 250*t2 - 67*Power(t2,2) + 
               Power(t1,5)*(-1 + 2*t2) - Power(t1,4)*(62 + 27*t2) + 
               Power(t1,3)*(-13 + 116*t2 + 15*Power(t2,2) - 
                  2*Power(t2,3)) - 
               t1*(774 + 316*t2 - 69*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(533 - 25*t2 - 13*Power(t2,2) + 9*Power(t2,3))\
) + Power(s2,5)*(-12 + 66*t2 + 35*Power(t2,2) - 88*Power(t2,3) + 
               19*Power(t2,4) + Power(t1,4)*(4 + 5*t2) + 
               Power(t1,3)*t2*(55 + 14*t2) - 
               Power(t1,2)*(24 + 59*t2 + 41*Power(t2,2) + 
                  2*Power(t2,3)) - 
               t1*(-32 + 67*t2 + 8*Power(t2,2) - 26*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s2,3)*(-111 + Power(t1,6) + 282*t2 + 344*Power(t2,2) - 
               186*Power(t2,3) + 31*Power(t2,4) + 
               3*Power(t1,4)*(-31 + 4*t2 + Power(t2,2)) + 
               Power(t1,5)*(21 + 17*t2 + 2*Power(t2,2)) + 
               t1*(357 - 739*t2 - 362*Power(t2,2) + 117*Power(t2,3) - 
                  5*Power(t2,4)) + 
               Power(t1,3)*(254 - 250*t2 + 4*Power(t2,2) - 
                  31*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,2)*(-429 + 678*t2 + 9*Power(t2,2) + 
                  84*Power(t2,3) + 6*Power(t2,4))) - 
            Power(s2,4)*(-6 + 171*t2 + 118*Power(t2,2) - 
               202*Power(t2,3) + 51*Power(t2,4) + Power(t1,5)*(5 + t2) + 
               Power(t1,4)*(16 + 47*t2 + 10*Power(t2,2)) - 
               Power(t1,3)*(72 + 88*t2 - 9*Power(t2,2) + 
                  5*Power(t2,3)) - 
               2*Power(t1,2)*
                (-35 - 101*t2 + 67*Power(t2,2) + Power(t2,3) + 
                  3*Power(t2,4)) + 
               t1*(-13 - 333*t2 - 3*Power(t2,2) + 141*Power(t2,3) + 
                  11*Power(t2,4))) + 
            Power(s2,2)*(363 - 318*Power(t2,2) + 79*Power(t2,3) - 
               6*Power(t2,4) - Power(t1,6)*(7 + 2*t2) + 
               Power(t1,5)*(39 - 3*t2 - 2*Power(t2,2)) + 
               Power(t1,4)*(85 + 14*t2 + 47*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(1551 - 180*t2 + 91*Power(t2,2) + 
                  26*Power(t2,3) - 12*Power(t2,4)) + 
               Power(t1,3)*(-782 + 80*t2 - 200*Power(t2,2) - 
                  17*Power(t2,3) + 2*Power(t2,4)) + 
               t1*(-1249 + 91*t2 + 382*Power(t2,2) - 100*Power(t2,3) + 
                  17*Power(t2,4))) + 
            Power(s1,3)*s2*(-22 + 16*Power(s2,6) + 54*t1 - 
               46*Power(t1,2) + 20*Power(t1,3) - 6*Power(t1,4) + 
               Power(s2,5)*(-41 + 3*t1 - 28*t2) - 
               2*Power(s2,4)*
                (-39 + 2*Power(t1,2) + 11*t2 + 24*Power(t2,2) + 
                  t1*(5 + t2)) + 
               2*Power(s2,3)*
                (-106 + 22*Power(t1,3) - 3*t2 + 48*Power(t2,2) - 
                  Power(t1,2)*(85 + 24*t2) + t1*(135 + 91*t2)) + 
               s2*(65 - 10*Power(t1,4) - 116*t2 + 
                  6*Power(t1,3)*(1 + t2) - 14*Power(t1,2)*(-8 + 3*t2) + 
                  t1*(-163 + 148*t2)) - 
               2*Power(s2,2)*(-114 + 8*Power(t1,4) - 94*t2 + 
                  24*Power(t2,2) - 3*Power(t1,2)*(26 + 7*t2) - 
                  Power(t1,3)*(5 + 13*t2) + t1*(181 + 164*t2))) + 
            Power(s1,2)*(-(Power(-1 + t1,2)*
                  (8 - 5*t1 + 3*Power(t1,2))) + 
               6*Power(s2,7)*(-3 + 3*t1 - 8*t2) + 
               Power(s2,6)*(-16 - 26*Power(t1,2) + t1*(42 - 9*t2) + 
                  123*t2 + 42*Power(t2,2)) - 
               s2*(-1 + t1)*(13 + 6*Power(t1,4) + 52*t2 - 
                  5*Power(t1,3)*(15 + 2*t2) - 3*t1*(13 + 25*t2) + 
                  Power(t1,2)*(91 + 27*t2)) + 
               Power(s2,5)*(39 + 56*Power(t1,3) - 256*t2 + 
                  60*Power(t2,2) + 32*Power(t2,3) - 
                  Power(t1,2)*(121 + 6*t2) + t1*(26 + 70*t2)) + 
               Power(s2,2)*(-538 - 26*Power(t1,5) - 13*t2 + 
                  118*Power(t2,2) + Power(t1,4)*(31 + 48*t2) + 
                  t1*(1138 + 86*t2 - 150*Power(t2,2)) - 
                  5*Power(t1,3)*(-44 + 29*t2 + 2*Power(t2,2)) + 
                  Power(t1,2)*(-825 - 6*t2 + 48*Power(t2,2))) - 
               Power(s2,4)*(62 + 34*Power(t1,4) - 606*t2 + 
                  78*Power(t2,2) + 64*Power(t2,3) + 
                  Power(t1,3)*(53 + 63*t2) - 
                  2*Power(t1,2)*(175 + 141*t2 + 39*Power(t2,2)) + 
                  3*t1*(67 + 207*t2 + 80*Power(t2,2))) + 
               Power(s2,3)*(398 + 8*Power(t1,5) - 584*t2 - 
                  102*Power(t2,2) + 32*Power(t2,3) + 
                  Power(t1,4)*(93 + 30*t2) + 
                  Power(t1,2)*(393 - 84*t2 + 42*Power(t2,2)) - 
                  Power(t1,3)*(308 + 91*t2 + 54*Power(t2,2)) + 
                  t1*(-584 + 681*t2 + 246*Power(t2,2)))) + 
            s1*(-(Power(-1 + t1,2)*
                  (23 + 4*Power(t1,3) + 2*t1*(-1 + t2) - 12*t2 - 
                    Power(t1,2)*(19 + 2*t2))) - 
               2*Power(s2,7)*(1 + Power(t1,2) - 18*t2 - 24*Power(t2,2) + 
                  2*t1*(-1 + 9*t2)) + 
               s2*(-1 + t1)*(-326 + 88*t2 + 30*Power(t2,2) + 
                  6*Power(t1,4)*(9 + t2) + 
                  t1*(481 - 132*t2 - 38*Power(t2,2)) + 
                  2*Power(t1,2)*(-23 + 64*t2 + 2*Power(t2,2)) - 
                  Power(t1,3)*(163 + 98*t2 + 2*Power(t2,2))) + 
               Power(s2,6)*(44 + 8*Power(t1,3) + 20*t2 - 
                  123*Power(t2,2) - 28*Power(t2,3) + 
                  4*Power(t1,2)*(7 + 10*t2) + 
                  t1*(-80 - 60*t2 + 9*Power(t2,2))) - 
               Power(s2,5)*(50 + 3*Power(t1,4) + 70*t2 - 
                  266*Power(t2,2) + 58*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t1,3)*(77 + 74*t2) - 
                  Power(t1,2)*(113 + 174*t2 + 12*Power(t2,2)) + 
                  t1*(-17 + 30*t2 + 86*Power(t2,2) - 2*Power(t2,3))) - 
               2*Power(s2,3)*(195 + 366*t2 - 271*Power(t2,2) + 
                  18*Power(t2,3) + 4*Power(t2,4) + 
                  Power(t1,5)*(20 + 6*t2) + 
                  Power(t1,4)*(32 + 37*t2 + 7*Power(t2,2)) - 
                  Power(t1,3)*
                   (326 + 120*t2 + 56*Power(t2,2) + 15*Power(t2,3)) + 
                  t1*(-592 - 450*t2 + 218*Power(t2,2) + 
                     24*Power(t2,3)) + 
                  Power(t1,2)*
                   (671 + 161*t2 + 78*Power(t2,2) + 25*Power(t2,3))) + 
               Power(s2,2)*(68 + 8*Power(t1,6) + 818*t2 - 
                  131*Power(t2,2) - 32*Power(t2,3) + 
                  Power(t1,5)*(6 + 22*t2) - 
                  2*Power(t1,4)*(-7 + 42*t2 + 20*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (-291 - 277*t2 + 66*Power(t2,2) + Power(t2,3)) + 
                  2*Power(t1,3)*
                   (-155 + 36*t2 + 78*Power(t2,2) + Power(t2,3)) + 
                  t1*(-368 - 1382*t2 + 177*Power(t2,2) + 28*Power(t2,3))) \
+ Power(s2,4)*(171 + 196*t2 - 596*Power(t2,2) + 122*Power(t2,3) + 
                  16*Power(t2,4) + Power(t1,4)*(99 + 52*t2) + 
                  2*Power(t1,3)*(-119 + 11*t2 + 7*Power(t2,2)) - 
                  2*Power(t1,2)*
                   (-175 + 206*t2 + 57*Power(t2,2) + 22*Power(t2,3)) + 
                  2*t1*(-191 + 71*t2 + 246*Power(t2,2) + 57*Power(t2,3))))\
))*R1q(s2))/((-1 + s1)*Power(-1 + s2,2)*s2*(-s + s2 - t1)*
       Power(-1 + s + t1,3)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       (-1 + s - s*s2 + s1*s2 + t1 - s2*t2)) - 
    (8*(70 + 114*s2 + 6*Power(s2,2) - 40*Power(s2,3) + 2*Power(s2,5) + 
         4*Power(s1,7)*s2*(2*Power(s2,2) - 3*s2*(-1 + t1) + 
            Power(-1 + t1,2))*(-1 + t1) - 454*t1 - 550*s2*t1 + 
         84*Power(s2,2)*t1 + 170*Power(s2,3)*t1 - 10*Power(s2,4)*t1 - 
         8*Power(s2,5)*t1 + 1194*Power(t1,2) + 940*s2*Power(t1,2) - 
         440*Power(s2,2)*Power(t1,2) - 262*Power(s2,3)*Power(t1,2) + 
         40*Power(s2,4)*Power(t1,2) + 12*Power(s2,5)*Power(t1,2) - 
         1584*Power(t1,3) - 482*s2*Power(t1,3) + 
         786*Power(s2,2)*Power(t1,3) + 148*Power(s2,3)*Power(t1,3) - 
         60*Power(s2,4)*Power(t1,3) - 8*Power(s2,5)*Power(t1,3) + 
         1002*Power(t1,4) - 458*s2*Power(t1,4) - 
         634*Power(s2,2)*Power(t1,4) + 28*Power(s2,3)*Power(t1,4) + 
         40*Power(s2,4)*Power(t1,4) + 2*Power(s2,5)*Power(t1,4) - 
         78*Power(t1,5) + 718*s2*Power(t1,5) + 
         200*Power(s2,2)*Power(t1,5) - 62*Power(s2,3)*Power(t1,5) - 
         10*Power(s2,4)*Power(t1,5) - 274*Power(t1,6) - 
         328*s2*Power(t1,6) + 12*Power(s2,2)*Power(t1,6) + 
         18*Power(s2,3)*Power(t1,6) + 148*Power(t1,7) + 
         42*s2*Power(t1,7) - 14*Power(s2,2)*Power(t1,7) - 
         24*Power(t1,8) + 4*s2*Power(t1,8) + 204*t2 + 427*s2*t2 + 
         201*Power(s2,2)*t2 - 103*Power(s2,3)*t2 - 67*Power(s2,4)*t2 + 
         16*Power(s2,5)*t2 + 2*Power(s2,6)*t2 - 1033*t1*t2 - 
         1760*s2*t1*t2 - 463*Power(s2,2)*t1*t2 + 524*Power(s2,3)*t1*t2 + 
         154*Power(s2,4)*t1*t2 - 56*Power(s2,5)*t1*t2 - 
         6*Power(s2,6)*t1*t2 + 1943*Power(t1,2)*t2 + 
         2392*s2*Power(t1,2)*t2 - 205*Power(s2,2)*Power(t1,2)*t2 - 
         914*Power(s2,3)*Power(t1,2)*t2 - 40*Power(s2,4)*Power(t1,2)*t2 + 
         72*Power(s2,5)*Power(t1,2)*t2 + 6*Power(s2,6)*Power(t1,2)*t2 - 
         1388*Power(t1,3)*t2 - 488*s2*Power(t1,3)*t2 + 
         1414*Power(s2,2)*Power(t1,3)*t2 + 
         604*Power(s2,3)*Power(t1,3)*t2 - 
         134*Power(s2,4)*Power(t1,3)*t2 - 40*Power(s2,5)*Power(t1,3)*t2 - 
         2*Power(s2,6)*Power(t1,3)*t2 - 376*Power(t1,4)*t2 - 
         1753*s2*Power(t1,4)*t2 - 1387*Power(s2,2)*Power(t1,4)*t2 - 
         23*Power(s2,3)*Power(t1,4)*t2 + 107*Power(s2,4)*Power(t1,4)*t2 + 
         8*Power(s2,5)*Power(t1,4)*t2 + 1257*Power(t1,5)*t2 + 
         1700*s2*Power(t1,5)*t2 + 451*Power(s2,2)*Power(t1,5)*t2 - 
         112*Power(s2,3)*Power(t1,5)*t2 - 20*Power(s2,4)*Power(t1,5)*t2 - 
         799*Power(t1,6)*t2 - 578*s2*Power(t1,6)*t2 - 
         Power(s2,2)*Power(t1,6)*t2 + 24*Power(s2,3)*Power(t1,6)*t2 + 
         212*Power(t1,7)*t2 + 60*s2*Power(t1,7)*t2 - 
         10*Power(s2,2)*Power(t1,7)*t2 - 20*Power(t1,8)*t2 + 
         74*Power(t2,2) + 398*s2*Power(t2,2) + 
         430*Power(s2,2)*Power(t2,2) - 60*Power(s2,3)*Power(t2,2) - 
         202*Power(s2,4)*Power(t2,2) - 14*Power(s2,5)*Power(t2,2) + 
         24*Power(s2,6)*Power(t2,2) - 148*t1*Power(t2,2) - 
         1122*s2*t1*Power(t2,2) - 734*Power(s2,2)*t1*Power(t2,2) + 
         706*Power(s2,3)*t1*Power(t2,2) + 
         520*Power(s2,4)*t1*Power(t2,2) - 44*Power(s2,5)*t1*Power(t2,2) - 
         46*Power(s2,6)*t1*Power(t2,2) - 314*Power(t1,2)*Power(t2,2) + 
         270*s2*Power(t1,2)*Power(t2,2) - 
         944*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         1594*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         332*Power(s2,4)*Power(t1,2)*Power(t2,2) + 
         132*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         20*Power(s2,6)*Power(t1,2)*Power(t2,2) + 
         1272*Power(t1,3)*Power(t2,2) + 2206*s2*Power(t1,3)*Power(t2,2) + 
         2906*Power(s2,2)*Power(t1,3)*Power(t2,2) + 
         1274*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         98*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         76*Power(s2,5)*Power(t1,3)*Power(t2,2) + 
         2*Power(s2,6)*Power(t1,3)*Power(t2,2) - 
         1592*Power(t1,4)*Power(t2,2) - 3022*s2*Power(t1,4)*Power(t2,2) - 
         2244*Power(s2,2)*Power(t1,4)*Power(t2,2) - 
         284*Power(s2,3)*Power(t1,4)*Power(t2,2) + 
         122*Power(s2,4)*Power(t1,4)*Power(t2,2) + 
         2*Power(s2,5)*Power(t1,4)*Power(t2,2) + 
         950*Power(t1,5)*Power(t2,2) + 1600*s2*Power(t1,5)*Power(t2,2) + 
         636*Power(s2,2)*Power(t1,5)*Power(t2,2) - 
         48*Power(s2,3)*Power(t1,5)*Power(t2,2) - 
         10*Power(s2,4)*Power(t1,5)*Power(t2,2) - 
         272*Power(t1,6)*Power(t2,2) - 362*s2*Power(t1,6)*Power(t2,2) - 
         50*Power(s2,2)*Power(t1,6)*Power(t2,2) + 
         6*Power(s2,3)*Power(t1,6)*Power(t2,2) + 
         30*Power(t1,7)*Power(t2,2) + 32*s2*Power(t1,7)*Power(t2,2) - 
         98*Power(t2,3) - 116*s2*Power(t2,3) + 
         53*Power(s2,2)*Power(t2,3) - 23*Power(s2,3)*Power(t2,3) - 
         225*Power(s2,4)*Power(t2,3) - 116*Power(s2,5)*Power(t2,3) + 
         17*Power(s2,6)*Power(t2,3) + 8*Power(s2,7)*Power(t2,3) + 
         498*t1*Power(t2,3) + 849*s2*t1*Power(t2,3) + 
         586*Power(s2,2)*t1*Power(t2,3) + 
         721*Power(s2,3)*t1*Power(t2,3) + 
         691*Power(s2,4)*t1*Power(t2,3) + 
         138*Power(s2,5)*t1*Power(t2,3) - 32*Power(s2,6)*t1*Power(t2,3) - 
         8*Power(s2,7)*t1*Power(t2,3) - 972*Power(t1,2)*Power(t2,3) - 
         2024*s2*Power(t1,2)*Power(t2,3) - 
         2028*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         1610*Power(s2,3)*Power(t1,2)*Power(t2,3) - 
         594*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         4*Power(s2,5)*Power(t1,2)*Power(t2,3) + 
         15*Power(s2,6)*Power(t1,2)*Power(t2,3) + 
         883*Power(t1,3)*Power(t2,3) + 2155*s2*Power(t1,3)*Power(t2,3) + 
         2234*Power(s2,2)*Power(t1,3)*Power(t2,3) + 
         1135*Power(s2,3)*Power(t1,3)*Power(t2,3) + 
         119*Power(s2,4)*Power(t1,3)*Power(t2,3) - 
         18*Power(s2,5)*Power(t1,3)*Power(t2,3) - 
         321*Power(t1,4)*Power(t2,3) - 1086*s2*Power(t1,4)*Power(t2,3) - 
         1015*Power(s2,2)*Power(t1,4)*Power(t2,3) - 
         237*Power(s2,3)*Power(t1,4)*Power(t2,3) + 
         9*Power(s2,4)*Power(t1,4)*Power(t2,3) - 
         27*Power(t1,5)*Power(t2,3) + 254*s2*Power(t1,5)*Power(t2,3) + 
         182*Power(s2,2)*Power(t1,5)*Power(t2,3) + 
         14*Power(s2,3)*Power(t1,5)*Power(t2,3) + 
         43*Power(t1,6)*Power(t2,3) - 32*s2*Power(t1,6)*Power(t2,3) - 
         12*Power(s2,2)*Power(t1,6)*Power(t2,3) - 
         6*Power(t1,7)*Power(t2,3) - 24*Power(t2,4) - 
         184*s2*Power(t2,4) - 281*Power(s2,2)*Power(t2,4) - 
         182*Power(s2,3)*Power(t2,4) - 130*Power(s2,4)*Power(t2,4) - 
         94*Power(s2,5)*Power(t2,4) - 19*Power(s2,6)*Power(t2,4) + 
         6*Power(s2,7)*Power(t2,4) + 66*t1*Power(t2,4) + 
         627*s2*t1*Power(t2,4) + 938*Power(s2,2)*t1*Power(t2,4) + 
         639*Power(s2,3)*t1*Power(t2,4) + 
         319*Power(s2,4)*t1*Power(t2,4) + 
         122*Power(s2,5)*t1*Power(t2,4) + 7*Power(s2,6)*t1*Power(t2,4) - 
         26*Power(t1,2)*Power(t2,4) - 710*s2*Power(t1,2)*Power(t2,4) - 
         1012*Power(s2,2)*Power(t1,2)*Power(t2,4) - 
         588*Power(s2,3)*Power(t1,2)*Power(t2,4) - 
         208*Power(s2,4)*Power(t1,2)*Power(t2,4) - 
         28*Power(s2,5)*Power(t1,2)*Power(t2,4) - 
         84*Power(t1,3)*Power(t2,4) + 237*s2*Power(t1,3)*Power(t2,4) + 
         347*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
         149*Power(s2,3)*Power(t1,3)*Power(t2,4) + 
         31*Power(s2,4)*Power(t1,3)*Power(t2,4) + 
         108*Power(t1,4)*Power(t2,4) + 74*s2*Power(t1,4)*Power(t2,4) + 
         6*Power(s2,2)*Power(t1,4)*Power(t2,4) - 
         24*Power(s2,3)*Power(t1,4)*Power(t2,4) - 
         46*Power(t1,5)*Power(t2,4) - 50*s2*Power(t1,5)*Power(t2,4) + 
         2*Power(s2,2)*Power(t1,5)*Power(t2,4) + 
         6*Power(t1,6)*Power(t2,4) + 6*s2*Power(t1,6)*Power(t2,4) + 
         14*Power(t2,5) + 13*s2*Power(t2,5) - 
         60*Power(s2,2)*Power(t2,5) - 88*Power(s2,3)*Power(t2,5) - 
         33*Power(s2,4)*Power(t2,5) - 22*Power(s2,5)*Power(t2,5) - 
         12*Power(s2,6)*Power(t2,5) - 57*t1*Power(t2,5) - 
         79*s2*t1*Power(t2,5) + 77*Power(s2,2)*t1*Power(t2,5) + 
         98*Power(s2,3)*t1*Power(t2,5) + 34*Power(s2,4)*t1*Power(t2,5) + 
         10*Power(s2,5)*t1*Power(t2,5) + 87*Power(t1,2)*Power(t2,5) + 
         148*s2*Power(t1,2)*Power(t2,5) + 
         47*Power(s2,2)*Power(t1,2)*Power(t2,5) + 
         18*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         11*Power(s2,4)*Power(t1,2)*Power(t2,5) - 
         59*Power(t1,3)*Power(t2,5) - 117*s2*Power(t1,3)*Power(t2,5) - 
         77*Power(s2,2)*Power(t1,3)*Power(t2,5) - 
         16*Power(s2,3)*Power(t1,3)*Power(t2,5) + 
         15*Power(t1,4)*Power(t2,5) + 41*s2*Power(t1,4)*Power(t2,5) + 
         13*Power(s2,2)*Power(t1,4)*Power(t2,5) - 
         6*s2*Power(t1,5)*Power(t2,5) + 12*s2*Power(t2,6) + 
         31*Power(s2,2)*Power(t2,6) + 24*Power(s2,3)*Power(t2,6) + 
         9*Power(s2,4)*Power(t2,6) + 6*Power(s2,5)*Power(t2,6) - 
         37*s2*t1*Power(t2,6) - 70*Power(s2,2)*t1*Power(t2,6) - 
         32*Power(s2,3)*t1*Power(t2,6) - 9*Power(s2,4)*t1*Power(t2,6) + 
         38*s2*Power(t1,2)*Power(t2,6) + 
         38*Power(s2,2)*Power(t1,2)*Power(t2,6) + 
         2*Power(s2,3)*Power(t1,2)*Power(t2,6) - 
         13*s2*Power(t1,3)*Power(t2,6) + 
         Power(s2,2)*Power(t1,3)*Power(t2,6) + 
         Power(s,9)*(6 + 6*s1*(-1 + s2) - 6*t1 + 4*Power(t1,2) + 
            6*s2*(-1 + t1 - t2) + 6*t2 - 8*t1*t2 + 4*Power(t2,2)) + 
         Power(s1,6)*(6*Power(s2,5) + 7*Power(s2,4)*(-1 + t1) + 
            4*Power(-1 + t1,4) + 
            Power(s2,3)*(-19 - 9*Power(t1,2) + t1*(22 - 48*t2) + 
               48*t2) + s2*Power(-1 + t1,2)*
             (7*Power(t1,2) + 4*(5 + 6*t2) - 4*t1*(7 + 6*t2)) - 
            Power(s2,2)*(-1 + t1)*
             (8 + 11*Power(t1,2) + 72*t2 - 4*t1*(7 + 18*t2))) - 
         Power(s,8)*(20 + 2*Power(s1,2)*(-8 + 11*s2) - 55*t1 + 
            43*Power(t1,2) - 18*Power(t1,3) + 
            30*Power(s2,2)*(-1 + t1 - t2) + 13*t2 - 60*t1*t2 + 
            26*Power(t1,2)*t2 + 17*Power(t2,2) + 2*t1*Power(t2,2) - 
            10*Power(t2,3) + s2*
             (16 + 5*t1 - 3*Power(t1,2) + 37*t2 - 28*t1*t2 + 
               31*Power(t2,2)) + 
            s1*(-4 + 30*Power(s2,2) + 27*t1 + 18*Power(t1,2) + 15*t2 - 
               28*t1*t2 + 10*Power(t2,2) - s2*(38 + 7*t1 + 35*t2))) + 
         Power(s1,5)*(12*Power(s2,6) - 2*Power(s2,5)*(-7 + t1 + 18*t2) - 
            Power(-1 + t1,3)*
             (-9*Power(t1,2) + 2*Power(t1,3) - 4*(2 + 5*t2) + 
               4*t1*(4 + 5*t2)) + 
            Power(s2,4)*(31 + 3*Power(t1,2) + 26*t2 - 
               2*t1*(23 + 13*t2)) + 
            s2*Power(-1 + t1,2)*
             (52 + 21*Power(t1,3) - 112*t2 - 60*Power(t2,2) - 
               Power(t1,2)*(37 + 35*t2) + 
               t1*(-19 + 153*t2 + 60*Power(t2,2))) + 
            Power(s2,3)*(149 - 7*Power(t1,3) + 71*t2 - 
               120*Power(t2,2) + 43*Power(t1,2)*(3 + t2) + 
               t1*(-283 - 78*t2 + 120*Power(t2,2))) - 
            Power(s2,2)*(-1 + t1)*
             (174 + 25*Power(t1,3) + Power(t1,2)*(34 - 54*t2) - 71*t2 - 
               180*Power(t2,2) + t1*(-241 + 179*t2 + 180*Power(t2,2)))) + 
         Power(s1,4)*(6*Power(s2,7) + Power(s2,6)*(-19 + 7*t1 - 60*t2) + 
            Power(s2,5)*(-97 - 15*Power(t1,2) - 78*t2 + 
               90*Power(t2,2) + 2*t1*(56 + 9*t2)) + 
            Power(s2,4)*(-105 + 37*Power(t1,3) - 157*t2 - 
               25*Power(t2,2) - Power(t1,2)*(227 + t2) + 
               t1*(307 + 218*t2 + 25*Power(t2,2))) + 
            Power(-1 + t1,3)*
             (52 + 2*Power(t1,4) - 44*t2 - 40*Power(t2,2) - 
               10*Power(t1,2)*(2 + 3*t2) + Power(t1,3)*(13 + 6*t2) + 
               t1*(-39 + 73*t2 + 40*Power(t2,2))) - 
            Power(s2,3)*(111 + 67*Power(t1,4) + 688*t2 + 
               70*Power(t2,2) - 160*Power(t2,3) - 
               2*Power(t1,3)*(189 + 8*t2) + 
               10*Power(t1,2)*(83 + 51*t2 + 8*Power(t2,2)) + 
               2*t1*(-312 - 621*t2 - 30*Power(t2,2) + 80*Power(t2,3))) - 
            s2*Power(-1 + t1,2)*
             (235 + 203*t2 - 260*Power(t2,2) - 80*Power(t2,3) + 
               Power(t1,3)*(-2 + 82*t2) + 
               Power(t1,2)*(178 - 153*t2 - 70*Power(t2,2)) + 
               t1*(-446 - 47*t2 + 345*Power(t2,2) + 80*Power(t2,3))) + 
            Power(s2,2)*(-1 + t1)*
             (261 + 30*Power(t1,4) + 766*t2 - 235*Power(t2,2) - 
               240*Power(t2,3) + Power(t1,3)*(-207 + 103*t2) + 
               Power(t1,2)*(722 + 102*t2 - 105*Power(t2,2)) + 
               t1*(-817 - 1011*t2 + 475*Power(t2,2) + 240*Power(t2,3)))) \
+ Power(s1,3)*(8*Power(s2,7)*(-1 + t1 - 3*t2) + 
            Power(s2,6)*(-17 - 15*Power(t1,2) + 76*t2 + 
               120*Power(t2,2) - 4*t1*(-8 + 7*t2)) + 
            Power(s2,5)*(117 + 35*Power(t1,3) + 385*t2 + 
               172*Power(t2,2) - 120*Power(t2,3) + 
               Power(t1,2)*(-29 + 73*t2) - 
               t1*(123 + 458*t2 + 52*Power(t2,2))) - 
            Power(s2,4)*(-225 + 47*Power(t1,4) - 437*t2 - 
               318*Power(t2,2) + 20*Power(t2,3) + 
               Power(t1,3)*(15 + 134*t2) + 
               Power(t1,2)*(-500 - 865*t2 + 26*Power(t2,2)) + 
               t1*(663 + 1216*t2 + 412*Power(t2,2) - 20*Power(t2,3))) - 
            Power(-1 + t1,3)*
             (80 + 4*Power(t1,4)*(-1 + t2) + 182*t2 - 98*Power(t2,2) - 
               40*Power(t2,3) + 
               Power(t1,2)*(151 - 88*t2 - 36*Power(t2,2)) + 
               Power(t1,3)*(-21 + 49*t2 + 6*Power(t2,2)) + 
               t1*(-227 - 115*t2 + 138*Power(t2,2) + 40*Power(t2,3))) + 
            s2*Power(-1 + t1,2)*
             (13 + 8*Power(t1,5) + 887*t2 + 284*Power(t2,2) - 
               320*Power(t2,3) - 60*Power(t2,4) - 
               8*Power(t1,4)*(-9 + 2*t2) + 
               Power(t1,3)*(-481 + 64*t2 + 126*Power(t2,2)) + 
               Power(t1,2)*(992 + 506*t2 - 266*Power(t2,2) - 
                  70*Power(t2,3)) + 
               t1*(-589 - 1581*t2 + 26*Power(t2,2) + 410*Power(t2,3) + 
                  60*Power(t2,4))) - 
            Power(s2,2)*(-1 + t1)*
             (-191 + 10*Power(t1,5) + 1046*t2 + 1314*Power(t2,2) - 
               390*Power(t2,3) - 180*Power(t2,4) + 
               Power(t1,4)*(239 + 70*t2) + 
               Power(t1,3)*(-1248 - 529*t2 + 172*Power(t2,2)) + 
               Power(t1,2)*(1736 + 2401*t2 + 38*Power(t2,2) - 
                  100*Power(t2,3)) + 
               2*t1*(-268 - 1516*t2 - 802*Power(t2,2) + 
                  335*Power(t2,3) + 90*Power(t2,4))) + 
            Power(s2,3)*(-31 + 21*Power(t1,5) + 493*t2 + 
               1258*Power(t2,2) - 50*Power(t2,3) - 120*Power(t2,4) + 
               Power(t1,4)*(193 + 203*t2) + 
               Power(t1,3)*(-1193 - 1195*t2 + 10*Power(t2,2)) + 
               2*Power(t1,2)*
                (832 + 1473*t2 + 369*Power(t2,2) + 35*Power(t2,3)) + 
               t1*(-654 - 2423*t2 - 2126*Power(t2,2) + 100*Power(t2,3) + 
                  120*Power(t2,4)))) + 
         Power(s1,2)*(12*Power(s2,7)*t2*(2 - 2*t1 + 3*t2) + 
            Power(s2,6)*(20 + 6*Power(t1,3) + 51*t2 - 114*Power(t2,2) - 
               120*Power(t2,3) + Power(t1,2)*(8 + 45*t2) + 
               t1*(-34 - 96*t2 + 42*Power(t2,2))) - 
            Power(s2,5)*(22 + 12*Power(t1,4) + 354*t2 + 
               573*Power(t2,2) + 188*Power(t2,3) - 90*Power(t2,4) + 
               Power(t1,3)*(26 + 84*t2) + 
               3*Power(t1,2)*(-22 - 14*t2 + 43*Power(t2,2)) - 
               2*t1*(-3 + 198*t2 + 351*Power(t2,2) + 34*Power(t2,3))) + 
            Power(s2,4)*(-205 + 12*Power(t1,5) - 685*t2 - 
               689*Power(t2,2) - 322*Power(t2,3) + 55*Power(t2,4) + 
               Power(t1,4)*(47 + 93*t2) + 
               Power(t1,3)*(-2 + 189*t2 + 188*Power(t2,2)) + 
               Power(t1,2)*(-390 - 1654*t2 - 1257*Power(t2,2) + 
                  54*Power(t2,3)) + 
               t1*(538 + 2057*t2 + 1830*Power(t2,2) + 388*Power(t2,3) - 
                  55*Power(t2,4))) + 
            Power(-1 + t1,3)*
             (8*Power(t1,5) + 
               2*Power(t1,4)*(23 - 10*t2 + Power(t2,2)) + 
               Power(t1,3)*(-279 + 11*t2 + 65*Power(t2,2) + 
                  2*Power(t2,3)) - 
               2*Power(t1,2)*
                (-193 - 160*t2 + 72*Power(t2,2) + 9*Power(t2,3)) - 
               2*(53 - 124*t2 - 116*Power(t2,2) + 55*Power(t2,3) + 
                  10*Power(t2,4)) + 
               t1*(-41 - 622*t2 - 107*Power(t2,2) + 136*Power(t2,3) + 
                  20*Power(t2,4))) - 
            s2*Power(-1 + t1,2)*
             (-465 + 104*t2 + 1253*Power(t2,2) + 154*Power(t2,3) - 
               220*Power(t2,4) - 24*Power(t2,5) + 
               2*Power(t1,5)*(-9 + 8*t2) + 
               Power(t1,4)*(370 + 150*t2 - 38*Power(t2,2)) + 
               2*Power(t1,3)*
                (-613 - 518*t2 + 86*Power(t2,2) + 48*Power(t2,3)) + 
               Power(t1,2)*(939 + 2466*t2 + 486*Power(t2,2) - 
                  250*Power(t2,3) - 35*Power(t2,4)) + 
               t1*(402 - 1655*t2 - 2083*Power(t2,2) + 170*Power(t2,3) + 
                  270*Power(t2,4) + 24*Power(t2,5))) - 
            Power(s2,3)*(51 + 12*Power(t1,6) - 49*t2 + 
               835*Power(t2,2) + 1136*Power(t2,3) - 145*Power(t2,4) - 
               48*Power(t2,5) + 4*Power(t1,5)*(-2 + 5*t2) + 
               Power(t1,4)*(355 + 649*t2 + 229*Power(t2,2)) + 
               Power(t1,3)*(-1322 - 3541*t2 - 1405*Power(t2,2) + 
                  56*Power(t2,3)) + 
               Power(t1,2)*(1602 + 4922*t2 + 3990*Power(t2,2) + 
                  444*Power(t2,3) + 25*Power(t2,4)) + 
               t1*(-690 - 2001*t2 - 3613*Power(t2,2) - 
                  1756*Power(t2,3) + 210*Power(t2,4) + 48*Power(t2,5))) \
+ Power(s2,2)*(-1 + t1)*(-473 + 6*Power(t1,6) - 479*t2 + 
               1590*Power(t2,2) + 1086*Power(t2,3) - 350*Power(t2,4) - 
               72*Power(t2,5) + Power(t1,5)*(-57 + 6*t2) + 
               Power(t1,4)*(662 + 624*t2 + 52*Power(t2,2)) + 
               Power(t1,3)*(-1849 - 3213*t2 - 429*Power(t2,2) + 
                  148*Power(t2,3)) + 
               Power(t1,2)*(1357 + 4641*t2 + 2991*Power(t2,2) - 
                  158*Power(t2,3) - 45*Power(t2,4)) + 
               t1*(354 - 1549*t2 - 4270*Power(t2,2) - 1156*Power(t2,3) + 
                  530*Power(t2,4) + 72*Power(t2,5)))) + 
         s1*(-24*Power(s2,7)*Power(t2,2)*(1 - t1 + t2) - 
            Power(s2,6)*(2 + 44*t2 + 51*Power(t2,2) - 76*Power(t2,3) - 
               60*Power(t2,4) + Power(t1,3)*(-2 + 8*t2) + 
               Power(t1,2)*(6 + 28*t2 + 45*Power(t2,2)) + 
               t1*(-6 - 80*t2 - 96*Power(t2,2) + 28*Power(t2,3))) + 
            Power(s2,5)*(-12 + 38*t2 + 353*Power(t2,2) + 
               379*Power(t2,3) + 102*Power(t2,4) - 36*Power(t2,5) + 
               4*Power(t1,4)*(-1 + 3*t2) + 
               Power(t1,3)*(24 + 94*t2 + 67*Power(t2,2)) + 
               3*Power(t1,2)*
                (-16 - 62*t2 - 3*Power(t2,2) + 33*Power(t2,3)) + 
               t1*(40 + 42*t2 - 411*Power(t2,2) - 478*Power(t2,3) - 
                  42*Power(t2,4))) - 
            Power(-1 + t1,3)*
             (8*Power(t1,5)*(-3 + t2) + 
               Power(t1,4)*(180 + 68*t2 - 22*Power(t2,2)) + 
               Power(t1,3)*(-337 - 427*t2 + 57*Power(t2,2) + 
                  35*Power(t2,3)) + 
               Power(t1,2)*(-24 + 646*t2 + 235*Power(t2,2) - 
                  104*Power(t2,3) - 3*Power(t2,4)) - 
               2*(100 + 95*t2 - 133*Power(t2,2) - 63*Power(t2,3) + 
                  31*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(403 - 77*t2 - 599*Power(t2,2) - 25*Power(t2,3) + 
                  70*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s2,4)*(73 + Power(t1,5)*(4 - 8*t2) + 415*t2 + 
               685*Power(t2,2) + 487*Power(t2,3) + 163*Power(t2,4) - 
               38*Power(t2,5) - 
               Power(t1,4)*(37 + 137*t2 + 55*Power(t2,2)) + 
               Power(t1,3)*(14 + 32*t2 - 293*Power(t2,2) - 
                  122*Power(t2,3)) + 
               Power(t1,2)*(140 + 794*t2 + 1748*Power(t2,2) + 
                  827*Power(t2,3) - 41*Power(t2,4)) + 
               t1*(-194 - 1096*t2 - 2085*Power(t2,2) - 
                  1240*Power(t2,3) - 182*Power(t2,4) + 38*Power(t2,5))) \
+ s2*Power(-1 + t1,2)*(-421 + 8*Power(t1,6) - 887*t2 + 
               207*Power(t2,2) + 785*Power(t2,3) + 8*Power(t2,4) - 
               80*Power(t2,5) - 4*Power(t2,6) + 
               4*Power(t1,5)*(-25 - 12*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(558 + 640*t2 + 110*Power(t2,2) - 
                  28*Power(t2,3)) + 
               Power(t1,3)*(-860 - 2102*t2 - 745*Power(t2,2) + 
                  144*Power(t2,3) + 37*Power(t2,4)) + 
               Power(t1,2)*(-55 + 1579*t2 + 2148*Power(t2,2) + 
                  166*Power(t2,3) - 129*Power(t2,4) - 7*Power(t2,5)) + 
               t1*(870 + 822*t2 - 1683*Power(t2,2) - 1207*Power(t2,3) + 
                  169*Power(t2,4) + 93*Power(t2,5) + 4*Power(t2,6))) + 
            Power(s2,3)*(101 + 117*t2 + 5*Power(t2,2) + 
               635*Power(t2,3) + 505*Power(t2,4) - 101*Power(t2,5) - 
               8*Power(t2,6) + 4*Power(t1,6)*(1 + 3*t2) + 
               Power(t1,5)*(-24 + 2*t2 - 15*Power(t2,2)) + 
               Power(t1,4)*(285 + 737*t2 + 693*Power(t2,2) + 
                  117*Power(t2,3)) + 
               Power(t1,3)*(-852 - 2728*t2 - 3483*Power(t2,2) - 
                  737*Power(t2,3) + 53*Power(t2,4)) + 
               Power(t1,2)*(1026 + 3294*t2 + 4868*Power(t2,2) + 
                  2462*Power(t2,3) + 69*Power(t2,4) - Power(t2,5)) + 
               t1*(-540 - 1434*t2 - 2068*Power(t2,2) - 
                  2453*Power(t2,3) - 687*Power(t2,4) + 
                  138*Power(t2,5) + 8*Power(t2,6))) - 
            Power(s2,2)*(-1 + t1)*
             (-203 - 917*t2 - 341*Power(t2,2) + 1086*Power(t2,3) + 
               424*Power(t2,4) - 163*Power(t2,5) - 12*Power(t2,6) + 
               2*Power(t1,6)*(7 + 4*t2) - 
               Power(t1,5)*(121 + 117*t2 + 16*Power(t2,2)) + 
               Power(t1,4)*(644 + 1256*t2 + 555*Power(t2,2) + 
                  14*Power(t2,3)) + 
               Power(t1,3)*(-1137 - 3477*t2 - 2810*Power(t2,2) - 
                  99*Power(t2,3) + 67*Power(t2,4)) + 
               Power(t1,2)*(553 + 2537*t2 + 4294*Power(t2,2) + 
                  1667*Power(t2,3) - 192*Power(t2,4) - 6*Power(t2,5)) + 
               t1*(250 + 710*t2 - 1652*Power(t2,2) - 2712*Power(t2,3) - 
                  339*Power(t2,4) + 223*Power(t2,5) + 12*Power(t2,6)))) + 
         Power(s,7)*(-17 + 16*Power(s1,3)*(-1 + 2*s2) - 53*t1 + 
            132*Power(t1,2) - 90*Power(t1,3) + 32*Power(t1,4) + 
            60*Power(s2,3)*(-1 + t1 - t2) - 17*t2 - 31*t1*t2 + 
            99*Power(t1,2)*t2 - 20*Power(t1,3)*t2 + 47*Power(t2,2) + 
            52*t1*Power(t2,2) - 48*Power(t1,2)*Power(t2,2) - 
            61*Power(t2,3) + 28*t1*Power(t2,3) + 8*Power(t2,4) + 
            Power(s2,2)*(-16 - 60*Power(t1,2) + t1*(108 - 13*t2) + 
               61*t2 + 77*Power(t2,2)) + 
            Power(s1,2)*(-34 + 83*Power(s2,2) + 32*Power(t1,2) + 
               21*t2 + 8*Power(t2,2) - 4*t1*(-26 + 9*t2) - 
               3*s2*(11 + 27*t1 + 24*t2)) + 
            s2*(141 - 49*Power(t1,3) + 157*t2 + 60*Power(t2,2) - 
               52*Power(t2,3) + 2*Power(t1,2)*(80 + 61*t2) - 
               t1*(284 + 368*t2 + 21*Power(t2,2))) + 
            s1*(71 + 60*Power(s2,3) - 80*Power(t1,3) - 3*t2 + 
               56*Power(t2,2) - 16*Power(t2,3) + 
               Power(t1,2)*(30 + 88*t2) - 
               Power(s2,2)*(63 + 49*t1 + 128*t2) + 
               t1*(-60 - 234*t2 + 8*Power(t2,2)) + 
               s2*(-148 + 46*Power(t1,2) + 41*t2 + 92*Power(t2,2) + 
                  t1*(255 + 2*t2)))) - 
         Power(s,6)*(-203 + 4*Power(s1,4)*(-3 + 7*s2) + 506*t1 - 
            358*Power(t1,2) + 35*Power(t1,3) + 50*Power(t1,4) - 
            28*Power(t1,5) + 60*Power(s2,4)*(-1 + t1 - t2) - 193*t2 + 
            472*t1*t2 - 294*Power(t1,2)*t2 + 44*Power(t1,3)*t2 - 
            20*Power(t1,4)*t2 - 52*Power(t2,2) + 102*t1*Power(t2,2) - 
            273*Power(t1,2)*Power(t2,2) + 90*Power(t1,3)*Power(t2,2) - 
            207*Power(t2,3) + 130*t1*Power(t2,3) - 
            10*Power(t1,2)*Power(t2,3) + 49*Power(t2,4) - 
            30*t1*Power(t2,4) - 2*Power(t2,5) - 
            2*Power(s2,3)*(51 + 64*Power(t1,2) + 9*t2 - 
               33*Power(t2,2) - t1*(129 + 40*t2)) + 
            Power(s1,3)*(6 + 90*Power(s2,2) + 28*Power(t1,2) + 33*t2 + 
               2*Power(t2,2) - 4*t1*(-26 + 5*t2) - 
               s2*(35 + 140*t1 + 86*t2)) + 
            Power(s2,2)*(260 - 16*Power(t1,3) + 399*t2 + 
               109*Power(t2,2) - 76*Power(t2,3) + 
               Power(t1,2)*(97 + 159*t2) - 
               t1*(377 + 679*t2 + 95*Power(t2,2))) + 
            s2*(232 + 113*Power(t1,4) + 224*t2 + 157*Power(t2,2) - 
               163*Power(t2,3) + 36*Power(t2,4) - 
               4*Power(t1,3)*(119 + 27*t2) + 
               Power(t1,2)*(1071 + 701*t2 - 171*Power(t2,2)) + 
               t1*(-934 - 753*t2 + 302*Power(t2,2) + 130*Power(t2,3))) + 
            Power(s1,2)*(-77 + 102*Power(s2,3) - 140*Power(t1,3) - 
               139*t2 + 19*Power(t2,2) - 6*Power(t2,3) - 
               9*Power(s2,2)*(-6 + 33*t1 + 20*t2) + 
               4*Power(t1,2)*(-35 + 27*t2) + 
               t1*(263 - 336*t2 + 10*Power(t2,2)) + 
               s2*(-122 + 225*Power(t1,2) + 105*t2 + 124*Power(t2,2) + 
                  t1*(291 + 170*t2))) + 
            s1*(304 + 60*Power(s2,4) + 140*Power(t1,4) + 87*t2 + 
               340*Power(t2,2) - 89*Power(t2,3) + 6*Power(t2,4) - 
               2*Power(t1,3)*(103 + 30*t2) - 
               2*Power(s2,3)*(-9 + 68*t1 + 70*t2) + 
               Power(t1,2)*(474 + 557*t2 - 126*Power(t2,2)) + 
               t1*(-725 - 355*t2 + 102*Power(t2,2) + 40*Power(t2,3)) + 
               Power(s2,2)*(-422 + 101*Power(t1,2) - 51*t2 + 
                  166*Power(t2,2) + t1*(514 + 244*t2)) - 
               s2*(121 + 222*Power(t1,3) + Power(t1,2)*(346 - 176*t2) - 
                  19*t2 - 93*Power(t2,2) + 102*Power(t2,3) + 
                  t1*(-613 + 871*t2 + 160*Power(t2,2))))) + 
         Power(s,5)*(-570 + 2*Power(s1,5)*(-5 + 11*s2) + 2087*t1 - 
            2814*Power(t1,2) + 1754*Power(t1,3) - 527*Power(t1,4) + 
            58*Power(t1,5) + 12*Power(t1,6) + 
            30*Power(s2,5)*(-1 + t1 - t2) - 873*t2 + 2152*t1*t2 - 
            2011*Power(t1,2)*t2 + 909*Power(t1,3)*t2 - 
            208*Power(t1,4)*t2 + 40*Power(t1,5)*t2 - 667*Power(t2,2) + 
            555*t1*Power(t2,2) - 255*Power(t1,2)*Power(t2,2) + 
            292*Power(t1,3)*Power(t2,2) - 60*Power(t1,4)*Power(t2,2) - 
            320*Power(t2,3) + 364*t1*Power(t2,3) + 
            21*Power(t1,2)*Power(t2,3) - 40*Power(t1,3)*Power(t2,3) + 
            181*Power(t2,4) - 152*t1*Power(t2,4) + 
            40*Power(t1,2)*Power(t2,4) - 11*Power(t2,5) + 
            8*t1*Power(t2,5) - 
            2*Power(s2,4)*(70 + 57*Power(t1,2) + 77*t2 + 
               13*Power(t2,2) - t1*(133 + 86*t2)) + 
            Power(s1,4)*(20 + 78*Power(s2,2) + 12*Power(t1,2) + 
               t1*(50 - 4*t2) + 41*t2 - s2*(93 + 106*t1 + 90*t2)) + 
            Power(s2,3)*(138 + 60*Power(t1,3) + 400*t2 + 
               204*Power(t2,2) + 34*Power(t2,3) - 
               Power(t1,2)*(239 + 32*t2) + 
               t1*(25 - 391*t2 - 170*Power(t2,2))) + 
            Power(s2,2)*(587 + 124*Power(t1,4) + 737*t2 + 
               388*Power(t2,2) - 26*Power(t2,3) + 28*Power(t2,4) - 
               Power(t1,3)*(581 + 113*t2) + 
               Power(t1,2)*(1714 + 1381*t2 - 139*Power(t2,2)) + 
               t1*(-1844 - 2042*t2 + 195*Power(t2,2) + 184*Power(t2,3))) \
+ s2*(77 - 105*Power(t1,5) + Power(t1,4)*(512 - 54*t2) + 140*t2 + 
               43*Power(t2,2) - 641*Power(t2,3) + 69*Power(t2,4) - 
               10*Power(t2,5) + 
               Power(t1,3)*(-1496 - 215*t2 + 341*Power(t2,2)) + 
               Power(t1,2)*(1852 + 316*t2 - 1293*Power(t2,2) - 
                  54*Power(t2,3)) + 
               t1*(-836 - 134*t2 + 742*Power(t2,2) + 255*Power(t2,3) - 
                  118*Power(t2,4))) + 
            Power(s1,3)*(143 + 28*Power(s2,3) - 120*Power(t1,3) - 
               133*t2 - 52*Power(t2,2) + 2*Power(t1,2)*(-87 + 28*t2) - 
               2*Power(s2,2)*(46 + 171*t1 + 105*t2) + 
               t1*(187 - 238*t2 + 4*Power(t2,2)) + 
               s2*(96 + 354*Power(t1,2) + 382*t2 + 148*Power(t2,2) + 
                  t1*(-1 + 256*t2))) + 
            Power(s1,2)*(-484 + 16*Power(s2,4) + 240*Power(t1,4) - 
               616*t2 + 387*Power(t2,2) + 10*Power(t2,3) - 
               2*Power(t1,3)*(37 + 30*t2) + 
               Power(s2,3)*(291 - 454*t1 + 42*t2) + 
               Power(t1,2)*(-347 + 903*t2 - 108*Power(t2,2)) + 
               t1*(529 - 274*t2 + 174*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,2)*(31 + 649*Power(t1,2) + 468*t2 + 
                  214*Power(t2,2) + t1*(-209 + 516*t2)) - 
               s2*(205 + 475*Power(t1,3) + 541*t2 + 416*Power(t2,2) + 
                  124*Power(t2,3) + Power(t1,2)*(656 + 222*t2) + 
                  t1*(-1278 + 587*t2 + 312*Power(t2,2)))) + 
            s1*(929 + 30*Power(s2,5) - 120*Power(t1,5) + 
               Power(t1,4)*(218 - 80*t2) + 1081*t2 + 793*Power(t2,2) - 
               455*Power(t2,3) + 22*Power(t2,4) + 
               Power(s2,4)*(156 - 198*t1 + 22*t2) + 
               Power(t1,2)*(2396 + 440*t2 - 750*Power(t2,2)) + 
               Power(t1,3)*(-915 - 328*t2 + 220*Power(t2,2)) - 
               t1*(2517 + 842*t2 + 277*Power(t2,2) - 166*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,3)*(-445 + 238*Power(t1,2) - 407*t2 - 
                  104*Power(t2,2) + t1*(262 + 520*t2)) - 
               Power(s2,2)*(696 + 319*Power(t1,3) + 325*t2 + 
                  350*Power(t2,2) + 110*Power(t2,3) + 
                  Power(t1,2)*(965 + 242*t2) + 
                  t1*(-2017 + 348*t2 + 358*Power(t2,2))) + 
               s2*(123 + 394*Power(t1,4) + 66*t2 + 1086*Power(t2,2) + 
                  58*Power(t2,3) + 54*Power(t2,4) - 
                  2*Power(t1,3)*(48 + 73*t2) - 
                  3*Power(t1,2)*(86 - 787*t2 + 26*Power(t2,2)) + 
                  t1*(-224 - 2052*t2 + 333*Power(t2,2) + 280*Power(t2,3))\
))) + Power(s,4)*(1024 + Power(s1,6)*(4 - 14*s2) - 4351*t1 + 
            7288*Power(t1,2) - 6233*Power(t1,3) + 2955*Power(t1,4) - 
            778*Power(t1,5) + 93*Power(t1,6) + 2*Power(t1,7) + 2225*t2 - 
            5919*t1*t2 + 5783*Power(t1,2)*t2 - 2827*Power(t1,3)*t2 + 
            824*Power(t1,4)*t2 - 136*Power(t1,5)*t2 + 
            22*Power(t1,6)*t2 + 1648*Power(t2,2) - 2081*t1*Power(t2,2) + 
            76*Power(t1,2)*Power(t2,2) + 428*Power(t1,3)*Power(t2,2) - 
            7*Power(t1,4)*Power(t2,2) - 6*Power(t1,5)*Power(t2,2) - 
            14*Power(t2,3) - 31*t1*Power(t2,3) - 
            113*Power(t1,2)*Power(t2,3) + 238*Power(t1,3)*Power(t2,3) - 
            50*Power(t1,4)*Power(t2,3) - 403*Power(t2,4) + 
            491*t1*Power(t2,4) - 152*Power(t1,2)*Power(t2,4) + 
            20*Power(t1,3)*Power(t2,4) + 44*Power(t2,5) - 
            36*t1*Power(t2,5) + 12*Power(t1,2)*Power(t2,5) + 
            Power(s2,6)*(6 - 6*t1 + 6*t2) + 
            Power(s1,5)*(26 - 80*Power(s2,2) - 33*t1 - 2*Power(t1,2) - 
               20*t2 + s2*(55 + 57*t1 + 71*t2)) + 
            Power(s2,5)*(82 + 45*Power(t1,2) + 173*t2 + 
               85*Power(t2,2) - t1*(129 + 158*t2)) - 
            Power(s2,4)*(-64 + 62*Power(t1,3) + 155*t2 + 
               325*Power(t2,2) + 204*Power(t2,3) - 
               4*Power(t1,2)*(92 + 57*t2) + 
               t1*(368 + 139*t2 - 198*Power(t2,2))) - 
            Power(s2,3)*(471 + 39*Power(t1,4) + 963*t2 + 
               662*Power(t2,2) + 193*Power(t2,3) - 95*Power(t2,4) + 
               Power(t1,3)*(-61 + 86*t2) + 
               2*Power(t1,2)*(373 + 587*t2 + 78*Power(t2,2)) + 
               t1*(-1197 - 2379*t2 - 676*Power(t2,2) + 84*Power(t2,3))) \
+ Power(s2,2)*(-648 + 116*Power(t1,5) - 922*t2 - 468*Power(t2,2) + 
               777*Power(t2,3) + 253*Power(t2,4) + 
               Power(t1,4)*(-641 + 91*t2) + 
               Power(t1,3)*(2527 + 885*t2 - 319*Power(t2,2)) + 
               Power(t1,2)*(-4087 - 2492*t2 + 1416*Power(t2,2) + 
                  146*Power(t2,3)) + 
               t1*(2731 + 2276*t2 - 742*Power(t2,2) - 348*Power(t2,3) + 
                  106*Power(t2,4))) - 
            s2*(-318 + 46*Power(t1,6) - 668*t2 - 1439*Power(t2,2) - 
               1528*Power(t2,3) + 298*Power(t2,4) + 28*Power(t2,5) + 
               Power(t2,6) + Power(t1,5)*(-199 + 136*t2) + 
               Power(t1,4)*(650 - 580*t2 - 244*Power(t2,2)) + 
               Power(t1,3)*(-723 + 2037*t2 + 1607*Power(t2,2) - 
                  124*Power(t2,3)) + 
               Power(t1,2)*(-205 - 3384*t2 - 2646*Power(t2,2) + 
                  284*Power(t2,3) + 151*Power(t2,4)) + 
               t1*(749 + 2380*t2 + 2545*Power(t2,2) + 
                  1256*Power(t2,3) - 244*Power(t2,4) + 34*Power(t2,5))) \
+ Power(s1,4)*(-237 - 10*Power(s2,3) + 50*Power(t1,3) + 
               Power(t1,2)*(47 - 10*t2) - 90*t2 + 40*Power(t2,2) + 
               12*t1*(8 + 13*t2) + 
               2*Power(s2,2)*(160 + 91*t1 + 155*t2) - 
               s2*(-240 + 229*Power(t1,2) + 288*t2 + 145*Power(t2,2) + 
                  2*t1*(71 + 111*t2))) + 
            Power(s1,3)*(-116 + 152*Power(s2,4) - 200*Power(t1,4) + 
               Power(s2,3)*(112 + 273*t1 - 99*t2) + 984*t2 + 
               70*Power(t2,2) - 40*Power(t2,3) + 
               4*Power(t1,3)*(-13 + 5*t2) + 
               Power(t1,2)*(663 - 499*t2 + 30*Power(t2,2)) - 
               t1*(342 + 329*t2 + 234*Power(t2,2)) - 
               Power(s2,2)*(251 + 768*Power(t1,2) + 1439*t2 + 
                  450*Power(t2,2) + t1*(-593 + 422*t2)) + 
               s2*(-1045 + 564*Power(t1,3) - 772*t2 + 562*Power(t2,2) + 
                  150*Power(t2,3) + Power(t1,2)*(-306 + 448*t2) + 
                  t1*(579 + 922*t2 + 358*Power(t2,2)))) + 
            Power(s1,2)*(1387 + 64*Power(s2,5) + 200*Power(t1,5) + 
               Power(s2,4)*(-469 + 402*t1 - 534*t2) + 418*t2 - 
               1660*Power(t2,2) + 70*Power(t2,3) + 20*Power(t2,4) + 
               8*Power(t1,4)*(-31 + 15*t2) + 
               Power(t1,3)*(218 + 892*t2 - 170*Power(t2,2)) + 
               Power(t1,2)*(97 - 1649*t2 + 705*Power(t2,2) - 
                  10*Power(t2,3)) + 
               t1*(-1606 + 343*t2 + 861*Power(t2,2) + 96*Power(t2,3)) - 
               Power(s2,3)*(538 + 963*Power(t1,2) + 655*t2 - 
                  323*Power(t2,2) + t1*(-1423 + 378*t2)) + 
               Power(s2,2)*(81 + 919*Power(t1,3) + 843*t2 + 
                  2171*Power(t2,2) + 290*Power(t2,3) + 
                  8*Power(t1,2)*(5 + 129*t2) + 
                  t1*(-1279 - 436*t2 + 404*Power(t2,2))) - 
               s2*(-1253 + 579*Power(t1,4) - 3588*t2 - 
                  526*Power(t2,2) + 508*Power(t2,3) + 80*Power(t2,4) + 
                  Power(t1,3)*(786 + 364*t2) + 
                  Power(t1,2)*(-3623 + 1042*t2 + 360*Power(t2,2)) + 
                  t1*(3301 + 1654*t2 + 1174*Power(t2,2) + 
                     312*Power(t2,3)))) + 
            s1*(-2100 - 6*Power(s2,6) - 50*Power(t1,6) + 
               Power(s2,5)*(-174 + 163*t1 - 151*t2) - 3049*t2 - 
               288*Power(t2,2) + 1316*Power(t2,3) - 120*Power(t2,4) - 
               4*Power(t2,5) - Power(t1,5)*(19 + 140*t2) + 
               Power(t1,4)*(-509 + 255*t2 + 130*Power(t2,2)) + 
               Power(s2,4)*(185 - 312*Power(t1,2) + t1*(189 - 566*t2) + 
                  762*t2 + 586*Power(t2,2)) + 
               Power(t1,3)*(3027 - 986*t2 - 1078*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,2)*(-6253 + 277*t2 + 1099*Power(t2,2) - 
                  101*Power(t2,3) - 20*Power(t2,4)) + 
               t1*(5932 + 3537*t2 + 30*Power(t2,2) - 1119*Power(t2,3) + 
                  51*Power(t2,4)) + 
               Power(s2,3)*(1011 + 365*Power(t1,3) + 1134*t2 + 
                  736*Power(t2,2) - 309*Power(t2,3) + 
                  Power(t1,2)*(922 + 985*t2) + 
                  t1*(-2458 - 1897*t2 + 189*Power(t2,2))) - 
               Power(s2,2)*(-674 + 455*Power(t1,4) - 477*t2 + 
                  1369*Power(t2,2) + 1305*Power(t2,3) + 
                  70*Power(t2,4) + Power(t1,3)*(649 + 370*t2) + 
                  2*Power(t1,2)*(-1372 + 906*t2 + 205*Power(t2,2)) + 
                  t1*(2148 - 2055*t2 - 191*Power(t2,2) + 
                     270*Power(t2,3))) + 
               s2*(-899 + 331*Power(t1,5) - 2522*t2 - 4071*Power(t2,2) + 
                  304*Power(t2,3) + 207*Power(t2,4) + 19*Power(t2,5) + 
                  Power(t1,4)*(-492 + 145*t2) + 
                  Power(t1,3)*(1514 + 2623*t2 - 324*Power(t2,2)) + 
                  Power(t1,2)*
                   (-3628 - 5969*t2 + 1632*Power(t2,2) + 
                     292*Power(t2,3)) + 
                  t1*(3095 + 5336*t2 + 2331*Power(t2,2) + 
                     150*Power(t2,3) + 153*Power(t2,4))))) + 
         Power(s,3)*(-1245 + 4*Power(s1,7)*s2 + 5853*t1 - 
            11063*Power(t1,2) + 11004*Power(t1,3) - 6419*Power(t1,4) + 
            2327*Power(t1,5) - 503*Power(t1,6) + 46*Power(t1,7) + 
            Power(s1,6)*(51*Power(s2,2) + 16*(-1 + t1) + 
               s2*(16 - 35*t1 - 24*t2)) - 3211*t2 + 10027*t1*t2 - 
            11039*Power(t1,2)*t2 + 5069*Power(t1,3)*t2 - 
            929*Power(t1,4)*t2 + 70*Power(t1,5)*t2 + 7*Power(t1,6)*t2 + 
            4*Power(t1,7)*t2 - 1996*Power(t2,2) + 3328*t1*Power(t2,2) - 
            25*Power(t1,2)*Power(t2,2) - 2360*Power(t1,3)*Power(t2,2) + 
            1239*Power(t1,4)*Power(t2,2) - 180*Power(t1,5)*Power(t2,2) + 
            8*Power(t1,6)*Power(t2,2) + 695*Power(t2,3) - 
            1697*t1*Power(t2,3) + 1334*Power(t1,2)*Power(t2,3) - 
            530*Power(t1,3)*Power(t2,3) + 197*Power(t1,4)*Power(t2,3) - 
            20*Power(t1,5)*Power(t2,3) + 517*Power(t2,4) - 
            882*t1*Power(t2,4) + 401*Power(t1,2)*Power(t2,4) - 
            28*Power(t1,3)*Power(t2,4) - 100*Power(t2,5) + 
            135*t1*Power(t2,5) - 42*Power(t1,2)*Power(t2,5) + 
            8*Power(t1,3)*Power(t2,5) - 
            Power(s2,6)*(18 + 6*Power(t1,2) + 81*t2 + 55*Power(t2,2) - 
               t1*(24 + 73*t2)) + 
            Power(s2,5)*(-87 + 17*Power(t1,3) - 3*t2 + 
               294*Power(t2,2) + 218*Power(t2,3) - 
               Power(t1,2)*(173 + 180*t2) + 
               t1*(243 + 219*t2 - 167*Power(t2,2))) + 
            Power(s2,4)*(152 - 6*Power(t1,4) + 694*t2 + 
               589*Power(t2,2) + 58*Power(t2,3) - 217*Power(t2,4) + 
               Power(t1,3)*(201 + 191*t2) + 
               Power(t1,2)*(-138 + 518*t2 + 331*Power(t2,2)) + 
               t1*(-209 - 1489*t2 - 1081*Power(t2,2) + 21*Power(t2,3))) \
+ Power(s2,3)*(527 - 41*Power(t1,5) + 897*t2 + 257*Power(t2,2) - 
               793*Power(t2,3) - 556*Power(t2,4) + 56*Power(t2,5) - 
               6*Power(t1,4)*(-26 + 21*t2) - 
               2*Power(t1,3)*(611 + 531*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(2563 + 3227*t2 - 366*Power(t2,2) - 
                  274*Power(t2,3)) + 
               t1*(-1983 - 2816*t2 + 462*Power(t2,2) + 
                  868*Power(t2,3) + 49*Power(t2,4))) + 
            Power(s2,2)*(406 + 44*Power(t1,6) + 326*t2 - 
               1109*Power(t2,2) - 2324*Power(t2,3) - 202*Power(t2,4) + 
               224*Power(t2,5) - Power(t2,6) + 
               2*Power(t1,5)*(-125 + 69*t2) + 
               Power(t1,4)*(1366 - 205*t2 - 238*Power(t2,2)) + 
               Power(t1,3)*(-3049 + 836*t2 + 2186*Power(t2,2) + 
                  2*Power(t2,3)) + 
               2*Power(t1,2)*
                (1635 - 752*t2 - 2336*Power(t2,2) - 124*Power(t2,3) + 
                  91*Power(t2,4)) + 
               t1*(-1787 + 313*t2 + 3631*Power(t2,2) + 
                  2472*Power(t2,3) + 88*Power(t2,4) + 13*Power(t2,5))) - 
            s2*(821 + 8*Power(t1,7) + 2441*t2 + 3628*Power(t2,2) + 
               1465*Power(t2,3) - 972*Power(t2,4) - 90*Power(t2,5) + 
               15*Power(t2,6) + 6*Power(t1,6)*(5 + 13*t2) - 
               Power(t1,5)*(371 + 655*t2 + 60*Power(t2,2)) + 
               Power(t1,4)*(1641 + 3225*t2 + 741*Power(t2,2) - 
                  176*Power(t2,3)) + 
               Power(t1,3)*(-3669 - 7520*t2 - 2058*Power(t2,2) + 
                  984*Power(t2,3) + 99*Power(t2,4)) + 
               Power(t1,2)*(4558 + 9663*t2 + 4612*Power(t2,2) - 
                  306*Power(t2,3) - 358*Power(t2,4) + 48*Power(t2,5)) + 
               t1*(-3018 - 7268*t2 - 6835*Power(t2,2) - 
                  1776*Power(t2,3) + 1141*Power(t2,4) + 
                  55*Power(t2,5) + 3*Power(t2,6))) + 
            Power(s1,5)*(-10 + 60*Power(s2,3) - 24*Power(t1,2) - 
               8*Power(t1,3) + t1*(41 - 80*t2) + 80*t2 - 
               Power(s2,2)*(99 + 125*t1 + 254*t2) + 
               s2*(-308 + 60*Power(t1,2) - 65*t2 + 60*Power(t2,2) + 
                  2*t1*(109 + 89*t2))) - 
            Power(s1,4)*(-523 + 147*Power(s2,4) + 32*Power(t1,3) - 
               80*Power(t1,4) + 20*t2 + 160*Power(t2,2) - 
               Power(t1,2)*(175 + 174*t2) + 
               Power(s2,3)*(467 + 23*t1 + 180*t2) + 
               t1*(738 + 149*t2 - 160*Power(t2,2)) - 
               Power(s2,2)*(-752 + 298*Power(t1,2) + 660*t2 + 
                  505*Power(t2,2) + t1*(420 + 473*t2)) + 
               s2*(-301 + 331*Power(t1,3) - 1402*t2 - 85*Power(t2,2) + 
                  80*Power(t2,3) + 8*Power(t1,2)*(-43 + 26*t2) + 
                  t1*(281 + 1087*t2 + 365*Power(t2,2)))) - 
            Power(s1,3)*(290 + 204*Power(s2,5) + 160*Power(t1,5) + 
               Power(s2,4)*(-51 + 99*t1 - 666*t2) + 2006*t2 - 
               220*Power(t2,2) - 160*Power(t2,3) + 
               Power(t1,4)*(-92 + 80*t2) + 
               Power(t1,3)*(-936 + 436*t2 - 40*Power(t2,2)) + 
               2*Power(t1,2)*(978 + 103*t2 + 168*Power(t2,2)) + 
               t1*(-1399 - 2696*t2 - 66*Power(t2,2) + 
                  160*Power(t2,3)) - 
               Power(s2,3)*(617 + 758*Power(t1,2) + 2081*t2 + 
                  124*Power(t2,2) - t1*(1353 + 104*t2)) + 
               Power(s2,2)*(-1745 + 982*Power(t1,3) - 2842*t2 + 
                  1610*Power(t2,2) + 500*Power(t2,3) + 
                  2*Power(t1,2)*(-925 + 338*t2) + 
                  t1*(2406 + 2132*t2 + 682*Power(t2,2))) + 
               s2*(-1709 - 496*Power(t1,4) + 
                  Power(t1,3)*(246 - 652*t2) + 1515*t2 + 
                  2448*Power(t2,2) + 10*Power(t2,3) - 60*Power(t2,4) - 
                  6*Power(t1,2)*(-22 - 25*t2 + 52*Power(t2,2)) - 
                  t1*(-1645 + 824*t2 + 2008*Power(t2,2) + 
                     380*Power(t2,3)))) + 
            Power(s1,2)*(-1959 - 51*Power(s2,6) + 80*Power(t1,6) + 
               985*t2 + 2960*Power(t2,2) - 440*Power(t2,3) - 
               80*Power(t2,4) + 4*Power(t1,5)*(-29 + 45*t2) + 
               Power(s2,5)*(368 - 243*t1 + 630*t2) + 
               Power(t1,4)*(758 + 283*t2 - 80*Power(t2,2)) - 
               2*Power(t1,3)*
                (1098 + 1101*t2 - 470*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,2)*(471 + 4226*t2 + 288*Power(t2,2) + 
                  204*Power(t2,3)) + 
               t1*(2976 - 3535*t2 - 4060*Power(t2,2) + 
                  286*Power(t2,3) + 80*Power(t2,4)) + 
               Power(s2,4)*(693 + 777*Power(t1,2) + 44*t2 - 
                  1108*Power(t2,2) + t1*(-1667 + 131*t2)) + 
               Power(s2,3)*(-88 - 1016*Power(t1,3) + 
                  Power(t1,2)*(1163 - 1410*t2) - 1707*t2 - 
                  3317*Power(t2,2) + 108*Power(t2,3) + 
                  t1*(326 + 2870*t2 + 326*Power(t2,2))) + 
               Power(s2,2)*(-1297 + 716*Power(t1,4) - 5650*t2 - 
                  3630*Power(t2,2) + 1860*Power(t2,3) + 
                  245*Power(t2,4) + 2*Power(t1,3)*(507 + 683*t2) + 
                  4*Power(t1,2)*(-1419 - 634*t2 + 160*Power(t2,2)) + 
                  t1*(5009 + 6308*t2 + 3092*Power(t2,2) + 
                     458*Power(t2,3))) - 
               s2*(3233 + 384*Power(t1,5) + 5263*t2 - 
                  3099*Power(t2,2) - 2012*Power(t2,3) + 
                  70*Power(t2,4) + 24*Power(t2,5) + 
                  Power(t1,4)*(597 + 396*t2) + 
                  2*Power(t1,3)*(-2027 + 746*t2 + 205*Power(t2,2)) + 
                  Power(t1,2)*
                   (6745 - 930*t2 + 374*Power(t2,2) + 288*Power(t2,3)) \
+ t1*(-6887 - 5666*t2 + 1946*Power(t2,2) + 1682*Power(t2,3) + 
                     205*Power(t2,4)))) - 
            s1*(-2985 + 8*Power(t1,7) + 
               Power(s2,6)*(-81 + 73*t1 - 106*t2) - 4081*t2 + 
               1390*Power(t2,2) + 1994*Power(t2,3) - 350*Power(t2,4) - 
               16*Power(t2,5) + 2*Power(t1,6)*(77 + 36*t2) - 
               Power(t1,5)*(291 + 350*t2) + 
               Power(s2,5)*(5 - 194*Power(t1,2) + t1*(225 - 406*t2) + 
                  658*t2 + 644*Power(t2,2)) + 
               Power(t1,4)*(-859 + 2287*t2 + 572*Power(t2,2) - 
                  80*Power(t2,3)) + 
               Power(t1,3)*(5309 - 4816*t2 - 1796*Power(t2,2) + 
                  444*Power(t2,3)) + 
               Power(t1,2)*(-10818 + 266*t2 + 3604*Power(t2,2) + 
                  658*Power(t2,3) - 24*Power(t2,4)) + 
               t1*(9480 + 6650*t2 - 3833*Power(t2,2) - 
                  2984*Power(t2,3) + 379*Power(t2,4) + 16*Power(t2,5)) + 
               Power(s2,4)*(753 + 277*Power(t1,3) + 1266*t2 + 
                  153*Power(t2,2) - 806*Power(t2,3) + 
                  Power(t1,2)*(457 + 1084*t2) + 
                  t1*(-1573 - 2708*t2 + 53*Power(t2,2))) + 
               Power(s2,3)*(837 - 302*Power(t1,4) + 225*t2 - 
                  1883*Power(t2,2) - 2259*Power(t2,3) + 
                  168*Power(t2,4) - Power(t1,3)*(1005 + 988*t2) + 
                  Power(t1,2)*(3549 + 745*t2 - 926*Power(t2,2)) + 
                  t1*(-2959 + 732*t2 + 2385*Power(t2,2) + 
                     248*Power(t2,3))) + 
               Power(s2,2)*(26 + 304*Power(t1,5) - 2226*t2 - 
                  6229*Power(t2,2) - 1742*Power(t2,3) + 
                  1035*Power(t2,4) + 46*Power(t2,5) + 
                  Power(t1,4)*(-48 + 398*t2) + 
                  Power(t1,3)*(56 + 3204*t2 + 386*Power(t2,2)) + 
                  Power(t1,2)*
                   (-1367 - 9928*t2 - 934*Power(t2,2) + 
                     444*Power(t2,3)) + 
                  t1*(933 + 8116*t2 + 6374*Power(t2,2) + 
                     1468*Power(t2,3) + 137*Power(t2,4))) - 
               s2*(2424 + 138*Power(t1,6) + 6849*t2 + 5019*Power(t2,2) - 
                  2857*Power(t2,3) - 748*Power(t2,4) + 59*Power(t2,5) + 
                  4*Power(t2,6) + Power(t1,5)*(-351 + 256*t2) + 
                  Power(t1,4)*(2400 + 1318*t2 - 276*Power(t2,2)) + 
                  Power(t1,3)*
                   (-7435 - 5592*t2 + 2722*Power(t2,2) + 
                     188*Power(t2,3)) + 
                  Power(t1,2)*
                   (10445 + 10637*t2 - 1104*Power(t2,2) - 
                     178*Power(t2,3) + 172*Power(t2,4)) + 
                  t1*(-7657 - 13422*t2 - 5797*Power(t2,2) + 
                     2544*Power(t2,3) + 598*Power(t2,4) + 50*Power(t2,5))\
))) + Power(s,2)*(-12*Power(s1,7)*s2*(1 + s2 - t1) + 
            2*Power(s2,7)*t2*(7 - 7*t1 + 6*t2) + 
            Power(s2,6)*(24 + 15*t2 - 125*Power(t2,2) - 
               98*Power(t2,3) + Power(t1,2)*(24 + 49*t2) + 
               t1*(-48 - 64*t2 + 85*Power(t2,2))) + 
            Power(s2,5)*(-19 + 2*Power(t1,4) - 267*t2 - 
               235*Power(t2,2) + 132*Power(t2,3) + 175*Power(t2,4) - 
               Power(t1,3)*(67 + 92*t2) + 
               Power(t1,2)*(109 - 139*t2 - 199*Power(t2,2)) + 
               t1*(-25 + 498*t2 + 530*Power(t2,2) - 54*Power(t2,3))) + 
            Power(s2,4)*(-159 + 2*Power(t1,5) - 275*t2 + 
               366*Power(t2,2) + 693*Power(t2,3) + 329*Power(t2,4) - 
               94*Power(t2,5) + Power(t1,4)*(35 + 81*t2) + 
               Power(t1,3)*(174 + 517*t2 + 143*Power(t2,2)) + 
               Power(t1,2)*(-620 - 1490*t2 + 58*Power(t2,2) + 
                  277*Power(t2,3)) + 
               t1*(568 + 1167*t2 - 753*Power(t2,2) - 1055*Power(t2,3) - 
                  89*Power(t2,4))) - 
            Power(s2,3)*(211 + 10*Power(t1,6) + 56*t2 - 
               775*Power(t2,2) - 1442*Power(t2,3) - 247*Power(t2,4) + 
               308*Power(t2,5) - 11*Power(t2,6) + 
               Power(t1,5)*(-41 + 66*t2) + 
               Power(t1,4)*(469 + 182*t2 - 71*Power(t2,2)) + 
               Power(t1,3)*(-1278 - 21*t2 + 1540*Power(t2,2) + 
                  186*Power(t2,3)) + 
               2*Power(t1,2)*
                (737 - 383*t2 - 2187*Power(t2,2) - 505*Power(t2,3) + 
                  73*Power(t2,4)) + 
               t1*(-845 + 483*t2 + 3498*Power(t2,2) + 
                  2257*Power(t2,3) + 41*Power(t2,4) - 56*Power(t2,5))) + 
            (-1 + t1)*(-947 + 8*Power(t1,7) - 2623*t2 - 
               1335*Power(t2,2) + 905*Power(t2,3) + 376*Power(t2,4) - 
               116*Power(t2,5) + 
               2*Power(t1,6)*(-71 + 16*t2 + Power(t2,2)) - 
               Power(t1,5)*(-663 + 249*t2 + 103*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,4)*(-2175 + 840*t2 + 893*Power(t2,2) + 
                  40*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,2)*(-6402 - 5709*t2 + 1798*Power(t2,2) + 
                  1317*Power(t2,3) + 48*Power(t2,4) - 18*Power(t2,5)) + 
               Power(t1,3)*(4998 + 578*t2 - 2447*Power(t2,2) - 
                  217*Power(t2,3) + 41*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(3997 + 7125*t2 + 1234*Power(t2,2) - 
                  2106*Power(t2,3) - 439*Power(t2,4) + 135*Power(t2,5))) \
+ s2*(968 + 3230*t2 + 3807*Power(t2,2) + 291*Power(t2,3) - 
               1330*Power(t2,4) - 43*Power(t2,5) + 45*Power(t2,6) - 
               2*Power(t1,7)*(21 + 8*t2) + 
               Power(t1,6)*(452 + 266*t2 - 12*Power(t2,2)) + 
               Power(t1,5)*(-1855 - 1730*t2 + 69*Power(t2,2) + 
                  102*Power(t2,3)) + 
               Power(t1,4)*(4233 + 4971*t2 - 626*Power(t2,2) - 
                  943*Power(t2,3) - 31*Power(t2,4)) + 
               Power(t1,3)*(-6537 - 9537*t2 + 212*Power(t2,2) + 
                  2230*Power(t2,3) + 210*Power(t2,4) - 40*Power(t2,5)) \
+ t1*(-3974 - 10373*t2 - 8441*Power(t2,2) + 709*Power(t2,3) + 
                  2545*Power(t2,4) + 51*Power(t2,5) - 43*Power(t2,6)) + 
               Power(t1,2)*(6755 + 13189*t2 + 4989*Power(t2,2) - 
                  2404*Power(t2,3) - 1359*Power(t2,4) + 
                  15*Power(t2,5) - 3*Power(t2,6))) + 
            Power(s2,2)*(-24 + 6*Power(t1,7) + 786*t2 + 
               2552*Power(t2,2) + 2063*Power(t2,3) - 703*Power(t2,4) - 
               443*Power(t2,5) + 53*Power(t2,6) + 
               Power(t1,6)*(1 + 58*t2) - 
               2*Power(t1,5)*(23 + 245*t2 + 44*Power(t2,2)) + 
               Power(t1,4)*(288 + 2996*t2 + 1264*Power(t2,2) - 
                  90*Power(t2,3)) + 
               2*Power(t1,3)*
                (-221 - 3448*t2 - 2184*Power(t2,2) + 343*Power(t2,3) + 
                  80*Power(t2,4)) + 
               Power(t1,2)*(139 + 7074*t2 + 6835*Power(t2,2) + 
                  290*Power(t2,3) - 532*Power(t2,4) + 39*Power(t2,5)) - 
               t1*(-78 + 3528*t2 + 6257*Power(t2,2) + 
                  2758*Power(t2,3) - 1145*Power(t2,4) - 
                  317*Power(t2,5) + Power(t2,6))) + 
            Power(s1,6)*(-44*Power(s2,3) + 24*Power(-1 + t1,2) + 
               Power(s2,2)*(-74 + 91*t1 + 72*t2) + 
               s2*(30 - 21*Power(t1,2) + 72*t2 - 2*t1*(5 + 36*t2))) + 
            Power(s1,5)*(36*Power(s2,4) + 
               Power(s2,3)*(133 + 53*t1 + 209*t2) - 
               (-1 + t1)*(-26 + 31*t1 - 14*Power(t1,2) + 
                  12*Power(t1,3) - 120*t2 + 120*t1*t2) - 
               Power(s2,2)*(-547 + 35*Power(t1,2) - 317*t2 + 
                  180*Power(t2,2) + t1*(433 + 454*t2)) + 
               s2*(58*Power(t1,3) + 12*Power(t1,2)*(16 + 9*t2) - 
                  5*(-83 + 39*t2 + 36*Power(t2,2)) + 
                  3*t1*(-216 + 31*t2 + 60*Power(t2,2)))) + 
            Power(s1,4)*(160*Power(s2,5) + 
               Power(s2,4)*(254 - 71*t1 - 238*t2) - 
               Power(s2,3)*(-467 + 138*Power(t1,2) + 852*t2 + 
                  385*Power(t2,2) + 6*t1*(37 + 24*t2)) + 
               (-1 + t1)*(527 + 60*Power(t1,4) - 190*t2 - 
                  240*Power(t2,2) + Power(t1,2)*(91 + 16*t2) + 
                  Power(t1,3)*(2 + 20*t2) + 
                  t1*(-656 + 169*t2 + 240*Power(t2,2))) + 
               Power(s2,2)*(16 + 392*Power(t1,3) - 2691*t2 - 
                  475*Power(t2,2) + 240*Power(t2,3) + 
                  Power(t1,2)*(-632 + 119*t2) + 
                  t1*(335 + 2169*t2 + 905*Power(t2,2))) - 
               s2*(1045 + 259*Power(t1,4) + 1783*t2 - 525*Power(t2,2) - 
                  240*Power(t2,3) + 24*Power(t1,3)*(-31 + 8*t2) + 
                  3*Power(t1,2)*(543 + 331*t2 + 75*Power(t2,2)) + 
                  t1*(-2224 - 2883*t2 + 315*Power(t2,2) + 
                     240*Power(t2,3)))) + 
            Power(s1,3)*(98*Power(s2,6) + 
               Power(s2,5)*(-163 + 63*t1 - 655*t2) + 
               Power(s2,4)*(-715 - 427*Power(t1,2) - 1115*t2 + 
                  592*Power(t2,2) + t1*(1297 + 326*t2)) + 
               Power(s2,3)*(-1153 + 790*Power(t1,3) - 1816*t2 + 
                  2066*Power(t2,2) + 330*Power(t2,3) + 
                  Power(t1,2)*(-2273 + 392*t2) + 
                  t1*(2553 + 1043*t2 + 58*Power(t2,2))) - 
               (-1 + t1)*(562 + 60*Power(t1,5) + 1939*t2 - 
                  530*Power(t2,2) - 240*Power(t2,3) + 
                  4*Power(t1,4)*(-3 + 25*t2) + 
                  Power(t1,3)*(-682 + 299*t2 - 10*Power(t2,2)) + 
                  3*Power(t1,2)*(638 + 11*t2 + 38*Power(t2,2)) + 
                  t1*(-1905 - 2275*t2 + 456*Power(t2,2) + 
                     240*Power(t2,3))) - 
               Power(s2,2)*(2079 + 588*Power(t1,4) - 339*t2 - 
                  5234*Power(t2,2) - 210*Power(t2,3) + 
                  180*Power(t2,4) + 4*Power(t1,3)*(-266 + 249*t2) + 
                  2*Power(t1,2)*(676 - 716*t2 + 93*Power(t2,2)) + 
                  2*t1*(-1395 + 589*t2 + 2113*Power(t2,2) + 
                     450*Power(t2,3))) + 
               s2*(-1021 + 216*Power(t1,5) + 4275*t2 + 
                  2902*Power(t2,2) - 750*Power(t2,3) - 
                  180*Power(t2,4) + Power(t1,4)*(303 + 538*t2) + 
                  2*Power(t1,3)*(-1227 - 721*t2 + 134*Power(t2,2)) + 
                  Power(t1,2)*
                   (2743 + 4866*t2 + 1812*Power(t2,2) + 
                     240*Power(t2,3)) + 
                  t1*(228 - 8377*t2 - 4812*Power(t2,2) + 
                     530*Power(t2,3) + 180*Power(t2,4)))) + 
            Power(s1,2)*(12*Power(s2,7) + 
               Power(s2,6)*(-137 + 97*t1 - 294*t2) + 
               Power(s2,5)*(-316 - 322*Power(t1,2) + 
                  t1*(734 - 168*t2) + 446*t2 + 1005*Power(t2,2)) + 
               Power(s2,4)*(363 + 565*Power(t1,3) + 2015*t2 + 
                  1797*Power(t2,2) - 708*Power(t2,3) + 
                  11*Power(t1,2)*(-71 + 93*t2) - 
                  t1*(333 + 3433*t2 + 528*Power(t2,2))) - 
               Power(s2,3)*(-934 + 494*Power(t1,4) - 3576*t2 - 
                  2478*Power(t2,2) + 2416*Power(t2,3) + 
                  110*Power(t2,4) + Power(t1,3)*(509 + 1498*t2) + 
                  Power(t1,2)*(-4210 - 4836*t2 + 516*Power(t2,2)) + 
                  t1*(3959 + 6739*t2 + 1461*Power(t2,2) - 
                     160*Power(t2,3))) + 
               (-1 + t1)*(-1506 + 12*Power(t1,6) + 1829*t2 + 
                  2673*Power(t2,2) - 710*Power(t2,3) - 
                  120*Power(t2,4) + 12*Power(t1,5)*(4 + 7*t2) + 
                  Power(t1,4)*(655 + 12*t2 + 18*Power(t2,2)) - 
                  Power(t1,3)*
                   (2770 + 1165*t2 - 633*Power(t2,2) + 30*Power(t2,3)) \
+ Power(t1,2)*(2273 + 4261*t2 - 159*Power(t2,2) + 106*Power(t2,3)) + 
                  t1*(1330 - 5210*t2 - 3021*Power(t2,2) + 
                     664*Power(t2,3) + 120*Power(t2,4))) + 
               Power(s2,2)*(2441 + 290*Power(t1,5) + 6385*t2 - 
                  1429*Power(t2,2) - 5026*Power(t2,3) + 
                  160*Power(t2,4) + 72*Power(t2,5) + 
                  2*Power(t1,4)*(540 + 403*t2) + 
                  Power(t1,3)*(-6040 - 694*t2 + 976*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (4528 + 1251*t2 - 750*Power(t2,2) + 97*Power(t2,3)) \
+ t1*(-6889 - 8478*t2 + 2496*Power(t2,2) + 3994*Power(t2,3) + 
                     445*Power(t2,4))) - 
               s2*(-3734 + 138*Power(t1,6) - 2779*t2 + 
                  6745*Power(t2,2) + 2158*Power(t2,3) - 
                  600*Power(t2,4) - 72*Power(t2,5) + 
                  Power(t1,5)*(185 + 186*t2) + 
                  Power(t1,4)*(-1128 + 1799*t2 + 330*Power(t2,2)) + 
                  Power(t1,3)*
                   (2031 - 6578*t2 - 862*Power(t2,2) + 232*Power(t2,3)) \
+ Power(t1,2)*(-5816 + 6150*t2 + 6204*Power(t2,2) + 1398*Power(t2,3) + 
                     135*Power(t2,4)) + 
                  t1*(8326 + 1267*t2 - 12627*Power(t2,2) - 
                     3618*Power(t2,3) + 480*Power(t2,4) + 72*Power(t2,5)\
))) + s1*(2*Power(s2,7)*(-7 + 7*t1 - 12*t2) + 
               Power(s2,6)*(-15 - 49*Power(t1,2) + t1*(64 - 182*t2) + 
                  262*t2 + 294*Power(t2,2)) + 
               Power(s2,5)*(286 + 101*Power(t1,3) + 551*t2 - 
                  415*Power(t2,2) - 685*Power(t2,3) + 
                  Power(t1,2)*(140 + 521*t2) + 
                  t1*(-527 - 1264*t2 + 159*Power(t2,2))) + 
               Power(s2,4)*(317 - 113*Power(t1,4) - 705*t2 - 
                  1993*Power(t2,2) - 1265*Power(t2,3) + 
                  412*Power(t2,4) - 9*Power(t1,3)*(61 + 80*t2) + 
                  Power(t1,2)*(1692 + 771*t2 - 873*Power(t2,2)) + 
                  t1*(-1347 + 1026*t2 + 3191*Power(t2,2) + 
                     362*Power(t2,3))) + 
               Power(s2,3)*(-30 + 121*Power(t1,5) - 1605*t2 - 
                  3865*Power(t2,2) - 1376*Power(t2,3) + 
                  1377*Power(t2,4) - 11*Power(t2,5) + 
                  Power(t1,4)*(310 + 451*t2) + 
                  Power(t1,3)*(-579 + 1861*t2 + 894*Power(t2,2)) + 
                  Power(t1,2)*
                   (-340 - 8188*t2 - 3573*Power(t2,2) + 
                     408*Power(t2,3)) + 
                  t1*(518 + 7117*t2 + 6443*Power(t2,2) + 
                     681*Power(t2,3) - 183*Power(t2,4))) - 
               (-1 + t1)*(-2472 - 2971*t2 + 2172*Power(t2,2) + 
                  1637*Power(t2,3) - 460*Power(t2,4) - 24*Power(t2,5) + 
                  4*Power(t1,6)*(22 + 3*t2) + 
                  Power(t1,5)*(-367 - 89*t2 + 22*Power(t2,2)) + 
                  Power(t1,4)*
                   (903 + 1604*t2 + 64*Power(t2,2) - 24*Power(t2,3)) + 
                  Power(t1,3)*
                   (446 - 5031*t2 - 700*Power(t2,2) + 
                     377*Power(t2,3) - 10*Power(t2,4)) + 
                  Power(t1,2)*
                   (-5291 + 3547*t2 + 3664*Power(t2,2) - 
                     53*Power(t2,3) + 4*Power(t2,4)) + 
                  t1*(6687 + 3012*t2 - 5411*Power(t2,2) - 
                     1841*Power(t2,3) + 481*Power(t2,4) + 24*Power(t2,5)\
)) - Power(s2,2)*(911 + 98*Power(t1,6) + 4925*t2 + 6369*Power(t2,2) - 
                  1777*Power(t2,3) - 2379*Power(t2,4) + 
                  191*Power(t2,5) + 12*Power(t2,6) + 
                  Power(t1,5)*(-263 + 210*t2) + 
                  Power(t1,4)*(2235 + 2160*t2 + 128*Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-3229 - 4912*t2 + 528*Power(t2,2) + 
                     266*Power(t2,3)) + 
                  Power(t1,2)*
                   (7498 + 15175*t2 + 1440*Power(t2,2) - 
                     1232*Power(t2,3) + 131*Power(t2,4)) + 
                  t1*(-4021 - 12770*t2 - 8446*Power(t2,2) + 
                     2798*Power(t2,3) + 1821*Power(t2,4) + 
                     86*Power(t2,5))) + 
               s2*(-3095 + 24*Power(t1,7) - 7679*t2 - 2049*Power(t2,2) + 
                  4845*Power(t2,3) + 667*Power(t2,4) - 255*Power(t2,5) - 
                  12*Power(t2,6) + 4*Power(t1,6)*(-17 + 35*t2) + 
                  Power(t1,5)*(1179 + 54*t2 - 132*Power(t2,2)) + 
                  Power(t1,4)*
                   (-4711 - 252*t2 + 2439*Power(t2,2) + 82*Power(t2,3)) \
+ Power(t1,3)*(9858 + 1679*t2 - 6354*Power(t2,2) - 374*Power(t2,3) + 
                     138*Power(t2,4)) + 
                  Power(t1,2)*
                   (-13310 - 11115*t2 + 5811*Power(t2,2) + 
                     4326*Power(t2,3) + 372*Power(t2,4) + 36*Power(t2,5)\
) + t1*(10123 + 17177*t2 + 330*Power(t2,2) - 9019*Power(t2,3) - 
                     1092*Power(t2,4) + 225*Power(t2,5) + 12*Power(t2,6))\
))) + s*(4*Power(s1,7)*s2*(2*Power(s2,2) - 6*s2*(-1 + t1) + 
               3*Power(-1 + t1,2)) + 
            2*Power(s2,7)*t2*(-1 - Power(t1,2) + t1*(2 - 9*t2) + 9*t2 + 
               8*Power(t2,2)) + 
            Power(s1,6)*(Power(s2,4) + 16*Power(-1 + t1,3) + 
               Power(s2,3)*(51 - 53*t1 - 48*t2) + 
               s2*(-1 + t1)*(52 - 61*t1 + 7*Power(t1,2) + 72*t2 - 
                  72*t1*t2) + 
               Power(s2,2)*(15 - 35*t1 + 29*Power(t1,2) - 144*t2 + 
                  144*t1*t2)) + 
            Power(s2,6)*t2*(40 + 12*Power(t1,3) + 30*t2 - 
               81*Power(t2,2) - 59*Power(t2,3) + 
               8*Power(t1,2)*(2 + 5*t2) + 
               t1*(-68 - 70*t2 + 43*Power(t2,2))) - 
            Power(s2,5)*(-16 + 15*t2 + 16*Power(t1,4)*t2 + 
               274*Power(t2,2) + 264*Power(t2,3) + 27*Power(t2,4) - 
               58*Power(t2,5) + 
               Power(t1,3)*(16 + 73*t2 + 47*Power(t2,2)) + 
               Power(t1,2)*(-48 - 179*t2 + 132*Power(t2,2) + 
                  122*Power(t2,3)) - 
               3*t1*(-16 - 25*t2 + 151*Power(t2,2) + 150*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s2,4)*(-25 - 199*t2 + 12*Power(t1,5)*t2 - 
               378*Power(t2,2) - 148*Power(t2,3) + 144*Power(t2,4) + 
               145*Power(t2,5) - 15*Power(t2,6) + 
               Power(t1,4)*(17 + 53*t2 - 4*Power(t2,2)) + 
               Power(t1,3)*(-26 + 198*t2 + 651*Power(t2,2) + 
                  121*Power(t2,3)) + 
               Power(t1,2)*(-24 - 802*t2 - 1872*Power(t2,2) - 
                  524*Power(t2,3) + 99*Power(t2,4)) + 
               t1*(58 + 738*t2 + 1603*Power(t2,2) + 483*Power(t2,3) - 
                  229*Power(t2,4) - 53*Power(t2,5))) + 
            Power(-1 + t1,2)*(-398 + 8*Power(t1,6)*(-2 + t2) - 1137*t2 - 
               480*Power(t2,2) + 486*Power(t2,3) + 146*Power(t2,4) - 
               65*Power(t2,5) - 
               2*Power(t1,5)*(-7 + 64*t2 + 10*Power(t2,2)) + 
               Power(t1,4)*(-133 + 695*t2 + 281*Power(t2,2) - 
                  13*Power(t2,3)) + 
               Power(t1,3)*(1021 - 798*t2 - 1097*Power(t2,2) + 
                  20*Power(t2,3) + 28*Power(t2,4)) - 
               Power(t1,2)*(2011 + 1344*t2 - 1351*Power(t2,2) - 
                  515*Power(t2,3) + 74*Power(t2,4) + 3*Power(t2,5)) + 
               t1*(1523 + 2698*t2 + 7*Power(t2,2) - 1071*Power(t2,3) - 
                  76*Power(t2,4) + 71*Power(t2,5))) - 
            Power(s2,3)*(-15 + 133*t2 + 436*Power(t2,2) + 
               449*Power(t2,3) - 384*Power(t2,4) - 310*Power(t2,5) + 
               47*Power(t2,6) + 2*Power(t1,6)*(4 + 7*t2) - 
               Power(t1,5)*(96 + 129*t2 + 49*Power(t2,2)) + 
               Power(t1,4)*(401 + 1199*t2 + 775*Power(t2,2) + 
                  16*Power(t2,3)) + 
               2*Power(t1,3)*
                (-346 - 1463*t2 - 1221*Power(t2,2) + 94*Power(t2,3) + 
                  62*Power(t2,4)) + 
               Power(t1,2)*(510 + 2834*t2 + 2881*Power(t2,2) - 
                  487*Power(t2,3) - 539*Power(t2,4) + 16*Power(t2,5)) - 
               t1*(116 + 1125*t2 + 1601*Power(t2,2) + 182*Power(t2,3) - 
                  955*Power(t2,4) - 220*Power(t2,5) + 13*Power(t2,6))) - 
            s2*(-1 + t1)*(-543 + 8*Power(t1,7) - 1914*t2 - 
               1931*Power(t2,2) + 292*Power(t2,3) + 807*Power(t2,4) - 
               22*Power(t2,5) - 41*Power(t2,6) + 
               4*Power(t1,6)*(-30 - 9*t2 + 2*Power(t2,2)) - 
               2*Power(t1,5)*
                (-190 - 83*t2 + 79*Power(t2,2) + 13*Power(t2,3)) + 
               Power(t1,4)*(-656 + 35*t2 + 1141*Power(t2,2) + 
                  341*Power(t2,3) - 5*Power(t2,4)) + 
               Power(t1,3)*(1423 + 684*t2 - 2755*Power(t2,2) - 
                  1222*Power(t2,3) + 18*Power(t2,4) + 22*Power(t2,5)) + 
               Power(t1,2)*(-2389 - 3987*t2 + 776*Power(t2,2) + 
                  2556*Power(t2,3) + 460*Power(t2,4) - 61*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(1897 + 5052*t2 + 2923*Power(t2,2) - 
                  1911*Power(t2,3) - 1350*Power(t2,4) + 95*Power(t2,5) + 
                  42*Power(t2,6))) + 
            Power(s2,2)*(-81 - 820*t2 - 1761*Power(t2,2) - 
               619*Power(t2,3) + 905*Power(t2,4) + 279*Power(t2,5) - 
               83*Power(t2,6) + 8*Power(t1,7)*(2 + t2) - 
               Power(t1,6)*(209 + 173*t2 + 12*Power(t2,2)) + 
               Power(t1,5)*(827 + 1268*t2 + 140*Power(t2,2) - 
                  66*Power(t2,3)) + 
               Power(t1,4)*(-1427 - 3248*t2 - 145*Power(t2,2) + 
                  794*Power(t2,3) + 58*Power(t2,4)) + 
               Power(t1,3)*(1250 + 4482*t2 + 35*Power(t2,2) - 
                  2400*Power(t2,3) - 361*Power(t2,4) + 39*Power(t2,5)) + 
               Power(t1,2)*(-659 - 4295*t2 - 2038*Power(t2,2) + 
                  2468*Power(t2,3) + 1619*Power(t2,4) + 
                  16*Power(t2,5) + Power(t2,6)) + 
               t1*(283 + 2778*t2 + 3781*Power(t2,2) - 167*Power(t2,3) - 
                  2232*Power(t2,4) - 342*Power(t2,5) + 91*Power(t2,6))) + 
            Power(s1,5)*(-50*Power(s2,5) + 
               Power(s2,4)*(-85 + 9*t1 + 10*t2) - 
               Power(-1 + t1,2)*
                (-28 + 49*t1 - 26*Power(t1,2) + 8*Power(t1,3) - 80*t2 + 
                  80*t1*t2) - 
               2*Power(s2,3)*
                (156 + 7*Power(t1,2) + 104*t2 - 60*Power(t2,2) - 
                  2*t1*(73 + 63*t2)) + 
               Power(s2,2)*(-542 - 15*Power(t1,3) + 8*t2 + 
                  360*Power(t2,2) - Power(t1,2)*(343 + 146*t2) + 
                  t1*(908 + 84*t2 - 360*Power(t2,2))) + 
               s2*(-1 + t1)*(236 + 54*Power(t1,3) + 
                  Power(t1,2)*(4 - 34*t2) - 301*t2 - 180*Power(t2,2) + 
                  t1*(-260 + 347*t2 + 180*Power(t2,2)))) + 
            Power(s1,4)*(-59*Power(s2,6) + 
               Power(s2,5)*(-9 + 13*t1 + 258*t2) + 
               Power(s2,4)*(124 + 53*Power(t1,2) + 485*t2 - 
                  65*Power(t2,2) - t1*(195 + 89*t2)) + 
               Power(-1 + t1,2)*
                (261 + 20*Power(t1,4) - 165*t2 - 160*Power(t2,2) + 
                  10*Power(t1,3)*(3 + 2*t2) - 
                  Power(t1,2)*(23 + 71*t2) + 
                  t1*(-264 + 231*t2 + 160*Power(t2,2))) + 
               Power(s2,3)*(109 - 192*Power(t1,3) + 1570*t2 + 
                  275*Power(t2,2) - 160*Power(t2,3) + 
                  Power(t1,2)*(618 + 52*t2) - 
                  t1*(675 + 1412*t2 + 465*Power(t2,2))) - 
               s2*(-1 + t1)*(860 + 79*Power(t1,4) + 962*t2 - 
                  725*Power(t2,2) - 240*Power(t2,3) + 
                  2*Power(t1,3)*(-137 + 99*t2) + 
                  Power(t1,2)*(1016 + 75*t2 - 65*Power(t2,2)) + 
                  t1*(-1751 - 1065*t2 + 820*Power(t2,2) + 
                     240*Power(t2,3))) + 
               Power(s2,2)*(599 + 228*Power(t1,4) + 2487*t2 - 
                  265*Power(t2,2) - 480*Power(t2,3) + 
                  Power(t1,3)*(-969 + 59*t2) + 
                  Power(t1,2)*(2101 + 1508*t2 + 295*Power(t2,2)) + 
                  t1*(-1970 - 4094*t2 + 105*Power(t2,2) + 
                     480*Power(t2,3)))) - 
            Power(s1,3)*(16*Power(s2,7) + 
               Power(s2,6)*(-81 + 43*t1 - 236*t2) + 
               Power(s2,5)*(-280 - 134*Power(t1,2) - 54*t2 + 
                  532*Power(t2,2) + t1*(478 + 54*t2)) + 
               Power(s2,4)*(-113 + 283*Power(t1,3) + 492*t2 + 
                  1090*Power(t2,2) - 140*Power(t2,3) + 
                  9*Power(t1,2)*(-97 + 26*t2) + 
                  t1*(635 - 766*t2 - 266*Power(t2,2))) + 
               Power(-1 + t1,2)*
                (357 + 8*Power(t1,5) + 933*t2 - 392*Power(t2,2) - 
                  160*Power(t2,3) + Power(t1,4)*(-2 + 40*t2) + 
                  Power(t1,2)*(879 - 191*t2 - 60*Power(t2,2)) + 
                  3*Power(t1,3)*(-75 + 58*t2 + 4*Power(t2,2)) + 
                  10*t1*(-108 - 86*t2 + 47*Power(t2,2) + 16*Power(t2,3))\
) + Power(s2,3)*(-409 - 298*Power(t1,4) + Power(t1,3)*(635 - 600*t2) + 
                  611*t2 + 3148*Power(t2,2) + 40*Power(t2,3) - 
                  120*Power(t2,4) + 
                  Power(t1,2)*(-165 + 2093*t2 + 56*Power(t2,2)) - 
                  t1*(-253 + 2680*t2 + 2704*Power(t2,2) + 
                     400*Power(t2,3))) - 
               s2*(-1 + t1)*(-181 + 46*Power(t1,5) + 3343*t2 + 
                  1448*Power(t2,2) - 930*Power(t2,3) - 
                  180*Power(t2,4) + Power(t1,4)*(381 + 148*t2) + 
                  Power(t1,3)*(-2075 - 508*t2 + 292*Power(t2,2)) + 
                  Power(t1,2)*
                   (3354 + 3124*t2 + 140*Power(t2,2) - 60*Power(t2,3)) \
+ t1*(-1495 - 6387*t2 - 1540*Power(t2,2) + 1030*Power(t2,3) + 
                     180*Power(t2,4))) + 
               Power(s2,2)*(-958 + 132*Power(t1,5) + 2578*t2 + 
                  4488*Power(t2,2) - 680*Power(t2,3) - 360*Power(t2,4) + 
                  Power(t1,4)*(330 + 602*t2) + 
                  6*Power(t1,3)*(-395 - 454*t2 + 21*Power(t2,2)) + 
                  Power(t1,2)*
                   (2617 + 7130*t2 + 2482*Power(t2,2) + 300*Power(t2,3)) \
+ t1*(259 - 7630*t2 - 7176*Power(t2,2) + 560*Power(t2,3) + 
                     360*Power(t2,4)))) + 
            Power(s1,2)*(-6*Power(s2,7)*(-3 + 3*t1 - 8*t2) + 
               Power(s2,6)*(42 + 52*Power(t1,2) - 243*t2 - 
                  354*Power(t2,2) + t1*(-94 + 129*t2)) + 
               Power(s2,5)*(-238 - 129*Power(t1,3) + 
                  Power(t1,2)*(68 - 378*t2) - 812*t2 - 
                  108*Power(t2,2) + 548*Power(t2,3) + 
                  t1*(299 + 1382*t2 + 84*Power(t2,2))) + 
               Power(s2,4)*(-374 + 156*Power(t1,4) - 318*t2 + 
                  756*Power(t2,2) + 1210*Power(t2,3) - 
                  145*Power(t2,4) + Power(t1,3)*(227 + 631*t2) + 
                  2*Power(t1,2)*(-750 - 1051*t2 + 204*Power(t2,2)) + 
                  t1*(1491 + 1585*t2 - 1176*Power(t2,2) - 
                     354*Power(t2,3))) + 
               Power(-1 + t1,2)*
                (-615 + 1130*t2 + 1229*Power(t2,2) - 466*Power(t2,3) - 
                  80*Power(t2,4) + 2*Power(t1,5)*(19 + 6*t2) + 
                  Power(t1,4)*(283 - 47*t2 + 20*Power(t2,2)) + 
                  Power(t1,2)*
                   (1642 + 1941*t2 - 387*Power(t2,2) - 14*Power(t2,3)) \
- Power(t1,3)*(1449 + 246*t2 - 286*Power(t2,2) + 4*Power(t2,3)) + 
                  t1*(143 - 2979*t2 - 1004*Power(t2,2) + 
                     514*Power(t2,3) + 80*Power(t2,4))) - 
               Power(s2,3)*(434 + 101*Power(t1,5) + 1251*t2 - 
                  1279*Power(t2,2) - 3144*Power(t2,3) + 
                  215*Power(t2,4) + 48*Power(t2,5) + 
                  Power(t1,4)*(529 + 528*t2) + 
                  Power(t1,3)*(-2554 - 802*t2 + 748*Power(t2,2)) + 
                  Power(t1,2)*
                   (3241 - 481*t2 - 2871*Power(t2,2) + 4*Power(t2,3)) + 
                  t1*(-1751 - 544*t2 + 4290*Power(t2,2) + 
                     2560*Power(t2,3) + 135*Power(t2,4))) - 
               s2*(-1 + t1)*(-2081 + 24*Power(t1,6) - 282*t2 + 
                  4913*Power(t2,2) + 932*Power(t2,3) - 
                  670*Power(t2,4) - 72*Power(t2,5) + 
                  2*Power(t1,5)*(-9 + 23*t2) + 
                  Power(t1,4)*(847 + 1031*t2 + 54*Power(t2,2)) + 
                  Power(t1,3)*
                   (-2819 - 4824*t2 - 176*Power(t2,2) + 
                     228*Power(t2,3)) + 
                  Power(t1,2)*
                   (966 + 8252*t2 + 3660*Power(t2,2) + 
                     10*Power(t2,3) - 25*Power(t2,4)) + 
                  t1*(3085 - 4133*t2 - 8871*Power(t2,2) - 
                     830*Power(t2,3) + 725*Power(t2,4) + 72*Power(t2,5))\
) + Power(s2,2)*(-1758 + 64*Power(t1,6) - 2705*t2 + 4264*Power(t2,2) + 
                  3962*Power(t2,3) - 755*Power(t2,4) - 144*Power(t2,5) + 
                  2*Power(t1,5)*(99 + 71*t2) + 
                  2*Power(t1,4)*(-439 + 778*t2 + 289*Power(t2,2)) + 
                  Power(t1,3)*
                   (1115 - 6940*t2 - 2902*Power(t2,2) + 174*Power(t2,3)) \
+ Power(t1,2)*(-2608 + 7050*t2 + 9576*Power(t2,2) + 1828*Power(t2,3) + 
                     155*Power(t2,4)) + 
                  t1*(3867 + 927*t2 - 11582*Power(t2,2) - 
                     6044*Power(t2,3) + 735*Power(t2,4) + 144*Power(t2,5)\
))) + s1*(2*Power(s2,7)*(1 + Power(t1,2) - 18*t2 - 24*Power(t2,2) + 
                  2*t1*(-1 + 9*t2)) - 
               Power(s2,6)*(40 + 12*Power(t1,3) + 72*t2 - 
                  243*Power(t2,2) - 236*Power(t2,3) + 
                  4*Power(t1,2)*(4 + 23*t2) + 
                  t1*(-68 - 164*t2 + 129*Power(t2,2))) + 
               Power(s2,5)*(1 + 16*Power(t1,4) + 508*t2 + 
                  796*Power(t2,2) + 90*Power(t2,3) - 282*Power(t2,4) + 
                  3*Power(t1,3)*(29 + 60*t2) + 
                  Power(t1,2)*(-221 + 52*t2 + 366*Power(t2,2)) - 
                  t1*(-117 + 740*t2 + 1354*Power(t2,2) + 58*Power(t2,3))) \
+ Power(s2,4)*(178 - 16*Power(t1,5) + 724*t2 + 353*Power(t2,2) - 
                  532*Power(t2,3) - 665*Power(t2,4) + 74*Power(t2,5) - 
                  2*Power(t1,4)*(53 + 86*t2) + 
                  Power(t1,3)*(6 - 790*t2 - 469*Power(t2,2)) + 
                  Power(t1,2)*
                   (548 + 3228*t2 + 1753*Power(t2,2) - 326*Power(t2,3)) \
+ t1*(-610 - 2990*t2 - 1433*Power(t2,2) + 834*Power(t2,3) + 
                     221*Power(t2,4))) + 
               Power(s2,3)*(158 + 22*Power(t1,6) + 822*t2 + 
                  1291*Power(t2,2) - 1161*Power(t2,3) - 
                  1564*Power(t2,4) + 184*Power(t2,5) + 8*Power(t2,6) + 
                  5*Power(t1,5)*(-7 + 16*t2) + 
                  6*Power(t1,4)*(134 + 190*t2 + 41*Power(t2,2)) + 
                  Power(t1,3)*
                   (-2410 - 4624*t2 + 21*Power(t2,2) + 464*Power(t2,3)) \
+ Power(t1,2)*(2596 + 5710*t2 - 1133*Power(t2,2) - 1935*Power(t2,3) + 
                     38*Power(t2,4)) - 
                  t1*(1135 + 3128*t2 + 473*Power(t2,2) - 
                     3240*Power(t2,3) - 1196*Power(t2,4) + 12*Power(t2,5)\
)) - Power(-1 + t1,2)*(-1093 + 16*Power(t1,6) - 1153*t2 + 
                  1259*Power(t2,2) + 703*Power(t2,3) - 276*Power(t2,4) - 
                  16*Power(t2,5) + 
                  Power(t1,4)*(776 + 542*t2 - 58*Power(t2,2)) + 
                  4*Power(t1,5)*(-38 + 3*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-1121 + 2705*t2 + 1577*Power(t2,2) - 
                     293*Power(t2,3) - 2*Power(t2,4)) - 
                  Power(t1,3)*
                   (981 + 2386*t2 + Power(t2,2) - 170*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  t1*(2549 + 364*t2 - 2970*Power(t2,2) - 
                     484*Power(t2,3) + 297*Power(t2,4) + 16*Power(t2,5))) \
+ s2*(-1 + t1)*(-1851 - 4116*t2 + 191*Power(t2,2) + 3237*Power(t2,3) + 
                  188*Power(t2,4) - 257*Power(t2,5) - 12*Power(t2,6) + 
                  8*Power(t1,6)*(3 + 4*t2) - 
                  2*Power(t1,5)*(3 + 94*t2 + 13*Power(t2,2)) + 
                  Power(t1,4)*
                   (237 + 1952*t2 + 991*Power(t2,2) - 20*Power(t2,3)) + 
                  Power(t1,3)*
                   (477 - 5290*t2 - 3971*Power(t2,2) + 76*Power(t2,3) + 
                     102*Power(t2,4)) - 
                  2*Power(t1,2)*
                   (1864 - 613*t2 - 3727*Power(t2,2) - 
                     1006*Power(t2,3) + 60*Power(t2,4) + Power(t2,5)) + 
                  t1*(4847 + 6392*t2 - 4549*Power(t2,2) - 
                     5585*Power(t2,3) + 271*Power(t2,5) + 12*Power(t2,6))\
) - Power(s2,2)*(-833 + 12*Power(t1,7) - 3541*t2 - 2366*Power(t2,2) + 
                  3190*Power(t2,3) + 1698*Power(t2,4) - 400*Power(t2,5) - 
                  24*Power(t2,6) + Power(t1,6)*(-66 + 64*t2) + 
                  Power(t1,5)*(849 + 244*t2 - 56*Power(t2,2)) + 
                  Power(t1,4)*
                   (-2703 - 781*t2 + 2020*Power(t2,2) + 262*Power(t2,3)) \
+ Power(t1,3)*(4244 + 890*t2 - 6970*Power(t2,2) - 1508*Power(t2,3) + 
                     131*Power(t2,4)) + 
                  Power(t1,2)*
                   (-4350 - 4558*t2 + 6901*Power(t2,2) + 
                     6166*Power(t2,3) + 527*Power(t2,4) + 34*Power(t2,5)) \
+ t1*(2847 + 7682*t2 + 501*Power(t2,2) - 8154*Power(t2,3) - 
                     2396*Power(t2,4) + 420*Power(t2,5) + 24*Power(t2,6)))\
)))*R1q(1 - s + s2 - t1))/
     ((-1 + s1)*Power(-1 + s + t1,3)*(-1 + s - s2 + t1)*(s - s2 + t1)*
       (-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       (-1 + s - s*s2 + s1*s2 + t1 - s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) + 
    (8*(101 + 6*s2 + 6*Power(s1,6)*s2 - 21*Power(s2,2) + 8*Power(s2,3) - 
         2*Power(s2,4) - 213*t1 + 48*s2*t1 + 9*Power(s2,2)*t1 - 
         10*Power(s2,3)*t1 + 2*Power(s2,4)*t1 + 71*Power(t1,2) - 
         76*s2*Power(t1,2) + 23*Power(s2,2)*Power(t1,2) + 
         2*Power(s2,3)*Power(t1,2) + 77*Power(t1,3) + 18*s2*Power(t1,3) - 
         11*Power(s2,2)*Power(t1,3) - 36*Power(t1,4) + 4*s2*Power(t1,4) + 
         Power(s1,5)*(7*Power(s2,2) - 2*(3 - 3*t1 + Power(t1,2)) + 
            s2*(14 + 19*t1 - 30*t2)) + 154*t2 + 158*s2*t2 - 
         34*Power(s2,2)*t2 - 8*Power(s2,3)*t2 + 8*Power(s2,4)*t2 - 
         2*Power(s2,5)*t2 - 188*t1*t2 - 102*s2*t1*t2 + 
         68*Power(s2,2)*t1*t2 - 28*Power(s2,3)*t1*t2 - 
         2*Power(s2,4)*t1*t2 - 82*Power(t1,2)*t2 - 
         126*s2*Power(t1,2)*t2 + 10*Power(s2,2)*Power(t1,2)*t2 + 
         14*Power(s2,3)*Power(t1,2)*t2 + 136*Power(t1,3)*t2 + 
         54*s2*Power(t1,3)*t2 - 10*Power(s2,2)*Power(t1,3)*t2 - 
         20*Power(t1,4)*t2 - 18*Power(t2,2) + 185*s2*Power(t2,2) + 
         58*Power(s2,2)*Power(t2,2) - 37*Power(s2,3)*Power(t2,2) + 
         13*Power(s2,4)*Power(t2,2) + 120*t1*Power(t2,2) - 
         19*s2*t1*Power(t2,2) + 64*Power(s2,2)*t1*Power(t2,2) - 
         27*Power(s2,3)*t1*Power(t2,2) - 3*Power(s2,4)*t1*Power(t2,2) - 
         125*Power(t1,2)*Power(t2,2) - 176*s2*Power(t1,2)*Power(t2,2) - 
         18*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         25*Power(t1,3)*Power(t2,2) + 32*s2*Power(t1,3)*Power(t2,2) - 
         60*Power(t2,3) - 37*s2*Power(t2,3) + 
         35*Power(s2,2)*Power(t2,3) + Power(s2,3)*Power(t2,3) + 
         Power(s2,4)*Power(t2,3) + 44*t1*Power(t2,3) + 
         128*s2*t1*Power(t2,3) + 50*Power(s2,2)*t1*Power(t2,3) + 
         18*Power(t1,2)*Power(t2,3) - 34*s2*Power(t1,2)*Power(t2,3) - 
         12*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         6*Power(t1,3)*Power(t2,3) + 13*Power(t2,4) - 55*s2*Power(t2,4) - 
         19*Power(s2,2)*Power(t2,4) - 2*Power(s2,3)*Power(t2,4) - 
         19*t1*Power(t2,4) - 13*s2*t1*Power(t2,4) + 
         9*Power(s2,2)*t1*Power(t2,4) + 6*Power(t1,2)*Power(t2,4) + 
         6*s2*Power(t1,2)*Power(t2,4) + 2*Power(t2,5) + 
         15*s2*Power(t2,5) + Power(s2,2)*Power(t2,5) - 
         6*s2*t1*Power(t2,5) + 
         Power(s,5)*(4 + 6*s1*(-1 + s2) + t1 + 4*Power(t1,2) + t2 - 
            8*t1*t2 + 4*Power(t2,2) - s2*(4 + t1 + t2)) + 
         Power(s1,4)*(-14 - 2*Power(s2,3) + 2*Power(t1,3) + 
            Power(s2,2)*(17 + 33*t1 - 29*t2) + 24*t2 - 5*t1*(1 + 4*t2) + 
            Power(t1,2)*(17 + 6*t2) + 
            s2*(-74 + 3*Power(t1,2) + t1*(29 - 76*t2) - 45*t2 + 
               60*Power(t2,2))) + 
         Power(s,4)*(9 - 6*Power(s1,2)*(-2 + 3*s2) + 21*t1 + 
            2*Power(t1,2) + 2*Power(t1,3) - 5*t2 + 13*t1*t2 + 
            6*Power(t1,2)*t2 - 7*Power(t2,2) - 18*t1*Power(t2,2) + 
            10*Power(t2,3) + Power(s2,2)*(16 + 2*t1 + 5*t2) - 
            s2*(29 + 12*t1 + 11*Power(t1,2) + 13*t2 - 15*t1*t2 + 
               12*Power(t2,2)) + 
            s1*(-16 - 23*Power(s2,2) - 30*t1 - 18*Power(t1,2) + t2 + 
               28*t1*t2 - 10*Power(t2,2) + s2*(49 + 17*t1 + 14*t2))) + 
         Power(s1,3)*(67 - 5*Power(s2,4) + Power(t1,3)*(3 - 4*t2) + 
            31*t2 - 38*Power(t2,2) + 
            Power(t1,2)*(23 - 57*t2 - 6*Power(t2,2)) + 
            t1*(-89 + 30*t2 + 24*Power(t2,2)) + 
            Power(s2,3)*(21*t1 + 4*(7 + t2)) - 
            Power(s2,2)*(131 + 32*t2 - 44*Power(t2,2) + 
               t1*(54 + 92*t2)) + 
            s2*(-114 + 8*Power(t1,3) + Power(t1,2)*(92 - 25*t2) + 
               281*t2 + 36*Power(t2,2) - 60*Power(t2,3) + 
               t1*(-73 - 64*t2 + 120*Power(t2,2)))) + 
         Power(s1,2)*(97 - 2*Power(s2,5) + 8*Power(t1,4) - 192*t2 - 
            7*Power(t2,2) + 30*Power(t2,3) + 
            Power(s2,4)*(11 + 7*t1 + 9*t2) + 
            Power(t1,3)*(53 - 22*t2 + 2*Power(t2,2)) + 
            Power(t1,2)*(-116 - 2*t2 + 69*Power(t2,2) + 2*Power(t2,3)) - 
            4*t1*(10 - 51*t2 + 16*Power(t2,2) + 3*Power(t2,3)) - 
            Power(s2,3)*(60 + 5*Power(t1,2) + 59*t2 + 4*Power(t2,2) + 
               t1*(21 + 32*t2)) + 
            Power(s2,2)*(87 + 6*Power(t1,3) + Power(t1,2)*(7 - 18*t2) + 
               297*t2 - 6*Power(t2,2) - 28*Power(t2,3) + 
               2*t1*(22 + 79*t2 + 47*Power(t2,2))) + 
            s2*(378 + 193*t2 - 395*Power(t2,2) + 22*Power(t2,3) + 
               30*Power(t2,4) - 8*Power(t1,3)*(-3 + 2*t2) + 
               Power(t1,2)*(-157 - 188*t2 + 47*Power(t2,2)) + 
               t1*(-241 + 244*t2 + 28*Power(t2,2) - 94*Power(t2,3)))) + 
         s1*(-245 + Power(t1,4)*(20 - 8*t2) - 81*t2 + 185*Power(t2,2) - 
            23*Power(t2,3) - 12*Power(t2,4) + 2*Power(s2,5)*(1 + t2) - 
            Power(s2,4)*(6 + 4*(6 + t1)*t2 + 5*Power(t2,2)) + 
            Power(t1,3)*(-119 - 62*t2 + 25*Power(t2,2)) - 
            Power(t1,2)*(61 - 209*t2 + 39*Power(t2,2) + 
               35*Power(t2,3)) + 
            t1*(405 - 62*t2 - 159*Power(t2,2) + 58*Power(t2,3) + 
               2*Power(t2,4)) + 
            Power(s2,3)*(10 + Power(t1,2)*(-4 + t2) + 101*t2 + 
               30*Power(t2,2) + 4*Power(t2,3) + 
               t1*(16 + 42*t2 + 11*Power(t2,2))) - 
            Power(s2,2)*(-59 + 141*t2 + 201*Power(t2,2) - 
               40*Power(t2,3) - 5*Power(t2,4) + 
               Power(t1,3)*(3 + 8*t2) - 
               Power(t1,2)*(13 + 25*t2 + 30*Power(t2,2)) + 
               t1*(103 + 124*t2 + 154*Power(t2,2) + 44*Power(t2,3))) + 
            s2*(-216 + 8*Power(t1,4) - 565*t2 - 42*Power(t2,2) + 
               243*Power(t2,3) - 42*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,3)*(-58 - 58*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(92 + 329*t2 + 130*Power(t2,2) - 
                  31*Power(t2,3)) + 
               t1*(190 + 268*t2 - 299*Power(t2,2) + 20*Power(t2,3) + 
                  37*Power(t2,4)))) + 
         Power(s,3)*(4 + 12*Power(s1,3)*s2 + 52*t1 - 14*Power(t1,2) + 
            15*Power(t1,3) + 13*t2 + 84*t1*t2 - 26*Power(t1,2)*t2 + 
            4*Power(t1,3)*t2 - 40*Power(t2,2) + 46*t1*Power(t2,2) - 
            23*Power(t2,3) - 12*t1*Power(t2,3) + 8*Power(t2,4) - 
            Power(s2,3)*(22 + t1 + 9*t2) + 
            Power(s2,2)*(58 + 10*Power(t1,2) + t1*(19 - 5*t2) + 46*t2 + 
               14*Power(t2,2)) + 
            Power(s1,2)*(2 + 56*Power(s2,2) + 32*Power(t1,2) + 
               t1*(80 - 36*t2) - 21*t2 + 8*Power(t2,2) - 
               s2*(65 + 64*t1 + 10*t2)) - 
            s2*(33 + 8*Power(t1,3) + 26*t2 - 28*Power(t2,2) + 
               24*Power(t2,3) + Power(t1,2)*(-13 + 5*t2) + 
               t1*(93 + 92*t2 - 25*Power(t2,2))) + 
            s1*(-25 + 32*Power(s2,3) - 8*Power(t1,3) + 26*t2 + 
               44*Power(t2,2) - 16*Power(t2,3) - 
               Power(t1,2)*(11 + 24*t2) - 
               Power(s2,2)*(109 + 41*t1 + 48*t2) + 
               2*t1*(-55 - 50*t2 + 24*Power(t2,2)) + 
               s2*(90 + 30*Power(t1,2) + 19*t2 + 22*Power(t2,2) + 
                  t1*(168 + 13*t2)))) + 
         Power(s,2)*(24 + 12*Power(s1,4)*(-1 + s2) - 88*t1 + 
            143*Power(t1,2) - 39*Power(t1,3) + 8*Power(t1,4) + 149*t2 - 
            131*t1*t2 + 66*Power(t1,2)*t2 - 2*Power(t1,3)*t2 + 
            123*Power(t2,2) + 46*t1*Power(t2,2) - 
            22*Power(t1,2)*Power(t2,2) + 2*Power(t1,3)*Power(t2,2) - 
            39*Power(t2,3) + 43*t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) - 19*Power(t2,4) - 
            2*t1*Power(t2,4) + 2*Power(t2,5) + Power(s2,4)*(12 + 7*t2) - 
            Power(s2,3)*(41 + 3*Power(t1,2) + 55*t2 + 8*Power(t2,2) + 
               t1*(8 + 5*t2)) - 
            Power(s1,3)*(10 + 36*Power(s2,2) + 28*Power(t1,2) + 
               t1*(68 - 20*t2) - 43*t2 + 2*Power(t2,2) + 
               s2*(-21 - 100*t1 + 48*t2)) + 
            Power(s2,2)*(35 + 6*Power(t1,3) + 
               2*Power(t1,2)*(-12 + t2) + 92*t2 - 2*Power(t2,2) + 
               20*Power(t2,3) + t1*(99 + 111*t2 - 13*Power(t2,2))) - 
            s2*(31 + 183*t2 + 40*Power(t2,2) - 84*Power(t2,3) + 
               16*Power(t2,4) + 4*Power(t1,3)*(1 + 4*t2) + 
               Power(t1,2)*(47 - 54*t2 - 29*Power(t2,2)) + 
               t1*(42 + 109*t2 + 177*Power(t2,2) + 5*Power(t2,3))) + 
            Power(s1,2)*(153 - 58*Power(s2,3) + 12*Power(t1,3) + 9*t2 - 
               69*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,2)*(33 + 36*t2) + 
               Power(s2,2)*(121 + 109*t1 + 48*t2) + 
               t1*(36 + 161*t2 - 42*Power(t2,2)) - 
               s2*(201 + 24*Power(t1,2) - 40*t2 - 44*Power(t2,2) + 
                  3*t1*(89 + 49*t2))) - 
            s1*(137 + 19*Power(s2,4) + 288*t2 - 40*Power(t2,2) - 
               57*Power(t2,3) + 6*Power(t2,4) + 
               3*Power(t1,3)*(9 + 4*t2) - 
               Power(s2,3)*(91 + 33*t1 + 54*t2) + 
               Power(t1,2)*(105 - 19*t2 + 6*Power(t2,2)) + 
               t1*(-127 + 90*t2 + 136*Power(t2,2) - 24*Power(t2,3)) + 
               Power(s2,2)*(145 + 20*Power(t1,2) + 105*t2 + 
                  32*Power(t2,2) + 2*t1*(96 + 37*t2)) - 
               s2*(221 + 24*Power(t1,3) + Power(t1,2)*(42 - 15*t2) + 
                  271*t2 - 145*Power(t2,2) + 8*Power(t2,3) + 
                  t1*(127 + 388*t2 + 52*Power(t2,2))))) + 
         s*(-142 + Power(s1,5)*(6 - 18*s2) + 199*t1 - 62*Power(t1,2) + 
            33*Power(t1,3) - 12*Power(t1,4) - 310*t2 + 230*t1*t2 + 
            98*Power(t1,2)*t2 - 42*Power(t1,3)*t2 + 8*Power(t1,4)*t2 - 
            56*Power(t2,2) - 134*t1*Power(t2,2) + 
            134*Power(t1,2)*Power(t2,2) - 23*Power(t1,3)*Power(t2,2) + 
            114*Power(t2,3) - 56*t1*Power(t2,3) + 
            12*Power(t1,2)*Power(t2,3) - 2*Power(t2,4) + 
            9*t1*Power(t2,4) - 4*Power(t2,5) - 2*Power(s2,5)*(1 + t2) + 
            Power(s2,4)*(8 + (23 + 3*t1)*t2 + 2*Power(t2,2)) + 
            Power(s1,4)*(26 - 4*Power(s2,2) + 12*Power(t1,2) + 
               t1*(11 - 4*t2) - 24*t2 + s2*(-15 - 71*t1 + 75*t2)) - 
            Power(s2,3)*(15 + 73*t2 + 30*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,2)*(-5 + 3*t2) + t1*(18 + 28*t2 - 8*Power(t2,2))) \
+ Power(s2,2)*(2 + 109*t2 + 86*Power(t2,2) - 41*Power(t2,3) + 
               10*Power(t2,4) + Power(t1,3)*(5 + 8*t2) - 
               2*Power(t1,2)*(15 + 31*t2 + 13*Power(t2,2)) + 
               t1*(79 + 175*t2 + 137*Power(t2,2) + 9*Power(t2,3))) - 
            s2*(-121 + 8*Power(t1,4) - 101*t2 + 211*Power(t2,2) + 
               88*Power(t2,3) - 68*Power(t2,4) + 3*Power(t2,5) + 
               Power(t1,3)*(-74 - 46*t2 + 8*Power(t2,2)) - 
               Power(t1,2)*(-105 - 241*t2 + Power(t2,2) + 
                  29*Power(t2,3)) + 
               4*t1*(33 + 10*t2 - 26*Power(t2,2) + 30*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s1,3)*(-59 + 28*Power(s2,3) - 8*Power(t1,3) - 74*t2 + 
               40*Power(t2,2) - Power(t1,2)*(41 + 24*t2) + 
               Power(s2,2)*(-53 - 103*t1 + 24*t2) + 
               6*t1*(11 - 9*t2 + 2*Power(t2,2)) + 
               s2*(242 + 2*Power(t1,2) - t2 - 114*Power(t2,2) + 
                  t1*(82 + 195*t2))) + 
            Power(s1,2)*(-258 + 22*Power(s2,4) + 210*t2 + 
               68*Power(t2,2) - 36*Power(t2,3) + 
               3*Power(t1,3)*(3 + 4*t2) - 
               Power(s2,3)*(79 + 53*t1 + 45*t2) + 
               4*Power(t1,2)*(30 + 16*t2 + 3*Power(t2,2)) - 
               4*t1*(-6 + 32*t2 - 21*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(228 + 10*Power(t1,2) + 65*t2 - 
                  26*Power(t2,2) + 3*t1*(71 + 57*t2)) - 
               s2*(184 + 24*Power(t1,3) + Power(t1,2)*(147 - 45*t2) + 
                  610*t2 - 115*Power(t2,2) - 72*Power(t2,3) + 
                  t1*(-103 + 244*t2 + 197*Power(t2,2)))) + 
            s1*(427 + 4*Power(s2,5) - 16*Power(t1,4) + 330*t2 - 
               265*Power(t2,2) - 18*Power(t2,3) + 18*Power(t2,4) - 
               Power(s2,4)*(27 + 9*t1 + 22*t2) + 
               Power(t1,3)*(10 + 24*t2 - 4*Power(t2,2)) - 
               Power(t1,2)*(27 + 232*t2 + 35*Power(t2,2)) + 
               2*t1*(-198 + 32*t2 + 59*Power(t2,2) - 25*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,3)*(84 + 8*Power(t1,2) + 107*t2 + 
                  24*Power(t2,2) + t1*(48 + 39*t2)) - 
               Power(s2,2)*(160 + 12*Power(t1,3) + 330*t2 - 
                  29*Power(t2,2) + 4*Power(t2,3) - 
                  Power(t1,2)*(13 + 16*t2) + 
                  t1*(139 + 322*t2 + 77*Power(t2,2))) + 
               s2*(-182 + 401*t2 + 456*Power(t2,2) - 167*Power(t2,3) - 
                  12*Power(t2,4) + 4*Power(t1,3)*(-5 + 8*t2) + 
                  Power(t1,2)*(252 + 110*t2 - 76*Power(t2,2)) + 
                  t1*(142 - 185*t2 + 282*Power(t2,2) + 93*Power(t2,3))))))*
       R2q(1 - s1 - t1 + t2))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       (-1 + s - s*s2 + s1*s2 + t1 - s2*t2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) - 
    (8*(-156 - 127*s2 + 23*Power(s2,2) + 4*Power(s1,8)*Power(s2,2) + 
         47*Power(s2,3) - 11*Power(s2,4) + 540*t1 + 381*s2*t1 - 
         119*Power(s2,2)*t1 - 111*Power(s2,3)*t1 + 29*Power(s2,4)*t1 - 
         666*Power(t1,2) - 395*s2*Power(t1,2) + 
         203*Power(s2,2)*Power(t1,2) + 75*Power(s2,3)*Power(t1,2) - 
         25*Power(s2,4)*Power(t1,2) + 346*Power(t1,3) + 
         145*s2*Power(t1,3) - 141*Power(s2,2)*Power(t1,3) - 
         5*Power(s2,3)*Power(t1,3) + 7*Power(s2,4)*Power(t1,3) - 
         82*Power(t1,4) + 6*s2*Power(t1,4) + 34*Power(s2,2)*Power(t1,4) - 
         6*Power(s2,3)*Power(t1,4) + 26*Power(t1,5) - 10*s2*Power(t1,5) - 
         8*Power(t1,6) + Power(s1,7)*
          (11*Power(s2,3) - Power(t1,2) + 
            s2*(-8 + Power(t1,2) - t1*(-10 + t2)) + 
            Power(s2,2)*(9 + 8*t1 - 27*t2)) - 119*t2 - 530*s2*t2 - 
         276*Power(s2,2)*t2 + 130*Power(s2,3)*t2 + 88*Power(s2,4)*t2 - 
         24*Power(s2,5)*t2 - Power(s2,6)*t2 + 216*t1*t2 + 1295*s2*t1*t2 + 
         513*Power(s2,2)*t1*t2 - 355*Power(s2,3)*t1*t2 - 
         116*Power(s2,4)*t1*t2 + 40*Power(s2,5)*t1*t2 + 
         Power(s2,6)*t1*t2 - 32*Power(t1,2)*t2 - 940*s2*Power(t1,2)*t2 - 
         209*Power(s2,2)*Power(t1,2)*t2 + 
         296*Power(s2,3)*Power(t1,2)*t2 + 13*Power(s2,4)*Power(t1,2)*t2 - 
         16*Power(s2,5)*Power(t1,2)*t2 - 87*Power(t1,3)*t2 + 
         147*s2*Power(t1,3)*t2 - 68*Power(s2,2)*Power(t1,3)*t2 - 
         67*Power(s2,3)*Power(t1,3)*t2 + 15*Power(s2,4)*Power(t1,3)*t2 + 
         8*Power(t1,4)*t2 + 24*s2*Power(t1,4)*t2 + 
         40*Power(s2,2)*Power(t1,4)*t2 - 4*Power(s2,3)*Power(t1,4)*t2 + 
         14*Power(t1,5)*t2 + 4*s2*Power(t1,5)*t2 + 87*Power(t2,2) - 
         187*s2*Power(t2,2) - 521*Power(s2,2)*Power(t2,2) - 
         157*Power(s2,3)*Power(t2,2) + 142*Power(s2,4)*Power(t2,2) + 
         38*Power(s2,5)*Power(t2,2) - 13*Power(s2,6)*Power(t2,2) - 
         Power(s2,7)*Power(t2,2) - 237*t1*Power(t2,2) - 
         20*s2*t1*Power(t2,2) + 695*Power(s2,2)*t1*Power(t2,2) + 
         100*Power(s2,3)*t1*Power(t2,2) - 
         193*Power(s2,4)*t1*Power(t2,2) - 7*Power(s2,5)*t1*Power(t2,2) + 
         10*Power(s2,6)*t1*Power(t2,2) + 163*Power(t1,2)*Power(t2,2) + 
         415*s2*Power(t1,2)*Power(t2,2) - 
         44*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         88*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         38*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         12*Power(s2,5)*Power(t1,2)*Power(t2,2) + 
         24*Power(t1,3)*Power(t2,2) - 187*s2*Power(t1,3)*Power(t2,2) - 
         151*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         54*Power(s2,3)*Power(t1,3)*Power(t2,2) + 
         8*Power(s2,4)*Power(t1,3)*Power(t2,2) - 
         48*Power(t1,4)*Power(t2,2) - 16*s2*Power(t1,4)*Power(t2,2) + 
         28*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         12*Power(t1,5)*Power(t2,2) + 8*Power(t2,3) + 
         272*s2*Power(t2,3) + 14*Power(s2,2)*Power(t2,3) - 
         175*Power(s2,3)*Power(t2,3) - 13*Power(s2,4)*Power(t2,3) + 
         42*Power(s2,5)*Power(t2,3) - 2*Power(s2,6)*Power(t2,3) - 
         Power(s2,7)*Power(t2,3) + 73*t1*Power(t2,3) - 
         435*s2*t1*Power(t2,3) - 389*Power(s2,2)*t1*Power(t2,3) - 
         10*Power(s2,3)*t1*Power(t2,3) - 27*Power(s2,4)*t1*Power(t2,3) - 
         6*Power(s2,5)*t1*Power(t2,3) + 3*Power(s2,6)*t1*Power(t2,3) - 
         163*Power(t1,2)*Power(t2,3) + 52*s2*Power(t1,2)*Power(t2,3) + 
         318*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         142*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         28*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         4*Power(s2,5)*Power(t1,2)*Power(t2,3) + 
         104*Power(t1,3)*Power(t2,3) + 123*s2*Power(t1,3)*Power(t2,3) - 
         16*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         36*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         26*Power(t1,4)*Power(t2,3) - 36*s2*Power(t1,4)*Power(t2,3) - 
         18*Power(t2,4) - 12*s2*Power(t2,4) + 
         214*Power(s2,2)*Power(t2,4) + 103*Power(s2,3)*Power(t2,4) - 
         12*Power(s2,4)*Power(t2,4) - Power(s2,5)*Power(t2,4) + 
         Power(s2,6)*Power(t2,4) + 28*t1*Power(t2,4) + 
         203*s2*t1*Power(t2,4) - 119*Power(s2,2)*t1*Power(t2,4) - 
         165*Power(s2,3)*t1*Power(t2,4) - 43*Power(s2,4)*t1*Power(t2,4) - 
         4*Power(s2,5)*t1*Power(t2,4) - 15*Power(t1,2)*Power(t2,4) - 
         212*s2*Power(t1,2)*Power(t2,4) - 
         102*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         24*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         12*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         6*Power(t1,3)*Power(t2,4) + 64*s2*Power(t1,3)*Power(t2,4) + 
         36*Power(s2,2)*Power(t1,3)*Power(t2,4) + 
         4*Power(t1,4)*Power(t2,4) + 13*Power(t2,5) - 50*s2*Power(t2,5) - 
         48*Power(s2,2)*Power(t2,5) + 42*Power(s2,3)*Power(t2,5) + 
         26*Power(s2,4)*Power(t2,5) + 2*Power(s2,5)*Power(t2,5) - 
         16*t1*Power(t2,5) + 43*s2*t1*Power(t2,5) + 
         135*Power(s2,2)*t1*Power(t2,5) + 29*Power(s2,3)*t1*Power(t2,5) - 
         6*Power(s2,4)*t1*Power(t2,5) + 7*Power(t1,2)*Power(t2,5) - 
         18*s2*Power(t1,2)*Power(t2,5) - 
         50*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
         12*Power(s2,3)*Power(t1,2)*Power(t2,5) - 
         5*Power(t1,3)*Power(t2,5) - 8*s2*Power(t1,3)*Power(t2,5) - 
         9*Power(t2,6) + 13*s2*Power(t2,6) - 25*Power(s2,2)*Power(t2,6) - 
         25*Power(s2,3)*Power(t2,6) - 2*Power(s2,4)*Power(t2,6) + 
         5*t1*Power(t2,6) - 8*s2*t1*Power(t2,6) + 
         13*Power(s2,2)*t1*Power(t2,6) + 12*Power(s2,3)*t1*Power(t2,6) + 
         2*Power(t1,2)*Power(t2,6) + 10*s2*Power(t1,2)*Power(t2,6) + 
         4*Power(s2,2)*Power(t1,2)*Power(t2,6) + 2*Power(t2,7) - 
         4*s2*Power(t2,7) + 2*Power(s2,2)*Power(t2,7) - 
         Power(s2,3)*Power(t2,7) - t1*Power(t2,7) - 3*s2*t1*Power(t2,7) - 
         5*Power(s2,2)*t1*Power(t2,7) + s2*Power(t2,8) + 
         Power(s2,2)*Power(t2,8) + 
         Power(s,7)*(-2 + 6*t1 + 3*Power(t1,2) + 
            Power(s2,2)*(-2 + t1 - t2) + 
            s1*(-1 + s2)*(-2 + 2*s2 + t1 - t2) - 6*t2 - 6*t1*t2 + 
            3*Power(t2,2) + s2*
             (4 - 7*t1 + Power(t1,2) + 7*t2 - 2*t1*t2 + Power(t2,2))) + 
         Power(s1,6)*(4 + 10*Power(s2,4) + 3*Power(t1,3) + 
            Power(s2,3)*(10 + 21*t1 - 63*t2) + t1*(-10 + 3*t2) + 
            Power(t1,2)*(1 + 4*t2) + 
            Power(s2,2)*(-77 + 8*Power(t1,2) + t1*(57 - 53*t2) - 
               40*t2 + 79*Power(t2,2)) + 
            s2*(-18 + Power(t1,3) - 6*Power(t1,2)*(-2 + t2) + 44*t2 + 
               Power(t2,2) + 3*t1*(4 - 21*t2 + 2*Power(t2,2)))) + 
         Power(s,6)*(6 - 3*t1 + 18*Power(t1,2) + 2*Power(t1,3) + 9*t2 - 
            4*t1*t2 + 10*Power(t1,2)*t2 - 14*Power(t2,2) - 
            26*t1*Power(t2,2) + 14*Power(t2,3) + 
            Power(s2,3)*(10 - 5*t1 + 6*t2) + 
            Power(s1,2)*(-4 - 9*Power(s2,2) + 5*t1 - 4*t2 + 
               s2*(13 - 6*t1 + 5*t2)) - 
            Power(s2,2)*(23 + 2*Power(t1,2) + 38*t2 + 7*Power(t2,2) - 
               t1*(29 + 9*t2)) + 
            s2*(7 + Power(t1,3) + Power(t1,2)*(-22 + t2) + 20*t2 + 
               7*Power(t2,2) + 3*Power(t2,3) + 
               t1*(-18 + 15*t2 - 5*Power(t2,2))) - 
            s1*(5 + 11*Power(s2,3) + 28*t1 + 21*Power(t1,2) + 
               Power(s2,2)*(-31 + 5*t1 - 15*t2) - 24*t2 - 31*t1*t2 + 
               10*Power(t2,2) + 
               s2*(15 - 31*t1 + 5*Power(t1,2) + 37*t2 - 13*t1*t2 + 
                  8*Power(t2,2)))) + 
         Power(s1,5)*(9 + 2*Power(s2,5) + Power(t1,3)*(7 - 13*t2) + 
            4*Power(s2,4)*(2 + 5*t1 - 12*t2) - 17*t2 - 2*Power(t2,2) + 
            t1*(-20 + 59*t2 - 14*Power(t2,2)) + 
            Power(t1,2)*(5 - 17*t2 - 5*Power(t2,2)) + 
            Power(s2,3)*(-152 + 14*Power(t1,2) + t1*(57 - 109*t2) - 
               13*t2 + 151*Power(t2,2)) + 
            Power(s2,2)*(-121 + 4*Power(t1,3) + 
               Power(t1,2)*(57 - 47*t2) + 407*t2 + 63*Power(t2,2) - 
               131*Power(t2,3) + t1*(-61 - 272*t2 + 150*Power(t2,2))) + 
            s2*(121 + Power(t1,3)*(30 - 4*t2) + 52*t2 - 96*Power(t2,2) - 
               6*Power(t2,3) + 
               Power(t1,2)*(61 - 81*t2 + 15*Power(t2,2)) - 
               t1*(189 + 17*t2 - 168*Power(t2,2) + 15*Power(t2,3)))) + 
         Power(s1,4)*(-55 - 2*Power(s2,6) + 24*Power(t1,4) + 
            2*Power(s2,5)*(5 + 4*t1 - 5*t2) - 12*t2 + 19*Power(t2,2) + 
            10*Power(t2,3) + Power(t1,3)*(-4 - 39*t2 + 22*Power(t2,2)) + 
            2*Power(t1,2)*(-46 + 5*t2 + 30*Power(t2,2)) + 
            Power(s2,4)*(-140 + 10*Power(t1,2) + t1*(23 - 74*t2) - 
               6*t2 + 90*Power(t2,2)) + 
            t1*(132 + 36*t2 - 131*Power(t2,2) + 25*Power(t2,3)) + 
            Power(s2,3)*(-57 + 4*Power(t1,3) + 
               Power(t1,2)*(38 - 72*t2) + 630*t2 - 73*Power(t2,2) - 
               195*Power(t2,3) + t1*(-87 - 163*t2 + 238*Power(t2,2))) + 
            Power(s2,2)*(445 + Power(t1,3)*(67 - 16*t2) + 380*t2 - 
               883*Power(t2,2) - 30*Power(t2,3) + 135*Power(t2,4) + 
               2*Power(t1,2)*(13 - 140*t2 + 56*Power(t2,2)) + 
               t1*(-586 + 449*t2 + 531*Power(t2,2) - 235*Power(t2,3))) + 
            s2*(212 + 6*Power(t1,4) - 550*t2 - 15*Power(t2,2) + 
               100*Power(t2,3) + 15*Power(t2,4) + 
               Power(t1,3)*(59 - 123*t2 + 6*Power(t2,2)) - 
               Power(t1,2)*(127 + 258*t2 - 214*Power(t2,2) + 
                  20*Power(t2,3)) + 
               t1*(-114 + 806*t2 - 60*Power(t2,2) - 245*Power(t2,3) + 
                  20*Power(t2,4)))) - 
         Power(s1,3)*(101 + Power(s2,7) - 206*t2 + 31*Power(t2,2) - 
            14*Power(t2,3) + 20*Power(t2,4) - 3*Power(s2,6)*(1 + t2) + 
            6*Power(t1,4)*(-5 + 11*t2) + 
            2*Power(t1,3)*(35 + 16*t2 - 40*Power(t2,2) + 
               9*Power(t2,3)) + 
            Power(t1,2)*(9 - 360*t2 + 67*Power(t2,2) + 90*Power(t2,3) - 
               5*Power(t2,4)) + 
            2*t1*(-77 + 244*t2 - 14*Power(t2,2) - 67*Power(t2,3) + 
               10*Power(t2,4)) + 
            Power(s2,5)*(68 - 5*Power(t1,2) + 30*t2 - 16*Power(t2,2) + 
               t1*(-17 + 15*t2)) + 
            Power(s2,4)*(-75 - 428*t2 + 56*Power(t2,2) + 
               80*Power(t2,3) + Power(t1,2)*(11 + 40*t2) + 
               t1*(44 + 24*t2 - 108*Power(t2,2))) + 
            Power(s2,3)*(-553 - 64*t2 + 1020*Power(t2,2) - 
               222*Power(t2,3) - 145*Power(t2,4) + 
               Power(t1,3)*(-53 + 16*t2) - 
               18*Power(t1,2)*(1 - 6*t2 + 8*Power(t2,2)) + 
               t1*(527 - 400*t2 - 118*Power(t2,2) + 282*Power(t2,3))) + 
            Power(s2,2)*(-218 - 8*Power(t1,4) + 1501*t2 + 
               366*Power(t2,2) - 1002*Power(t2,3) + 25*Power(t2,4) + 
               89*Power(t2,5) + 
               Power(t1,3)*(8 + 238*t2 - 24*Power(t2,2)) + 
               Power(t1,2)*(297 - 118*t2 - 548*Power(t2,2) + 
                  138*Power(t2,3)) - 
               2*t1*(70 + 869*t2 - 558*Power(t2,2) - 272*Power(t2,3) + 
                  110*Power(t2,4))) + 
            s2*(474 + 548*t2 - 974*Power(t2,2) + 100*Power(t2,3) + 
               40*Power(t2,4) + 20*Power(t2,5) + 
               Power(t1,4)*(-94 + 20*t2) + 
               Power(t1,3)*(86 + 262*t2 - 197*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(643 - 734*t2 - 426*Power(t2,2) + 
                  286*Power(t2,3) - 15*Power(t2,4)) + 
               t1*(-1131 + 55*t2 + 1327*Power(t2,2) - 170*Power(t2,3) - 
                  210*Power(t2,4) + 15*Power(t2,5)))) + 
         Power(s1,2)*(171 + Power(t1,5)*(42 - 4*t2) + 181*t2 - 
            265*Power(t2,2) + 75*Power(t2,3) - 46*Power(t2,4) + 
            20*Power(t2,5) + Power(s2,7)*(-t1 + t2) + 
            Power(t1,4)*(-80 - 86*t2 + 64*Power(t2,2)) + 
            Power(t1,3)*(-199 + 306*t2 + 82*Power(t2,2) - 
               76*Power(t2,3) + 7*Power(t2,4)) + 
            Power(t1,2)*(669 - 282*t2 - 459*Power(t2,2) + 
               91*Power(t2,3) + 65*Power(t2,4) - 4*Power(t2,5)) + 
            t1*(-602 - 127*t2 + 608*Power(t2,2) - 100*Power(t2,3) - 
               56*Power(t2,4) + 5*Power(t2,5)) + 
            Power(s2,6)*(-9 + 2*Power(t1,2) - 8*t2 + Power(t2,2) + 
               t1*(4 + 3*t2)) - 
            Power(s2,5)*(-41 + Power(t1,3) - 184*t2 - 29*Power(t2,2) + 
               8*Power(t2,3) + 2*Power(t1,2)*(3 + 5*t2) + 
               t1*(15 + 50*t2 - 2*Power(t2,2))) + 
            Power(s2,4)*(165 + Power(t1,3)*(14 - 4*t2) - 151*t2 - 
               448*Power(t2,2) + 124*Power(t2,3) + 30*Power(t2,4) + 
               Power(t1,2)*(29 + 74*t2 + 62*Power(t2,2)) - 
               t1*(213 - 29*t2 + 64*Power(t2,2) + 80*Power(t2,3))) + 
            Power(s2,3)*(-307 + 2*Power(t1,4) - 1267*t2 + 
               146*Power(t2,2) + 800*Power(t2,3) - 248*Power(t2,4) - 
               61*Power(t2,5) + 
               Power(t1,3)*(-53 - 137*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-45 + 110*t2 + 126*Power(t2,2) - 
                  140*Power(t2,3)) + 
               t1*(380 + 1021*t2 - 704*Power(t2,2) + 54*Power(t2,3) + 
                  193*Power(t2,4))) + 
            Power(s2,2)*(-850 + Power(t1,4)*(74 - 24*t2) - 433*t2 + 
               1881*Power(t2,2) + 28*Power(t2,3) - 623*Power(t2,4) + 
               36*Power(t2,5) + 37*Power(t2,6) + 
               Power(t1,3)*(-138 + 46*t2 + 311*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(t1,2)*(-515 + 829*t2 - 416*Power(t2,2) - 
                  534*Power(t2,3) + 92*Power(t2,4)) + 
               t1*(1436 - 613*t2 - 1837*Power(t2,2) + 
                  1264*Power(t2,3) + 311*Power(t2,4) - 123*Power(t2,5))) \
+ s2*(-297 + 4*Power(t1,5) + 1174*t2 + 448*Power(t2,2) - 
               832*Power(t2,3) + 140*Power(t2,4) - 12*Power(t2,5) + 
               15*Power(t2,6) + 
               Power(t1,4)*(-34 - 214*t2 + 22*Power(t2,2)) + 
               Power(t1,3)*(-283 + 354*t2 + 411*Power(t2,2) - 
                  153*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(510 + 1146*t2 - 1299*Power(t2,2) - 
                  340*Power(t2,3) + 204*Power(t2,4) - 6*Power(t2,5)) + 
               t1*(105 - 2528*t2 + 655*Power(t2,2) + 1035*Power(t2,3) - 
                  160*Power(t2,4) - 105*Power(t2,5) + 6*Power(t2,6)))) + 
         s1*(128 - 255*t2 - 88*Power(t2,2) + 132*Power(t2,3) - 
            54*Power(t2,4) + 35*Power(t2,5) - 10*Power(t2,6) + 
            Power(s2,7)*t2*(1 + t1 + t2) + 
            2*Power(t1,5)*(-6 - 23*t2 + 2*Power(t2,2)) + 
            Power(t1,4)*(-88 + 114*t2 + 82*Power(t2,2) - 
               26*Power(t2,3)) - 
            Power(t1,3)*(-297 - 167*t2 + 340*Power(t2,2) + 
               52*Power(t2,3) - 33*Power(t2,4) + Power(t2,5)) + 
            Power(t1,2)*(-147 - 803*t2 + 454*Power(t2,2) + 
               206*Power(t2,3) - 46*Power(t2,4) - 21*Power(t2,5) + 
               Power(t2,6)) + 
            t1*(-178 + 821*t2 - 100*Power(t2,2) - 280*Power(t2,3) + 
               72*Power(t2,4) - Power(t2,5) + 2*Power(t2,6)) - 
            Power(s2,6)*(Power(t1,2)*(1 + t2) + 
               t2*(-23 - 7*t2 + 3*Power(t2,2)) + 
               t1*(-1 + 16*t2 + 6*Power(t2,2))) + 
            Power(s2,5)*(21 + Power(t1,3) - 80*t2 - 158*Power(t2,2) - 
               8*Power(t2,3) - 2*Power(t2,4) + 
               Power(t1,2)*(11 + 19*t2 + 9*Power(t2,2)) + 
               t1*(-33 + 23*t2 + 39*Power(t2,2) + 9*Power(t2,3))) + 
            Power(s2,4)*(-91 - 317*t2 + 89*Power(t2,2) + 
               172*Power(t2,3) - 96*Power(t2,4) + 
               Power(t1,3)*(-11 - 13*t2 + 4*Power(t2,2)) - 
               Power(t1,2)*(24 + 95*t2 + 91*Power(t2,2) + 
                  44*Power(t2,3)) + 
               t1*(126 + 435*t2 + 42*Power(t2,2) + 108*Power(t2,3) + 
                  32*Power(t2,4))) - 
            Power(s2,3)*(118 - 457*t2 - 889*Power(t2,2) + 
               256*Power(t2,3) + 300*Power(t2,4) - 127*Power(t2,5) - 
               13*Power(t2,6) + Power(t1,4)*(-4 + 8*t2) + 
               Power(t1,3)*(-51 - 136*t2 - 120*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(252 + 90*t2 + 270*Power(t2,2) + 
                  80*Power(t2,3) - 66*Power(t2,4)) + 
               t1*(-315 + 449*t2 + 484*Power(t2,2) - 556*Power(t2,3) + 
                  95*Power(t2,4) + 73*Power(t2,5))) + 
            Power(s2,2)*(349 + 4*Power(t1,5) + 1379*t2 + 
               201*Power(t2,2) - 1039*Power(t2,3) + 127*Power(t2,4) + 
               199*Power(t2,5) - 15*Power(t2,6) - 9*Power(t2,7) + 
               2*Power(t1,4)*(-25 - 47*t2 + 8*Power(t2,2)) + 
               2*Power(t1,3)*
                (5 + 128*t2 - 11*Power(t2,2) - 88*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(412 + 609*t2 - 850*Power(t2,2) + 
                  374*Power(t2,3) + 259*Power(t2,4) - 31*Power(t2,5)) + 
               t1*(-725 - 2164*t2 + 862*Power(t2,2) + 804*Power(t2,3) - 
                  671*Power(t2,4) - 96*Power(t2,5) + 38*Power(t2,6))) - 
            s2*(-591 - 494*t2 + 972*Power(t2,2) + 100*Power(t2,3) - 
               337*Power(t2,4) + 72*Power(t2,5) - 16*Power(t2,6) + 
               6*Power(t2,7) + 2*Power(t1,5)*(-9 + 4*t2) + 
               2*Power(t1,4)*
                (45 - 41*t2 - 78*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(136 - 386*t2 + 391*Power(t2,2) + 
                  272*Power(t2,3) - 57*Power(t2,4)) - 
               Power(t1,2)*(1089 - 827*t2 - 555*Power(t2,2) + 
                  904*Power(t2,3) + 129*Power(t2,4) - 73*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(1472 + 137*t2 - 1832*Power(t2,2) + 689*Power(t2,3) + 
                  368*Power(t2,4) - 63*Power(t2,5) - 28*Power(t2,6) + 
                  Power(t2,7)))) + 
         Power(s,5)*(2 - 8*t1 + 5*Power(t1,2) + 34*Power(t1,3) + 
            5*Power(s2,4)*(-4 + 2*t1 - 3*t2) + 36*t2 + 40*t1*t2 - 
            38*Power(t1,2)*t2 + 9*Power(t1,3)*t2 + 21*Power(t2,2) + 
            48*t1*Power(t2,2) + 8*Power(t1,2)*Power(t2,2) - 
            44*Power(t2,3) - 43*t1*Power(t2,3) + 26*Power(t2,4) + 
            Power(s1,3)*(-4 + 11*Power(s2,2) - 10*t1 + 
               s2*(-4 + 15*t1 - 10*t2) + 6*t2) + 
            Power(s2,3)*(47 - 2*Power(t1,2) + 83*t2 + 20*Power(t2,2) - 
               t1*(43 + 14*t2)) - 
            Power(s2,2)*(2 + 4*Power(t1,3) + 8*t2 + 48*Power(t2,2) + 
               16*Power(t2,3) - 5*Power(t1,2)*(15 + t2) + 
               t1*(26 + 41*t2 - 15*Power(t2,2))) + 
            s2*(-30 - 114*t2 - 38*Power(t2,2) - 27*Power(t2,3) + 
               Power(t2,4) + Power(t1,3)*(-6 + 4*t2) - 
               Power(t1,2)*(112 + 62*t2 + 7*Power(t2,2)) + 
               t1*(80 + 98*t2 + 95*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,2)*(36 + 43*Power(s2,3) + 55*t1 + 61*Power(t1,2) + 
               2*Power(s2,2)*(-31 + t1 - 19*t2) - 18*t2 - 67*t1*t2 + 
               14*Power(t2,2) + 
               s2*(-26 + 9*Power(t1,2) + 38*t2 + 21*Power(t2,2) - 
                  2*t1*(23 + 17*t2))) + 
            s1*(-25 + 25*Power(s2,4) - 13*Power(t1,3) + 
               Power(s2,3)*(-86 + 11*t1 - 59*t2) - 94*t2 + 
               66*Power(t2,2) - 46*Power(t2,3) - 
               Power(t1,2)*(75 + 61*t2) + 
               t1*(8 - 49*t2 + 120*Power(t2,2)) + 
               Power(s2,2)*(39 + 2*Power(t1,2) + 133*t2 + 
                  43*Power(t2,2) - t1*(57 + 23*t2)) + 
               s2*(56 - 6*Power(t1,3) + 60*t2 - 7*Power(t2,2) - 
                  12*Power(t2,3) + Power(t1,2)*(129 + t2) + 
                  t1*(9 - 94*t2 + 17*Power(t2,2))))) + 
         Power(s,4)*(-29 + 100*t1 - 100*Power(t1,2) + 44*Power(t1,3) + 
            16*Power(t1,4) - 156*t2 + 198*t1*t2 + 14*Power(t1,2)*t2 + 
            29*Power(t1,3)*t2 - 24*Power(t2,2) + 63*t1*Power(t2,2) - 
            96*Power(t1,2)*Power(t2,2) + 16*Power(t1,3)*Power(t2,2) + 
            49*Power(t2,3) + 141*t1*Power(t2,3) - 
            8*Power(t1,2)*Power(t2,3) - 90*Power(t2,4) - 
            32*t1*Power(t2,4) + 24*Power(t2,5) - 
            10*Power(s2,5)*(t1 - 2*(1 + t2)) + 
            2*Power(s1,4)*(8 + 5*Power(s2,2) + 5*t1 - 2*t2 + 
               s2*(-17 - 10*t1 + 5*t2)) + 
            Power(s2,3)*(-20 + 4*Power(t1,3) - 34*t2 + 75*Power(t2,2) + 
               31*Power(t2,3) - Power(t1,2)*(101 + 16*t2) + 
               t1*(104 + 78*t2 - 14*Power(t2,2))) + 
            Power(s2,4)*(8*Power(t1,2) + 6*t1*(4 + t2) - 
               2*(21 + 46*t2 + 15*Power(t2,2))) + 
            Power(s2,2)*(10 + Power(t1,3)*(27 - 16*t2) + 159*t2 + 
               207*Power(t2,2) + 56*Power(t2,3) - 13*Power(t2,4) + 
               2*Power(t1,2)*(61 + 87*t2 + 22*Power(t2,2)) - 
               t1*(112 + 215*t2 + 262*Power(t2,2) + 15*Power(t2,3))) + 
            s2*(77 + 6*Power(t1,4) + 206*t2 - 74*Power(t2,2) - 
               100*Power(t2,3) - 50*Power(t2,4) - 6*Power(t2,5) + 
               Power(t1,3)*(-157 - 37*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(205 + 44*t2 - 39*Power(t2,2) - 
                  18*Power(t2,3)) + 
               t1*(-196 - 382*t2 + 63*Power(t2,2) + 120*Power(t2,3) + 
                  18*Power(t2,4))) - 
            Power(s1,3)*(51*Power(s2,3) + 95*Power(t1,2) + 
               t1*(68 - 78*t2) + Power(s2,2)*(11 - 30*t1 + 5*t2) + 
               6*(5 + 5*t2 + 2*Power(t2,2)) + 
               s2*(5*Power(t1,2) - 3*t1*(8 + 15*t2) + 
                  4*(-25 - 21*t2 + 6*Power(t2,2)))) + 
            Power(s1,2)*(-82*Power(s2,4) + 35*Power(t1,3) + 
               Power(s2,3)*(125 + 27*t1 + 117*t2) + 
               Power(t1,2)*(97 + 148*t2) + 
               t1*(18 + 223*t2 - 218*Power(t2,2)) + 
               2*(-51 + 94*t2 - 39*Power(t2,2) + 30*Power(t2,3)) + 
               Power(s2,2)*(39 + 20*Power(t1,2) + 22*t2 - 
                  33*Power(t2,2) - t1*(25 + 51*t2)) + 
               s2*(15*Power(t1,3) - 4*Power(t1,2)*(71 + 5*t2) + 
                  t1*(-16 + 221*t2 - 12*Power(t2,2)) + 
                  4*(11 - 94*t2 - 29*Power(t2,2) + 3*Power(t2,3)))) + 
            s1*(127 - 30*Power(s2,5) - 
               2*Power(s2,4)*(-58 + 7*t1 - 53*t2) + 141*t2 - 
               207*Power(t2,2) + 182*Power(t2,3) - 68*Power(t2,4) - 
               Power(t1,3)*(153 + 49*t2) + 
               Power(s2,3)*(-48 + 22*Power(t1,2) + t1*(12 - 9*t2) - 
                  205*t2 - 97*Power(t2,2)) + 
               Power(t1,2)*(27 + 107*t2 - 45*Power(t2,2)) + 
               t1*(-136 - 187*t2 - 296*Power(t2,2) + 162*Power(t2,3)) + 
               Power(s2,2)*(-14 + 20*Power(t1,3) - 263*t2 - 
                  67*Power(t2,2) + 41*Power(t2,3) - 
                  Power(t1,2)*(297 + 67*t2) + 
                  t1*(103 + 346*t2 + 36*Power(t2,2))) + 
               s2*(-191 + Power(t1,3)*(54 - 20*t2) + 112*t2 + 
                  376*Power(t2,2) + 116*Power(t2,3) + 8*Power(t2,4) + 
                  Power(t1,2)*(277 + 285*t2 + 43*Power(t2,2)) - 
                  t1*(-145 + 143*t2 + 365*Power(t2,2) + 31*Power(t2,3))))\
) - Power(s,3)*(-183 + 484*t1 - 463*Power(t1,2) + 212*Power(t1,3) - 
            70*Power(t1,4) - 5*Power(s2,6)*(-2 + t1 - 3*t2) - 470*t2 + 
            760*t1*t2 - 382*Power(t1,2)*t2 + 60*Power(t1,3)*t2 - 
            26*Power(t1,4)*t2 - 53*Power(t2,2) - 10*t1*Power(t2,2) - 
            157*Power(t1,2)*Power(t2,2) + 18*Power(t1,3)*Power(t2,2) + 
            44*Power(t2,3) + 82*t1*Power(t2,3) + 
            34*Power(t1,2)*Power(t2,3) - 14*Power(t1,3)*Power(t2,3) - 
            125*Power(t2,4) - 108*t1*Power(t2,4) + 
            17*Power(t1,2)*Power(t2,4) + 82*Power(t2,5) + 
            8*t1*Power(t2,5) - 11*Power(t2,6) + 
            Power(s1,5)*(14 + 40*Power(s2,2) + 5*t1 - t2 + 
               s2*(-56 - 15*t1 + 5*t2)) + 
            Power(s2,5)*(-14 + 7*Power(t1,2) - 53*t2 - 25*Power(t2,2) - 
               t1*(1 + 6*t2)) + 
            Power(s2,4)*(-32 - 44*t2 + 47*Power(t2,2) + 
               26*Power(t2,3) - 12*Power(t1,2)*(4 + t2) + 
               t1*(90 + 66*t2)) - 
            Power(s2,3)*(34 + 34*t2 - 200*Power(t2,2) - 
               80*Power(t2,3) + 28*Power(t2,4) + 
               Power(t1,3)*(-31 + 16*t2) + 
               2*t1*t2*(40 + 145*t2 + 8*Power(t2,2)) - 
               3*Power(t1,2)*(-3 + 57*t2 + 20*Power(t2,2))) + 
            Power(s2,2)*(33 + 8*Power(t1,4) + 541*t2 + 
               300*Power(t2,2) - 274*Power(t2,3) - 151*Power(t2,4) - 
               3*Power(t2,5) + 
               Power(t1,3)*(-200 - 98*t2 + 24*Power(t2,2)) + 
               Power(t1,2)*(339 + 224*t2 - 127*Power(t2,2) - 
                  86*Power(t2,3)) + 
               t1*(-199 - 832*t2 - 86*Power(t2,2) + 358*Power(t2,3) + 
                  65*Power(t2,4))) + 
            s2*(267 + Power(t1,4)*(54 - 20*t2) + 138*t2 - 
               585*Power(t2,2) - 226*Power(t2,3) + 77*Power(t2,4) + 
               20*Power(t2,5) + 9*Power(t2,6) + 
               Power(t1,3)*(-28 + 212*t2 + 91*Power(t2,2) - 
                  4*Power(t2,3)) + 
               Power(t1,2)*(215 - 242*t2 - 309*Power(t2,2) - 
                  57*Power(t2,3) + 17*Power(t2,4)) - 
               2*t1*(232 - 64*t2 - 510*Power(t2,2) - 73*Power(t2,3) + 
                  17*Power(t2,4) + 11*Power(t2,5))) + 
            Power(s1,4)*(26 + 6*Power(s2,3) - 85*Power(t1,2) + 
               Power(s2,2)*(-116 + 75*t1 - 135*t2) - 48*t2 - 
               7*Power(t2,2) + t1*(-64 + 52*t2) + 
               s2*(46 + 5*Power(t1,2) + 215*t2 - 11*Power(t2,2) + 
                  t1*(11 + 30*t2))) + 
            Power(s1,3)*(-86*Power(s2,4) + 
               Power(s2,3)*(-13 + 118*t1 - 6*t2) + 
               2*Power(s2,2)*
                (-54 + 30*Power(t1,2) + 229*t2 + 84*Power(t2,2) - 
                  t1*(35 + 127*t2)) + 
               2*(-104 + 25*Power(t1,3) + 25*t2 - 11*Power(t2,2) + 
                  19*Power(t2,3) + Power(t1,2)*(11 + 91*t2) + 
                  t1*(14 + 183*t2 - 97*Power(t2,2))) + 
               s2*(502 + 20*Power(t1,3) - 392*t2 - 329*Power(t2,2) - 
                  6*Power(t2,3) - 2*Power(t1,2)*(143 + 25*t2) + 
                  2*t1*(-95 + 93*t2 + 11*Power(t2,2)))) + 
            Power(s1,2)*(-104 - 78*Power(s2,5) + 596*t2 - 
               303*Power(t2,2) + 214*Power(t2,3) - 62*Power(t2,4) - 
               2*Power(t1,3)*(124 + 53*t2) + 
               Power(s2,4)*(127 + 38*t1 + 170*t2) + 
               Power(t1,2)*(92 + 164*t2 - 92*Power(t2,2)) + 
               2*t1*(9 - 142*t2 - 324*Power(t2,2) + 110*Power(t2,3)) + 
               Power(s2,3)*(-57 + 68*Power(t1,2) + 101*t2 - 
                  34*Power(t2,2) - t1*(147 + 220*t2)) + 
               Power(s2,2)*(585 + 40*Power(t1,3) - 138*t2 - 
                  719*Power(t2,2) - 94*Power(t2,3) - 
                  2*Power(t1,2)*(192 + 109*t2) + 
                  4*t1*(-67 + 160*t2 + 87*Power(t2,2))) + 
               s2*(-478 + Power(t1,3)*(156 - 40*t2) - 1128*t2 + 
                  723*Power(t2,2) + 257*Power(t2,3) + 34*Power(t2,4) + 
                  6*Power(t1,2)*(22 + 67*t2 + 17*Power(t2,2)) + 
                  t1*(626 + 476*t2 - 439*Power(t2,2) - 96*Power(t2,3)))) \
+ s1*(442 - 20*Power(s2,6) + 72*Power(t1,4) + 103*t2 - 432*Power(t2,2) + 
               352*Power(t2,3) - 240*Power(t2,4) + 43*Power(t2,5) + 
               Power(s2,5)*(82 - 11*t1 + 99*t2) + 
               2*Power(t1,3)*(30 + 72*t2 + 35*Power(t2,2)) - 
               2*Power(t1,2)*
                (-62 - 61*t2 + 110*Power(t2,2) + 11*Power(t2,3)) + 
               t1*(-613 + 68*t2 + 174*Power(t2,2) + 454*Power(t2,3) - 
                  91*Power(t2,4)) + 
               Power(s2,4)*(-26 + 34*Power(t1,2) - 161*t2 - 
                  110*Power(t2,2) - t1*(37 + 42*t2)) + 
               Power(s2,2)*(-386 + Power(t1,3)*(148 - 64*t2) - 852*t2 + 
                  520*Power(t2,2) + 528*Power(t2,3) + 24*Power(t2,4) + 
                  Power(t1,2)*(62 + 464*t2 + 244*Power(t2,2)) + 
                  t1*(386 + 342*t2 - 928*Power(t2,2) - 234*Power(t2,3))) \
+ Power(s2,3)*(16*Power(t1,3) - 15*Power(t1,2)*(19 + 8*t2) + 
                  2*t1*(49 + 226*t2 + 59*Power(t2,2)) + 
                  2*(69 - 81*t2 - 84*Power(t2,2) + 31*Power(t2,3))) + 
               s2*(-319 + 24*Power(t1,4) + 1133*t2 + 852*Power(t2,2) - 
                  454*Power(t2,3) - 107*Power(t2,4) - 31*Power(t2,5) + 
                  Power(t1,3)*(-508 - 234*t2 + 24*Power(t2,2)) + 
                  Power(t1,2)*
                   (412 + 344*t2 - 59*Power(t2,2) - 74*Power(t2,3)) + 
                  t1*(107 - 1872*t2 - 432*Power(t2,2) + 
                     276*Power(t2,3) + 81*Power(t2,4))))) + 
         Power(s,2)*(-461 + 1177*t1 - 1134*Power(t1,2) + 
            526*Power(t1,3) - 128*Power(t1,4) + 26*Power(t1,5) - 
            738*t2 + 1200*t1*t2 - 626*Power(t1,2)*t2 + 
            110*Power(t1,3)*t2 + 32*Power(t1,4)*t2 - 4*Power(t1,5)*t2 + 
            42*Power(t2,2) - 358*t1*Power(t2,2) + 
            259*Power(t1,2)*Power(t2,2) - 58*Power(t1,3)*Power(t2,2) + 
            28*Power(t1,4)*Power(t2,2) + 82*Power(t2,3) + 
            10*t1*Power(t2,3) + 87*Power(t1,2)*Power(t2,3) - 
            32*Power(t1,3)*Power(t2,3) - 97*Power(t2,4) - 
            55*t1*Power(t2,4) + 20*Power(t1,2)*Power(t2,4) + 
            6*Power(t1,3)*Power(t2,4) + 106*Power(t2,5) + 
            20*t1*Power(t2,5) - 10*Power(t1,2)*Power(t2,5) - 
            32*Power(t2,6) + 2*t1*Power(t2,6) + 2*Power(t2,7) + 
            Power(s2,7)*(2 - t1 + 6*t2) + 
            Power(s1,6)*(4 + 43*Power(s2,2) + t1 + 
               s2*(-35 - 6*t1 + t2)) + 
            Power(s2,6)*(1 + 2*Power(t1,2) - 14*t2 - 11*Power(t2,2) - 
               t1*(5 + 7*t2)) - 
            Power(s2,5)*(19 + Power(t1,3) + 18*t2 - 7*Power(t2,2) - 
               7*Power(t2,3) + Power(t1,2)*(1 + t2) - 
               t1*(26 + 23*t2 + 7*Power(t2,2))) + 
            Power(s2,4)*(-16 + Power(t1,3)*(7 - 4*t2) - 147*t2 - 
               4*Power(t2,2) + 40*Power(t2,3) - 20*Power(t2,4) + 
               Power(t1,2)*(-42 + 38*t2 + 24*Power(t2,2)) + 
               t1*(37 + 94*t2 - 72*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,3)*(-57 + 2*Power(t1,4) + 330*t2 + 
               520*Power(t2,2) - 126*Power(t2,3) - 181*Power(t2,4) + 
               12*Power(t2,5) + 
               Power(t1,3)*(-33 - 51*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(60 + 48*t2 - 168*Power(t2,2) - 
                  92*Power(t2,3)) + 
               t1*(39 - 344*t2 - 172*Power(t2,2) + 360*Power(t2,3) + 
                  55*Power(t2,4))) + 
            Power(s2,2)*(245 + 848*t2 - 159*Power(t2,2) - 
               668*Power(t2,3) + 65*Power(t2,4) + 114*Power(t2,5) + 
               11*Power(t2,6) - 6*Power(t1,4)*(-5 + 4*t2) + 
               Power(t1,3)*(-152 + 298*t2 + 183*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(t1,2)*(423 + 227*t2 - 670*Power(t2,2) - 
                  66*Power(t2,3) + 74*Power(t2,4)) - 
               t1*(536 + 1231*t2 - 870*Power(t2,2) - 606*Power(t2,3) + 
                  194*Power(t2,4) + 69*Power(t2,5))) + 
            s2*(285 + 4*Power(t1,5) - 648*t2 - 1246*Power(t2,2) + 
               125*Power(t2,3) + 295*Power(t2,4) - 44*Power(t2,5) + 
               10*Power(t2,6) - 5*Power(t2,7) + 
               2*Power(t1,4)*(-38 - 59*t2 + 11*Power(t2,2)) + 
               Power(t1,3)*(129 + 284*t2 - 33*Power(t2,2) - 
                  103*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(116 - 1235*t2 - 258*Power(t2,2) + 
                  256*Power(t2,3) + 103*Power(t2,4) - 7*Power(t2,5)) + 
               t1*(-475 + 1672*t2 + 1347*Power(t2,2) - 
                  756*Power(t2,3) - 171*Power(t2,4) - 32*Power(t2,5) + 
                  11*Power(t2,6))) + 
            Power(s1,5)*(35 + 59*Power(s2,3) - 43*Power(t1,2) + 
               Power(s2,2)*(-85 + 79*t1 - 199*t2) - 18*t2 - 
               2*Power(t2,2) + t1*(-40 + 19*t2) + 
               s2*(-61 + 9*Power(t1,2) + 169*t2 + t1*(31 + 7*t2))) - 
            Power(s1,4)*(44 + 16*Power(s2,4) - 40*Power(t1,3) + 
               Power(t1,2)*(36 - 118*t2) + 87*t2 - 10*Power(t2,3) + 
               Power(s2,3)*(97 - 157*t1 + 220*t2) + 
               t1*(41 - 282*t2 + 84*Power(t2,2)) - 
               Power(s2,2)*(-439 + 70*Power(t1,2) + t1*(75 - 361*t2) + 
                  468*t2 + 377*Power(t2,2)) + 
               s2*(-529 - 15*Power(t1,3) - 64*t2 + 316*Power(t2,2) + 
                  15*Power(t2,3) + Power(t1,2)*(114 + 55*t2) + 
                  t1*(266 + 53*t2 - 43*Power(t2,2)))) + 
            Power(s1,3)*(-438 - 64*Power(s2,5) + 346*t2 - 
               55*Power(t2,2) + 100*Power(t2,3) - 20*Power(t2,4) + 
               2*Power(s2,4)*(8 + 49*t1 + 17*t2) - 
               2*Power(t1,3)*(83 + 57*t2) + 
               Power(t1,2)*(24 + 142*t2 - 86*Power(t2,2)) + 
               2*t1*(198 + 5*t2 - 313*Power(t2,2) + 63*Power(t2,3)) + 
               Power(s2,3)*(-418 + 92*Power(t1,2) + 466*t2 + 
                  294*Power(t2,2) - t1*(101 + 466*t2)) + 
               Power(s2,2)*(909 + 40*Power(t1,3) + 1118*t2 - 
                  1008*Power(t2,2) - 378*Power(t2,3) - 
                  2*Power(t1,2)*(60 + 151*t2) + 
                  2*t1*(-399 + 67*t2 + 339*Power(t2,2))) + 
               s2*(400 + Power(t1,3)*(204 - 40*t2) - 1912*t2 + 
                  218*Power(t2,2) + 274*Power(t2,3) + 40*Power(t2,4) + 
                  2*Power(t1,2)*(-29 + 58*t2 + 59*Power(t2,2)) + 
                  t1*(122 + 1092*t2 + 5*Power(t2,2) - 122*Power(t2,3)))) \
+ Power(s1,2)*(166 - 37*Power(s2,6) + 120*Power(t1,4) + 982*t2 - 
               657*Power(t2,2) + 337*Power(t2,3) - 180*Power(t2,4) + 
               20*Power(t2,5) + Power(s2,5)*(62 + 16*t1 + 117*t2) + 
               2*Power(t1,3)*(-56 + 81*t2 + 57*Power(t2,2)) - 
               4*Power(t1,2)*
                (-78 - 84*t2 + 39*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(-623 - 956*t2 + 48*Power(t2,2) + 586*Power(t2,3) - 
                  79*Power(t2,4)) + 
               2*Power(s2,4)*
                (-60 + 27*Power(t1,2) + 14*t2 - 20*Power(t2,2) - 
                  t1*(43 + 88*t2)) + 
               Power(s2,3)*(806 + 24*Power(t1,3) + 706*t2 - 
                  822*Power(t2,2) - 152*Power(t2,3) - 
                  Power(t1,2)*(229 + 264*t2) + 
                  t1*(-391 + 565*t2 + 516*Power(t2,2))) + 
               Power(s2,2)*(298 + Power(t1,3)*(282 - 96*t2) - 
                  2459*t2 - 854*Power(t2,2) + 1066*Power(t2,3) + 
                  217*Power(t2,4) + 
                  2*Power(t1,2)*(-140 + 63*t2 + 234*Power(t2,2)) + 
                  t1*(28 + 2174*t2 - 687*Power(t2,2) - 658*Power(t2,3))\
) + s2*(-1656 + 36*Power(t1,4) - 527*t2 + 2532*Power(t2,2) - 
                  428*Power(t2,3) - 91*Power(t2,4) - 45*Power(t2,5) + 
                  6*Power(t1,3)*(-81 - 80*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (-98 + 570*t2 + 213*Power(t2,2) - 114*Power(t2,3)) + 
                  t1*(2034 - 1366*t2 - 1557*Power(t2,2) - 
                     7*Power(t2,3) + 128*Power(t2,4)))) + 
            s1*(-7*Power(s2,7) + Power(s2,6)*(29 - 5*t1 + 47*t2) - 
               2*Power(t1,4)*(87 + 59*t2) + 
               Power(t1,3)*(210 + 212*t2 + 36*Power(t2,2) - 
                  46*Power(t2,3)) + 
               Power(t1,2)*(297 - 772*t2 - 447*Power(t2,2) + 
                  30*Power(t2,3) + 41*Power(t2,4)) + 
               t1*(-1052 + 1167*t2 + 550*Power(t2,2) + 38*Power(t2,3) - 
                  222*Power(t2,4) + 15*Power(t2,5)) - 
               2*(-369 + 132*t2 + 313*Power(t2,2) - 226*Power(t2,3) + 
                  168*Power(t2,4) - 63*Power(t2,5) + 5*Power(t2,6)) + 
               Power(s2,5)*(-1 + 19*Power(t1,2) - 59*t2 - 
                  60*Power(t2,2) - t1*(31 + 29*t2)) + 
               Power(s2,4)*(137 + 124*t2 - 84*Power(t2,2) + 
                  42*Power(t2,3) - Power(t1,2)*(99 + 64*t2) + 
                  t1*t2*(139 + 74*t2)) + 
               Power(s2,3)*(-215 + Power(t1,3)*(115 - 48*t2) - 
                  1354*t2 - 162*Power(t2,2) + 634*Power(t2,3) + 
                  7*Power(t2,4) + 
                  8*Power(t1,2)*(-7 + 47*t2 + 33*Power(t2,2)) + 
                  t1*(162 + 616*t2 - 824*Power(t2,2) - 262*Power(t2,3))) \
+ Power(s2,2)*(-977 + 24*Power(t1,4) - 97*t2 + 2218*Power(t2,2) + 
                  110*Power(t2,3) - 555*Power(t2,4) - 71*Power(t2,5) + 
                  Power(t1,3)*(-504 - 434*t2 + 72*Power(t2,2)) + 
                  Power(t1,2)*
                   (13 + 954*t2 + 60*Power(t2,2) - 310*Power(t2,3)) + 
                  t1*(1311 - 970*t2 - 1982*Power(t2,2) + 
                     672*Power(t2,3) + 331*Power(t2,4))) + 
               s2*(499 + Power(t1,4)*(202 - 60*t2) + 2907*t2 + 
                  2*Power(t2,2) - 1444*Power(t2,3) + 251*Power(t2,4) - 
                  11*Power(t2,5) + 24*Power(t2,6) + 
                  Power(t1,3)*
                   (-226 + 418*t2 + 379*Power(t2,2) - 12*Power(t2,3)) + 
                  Power(t1,2)*
                   (923 + 512*t2 - 768*Power(t2,2) - 318*Power(t2,3) + 
                     49*Power(t2,4)) + 
                  t1*(-1332 - 3443*t2 + 2000*Power(t2,2) + 
                     902*Power(t2,3) + 56*Power(t2,4) - 61*Power(t2,5))))\
) + s*(457 - 1328*t1 + 1387*Power(t1,2) - 668*Power(t1,3) + 
            180*Power(t1,4) - 28*Power(t1,5) + 
            Power(s1,7)*s2*(8 - 21*s2 + t1) + 504*t2 - Power(s2,8)*t2 - 
            878*t1*t2 + 356*Power(t1,2)*t2 + 17*Power(t1,3)*t2 - 
            32*Power(t1,4)*t2 + 30*Power(t1,5)*t2 - 168*Power(t2,2) + 
            518*t1*Power(t2,2) - 425*Power(t1,2)*Power(t2,2) + 
            110*Power(t1,3)*Power(t2,2) - 24*Power(t1,4)*Power(t2,2) - 
            4*Power(t1,5)*Power(t2,2) - 64*Power(t2,3) - 
            83*t1*Power(t2,3) + 146*Power(t1,2)*Power(t2,3) - 
            28*Power(t1,3)*Power(t2,3) + 22*Power(t1,4)*Power(t2,3) + 
            57*Power(t2,4) - 44*t1*Power(t2,4) + 
            26*Power(t1,2)*Power(t2,4) - 24*Power(t1,3)*Power(t2,4) - 
            60*Power(t2,5) + 12*t1*Power(t2,5) + 
            16*Power(t1,2)*Power(t2,5) + Power(t1,3)*Power(t2,5) + 
            30*Power(t2,6) - 6*t1*Power(t2,6) - 
            2*Power(t1,2)*Power(t2,6) - 4*Power(t2,7) + t1*Power(t2,7) + 
            Power(s2,7)*(-1 + t1 + t2 + 2*t1*t2 + 2*Power(t2,2)) - 
            Power(s1,6)*(8 + 45*Power(s2,3) - 11*Power(t1,2) + 
               4*Power(s2,2)*(-1 + 10*t1 - 30*t2) + t1*(-11 + 3*t2) + 
               s2*(-52 + 5*Power(t1,2) - 2*t1*(-16 + t2) + 46*t2 + 
                  Power(t2,2))) - 
            Power(s2,6)*(Power(t1,2)*(2 + t2) + 
               t1*(2 + 3*t2 + 3*Power(t2,2)) - 
               2*(2 + t2 + 2*Power(t2,2) + Power(t2,3))) + 
            Power(s2,5)*(-4 + Power(t1,3) + 60*t2 + 62*Power(t2,2) - 
               Power(t2,3) + 3*Power(t2,4) + 
               Power(t1,2)*(5 + 11*t2 - Power(t2,2)) - 
               t1*(2 + 54*t2 + 35*Power(t2,2) + 10*Power(t2,3))) - 
            Power(s2,4)*(-47 - 29*t2 + 233*Power(t2,2) + 
               66*Power(t2,3) - 63*Power(t2,4) + 11*Power(t2,5) + 
               Power(t1,3)*(10 + 7*t2 + 4*Power(t2,2)) - 
               Power(t1,2)*(41 + 82*t2 + 84*Power(t2,2) + 
                  32*Power(t2,3)) + 
               t1*(78 + 136*t2 - 60*Power(t2,2) + 91*Power(t2,3) + 
                  6*Power(t2,4))) + 
            Power(s2,3)*(8*Power(t1,4)*(1 + t2) + 
               Power(t1,3)*(-39 - 102*t2 - 80*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(31 - 121*t2 + 154*Power(t2,2) - 
                  38*Power(t2,3) - 58*Power(t2,4)) + 
               2*t1*(29 + 390*t2 + 38*Power(t2,2) - 131*Power(t2,3) + 
                  98*Power(t2,4) + 23*Power(t2,5)) - 
               2*(29 + 286*t2 + 130*Power(t2,2) - 187*Power(t2,3) - 
                  7*Power(t2,4) + 59*Power(t2,5))) + 
            Power(s2,2)*(-248 - 4*Power(t1,5) - 158*t2 + 
               909*Power(t2,2) + 434*Power(t2,3) - 394*Power(t2,4) - 
               66*Power(t2,5) + 33*Power(t2,6) + 6*Power(t2,7) + 
               Power(t1,4)*(24 + 50*t2 - 16*Power(t2,2)) - 
               2*Power(t1,3)*
                (-49 + 58*t2 - 77*Power(t2,2) - 74*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(-490 - 23*t2 + 419*Power(t2,2) - 
                  554*Power(t2,3) - 144*Power(t2,4) + 29*Power(t2,5)) + 
               t1*(620 + 285*t2 - 1377*Power(t2,2) + 240*Power(t2,3) + 
                  558*Power(t2,4) - 15*Power(t2,5) - 31*Power(t2,6))) + 
            s2*(51 + 1191*t2 + 931*Power(t2,2) - 522*Power(t2,3) - 
               168*Power(t2,4) + 115*Power(t2,5) - 21*Power(t2,6) + 
               8*Power(t2,7) - Power(t2,8) + Power(t1,5)*(-2 + 8*t2) + 
               2*Power(t1,4)*
                (26 - 41*t2 - 60*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-218 - 308*t2 + 399*Power(t2,2) + 
                  126*Power(t2,3) - 51*Power(t2,4)) + 
               Power(t1,2)*(292 + 1800*t2 - 486*Power(t2,2) - 
                  567*Power(t2,3) + 45*Power(t2,4) + 57*Power(t2,5) - 
                  Power(t2,6)) + 
               t1*(-175 - 2622*t2 - 646*Power(t2,2) + 1106*Power(t2,3) - 
                  105*Power(t2,4) - 66*Power(t2,5) - 22*Power(t2,6) + 
                  2*Power(t2,7))) + 
            Power(s1,5)*(-31 - 23*Power(s2,4) - 17*Power(t1,3) + 
               Power(t1,2)*(17 - 37*t2) + 34*t2 + 4*Power(t2,2) + 
               Power(s2,3)*(25 - 93*t1 + 213*t2) + 
               t1*(56 - 89*t2 + 14*Power(t2,2)) - 
               Power(s2,2)*(-353 + 38*Power(t1,2) + t1*(149 - 225*t2) + 
                  87*t2 + 291*Power(t2,2)) + 
               s2*(-6*Power(t1,3) + Power(t1,2)*(-7 + 29*t2) + 
                  t1*(89 + 160*t2 - 27*Power(t2,2)) + 
                  2*(-58 - 102*t2 + 51*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(s1,4)*(112 + 14*Power(s2,5) + 60*t2 - 26*Power(t2,2) - 
               20*Power(t2,3) + Power(t1,3)*(30 + 61*t2) + 
               Power(s2,4)*(7 - 76*t1 + 93*t2) + 
               Power(t1,2)*(31 - 30*t2 + 36*Power(t2,2)) - 
               t1*(158 + 222*t2 - 240*Power(t2,2) + 25*Power(t2,3)) - 
               2*Power(s2,3)*
                (-251 + 29*Power(t1,2) + 115*t2 + 201*Power(t2,2) - 
                  9*t1*(-4 + 21*t2)) + 
               Power(s2,2)*(-157 - 20*Power(t1,3) - 1396*t2 + 
                  341*Power(t2,2) + 390*Power(t2,3) + 
                  Power(t1,2)*(-99 + 193*t2) + 
                  t1*(514 + 473*t2 - 531*Power(t2,2))) + 
               s2*(-672 + 686*t2 + 275*Power(t2,2) - 100*Power(t2,3) - 
                  15*Power(t2,4) + 2*Power(t1,3)*(-63 + 10*t2) + 
                  Power(t1,2)*(-36 + 144*t2 - 67*Power(t2,2)) + 
                  t1*(484 - 554*t2 - 342*Power(t2,2) + 70*Power(t2,3)))) \
+ Power(s1,3)*(364 + 19*Power(s2,6) - 88*Power(t1,4) - 490*t2 + 
               66*Power(t2,2) - 76*Power(t2,3) + 40*Power(t2,4) - 
               Power(s2,5)*(17 + 25*t1 + 28*t2) + 
               2*Power(s2,4)*
                (154 + t1 - 19*Power(t1,2) - 41*t2 + 101*t1*t2 - 
                  65*Power(t2,2)) - 
               2*Power(t1,3)*(-66 + 4*t2 + 41*Power(t2,2)) + 
               Power(t1,2)*(60 - 302*t2 - 28*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-460 + 740*t2 + 318*Power(t2,2) - 290*Power(t2,3) + 
                  20*Power(t2,4)) + 
               Power(s2,3)*(-507 - 16*Power(t1,3) - 1486*t2 + 
                  658*Power(t2,2) + 378*Power(t2,3) + 
                  Power(t1,2)*(7 + 232*t2) + 
                  t1*(448 - 28*t2 - 622*Power(t2,2))) + 
               Power(s2,2)*(-1379 + 894*t2 + 2136*Power(t2,2) - 
                  574*Power(t2,3) - 315*Power(t2,4) + 
                  4*Power(t1,3)*(-57 + 16*t2) + 
                  Power(t1,2)*(194 + 444*t2 - 380*Power(t2,2)) + 
                  2*t1*(560 - 1061*t2 - 255*Power(t2,2) + 
                     337*Power(t2,3))) + 
               s2*(503 - 24*Power(t1,4) + 2038*t2 - 1477*Power(t2,2) - 
                  100*Power(t2,3) + 20*Power(t2,4) + 20*Power(t2,5) + 
                  Power(t1,3)*(76 + 406*t2 - 24*Power(t2,2)) + 
                  Power(t1,2)*
                   (496 - 12*t2 - 447*Power(t2,2) + 78*Power(t2,3)) + 
                  t1*(-1307 - 1086*t2 + 1194*Power(t2,2) + 
                     408*Power(t2,3) - 85*Power(t2,4)))) + 
            Power(s1,2)*(-371 + 7*Power(s2,7) - 730*t2 + 
               701*Power(t2,2) - 248*Power(t2,3) + 164*Power(t2,4) - 
               40*Power(t2,5) - Power(s2,6)*(11 + 32*t2) + 
               2*Power(t1,4)*(37 + 79*t2) + 
               Power(s2,5)*(63 + t1 - 17*Power(t1,2) + 25*t2 + 
                  38*t1*t2 + 17*Power(t2,2)) + 
               Power(t1,3)*(56 - 216*t2 - 98*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(t1,2)*(-935 + 74*t2 + 537*Power(t2,2) + 
                  94*Power(t2,3) - 29*Power(t2,4)) + 
               t1*(1183 + 691*t2 - 1050*Power(t2,2) - 182*Power(t2,3) + 
                  155*Power(t2,4) - 5*Power(t2,5)) + 
               Power(s2,4)*(-356 - 706*t2 + 206*Power(t2,2) + 
                  62*Power(t2,3) + Power(t1,2)*(62 + 92*t2) + 
                  t1*(177 - 51*t2 - 182*Power(t2,2))) + 
               Power(s2,3)*(-568 + 1373*t2 + 1480*Power(t2,2) - 
                  844*Power(t2,3) - 177*Power(t2,4) + 
                  Power(t1,3)*(-137 + 48*t2) + 
                  Power(t1,2)*(29 - 97*t2 - 348*Power(t2,2)) + 
                  t1*(557 - 1098*t2 + 468*Power(t2,2) + 528*Power(t2,3))\
) + Power(s2,2)*(1292 - 24*Power(t1,4) + 3152*t2 - 1711*Power(t2,2) - 
                  1562*Power(t2,3) + 486*Power(t2,4) + 
                  156*Power(t2,5) + 
                  Power(t1,3)*(312 + 574*t2 - 72*Power(t2,2)) + 
                  Power(t1,2)*
                   (635 - 992*t2 - 735*Power(t2,2) + 362*Power(t2,3)) - 
                  2*t1*(1045 + 944*t2 - 1630*Power(t2,2) - 
                     91*Power(t2,3) + 243*Power(t2,4))) + 
               s2*(1423 - 1511*t2 - 2228*Power(t2,2) + 
                  1475*Power(t2,3) - 90*Power(t2,4) + 42*Power(t2,5) - 
                  15*Power(t2,6) + Power(t1,4)*(-242 + 60*t2) + 
                  Power(t1,3)*
                   (380 + 56*t2 - 485*Power(t2,2) + 12*Power(t2,3)) + 
                  Power(t1,2)*
                   (219 - 1664*t2 + 177*Power(t2,2) + 547*Power(t2,3) - 
                     47*Power(t2,4)) + 
                  t1*(-1719 + 3724*t2 + 615*Power(t2,2) - 
                     1148*Power(t2,3) - 292*Power(t2,4) + 54*Power(t2,5))\
)) + s1*(-523 + Power(s2,8) + Power(s2,7)*(-4 + t1 - 9*t2) + 550*t2 + 
               430*Power(t2,2) - 380*Power(t2,3) + 213*Power(t2,4) - 
               118*Power(t2,5) + 20*Power(t2,6) + 
               Power(t1,5)*(-68 + 8*t2) - 
               2*Power(t1,4)*(-72 + 5*t2 + 46*Power(t2,2)) + 
               Power(t1,3)*(-291 - 284*t2 + 112*Power(t2,2) + 
                  100*Power(t2,3) - 13*Power(t2,4)) + 
               Power(t1,2)*(-27 + 1473*t2 - 280*Power(t2,2) - 
                  292*Power(t2,3) - 69*Power(t2,4) + 15*Power(t2,5)) + 
               t1*(768 - 1751*t2 - 148*Power(t2,2) + 512*Power(t2,3) + 
                  18*Power(t2,4) - 21*Power(t2,5) - 2*Power(t2,6)) + 
               Power(s2,6)*(-3 - 4*Power(t1,2) + 5*t2 + 11*Power(t2,2) + 
                  t1*(9 + 5*t2)) + 
               Power(s2,5)*(-36 + 2*Power(t1,3) - 130*t2 - 
                  7*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t1,2)*(4 + 11*t2) + 
                  t1*(13 + 46*t2 - 3*Power(t2,2))) + 
               Power(s2,4)*(-24 + 606*t2 + 464*Power(t2,2) - 
                  194*Power(t2,3) + 9*Power(t2,4) + 
                  Power(t1,3)*(-21 + 8*t2) + 
                  Power(t1,2)*(3 - 143*t2 - 86*Power(t2,2)) + 
                  t1*(74 - 261*t2 + 140*Power(t2,2) + 62*Power(t2,3))) + 
               Power(s2,3)*(621 - 4*Power(t1,4) + 845*t2 - 
                  1240*Power(t2,2) - 510*Power(t2,3) + 509*Power(t2,4) + 
                  33*Power(t2,5) + 
                  Power(t1,3)*(110 + 188*t2 - 40*Power(t2,2)) + 
                  Power(t1,2)*
                   (111 - 100*t2 + 128*Power(t2,2) + 232*Power(t2,3)) - 
                  t1*(831 + 704*t2 - 912*Power(t2,2) + 564*Power(t2,3) + 
                     237*Power(t2,4))) + 
               Power(s2,2)*(195 - 2223*t2 - 2207*Power(t2,2) + 
                  1368*Power(t2,3) + 535*Power(t2,4) - 203*Power(t2,5) - 
                  45*Power(t2,6) + 8*Power(t1,4)*(-13 + 6*t2) + 
                  Power(t1,3)*
                   (242 - 480*t2 - 494*Power(t2,2) + 32*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (17 + 535*t2 - 676*Power(t2,2) - 267*Power(t2,3) + 
                     83*Power(t2,4)) + 
                  t1*(-337 + 3511*t2 + 528*Power(t2,2) - 
                     2210*Power(t2,3) + 19*Power(t2,4) + 189*Power(t2,5))\
) + s2*(-1249 - 8*Power(t1,5) - 2360*t2 + 1530*Power(t2,2) + 
                  1030*Power(t2,3) - 683*Power(t2,4) + 88*Power(t2,5) - 
                  34*Power(t2,6) + 6*Power(t2,7) + 
                  Power(t1,4)*(174 + 332*t2 - 44*Power(t2,2)) - 
                  2*Power(t1,3)*
                   (-103 + 355*t2 + 129*Power(t2,2) - 128*Power(t2,3) + 
                     Power(t2,4)) + 
                  Power(t1,2)*
                   (-1897 + 207*t2 + 1735*Power(t2,2) - 
                     174*Power(t2,3) - 294*Power(t2,4) + 13*Power(t2,5)) \
+ t1*(2787 + 2392*t2 - 3523*Power(t2,2) + 92*Power(t2,3) + 
                     485*Power(t2,4) + 120*Power(t2,5) - 17*Power(t2,6))))\
))*T2q(1 - s + s2 - t1,1 - s1 - t1 + t2))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)*
       (-3 + 2*s + Power(s,2) + 2*s1 - 2*s*s1 + Power(s1,2) - 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) + 4*t1 - 2*t2 + 2*s*t2 - 2*s1*t2 - 
         2*s2*t2 + Power(t2,2))) + 
    (8*(-2 + 19*s2 - 4*Power(s1,6)*Power(s2,2)*(2 + s2 - t1)*(-1 + t1) + 
         4*t1 - 98*s2*t1 + 10*Power(t1,2) + 205*s2*Power(t1,2) - 
         40*Power(t1,3) - 220*s2*Power(t1,3) + 50*Power(t1,4) + 
         125*s2*Power(t1,4) - 28*Power(t1,5) - 34*s2*Power(t1,5) + 
         6*Power(t1,6) + 3*s2*Power(t1,6) - 21*t2 + 32*s2*t2 + 
         53*Power(s2,2)*t2 + Power(s2,3)*t2 + 79*t1*t2 - 154*s2*t1*t2 - 
         220*Power(s2,2)*t1*t2 - 4*Power(s2,3)*t1*t2 - 
         101*Power(t1,2)*t2 + 297*s2*Power(t1,2)*t2 + 
         350*Power(s2,2)*Power(t1,2)*t2 + 6*Power(s2,3)*Power(t1,2)*t2 + 
         34*Power(t1,3)*t2 - 288*s2*Power(t1,3)*t2 - 
         260*Power(s2,2)*Power(t1,3)*t2 - 4*Power(s2,3)*Power(t1,3)*t2 + 
         29*Power(t1,4)*t2 + 142*s2*Power(t1,4)*t2 + 
         85*Power(s2,2)*Power(t1,4)*t2 + Power(s2,3)*Power(t1,4)*t2 - 
         25*Power(t1,5)*t2 - 30*s2*Power(t1,5)*t2 - 
         8*Power(s2,2)*Power(t1,5)*t2 + 5*Power(t1,6)*t2 + 
         s2*Power(t1,6)*t2 - 20*Power(t2,2) - 50*s2*Power(t2,2) + 
         68*Power(s2,2)*Power(t2,2) + 46*Power(s2,3)*Power(t2,2) + 
         Power(s2,4)*Power(t2,2) + 41*t1*Power(t2,2) + 
         124*s2*t1*Power(t2,2) - 232*Power(s2,2)*t1*Power(t2,2) - 
         144*Power(s2,3)*t1*Power(t2,2) - 3*Power(s2,4)*t1*Power(t2,2) + 
         6*Power(t1,2)*Power(t2,2) - 62*s2*Power(t1,2)*Power(t2,2) + 
         290*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         156*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         3*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         63*Power(t1,3)*Power(t2,2) - 58*s2*Power(t1,3)*Power(t2,2) - 
         158*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         64*Power(s2,3)*Power(t1,3)*Power(t2,2) - 
         Power(s2,4)*Power(t1,3)*Power(t2,2) + 
         43*Power(t1,4)*Power(t2,2) + 56*s2*Power(t1,4)*Power(t2,2) + 
         34*Power(s2,2)*Power(t1,4)*Power(t2,2) + 
         6*Power(s2,3)*Power(t1,4)*Power(t2,2) - 
         6*Power(t1,5)*Power(t2,2) - 10*s2*Power(t1,5)*Power(t2,2) - 
         2*Power(s2,2)*Power(t1,5)*Power(t2,2) - 
         Power(t1,6)*Power(t2,2) + 25*Power(t2,3) - 33*s2*Power(t2,3) - 
         29*Power(s2,2)*Power(t2,3) + 54*Power(s2,3)*Power(t2,3) + 
         15*Power(s2,4)*Power(t2,3) - 89*t1*Power(t2,3) + 
         13*s2*t1*Power(t2,3) + 24*Power(s2,2)*t1*Power(t2,3) - 
         119*Power(s2,3)*t1*Power(t2,3) - 31*Power(s2,4)*t1*Power(t2,3) + 
         113*Power(t1,2)*Power(t2,3) + 91*s2*Power(t1,2)*Power(t2,3) + 
         44*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         77*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         17*Power(s2,4)*Power(t1,2)*Power(t2,3) - 
         57*Power(t1,3)*Power(t2,3) - 87*s2*Power(t1,3)*Power(t2,3) - 
         44*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         13*Power(s2,3)*Power(t1,3)*Power(t2,3) - 
         Power(s2,4)*Power(t1,3)*Power(t2,3) + 
         6*Power(t1,4)*Power(t2,3) + 14*s2*Power(t1,4)*Power(t2,3) + 
         5*Power(s2,2)*Power(t1,4)*Power(t2,3) + 
         Power(s2,3)*Power(t1,4)*Power(t2,3) + 
         2*Power(t1,5)*Power(t2,3) + 2*s2*Power(t1,5)*Power(t2,3) - 
         2*Power(t2,4) + 45*s2*Power(t2,4) - 6*Power(s2,2)*Power(t2,4) + 
         6*Power(s2,3)*Power(t2,4) + 20*Power(s2,4)*Power(t2,4) + 
         2*Power(s2,5)*Power(t2,4) + 7*t1*Power(t2,4) - 
         112*s2*t1*Power(t2,4) - 37*Power(s2,2)*t1*Power(t2,4) - 
         18*Power(s2,3)*t1*Power(t2,4) - 22*Power(s2,4)*t1*Power(t2,4) - 
         2*Power(s2,5)*t1*Power(t2,4) - 9*Power(t1,2)*Power(t2,4) + 
         82*s2*Power(t1,2)*Power(t2,4) + 
         49*Power(s2,2)*Power(t1,2)*Power(t2,4) + 
         12*Power(s2,3)*Power(t1,2)*Power(t2,4) + 
         2*Power(s2,4)*Power(t1,2)*Power(t2,4) + 
         5*Power(t1,3)*Power(t2,4) - 12*s2*Power(t1,3)*Power(t2,4) - 
         5*Power(s2,2)*Power(t1,3)*Power(t2,4) - 
         Power(t1,4)*Power(t2,4) - 3*s2*Power(t1,4)*Power(t2,4) - 
         Power(s2,2)*Power(t1,4)*Power(t2,4) - s2*Power(t2,5) + 
         21*Power(s2,2)*Power(t2,5) + 6*Power(s2,3)*Power(t2,5) + 
         6*Power(s2,4)*Power(t2,5) + 2*Power(s2,5)*Power(t2,5) + 
         3*s2*t1*Power(t2,5) - 31*Power(s2,2)*t1*Power(t2,5) - 
         10*Power(s2,3)*t1*Power(t2,5) - 3*s2*Power(t1,2)*Power(t2,5) + 
         7*Power(s2,2)*Power(t1,2)*Power(t2,5) - 
         2*Power(s2,3)*Power(t1,2)*Power(t2,5) + 
         s2*Power(t1,3)*Power(t2,5) + 
         Power(s2,2)*Power(t1,3)*Power(t2,5) + 
         Power(s,7)*(2 + 2*t1 + 3*Power(t1,2) - 2*t2 - 6*t1*t2 + 
            3*Power(t2,2) - s1*(-1 + s2)*(-2 + 2*s2 - t1 + t2) + 
            Power(s2,2)*(2 - 3*t1 + 3*t2) + 
            s2*(-4 + t1 + Power(t1,2) - t2 - 2*t1*t2 + Power(t2,2))) - 
         2*Power(s1,5)*s2*(3*Power(s2,3) + Power(s2,4) - 
            4*(-2 + t1)*Power(-1 + t1,2) + 
            Power(s2,2)*(1 + Power(t1,2) + 10*t2 - 5*t1*(1 + 2*t2)) + 
            s2*(14 - 2*Power(t1,3) + 20*t2 + 2*Power(t1,2)*(8 + 5*t2) - 
               t1*(29 + 30*t2))) + 
         Power(s1,4)*(Power(-1 + t1,3)*(-8 + 4*t1 + Power(t1,2)) - 
            2*Power(s2,5)*(-1 + t1 - 5*t2) - 
            6*Power(s2,4)*(-3 + 3*t1 - 5*t2) - 
            s2*(-1 + t1)*(44 + Power(t1,4) + 64*t2 - 
               Power(t1,3)*(8 + t2) + Power(t1,2)*(49 + 34*t2) - 
               t1*(90 + 97*t2)) + 
            Power(s2,3)*(5 + Power(t1,3) + 12*t2 + 40*Power(t2,2) + 
               Power(t1,2)*(9 + 4*t2) - t1*(15 + 46*t2 + 40*Power(t2,2))\
) + Power(s2,2)*(-33 + 2*Power(t1,4) + 135*t2 + 80*Power(t2,2) - 
               3*Power(t1,3)*(7 + 5*t2) + 
               t1*(7 - 267*t2 - 120*Power(t2,2)) + 
               Power(t1,2)*(45 + 137*t2 + 40*Power(t2,2)))) + 
         Power(s1,3)*(-4*Power(s2,5)*t2*(2 - 2*t1 + 5*t2) - 
            Power(-1 + t1,2)*
             (3*Power(t1,4) + Power(t1,3)*(-7 + t2) + 4*(5 + 6*t2) + 
               2*Power(t1,2)*(11 + 7*t2) - t1*(40 + 39*t2)) - 
            Power(s2,4)*(13 + Power(t1,3) + 74*t2 + 60*Power(t2,2) + 
               Power(t1,2)*(11 + 2*t2) - t1*(25 + 76*t2)) - 
            s2*(-1 + t1)*(72 + Power(t1,5) - 180*t2 - 95*Power(t2,2) - 
               Power(t1,4)*(10 + 3*t2) + 
               Power(t1,3)*(49 + 20*t2 + 3*Power(t2,2)) - 
               Power(t1,2)*(66 + 163*t2 + 53*Power(t2,2)) + 
               t1*(-46 + 342*t2 + 145*Power(t2,2))) + 
            Power(s2,3)*(-54 + Power(t1,4) + Power(t1,3)*(7 - 5*t2) - 
               19*t2 - 30*Power(t2,2) - 40*Power(t2,3) + 
               Power(t1,2)*(-71 - 33*t2 + 2*Power(t2,2)) + 
               t1*(117 + 57*t2 + 88*Power(t2,2) + 40*Power(t2,3))) + 
            Power(s2,2)*(46 + Power(t1,5) + 108*t2 - 258*Power(t2,2) - 
               80*Power(t2,3) - 2*Power(t1,4)*(1 + 2*t2) + 
               2*Power(t1,3)*(6 + 31*t2 + 10*Power(t2,2)) - 
               2*Power(t1,2)*
                (-12 + 86*t2 + 113*Power(t2,2) + 20*Power(t2,3)) + 
               t1*(-81 + 6*t2 + 484*Power(t2,2) + 120*Power(t2,3)))) + 
         Power(s1,2)*(4*Power(s2,5)*Power(t2,2)*(3 - 3*t1 + 5*t2) + 
            Power(-1 + t1,2)*
             (-37 + 65*t2 + 22*Power(t2,2) + Power(t1,4)*(6 + 4*t2) + 
               t1*(27 - 115*t2 - 39*Power(t2,2)) - 
               Power(t1,3)*(30 + 4*t2 + Power(t2,2)) + 
               2*Power(t1,2)*(17 + 22*t2 + 9*Power(t2,2))) + 
            Power(s2,4)*(-Power(t1,4) + Power(t1,3)*(3 + t2) + 
               t1*(1 - 81*t2 - 120*Power(t2,2)) + 
               Power(t1,2)*(-3 + 39*t2 + 6*Power(t2,2)) + 
               t2*(41 + 114*t2 + 60*Power(t2,2))) + 
            s2*(-1 + t1)*(83 + 178*t2 + Power(t1,5)*t2 - 
               273*Power(t2,2) - 61*Power(t2,3) - 
               Power(t1,4)*(8 + 9*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(7 + 92*t2 + 13*Power(t2,2) + 
                  3*Power(t2,3)) - 
               Power(t1,2)*(-93 + 181*t2 + 194*Power(t2,2) + 
                  35*Power(t2,3)) + 
               t1*(-175 - 81*t2 + 481*Power(t2,2) + 93*Power(t2,3))) + 
            Power(s2,3)*(39 + 2*Power(t1,5) + 162*t2 + 29*Power(t2,2) + 
               38*Power(t2,3) + 20*Power(t2,4) - Power(t1,4)*(9 + t2) + 
               Power(t1,3)*(-24 - 27*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(106 + 219*t2 + 51*Power(t2,2) - 
                  10*Power(t2,3)) - 
               t1*(114 + 353*t2 + 87*Power(t2,2) + 88*Power(t2,3) + 
                  20*Power(t2,4))) - 
            Power(s2,2)*(-60 + Power(t1,6) + Power(t1,5)*(-14 + t2) + 
               124*t2 + 123*Power(t2,2) - 244*Power(t2,3) - 
               40*Power(t2,4) - Power(t1,4)*(-28 + 2*t2 + Power(t2,2)) + 
               2*Power(t1,3)*
                (25 + 25*t2 + 33*Power(t2,2) + 5*Power(t2,3)) - 
               Power(t1,2)*(193 - 26*t2 + 258*Power(t2,2) + 
                  176*Power(t2,3) + 20*Power(t2,4)) + 
               t1*(188 - 199*t2 + 70*Power(t2,2) + 430*Power(t2,3) + 
                  60*Power(t2,4)))) + 
         s1*(-2*Power(s2,5)*Power(t2,3)*(4 - 4*t1 + 5*t2) - 
            Power(s2,4)*t2*(1 - Power(t1,4) - Power(t1,3)*(-2 + t2) + 
               43*t2 + 78*Power(t2,2) + 30*Power(t2,3) + 
               3*Power(t1,2)*t2*(15 + 2*t2) - 
               t1*(2 + 87*t2 + 84*Power(t2,2))) - 
            Power(-1 + t1,2)*
             (-38 - 58*t2 + 70*Power(t2,2) + 4*Power(t2,3) + 
               Power(t1,4)*(7 + t2 + Power(t2,2)) + 
               t1*(86 + 35*t2 - 114*Power(t2,2) - 9*Power(t2,3)) - 
               Power(t1,3)*(4 + 25*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(-51 + 47*t2 + 32*Power(t2,2) + 
                  6*Power(t2,3))) + 
            s2*(-1 + t1)*(20 + 8*Power(t1,5) - 134*t2 - 
               139*Power(t2,2) + 182*Power(t2,3) + 13*Power(t2,4) + 
               Power(t1,4)*(-19 + 17*t2 - 3*Power(t2,2) + 
                  Power(t2,3)) + 
               t1*(-65 + 253*t2 + 15*Power(t2,2) - 296*Power(t2,3) - 
                  19*Power(t2,4)) - 
               Power(t1,3)*(11 + 49*t2 + 59*Power(t2,2) - 
                  2*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(67 - 87*t2 + 186*Power(t2,2) + 
                  95*Power(t2,3) + 7*Power(t2,4))) - 
            Power(s2,3)*(Power(t1,5)*(1 + t2) + 
               Power(t1,4)*(-4 + 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(6 - 98*t2 - 33*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(t1,2)*(-4 + 272*t2 + 225*Power(t2,2) + 
                  39*Power(t2,3) - 8*Power(t2,4)) + 
               t2*(86 + 162*t2 + 21*Power(t2,2) + 24*Power(t2,3) + 
                  4*Power(t2,4)) - 
               t1*(-1 + 263*t2 + 355*Power(t2,2) + 63*Power(t2,3) + 
                  46*Power(t2,4) + 4*Power(t2,5))) + 
            Power(s2,2)*(-47 + Power(t1,6) - 130*t2 + 107*Power(t2,2) + 
               54*Power(t2,3) - 114*Power(t2,4) - 8*Power(t2,5) - 
               Power(t1,5)*(3 + 5*t2) + 
               Power(t1,4)*(-45 - 26*t2 - 5*Power(t2,2) + 
                  2*Power(t2,3)) + 
               2*Power(t1,3)*
                (95 + 119*t2 + 41*Power(t2,2) + 15*Power(t2,3)) - 
               Power(t1,2)*(285 + 508*t2 + 42*Power(t2,2) + 
                  180*Power(t2,3) + 62*Power(t2,4) + 4*Power(t2,5)) + 
               t1*(189 + 431*t2 - 142*Power(t2,2) + 94*Power(t2,3) + 
                  186*Power(t2,4) + 12*Power(t2,5)))) + 
         Power(s,6)*(-6 + t1 - 4*Power(t1,2) + 11*Power(t1,3) + 
            Power(s2,3)*(-4 + 6*t1 - 5*t2) + 25*t2 + 20*t1*t2 - 
            17*Power(t1,2)*t2 - 16*Power(t2,2) + t1*Power(t2,2) + 
            5*Power(t2,3) + Power(s2,2)*
             (1 - 10*Power(t1,2) + t2 + 5*Power(t2,2) + 5*t1*(2 + t2)) + 
            Power(s1,2)*(6 + 9*Power(s2,2) + 2*t1 - t2 + 
               s2*(-15 - 3*t1 + 2*t2)) + 
            s2*(4*Power(t1,3) - 4*Power(t1,2)*(1 + 2*t2) + 
               t1*(-14 + 19*t2 + 4*Power(t2,2)) - 
               3*(-3 + 8*t2 + 5*Power(t2,2))) + 
            s1*(-3 + 3*Power(s2,3) - 16*t1 - 15*Power(t1,2) + 
               Power(s2,2)*(-11 + 5*t1 - 15*t2) - 8*t2 + 19*t1*t2 - 
               4*Power(t2,2) + 
               s2*(11 + Power(t1,2) + 25*t2 - 2*Power(t2,2) + 
                  t1*(9 + t2)))) + 
         Power(s,5)*(-4 + 14*t1 + 2*Power(t1,2) - 21*Power(t1,3) + 
            15*Power(t1,4) - 86*t2 + 48*t1*t2 + 45*Power(t1,2)*t2 - 
            12*Power(t1,3)*t2 + 60*Power(t2,2) + 3*t1*Power(t2,2) - 
            19*Power(t1,2)*Power(t2,2) - 27*Power(t2,3) + 
            14*t1*Power(t2,3) + 2*Power(t2,4) + 
            Power(s2,4)*(2 - 3*t1 + t2) - 
            Power(s1,3)*(8 + 20*Power(s2,2) + t1 + 
               s2*(-27 - 3*t1 + t2)) + 
            Power(s2,3)*(8 + 14*Power(t1,2) + 10*t2 - 5*Power(t2,2) - 
               t1*(23 + 5*t2)) + 
            Power(s1,2)*(-1 - 7*Power(s2,3) + 22*Power(t1,2) + 
               t1*(26 - 16*t2) + 21*t2 + 2*Power(t2,2) + 
               Power(s2,2)*(30 + 2*t1 + 49*t2) - 
               s2*(19 + 9*Power(t1,2) + t1*(32 - 5*t2) + 73*t2)) + 
            Power(s2,2)*(-21 - 15*Power(t1,3) + 32*t2 + 
               43*Power(t2,2) + 10*Power(t2,3) + 
               Power(t1,2)*(29 + 9*t2) + t1*(2 - 42*t2 - 4*Power(t2,2))) \
+ s2*(16 + 6*Power(t1,4) + 46*t2 - 44*Power(t2,2) - 15*Power(t2,3) - 
               2*Power(t2,4) - Power(t1,3)*(19 + 11*t2) + 
               2*Power(t1,2)*(-2 + 32*t2 + Power(t2,2)) + 
               t1*(2 - 92*t2 - 30*Power(t2,2) + 5*Power(t2,3))) + 
            s1*(31 - 46*Power(t1,3) - 32*t2 + 14*Power(t2,2) - 
               4*Power(t2,3) + Power(s2,3)*(3 - 8*t1 + 13*t2) + 
               Power(t1,2)*(-11 + 47*t2) + 
               3*t1*(8 - 35*t2 + Power(t2,2)) + 
               Power(s2,2)*(15 + 22*Power(t1,2) - 67*t2 - 
                  39*Power(t2,2) - 5*t1*(6 + t2)) + 
               s2*(-52 - 6*Power(t1,3) + 87*t2 + 61*Power(t2,2) + 
                  3*Power(t2,3) + Power(t1,2)*(27 + 16*t2) + 
                  t1*(24 + 28*t2 - 13*Power(t2,2))))) + 
         Power(s,4)*(35 - 108*t1 + 87*Power(t1,2) - Power(t1,3) - 
            22*Power(t1,4) + 9*Power(t1,5) + 
            Power(s1,4)*(4 + 25*Power(s2,2) - s2*(24 + t1)) + 122*t2 + 
            Power(s2,5)*t2 - 196*t1*t2 + 82*Power(t1,2)*t2 + 
            3*Power(t1,3)*t2 + 6*Power(t1,4)*t2 - 161*Power(t2,2) + 
            76*t1*Power(t2,2) + 90*Power(t1,2)*Power(t2,2) - 
            32*Power(t1,3)*Power(t2,2) + 73*Power(t2,3) - 
            61*t1*Power(t2,3) + 10*Power(t1,2)*Power(t2,3) - 
            10*Power(t2,4) + 7*t1*Power(t2,4) - 
            Power(s2,4)*(9 + 5*Power(t1,2) + 15*t2 + 9*Power(t2,2) - 
               7*t1*(2 + t2)) + 
            Power(s1,3)*(24 + 15*Power(s2,3) - 28*t1 - 11*Power(t1,2) - 
               12*t2 + 3*t1*t2 - Power(s2,2)*(42 + 19*t1 + 84*t2) + 
               s2*(-13 + 11*Power(t1,2) + t1*(61 - 5*t2) + 81*t2 + 
                  Power(t2,2))) + 
            Power(s2,3)*(-3 + 14*Power(t1,3) - 46*t2 - 41*Power(t2,2) - 
               9*Power(t2,3) - Power(t1,2)*(47 + 10*t2) + 
               t1*(36 + 71*t2 + 10*Power(t2,2))) + 
            Power(s2,2)*(13 - 13*Power(t1,4) - 117*t2 - 
               16*Power(t2,2) + 59*Power(t2,3) + 10*Power(t2,4) + 
               Power(t1,3)*(59 + 17*t2) - 
               Power(t1,2)*(64 + 141*t2 + 20*Power(t2,2)) + 
               t1*(5 + 206*t2 + 78*Power(t2,2) + 6*Power(t2,3))) + 
            s2*(-35 + 4*Power(t1,5) + 120*t2 + 245*Power(t2,2) - 
               24*Power(t2,3) + 4*Power(t2,4) - Power(t2,5) - 
               Power(t1,4)*(23 + 5*t2) + 
               Power(t1,3)*(2 + 63*t2 - 8*Power(t2,2)) + 
               2*Power(t1,2)*
                (12 - 24*t2 + 11*Power(t2,2) + 7*Power(t2,3)) - 
               2*t1*(-14 + 70*t2 + 100*Power(t2,2) + 33*Power(t2,3) + 
                  2*Power(t2,4))) - 
            Power(s1,2)*(90 + 10*Power(s2,4) - 62*Power(t1,3) + 43*t2 - 
               2*Power(t2,2) + Power(t1,2)*(-5 + 38*t2) + 
               Power(s2,3)*(27 - 11*t1 + 41*t2) - 
               t1*(-10 + 135*t2 + Power(t2,2)) + 
               Power(s2,2)*(44 + 21*Power(t1,2) - 154*t2 - 
                  103*Power(t2,2) - t1*(62 + 41*t2)) + 
               s2*(-189 + 6*Power(t1,3) + 11*t2 + 86*Power(t2,2) + 
                  3*Power(t2,3) + Power(t1,2)*(34 + 5*t2) + 
                  t1*(77 + 182*t2 - 9*Power(t2,2)))) + 
            s1*(-27 - Power(s2,5) - 58*Power(t1,4) + 244*t2 - 
               54*Power(t2,2) + 16*Power(t2,3) + 
               5*Power(t1,3)*(5 + 6*t2) + 
               Power(s2,4)*(11 - 3*t1 + 19*t2) + 
               Power(s2,3)*(3 - 20*Power(t1,2) + t1*(2 - 18*t2) + 
                  65*t2 + 35*Power(t2,2)) + 
               Power(t1,2)*(70 - 215*t2 + 39*Power(t2,2)) + 
               t1*(-27 + t2 - 46*Power(t2,2) - 11*Power(t2,3)) + 
               Power(s2,2)*(46 + 30*Power(t1,3) + 38*t2 - 
                  171*Power(t2,2) - 54*Power(t2,3) + 
                  Power(t1,2)*(-37 + 22*t2) - 
                  t1*(4 + 99*t2 + 28*Power(t2,2))) + 
               s2*(-40 - 14*Power(t1,4) - 475*t2 + 48*Power(t2,2) + 
                  25*Power(t2,3) + 3*Power(t2,4) + 
                  Power(t1,3)*(68 + 30*t2) - 
                  2*Power(t1,2)*(31 + 35*t2 + 10*Power(t2,2)) + 
                  t1*(58 + 384*t2 + 187*Power(t2,2) + Power(t2,3))))) + 
         Power(s,2)*(17 + 4*Power(s1,6)*Power(s2,2) - 119*t1 + 
            262*Power(t1,2) - 242*Power(t1,3) + 89*Power(t1,4) - 
            7*Power(t1,5) + 4*Power(s1,5)*s2*
             (-8 + 4*Power(s2,2) + 6*t1 + s2*(6 - 7*t1 - 5*t2)) - 
            62*t2 + 139*t1*t2 - 47*Power(t1,2)*t2 - 96*Power(t1,3)*t2 + 
            90*Power(t1,4)*t2 - 27*Power(t1,5)*t2 + 3*Power(t1,6)*t2 - 
            219*Power(t2,2) + 376*t1*Power(t2,2) - 
            141*Power(t1,2)*Power(t2,2) - 54*Power(t1,3)*Power(t2,2) + 
            39*Power(t1,4)*Power(t2,2) - Power(t1,5)*Power(t2,2) + 
            161*Power(t2,3) - 282*t1*Power(t2,3) + 
            111*Power(t1,2)*Power(t2,3) + 15*Power(t1,3)*Power(t2,3) - 
            7*Power(t1,4)*Power(t2,3) - 20*Power(t2,4) + 
            42*t1*Power(t2,4) - 27*Power(t1,2)*Power(t2,4) + 
            5*Power(t1,3)*Power(t2,4) + 
            3*Power(s2,5)*t2*
             (-1 - Power(t1,2) - 2*t1*(-1 + t2) + 2*t2 + 4*Power(t2,2)) \
- Power(s2,4)*(5 + Power(t1,4) - 17*t2 - 38*Power(t2,2) - 
               9*Power(t2,3) + 8*Power(t2,4) - Power(t1,3)*(8 + 9*t2) + 
               Power(t1,2)*(18 + t2 + 3*Power(t2,2)) - 
               t1*(16 - 25*t2 - 35*Power(t2,2) + Power(t2,3))) + 
            Power(s2,3)*(19 + 2*Power(t1,5) - 34*t2 + 9*Power(t2,2) + 
               39*Power(t2,3) - 60*Power(t2,4) - 2*Power(t2,5) - 
               3*Power(t1,4)*(7 + 3*t2) + 
               Power(t1,3)*(32 + 47*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(10 - 101*t2 + 43*Power(t2,2) + 
                  12*Power(t2,3)) - 
               t1*(42 - 97*t2 + 58*Power(t2,2) + 31*Power(t2,3) + 
                  14*Power(t2,4))) + 
            Power(s2,2)*(-38 - Power(t1,6) + 37*t2 + 313*Power(t2,2) + 
               10*Power(t2,3) - 86*Power(t2,4) + 5*Power(t2,5) + 
               Power(t1,5)*(17 + 2*t2) + 
               Power(t1,4)*(-42 - 8*t2 + 7*Power(t2,2)) - 
               Power(t1,3)*(-72 + 49*t2 + 112*Power(t2,2) + 
                  22*Power(t2,3)) + 
               Power(t1,2)*(-119 + 151*t2 + 328*Power(t2,2) + 
                  96*Power(t2,3) + 7*Power(t2,4)) + 
               t1*(111 - 133*t2 - 536*Power(t2,2) - 30*Power(t2,3) + 
                  59*Power(t2,4) + 7*Power(t2,5))) + 
            s2*(95 + Power(t1,6)*(-1 + t2) + 436*t2 + 68*Power(t2,2) - 
               307*Power(t2,3) + 99*Power(t2,4) - 6*Power(t2,5) - 
               2*Power(t1,5)*(15 + 4*t2 + 2*Power(t2,2)) + 
               Power(t1,4)*(159 + 102*t2 + 57*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,3)*(-382 - 521*t2 - 158*Power(t2,2) - 
                  36*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,2)*(507 + 1199*t2 + 147*Power(t2,2) - 
                  52*Power(t2,3) - 21*Power(t2,4) - 3*Power(t2,5)) + 
               t1*(-348 - 1209*t2 - 110*Power(t2,2) + 335*Power(t2,3) - 
                  50*Power(t2,4) + 9*Power(t2,5))) + 
            Power(s1,4)*(-10*Power(s2,4) + 
               3*(12 - 20*t1 + 7*Power(t1,2) + Power(t1,3)) - 
               Power(s2,3)*(35 + 11*t1 + 68*t2) + 
               s2*(4 - 6*Power(t1,3) + t1*(63 - 99*t2) + 
                  3*Power(t1,2)*(-15 + t2) + 128*t2) + 
               Power(s2,2)*(-130 - 3*Power(t1,2) - 89*t2 + 
                  40*Power(t2,2) + t1*(107 + 119*t2))) + 
            Power(s1,3)*(-12*Power(s2,5) - 34*Power(t1,4) + 
               Power(t1,3)*(43 + 6*t2) - 12*(5 + 9*t2) - 
               3*Power(t1,2)*(9 + 32*t2) + 
               Power(s2,4)*(-15 + 5*t1 + 38*t2) + 2*t1*(40 + 99*t2) + 
               Power(s2,3)*(-21 - 24*Power(t1,2) + 159*t2 + 
                  110*Power(t2,2) + t1*(25 + 53*t2)) + 
               2*Power(s2,2)*
                (-36 + 5*Power(t1,3) + 250*t2 + 59*Power(t2,2) - 
                  20*Power(t2,3) + Power(t1,2)*(-6 + 4*t2) + 
                  t1*(10 - 205*t2 - 98*Power(t2,2))) + 
               s2*(401 + 6*Power(t1,4) - 105*t2 + 2*Power(t1,3)*t2 - 
                  186*Power(t2,2) + 
                  Power(t1,2)*(148 + 198*t2 - 6*Power(t2,2)) + 
                  t1*(-497 - 175*t2 + 144*Power(t2,2)))) + 
            Power(s1,2)*(-287 + 40*Power(t1,5) - 
               6*Power(s2,5)*(-1 + t1 - 6*t2) + 243*t2 + 
               88*Power(t2,2) + Power(t1,4)*(-49 + 11*t2) + 
               t1*(473 - 266*t2 - 174*Power(t2,2)) + 
               Power(t1,3)*(-40 + 129*t2 - 16*Power(t2,2)) + 
               Power(t1,2)*(-137 - 123*t2 + 102*Power(t2,2)) + 
               Power(s2,4)*(44 + 3*Power(t1,2) + 39*t2 - 
                  54*Power(t2,2) - t1*(47 + 9*t2)) + 
               Power(s2,3)*(-18 + 3*Power(t1,3) + 87*t2 - 
                  273*Power(t2,2) - 82*Power(t2,3) + 
                  22*Power(t1,2)*(1 + 3*t2) - 
                  t1*(7 + 93*t2 + 87*Power(t2,2))) + 
               s2*(1 + 9*Power(t1,5) - 1153*t2 + 297*Power(t2,2) + 
                  110*Power(t2,3) - Power(t1,4)*(71 + 22*t2) + 
                  Power(t1,3)*(225 + 44*t2 + 18*Power(t2,2)) - 
                  6*Power(t1,2)*(59 + 86*t2 + 47*Power(t2,2)) + 
                  t1*(190 + 1473*t2 + 111*Power(t2,2) - 78*Power(t2,3))\
) + Power(s2,2)*(253 - 11*Power(t1,4) + 158*t2 - 696*Power(t2,2) - 
                  60*Power(t2,3) + 20*Power(t2,4) - 
                  4*Power(t1,3)*(3 + 10*t2) + 
                  2*Power(t1,2)*(61 + 60*t2) + 
                  2*t1*(-176 - 38*t2 + 279*Power(t2,2) + 77*Power(t2,3))\
)) + s1*(192 - 7*Power(t1,6) + 530*t2 - 344*Power(t2,2) + 
               4*Power(t2,3) - 5*Power(t1,5)*(1 + 5*t2) + 
               6*Power(t1,4)*(13 + 5*Power(t2,2)) + 
               Power(t1,2)*(625 + 432*t2 + 39*Power(t2,2)) + 
               Power(t1,3)*(-306 + 16*t2 - 187*Power(t2,2) + 
                  2*Power(t2,3)) - 
               t1*(577 + 953*t2 - 468*Power(t2,2) + 6*Power(t2,3)) + 
               3*Power(s2,5)*
                (1 + Power(t1,2) - 4*t2 - 12*Power(t2,2) + 
                  t1*(-2 + 4*t2)) - 
               Power(s2,4)*(25 + 23*Power(t1,2) + Power(t1,3) + 82*t2 + 
                  33*Power(t2,2) - 34*Power(t2,3) - 
                  t1*(49 + 82*t2 + 3*Power(t2,2))) + 
               Power(s2,3)*(-4 - 13*Power(t1,4) - 
                  Power(t1,3)*(-57 + t2) + t2 - 105*Power(t2,2) + 
                  209*Power(t2,3) + 26*Power(t2,4) - 
                  Power(t1,2)*(79 + 89*t2 + 54*Power(t2,2)) + 
                  t1*(39 + 89*t2 + 99*Power(t2,2) + 59*Power(t2,3))) + 
               Power(s2,2)*(-12 + 13*Power(t1,5) - 601*t2 - 
                  96*Power(t2,2) + 412*Power(t2,3) + 2*Power(t2,4) - 
                  4*Power(t2,5) - Power(t1,4)*(97 + 15*t2) + 
                  Power(t1,3)*(249 + 216*t2 + 52*Power(t2,2)) - 
                  Power(t1,2)*
                   (271 + 612*t2 + 204*Power(t2,2) + 12*Power(t2,3)) + 
                  t1*(118 + 1012*t2 + 86*Power(t2,2) - 
                     314*Power(t2,3) - 56*Power(t2,4))) + 
               s2*(-339 - 3*Power(t1,6) - 72*t2 + 1059*Power(t2,2) - 
                  295*Power(t2,3) - 14*Power(t2,4) + 
                  Power(t1,5)*(27 + t2) + 
                  Power(t1,4)*(-9 - 35*t2 + 14*Power(t2,2)) + 
                  Power(t1,3)*
                   (51 + 47*t2 - 8*Power(t2,2) - 18*Power(t2,3)) + 
                  t1*(780 - 40*t2 - 1311*Power(t2,2) + 51*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (-169 + 33*t2 + 140*Power(t2,2) + 50*Power(t2,3) + 
                     2*Power(t2,4))))) - 
         Power(s,3)*(45 + 8*Power(s1,5)*s2*(-1 + 2*s2) - 191*t1 + 
            266*Power(t1,2) - 136*Power(t1,3) + 11*Power(t1,4) + 
            7*Power(t1,5) - 2*Power(t1,6) + 47*t2 - 138*t1*t2 + 
            184*Power(t1,2)*t2 - 126*Power(t1,3)*t2 + 
            43*Power(t1,4)*t2 - 10*Power(t1,5)*t2 + 
            Power(s2,5)*(-5 + 5*t1 - 8*t2)*t2 - 252*Power(t2,2) + 
            296*t1*Power(t2,2) + 21*Power(t1,2)*Power(t2,2) - 
            109*Power(t1,3)*Power(t2,2) + 17*Power(t1,4)*Power(t2,2) + 
            136*Power(t2,3) - 156*t1*Power(t2,3) + 
            31*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) - 
            20*Power(t2,4) + 28*t1*Power(t2,4) - 
            9*Power(t1,2)*Power(t2,4) + 
            Power(s1,4)*(20 + 23*Power(s2,3) - 16*t1 - Power(t1,2) + 
               s2*(-60 + 4*Power(t1,2) - t1*(-63 + t2) + 32*t2) - 
               Power(s2,2)*(8 + 35*t1 + 67*t2)) + 
            Power(s2,4)*(-15 + 3*Power(t1,3) - 21*t2 + 4*Power(t2,2) + 
               13*Power(t2,3) - Power(t1,2)*(21 + 11*t2) + 
               t1*(33 + 32*t2 + 3*Power(t2,2))) + 
            Power(s2,3)*(3 - 8*Power(t1,4) - 61*t2 + 3*Power(t2,2) + 
               83*Power(t2,3) + 8*Power(t2,4) + 
               Power(t1,3)*(49 + 18*t2) - 
               Power(t1,2)*(71 + 101*t2 + 22*Power(t2,2)) + 
               t1*(27 + 144*t2 - 13*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s2,2)*(-21 + 6*Power(t1,5) - 89*t2 + 
               190*Power(t2,2) + 110*Power(t2,3) - 24*Power(t2,4) - 
               3*Power(t2,5) - 6*Power(t1,4)*(9 + 2*t2) + 
               Power(t1,3)*(111 + 112*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-105 - 181*t2 + 52*Power(t2,2) + 
                  22*Power(t2,3)) + 
               t1*(63 + 170*t2 - 224*Power(t2,2) - 156*Power(t2,3) - 
                  19*Power(t2,4))) + 
            s2*(32 - Power(t1,6) - Power(t1,5)*(-10 + t2) + 417*t2 + 
               300*Power(t2,2) - 213*Power(t2,3) + 25*Power(t2,4) - 
               4*Power(t2,5) + 
               Power(t1,4)*(27 - 13*t2 + 11*Power(t2,2)) - 
               Power(t1,3)*(147 + 104*t2 + 84*Power(t2,2) + 
                  12*Power(t2,3)) + 
               Power(t1,2)*(210 + 587*t2 + 292*Power(t2,2) + 
                  88*Power(t2,3)) + 
               t1*(-131 - 886*t2 - 443*Power(t2,2) + 60*Power(t2,3) + 
                  3*Power(t2,4) + 3*Power(t2,5))) - 
            Power(s1,3)*(17*Power(s2,4) - 30*Power(t1,3) + 
               t1*(48 - 60*t2) + 60*t2 + Power(t1,2)*(-11 + 8*t2) + 
               Power(s2,3)*(61 - 8*t1 + 81*t2) + 
               4*Power(s2,2)*
                (35 + 4*Power(t1,2) - 15*t2 - 27*Power(t2,2) - 
                  3*t1*(11 + 10*t2)) + 
               s2*(-209 + 14*Power(t1,3) + Power(t1,2)*(32 - 6*t2) - 
                  173*t2 + 44*Power(t2,2) + 4*t1*(21 + 58*t2))) + 
            Power(s1,2)*(-247 - 8*Power(s2,5) - 74*Power(t1,4) + 
               64*t2 + 40*Power(t2,2) + 3*Power(t1,3)*(19 + 8*t2) + 
               Power(s2,4)*(6 + t1 + 47*t2) + 
               t1*(234 + 172*t2 - 44*Power(t2,2)) + 
               Power(t1,2)*(3 - 231*t2 + 10*Power(t2,2)) + 
               Power(s2,3)*(6 - 19*Power(t1,2) + 205*t2 + 
                  101*Power(t2,2) - t1*(19 + 12*t2)) + 
               2*Power(s2,2)*
                (51 + 9*Power(t1,3) + 204*t2 - 60*Power(t2,2) - 
                  41*Power(t2,3) + Power(t1,2)*(13 + 28*t2) - 
                  t1*(61 + 220*t2 + 77*Power(t2,2))) + 
               s2*(246 - 6*Power(t1,4) - 675*t2 - 141*Power(t2,2) + 
                  20*Power(t2,3) + 12*Power(t1,3)*(5 + 2*t2) + 
                  Power(t1,2)*(-39 + 100*t2 - 24*Power(t2,2)) + 
                  t1*(-185 + 316*t2 + 278*Power(t2,2) + 6*Power(t2,3)))) \
+ s1*(85 + 33*Power(t1,5) + 521*t2 - 200*Power(t2,2) + 20*Power(t2,3) + 
               Power(s2,5)*(5 - 5*t1 + 16*t2) + 
               Power(t1,4)*(-23 + 17*t2) - 
               2*Power(t1,3)*(49 - 66*t2 + 29*Power(t2,2)) + 
               Power(t1,2)*(244 - 42*t2 + 189*Power(t2,2) + 
                  8*Power(t2,3)) - 
               t1*(241 + 574*t2 - 32*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s2,4)*(13 + 3*Power(t1,2) - 10*t2 - 
                  43*Power(t2,2) - 4*t1*(4 + t2)) + 
               Power(s2,3)*(-1 + 18*Power(t1,3) - 15*t2 - 
                  227*Power(t2,2) - 51*Power(t2,3) + 4*t1*(4 + 11*t2) + 
                  Power(t1,2)*(-33 + 35*t2)) + 
               Power(s2,2)*(65 - 26*Power(t1,4) - 332*t2 - 
                  378*Power(t2,2) + 92*Power(t2,3) + 28*Power(t2,4) + 
                  2*Power(t1,3)*(46 + t2) - 
                  Power(t1,2)*(137 + 170*t2 + 62*Power(t2,2)) + 
                  t1*(6 + 452*t2 + 464*Power(t2,2) + 88*Power(t2,3))) + 
               s2*(-290 + 11*Power(t1,5) - 574*t2 + 679*Power(t2,2) + 
                  3*Power(t2,3) + 4*Power(t2,4) - 
                  Power(t1,4)*(72 + 19*t2) + 
                  2*Power(t1,3)*(61 + 58*t2 + Power(t2,2)) + 
                  Power(t1,2)*
                   (-281 - 423*t2 - 156*Power(t2,2) + 14*Power(t2,3)) - 
                  2*t1*(-255 - 374*t2 + 146*Power(t2,2) + 
                     56*Power(t2,3) + 4*Power(t2,4))))) + 
         s*(-4*Power(s1,6)*Power(s2,2)*(3 + s2 - 2*t1) + 
            2*Power(s1,5)*s2*(Power(s2,3) + 
               4*(5 - 8*t1 + 3*Power(t1,2)) + 
               Power(s2,2)*(7*t1 + 10*t2) - 
               2*s2*(-6 + 2*t1 + 2*Power(t1,2) - 15*t2 + 10*t1*t2)) + 
            Power(s2,5)*t2*(1 + 3*Power(t1,2) - Power(t1,3) + 
               6*Power(t2,2) + 8*Power(t2,3) - 3*t1*(1 + 2*Power(t2,2))) \
+ Power(s2,4)*(1 + 2*t2 + 32*Power(t2,2) + 47*Power(t2,3) + 
               14*Power(t2,4) - 2*Power(t2,5) + Power(t1,4)*(1 + 2*t2) - 
               Power(t1,3)*(4 + 8*t2 + Power(t2,2)) + 
               Power(t1,2)*(6 + 12*t2 + 34*Power(t2,2) + Power(t2,3)) + 
               t1*(-4 - 8*t2 - 65*Power(t2,2) - 48*Power(t2,3) + 
                  2*Power(t2,4))) + 
            (-1 + t1)*(-3 - 71*t2 + Power(t1,5)*(-4 + t2)*t2 - 
               101*Power(t2,2) + 101*Power(t2,3) - 10*Power(t2,4) + 
               Power(t1,4)*(27 + 24*t2 + Power(t2,2) - 2*Power(t2,3)) + 
               Power(t1,2)*(72 - 73*t2 + 21*Power(t2,2) + 
                  46*Power(t2,3) - 9*Power(t2,4)) + 
               Power(t1,3)*(-78 - 27*t2 - 22*Power(t2,2) + 
                  12*Power(t2,3) + Power(t2,4)) + 
               t1*(-18 + 151*t2 + 100*Power(t2,2) - 161*Power(t2,3) + 
                  18*Power(t2,4))) - 
            Power(s2,3)*(1 - 57*t2 - 16*Power(t2,2) + 39*Power(t2,3) - 
               16*Power(t2,4) + 18*Power(t2,5) + Power(t1,5)*(2 + t2) + 
               Power(t1,4)*(-7 - 7*t2 + Power(t2,2)) + 
               Power(t1,3)*(8 + 72*t2 - 15*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-2 - 184*t2 + 11*Power(t2,2) + 
                  Power(t2,3) + 6*Power(t2,4)) + 
               t1*(-2 + 175*t2 + 19*Power(t2,2) - 32*Power(t2,3) + 
                  18*Power(t2,4) + 4*Power(t2,5))) - 
            s2*(68 + 192*t2 - 95*Power(t2,2) - 166*Power(t2,3) + 
               121*Power(t2,4) - 4*Power(t2,5) + 
               Power(t1,6)*(7 + 2*t2) + 
               Power(t1,5)*(-46 - 19*t2 - 10*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(t1,4)*(182 + 149*t2 + 32*Power(t2,2) - 
                  3*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,2)*(487 + 901*t2 - 149*Power(t2,2) - 
                  41*Power(t2,3) + 37*Power(t2,4) - 6*Power(t2,5)) + 
               Power(t1,3)*(-408 - 545*t2 - Power(t2,2) + 
                  2*Power(t2,3) + 17*Power(t2,4) + Power(t2,5)) + 
               t1*(-290 - 680*t2 + 223*Power(t2,2) + 207*Power(t2,3) - 
                  177*Power(t2,4) + 9*Power(t2,5))) + 
            Power(s2,2)*(26 + Power(t1,6) - 88*t2 - 221*Power(t2,2) + 
               46*Power(t2,3) + 44*Power(t2,4) - 33*Power(t2,5) + 
               2*Power(t1,5)*(-1 + 3*t2 + Power(t2,2)) - 
               Power(t1,4)*(-24 + 42*t2 + 27*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,3)*(-96 + 178*t2 + 94*Power(t2,2) + 
                  4*Power(t2,3) - 3*Power(t2,4)) + 
               Power(t1,2)*(149 - 342*t2 - 336*Power(t2,2) + 
                  48*Power(t2,3) + 30*Power(t2,4) + 5*Power(t2,5)) + 
               t1*(-102 + 288*t2 + 488*Power(t2,2) - 94*Power(t2,3) - 
                  35*Power(t2,4) + 12*Power(t2,5))) + 
            Power(s1,4)*(8*Power(s2,5) + 
               Power(-1 + t1,2)*(-28 + 16*t1 + 3*Power(t1,2)) - 
               2*Power(s2,4)*(-9 + t1 + 5*t2) + 
               Power(s2,3)*(7 + 13*Power(t1,2) - 14*t2 - 
                  40*Power(t2,2) - 4*t1*(7 + 16*t2)) + 
               s2*(-4*Power(t1,4) + 3*Power(t1,3)*(1 + t2) - 
                  6*Power(t1,2)*(9 + 17*t2) - 4*(21 + 40*t2) + 
                  t1*(143 + 259*t2)) + 
               Power(s2,2)*(116 - 11*Power(t1,3) - 133*t2 - 
                  120*Power(t2,2) + Power(t1,2)*(78 + 37*t2) + 
                  t1*(-147 + 48*t2 + 80*Power(t2,2)))) + 
            Power(s1,3)*(Power(s2,5)*(-6 + 6*t1 - 32*t2) - 
               Power(s2,4)*(47 + Power(t1,2) + 68*t2 - 20*Power(t2,2) - 
                  4*t1*(12 + t2)) - 
               (-1 + t1)*(64 - 30*Power(t1,3) + 17*Power(t1,4) + 
                  84*t2 + 60*Power(t1,2)*(1 + t2) - t1*(115 + 144*t2)) + 
               Power(s2,3)*(37 - 37*t2 + 60*Power(t2,2) + 
                  40*Power(t2,3) - Power(t1,2)*(17 + 33*t2) + 
                  2*t1*(-10 + 51*t2 + 56*Power(t2,2))) + 
               s2*(-278 - Power(t1,5) + 378*t2 + 236*Power(t2,2) + 
                  Power(t1,4)*(13 + 7*t2) + 
                  t1*(447 - 606*t2 - 384*Power(t2,2)) - 
                  8*Power(t1,3)*(1 - 3*t2 + Power(t2,2)) + 
                  Power(t1,2)*(-173 + 181*t2 + 156*Power(t2,2))) - 
               2*Power(s2,2)*(19 + 2*Power(t1,4) + 204*t2 - 
                  144*Power(t2,2) - 60*Power(t2,3) - 
                  2*Power(t1,3)*(19 + 10*t2) + 
                  2*Power(t1,2)*(44 + 72*t2 + 17*Power(t2,2)) + 
                  t1*(-71 - 256*t2 + 54*Power(t2,2) + 40*Power(t2,3)))) + 
            Power(s1,2)*(6*Power(s2,5)*t2*(3 - 3*t1 + 8*t2) + 
               Power(s2,4)*(30 + Power(t1,3) + 141*t2 + 
                  96*Power(t2,2) - 20*Power(t2,3) + 
                  Power(t1,2)*(28 + 3*t2) - t1*(59 + 144*t2)) + 
               (-1 + t1)*(-162 + 8*Power(t1,5) + 221*t2 + 
                  74*Power(t2,2) + Power(t1,4)*(7 + 16*t2) + 
                  t1*(196 - 343*t2 - 138*Power(t2,2)) + 
                  Power(t1,3)*(-81 + 16*t2 - 8*Power(t2,2)) + 
                  Power(t1,2)*(32 + 78*t2 + 72*Power(t2,2))) + 
               Power(s2,3)*(41 + 4*Power(t1,4) - 117*t2 + 
                  69*Power(t2,2) - 96*Power(t2,3) - 20*Power(t2,4) + 
                  Power(t1,3)*(-25 + 12*t2) + 
                  Power(t1,2)*(79 + 21*t2 + 21*Power(t2,2)) - 
                  t1*(99 - 84*t2 + 138*Power(t2,2) + 92*Power(t2,3))) + 
               s2*(173 + 3*Power(t1,6) + 738*t2 - 625*Power(t2,2) - 
                  148*Power(t2,3) + 5*Power(t1,4)*(24 + 5*t2) - 
                  Power(t1,5)*(28 + 5*t2) + 
                  Power(t1,2)*
                   (580 + 531*t2 - 237*Power(t2,2) - 96*Power(t2,3)) + 
                  Power(t1,3)*
                   (-335 - 110*t2 - 74*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(-513 - 1179*t2 + 960*Power(t2,2) + 
                     238*Power(t2,3))) + 
               Power(s2,2)*(-204 - 8*Power(t1,5) + 129*t2 + 
                  512*Power(t2,2) - 306*Power(t2,3) - 60*Power(t2,4) + 
                  Power(t1,4)*(60 + 7*t2) - 
                  2*Power(t1,3)*(62 + 82*t2 + 25*Power(t2,2)) + 
                  2*Power(t1,2)*
                   (-52 + 215*t2 + 186*Power(t2,2) + 31*Power(t2,3)) + 
                  2*t1*(190 - 201*t2 - 309*Power(t2,2) + 
                     58*Power(t2,3) + 20*Power(t2,4)))) + 
            s1*(Power(s2,5)*(-1 - 3*Power(t1,2) + Power(t1,3) - 
                  18*Power(t2,2) - 32*Power(t2,3) + 
                  3*t1*(1 + 6*Power(t2,2))) + 
               Power(s2,4)*(1 - 4*Power(t1,3) + Power(t1,4) - 62*t2 - 
                  141*Power(t2,2) - 60*Power(t2,3) + 10*Power(t2,4) + 
                  Power(t1,2)*(6 - 62*t2 - 3*Power(t2,2)) - 
                  4*t1*(1 - 31*t2 - 36*Power(t2,2) + Power(t2,3))) - 
               (-1 + t1)*(-144 - 272*t2 + 258*Power(t2,2) + 
                  8*Power(t2,3) + Power(t1,5)*(6 + 7*t2) - 
                  Power(t1,4)*(10 + 6*t2 + 3*Power(t2,2)) + 
                  t1*(374 + 343*t2 - 389*Power(t2,2) - 20*Power(t2,3)) + 
                  Power(t1,3)*
                   (88 - 40*t2 + 58*Power(t2,2) - 4*Power(t2,3)) + 
                  2*Power(t1,2)*
                   (-157 - 16*t2 + 32*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(146 + Power(t1,6)*(1 - 2*t2) - 272*t2 - 
                  626*Power(t2,2) + 452*Power(t2,3) + 28*Power(t2,4) + 
                  Power(t1,5)*(46 + 8*t2 + 7*Power(t2,2)) - 
                  Power(t1,4)*
                   (147 + 62*t2 + 41*Power(t2,2) + 5*Power(t2,3)) + 
                  Power(t1,3)*
                   (9 + 310*t2 + 120*Power(t2,2) + 64*Power(t2,3)) + 
                  t1*(-435 + 746*t2 + 939*Power(t2,2) - 
                     674*Power(t2,3) - 40*Power(t2,4)) + 
                  Power(t1,2)*
                   (380 - 728*t2 - 399*Power(t2,2) + 147*Power(t2,3) + 
                     12*Power(t2,4))) + 
               Power(s2,3)*(-50 - 4*Power(t1,5) - 52*t2 + 
                  119*Power(t2,2) - 55*Power(t2,3) + 68*Power(t2,4) + 
                  4*Power(t2,5) + 2*Power(t1,4)*(10 + t2) - 
                  2*Power(t1,3)*(-7 + 5*t2 + 10*Power(t2,2)) + 
                  Power(t1,2)*
                   (-122 - 38*t2 - 3*Power(t2,2) + 5*Power(t2,3)) + 
                  2*t1*(71 + 49*t2 - 48*Power(t2,2) + 41*Power(t2,3) + 
                     17*Power(t2,4))) + 
               Power(s2,2)*(66 + 2*Power(t1,6) + 439*t2 - 
                  137*Power(t2,2) - 264*Power(t2,3) + 160*Power(t2,4) + 
                  12*Power(t2,5) - Power(t1,5)*(22 + t2) + 
                  Power(t1,4)*(64 + 9*t2 + Power(t2,2)) + 
                  2*Power(t1,3)*
                   (-73 - 34*t2 + 42*Power(t2,2) + 12*Power(t2,3)) - 
                  2*Power(t1,2)*
                   (-122 - 276*t2 + 151*Power(t2,2) + 96*Power(t2,3) + 
                     14*Power(t2,4)) - 
                  t1*(208 + 931*t2 - 354*Power(t2,2) - 288*Power(t2,3) + 
                     60*Power(t2,4) + 8*Power(t2,5))))))*
       T3q(s2,1 - s + s2 - t1))/
     ((-1 + s1)*Power(-1 + s + t1,2)*(s - s2 + t1)*(-s + s1 - t2)*
       (-1 + s1 + t1 - t2)*Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)) + 
    (8*(-14 + 29*s2 - 38*Power(s2,2) + 7*Power(s2,3) + 
         4*Power(s1,5)*Power(s2,2)*(1 + s2) + 50*t1 - 87*s2*t1 + 
         94*Power(s2,2)*t1 - 17*Power(s2,3)*t1 - 46*Power(t1,2) + 
         83*s2*Power(t1,2) - 74*Power(s2,2)*Power(t1,2) + 
         13*Power(s2,3)*Power(t1,2) - 2*Power(t1,3) - 21*s2*Power(t1,3) + 
         18*Power(s2,2)*Power(t1,3) - 3*Power(s2,3)*Power(t1,3) + 
         12*Power(t1,4) - 4*s2*Power(t1,4) - 35*t2 + 8*s2*t2 + 
         28*Power(s2,2)*t2 - 67*Power(s2,3)*t2 + 17*Power(s2,4)*t2 + 
         Power(s2,5)*t2 + 60*t1*t2 - 67*Power(s2,2)*t1*t2 + 
         97*Power(s2,3)*t1*t2 - 25*Power(s2,4)*t1*t2 - 
         Power(s2,5)*t1*t2 + 11*s2*Power(t1,2)*t2 + 
         34*Power(s2,2)*Power(t1,2)*t2 - 29*Power(s2,3)*Power(t1,2)*t2 + 
         8*Power(s2,4)*Power(t1,2)*t2 - 33*Power(t1,3)*t2 - 
         19*s2*Power(t1,3)*t2 + 5*Power(s2,2)*Power(t1,3)*t2 - 
         Power(s2,3)*Power(t1,3)*t2 + 8*Power(t1,4)*t2 + 12*Power(t2,2) - 
         80*s2*Power(t2,2) + 48*Power(s2,2)*Power(t2,2) - 
         2*Power(s2,3)*Power(t2,2) - 29*Power(s2,4)*Power(t2,2) + 
         10*Power(s2,5)*Power(t2,2) + Power(s2,6)*Power(t2,2) - 
         31*t1*Power(t2,2) + 60*s2*t1*Power(t2,2) - 
         41*Power(s2,2)*t1*Power(t2,2) - 10*Power(s2,3)*t1*Power(t2,2) + 
         12*Power(s2,4)*t1*Power(t2,2) - 6*Power(s2,5)*t1*Power(t2,2) + 
         27*Power(t1,2)*Power(t2,2) + 44*s2*Power(t1,2)*Power(t2,2) + 
         5*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         2*Power(s2,4)*Power(t1,2)*Power(t2,2) - 
         7*Power(t1,3)*Power(t2,2) - 18*s2*Power(t1,3)*Power(t2,2) + 
         Power(s2,2)*Power(t1,3)*Power(t2,2) + 3*Power(t2,3) + 
         19*s2*Power(t2,3) - 52*Power(s2,2)*Power(t2,3) + 
         33*Power(s2,3)*Power(t2,3) - 4*Power(s2,4)*Power(t2,3) + 
         Power(s2,6)*Power(t2,3) - 2*t1*Power(t2,3) - 
         38*s2*t1*Power(t2,3) + Power(s2,2)*t1*Power(t2,3) - 
         5*Power(s2,3)*t1*Power(t2,3) - 3*Power(s2,4)*t1*Power(t2,3) - 
         Power(s2,5)*t1*Power(t2,3) - 2*Power(t1,2)*Power(t2,3) + 
         18*s2*Power(t1,2)*Power(t2,3) + 
         10*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         2*Power(s2,3)*Power(t1,2)*Power(t2,3) - 2*Power(t2,4) + 
         5*s2*Power(t2,4) + 10*Power(s2,2)*Power(t2,4) - 
         11*Power(s2,3)*Power(t2,4) + 6*Power(s2,4)*Power(t2,4) + 
         t1*Power(t2,4) + s2*t1*Power(t2,4) - 
         12*Power(s2,2)*t1*Power(t2,4) + Power(s2,3)*t1*Power(t2,4) + 
         Power(s2,4)*t1*Power(t2,4) - s2*Power(t2,5) + 
         Power(s2,2)*Power(t2,5) + Power(s2,3)*Power(t2,5) - 
         Power(s2,4)*Power(t2,5) + 
         Power(s1,4)*(Power(s2,5) - Power(t1,2) + 
            s2*(-8 + 3*Power(t1,2) - t1*(-10 + t2)) + 
            Power(s2,2)*(-59 + 5*Power(t1,2) + t1*(4 - 6*t2) - 3*t2) + 
            Power(s2,4)*(1 - 2*t1 + t2) + 
            Power(s2,3)*(17 + Power(t1,2) - 14*t2 - t1*(4 + t2))) + 
         Power(s,4)*Power(-1 + s2,2)*
          (6 + 4*t1 - 3*Power(t1,2) + Power(s2,2)*(-2 + t1 - t2) - 
            4*t2 + 6*t1*t2 - 3*Power(t2,2) - 
            s2*(4 - 3*t1 + Power(t1,2) + 3*t2 - 2*t1*t2 + Power(t2,2)) + 
            s1*(-6 + 2*Power(s2,2) - 7*t1 + 7*t2 + s2*(4 - t1 + t2))) - 
         Power(s,3)*(-1 + s2)*
          (-30 + 11*t1 + 23*Power(t1,2) - 2*Power(t1,3) + 
            Power(s2,4)*(-4 + 2*t1 - 3*t2) + 9*t2 - 26*t1*t2 - 
            Power(t1,2)*t2 + 3*Power(t2,2) + 8*t1*Power(t2,2) - 
            5*Power(t2,3) - Power(s2,3)*
             (3 + 3*Power(t1,2) + 5*t2 + 3*Power(t2,2) - t1*(7 + 6*t2)) \
+ Power(s1,2)*(2 + 5*Power(s2,3) + 6*t1 - 7*t2 + 
               Power(s2,2)*(16 - 3*t1 + 2*t2) + s2*(-23 - 19*t1 + 21*t2)\
) + Power(s2,2)*(12 + Power(t1,3) + 25*t2 + 3*Power(t2,2) - 
               Power(t1,2)*(1 + 2*t2) + t1*(-6 - 2*t2 + Power(t2,2))) + 
            s1*(25 + 5*Power(s2,4) - t1 - 12*Power(t1,2) - 15*t2 + 
               12*Power(t2,2) - Power(s2,3)*(2 + t1 + t2) + 
               s2*(6 + 27*t1 - 2*Power(t1,2) + 3*t2 + 28*t1*t2 - 
                  26*Power(t2,2)) - 
               Power(s2,2)*(34 - 7*t1 + 2*Power(t1,2) + 19*t2 - 
                  4*t1*t2 + 2*Power(t2,2))) + 
            s2*(25 + Power(t1,3) + 3*Power(t1,2)*(-1 + t2) - 10*t2 + 
               13*Power(t2,2) + 5*Power(t2,3) - 
               t1*(30 + 10*t2 + 9*Power(t2,2)))) + 
         Power(s1,3)*(4 + Power(s2,6) + 3*Power(t1,3) + 
            Power(t1,2)*(4 + t2) - Power(s2,5)*(t1 + t2) + 
            t1*(-10 + 3*t2) - 
            Power(s2,4)*(-29 + Power(t1,2) + t1*(9 - 4*t2) + 10*t2 + 
               2*Power(t2,2)) + 
            s2*(106 + 11*Power(t1,3) - 4*t2 + Power(t2,2) - 
               Power(t1,2)*(16 + 21*t2) + 
               t1*(-102 + 4*t2 + 3*Power(t2,2))) + 
            Power(s2,3)*(-68 + Power(t1,3) - 41*t2 + 17*Power(t2,2) - 
               Power(t1,2)*(2 + 3*t2) + t1*(47 + 10*t2 + 3*Power(t2,2))) \
+ Power(s2,2)*(Power(t1,3) - Power(t1,2)*(25 + 17*t2) + 
               8*(5 + 19*t2 - 2*Power(t2,2)) + 
               t1*(11 + 19*t2 + 18*Power(t2,2)))) + 
         Power(s1,2)*(-51 + 8*Power(t1,4) + Power(s2,6)*(t1 - t2) + 
            7*t2 - 2*Power(t2,2) - 4*Power(t1,3)*(5 + 3*t2) + 
            t1*(98 - 12*t2 - 5*Power(t2,2)) + 
            Power(s2,5)*(5 + t1 - 2*Power(t1,2) + t1*t2 - Power(t2,2)) + 
            Power(t1,2)*(-34 + 14*t2 + Power(t2,2)) + 
            Power(s2,4)*(-38 + Power(t1,3) + Power(t1,2)*(-7 + t2) - 
               65*t2 + 23*Power(t2,2) + t1*(29 + 19*t2 - Power(t2,2))) + 
            s2*(-131 - 168*t2 + 37*Power(t2,2) - 3*Power(t2,3) - 
               Power(t1,3)*(38 + 25*t2) + 
               Power(t1,2)*(8 + 83*t2 + 33*Power(t2,2)) + 
               t1*(167 + 111*t2 - 37*Power(t2,2) - 3*Power(t2,3))) - 
            Power(s2,3)*(32 - 168*t2 - 20*Power(t2,2) + 7*Power(t2,3) + 
               Power(t1,3)*(2 + t2) + 
               Power(t1,2)*(2 + 3*t2 - 3*Power(t2,2)) + 
               t1*(-26 + 94*t2 + 7*Power(t2,2) + 3*Power(t2,3))) + 
            Power(s2,2)*(119 + Power(t1,3)*(3 - 2*t2) - 133*t2 - 
               117*Power(t2,2) + 26*Power(t2,3) + 
               Power(t1,2)*(69 + 57*t2 + 19*Power(t2,2)) - 
               t1*(178 + 17*t2 + 62*Power(t2,2) + 18*Power(t2,3)))) + 
         s1*(74 + 30*t2 - 14*Power(t2,2) + 4*Power(t2,3) - 
            Power(s2,6)*t2*(1 + t1 + t2) - 4*Power(t1,4)*(5 + 2*t2) + 
            Power(t1,3)*(21 + 37*t2 + 9*Power(t2,2)) - 
            Power(t1,2)*(-99 + 22*t2 + 16*Power(t2,2) + Power(t2,3)) + 
            t1*(-174 - 39*t2 + 24*Power(t2,2) + Power(t2,3)) + 
            Power(s2,5)*(Power(t1,2)*(1 + t2) + 
               t2*(-16 + Power(t2,2)) + t1*(-1 + 7*t2 + Power(t2,2))) - 
            Power(s2,4)*(13 + Power(t1,3) + Power(t1,2)*(2 - 3*t2) - 
               67*t2 - 40*Power(t2,2) + 20*Power(t2,3) - 
               2*Power(t2,4) + 
               t1*(-16 + 40*t2 + 7*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s2,3)*(76 - 6*Power(t1,3) + 38*t2 - 133*Power(t2,2) + 
               15*Power(t2,3) - Power(t2,4) + 
               Power(t1,2)*(52 + 8*t2 + 7*Power(t2,2) - Power(t2,3)) + 
               t1*(-122 - 26*t2 + 52*Power(t2,2) + Power(t2,4))) + 
            s2*(-38 + 4*Power(t1,4) + 208*t2 + 43*Power(t2,2) - 
               30*Power(t2,3) + 3*Power(t2,4) + 
               2*Power(t1,3)*(17 + 29*t2 + 7*Power(t2,2)) - 
               Power(t1,2)*(83 + 59*t2 + 85*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(83 - 219*t2 + 29*Power(t2,2) + 22*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s2,2)*(-27 - 166*t2 + Power(t1,3)*(-7 + t2)*t2 + 
               145*Power(t2,2) + 14*Power(t2,3) - 12*Power(t2,4) - 
               Power(t1,2)*(43 + 67*t2 + 42*Power(t2,2) + 
                  7*Power(t2,3)) + 
               t1*(70 + 214*t2 + 5*Power(t2,2) + 51*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         Power(s,2)*(16 - 47*t1 + 13*Power(t1,2) + 24*Power(t1,3) + 
            Power(s2,6)*(-2 + t1 - 3*t2) - 43*t2 - 3*t1*t2 + 
            2*Power(t1,2)*t2 - 3*Power(t1,3)*t2 + 14*Power(t2,2) - 
            40*t1*Power(t2,2) + 4*Power(t1,2)*Power(t2,2) + 
            14*Power(t2,3) + t1*Power(t2,3) - 2*Power(t2,4) - 
            Power(s2,5)*(4 + 2*Power(t1,2) + t2 + 3*Power(t2,2) - 
               t1*(7 + 6*t2)) + 
            Power(s1,3)*(4 + 4*Power(s2,4) + t1 + 
               s2*(1 + 11*t1 - 15*t2) + Power(s2,3)*(25 - 3*t1 + t2) + 
               Power(s2,2)*(-34 - 17*t1 + 22*t2)) + 
            s2*(43 + 5*Power(t1,3)*(-3 + t2) + 166*t2 - 
               25*Power(t2,2) - 9*Power(t2,3) + 6*Power(t2,4) - 
               Power(t1,2)*(52 + 53*t2 + 4*Power(t2,2)) + 
               t1*(13 - 7*t2 + 77*Power(t2,2) - 7*Power(t2,3))) + 
            Power(s2,4)*(Power(t1,3) - Power(t1,2)*(7 + 2*t2) - 
               t1*(2 + 4*t2 + Power(t2,2)) + 
               2*(2 + 10*t2 + 8*Power(t2,2) + Power(t2,3))) - 
            Power(s2,3)*(-63 - 39*t2 + 28*Power(t2,2) + Power(t2,3) - 
               2*Power(t2,4) + Power(t1,3)*(1 + t2) - 
               Power(t1,2)*(34 + 3*t2 + 4*Power(t2,2)) + 
               t1*(86 + 55*t2 + Power(t2,2) + 5*Power(t2,3))) - 
            Power(s2,2)*(Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(2 - 26*t2 + 4*Power(t2,2)) - 
               t1*(122 + 95*t2 - 11*Power(t2,2) + 11*Power(t2,3)) + 
               2*(60 + 93*t2 - 5*Power(t2,2) + 7*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(s1,2)*(-57 + 9*Power(s2,5) + 8*Power(t1,2) + 22*t2 - 
               2*Power(t2,2) + Power(s2,4)*(3 - 7*t1 + t2) - 
               t1*(26 + 11*t2) + 
               Power(s2,3)*(-41 - 51*t2 + t1*(8 + t2)) + 
               s2*(178 - 32*Power(t1,2) - 45*t2 + 36*Power(t2,2) + 
                  t1*(-2 + 7*t2)) + 
               Power(s2,2)*(-92 + 8*Power(t1,2) + 49*t2 - 
                  50*Power(t2,2) + t1*(51 + 35*t2))) + 
            s1*(46 + 4*Power(s2,6) - 17*Power(t1,3) + 
               Power(s2,5)*(-8 + t1 - 5*t2) + 4*Power(t1,2)*(-6 + t2) + 
               42*t2 - 40*Power(t2,2) + 4*Power(t2,3) + 
               t1*(35 + 52*t2 + 9*Power(t2,2)) - 
               Power(s2,4)*(10 + 7*Power(t1,2) + 26*t2 + 
                  7*Power(t2,2) - 2*t1*(5 + 6*t2)) + 
               s2*(-253 + 5*Power(t1,3) - 128*t2 + 53*Power(t2,2) - 
                  27*Power(t2,3) + Power(t1,2)*(93 + 33*t2) + 
                  t1*(58 - 100*t2 - 11*Power(t2,2))) + 
               Power(s2,3)*(-61 + 3*Power(t1,3) + 
                  Power(t1,2)*(3 - 7*t2) + 71*t2 + 27*Power(t2,2) - 
                  3*Power(t2,3) + t1*(57 - 4*t2 + 7*Power(t2,2))) + 
               Power(s2,2)*(282 + Power(t1,3) + 70*t2 - Power(t2,2) + 
                  34*Power(t2,3) - 3*Power(t1,2)*(11 + 2*t2) - 
                  t1*(185 + 24*t2 + 29*Power(t2,2))))) - 
         s*(-21 + 28*t1 + 21*Power(t1,2) - 20*Power(t1,3) - 
            8*Power(t1,4) - 70*t2 - Power(s2,7)*t2 + 42*t1*t2 + 
            51*Power(t1,2)*t2 - 20*Power(t1,3)*t2 + 27*Power(t2,2) - 
            55*t1*Power(t2,2) + 32*Power(t1,2)*Power(t2,2) + 
            Power(t1,3)*Power(t2,2) + 12*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) - 4*Power(t2,4) + t1*Power(t2,4) + 
            Power(s2,6)*(-1 + t1 + 2*t1*t2 - Power(t2,2)) + 
            Power(s1,4)*s2*(-8 + Power(s2,3) - Power(s2,2)*(-18 + t1) - 
               t1 + s2*(-3 - 6*t1 + 8*t2)) - 
            Power(s2,5)*(-3 - t2 - 6*Power(t2,2) - 3*Power(t2,3) + 
               Power(t1,2)*(2 + t2) + t1*(1 + Power(t2,2))) + 
            Power(s2,4)*(1 + Power(t1,3) + 55*t2 + 6*Power(t2,2) - 
               13*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(5 + 8*t2 + 2*Power(t2,2)) - 
               t1*(7 + 46*t2 + 7*Power(t2,2) + 4*Power(t2,3))) + 
            s2*(101 + 146*t2 - 97*Power(t2,2) + 13*Power(t2,3) + 
               10*Power(t2,4) - Power(t2,5) + 
               Power(t1,3)*(35 + 38*t2 - 2*Power(t2,2)) + 
               Power(t1,2)*(56 - 16*t2 - 6*Power(t2,2) + 
                  3*Power(t2,3)) - 
               2*t1*(96 + 76*t2 - 10*Power(t2,2) + 21*Power(t2,3))) + 
            Power(s2,2)*(-123 - 23*t2 + 160*Power(t2,2) - 
               23*Power(t2,3) + 2*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,3)*(9 + Power(t2,2)) - 
               Power(t1,2)*(126 + 57*t2 + 34*Power(t2,2)) + 
               t1*(240 + 58*t2 - 6*Power(t2,2) + 32*Power(t2,3) - 
                  3*Power(t2,4))) - 
            Power(s2,3)*(-48 + 92*t2 + 85*Power(t2,2) - 24*Power(t2,3) + 
               2*Power(t2,4) + Power(t2,5) + Power(t1,3)*(9 + 2*t2) + 
               Power(t1,2)*(-46 + t2 - 6*Power(t2,2) + Power(t2,3)) + 
               t1*(85 - 80*t2 - 33*Power(t2,2) + 2*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s1,3)*(8 + 5*Power(s2,5) - 8*Power(t1,2) + 
               Power(s2,4)*(8 - 7*t1 + 2*t2) + t1*(-11 + 3*t2) + 
               Power(s2,3)*(13 + 2*Power(t1,2) - 48*t2 + Power(t2,2) - 
                  t1*(3 + 2*t2)) + 
               Power(s2,2)*(-174 + 12*Power(t1,2) + 24*t2 - 
                  26*Power(t2,2) + t1*(34 + 7*t2)) + 
               s2*(108 - 14*Power(t1,2) - 18*t2 + Power(t2,2) + 
                  3*t1*(9 + 8*t2))) + 
            Power(s1,2)*(-105 + 4*Power(s2,6) - 16*Power(t1,3) + 22*t2 - 
               4*Power(t2,2) - Power(s2,5)*(2*t1 + 3*t2) + 
               Power(t1,2)*(39 + 28*t2) + 
               t1*(87 - 18*t2 - 5*Power(t2,2)) - 
               Power(s2,4)*(-24 + 5*Power(t1,2) + t1*(4 - 10*t2) + 
                  35*t2 + 5*Power(t2,2)) + 
               Power(s2,2)*(287 + 2*Power(t1,3) + 291*t2 - 
                  37*Power(t2,2) + 30*Power(t2,3) - 
                  28*Power(t1,2)*(2 + t2) + 
                  t1*(-131 + 8*t2 + Power(t2,2))) + 
               Power(s2,3)*(-182 + 3*Power(t1,3) - 9*t2 + 
                  40*Power(t2,2) - 3*Power(t2,3) - 
                  Power(t1,2)*(1 + 8*t2) + 
                  t1*(124 + 12*t2 + 9*Power(t2,2))) + 
               s2*(20 + 19*Power(t1,3) - 194*t2 + 70*Power(t2,2) - 
                  3*Power(t2,3) + Power(t1,2)*(39 + 16*t2) - 
                  t1*(146 + 92*t2 + 45*Power(t2,2)))) + 
            s1*(136 + Power(s2,7) + 8*Power(t1,4) + 62*t2 - 
               42*Power(t2,2) + 8*Power(t2,3) + Power(t1,3)*(40 + 9*t2) - 
               Power(t1,2)*(33 + 77*t2 + 18*Power(t2,2)) + 
               t1*(-154 - 4*t2 + 29*Power(t2,2) + Power(t2,3)) + 
               Power(s2,6)*(t1 - 3*(1 + t2)) - 
               Power(s2,5)*(5 + 4*Power(t1,2) + 8*t2 + 5*Power(t2,2) - 
                  t1*(9 + 5*t2)) + 
               Power(s2,4)*(-48 + 2*Power(t1,3) - 34*t2 + 
                  40*Power(t2,2) - Power(t1,2)*(11 + t2) + 
                  t1*(40 + 19*t2 + Power(t2,2))) + 
               Power(s2,2)*(73 + Power(t1,3)*(2 - 3*t2) - 443*t2 - 
                  94*Power(t2,2) + 14*Power(t2,3) - 14*Power(t2,4) + 
                  2*Power(t1,2)*(45 + 48*t2 + 8*Power(t2,2)) + 
                  t1*(-143 + 127*t2 - 74*Power(t2,2) + Power(t2,3))) - 
               Power(s2,3)*(-94 - 276*t2 + 28*Power(t2,2) + 
                  8*Power(t2,3) - 3*Power(t2,4) + 
                  Power(t1,3)*(3 + 2*t2) + 
                  Power(t1,2)*(-31 + 4*t2 - 7*Power(t2,2)) + 
                  t1*(107 + 168*t2 + 7*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(-280 + 94*t2 + 73*Power(t2,2) - 54*Power(t2,3) + 
                  3*Power(t2,4) - 5*Power(t1,3)*(13 + 4*t2) - 
                  Power(t1,2)*(81 + 6*t2 + 5*Power(t2,2)) + 
                  t1*(410 + 85*t2 + 107*Power(t2,2) + 22*Power(t2,3))))))*
       T4q(-1 + s2))/
     ((-1 + s1)*(-1 + s2)*(-s + s2 - t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)) + 
    (8*(14 - 17*s2 - 40*t1 + 54*s2*t1 + 36*Power(t1,2) - 
         60*s2*Power(t1,2) - 8*Power(t1,3) + 26*s2*Power(t1,3) - 
         2*Power(t1,4) - 3*s2*Power(t1,4) + 17*t2 + 3*s2*t2 - 
         41*Power(s2,2)*t2 - Power(s2,3)*t2 - 37*t1*t2 + 13*s2*t1*t2 + 
         90*Power(s2,2)*t1*t2 + 2*Power(s2,3)*t1*t2 + 18*Power(t1,2)*t2 - 
         36*s2*Power(t1,2)*t2 - 57*Power(s2,2)*Power(t1,2)*t2 - 
         Power(s2,3)*Power(t1,2)*t2 + 7*Power(t1,3)*t2 + 
         21*s2*Power(t1,3)*t2 + 8*Power(s2,2)*Power(t1,3)*t2 - 
         5*Power(t1,4)*t2 - s2*Power(t1,4)*t2 - 9*Power(t2,2) + 
         20*s2*Power(t2,2) - 23*Power(s2,2)*Power(t2,2) - 
         27*Power(s2,3)*Power(t2,2) - Power(s2,4)*Power(t2,2) + 
         19*t1*Power(t2,2) - 14*s2*t1*Power(t2,2) + 
         49*Power(s2,2)*t1*Power(t2,2) + 33*Power(s2,3)*t1*Power(t2,2) + 
         Power(s2,4)*t1*Power(t2,2) - 18*Power(t1,2)*Power(t2,2) - 
         17*s2*Power(t1,2)*Power(t2,2) - 
         28*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         6*Power(s2,3)*Power(t1,2)*Power(t2,2) + 
         7*Power(t1,3)*Power(t2,2) + 11*s2*Power(t1,3)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) + Power(t1,4)*Power(t2,2) - 
         7*Power(t2,3) - 17*s2*Power(t2,3) + 5*Power(s2,2)*Power(t2,3) - 
         14*Power(s2,3)*Power(t2,3) - 4*Power(s2,4)*Power(t2,3) + 
         12*t1*Power(t2,3) + 29*s2*t1*Power(t2,3) + 
         10*Power(s2,2)*t1*Power(t2,3) + 11*Power(s2,3)*t1*Power(t2,3) + 
         Power(s2,4)*t1*Power(t2,3) - 3*Power(t1,2)*Power(t2,3) - 
         18*s2*Power(t1,2)*Power(t2,3) - 
         7*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         Power(s2,3)*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) - 
         2*s2*Power(t1,3)*Power(t2,3) - Power(t2,4) - 10*s2*Power(t2,4) - 
         9*Power(s2,2)*Power(t2,4) - Power(s2,4)*Power(t2,4) - 
         t1*Power(t2,4) + 8*s2*t1*Power(t2,4) + 
         10*Power(s2,2)*t1*Power(t2,4) + Power(s2,3)*t1*Power(t2,4) + 
         3*Power(t1,2)*Power(t2,4) + 5*s2*Power(t1,2)*Power(t2,4) + 
         Power(s2,2)*Power(t1,2)*Power(t2,4) + 2*Power(t2,5) - 
         5*Power(s2,2)*Power(t2,5) - t1*Power(t2,5) - 
         4*s2*t1*Power(t2,5) - 2*Power(s2,2)*t1*Power(t2,5) + 
         s2*Power(t2,6) + Power(s2,2)*Power(t2,6) + 
         Power(s1,5)*(Power(s2,3) - Power(t1,2) + s2*t1*(2 + t1 - t2) + 
            Power(s2,2)*(1 - 2*t1 + t2)) + 
         Power(s,4)*(2 + 6*t1 - Power(t1,2) - 3*Power(t1,3) - 6*t2 + 
            2*t1*t2 + 9*Power(t1,2)*t2 - Power(t2,2) - 9*t1*Power(t2,2) + 
            3*Power(t2,3) + Power(s1,2)*(-1 + s2)*(-2 + 2*s2 - t1 + t2) + 
            Power(s2,2)*(2 + t1 + Power(t1,2) - t2 - 2*t1*t2 + 
               Power(t2,2)) + 
            s1*(-4 - 3*t1 - 2*Power(t1,2) + 
               Power(s2,2)*(-4 + 3*t1 - 3*t2) + 3*t2 + 4*t1*t2 - 
               2*Power(t2,2) - 
               2*s2*(-4 + Power(t1,2) - 2*t1*t2 + Power(t2,2))) + 
            s2*(-4 - Power(t1,3) + 7*t2 + 4*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(4 + 3*t2) - t1*(7 + 8*t2 + 3*Power(t2,2)))) + 
         Power(s1,4)*(Power(s2,4) - Power(s2,3)*(7 + 2*t2) + 
            t1*(-2 + t1 + 2*Power(t1,2) + 3*t2 + 2*t1*t2) + 
            Power(s2,2)*(17 - 3*Power(t1,2) - 12*t2 - 3*Power(t2,2) + 
               t1*(4 + 7*t2)) + 
            s2*(-2 + 2*Power(t1,3) - 4*t2 + Power(t2,2) - 
               Power(t1,2)*(7 + 5*t2) + t1*(8 - 7*t2 + 4*Power(t2,2)))) + 
         Power(s1,3)*(1 + 3*Power(t1,4) + Power(s2,4)*(1 + 2*t1 - 2*t2) - 
            4*Power(t1,2)*(-3 + t2) + 3*t2 - 2*Power(t2,2) - 
            3*Power(t1,3)*(3 + 2*t2) + 
            Power(s2,3)*(13 - 3*Power(t1,2) + t1*(-6 + t2) + 19*t2) + 
            t1*(-6 + 3*t2 - 8*Power(t2,2)) + 
            Power(s2,2)*(-14 - 46*t2 + 35*Power(t2,2) + 2*Power(t2,3) + 
               Power(t1,2)*(-7 + 6*t2) + t1*(13 - 16*t2 - 7*Power(t2,2))) \
+ s2*(-37 + Power(t1,4) + 20*t2 + 12*Power(t2,2) - 4*Power(t2,3) - 
               Power(t1,3)*(9 + 5*t2) + 
               3*Power(t1,2)*(8 + 4*t2 + 3*Power(t2,2)) + 
               t1*(29 - 33*t2 + 13*Power(t2,2) - 6*Power(t2,3)))) + 
         Power(s1,2)*(19 - 8*t2 - 7*Power(t2,2) + 6*Power(t2,3) - 
            2*Power(t1,4)*(3 + 2*t2) + 
            Power(t1,3)*(25 + 5*t2 + 6*Power(t2,2)) - 
            Power(t1,2)*(5 + 12*t2 - 8*Power(t2,2) + 2*Power(t2,3)) + 
            t1*(-33 + 16*t2 - Power(t2,2) + 6*Power(t2,3)) + 
            Power(s2,4)*(Power(t1,2) - 6*t2 - t1*(1 + 3*t2)) + 
            Power(s2,3)*(-19 - 2*Power(t1,3) - 41*t2 - 17*Power(t2,2) + 
               2*Power(t2,3) + Power(t1,2)*(6 + 4*t2) + 
               t1*(15 + 25*t2 - Power(t2,2))) + 
            Power(s2,2)*(-15 - 13*Power(t1,3) + Power(t1,4) + 34*t2 + 
               32*Power(t2,2) - 43*Power(t2,3) + 2*Power(t2,4) + 
               Power(t1,2)*(7 + 8*t2 - 2*Power(t2,2)) - 
               t1*(-20 + 18*t2 - 30*Power(t2,2) + Power(t2,3))) + 
            s2*(49 + 61*t2 - Power(t1,4)*t2 - 44*Power(t2,2) - 
               12*Power(t2,3) + 6*Power(t2,4) + 
               Power(t1,3)*(2 + 8*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(30 - 49*t2 + 2*Power(t2,2) - 7*Power(t2,3)) + 
               t1*(-81 - 43*t2 + 50*Power(t2,2) - 17*Power(t2,3) + 
                  4*Power(t2,4)))) + 
         s1*(-28 - 13*t2 + 14*Power(t2,2) + 5*Power(t2,3) - 
            6*Power(t2,4) + Power(t1,4)*(7 + t2 + Power(t2,2)) + 
            Power(s2,4)*t2*(1 - Power(t1,2) + 9*t2 + 2*Power(t2,2)) + 
            t1*(68 + 27*t2 - 22*Power(t2,2) + Power(t2,3)) - 
            Power(t1,3)*(2 + 17*t2 - 7*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(-45 + 2*t2 + 3*Power(t2,2) - 8*Power(t2,3) + 
               Power(t2,4)) + 
            Power(s2,3)*(Power(t1,3)*(1 + t2) + 
               Power(t1,2)*(-2 + 3*t2) + 
               t2*(47 + 42*t2 + 5*Power(t2,2) - Power(t2,3)) - 
               t1*(-1 + 51*t2 + 30*Power(t2,2) + Power(t2,3))) + 
            Power(s2,2)*(35 - Power(t1,4) + 40*t2 - 25*Power(t2,2) + 
               6*Power(t2,3) + 24*Power(t2,4) - 3*Power(t2,5) + 
               Power(t1,3)*(1 + 6*t2) + 
               Power(t1,2)*(36 + 30*t2 + 6*Power(t2,2) - 
                  2*Power(t2,3)) + 
               t1*(-71 - 76*t2 - 5*Power(t2,2) - 28*Power(t2,3) + 
                  5*Power(t2,4))) - 
            s2*(13 + 8*Power(t1,4) + 70*t2 + 7*Power(t2,2) - 
               36*Power(t2,3) - 4*Power(t2,4) + 4*Power(t2,5) + 
               Power(t1,3)*(-16 + 12*t2 - 3*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(21 + 16*t2 - 43*Power(t2,2) + 
                  12*Power(t2,3) - 2*Power(t2,4)) + 
               t1*(-26 - 98*t2 + 15*Power(t2,2) + 33*Power(t2,3) - 
                  13*Power(t2,4) + Power(t2,5)))) + 
         Power(s,3)*(6 + 3*t1 + 8*Power(t1,2) + Power(t1,3) - 
            2*Power(t1,4) + t2 - 4*t1*t2 - 8*Power(t1,2)*t2 + 
            Power(t1,3)*t2 - 4*Power(t2,2) + 13*t1*Power(t2,2) + 
            9*Power(t1,2)*Power(t2,2) - 6*Power(t2,3) - 
            13*t1*Power(t2,3) + 5*Power(t2,4) + 
            Power(s1,3)*(-2 - 5*Power(s2,2) - 2*t1 + 
               s2*(7 + 3*t1 - 2*t2) + t2) - 
            Power(s2,3)*(8 + 2*Power(t1,2) + t2 + 3*Power(t2,2) - 
               t1*(4 + 5*t2)) + 
            Power(s2,2)*(21 + 3*Power(t1,3) - 29*t2 - 21*Power(t2,2) - 
               3*Power(t2,3) - Power(t1,2)*(10 + 9*t2) + 
               t1*(14 + 31*t2 + 9*Power(t2,2))) - 
            s2*(19 + Power(t1,4) - 26*t2 - 11*Power(t2,2) - 
               Power(t1,3)*(5 + 3*t2) + 
               Power(t1,2)*(5 + 10*t2 + 3*Power(t2,2)) - 
               t1*(-18 - 6*t2 + 5*Power(t2,2) + Power(t2,3))) - 
            Power(s1,2)*(1 + 5*Power(s2,3) - 10*Power(t1,2) + 
               2*Power(s2,2)*(-7 + 2*t1 - 3*t2) + 4*t2 - 3*Power(t2,2) + 
               t1*(-21 + 13*t2) + 
               s2*(8 - 5*Power(t1,2) + 3*t2 - 4*Power(t2,2) + 
                  t1*(16 + 9*t2))) + 
            s1*(1 + 10*Power(t1,3) + Power(t1,2)*(8 - 29*t2) + 3*t2 + 
               12*Power(t2,2) - 9*Power(t2,3) + 
               Power(s2,3)*(9 - 7*t1 + 8*t2) + 
               t1*(-27 - 20*t2 + 28*Power(t2,2)) + 
               Power(s2,2)*(4*Power(t1,2) - 3*t1*(3 + 2*t2) + 
                  2*(-9 + 5*t2 + Power(t2,2))) + 
               s2*(Power(t1,3) - 2*Power(t1,2)*(9 + 2*t2) + 
                  t1*(38 + 22*t2 + 5*Power(t2,2)) - 
                  2*(-4 + 8*t2 + 2*Power(t2,2) + Power(t2,3))))) + 
         Power(s,2)*(8 - 13*t1 + 5*Power(t1,3) + 47*t2 - 5*t1*t2 - 
            14*Power(t1,2)*t2 + 5*Power(t1,3)*t2 - 3*Power(t1,4)*t2 + 
            5*Power(t2,2) + 13*t1*Power(t2,2) - 
            22*Power(t1,2)*Power(t2,2) + 7*Power(t1,3)*Power(t2,2) - 
            4*Power(t2,3) + 29*t1*Power(t2,3) - 
            3*Power(t1,2)*Power(t2,3) - 12*Power(t2,4) - 
            3*t1*Power(t2,4) + 2*Power(t2,5) + 
            Power(s1,4)*(4*Power(s2,2) + t1 + s2*(-3 - 3*t1 + t2)) + 
            Power(s2,4)*(2 + Power(t1,2) + 3*t2 + 3*Power(t2,2) - 
               t1*(3 + 4*t2)) + 
            Power(s2,3)*(-2 - 2*Power(t1,3) + 11*t2 + 24*Power(t2,2) + 
               3*Power(t2,3) + Power(t1,2)*(11 + 8*t2) - 
               t1*(7 + 24*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(12 + Power(t1,4) + 53*t2 - 43*Power(t2,2) - 
               20*Power(t2,3) - 2*Power(t2,4) - 3*Power(t1,3)*(4 + t2) + 
               Power(t1,2)*(16 + 25*t2 + Power(t2,2)) + 
               t1*(-17 - 28*t2 + 7*Power(t2,2) + 3*Power(t2,3))) - 
            s2*(19 + Power(t1,4)*(-1 + t2) + 125*t2 + 9*Power(t2,2) + 
               6*Power(t2,3) + 5*Power(t2,4) + 2*Power(t2,5) + 
               Power(t1,3)*(-13 + t2 - 5*Power(t2,2)) + 
               Power(t1,2)*(41 + 42*t2 + 6*Power(t2,2) + 
                  9*Power(t2,3)) - 
               t1*(46 + 98*t2 + 35*Power(t2,2) + 11*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(s1,3)*(3 + 9*Power(s2,3) - 15*Power(t1,2) + 2*t2 - 
               2*Power(t2,2) - 3*Power(s2,2)*(3 + t1 + t2) + 
               3*t1*(-7 + 4*t2) + 
               s2*(-7 - 3*Power(t1,2) + t2 - Power(t2,2) + 
                  t1*(32 + 5*t2))) + 
            Power(s1,2)*(21 + 4*Power(s2,4) - 9*Power(t1,3) + 
               Power(s2,3)*(-17 + 10*t1 - 14*t2) + 7*t2 - 
               16*Power(t2,2) + 6*Power(t2,3) + 
               3*Power(t1,2)*(4 + 11*t2) + 
               t1*(6 + 45*t2 - 30*Power(t2,2)) + 
               Power(s2,2)*(25 - 14*Power(t1,2) - 22*t2 - 
                  8*Power(t2,2) + 10*t1*(3 + 2*t2)) + 
               s2*(-27 + 3*Power(t1,3) + Power(t1,2)*(17 - 6*t2) + 
                  15*t2 + 2*Power(t2,2) - 3*Power(t2,3) + 
                  t1*(-50 - 46*t2 + 6*Power(t2,2)))) + 
            s1*(-34 + 7*Power(t1,4) + Power(s2,4)*(-4 + 5*t1 - 7*t2) + 
               Power(t1,3)*(9 - 6*t2) - 37*t2 - 6*Power(t2,2) + 
               26*Power(t2,3) - 6*Power(t2,4) + 
               Power(t1,2)*(-34 + 18*t2 - 15*Power(t2,2)) - 
               Power(s2,3)*(-6 + Power(t1,2) + 8*t2 - 2*Power(t2,2)) + 
               t1*(22 - 8*t2 - 53*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,2)*(-32 - 6*Power(t1,3) + 25*t2 + 
                  51*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t1,2)*(18 + 17*t2) - 
                  t1*(27 + 48*t2 + 20*Power(t2,2))) + 
               s2*(60 + 3*Power(t1,4) + 51*t2 - 2*Power(t2,2) + 
                  5*Power(t2,3) + 5*Power(t2,4) - 
                  Power(t1,3)*(17 + 11*t2) + 
                  3*Power(t1,2)*(11 + 3*t2 + 6*Power(t2,2)) - 
                  t1*(8 + 17*t2 - 3*Power(t2,2) + 15*Power(t2,3))))) - 
         s*(29 + Power(s1,5)*s2*(s2 - t1) - 53*t1 + 19*Power(t1,2) + 
            5*Power(t1,3) + 55*t2 - 56*t1*t2 - 10*Power(t1,2)*t2 + 
            15*Power(t1,3)*t2 - 4*Power(t1,4)*t2 - 13*Power(t2,2) + 
            24*t1*Power(t2,2) - 26*Power(t1,2)*Power(t2,2) + 
            11*Power(t1,3)*Power(t2,2) + Power(t1,4)*Power(t2,2) - 
            15*Power(t2,3) + 19*t1*Power(t2,3) - 
            6*Power(t1,2)*Power(t2,3) - 3*Power(t1,3)*Power(t2,3) - 
            8*Power(t2,4) - 5*t1*Power(t2,4) + 3*Power(t1,2)*Power(t2,4) + 
            4*Power(t2,5) - t1*Power(t2,5) + 
            Power(s2,5)*t2*(1 - t1 + t2) - 
            Power(s2,3)*(1 - 25*t2 + 2*Power(t2,2) + 17*Power(t2,3) + 
               3*Power(t2,4) + Power(t1,3)*(2 + t2) - 
               Power(t1,2)*(3 + 7*t2) + t1*t2*(31 + t2 - 4*Power(t2,2))) + 
            Power(s1,4)*(5*Power(s2,3) + Power(s2,2)*(1 - 6*t1 + t2) + 
               t1*(-3 - 8*t1 + 3*t2) + 
               s2*(-4 + Power(t1,2) - t1*(-18 + t2) - 2*t2 + Power(t2,2))) \
+ Power(s2,4)*(1 + 3*t2 + 7*Power(t2,2) + Power(t2,3) + 
               Power(t1,2)*(1 + 2*t2) - t1*(2 + 5*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(16 + Power(t1,4) - 53*t2 - 92*Power(t2,2) + 
               6*Power(t2,3) + 7*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,3)*t2*(5 + 2*t2) - 
               Power(t1,2)*(-13 + 19*t2 + 19*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*(-30 + 67*t2 + 70*Power(t2,2) + 6*Power(t2,3) + 
                  6*Power(t2,4))) - 
            s2*(51 + 73*t2 - 39*Power(t2,2) + 10*Power(t2,3) + 
               7*Power(t2,4) + Power(t2,5) - Power(t2,6) + 
               Power(t1,4)*(7 + 2*t2) + 
               Power(t1,3)*(-31 - 17*t2 - 12*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(92 + 96*t2 + 20*Power(t2,2) + 
                  19*Power(t2,3) - 3*Power(t2,4)) + 
               t1*(-119 - 154*t2 - 19*Power(t2,2) - 17*Power(t2,3) - 
                  10*Power(t2,4) + 3*Power(t2,5))) + 
            Power(s1,3)*(3 + 4*Power(s2,4) + 
               Power(s2,3)*(-13 + 3*t1 - 8*t2) + 6*t2 - 4*Power(t2,2) + 
               5*Power(t1,2)*(4 + 3*t2) - 
               2*t1*(9 - 11*t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(38 - 12*Power(t1,2) - 25*t2 - 
                  7*Power(t2,2) + t1*(25 + 19*t2)) + 
               s2*(-38 + 5*Power(t1,3) + 8*t2 + 7*Power(t2,2) - 
                  4*Power(t2,3) - 4*Power(t1,2)*(1 + 3*t2) + 
                  t1*(-1 - 41*t2 + 12*Power(t2,2)))) + 
            Power(s1,2)*(37 + Power(s2,5) + 8*Power(t1,4) + 
               Power(t1,3)*(1 - 11*t2) + Power(s2,4)*(-1 + 5*t1 - 7*t2) - 
               9*t2 - 20*Power(t2,2) + 12*Power(t2,3) - 
               Power(s2,3)*(-4 + t1 + 6*Power(t1,2) - 3*t2 - 4*t1*t2 + 
                  2*Power(t2,2)) - 
               Power(t1,2)*(29 + 12*t2 + 3*Power(t2,2)) + 
               t1*(-20 + 17*t2 - 40*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(-63 - 3*Power(t1,3) - 76*t2 + 
                  54*Power(t2,2) + 5*Power(t2,3) + 
                  2*Power(t1,2)*(2 + 7*t2) + 
                  t1*(23 - 34*t2 - 14*Power(t2,2))) + 
               s2*(18 + 3*Power(t1,4) + 88*t2 - 11*Power(t2,2) - 
                  9*Power(t2,3) + 6*Power(t2,4) - 
                  Power(t1,3)*(21 + 13*t2) + 
                  Power(t1,2)*(64 + 5*t2 + 24*Power(t2,2)) - 
                  t1*(14 + 17*t2 - 38*Power(t2,2) + 20*Power(t2,3)))) + 
            s1*(-61 + Power(s2,5)*(-1 + t1 - 2*t2) - 36*t2 + 
               21*Power(t2,2) + 22*Power(t2,3) - 12*Power(t2,4) - 
               Power(t1,4)*(6 + 7*t2) + 
               Power(t1,3)*(11 - 6*t2 + 14*Power(t2,2)) - 
               Power(t1,2)*(18 - 25*t2 + 2*Power(t2,2) + 7*Power(t2,3)) + 
               2*t1*(37 + 15*t2 - 9*Power(t2,2) + 13*Power(t2,3)) + 
               Power(s2,4)*(Power(t1,2) + 2*(-3 + t2)*t2 - 
                  t1*(1 + 2*t2)) + 
               Power(s2,3)*(-15 - 4*Power(t1,3) + 27*Power(t2,2) + 
                  8*Power(t2,3) + Power(t1,2)*(13 + 8*t2) + 
                  t1*(6 - 2*t2 - 11*Power(t2,2))) + 
               Power(s2,2)*(43 + 2*Power(t1,4) + 163*t2 + 
                  32*Power(t2,2) - 37*Power(t2,3) + 2*Power(t2,4) - 
                  Power(t1,3)*(20 + 3*t2) + 
                  Power(t1,2)*(33 + 31*t2 + 4*Power(t2,2)) - 
                  t1*(58 + 113*t2 - 3*Power(t2,2) + 5*Power(t2,3))) + 
               s2*(47 + Power(t1,4)*(1 - 2*t2) - 56*t2 - 40*Power(t2,2) + 
                  14*Power(t2,3) + 5*Power(t2,4) - 4*Power(t2,5) + 
                  Power(t1,3)*(21 + t2 + 9*Power(t2,2)) - 
                  Power(t1,2)*
                   (3 + 30*t2 - 18*Power(t2,2) + 16*Power(t2,3)) + 
                  t1*(-66 - 13*t2 + Power(t2,2) - 25*Power(t2,3) + 
                     13*Power(t2,4))))))*T5q(1 - s1 - t1 + t2))/
     ((-1 + s1)*(s - s2 + t1)*(-s + s1 - t2)*(-1 + s1 + t1 - t2)*
       Power(-1 + s - s*s2 + s1*s2 + t1 - s2*t2,2)));
   return a;
};
