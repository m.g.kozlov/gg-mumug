#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m123_1_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(s1*(-1 + t2)*(7 + 2*Power(s1,3) + 9*t1 - 
            Power(s1,2)*(6 + s2 - 3*t1 - 2*t2) + s2*(-2 + t2) - 41*t2 - 
            6*t1*t2 + 18*Power(t2,2) - 
            s1*(-41 + s2*(-3 + t2) - 6*t1*(-2 + t2) + 17*t2 + 
               6*Power(t2,2))) + 
         Power(s,2)*(1 - 2*Power(s1,3) + Power(s1,2)*(9 - t1 + t2) + 
            s1*(t1 - 5*(4 + t2))) + 
         s*(-1 + 2*Power(s1,4) + Power(s1,3)*(-4 + s2 + t1) + t2 - 
            s1*(-37 + 3*t1*(-4 + t2) + 64*t2 - 11*Power(t2,2) + 
               s2*(6 + t2)) + Power(s1,2)*
             (22 - t2 - 3*Power(t2,2) + s2*(5 + t2) + t1*(-13 + 3*t2)))))/
     (s*Power(-1 + s1,2)*s1*(-s + s1 - t2)*(-1 + s + t2)) + 
    (8*(Power(s1,5)*(s2 - t1) - 
         Power(s1,4)*(-5 + 3*s2 - 2*t1)*(-1 + t2) - 
         Power(s1,3)*(27 - 33*t2 + 6*Power(t2,2) + 
            s2*(6 + t2 - 3*Power(t2,2)) + t1*(-5 + Power(t2,2))) - 
         Power(s1,2)*(-6 - 44*t2 + 47*Power(t2,2) + 3*Power(t2,3) + 
            t1*(8 - 6*Power(t2,2)) + 
            s2*(6 - 16*t2 + 7*Power(t2,2) + Power(t2,3))) + 
         2*(4 + (23 - 4*s2)*t2 + (-31 + 5*s2)*Power(t2,2) - 
            2*(-5 + s2)*Power(t2,3) - 6*Power(t2,4) + 
            t1*(-4 + 5*t2 - 2*Power(t2,2) + 2*Power(t2,3))) + 
         s1*(-70 + 80*t2 - 45*Power(t2,2) + 31*Power(t2,3) + 
            4*Power(t2,4) - t1*
             (-14 + 12*t2 + Power(t2,2) + 4*Power(t2,3)) + 
            s2*(8 - 4*t2 - 6*Power(t2,2) + 5*Power(t2,3))) + 
         Power(s,3)*((-1 + s1)*t1*(1 + s1 + 2*t2) + 
            t2*(-5 + Power(s1,2) + 6*t2 - 2*s1*(4 + t2))) + 
         Power(s,2)*(-1 - t1 + Power(s1,3)*(s2 - 3*t1 - 2*t2) + 39*t2 - 
            3*s2*t2 + 3*t1*t2 - 27*Power(t2,2) - 4*t1*Power(t2,2) + 
            12*Power(t2,3) - Power(s1,2)*
             (7 + (-20 + s2)*t2 - 6*Power(t2,2) + 2*t1*(1 + t2)) - 
            s1*(4 + s2 - 39*t2 - 4*s2*t2 + 23*Power(t2,2) + 
               4*Power(t2,3) + t1*(-6 + t2 - 4*Power(t2,2)))) - 
         s*(2 - 10*t1 + Power(s1,4)*(2*s2 - 3*t1 - t2) + 87*t2 - 
            6*s2*t2 + t1*t2 - 55*Power(t2,2) + 7*s2*Power(t2,2) - 
            8*t1*Power(t2,2) + 34*Power(t2,3) + 2*t1*Power(t2,3) - 
            6*Power(t2,4) + Power(s1,3)*
             (-16 + s2*(3 - 4*t2) + 2*t1*(-2 + t2) + 17*t2 + 
               4*Power(t2,2)) + 
            Power(s1,2)*(-11 + 69*t2 - 22*Power(t2,2) - 5*Power(t2,3) + 
               3*t1*(2 - t2 + Power(t2,2)) + 
               s2*(-11 + 6*t2 + 2*Power(t2,2))) + 
            s1*(-43 + 76*t2 - 79*Power(t2,2) + 11*Power(t2,3) + 
               2*Power(t2,4) + s2*(6 + 4*t2 - 9*Power(t2,2)) + 
               t1*(11 + 5*Power(t2,2) - 2*Power(t2,3)))))*
       B1(s,1 - s + s1 - t2,s1))/
     (Power(-1 + s1,2)*(-s + s1 - t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (16*(Power(s,4)*(1 - s1 + 7*Power(s1,2) + Power(s1,3)) + 
         Power(s,3)*(-2*Power(s1,4) + 2*(-2 + t2) + 
            Power(s1,3)*(-16 - 4*s2 + t1 + 3*t2) + 
            Power(s1,2)*(-28 + 4*s2 + t1*(-1 + t2) + t2 - Power(t2,2)) + 
            s1*(2 - (-2 + t1)*t2 + Power(t2,2))) + 
         Power(s,2)*(6 + Power(s1,5) + 
            Power(s1,4)*(19 + 7*s2 - 4*t1 - 5*t2) - 6*t2 + Power(t2,2) + 
            s1*(-4 + (-7 + 3*t1)*t2 - (-2 + s2 + t1)*Power(t2,2) + 
               Power(t2,3)) - 
            Power(s1,3)*(26 + 2*t2 - 7*Power(t2,2) + 3*t1*(1 + t2) + 
               s2*(-5 + 4*t2)) + 
            Power(s1,2)*(64 - 36*t2 - 14*Power(t2,2) - Power(t2,3) + 
               t1*(7 + Power(t2,2)) + s2*(-12 + 4*t2 + Power(t2,2)))) + 
         (-1 + s1)*(2*Power(s1,5)*(s2 - t1) - Power(-1 + t2,2) - 
            s1*(-1 + t2)*(2 - (1 + t1)*t2 + (-1 + s2)*Power(t2,2)) + 
            Power(s1,4)*(-5 + 2*t2 + 3*Power(t2,2) + s2*(1 + t2) - 
               t1*(1 + t2)) + 
            Power(s1,3)*(13 - 12*t2 + Power(t2,2) - 2*Power(t2,3) + 
               t1*(8 + t2 + Power(t2,2)) + 
               s2*(-5 - 7*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(-15 + 29*t2 - 21*Power(t2,2) + 7*Power(t2,3) + 
               t1*(-5 + t2 - 2*Power(t2,2)) + 
               s2*(4 + 3*Power(t2,2) - Power(t2,3)))) + 
         s*(Power(s1,5)*(-6 - 5*s2 + 5*t1 + 2*t2) - 
            2*(2 - 3*t2 + Power(t2,2)) + 
            s1*(6 + 2*(-3 + s2)*Power(t2,2) - s2*Power(t2,3) + 
               t1*t2*(-3 + 2*t2)) + 
            Power(s1,4)*(15 + 6*t2 - 9*Power(t2,2) + s2*(-13 + 3*t2) + 
               t1*(7 + 3*t2)) - 
            Power(s1,3)*(-29 + 57*t2 - 13*Power(t2,2) - 3*Power(t2,3) + 
               t1*(1 + t2 + 2*Power(t2,2)) + s2*(-6 - t2 + 3*Power(t2,2))\
) + Power(s1,2)*(-60 + t1*(-11 + t2) + 79*t2 - 8*Power(t2,2) - 
               7*Power(t2,3) + s2*(12 - 4*t2 + Power(t2,2) + Power(t2,3)))\
))*R1q(s1))/(Power(-1 + s1,2)*s1*
       (1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*(-s + s1 - t2)*
       Power(-1 + s + t2,2)) - 
    (32*(2*Power(s,4) + Power(s1,4)*(s2 - t1) + 
         Power(s,3)*(-8 + s2 - s1*(4 + s2) + 4*t2) + 
         Power(s1,3)*(-3 - s2*(-2 + t2) + t1*(-2 + t2) + 2*t2 + 
            Power(t2,2)) + (-1 + t2)*
          (-1 + t1 + 6*t2 - 2*s2*t2 - 2*Power(t2,2) + t1*Power(t2,2) - 
            3*Power(t2,3)) + Power(s,2)*
          (15 - 3*s2 + Power(s1,2)*(5 + 2*s2 - t1) - 16*t2 + 2*s2*t2 + 
            t1*t2 - Power(t2,2) + 
            s1*(s2 + t1 - 2*s2*t2 - t1*t2 + (-4 + t2)*t2)) + 
         Power(s1,2)*(-(s2*(1 + 4*t2)) - 
            2*t2*(-3 + 2*t2 + Power(t2,2)) + t1*(2 + 2*t2 + Power(t2,2))) \
+ s1*(4 - 5*t2 - 5*Power(t2,2) + 5*Power(t2,3) + Power(t2,4) + 
            s2*(-2 + 3*t2 + 2*Power(t2,2)) - t1*(-2 + 4*t2 + Power(t2,3))\
) + s*(-10 - 2*Power(s1,3)*(1 + s2 - t1) + t1 + 23*t2 - t1*t2 - 
            7*Power(t2,2) + 2*t1*Power(t2,2) - 6*Power(t2,3) + 
            Power(s1,2)*(5 + 2*t1 + 2*s2*(-2 + t2) + t2 - 
               2*Power(t2,2)) + s2*(2 - 5*t2 + Power(t2,2)) + 
            s1*(-5 - 4*t2 + 5*Power(t2,2) + 2*Power(t2,3) + 
               t1*(-5 + t2 - 2*Power(t2,2)) + s2*(4 + 3*t2 - Power(t2,2)))\
))*R1q(1 - s + s1 - t2))/
     (Power(-1 + s1,2)*Power(-s + s1 - t2,2)*Power(-1 + s + t2,2)) + 
    (16*(-4*Power(s,4) + Power(s,3)*
          (-2 - 2*Power(s1,2) - 2*s2 + t1 - t2 + s1*(8 + 2*s2 - t1 + t2)) \
+ Power(-1 + s1,2)*(4 + Power(s1,3) - s2 - Power(s1,2)*(2 + s2 - 2*t1) + 
            4*t1 - 18*t2 - 2*t1*t2 + 6*Power(t2,2) + 
            s1*(17 + 2*s2 + 2*t1*(-3 + t2) - 6*t2 - 2*Power(t2,2))) + 
         Power(s,2)*(15 + 5*Power(s1,3) + s2 + 2*t1 - 14*t2 + s2*t2 - 
            2*t1*t2 + 6*Power(t2,2) + 
            s1*(68 + s2 + 2*t1*(-3 + t2) + t2 - s2*t2 - 2*Power(t2,2)) - 
            Power(s1,2)*(2*s2 - 4*t1 + 3*(4 + t2))) + 
         s*(-19 - 4*Power(s1,4) - 7*t1 + 29*t2 + 4*t1*t2 - 
            12*Power(t2,2) + s2*(2 + t2) + 
            Power(s1,3)*(8 + s2 - 5*t1 + 2*t2) + 
            Power(s1,2)*(-47 + t1*(11 - 4*t2) + t2 + s2*t2 + 
               4*Power(t2,2)) + 
            s1*(-18 + t1 + 64*t2 - 8*Power(t2,2) - s2*(3 + 2*t2))))*R2q(s))/
     (s*Power(-1 + s1,2)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(-s + s1 - t2)) + 
    (8*(Power(-1 + s1,4)*(-4 + 2*s1 + Power(s1,2) + 2*t2 - s1*t2)*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) + 
         Power(s,5)*((-1 + Power(s1,2))*t1 + 
            (7 + 4*s1 + Power(s1,2) - 4*t2)*t2) + 
         Power(s,4)*(-1 + Power(s1,3)*(s2 - 5*t1 - 4*t2) + 
            (-22 - 3*s2 + 2*t1)*t2 - 2*(-14 + s2)*Power(t2,2) - 
            4*Power(t2,3) + Power(s1,2)*
             (5 + t1*(-5 + t2) - 7*t2 - 3*s2*t2 + 3*Power(t2,2)) + 
            s1*(8 + 10*t1 - 67*t2 - 3*t1*t2 + 13*Power(t2,2) + 
               s2*(-1 + 6*t2 + 2*Power(t2,2)))) - 
         s*Power(-1 + s1,2)*(11 + Power(s1,4)*(4*s2 - 5*t1 - t2) - 
            17*t2 + 13*s2*t2 + 8*Power(t2,2) - 6*s2*Power(t2,2) + 
            4*Power(t2,3) - 2*s2*Power(t2,3) + t1*(-15 + 8*t2) + 
            Power(s1,3)*(-2 - 6*t1 + s2*(10 - 9*t2) + 2*t2 + 4*t1*t2 + 
               3*Power(t2,2)) + 
            Power(s1,2)*(4 + 20*t1 - 8*t1*t2 + Power(t2,2) - 
               2*Power(t2,3) + s2*(-9 - 7*t2 + 7*Power(t2,2))) - 
            s1*(-3 - 6*t1 + 12*t2 + 4*t1*t2 + 4*Power(t2,2) - 
               2*Power(t2,3) + 
               s2*(5 + t2 - 7*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(s,3)*(-1 + 10*t1 + 16*t2 + s2*t2 - 8*t1*t2 - 
            48*Power(t2,2) + 6*s2*Power(t2,2) + 20*Power(t2,3) - 
            2*s2*Power(t2,3) + Power(s1,4)*(-4*s2 + 10*t1 + 6*t2) - 
            Power(s1,3)*(2 + s2*(6 - 11*t2) + 4*t1*(-3 + t2) + 
               9*Power(t2,2)) + 
            Power(s1,2)*(-64 + 80*t2 - 11*Power(t2,2) + 2*Power(t2,3) + 
               4*t1*(-5 + 2*t2) - 3*s2*(-5 + 5*t2 + 3*Power(t2,2))) + 
            s1*(-5 + 4*t1*(-3 + t2) + 162*t2 - 84*Power(t2,2) + 
               2*Power(t2,3) + 
               s2*(-5 + 3*t2 + 3*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(s,2)*(9 + 2*Power(s1,5)*(3*s2 - 5*t1 - 2*t2) - 12*t2 + 
            11*s2*t2 + 18*Power(t2,2) - 8*s2*Power(t2,2) - 
            20*Power(t2,3) + 4*s2*Power(t2,3) + 4*t1*(-5 + 3*t2) + 
            Power(s1,4)*(-4 + 6*t1*(-1 + t2) + 6*t2 + 9*Power(t2,2) - 
               5*s2*(-2 + 3*t2)) + 
            Power(s1,2)*(77 - 134*t2 + 23*Power(t2,2) + 4*Power(t2,3) + 
               2*t1*(-7 + 3*t2) + 
               s2*(8 + 8*t2 - 6*Power(t2,2) - 4*Power(t2,3))) + 
            s1*(s2*(5 - 16*t2 + Power(t2,2)) - 
               2*(8 + 30*t2 - 56*Power(t2,2) + 6*Power(t2,3) + 
                  t1*(-8 + 5*t2))) + 
            Power(s1,3)*(s2*(-29 + 12*t2 + 13*Power(t2,2)) - 
               2*(-15 + 10*t2 + Power(t2,2) + 2*Power(t2,3) + 
                  t1*(-17 + 7*t2)))))*T2q(s1,s))/
     (s*Power(-1 + s1,2)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(-s + s1 - t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))\
) + (8*(-((-1 + s1)*Power(-1 + t2,2)*
            (-4 + 2*s1 + Power(s1,2) + 2*t2 - s1*t2)*
            (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2)) + 
         Power(s,4)*(-((-1 + s1)*t1*(1 + s1 + 2*t2)) + 
            t2*(-11 - Power(s1,2) - 6*t2 + 2*s1*(4 + t2))) + 
         Power(s,3)*(1 + t1 + 33*t2 - 5*s2*t2 - 2*t1*t2 - 
            8*Power(t2,2) + 6*t1*Power(t2,2) - 18*Power(t2,3) + 
            Power(s1,3)*(-s2 + 2*t1 + t2) + 
            Power(s1,2)*(7 - t1*(-2 + t2) + (-12 + s2)*t2 - 
               5*Power(t2,2)) + 
            s1*(-12 + s2 - 42*t2 + 4*s2*t2 + 33*Power(t2,2) + 
               6*Power(t2,3) + t1*(-5 + 3*t2 - 6*Power(t2,2)))) + 
         Power(s,2)*(2 + Power(s1,4)*(s2 - t1) - 9*t1 - 47*t2 + 
            18*s2*t2 + 4*t1*t2 + 44*Power(t2,2) - 10*s2*Power(t2,2) - 
            9*t1*Power(t2,2) + 19*Power(t2,3) + 6*t1*Power(t2,3) - 
            18*Power(t2,4) + Power(s1,3)*
             (-9 + s2*(3 - 4*t2) + 7*t2 + 2*Power(t2,2) + 
               t1*(-4 + 5*t2)) - 
            Power(s1,2)*(31 - (73 + 6*t1)*t2 - 
               3*(-11 + t1)*Power(t2,2) + 9*Power(t2,3) + 
               s2*(2 + 6*t2 + Power(t2,2))) + 
            s1*(14 + 59*t2 - 121*Power(t2,2) + 42*Power(t2,3) + 
               6*Power(t2,4) + s2*(-2 - 8*t2 + 11*Power(t2,2)) + 
               t1*(14 - 15*t2 + 6*Power(t2,2) - 6*Power(t2,3)))) + 
         s*(-1 + t2)*(7 + 2*Power(s1,4)*(s2 - t1) - 11*t1 - 32*t2 + 
            17*s2*t2 + 3*t1*t2 + 21*Power(t2,2) - 7*s2*Power(t2,2) - 
            4*t1*Power(t2,2) + 10*Power(t2,3) + 2*t1*Power(t2,3) - 
            6*Power(t2,4) + Power(s1,3)*
             (s2*(5 + t2) + 3*(-6 - 2*t1 + 5*t2 + Power(t2,2))) - 
            Power(s1,2)*(23 - 51*t2 + 23*Power(t2,2) + 5*Power(t2,3) + 
               t1*(4 - 9*t2 - 3*Power(t2,2)) + 
               s2*(-2 + 7*t2 + 3*Power(t2,2))) + 
            s1*(6 + 38*t2 - 65*Power(t2,2) + 19*Power(t2,3) + 
               2*Power(t2,4) + s2*(-9 - 11*t2 + 10*Power(t2,2)) + 
               t1*(23 - 12*t2 + Power(t2,2) - 2*Power(t2,3)))))*
       T3q(1 - s + s1 - t2,s1))/
     (s*Power(-1 + s1,2)*(-s + s1 - t2)*(-1 + s + t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*((-1 + s1)*(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2)*
          (Power(s1,3) - 2*Power(s1,2)*(-1 + t2) - 2*(-2 + t2)*t2 + 
            s1*(-4 + Power(t2,2))) + 
         Power(s,3)*((-1 + s1)*t1*(1 + s1 + 2*t2) + 
            t2*(7 + Power(s1,2) + 6*t2 - 2*s1*(2 + t2))) + 
         Power(s,2)*(-1 + Power(s1,3)*(s2 - 3*t1 - 2*t2) + 17*t2 + 
            s2*t2 - 13*Power(t2,2) + 12*Power(t2,3) + 
            t1*(-3 + 5*t2 - 4*Power(t2,2)) + 
            Power(s1,2)*(-3 - (-6 + s2 + 2*t1)*t2 + 6*Power(t2,2)) - 
            s1*(-8 + s2 - 3*t2 + 13*Power(t2,2) + 4*Power(t2,3) + 
               t1*(-6 + 3*t2 - 4*Power(t2,2)))) - 
         s*(4 - 4*t1 + Power(s1,4)*(2*s2 - 3*t1 - t2) + 31*t2 - 4*s2*t2 + 
            9*t1*t2 - 43*Power(t2,2) + s2*Power(t2,2) - 
            6*t1*Power(t2,2) + 20*Power(t2,3) + 2*t1*Power(t2,3) - 
            6*Power(t2,4) + Power(s1,3)*
             (-4 + s2 - 4*s2*t2 + (3 + 2*t1)*t2 + 4*Power(t2,2)) + 
            Power(s1,2)*(7 + 7*t2 - 6*Power(t2,2) - 5*Power(t2,3) + 
               3*t1*(4 - 3*t2 + Power(t2,2)) + 
               s2*(-7 + 2*t2 + 2*Power(t2,2))) + 
            s1*(-35 + 32*t2 - 19*Power(t2,2) + 9*Power(t2,3) + 
               2*Power(t2,4) + s2*(4 + 6*t2 - 3*Power(t2,2)) - 
               t1*(5 + 2*t2 - 3*Power(t2,2) + 2*Power(t2,3)))))*
       T4q(-s + s1 - t2))/
     (s*Power(-1 + s1,2)*(-s + s1 - t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(-4*(s1 - t2)*(-1 + t2)*
          (-1 + t1 - 5*t2 + s2*t2 - 2*t1*t2 + 6*Power(t2,2) + 
            Power(s1,2)*(-2 + s2 - t1 + 2*t2) - 
            s1*(-7 + s2 + 5*t2 + s2*t2 - 2*t1*t2 + 2*Power(t2,2))) + 
         Power(s,3)*(-((-1 + s1)*t1*(1 + s1 + 2*t2)) + 
            t2*(-3 - Power(s1,2) - 6*t2 + 2*s1*(4 + t2))) + 
         Power(s,2)*(1 - 17*t2 - s2*t2 + 32*Power(t2,2) - 6*Power(t2,3) + 
            Power(s1,3)*(-s2 + 2*t1 + t2) + 
            Power(s1,2)*(7 + (-10 + s2 + t1)*t2 - 3*Power(t2,2)) + 
            t1*(3 - 6*t2 + 2*Power(t2,2)) + 
            s1*(-4 + s2 - 26*t2 + 7*Power(t2,2) + 2*Power(t2,3) + 
               t1*(-5 + 5*t2 - 2*Power(t2,2)))) + 
         s*(Power(s1,4)*(s2 - t1) + 
            Power(s1,3)*(-5 + s2 - 2*s2*t2 + (5 + t1)*t2) + 
            2*(2 + (7 - 2*s2)*t2 + (-37 + 3*s2)*Power(t2,2) + 
               28*Power(t2,3) + t1*(-2 + 9*t2 - 8*Power(t2,2))) + 
            Power(s1,2)*(-9 + t1*(7 - 8*t2) - 2*t2 + 11*Power(t2,2) + 
               s2*(-6 + 6*t2 + Power(t2,2))) - 
            s1*(30 - 103*t2 + 57*Power(t2,2) + 16*Power(t2,3) + 
               t1*(2 + 11*t2 - 16*Power(t2,2)) + s2*(-4 + 7*Power(t2,2)))))*
       T5q(s))/(s*Power(-1 + s1,2)*(-s + s1 - t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))));
   return a;
};
