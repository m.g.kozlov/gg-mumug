#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m321_5_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(2*Power(s,2)*s2*(2 - 9*t1 + s2*(6 + t1 - t2) + t2) - 
         s2*(s2 - t1)*(13 + 2*Power(s2,3) - 
            s1*(-1 + s2)*(-2 + 7*s2 - 5*t1) - 11*t1 - 18*Power(t1,2) + 
            3*t2 + 6*t1*t2 + Power(s2,2)*(6 - 10*t1 + 9*t2) + 
            s2*(-1 + 13*t1 + 6*Power(t1,2) - 12*t2 - 6*t1*t2)) + 
         s*(2*Power(s2,4) - t1 + Power(s2,3)*(9 - 6*s1 - 11*t1 + 11*t2) + 
            Power(s2,2)*(9*Power(t1,2) + 6*s1*(1 + t1) + t1*(26 - 9*t2) - 
               5*(6 + t2)) + s2*
             (7 - 37*Power(t1,2) - 6*t2 + t1*(26 - 6*s1 + 9*t2)))))/
     (s*Power(-1 + s2,2)*s2*(s2 - t1)*(-1 + t1)) + 
    (8*(4*Power(s,3)*(-1 + t1)*t1 + 
         s2*(4*Power(t1,4) - s1*
             (-14 - 15*t1 + 4*Power(t1,2) + Power(t1,3)) + 
            t1*(59 - 10*t2) + Power(t1,3)*(5 - 4*t2) + 
            Power(t1,2)*(-13 + t2) - 11*(5 + t2)) + 
         Power(s2,3)*(-8 + s1*(7 + 2*t1 - Power(t1,2)) - 7*t2 - 
            2*t1*t2 + Power(t1,2)*(8 + t2)) + 
         Power(s2,2)*(5 + t1 - 13*Power(t1,3) + 
            s1*(-21 - 3*t1 - Power(t1,2) + Power(t1,3)) + 20*t2 - 
            2*t1*t2 + Power(t1,2)*(7 + 6*t2)) + 
         2*(1 - 6*Power(t1,4) + Power(t1,2)*(-33 + 3*s1 - 4*t2) - t2 + 
            2*Power(t1,3)*(6 + t2) + t1*(26 - 7*s1 + 7*t2)) + 
         Power(s,2)*(10*Power(t1,3) + Power(t1,2)*(-21 + 2*s1 - 2*t2) + 
            3*t2 + t1*(31 - 2*s1 + 3*t2) + 
            s2*(-4 - 2*Power(t1,3) + t1*(17 + 2*s1 - 3*t2) - 3*t2 + 
               Power(t1,2)*(1 - 2*s1 + 2*t2))) + 
         s*(3 + 6*Power(t1,4) + t1*(-72 + 11*s1 - 7*t2) - 
            5*Power(t1,2)*(-11 + s1 - 2*t2) + 
            2*Power(t1,3)*(-16 + s1 - t2) - t2 + 
            s2*(21 - 2*Power(t1,4) + 
               s1*(1 - 8*t1 + Power(t1,2) - 2*Power(t1,3)) + 
               Power(t1,2)*(15 - 7*t2) - 5*t2 + t1*(-55 + 2*t2) + 
               Power(t1,3)*(5 + 2*t2)) + 
            Power(s2,2)*(3*Power(t1,3) + s1*(-1 - 3*t1 + 4*Power(t1,2)) + 
               6*(2 + t2) - Power(t1,2)*(14 + 3*t2) + t1*(-9 + 5*t2))))*
       B1(s,t1,s2))/
     (Power(-1 + s2,2)*(-1 + t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) + 
    (16*(Power(s2,7) + Power(-1 + s,2)*Power(t1,2) + 
         Power(s2,6)*(7 - 2*s + 2*s1 + 2*t2) + 
         Power(s2,5)*(-24 + Power(s,2) - 20*t1 - 3*Power(t1,2) - 
            s1*(5 + 3*t1) - t2 + t1*t2 - s*(16 + s1 - 2*t1 + 5*t2)) + 
         Power(s2,4)*(35 + 34*t1 + 20*Power(t1,2) + 2*Power(t1,3) + 
            2*s1*(1 + 5*t1) - Power(s,2)*(-7 + s1 + 3*t1 - 4*t2) - 
            9*t2 - 2*t1*t2 - Power(t1,2)*t2 + 
            s*(-9 + 28*t1 + 5*Power(t1,2) + s1*(3 + t1) - 7*t2 - 3*t1*t2)\
) + Power(s2,2)*(6 + 6*Power(t1,3) + Power(t1,2)*(3 + 4*s1 - 3*t2) - 
            2*t1*(-7 + s1 - t2) - 5*t2 + 
            Power(s,3)*(2 + Power(t1,2) + t2 - t1*(7 + t2)) - 
            s*(10 - (-14 + s1)*Power(t1,2) + (-7 + s1)*Power(t1,3) - 
               11*t2 + t1*(27 - 4*s1 + t2)) + 
            Power(s,2)*(2 - 2*(-8 + s1)*t1 + Power(t1,3) - 7*t2 - 
               Power(t1,2)*(-8 + s1 + t2))) + 
         s2*t1*(-1 - (-1 + s1)*Power(t1,2) - t2 + t1*(-5 + s1 + t2) + 
            Power(s,3)*(2 - t1 + t2) + 
            s*(4 + s1*(-2 + t1)*t1 + 3*t2 - 2*t1*t2) + 
            Power(s,2)*(-5 - Power(t1,2) - 3*t2 + t1*(2 + s1 + t2))) + 
         Power(s2,3)*(-25 + s1 - 27*t1 - 5*s1*t1 - 16*Power(t1,2) - 
            5*s1*Power(t1,2) - 9*Power(t1,3) + s1*Power(t1,3) + 
            Power(s,3)*(2 + t1 - t2) + 13*t2 + 3*Power(t1,2)*t2 + 
            s*(9 - 3*Power(t1,3) + s1*(-2 - 5*t1 + Power(t1,2)) + t2 + 
               t1*(53 + t2) + Power(t1,2)*(-25 + 2*t2)) + 
            Power(s,2)*(s1 + 2*s1*t1 - 3*Power(t1,2) + 3*(-6 + t2) + 
               t1*(-8 + 3*t2))))*R1q(s2))/
     (Power(-1 + s2,2)*s2*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*Power(s2 - t1,2)*(-1 + t1)) - 
    (32*(-(Power(s2,2)*(3 + 2*Power(t1,3) + 
              Power(t1,2)*(3 + 2*s1 - 2*t2) + t1*(-8 + 2*s + s1 - t2))) + 
         Power(s2,3)*(-2 + t1 + s1*t1 + Power(t1,2) - t2) + 
         s2*(-1 + Power(t1,4) + Power(t1,2)*(-17 + 2*s1 - s*(-3 + t2)) + 
            Power(t1,3)*(4 + s + s1 - t2) + t2 + t1*(13 + (-3 + s)*t2)) - 
         t1*(2 + 3*Power(t1,3) + Power(t1,2)*(-10 + s1 - t2) - 2*t2 + 
            t1*(5 + 2*t2) + s*(2 + 5*Power(t1,2) + t2 - t1*(5 + t2))))*
       R1q(t1))/(Power(-1 + s2,2)*Power(s2 - t1,2)*Power(-1 + t1,2)) - 
    (16*(Power(s,3)*(6 - 11*t1 + 3*s2*(2 + t1 - t2) + 3*t2) + 
         Power(-1 + s2,2)*(Power(s2,3) - 
            s1*(-1 + s2)*(-1 + 3*s2 - 2*t1) + 
            s2*(3 + 2*Power(t1,2) - 2*t1*(-2 + t2) - 6*t2) + 
            2*(3 - 3*Power(t1,2) + t1*(-4 + t2) + t2) + 
            Power(s2,2)*(2 - 4*t1 + 4*t2)) + 
         Power(s,2)*(-5 + Power(s2,3) - s1*(-1 + s2)*(3 + 6*s2 - t1) + 
            16*t1 - 6*Power(t1,2) - 4*t2 + 2*t1*t2 + 
            Power(s2,2)*(-2 - 11*t1 + 10*t2) + 
            s2*(-22 + 27*t1 + 2*Power(t1,2) - 6*t2 - 2*t1*t2)) + 
         s*(-2*Power(s2,4) + s1*(-1 + s2)*
             (2 + 9*Power(s2,2) - 5*t1 - 3*s2*(1 + t1)) + 
            Power(s2,3)*(-4 + 12*t1 - 11*t2) + 
            (1 + 4*t1)*(-1 + 3*t1 - t2) + 
            s2*(-38 + 46*t1 + 8*Power(t1,2) - t2) + 
            Power(s2,2)*(-3 - 25*t1 - 4*Power(t1,2) + 13*t2 + 4*t1*t2)))*
       R2q(s))/(s*Power(-1 + s2,2)*
       (1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + Power(s2,2))*(-1 + t1)) + 
    (8*(4*Power(s,6)*t1 - Power(-1 + s2,4)*(-2*(1 + t1) + s2*(3 + t1))*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) + 
         Power(s,5)*((-25 + 2*s1)*t1 + 8*Power(t1,2) + 3*t2 - 
            s2*(-4 + (3 + 2*s1)*t1 + 3*t2)) + 
         s*Power(-1 + s2,2)*(3 + 4*(-5 + 3*s1)*Power(t1,2) - 
            2*(-2 + s1)*Power(t1,3) - 5*t2 - t1*(-25 + s1 + 8*t2) - 
            Power(s2,2)*(-2 - 21*t1 + Power(t1,2) + 2*Power(t1,3) + 
               s1*(7 + 13*t1 + Power(t1,2)) - 17*t2 - 8*t1*t2) + 
            Power(s2,3)*(-8 + t1 + 3*Power(t1,2) + 5*s1*(2 + t1) - 
               15*t2 - 4*t1*t2) + 
            s2*(-21 + 5*t1 - 14*Power(t1,2) + 2*Power(t1,3) + 
               s1*(5 - 11*t1 + 5*Power(t1,2) - 2*Power(t1,3)) + 3*t2 + 
               4*t1*t2)) + Power(s,4)*
          (3 + 4*(-11 + s1)*Power(t1,2) + 4*Power(t1,3) - 10*t2 + 
            t1*(52 - 7*s1 + 2*t2) + 
            Power(s2,2)*(4 - 3*Power(t1,2) + s1*(-5 + 4*t1) + 
               t1*(-16 + t2) + 15*t2) - 
            s2*(27 + 5*Power(t1,2) + s1*(-5 - 3*t1 + 4*Power(t1,2)) + 
               5*t2 + t1*(-16 + 3*t2))) + 
         Power(s,3)*(-7 - 12*(-6 + s1)*Power(t1,2) + 
            2*(-10 + s1)*Power(t1,3) + 10*t2 + 
            s2*(33 + 18*Power(t1,2) - 2*Power(t1,3) + 
               s1*(-9 + 13*t1 + 3*Power(t1,2) - 2*Power(t1,3)) + 
               4*t1*(-6 + t2) + 6*t2) + t1*(3*s1 - 8*(4 + t2)) + 
            Power(s2,3)*(9*Power(t1,2) + s1*(14 + t1) + 
               t1*(26 - 4*t2) - 6*(4 + 5*t2)) + 
            Power(s2,2)*(6 - 11*Power(t1,2) - 2*Power(t1,3) + 
               s1*(-5 - 17*t1 + 9*Power(t1,2)) + 14*t2 + t1*(22 + 8*t2))) \
- Power(s,2)*(-3 + 2*(15 + s1)*Power(t1,2) + 4*(-5 + s1)*Power(t1,3) + 
            t1*(24 - 5*s1 - 12*t2) + 
            Power(s2,4)*(-24 + 16*s1 + 12*t1 + 7*s1*t1 + 9*Power(t1,2) - 
               30*t2 - 6*t1*t2) + 
            Power(s2,2)*(-39 + 16*t1 + 7*Power(t1,2) + 4*Power(t1,3) + 
               2*s1*(-1 + t1 + 6*Power(t1,2) - 2*Power(t1,3)) - 10*t2 - 
               6*t1*t2) + s2*(s1*(3 + 22*t1 - 19*Power(t1,2)) - 
               2*(-3 + 19*t1 - Power(t1,2) + 6*Power(t1,3) + t2 - 
                  5*t1*t2)) + 
            Power(s2,3)*(s1*(-17 - 26*t1 + 5*Power(t1,2)) + 
               2*(14 + 9*t1 - 8*Power(t1,2) - 2*Power(t1,3) + 21*t2 + 
                  7*t1*t2))))*T2q(s2,s))/
     (s*Power(-1 + s2,2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*(-1 + t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) - 
    (8*(2*Power(s,3)*t1*(2 + 4*s2 - 5*t1 - 2*Power(t1,2) + 
            Power(s2,2)*(t1 - t2) + t2) - 
         (-1 + s2)*Power(s2 - t1,2)*(-2*(1 + t1) + s2*(3 + t1))*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) + 
         s*(s2 - t1)*(6 + 6*Power(t1,4) + 2*Power(t1,3)*(-6 + s1 - t2) - 
            6*t2 + Power(t1,2)*(-7 - 3*s1 + 2*t2) + 
            t1*(19 - 2*s1 + 5*t2) + 
            Power(s2,3)*(4 + 3*s1 - Power(t1,2) - 4*t2 - 
               t1*(7 + 3*t2)) + 
            s2*(-19 - 2*Power(t1,4) + 
               s1*(2 + 8*t1 + Power(t1,2) - 2*Power(t1,3)) - 5*t2 + 
               Power(t1,2)*(20 + t2) + Power(t1,3)*(1 + 2*t2) - 
               t1*(28 + 3*t2)) + 
            Power(s2,2)*(3*Power(t1,3) + 
               s1*(-5 - 6*t1 + 2*Power(t1,2)) + Power(t1,2)*(4 - 3*t2) + 
               t1*(-8 + t2) + 3*(7 + 5*t2))) + 
         Power(s,2)*(-(t1*(-2 + 10*Power(t1,3) + 
                 t1*(-32 + 2*s1 - 3*t2) + 
                 Power(t1,2)*(1 + 2*s1 - 2*t2) + 8*t2)) + 
            Power(s2,3)*(-2*Power(t1,2) + t2 + t1*(9 + 4*t2)) + 
            Power(s2,2)*(8 - 2*(3 + s1)*Power(t1,2) - 3*t2 + 
               t1*(7 - 2*s1 + 6*t2)) + 
            s2*(2*Power(t1,4) + Power(t1,3)*(5 + 2*s1 - 2*t2) + 
               2*t1*(-19 + s1 - t2) + 2*(2 + t2) + 
               Power(t1,2)*(4*s1 - 3*(4 + t2)))))*T3q(s2,t1))/
     (s*Power(-1 + s2,2)*(s2 - t1)*(-1 + t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) + 
    (8*(-4*Power(s,3)*t1*(3 + t1) + 
         (-1 + s2)*(-1 + t1)*(-2*(1 + t1) + s2*(3 + t1))*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) + 
         Power(s,2)*(-10*Power(t1,3) + 3*t2 + 
            Power(t1,2)*(-13 - 2*s1 + 2*t2) + t1*(-1 - 6*s1 + 3*t2) + 
            s2*(2*Power(t1,3) + t1*(9 + 6*s1 - 3*t2) + 
               Power(t1,2)*(9 + 2*s1 - 2*t2) - 3*(4 + t2))) + 
         s*(3 - (11 + 5*s1)*Power(t1,2) - 6*Power(t1,4) - t2 + 
            Power(t1,3)*(2 - 2*s1 + 2*t2) + t1*(12 - s1 + 7*t2) + 
            s2*(-19 + 2*Power(t1,4) + 
               s1*(-3 + 8*t1 + 9*Power(t1,2) + 2*Power(t1,3)) + 
               t1*(11 - 12*t2) + Power(t1,3)*(9 - 2*t2) + t2 - 
               3*Power(t1,2)*(1 + t2)) + 
            Power(s2,2)*(4 + t1 - 3*Power(t1,3) + 
               s1*(3 - 7*t1 - 4*Power(t1,2)) + 5*t1*t2 + 
               Power(t1,2)*(-2 + 3*t2))))*T4q(t1))/
     (s*Power(-1 + s2,2)*(-1 + t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) \
+ (8*(-4*Power(s,4)*t1 + Power(s,3)*
          (-10*Power(t1,2) - 3*t2 + t1*(21 - 2*s1 + 2*t2) + 
            s2*(-4 + 2*Power(t1,2) + t1*(-1 + 2*s1 - 2*t2) + 3*t2)) + 
         4*(s2 - t1)*(-1 + t1)*
          (-1 + 6*Power(t1,2) + t1*(-5 + s1 - 2*t2) + 
            Power(s2,2)*(-2 + s1 + 2*t1 - t2) + t2 - 
            s2*(-7 + s1 + 5*t1 + s1*t1 + 2*Power(t1,2) - 2*t1*t2)) - 
         Power(s,2)*(3 + 6*Power(t1,3) + 2*Power(t1,2)*(-8 + s1 - t2) - 
            t2 + t1*(7 - 5*s1 + 6*t2) + 
            Power(s2,2)*(3*Power(t1,2) + s1*(-5 + 4*t1) + 8*(1 + t2) - 
               3*t1*(5 + t2)) + 
            s2*(-23 - 2*Power(t1,3) + s1*(5 + t1 - 2*Power(t1,2)) - 
               3*t1*(-4 + t2) - 7*t2 + Power(t1,2)*(1 + 2*t2))) + 
         s*(Power(s2,2)*(1 - 2*t1 + Power(t1,2) + 
               s1*(7 + 2*t1 - Power(t1,2)) - 8*t2) - 
            2*(-1 + t1)*(-1 + 16*Power(t1,2) + t1*(-5 + s1 - 4*t2) + t2) + 
            Power(s2,3)*(8 + s1*(-5 + t1) + 5*t2 - t1*(8 + t2)) + 
            s2*(17 + 8*Power(t1,3) + s1*(-2 - 5*t1 + 3*Power(t1,2)) + 
               Power(t1,2)*(29 - 8*t2) + t2 + t1*(-54 + 11*t2))))*T5q(s))/
     (s*Power(-1 + s2,2)*(-1 + t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))));
   return a;
};
