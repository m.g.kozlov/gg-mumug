#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m231_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(Power(t1,5)*(-2 + 7*(1 + s)*t2 + (3 + s)*Power(t2,2)) - 
         2*Power(t2,2)*(-1 + s2 + t2)*(1 - 2*t2 + Power(t2,3)) + 
         Power(t1,4)*(-2 + (52 - 5*s + 2*Power(s,2))*t2 + 
            (-64 + 7*s + 6*Power(s,2))*Power(t2,2) - 
            2*(1 + s)*Power(t2,3) + 
            s2*(2 - 2*(6 + s)*t2 - (7 + 6*s)*Power(t2,2) + Power(t2,3))) - 
         t1*t2*(-10 + (2 - 8*s)*t2 + 
            (65 - 30*s - 4*Power(s,2))*Power(t2,2) + 
            (-47 + s + 10*Power(s,2))*Power(t2,3) + 
            (-25 + 40*s + 2*Power(s,2))*Power(t2,4) - 
            3*(-5 + s)*Power(t2,5) + 
            Power(s2,2)*(8*t2 - 22*Power(t2,3) + 6*Power(t2,4)) + 
            s2*(10 + (6 + 8*s)*t2 - 2*(9 + 4*s)*Power(t2,2) + 
               (13 + 24*s)*Power(t2,3) - 2*(7 + 4*s)*Power(t2,4) + 
               3*Power(t2,5))) + 
         Power(t1,3)*t2*(-39 - 53*t2 + 120*Power(t2,2) - 20*Power(t2,3) - 
            2*Power(s2,2)*(2 - 9*t2 + 3*Power(t2,2)) - 
            2*Power(s,2)*(-2 + 7*t2 + 7*Power(t2,2)) + 
            s2*(-32 + 29*t2 + 40*Power(t2,2) - 5*Power(t2,3)) + 
            s*(46 + t2 - 75*Power(t2,2) + 4*Power(t2,3) + 
               4*s2*(-2 - 3*t2 + 5*Power(t2,2)))) - 
         Power(s1,2)*t1*(-1 + t2)*
          (-3*Power(t1,3)*t2 + 
            Power(t1,2)*(2 + 3*(-6 + s2)*t2 + 2*Power(t2,2)) + 
            t2*(10 - 14*t2 - 4*Power(s2,2)*t2 + s2*(-10 + Power(t2,2))) + 
            t1*(2 + 12*t2 + 4*Power(s2,2)*t2 + 6*Power(t2,2) + 
               Power(t2,3) + s2*(-2 + 8*Power(t2,2)))) + 
         Power(t1,2)*t2*(-4 + 90*t2 - 34*Power(t2,2) - 86*Power(t2,3) + 
            34*Power(t2,4) + 2*Power(s,2)*t2*
             (-4 + 11*t2 + 5*Power(t2,2)) + 
            4*Power(s2,2)*(2 + t2 - 10*Power(t2,2) + 3*Power(t2,3)) + 
            s2*(14 + 24*t2 - 14*Power(t2,2) - 47*Power(t2,3) + 
               7*Power(t2,4)) + 
            s*(-8 - 76*t2 + 5*Power(t2,2) + 101*Power(t2,3) - 
               6*Power(t2,4) + s2*(8 + 38*Power(t2,2) - 22*Power(t2,3)))) \
- s1*t1*(Power(t1,4)*t2*(7 + t2) + 
            Power(t1,3)*(2 - (-16 + s + 21*s2)*t2 + 
               (-29 + 9*s + 5*s2)*Power(t2,2) - 5*Power(t2,3)) + 
            Power(t1,2)*(2 + (-65 + 32*s)*t2 - 
               2*Power(s2,2)*(-5 + t2)*t2 + (40 - 31*s)*Power(t2,2) + 
               (24 - 17*s)*Power(t2,3) + 7*Power(t2,4) + 
               s2*(-2 - (15 + 14*s)*t2 + (47 + 6*s)*Power(t2,2) + 
                  2*Power(t2,3))) + 
            t2*(2*Power(s2,2)*t2*(-6 + 9*t2 + Power(t2,2)) + 
               (-1 + t2)*(-24 + (14 - 8*s)*t2 + 
                  (55 - 24*s)*Power(t2,2) + (-9 + s)*Power(t2,3)) + 
               s2*(-24 + (10 - 8*s)*t2 + (29 + 2*s)*Power(t2,2) - 
                  (15 + 2*s)*Power(t2,3))) + 
            t1*t2*(12 + 80*t2 - 96*Power(t2,2) + 7*Power(t2,3) - 
               3*Power(t2,4) - 4*Power(s2,2)*(-3 + 7*t2) + 
               s2*(16 + 10*t2 - 35*Power(t2,2) - 7*Power(t2,3)) + 
               s*(-8 - 48*t2 + 57*Power(t2,2) + 7*Power(t2,3) - 
                  4*s2*(-2 - 3*t2 + Power(t2,2)))))))/
     ((-1 + s1)*(-1 + t1)*t1*Power(t1 - t2,2)*(1 - s2 + t1 - t2)*
       Power(-1 + t2,2)*t2) - (8*
       (2*Power(t1,5)*(-2 - s*(-2 + t2) + t2 + s1*t2) - 
         t2*Power(1 + t2,2)*Power(1 + (-1 + s)*t2,2)*
          (3 - 4*t2 + Power(t2,2)) + 
         Power(s2,5)*(-1 + t2)*t2*(-((-2 + t2)*t2) + s1*(2 + t2)) + 
         Power(t1,4)*(33 - t2 - 14*Power(t2,2) + 
            Power(s1,2)*t2*(-5 + Power(t2,2)) + 
            Power(s,2)*(2 - 4*t2 + Power(t2,2) + Power(t2,3)) - 
            s1*(-7 - 12*t2 + 8*Power(t2,2) + Power(t2,3)) - 
            s*(31 + 3*t2 - 11*Power(t2,2) + Power(t2,3) + 
               s1*(-2 - 9*t2 + Power(t2,2) + 2*Power(t2,3)))) + 
         t1*(Power(s,2)*t2*(-3 + 9*t2 + 8*Power(t2,2) - 
               17*Power(t2,3) - 3*Power(t2,4) + 2*Power(t2,5)) - 
            s*(3 + (9 - 13*s1)*t2 + 20*Power(t2,2) + 
               5*(1 + 4*s1)*Power(t2,3) - 2*(21 + s1)*Power(t2,4) + 
               (2 - 7*s1)*Power(t2,5) + (3 + 2*s1)*Power(t2,6)) + 
            Power(-1 + t2,2)*(-15 - 24*t2 - 8*Power(t2,2) + 
               8*Power(t2,3) - Power(t2,4) + 
               s1*(7 + 3*t2 - 9*Power(t2,2) - 3*Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(t1,3)*(15 - 62*t2 - 20*Power(t2,2) + 26*Power(t2,3) + 
            Power(t2,4) - Power(s1,2)*
             (5 - 13*t2 + 3*Power(t2,2) + Power(t2,3)) + 
            Power(s,2)*(1 - 3*t2 + Power(t2,2) - Power(t2,3) - 
               2*Power(t2,4)) + 
            s1*(37 - 58*t2 - 11*Power(t2,2) + 14*Power(t2,3) - 
               2*Power(t2,4)) + 
            s*(3*(-3 + 28*t2 + 4*Power(t2,2) - 8*Power(t2,3) + 
                  Power(t2,4)) + 
               s1*(-12 - 8*t2 + 3*Power(t2,2) - Power(t2,3) + 
                  2*Power(t2,4)))) + 
         Power(t1,2)*(Power(s,2)*
             (3 - 7*t2 - 5*Power(t2,2) + 16*Power(t2,3) + Power(t2,4)) - 
            (-1 + t2)*(-21 - 87*t2 - 2*Power(t2,2) + 23*Power(t2,3) - 
               Power(t2,4) - 
               s1*(-21 + 10*t2 + 20*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s1,2)*(-1 + 6*t2 - 4*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4))) + 
            s*(7 + 56*t2 - 82*Power(t2,2) - 47*Power(t2,3) + 
               23*Power(t2,4) - Power(t2,5) + 
               s1*(-6 - 12*t2 + 49*Power(t2,2) - 20*Power(t2,3) - 
                  5*Power(t2,4) + 2*Power(t2,5)))) + 
         Power(s2,3)*(Power(s1,2)*
             (2 + t1 - 4*t2 - 2*t1*t2 - 2*Power(t2,2) + 
               5*t1*Power(t2,2)) + 
            t1*(-2 - (31 - 5*s + Power(s,2))*t2 + 
               2*(-35 + 8*s)*Power(t2,2) + 
               (-7 - 4*s + Power(s,2))*Power(t2,3) + 
               (24 - 5*s)*Power(t2,4) + 2*Power(t2,5)) - 
            Power(t1,2)*(2 + t2 - 7*Power(t2,2) + 7*Power(t2,3) + 
               Power(t2,4) + s*(-2 + t2 + Power(t2,2) - 2*Power(t2,3))) \
+ t2*(-5 + 21*t2 + 32*Power(t2,2) + 4*Power(t2,3) - 11*Power(t2,4) - 
               Power(t2,5) + Power(s,2)*(t2 - Power(t2,3)) + 
               s*(5 - 2*t2 - 26*Power(t2,2) + 4*Power(t2,3) + 
                  5*Power(t2,4))) - 
            s1*(-4 - (5 + 4*s)*t2 + 5*Power(t2,2) - 
               7*(2 + s)*Power(t2,3) + 3*(1 + s)*Power(t2,4) - 
               5*Power(t2,5) + 
               Power(t1,2)*(6 + 7*t2 + 6*Power(t2,2) + Power(t2,3)) + 
               t1*(5 + 7*t2 - 3*Power(t2,2) + 3*Power(t2,3) + 
                  4*Power(t2,4) + s*(3 + t2 + 4*Power(t2,2))))) + 
         Power(s2,4)*(2*Power(s1,2)*(-1 + t2) - 
            s1*(t2*(-5 + s*Power(-1 + t2,2) + 7*t2 - 4*Power(t2,3)) + 
               t1*(-2 - 5*t2 + Power(t2,3))) - 
            t2*(-2 + (-12 + s)*t2 + (-11 + s)*Power(t2,2) + 
               (1 - 2*s)*Power(t2,3) + 2*Power(t2,4) + 
               t1*(s*(2 - 3*t2 + Power(t2,2)) - 
                  2*(2 - 2*t2 + Power(t2,3))))) + 
         Power(s2,2)*(-2 - 7*t2 - 12*s*t2 - 39*Power(t2,2) - 
            20*s*Power(t2,2) - 6*Power(s,2)*Power(t2,2) - 
            5*Power(t2,3) + 5*s*Power(t2,3) + 2*Power(s,2)*Power(t2,3) + 
            71*Power(t2,4) - 36*s*Power(t2,4) + 
            3*Power(s,2)*Power(t2,4) - 12*Power(t2,5) + 
            15*s*Power(t2,5) - 3*Power(s,2)*Power(t2,5) - 
            6*Power(t2,6) + 4*s*Power(t2,6) + 
            Power(t1,3)*(2 - 10*t2 + 4*Power(t2,2) + 4*Power(t2,3) - 
               s*(2 - 6*t2 + 5*Power(t2,2) + Power(t2,3))) - 
            Power(t1,2)*(-19 - 111*t2 - 56*Power(t2,2) + 
               49*Power(t2,3) + 17*Power(t2,4) + 
               Power(s,2)*(1 + 2*Power(t2,2) + Power(t2,3)) + 
               s*(4 + 52*t2 + 23*Power(t2,2) - 29*Power(t2,3) - 
                  2*Power(t2,4))) + 
            Power(s1,2)*(4*Power(-1 + t2,2)*t2 + 
               Power(t1,2)*(4 - 7*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               t1*(4 - 3*t2 + 11*Power(t2,2) - Power(t2,3) + 
                  Power(t2,4))) + 
            t1*(5 - 21*t2 - 68*Power(t2,2) - 84*Power(t2,3) + 
               27*Power(t2,4) + 21*Power(t2,5) + 
               Power(s,2)*t2*(7 - 2*t2 - Power(t2,2) + 4*Power(t2,3)) + 
               s*(-1 + 15*t2 + 94*Power(t2,2) + 30*Power(t2,3) - 
                  39*Power(t2,4) - 5*Power(t2,5))) + 
            s1*(-4 - (11 + 3*s)*t2 + (34 + 15*s)*Power(t2,2) - 
               2*(11 + 4*s)*Power(t2,3) + (4 + 7*s)*Power(t2,4) - 
               3*(1 + s)*Power(t2,5) + 2*Power(t2,6) + 
               Power(t1,3)*(6 + 9*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(14 + s + 13*t2 + 11*s*t2 + 15*Power(t2,2) + 
                  12*s*Power(t2,2) - 5*Power(t2,3) - Power(t2,4)) - 
               t1*(11 - 28*t2 + 55*Power(t2,2) + 28*Power(t2,3) - 
                  10*Power(t2,4) + 4*Power(t2,5) + 
                  s*(-4 + 13*t2 + 30*Power(t2,2) - 10*Power(t2,3) + 
                     3*Power(t2,4))))) + 
         s2*(2 + (15 + 7*s - s1)*t2 + 
            (5*Power(s,2) + s*(19 - 7*s1) + 3*(-4 + s1))*Power(t2,2) + 
            (-25 + Power(s,2) - 2*s*(-5 + s1) - 2*s1)*Power(t2,3) + 
            (6 - 4*Power(s,2) - 2*s1 + s*(-15 + 8*s1))*Power(t2,4) + 
            (25 + 5*Power(s,2) + 2*s*(-17 + s1) + 3*s1)*Power(t2,5) - 
            (12 + 3*Power(s,2) + s*(-12 + s1) + s1)*Power(t2,6) + 
            (1 + s)*Power(t2,7) - 
            Power(t1,4)*(-4 - 5*t2 + 5*Power(t2,2) + 
               s*(4 + t2 - 3*Power(t2,2)) + s1*(2 + 7*t2 + 3*Power(t2,2))\
) + Power(t1,3)*(-52 - 81*t2 + 28*Power(t2,2) + 29*Power(t2,3) + 
               Power(s1,2)*(-3 + 12*t2 + 5*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(s,2)*(-1 + t2 + 5*Power(t2,2) - Power(t2,3)) + 
               s1*(-16 - 23*t2 - 3*Power(t2,2) + 9*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(37 + (56 - 18*s1)*t2 - 3*(7 + 3*s1)*Power(t2,2) + 
                  (-13 + 3*s1)*Power(t2,3) + Power(t2,4))) - 
            Power(t1,2)*(Power(s1,2)*
                (1 + 6*t2 + 6*Power(t2,2) - 2*Power(t2,3) + Power(t2,4)) \
+ Power(s,2)*(2 - 7*t2 - Power(t2,2) + 9*Power(t2,3) + Power(t2,4)) + 
               t2*(-21 - 142*t2 - 4*Power(t2,2) + 46*Power(t2,3) + 
                  Power(t2,4)) + 
               s1*(27 - 14*t2 - 85*Power(t2,2) + 6*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s*(15 + 65*t2 + 114*Power(t2,2) - 22*Power(t2,3) - 
                  27*Power(t2,4) + Power(t2,5) - 
                  s1*(15 + 21*t2 - 3*Power(t2,2) + 4*Power(t2,3) + 
                     3*Power(t2,4)))) + 
            t1*(Power(s,2)*t2*
                (-3 - 7*t2 + 2*Power(t2,2) - Power(t2,3) + 5*Power(t2,4)) \
- (-1 + t2)*(2*(5 + 36*t2 + 69*Power(t2,2) - 8*Power(t2,3) - 
                     14*Power(t2,4)) - 
                  Power(s1,2)*(7 - 6*t2 - 2*Power(t2,3) + Power(t2,4)) + 
                  s1*(11 - 32*t2 + 21*Power(t2,2) + 15*Power(t2,3) - 
                     8*Power(t2,4) + Power(t2,5))) - 
               s*(-4 - 11*t2 + 47*Power(t2,2) - 122*Power(t2,3) - 
                  28*Power(t2,4) + 29*Power(t2,5) + Power(t2,6) + 
                  s1*(1 + 19*t2 - 30*Power(t2,2) + 36*Power(t2,3) - 
                     15*Power(t2,4) + 5*Power(t2,5))))))*
       B1(1 - s2 + t1 - t2,t2,t1))/
     ((-1 + s1)*(-1 + t1)*(1 - s2 + t1 - t2)*(-1 + t2)*Power(t1 - s2*t2,2)) \
+ (16*(Power(s2,5)*t1*(t1 - t2)*t2*
          (Power(t1,2)*(-2 + s1 + t2) + 
            t2*(2 - (-2 + s1)*t2 + Power(t2,2)) + 
            2*t1*(1 + Power(s1,2) + t2 - Power(t2,2) - s1*(3 + t2))) + 
         Power(s2,4)*(-(Power(t2,3)*(-1 + t2 + Power(t2,2))) - 
            Power(t1,5)*(-2 - (6 + s)*t2 + 4*Power(t2,2) + 
               s1*(1 + t2)) + 
            Power(t1,4)*(-2 - 2*(9 + s)*t2 - 2*Power(s1,2)*t2 - 
               (21 + 2*s)*Power(t2,2) + 16*Power(t2,3) + 
               s1*(6 + (29 + 2*s)*t2 + 11*Power(t2,2))) + 
            Power(t1,2)*t2*(-3 - 4*t2 + 2*(5 + s)*Power(t2,2) + 
               (1 - 2*s)*Power(t2,3) + 14*Power(t2,4) + 
               Power(s1,2)*(-6 + t2 - 5*Power(t2,2)) + 
               s1*(10 + 4*(2 + s)*t2 + (19 - 2*s)*Power(t2,2) + 
                  Power(t2,3))) + 
            t1*Power(t2,2)*(-4 + t2 - 2*s*t2 - 4*Power(t2,2) + 
               (-4 + s)*Power(t2,3) - 3*Power(t2,4) + 
               s1*(2 - Power(t2,2) + 3*Power(t2,3))) - 
            Power(t1,3)*t2*(-6 + Power(s1,2)*(1 - 13*t2) - 17*t2 - 
               18*Power(t2,2) + 23*Power(t2,3) + 
               2*s1*(7 + 29*t2 + 7*Power(t2,2)) + 
               s*(4*s1 - 2*(1 + Power(t2,2))))) - 
         Power(s2,2)*(Power(t2,4)*
             (2 - 5*t2 + Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,7)*(-3 + 5*t2 + 4*s1*(1 + t2) - s*(5 + 4*t2)) + 
            t1*Power(t2,2)*(-1 + 15*Power(t2,2) - 
               (17 + s)*Power(t2,3) - 17*Power(t2,4) - 
               (4 + 3*s)*Power(t2,5) + Power(t2,6) + 
               s1*t2*(4 - 10*t2 + 7*Power(t2,2) + 3*Power(t2,3) - 
                  Power(t2,4))) - 
            Power(t1,4)*(3 - 53*t2 - 131*Power(t2,2) - 
               232*Power(t2,3) + 25*Power(t2,4) + 44*Power(t2,5) + 
               6*Power(s,2)*Power(t2,2)*(4 + t2) - 
               Power(s1,2)*(7 - 11*t2 + 26*Power(t2,2) + 
                  13*Power(t2,3)) + 
               s*(2 - 4*s1*Power(1 - 4*t2,2) - 10*t2 + 
                  30*Power(t2,2) + 39*Power(t2,3) - 35*Power(t2,4)) + 
               s1*(4 + 42*t2 + 279*Power(t2,2) + 87*Power(t2,3) + 
                  36*Power(t2,4))) + 
            Power(t1,2)*t2*(4 + 2*(-18 + s)*t2 - 
               (9 + 26*s)*Power(t2,2) + 
               (48 + 7*s - 6*Power(s,2))*Power(t2,3) + 
               (33 - 5*s + Power(s,2))*Power(t2,4) + 
               2*(6 + 5*s)*Power(t2,5) - 12*Power(t2,6) + 
               Power(s1,2)*t2*
                (-12 + 20*t2 - 14*Power(t2,2) + Power(t2,3)) + 
               s1*(-2 + 34*t2 + 8*s*Power(t2,2) - 22*Power(t2,3) + 
                  (3 + 6*s)*Power(t2,4) + Power(t2,5))) + 
            Power(t1,3)*(3 - 11*t2 + 2*(-21 + 8*s)*Power(t2,2) + 
               (6 + 26*s + 20*Power(s,2))*Power(t2,3) + 
               3*(-33 + 8*s)*Power(t2,4) - 3*(3 + 7*s)*Power(t2,5) + 
               35*Power(t2,6) + 
               Power(s1,2)*(6 - 43*t2 - 7*Power(t2,2) + 
                  33*Power(t2,3) - 16*Power(t2,4)) + 
               s1*(-10 + (62 - 4*s)*t2 + (35 + 22*s)*Power(t2,2) + 
                  (20 - 48*s)*Power(t2,3) + (38 - 6*s)*Power(t2,4) + 
                  17*Power(t2,5))) + 
            Power(t1,6)*(25 + Power(s1,2)*(-3 + t2) + 75*t2 - 
               27*Power(t2,2) - 6*Power(t2,3) - Power(s,2)*(2 + 3*t2) - 
               s1*(25 + 4*t2 + 17*Power(t2,2)) + 
               s*(3 + 8*t2 + 19*Power(t2,2) + 2*s1*(2 + t2))) + 
            Power(t1,5)*(-12 + (-146 - 5*s + 12*Power(s,2))*t2 + 
               (-230 + 17*s + 8*Power(s,2))*Power(t2,2) + 
               (48 - 36*s)*Power(t2,3) + 26*Power(t2,4) + 
               Power(s1,2)*(-2 + 6*t2 - 5*Power(t2,2)) + 
               s1*(17 + 203*t2 + 55*Power(t2,2) + 32*Power(t2,3) - 
                  2*s*(-1 + 10*t2 + Power(t2,2))))) + 
         Power(s2,3)*(-(Power(t2,3)*
               (1 - 4*t2 + 2*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,6)*(-7 + s1 + t2 + 4*s1*t2 + 3*Power(t2,2) - 
               s*(1 + 5*t2)) + 
            Power(t1,5)*(14 + 49*t2 + 11*Power(t2,2) - 19*Power(t2,3) + 
               Power(s1,2)*(-3 + 2*t2) - 
               2*s1*(13 + 18*t2 + 8*Power(t2,2)) + 
               s*(2 - 2*s1*(-1 + t2) + 5*t2 + 18*Power(t2,2))) + 
            Power(t1,2)*t2*(7 + 2*(-2 + s)*t2 - 
               (31 + 12*s)*Power(t2,2) + (3 + 2*s)*Power(t2,3) - 
               2*(3 + 4*s)*Power(t2,4) + 21*Power(t2,5) + 
               Power(s1,2)*(6 - 19*t2 + 6*Power(t2,2) - 
                  4*Power(t2,3)) - 
               s1*(12 + 2*(-9 + 2*s)*t2 - 10*(3 + s)*Power(t2,2) + 
                  2*(-8 + 3*s)*Power(t2,3) + Power(t2,4))) + 
            t1*Power(t2,2)*(3 + (-11 + 2*s)*t2 - 
               2*(-6 + s)*Power(t2,2) + 7*Power(t2,3) + 
               3*s*Power(t2,4) - 3*Power(t2,5) + 
               s1*(-2 + 6*t2 - 4*Power(t2,2) - 3*Power(t2,3) + 
                  3*Power(t2,4))) + 
            Power(t1,3)*(3 + (5 - 2*s)*t2 + 8*(3 + 2*s)*Power(t2,2) + 
               (68 + 7*s)*Power(t2,3) + 5*(7 + 3*s)*Power(t2,4) - 
               46*Power(t2,5) + 
               Power(s1,2)*(6 + 8*t2 - 5*Power(t2,2) + 
                  24*Power(t2,3)) - 
               s1*(10 + 4*t2 + 2*(30 + s)*Power(t2,2) + 
                  (119 - 2*s)*Power(t2,3) + 25*Power(t2,4))) + 
            Power(t1,4)*(-6 - 29*t2 - 105*Power(t2,2) - 41*Power(t2,3) + 
               44*Power(t2,4) - 
               Power(s1,2)*(1 + 10*t2 + 10*Power(t2,2)) + 
               s1*(4 + 84*t2 + 117*Power(t2,2) + 35*Power(t2,3)) - 
               s*(2 + 4*t2 + 13*Power(t2,2) + 23*Power(t2,3) - 
                  2*s1*(2 - 5*t2 + 3*Power(t2,2))))) + 
         s2*((2 - 4*s + 4*s1)*Power(t1,8) - 
            Power(t2,5)*(1 - 2*t2 + Power(t2,3)) + 
            t1*Power(t2,3)*(6 - (9 + 2*s1)*t2 - 
               2*(6 + s - 3*s1)*Power(t2,2) + 
               (7 + s - 3*s1)*Power(t2,3) - (-8 + s1)*Power(t2,4) + 
               (2 + s)*Power(t2,5)) + 
            Power(t1,2)*Power(t2,2)*
             (-20 + (39 - 8*s)*t2 + 
               (7 + 3*s - 8*Power(s,2))*Power(t2,2) + 
               (-28 - 17*s + 9*Power(s,2))*Power(t2,3) + 
               (-35 + 6*s - 2*Power(s,2))*Power(t2,4) - 
               (5 + 4*s)*Power(t2,5) + 2*Power(t2,6) + 
               3*Power(s1,2)*t2*(2 - 5*t2 + 3*Power(t2,2)) + 
               s1*(12 - 28*t2 + (17 + 12*s)*Power(t2,2) + 
                  (11 - 6*s)*Power(t2,3) - 2*(3 + s)*Power(t2,4))) + 
            Power(t1,3)*t2*(-34 - (43 + 20*s)*t2 + 
               5*(10 + 7*s + 4*Power(s,2))*Power(t2,2) + 
               (13 + 17*s - 24*Power(s,2))*Power(t2,3) + 
               (91 - 21*s + 3*Power(s,2))*Power(t2,4) + 
               8*(-1 + s)*Power(t2,5) - 9*Power(t2,6) + 
               Power(s1,2)*(-36 + 48*t2 - 25*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(68 + 6*(1 + 4*s)*t2 - (57 + 58*s)*Power(t2,2) + 
                  (-3 + 48*s)*Power(t2,3) + (17 + 2*s)*Power(t2,4) - 
                  3*Power(t2,5))) + 
            Power(t1,4)*(-12 + 2*(5 + 12*s)*t2 + 
               (29 - 11*s - 12*Power(s,2))*Power(t2,2) + 
               (-70 + 27*s + 22*Power(s,2))*Power(t2,3) + 
               2*(-85 + 14*s + 2*Power(s,2))*Power(t2,4) + 
               (50 - 14*s)*Power(t2,5) + 15*Power(t2,6) - 
               Power(s1,2)*(24 + 46*t2 - 55*Power(t2,2) + 
                  6*Power(t2,4)) + 
               s1*(40 + (12 - 8*s)*t2 + (-37 + 14*s)*Power(t2,2) + 
                  (135 - 70*s)*Power(t2,3) - 6*Power(t2,4) + 
                  11*Power(t2,5))) - 
            Power(t1,7)*(-48 - 22*t2 + 16*Power(t2,2) + 
               Power(s1,2)*(2 + t2) + Power(s,2)*(4 + t2) + 
               s1*(-12 + 5*t2 + 4*Power(t2,2)) - 
               s*(-10 + 11*t2 + 4*Power(t2,2) + 2*s1*(3 + t2))) + 
            Power(t1,6)*(-62 - 198*t2 - 106*Power(t2,2) + 
               59*Power(t2,3) + 3*Power(t2,4) + 
               Power(s1,2)*(18 + 5*t2) + 
               Power(s,2)*(4 + 9*t2 + 6*Power(t2,2)) + 
               s1*(90 + 104*t2 - 4*Power(t2,2) + 13*Power(t2,3)) - 
               2*s*(-6 - 21*t2 + 3*Power(t2,2) + 7*Power(t2,3) + 
                  3*s1*(3 + 2*t2 + Power(t2,2)))) + 
            Power(t1,5)*(16 + 46*t2 + 216*Power(t2,2) + 
               189*Power(t2,3) - 82*Power(t2,4) - 11*Power(t2,5) - 
               2*Power(s,2)*t2*(2 + 6*t2 + 5*Power(t2,2)) + 
               Power(s1,2)*(4 + 2*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               s1*(-12 - 139*t2 - 232*Power(t2,2) + Power(t2,3) - 
                  17*Power(t2,4)) + 
               s*(4 - 37*t2 - 60*Power(t2,2) - 14*Power(t2,3) + 
                  19*Power(t2,4) + 
                  2*s1*(-8 + 25*t2 + 17*Power(t2,2) + 2*Power(t2,3))))) + 
         t1*(Power(t2,4)*(1 - 2*t2 + Power(t2,3)) + 
            t1*Power(t2,2)*(-4 + 2*(2 + s1)*t2 + 
               2*(8 + s - 3*s1)*Power(t2,2) + 
               (-11 + 5*s + 3*s1)*Power(t2,3) + 
               (-3 - 8*s + 3*Power(s,2) + s1)*Power(t2,4) + 
               (-3 + s - Power(s,2))*Power(t2,5)) + 
            2*Power(t1,7)*(Power(s,2) + Power(s1,2) + 5*(-3 + t2) + 
               2*s1*(-3 + t2) - 2*s*(-2 + s1 + t2)) + 
            Power(t1,2)*t2*(16 + (-27 + 8*s)*t2 + 
               (-15 - 14*s + 8*Power(s,2))*Power(t2,2) + 
               (15 + 13*s - 14*Power(s,2))*Power(t2,3) + 
               (25 + 9*s - 8*Power(s,2))*Power(t2,4) + 
               (7 + 3*Power(s,2))*Power(t2,5) - 2*Power(t2,6) - 
               3*Power(s1,2)*t2*(2 - 5*t2 + 3*Power(t2,2)) + 
               s1*(-8 + 18*t2 - 4*(4 + 3*s)*Power(t2,2) + 
                  6*Power(t2,3) + 5*(-1 + 2*s)*Power(t2,4) - 
                  2*(-1 + s)*Power(t2,5))) - 
            Power(t1,3)*(-12 - 28*t2 + 
               (25 + 4*s + 24*Power(s,2))*Power(t2,2) + 
               (-30 + 45*s - 44*Power(s,2))*Power(t2,3) + 
               (78 - 23*s - 8*Power(s,2))*Power(t2,4) + 
               (3 + 8*s + 2*Power(s,2))*Power(t2,5) - 9*Power(t2,6) + 
               Power(s1,2)*(-24 + 28*t2 + 11*Power(t2,2) - 
                  19*Power(t2,3) - 2*Power(t2,4) + Power(t2,5)) + 
               s1*(40 + 8*(-1 + 2*s)*t2 - 2*(13 + 34*s)*Power(t2,2) + 
                  (5 + 38*s)*Power(t2,3) + 4*(1 + 5*s)*Power(t2,4) - 
                  (1 + 4*s)*Power(t2,5))) - 
            Power(t1,6)*(-32 - 110*t2 + 33*Power(t2,2) + 3*Power(t2,3) + 
               Power(s1,2)*(14 - 2*t2 + Power(t2,2)) + 
               Power(s,2)*(2 + 8*t2 + Power(t2,2)) + 
               s1*(78 - 34*t2 + 13*Power(t2,2)) - 
               2*s*(-3 - 14*t2 + 7*Power(t2,2) + 
                  s1*(6 + 3*t2 + Power(t2,2)))) + 
            Power(t1,5)*(22 - 36*t2 - 167*Power(t2,2) + 38*Power(t2,3) + 
               11*Power(t2,4) + 
               Power(s1,2)*(-16 + 22*t2 - 6*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(-8 + 20*t2 + 11*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s1*(-26 + 178*t2 - 41*Power(t2,2) + 19*Power(t2,3)) + 
               s*(-6 + 4*t2 + 49*Power(t2,2) - 21*Power(t2,3) + 
                  s1*(24 - 58*t2 - 4*Power(t2,3)))) - 
            Power(t1,4)*(12 + 20*t2 + 24*Power(t2,2) - 142*Power(t2,3) + 
               16*Power(t2,4) + 15*Power(t2,5) + 
               2*Power(s,2)*t2*
                (-12 + 24*t2 + 4*Power(t2,2) + Power(t2,3)) - 
               Power(s1,2)*(28 - 12*t2 - 12*Power(t2,2) + Power(t2,4)) + 
               s1*(16 - 70*t2 + 116*Power(t2,2) - 27*Power(t2,3) + 
                  13*Power(t2,4)) - 
               s*(-8 + 22*t2 + 29*Power(t2,2) - 53*Power(t2,3) + 
                  18*Power(t2,4) + 
                  4*s1*(4 - 20*t2 + 21*Power(t2,2) + 2*Power(t2,3))))))*
       R1(t1))/((-1 + s1)*(-1 + t1)*t1*Power(t1 - t2,3)*(1 - s2 + t1 - t2)*
       (-1 + t2)*(t1 - s2*t2)*(-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) \
- (16*(Power(t1,6)*(1 + (-9 + 4*s)*t2 + (15 - 2*s)*Power(t2,2) + 
            (-3 + 2*s)*Power(t2,3)) + 
         Power(t1,5)*(1 + (-11 + 5*s)*t2 + 
            (41 - 18*s + 2*Power(s,2))*Power(t2,2) + 
            (-50 + 16*s + Power(s,2))*Power(t2,3) + 
            (6 - 7*s + Power(s,2))*Power(t2,4) + Power(t2,5) - 
            s2*(1 + 2*(-5 + 2*s)*t2 + 2*(4 + s)*Power(t2,2) + 
               18*Power(t2,3) + (-5 + 2*s)*Power(t2,4))) + 
         Power(t2,3)*(Power(t2,2)*Power(1 + (-1 + s)*t2,2)*
             (3 - 4*t2 + Power(t2,2)) + 
            Power(s2,3)*t2*(4 - 5*t2 - 4*Power(t2,2) + 11*Power(t2,3) - 
               2*Power(t2,4)) + 
            Power(s2,2)*(6 + 4*(-2 + s)*t2 + (5 - 9*s)*Power(t2,2) + 
               2*(-2 + s)*Power(t2,3) - 3*(3 + 2*s)*Power(t2,4) + 
               (12 + s)*Power(t2,5) - 2*Power(t2,6)) + 
            s2*(-6 - 4*(-3 + s)*t2 + (-7 + 9*s)*Power(t2,2) + 
               (2 - 9*s + 2*Power(s,2))*Power(t2,3) + 
               (-6 + 20*s - 7*Power(s,2))*Power(t2,4) + 
               (10 - 17*s + Power(s,2))*Power(t2,5) + 
               (-5 + s)*Power(t2,6))) + 
         Power(t1,3)*t2*(-1 - 3*(-4 + s)*t2 - 
            2*(34 - 13*s + 6*Power(s,2))*Power(t2,2) + 
            2*(64 - 39*s + 12*Power(s,2))*Power(t2,3) + 
            (-86 + 87*s + 10*Power(s,2))*Power(t2,4) + 
            2*(4 - 6*s + Power(s,2))*Power(t2,5) + 3*Power(t2,6) + 
            2*Power(s2,3)*t2*(1 - t2 - 3*Power(t2,2) + Power(t2,3)) + 
            s2*(1 + (-25 + 2*s)*t2 + 
               4*(2 + 2*s + Power(s,2))*Power(t2,2) + 
               (99 - 14*s + 4*Power(s,2))*Power(t2,3) + 
               2*(-68 + 19*s + 4*Power(s,2))*Power(t2,4) - 
               2*(-7 + 5*s)*Power(t2,5) + 3*Power(t2,6)) + 
            Power(s2,2)*t2*(s*
                (1 - 12*t2 + 16*Power(t2,2) - 9*Power(t2,3)) + 
               t2*(24 - 51*t2 - 16*Power(t2,2) + 7*Power(t2,3)))) - 
         Power(t1,4)*t2*(2 - 40*t2 + 102*Power(t2,2) - 83*Power(t2,3) + 
            4*Power(t2,4) + 3*Power(t2,5) + 
            Power(s2,2)*(1 + 9*t2 - 23*Power(t2,2) - Power(t2,3) + 
               2*Power(t2,4)) + 
            s2*(-2 + 13*t2 + 11*Power(t2,2) - 72*Power(t2,3) + 
               13*Power(t2,4) + Power(t2,5)) + 
            Power(s,2)*t2*(-4 + 13*t2 + 4*Power(t2,2) + 3*Power(t2,3) + 
               s2*(2 - t2 + 3*Power(t2,2))) + 
            s*(-1 + 20*t2 - 52*Power(t2,2) + 57*Power(t2,3) - 
               12*Power(t2,4) - 
               2*Power(s2,2)*t2*(2 - t2 + Power(t2,2)) + 
               s2*(1 - 7*t2 - 2*Power(t2,2) + 7*Power(t2,3) - 
                  7*Power(t2,4)))) + 
         t1*Power(t2,2)*(6 + 2*(-7 + 2*s)*t2 + (8 - 13*s)*Power(t2,2) + 
            (8 + s - 4*Power(s,2))*Power(t2,3) - 
            2*(5 - 6*s + Power(s,2))*Power(t2,4) + 
            (-2 - 7*s + 13*Power(s,2))*Power(t2,5) + 
            (4 + 3*s - 3*Power(s,2))*Power(t2,6) + 
            2*Power(s2,3)*t2*
             (-2 + 3*t2 + 3*Power(t2,2) - 13*Power(t2,3) + 
               3*Power(t2,4)) + 
            s2*(-6 + 9*t2 + 6*(1 + s)*Power(t2,2) + 
               (-38 + 4*s - 4*Power(s,2))*Power(t2,3) + 
               (71 - 48*s + 20*Power(s,2))*Power(t2,4) + 
               (-56 + 58*s)*Power(t2,5) + (13 - 4*s)*Power(t2,6) + 
               Power(t2,7)) + 
            Power(s2,2)*t2*(-7 - 25*t2 + 55*Power(t2,2) + 
               2*Power(t2,3) - 44*Power(t2,4) + 7*Power(t2,5) + 
               s*(-4 + 7*t2 + 4*Power(t2,2) + 20*Power(t2,3) - 
                  7*Power(t2,4)))) + 
         Power(s1,2)*(-1 + t2)*
          (Power(t1,5)*t2*(3 - 2*t2 + Power(t2,2)) - 
            Power(t1,4)*(-1 + 4*Power(t2,2) + 2*Power(t2,3) + 
               Power(t2,4) + s2*t2*(4 - t2 + Power(t2,2))) + 
            s2*Power(t2,3)*(3 - 7*t2 + Power(s2,2)*(-1 + t2)*t2 + 
               9*Power(t2,2) - 5*Power(t2,3) + 
               s2*(-3 + 3*t2 - 3*Power(t2,2) + Power(t2,3))) + 
            Power(t1,2)*t2*(-4 + 6*t2 - Power(s2,3)*(-1 + t2)*t2 + 
               4*Power(t2,2) - 5*Power(t2,3) - 4*Power(t2,4) + 
               Power(t2,5) + 
               Power(s2,2)*(1 - 6*t2 + 3*Power(t2,2) - 4*Power(t2,3)) - 
               s2*(-3 - 3*t2 + 17*Power(t2,2) + Power(t2,4))) - 
            t1*Power(t2,2)*(3 - 7*t2 + 9*Power(t2,2) - 5*Power(t2,3) + 
               Power(s2,2)*(4 - 8*t2 - 6*Power(t2,2) + 4*Power(t2,3)) + 
               s2*(-7 + 9*t2 + 4*Power(t2,2) - 11*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,3)*(1 - 8*t2 + 11*Power(t2,2) - 5*Power(t2,3) + 
               8*Power(t2,4) - Power(t2,5) + 
               Power(s2,2)*(t2 + Power(t2,3)) + 
               s2*(-1 + t2 + 4*Power(t2,2) + 5*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(t1,2)*Power(t2,2)*
          (3 - 22*t2 + 52*Power(t2,2) - 66*Power(t2,3) + 
            46*Power(t2,4) - 12*Power(t2,5) - Power(t2,6) + 
            2*Power(s,2)*Power(t2,2)*
             (6 - (7 + 9*s2)*t2 - (8 + 3*s2)*Power(t2,2) + Power(t2,3)) \
- 3*Power(s2,3)*(t2 - 7*Power(t2,3) + 2*Power(t2,4)) + 
            Power(s2,2)*(3 + 21*t2 - 54*Power(t2,2) + 29*Power(t2,3) + 
               47*Power(t2,4) - 10*Power(t2,5)) + 
            s2*(2 + 25*t2 + 7*Power(t2,2) - 133*Power(t2,3) + 
               128*Power(t2,4) - 14*Power(t2,5) - 3*Power(t2,6)) + 
            s*(-4 + 15*t2 - 18*Power(t2,2) + 42*Power(t2,3) - 
               47*Power(t2,4) + 4*Power(t2,5) + 
               Power(s2,2)*t2*
                (1 + 2*t2 - 28*Power(t2,2) + 13*Power(t2,3)) + 
               2*s2*(2 - 8*t2 - 3*Power(t2,2) + 21*Power(t2,3) - 
                  36*Power(t2,4) + 4*Power(t2,5)))) - 
         s1*(2*Power(t1,6)*t2*(3 - 2*t2 + Power(t2,2)) + 
            Power(t1,5)*(-1 - (-16 + s + 10*s2)*t2 + 
               (-47 + 5*s - 2*s2)*Power(t2,2) + 
               (26 - 2*s + 2*s2)*Power(t2,3) + 
               2*(-3 + s - s2)*Power(t2,4)) + 
            s2*Power(t2,3)*(Power(s2,2)*t2*
                (3 + t2 - 3*Power(t2,2) + 3*Power(t2,3)) + 
               (-1 + t2)*(10 + 2*(-9 + s)*t2 + (10 - 3*s)*Power(t2,2) + 
                  (7 - 5*s)*Power(t2,3) - 2*(5 + s)*Power(t2,4) + 
                  Power(t2,5)) + 
               s2*(10 + (-17 + 2*s)*t2 + (4 - 7*s)*Power(t2,2) + 
                  (7 + 3*s)*Power(t2,3) - 2*(4 + s)*Power(t2,4) + 
                  4*Power(t2,5))) + 
            Power(t1,4)*(-1 + (13 - 3*s)*t2 + (-55 + 13*s)*Power(t2,2) + 
               (98 - 16*s)*Power(t2,3) - 2*(27 + s)*Power(t2,4) + 
               (11 - 4*s)*Power(t2,5) + 
               2*Power(s2,2)*t2*
                (2 + 5*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               s2*(1 + (-16 + s)*t2 + (44 - 8*s)*Power(t2,2) + 
                  (22 + 3*s)*Power(t2,3) - (21 + 4*s)*Power(t2,4) + 
                  6*Power(t2,5))) + 
            t1*Power(t2,2)*(-4*Power(s2,3)*Power(t2,2)*
                (4 - 2*t2 + Power(t2,2)) + 
               Power(s2,2)*(2 - 10*t2 + (-2 + 5*s)*Power(t2,2) + 
                  5*(-1 + s)*Power(t2,3) + 2*(9 + s)*Power(t2,4) - 
                  15*Power(t2,5)) - 
               (-1 + t2)*(10 + 2*(-9 + s)*t2 + (16 - 3*s)*Power(t2,2) + 
                  (-7 + s)*Power(t2,3) - 10*s*Power(t2,4) + 
                  (-1 + 2*s)*Power(t2,5)) + 
               s2*(-12 - 2*(-6 + s)*t2 + (19 + 8*s)*Power(t2,2) - 
                  2*(5 + 13*s)*Power(t2,3) + (-30 + 22*s)*Power(t2,4) + 
                  (26 + 6*s)*Power(t2,5) - 5*Power(t2,6))) + 
            Power(t1,2)*t2*(2 + t2 - (21 + 5*s)*Power(t2,2) + 
               (23 + 15*s)*Power(t2,3) + 6*(1 + 2*s)*Power(t2,4) - 
               2*(7 + 15*s)*Power(t2,5) + (3 + 4*s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (-3 + 19*t2 - 5*Power(t2,2) + Power(t2,3)) + 
               s2*(-2 + (11 + 2*s)*t2 + (4 - 6*s)*Power(t2,2) + 
                  7*(-5 + 2*s)*Power(t2,3) + (53 - 24*s)*Power(t2,4) - 
                  2*(14 + 5*s)*Power(t2,5) + 9*Power(t2,6)) + 
               Power(s2,2)*t2*
                (1 - 4*t2 + 67*Power(t2,2) - 42*Power(t2,3) + 
                  14*Power(t2,4) - 
                  s*(2 + t2 + 7*Power(t2,2) + 2*Power(t2,3)))) + 
            Power(t1,3)*t2*(4 - 25*t2 - 4*Power(s2,3)*t2 + 
               63*Power(t2,2) - 82*Power(t2,3) + 47*Power(t2,4) - 
               11*Power(t2,5) + 
               Power(s2,2)*(2 - 2*t2 - 55*Power(t2,2) + 24*Power(t2,3) - 
                  5*Power(t2,4)) - 
               s2*(4 - 21*t2 + 40*Power(t2,2) + 36*Power(t2,3) - 
                  32*Power(t2,4) + 9*Power(t2,5)) + 
               s*(-2 + 13*t2 - 31*Power(t2,2) + 10*Power(t2,3) + 
                  22*Power(t2,4) + 
                  Power(s2,2)*t2*(3 - t2 + 2*Power(t2,2)) + 
                  2*s2*(1 - 4*t2 + 9*Power(t2,2) + Power(t2,3) + 
                     5*Power(t2,4))))))*R1(t2))/
     ((-1 + s1)*(-1 + t1)*Power(t1 - t2,3)*(1 - s2 + t1 - t2)*
       Power(-1 + t2,3)*t2*(t1 - s2*t2)) + 
    (8*(-3*Power(s2,5)*t2*(-2 + s1 + t2) + 
         2*(-3 + t2)*Power(t2,2)*Power(1 + (-1 + s)*t2,2) - 
         4*Power(t1,4)*(-14 + Power(s,2) + Power(s1,2) + 
            s*(5 - 2*s1 - 2*t2) + 5*t2 + s1*(-7 + 2*t2)) + 
         t1*(-16 - 30*t2 + 95*Power(t2,2) - 34*Power(t2,3) + 
            Power(t2,4) + 2*Power(s1,2)*t2*(-4 + 5*t2) + 
            Power(s,2)*t2*(-8 + 28*t2 + Power(t2,2) - 2*Power(t2,3)) - 
            2*s1*(-8 - 3*t2 + 23*Power(t2,2) - 4*Power(t2,3) + 
               Power(t2,4)) + 
            s*(-16 + 2*(19 + 8*s1)*t2 + (-81 + 10*s1)*Power(t2,2) + 
               (26 - 20*s1)*Power(t2,3) + (-3 + 4*s1)*Power(t2,4))) + 
         Power(s2,4)*(2*Power(s1,2)*(-2 + t2) + 
            t2*(9 - 6*s + 30*t2 - 11*Power(t2,2)) + 
            t1*(-6 - 14*t2 - 3*s*t2 + 11*Power(t2,2)) + 
            s1*((-9 + 3*s - 11*t2)*t2 + t1*(3 + 8*t2))) + 
         Power(t1,2)*(-168 + 36*t2 + 22*Power(t2,2) + 14*Power(t2,3) - 
            6*Power(t2,4) + s1*
             (124 - 42*t2 + 33*Power(t2,2) - 7*Power(t2,3)) + 
            Power(s,2)*(8 - 30*t2 + Power(t2,2) - 2*Power(t2,3)) + 
            2*Power(s1,2)*(-4 - 3*t2 - 4*Power(t2,2) + Power(t2,3)) + 
            s*(144 - 30*t2 - 28*Power(t2,2) + 8*Power(t2,3) + 
               s1*(-96 + 80*t2 - 3*Power(t2,2)))) + 
         Power(t1,3)*(32 - 98*t2 + 13*Power(t2,2) + 6*Power(t2,3) + 
            2*Power(s,2)*t2*(5 + t2) + 
            2*Power(s1,2)*(14 - 5*t2 + Power(t2,2)) + 
            s1*(8 - 16*t2 + 5*Power(t2,2)) + 
            s*(36 + 16*t2 - 7*Power(t2,2) - 4*s1*(5 + Power(t2,2)))) + 
         Power(s2,3)*(-2*Power(s1,2)*(t1*(-4 + t2) + (9 - 2*t2)*t2) + 
            Power(t1,2)*(17 - 8*t2 - 6*Power(t2,2) + s*(3 + 13*t2)) + 
            t1*(-9 - 84*t2 + 3*Power(s,2)*t2 - 45*Power(t2,2) + 
               28*Power(t2,3) + s*(6 + 14*t2 - 10*Power(t2,2))) + 
            t2*(-17 + 3*Power(s,2)*(-2 + t2) + 36*t2 + 43*Power(t2,2) - 
               13*Power(t2,3) + s*(21 - 34*t2 + 3*Power(t2,2))) + 
            s1*(8 + 2*(13 + s)*t2 + 2*(-3 + 5*s)*Power(t2,2) - 
               15*Power(t2,3) - Power(t1,2)*(8 + 11*t2) + 
               t1*(9 + 12*t2 + 19*Power(t2,2) - s*(11 + t2)))) + 
         Power(s2,2)*(-4 + 2*
             (-22 + 4*Power(s,2) + 9*s1 + 4*Power(s1,2) - s*(5 + 8*s1))*
             t2 + (-86 - 18*Power(s,2) + 44*s1 - 24*Power(s1,2) + 
               s*(70 + 4*s1))*Power(t2,2) + 
            (51 + 8*Power(s,2) + 15*s1 + 2*Power(s1,2) + 
               s*(-54 + 11*s1))*Power(t2,3) + 
            (20 + 6*s - 9*s1)*Power(t2,4) - 5*Power(t2,5) + 
            Power(t1,3)*(-3 + 10*t2 + s1*(11 + 8*t2) - s*(13 + 8*t2)) - 
            Power(t1,2)*(-54 + s1 + 8*Power(s1,2) - 182*t2 - 9*s1*t2 + 
               46*Power(t2,2) + 16*s1*Power(t2,2) + 12*Power(t2,3) + 
               Power(s,2)*(7 + 8*t2) + 
               s*(14 + 34*t2 - 20*Power(t2,2) - s1*(13 + 8*t2))) + 
            t1*(17 - 48*t2 - 127*Power(t2,2) - 32*Power(t2,3) + 
               23*Power(t2,4) + 
               Power(s1,2)*(28 + 18*t2 - 4*Power(t2,2)) + 
               Power(s,2)*(6 + 7*t2 + 4*Power(t2,2)) + 
               s1*(-46 + 28*t2 + 3*Power(t2,2) + 14*Power(t2,3)) + 
               s*(-13 + 104*t2 + 43*Power(t2,2) - 11*Power(t2,3) + 
                  2*s1*(3 - 31*t2 + Power(t2,2))))) + 
         s2*((-4 + 8*s - 8*s1)*Power(t1,4) + 
            t2*(26 + 6*t2 + 2*Power(s1,2)*(4 - 5*t2)*t2 - 
               75*Power(t2,2) + 26*Power(t2,3) + Power(t2,4) + 
               Power(s,2)*t2*(8 - 18*t2 + 7*Power(t2,2)) - 
               2*s1*(8 - 3*t2 - 9*Power(t2,2) - 6*Power(t2,3) + 
                  Power(t2,4)) + 
               s*(16 - 2*(9 + 8*s1)*t2 + (65 + 2*s1)*Power(t2,2) + 
                  (-30 + 4*s1)*Power(t2,3) + 3*Power(t2,4))) + 
            2*Power(t1,3)*(-63 - 19*t2 + 16*Power(t2,2) + 
               Power(s1,2)*(3 + t2) + Power(s,2)*(5 + t2) + 
               2*s1*(-7 - 3*t2 + 2*Power(t2,2)) - 
               2*s*(-11 + 2*Power(t2,2) + s1*(4 + t2))) - 
            Power(t1,2)*(-12 - 52*t2 - 143*Power(t2,2) + 
               36*Power(t2,3) + 6*Power(t2,4) + 
               Power(s1,2)*(46 + 4*t2 - 2*Power(t2,2)) + 
               2*Power(s,2)*(1 - t2 + 5*Power(t2,2)) + 
               s1*(2 + 8*t2 - 6*Power(t2,2) + 5*Power(t2,3)) + 
               s*(82 + 74*t2 + 9*Power(t2,2) - 7*Power(t2,3) + 
                  s1*(-60 + 2*t2 - 8*Power(t2,2)))) + 
            t1*(54 + 244*t2 - 81*Power(t2,2) - 44*Power(t2,3) - 
               9*Power(t2,4) + 6*Power(t2,5) - 
               2*Power(s1,2)*(4 - 13*t2 - 9*Power(t2,2) + Power(t2,3)) - 
               Power(s,2)*(8 - 6*t2 - 4*Power(t2,2) + Power(t2,3)) + 
               s1*(-38 - 120*t2 - 13*Power(t2,2) - 8*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s*(2 - 222*t2 + 104*Power(t2,2) + 14*Power(t2,3) - 
                  4*Power(t2,4) + 
                  s1*(16 + 72*t2 - 75*Power(t2,2) + 7*Power(t2,3))))))*
       R2(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(1 - s2 + t1 - t2)*(-1 + t2)*(t1 - s2*t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) + 
    (8*(8*Power(t1,6) + Power(t2,4)*Power(1 + (-1 + s)*t2,2)*
          (-3 - 2*t2 + Power(t2,2)) + 
         t1*Power(t2,2)*(18 + (-39 + 33*s + 7*s1)*t2 + 
            (-9 + 15*Power(s,2) - 11*s1 + s*(-21 + 13*s1))*Power(t2,2) + 
            (47 - 65*s + 21*Power(s,2) - s1)*Power(t2,3) + 
            (-19 - 4*Power(s,2) + s*(18 - 7*s1) + 7*s1)*Power(t2,4) + 
            (2 + s - Power(s,2) - 2*s1 + 2*s*s1)*Power(t2,5)) - 
         Power(s2,7)*t2*(s1*(2 + t2) + t2*(-2 + 3*t2)) + 
         2*Power(t1,5)*(-32 - 17*t2 + 3*Power(s,2)*t2 + 
            3*Power(s1,2)*t2 + 6*Power(t2,2) + 
            2*s1*(-12 - 4*t2 + Power(t2,2)) - 
            2*s*(-20 + 3*s1*t2 + Power(t2,2))) + 
         Power(t1,2)*(16 + 86*t2 - 66*Power(t2,2) - 170*Power(t2,3) + 
            82*Power(t2,4) + Power(t2,5) + Power(t2,6) - 
            Power(s,2)*t2*(-8 + 6*t2 + 65*Power(t2,2) + 
               10*Power(t2,3) - 6*Power(t2,4) + Power(t2,5)) + 
            Power(s1,2)*t2*(8 - 16*t2 + 7*Power(t2,2) - Power(t2,3) - 
               3*Power(t2,4) + Power(t2,5)) + 
            s1*(-16 - 42*t2 + 114*Power(t2,2) + 14*Power(t2,3) - 
               65*Power(t2,4) + 24*Power(t2,5) - 3*Power(t2,6)) + 
            s*(16 - 2*(9 + 8*s1)*t2 + (48 - 58*s1)*Power(t2,2) - 
               14*(-16 + s1)*Power(t2,3) + (-32 + 51*s1)*Power(t2,4) - 
               6*(3 + 2*s1)*Power(t2,5) + 2*Power(t2,6))) - 
         2*Power(t1,4)*(84 - 61*t2 - 53*Power(t2,2) + 5*Power(t2,3) + 
            2*Power(t2,4) + 3*Power(s1,2)*
             (4 + 2*t2 - 2*Power(t2,2) + Power(t2,3)) - 
            s1*(4 + 9*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s,2)*(-4 + 2*t2 + 5*Power(t2,2) + 3*Power(t2,3)) + 
            s*(-36 + 51*t2 + 20*Power(t2,2) - 4*Power(t2,3) + 
               s1*(-8 + Power(t2,2) - 6*Power(t2,3)))) + 
         Power(t1,3)*(176 + 226*t2 - 46*Power(t2,2) - 73*Power(t2,3) - 
            22*Power(t2,4) + 4*Power(t2,5) - 
            s1*(232 - 124*Power(t2,2) + 71*Power(t2,3) - 
               13*Power(t2,4) + Power(t2,5)) + 
            Power(s1,2)*(48 - 38*t2 + 14*Power(t2,2) + 16*Power(t2,3) - 
               7*Power(t2,4) + Power(t2,5)) + 
            Power(s,2)*(-32 + 34*t2 + 42*Power(t2,2) - 4*Power(t2,3) + 
               6*Power(t2,4) + Power(t2,5)) - 
            s*(40 + 288*t2 + 24*Power(t2,2) - 85*Power(t2,3) + 
               6*Power(t2,4) + Power(t2,5) + 
               s1*(-48 - 84*t2 + 104*Power(t2,2) - 12*Power(t2,3) - 
                  Power(t2,4) + 2*Power(t2,5)))) + 
         Power(s2,6)*(-2*Power(s1,2) + 
            s1*(t1*(2 + 9*t2 + 4*Power(t2,2)) - 
               t2*(-3 + s + 15*t2 - s*t2 + 6*Power(t2,2))) - 
            t2*(-2 + (-8 + s)*t2 - 2*(9 + s)*Power(t2,2) + 
               12*Power(t2,3) + 
               t1*(4 + 2*t2 - 7*Power(t2,2) + s*(2 + 3*t2)))) + 
         Power(s2,5)*(t1*(-2 - (23 - 3*s + Power(s,2))*t2 + 
               (-77 - s + 3*Power(s,2))*Power(t2,2) - 
               (9 + 11*s)*Power(t2,3) + 25*Power(t2,4)) + 
            Power(s1,2)*(-10*t2 + 3*t1*(1 + t2)) + 
            Power(t1,2)*(2 + 13*t2 - 6*Power(t2,2) - 4*Power(t2,3) + 
               s*(2 + 11*t2 + 7*Power(t2,2))) + 
            t2*(-3 + 12*t2 + Power(s,2)*(-3 + t2)*t2 + 52*Power(t2,2) + 
               49*Power(t2,3) - 18*Power(t2,4) + 
               s*(5 + 14*t2 - 17*Power(t2,2) + 7*Power(t2,3))) - 
            s1*(-4 - 3*(4 + s)*t2 + (-11 + 3*s)*Power(t2,2) - 
               5*(-7 + s)*Power(t2,3) + 14*Power(t2,4) + 
               Power(t1,2)*(8 + 14*t2 + 7*Power(t2,2)) + 
               t1*(3 - 22*t2 - 40*Power(t2,2) - 13*Power(t2,3) + 
                  3*s*(1 + t2 + Power(t2,2))))) - 
         Power(s2,4)*(2 + (12 + 7*s - 13*s1)*t2 + 
            (46 + Power(s,2) - 73*s1 + 26*Power(s1,2) - 
               2*s*(-7 + 8*s1))*Power(t2,2) + 
            (31 + 11*Power(s,2) - 24*s1 + s*(-49 + 5*s1))*Power(t2,3) + 
            (-107 - 5*Power(s,2) + s*(55 - 10*s1) + 34*s1)*Power(t2,4) - 
            2*(29 + 4*s - 8*s1)*Power(t2,5) + 12*Power(t2,6) + 
            Power(t1,3)*(8 + 9*t2 - 8*Power(t2,2) + 
               s*(8 + 15*t2 + 4*Power(t2,2)) - 
               s1*(10 + 15*t2 + 4*Power(t2,2))) + 
            Power(t1,2)*(-15 - 106*t2 - 119*Power(t2,2) + 
               50*Power(t2,3) + 12*Power(t2,4) + 
               Power(s1,2)*(3 + 4*t2 + Power(t2,2)) + 
               Power(s,2)*(1 + 8*t2 + 4*Power(t2,2)) + 
               s1*(7 + 65*t2 + 30*Power(t2,2) + 16*Power(t2,3)) - 
               s*(-2 - 16*t2 + 17*Power(t2,2) + 18*Power(t2,3) + 
                  s1*(4 + 11*t2 + 5*Power(t2,2)))) + 
            t1*(-3 + 16*t2 + 145*Power(t2,2) + 255*Power(t2,3) + 
               22*Power(t2,4) - 33*Power(t2,5) + 
               Power(s,2)*t2*(-14 + t2 - 7*Power(t2,2)) + 
               Power(s1,2)*(-19 - 12*t2 - 14*Power(t2,2) + 
                  Power(t2,3)) + 
               s1*(18 + 37*t2 - 53*Power(t2,2) - 60*Power(t2,3) - 
                  16*Power(t2,4)) + 
               s*(1 + 12*t2 - 83*Power(t2,2) - 22*Power(t2,3) + 
                  15*Power(t2,4) + 
                  s1*(-1 + 13*t2 + 28*Power(t2,2) + 5*Power(t2,3))))) + 
         Power(s2,3)*(-(t2*(3 + 
                 (48 - 8*Power(s,2) - 7*s1 - 8*Power(s1,2) + 
                    s*(3 + 16*s1))*t2 + 
                 (108 - 103*s1 + 30*Power(s1,2) - 2*s*(-7 + 8*s1))*
                  Power(t2,2) + 
                 (109 + 17*Power(s,2) - 24*s1 + s*(-87 + 7*s1))*
                  Power(t2,3) + 
                 (-102 + 75*s - 10*Power(s,2) + 12*s1 - 10*s*s1)*
                  Power(t2,4) + (-30 - 2*s + 9*s1)*Power(t2,5) + 
                 3*Power(t2,6))) + 
            t1*(15 + 104*t2 + 135*Power(t2,2) - 292*Power(t2,3) - 
               299*Power(t2,4) - 26*Power(t2,5) + 19*Power(t2,6) + 
               Power(s1,2)*t2*
                (73 + 4*t2 + 22*Power(t2,2) - 3*Power(t2,3)) + 
               Power(s,2)*t2*
                (-5 + 31*t2 - 5*Power(t2,2) + 2*Power(t2,3)) + 
               s*(3 + (8 - 28*s1)*t2 + (-151 + 17*s1)*Power(t2,2) + 
                  (215 - 67*s1)*Power(t2,3) + 
                  5*(11 + s1)*Power(t2,4) - 9*Power(t2,5)) + 
               s1*(-31 - 195*t2 - 74*Power(t2,2) + 70*Power(t2,3) + 
                  28*Power(t2,4) + 10*Power(t2,5))) + 
            Power(t1,4)*(8*s*(1 + t2) - 4*(-2 + t2 + 2*s1*(1 + t2))) + 
            Power(t1,3)*(-47 - 164*t2 - 25*Power(t2,2) + 
               36*Power(t2,3) + Power(s1,2)*(2 + 5*t2 + Power(t2,2)) + 
               Power(s,2)*(4 + 8*t2 + Power(t2,2)) + 
               s1*(31 + 33*t2 + 32*Power(t2,2) + 8*Power(t2,3)) - 
               s*(-15 + 8*t2 + 38*Power(t2,2) + 8*Power(t2,3) + 
                  s1*(6 + 13*t2 + 2*Power(t2,2)))) - 
            Power(t1,2)*(-4 - 137*t2 - 489*Power(t2,2) - 
               254*Power(t2,3) + 84*Power(t2,4) + 12*Power(t2,5) + 
               Power(s1,2)*(25 + 42*t2 + 5*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(s,2)*(7 - t2 + 22*Power(t2,2) + 9*Power(t2,3)) + 
               s1*(-26 - 22*t2 + 67*Power(t2,2) + 13*Power(t2,3) + 
                  10*Power(t2,4)) + 
               s*(4 + 92*t2 + 164*Power(t2,2) + 3*Power(t2,3) - 
                  16*Power(t2,4) - 
                  s1*(32 + 49*t2 + 23*Power(t2,2) + 11*Power(t2,3))))) + 
         Power(s2,2)*(-4*(s - s1)*Power(t1,5) + 
            t1*(12 + (133 - 16*Power(s,2) - 35*s1 - 16*Power(s1,2) + 
                  s*(27 + 32*s1))*t2 + 
               (370 - 17*Power(s,2) + s*(14 - 26*s1) - 351*s1 + 
                  91*Power(s1,2))*Power(t2,2) + 
               (423 + 45*Power(s,2) - 66*s1 - 8*Power(s1,2) + 
                  s*(-327 + 49*s1))*Power(t2,3) + 
               (-260 - 15*Power(s,2) - 69*s*(-3 + s1) + 66*s1 + 
                  14*Power(s1,2))*Power(t2,4) - 
               (127 + 6*Power(s,2) + 11*s1 + 3*Power(s1,2) - 
                  3*s*(17 + 5*s1))*Power(t2,5) - 
               2*(6 + s - 2*s1)*Power(t2,6) + 4*Power(t2,7)) + 
            Power(t2,2)*(31 + 20*t2 + 4*Power(s1,2)*(2 - 3*t2)*t2 - 
               60*Power(t2,2) - 97*Power(t2,3) + 52*Power(t2,4) + 
               4*Power(t2,5) + 
               Power(s,2)*t2*(8 - 15*Power(t2,2) + 10*Power(t2,3)) + 
               s1*(-16 - 7*t2 + 45*Power(t2,2) + 5*Power(t2,3) + 
                  Power(t2,4) - 2*Power(t2,5)) + 
               s*(16 + (3 - 16*s1)*t2 + (22 - 4*s1)*Power(t2,2) + 
                  (89 - 6*s1)*Power(t2,3) + 5*(-10 + s1)*Power(t2,4) - 
                  2*Power(t2,5))) - 
            2*Power(t1,4)*(-33 - 50*t2 + 14*Power(t2,2) + 
               2*Power(s,2)*(1 + t2) + Power(s1,2)*(1 + 2*t2) + 
               2*s1*(4 + 5*t2 + 4*Power(t2,2)) - 
               s*(1 + 12*t2 + 8*Power(t2,2) + s1*(3 + 4*t2))) + 
            Power(t1,3)*(-44 - 421*t2 - 430*Power(t2,2) + 
               35*Power(t2,3) + 36*Power(t2,4) + 
               Power(s1,2)*(26 + 28*t2 + 5*Power(t2,2) + 
                  3*Power(t2,3)) + 
               Power(s,2)*(10 + 32*t2 + 20*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s1*(-36 - 21*t2 - 67*Power(t2,2) + 10*Power(t2,3) + 
                  4*Power(t2,4)) - 
               s*(-24 - 275*t2 - 118*Power(t2,2) + 32*Power(t2,3) + 
                  4*Power(t2,4) + 
                  s1*(36 + 52*t2 + 25*Power(t2,2) + 6*Power(t2,3)))) - 
            Power(t1,2)*(58 + 196*t2 - 121*Power(t2,2) - 
               545*Power(t2,3) - 188*Power(t2,4) + 48*Power(t2,5) + 
               4*Power(t2,6) + 
               Power(s1,2)*(62 + 29*t2 + 56*Power(t2,2) - 
                  3*Power(t2,3)) + 
               s1*(-148 - 114*t2 + 24*Power(t2,2) - 19*Power(t2,3) + 
                  5*Power(t2,4)) + 
               Power(s,2)*(-4 + 53*t2 - 11*Power(t2,2) + 
                  6*Power(t2,3) + 7*Power(t2,4)) + 
               s*(10 - 84*t2 + 262*Power(t2,2) + 248*Power(t2,3) + 
                  9*Power(t2,4) - 6*Power(t2,5) + 
                  s1*(6 - 26*t2 - 101*Power(t2,2) + 11*Power(t2,3) - 
                     7*Power(t2,4))))) + 
         s2*(-(Power(t2,3)*(7 + (-24 + 11*s + s1)*t2 + 
                 (4*Power(s,2) - 3*(1 + s1) + s*(-19 + 7*s1))*
                  Power(t2,2) + 
                 (33 - 43*s + 8*Power(s,2) + 3*s1 + 2*s*s1)*
                  Power(t2,3) - 
                 (14 + 5*Power(s,2) + s*(-16 + s1) + s1)*Power(t2,4) + 
                 (1 + s)*Power(t2,5))) + 
            2*Power(t1,5)*(-25 + Power(s,2) + Power(s1,2) - 2*t2 - 
               2*s*(1 + s1 + 2*t2) + s1*(2 + 4*t2)) + 
            t1*t2*(-62 - 85*t2 + 132*Power(t2,2) + 249*Power(t2,3) - 
               125*Power(t2,4) - 8*Power(t2,5) - Power(t2,6) - 
               Power(s1,2)*t2*
                (16 - 25*t2 + 3*Power(t2,2) - 3*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s,2)*t2*
                (16 + 9*t2 - 49*Power(t2,2) + 14*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s*(-32 + (-15 + 32*s1)*t2 + 40*(-2 + s1)*Power(t2,2) + 
                  (-281 + 22*s1)*Power(t2,3) + 
                  (90 - 34*s1)*Power(t2,4) + 2*(9 + 5*s1)*Power(t2,5)) + 
               s1*(32 + 27*t2 - 129*Power(t2,2) - 7*Power(t2,3) + 
                  36*Power(t2,4) - 12*Power(t2,5) + Power(t2,6))) - 
            2*Power(t1,4)*(-69 - 140*t2 - 25*Power(t2,2) + 
               18*Power(t2,3) + Power(s,2)*(6 + 5*t2 + 3*Power(t2,2)) + 
               Power(s1,2)*(6 + 5*t2 + 3*Power(t2,2)) + 
               s1*(-19 - 57*t2 - 8*Power(t2,2) + 4*Power(t2,3)) - 
               s*(-69 - 91*t2 + 8*Power(t2,2) + 4*Power(t2,3) + 
                  2*s1*(6 + 5*t2 + 3*Power(t2,2)))) + 
            Power(t1,3)*(90 + 234*t2 - 427*Power(t2,2) - 
               256*Power(t2,3) + 39*Power(t2,4) + 8*Power(t2,5) + 
               Power(s1,2)*(46 + 64*t2 - 6*Power(t2,2) - 
                  3*Power(t2,3) + 3*Power(t2,4)) + 
               Power(s,2)*(14 - 44*t2 + 14*Power(t2,3) + 
                  3*Power(t2,4)) - 
               s1*(60 + 40*t2 + 35*Power(t2,2) + 29*Power(t2,3) + 
                  4*Power(t2,4)) + 
               s*(28 + 40*t2 + 281*Power(t2,2) + 64*Power(t2,3) - 
                  14*Power(t2,4) - 
                  s1*(60 + 36*t2 - 22*Power(t2,2) + 11*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            Power(t1,2)*(-98 - 440*t2 - 530*Power(t2,2) + 
               197*Power(t2,3) + 173*Power(t2,4) + 37*Power(t2,5) - 
               8*Power(t2,6) + 
               Power(s,2)*(8 + 58*t2 - 27*Power(t2,2) - 9*Power(t2,3) + 
                  14*Power(t2,4) - 3*Power(t2,5)) + 
               Power(s1,2)*(8 - 98*t2 + 31*Power(t2,2) - 
                  34*Power(t2,3) + Power(t2,4) + 2*Power(t2,5)) + 
               s1*(58 + 438*t2 + 30*Power(t2,2) - 142*Power(t2,3) + 
                  73*Power(t2,4) - 11*Power(t2,5) + Power(t2,6)) + 
               s*(-6 + 62*t2 + 504*Power(t2,2) - 124*Power(t2,3) - 
                  132*Power(t2,4) + 2*Power(t2,5) + Power(t2,6) + 
                  s1*(-16 - 8*t2 - 116*Power(t2,2) + 131*Power(t2,3) - 
                     35*Power(t2,4) + Power(t2,5))))))*
       T2(t1,1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(1 - s2 + t1 - t2)*(-1 + t2)*Power(t1 - s2*t2,2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) + 
    (8*(2*(-1 + s - s1)*Power(t1,7) + 
         Power(t1,6)*(3 + 6*s2 + 18*t2 + 5*s2*t2 + 
            Power(s1,2)*(2 + t2 - Power(t2,2)) - 
            Power(s,2)*(-2 + Power(t2,2)) + 
            s1*(-15 + 11*t2 + Power(t2,2) + 3*s2*(2 + t2)) + 
            s*(1 - 16*t2 + Power(t2,2) - 3*s2*(2 + t2) + 
               s1*(-4 - t2 + 2*Power(t2,2)))) + 
         Power(t2,2)*(Power(t2,2)*Power(1 + (-1 + s)*t2,2)*
             (-3 - 2*t2 + Power(t2,2)) + 
            s2*t2*(2 + (12 + 7*s - s1)*t2 + 
               (-3 + 13*s + 5*Power(s,2) + 3*s1 - 7*s*s1)*Power(t2,2) - 
               (2*Power(s,2) + 3*(7 + s1) + s*(-25 + 2*s1))*
                Power(t2,3) + 
               (11 + 2*Power(s,2) + s*(-10 + s1) + s1)*Power(t2,4) - 
               (1 + s)*Power(t2,5)) + 
            Power(s2,4)*t2*(-4 - 2*Power(s1,2) - 2*t2 + 2*Power(t2,2) + 
               Power(t2,3) + s1*(6 + 4*t2 - Power(t2,2))) + 
            Power(s2,3)*(-4 - 2*(7 + 2*s)*t2 + 2*(-9 + s)*Power(t2,2) + 
               (8 - 3*s)*Power(t2,3) - 2*(-5 + s)*Power(t2,4) + 
               Power(t2,5) - 2*Power(s1,2)*(2 + Power(t2,2)) + 
               s1*(8 + 4*(4 + s)*t2 - (-7 + s)*Power(t2,2) + 
                  (3 + s)*Power(t2,3) - 3*Power(t2,4))) + 
            Power(s2,2)*(4 + 2*(5 + 2*s)*t2 - 5*(5 + 3*s)*Power(t2,2) - 
               (52 - 2*s + Power(s,2))*Power(t2,3) + 
               (6 - 12*s + Power(s,2))*Power(t2,4) + 
               (7 - 3*s)*Power(t2,5) - 
               4*Power(s1,2)*(-1 + t2 + Power(t2,2)) + 
               s1*(-8 - 2*(5 + 2*s)*t2 + (32 + 7*s)*Power(t2,2) - 
                  2*(-6 + s)*Power(t2,3) + 2*(1 + s)*Power(t2,4) - 
                  2*Power(t2,5)))) - 
         Power(t1,5)*(-36 + 9*s2 + 2*Power(s2,2) - 35*t2 + 44*s2*t2 + 
            13*Power(s2,2)*t2 + 55*Power(t2,2) + 39*s2*Power(t2,2) + 
            4*Power(s2,2)*Power(t2,2) + Power(t2,3) + 
            Power(s,2)*(3 + s2 + 2*t2 + 4*s2*t2 - 4*Power(t2,3)) + 
            Power(s1,2)*(7 + 5*t2 - 2*Power(t2,3) + 
               s2*(5 + 4*t2 - Power(t2,2))) + 
            s1*(12 - 32*t2 + 33*Power(t2,2) + 
               Power(s2,2)*(6 + 6*t2 + Power(t2,2)) + 
               s2*(-29 - t2 + 13*Power(t2,2) + Power(t2,3))) + 
            s*(10 + 21*t2 - 52*Power(t2,2) + 5*Power(t2,3) + 
               s2*t2*(-36 - 21*t2 + Power(t2,2)) - 
               Power(s2,2)*(2 + 7*t2 + Power(t2,2)) + 
               s1*(-6 - 3*t2 - 3*Power(t2,2) + 6*Power(t2,3) + 
                  s2*(-4 - 7*t2 + Power(t2,2))))) + 
         Power(t1,4)*(-3 - 21*s2 + 2*Power(s2,2) - 105*t2 - 93*s2*t2 + 
            33*Power(s2,2)*t2 + 4*Power(s2,3)*t2 - 110*Power(t2,2) + 
            18*s2*Power(t2,2) + 76*Power(s2,2)*Power(t2,2) + 
            8*Power(s2,3)*Power(t2,2) + 90*Power(t2,3) + 
            107*s2*Power(t2,3) + 25*Power(s2,2)*Power(t2,3) + 
            Power(s2,3)*Power(t2,3) + Power(t2,4) + s2*Power(t2,4) + 
            Power(s,2)*t2*(12 + Power(s2,2)*(-1 + t2) - 15*t2 - 
               2*Power(t2,2) - 5*Power(t2,3) + 
               s2*(9 + 14*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(Power(s2,2)*(5 + 7*t2) + 
               3*(-2 + t2 + Power(t2,2) - 2*Power(t2,3)) + 
               s2*(9 + 18*t2 + 9*Power(t2,2) - 2*Power(t2,3))) + 
            s1*(25 + 51*t2 - 41*Power(t2,2) + 46*Power(t2,3) - 
               4*Power(t2,4) + Power(s2,3)*(2 + 5*t2) + 
               Power(s2,2)*(-9 - 28*t2 + 7*Power(t2,2) + 
                  2*Power(t2,3)) + 
               s2*(-4 - 15*t2 + 20*Power(t2,2) + 27*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s*(7 - 53*t2 - 86*Power(t2,2) + 86*Power(t2,3) - 
               8*Power(t2,4) + Power(s2,3)*t2*(2 + t2) + 
               Power(s2,2)*t2*(7 + 42*t2 + 5*Power(t2,2)) + 
               s2*(-1 + 6*t2 + 68*Power(t2,2) + 62*Power(t2,3) - 
                  3*Power(t2,4)) + 
               s1*(-10 + 45*t2 - 15*Power(t2,2) - 4*Power(t2,3) - 
                  4*Power(t2,4) + Power(s2,2)*(3 + 3*t2 + Power(t2,2)) + 
                  s2*(1 + 14*Power(t2,2) + Power(t2,3))))) - 
         t1*t2*(Power(s2,4)*t2*
             (-4 + 2*Power(s1,2)*(-1 + t2) + 2*t2 + 8*Power(t2,2) + 
               3*Power(t2,3) + s1*(6 + 2*t2 - 5*Power(t2,2))) + 
            Power(s2,2)*(-8 - 2*(15 + 2*s)*t2 - 
               (59 + 31*s)*Power(t2,2) - 
               (133 + 5*s + 4*Power(s,2))*Power(t2,3) + 
               (19 - 57*s + 4*Power(s,2))*Power(t2,4) + 
               (34 - 11*s)*Power(t2,5) - 
               Power(s1,2)*(8 + 20*t2 - 9*Power(t2,2) + 
                  11*Power(t2,3)) + 
               s1*(16 + (38 + 4*s)*t2 + (33 + 4*s)*Power(t2,2) + 
                  (36 + s)*Power(t2,3) + (31 + 5*s)*Power(t2,4) - 
                  7*Power(t2,5))) + 
            s2*(8 + (26 + 8*s)*t2 - 17*Power(t2,2) + 
               (-99 + 59*s + 21*Power(s,2))*Power(t2,3) + 
               (-69 + 79*s - 4*Power(s,2))*Power(t2,4) + 
               (53 - 47*s + 8*Power(s,2))*Power(t2,5) - 
               (2 + 3*s)*Power(t2,6) + 
               Power(s1,2)*(8 - 8*t2 - 9*Power(t2,2) + 3*Power(t2,3) - 
                  3*Power(t2,4) + Power(t2,5)) - 
               s1*(16 + 4*(3 + 2*s)*t2 - (67 + 21*s)*Power(t2,2) + 
                  (-27 + 35*s)*Power(t2,3) - (-22 + s)*Power(t2,4) + 
                  (-9 + s)*Power(t2,5) + Power(t2,6))) + 
            Power(s2,3)*t2*(-10 - 12*Power(s1,2)*(-1 + t2) - 62*t2 + 
               6*Power(t2,2) + 38*Power(t2,3) + 4*Power(t2,4) + 
               s1*(4 + 45*t2 + 28*Power(t2,2) - 9*Power(t2,3)) + 
               s*(-4 + 4*t2 - 13*Power(t2,2) - 7*Power(t2,3) + 
                  s1*(4 - t2 + 5*Power(t2,2)))) + 
            Power(t2,2)*(Power(s,2)*t2*
                (3 - 18*t2 - 8*Power(t2,2) + 4*Power(t2,3)) + 
               s*(3 - (9 + 13*s1)*t2 + 35*Power(t2,2) + 
                  (12 + 7*s1)*Power(t2,3) - (7 + 2*s1)*Power(t2,4)) + 
               (-1 + t2)*(-6 - 15*t2 + 2*Power(t2,2) + Power(t2,3) + 
                  s1*(7 - 4*t2 - 5*Power(t2,2) + 2*Power(t2,3))))) + 
         Power(t1,2)*(4 + 10*t2 + 4*s*t2 + 8*Power(t2,2) - 
            s*Power(t2,2) - 35*Power(t2,3) + 29*s*Power(t2,3) + 
            12*Power(s,2)*Power(t2,3) - 75*Power(t2,4) + 
            111*s*Power(t2,4) - 40*Power(s,2)*Power(t2,4) + 
            40*Power(t2,5) - 16*s*Power(t2,5) - 
            12*Power(s,2)*Power(t2,5) - 2*Power(t2,6) - 
            7*s*Power(t2,6) + 5*Power(s,2)*Power(t2,6) + 
            Power(s2,4)*Power(t2,2)*(4 + 8*t2 + 3*Power(t2,2)) + 
            Power(s2,3)*t2*(-8 + 2*(-13 + s)*t2 - 
               15*(-2 + s)*Power(t2,2) + (58 - 9*s)*Power(t2,3) + 
               6*Power(t2,4)) + 
            s2*(-4 + 4*(-3 + s)*t2 - (39 + 20*s)*Power(t2,2) + 
               (-250 + 85*s + 34*Power(s,2))*Power(t2,3) + 
               (-115 + 69*s + 4*Power(s,2))*Power(t2,4) + 
               3*(39 - 31*s + 4*Power(s,2))*Power(t2,5) - 
               2*s*Power(t2,6)) - 
            Power(s2,2)*t2*(8 + 121*t2 + 159*Power(t2,2) - 
               6*Power(s,2)*(-1 + t2)*Power(t2,2) - 58*Power(t2,3) - 
               64*Power(t2,4) + 
               s*(8 + 17*t2 + 31*Power(t2,2) + 106*Power(t2,3) + 
                  16*Power(t2,4))) + 
            Power(s1,2)*(4 - 4*t2 - 8*Power(t2,2) + 
               2*Power(s2,4)*Power(t2,2) + 7*Power(t2,3) - 
               Power(t2,4) - 3*Power(t2,5) + Power(t2,6) - 
               9*Power(s2,2)*t2*(-2 + t2 + Power(t2,2)) + 
               2*Power(s2,3)*t2*(1 + 3*t2 + 3*Power(t2,2)) + 
               s2*(-4 - 34*t2 + 15*Power(t2,2) - 16*Power(t2,3) - 
                  5*Power(t2,4) + 2*Power(t2,5))) + 
            s1*(-8 - 2*(3 + 2*s)*t2 + (23 + 10*s)*Power(t2,2) + 
               (39 - 55*s)*Power(t2,3) - 5*Power(s2,4)*Power(t2,3) + 
               (-16 + 9*s)*Power(t2,4) + 3*(-3 + 7*s)*Power(t2,5) + 
               (3 - 6*s)*Power(t2,6) + 
               Power(s2,3)*t2*
                (12 + (3 + s)*t2 + (8 + 3*s)*Power(t2,2) - 
                  13*Power(t2,3)) + 
               s2*(8 - 4*(-13 + s)*t2 + (57 + 41*s)*Power(t2,2) + 
                  (68 - 53*s)*Power(t2,3) + (-7 + 8*s)*Power(t2,4) + 
                  (25 - 8*s)*Power(t2,5) - 2*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-22 + 85*t2 + 68*Power(t2,2) + 63*Power(t2,3) - 
                  8*Power(t2,4) + 
                  s*(8 - 16*t2 + 17*Power(t2,2) + 3*Power(t2,3))))) + 
         Power(t1,3)*(2 + 4*Power(s2,2) - 8*t2 + 67*s2*t2 + 
            49*Power(s2,2)*t2 - 6*Power(s2,3)*t2 + 105*Power(t2,2) + 
            268*s2*Power(t2,2) + 35*Power(s2,2)*Power(t2,2) - 
            32*Power(s2,3)*Power(t2,2) - 2*Power(s2,4)*Power(t2,2) + 
            128*Power(t2,3) + 87*s2*Power(t2,3) - 
            108*Power(s2,2)*Power(t2,3) - 38*Power(s2,3)*Power(t2,3) - 
            Power(s2,4)*Power(t2,3) - 86*Power(t2,4) - 
            148*s2*Power(t2,4) - 58*Power(s2,2)*Power(t2,4) - 
            4*Power(s2,3)*Power(t2,4) + 2*Power(t2,5) - 
            2*s2*Power(t2,5) - 
            2*Power(s,2)*Power(t2,2)*
             (9 + 2*Power(s2,2)*(-1 + t2) - 20*t2 - 4*Power(t2,2) + 
               s2*(13 + 8*t2 + 4*Power(t2,2))) + 
            Power(s1,2)*(16 + 2*t2 - 7*Power(t2,2) + Power(t2,3) + 
               8*Power(t2,4) - 2*Power(t2,5) - 
               2*Power(s2,3)*Power(1 + t2,2) + 
               Power(s2,2)*(2 - 19*t2 - 21*Power(t2,2)) + 
               s2*(-10 + 3*t2 + 18*Power(t2,2) - 3*Power(t2,3))) + 
            s1*(-18 - 31*t2 + Power(s2,4)*(-2 + t2)*t2 - 
               59*Power(t2,2) + 41*Power(t2,3) - 20*Power(t2,4) + 
               2*Power(t2,5) + 
               Power(s2,3)*t2*(9 + 4*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(-2 - 3*t2 + 14*Power(t2,2) - 
                  35*Power(t2,3) + 2*Power(t2,4)) - 
               s2*(-8 + 57*t2 + 82*Power(t2,2) + 39*Power(t2,3) + 
                  34*Power(t2,4))) + 
            s*(-4 + 11*t2 - 75*Power(t2,2) - 144*Power(t2,3) + 
               70*Power(t2,4) - 2*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(7 + 5*t2) + 
               Power(s2,2)*t2*
                (1 + 29*t2 + 96*Power(t2,2) + 12*Power(t2,3)) + 
               s2*(4 + 12*t2 - 33*Power(t2,2) + 23*Power(t2,3) + 
                  100*Power(t2,4) - 2*Power(t2,5)) + 
               s1*(4 - 20*t2 + Power(s2,3)*(-1 + t2)*t2 + 
                  81*Power(t2,2) - 23*Power(t2,3) - 20*Power(t2,4) + 
                  4*Power(t2,5) + 
                  Power(s2,2)*t2*(16 - 11*t2 + Power(t2,2)) + 
                  s2*(-4 - 19*t2 + 21*Power(t2,2) + 2*Power(t2,3) + 
                     8*Power(t2,4))))))*T3(t2,t1))/
     ((-1 + s1)*(-1 + t1)*Power(t1 - t2,2)*(1 - s2 + t1 - t2)*(-1 + t2)*
       Power(t1 - s2*t2,2)) + (8*
       (Power(-1 + t2,2)*t2*Power(1 + (-1 + s)*t2,2)*
          (-3 - 2*t2 + Power(t2,2)) + 
         2*Power(t1,4)*((-5 + t2)*t2 - s*(2 + t2 + Power(t2,2)) + 
            s1*(2 + t2 + Power(t2,2))) + 
         Power(s2,4)*t2*(s1*(2 + t2 + 6*Power(t2,2) - Power(t2,3)) + 
            t2*(10 - 25*t2 + 6*Power(t2,2) + Power(t2,3))) - 
         t1*Power(-1 + t2,2)*(Power(s,2)*t2*
             (3 - 9*t2 - 2*Power(t2,2) + Power(t2,3)) - 
            s*(-3 + (-9 + 13*s1)*t2 - 29*Power(t2,2) + 
               (6 - 7*s1)*Power(t2,3) + (1 + 2*s1)*Power(t2,4)) + 
            (-1 + t2)*(-15 - 12*t2 + 11*Power(t2,2) - 2*Power(t2,3) + 
               s1*(7 - 4*t2 - 5*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(t1,3)*(Power(s,2)*t2*
             (-6 - t2 - 2*Power(t2,2) + Power(t2,3)) + 
            (-1 + t2)*(5 + 15*t2 - 16*Power(t2,2) + 
               Power(s1,2)*(2 - t2 - 2*Power(t2,2) + Power(t2,3)) - 
               s1*(-1 - 8*t2 + 4*Power(t2,2) + Power(t2,3))) + 
            s*(1 - 4*t2 - 24*Power(t2,2) + 12*Power(t2,3) - 
               Power(t2,4) + s1*
                (2 + 3*t2 + 5*Power(t2,3) - 2*Power(t2,4)))) + 
         Power(t1,2)*(-(Power(s,2)*
               (5 - 4*t2 - 13*Power(t2,2) + 5*Power(t2,3) - 
                 2*Power(t2,4) + Power(t2,5))) + 
            Power(-1 + t2,2)*
             (12 - 4*t2 + 17*Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(7 - t2 - 3*Power(t2,2) + Power(t2,3)) - 
               s1*(32 + 15*t2 - 12*Power(t2,2) + 3*Power(t2,3))) + 
            s*(-1 + t2)*(3*s1*(-2 - 9*t2 + 3*Power(t2,2)) + 
               2*(2 + 23*t2 + 17*Power(t2,2) - 9*Power(t2,3) + 
                  Power(t2,4)))) + 
         Power(s2,3)*(2*Power(s1,2)*Power(-1 + t2,2) - 
            s1*(t1*(2 + 13*t2 + 4*Power(t2,2) + 13*Power(t2,3)) + 
               t2*(3 - 19*t2 + 28*Power(t2,2) - 15*Power(t2,3) + 
                  3*Power(t2,4) - 
                  s*(1 - 7*t2 - 3*Power(t2,2) + Power(t2,3)))) + 
            t2*(-2 + 28*Power(t2,2) - 43*Power(t2,3) + 16*Power(t2,4) + 
               Power(t2,5) - s*t2*
                (3 - 24*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(-20 + 42*t2 + 19*Power(t2,2) - 8*Power(t2,3) - 
                  Power(t2,4) + 
                  s*(2 - 5*t2 + 10*Power(t2,2) + Power(t2,3))))) + 
         Power(s2,2)*(t1*(2 + (11 + 5*s + Power(s,2))*t2 - 
               (83 + 27*s + Power(s,2))*Power(t2,2) + 
               (96 - 43*s - 7*Power(s,2))*Power(t2,3) - 
               (9 - 15*s + Power(s,2))*Power(t2,4) + 
               (-17 + 2*s)*Power(t2,5)) - 
            Power(s1,2)*Power(-1 + t2,2)*(-4*t2 + 3*t1*(1 + t2)) - 
            Power(t1,2)*(-10 + 9*t2 + 56*Power(t2,2) - 3*Power(t2,3) - 
               4*Power(t2,4) + 
               s*(2 - t2 + 11*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t2,4))) - 
            t2*(Power(s,2)*t2*(9 - 17*t2 + Power(t2,2) - Power(t2,3)) - 
               Power(-1 + t2,2)*
                (3 - 2*t2 + 14*Power(t2,2) + 11*Power(t2,3)) + 
               s*(5 + 12*t2 + 41*Power(t2,2) - 67*Power(t2,3) + 
                  6*Power(t2,4) + 3*Power(t2,5))) + 
            s1*(Power(t1,2)*(12 + 12*t2 + 13*Power(t2,2) + 
                  10*Power(t2,3) + Power(t2,4)) - 
               (-1 + t2)*(-4 - (8 + 3*s)*t2 + 
                  (-14 + 23*s)*Power(t2,2) + (32 + 6*s)*Power(t2,3) - 
                  2*(4 + s)*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(3 - 18*t2 + 6*Power(t2,2) + 19*Power(t2,3) - 
                  11*Power(t2,4) + Power(t2,5) + 
                  s*(3 + 5*t2 + 8*Power(t2,2) + 7*Power(t2,3) + 
                     Power(t2,4))))) + 
         s2*(-(Power(-1 + t2,2)*
               (-2 + (-12 - 7*s + s1)*t2 + 
                 (3 - 5*Power(s,2) - 3*s1 + s*(-13 + 7*s1))*
                  Power(t2,2) + 
                 (2*Power(s,2) + 3*(7 + s1) + s*(-25 + 2*s1))*
                  Power(t2,3) - 
                 (11 + 2*Power(s,2) + s*(-10 + s1) + s1)*Power(t2,4) + 
                 (1 + s)*Power(t2,5))) + 
            Power(t1,3)*(-8 + 31*t2 + 14*Power(t2,2) - 5*Power(t2,3) - 
               s1*(14 + 3*t2 + 12*Power(t2,2) + 3*Power(t2,3)) + 
               s*(4 + 5*t2 + 12*Power(t2,2) + 3*Power(t2,3))) + 
            Power(t1,2)*(Power(s,2)*
                (1 - 2*t2 + 13*Power(t2,2) + 4*Power(t2,3)) + 
               s*(-2 + 2*t2 + 50*Power(t2,2) + 11*Power(t2,3) - 
                  14*Power(t2,4) + Power(t2,5) + 
                  s1*(-8 - t2 - 7*Power(t2,2) - 9*Power(t2,3) + 
                     Power(t2,4))) + 
               (-1 + t2)*(11 - 49*t2 - 6*Power(t2,2) + 32*Power(t2,3) - 
                  Power(s1,2)*(3 + t2 - 5*Power(t2,2) + Power(t2,3)) + 
                  s1*(1 - 22*t2 + 5*Power(t2,2) + 3*Power(t2,3) + 
                     Power(t2,4)))) + 
            t1*(-2*Power(s,2)*t2*
                (-5 + 9*t2 + 4*Power(t2,2) - Power(t2,3) + Power(t2,4)) - 
               Power(-1 + t2,2)*
                (3 + 10*t2 + 10*Power(t2,2) + 28*Power(t2,3) + 
                  Power(t2,4) + 
                  Power(s1,2)*(7 + 3*t2 - 3*Power(t2,2) + Power(t2,3)) - 
                  s1*(18 + 58*t2 + 5*Power(t2,2) - 6*Power(t2,3) + 
                     Power(t2,4))) + 
               s*(-1 + t2)*(-1 - 27*t2 - 104*Power(t2,2) - 
                  21*Power(t2,3) + 17*Power(t2,4) + 
                  s1*(1 + 29*t2 + 25*Power(t2,2) - 11*Power(t2,3) + 
                     4*Power(t2,4))))))*T4(-1 + t2))/
     ((-1 + s1)*(-1 + t1)*(1 - s2 + t1 - t2)*Power(-1 + t2,2)*
       Power(t1 - s2*t2,2)) + (8*
       ((2 - 2*s + 2*s1)*Power(t1,5) - 
         t2*Power(1 + (-1 + s)*t2,2)*
          (3 - t2 - 3*Power(t2,2) + Power(t2,3)) - 
         Power(s2,5)*t2*(-(s1*(-2 + t2)) + t2*(2 + t2)) + 
         Power(t1,3)*(5 - 45*t2 + 25*Power(t2,2) + Power(t2,3) - 
            Power(s1,2)*(1 + Power(t2,2)) + 
            Power(s,2)*(1 - 2*t2 + Power(t2,2) - 2*Power(t2,3)) + 
            s1*(7 - 17*t2 + 16*Power(t2,2) - 2*Power(t2,3)) + 
            s*(-5 + (29 + 6*s1)*t2 - 3*(9 + s1)*Power(t2,2) + 
               (3 + 2*s1)*Power(t2,3))) + 
         Power(t1,4)*(5 - 14*t2 + Power(s,2)*(-2 + Power(t2,2)) + 
            Power(s1,2)*(-2 - t2 + Power(t2,2)) - 
            s1*(-7 + 7*t2 + Power(t2,2)) + 
            s*(-1 + 12*t2 - Power(t2,2) + s1*(4 + t2 - 2*Power(t2,2)))) + 
         t1*(Power(s,2)*t2*(-3 + 12*t2 - 10*Power(t2,2) - 
               5*Power(t2,3) + 2*Power(t2,4)) - 
            s*(-1 + t2)*(-3 + (-9 + 13*s1)*t2 - 35*Power(t2,2) + 
               (2 - 7*s1)*Power(t2,3) + (3 + 2*s1)*Power(t2,4)) + 
            Power(-1 + t2,2)*(-15 - 15*t2 + 9*Power(t2,2) - Power(t2,3) + 
               s1*(7 - 4*t2 - 5*Power(t2,2) + 2*Power(t2,3)))) + 
         Power(s2,3)*(t1*(-2 - (17 + 3*s + Power(s,2))*t2 + 
               (15 - 25*s + Power(s,2))*Power(t2,2) + 
               (32 - 5*s)*Power(t2,3) + 2*Power(t2,4)) + 
            Power(s1,2)*(2 - 6*t2 + 6*Power(t2,2) + t1*(5 + 3*t2)) - 
            Power(t1,2)*(2 + 17*t2 + 12*Power(t2,2) + Power(t2,3) - 
               s*(2 + 9*t2 + 2*Power(t2,2))) - 
            s1*(-4 - (9 + 4*s)*t2 + 4*(-1 + s)*Power(t2,2) + 
               (26 + 3*s)*Power(t2,3) - 5*Power(t2,4) + 
               Power(t1,2)*(8 + 11*t2 + Power(t2,2)) + 
               t1*(5 + s*(3 - 2*t2) + 2*t2 - 21*Power(t2,2) + 
                  4*Power(t2,3))) + 
            t2*(-5 + 26*t2 - Power(s,2)*(-1 + t2)*t2 + 8*Power(t2,2) - 
               14*Power(t2,3) - Power(t2,4) + 
               s*(5 + t2 + 13*Power(t2,2) + 5*Power(t2,3)))) + 
         Power(t1,2)*(Power(s,2)*
             (3 - 10*t2 + 13*Power(t2,2) + Power(t2,3)) - 
            (-1 + t2)*(-21 - 34*t2 + 24*Power(t2,2) - Power(t2,3) - 
               s1*(-1 + 15*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(s1,2)*(3 - t2 - 3*Power(t2,2) + Power(t2,3))) + 
            s*(19 + 5*t2 - 65*Power(t2,2) + 24*Power(t2,3) - 
               Power(t2,4) + s1*
                (-18 + 34*t2 - 9*Power(t2,2) - 7*Power(t2,3) + 
                  2*Power(t2,4)))) - 
         Power(s2,4)*(2*Power(s1,2) + 
            s1*(t2*(-5 + s + 12*t2 + s*t2 - 4*Power(t2,2)) + 
               t1*(-2 - 7*t2 + Power(t2,2))) + 
            t2*(-2 - (4 + 3*s)*t2 + (11 - 2*s)*Power(t2,2) + 
               2*Power(t2,3) + 
               t1*(s*(2 + t2) - 2*(2 + 5*t2 + Power(t2,2))))) + 
         Power(s2,2)*(-2 - 9*t2 - 12*s*t2 - 20*Power(t2,2) - 
            4*s*Power(t2,2) - 6*Power(s,2)*Power(t2,2) + 37*Power(t2,3) - 
            23*s*Power(t2,3) + 4*Power(s,2)*Power(t2,3) + 
            17*s*Power(t2,4) - 3*Power(s,2)*Power(t2,4) - 6*Power(t2,5) + 
            4*s*Power(t2,5) + 
            Power(t1,3)*(8 + 18*t2 + 4*Power(t2,2) - 
               s*(8 + 10*t2 + Power(t2,2))) - 
            Power(t1,2)*(-13 - 16*t2 + 60*Power(t2,2) + 17*Power(t2,3) + 
               Power(s,2)*(1 + 3*t2 + Power(t2,2)) - 
               s*t2*(18 + 41*t2 + 2*Power(t2,2))) + 
            Power(s1,2)*(4*Power(-1 + t2,2)*t2 + 
               Power(t1,2)*(-6 - 7*t2 + Power(t2,2)) + 
               t1*(4 - 5*t2 - 6*Power(t2,2) + Power(t2,3))) + 
            t1*(5 - 26*t2 - 56*Power(t2,2) + 14*Power(t2,3) + 
               21*Power(t2,4) + Power(s,2)*t2*(7 - t2 + 4*Power(t2,2)) - 
               s*(1 - 8*t2 + 2*Power(t2,2) + 48*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s1*(-4 - (7 + 3*s)*t2 + (9 + 6*s)*Power(t2,2) + 
               (13 + 2*s)*Power(t2,3) - (13 + 3*s)*Power(t2,4) + 
               2*Power(t2,5) + Power(t1,3)*(12 + 9*t2 + Power(t2,2)) - 
               Power(t1,2)*(-14 + 9*t2 + 16*Power(t2,2) + Power(t2,3) - 
                  s*(5 + 6*t2)) + 
               t1*(-19 - 9*t2 + 16*Power(t2,2) + 28*Power(t2,3) - 
                  4*Power(t2,4) + 
                  s*(4 - 9*t2 + 19*Power(t2,2) - 3*Power(t2,3))))) + 
         s2*(2 + (13 + 7*s - s1)*t2 + 
            (-19 + 5*Power(s,2) + s*(12 - 7*s1) + 4*s1)*Power(t2,2) + 
            (-20 - 4*Power(s,2) - 6*s1 + 5*s*(2 + s1))*Power(t2,3) + 
            (6*Power(s,2) + 4*(9 + s1) + s*(-41 + 3*s1))*Power(t2,4) - 
            (13 + 3*Power(s,2) + s*(-11 + s1) + s1)*Power(t2,5) + 
            (1 + s)*Power(t2,6) + 
            Power(t1,4)*(-8 - 5*t2 + s*(8 + 3*t2) - s1*(8 + 3*t2)) + 
            Power(t1,3)*(-20 + 25*t2 + 29*Power(t2,2) + 
               Power(s1,2)*(5 + 5*t2 - 2*Power(t2,2)) + 
               Power(s,2)*(3 + 4*t2 - Power(t2,2)) + 
               s1*(-16 + 13*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               s*(5 - 35*t2 - 14*Power(t2,2) + Power(t2,3) + 
                  s1*(-6 - 8*t2 + 3*Power(t2,2)))) - 
            Power(t1,2)*(Power(s1,2)*
                (5 - 11*t2 - Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(2 - t2 + 6*Power(t2,2) + Power(t2,3)) + 
               t2*(-43 - 45*t2 + 45*Power(t2,2) + Power(t2,3)) + 
               s1*(-11 - 7*t2 + 24*Power(t2,2) + 6*Power(t2,3)) + 
               s*(15 + 6*t2 - 18*Power(t2,2) - 28*Power(t2,3) + 
                  Power(t2,4) - 
                  s1*(7 - 24*t2 + Power(t2,2) + 3*Power(t2,3)))) + 
            t1*(Power(s,2)*t2*(-3 - 4*Power(t2,2) + 5*Power(t2,3)) - 
               (-1 + t2)*(14 + 52*t2 + 26*Power(t2,2) - 28*Power(t2,3) - 
                  Power(s1,2)*(7 - 5*t2 - 3*Power(t2,2) + Power(t2,3)) + 
                  s1*(11 + 9*t2 + 4*Power(t2,2) - 9*Power(t2,3) + 
                     Power(t2,4))) - 
               s*(-4 + 9*t2 - 26*Power(t2,2) - 44*Power(t2,3) + 
                  28*Power(t2,4) + Power(t2,5) + 
                  s1*(1 - 6*t2 + 20*Power(t2,2) - 16*Power(t2,3) + 
                     5*Power(t2,4))))))*T5(1 - s2 + t1 - t2))/
     ((-1 + s1)*(-1 + t1)*(1 - s2 + t1 - t2)*(-1 + t2)*Power(t1 - s2*t2,2)));
   return a;
};
