#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m213_6_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-16*(2*Power(s,2)*s1*(-2 - t1 + s1*(-6 + t1 - t2) + 9*t2) + 
         s*(-2*Power(s1,4) + t2 + 
            Power(s1,3)*(-9 + 6*s2 - 11*t1 + 11*t2) + 
            s1*(-7 + t1*(6 - 9*t2) - 26*t2 + 6*s2*t2 + 37*Power(t2,2)) + 
            Power(s1,2)*(30 + 5*t1 - 26*t2 + 9*t1*t2 - 9*Power(t2,2) - 
               6*s2*(1 + t2))) + 
         s1*(s1 - t2)*(13 + 2*Power(s1,3) - 2*s2 + 3*t1 + 
            Power(s1,2)*(6 - 7*s2 + 9*t1 - 10*t2) - 11*t2 - 5*s2*t2 + 
            6*t1*t2 - 18*Power(t2,2) + 
            s1*(-1 + 13*t2 + 6*Power(t2,2) - 6*t1*(2 + t2) + s2*(9 + 5*t2))\
)))/(s*Power(-1 + s1,2)*s1*(s1 - t2)*(-1 + t2)) + 
    (8*(4*Power(s,3)*(-1 + t2)*t2 + 
         Power(s1,3)*(s2*(7 + 2*t2 - Power(t2,2)) + 
            8*(-1 + Power(t2,2)) + t1*(-7 - 2*t2 + Power(t2,2))) + 
         Power(s1,2)*(5 + t2 + 7*Power(t2,2) - 13*Power(t2,3) + 
            t1*(20 - 2*t2 + 6*Power(t2,2)) + 
            s2*(-21 - 3*t2 - Power(t2,2) + Power(t2,3))) + 
         s1*(-55 + 59*t2 - 13*Power(t2,2) + 5*Power(t2,3) + 
            4*Power(t2,4) + t1*
             (-11 - 10*t2 + Power(t2,2) - 4*Power(t2,3)) - 
            s2*(-14 - 15*t2 + 4*Power(t2,2) + Power(t2,3))) + 
         2*(1 + (26 - 7*s2)*t2 + 3*(-11 + s2)*Power(t2,2) + 
            12*Power(t2,3) - 6*Power(t2,4) + 
            t1*(-1 + 7*t2 - 4*Power(t2,2) + 2*Power(t2,3))) + 
         Power(s,2)*(t1*(3 + 3*t2 - 2*Power(t2,2)) + 
            t2*(31 + 2*s2*(-1 + t2) - 21*t2 + 10*Power(t2,2)) + 
            s1*(-4 + 17*t2 + 2*s2*t2 + Power(t2,2) - 2*s2*Power(t2,2) - 
               2*Power(t2,3) + t1*(-3 - 3*t2 + 2*Power(t2,2)))) + 
         s*(3 - t1 - 72*t2 + 11*s2*t2 - 7*t1*t2 + 55*Power(t2,2) - 
            5*s2*Power(t2,2) + 10*t1*Power(t2,2) - 32*Power(t2,3) + 
            2*s2*Power(t2,3) - 2*t1*Power(t2,3) + 6*Power(t2,4) + 
            Power(s1,2)*(12 - 9*t2 - 14*Power(t2,2) + 3*Power(t2,3) + 
               t1*(6 + 5*t2 - 3*Power(t2,2)) + 
               s2*(-1 - 3*t2 + 4*Power(t2,2))) + 
            s1*(21 - 55*t2 + 15*Power(t2,2) + 5*Power(t2,3) - 
               2*Power(t2,4) + 
               s2*(1 - 8*t2 + Power(t2,2) - 2*Power(t2,3)) + 
               t1*(-5 + 2*t2 - 7*Power(t2,2) + 2*Power(t2,3)))))*
       B1(s,t2,s1))/
     (Power(-1 + s1,2)*(-1 + t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (16*(Power(s1,7) + Power(s1,6)*(7 - 2*s + 2*s2 + 2*t1) + 
         Power(-1 + s,2)*Power(t2,2) + 
         Power(s1,5)*(-24 + Power(s,2) - t1 - 
            s*(16 + s2 + 5*t1 - 2*t2) - 20*t2 + t1*t2 - 3*Power(t2,2) - 
            s2*(5 + 3*t2)) + s1*t2*
          (-1 - t1 + Power(s,3)*(2 + t1 - t2) - 5*t2 + s2*t2 + t1*t2 + 
            Power(t2,2) - s2*Power(t2,2) + 
            s*(4 + t1*(3 - 2*t2) + s2*(-2 + t2)*t2) + 
            Power(s,2)*(-5 + t1*(-3 + t2) + (2 + s2)*t2 - Power(t2,2))) + 
         Power(s1,4)*(35 - 9*t1 + 34*t2 - 2*t1*t2 + 20*Power(t2,2) - 
            t1*Power(t2,2) + 2*Power(t2,3) - 
            Power(s,2)*(-7 + s2 - 4*t1 + 3*t2) + 2*s2*(1 + 5*t2) + 
            s*(-9 - 7*t1 + 28*t2 - 3*t1*t2 + 5*Power(t2,2) + s2*(3 + t2))\
) + Power(s1,2)*(6 - 5*t1 + 14*t2 - 2*s2*t2 + 2*t1*t2 + 3*Power(t2,2) + 
            4*s2*Power(t2,2) - 3*t1*Power(t2,2) + 6*Power(t2,3) + 
            Power(s,3)*(2 + t1 - 7*t2 - t1*t2 + Power(t2,2)) - 
            s*(10 + t1*(-11 + t2) + (27 - 4*s2)*t2 - 
               (-14 + s2)*Power(t2,2) + (-7 + s2)*Power(t2,3)) + 
            Power(s,2)*(2 - 2*(-8 + s2)*t2 - (-8 + s2)*Power(t2,2) + 
               Power(t2,3) - t1*(7 + Power(t2,2)))) + 
         Power(s1,3)*(-25 + 13*t1 - 27*t2 - 16*Power(t2,2) + 
            3*t1*Power(t2,2) - 9*Power(t2,3) + Power(s,3)*(2 - t1 + t2) + 
            s2*(1 - 5*t2 - 5*Power(t2,2) + Power(t2,3)) + 
            Power(s,2)*(-18 + s2 - 8*t2 + 2*s2*t2 - 3*Power(t2,2) + 
               3*t1*(1 + t2)) + 
            s*(9 + 53*t2 - 25*Power(t2,2) - 3*Power(t2,3) + 
               s2*(-2 - 5*t2 + Power(t2,2)) + t1*(1 + t2 + 2*Power(t2,2)))\
))*R1q(s1))/(Power(-1 + s1,2)*s1*
       (1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       Power(s1 - t2,2)*(-1 + t2)) - 
    (32*(Power(s1,3)*(-2 - t1 + t2 + s2*t2 + Power(t2,2)) - 
         Power(s1,2)*(3 + (-8 + 2*s + s2 - t1)*t2 + 
            (3 + 2*s2 - 2*t1)*Power(t2,2) + 2*Power(t2,3)) - 
         t2*(2 + 5*t2 - 10*Power(t2,2) + s2*Power(t2,2) + 
            3*Power(t2,3) - t1*(2 - 2*t2 + Power(t2,2)) + 
            s*(2 + t1 - 5*t2 - t1*t2 + 5*Power(t2,2))) + 
         s1*(-1 + 13*t2 + (-17 + 3*s + 2*s2)*Power(t2,2) + 
            (4 + s + s2)*Power(t2,3) + Power(t2,4) - 
            t1*(-1 - (-3 + s)*t2 + s*Power(t2,2) + Power(t2,3))))*R1q(t2))/
     (Power(-1 + s1,2)*Power(s1 - t2,2)*Power(-1 + t2,2)) + 
    (16*(Power(s,3)*(-6 - 3*t1 + 3*s1*(-2 + t1 - t2) + 11*t2) - 
         Power(-1 + s1,2)*(6 + Power(s1,3) - s2 + 2*t1 + 
            Power(s1,2)*(2 - 3*s2 + 4*t1 - 4*t2) - 8*t2 - 2*s2*t2 + 
            2*t1*t2 - 6*Power(t2,2) + 
            s1*(3 + 4*t2 + 2*Power(t2,2) + 2*s2*(2 + t2) - 2*t1*(3 + t2))\
) + Power(s,2)*(5 - Power(s1,3) - 3*s2 + 4*t1 - 16*t2 + s2*t2 - 
            2*t1*t2 + 6*Power(t2,2) + 
            Power(s1,2)*(2 + 6*s2 - 10*t1 + 11*t2) - 
            s1*(-22 + 27*t2 + 2*Power(t2,2) + s2*(3 + t2) - 
               2*t1*(3 + t2))) + 
         s*(2*Power(s1,4) + Power(s1,3)*(4 - 9*s2 + 11*t1 - 12*t2) + 
            s2*(2 - 5*t2) + (1 + t1 - 3*t2)*(1 + 4*t2) + 
            Power(s1,2)*(3 - 13*t1 + 25*t2 - 4*t1*t2 + 4*Power(t2,2) + 
               3*s2*(4 + t2)) + 
            s1*(38 + t1 - 46*t2 - 8*Power(t2,2) + s2*(-5 + 2*t2))))*R2q(s))/
     (s*Power(-1 + s1,2)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(-1 + t2)) + 
    (8*(4*Power(s,6)*t2 - Power(-1 + s1,4)*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2)*
          (-2*(1 + t2) + s1*(3 + t2)) + 
         Power(s,5)*(3*t1 + t2*(-25 + 2*s2 + 8*t2) - 
            s1*(-4 + 3*t1 + 3*t2 + 2*s2*t2)) + 
         Power(s,4)*(3 + 2*t1*(-5 + t2) + 52*t2 - 7*s2*t2 - 
            44*Power(t2,2) + 4*s2*Power(t2,2) + 4*Power(t2,3) + 
            Power(s1,2)*(4 - 16*t2 - 3*Power(t2,2) + t1*(15 + t2) + 
               s2*(-5 + 4*t2)) - 
            s1*(27 - 16*t2 + 5*Power(t2,2) + t1*(5 + 3*t2) + 
               s2*(-5 - 3*t2 + 4*Power(t2,2)))) + 
         Power(s,3)*(-7 + t1*(10 - 8*t2) - 32*t2 + 3*s2*t2 + 
            72*Power(t2,2) - 12*s2*Power(t2,2) - 20*Power(t2,3) + 
            2*s2*Power(t2,3) + 
            Power(s1,3)*(-24 - 30*t1 + 26*t2 - 4*t1*t2 + 
               9*Power(t2,2) + s2*(14 + t2)) + 
            Power(s1,2)*(6 + 14*t1 + 22*t2 + 8*t1*t2 - 11*Power(t2,2) - 
               2*Power(t2,3) + s2*(-5 - 17*t2 + 9*Power(t2,2))) + 
            s1*(33 + 6*t1 - 24*t2 + 4*t1*t2 + 18*Power(t2,2) - 
               2*Power(t2,3) + 
               s2*(-9 + 13*t2 + 3*Power(t2,2) - 2*Power(t2,3)))) + 
         s*Power(-1 + s1,2)*(3 + 25*t2 - s2*t2 - 20*Power(t2,2) + 
            12*s2*Power(t2,2) + 4*Power(t2,3) - 2*s2*Power(t2,3) - 
            t1*(5 + 8*t2) + Power(s1,3)*
             (-8 - 15*t1 + t2 - 4*t1*t2 + 3*Power(t2,2) + 5*s2*(2 + t2)) \
- Power(s1,2)*(-2 - 17*t1 - 21*t2 - 8*t1*t2 + Power(t2,2) + 
               2*Power(t2,3) + s2*(7 + 13*t2 + Power(t2,2))) + 
            s1*(-21 + 3*t1 + 5*t2 + 4*t1*t2 - 14*Power(t2,2) + 
               2*Power(t2,3) + 
               s2*(5 - 11*t2 + 5*Power(t2,2) - 2*Power(t2,3)))) - 
         Power(s,2)*(-3 + (24 - 5*s2 - 12*t1)*t2 + 
            2*(15 + s2)*Power(t2,2) + 4*(-5 + s2)*Power(t2,3) + 
            Power(s1,2)*(-39 + 16*t2 + 7*Power(t2,2) + 4*Power(t2,3) - 
               2*t1*(5 + 3*t2) + 
               2*s2*(-1 + t2 + 6*Power(t2,2) - 2*Power(t2,3))) + 
            Power(s1,3)*(s2*(-17 - 26*t2 + 5*Power(t2,2)) + 
               2*(14 + 9*t2 - 8*Power(t2,2) - 2*Power(t2,3) + 
                  7*t1*(3 + t2))) + 
            Power(s1,4)*(s2*(16 + 7*t2) - 
               3*(8 - 4*t2 - 3*Power(t2,2) + 2*t1*(5 + t2))) + 
            s1*(s2*(3 + 22*t2 - 19*Power(t2,2)) + 
               2*(3 - 19*t2 + Power(t2,2) - 6*Power(t2,3) + 
                  t1*(-1 + 5*t2)))))*T2q(s1,s))/
     (s*Power(-1 + s1,2)*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2))*(-1 + t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) - 
    (8*(-((-1 + s1)*Power(s1 - t2,2)*
            (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2)*
            (-2*(1 + t2) + s1*(3 + t2))) + 
         2*Power(s,3)*t2*(2 + 4*s1 + t1 - 5*t2 - 2*Power(t2,2) + 
            Power(s1,2)*(-t1 + t2)) + 
         s*(s1 - t2)*(6 - 6*t1 + 19*t2 - 2*s2*t2 + 5*t1*t2 - 
            7*Power(t2,2) - 3*s2*Power(t2,2) + 2*t1*Power(t2,2) - 
            12*Power(t2,3) + 2*s2*Power(t2,3) - 2*t1*Power(t2,3) + 
            6*Power(t2,4) + Power(s1,3)*
             (4 + 3*s2 - 7*t2 - Power(t2,2) - t1*(4 + 3*t2)) + 
            Power(s1,2)*(21 - 8*t2 + 4*Power(t2,2) + 3*Power(t2,3) + 
               t1*(15 + t2 - 3*Power(t2,2)) + 
               s2*(-5 - 6*t2 + 2*Power(t2,2))) + 
            s1*(-19 - 28*t2 + 20*Power(t2,2) + Power(t2,3) - 
               2*Power(t2,4) + 
               s2*(2 + 8*t2 + Power(t2,2) - 2*Power(t2,3)) + 
               t1*(-5 - 3*t2 + Power(t2,2) + 2*Power(t2,3)))) + 
         Power(s,2)*(Power(s1,3)*(t1 + 4*t1*t2 + (9 - 2*t2)*t2) + 
            Power(s1,2)*(8 + (7 - 2*s2)*t2 - 2*(3 + s2)*Power(t2,2) + 
               t1*(-3 + 6*t2)) - 
            t2*(-2 + 2*(-16 + s2)*t2 + (1 + 2*s2)*Power(t2,2) + 
               10*Power(t2,3) + t1*(8 - 3*t2 - 2*Power(t2,2))) + 
            s1*(4 + 2*(-19 + s2)*t2 + 4*(-3 + s2)*Power(t2,2) + 
               (5 + 2*s2)*Power(t2,3) + 2*Power(t2,4) - 
               t1*(-2 + 2*t2 + 3*Power(t2,2) + 2*Power(t2,3)))))*T3q(t2,s1))/
     (s*Power(-1 + s1,2)*(s1 - t2)*(-1 + t2)*
       (-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(-4*Power(s,3)*t2*(3 + t2) + 
         (-1 + s1)*(-1 + t2)*(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2)*
          (-2*(1 + t2) + s1*(3 + t2)) - 
         Power(s,2)*(-(t1*(3 + 3*t2 + 2*Power(t2,2))) + 
            t2*(1 + 13*t2 + 10*Power(t2,2) + 2*s2*(3 + t2)) + 
            s1*(12 - 3*(3 + 2*s2)*t2 - (9 + 2*s2)*Power(t2,2) - 
               2*Power(t2,3) + t1*(3 + 3*t2 + 2*Power(t2,2)))) + 
         s*(3 - t1 + 12*t2 - s2*t2 + 7*t1*t2 - 11*Power(t2,2) - 
            5*s2*Power(t2,2) + 2*Power(t2,3) - 2*s2*Power(t2,3) + 
            2*t1*Power(t2,3) - 6*Power(t2,4) + 
            Power(s1,2)*(4 + t2 + 5*t1*t2 + (-2 + 3*t1)*Power(t2,2) - 
               3*Power(t2,3) + s2*(3 - 7*t2 - 4*Power(t2,2))) + 
            s1*(-19 + 11*t2 - 3*Power(t2,2) + 9*Power(t2,3) + 
               2*Power(t2,4) - 
               t1*(-1 + 12*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-3 + 8*t2 + 9*Power(t2,2) + 2*Power(t2,3)))))*
       T4q(-1 + t2))/
     (s*Power(-1 + s1,2)*(-1 + t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) \
+ (8*(-4*Power(s,4)*t2 + 4*(s1 - t2)*(-1 + t2)*
          (-1 + t1 - 5*t2 + s2*t2 - 2*t1*t2 + 6*Power(t2,2) + 
            Power(s1,2)*(-2 + s2 - t1 + 2*t2) - 
            s1*(-7 + s2 + 5*t2 + s2*t2 - 2*t1*t2 + 2*Power(t2,2))) - 
         Power(s,3)*(t1*(3 - 2*t2) + t2*(-21 + 2*s2 + 10*t2) + 
            s1*(4 + t2 - 2*s2*t2 - 2*Power(t2,2) + t1*(-3 + 2*t2))) - 
         Power(s,2)*(3 + 7*t2 - 5*s2*t2 - 16*Power(t2,2) + 
            2*s2*Power(t2,2) + 6*Power(t2,3) + 
            t1*(-1 + 6*t2 - 2*Power(t2,2)) + 
            Power(s1,2)*(8 + t1*(8 - 3*t2) - 15*t2 + 3*Power(t2,2) + 
               s2*(-5 + 4*t2)) + 
            s1*(-23 + 12*t2 + Power(t2,2) - 2*Power(t2,3) + 
               s2*(5 + t2 - 2*Power(t2,2)) + 
               t1*(-7 - 3*t2 + 2*Power(t2,2)))) + 
         s*(Power(s1,3)*(8 + s2*(-5 + t2) - t1*(-5 + t2) - 8*t2) - 
            2*(-1 + t2)*(-1 + t1 + (-5 + s2)*t2 - 4*t1*t2 + 
               16*Power(t2,2)) + 
            Power(s1,2)*(-8*t1 + Power(-1 + t2,2) + 
               s2*(7 + 2*t2 - Power(t2,2))) + 
            s1*(17 + t1 - 54*t2 + 11*t1*t2 + 29*Power(t2,2) - 
               8*t1*Power(t2,2) + 8*Power(t2,3) + 
               s2*(-2 - 5*t2 + 3*Power(t2,2)))))*T5q(s))/
     (s*Power(-1 + s1,2)*(-1 + t2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))));
   return a;
};
