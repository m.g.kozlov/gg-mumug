#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_1_m132_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(-2*Power(s1,5) - 32*s2 - 12*Power(s2,2) + 32*t1 + 24*s2*t1 - 
         12*Power(t1,2) + 2*Power(s,4)*(s1 - s2 + t1 - t2) - 22*t2 + 
         37*s2*t2 + 8*Power(s2,2)*t2 - 43*t1*t2 - 9*s2*t1*t2 + 
         Power(t1,2)*t2 + 45*Power(t2,2) - 25*s2*Power(t2,2) + 
         4*Power(s2,2)*Power(t2,2) + 40*t1*Power(t2,2) - 
         16*s2*t1*Power(t2,2) + 12*Power(t1,2)*Power(t2,2) - 
         41*Power(t2,3) + 20*s2*Power(t2,3) + Power(s2,2)*Power(t2,3) - 
         30*t1*Power(t2,3) - s2*t1*Power(t2,3) + 18*Power(t2,4) + 
         s2*Power(t2,4) + Power(s1,4)*(-2 + s2 - 3*t1 + 8*t2) + 
         Power(s,3)*(-10*Power(s1,2) + 2*Power(s2,2) - 2*t1 + 
            Power(t1,2) + 2*t2 + 4*t1*t2 - 5*Power(t2,2) - 
            3*s2*(t1 + t2) + s1*(8*s2 - 7*t1 + 13*t2)) + 
         Power(s1,3)*(-Power(s2,2) + Power(t1,2) - 2*s2*(3 + 2*t2) + 
            t1*(9 + 7*t2) + 2*(-6 + t2 - 6*Power(t2,2))) + 
         Power(s,2)*(-10 + 12*Power(s1,3) + 36*t1 + 11*Power(t1,2) - 
            36*t2 - 32*t1*t2 + 2*Power(t1,2)*t2 + 21*Power(t2,2) + 
            2*t1*Power(t2,2) - 4*Power(t2,3) + Power(s2,2)*(3 + 5*t2) - 
            Power(s1,2)*(8 + 9*s2 - 5*t1 + 24*t2) - 
            s1*(-46 + s2 + 5*Power(s2,2) - 9*t1 - 6*s2*t1 + 
               Power(t1,2) + 13*t2 - 8*s2*t2 + 9*t1*t2 - 16*Power(t2,2)) \
+ s2*(-28 + 18*t2 + Power(t2,2) - 7*t1*(2 + t2))) + 
         Power(s1,2)*(9 + Power(t1,2)*(9 - 2*t2) - 3*t2 + 
            3*Power(s2,2)*t2 + 20*Power(t2,2) + 8*Power(t2,3) - 
            t1*(25 + 34*t2 + 5*Power(t2,2)) + 
            s2*(36 + 16*t2 + 6*Power(t2,2) - t1*(9 + t2))) + 
         s1*(Power(t1,2)*(-5 - 21*t2 + Power(t2,2)) - 
            Power(s2,2)*(12 + 4*t2 + 3*Power(t2,2)) + 
            t1*(-5 + t2 + 55*Power(t2,2) + Power(t2,3)) - 
            2*(-11 + 27*t2 - 28*Power(t2,2) + 19*Power(t2,3) + 
               Power(t2,4)) + 
            s2*(11 - 27*t2 - 30*Power(t2,2) - 4*Power(t2,3) + 
               t1*(17 + 25*t2 + 2*Power(t2,2)))) - 
         s*(-10 + 2*Power(s1,4) + 67*t1 - Power(t1,2) - 47*t2 - 76*t1*t2 - 
            23*Power(t1,2)*t2 + 77*Power(t2,2) + 60*t1*Power(t2,2) - 
            Power(t1,2)*Power(t2,2) - 37*Power(t2,3) + Power(t2,4) - 
            Power(s2,2)*(8 + 7*t2 + 4*Power(t2,2)) - 
            Power(s1,3)*(2*s2 + 3*t1 + 5*(2 + t2)) + 
            Power(s1,2)*(22 - 4*Power(s2,2) + Power(t1,2) - 3*t2 + 
               5*Power(t2,2) + 2*t1*(8 + t2) + s2*(-7 + 3*t1 + t2)) + 
            s2*(-61 + 53*t2 - 38*Power(t2,2) - 3*Power(t2,3) + 
               t1*(9 + 30*t2 + 5*Power(t2,2))) + 
            s1*(59 + 20*Power(t1,2) - 101*t2 + 50*Power(t2,2) - 
               3*Power(t2,3) + Power(s2,2)*(3 + 8*t2) + 
               t1*(3 - 62*t2 + Power(t2,2)) + 
               s2*(16 + 29*t2 + 4*Power(t2,2) - t1*(23 + 8*t2))))))/
     ((s - s2 + t1)*(-1 + s - s1 + t2)*Power(s - s1 + t2,2)*
       (-1 + s2 - t1 + t2)*(-s1 + s2 - t1 + t2)) - 
    (8*(32 + 44*s2 + 12*Power(s2,2) - 44*t1 - 24*s2*t1 + 12*Power(t1,2) + 
         Power(s1,3)*(-Power(s2,3) + Power(s2,2)*(-8 + 3*t1) + 
            s2*(-8 + 16*t1 - 3*Power(t1,2)) + t1*(8 - 8*t1 + Power(t1,2))\
) + Power(s,5)*(t1 - t2) - 20*t2 - 53*s2*t2 - 18*Power(s2,2)*t2 + 
         49*t1*t2 + 33*s2*t1*t2 + 2*Power(s2,2)*t1*t2 - 
         15*Power(t1,2)*t2 - 4*s2*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - 
         16*Power(t2,2) + 16*s2*Power(t2,2) + 6*Power(s2,2)*Power(t2,2) - 
         2*Power(s2,3)*Power(t2,2) - 16*t1*Power(t2,2) - 
         12*s2*t1*Power(t2,2) + 3*Power(s2,2)*t1*Power(t2,2) + 
         6*Power(t1,2)*Power(t2,2) - Power(t1,3)*Power(t2,2) + 
         4*Power(t2,3) + s2*Power(t2,3) + 2*Power(s2,2)*Power(t2,3) + 
         Power(s2,3)*Power(t2,3) + 3*t1*Power(t2,3) - s2*t1*Power(t2,3) - 
         2*Power(s2,2)*t1*Power(t2,3) - Power(t1,2)*Power(t2,3) + 
         s2*Power(t1,2)*Power(t2,3) + 
         Power(s,4)*(5 - 2*(3 + s2)*t1 + 2*Power(t1,2) + 4*t2 + 
            3*s2*t2 - 2*Power(t2,2) + s1*(-6 + s2 - 3*t1 + 4*t2)) + 
         Power(s,3)*(-31 + 22*t1 - 9*Power(t1,2) + Power(t1,3) + 
            Power(s2,2)*(t1 - 3*t2) + Power(s1,2)*(14 + t1 - 3*t2) + 
            4*t2 - 5*t1*t2 + 3*Power(t1,2)*t2 + 6*Power(t2,2) - 
            3*t1*Power(t2,2) - Power(t2,3) + 
            s1*(18 - 2*Power(s2,2) - 2*t1 - 6*Power(t1,2) + 
               s2*(17 + 8*t1 - 10*t2) - 17*t2 + 6*t1*t2 + 4*Power(t2,2)) \
+ s2*(-16 + 9*t1 - 2*Power(t1,2) - 10*t2 + 6*Power(t2,2))) + 
         Power(s1,2)*(8 - 8*t2 + 4*t1*(1 + t2) - 
            Power(t1,3)*(3 + 2*t2) + Power(s2,3)*(-2 + 3*t2) + 
            2*Power(t1,2)*(-6 + 7*t2) + 
            Power(s2,2)*(-16 + t1 + 18*t2 - 8*t1*t2) + 
            s2*(4*(-3 + t2) + Power(t1,2)*(4 + 7*t2) - 4*t1*(-7 + 8*t2))) \
+ Power(s,2)*(53 - 42*t1 + 11*Power(t1,2) - 3*Power(t1,3) + 
            Power(s1,3)*(-8 - s2 + t1) - 47*t2 + Power(s2,3)*t2 + 
            43*t1*t2 - 13*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - 
            6*Power(t2,2) + 2*t1*Power(t2,2) + 2*Power(t2,3) - 
            2*t1*Power(t2,3) + 
            Power(s2,2)*(7 - 3*t1 + 8*t2 - 6*Power(t2,2)) + 
            Power(s1,2)*(-35 + 2*Power(t1,2) + t1*(20 - 8*t2) + 19*t2 + 
               s2*(-29 - 2*t1 + 9*t2)) + 
            s2*(45 - 3*Power(t1,2)*(-2 + t2) - 29*t2 - 14*Power(t2,2) + 
               3*Power(t2,3) + t1*(-18 + 5*t2 + 6*Power(t2,2))) + 
            s1*(19 + Power(s2,3) + 6*Power(t1,2) - 3*Power(t1,3) + 
               12*t2 - 9*Power(t2,2) + Power(s2,2)*(-17 - 5*t1 + 8*t2) + 
               s2*(-35 + 7*Power(t1,2) + t1*(11 - 8*t2) + 40*t2 - 
                  11*Power(t2,2)) + t1*(17 - 18*t2 + 9*Power(t2,2)))) + 
         s1*(t1*(19 + 56*t2 - 19*Power(t2,2)) + 
            Power(s2,3)*(-12 + 8*t2 - 3*Power(t2,2)) + 
            Power(t1,3)*(10 + Power(t2,2)) + 
            4*(5 - 6*t2 + Power(t2,2)) - 
            Power(t1,2)*(45 - 2*t2 + Power(t2,2)) + 
            Power(s2,2)*(-42 + 6*t2 - 8*Power(t2,2) + 
               t1*(34 - 16*t2 + 7*Power(t2,2))) + 
            s2*(-15 - 48*t2 + 7*Power(t2,2) + 
               Power(t1,2)*(-32 + 8*t2 - 5*Power(t2,2)) + 
               t1*(87 - 8*t2 + 9*Power(t2,2)))) + 
         s*(-64 + 73*t1 - 15*Power(t1,2) + 2*Power(t1,3) + 
            2*Power(s1,3)*(8 + Power(s2,2) - 2*s2*(-4 + t1) - 8*t1 + 
               Power(t1,2)) + 25*t2 - 58*t1*t2 + 17*Power(t1,2)*t2 - 
            4*Power(t1,3)*t2 + 2*Power(s2,3)*(-1 + t2)*t2 - 
            12*Power(t2,2) + 24*t1*Power(t2,2) - 
            5*Power(t1,2)*Power(t2,2) + Power(t1,3)*Power(t2,2) - 
            5*Power(t2,3) + t1*Power(t2,3) - Power(t1,2)*Power(t2,3) + 
            Power(s2,2)*(-18 + 13*t2 + 10*Power(t2,2) - 3*Power(t2,3) + 
               t1*(2 - 3*Power(t2,2))) + 
            s2*(-77 + 61*t2 - 12*Power(t2,2) - 4*Power(t2,3) + 
               Power(t1,2)*(-4 + 6*t2) + 
               t1*(33 - 30*t2 - 5*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(Power(t1,3) + Power(s2,2)*(17 + t1 - 9*t2) + 
               Power(t1,2)*(3 - 7*t2) - 4*(1 + 5*t2) + 
               t1*(-23 + 33*t2) + 
               s2*(35 - 2*Power(t1,2) - 37*t2 + 4*t1*(-5 + 4*t2))) + 
            s1*(-13 - 2*Power(s2,3)*(-3 + t2) - 2*Power(t1,3)*(-1 + t2) + 
               72*t2 + 5*Power(t2,2) - 
               2*t1*(19 + 17*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(33 - t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(41 + 2*t1*(-5 + t2) - 31*t2 + 
                  10*Power(t2,2)) + 
               s2*(43 + 6*t2 + 17*Power(t2,2) + 2*Power(t1,2)*(1 + t2) - 
                  2*t1*(37 - 16*t2 + 8*Power(t2,2))))))*
       B1(1 - s2 + t1 - t2,1 - s + s2 - t1,1 - s + s1 - t2))/
     ((s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       Power(s - s1 + t2,2)) - 
    (32*(-4*Power(s2,2) + 4*Power(s2,3) + 2*Power(s2,4) + 
         Power(s1,3)*(2 + s2)*(s2 - t1) + 8*s2*t1 - 12*Power(s2,2)*t1 - 
         8*Power(s2,3)*t1 - 4*Power(t1,2) + 12*s2*Power(t1,2) + 
         12*Power(s2,2)*Power(t1,2) - 4*Power(t1,3) - 8*s2*Power(t1,3) + 
         2*Power(t1,4) + Power(s,4)*(t1 - t2) - s2*t2 + 
         14*Power(s2,2)*t2 + 4*Power(s2,3)*t2 + t1*t2 - 31*s2*t1*t2 - 
         14*Power(s2,2)*t1*t2 - Power(s2,3)*t1*t2 + 17*Power(t1,2)*t2 + 
         16*s2*Power(t1,2)*t2 + 3*Power(s2,2)*Power(t1,2)*t2 - 
         6*Power(t1,3)*t2 - 3*s2*Power(t1,3)*t2 + Power(t1,4)*t2 - 
         2*Power(t2,2) + 10*s2*Power(t2,2) + 6*Power(s2,2)*Power(t2,2) - 
         10*t1*Power(t2,2) - 10*s2*t1*Power(t2,2) - 
         Power(s2,2)*t1*Power(t2,2) + 4*Power(t1,2)*Power(t2,2) + 
         2*s2*Power(t1,2)*Power(t2,2) - Power(t1,3)*Power(t2,2) + 
         2*Power(t2,3) - Power(s2,2)*Power(t2,3) + s2*t1*Power(t2,3) - 
         Power(s,3)*(-5 + Power(s1,2) + 2*t1 - 2*Power(t1,2) + 
            s1*(2 + 3*t1 - 3*t2) + 2*s2*(1 + t1 - t2) - 2*t2 + 
            2*Power(t2,2)) + Power(s1,2)*
          (Power(s2,2)*(1 - 3*t2) - (1 + 2*t1)*(2 + t1 - 2*t2) + 
            s2*(5 + t1 - 4*t2 + 3*t1*t2)) + 
         Power(s,2)*(-5 + Power(s1,3) + 12*t1 - Power(t1,2) + 
            2*Power(t1,3) + Power(s1,2)*(3 + 2*s2 + t1 - 3*t2) + 
            Power(s2,2)*(5 + 2*t1 - t2) + 3*t2 - 4*t1*t2 + 
            Power(t1,2)*t2 + 5*Power(t2,2) - 2*t1*Power(t2,2) - 
            Power(t2,3) - s2*
             (9 + 4*t1 + 4*Power(t1,2) + 2*t2 - 4*Power(t2,2)) + 
            s1*(-2 - 4*Power(t1,2) + s2*(5 + 4*t1 - 6*t2) - 8*t2 + 
               3*Power(t2,2) + t1*(2 + t2))) - 
         s*(Power(s1,3)*(3 + 2*s2 - t1) + 7*t1 - 5*Power(t1,2) - 
            2*Power(t1,3) - Power(t1,4) + Power(s2,3)*(4 + t1) + 3*t2 - 
            14*t1*t2 + 9*Power(t1,2)*t2 - Power(t1,3)*t2 + 
            4*Power(t2,2) - 6*t1*Power(t2,2) + Power(t1,2)*Power(t2,2) - 
            Power(t2,3) + t1*Power(t2,3) + 
            Power(s2,2)*(-2 - 3*Power(t1,2) + t2 + 2*Power(t2,2) - 
               t1*(10 + t2)) + 
            Power(s1,2)*(3 + Power(s2,2) - 2*Power(t1,2) + 
               s2*(1 + t1 - 6*t2) - 7*t2 + t1*(2 + 3*t2)) + 
            s2*(-7 + 3*Power(t1,3) + 11*t2 + 8*Power(t2,2) - 
               2*Power(t2,3) + 2*Power(t1,2)*(4 + t2) + 
               t1*(7 - 10*t2 - 3*Power(t2,2))) + 
            s1*(-3 + 2*Power(t1,3) + Power(s2,2)*(6 + 2*t1 - 3*t2) + 
               Power(t1,2)*(-3 + t2) - 7*t2 + 5*Power(t2,2) - 
               s2*(8 + 4*Power(t1,2) + t1*(3 - 2*t2) + 9*t2 - 
                  6*Power(t2,2)) + t1*(11 + 4*t2 - 3*Power(t2,2)))) + 
         s1*(Power(s2,3)*t1 - Power(t1,4) - 4*(-1 + t2)*t2 + 
            Power(t1,3)*(2 + t2) - Power(t1,2)*(13 + 2*t2) + 
            t1*(-1 + 15*t2 - 2*Power(t2,2)) + 
            Power(s2,2)*(-10 - 3*Power(t1,2) - 7*t2 + 3*Power(t2,2) + 
               t1*(2 + t2)) + 
            s2*(1 + 3*Power(t1,3) - 15*t2 + 2*Power(t2,2) - 
               2*Power(t1,2)*(2 + t2) + t1*(23 + 9*t2 - 3*Power(t2,2)))))*
       R1(1 - s + s2 - t1))/
     (Power(s - s2 + t1,2)*Power(s - s1 + t2,2)*Power(-s1 + s2 - t1 + t2,2)) \
+ (16*(Power(s1,7) + 12*s2 - 36*Power(s2,2) + 16*Power(s2,3) + 
         8*Power(s2,4) - 12*t1 + 72*s2*t1 - 48*Power(s2,2)*t1 - 
         32*Power(s2,3)*t1 - 36*Power(t1,2) + 48*s2*Power(t1,2) + 
         48*Power(s2,2)*Power(t1,2) - 16*Power(t1,3) - 
         32*s2*Power(t1,3) + 8*Power(t1,4) + 
         Power(s1,6)*(12 + 4*s2 - 5*t2) + 2*Power(s,6)*(t1 - t2) - 
         18*t2 - 81*s2*t2 + 155*Power(s2,2)*t2 - 14*Power(s2,3)*t2 - 
         16*Power(s2,4)*t2 + 99*t1*t2 - 319*s2*t1*t2 + 
         35*Power(s2,2)*t1*t2 + 60*Power(s2,3)*t1*t2 + 
         164*Power(t1,2)*t2 - 28*s2*Power(t1,2)*t2 - 
         84*Power(s2,2)*Power(t1,2)*t2 + 7*Power(t1,3)*t2 + 
         52*s2*Power(t1,3)*t2 - 12*Power(t1,4)*t2 + 17*Power(t2,2) + 
         218*s2*Power(t2,2) - 176*Power(s2,2)*Power(t2,2) - 
         15*Power(s2,3)*Power(t2,2) + 10*Power(s2,4)*Power(t2,2) - 
         281*t1*Power(t2,2) + 370*s2*t1*Power(t2,2) + 
         54*Power(s2,2)*t1*Power(t2,2) - 32*Power(s2,3)*t1*Power(t2,2) - 
         194*Power(t1,2)*Power(t2,2) - 63*s2*Power(t1,2)*Power(t2,2) + 
         36*Power(s2,2)*Power(t1,2)*Power(t2,2) + 
         24*Power(t1,3)*Power(t2,2) - 16*s2*Power(t1,3)*Power(t2,2) + 
         2*Power(t1,4)*Power(t2,2) + 73*Power(t2,3) - 
         229*s2*Power(t2,3) + 37*Power(s2,2)*Power(t2,3) + 
         18*Power(s2,3)*Power(t2,3) - Power(s2,4)*Power(t2,3) + 
         306*t1*Power(t2,3) - 86*s2*t1*Power(t2,3) - 
         51*Power(s2,2)*t1*Power(t2,3) + 49*Power(t1,2)*Power(t2,3) + 
         48*s2*Power(t1,2)*Power(t2,3) + 
         6*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         15*Power(t1,3)*Power(t2,3) - 8*s2*Power(t1,3)*Power(t2,3) + 
         3*Power(t1,4)*Power(t2,3) - 127*Power(t2,4) + 
         70*s2*Power(t2,4) + 28*Power(s2,2)*Power(t2,4) - 
         Power(s2,3)*Power(t2,4) - 107*t1*Power(t2,4) - 
         50*s2*t1*Power(t2,4) - 2*Power(s2,2)*t1*Power(t2,4) + 
         22*Power(t1,2)*Power(t2,4) + 7*s2*Power(t1,2)*Power(t2,4) - 
         4*Power(t1,3)*Power(t2,4) + 56*Power(t2,5) + 14*s2*Power(t2,5) - 
         2*Power(s2,2)*Power(t2,5) - 9*t1*Power(t2,5) + 
         s2*t1*Power(t2,5) + Power(t1,2)*Power(t2,5) + 
         Power(s1,5)*(-9 + 2*Power(s2,2) - 2*Power(t1,2) + 
            s2*(7 - 19*t2) - 48*t2 + 10*Power(t2,2) + t1*(3 + t2)) - 
         Power(s,5)*(3*Power(s1,2) + Power(s2,2) + 
            s1*(4 - 2*s2 + 15*t1 - 15*t2) + s2*(4 + 3*t1 - 3*t2) - 
            2*(5 - t1 + 2*Power(t1,2) + t2 + 2*t1*t2 - 4*Power(t2,2))) + 
         Power(s,4)*(13*Power(s1,3) + 2*Power(s2,3) + 15*t1 - 
            6*Power(t1,2) + 5*Power(t1,3) + 
            Power(s2,2)*(18 + t1 - 3*t2) + 35*t2 + 2*t1*t2 + 
            8*Power(t1,2)*t2 + 4*Power(t2,2) - t1*Power(t2,2) - 
            12*Power(t2,3) - Power(s1,2)*(-24 + s2 - 38*t1 + 50*t2) - 
            s2*(27 + 12*t1 + 8*Power(t1,2) + 8*t2 + 5*t1*t2 - 
               13*Power(t2,2)) + 
            s1*(-43 + 16*t1 - 22*Power(t1,2) + 2*s2*(7 + 11*t1 - 9*t2) - 
               36*t2 - 25*t1*t2 + 47*Power(t2,2))) + 
         Power(s1,4)*(-71 + 4*Power(t1,3) + 
            2*Power(s2,2)*(4 + 2*t1 - 5*t2) + 2*Power(t1,2)*(-8 + t2) + 
            103*t2 + 69*Power(t2,2) - 10*Power(t2,3) + 
            t1*(-53 + 12*t2 - 3*Power(t2,2)) + 
            4*s2*(10 - 2*Power(t1,2) - 8*t2 + 9*Power(t2,2) + 
               2*t1*(1 + t2))) + 
         Power(s1,3)*(-58 + Power(s2,4) - 3*Power(t1,4) + 
            Power(t1,3)*(3 - 7*t2) + 331*t2 - 316*Power(t2,2) - 
            39*Power(t2,3) + 5*Power(t2,4) + Power(s2,3)*(11 + t2) + 
            4*Power(t1,2)*(-25 + 9*t2 + Power(t2,2)) + 
            t1*(-163 + 319*t2 - 47*Power(t2,2) + 3*Power(t2,3)) - 
            Power(s2,2)*(56 + 6*Power(t1,2) + 28*t2 - 20*Power(t2,2) + 
               t1*(19 + 9*t2)) + 
            s2*(105 + 8*Power(t1,3) - 220*t2 + 45*Power(t2,2) - 
               34*Power(t2,3) + 5*Power(t1,2)*(1 + 3*t2) - 
               4*t1*(-39 + 2*t2 + 6*Power(t2,2)))) - 
         Power(s1,2)*(-11 - 179*t2 + 578*Power(t2,2) - 415*Power(t2,3) - 
            3*Power(t2,4) + Power(t2,5) + Power(s2,4)*(-10 + 3*t2) - 
            Power(t1,4)*(2 + 9*t2) + 
            2*Power(t1,3)*(9 + 10*t2 + Power(t2,2)) + 
            Power(s2,3)*(-35 + 32*t1 + 4*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(197 - 285*t2 + 3*Power(t2,2) + 5*Power(t2,3)) + 
            t1*(174 - 651*t2 + 593*Power(t2,2) - 37*Power(t2,3) + 
               Power(t2,4)) - 
            2*Power(s2,2)*(-82 + 96*t2 + 30*Power(t2,2) - 
               10*Power(t2,3) + 9*Power(t1,2)*(2 + t2) + 
               2*t1*(-22 - 3*t2 + Power(t2,2))) + 
            s2*(-117 + 448*t2 - 399*Power(t2,2) + 8*Power(t2,3) - 
               16*Power(t2,4) + 8*Power(t1,3)*(2 + 3*t2) - 
               Power(t1,2)*(71 + 36*t2 + Power(t2,2)) + 
               t1*(-361 + 477*t2 + 57*Power(t2,2) - 25*Power(t2,3)))) + 
         s1*(18 - 28*t2 - 194*Power(t2,2) + 445*Power(t2,3) - 
            249*Power(t2,4) + 3*Power(t2,5) + 
            Power(t1,4)*(12 - 4*t2 - 9*Power(t2,2)) + 
            Power(s2,4)*(16 - 20*t2 + 3*Power(t2,2)) + 
            Power(t1,3)*(-31 - 6*t2 + 32*Power(t2,2) + 9*Power(t2,3)) - 
            Power(t1,2)*(148 - 411*t2 + 234*Power(t2,2) + 
               39*Power(t2,3)) + 
            t1*(-79 + 451*t2 - 798*Power(t2,2) + 434*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(s2,3)*(38 - 20*t2 - 25*Power(t2,2) + 3*Power(t2,3) + 
               t1*(-60 + 64*t2)) + 
            Power(s2,2)*(-139 + 360*t2 - 173*Power(t2,2) - 
               68*Power(t2,3) + 10*Power(t2,4) - 
               6*Power(t1,2)*(-14 + 12*t2 + 3*Power(t2,2)) + 
               t1*(-107 + 34*t2 + 82*Power(t2,2) + 3*Power(t2,3))) + 
            s2*(61 - 331*t2 + 576*Power(t2,2) - 289*Power(t2,3) - 
               26*Power(t2,4) - 3*Power(t2,5) + 
               4*Power(t1,3)*(-13 + 8*t2 + 6*Power(t2,2)) - 
               Power(t1,2)*(-100 + 8*t2 + 89*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(287 - 771*t2 + 407*Power(t2,2) + 107*Power(t2,3) - 
                  10*Power(t2,4)))) - 
         Power(s,3)*(60 + 22*Power(s1,4) + Power(s2,4) + 29*t1 - 
            48*Power(t1,2) + Power(t1,3) - 3*Power(t1,4) + 
            Power(s1,3)*(60 + 13*s2 + 44*t1 - 85*t2) - 
            5*Power(s2,3)*(-4 + t2) - 29*t2 + 36*t1*t2 + 
            26*Power(t1,2)*t2 - 11*Power(t1,3)*t2 - 88*Power(t2,2) - 
            35*t1*Power(t2,2) - Power(t1,2)*Power(t2,2) + 
            8*Power(t2,3) + 7*t1*Power(t2,3) + 8*Power(t2,4) - 
            Power(s2,2)*(6 + 6*Power(t1,2) + 28*t2 - 5*Power(t2,2) + 
               t1*(39 + t2)) - 
            Power(s1,2)*(88 + 3*Power(s2,2) + 44*Power(t1,2) + 132*t2 - 
               107*Power(t2,2) + t1*(-41 + 45*t2) + 
               s2*(-25 - 47*t1 + 62*t2)) + 
            s2*(-81 + 8*Power(t1,3) + 54*t2 + 20*Power(t2,2) - 
               21*Power(t2,3) + Power(t1,2)*(18 + 17*t2) + 
               t1*(54 + 2*t2 - 4*Power(t2,2))) + 
            s1*(-27 + 5*Power(s2,3) + 19*Power(t1,3) + 
               Power(s2,2)*(59 + 9*t1 - 4*t2) + 166*t2 + 
               62*Power(t2,2) - 52*Power(t2,3) + 
               4*Power(t1,2)*(-8 + 9*t2) + 
               t1*(6 + 10*t2 - 3*Power(t2,2)) - 
               s2*(60 + 33*Power(t1,2) + 39*t2 - 69*Power(t2,2) + 
                  t1*(27 + 32*t2)))) + 
         Power(s,2)*(80 + 18*Power(s1,5) - t1 - 126*Power(t1,2) - 
            27*Power(t1,3) + 2*Power(t1,4) + 
            Power(s1,4)*(76 + 25*s2 + 24*t1 - 75*t2) + 
            Power(s2,4)*(10 - 3*t2) - 179*t2 + 165*t1*t2 + 
            226*Power(t1,2)*t2 - 17*Power(t1,3)*t2 + 9*Power(t1,4)*t2 - 
            39*Power(t2,2) - 281*t1*Power(t2,2) - 
            12*Power(t1,2)*Power(t2,2) + 3*Power(t1,3)*Power(t2,2) + 
            182*Power(t2,3) + 47*t1*Power(t2,3) - 
            5*Power(t1,2)*Power(t2,3) - 20*Power(t2,4) - 
            5*t1*Power(t2,4) - 2*Power(t2,5) + 
            Power(s2,3)*(48 - 32*t1 - 22*t2 + 3*Power(t2,2)) + 
            Power(s1,3)*(-96 - 40*Power(t1,2) + 
               s2*(31 + 40*t1 - 101*t2) + t1*(45 - 30*t2) - 212*t2 + 
               115*Power(t2,2)) + 
            Power(s2,2)*(-94 + 142*t2 + 30*Power(t2,2) - 
               7*Power(t2,3) + 18*Power(t1,2)*(2 + t2) - 
               3*t1*(41 - 9*t2 + Power(t2,2))) + 
            s2*(-53 + 23*t2 + 104*Power(t2,2) - 18*Power(t2,3) + 
               15*Power(t2,4) - 8*Power(t1,3)*(2 + 3*t2) - 
               3*Power(t1,2)*(-34 - 4*t2 + Power(t2,2)) + 
               2*t1*(110 - 184*t2 - 9*Power(t2,2) + 6*Power(t2,3))) + 
            Power(s1,2)*(-125 + 4*Power(s2,3) + 27*Power(t1,3) + 
               Power(s2,2)*(73 + 19*t1 - 11*t2) + 343*t2 + 
               170*Power(t2,2) - 79*Power(t2,3) + 
               Power(t1,2)*(-62 + 50*t2) + 
               t1*(-78 + 21*t2 - 10*Power(t2,2)) - 
               s2*(6 + 50*Power(t1,2) + 84*t2 - 140*Power(t2,2) + 
                  t1*(11 + 39*t2))) + 
            s1*(109 + 3*Power(s2,4) - 9*Power(t1,4) + 
               Power(t1,3)*(5 - 29*t2) + Power(s2,3)*(50 - 7*t2) + 
               152*t2 - 427*Power(t2,2) - 14*Power(t2,3) + 
               23*Power(t2,4) + 
               Power(t1,2)*(-185 + 82*t2 - 6*Power(t2,2)) + 
               t1*(-71 + 354*t2 - 113*Power(t2,2) + 21*Power(t2,3)) - 
               Power(s2,2)*(60 + 18*Power(t1,2) + 81*t2 - 
                  18*Power(t2,2) + 5*t1*(19 + 3*t2)) + 
               s2*(-93 + 24*Power(t1,3) - 67*t2 + 75*Power(t2,2) - 
                  79*Power(t2,3) + Power(t1,2)*(40 + 51*t2) - 
                  t1*(-245 + t2 + 12*Power(t2,2))))) + 
         s*(-30 - 7*Power(s1,6) + 27*t1 + 116*Power(t1,2) + 
            39*Power(t1,3) - 12*Power(t1,4) - 
            Power(s1,5)*(48 + 17*s2 + 5*t1 - 32*t2) + 133*t2 - 
            234*t1*t2 - 368*Power(t1,2)*t2 - 3*Power(t1,3)*t2 + 
            4*Power(t1,4)*t2 - 62*Power(t2,2) + 532*t1*Power(t2,2) + 
            227*Power(t1,2)*Power(t2,2) - 31*Power(t1,3)*Power(t2,2) + 
            9*Power(t1,4)*Power(t2,2) - 203*Power(t2,3) - 
            337*t1*Power(t2,3) + 30*Power(t1,2)*Power(t2,3) - 
            7*Power(t1,3)*Power(t2,3) + 175*Power(t2,4) + 
            7*t1*Power(t2,4) - Power(t1,2)*Power(t2,4) - 10*Power(t2,5) - 
            t1*Power(t2,5) + Power(s2,4)*(-16 + 20*t2 - 3*Power(t2,2)) - 
            Power(s2,3)*(46 - 33*t2 - 16*Power(t2,2) + Power(t2,3) + 
               t1*(-60 + 64*t2)) + 
            Power(s1,4)*(50 - 4*Power(s2,2) + 16*Power(t1,2) + 162*t2 - 
               57*Power(t2,2) + t1*(-21 + 5*t2) + 
               s2*(-23 - 12*t1 + 73*t2)) - 
            Power(s1,3)*(-169 + Power(s2,3) + 17*Power(t1,3) + 
               5*Power(s2,2)*(8 + 3*t1 - 4*t2) + 315*t2 + 
               181*Power(t2,2) - 49*Power(t2,3) + 
               4*Power(t1,2)*(-13 + 6*t2) + 
               t1*(-122 + 25*t2 - 11*Power(t2,2)) + 
               s2*(67 - 33*Power(t1,2) - 4*t1*(-3 + t2) - 85*t2 + 
                  120*Power(t2,2))) + 
            Power(s2,2)*(107 - 318*t2 + 173*Power(t2,2) + 
               48*Power(t2,3) - 6*Power(t2,4) + 
               6*Power(t1,2)*(-14 + 12*t2 + 3*Power(t2,2)) - 
               t1*(-131 + 69*t2 + 63*Power(t2,2) + 5*Power(t2,3))) + 
            s2*(-9 + 117*t2 - 319*Power(t2,2) + 201*Power(t2,3) + 
               12*Power(t2,4) + 4*Power(t2,5) - 
               4*Power(t1,3)*(-13 + 8*t2 + 6*Power(t2,2)) + 
               Power(t1,2)*(-124 + 39*t2 + 78*Power(t2,2) + 
                  13*Power(t2,3)) + 
               t1*(-223 + 686*t2 - 400*Power(t2,2) - 78*Power(t2,3) + 
                  7*Power(t2,4))) + 
            Power(s1,2)*(5 - 3*Power(s2,4) + 9*Power(t1,4) + 
               Power(s2,3)*(-41 + t2) - 508*t2 + 658*Power(t2,2) + 
               58*Power(t2,3) - 20*Power(t2,4) + 
               Power(t1,3)*(-7 + 25*t2) + 
               Power(t1,2)*(237 - 92*t2 + Power(t2,2)) + 
               t1*(259 - 629*t2 + 122*Power(t2,2) - 18*Power(t2,3)) + 
               Power(s2,2)*(110 + 18*Power(t1,2) + 82*t2 - 
                  34*Power(t2,2) + t1*(75 + 23*t2)) - 
               s2*(89 + 24*Power(t1,3) - 334*t2 + 98*Power(t2,2) - 
                  93*Power(t2,3) + Power(t1,2)*(27 + 49*t2) + 
                  t1*(347 - 10*t2 - 33*Power(t2,2)))) + 
            s1*(-107 + 63*t2 + 540*Power(t2,2) - 568*Power(t2,3) + 
               19*Power(t2,4) + 3*Power(t2,5) + 
               Power(s2,4)*(-20 + 6*t2) - 2*Power(t1,4)*(2 + 9*t2) + 
               Power(t1,3)*(45 + 37*t2 - Power(t2,2)) + 
               Power(s2,3)*(-83 + 64*t1 + 25*t2 + Power(t2,2)) + 
               Power(t1,2)*(319 - 500*t2 + 11*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*(155 - 778*t2 + 851*Power(t2,2) - 83*Power(t2,3) + 
                  8*Power(t2,4)) - 
               Power(s2,2)*(-254 + 326*t2 + 90*Power(t2,2) - 
                  24*Power(t2,3) + 36*Power(t1,2)*(2 + t2) + 
                  t1*(-211 + 13*t2 + 3*Power(t2,2))) + 
               s2*(-44 + 385*t2 - 477*Power(t2,2) + 24*Power(t2,3) - 
                  33*Power(t2,4) + 16*Power(t1,3)*(2 + 3*t2) + 
                  Power(t1,2)*(-173 - 49*t2 + 3*Power(t2,2)) + 
                  t1*(-573 + 826*t2 + 79*Power(t2,2) - 32*Power(t2,3))))))*
       R1(1 - s + s1 - t2))/
     ((s - s2 + t1)*(-1 + s - s1 + t2)*Power(s - s1 + t2,2)*
       Power(-s1 + s2 - t1 + t2,2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (16*(4 + Power(s,5) + Power(s1,5) + 68*s2 - 64*Power(s2,2) - 
         12*Power(s2,3) + 4*Power(s2,4) - 68*t1 + 128*s2*t1 + 
         36*Power(s2,2)*t1 - 16*Power(s2,3)*t1 - 64*Power(t1,2) - 
         36*s2*Power(t1,2) + 24*Power(s2,2)*Power(t1,2) + 
         12*Power(t1,3) - 16*s2*Power(t1,3) + 4*Power(t1,4) + 
         Power(s1,4)*(-1 + 2*s2 - t1 - 3*t2) + 27*t2 - 154*s2*t2 + 
         35*Power(s2,2)*t2 + 12*Power(s2,3)*t2 + 153*t1*t2 - 
         70*s2*t1*t2 - 33*Power(s2,2)*t1*t2 - 2*Power(s2,3)*t1*t2 + 
         35*Power(t1,2)*t2 + 30*s2*Power(t1,2)*t2 + 
         6*Power(s2,2)*Power(t1,2)*t2 - 9*Power(t1,3)*t2 - 
         6*s2*Power(t1,3)*t2 + 2*Power(t1,4)*t2 - 55*Power(t2,2) + 
         50*s2*Power(t2,2) + 14*Power(s2,2)*Power(t2,2) - 
         Power(s2,3)*Power(t2,2) - 51*t1*Power(t2,2) - 
         23*s2*t1*Power(t2,2) - Power(s2,2)*t1*Power(t2,2) + 
         9*Power(t1,2)*Power(t2,2) + 5*s2*Power(t1,2)*Power(t2,2) - 
         3*Power(t1,3)*Power(t2,2) + 16*Power(t2,3) + 10*s2*Power(t2,3) - 
         2*Power(s2,2)*Power(t2,3) - 7*t1*Power(t2,3) + 
         s2*t1*Power(t2,3) + Power(t1,2)*Power(t2,3) - 
         Power(s,4)*(-3 + 5*s1 + 4*s2 - 5*t1 + t2) - 
         Power(s1,3)*(-9 + s2 + t1 - 2*s2*t1 + 2*Power(t1,2) - 2*t2 + 
            7*s2*t2 - 5*t1*t2 - 3*Power(t2,2)) + 
         Power(s,3)*(13 + 8*Power(s1,2) + 5*Power(s2,2) + 3*t1 + 
            6*Power(t1,2) + 3*t2 + 4*t1*t2 - 4*Power(t2,2) - 
            s2*(2 + 11*t1 + t2) + 2*s1*(-8 + 7*s2 - 9*t1 + 2*t2)) - 
         Power(s1,2)*(30 + Power(s2,3) - 4*Power(t1,3) - 13*t2 + 
            10*Power(t2,2) + Power(t2,3) + 
            Power(s2,2)*(-1 - 6*t1 + 2*t2) + Power(t1,2)*(-4 + 3*t2) + 
            2*t1*(5 - 2*t2 + 2*Power(t2,2)) + 
            s2*(9*Power(t1,2) - 5*t1*(-1 + t2) + 
               2*(-5 + t2 - 4*Power(t2,2)))) - 
         Power(s,2)*(4*Power(s1,3) + 2*Power(s2,3) + 
            2*Power(s1,2)*(-11 + 7*s2 - 10*t1 + 4*t2) - 
            Power(s2,2)*(3 + 8*t1 + 5*t2) + 
            s1*(19 + 12*Power(s2,2) + 11*t1 + 16*Power(t1,2) + 16*t2 + 
               7*t1*t2 - 11*Power(t2,2) + s2*(-7 - 28*t1 + t2)) + 
            s2*(47 + 10*Power(t1,2) - 4*t2 - 7*Power(t2,2) + 
               t1*(7 + 9*t2)) + 
            2*(-13 - 2*Power(t1,3) + 4*t2 + Power(t2,2) + Power(t2,3) - 
               2*Power(t1,2)*(1 + t2) + t1*(-23 + t2 + Power(t2,2)))) + 
         s1*(-39 - 2*Power(t1,4) + 97*t2 - 42*Power(t2,2) + 
            9*Power(t2,3) - Power(t1,3)*(7 + t2) + 
            2*Power(s2,3)*(2 + t1 + t2) + 
            t1*(-89 + 73*t2 - 4*Power(t2,2)) + 
            Power(t1,2)*(-55 + 7*t2 + 4*Power(t2,2)) - 
            Power(s2,2)*(55 + 6*Power(t1,2) - 5*t2 - 4*Power(t2,2) + 
               5*t1*(3 + t2)) + 
            s2*(90 + 6*Power(t1,3) - 72*t2 + Power(t2,2) - 
               3*Power(t2,3) + 2*Power(t1,2)*(9 + 2*t2) - 
               2*t1*(-55 + 6*t2 + 4*Power(t2,2)))) - 
         s*(41 + Power(s1,4) - 25*t1 - 71*Power(t1,2) - 7*Power(t1,3) - 
            2*Power(t1,4) - 35*t2 + 41*t1*t2 + 11*Power(t1,2)*t2 - 
            Power(t1,3)*t2 - 7*Power(t2,2) - 4*t1*Power(t2,2) + 
            Power(t1,2)*Power(t2,2) + 6*Power(t2,3) + t1*Power(t2,3) + 
            Power(s2,3)*(4 + 2*t1 + 3*t2) - 
            2*Power(s1,3)*(-4 + s2 - 3*t1 + 4*t2) + 
            Power(s1,2)*(3 - 7*Power(s2,2) - 12*Power(t1,2) + 
               s2*(4 + 19*t1 - 9*t2) - 11*t2 + 10*Power(t2,2) + 
               t1*(-9 + 2*t2)) - 
            Power(s2,2)*(71 + 6*Power(t1,2) - 7*t2 - 2*Power(t2,2) + 
               t1*(15 + 7*t2)) + 
            s2*(26 + 6*Power(t1,3) - 39*t2 - 4*Power(t2,3) + 
               Power(t1,2)*(18 + 5*t2) + t1*(142 - 18*t2 - 3*Power(t2,2))\
) + s1*(-16 - 3*Power(s2,3) + 8*Power(t1,3) + 15*t2 - 4*Power(t2,2) - 
               3*Power(t2,3) + 3*Power(t1,2)*(4 + t2) + 
               Power(s2,2)*(8 + 14*t1 + 5*t2) - 
               2*t1*(-14 + t2 + 3*Power(t2,2)) - 
               s2*(29 + 19*Power(t1,2) - 6*t2 - 15*Power(t2,2) + 
                  4*t1*(5 + 2*t2)))))*R2(1 - s2 + t1 - t2))/
     ((s - s2 + t1)*Power(s - s1 + t2,2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) + 
    (8*(-36 + 24*s2 + 48*Power(s2,2) - 24*Power(s2,3) - 12*Power(s2,4) - 
         24*t1 - 96*s2*t1 + 72*Power(s2,2)*t1 + 48*Power(s2,3)*t1 + 
         48*Power(t1,2) - 72*s2*Power(t1,2) - 
         72*Power(s2,2)*Power(t1,2) + 24*Power(t1,3) + 
         48*s2*Power(t1,3) - 12*Power(t1,4) - 
         Power(s1,6)*(Power(s2,2) - 2*s2*(-2 + t1) + (-4 + t1)*t1) + 
         Power(s,7)*(t1 - t2) + 115*t2 + 17*s2*t2 - 169*Power(s2,2)*t2 + 
         19*Power(s2,3)*t2 + 18*Power(s2,4)*t2 - 20*t1*t2 + 
         349*s2*t1*t2 - 68*Power(s2,2)*t1*t2 - 71*Power(s2,3)*t1*t2 + 
         2*Power(s2,4)*t1*t2 - 180*Power(t1,2)*t2 + 
         79*s2*Power(t1,2)*t2 + 105*Power(s2,2)*Power(t1,2)*t2 - 
         8*Power(s2,3)*Power(t1,2)*t2 - 30*Power(t1,3)*t2 - 
         69*s2*Power(t1,3)*t2 + 12*Power(s2,2)*Power(t1,3)*t2 + 
         17*Power(t1,4)*t2 - 8*s2*Power(t1,4)*t2 + 2*Power(t1,5)*t2 - 
         66*Power(t2,2) - 240*s2*Power(t2,2) + 
         184*Power(s2,2)*Power(t2,2) + 38*Power(s2,3)*Power(t2,2) - 
         10*Power(s2,4)*Power(t2,2) - 2*Power(s2,5)*Power(t2,2) + 
         234*t1*Power(t2,2) - 377*s2*t1*Power(t2,2) - 
         89*Power(s2,2)*t1*Power(t2,2) + 33*Power(s2,3)*t1*Power(t2,2) + 
         7*Power(s2,4)*t1*Power(t2,2) + 193*Power(t1,2)*Power(t2,2) + 
         64*s2*Power(t1,2)*Power(t2,2) - 
         39*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         8*Power(s2,3)*Power(t1,2)*Power(t2,2) - 
         13*Power(t1,3)*Power(t2,2) + 19*s2*Power(t1,3)*Power(t2,2) + 
         2*Power(s2,2)*Power(t1,3)*Power(t2,2) - 
         3*Power(t1,4)*Power(t2,2) + 2*s2*Power(t1,4)*Power(t2,2) - 
         Power(t1,5)*Power(t2,2) - 97*Power(t2,3) + 303*s2*Power(t2,3) - 
         56*Power(s2,2)*Power(t2,3) - 32*Power(s2,3)*Power(t2,3) + 
         5*Power(s2,4)*Power(t2,3) + Power(s2,5)*Power(t2,3) - 
         274*t1*Power(t2,3) + 102*s2*t1*Power(t2,3) + 
         74*Power(s2,2)*t1*Power(t2,3) - 14*Power(s2,3)*t1*Power(t2,3) - 
         4*Power(s2,4)*t1*Power(t2,3) - 46*Power(t1,2)*Power(t2,3) - 
         52*s2*Power(t1,2)*Power(t2,3) + 
         12*Power(s2,2)*Power(t1,2)*Power(t2,3) + 
         6*Power(s2,3)*Power(t1,2)*Power(t2,3) + 
         10*Power(t1,3)*Power(t2,3) - 2*s2*Power(t1,3)*Power(t2,3) - 
         4*Power(s2,2)*Power(t1,3)*Power(t2,3) - 
         Power(t1,4)*Power(t2,3) + s2*Power(t1,4)*Power(t2,3) + 
         100*Power(t2,4) - 108*s2*Power(t2,4) + 
         4*Power(s2,2)*Power(t2,4) + 8*Power(s2,3)*Power(t2,4) + 
         84*t1*Power(t2,4) - 16*Power(s2,2)*t1*Power(t2,4) - 
         4*Power(t1,2)*Power(t2,4) + 8*s2*Power(t1,2)*Power(t2,4) - 
         16*Power(t2,5) + 8*s2*Power(t2,5) - 4*t1*Power(t2,5) + 
         Power(s,6)*(1 + 4*Power(t1,2) - t2 + 5*s2*t2 - 2*Power(t2,2) - 
            t1*(1 + 4*s2 + 2*t2) + s1*(-2 + s2 - 6*t1 + 7*t2)) + 
         Power(s1,5)*(4 - Power(s2,3) + Power(t1,3) - 4*t2 + 
            4*Power(t1,2)*t2 - 8*t1*(1 + 2*t2) + 
            Power(s2,2)*(-9 + 3*t1 + 5*t2) - 
            3*s2*(Power(t1,2) + 3*t1*(-1 + t2) - 4*(1 + t2))) - 
         Power(s,5)*(2 + 15*t1 + 14*Power(t1,2) - 6*Power(t1,3) - 
            21*t2 - 14*t1*t2 - 2*Power(t1,2)*t2 + 12*Power(t2,2) + 
            7*t1*Power(t2,2) + Power(t2,3) + 
            Power(s2,2)*(-6*t1 + 10*t2) + 
            Power(s1,2)*(-8 + 3*s2 - 13*t1 + 18*t2) + 
            2*s2*(1 - 7*t1 + 6*Power(t1,2) + 3*t2 - 4*t1*t2 - 
               5*Power(t2,2)) + 
            s1*(3 + 4*Power(s2,2) - 5*t1 + 19*Power(t1,2) + 5*t2 - 
               13*t1*t2 - 12*Power(t2,2) + s2*(-6 - 23*t1 + 28*t2))) + 
         Power(s1,4)*(-11 + Power(s2,4) + Power(t1,4) - 6*t2 + 
            17*Power(t2,2) - Power(t1,3)*(5 + 8*t2) + 
            Power(s2,3)*(-12 - 4*t1 + 9*t2) + 
            Power(s2,2)*(15 + 6*Power(t1,2) + t1*(19 - 26*t2) + 25*t2 - 
               7*Power(t2,2)) + 
            Power(t1,2)*(-8 + 12*t2 - 3*Power(t2,2)) + 
            t1*(-11 + 50*t2 + 13*Power(t2,2)) - 
            s2*(-6 + 4*Power(t1,3) + Power(t1,2)*(2 - 25*t2) + 52*t2 + 
               6*Power(t2,2) + t1*(7 + 37*t2 - 10*Power(t2,2)))) + 
         Power(s1,3)*(3 + Power(s2,5) - Power(t1,5) + 49*t2 - 
            35*Power(t2,2) - 17*Power(t2,3) + 2*Power(t1,4)*(9 + 2*t2) + 
            Power(s2,4)*(-1 - 5*t1 + 3*t2) + 
            Power(t1,3)*(5 - 30*t2 + 7*Power(t2,2)) + 
            Power(t1,2)*(-3 + 12*t2 + 11*Power(t2,2)) - 
            t1*(-14 - 45*t2 + 106*Power(t2,2) + Power(t2,3)) + 
            Power(s2,3)*(-1 + 10*Power(t1,2) + 50*t2 - 15*Power(t2,2) - 
               t1*(15 + 13*t2)) + 
            Power(s2,2)*(27 - 10*Power(t1,3) - 61*t2 + 3*Power(t2,2) + 
               3*Power(t2,3) + 3*Power(t1,2)*(17 + 7*t2) + 
               t1*(7 - 130*t2 + 37*Power(t2,2))) + 
            s2*(-29 + 5*Power(t1,4) - 14*t2 + 97*Power(t2,2) - 
               6*Power(t2,3) - Power(t1,3)*(53 + 15*t2) + 
               Power(t1,2)*(-11 + 110*t2 - 29*Power(t2,2)) - 
               t1*(24 - 49*t2 + 14*Power(t2,2) + 3*Power(t2,3)))) - 
         Power(s1,2)*(-15 + 5*Power(t1,5) + Power(s2,5)*(-2 + t2) + 
            21*t2 + 87*Power(t2,2) - 89*Power(t2,3) - 4*Power(t2,4) + 
            Power(t1,4)*(-43 + 20*t2 + 5*Power(t2,2)) + 
            Power(s2,4)*(-34 + t1*(13 - 4*t2) - 17*t2 + 9*Power(t2,2)) - 
            2*Power(t1,3)*(30 - 64*t2 + 25*Power(t2,2)) + 
            Power(t1,2)*(30 + 107*t2 - 137*Power(t2,2) + 
               43*Power(t2,3)) + 
            t1*(27 + 4*t2 - 11*Power(t2,2) - 28*Power(t2,3) - 
               4*Power(t2,4)) + 
            Power(s2,3)*(32 - 109*t2 + 50*Power(t2,2) - 7*Power(t2,3) + 
               Power(t1,2)*(-32 + 6*t2) + 
               t1*(145 + 31*t2 - 32*Power(t2,2))) + 
            Power(s2,2)*(47 + Power(t1,3)*(38 - 4*t2) + 149*t2 - 
               232*Power(t2,2) + 47*Power(t2,3) + 
               3*Power(t1,2)*(-77 + 3*t2 + 14*Power(t2,2)) + 
               2*t1*(-62 + 173*t2 - 75*Power(t2,2) + 7*Power(t2,3))) + 
            s2*(-28 + Power(t1,4)*(-22 + t2) - 29*t2 + 52*Power(t2,2) + 
               17*Power(t2,3) + 
               Power(t1,3)*(163 - 43*t2 - 24*Power(t2,2)) + 
               Power(t1,2)*(152 - 365*t2 + 150*Power(t2,2) - 
                  7*Power(t2,3)) - 
               t1*(77 + 256*t2 - 369*Power(t2,2) + 90*Power(t2,3)))) + 
         s1*(-43 + 55*t2 + 35*Power(t2,2) - 15*Power(t2,3) - 
            28*Power(t2,4) - 4*Power(t2,5) + 
            Power(t1,4)*(23 - 4*t2 - 13*Power(t2,2)) - 
            Power(s2,5)*(-12 + 4*t2 + Power(t2,2)) + 
            Power(t1,5)*(-14 + 10*t2 + Power(t2,2)) + 
            Power(t1,3)*(94 - 191*t2 + 81*Power(t2,2) + 9*Power(t2,3)) + 
            Power(t1,2)*(20 - 211*t2 + 332*Power(t2,2) - 
               137*Power(t2,3) + 4*Power(t2,4)) + 
            t1*(-80 + 21*t2 + 224*Power(t2,2) - 209*Power(t2,3) + 
               52*Power(t2,4)) + 
            Power(s2,4)*(22 + 12*t2 - 37*Power(t2,2) + 5*Power(t2,3) + 
               t1*(-62 + 26*t2 + 5*Power(t2,2))) - 
            Power(s2,3)*(83 - 138*t2 + 44*Power(t2,2) + 
               20*Power(t2,3) + 
               2*Power(t1,2)*(-64 + 32*t2 + 5*Power(t2,2)) + 
               t1*(89 + 32*t2 - 124*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s2,2)*(9 - 185*t2 + 354*Power(t2,2) - 
               190*Power(t2,3) + 12*Power(t2,4) + 
               2*Power(t1,3)*(-66 + 38*t2 + 5*Power(t2,2)) + 
               3*Power(t1,2)*
                (45 + 8*t2 - 50*Power(t2,2) + 5*Power(t2,3)) + 
               t1*(260 - 467*t2 + 169*Power(t2,2) + 49*Power(t2,3))) - 
            s2*(-83 + 16*t2 + 263*Power(t2,2) - 248*Power(t2,3) + 
               60*Power(t2,4) + 
               Power(t1,4)*(-68 + 44*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(91 - 76*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,2)*(271 - 520*t2 + 206*Power(t2,2) + 
                  38*Power(t2,3)) + 
               t1*(29 - 396*t2 + 686*Power(t2,2) - 327*Power(t2,3) + 
                  16*Power(t2,4)))) + 
         Power(s,4)*(1 + 16*t1 - 19*Power(t1,2) - 28*Power(t1,3) + 
            4*Power(t1,4) + 2*t2 - 6*t1*t2 + 12*Power(t1,2)*t2 + 
            8*Power(t1,3)*t2 + 40*Power(t2,2) + 5*t1*Power(t2,2) - 
            8*Power(t1,2)*Power(t2,2) - 19*Power(t2,3) - 
            4*t1*Power(t2,3) + Power(s2,3)*(-4*t1 + 10*t2) + 
            2*Power(s1,3)*(-6 + s2 - 6*t1 + 11*t2) + 
            2*Power(s2,2)*(2 + 6*Power(t1,2) + 13*t2 - 10*Power(t2,2) - 
               2*t1*(7 + 3*t2)) + 
            Power(s1,2)*(11*Power(s2,2) + 33*Power(t1,2) + 
               4*(7 - 6*t2)*t2 - 2*t1*(5 + 17*t2) + 
               s2*(-22 - 44*t1 + 62*t2)) + 
            s2*(-6 - 12*Power(t1,3) + Power(t1,2)*(56 - 6*t2) - 56*t2 + 
               22*Power(t2,2) + 5*Power(t2,3) + 
               t1*(15 - 38*t2 + 28*Power(t2,2))) + 
            s1*(20 + 6*Power(s2,3) - 21*Power(t1,3) + 
               Power(t1,2)*(62 - 3*t2) - 102*t2 + 39*Power(t2,2) + 
               5*Power(t2,3) + Power(s2,2)*(-4 - 33*t1 + 42*t2) + 
               t1*(64 - 71*t2 + 34*Power(t2,2)) + 
               s2*(48*Power(t1,2) + (60 - 49*t2)*t2 - t1*(58 + 39*t2)))) \
+ Power(s,3)*(7 + 44*t1 + 65*Power(t1,2) + 9*Power(t1,3) - 
            18*Power(t1,4) + Power(t1,5) + 
            Power(s1,4)*(8 + 2*s2 + 3*t1 - 13*t2) + 
            Power(s2,4)*(t1 - 5*t2) - 64*t2 - 61*t1*t2 - 
            56*Power(t1,2)*t2 - 26*Power(t1,3)*t2 + 7*Power(t1,4)*t2 + 
            88*Power(t2,2) + 59*t1*Power(t2,2) + 
            38*Power(t1,2)*Power(t2,2) - 2*Power(t1,3)*Power(t2,2) + 
            8*Power(t2,3) - 26*t1*Power(t2,3) - 
            6*Power(t1,2)*Power(t2,3) - 8*Power(t2,4) + 
            Power(s2,3)*(2 - 4*Power(t1,2) - 32*t2 + 20*Power(t2,2) + 
               2*t1*(9 + 4*t2)) + 
            Power(s1,3)*(8 - 8*Power(s2,2) - 24*Power(t1,2) + 
               s2*(34 + 32*t1 - 68*t2) - 44*t2 + 20*Power(t2,2) + 
               t1*(6 + 44*t2)) + 
            Power(s2,2)*(8 + 6*Power(t1,3) + 6*Power(t1,2)*(-9 + t2) + 
               58*t2 + 8*Power(t2,2) - 10*Power(t2,3) + 
               t1*(5 + 38*t2 - 42*Power(t2,2))) - 
            s2*(17 + 4*Power(t1,4) - 44*t2 + 126*Power(t2,2) - 
               52*Power(t2,3) + 2*Power(t1,3)*(-27 + 8*t2) - 
               4*Power(t1,2)*(-4 + 5*t2 + 6*Power(t2,2)) + 
               t1*(73 + 2*t2 + 46*Power(t2,2) - 16*Power(t2,3))) - 
            Power(s1,2)*(42 + 15*Power(s2,3) - 28*Power(t1,3) - 
               164*t2 + 37*Power(t2,2) + 7*Power(t2,3) + 
               3*Power(t1,2)*(32 + 5*t2) + 
               Power(s2,2)*(-23 - 58*t1 + 77*t2) + 
               t1*(80 - 125*t2 + 56*Power(t2,2)) + 
               s2*(14 + 71*Power(t1,2) + 134*t2 - 81*Power(t2,2) - 
                  t1*(73 + 92*t2))) - 
            s1*(10 + 4*Power(s2,4) + 9*Power(t1,4) - 52*t2 + 
               182*Power(t2,2) - 52*Power(t2,3) + 
               Power(t1,3)*(-105 + 17*t2) + 
               Power(s2,3)*(8 - 21*t1 + 28*t2) + 
               Power(t1,2)*(-67 + 76*t2 - 31*Power(t2,2)) + 
               t1*(65 + 7*t2 + Power(t2,2) - 15*Power(t2,3)) + 
               Power(s2,2)*(4 + 39*Power(t1,2) + 146*t2 - 
                  76*Power(t2,2) - t1*(121 + 39*t2)) + 
               s2*(-30 - 31*Power(t1,3) + Power(t1,2)*(218 - 6*t2) - 
                  192*t2 + 56*Power(t2,2) + 20*Power(t2,3) + 
                  t1*(63 - 222*t2 + 107*Power(t2,2))))) + 
         Power(s,2)*(-62 - 97*t1 + 24*Power(t1,2) + 72*Power(t1,3) + 
            10*Power(t1,4) - 3*Power(t1,5) + 92*t2 + Power(s2,5)*t2 + 
            14*t1*t2 - 69*Power(t1,2)*t2 - 4*Power(t1,3)*t2 - 
            27*Power(t1,4)*t2 + 2*Power(t1,5)*t2 - 104*Power(t2,2) + 
            33*t1*Power(t2,2) - 21*Power(t1,2)*Power(t2,2) + 
            12*Power(t1,3)*Power(t2,2) + 2*Power(t1,4)*Power(t2,2) + 
            112*Power(t2,3) + 50*t1*Power(t2,3) + 
            4*Power(t1,2)*Power(t2,3) - 4*Power(t1,3)*Power(t2,3) - 
            20*Power(t2,4) - 16*t1*Power(t2,4) + 
            Power(s1,5)*(-2 - 3*s2 + 2*t1 + 3*t2) - 
            Power(s2,4)*(5 + 3*t1 - 15*t2 + 2*t1*t2 + 10*Power(t2,2)) - 
            Power(s1,4)*(9 + 2*Power(s2,2) - 4*Power(t1,2) + 
               s2*(30 + 2*t1 - 37*t2) - 29*t2 + 6*Power(t2,2) + 
               7*t1*(-1 + 4*t2)) + 
            Power(s2,3)*(-50 - 2*Power(t1,2)*(-6 + t2) - 8*t2 - 
               36*Power(t2,2) + 10*Power(t2,3) + 
               t1*(5 - 18*t2 + 28*Power(t2,2))) + 
            Power(s2,2)*(37 - 196*t2 + 138*Power(t2,2) - 
               42*Power(t2,3) + 2*Power(t1,3)*(-9 + 4*t2) - 
               3*Power(t1,2)*(-5 + 12*t2 + 8*Power(t2,2)) + 
               4*t1*(43 + 3*t2 + 21*Power(t2,2) - 6*Power(t2,3))) + 
            s2*(80 + Power(t1,4)*(12 - 7*t2) + 80*t2 - 
               122*Power(t2,2) - 64*Power(t2,3) + 24*Power(t2,4) + 
               Power(t1,3)*(-25 + 66*t2 + 4*Power(t2,2)) + 
               2*Power(t1,2)*(-97 - 30*Power(t2,2) + 9*Power(t2,3)) + 
               t1*(-61 + 265*t2 - 117*Power(t2,2) + 38*Power(t2,3))) + 
            Power(s1,3)*(36 + 11*Power(s2,3) - 16*Power(t1,3) - 
               110*t2 + 5*Power(t2,2) + 3*Power(t2,3) + 
               Power(t1,2)*(62 + 35*t2) + 
               Power(s2,2)*(-43 - 38*t1 + 73*t2) + 
               t1*(20 - 109*t2 + 38*Power(t2,2)) + 
               s2*(48 + 43*Power(t1,2) + 124*t2 - 55*Power(t2,2) - 
                  t1*(19 + 108*t2))) + 
            Power(s1,2)*(-4 + 9*Power(s2,4) - 127*Power(t1,3) + 
               9*Power(t1,4) - 82*t2 + 229*Power(t2,2) - 
               39*Power(t2,3) + Power(s2,3)*(-2 - 36*t1 + 39*t2) - 
               3*Power(t1,2)*(31 - 42*t2 + 15*Power(t2,2)) + 
               t1*(51 + 116*t2 - 12*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s2,2)*(13 + 54*Power(t1,2) + 225*t2 - 
                  99*Power(t2,2) - 3*t1*(41 + 26*t2)) + 
               s2*(-22 - 36*Power(t1,3) - 292*t2 + 48*Power(t2,2) + 
                  21*Power(t2,3) + 3*Power(t1,2)*(84 + 13*t2) + 
                  t1*(80 - 351*t2 + 144*Power(t2,2)))) + 
            s1*(51 + Power(s2,5) - Power(t1,5) + 
               Power(t1,4)*(62 - 8*t2) + 86*t2 - 198*Power(t2,2) - 
               22*Power(t2,3) + 12*Power(t2,4) + 
               Power(t1,3)*(-67 + 19*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(-195 + 277*t2 - 84*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(-14 + 234*t2 - 356*Power(t2,2) + 81*Power(t2,3)) + 
               Power(s2,4)*(-5*t1 + 7*(2 + t2)) + 
               Power(s2,3)*(44 + 10*Power(t1,2) + 128*t2 - 
                  54*Power(t2,2) - 13*t1*(8 + t2)) - 
               Power(s2,2)*(70 + 10*Power(t1,3) + 
                  3*Power(t1,2)*(-76 + t2) - 18*t2 + 42*Power(t2,2) - 
                  30*Power(t2,3) + t1*(155 + 237*t2 - 117*Power(t2,2))) \
+ s2*(5*Power(t1,4) + Power(t1,3)*(-200 + 17*t2) + 
                  Power(t1,2)*(178 + 90*t2 - 72*Power(t2,2)) + 
                  t1*(265 - 295*t2 + 126*Power(t2,2) - 45*Power(t2,3)) - 
                  2*(20 + 107*t2 - 252*Power(t2,2) + 62*Power(t2,3))))) + 
         s*(91 + Power(s1,6)*(s2 - t1) + 76*t1 - 108*Power(t1,2) - 
            78*Power(t1,3) + 17*Power(t1,4) + 2*Power(t1,5) - 176*t2 + 
            65*t1*t2 + 289*Power(t1,2)*t2 + 59*Power(t1,3)*t2 + 
            7*Power(t1,4)*t2 - 4*Power(t1,5)*t2 + 
            2*Power(s2,5)*(-1 + t2)*t2 + 12*Power(t2,2) - 
            352*t1*Power(t2,2) - 180*Power(t1,2)*Power(t2,2) - 
            3*Power(t1,3)*Power(t2,2) - 10*Power(t1,4)*Power(t2,2) + 
            Power(t1,5)*Power(t2,2) + 73*Power(t2,3) + 
            194*t1*Power(t2,3) + 12*Power(t1,2)*Power(t2,3) + 
            10*Power(t1,3)*Power(t2,3) - Power(t1,4)*Power(t2,3) + 
            12*Power(t2,4) - 4*t1*Power(t2,4) - 
            8*Power(t1,2)*Power(t2,4) - 8*Power(t2,5) + 
            Power(s1,5)*(3 + 4*Power(s2,2) + 3*Power(t1,2) + 
               s2*(16 - 7*t1 - 8*t2) - 7*t2 + t1*(-11 + 7*t2)) + 
            Power(s2,4)*(18 - 15*t2 + 20*Power(t2,2) - 5*Power(t2,3) + 
               t1*(2 + 4*t2 - 7*Power(t2,2))) - 
            Power(s1,4)*(16 + Power(s2,3) - 2*Power(t1,3) + 
               Power(s2,2)*(-4*t1 + 33*(-1 + t2)) - 31*t2 - 
               5*Power(t2,2) + Power(t1,2)*(14 + 23*t2) + 
               s2*(44 + 5*Power(t1,2) + t1*(19 - 56*t2) + 56*t2 - 
                  13*Power(t2,2)) + t1*(-19 - 57*t2 + 9*Power(t2,2))) + 
            Power(s2,2)*(-97 + 293*t2 - 260*Power(t2,2) + 
               88*Power(t2,3) - 24*Power(t2,4) - 
               2*Power(t1,3)*(-6 + 8*t2 + Power(t2,2)) + 
               t1*(-212 + 83*t2 + 81*Power(t2,2) + 2*Power(t2,3)) - 
               3*Power(t1,2)*(-35 + 8*t2 - 10*Power(t2,2) + 
                  6*Power(t2,3))) + 
            Power(s2,3)*(67 - 12*t2 - 42*Power(t2,2) + 4*Power(t2,3) + 
               4*Power(t1,2)*(-2 + t2 + 2*Power(t2,2)) + 
               t1*(-71 + 38*t2 - 50*Power(t2,2) + 16*Power(t2,3))) - 
            s2*(79 + 88*t2 - 448*Power(t2,2) + 280*Power(t2,3) - 
               16*Power(t2,4) + 2*Power(t1,4)*(4 - 7*t2 + Power(t2,2)) + 
               Power(t1,3)*(69 + 6*t2 - 10*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-223 + 130*t2 + 36*Power(t2,2) + 
                  16*Power(t2,3)) + 
               t1*(-205 + 582*t2 - 440*Power(t2,2) + 100*Power(t2,3) - 
                  32*Power(t2,4))) + 
            Power(s1,3)*(24 - 6*Power(s2,4) - 5*Power(t1,4) + 
               Power(s2,3)*(22 + 23*t1 - 30*t2) + 34*t2 - 
               104*Power(t2,2) + 6*Power(t2,3) + 
               Power(t1,3)*(55 + 17*t2) + 
               Power(t1,2)*(53 - 74*t2 + 25*Power(t2,2)) + 
               t1*(9 - 153*t2 - 5*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s2,2)*(-28 - 33*Power(t1,2) - 130*t2 + 
                  50*Power(t2,2) + 11*t1*(1 + 7*t2)) + 
               s2*(-8 + 21*Power(t1,3) + 208*t2 - 8*Power(t2,2) - 
                  6*Power(t2,3) - 8*Power(t1,2)*(11 + 8*t2) + 
                  t1*(-25 + 204*t2 - 75*Power(t2,2)))) + 
            Power(s1,2)*(-49 - 2*Power(s2,5) + Power(t1,5) + 
               Power(s2,4)*(-9 + 9*t1 - 5*t2) - 89*t2 + 
               153*Power(t2,2) + 25*Power(t2,3) - 
               Power(t1,4)*(58 + t2) + 
               Power(t1,3)*(45 + 25*t2 - 18*Power(t2,2)) + 
               Power(t1,2)*(117 - 217*t2 + 51*Power(t2,2) - 
                  7*Power(t2,3)) - 
               t1*(36 + 206*t2 - 399*Power(t2,2) + 66*Power(t2,3)) + 
               Power(s2,3)*(-37 - 16*Power(t1,2) - 136*t2 + 
                  51*Power(t2,2) + t1*(85 + 16*t2)) + 
               Power(s2,2)*(19 + 14*Power(t1,3) + 11*t2 + 
                  39*Power(t2,2) - 21*Power(t2,3) - 
                  3*Power(t1,2)*(67 + 6*t2) + 
                  t1*(119 + 297*t2 - 120*Power(t2,2))) + 
               s2*(78 - 6*Power(t1,4) + 166*t2 - 461*Power(t2,2) + 
                  86*Power(t2,3) + Power(t1,3)*(183 + 8*t2) + 
                  Power(t1,2)*(-127 - 186*t2 + 87*Power(t2,2)) + 
                  2*t1*(-68 + 103*t2 - 45*Power(t2,2) + 14*Power(t2,3)))) \
+ s1*(-1 - 6*Power(s2,5) + 12*Power(t1,5) + 23*t2 + 182*Power(t2,2) - 
               264*Power(t2,3) + 40*Power(t2,4) + 
               Power(t1,4)*(-77 + 39*t2 + Power(t2,2)) + 
               Power(s2,4)*(-53 + 36*t1 - 33*t2 + 16*Power(t2,2)) + 
               Power(t1,3)*(-116 + 166*t2 - 57*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(78 + 50*t2 - 85*Power(t2,2) + 
                  38*Power(t2,3)) + 
               t1*(104 - 86*t2 + 131*Power(t2,2) - 151*Power(t2,3) + 
                  16*Power(t2,4)) + 
               Power(s2,3)*(66 - 84*Power(t1,2) - 140*t2 + 
                  96*Power(t2,2) - 20*Power(t2,3) + 
                  t1*(236 + 60*t2 - 49*Power(t2,2))) + 
               Power(s2,2)*(82 + 96*Power(t1,3) + 200*t2 - 
                  318*Power(t2,2) + 92*Power(t2,3) + 
                  3*Power(t1,2)*(-130 + 6*t2 + 17*Power(t2,2)) + 
                  t1*(-248 + 446*t2 - 249*Power(t2,2) + 45*Power(t2,3))) \
- s2*(54*Power(t1,4) + Power(t1,3)*(-284 + 84*t2 + 19*Power(t2,2)) + 
                  Power(t1,2)*
                   (-298 + 472*t2 - 210*Power(t2,2) + 30*Power(t2,3)) + 
                  t1*(160 + 250*t2 - 403*Power(t2,2) + 130*Power(t2,3)) + 
                  2*(44 + 9*t2 + 18*Power(t2,2) - 86*Power(t2,3) + 
                     12*Power(t2,4))))))*
       T2(1 - s + s1 - t2,1 - s2 + t1 - t2))/
     ((s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       Power(s - s1 + t2,2)*(-1 + s2 - t1 + t2)*
       (-3 + 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2) + 2*s2 - 
         2*s*s2 + 2*s1*s2 + Power(s2,2) - 2*t1 + 2*s*t1 - 2*s1*t1 - 
         2*s2*t1 + Power(t1,2) + 4*t2)) - 
    (8*(-32*s2 + 36*Power(s2,2) - 4*Power(s2,3) + 32*t1 - 72*s2*t1 + 
         12*Power(s2,2)*t1 + 36*Power(t1,2) - 12*s2*Power(t1,2) + 
         4*Power(t1,3) + Power(s1,5)*
          (Power(s2,2) - 2*s2*(-2 + t1) + (-4 + t1)*t1) - 12*t2 + 
         122*s2*t2 - 81*Power(s2,2)*t2 - 2*Power(s2,3)*t2 - 134*t1*t2 + 
         172*s2*t1*t2 + 7*Power(s2,2)*t1*t2 + 2*Power(s2,3)*t1*t2 - 
         91*Power(t1,2)*t2 - 8*s2*Power(t1,2)*t2 - 
         6*Power(s2,2)*Power(t1,2)*t2 + 3*Power(t1,3)*t2 + 
         6*s2*Power(t1,3)*t2 - 2*Power(t1,4)*t2 + 68*Power(t2,2) - 
         182*s2*Power(t2,2) + 32*Power(s2,2)*Power(t2,2) + 
         10*Power(s2,3)*Power(t2,2) - 2*Power(s2,4)*Power(t2,2) + 
         214*t1*Power(t2,2) - 74*s2*t1*Power(t2,2) - 
         28*Power(s2,2)*t1*Power(t2,2) + 5*Power(s2,3)*t1*Power(t2,2) + 
         42*Power(t1,2)*Power(t2,2) + 26*s2*Power(t1,2)*Power(t2,2) - 
         3*Power(s2,2)*Power(t1,2)*Power(t2,2) - 
         8*Power(t1,3)*Power(t2,2) - s2*Power(t1,3)*Power(t2,2) + 
         Power(t1,4)*Power(t2,2) - 117*Power(t2,3) + 88*s2*Power(t2,3) + 
         21*Power(s2,2)*Power(t2,3) - 4*Power(s2,3)*Power(t2,3) + 
         Power(s2,4)*Power(t2,3) - 111*t1*Power(t2,3) - 
         42*s2*t1*Power(t2,3) + 7*Power(s2,2)*t1*Power(t2,3) - 
         3*Power(s2,3)*t1*Power(t2,3) + 21*Power(t1,2)*Power(t2,3) - 
         2*s2*Power(t1,2)*Power(t2,3) + 
         3*Power(s2,2)*Power(t1,2)*Power(t2,3) - 
         Power(t1,3)*Power(t2,3) - s2*Power(t1,3)*Power(t2,3) + 
         66*Power(t2,4) + 8*s2*Power(t2,4) - 2*Power(s2,2)*Power(t2,4) + 
         2*Power(s2,3)*Power(t2,4) - 6*t1*Power(t2,4) + 
         3*s2*t1*Power(t2,4) - 4*Power(s2,2)*t1*Power(t2,4) - 
         Power(t1,2)*Power(t2,4) + 2*s2*Power(t1,2)*Power(t2,4) - 
         5*Power(t2,5) + Power(s2,2)*Power(t2,5) + t1*Power(t2,5) - 
         s2*t1*Power(t2,5) - Power(s,4)*(t1 - t2)*
          (-2 - Power(s1,2) + Power(s2,2) - 2*s2*t1 + Power(t1,2) + 
            2*s2*t2 - 2*t1*t2 + Power(t2,2) - 2*s1*(-2 + s2 - t1 + t2)) + 
         Power(s1,4)*(2*Power(s2,3) + 2*Power(t1,3) + 4*(-1 + t2) - 
            Power(s2,2)*(-2 + 2*t1 + t2) + 4*t1*(3 + 2*t2) - 
            Power(t1,2)*(3 + 4*t2) + 
            s2*(t1 - 2*Power(t1,2) + 5*t1*t2 - 4*(4 + t2))) - 
         Power(s1,3)*(-15 + Power(s2,4) + Power(t1,4) + 6*t2 + 
            9*Power(t2,2) + 2*Power(t1,3)*(2 + t2) + 
            Power(s2,3)*(-6 - 4*t1 + 8*t2) - 
            2*s2*(-17 + 2*Power(t1,3) + Power(t1,2)*(7 - 2*t2) + 
               t1*(-37 + t2) + 40*t2 - 7*Power(t2,2)) + 
            t1*(-31 + 66*t2 - 3*Power(t2,2)) - 
            2*Power(t1,2)*(21 + t2 + 2*Power(t2,2)) + 
            2*Power(s2,2)*(3*Power(t1,2) + t1*(8 - 7*t2) + 
               2*(-8 + t2 + Power(t2,2)))) + 
         Power(s,3)*(10 - 12*t1 - 11*Power(t1,2) + 4*Power(t1,3) - 
            Power(t1,4) + Power(s1,3)*(2 - 3*s2 + t1) + 
            Power(s2,3)*(t1 - 2*t2) + 12*t2 + 32*t1*t2 - 
            10*Power(t1,2)*t2 + Power(t1,3)*t2 - 21*Power(t2,2) + 
            8*t1*Power(t2,2) + 3*Power(t1,2)*Power(t2,2) - 
            2*Power(t2,3) - 5*t1*Power(t2,3) + 2*Power(t2,4) - 
            Power(s2,2)*(9 + 3*Power(t1,2) + 2*t2 + 2*Power(t2,2) - 
               t1*(4 + 5*t2)) + 
            Power(s1,2)*(17 + 2*Power(s2,2) + 11*Power(t1,2) + 
               t1*(24 - 17*t2) - 26*t2 + 6*Power(t2,2) + 
               s2*(-16 - 13*t1 + 12*t2)) + 
            s2*(3*Power(t1,3) - 4*Power(t1,2)*(2 + t2) + 
               t1*(20 + 12*t2 - Power(t2,2)) + 
               2*t2*(-11 - 2*t2 + Power(t2,2))) + 
            s1*(-28 - Power(s2,3) + Power(t1,3) + 
               Power(s2,2)*(10 + 3*t1 - 2*t2) + 6*t2 + 30*Power(t2,2) - 
               8*Power(t2,3) - 2*Power(t1,2)*(-7 + 5*t2) + 
               s2*(16 - 3*Power(t1,2) + 12*t1*(-2 + t2) + 24*t2 - 
                  9*Power(t2,2)) + t1*(-4 - 44*t2 + 17*Power(t2,2)))) + 
         Power(s1,2)*(32 - 111*t2 + 78*Power(t2,2) + Power(t2,3) + 
            Power(t1,4)*(-5 + 2*t2) + Power(s2,4)*(-2 + 3*t2) - 
            18*Power(t1,2)*(-3 + 7*t2) - 
            2*Power(t1,3)*(-19 - 8*t2 + Power(t2,2)) + 
            t1*(58 - 135*t2 + 106*Power(t2,2) - 13*Power(t2,3)) + 
            Power(s2,2)*(36 - 92*t2 + 8*Power(t2,3) + 
               3*Power(t1,2)*(-7 + 5*t2) + 
               t1*(94 + 48*t2 - 26*Power(t2,2))) + 
            Power(s2,3)*(-11*t1*(-1 + t2) + 
               4*(-7 - 4*t2 + 3*Power(t2,2))) - 
            s2*(46 - 122*t2 + 116*Power(t2,2) - 24*Power(t2,3) + 
               Power(t1,3)*(-17 + 9*t2) - 
               8*Power(t1,2)*(-13 - 6*t2 + 2*Power(t2,2)) + 
               t1*(90 - 218*t2 + 8*Power(t2,3)))) + 
         s1*(12 - 100*t2 + 213*Power(t2,2) - 134*Power(t2,3) + 
            9*Power(t2,4) + Power(s2,4)*(4 + 8*t2 - 3*Power(t2,2)) + 
            Power(t1,4)*(6 + 8*t2 - Power(t2,2)) + 
            Power(t1,3)*(33 - 50*t2 - 15*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(71 - 132*t2 + 83*Power(t2,2) + 2*Power(t2,3) - 
               Power(t2,4)) + 
            t1*(54 - 208*t2 + 199*Power(t2,2) - 46*Power(t2,3) + 
               5*Power(t2,4)) + 
            2*Power(s2,3)*(-17 + 19*t2 + 9*Power(t2,2) - 
               4*Power(t2,3) + t1*(-9 - 16*t2 + 5*Power(t2,2))) + 
            Power(s2,2)*(61 - 104*t2 + 59*Power(t2,2) + 4*Power(t2,3) - 
               5*Power(t2,4) - 
               6*Power(t1,2)*(-5 - 8*t2 + 2*Power(t2,2)) + 
               t1*(101 - 126*t2 - 51*Power(t2,2) + 18*Power(t2,3))) + 
            2*s2*(-21 + 82*t2 - 80*Power(t2,2) + 22*Power(t2,3) - 
               5*Power(t2,4) + 
               Power(t1,3)*(-11 - 16*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-50 + 69*t2 + 24*Power(t2,2) - 
                  6*Power(t2,3)) + 
               t1*(-66 + 118*t2 - 71*Power(t2,2) - 3*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(s,2)*(-30 + 40*t1 + 49*Power(t1,2) - 11*Power(t1,3) + 
            3*Power(t1,4) - 10*t2 + Power(s2,4)*t2 - 140*t1*t2 - 
            4*Power(t1,2)*t2 - Power(t1,3)*t2 - 2*Power(t1,4)*t2 + 
            91*Power(t2,2) + 59*t1*Power(t2,2) - 
            9*Power(t1,2)*Power(t2,2) + 5*Power(t1,3)*Power(t2,2) - 
            44*Power(t2,3) + 9*t1*Power(t2,3) - 
            3*Power(t1,2)*Power(t2,3) - 2*Power(t2,4) - t1*Power(t2,4) + 
            Power(t2,5) + Power(s1,4)*(-2 + 4*s2 - 3*t1 + t2) - 
            Power(s2,3)*(-7 - 4*t2 + 2*Power(t2,2) + t1*(3 + t2)) + 
            Power(s1,3)*(-26 + 3*Power(s2,2) - 9*Power(t1,2) + 
               3*s2*(7 + 2*t1 - 4*t2) + 29*t2 - 6*Power(t2,2) + 
               6*t1*(-4 + 3*t2)) + 
            Power(s1,2)*(-9 - 2*Power(s2,3) + 9*Power(t1,3) + 
               Power(s2,2)*(12 + 13*t1 - 12*t2) + 80*t2 - 
               66*Power(t2,2) + 10*Power(t2,3) + Power(t1,2)*(3 + t2) + 
               t1*(-27 + 57*t2 - 20*Power(t2,2)) - 
               s2*(7 + 20*Power(t1,2) + t1*(15 - 11*t2) + 24*t2 - 
                  6*Power(t2,2))) + 
            Power(s2,2)*(37 - 3*Power(t1,2)*(-3 + t2) - 6*t2 + 
               6*Power(t2,2) - 6*Power(t2,3) + 
               t1*(-25 - 9*t2 + 9*Power(t2,2))) + 
            s2*(-18 + 92*t2 - 45*Power(t2,2) - 2*Power(t2,4) + 
               Power(t1,3)*(-9 + 5*t2) + 
               Power(t1,2)*(29 + 6*t2 - 12*Power(t2,2)) + 
               t1*(-86 + 10*t2 + 3*Power(t2,2) + 9*Power(t2,3))) + 
            s1*(72 + Power(s2,4) + 3*Power(t1,4) - 116*t2 - 
               8*Power(t2,2) + 41*Power(t2,3) - 6*Power(t2,4) - 
               4*Power(t1,3)*(-4 + 3*t2) + 
               Power(s2,3)*(-13 - 6*t1 + 6*t2) + 
               Power(t1,2)*(-28 - 15*t2 + 9*Power(t2,2)) + 
               t1*(26 + 42*t2 - 42*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,2)*(-34 + 12*Power(t1,2) - 21*t2 + 
                  15*Power(t2,2) - 6*t1*(-7 + 4*t2)) + 
               s2*(-16 - 10*Power(t1,3) - 12*t2 + 9*Power(t2,2) + 
                  4*Power(t2,3) + 15*Power(t1,2)*(-3 + 2*t2) + 
                  t1*(62 + 36*t2 - 24*Power(t2,2))))) + 
         s*(20 - 62*t1 - 79*Power(t1,2) + 3*Power(t1,3) - 2*Power(t1,4) + 
            Power(s1,5)*(-s2 + t1) + 2*t2 + 242*t1*t2 + 
            91*Power(t1,2)*t2 - 19*Power(t1,3)*t2 + 4*Power(t1,4)*t2 + 
            2*Power(s2,4)*(-1 + t2)*t2 - 133*Power(t2,2) - 
            239*t1*Power(t2,2) + 28*Power(t1,2)*Power(t2,2) - 
            6*Power(t1,3)*Power(t2,2) - Power(t1,4)*Power(t2,2) + 
            145*Power(t2,3) + 23*t1*Power(t2,3) + 
            3*Power(t1,3)*Power(t2,3) - 30*Power(t2,4) + 
            2*t1*Power(t2,4) - 3*Power(t1,2)*Power(t2,4) + 
            t1*Power(t2,5) - Power(s1,4)*
             (-13 + 6*Power(s2,2) + s2*(13 - 7*t1) + Power(t1,2) + 
               3*t1*(-4 + t2) + 9*t2) + 
            Power(s2,3)*(-2 + 17*t2 + 2*Power(t2,3) + 
               t1*(2 + 2*t2 - 5*Power(t2,2))) + 
            Power(s1,3)*(25 + Power(s2,3) - 11*Power(t1,3) - 72*t2 + 
               31*Power(t2,2) + 2*Power(t1,2)*(-1 + 8*t2) + 
               Power(s2,2)*(-12 - 13*t1 + 20*t2) + 
               t1*(13 - 25*t2 - 2*Power(t2,2)) + 
               s2*(11 + 23*Power(t1,2) + t1*(14 - 36*t2) + 9*t2 + 
                  10*Power(t2,2))) + 
            Power(s2,2)*(-69 + 69*t2 + 24*Power(t2,2) + 6*Power(t2,3) - 
               2*Power(t2,4) + 3*Power(t1,2)*(-2 + 2*t2 + Power(t2,2)) - 
               t1*(-7 + 53*t2 + 6*Power(t2,2) + Power(t2,3))) + 
            s2*(50 - 188*t2 + 180*Power(t2,2) - 15*Power(t2,3) + 
               4*Power(t2,4) - 2*Power(t2,5) + 
               Power(t1,3)*(6 - 10*t2 + Power(t2,2)) + 
               Power(t1,2)*(-8 + 55*t2 + 12*Power(t2,2) - 
                  4*Power(t2,3)) + 
               t1*(148 - 160*t2 - 52*Power(t2,2) - 6*Power(t2,3) + 
                  5*Power(t2,4))) + 
            Power(s1,2)*(-49 - Power(t1,4) + 
               Power(s2,3)*(-5 + t1 - 2*t2) + 13*t2 + 95*Power(t2,2) - 
               35*Power(t2,3) + Power(t1,3)*(-4 + 19*t2) + 
               Power(t1,2)*(-19 + 33*t2 - 28*Power(t2,2)) + 
               t1*(-25 - 21*t2 + 10*Power(t2,3)) + 
               Power(s2,2)*(-5 - 3*Power(t1,2) + 33*t2 - 
                  24*Power(t2,2) + t1*(6 + 23*t2)) + 
               s2*(30 + 3*Power(t1,3) + Power(t1,2)*(3 - 40*t2) - 
                  33*t2 + 33*Power(t2,2) - 18*Power(t2,3) + 
                  t1*(24 - 66*t2 + 52*Power(t2,2)))) - 
            s1*(46 + 2*Power(s2,4)*(-3 + t2) - 210*t2 + 187*Power(t2,2) + 
               6*Power(t2,3) - 13*Power(t2,4) - 2*Power(t1,4)*(3 + t2) + 
               Power(s2,3)*(-25 - 4*t1*(-6 + t2) + 3*t2 + Power(t2,2)) + 
               Power(t1,3)*(31 - 9*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(31 - 34*t2 + 39*Power(t2,2) - 
                  16*Power(t2,3)) + 
               t1*(62 - 122*t2 - 9*Power(t2,2) - 11*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(s2,2)*(1 - 36*Power(t1,2) - 10*t2 + 
                  39*Power(t2,2) - 12*Power(t2,3) + 
                  3*t1*(27 - 5*t2 + 3*Power(t2,2))) + 
               s2*(-28 + 72*t2 - 9*Power(t2,2) + 33*Power(t2,3) - 
                  11*Power(t2,4) + 4*Power(t1,3)*(6 + t2) - 
                  3*Power(t1,2)*(29 - 7*t2 + 7*Power(t2,2)) + 
                  t1*(-32 + 44*t2 - 78*Power(t2,2) + 28*Power(t2,3))))))*
       T3(1 - s + s1 - t2,1 - s + s2 - t1))/
     ((s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       Power(s - s1 + t2,2)*(-1 + s2 - t1 + t2)*(-s1 + s2 - t1 + t2)) - 
    (8*(-32 + 36*s2 - 4*Power(s2,2) - 
         Power(s1,3)*Power(s2 - t1,2)*(4 + s2 - t1) - 36*t1 + 8*s2*t1 - 
         4*Power(t1,2) + Power(s,5)*(t1 - t2) + 72*t2 - 45*s2*t2 + 
         2*Power(s2,2)*t2 + 45*t1*t2 - 7*s2*t1*t2 + 2*Power(s2,2)*t1*t2 + 
         5*Power(t1,2)*t2 - 4*s2*Power(t1,2)*t2 + 2*Power(t1,3)*t2 - 
         48*Power(t2,2) + 10*s2*Power(t2,2) + 6*Power(s2,2)*Power(t2,2) - 
         2*Power(s2,3)*Power(t2,2) - 10*t1*Power(t2,2) - 
         10*s2*t1*Power(t2,2) + 3*Power(s2,2)*t1*Power(t2,2) + 
         4*Power(t1,2)*Power(t2,2) - Power(t1,3)*Power(t2,2) + 
         8*Power(t2,3) - s2*Power(t2,3) + Power(s2,3)*Power(t2,3) + 
         t1*Power(t2,3) + s2*t1*Power(t2,3) - 
         2*Power(s2,2)*t1*Power(t2,3) - Power(t1,2)*Power(t2,3) + 
         s2*Power(t1,2)*Power(t2,3) + 
         Power(s,4)*(1 - 2*(2 + s2)*t1 + 2*Power(t1,2) + 2*t2 + 
            3*s2*t2 - 2*Power(t2,2) + s1*(-2 + s2 - 3*t1 + 4*t2)) + 
         Power(s1,2)*(s2 - t1)*
          (4*(-1 + t2) - 4*t1*(3 + 2*t2) + Power(t1,2)*(3 + 2*t2) + 
            Power(s2,2)*(2 + 3*t2) + s2*(8 + 12*t2 - 5*t1*(1 + t2))) + 
         Power(s,3)*(-1 + 4*t1 - 7*Power(t1,2) + Power(t1,3) + 
            Power(s2,2)*(t1 - 3*t2) + Power(s1,2)*(2 + t1 - 3*t2) + 
            6*t2 - 3*t1*t2 + 3*Power(t1,2)*t2 + 2*Power(t2,2) - 
            3*t1*Power(t2,2) - Power(t2,3) + 
            s2*(-4 + 7*t1 - 2*Power(t1,2) - 6*t2 + 6*Power(t2,2)) + 
            s1*(2 - 2*Power(s2,2) - 6*Power(t1,2) + 
               s2*(3 + 8*t1 - 10*t2) - 11*t2 + 4*Power(t2,2) + 
               6*t1*(1 + t2))) + 
         s1*(8*Power(-1 + t2,2) + t1*(-25 + 42*t2 - 17*Power(t2,2)) + 
            Power(s2,3)*(4 + 4*t2 - 3*Power(t2,2)) + 
            Power(t1,3)*(-6 + Power(t2,2)) + 
            Power(t1,2)*(-41 + 4*t2 + Power(t2,2)) + 
            s2*(25 - 42*t2 + 17*Power(t2,2) + 
               Power(t1,2)*(16 + 4*t2 - 5*Power(t2,2)) + 
               t1*(79 - 10*t2 + 3*Power(t2,2))) + 
            Power(s2,2)*(-38 + 6*t2 - 4*Power(t2,2) + 
               t1*(-14 - 8*t2 + 7*Power(t2,2)))) - 
         Power(s,2)*(-1 + Power(s1,3)*(s2 - t1) - 4*t1 - 5*Power(t1,2) + 
            3*Power(t1,3) + 3*t2 - Power(s2,3)*t2 - 13*t1*t2 + 
            9*Power(t1,2)*t2 - 2*Power(t1,3)*t2 - 6*Power(t2,2) + 
            2*t1*Power(t2,3) + 
            3*Power(s2,2)*(-1 + t1 - 2*t2 + 2*Power(t2,2)) + 
            Power(s1,2)*(7 - 2*Power(t1,2) + s2*(3 + 2*t1 - 9*t2) - 
               11*t2 + t1*(2 + 8*t2)) + 
            s2*(1 + 3*Power(t1,2)*(-2 + t2) + 15*t2 + 6*Power(t2,2) - 
               3*Power(t2,3) + t1*(8 - 3*t2 - 6*Power(t2,2))) + 
            s1*(-5 - Power(s2,3) - 10*Power(t1,2) + 3*Power(t1,3) + 
               Power(s2,2)*(3 + 5*t1 - 8*t2) + 6*t2 + 7*Power(t2,2) + 
               t1*(9 + 6*t2 - 9*Power(t2,2)) + 
               s2*(-5 - 7*Power(t1,2) - 20*t2 + 11*Power(t2,2) + 
                  t1*(7 + 8*t2)))) + 
         s*(36 + 37*t1 + 5*Power(t1,2) + 2*Power(t1,3) + 
            2*Power(s1,3)*(Power(s2,2) - 2*s2*(-1 + t1) + (-2 + t1)*t1) - 
            43*t2 - 6*t1*t2 + 9*Power(t1,2)*t2 - 4*Power(t1,3)*t2 + 
            2*Power(s2,3)*(-1 + t2)*t2 + 6*Power(t2,2) + 
            10*t1*Power(t2,2) - 3*Power(t1,2)*Power(t2,2) + 
            Power(t1,3)*Power(t2,2) + Power(t2,3) - t1*Power(t2,3) - 
            Power(t1,2)*Power(t2,3) + 
            Power(s2,2)*(2 + 9*t2 + 6*Power(t2,2) - 3*Power(t2,3) + 
               t1*(2 - 3*Power(t2,2))) + 
            s2*(-37 + 9*t2 - 12*Power(t2,2) + Power(t1,2)*(-4 + 6*t2) + 
               t1*(-7 - 18*t2 - 3*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(Power(t1,3) + Power(s2,2)*(-1 + t1 - 9*t2) - 
               12*(-1 + t2) - 7*Power(t1,2)*(1 + t2) + t1*(13 + 19*t2) + 
               s2*(-9 - 2*Power(t1,2) - 23*t2 + 8*t1*(1 + 2*t2))) + 
            s1*(-49 - 2*Power(s2,3)*(-1 + t2) - 2*Power(t1,3)*(-1 + t2) + 
               58*t2 - 9*Power(t2,2) - 2*t1*(22 + 9*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-9 + 5*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(-3 + 2*t1*(-1 + t2) - 13*t2 + 
                  10*Power(t2,2)) + 
               s2*(41 + 2*Power(t1,2)*(-1 + t2) + 16*t2 + 
                  11*Power(t2,2) + t1*(12 + 8*t2 - 16*Power(t2,2))))))*
       T4(1 - s + s2 - t1))/
     ((s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*
       Power(s - s1 + t2,2)*(-1 + s2 - t1 + t2)) - 
    (8*(20 - 32*s2 - 4*Power(s2,2) + 32*t1 + 8*s2*t1 - 4*Power(t1,2) - 
         Power(s,4)*(t1 - t2)*(1 - s2 + t1 - t2) - 57*t2 + 35*s2*t2 + 
         10*Power(s2,2)*t2 - 34*t1*t2 - 15*s2*t1*t2 - 
         2*Power(s2,2)*t1*t2 + 5*Power(t1,2)*t2 + 4*s2*Power(t1,2)*t2 - 
         2*Power(t1,3)*t2 + 51*Power(t2,2) + 6*s2*Power(t2,2) - 
         12*Power(s2,2)*Power(t2,2) + 2*Power(s2,3)*Power(t2,2) - 
         9*t1*Power(t2,2) + 17*s2*t1*Power(t2,2) - 
         3*Power(s2,2)*t1*Power(t2,2) - 5*Power(t1,2)*Power(t2,2) + 
         Power(t1,3)*Power(t2,2) - 11*Power(t2,3) - 9*s2*Power(t2,3) + 
         3*Power(s2,2)*Power(t2,3) - Power(s2,3)*Power(t2,3) + 
         12*t1*Power(t2,3) - 3*s2*t1*Power(t2,3) + 
         2*Power(s2,2)*t1*Power(t2,3) - s2*Power(t1,2)*Power(t2,3) - 
         3*Power(t2,4) - Power(s2,2)*Power(t2,4) - t1*Power(t2,4) + 
         s2*t1*Power(t2,4) + Power(s1,3)*(s2 - t1)*
          (Power(s2,2) + Power(t1,2) + 4*(-1 + t2) - t1*(7 + t2) + 
            s2*(7 - 2*t1 + t2)) - 
         Power(s,3)*(13 + 8*Power(s1,2) - 3*t1 - 3*Power(t1,2) + 
            Power(t1,3) + Power(s2,2)*(t1 - 2*t2) + t2 + 7*t1*t2 - 
            4*Power(t2,2) - 3*t1*Power(t2,2) + 2*Power(t2,3) + 
            s2*(-5 + 3*t1 - 2*Power(t1,2) + 2*t1*t2) - 
            s1*(22 + Power(s2,2) + 5*t1 + 3*Power(t1,2) - 6*t2 - 
               7*t1*t2 + 4*Power(t2,2) + s2*(-7 - 4*t1 + 5*t2))) + 
         Power(s,2)*(13 + 8*Power(s1,3) + 2*t1 - 4*Power(t1,2) + 
            3*Power(t1,3) - 43*t2 - Power(s2,3)*t2 + 10*t1*t2 - 
            2*Power(t1,3)*t2 - 6*t1*Power(t2,2) + 
            3*Power(t1,2)*Power(t2,2) + 3*Power(t2,3) - Power(t2,4) + 
            Power(s2,2)*(-7 + 3*t1 - 3*t2 + 3*Power(t2,2)) + 
            Power(s1,2)*(-46 - Power(t1,2) + s2*(26 + t1 - 3*t2) + 
               5*t2 - 3*Power(t2,2) + t1*(-23 + 4*t2)) + 
            s2*(-10 + 3*Power(t1,2)*(-2 + t2) + 11*t2 - 6*Power(t2,2) + 
               3*Power(t2,3) + t1*(11 + 3*t2 - 6*Power(t2,2))) - 
            s1*(-26 + Power(s2,3) - 3*Power(t1,3) - 45*t2 + 
               11*Power(t2,2) - 4*Power(t2,3) + Power(t1,2)*(1 + 5*t2) + 
               Power(s2,2)*(-10 - 5*t1 + 7*t2) + 
               t1*(-10 - 9*t2 + 2*Power(t2,2)) + 
               s2*(19 + 7*Power(t1,2) + t1*(9 - 12*t2) + 4*t2 + 
                  2*Power(t2,2)))) - 
         Power(s1,2)*(Power(t1,3)*(1 - 2*t2) - 4*Power(-1 + t2,2) + 
            Power(s2,3)*(-2 + 3*t2) - 4*t1*(5 - 6*t2 + Power(t2,2)) + 
            Power(t1,2)*(13 + 5*t2 + 2*Power(t2,2)) + 
            Power(s2,2)*(6 + t1*(5 - 8*t2) + 11*t2 + 3*Power(t2,2)) + 
            s2*(Power(t1,2)*(-4 + 7*t2) + 8*(3 - 4*t2 + Power(t2,2)) - 
               t1*(19 + 16*t2 + 5*Power(t2,2)))) + 
         s1*(-(Power(-1 + t2,2)*(-17 + 5*t2)) - 
            Power(t1,3)*(2 - 4*t2 + Power(t2,2)) + 
            Power(s2,3)*(4 - 8*t2 + 3*Power(t2,2)) + 
            Power(t1,2)*(35 + 6*t2 - 10*Power(t2,2) + Power(t2,3)) + 
            t1*(14 - 31*t2 + 12*Power(t2,2) + 5*Power(t2,3)) + 
            Power(s2,2)*(30 + 6*t2 - 7*Power(t2,2) + 3*Power(t2,3) + 
               t1*(-10 + 20*t2 - 7*Power(t2,2))) + 
            s2*(-15 + 38*t2 - 23*Power(t2,2) + 
               Power(t1,2)*(8 - 16*t2 + 5*Power(t2,2)) - 
               t1*(65 + 12*t2 - 17*Power(t2,2) + 4*Power(t2,3)))) - 
         s*(25 + 42*t1 - 5*Power(t1,2) + 2*Power(t1,3) - 68*t2 + 7*t1*t2 + 
            9*Power(t1,2)*t2 - 4*Power(t1,3)*t2 + 
            2*Power(s2,3)*(-1 + t2)*t2 + 41*Power(t2,2) - 
            19*t1*Power(t2,2) + 3*Power(t1,2)*Power(t2,2) + 
            Power(t1,3)*Power(t2,2) + 2*Power(t2,3) + t1*Power(t2,3) - 
            2*Power(t1,2)*Power(t2,3) + t1*Power(t2,4) + 
            Power(s1,3)*(Power(s2,2) + Power(t1,2) + 8*(-1 + t2) - 
               t1*(19 + t2) + s2*(19 - 2*t1 + t2)) + 
            Power(s2,2)*(-10 + 19*t2 + t1*(2 - 3*Power(t2,2))) + 
            Power(s1,2)*(Power(t1,3) + Power(t1,2)*(5 - 6*t2) + 
               Power(s2,2)*(13 + t1 - 6*t2) - 15*Power(-1 + t2,2) - 
               2*s2*(25 + Power(t1,2) + t1*(9 - 6*t2) + 10*t2 + 
                  3*Power(t2,2)) + t1*(49 + 22*t2 + 5*Power(t2,2))) + 
            s2*(-43 + 4*t2 + 3*Power(t2,2) + 6*Power(t2,3) - 
               2*Power(t2,4) + Power(t1,2)*(-4 + 6*t2) + 
               t1*(15 - 28*t2 - 3*Power(t2,2) + 2*Power(t2,3))) + 
            s1*(25 - 2*Power(s2,3)*(-3 + t2) - 19*t2 - 9*Power(t2,2) + 
               3*Power(t2,3) - 2*Power(t1,3)*(1 + t2) + 
               Power(s2,2)*(11 + 2*t1*(-7 + t2) - 9*t2 + 5*Power(t2,2)) + 
               Power(t1,2)*(7 + 5*t2 + 7*Power(t2,2)) - 
               t1*(38 + 42*t2 + 3*Power(t2,2) + 5*Power(t2,3)) + 
               s2*(26 + 64*t2 - 9*Power(t2,2) + 7*Power(t2,3) + 
                  2*Power(t1,2)*(5 + t2) - 2*t1*(9 - 2*t2 + 6*Power(t2,2)))\
)))*T5(1 - s2 + t1 - t2))/
     ((s - s2 + t1)*(1 - s + s*s1 - s1*s2 + s1*t1 - t2)*Power(s - s1 + t2,2)*
       (-1 + s2 - t1 + t2)));
   return a;
};
