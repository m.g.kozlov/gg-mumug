#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libconfig.h> 
#include <unistd.h>
#include <getopt.h>



struct my_str
{
    int N;
    double E;
    double omega;
    char file[30];
    char conf[30];
};


struct parameters
{
    int N;
    int nlo1;
    int nlo2;
    int nlo3;
    double E;
    double omega;
    double th_beam;
    double th_max;
    double th_gmu;
    double si_min;
    double om_s; //omega*
    char file[100];
};

void help(char *name)
{
    printf("usage: %s\n \
     \t-h ( --help ) this message\n \
     \t-c [config file]  ( --config=config_file ), default value 1.cfg\n \
     \t-o [results file] ( --results=name), default value num.txt \n \
     \t-E [energy in MeV]  energy of inital photon ( --energy=value ), default E=500 MeV\n \
     \t-N [number of MC points]  ( --N=value ), default N=50000\n \
     \t-w [minimum energy of final gamma in MeV] ( --omegamin=value), default omega=30 MeV \n \
     \t  \n \
     \t  \n", name);
    return;
};


int get_params (int ac, char *av[],struct my_str *str)
{
        
int c;
int optIdx;

static struct option long_opt[] = {
                    {"help",  0, 0, 'h'},
                    {"config", 1, 0, 'c'},
                    {"results", 1, 0, 'o'},
                    {"energy",1,0,'E'},
                    {"N",1,0,'N'},
                    {"omegamin",1,0,'w'},
                    {0,0,0,0} 
                   };
  
while (1){
        

  if((c = getopt_long_only(ac, av, "h :w:c:o:E:N:", long_opt, &optIdx)) == -1)
   break;
 
  switch( c ){
     case 'h':
          help(av[0]);
          return(-1);
     
     case 'c':
          strcpy(str->conf,optarg);
          break;
          
     case 'o':
          strcpy(str->file,optarg);  
          break;
          
     case 'E':
          str->E = atof(optarg);
          break;
          
     case 'N':
          str->N = atoi(optarg);
          break;
     
     case 'w':
          str->omega=atof(optarg);
          break;
          
        };
    };
    
    return(0);
    
};





int read_my_config(char *config_file_name,struct parameters *x)
{
    config_t cfg;               /*Returns all parameters in this structure */
    //config_setting_t *setting;
    //const char *str1;
    double dtmp;    
    int itmp;
    const char *ftmp;

    /*Initialization */
    config_init(&cfg);

    /* Read the file. If there is an error, report it and exit. */
    if (!config_read_file(&cfg, config_file_name))
    {
        //printf("\n%s:%d - %s", config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
        config_destroy(&cfg);
        return -1;
    };
    
    
    /* Get the configuration file name. */
    if (config_lookup_float(&cfg, "E", &dtmp)) x->E=dtmp;
    if (config_lookup_int(&cfg, "N", &itmp)) x->N=itmp;
    
    if (config_lookup_float(&cfg, "omega", &dtmp)) x->omega=dtmp;
    
    if (config_lookup_int(&cfg, "nlo1", &itmp)) x->nlo1=itmp;
    if (config_lookup_int(&cfg, "nlo2", &itmp)) x->nlo2=itmp;
    if (config_lookup_int(&cfg, "nlo3", &itmp)) x->nlo3=itmp;
    
    if (config_lookup_float(&cfg, "theta_beam", &dtmp)) x->th_beam=dtmp;
    if (config_lookup_float(&cfg, "theta_max", &dtmp)) x->th_max=dtmp;
    if (config_lookup_float(&cfg, "theta_gmu", &dtmp)) x->th_gmu=dtmp;
    if (config_lookup_float(&cfg, "omega_s", &dtmp)) x->om_s=dtmp;
    if (config_lookup_float(&cfg, "si_min", &dtmp)) x->si_min=dtmp;
    
    //int config_lookup_string (const config_t * config, const char * path, const char ** value)
    
    if (config_lookup_string(&cfg, "file_result", &ftmp)) strcpy(x->file,ftmp);
  
   return 1; 
    
};