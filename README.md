






To build an executable file that calculates the scattering cross section, 
go to the directory "cross_section" and run make:

cd cross_section \
make

To assemble a program for calculating a differential cross section, 
you first need to assemble a program for calculating 
the scattering cross section. After you need to run the following commands:

cd cross_section \
cp ir.o nlo.o born.o libbox.a  libbubl.a  liblbl.a  libnlofunc.a  libpent.a  libtri.a ../differential_cross_section \
cd ../differential_cross_section \
make

