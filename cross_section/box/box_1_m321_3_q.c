#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_1_m321_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((16*(8*Power(s,3)*s2*t1*(s2 + Power(s2,2) - 3*s2*t1 + 
            t1*(-1 + 2*t1)) - s2*Power(s2 - t1,2)*(-1 + t1)*t1*
          (11 + 2*Power(s2,3) - s1*(-1 + s2)*(-2 + 9*s2 - 7*t1) - 9*t1 - 
            18*Power(t1,2) + 5*t2 + 6*t1*t2 + 
            Power(s2,2)*(6 - 10*t1 + 11*t2) + 
            s2*(1 + 11*t1 + 6*Power(t1,2) - 16*t2 - 6*t1*t2)) + 
         s*(s2 - t1)*(Power(-1 + t1,2)*t1 + 
            4*Power(s2,4)*t1*(-2 + s1 + 2*t1 - t2) - 
            s2*(47*Power(t1,4) + Power(t1,3)*(-38 + 15*s1 - 11*t2) - 
               2*t1*(-1 + t2) + t2 + Power(t1,2)*(-11 - 7*s1 + 4*t2)) + 
            Power(s2,2)*(2 + 11*Power(t1,4) + 
               Power(t1,2)*(-27 + 12*s1 - 20*t2) + 
               Power(t1,3)*(36 + 15*s1 - 11*t2) + t1*(-22 - 7*s1 + 11*t2)\
) + Power(s2,3)*(2 - 19*Power(t1,3) + t1*(7 + 3*s1 - 9*t2) + t2 + 
               Power(t1,2)*(10 - 19*s1 + 24*t2))) - 
         Power(s,2)*t1*((-1 + t1)*t1 + 
            Power(s2,4)*(2 + 4*s1 + 6*t1 - 4*t2) - 
            Power(s2,3)*(-11 + 11*Power(t1,2) + t1*(32 + 12*s1 - 15*t2) + 
               3*t2) + Power(s2,2)*
             (16 + 5*Power(t1,3) + s1*(-4 + 8*t1 + 8*Power(t1,2)) + 
               Power(t1,2)*(69 - 5*t2) + t2 - 2*t1*(25 + 4*t2)) + 
            s2*(-5 - 45*Power(t1,3) + t1*(-11 + 4*s1 - 7*t2) + 6*t2 + 
               Power(t1,2)*(-8*s1 + 5*(9 + t2))))))/
     (s*Power(-1 + s2,2)*s2*Power(s2 - t1,2)*(-s + s2 - t1)*(-1 + t1)*t1) + 
    (8*(2 - 4*Power(s2,4)*(-1 + t1) + 39*t1 - 6*s1*t1 + 
         8*Power(s,3)*(-2 + t1)*t1 - 2*Power(t1,2) + 9*s1*Power(t1,2) - 
         27*Power(t1,3) - 5*s1*Power(t1,3) - 12*Power(t1,4) - 2*t2 - 
         9*t1*t2 + 9*Power(t1,2)*t2 + 4*Power(t1,3)*t2 + 
         s2*(-41 + 4*Power(t1,4) + 
            s1*(6 - 10*t1 + 7*Power(t1,2) + 3*Power(t1,3)) + 
            Power(t1,2)*(37 - 24*t2) + Power(t1,3)*(33 - 4*t2) + 
            11*t1*(-3 + t2) + 11*t2) + 
         Power(s2,3)*(-20 + s1*(-7 + 11*t1 - 2*Power(t1,2)) + 7*t2 - 
            11*t1*t2 + 2*Power(t1,2)*(10 + t2)) + 
         Power(s2,2)*(31 - 22*Power(t1,3) + 
            s1*(1 + 5*t1 - 14*Power(t1,2) + 2*Power(t1,3)) - 16*t2 + 
            t1*(14 + 9*t2) + Power(t1,2)*(-23 + 13*t2)) - 
         2*Power(s,2)*(-5*Power(t1,3) + t1*(-31 + 4*s1 - t2) + t2 + 
            Power(t1,2)*(23 - 2*s1 + t2) + 
            s2*(8 + Power(t1,3) + Power(t1,2)*(1 + 2*s1 - t2) - t2 + 
               t1*(-21 - 4*s1 + t2))) + 
         s*(-2 + 4*Power(s2,3)*t1 + (-37 + 2*s1)*Power(t1,3) + 4*t2 + 
            7*t1*(-12 + 2*s1 + t2) + Power(t1,2)*(33 - 18*s1 + 9*t2) + 
            Power(s2,2)*(16 + 2*Power(t1,3) + 
               2*s1*(5 - 9*t1 + 3*Power(t1,2)) - 5*t2 - 
               Power(t1,2)*(21 + 4*t2) + t1*(-31 + 13*t2)) + 
            s2*(50 + 11*Power(t1,3) - 
               2*s1*(5 - 2*t1 - 6*Power(t1,2) + Power(t1,3)) + 
               Power(t1,2)*(52 - 5*t2) + t2 - t1*(57 + 20*t2))))*
       B1(s,t1,s2))/
     (Power(-1 + s2,2)*(-s + s2 - t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) - 
    (16*(-(Power(-1 + s,2)*Power(t1,2)*(-1 + s + t1)) + 
         Power(s2,7)*(-3 + s + 5*s1 - 3*t1 - 5*t2) + 
         s2*t1*(-3 - 4*Power(s,4)*t1 + (-6 + s1)*t1 + 
            (13 - 2*s1)*Power(t1,2) + 2*Power(t1,3) + 
            s*(10 - 4*(-5 + s1)*t1 + (-22 + 4*s1)*Power(t1,2) - 
               2*Power(t1,3) - 3*t2) + t2 - 
            Power(s,3)*(-4 + 2*(-10 + s1)*t1 + 4*Power(t1,2) + t2) + 
            Power(s,2)*(-11 + 5*(-6 + s1)*t1 + (17 - 2*s1)*Power(t1,2) + 
               3*t2)) + Power(s2,6)*
          (4 - 2*Power(s,2) + 18*t1 + 6*Power(t1,2) - 4*s1*(2 + 3*t1) + 
            15*t2 + t1*t2 + s*(21 - 8*s1 + 5*t1 + 15*t2)) + 
         Power(s2,3)*(-11 + 4*Power(s,4) + s1 - 37*t1 + 6*s1*t1 + 
            38*Power(t1,2) - 6*s1*Power(t1,2) + 26*Power(t1,3) - 
            2*s1*Power(t1,3) - 2*Power(t1,4) + 
            Power(s,3)*(-13 + 2*s1 + t1*(-8 + t2)) + 3*t2 + 2*t1*t2 + 
            12*Power(t1,2)*t2 + 
            Power(s,2)*(1 - 3*Power(t1,3) + 
               s1*(-3 + 2*t1 - 7*Power(t1,2)) + t1*(14 - 3*t2) + 
               11*t2 + 4*Power(t1,2)*t2) + 
            s*(-1 + 8*s1*Power(t1,2) + 10*Power(t1,3) + 10*t2 + 
               4*t1*(3 + t2))) + 
         Power(s2,5)*(-14 + Power(s,3) - 9*t1 - 20*Power(t1,2) - 
            3*Power(t1,3) + s1*(6 + 14*t1 + 9*Power(t1,2)) + 
            Power(s,2)*(-19 + 5*s1 - t1 - 15*t2) - 14*t2 - 3*t1*t2 + 
            4*Power(t1,2)*t2 - 
            s*(38 + 12*Power(t1,2) - 12*s1*(1 + t1) + 10*t2 + 
               t1*(42 + t2))) + 
         Power(s2,2)*(2 + 6*(-7 + s1)*Power(t1,3) + 
            Power(s,3)*(3 - 7*t1 + (5 + 2*s1)*Power(t1,2) - 5*t2) + 
            Power(t1,2)*(2 - 4*t2) + t1*(18 - 2*s1 - 3*t2) - t2 - 
            s*(1 + 4*(-3 + s1)*Power(t1,3) + 2*Power(t1,4) + 
               Power(t1,2)*(12 - 8*t2) + t1*(33 - 4*s1 - 2*t2) + 3*t2) + 
            Power(s,2)*(-4 + (3 + 2*s1)*Power(t1,3) + 
               Power(t1,2)*(-11 + 2*s1 - 4*t2) + 9*t2 + 
               t1*(18 - 2*s1 + t2))) - 
         Power(s2,4)*(-22 + 4*s1 - 16*t1 + 6*s1*t1 + 21*Power(t1,2) + 
            4*s1*Power(t1,2) - 7*Power(t1,3) + 2*s1*Power(t1,3) + 
            Power(s,3)*(-1 + 2*s1 + t1 - 5*t2) - 2*t2 - 2*t1*t2 + 
            12*Power(t1,2)*t2 + 
            Power(s,2)*(8 + 2*s1 - 6*Power(t1,2) + t1*(-28 + t2) + 
               5*t2) + s*(-22 - 6*Power(t1,3) + 
               4*s1*(1 + 4*t1 + Power(t1,2)) + 2*t1*(-20 + t2) + 12*t2 + 
               Power(t1,2)*(-7 + 8*t2))))*R1q(s2))/
     (Power(-1 + s2,2)*s2*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*Power(s2 - t1,3)*(-s + s2 - t1)) + 
    (16*(2*Power(s2,5)*t1*(-2 + t1 + 2*s1*t1 + Power(t1,2) - t2 - 
            t1*t2) + Power(s2,3)*
          (-2 + 12*Power(t1,5) + 
            Power(t1,2)*(-64 + 8*Power(s,2) + 15*s1 + 
               4*s*(-4 + s1 - 3*t2) - 39*t2) + 
            Power(t1,4)*(18 + 8*s + 21*s1 - 13*t2) + 
            Power(t1,3)*(22 + 19*s1 + 4*s*(10 + 3*s1 - t2) - 5*t2) + 
            t1*(14 + 8*s + s1 + t2)) - 
         Power(s2,2)*(7*Power(t1,2) - 92*Power(t1,3) + 
            24*Power(s,2)*Power(t1,3) + 72*Power(t1,4) + 8*Power(t1,5) + 
            5*Power(t1,6) + s1*t1*
             (-1 + 9*t1 + 21*Power(t1,2) + 19*Power(t1,3) + 
               16*Power(t1,4)) - t2 + 3*t1*t2 - 17*Power(t1,2)*t2 - 
            33*Power(t1,3)*t2 - 6*Power(t1,4)*t2 - 10*Power(t1,5)*t2 + 
            s*t1*(-4 + 9*Power(t1,4) + t1*(21 + 4*s1 - 13*t2) + 
               Power(t1,3)*(55 + 16*s1 - 11*t2) + 
               Power(t1,2)*(-9 + 4*s1 - t2) + t2)) - 
         Power(s2,4)*(2 + 9*Power(t1,4) + 
            Power(t1,2)*(-7 + 8*s1 + 2*s*(5 + 2*s1 - t2) - 13*t2) + 
            Power(t1,3)*(9 + 2*s + 14*s1 - 5*t2) + t2 - 
            t1*(13 - 2*s1 + 7*t2 + 2*s*(2 + t2))) + 
         s2*t1*(1 + (-3 + 3*s + 5*s1)*Power(t1,5) + 
            Power(t1,4)*(43 + 13*s1 + s*(38 + 8*s1 - 3*t2) - 12*t2) - 
            (3 + s)*t2 + Power(t1,3)*
             (32*Power(s,2) + 5*s1 + s*(-20 + 8*s1 - 6*t2) - 3*(6 + t2)) \
+ t1*(3 + 8*Power(s,2) - 2*s1 + 9*t2 + s*(-23 + 4*s1 + 2*t2)) - 
            Power(t1,2)*(26 + 16*Power(s,2) - 15*s1 + 27*t2 + 
               s*(-58 + 4*s1 + 8*t2))) + 
         Power(t1,2)*(-1 + (4 - 5*s1)*Power(t1,4) - 
            8*Power(s,2)*t1*(1 - 2*t1 + 2*Power(t1,2)) + 
            t1*(-1 + s1 - 6*t2) + 2*t2 + 
            Power(t1,3)*(-23 + 3*s1 + 2*t2) + 
            Power(t1,2)*(21 - 7*s1 + 10*t2) + 
            s*(6 - 19*Power(t1,4) - 5*t2 + 
               Power(t1,3)*(35 - 8*s1 + 3*t2) + t1*(7 - 4*s1 + 11*t2) + 
               Power(t1,2)*(8*s1 - 5*(9 + t2)))))*R1q(t1))/
     (Power(-1 + s2,2)*Power(s2 - t1,3)*(-s + s2 - t1)*Power(-1 + t1,2)*t1) \
+ (16*(12*Power(s,4) + Power(s,3)*
          (-24 - 6*s1*(-1 + s2) + 2*Power(s2,2) + 21*t1 - 5*t2 + 
            s2*(-22 - 5*t1 + 5*t2)) + 
         s*(8 + 4*Power(s2,4) + 7*t1 - 12*Power(t1,2) - 
            2*s1*(-1 + s2)*(1 + 5*Power(s2,2) - 3*t1 - 3*s2*t1) + t2 + 
            4*t1*t2 + s2*(20 - 55*t1 - 8*Power(t1,2) + 5*t2) + 
            Power(s2,3)*(-13*t1 + 15*t2) + 
            Power(s2,2)*(16 + 29*t1 + 4*Power(t1,2) - 21*t2 - 4*t1*t2)) - 
         Power(-1 + s2,2)*(5 + Power(s2,3) - 
            s1*(-1 + s2)*(-1 + 4*s2 - 3*t1) - 7*t1 - 6*Power(t1,2) + 
            3*t2 + 2*t1*t2 + Power(s2,2)*(2 - 4*t1 + 5*t2) + 
            s2*(4 + 3*t1 + 2*Power(t1,2) - 8*t2 - 2*t1*t2)) + 
         Power(s,2)*(13 - 5*Power(s2,3) + 
            3*s1*(-1 + s2)*(3 + 4*s2 - t1) - 31*t1 + 6*Power(t1,2) + 
            Power(s2,2)*(14 + 14*t1 - 15*t2) + 7*t2 - 2*t1*t2 + 
            s2*(-42 - 31*t1 - 2*Power(t1,2) + 8*t2 + 2*t1*t2)))*R2q(s))/
     (s*Power(-1 + s2,2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*(-s + s2 - t1)) - 
    (8*(8*Power(s,6)*t1 + Power(-1 + s2,4)*(-2 + t1 + s2*(-1 + 2*t1))*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) - 
         2*Power(s,5)*(-5*Power(t1,2) - t2 + t1*(21 - 2*s1 + t2) + 
            s2*(-4 + Power(t1,2) + t1*(11 + 2*s1 - t2) + t2)) + 
         Power(s,4)*(2 + (-53 + 2*s1)*Power(t1,2) - 10*t2 + 
            t1*(66 - 14*s1 + 13*t2) + 
            Power(s2,2)*(-12 + 15*t1 + 8*Power(t1,2) + 
               2*s1*(-3 + 7*t1) + 9*t2 - 10*t1*t2) + 
            s2*(-38 + 39*t1 - 11*Power(t1,2) - 2*s1*(-3 + Power(t1,2)) + 
               t2 - 3*t1*t2)) + 
         Power(s,2)*(12 + 20*Power(t1,3) - 20*t2 + 
            Power(s2,3)*(17 + 17*Power(t1,2) - 
               2*s1*(-3 + 7*t1 + 2*Power(t1,2)) + 2*t1*(-11 + t2) + 2*t2\
) - Power(t1,2)*(36 + s1 + 8*t2) + t1*(-19 + 4*s1 + 26*t2) + 
            Power(s2,4)*(12 - 27*t1 + 8*Power(t1,2) + 
               s1*(-13 + 16*t1) + 14*t2 - 20*t1*t2) + 
            Power(s2,2)*(s1*(-1 + 16*t1 + 3*Power(t1,2)) - 
               2*(-69 + 2*Power(t1,3) + Power(t1,2)*(9 - 4*t2) - 
                  t1*(-71 + t2) + t2)) + 
            s2*(-19 + 125*Power(t1,2) + 16*Power(t1,3) + 
               2*s1*(4 - 11*t1 + Power(t1,2)) + 6*t2 - 2*t1*(39 + 5*t2))) \
- s*Power(-1 + s2,2)*(8 + 16*Power(t1,3) - 10*t2 - 
            Power(t1,2)*(5*s1 + 4*t2) + t1*(-21 + 8*s1 + 10*t2) + 
            Power(s2,2)*(9 + 15*Power(t1,2) + 
               s1*(5 - 4*t1 - 4*Power(t1,2)) + 6*t2 - t1*(19 + 2*t2)) + 
            s2*(11 + s1*(-4 - 4*t1 + 5*Power(t1,2)) - 2*t2 + 
               t1*(-5 + 2*t2) + Power(t1,2)*(-13 + 4*t2)) + 
            Power(s2,3)*(8 + 2*Power(t1,2) + s1*(-5 + 8*t1) + 6*t2 - 
               t1*(11 + 10*t2))) + 
         Power(s,3)*(-8 - 12*Power(t1,3) + t1*(-31 + 12*s1 - 28*t2) + 
            20*t2 + Power(t1,2)*(70 - 5*s1 + 4*t2) + 
            s2*(43 + 4*Power(t1,3) + s1*(-16 + 12*t1 + Power(t1,2)) + 
               Power(t1,2)*(21 - 4*t2) + t1*(89 - 4*t2) + 4*t2) + 
            Power(s2,3)*(-12*Power(t1,2) - 5*s1*(-3 + 4*t1) - 16*t2 + 
               5*t1*(3 + 4*t2)) + 
            Power(s2,2)*(-11 + s1*Power(1 - 2*t1,2) - 7*Power(t1,2) - 
               8*t2 + t1*(31 + 12*t2))))*T2q(s2,s))/
     (s*Power(-1 + s2,2)*(1 - 2*s + Power(s,2) - 2*s2 - 2*s*s2 + 
         Power(s2,2))*(-s + s2 - t1)*(-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))\
) + (8*(8*Power(s,4)*(1 + s2)*(s2 - t1)*t1 + 
         (-1 + s2)*Power(s2 - t1,3)*(-2 + t1 + s2*(-1 + 2*t1))*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) - 
         4*Power(s,3)*(-(Power(s2,2)*
               (2 + (3 + s1)*Power(t1,2) + 2*t1*(-3 + t2))) + 
            Power(s2,3)*(-2 + s1*t1 - t1*t2) + 
            s2*t1*(7 - s1 + 2*Power(t1,2) + 2*t2 - t1*(3 + t2)) + 
            t1*(-1 + 2*Power(t1,2) + 2*Power(t1,3) + t2 + 
               t1*(-8 + s1 + t2))) - 
         2*Power(s,2)*(s2 - t1)*
          (-5*Power(t1,4) + Power(t1,2)*(10 - 4*s1 - 3*t2) + 
            2*(-1 + t2) + Power(t1,3)*(5 - 2*s1 + t2) + 
            2*t1*(-6 + s1 + t2) + 
            Power(s2,3)*(-4 + s1*(2 - 4*t1) + Power(t1,2) - 3*t2 + 
               t1*(9 + 5*t2)) + 
            s2*(6 + Power(t1,4) + 
               2*s1*(-1 + 2*t1 + Power(t1,2) + Power(t1,3)) + 
               Power(t1,2)*(-12 + t2) + 4*t2 - Power(t1,3)*(7 + t2) - 
               2*t1*(-5 + 7*t2)) + 
            Power(s2,2)*(-2*Power(t1,3) - 3*t2 + 
               Power(t1,2)*(-3 + 2*s1 + 2*t2) + t1*(5 - 2*s1 + 7*t2))) - 
         s*Power(s2 - t1,2)*((-3 + 2*s1)*Power(t1,3) + 
            t1*(2 - 4*s1 - 8*t2) + 4*(-1 + t2) + 
            Power(t1,2)*(8 + 2*s1 + 3*t2) + 
            Power(s2,2)*(2 - 5*t1 + 2*Power(t1,3) + 
               s1*(2 + 2*t1 - 4*Power(t1,2)) + Power(t1,2)*(6 - 4*t2) + 
               5*t2) + s2*(22 + 17*Power(t1,3) - 
               2*s1*(-2 + 2*t1 - Power(t1,2) + Power(t1,3)) + 
               Power(t1,2)*(-12 + t2) - 16*t2 + 2*t1*(-17 + 8*t2)) + 
            Power(s2,3)*(12 + 6*s1*(-1 + t1) - 2*Power(t1,2) + 7*t2 - 
               t1*(11 + 8*t2))))*T3q(s2,t1))/
     (s*Power(-1 + s2,2)*Power(s2 - t1,2)*(-s + s2 - t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) + 
    (8*(-16*Power(s,4)*t1 + 8*Power(s,3)*
          (s2*(-2 + Power(t1,2) + t1*(3 + s1 - t2)) - 
            t1*(-1 + s1 + 4*t1 + Power(t1,2) - t2)) + 
         (-1 + s2)*Power(-1 + t1,2)*(-2 + t1 + s2*(-1 + 2*t1))*
          (-1 + s1*(s2 - t1) + t1 + t2 - s2*t2) - 
         2*Power(s,2)*((-29 + 4*s1)*t1 + 5*Power(t1,4) + 
            2*Power(t1,2)*(6 + s1 - 3*t2) + 
            Power(t1,3)*(12 + 2*s1 - t2) - t2 + 
            4*Power(s2,2)*(-1 + 2*Power(t1,2) + s1*(-1 + 2*t1) + t2 - 
               t1*(1 + 2*t2)) - 
            s2*(-4 + Power(t1,4) + 
               2*s1*(-2 + 6*t1 + Power(t1,2) + Power(t1,3)) + 
               Power(t1,2)*(16 - 6*t2) - Power(t1,3)*(-8 + t2) + 3*t2 - 
               t1*(21 + 8*t2))) + 
         s*(-1 + t1)*(-2 - (19 + 2*s1)*Power(t1,3) + 
            Power(t1,2)*(2*s1 + 7*(-5 + t2)) - 
            Power(s2,2)*(-24 + 2*Power(t1,3) + 
               s1*(22 + 6*t1 + 6*Power(t1,2)) + 
               Power(t1,2)*(11 - 4*t2) - 11*t1*(-1 + t2) - 19*t2) + 
            8*Power(s2,3)*(-1 + s1 + t1 - t2) + 4*t2 + 
            t1*(-18*s1 + 7*(8 + t2)) + 
            s2*(-46 + 5*Power(t1,3) + 
               2*s1*(7 + 12*t1 + 2*Power(t1,2) + Power(t1,3)) + 
               Power(t1,2)*(46 - 11*t2) - 15*t2 - t1*(5 + 18*t2))))*T4q(t1))/
     (s*Power(-1 + s2,2)*(-s + s2 - t1)*(-1 + t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))) + 
    (8*(-8*Power(s,4)*t1 + 2*Power(s,3)*
          (-5*Power(t1,2) + s2*
             (-4 + Power(t1,2) + t1*(3 + 2*s1 - t2) - t2) + t2 + 
            t1*(-3 - 2*s1 + t2)) + 
         Power(s,2)*(2 - (45 + 2*s1)*Power(t1,2) - 4*t2 + 
            t1*(36 - 2*s1 + 9*t2) + 
            s2*(-18 + 31*t1 + 7*Power(t1,2) + 
               2*s1*(-1 + 4*t1 + Power(t1,2)) + 5*t2 - 13*t1*t2) - 
            Power(s2,2)*(-5*t1 + 2*Power(t1,2) + s1*(-2 + 6*t1) + t2 - 
               4*t1*t2)) + 4*(s2 - t1)*(-1 + t1)*
          (-1 + 6*Power(t1,2) + t1*(-5 + s1 - 2*t2) + 
            Power(s2,2)*(-2 + s1 + 2*t1 - t2) + t2 - 
            s2*(-7 + s1 + 5*t1 + s1*t1 + 2*Power(t1,2) - 2*t1*t2)) + 
         s*(-56*Power(t1,3) + t1*(-19 + 6*s1 - 15*t2) + 2*(-1 + t2) + 
            s2*(27 + 16*Power(t1,3) + s1*(-6 + 11*Power(t1,2)) + 
               Power(t1,2)*(53 - 16*t2) + 4*t1*(-24 + t2) + 7*t2) + 
            Power(t1,2)*(77 - 9*s1 + 16*t2) - 
            Power(s2,2)*(-11 + t1 + 10*Power(t1,2) + 
               s1*(-9 + 8*t1 + 2*Power(t1,2)) + 12*t2 - 13*t1*t2) + 
            Power(s2,3)*(4 + s1*(-3 + 2*t1) + 3*t2 - 2*t1*(2 + t2))))*T5q(s))/
     (s*Power(-1 + s2,2)*(-s + s2 - t1)*
       (-s2 + t1 - s*t1 + s2*t1 - Power(t1,2))));
   return a;
};
