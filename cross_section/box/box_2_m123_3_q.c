#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>

#define Power p128
#define Pi M_PIq



long double  box_2_m123_3_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((8*(2*Power(s,10)*t2*(1 + s1*(5 + 2*t2)) - 
         Power(s,9)*(6*(3 - 2*t2)*t2 + 
            2*Power(s1,2)*(-5 + (35 + t1)*t2 + 15*Power(t2,2)) + 
            s1*(-2 + Power(t1,3) + 4*(29 + s2)*t2 + 3*Power(t1,2)*t2 + 
               4*(-17 + s2)*Power(t2,2) - 15*Power(t2,3) - t1*t2*(28 + t2)\
)) - Power(s,8)*(2*Power(s1,3)*
             (32 + t1 + (-104 + 3*s2)*t2 - 9*t1*t2 - 48*Power(t2,2)) - 
            2*t2*(33 - 49*t2 + 15*Power(t2,2)) + 
            s1*(16 + (-596 - 50*s2 + 8*Power(s2,2))*t2 + 
               2*(362 + 23*s2 + Power(s2,2))*Power(t2,2) + 
               (-227 + 8*s2)*Power(t2,3) - 15*Power(t2,4) + 
               Power(t1,3)*(-7 + 11*t2) + 
               t1*t2*(305 - 2*Power(s2,2) + s2*(-30 + t2) - 217*t2 + 
                  Power(t2,2)) + 
               Power(t1,2)*(3 - (19 + s2)*t2 + 15*Power(t2,2))) + 
            Power(s1,2)*(-8*Power(t1,3) - 9*Power(t1,2)*t2 + 
               s2*(4 + 3*Power(t1,2) - 88*t2 - 7*t1*t2 - 
                  14*Power(t2,2)) + t1*(-28 + 220*t2 + 21*Power(t2,2)) + 
               2*(52 - 324*t2 + 195*Power(t2,2) + 54*Power(t2,3)))) + 
         Power(s,7)*(4*t2*(-32 + 80*t2 - 55*Power(t2,2) + 
               10*Power(t2,3)) - 
            2*Power(s1,4)*(s2*(3 - 18*t2) + t1*(-8 + 33*t2) + 
               85*(-1 + 2*t2 + Power(t2,2))) + 
            Power(s1,3)*(418 - 28*Power(t1,3) - 1554*t2 + 
               919*Power(t2,2) + 321*Power(t2,3) + 
               Power(s2,2)*(-3*t1 + 14*t2) + Power(t1,2)*(-12 + 23*t2) + 
               s2*(88 + 21*Power(t1,2) + t1*(13 - 76*t2) - 425*t2 - 
                  25*Power(t2,2)) + t1*(-198 + 697*t2 + 109*Power(t2,2))) \
- s1*(-50 + 2*(888 + 57*s2 + 12*Power(s2,2) - 7*Power(s2,3))*t2 - 
               (3027 + 463*s2 - 21*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,2) + 
               (1897 + 241*s2 + 17*Power(s2,2))*Power(t2,3) - 
               4*(108 + 5*s2)*Power(t2,4) + 10*Power(t2,5) + 
               Power(t1,3)*(19 - 44*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(-18 + (185 - 52*s2)*t2 + 
                  (-148 + s2)*Power(t2,2) + 30*Power(t2,3)) + 
               t1*(3 + (-1289 + 101*s2 + 45*Power(s2,2))*t2 - 
                  3*(-613 + 31*s2 + 4*Power(s2,2))*Power(t2,2) - 
                  (684 + s2)*Power(t2,3) + 20*Power(t2,4))) + 
            Power(s1,2)*(466 - 2685*t2 + 2*Power(s2,3)*t2 + 
               3117*Power(t2,2) - 1045*Power(t2,3) - 122*Power(t2,4) + 
               7*Power(t1,3)*(-7 + 10*t2) + 
               Power(s2,2)*(-8 + t1*(2 - 23*t2) + 10*t2 + 
                  35*Power(t2,2)) + 
               Power(t1,2)*(19 - 108*t2 + 39*Power(t2,2)) - 
               t1*(271 - 1968*t2 + 1346*Power(t2,2) + 43*Power(t2,3)) + 
               s2*(46 - 694*t2 + 607*Power(t2,2) - 5*Power(t2,3) - 
                  2*Power(t1,2)*(-8 + 9*t2) + 
                  t1*(24 - 67*t2 + 15*Power(t2,2))))) - 
         Power(s,6)*(-10*Power(s1,5)*
             (-24 - 5*t1 + s2*(3 - 9*t2) + 33*t2 + 13*t1*t2 + 
               18*Power(t2,2)) - 
            2*t2*(71 - 271*t2 + 314*Power(t2,2) - 130*Power(t2,3) + 
               15*Power(t2,4)) + 
            Power(s1,4)*(664 + Power(s2,3) - 56*Power(t1,3) - 2012*t2 + 
               1126*Power(t2,2) + 510*Power(t2,3) + 
               Power(s2,2)*(-17 - 18*t1 + 84*t2) + 
               Power(t1,2)*(-74 + 159*t2) + 
               s2*(356 + 63*Power(t1,2) + t1*(99 - 303*t2) - 939*t2 - 
                  49*Power(t2,2)) + t1*(-548 + 1187*t2 + 259*Power(t2,2))\
) + s1*(79 + (-3353 + 42*s2 - 211*Power(s2,2) + 60*Power(s2,3))*t2 + 
               (7229 + 1193*s2 + 162*Power(s2,2) - 55*Power(s2,3))*
                Power(t2,2) - 
               (6288 + 1601*s2 + 40*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,3) + 
               (2617 + 600*s2 + 55*Power(s2,2))*Power(t2,4) - 
               4*(117 + 20*s2)*Power(t2,5) + 30*Power(t2,6) + 
               Power(t1,3)*(-23 + 50*t2 - 112*Power(t2,2) + 
                  70*Power(t2,3)) + 
               Power(t1,2)*(39 + (-791 + 279*s2)*t2 + 
                  (1127 - 275*s2)*Power(t2,2) + 
                  (-438 + 20*s2)*Power(t2,3) + 30*Power(t2,4)) + 
               t1*(-15 + (3082 - 193*s2 - 165*Power(s2,2))*t2 + 
                  (-6334 + 117*s2 + 218*Power(s2,2))*Power(t2,2) - 
                  (-4474 + s2 + 30*Power(s2,2))*Power(t2,3) - 
                  4*(283 + 5*s2)*Power(t2,4) + 50*Power(t2,5))) + 
            Power(s1,3)*(1256 - 4901*t2 + 5479*Power(t2,2) - 
               1914*Power(t2,3) - 361*Power(t2,4) + 
               Power(s2,3)*(-2 + 22*t2) + 4*Power(t1,3)*(-35 + 46*t2) + 
               Power(t1,2)*(68 - 233*t2 - 81*Power(t2,2)) - 
               t1*(1291 - 5168*t2 + 3457*Power(t2,2) + 
                  217*Power(t2,3)) + 
               Power(s2,2)*(-1 - 115*t2 + 110*Power(t2,2) - 
                  4*t1*(-3 + 25*t2)) + 
               s2*(508 + Power(t1,2)*(80 - 66*t2) - 2625*t2 + 
                  2247*Power(t2,2) - 109*Power(t2,3) + 
                  t1*(17 + 42*t2 + 170*Power(t2,2)))) + 
            Power(s1,2)*(1185 - 6594*t2 + 9943*Power(t2,2) - 
               6182*Power(t2,3) + 1563*Power(t2,4) + 8*Power(t2,5) + 
               Power(t1,3)*(-93 + 226*t2 - 218*Power(t2,2)) + 
               Power(s2,3)*(-14 + 83*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(211 - 1199*t2 + 754*Power(t2,2) - 
                  60*Power(t2,3)) + 
               t1*(-1000 + 6628*t2 - 8949*Power(t2,2) + 
                  3361*Power(t2,3) - 10*Power(t2,4)) + 
               Power(s2,2)*(32 - 313*t2 + 181*Power(t2,2) - 
                  195*Power(t2,3) + t1*(43 - 310*t2 + 102*Power(t2,2))) + 
               s2*(71 - 1758*t2 + 3691*Power(t2,2) - 1875*Power(t2,3) + 
                  184*Power(t2,4) + 
                  Power(t1,2)*(-29 + 215*t2 + 25*Power(t2,2)) + 
                  t1*(47 + 410*t2 - 158*Power(t2,2) + 45*Power(t2,3))))) + 
         Power(s,5)*(-2*Power(s1,6)*
             (-95 - 40*t1 + s2*(30 - 60*t2) + 95*t2 + 75*t1*t2 + 
               57*Power(t2,2)) + 
            2*t2*(-45 + 257*t2 - 452*Power(t2,2) + 316*Power(t2,3) - 
               85*Power(t2,4) + 6*Power(t2,5)) + 
            Power(s1,5)*(516 + 5*Power(s2,3) - 70*Power(t1,3) - 
               1496*t2 + 734*Power(t2,2) + 465*Power(t2,3) + 
               5*Power(t1,2)*(-38 + 69*t2) + 
               Power(s2,2)*(-82 - 45*t1 + 210*t2) + 
               s2*(634 + 105*Power(t1,2) + t1*(294 - 620*t2) - 
                  1138*t2 - 80*Power(t2,2)) + 
               t1*(-794 + 1210*t2 + 325*Power(t2,2))) - 
            s1*(-68 + (4203 - 500*s2 + 444*Power(s2,2) - 
                  100*Power(s2,3))*t2 + 
               (-10884 - 1117*s2 - 912*Power(s2,2) + 228*Power(s2,3))*
                Power(t2,2) + 
               (11751 + 3709*s2 + 484*Power(s2,2) - 76*Power(s2,3))*
                Power(t2,3) - 
               (6671 + 2696*s2 + 224*Power(s2,2) + 20*Power(s2,3))*
                Power(t2,4) + 
               (1963 + 770*s2 + 90*Power(s2,2))*Power(t2,5) - 
               4*(68 + 25*s2)*Power(t2,6) + 21*Power(t2,7) + 
               Power(t1,3)*(5 + 25*t2 + 32*Power(t2,2) - 
                  148*Power(t2,3) + 65*Power(t2,4)) + 
               Power(t1,2)*(-30 + (1682 - 610*s2)*t2 + 
                  (-3758 + 1205*s2)*Power(t2,2) + 
                  (2707 - 592*s2)*Power(t2,3) + 
                  (-652 + 50*s2)*Power(t2,4) + 15*Power(t2,5)) + 
               t1*(24 + (-4771 + 430*s2 + 250*Power(s2,2))*t2 + 
                  (12346 + 100*s2 - 706*Power(s2,2))*Power(t2,2) + 
                  (-12342 - 641*s2 + 426*Power(s2,2))*Power(t2,3) + 
                  (5610 + 338*s2 - 40*Power(s2,2))*Power(t2,4) - 
                  2*(524 + 25*s2)*Power(t2,5) + 55*Power(t2,6))) + 
            Power(s1,4)*(1314 - 4337*t2 + 4938*Power(t2,2) - 
               1708*Power(t2,3) - 524*Power(t2,4) + 
               5*Power(t1,3)*(-41 + 50*t2) + Power(s2,3)*(-19 + 80*t2) + 
               Power(t1,2)*(173 - 224*t2 - 461*Power(t2,2)) + 
               Power(s2,2)*(109 + t1*(55 - 240*t2) - 394*t2 + 
                  101*Power(t2,2)) - 
               t1*(2325 - 7250*t2 + 4787*Power(t2,2) + 
                  399*Power(t2,3)) + 
               s2*(1256 + Power(t1,2)*(139 - 80*t2) - 4653*t2 + 
                  3903*Power(t2,2) - 247*Power(t2,3) + 
                  t1*(-171 + 254*t2 + 634*Power(t2,2)))) + 
            Power(s1,3)*(2232 - 9357*t2 + 12595*Power(t2,2) - 
               7789*Power(t2,3) + 2173*Power(t2,4) + 112*Power(t2,5) + 
               Power(t1,3)*(-160 + 467*t2 - 474*Power(t2,2)) + 
               Power(s2,3)*(-69 + 233*t2 - 33*Power(t2,2)) + 
               Power(t1,2)*(893 - 3104*t2 + 1710*Power(t2,2) + 
                  136*Power(t2,3)) + 
               t1*(-2917 + 13234*t2 - 17288*Power(t2,2) + 
                  6816*Power(t2,3) + 144*Power(t2,4)) + 
               Power(s2,2)*(306 - 1433*t2 + 1214*Power(t2,2) - 
                  637*Power(t2,3) + t1*(233 - 883*t2 + 369*Power(t2,2))) \
+ s2*(802 - 5227*t2 + 9218*Power(t2,2) - 4944*Power(t2,3) + 
                  527*Power(t2,4) + 
                  Power(t1,2)*(-138 + 417*t2 + 71*Power(t2,2)) + 
                  t1*(-626 + 2901*t2 - 1666*Power(t2,2) + 78*Power(t2,3))\
)) + Power(s1,2)*(1882 - 10084*t2 + 18280*Power(t2,2) - 
               14807*Power(t2,3) + 5937*Power(t2,4) - 1247*Power(t2,5) + 
               78*Power(t2,6) - 
               Power(s2,3)*(46 - 281*t2 + 268*Power(t2,2) + 
                  52*Power(t2,3)) + 
               Power(t1,3)*(-47 + 192*t2 - 402*Power(t2,2) + 
                  332*Power(t2,3)) + 
               Power(t1,2)*(682 - 4134*t2 + 5623*Power(t2,2) - 
                  1931*Power(t2,3) + 30*Power(t2,4)) + 
               t1*(-2076 + 12546*t2 - 22535*Power(t2,2) + 
                  16214*Power(t2,3) - 4330*Power(t2,4) + 120*Power(t2,5)) \
+ Power(s2,2)*(179 - 1225*t2 + 1761*Power(t2,2) - 927*Power(t2,3) + 
                  444*Power(t2,4) + 
                  t1*(122 - 897*t2 + 1190*Power(t2,2) - 182*Power(t2,3))) \
+ s2*(-101 - 2221*t2 + 7640*Power(t2,2) - 7887*Power(t2,3) + 
                  3010*Power(t2,4) - 406*Power(t2,5) + 
                  Power(t1,2)*
                   (-220 + 1085*t2 - 1102*Power(t2,2) + 32*Power(t2,3)) + 
                  t1*(98 + 1622*t2 - 3475*Power(t2,2) + 
                     1408*Power(t2,3) - 202*Power(t2,4))))) - 
         Power(-1 + s1,2)*s1*(-1 + t2)*
          (s1 + Power(s1,2) - 2*s1*t2 + (-1 + t2)*t2)*
          (-10 + 56*t2 - 22*s2*t2 - 77*Power(t2,2) + 11*s2*Power(t2,2) - 
            31*Power(s2,2)*Power(t2,2) + 4*Power(s2,3)*Power(t2,2) + 
            27*Power(t2,3) + 36*s2*Power(t2,3) + 
            38*Power(s2,2)*Power(t2,3) - 7*Power(s2,3)*Power(t2,3) + 
            3*Power(t2,4) - 17*s2*Power(t2,4) - 
            14*Power(s2,2)*Power(t2,4) - 2*Power(s2,3)*Power(t2,4) + 
            Power(t2,5) - 8*s2*Power(t2,5) + 7*Power(s2,2)*Power(t2,5) + 
            Power(s1,4)*Power(s2 - t1,2)*(-4 + 5*s2 - 5*t1 + 4*t2) + 
            Power(t1,3)*(10 - 14*t2 + 3*Power(t2,2) + 6*Power(t2,3)) + 
            Power(t1,2)*(-30 + (84 - 22*s2)*t2 + 
               (-145 + 46*s2)*Power(t2,2) + (127 - 45*s2)*Power(t2,3) + 
               6*(-6 + s2)*Power(t2,4)) + 
            t1*(30 + 2*(-63 + 22*s2)*t2 + 
               (187 + 11*s2 - 9*Power(s2,2))*Power(t2,2) + 
               2*(-55 - 49*s2 + 13*Power(s2,2))*Power(t2,3) + 
               (13 + 49*s2 - 2*Power(s2,2))*Power(t2,4) - 
               6*(-1 + s2)*Power(t2,5)) - 
            Power(s1,3)*(4*Power(-1 + t2,2) + 5*t1*Power(-1 + t2,2) - 
               2*Power(t1,3)*(2 + 3*t2) + Power(s2,3)*(1 + 9*t2) + 
               3*Power(t1,2)*(3 - 5*t2 + 2*Power(t2,2)) + 
               3*Power(s2,2)*
                (6 - 11*t2 + 5*Power(t2,2) - 2*t1*(1 + 4*t2)) + 
               s2*(-13*Power(-1 + t2,2) + 3*Power(t1,2)*(3 + 7*t2) + 
                  t1*(-23 + 40*t2 - 17*Power(t2,2)))) + 
            Power(s1,2)*(-1 + t2)*
             (-3 + Power(s2,3)*(-4 + t2) - 2*t2 + 5*Power(t2,2) + 
               Power(t1,3)*(-7 + 5*t2) + 
               Power(t1,2)*(73 - 56*t2 - 4*Power(t2,2)) + 
               t1*(-31 + 7*t2 + 24*Power(t2,2)) + 
               Power(s2,2)*(31 + t1*(9 - 5*t2) - 43*t2 + 
                  25*Power(t2,2)) - 
               s2*(-41 + 7*t2 + 34*Power(t2,2) + 
                  Power(t1,2)*(-6 + 5*t2) + 
                  t1*(115 - 105*t2 + 16*Power(t2,2)))) + 
            s1*(-2*Power(-1 + t2,2)*(13 + 4*t2 + Power(t2,2)) + 
               Power(s2,3)*t2*(-8 + 13*t2 + 5*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(-36 + 56*t2 + 25*Power(t2,2)) - 
               2*Power(t1,3)*
                (8 - 10*t2 + 4*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(6 + 128*t2 - 217*Power(t2,2) + 
                  77*Power(t2,3) + 6*Power(t2,4)) + 
               Power(s2,2)*t2*
                (62 - 94*t2 + 53*Power(t2,2) - 21*Power(t2,3) - 
                  2*t1*(-9 + 23*t2 + Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(22 + 74*t2 + 29*Power(t2,2)) + 
                  Power(t1,2)*
                   (22 - 40*t2 + 43*Power(t2,2) + 5*Power(t2,3)) + 
                  t1*(-44 - 126*t2 + 295*Power(t2,2) - 138*Power(t2,3) + 
                     13*Power(t2,4))))) + 
         Power(s,4)*(2*Power(s1,7)*
             (-40 - 35*t1 + s2*(30 - 45*t2) + 30*t2 + 51*t1*t2 + 
               20*Power(t2,2)) + 
            2*t2*(15 - 136*t2 + 352*Power(t2,2) - 371*Power(t2,3) + 
               169*Power(t2,4) - 29*Power(t2,5) + Power(t2,6)) - 
            Power(s1,6)*(210 + 10*Power(s2,3) - 56*Power(t1,3) - 
               2*Power(s2,2)*(79 + 30*t1 - 140*t2) - 616*t2 + 
               214*Power(t2,2) + 240*Power(t2,3) + 
               Power(t1,2)*(-260 + 397*t2) + 
               s2*(592 + 105*Power(t1,2) + t1*(446 - 725*t2) - 806*t2 - 
                  64*Power(t2,2)) + t1*(-660 + 778*t2 + 211*Power(t2,2))) \
+ s1*(-30 + (3592 - 808*s2 + 426*Power(s2,2) - 80*Power(s2,3))*t2 + 
               (-10863 + 184*s2 - 1590*Power(s2,2) + 342*Power(s2,3))*
                Power(t2,2) + 
               (13583 + 3991*s2 + 1654*Power(s2,2) - 326*Power(s2,3))*
                Power(t2,3) + 
               (-9329 - 5149*s2 - 790*Power(s2,2) + 34*Power(s2,3))*
                Power(t2,4) + 
               (3680 + 2358*s2 + 356*Power(s2,2) + 20*Power(s2,3))*
                Power(t2,5) - 
               (733 + 514*s2 + 80*Power(s2,2))*Power(t2,6) + 
               (71 + 56*s2)*Power(t2,7) - 5*Power(t2,8) + 
               Power(t1,3)*(-19 + 95*t2 - 178*Power(t2,2) + 
                  20*Power(t2,3) + 107*Power(t2,4) - 31*Power(t2,5)) + 
               Power(t1,2)*(15 + (2051 - 725*s2)*t2 + 
                  3*(-2131 + 724*s2)*Power(t2,2) + 
                  (7081 - 2066*s2)*Power(t2,3) + 
                  (-3236 + 658*s2)*Power(t2,4) + 
                  (523 - 55*s2)*Power(t2,5) - 3*Power(t2,6)) + 
               t1*(6 + 5*(-991 + 152*s2 + 36*Power(s2,2))*t2 + 
                  (15270 - 159*s2 - 944*Power(s2,2))*Power(t2,2) + 
                  (-19109 - 1906*s2 + 1194*Power(s2,2))*Power(t2,3) + 
                  (12037 + 1748*s2 - 424*Power(s2,2))*Power(t2,4) + 
                  (-3795 - 552*s2 + 30*Power(s2,2))*Power(t2,5) + 
                  (525 + 55*s2)*Power(t2,6) - 29*Power(t2,7))) + 
            Power(s1,5)*(-606 + Power(t1,3)*(150 - 170*t2) + 
               Power(s2,3)*(61 - 145*t2) + 1979*t2 - 2422*Power(t2,2) + 
               685*Power(t2,3) + 401*Power(t2,4) + 
               Power(t1,2)*(-299 + 69*t2 + 775*Power(t2,2)) + 
               t1*(2052 - 5892*t2 + 3863*Power(t2,2) + 
                  321*Power(t2,3)) + 
               Power(s2,2)*(-294 + 548*t2 + 62*Power(t2,2) + 
                  5*t1*(-31 + 73*t2)) - 
               s2*(1339 - 4446*t2 + 3636*Power(t2,2) - 278*Power(t2,3) + 
                  Power(t1,2)*(61 + 30*t2) + 
                  t1*(-495 + 310*t2 + 1093*Power(t2,2)))) + 
            Power(s1,4)*(-1873 + 6248*t2 - 7488*Power(t2,2) + 
               4834*Power(t2,3) - 1352*Power(t2,4) - 188*Power(t2,5) + 
               Power(s2,3)*(153 - 403*t2 + 172*Power(t2,2)) + 
               Power(t1,3)*(107 - 481*t2 + 500*Power(t2,2)) - 
               Power(t1,2)*(1628 - 4422*t2 + 2311*Power(t2,2) + 
                  530*Power(t2,3)) + 
               t1*(3139 - 12850*t2 + 17177*Power(t2,2) - 
                  7196*Power(t2,3) - 278*Power(t2,4)) + 
               Power(s2,2)*(-893 + 2961*t2 - 2647*Power(t2,2) + 
                  967*Power(t2,3) + 
                  t1*(-509 + 1405*t2 - 750*Power(t2,2))) + 
               s2*(-1028 + 5952*t2 - 10766*Power(t2,2) + 
                  6174*Power(t2,3) - 713*Power(t2,4) + 
                  Power(t1,2)*(311 - 573*t2 + 37*Power(t2,2)) + 
                  t1*(1982 - 5947*t2 + 3746*Power(t2,2) + 67*Power(t2,3))\
)) + Power(s1,3)*(-2164 + 10618*t2 - 17473*Power(t2,2) + 
               12371*Power(t2,3) - 4786*Power(t2,4) + 1195*Power(t2,5) - 
               60*Power(t2,6) + 
               Power(t1,3)*(21 - 174*t2 + 539*Power(t2,2) - 
                  616*Power(t2,3)) + 
               Power(s2,3)*(123 - 548*t2 + 598*Power(t2,2) + 
                  57*Power(t2,3)) + 
               Power(t1,2)*(-1856 + 8492*t2 - 11089*Power(t2,2) + 
                  3862*Power(t2,3) + 164*Power(t2,4)) + 
               t1*(3594 - 17433*t2 + 29603*Power(t2,2) - 
                  22476*Power(t2,3) + 6682*Power(t2,4) - 56*Power(t2,5)) \
+ Power(s2,2)*(-598 + 3377*t2 - 5575*Power(t2,2) + 3247*Power(t2,3) - 
                  1199*Power(t2,4) + 
                  t1*(-383 + 1950*t2 - 2620*Power(t2,2) + 
                     487*Power(t2,3))) + 
               s2*(-787 + 5460*t2 - 12004*Power(t2,2) + 
                  12582*Power(t2,3) - 5391*Power(t2,4) + 
                  779*Power(t2,5) + 
                  Power(t1,2)*
                   (471 - 1844*t2 + 1970*Power(t2,2) + 2*Power(t2,3)) + 
                  t1*(1180 - 7654*t2 + 11915*Power(t2,2) - 
                     5098*Power(t2,3) + 555*Power(t2,4)))) - 
            Power(s1,2)*(1983 - 9826*t2 + 19800*Power(t2,2) - 
               19900*Power(t2,3) + 9913*Power(t2,4) - 2492*Power(t2,5) + 
               433*Power(t2,6) - 52*Power(t2,7) + 
               Power(t1,3)*(42 - 109*t2 - 67*Power(t2,2) + 
                  328*Power(t2,3) - 268*Power(t2,4)) + 
               Power(s2,3)*(-54 + 411*t2 - 703*Power(t2,2) + 
                  296*Power(t2,3) + 98*Power(t2,4)) + 
               Power(t1,2)*(1093 - 6853*t2 + 13481*Power(t2,2) - 
                  10173*Power(t2,3) + 2409*Power(t2,4) + 15*Power(t2,5)) \
+ t1*(-2725 + 15096*t2 - 31237*Power(t2,2) + 29946*Power(t2,3) - 
                  13994*Power(t2,4) + 2960*Power(t2,5) - 143*Power(t2,6)) \
+ Power(s2,2)*(265 - 1964*t2 + 4048*Power(t2,2) - 3702*Power(t2,3) + 
                  1689*Power(t2,4) - 526*Power(t2,5) + 
                  t1*(128 - 1179*t2 + 2551*Power(t2,2) - 
                     1782*Power(t2,3) + 168*Power(t2,4))) + 
               s2*(-387 - 1394*t2 + 8939*Power(t2,2) - 
                  12712*Power(t2,3) + 8043*Power(t2,4) - 
                  2490*Power(t2,5) + 374*Power(t2,6) + 
                  Power(t1,2)*
                   (-405 + 2075*t2 - 3347*Power(t2,2) + 
                     1962*Power(t2,3) - 123*Power(t2,4)) + 
                  t1*(320 + 1935*t2 - 8194*Power(t2,2) + 
                     8455*Power(t2,3) - 2756*Power(t2,4) + 303*Power(t2,5)\
)))) + Power(s,3)*(-2*t2*(2 - 37*t2 + 146*Power(t2,2) - 
               222*Power(t2,3) + 151*Power(t2,4) - 44*Power(t2,5) + 
               4*Power(t2,6)) + 
            2*Power(s1,8)*(7 + t1*(16 - 19*t2) - 4*t2 - 3*Power(t2,2) + 
               3*s2*(-5 + 6*t2)) + 
            s1*(2 + 2*(-1037 + 308*s2 - 98*Power(s2,2) + 
                  15*Power(s2,3))*t2 + 
               (7343 - 1165*s2 + 1275*Power(s2,2) - 238*Power(s2,3))*
                Power(t2,2) + 
               (-10286 - 1773*s2 - 2251*Power(s2,2) + 436*Power(s2,3))*
                Power(t2,3) + 
               (7715 + 4912*s2 + 1615*Power(s2,2) - 208*Power(s2,3))*
                Power(t2,4) - 
               (3534 + 3452*s2 + 733*Power(s2,2) + 14*Power(s2,3))*
                Power(t2,5) + 
               (922 + 1013*s2 + 271*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,6) - 
               (92 + 161*s2 + 37*Power(s2,2))*Power(t2,7) + 
               4*(1 + 3*s2)*Power(t2,8) + 
               Power(t1,3)*(23 - 54*t2 + 172*Power(t2,2) - 
                  217*Power(t2,3) + 37*Power(t2,4) + 40*Power(t2,5) - 
                  6*Power(t2,6)) + 
               Power(t1,2)*(-42 + (-1541 + 504*s2)*t2 + 
                  (6140 - 2043*s2)*Power(t2,2) + 
                  (-9303 + 2966*s2)*Power(t2,3) - 
                  18*(-361 + 98*s2)*Power(t2,4) + 
                  8*(-253 + 49*s2)*Power(t2,5) + 
                  (216 - 29*s2)*Power(t2,6)) + 
               t1*(21 + (3375 - 745*s2 - 57*Power(s2,2))*t2 + 
                  (-12254 + 917*s2 + 596*Power(s2,2))*Power(t2,2) + 
                  (18202 + 1840*s2 - 1365*Power(s2,2))*Power(t2,3) + 
                  (-14190 - 3295*s2 + 998*Power(s2,2))*Power(t2,4) + 
                  (6015 + 1749*s2 - 221*Power(s2,2))*Power(t2,5) + 
                  (-1285 - 383*s2 + 12*Power(s2,2))*Power(t2,6) + 
                  (120 + 29*s2)*Power(t2,7) - 6*Power(t2,8))) + 
            Power(s1,7)*(48 + 10*Power(s2,3) - 28*Power(t1,3) - 114*t2 - 
               Power(t2,2) + 63*Power(t2,3) + 
               Power(s2,2)*(-152 - 45*t1 + 210*t2) + 
               Power(t1,2)*(-200 + 261*t2) + 
               t1*(-314 + 325*t2 + 51*Power(t2,2)) + 
               s2*(296 + 63*Power(t1,2) - 341*t2 - 9*Power(t2,2) - 
                  123*t1*(-3 + 4*t2))) + 
            Power(s1,6)*(140 - 560*t2 + 629*Power(t2,2) - 
               39*Power(t2,3) - 154*Power(t2,4) + 
               Power(t1,3)*(-27 + 26*t2) + 2*Power(s2,3)*(-47 + 75*t2) + 
               Power(t1,2)*(321 + 20*t2 - 635*Power(t2,2)) + 
               Power(s2,2)*(362 + t1*(240 - 367*t2) - 420*t2 - 
                  171*Power(t2,2)) - 
               t1*(939 - 2717*t2 + 1804*Power(t2,2) + 73*Power(t2,3)) + 
               s2*(699 - 2258*t2 + 1821*Power(t2,2) - 199*Power(t2,3) + 
                  2*Power(t1,2)*(-53 + 87*t2) + 
                  t1*(-633 + 217*t2 + 963*Power(t2,2)))) + 
            Power(s1,5)*(785 - 2088*t2 + 2491*Power(t2,2) - 
               1629*Power(t2,3) + 263*Power(t2,4) + 122*Power(t2,5) + 
               Power(s2,3)*(-175 + 444*t2 - 297*Power(t2,2)) + 
               Power(t1,3)*(-15 + 234*t2 - 220*Power(t2,2)) + 
               Power(t1,2)*(1511 - 3889*t2 + 2026*Power(t2,2) + 
                  610*Power(t2,3)) + 
               t1*(-1445 + 6438*t2 - 9236*Power(t2,2) + 
                  4011*Power(t2,3) + 170*Power(t2,4)) + 
               Power(s2,2)*(1080 - 3184*t2 + 2921*Power(t2,2) - 
                  797*Power(t2,3) + t1*(546 - 1375*t2 + 930*Power(t2,2))\
) - s2*(-289 + 3250*t2 - 6468*Power(t2,2) + 3834*Power(t2,3) - 
                  517*Power(t2,4) + 
                  Power(t1,2)*(347 - 614*t2 + 323*Power(t2,2)) + 
                  t1*(2275 - 6342*t2 + 4232*Power(t2,2) + 
                     201*Power(t2,3)))) + 
            Power(s1,4)*(1335 - 5868*t2 + 8041*Power(t2,2) - 
               4920*Power(t2,3) + 1909*Power(t2,4) - 391*Power(t2,5) + 
               6*Power(t2,6) + 
               Power(s2,3)*(-103 + 594*t2 - 741*Power(t2,2) + 
                  58*Power(t2,3)) + 
               Power(t1,3)*(25 - 65*t2 - 354*Power(t2,2) + 
                  544*Power(t2,3)) + 
               Power(t1,2)*(1925 - 8481*t2 + 11558*Power(t2,2) - 
                  4462*Power(t2,3) - 360*Power(t2,4)) - 
               t1*(2489 - 10372*t2 + 17843*Power(t2,2) - 
                  15219*Power(t2,3) + 4953*Power(t2,4) + 54*Power(t2,5)) \
+ Power(s2,2)*(765 - 4552*t2 + 7579*Power(t2,2) - 4806*Power(t2,3) + 
                  1548*Power(t2,4) + 
                  t1*(375 - 2147*t2 + 2959*Power(t2,2) - 
                     737*Power(t2,3))) + 
               s2*(727 - 2301*t2 + 6411*Power(t2,2) - 8967*Power(t2,3) + 
                  4344*Power(t2,4) - 714*Power(t2,5) + 
                  Power(t1,2)*
                   (-377 + 1830*t2 - 1939*Power(t2,2) + 54*Power(t2,3)) \
+ t1*(-1954 + 10097*t2 - 15733*Power(t2,2) + 7753*Power(t2,3) - 
                     717*Power(t2,4)))) + 
            Power(s1,3)*(1031 - 5876*t2 + 12822*Power(t2,2) - 
               11838*Power(t2,3) + 4505*Power(t2,4) - 877*Power(t2,5) + 
               174*Power(t2,6) - 41*Power(t2,7) + 
               Power(t1,3)*(51 - 128*t2 + 170*Power(t2,2) + 
                  173*Power(t2,3) - 424*Power(t2,4)) + 
               Power(s2,3)*(-95 + 457*t2 - 826*Power(t2,2) + 
                  523*Power(t2,3) + 151*Power(t2,4)) + 
               Power(t1,2)*(1630 - 9123*t2 + 17717*Power(t2,2) - 
                  14680*Power(t2,3) + 3996*Power(t2,4) + 141*Power(t2,5)\
) + t1*(-2442 + 13237*t2 - 25618*Power(t2,2) + 23996*Power(t2,3) - 
                  12477*Power(t2,4) + 3225*Power(t2,5) - 119*Power(t2,6)\
) + Power(s2,2)*(504 - 2945*t2 + 7129*Power(t2,2) - 7795*Power(t2,3) + 
                  3641*Power(t2,4) - 1092*Power(t2,5) + 
                  t1*(264 - 1578*t2 + 3413*Power(t2,2) - 
                     2900*Power(t2,3) + 294*Power(t2,4))) + 
               s2*(551 - 4821*t2 + 9201*Power(t2,2) - 9322*Power(t2,3) + 
                  6993*Power(t2,4) - 2702*Power(t2,5) + 
                  514*Power(t2,6) + 
                  Power(t1,2)*
                   (-458 + 2027*t2 - 3798*Power(t2,2) + 
                     2781*Power(t2,3) - 81*Power(t2,4)) + 
                  t1*(-905 + 7543*t2 - 17520*Power(t2,2) + 
                     17092*Power(t2,3) - 6108*Power(t2,4) + 
                     643*Power(t2,5)))) + 
            Power(s1,2)*(1407 - 6416*t2 + 12565*Power(t2,2) - 
               14074*Power(t2,3) + 8790*Power(t2,4) - 2396*Power(t2,5) + 
               148*Power(t2,6) - 2*Power(t2,7) + 10*Power(t2,8) - 
               Power(s2,3)*(26 - 317*t2 + 766*Power(t2,2) - 
                  599*Power(t2,3) + 114*Power(t2,4) + 82*Power(t2,5)) + 
               Power(t1,3)*(35 - 231*t2 + 274*Power(t2,2) - 
                  48*Power(t2,3) - 97*Power(t2,4) + 110*Power(t2,5)) + 
               Power(t1,2)*(1042 - 6395*t2 + 15319*Power(t2,2) - 
                  16773*Power(t2,3) + 8559*Power(t2,4) - 
                  1561*Power(t2,5) - 21*Power(t2,6)) + 
               t1*(-2296 + 11763*t2 - 26016*Power(t2,2) + 
                  29308*Power(t2,3) - 17096*Power(t2,4) + 
                  5261*Power(t2,5) - 940*Power(t2,6) + 69*Power(t2,7)) + 
               Power(s2,2)*(161 - 1687*t2 + 4361*Power(t2,2) - 
                  5264*Power(t2,3) + 3772*Power(t2,4) - 
                  1451*Power(t2,5) + 339*Power(t2,6) + 
                  t1*(52 - 820*t2 + 2478*Power(t2,2) - 
                     2730*Power(t2,3) + 1272*Power(t2,4) - 87*Power(t2,5)\
)) + s2*(-427 + 53*t2 + 6505*Power(t2,2) - 12459*Power(t2,3) + 
                  9015*Power(t2,4) - 3554*Power(t2,5) + 903*Power(t2,6) - 
                  157*Power(t2,7) + 
                  Power(t1,2)*
                   (-362 + 2089*t2 - 4144*Power(t2,2) + 
                     3795*Power(t2,3) - 1652*Power(t2,4) + 
                     134*Power(t2,5)) + 
                  t1*(482 + 745*t2 - 8291*Power(t2,2) + 
                     13507*Power(t2,3) - 8972*Power(t2,4) + 
                     2393*Power(t2,5) - 225*Power(t2,6))))) + 
         Power(s,2)*(-6*Power(s1,9)*(s2 - t1)*(-1 + t2) + 
            2*Power(-1 + t2,2)*Power(t2,2)*
             (-4 + 21*t2 - 18*Power(t2,2) + 4*Power(t2,3)) + 
            Power(s1,8)*(-5*Power(s2,3) + 8*Power(t1,3) + 
               Power(t1,2)*(82 - 93*t2) + 
               Power(s2,2)*(73 + 18*t1 - 84*t2) + 
               t1*(76 - 87*t2 + 11*Power(t2,2)) - 
               s2*(72 + 21*Power(t1,2) + t1*(159 - 181*t2) - 87*t2 + 
                  15*Power(t2,2)) + 
               2*(-3 + t2 + 5*Power(t2,2) - 3*Power(t2,3))) + 
            s1*(5 + (751 - 232*s2 + 35*Power(s2,2) - 4*Power(s2,3))*t2 + 
               (-3226 + 873*s2 - 478*Power(s2,2) + 75*Power(s2,3))*
                Power(t2,2) + 
               (5201 - 107*s2 + 1409*Power(s2,2) - 252*Power(s2,3))*
                Power(t2,3) + 
               (-4106 - 2215*s2 - 1575*Power(s2,2) + 249*Power(s2,3))*
                Power(t2,4) + 
               (1794 + 2542*s2 + 889*Power(s2,2) - 48*Power(s2,3))*
                Power(t2,5) - 
               (479 + 1004*s2 + 373*Power(s2,2) + 17*Power(s2,3))*
                Power(t2,6) + 
               (54 + 159*s2 + 100*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,7) + (6 - 16*s2 - 7*Power(s2,2))*Power(t2,8) + 
               Power(t1,3)*(-11 - 24*t2 + 32*Power(t2,2) + 
                  91*Power(t2,3) - 121*Power(t2,4) + 22*Power(t2,5) + 
                  6*Power(t2,6)) + 
               Power(t1,2)*(27 + (733 - 197*s2)*t2 + 
                  (-3425 + 1043*s2)*Power(t2,2) + 
                  (6524 - 2092*s2)*Power(t2,3) + 
                  (-6247 + 1913*s2)*Power(t2,4) + 
                  (2974 - 761*s2)*Power(t2,5) + 
                  (-622 + 115*s2)*Power(t2,6) - 6*(-6 + s2)*Power(t2,7)) \
+ t1*(-21 + (-1412 + 361*s2 + 5*Power(s2,2))*t2 + 
                  (6105 - 967*s2 - 166*Power(s2,2))*Power(t2,2) + 
                  (-10641 - 460*s2 + 711*Power(s2,2))*Power(t2,3) + 
                  (9779 + 2674*s2 - 928*Power(s2,2))*Power(t2,4) + 
                  (-5007 - 2298*s2 + 415*Power(s2,2))*Power(t2,5) + 
                  (1345 + 803*s2 - 54*Power(s2,2))*Power(t2,6) + 
                  (-154 - 119*s2 + 2*Power(s2,2))*Power(t2,7) + 
                  6*(1 + s2)*Power(t2,8))) + 
            Power(s1,7)*(-50 + Power(s2,3)*(76 - 92*t2) + 140*t2 - 
               79*Power(t2,2) - 34*Power(t2,3) + 23*Power(t2,4) + 
               4*Power(t1,3)*(-9 + 10*t2) + 
               Power(t1,2)*(-194 + 7*t2 + 251*Power(t2,2)) + 
               t1*(193 - 591*t2 + 439*Power(t2,2) - 41*Power(t2,3)) + 
               Power(s2,2)*(-239 + 201*t2 + 102*Power(t2,2) + 
                  t1*(-202 + 238*t2)) + 
               s2*(-141 + 479*t2 - 431*Power(t2,2) + 93*Power(t2,3) - 
                  2*Power(t1,2)*(-79 + 91*t2) - 
                  2*t1*(-207 + 67*t2 + 204*Power(t2,2)))) + 
            Power(s1,6)*(-116 + 448*t2 - 647*Power(t2,2) + 
               282*Power(t2,3) + 61*Power(t2,4) - 28*Power(t2,5) + 
               Power(t1,3)*(7 - 20*t2 - 30*Power(t2,2)) + 
               Power(s2,3)*(91 - 293*t2 + 252*Power(t2,2)) - 
               Power(t1,2)*(768 - 2067*t2 + 1110*Power(t2,2) + 
                  280*Power(t2,3)) + 
               t1*(322 - 1685*t2 + 2367*Power(t2,2) - 992*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(s2,2)*(-583 + 1887*t2 - 1786*Power(t2,2) + 
                  391*Power(t2,3) + t1*(-261 + 820*t2 - 702*Power(t2,2))\
) + s2*(-90 + 1015*t2 - 1702*Power(t2,2) + 980*Power(t2,3) - 
                  203*Power(t2,4) + 
                  Power(t1,2)*(144 - 441*t2 + 433*Power(t2,2)) + 
                  t1*(1290 - 3806*t2 + 2623*Power(t2,2) + 75*Power(t2,3))\
)) + Power(s1,5)*(-534 + 1718*t2 - 2128*Power(t2,2) + 1457*Power(t2,3) - 
               446*Power(t2,4) - 73*Power(t2,5) + 6*Power(t2,6) + 
               Power(t1,3)*(-32 + 85*t2 + 168*Power(t2,2) - 
                  206*Power(t2,3)) + 
               Power(s2,3)*(21 - 339*t2 + 488*Power(t2,2) - 
                  153*Power(t2,3)) + 
               Power(t1,2)*(-791 + 4419*t2 - 6882*Power(t2,2) + 
                  2952*Power(t2,3) + 246*Power(t2,4)) + 
               t1*(699 - 2720*t2 + 5495*Power(t2,2) - 
                  5014*Power(t2,3) + 1494*Power(t2,4) + 46*Power(t2,5)) \
+ Power(s2,2)*(-391 + 2754*t2 - 4977*Power(t2,2) + 3617*Power(t2,3) - 
                  1059*Power(t2,4) + 
                  t1*(-105 + 1156*t2 - 1729*Power(t2,2) + 
                     659*Power(t2,3))) + 
               s2*(240 - 444*t2 - 1753*Power(t2,2) + 2995*Power(t2,3) - 
                  1353*Power(t2,4) + 315*Power(t2,5) + 
                  Power(t1,2)*
                   (141 - 918*t2 + 958*Power(t2,2) - 194*Power(t2,3)) + 
                  t1*(749 - 5873*t2 + 10704*Power(t2,2) - 
                     6003*Power(t2,3) + 535*Power(t2,4)))) - 
            Power(s1,4)*(253 - 2224*t2 + 4756*Power(t2,2) - 
               3884*Power(t2,3) + 1476*Power(t2,4) - 296*Power(t2,5) - 
               71*Power(t2,6) - 10*Power(t2,7) + 
               Power(t1,3)*t2*
                (-137 + 386*t2 + 7*Power(t2,2) - 314*Power(t2,3)) + 
               Power(s2,3)*(-30 + 108*t2 - 489*Power(t2,2) + 
                  448*Power(t2,3) + 84*Power(t2,4)) + 
               Power(t1,2)*(914 - 4556*t2 + 10307*Power(t2,2) - 
                  10612*Power(t2,3) + 3523*Power(t2,4) + 185*Power(t2,5)\
) + t1*(-1021 + 4715*t2 - 8076*Power(t2,2) + 8170*Power(t2,3) - 
                  5135*Power(t2,4) + 1376*Power(t2,5) - 29*Power(t2,6)) \
+ Power(s2,2)*(240 - 1612*t2 + 5120*Power(t2,2) - 6354*Power(t2,3) + 
                  3421*Power(t2,4) - 1054*Power(t2,5) + 
                  t1*(120 - 587*t2 + 2078*Power(t2,2) - 
                     2187*Power(t2,3) + 276*Power(t2,4))) + 
               s2*(688 - 1041*t2 - 488*Power(t2,2) - 776*Power(t2,3) + 
                  2325*Power(t2,4) - 1014*Power(t2,5) + 
                  306*Power(t2,6) + 
                  Power(t1,2)*
                   (-114 + 840*t2 - 2392*Power(t2,2) + 
                     1902*Power(t2,3) + Power(t2,4)) + 
                  t1*(-974 + 4218*t2 - 11126*Power(t2,2) + 
                     13876*Power(t2,3) - 6155*Power(t2,4) + 
                     639*Power(t2,5)))) + 
            Power(s1,3)*(-226 + 948*t2 - 2877*Power(t2,2) + 
               4411*Power(t2,3) - 2615*Power(t2,4) + 386*Power(t2,5) + 
               35*Power(t2,6) - 57*Power(t2,7) - 5*Power(t2,8) + 
               Power(s2,3)*(47 - 240*t2 + 314*Power(t2,2) - 
                  321*Power(t2,3) + 195*Power(t2,4) + 111*Power(t2,5)) - 
               Power(t1,3)*(43 - 25*t2 + 2*Power(t2,2) - 
                  223*Power(t2,3) + 121*Power(t2,4) + 144*Power(t2,5)) + 
               Power(t1,2)*(-652 + 4564*t2 - 10463*Power(t2,2) + 
                  12398*Power(t2,3) - 8171*Power(t2,4) + 
                  2057*Power(t2,5) + 67*Power(t2,6)) + 
               t1*(719 - 4738*t2 + 10853*Power(t2,2) - 
                  11077*Power(t2,3) + 5823*Power(t2,4) - 
                  2102*Power(t2,5) + 573*Power(t2,6) - 51*Power(t2,7)) + 
               Power(s2,2)*(-279 + 1535*t2 - 3396*Power(t2,2) + 
                  5168*Power(t2,3) - 4500*Power(t2,4) + 
                  1780*Power(t2,5) - 508*Power(t2,6) + 
                  t1*(-113 + 749*t2 - 1449*Power(t2,2) + 
                     1890*Power(t2,3) - 1438*Power(t2,4) + 
                     87*Power(t2,5))) + 
               s2*(-111 + 2930*t2 - 6469*Power(t2,2) + 
                  3976*Power(t2,3) - 848*Power(t2,4) + 690*Power(t2,5) - 
                  314*Power(t2,6) + 146*Power(t2,7) + 
                  Power(t1,2)*
                   (231 - 914*t2 + 1798*Power(t2,2) - 
                     2554*Power(t2,3) + 1763*Power(t2,4) - 
                     94*Power(t2,5)) + 
                  t1*(409 - 4579*t2 + 10815*Power(t2,2) - 
                     13202*Power(t2,3) + 9933*Power(t2,4) - 
                     3309*Power(t2,5) + 333*Power(t2,6)))) + 
            Power(s1,2)*(-626 + 2898*t2 - 5002*Power(t2,2) + 
               4600*Power(t2,3) - 2782*Power(t2,4) + 890*Power(t2,5) + 
               96*Power(t2,6) - 96*Power(t2,7) + 22*Power(t2,8) + 
               Power(t1,3)*(27 + 69*t2 - 206*Power(t2,2) + 
                  75*Power(t2,3) + 24*Power(t2,4) + 18*Power(t2,5) + 
                  18*Power(t2,6)) - 
               Power(s2,3)*(-4 + 122*t2 - 462*Power(t2,2) + 
                  476*Power(t2,3) - 128*Power(t2,4) + Power(t2,5) + 
                  31*Power(t2,6)) - 
               Power(t1,2)*(622 - 3465*t2 + 9228*Power(t2,2) - 
                  12260*Power(t2,3) + 8449*Power(t2,4) - 
                  3132*Power(t2,5) + 497*Power(t2,6) + 6*Power(t2,7)) + 
               t1*(1181 - 5774*t2 + 12736*Power(t2,2) - 
                  15417*Power(t2,3) + 9947*Power(t2,4) - 
                  2909*Power(t2,5) + 275*Power(t2,6) - 51*Power(t2,7) + 
                  12*Power(t2,8)) + 
               Power(s2,2)*(-35 + 757*t2 - 2704*Power(t2,2) + 
                  3750*Power(t2,3) - 3108*Power(t2,4) + 
                  1848*Power(t2,5) - 564*Power(t2,6) + 111*Power(t2,7) + 
                  t1*(-5 + 279*t2 - 1340*Power(t2,2) + 
                     1895*Power(t2,3) - 1122*Power(t2,4) + 
                     416*Power(t2,5) - 26*Power(t2,6))) + 
               s2*(204 - 642*t2 - 2335*Power(t2,2) + 7563*Power(t2,3) - 
                  6532*Power(t2,4) + 1963*Power(t2,5) - 224*Power(t2,6) + 
                  27*Power(t2,7) - 24*Power(t2,8) + 
                  Power(t1,2)*
                   (169 - 1154*t2 + 2692*Power(t2,2) - 
                     2852*Power(t2,3) + 1637*Power(t2,4) - 
                     643*Power(t2,5) + 65*Power(t2,6)) + 
                  t1*(-305 + 318*t2 + 4465*Power(t2,2) - 
                     10340*Power(t2,3) + 9077*Power(t2,4) - 
                     4188*Power(t2,5) + 946*Power(t2,6) - 83*Power(t2,7)))\
)) + s*(Power(s1,9)*(s2 - t1)*
             (Power(s2,2) + Power(t1,2) - 2*s2*(7 + t1 - 7*t2) - 
               14*t1*(-1 + t2) + 6*Power(-1 + t2,2)) - 
            2*(-2 + t2)*Power(-1 + t2,3)*Power(t2,3) - 
            s1*(-1 + t2)*(-2 + (-145 + 34*s2)*t2 + 
               (647 - 217*s2 + 66*Power(s2,2) - 8*Power(s2,3))*
                Power(t2,2) + 
               4*(-232 + 21*s2 - 79*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,3) + 
               (510 + 424*s2 + 365*Power(s2,2) - 58*Power(s2,3))*
                Power(t2,4) - 
               (83 + 396*s2 + 178*Power(s2,2))*Power(t2,5) + 
               (-3 + 67*s2 + 77*Power(s2,2) + 4*Power(s2,3))*
                Power(t2,6) + (4 + 4*s2 - 14*Power(s2,2))*Power(t2,7) + 
               Power(t1,3)*(2 + 37*t2 - 63*Power(t2,2) + 6*Power(t2,3) + 
                  34*Power(t2,4) - 6*Power(t2,5)) + 
               Power(t1,2)*(-6 + (-219 + 34*s2)*t2 + 
                  (839 - 229*s2)*Power(t2,2) + 
                  (-1507 + 481*s2)*Power(t2,3) + 
                  (1357 - 435*s2)*Power(t2,4) + 
                  (-536 + 131*s2)*Power(t2,5) - 12*(-6 + s2)*Power(t2,6)) \
+ t1*(6 + (327 - 68*s2)*t2 + (-1347 + 310*s2 + 14*Power(s2,2))*
                   Power(t2,2) + 
                  (2119 + 116*s2 - 139*Power(s2,2))*Power(t2,3) + 
                  (-1619 - 769*s2 + 219*Power(s2,2))*Power(t2,4) + 
                  (590 + 538*s2 - 68*Power(s2,2))*Power(t2,5) + 
                  (-70 - 139*s2 + 4*Power(s2,2))*Power(t2,6) + 
                  6*(-1 + 2*s2)*Power(t2,7))) + 
            Power(s1,8)*(Power(t1,3)*(25 - 26*t2) + 8*Power(-1 + t2,2) + 
               t1*Power(-1 + t2,2)*(-1 + 19*t2) + 
               Power(s2,3)*(-31 + 32*t2) + 
               Power(t1,2)*(55 - 24*t2 - 31*Power(t2,2)) + 
               Power(s2,2)*(73 + t1*(87 - 90*t2) - 60*t2 - 
                  13*Power(t2,2)) + 
               s2*(-(Power(-1 + t2,2)*(13 + 21*t2)) + 
                  Power(t1,2)*(-81 + 84*t2) + 
                  4*t1*(-30 + 17*t2 + 13*Power(t2,2)))) + 
            Power(s1,7)*(Power(s2,3)*(-8 + 101*t2 - 108*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(91 - 5*t2 + 14*Power(t2,2)) - 
               Power(-1 + t2,2)*(-15 + 45*t2 + 14*Power(t2,2)) + 
               Power(t1,3)*(-22 - 25*t2 + 62*Power(t2,2)) + 
               2*Power(t1,2)*(95 - 267*t2 + 162*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(s2,2)*(136 - 571*t2 + 560*Power(t2,2) - 
                  125*Power(t2,3) + t1*(13 - 265*t2 + 297*Power(t2,2))) + 
               s2*(Power(t1,2)*(25 + 173*t2 - 243*Power(t2,2)) + 
                  Power(-1 + t2,2)*(85 + 75*t2 + 36*Power(t2,2)) + 
                  t1*(-339 + 1084*t2 - 803*Power(t2,2) + 58*Power(t2,3)))) \
+ Power(s1,6)*(Power(t1,3)*(2 + 61*t2 - 92*Power(t2,2) + 4*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (67 - 473*t2 + 3*Power(t2,2) + 6*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (57 - 57*t2 + 112*Power(t2,2) + 48*Power(t2,3)) + 
               Power(s2,3)*(5 + 51*t2 - 135*Power(t2,2) + 
                  104*Power(t2,3)) + 
               Power(t1,2)*(139 - 1263*t2 + 2101*Power(t2,2) - 
                  939*Power(t2,3) - 38*Power(t2,4)) + 
               Power(s2,2)*(54 - 659*t2 + 1566*Power(t2,2) - 
                  1341*Power(t2,3) + 380*Power(t2,4) - 
                  t1*(9 + 160*t2 - 419*Power(t2,2) + 325*Power(t2,3))) + 
               s2*(-(Power(-1 + t2,2)*
                     (73 + 456*t2 + 178*Power(t2,2) + 54*Power(t2,3))) + 
                  Power(t1,2)*
                   (-3 + 19*t2 - 119*Power(t2,2) + 178*Power(t2,3)) + 
                  t1*(-109 + 1825*t2 - 3632*Power(t2,2) + 
                     2165*Power(t2,3) - 249*Power(t2,4)))) + 
            Power(s1,5)*(Power(t1,3)*
                (28 - 149*t2 + 162*Power(t2,2) + 67*Power(t2,3) - 
                  103*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-162 + 324*t2 - 857*Power(t2,2) - 3*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s2,3)*(9 - 31*t2 - 123*Power(t2,2) + 
                  135*Power(t2,3) + 5*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-130 + 387*t2 - 105*Power(t2,2) + 180*Power(t2,3) + 
                  60*Power(t2,4)) + 
               Power(t1,2)*(23 - 447*t2 + 2829*Power(t2,2) - 
                  3937*Power(t2,3) + 1450*Power(t2,4) + 82*Power(t2,5)) + 
               Power(s2,2)*(-44 - 39*t2 + 1127*Power(t2,2) - 
                  2055*Power(t2,3) + 1495*Power(t2,4) - 
                  484*Power(t2,5) + 
                  t1*(-4 + 18*t2 + 495*Power(t2,2) - 636*Power(t2,3) + 
                     142*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (-65 + 307*t2 - 681*Power(t2,2) + 421*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  2*Power(-1 + t2,2)*
                   (-72 + 311*t2 + 389*Power(t2,2) + 127*Power(t2,3) + 
                     33*Power(t2,4)) + 
                  t1*(221 - 530*t2 - 2601*Power(t2,2) + 
                     5439*Power(t2,3) - 2840*Power(t2,4) + 311*Power(t2,5)\
))) + Power(s1,4)*(t1*Power(-1 + t2,2)*
                (-43 + 281*t2 - 259*Power(t2,2) + 550*Power(t2,3) + 
                  24*Power(t2,4) + 11*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (-116 - 94*t2 + 597*Power(t2,2) - 61*Power(t2,3) + 
                  178*Power(t2,4) + 32*Power(t2,5)) - 
               Power(s2,3)*(16 + 13*t2 - 109*Power(t2,2) - 
                  83*Power(t2,3) + 124*Power(t2,4) + 60*Power(t2,5)) + 
               Power(t1,3)*(-44 + 128*t2 + 32*Power(t2,2) - 
                  290*Power(t2,3) + 113*Power(t2,4) + 82*Power(t2,5)) + 
               Power(t1,2)*(251 - 704*t2 + 1001*Power(t2,2) - 
                  2861*Power(t2,3) + 3615*Power(t2,4) - 
                  1243*Power(t2,5) - 59*Power(t2,6)) + 
               Power(s2,2)*(105 - 141*t2 + 102*Power(t2,2) - 
                  1008*Power(t2,3) + 1466*Power(t2,4) - 
                  847*Power(t2,5) + 323*Power(t2,6) - 
                  t1*(-46 + 38*t2 + 135*Power(t2,2) + 449*Power(t2,3) - 
                     672*Power(t2,4) + 33*Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (48 - 95*t2 - 255*Power(t2,2) + 1067*Power(t2,3) - 
                     834*Power(t2,4) + 6*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (-326 + 9*t2 + 1304*Power(t2,2) + 459*Power(t2,3) + 
                     249*Power(t2,4) + 45*Power(t2,5)) + 
                  t1*(-555 + 1037*t2 + 211*Power(t2,2) + 
                     1456*Power(t2,3) - 3917*Power(t2,4) + 
                     1979*Power(t2,5) - 211*Power(t2,6)))) + 
            Power(s1,3)*(Power(t1,3)*
                (45 - 49*t2 - 168*Power(t2,2) + 256*Power(t2,3) + 
                  15*Power(t2,4) - 100*Power(t2,5) - 18*Power(t2,6)) - 
               t1*Power(-1 + t2,2)*
                (51 - 196*t2 + 111*Power(t2,2) + 234*Power(t2,3) - 
                  56*Power(t2,4) + 46*Power(t2,5) + 6*Power(t2,6)) - 
               Power(-1 + t2,2)*
                (-45 - 299*t2 + 355*Power(t2,2) + 208*Power(t2,3) + 
                  52*Power(t2,4) + 87*Power(t2,5) + 6*Power(t2,6)) + 
               Power(s2,3)*(-8 + 92*t2 - 111*Power(t2,2) - 
                  87*Power(t2,3) + 43*Power(t2,4) + 60*Power(t2,5) + 
                  30*Power(t2,6)) + 
               Power(t1,2)*(29 - 912*t2 + 2271*Power(t2,2) - 
                  1953*Power(t2,3) + 1500*Power(t2,4) - 
                  1453*Power(t2,5) + 506*Power(t2,6) + 12*Power(t2,7)) + 
               Power(s2,2)*(66 - 592*t2 + 1095*Power(t2,2) - 
                  846*Power(t2,3) + 846*Power(t2,4) - 705*Power(t2,5) + 
                  245*Power(t2,6) - 109*Power(t2,7) + 
                  t1*(14 - 245*t2 + 446*Power(t2,2) - 35*Power(t2,3) + 
                     60*Power(t2,4) - 313*Power(t2,5) + 16*Power(t2,6))) \
- s2*(-(Power(-1 + t2,2)*(-57 - 917*t2 + 865*Power(t2,2) + 
                       947*Power(t2,3) - 21*Power(t2,4) + 
                       151*Power(t2,5) + 12*Power(t2,6))) + 
                  Power(t1,2)*
                   (69 - 162*t2 - 17*Power(t2,2) + 195*Power(t2,3) + 
                     313*Power(t2,4) - 498*Power(t2,5) + 43*Power(t2,6)) \
+ t1*(10 - 1820*t2 + 4718*Power(t2,2) - 3635*Power(t2,3) + 
                     1635*Power(t2,4) - 1528*Power(t2,5) + 
                     699*Power(t2,6) - 79*Power(t2,7)))) - 
            Power(s1,2)*(Power(t1,3)*
                (35 - 25*t2 - 104*Power(t2,2) + 106*Power(t2,3) + 
                  53*Power(t2,4) - 58*Power(t2,5) - 12*Power(t2,6)) + 
               Power(s2,3)*t2*
                (-16 + 136*t2 - 225*Power(t2,2) + 54*Power(t2,3) + 
                  50*Power(t2,4) + 2*Power(t2,5) + 4*Power(t2,6)) - 
               Power(-1 + t2,2)*
                (143 - 517*t2 + 211*Power(t2,2) + 249*Power(t2,3) - 
                  58*Power(t2,4) + 54*Power(t2,5) + 14*Power(t2,6)) - 
               t1*Power(-1 + t2,2)*
                (-321 + 993*t2 - 1174*Power(t2,2) + 476*Power(t2,3) + 
                  160*Power(t2,4) - 137*Power(t2,5) + 24*Power(t2,6)) + 
               Power(t1,2)*(-213 + 997*t2 - 2767*Power(t2,2) + 
                  4184*Power(t2,3) - 3009*Power(t2,4) + 973*Power(t2,5) - 
                  225*Power(t2,6) + 60*Power(t2,7)) + 
               Power(s2,2)*t2*
                (132 - 869*t2 + 1591*Power(t2,2) - 1272*Power(t2,3) + 
                  697*Power(t2,4) - 317*Power(t2,5) + 52*Power(t2,6) - 
                  14*Power(t2,7) + 
                  t1*(28 - 352*t2 + 762*Power(t2,2) - 448*Power(t2,3) + 
                     31*Power(t2,4) - 40*Power(t2,5) + 4*Power(t2,6))) + 
               s2*(Power(-1 + t2,2)*
                   (34 - 240*t2 - 690*Power(t2,2) + 1037*Power(t2,3) + 
                     121*Power(t2,4) - 77*Power(t2,5) + 40*Power(t2,6)) + 
                  Power(t1,2)*
                   (34 - 332*t2 + 920*Power(t2,2) - 1059*Power(t2,3) + 
                     420*Power(t2,4) - 26*Power(t2,5) + 70*Power(t2,6) - 
                     12*Power(t2,7)) + 
                  t1*(-68 + 368*t2 + 1071*Power(t2,2) - 4345*Power(t2,3) + 
                     4514*Power(t2,4) - 1971*Power(t2,5) + 
                     533*Power(t2,6) - 114*Power(t2,7) + 12*Power(t2,8)))))\
))/(s*(-1 + s1)*s1*(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2))*
       (-1 + s2)*(-s + s2 - t1)*Power(-s + s1 - t2,2)*(1 - s + s1 - t2)*
       Power(-1 + s + t2,2)*(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2))) + 
    (8*(Power(s,8)*(Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
         Power(s,7)*(Power(t1,3)*(-8 - 7*s1 + 4*t2) + 
            Power(t1,2)*(3 + (-2 + s2)*t2 + s1*(-3 + 3*s2 + t2)) + 
            Power(t2,2)*(4 + 15*t2 - 9*Power(t2,2) + s2*(-3 + 2*t2) + 
               2*s1*(-5 + 2*s2 + 5*t2)) + 
            t1*t2*(s1*(-1 + 3*s2 - 8*t2) - s2*(3 + t2) + 
               3*(1 + t2 + Power(t2,2)))) + 
         (s1 - t2)*(-1 + t2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,
           2)*(-6 + 23*t2 - 2*s2*t2 - 17*Power(t2,2) + 
            5*s2*Power(t2,2) + Power(s1,3)*(-2 + 3*s2 - 3*t1 + 2*t2) + 
            t1*(6 - 11*t2 + 2*Power(t2,2)) - 
            Power(s1,2)*(1 + s2 + 2*t1 - 3*t2 + 2*s2*t2 - 5*t1*t2 + 
               2*Power(t2,2)) - 
            s1*(11 + t1 - 12*t2 - 6*t1*t2 + Power(t2,2) + 
               2*t1*Power(t2,2) + s2*(-2 + 4*t2 + Power(t2,2)))) + 
         Power(s,6)*(-(Power(t1,2)*
               (21 + 3*(-7 + s2)*t2 - 4*(-1 + s2)*Power(t2,2) + 
                 Power(t2,3) + 2*Power(s1,2)*(-8 + 9*s2 + 3*t2) + 
                 s1*(-1 + s2*(17 - 7*t2) + 15*t2 - 4*Power(t2,2)))) + 
            Power(t1,3)*(21*Power(s1,2) + s1*(43 - 24*t2) + 
               6*(4 - 5*t2 + Power(t2,2))) + 
            t1*(3 + 5*(-5 + 4*s2)*t2 - (19 + 16*s2)*Power(t2,2) + 
               (25 - 4*s2)*Power(t2,3) + 3*Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) - 4*t2 + 
                  25*Power(t2,2) - s2*(3 + 13*t2)) - 
               s1*(3 - 20*t2 + Power(s2,2)*t2 + 14*Power(t2,2) + 
                  25*Power(t2,3) + s2*(-3 + 5*t2 - 21*Power(t2,2)))) + 
            t2*(3 - 21*t2 + Power(s2,2)*t2 - 39*Power(t2,2) + 
               55*Power(t2,3) - 16*Power(t2,4) + 
               Power(s1,2)*(-11 + 3*Power(s2,2) + s2*(2 - 17*t2) + 
                  37*t2 - 20*Power(t2,2)) + 
               s2*(-3 + 20*t2 - 26*Power(t2,2) + 9*Power(t2,3)) + 
               s1*(2 + 43*t2 - 97*Power(t2,2) + 36*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + s2*(3 + 7*t2 + 5*Power(t2,2)))\
)) + Power(s,5)*(1 - 20*t2 + 16*s2*t2 + 41*Power(t2,2) - 
            40*s2*Power(t2,2) - 13*Power(s2,2)*Power(t2,2) + 
            64*Power(t2,3) + 116*s2*Power(t2,3) + 
            5*Power(s2,2)*Power(t2,3) - 126*Power(t2,4) - 
            70*s2*Power(t2,4) + 77*Power(t2,5) + 16*s2*Power(t2,5) - 
            14*Power(t2,6) + Power(t1,3)*
             (-30 + 78*t2 - 41*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,2)*(51 - (77 + 9*s2)*t2 + 
               (29 - 12*s2)*Power(t2,2) + (4 + 6*s2)*Power(t2,3) - 
               3*Power(t2,4)) + 
            t1*(-18 - 37*(-2 + s2)*t2 + 
               (-37 + 113*s2 + 2*Power(s2,2))*Power(t2,2) - 
               2*(63 + 19*s2)*Power(t2,3) + (58 - 7*s2)*Power(t2,4) + 
               Power(t2,5)) + 
            Power(s1,3)*(-4 + Power(s2,3) - 35*Power(t1,3) + 31*t2 - 
               51*Power(t2,2) + 20*Power(t2,3) + 
               5*Power(t1,2)*(-7 + 3*t2) - 
               Power(s2,2)*(15*t1 + 11*t2) + 
               t1*(-8 + 26*t2 - 40*Power(t2,2)) + 
               s2*(1 + 45*Power(t1,2) - 10*t2 + 28*Power(t2,2) + 
                  2*t1*(7 + 10*t2))) + 
            Power(s1,2)*(1 + 
               (37 + 8*s2 - 5*Power(s2,2) - 2*Power(s2,3))*t2 + 
               (-172 - s2 + 27*Power(s2,2))*Power(t2,2) + 
               (195 - 30*s2)*Power(t2,3) - 54*Power(t2,4) + 
               Power(t1,3)*(-95 + 60*t2) + 
               Power(t1,2)*(-40 + s2*(77 - 50*t2) + 89*t2 - 
                  20*Power(t2,2)) + 
               t1*(23 - 57*t2 - 14*Power(t2,2) + 70*Power(t2,3) + 
                  Power(s2,2)*(-13 + 16*t2) + 
                  s2*(-4 + 25*t2 - 65*Power(t2,2)))) - 
            s1*(Power(t1,3)*(95 - 136*t2 + 30*Power(t2,2)) - 
               Power(t1,2)*(52 + 50*t2 - 59*Power(t2,2) + 
                  10*Power(t2,3) + s2*(31 - 52*t2 + 2*Power(t2,2))) + 
               t1*(1 + 49*t2 - 198*Power(t2,2) + 69*Power(t2,3) + 
                  31*Power(t2,4) + Power(s2,2)*t2*(-13 + 5*t2) + 
                  s2*(13 + 64*t2 + 23*Power(t2,2) - 49*Power(t2,3))) - 
               t2*(-5 - 86*t2 + Power(s2,3)*t2 + 293*Power(t2,2) - 
                  224*Power(t2,3) + 48*Power(t2,4) + 
                  Power(s2,2)*(13 + 12*t2 - 16*Power(t2,2)) + 
                  s2*(1 - 110*t2 + 81*Power(t2,2) - 14*Power(t2,3))))) - 
         Power(s,4)*(5 - 39*t2 + 19*s2*t2 + 22*Power(t2,2) + 
            6*s2*Power(t2,2) - 44*Power(s2,2)*Power(t2,2) + 
            122*Power(t2,3) + 170*s2*Power(t2,3) + 
            60*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) - 
            209*Power(t2,4) - 211*s2*Power(t2,4) - 
            10*Power(s2,2)*Power(t2,4) + 128*Power(t2,5) + 
            84*s2*Power(t2,5) - 52*Power(t2,6) - 14*s2*Power(t2,6) + 
            6*Power(t2,7) - Power(t1,3)*
             (3 - 69*t2 + 87*Power(t2,2) - 24*Power(t2,3) + Power(t2,4)) \
+ Power(t1,2)*(39 - 3*(29 + 17*s2)*t2 + (54 + 32*s2)*Power(t2,2) + 
               (13 + 16*s2)*Power(t2,3) - (15 + 4*s2)*Power(t2,4) + 
               3*Power(t2,5)) + 
            t1*(-33 + 2*(37 + 8*s2)*t2 + 
               (-181 + 182*s2 + 10*Power(s2,2))*Power(t2,2) - 
               2*(15 + 134*s2 + 2*Power(s2,2))*Power(t2,3) + 
               (205 + 46*s2)*Power(t2,4) + (-55 + 7*s2)*Power(t2,5)) + 
            Power(s1,4)*(-8 + 4*Power(s2,3) - 35*Power(t1,3) + 
               20*Power(t1,2)*(-2 + t2) + 29*t2 - 31*Power(t2,2) + 
               10*Power(t2,3) - Power(s2,2)*(1 + 30*t1 + 14*t2) + 
               t1*(-18 + 44*t2 - 35*Power(t2,2)) + 
               s2*(5 + 60*Power(t1,2) - 18*t2 + 22*Power(t2,2) + 
                  2*t1*(13 + 5*t2))) - 
            Power(s1,3)*(5 + Power(t1,3)*(110 - 80*t2) - 81*t2 + 
               200*Power(t2,2) - 161*Power(t2,3) + 36*Power(t2,4) + 
               2*Power(s2,3)*(-2 + 5*t2) + 
               Power(t1,2)*(89 - 166*t2 + 40*Power(t2,2)) + 
               t1*(-25 + 6*t2 + 97*Power(t2,2) - 90*Power(t2,3)) + 
               Power(s2,2)*(-3 + 24*t2 - 46*Power(t2,2) - 
                  6*t1*(-8 + 9*t2)) + 
               s2*(12 - 25*t2 - 15*Power(t2,2) + 40*Power(t2,3) + 
                  2*Power(t1,2)*(-69 + 55*t2) + 
                  t1*(-18 + 5*t2 + 70*Power(t2,2)))) + 
            Power(s1,2)*(-7 + 82*t2 - 341*Power(t2,2) + 
               522*Power(t2,3) - 291*Power(t2,4) + 48*Power(t2,5) + 
               4*Power(s2,3)*t2*(-3 + 2*t2) + 
               Power(t1,3)*(-148 + 247*t2 - 60*Power(t2,2)) + 
               Power(t1,2)*(-19 + 335*t2 - 233*Power(t2,2) + 
                  30*Power(t2,3)) + 
               t1*(63 - 216*t2 + 277*Power(t2,2) + 14*Power(t2,3) - 
                  75*Power(t2,4)) + 
               Power(s2,2)*(t2*(25 + 74*t2 - 50*Power(t2,2)) - 
                  2*t1*(9 - 45*t2 + 16*Power(t2,2))) + 
               s2*(1 + 73*t2 - 235*Power(t2,2) + 96*Power(t2,3) + 
                  Power(t1,2)*(103 - 224*t2 + 48*Power(t2,2)) + 
                  t1*(-17 - 139*t2 - 106*Power(t2,2) + 107*Power(t2,3)))\
) + s1*(6 - 12*t2 - 167*Power(t2,2) - 
               2*Power(s2,3)*(-5 + t2)*Power(t2,2) + 467*Power(t2,3) - 
               470*Power(t2,4) + 211*Power(t2,5) - 28*Power(t2,6) + 
               Power(t1,3)*(-85 + 244*t2 - 151*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(t1,2)*(127 + 25*t2 - 261*Power(t2,2) + 
                  113*Power(t2,3) - 13*Power(t2,4)) + 
               t1*(-43 + 42*t2 + 293*Power(t2,2) - 481*Power(t2,3) + 
                  103*Power(t2,4) + 20*Power(t2,5)) + 
               Power(s2,2)*t2*
                (18 - 62*t2 - 43*Power(t2,2) + 18*Power(t2,3) + 
                  t1*(38 - 48*t2 + 8*Power(t2,2))) + 
               s2*(-1 + 44*t2 - 261*Power(t2,2) + 441*Power(t2,3) - 
                  185*Power(t2,4) + 32*Power(t2,5) + 
                  Power(t1,2)*
                   (17 - 103*t2 + 48*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(-16 - 238*t2 + 361*Power(t2,2) + 44*Power(t2,3) - 
                     54*Power(t2,4))))) + 
         Power(s,3)*(6 + t2 - 24*s2*t2 - 92*Power(t2,2) + 
            136*s2*Power(t2,2) - 52*Power(s2,2)*Power(t2,2) + 
            258*Power(t2,3) - 100*s2*Power(t2,3) + 
            195*Power(s2,2)*Power(t2,3) - 10*Power(s2,3)*Power(t2,3) - 
            313*Power(t2,4) - 205*s2*Power(t2,4) - 
            93*Power(s2,2)*Power(t2,4) + 6*Power(s2,3)*Power(t2,4) + 
            183*Power(t2,5) + 157*s2*Power(t2,5) + 
            9*Power(s2,2)*Power(t2,5) - 60*Power(t2,6) - 
            49*s2*Power(t2,6) + 18*Power(t2,7) + 6*s2*Power(t2,7) - 
            Power(t2,8) + Power(t1,3)*
             (30 - 33*t2 - 37*Power(t2,2) + 36*Power(t2,3) - 
               5*Power(t2,4)) - 
            Power(t1,2)*(30 + (-94 + 84*s2)*t2 + 
               (84 - 157*s2)*Power(t2,2) + (-63 + 46*s2)*Power(t2,3) + 
               (45 + 8*s2)*Power(t2,4) - (12 + s2)*Power(t2,5) + 
               Power(t2,6)) + 
            t1*(-6 + 4*(-20 + 29*s2)*t2 + 
               (-6 - 79*s2 + 18*Power(s2,2))*Power(t2,2) + 
               (265 - 374*s2 - 26*Power(s2,2))*Power(t2,3) + 
               (9 + 298*s2 + Power(s2,2))*Power(t2,4) - 
               (124 + 25*s2)*Power(t2,5) + (21 - 4*s2)*Power(t2,6)) + 
            Power(s1,5)*(-4 + 6*Power(s2,3) - 21*Power(t1,3) + 9*t2 - 
               7*Power(t2,2) + 2*Power(t2,3) - 
               3*Power(s2,2)*(1 + 10*t1 + 2*t2) + 
               5*Power(t1,2)*(-5 + 3*t2) + 
               t1*(-16 + 31*t2 - 16*Power(t2,2)) + 
               s2*(7 + 45*Power(t1,2) + t1*(24 - 5*t2) - 14*t2 + 
                  8*Power(t2,2))) - 
            Power(s1,4)*(Power(t1,3)*(70 - 60*t2) + 
               Power(s2,3)*(-11 + 18*t2) + 
               Power(s2,2)*(t1*(66 - 76*t2) + 2*(5 - 13*t2)*t2) + 
               Power(-1 + t2,2)*(7 - 32*t2 + 9*Power(t2,2)) + 
               2*Power(t1,2)*(41 - 72*t2 + 20*Power(t2,2)) + 
               t1*(6 - 66*t2 + 112*Power(t2,2) - 55*Power(t2,3)) + 
               s2*(12 - 8*t2 - 21*Power(t2,2) + 20*Power(t2,3) + 
                  Power(t1,2)*(-122 + 115*t2) + 
                  t1*(-47 + 70*t2 + 15*Power(t2,2)))) + 
            Power(s1,3)*(-15 + 115*t2 - 300*Power(t2,2) + 
               321*Power(t2,3) - 137*Power(t2,4) + 16*Power(t2,5) + 
               3*Power(s2,3)*(2 - 13*t2 + 6*Power(t2,2)) - 
               3*Power(t1,3)*(39 - 76*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-138 + 494*t2 - 307*Power(t2,2) + 
                  40*Power(t2,3)) + 
               t1*(47 - 84*t2 - 15*Power(t2,2) + 147*Power(t2,3) - 
                  69*Power(t2,4)) + 
               Power(s2,2)*(-6 + 28*t2 + 83*Power(t2,2) - 
                  42*Power(t2,3) + t1*(-50 + 191*t2 - 66*Power(t2,2))) \
+ s2*(-26 + 131*t2 - 182*Power(t2,2) + 45*Power(t2,3) + 6*Power(t2,4) + 
                  Power(t1,2)*(136 - 339*t2 + 92*Power(t2,2)) + 
                  t1*(55 - 255*t2 - 19*Power(t2,2) + 67*Power(t2,3)))) - 
            Power(s1,2)*(Power(s2,3)*t2*(26 - 51*t2 + 6*Power(t2,2)) + 
               Power(t1,3)*(85 - 279*t2 + 215*Power(t2,2) - 
                  24*Power(t2,3)) + 
               Power(t1,2)*(12 - 500*t2 + 785*Power(t2,2) - 
                  263*Power(t2,3) + 21*Power(t2,4)) + 
               t1*(-22 + 179*t2 - 433*Power(t2,2) + 314*Power(t2,3) + 
                  33*Power(t2,4) - 37*Power(t2,5)) + 
               2*(8 - 78*t2 + 237*Power(t2,2) - 335*Power(t2,3) + 
                  244*Power(t2,4) - 83*Power(t2,5) + 7*Power(t2,6)) + 
               Power(s2,2)*(t2*
                   (-119 + 113*t2 + 119*Power(t2,2) - 30*Power(t2,3)) \
+ t1*(10 - 136*t2 + 185*Power(t2,2) - 24*Power(t2,3))) + 
               s2*(9 - 115*t2 + 430*Power(t2,2) - 498*Power(t2,3) + 
                  162*Power(t2,4) - 22*Power(t2,5) + 
                  Power(t1,2)*
                   (-32 + 226*t2 - 225*Power(t2,2) + 18*Power(t2,3)) + 
                  t1*(-45 + 517*t2 - 519*Power(t2,2) - 
                     164*Power(t2,3) + 73*Power(t2,4)))) - 
            s1*(-16 + (14 - 49*s2 - 16*Power(s2,2))*t2 + 
               (235 + 193*s2 + 256*Power(s2,2) - 30*Power(s2,3))*
                Power(t2,2) + 
               (-591 - 584*s2 - 162*Power(s2,2) + 29*Power(s2,3))*
                Power(t2,3) - 
               8*(-71 - 63*s2 + 5*Power(s2,2))*Power(t2,4) + 
               (-294 - 159*s2 + 8*Power(s2,2))*Power(t2,5) + 
               (90 + 22*s2)*Power(t2,6) - 6*Power(t2,7) + 
               Power(t1,3)*(17 - 138*t2 + 200*Power(t2,2) - 
                  68*Power(t2,3) + 3*Power(t2,4)) + 
               Power(t1,2)*(-77 + 20*t2 + 453*Power(t2,2) - 
                  428*Power(t2,3) + 94*Power(t2,4) - 7*Power(t2,5) + 
                  s2*(-4 + 67*t2 - 104*Power(t2,2) + 10*Power(t2,3) + 
                     5*Power(t2,4))) + 
               t1*(58 - 206*t2 + 267*Power(t2,2) + 280*Power(t2,3) - 
                  383*Power(t2,4) + 50*Power(t2,5) + 7*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (-28 + 112*t2 - 75*Power(t2,2) + 4*Power(t2,3)) + 
                  s2*(12 + 264*t2 - 1072*Power(t2,2) + 667*Power(t2,3) + 
                     58*Power(t2,4) - 30*Power(t2,5))))) + 
         Power(s,2)*(4 - 36*t1 + 60*Power(t1,2) - 28*Power(t1,3) - 
            78*t2 + 52*s2*t2 + 255*t1*t2 - 112*s2*t1*t2 - 
            270*Power(t1,2)*t2 + 60*s2*Power(t1,2)*t2 + 
            93*Power(t1,3)*t2 + 250*Power(t2,2) - 203*s2*Power(t2,2) + 
            18*Power(s2,2)*Power(t2,2) - 488*t1*Power(t2,2) + 
            359*s2*t1*Power(t2,2) - 14*Power(s2,2)*t1*Power(t2,2) + 
            384*Power(t1,2)*Power(t2,2) - 
            204*s2*Power(t1,2)*Power(t2,2) - 
            76*Power(t1,3)*Power(t2,2) - 392*Power(t2,3) + 
            429*s2*Power(t2,3) - 217*Power(s2,2)*Power(t2,3) + 
            14*Power(s2,3)*Power(t2,3) + 60*t1*Power(t2,3) - 
            119*s2*t1*Power(t2,3) + 51*Power(s2,2)*t1*Power(t2,3) - 
            211*Power(t1,2)*Power(t2,3) + 
            170*s2*Power(t1,2)*Power(t2,3) + 9*Power(t1,3)*Power(t2,3) + 
            376*Power(t2,4) - 245*s2*Power(t2,4) + 
            278*Power(s2,2)*Power(t2,4) - 26*Power(s2,3)*Power(t2,4) + 
            296*t1*Power(t2,4) - 320*s2*t1*Power(t2,4) - 
            22*Power(s2,2)*t1*Power(t2,4) + 83*Power(t1,2)*Power(t2,4) - 
            33*s2*Power(t1,2)*Power(t2,4) + 3*Power(t1,3)*Power(t2,4) - 
            225*Power(t2,5) - 64*s2*Power(t2,5) - 
            60*Power(s2,2)*Power(t2,5) + 6*Power(s2,3)*Power(t2,5) - 
            70*t1*Power(t2,5) + 151*s2*t1*Power(t2,5) - 
            2*Power(s2,2)*t1*Power(t2,5) - 27*Power(t1,2)*Power(t2,5) - 
            s2*Power(t1,2)*Power(t2,5) + 83*Power(t2,6) + 
            44*s2*Power(t2,6) + 3*Power(s2,2)*Power(t2,6) - 
            19*t1*Power(t2,6) - 2*s2*t1*Power(t2,6) + 
            3*Power(t1,2)*Power(t2,6) - 21*Power(t2,7) - 
            14*s2*Power(t2,7) + 2*t1*Power(t2,7) - s2*t1*Power(t2,7) + 
            3*Power(t2,8) + s2*Power(t2,8) - 
            Power(s1,6)*(4*Power(s2,3) + 
               Power(s2,2)*(-3 - 15*t1 + t2) + 
               s2*(3 + 18*Power(t1,2) + t1*(11 - 7*t2) - 4*t2 + 
                  Power(t2,2)) - 
               t1*(5 + 7*Power(t1,2) + t1*(8 - 6*t2) - 8*t2 + 
                  3*Power(t2,2))) + 
            Power(s1,5)*(Power(t1,3)*(23 - 24*t2) - 
               Power(-1 + t2,2)*(-1 + 2*t2) + 
               2*Power(s2,3)*(-5 + 7*t2) + 
               Power(s2,2)*(9 + t1*(40 - 49*t2) - 17*t2 + 
                  3*Power(t2,2)) + 
               Power(t1,2)*(34 - 59*t2 + 20*Power(t2,2)) + 
               t1*(12 - 40*t2 + 41*Power(t2,2) - 13*Power(t2,3)) + 
               s2*(-1 + 8*t2 - 10*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t1,2)*(-53 + 59*t2) + 
                  t1*(-39 + 68*t2 - 19*Power(t2,2)))) + 
            Power(s1,4)*(Power(s2,3)*(-11 + 43*t2 - 16*Power(t2,2)) + 
               Power(-1 + t2,2)*(8 - 25*t2 + 15*Power(t2,2)) + 
               Power(t1,3)*(53 - 112*t2 + 30*Power(t2,2)) + 
               Power(t1,2)*(120 - 292*t2 + 173*Power(t2,2) - 
                  25*Power(t2,3)) + 
               t1*(23 - 94*t2 + 150*Power(t2,2) - 101*Power(t2,3) + 
                  22*Power(t2,4)) + 
               Power(s2,2)*(22 - 50*t2 + 6*Power(t2,2) - 
                  2*Power(t2,3) + t1*(56 - 173*t2 + 56*Power(t2,2))) - 
               s2*(-10 + 31*t2 - 22*Power(t2,2) + Power(t2,4) + 
                  Power(t1,2)*(96 - 238*t2 + 68*Power(t2,2)) + 
                  t1*(107 - 256*t2 + 112*Power(t2,2) - 11*Power(t2,3)))) \
+ Power(s1,3)*(Power(t1,3)*(31 - 142*t2 + 149*Power(t2,2) - 
                  16*Power(t2,3)) + 
               Power(s2,3)*(-6 + 51*t2 - 71*Power(t2,2) + 
                  4*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-29 + 93*t2 - 89*Power(t2,2) + 34*Power(t2,3)) + 
               Power(t1,2)*(122 - 564*t2 + 674*Power(t2,2) - 
                  207*Power(t2,3) + 15*Power(t2,4)) + 
               t1*(-22 + 19*t2 + 66*Power(t2,2) - 151*Power(t2,3) + 
                  106*Power(t2,4) - 18*Power(t2,5)) + 
               Power(s2,2)*(44 - 198*t2 + 147*Power(t2,2) + 
                  49*Power(t2,3) - 2*Power(t2,4) - 
                  6*t1*(-2 + 27*t2 - 40*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(-3 - 21*t2 + 113*Power(t2,2) - 125*Power(t2,3) + 
                  42*Power(t2,4) - 6*Power(t2,5) + 
                  Power(t1,2)*
                   (-16 + 205*t2 - 285*Power(t2,2) + 30*Power(t2,3)) + 
                  t1*(-117 + 529*t2 - 477*Power(t2,2) - 
                     27*Power(t2,3) + 12*Power(t2,4)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (26 - 95*t2 + 59*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(8 - 71*t2 + 144*Power(t2,2) - 
                  71*Power(t2,3) + 3*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (3 - 111*t2 + 194*Power(t2,2) - 114*Power(t2,3) + 
                  34*Power(t2,4)) + 
               Power(t1,2)*(27 - 430*t2 + 992*Power(t2,2) - 
                  674*Power(t2,3) + 114*Power(t2,4) - 5*Power(t2,5)) + 
               t1*(32 - 236*t2 + 267*Power(t2,2) - 13*Power(t2,3) - 
                  18*Power(t2,4) - 39*Power(t2,5) + 7*Power(t2,6)) + 
               Power(s2,2)*(2 - 209*t2 + 488*Power(t2,2) - 
                  200*Power(t2,3) - 60*Power(t2,4) + 3*Power(t2,5) + 
                  t1*(2 - 69*t2 + 254*Power(t2,2) - 163*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(22 - 14*t2 + 114*Power(t2,2) - 300*Power(t2,3) + 
                  247*Power(t2,4) - 78*Power(t2,5) + 9*Power(t2,6) + 
                  Power(t1,2)*
                   (21 - 11*t2 - 140*Power(t2,2) + 100*Power(t2,3) - 
                     2*Power(t2,4)) + 
                  t1*(-91 + 777*t2 - 1419*Power(t2,2) + 
                     567*Power(t2,3) + 135*Power(t2,4) - 17*Power(t2,5))\
)) - s1*(Power(s2,3)*Power(t2,2)*
                (34 - 81*t2 + 27*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,3)*(14 - 41*Power(t2,2) + 50*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-5 + 49*t2 - 171*Power(t2,2) + 157*Power(t2,3) - 
                  64*Power(t2,4) + 16*Power(t2,5)) - 
               Power(t1,2)*(39 - 81*t2 + 293*Power(t2,2) - 
                  543*Power(t2,3) + 262*Power(t2,4) - 30*Power(t2,5) + 
                  Power(t2,6)) + 
               t1*(30 - 620*Power(t2,2) + 805*Power(t2,3) - 
                  148*Power(t2,4) - 69*Power(t2,5) + Power(t2,6) + 
                  Power(t2,7)) + 
               Power(s2,2)*t2*
                (20 - 382*t2 + 590*Power(t2,2) - 154*Power(t2,3) - 
                  16*Power(t2,4) + Power(t2,5) - 
                  t1*(12 + 6*t2 - 126*Power(t2,2) + 58*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(12 - 61*t2 + 288*Power(t2,2) - 94*Power(t2,3) - 
                  287*Power(t2,4) + 193*Power(t2,5) - 56*Power(t2,6) + 
                  5*Power(t2,7) + 
                  Power(t1,2)*
                   (20 - 63*t2 + 19*Power(t2,2) - 16*Power(t2,3) - 
                     5*Power(t2,4) + Power(t2,5)) + 
                  t1*(-32 + 28*t2 + 789*Power(t2,2) - 1413*Power(t2,3) + 
                     466*Power(t2,4) + 51*Power(t2,5) - 7*Power(t2,6))))) \
+ s*(-8 + 24*t1 - 24*Power(t1,2) + 8*Power(t1,3) + 67*t2 - 16*s2*t2 - 
            183*t1*t2 + 32*s2*t1*t2 + 165*Power(t1,2)*t2 - 
            16*s2*Power(t1,2)*t2 - 49*Power(t1,3)*t2 - 210*Power(t2,2) + 
            103*s2*Power(t2,2) - 4*Power(s2,2)*Power(t2,2) + 
            496*t1*Power(t2,2) - 204*s2*t1*Power(t2,2) + 
            4*Power(s2,2)*t1*Power(t2,2) - 364*Power(t1,2)*Power(t2,2) + 
            101*s2*Power(t1,2)*Power(t2,2) + 78*Power(t1,3)*Power(t2,2) + 
            338*Power(t2,3) - 342*s2*Power(t2,3) + 
            89*Power(s2,2)*Power(t2,3) - 4*Power(s2,3)*Power(t2,3) - 
            476*t1*Power(t2,3) + 375*s2*t1*Power(t2,3) - 
            45*Power(s2,2)*t1*Power(t2,3) + 296*Power(t1,2)*Power(t2,3) - 
            149*s2*Power(t1,2)*Power(t2,3) - 42*Power(t1,3)*Power(t2,3) - 
            310*Power(t2,4) + 445*s2*Power(t2,4) - 
            225*Power(s2,2)*Power(t2,4) + 21*Power(s2,3)*Power(t2,4) + 
            45*t1*Power(t2,4) - 144*s2*t1*Power(t2,4) + 
            56*Power(s2,2)*t1*Power(t2,4) - 91*Power(t1,2)*Power(t2,4) + 
            68*s2*Power(t1,2)*Power(t2,4) + 7*Power(t1,3)*Power(t2,4) + 
            171*Power(t2,5) - 198*s2*Power(t2,5) + 
            154*Power(s2,2)*Power(t2,5) - 21*Power(s2,3)*Power(t2,5) + 
            129*t1*Power(t2,5) - 89*s2*t1*Power(t2,5) - 
            8*Power(s2,2)*t1*Power(t2,5) + 24*Power(t1,2)*Power(t2,5) - 
            10*s2*Power(t1,2)*Power(t2,5) - 66*Power(t2,6) + 
            8*s2*Power(t2,6) - 14*Power(s2,2)*Power(t2,6) + 
            2*Power(s2,3)*Power(t2,6) - 39*t1*Power(t2,6) + 
            28*s2*t1*Power(t2,6) - Power(s2,2)*t1*Power(t2,6) - 
            6*Power(t1,2)*Power(t2,6) + 24*Power(t2,7) + 
            2*s2*Power(t2,7) + 4*t1*Power(t2,7) + 2*s2*t1*Power(t2,7) - 
            6*Power(t2,8) - 2*s2*Power(t2,8) + 
            Power(s1,7)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) - 
            Power(s1,6)*(s2 - t1)*
             (-Power(-1 + t2,2) + Power(s2,2)*(-3 + 4*t2) + 
               Power(t1,2)*(-3 + 4*t2) + 
               t1*(-5 + 9*t2 - 4*Power(t2,2)) + 
               s2*(6 + t1*(6 - 8*t2) - 11*t2 + 5*Power(t2,2))) + 
            Power(s1,5)*(Power(s2,3)*(8 - 19*t2 + 5*Power(t2,2)) + 
               s2*(-(Power(-1 + t2,2)*(-5 + 4*t2)) + 
                  Power(t1,2)*(41 - 77*t2 + 18*Power(t2,2)) - 
                  4*t1*(-13 + 26*t2 - 17*Power(t2,2) + 4*Power(t2,3))) + 
               Power(s2,2)*(t1*(-33 + 68*t2 - 17*Power(t2,2)) + 
                  2*(-9 + 19*t2 - 15*Power(t2,2) + 5*Power(t2,3))) + 
               t1*(Power(-1 + t2,2)*(-13 + 12*t2) - 
                  2*Power(t1,2)*(8 - 14*t2 + 3*Power(t2,2)) + 
                  t1*(-34 + 66*t2 - 38*Power(t2,2) + 6*Power(t2,3)))) + 
            Power(s1,4)*(-8*Power(-1 + t2,4) - 
               t1*Power(-1 + t2,2)*(9 - 53*t2 + 28*Power(t2,2)) + 
               Power(s2,3)*(3 - 31*t2 + 40*Power(t2,2)) + 
               Power(t1,3)*(3 + 33*t2 - 52*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-41 + 178*t2 - 190*Power(t2,2) + 
                  57*Power(t2,3) - 4*Power(t2,4)) + 
               Power(s2,2)*(-33 + 123*t2 - 110*Power(t2,2) + 
                  30*Power(t2,3) - 10*Power(t2,4) + 
                  t1*(2 + 89*t2 - 135*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(13 - 33*t2 + 4*Power(t2,2)) - 
                  Power(t1,2)*
                   (8 + 91*t2 - 147*Power(t2,2) + 12*Power(t2,3)) + 
                  t1*(54 - 241*t2 + 240*Power(t2,2) - 67*Power(t2,3) + 
                     14*Power(t2,4)))) + 
            Power(s1,2)*(-2*Power(-1 + t2,3)*
                (-2 + 19*t2 - 26*Power(t2,2) + 17*Power(t2,3)) + 
               Power(t1,3)*(-8 - 12*t2 + 30*Power(t2,2) + 
                  6*Power(t2,3) - 7*Power(t2,4)) + 
               Power(s2,3)*t2*
                (-12 + 72*t2 - 92*Power(t2,2) + 19*Power(t2,3) + 
                  4*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (2 - 278*t2 + 150*Power(t2,2) - 31*Power(t2,3) + 
                  11*Power(t2,4)) + 
               Power(t1,2)*(14 + 28*t2 - 356*Power(t2,2) + 
                  487*Power(t2,3) - 185*Power(t2,4) + 12*Power(t2,5)) + 
               s2*(-(Power(-1 + t2,2)*
                     (-1 + 172*t2 - 53*Power(t2,2) + 17*Power(t2,3) + 
                       11*Power(t2,4))) + 
                  Power(t1,2)*
                   (-1 + 109*t2 - 148*Power(t2,2) - 11*Power(t2,3) + 
                     24*Power(t2,4)) + 
                  t1*t2*(-283 + 1034*t2 - 974*Power(t2,2) + 
                     173*Power(t2,3) + 49*Power(t2,4) + Power(t2,5))) - 
               Power(s2,2)*(4 - 175*t2 + 550*Power(t2,2) - 
                  483*Power(t2,3) + 94*Power(t2,4) + 9*Power(t2,5) + 
                  Power(t2,6) + 
                  t1*(-4 + 43*t2 + 50*Power(t2,2) - 183*Power(t2,3) + 
                     63*Power(t2,4) + 4*Power(t2,5)))) + 
            Power(s1,3)*(Power(-1 + t2,3)*
                (14 - 37*t2 + 26*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (-42 + 50*t2 - 63*Power(t2,2) + 26*Power(t2,3)) + 
               Power(s2,3)*(4 - 30*t2 + 66*Power(t2,2) - 
                  38*Power(t2,3) - 5*Power(t2,4)) - 
               Power(t1,3)*(-6 + 6*t2 + 30*Power(t2,2) - 
                  34*Power(t2,3) + Power(t2,4)) + 
               Power(t1,2)*(-66 + 307*t2 - 474*Power(t2,2) + 
                  269*Power(t2,3) - 37*Power(t2,4) + Power(t2,5)) + 
               Power(s2,2)*(-43 + 212*t2 - 313*Power(t2,2) + 
                  144*Power(t2,3) - 5*Power(t2,4) + 5*Power(t2,5) + 
                  t1*(-1 + 50*t2 - 163*Power(t2,2) + 120*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (25 - 47*t2 + 47*Power(t2,2) + 4*Power(t2,3)) + 
                  Power(t1,2)*
                   (-20 + 19*t2 + 94*Power(t2,2) - 105*Power(t2,3) + 
                     3*Power(t2,4)) - 
                  t1*(-111 + 468*t2 - 622*Power(t2,2) + 
                     244*Power(t2,3) + 15*Power(t2,4) + 6*Power(t2,5)))) \
+ s1*(Power(t1,3)*(11 + 2*t2 - 20*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(s2,3)*Power(t2,2)*
                (-12 + 66*t2 - 70*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(-1 + t2,3)*
                (29 - 55*t2 + 54*Power(t2,2) - 29*Power(t2,3) + 
                  22*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (69 - 140*t2 - 317*Power(t2,2) + 140*Power(t2,3) - 
                  12*Power(t2,4) + 2*Power(t2,5)) + 
               Power(t1,2)*(-51 + 134*t2 - 54*Power(t2,2) + 
                  85*Power(t2,3) - 163*Power(t2,4) + 51*Power(t2,5) - 
                  2*Power(t2,6)) + 
               Power(s2,2)*t2*
                (8 - 221*t2 + 596*Power(t2,2) - 429*Power(t2,3) + 
                  42*Power(t2,4) + 4*Power(t2,5) + 
                  t1*(-8 + 89*t2 - 58*Power(t2,2) - 68*Power(t2,3) + 
                     20*Power(t2,4) + Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (16 - 100*t2 + 60*Power(t2,2) + 69*Power(t2,3) - 
                     23*Power(t2,4) + 2*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (16 - 72*t2 + 331*Power(t2,2) - 25*Power(t2,3) + 
                     8*Power(t2,5)) - 
                  t1*(32 - 204*t2 + 203*Power(t2,2) + 476*Power(t2,3) - 
                     630*Power(t2,4) + 104*Power(t2,5) + 19*Power(t2,6))))\
))*B1(s,1 - s + s1 - t2,s1))/
     (s*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*(2*Power(-1 + s,3)*s*(1 - 3*s + Power(s,2))*Power(t2,2)*
          (-2 + s + t2)*Power(-1 + s + t2,4) - 
         (-1 + s)*s*s1*t2*Power(-1 + s + t2,3)*
          (8 + (66 - 16*s2 + 4*t1)*t2 + 
            2*(-60 + 11*s2 - 3*t1)*Power(t2,2) + 
            (50 - 8*s2 + 4*t1)*Power(t2,3) - 4*Power(t2,4) + 
            2*Power(s,6)*t2*(2 - t1 + t2) + 
            2*Power(s,5)*(-2 + (-17 + 5*s2 + 4*t1)*t2 - 
               (s2 + t1)*Power(t2,2) + Power(t2,3)) + 
            Power(s,4)*(28 + (134 - 66*s2 - 3*t1)*t2 + 
               (-57 + 26*s2 + 2*t1)*Power(t2,2) - 2*(1 + s2)*Power(t2,3)\
) + Power(s,3)*(-72 + (-325 + 166*s2 - 29*t1)*t2 + 
               (247 - 101*s2 + 16*t1)*Power(t2,2) + 
               2*(-15 + 7*s2 - 2*t1)*Power(t2,3)) + 
            Power(s,2)*(84 + (452 - 190*s2 + 49*t1)*t2 + 
               (-497 + 160*s2 - 36*t1)*Power(t2,2) + 
               (-34*s2 + 12*(9 + t1))*Power(t2,3) - 4*Power(t2,4)) + 
            s*(-44 + 3*(-99 + 32*s2 - 9*t1)*t2 + 
               (445 - 105*s2 + 26*t1)*Power(t2,2) + 
               2*(-74 + 15*s2 - 6*t1)*Power(t2,3) + 8*Power(t2,4))) + 
         Power(s1,10)*(s2 - t1)*
          (s*(5*Power(s2,2) + 5*Power(t1,2) - 2*s2*(9 + 5*t1 - 9*t2) - 
               18*t1*(-1 + t2) + 6*Power(-1 + t2,2))*(-1 + t2) - 
            (s2 - t1)*Power(-1 + t2,2)*(-2 + 3*s2 - 3*t1 + 2*t2) + 
            2*Power(s,2)*(Power(s2,2) + t1 + Power(t1,2) - 
               3*Power(-1 + t2,2) - t1*t2 + s2*(-1 - 2*t1 + t2))) - 
         Power(s1,2)*Power(-1 + s + t2,2)*
          (6*Power(s,9)*Power(t2,2) - 
            Power(-1 + t1 + t2 - s2*t2,2)*
             (-4 + 19*t2 + 3*(-5 + s2)*Power(t2,2) + 
               t1*(4 - 9*t2 + 2*Power(t2,2))) + 
            2*Power(s,8)*(Power(t1,3) + t1*t2*(-2 + 9*t2) - 
               t2*(-4 + (23 + 10*s2)*t2 + 4*Power(t2,2))) + 
            Power(s,6)*(16 + (380 - 158*s2)*t2 + 
               (-606 + 129*s2 - 122*Power(s2,2))*Power(t2,2) + 
               (277 + 44*s2 + 28*Power(s2,2))*Power(t2,3) + 
               39*Power(t2,4) - 16*Power(t2,5) + 
               Power(t1,3)*(38 - 25*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-36 + (26 - 4*s2)*t2 + 
                  (7 + 6*s2)*Power(t2,2) - 4*Power(t2,3)) + 
               t1*(6 + (-66 + 34*s2)*t2 + 
                  (54 + 40*s2 - 2*Power(s2,2))*Power(t2,2) - 
                  (75 + 16*s2)*Power(t2,3) + 18*Power(t2,4))) + 
            Power(s,5)*(-48 + 2*(-544 + 245*s2)*t2 + 
               (1813 - 1350*s2 + 438*Power(s2,2))*Power(t2,2) - 
               (1043 - 548*s2 + 230*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 
               (189 - 43*s2 + 20*Power(s2,2))*Power(t2,4) + 
               8*(3 + s2)*Power(t2,5) - 2*Power(t2,6) + 
               Power(t1,3)*(-46 + 56*t2 - 8*Power(t2,2)) + 
               Power(t1,2)*(78 - (55 + 18*s2)*t2 + 
                  (50 - 27*s2)*Power(t2,2) + 4*(1 + s2)*Power(t2,3)) + 
               2*t1*(-15 - 26*(-1 + s2)*t2 + 
                  (154 - 107*s2 + 13*Power(s2,2))*Power(t2,2) + 
                  (-88 + 52*s2)*Power(t2,3) + (2 - 8*s2)*Power(t2,4) + 
                  Power(t2,5))) + 
            Power(s,2)*(30 + (703 - 154*s2)*t2 + 
               (-1577 + 650*s2 - 78*Power(s2,2))*Power(t2,2) + 
               (998 - 908*s2 + 5*Power(s2,2) + 32*Power(s2,3))*
                Power(t2,3) - 
               (84 - 451*s2 + 46*Power(s2,2) + 19*Power(s2,3))*
                Power(t2,4) + 
               (-74 - 52*s2 + 20*Power(s2,2))*Power(t2,5) - 
               2*(-2 + s2)*Power(t2,6) + 
               Power(t1,3)*(-46 + 61*t2 - 10*Power(t2,2)) + 
               Power(t1,2)*(84 + 4*(-74 + 27*s2)*t2 + 
                  (61 - 108*s2)*Power(t2,2) + 4*(5 + 2*s2)*Power(t2,3)) \
+ t1*(-42 + (414 - 182*s2)*t2 + 
                  (-644 + 676*s2 - 98*Power(s2,2))*Power(t2,2) + 
                  (321 - 284*s2 + 72*Power(s2,2))*Power(t2,3) + 
                  (-38 + 32*s2)*Power(t2,4) + (4 - 12*s2)*Power(t2,5))) \
+ Power(s,4)*(70 - 368*(-5 + 2*s2)*t2 + 
               (-3600 + 2866*s2 - 684*Power(s2,2))*Power(t2,2) + 
               2*(1164 - 1071*s2 + 284*Power(s2,2) + 8*Power(s2,3))*
                Power(t2,3) - 
               (588 - 376*s2 + 127*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,4) + 4*(10 - 5*s2 + Power(s2,2))*Power(t2,5) + 
               2*(2 + s2)*Power(t2,6) + 
               5*Power(t1,3)*(2 - 9*t2 + 2*Power(t2,2)) + 
               Power(t1,2)*(-60 + (-5 + 80*s2)*t2 + 
                  9*(-17 + 3*s2)*Power(t2,2) - 
                  4*(-5 + 3*s2)*Power(t2,3)) + 
               t1*(48 - 7*(-15 + 4*s2)*t2 + 
                  (-800 + 558*s2 - 88*Power(s2,2))*Power(t2,2) + 
                  (675 - 250*s2 + 9*Power(s2,2))*Power(t2,3) + 
                  2*(-73 + 30*s2 + Power(s2,2))*Power(t2,4) - 
                  4*(-3 + s2)*Power(t2,5))) - 
            Power(s,3)*(56 + (1687 - 540*s2)*t2 + 
               (-3719 + 2447*s2 - 460*Power(s2,2))*Power(t2,2) + 
               (2694 - 2630*s2 + 477*Power(s2,2) + 36*Power(s2,3))*
                Power(t2,3) - 
               (691 - 812*s2 + 202*Power(s2,2) + 11*Power(s2,3))*
                Power(t2,4) + 
               (42 - 52*s2 + 22*Power(s2,2))*Power(t2,5) + 
               2*(-2 + s2)*Power(t2,6) + Power(t1,3)*(-38 + 20*t2) - 
               2*Power(t1,2)*
                (-15 + (95 - 65*s2)*t2 + 7*(8 + 3*s2)*Power(t2,2) + 
                  4*(-5 + s2)*Power(t2,3)) - 
               2*t1*(-6 + 3*(-56 + 27*s2)*t2 + 
                  (448 - 400*s2 + 66*Power(s2,2))*Power(t2,2) + 
                  (-351 + 160*s2 - 21*Power(s2,2))*Power(t2,3) - 
                  2*(-44 + 18*s2 + Power(s2,2))*Power(t2,4) + 
                  6*(-2 + s2)*Power(t2,5))) + 
            s*(-14 - (49 + 10*s2)*t2 + 
               (48 + 139*s2 - 30*Power(s2,2))*Power(t2,2) + 
               (223 - 220*s2 + 131*Power(s2,2) - 10*Power(s2,3))*
                Power(t2,3) + 
               (-334 + 109*s2 - 70*Power(s2,2) + 13*Power(s2,3))*
                Power(t2,4) - 
               2*(-68 + 10*s2 + Power(s2,2))*Power(t2,5) + 
               2*(-5 + s2)*Power(t2,6) + 
               Power(t1,3)*(22 - 40*t2 + 8*Power(t2,2)) + 
               Power(t1,2)*(-54 + (189 - 46*s2)*t2 + 
                  (-110 + 81*s2)*Power(t2,2) + (4 - 12*s2)*Power(t2,3)) \
+ 2*t1*(21 + (-127 + 44*s2)*t2 + 
                  (169 - 163*s2 + 17*Power(s2,2))*Power(t2,2) + 
                  (-60 + 92*s2 - 27*Power(s2,2))*Power(t2,3) + 
                  2*(-3 - 2*s2 + Power(s2,2))*Power(t2,4) + 
                  (3 + 2*s2)*Power(t2,5))) + 
            2*Power(s,7)*(-1 + 10*(-4 + s2)*t2 + 
               (93 + 41*s2 + 6*Power(s2,2))*Power(t2,2) - 
               (8 + 13*s2)*Power(t2,3) - 14*Power(t2,4) + 
               Power(t1,3)*(-7 + 2*t2) + 
               Power(t1,2)*(3 + (-2 + s2)*t2 - 2*Power(t2,2)) + 
               t1*t2*(13 - 39*t2 + 17*Power(t2,2) - s2*(3 + 2*t2)))) + 
         Power(s1,9)*(2*Power(s,4)*Power(-1 + t2,2) - 
            Power(s,3)*(10*Power(s2,3) - 16*Power(t1,3) - 
               4*Power(s2,2)*(2 + 9*t1 - t2) + 
               2*Power(-1 + t2,2)*(5 + t2) + 
               2*Power(t1,2)*(-9 + 7*t2) + 
               s2*(-25 + 42*Power(t1,2) + t1*(26 - 18*t2) + 66*t2 - 
                  41*Power(t2,2)) + t1*(21 - 58*t2 + 37*Power(t2,2))) + 
            (s2 - t1)*Power(-1 + t2,2)*
             (-4*Power(-1 + t2,2) + 2*Power(s2,2)*(5 + 4*t2) + 
               Power(t1,2)*(13 + 5*t2) - t1*(-3 + t2 + 2*Power(t2,2)) + 
               s2*(1 - 7*t2 + 6*Power(t2,2) - t1*(23 + 13*t2))) + 
            s*(-1 + t2)*(Power(t1,3)*(27 - 8*t2) + 8*Power(-1 + t2,3) + 
               6*t1*Power(-1 + t2,2)*(1 + 2*t2) + 
               Power(s2,3)*(-39 + 20*t2) + 
               Power(t1,2)*(77 - 46*t2 - 31*Power(t2,2)) + 
               Power(s2,2)*(95 + t1*(105 - 48*t2) - 82*t2 - 
                  13*Power(t2,2)) + 
               s2*(-2*Power(-1 + t2,2)*(5 + 4*t2) + 
                  Power(t1,2)*(-93 + 36*t2) + 
                  4*t1*(-43 + 32*t2 + 11*Power(t2,2)))) - 
            Power(s,2)*(Power(t1,3)*(30 - 43*t2) + 
               4*(-3 + t2)*Power(-1 + t2,3) + 
               Power(s2,3)*(-15 + 28*t2) - 
               t1*Power(-1 + t2,2)*(-103 + 29*t2) + 
               Power(s2,2)*(82 + t1*(60 - 99*t2) - 189*t2 + 
                  107*Power(t2,2)) + 
               Power(t1,2)*(106 - 237*t2 + 131*Power(t2,2)) + 
               s2*(Power(-1 + t2,2)*(-95 + 21*t2) + 
                  3*Power(t1,2)*(-25 + 38*t2) - 
                  2*t1*(94 - 213*t2 + 119*Power(t2,2))))) + 
         Power(s1,8)*(-2*Power(s,5)*(7 - 16*t2 + 9*Power(t2,2)) + 
            Power(s,4)*(34 + 20*Power(s2,3) - 56*Power(t1,3) - 93*t2 + 
               58*Power(t2,2) + Power(t2,3) - 
               2*Power(s2,2)*(6 + 45*t1 + 5*t2) + 
               6*Power(t1,2)*(-11 + 7*t2) + 
               s2*(-51 + 126*Power(t1,2) + t1*(70 - 24*t2) + 168*t2 - 
                  121*Power(t2,2)) + 2*t1*(9 - 52*t2 + 45*Power(t2,2))) \
+ Power(s,3)*(Power(t1,3)*(93 - 161*t2) + Power(s2,3)*(-25 + 66*t2) + 
               Power(-1 + t2,2)*(103 - 60*t2 + 27*Power(t2,2)) + 
               Power(s2,2)*(166 + t1*(122 - 272*t2) - 457*t2 + 
                  272*Power(t2,2)) + 
               Power(t1,2)*(270 - 701*t2 + 412*Power(t2,2)) + 
               t1*(274 - 774*t2 + 573*Power(t2,2) - 73*Power(t2,3)) + 
               s2*(-196 + 530*t2 - 319*Power(t2,2) - 15*Power(t2,3) + 
                  Power(t1,2)*(-190 + 367*t2) - 
                  2*t1*(207 - 557*t2 + 331*Power(t2,2)))) - 
            s*(-1 + t2)*(18*Power(-1 + t2,3)*(2 + t2) + 
               Power(t1,3)*(23 + 12*t2 - 20*Power(t2,2)) + 
               5*Power(s2,3)*(-9 - 5*t2 + 11*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-65 + 32*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(53 + 132*t2 - 162*Power(t2,2) - 
                  23*Power(t2,3)) + 
               Power(s2,2)*(81 + 124*t2 - 230*Power(t2,2) + 
                  25*Power(t2,3) + t1*(98 + 92*t2 - 145*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(89 - 78*t2 + 10*Power(t2,2)) + 
                  Power(t1,2)*(-76 - 79*t2 + 110*Power(t2,2)) - 
                  2*t1*(66 + 131*t2 - 199*Power(t2,2) + 2*Power(t2,3)))) \
- Power(-1 + t2,2)*(2*Power(-1 + t2,3) + 
               t1*Power(-1 + t2,2)*(11 + 4*t2) - 
               Power(t1,2)*(6 - 7*t2 + Power(t2,2)) + 
               6*Power(s2,3)*(2 + 4*t2 + Power(t2,2)) - 
               Power(t1,3)*(19 + 21*t2 + 2*Power(t2,2)) - 
               Power(s2,2)*(-6 + 11*t2 + Power(t2,2) - 6*Power(t2,3) + 
                  t1*(46 + 63*t2 + 17*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(7 + 8*t2)) + 
                  Power(t1,2)*(53 + 60*t2 + 13*Power(t2,2)) - 
                  2*t1*(1 - 5*t2 + 2*Power(t2,2) + 2*Power(t2,3)))) - 
            Power(s,2)*(-(Power(-1 + t2,3)*
                  (5 - 57*t2 + 8*Power(t2,2))) + 
               Power(t1,3)*(-53 + 53*t2 + 27*Power(t2,2)) + 
               Power(s2,3)*(70 - 160*t2 + 63*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-174 - 222*t2 + 79*Power(t2,2)) + 
               Power(t1,2)*(-175 + 234*t2 + 178*Power(t2,2) - 
                  237*Power(t2,3)) - 
               Power(s2,2)*(209 - 432*t2 + 116*Power(t2,2) + 
                  107*Power(t2,3) + t1*(220 - 427*t2 + 126*Power(t2,2))) \
+ s2*(Power(-1 + t2,2)*(239 + 78*t2) + 
                  Power(t1,2)*(203 - 320*t2 + 36*Power(t2,2)) + 
                  t1*(402 - 720*t2 - 8*Power(t2,2) + 326*Power(t2,3))))) \
+ Power(s1,7)*(2*Power(s,6)*(18 - 50*t2 + 33*Power(t2,2)) + 
            Power(s,5)*(-76 - 20*Power(s2,3) + 112*Power(t1,3) + 
               Power(t1,2)*(130 - 70*t2) + 263*t2 - 238*Power(t2,2) + 
               39*Power(t2,3) + 8*Power(s2,2)*(1 + 15*t1 + 5*t2) + 
               t1*(20 + 66*t2 - 97*Power(t2,2)) - 
               s2*(-43 + 210*Power(t1,2) + 212*t2 - 180*Power(t2,2) + 
                  10*t1*(10 + t2))) + 
            Power(s,4)*(-27 + Power(s2,3)*(35 - 84*t2) + 235*t2 - 
               316*Power(t2,2) + 180*Power(t2,3) - 72*Power(t2,4) + 
               Power(t1,3)*(-208 + 343*t2) + 
               Power(t1,2)*(-406 + 1206*t2 - 729*Power(t2,2)) + 
               t1*(-326 + 1246*t2 - 1135*Power(t2,2) + 
                  170*Power(t2,3)) + 
               Power(s2,2)*(-194 + 653*t2 - 410*Power(t2,2) + 
                  4*t1*(-47 + 100*t2)) + 
               s2*(183 + Power(t1,2)*(350 - 648*t2) - 630*t2 + 
                  353*Power(t2,2) + 139*Power(t2,3) + 
                  2*t1*(241 - 799*t2 + 498*Power(t2,2)))) + 
            Power(s,3)*(Power(s2,3)*(53 - 192*t2 + 123*Power(t2,2)) + 
               Power(t1,3)*(15 - 115*t2 + 162*Power(t2,2)) - 
               Power(-1 + t2,2)*
                (200 + 98*t2 - 159*Power(t2,2) + 53*Power(t2,3)) - 
               2*Power(t1,2)*
                (11 + 124*t2 - 466*Power(t2,2) + 331*Power(t2,3)) + 
               t1*(77 - 516*t2 + 1537*Power(t2,2) - 1359*Power(t2,3) + 
                  261*Power(t2,4)) + 
               Power(s2,2)*(-29 + 81*t2 + 287*Power(t2,2) - 
                  339*Power(t2,3) - 
                  2*t1*(78 - 241*t2 + 116*Power(t2,2))) - 
               s2*(22 + 96*t2 + 95*Power(t2,2) - 91*Power(t2,3) - 
                  122*Power(t2,4) + 
                  Power(t1,2)*(-106 + 211*t2 + 35*Power(t2,2)) + 
                  t1*(-188 + 294*t2 + 708*Power(t2,2) - 814*Power(t2,3))\
)) + Power(-1 + t2,2)*(Power(-1 + t2,3)*(5 + 2*t2) + 
               t1*Power(-1 + t2,2)*(31 + 9*t2) + 
               6*Power(s2,3)*(1 + 4*t2 + 2*Power(t2,2)) - 
               3*Power(t1,3)*(3 + 7*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(-43 + 45*t2 + 2*Power(t2,2) - 
                  4*Power(t2,3)) + 
               2*s2*(-(Power(-1 + t2,2)*(9 + 9*t2 + 2*Power(t2,2))) - 
                  t1*(-18 + 17*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (20 + 19*t2 + 22*Power(t2,2) + 2*Power(t2,3))) - 
               Power(s2,2)*(4 + 14*t2 - 5*Power(t2,2) - 
                  11*Power(t2,3) - 2*Power(t2,4) + 
                  t1*(34 + 50*t2 + 35*Power(t2,2) + 7*Power(t2,3)))) + 
            s*(-1 + t2)*(Power(-1 + t2,3)*
                (41 + 105*t2 + 18*Power(t2,2)) - 
               Power(t1,3)*(49 - 52*t2 + 24*Power(t2,2) + 
                  8*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-65 - 114*t2 + 44*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,3)*(13 - 37*t2 + 18*Power(t2,2) + 
                  35*Power(t2,3)) + 
               Power(t1,2)*(-36 + 129*t2 + 94*Power(t2,2) - 
                  179*Power(t2,3) - 8*Power(t2,4)) - 
               Power(s2,2)*(9 - 201*t2 + 112*Power(t2,2) + 
                  91*Power(t2,3) - 11*Power(t2,4) + 
                  3*t1*(40 - 60*t2 + 11*Power(t2,2) + 38*Power(t2,3))) \
+ s2*(Power(-1 + t2,2)*(165 + 32*t2 - 92*Power(t2,2) + 
                     22*Power(t2,3)) + 
                  Power(t1,2)*
                   (160 - 207*t2 + 51*Power(t2,2) + 83*Power(t2,3)) - 
                  2*t1*(-4 + 101*t2 + 72*Power(t2,2) - 
                     179*Power(t2,3) + 10*Power(t2,4)))) + 
            Power(s,2)*(Power(t1,3)*
                (41 - 124*t2 + 113*Power(t2,2) - 17*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (135 - 196*t2 - 69*Power(t2,2) + 8*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-11 - 616*t2 - 104*Power(t2,2) + 86*Power(t2,3)) + 
               Power(s2,3)*(-61 + 182*t2 - 296*Power(t2,2) + 
                  162*Power(t2,3)) + 
               Power(t1,2)*(257 - 888*t2 + 1115*Power(t2,2) - 
                  329*Power(t2,3) - 155*Power(t2,4)) + 
               Power(s2,2)*(162 - 592*t2 + 939*Power(t2,2) - 
                  485*Power(t2,3) - 24*Power(t2,4) + 
                  t1*(138 - 536*t2 + 876*Power(t2,2) - 439*Power(t2,3))) \
+ s2*(Power(-1 + t2,2)*(123 + 693*t2 - 272*Power(t2,2) + 
                     101*Power(t2,3)) + 
                  Power(t1,2)*
                   (-126 + 502*t2 - 717*Power(t2,2) + 302*Power(t2,3)) + 
                  2*t1*(-251 + 916*t2 - 1306*Power(t2,2) + 
                     603*Power(t2,3) + 38*Power(t2,4))))) + 
         Power(s1,6)*(-2*Power(s,7)*(22 - 80*t2 + 65*Power(t2,2)) + 
            Power(s,6)*(124 + 10*Power(s2,3) - 140*Power(t1,3) - 
               489*t2 + 534*Power(t2,2) - 138*Power(t2,3) + 
               10*Power(t1,2)*(-15 + 7*t2) - 
               2*Power(s2,2)*(1 + 45*t1 + 25*t2) + 
               t1*(-50 + 40*t2 + 8*Power(t2,2)) + 
               s2*(17 + 210*Power(t1,2) + 68*t2 - 100*Power(t2,2) + 
                  20*t1*(4 + 3*t2))) + 
            Power(s,5)*(-131 + 396*t2 - 333*Power(t2,2) + 
               21*Power(t2,3) + 104*Power(t2,4) + 
               Power(s2,3)*(-30 + 61*t2) - 
               5*Power(t1,3)*(-69 + 91*t2) + 
               2*Power(t1,2)*(204 - 645*t2 + 395*Power(t2,2)) + 
               Power(s2,2)*(116 + t1*(207 - 335*t2) - 549*t2 + 
                  418*Power(t2,2)) - 
               2*t1*(-106 + 606*t2 - 729*Power(t2,2) + 
                  179*Power(t2,3)) + 
               s2*(-196 + 740*t2 - 521*Power(t2,2) - 122*Power(t2,3) + 
                  25*Power(t1,2)*(-19 + 27*t2) - 
                  2*t1*(148 - 629*t2 + 430*Power(t2,2)))) + 
            Power(-1 + t2,3)*
             (-(Power(-1 + t2,2)*(14 + 5*t2)) + 
               5*Power(t1,3)*(-1 + 6*t2) + 
               5*Power(t1,2)*(-18 - 15*t2 + 4*Power(t2,2)) + 
               Power(s2,3)*(1 + 9*t2 + 9*Power(t2,2) + Power(t2,3)) + 
               t1*(82 - 47*t2 - 37*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(-44 - 16*t2 + 49*Power(t2,2) + 11*Power(t2,3) - 
                  5*Power(t1,2)*(-7 + 9*t2 + 4*Power(t2,2)) + 
                  t1*(64 + 208*t2 + 22*Power(t2,2) - 4*Power(t2,3))) + 
               Power(s2,2)*(-8 - 76*t2 - 54*Power(t2,2) - 
                  7*Power(t2,3) + 
                  t1*(-21 - 11*t2 + 15*Power(t2,2) + 2*Power(t2,3)))) - 
            Power(s,4)*(239 - 74*t2 - 178*Power(t2,2) - 
               221*Power(t2,3) + 396*Power(t2,4) - 162*Power(t2,5) + 
               Power(s2,3)*(21 - 134*t2 + 170*Power(t2,2)) + 
               Power(t1,3)*(223 - 565*t2 + 335*Power(t2,2)) + 
               Power(t1,2)*(296 - 1252*t2 + 1865*Power(t2,2) - 
                  980*Power(t2,3)) + 
               t1*(352 - 1704*t2 + 3080*Power(t2,2) - 
                  2411*Power(t2,3) + 550*Power(t2,4)) + 
               Power(s2,2)*(139 - 578*t2 + 935*Power(t2,2) - 
                  603*Power(t2,3) - 15*t1*(-3 - 8*t2 + 21*Power(t2,2))) \
+ s2*(-583 + 1726*t2 - 1562*Power(t2,2) + 314*Power(t2,3) + 
                  238*Power(t2,4) + 
                  Power(t1,2)*(-213 + 398*t2 - 85*Power(t2,2)) + 
                  2*t1*(-98 + 365*t2 - 674*Power(t2,2) + 
                     496*Power(t2,3)))) - 
            Power(s,3)*(2*Power(t1,3)*
                (23 - 50*t2 - 6*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s2,3)*(-152 + 566*t2 - 657*Power(t2,2) + 
                  296*Power(t2,3)) + 
               Power(t1,2)*(322 - 1243*t2 + 1182*Power(t2,2) + 
                  20*Power(t2,3) - 362*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (352 + 399*t2 - 476*Power(t2,2) - 77*Power(t2,3) + 
                  56*Power(t2,4)) + 
               4*t1*(184 - 403*t2 + 310*Power(t2,2) + 79*Power(t2,3) - 
                  236*Power(t2,4) + 66*Power(t2,5)) + 
               Power(s2,2)*(599 - 1697*t2 + 1364*Power(t2,2) - 
                  88*Power(t2,3) - 259*Power(t2,4) - 
                  2*t1*(-142 + 570*t2 - 729*Power(t2,2) + 
                     367*Power(t2,3))) + 
               s2*(-419 + 382*t2 - 338*Power(t2,2) + 844*Power(t2,3) - 
                  747*Power(t2,4) + 278*Power(t2,5) + 
                  Power(t1,2)*
                   (-234 + 874*t2 - 1021*Power(t2,2) + 486*Power(t2,3)) \
+ 2*t1*(-478 + 1819*t2 - 2161*Power(t2,2) + 833*Power(t2,3) + 
                     68*Power(t2,4)))) - 
            s*(-1 + t2)*(Power(t1,3)*
                (-91 + 8*t2 + 52*Power(t2,2) - 24*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (39 + 169*t2 + 105*Power(t2,2) + 10*Power(t2,3)) + 
               2*t1*Power(-1 + t2,2)*
                (-4 - 3*t2 - 40*Power(t2,2) + 15*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,3)*(62 + 7*t2 + 30*Power(t2,2) - 
                  54*Power(t2,3) + 10*Power(t2,4)) - 
               Power(t1,2)*(22 + 387*t2 - 645*Power(t2,2) + 
                  176*Power(t2,3) + 60*Power(t2,4)) - 
               Power(s2,2)*(105 - 205*t2 + 208*Power(t2,2) - 
                  71*Power(t2,3) - 52*Power(t2,4) + 15*Power(t2,5) + 
                  t1*(219 + 172*t2 - 320*Power(t2,2) + 
                     58*Power(t2,3) + 36*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (209 - 7*t2 - 148*Power(t2,2) - 4*Power(t2,3) + 
                     12*Power(t2,4)) + 
                  Power(t1,2)*
                   (273 + 88*t2 - 345*Power(t2,2) + 129*Power(t2,3) + 
                     20*Power(t2,4)) - 
                  2*t1*(-6 - 232*t2 + 283*Power(t2,2) + 
                     25*Power(t2,3) - 76*Power(t2,4) + 6*Power(t2,5)))) \
+ Power(s,2)*(Power(s2,3)*(20 + 144*t2 - 461*Power(t2,2) + 
                  388*Power(t2,3) - 116*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (229 - 91*t2 - 396*Power(t2,2) - 36*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(t1,3)*(-42 + 88*t2 + 14*Power(t2,2) - 
                  45*Power(t2,3) + 10*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (-344 + 487*t2 - 866*Power(t2,2) + 101*Power(t2,3) + 
                  46*Power(t2,4)) + 
               Power(t1,2)*(-380 + 373*t2 + 720*Power(t2,2) - 
                  1208*Power(t2,3) + 459*Power(t2,4) + 36*Power(t2,5)) + 
               Power(s2,2)*(49 - 950*t2 + 1804*Power(t2,2) - 
                  1086*Power(t2,3) + 110*Power(t2,4) + 73*Power(t2,5) + 
                  t1*(-166 + 248*t2 + 348*Power(t2,2) - 
                     653*Power(t2,3) + 298*Power(t2,4))) + 
               s2*(-2*Power(-1 + t2,2)*
                   (202 + 51*t2 + 106*Power(t2,2) - 126*Power(t2,3) + 
                     55*Power(t2,4)) - 
                  3*Power(t1,2)*
                   (-52 + 120*t2 + 23*Power(t2,2) - 138*Power(t2,3) + 
                     72*Power(t2,4)) + 
                  2*t1*(230 + 219*t2 - 1535*Power(t2,2) + 
                     1738*Power(t2,3) - 692*Power(t2,4) + 40*Power(t2,5))\
))) + Power(s1,5)*(2*Power(s,8)*(13 - 70*t2 + 75*Power(t2,2)) + 
            Power(s,7)*(-110 - 2*Power(s2,3) + 112*Power(t1,3) + 
               557*t2 - 710*Power(t2,2) + 210*Power(t2,3) - 
               6*Power(t1,2)*(-17 + 7*t2) + 
               4*Power(s2,2)*(9*t1 + 7*t2) + 
               t1*(39 - 98*t2 + 99*Power(t2,2)) - 
               s2*(48 + 126*Power(t1,2) - 126*t2 + 79*Power(t2,2) + 
                  t1*(34 + 66*t2))) - 
            Power(s,6)*(-153 + 857*t2 - 1255*Power(t2,2) + 
               455*Power(t2,3) + 104*Power(t2,4) + 
               2*Power(s2,3)*(-5 + 12*t2) - 
               5*Power(t1,3)*(-78 + 77*t2) + 
               Power(s2,2)*(12 + t1*(128 - 155*t2) - 220*t2 + 
                  275*Power(t2,2)) + 
               Power(t1,2)*(270 - 841*t2 + 533*Power(t2,2)) + 
               t1*(69 - 787*t2 + 1331*Power(t2,2) - 521*Power(t2,3)) + 
               s2*(-242 + 1051*t2 - 1110*Power(t2,2) + 
                  201*Power(t2,3) + 5*Power(t1,2)*(-83 + 82*t2) + 
                  t1*(-72 + 454*t2 - 406*Power(t2,2)))) + 
            Power(s,5)*(48 - 226*t2 - 579*Power(t2,2) + 
               690*Power(t2,3) + 263*Power(t2,4) - 292*Power(t2,5) + 
               Power(s2,3)*(2 - 73*t2 + 165*Power(t2,2)) + 
               Power(t1,3)*(459 - 939*t2 + 372*Power(t2,2)) + 
               Power(t1,2)*(319 - 1601*t2 + 1889*Power(t2,2) - 
                  843*Power(t2,3)) + 
               t1*(202 - 1684*t2 + 3357*Power(t2,2) - 
                  2612*Power(t2,3) + 762*Power(t2,4)) - 
               Power(s2,2)*(56 + 304*t2 - 736*Power(t2,2) + 
                  556*Power(t2,3) + 
                  t1*(-149 + 179*t2 + 284*Power(t2,2))) + 
               s2*(-437 + 2690*t2 - 3696*Power(t2,2) + 
                  1396*Power(t2,3) + 6*Power(t2,4) + 
                  Power(t1,2)*(-451 + 809*t2 - 26*Power(t2,2)) + 
                  8*t1*(-11 + 99*t2 - 106*Power(t2,2) + 73*Power(t2,3)))\
) - Power(-1 + t2,2)*(-2*Power(-1 + t2,3)*(14 + 9*t2) + 
               6*Power(s2,3)*Power(t2,2)*(2 + 4*t2 + Power(t2,2)) + 
               2*t1*Power(-1 + t2,2)*(-49 - 65*t2 + 4*Power(t2,2)) + 
               Power(t1,3)*(23 - 105*t2 + 40*Power(t2,2)) + 
               Power(t1,2)*(57 + 173*t2 - 270*Power(t2,2) + 
                  40*Power(t2,3)) - 
               2*s2*(-(Power(-1 + t2,2)*
                     (17 + 72*t2 + 21*Power(t2,2))) + 
                  Power(t1,2)*
                   (22 - 65*t2 - 40*Power(t2,2) + 20*Power(t2,3)) + 
                  t1*(5 + 171*t2 - 102*Power(t2,2) - 82*Power(t2,3) + 
                     8*Power(t2,4))) + 
               Power(s2,2)*(-3 + 83*t2 + 60*Power(t2,2) - 
                  110*Power(t2,3) - 30*Power(t2,4) + 
                  t1*(13 - 15*t2 - 114*Power(t2,2) - 18*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(s,4)*(163 + 831*t2 - 1188*Power(t2,2) - 
               562*Power(t2,3) + 695*Power(t2,4) + 219*Power(t2,5) - 
               158*Power(t2,6) + 
               Power(t1,3)*(-167 + 453*t2 - 433*Power(t2,2) + 
                  105*Power(t2,3)) + 
               2*Power(s2,3)*
                (-52 + 303*t2 - 433*Power(t2,2) + 177*Power(t2,3)) - 
               Power(t1,2)*(45 + 91*t2 + 173*Power(t2,2) - 
                  719*Power(t2,3) + 428*Power(t2,4)) + 
               t1*(-126 - 676*t2 + 135*Power(t2,2) + 
                  1523*Power(t2,3) - 1460*Power(t2,4) + 444*Power(t2,5)\
) + Power(s2,2)*(581 - 2488*t2 + 2308*Power(t2,2) + 12*Power(t2,3) - 
                  395*Power(t2,4) + 
                  t1*(71 - 799*t2 + 1387*Power(t2,2) - 707*Power(t2,3))\
) + s2*(-284 + 586*t2 + 1473*Power(t2,2) - 1863*Power(t2,3) - 
                  26*Power(t2,4) + 274*Power(t2,5) + 
                  Power(t1,2)*
                   (74 + 234*t2 - 702*Power(t2,2) + 494*Power(t2,3)) - 
                  2*t1*(199 - 1409*t2 + 2027*Power(t2,2) - 
                     827*Power(t2,3) + 10*Power(t2,4)))) + 
            Power(s,3)*(2*Power(t1,3)*
                (-5 + 9*t2 - 22*Power(t2,2) + 8*Power(t2,3)) + 
               2*Power(s2,3)*
                (67 - 296*t2 + 613*Power(t2,2) - 496*Power(t2,3) + 
                  111*Power(t2,4)) - 
               Power(-1 + t2,2)*
                (671 + 281*t2 - 362*Power(t2,2) - 619*Power(t2,3) + 
                  19*Power(t2,4) + 32*Power(t2,5)) - 
               2*Power(t1,2)*
                (148 - 293*t2 + 728*Power(t2,2) - 750*Power(t2,3) + 
                  177*Power(t2,4) + 32*Power(t2,5)) + 
               t1*(101 + 2000*t2 - 5234*Power(t2,2) + 
                  4504*Power(t2,3) - 1401*Power(t2,4) - 
                  76*Power(t2,5) + 106*Power(t2,6)) - 
               Power(s2,2)*(778 - 3523*t2 + 6380*Power(t2,2) - 
                  4064*Power(t2,3) + 330*Power(t2,4) + 
                  183*Power(t2,5) + 
                  2*t1*(81 - 288*t2 + 685*Power(t2,2) - 
                     660*Power(t2,3) + 190*Power(t2,4))) + 
               2*s2*(384 - 2354*t2 + 3450*Power(t2,2) - 
                  1564*Power(t2,3) + 218*Power(t2,4) - 
                  230*Power(t2,5) + 96*Power(t2,6) + 
                  Power(t1,2)*
                   (42 - 155*t2 + 401*Power(t2,2) - 416*Power(t2,3) + 
                     147*Power(t2,4)) + 
                  t1*(367 - 1383*t2 + 3402*Power(t2,2) - 
                     3314*Power(t2,3) + 1118*Power(t2,4) - 
                     106*Power(t2,5)))) + 
            s*(-1 + t2)*(Power(t1,3)*
                (-79 + 32*t2 + 48*Power(t2,2) - 16*Power(t2,3)) + 
               Power(t1,2)*(-19 - 636*t2 + 589*Power(t2,2) + 
                  194*Power(t2,3) - 128*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (-39 + 258*t2 + 197*Power(t2,2) + 40*Power(t2,3) + 
                  2*Power(t2,4)) + 
               2*t1*Power(-1 + t2,2)*
                (23 + 176*t2 - 55*Power(t2,2) + 7*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(s2,3)*(38 + 85*t2 - 36*Power(t2,2) - 
                  10*Power(t2,3) - 67*Power(t2,4) + 5*Power(t2,5)) - 
               Power(s2,2)*(66 + 131*t2 - 185*Power(t2,2) + 
                  338*Power(t2,3) - 357*Power(t2,4) + Power(t2,5) + 
                  6*Power(t2,6) + 
                  t1*(121 + 348*t2 - 274*Power(t2,2) - 
                     196*Power(t2,3) + 42*Power(t2,4) + 4*Power(t2,5))) \
+ s2*(Power(t1,2)*(179 + 238*t2 - 410*Power(t2,2) - 10*Power(t2,3) + 
                     48*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (91 + 81*t2 - 431*Power(t2,2) - 79*Power(t2,3) + 
                     26*Power(t2,4) + 2*Power(t2,5)) - 
                  4*t1*(-9 - 139*t2 - 42*Power(t2,2) + 
                     229*Power(t2,3) - 36*Power(t2,4) - 4*Power(t2,5) + 
                     Power(t2,6)))) + 
            Power(s,2)*(Power(t1,3)*
                (-60 + 43*t2 - 54*Power(t2,2) + 46*Power(t2,3) - 
                  8*Power(t2,4)) + 
               Power(t1,2)*(228 - 155*t2 - 629*Power(t2,2) + 
                  518*Power(t2,3) + 158*Power(t2,4) - 120*Power(t2,5)) - 
               2*Power(-1 + t2,3)*
                (229 - 138*t2 - 258*Power(t2,2) - 111*Power(t2,3) - 
                  4*Power(t2,4) + Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (-199 - 27*t2 + 655*Power(t2,2) - 467*Power(t2,3) + 
                  96*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,3)*(-2 + 130*t2 - 282*Power(t2,2) + 
                  516*Power(t2,3) - 383*Power(t2,4) + 54*Power(t2,5)) - 
               Power(s2,2)*(-196 + 859*t2 - 2506*Power(t2,2) + 
                  3528*Power(t2,3) - 1762*Power(t2,4) + 
                  18*Power(t2,5) + 59*Power(t2,6) + 
                  t1*(74 + 21*t2 + 334*Power(t2,2) - 232*Power(t2,3) - 
                     160*Power(t2,4) + 62*Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (139 - 46*t2 + 302*Power(t2,2) - 356*Power(t2,3) + 
                     20*Power(t2,4) + 40*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (-116 + 1717*t2 - 1625*Power(t2,2) - 
                     115*Power(t2,3) + 31*Power(t2,4) + 42*Power(t2,5)) \
- 2*t1*(130 + 159*t2 - 531*Power(t2,2) - 332*Power(t2,3) + 
                     828*Power(t2,4) - 284*Power(t2,5) + 30*Power(t2,6)))\
)) + Power(s1,3)*(-1 + s + t2)*
          (2*Power(s,9)*t2*(-6 + 19*t2) + 
            Power(s,8)*(-4 + 16*Power(t1,3) + 10*(9 + 4*s2)*t2 - 
               2*(104 + 47*s2)*Power(t2,2) + 21*Power(t2,3) - 
               2*Power(t1,2)*(-3 + 3*s2 + t2) + 
               t1*(2 - 2*(13 + 3*s2)*t2 + 65*Power(t2,2))) + 
            Power(s,7)*(42 - 286*t2 + 545*Power(t2,2) - 
               176*Power(t2,3) - 97*Power(t2,4) + 
               2*Power(s2,2)*t2*(-9 + t1 + 8*t2) + 
               Power(t1,3)*(-92 + 45*t2) + 
               Power(t1,2)*(4 + 28*t2 - 45*Power(t2,2)) + 
               t1*(-4 + 112*t2 - 330*Power(t2,2) + 183*Power(t2,3)) - 
               s2*(10 + 208*t2 - 568*Power(t2,2) + 201*Power(t2,3) + 
                  Power(t1,2)*(-34 + 6*t2) + 
                  2*t1*(3 - 4*t2 + Power(t2,2)))) + 
            Power(s,6)*(-204 + 609*t2 - 913*Power(t2,2) + 
               24*Power(s2,3)*Power(t2,2) + 593*Power(t2,3) + 
               79*Power(t2,4) - 122*Power(t2,5) + 
               Power(t1,3)*(197 - 220*t2 + 37*Power(t2,2)) - 
               Power(t1,2)*(104 + 82*t2 - 145*Power(t2,2) + 
                  67*Power(t2,3)) + 
               t1*(19 - 74*t2 + 495*Power(t2,2) - 506*Power(t2,3) + 
                  178*Power(t2,4)) + 
               2*Power(s2,2)*t2*
                (111 - 188*t2 + 36*Power(t2,2) - t1*(11 + 19*t2)) + 
               s2*(76 + 38*t2 - 923*Power(t2,2) + 822*Power(t2,3) - 
                  111*Power(t2,4) + 
                  Power(t1,2)*(-74 + 67*t2 + 47*Power(t2,2)) + 
                  t1*(26 + 8*t2 + 166*Power(t2,2) - 64*Power(t2,3)))) - 
            (-1 + t2)*(2*Power(s2,3)*Power(t2,3)*(4 + 5*t2) - 
               Power(-1 + t2,3)*(-1 + 44*t2) + 
               t1*Power(-1 + t2,2)*(21 - 149*t2 + 8*Power(t2,2)) + 
               Power(t1,3)*(19 - 49*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-39 + 193*t2 - 174*Power(t2,2) + 
                  20*Power(t2,3)) + 
               Power(s2,2)*t2*
                (-8 + 47*t2 + 25*Power(t2,2) - 64*Power(t2,3) + 
                  t1*(8 - 17*t2 - 53*Power(t2,2) + 8*Power(t2,3))) - 
               2*s2*(-(Power(-1 + t2,2)*
                     (-4 + 15*t2 + 49*Power(t2,2))) + 
                  Power(t1,2)*
                   (4 + 5*t2 - 46*Power(t2,2) + 10*Power(t2,3)) + 
                  t1*(-8 + 18*t2 + 91*Power(t2,2) - 109*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(s,5)*(597 - 1562*t2 + 2078*Power(t2,2) - 
               1300*Power(t2,3) + 419*Power(t2,4) + 91*Power(t2,5) - 
               48*Power(t2,6) + 
               2*Power(s2,3)*Power(t2,2)*(-71 + 20*t2) + 
               Power(t1,3)*(-184 + 353*t2 - 124*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(199 + 82*t2 + 40*Power(t2,2) + 
                  81*Power(t2,3) - 24*Power(t2,4)) + 
               t1*(-54 - 559*t2 + 668*Power(t2,2) - 241*Power(t2,3) - 
                  168*Power(t2,4) + 68*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-876 + 2036*t2 - 930*Power(t2,2) + 85*Power(t2,3) + 
                  t1*(36 + 240*t2 - 51*Power(t2,2))) + 
               s2*(-234 + 1841*t2 - 2102*Power(t2,2) + 
                  273*Power(t2,3) + 295*Power(t2,4) + 20*Power(t2,5) + 
                  Power(t1,2)*
                   (78 - 165*t2 - 177*Power(t2,2) + 67*Power(t2,3)) - 
                  2*t1*(22 - 129*t2 + 655*Power(t2,2) - 
                     324*Power(t2,3) + 64*Power(t2,4)))) + 
            Power(s,4)*(-1017 + 3102*t2 - 4732*Power(t2,2) + 
               3452*Power(t2,3) - 1046*Power(t2,4) + 177*Power(t2,5) + 
               22*Power(t2,6) - 6*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*
                (224 - 165*t2 + 19*Power(t2,2)) + 
               Power(t1,3)*(65 - 180*t2 + 107*Power(t2,2) - 
                  12*Power(t2,3)) - 
               Power(t1,2)*(85 + 102*t2 + 325*Power(t2,2) - 
                  196*Power(t2,3) + 4*Power(t2,4)) + 
               t1*(-15 + 1470*t2 - 2695*Power(t2,2) + 
                  2116*Power(t2,3) - 695*Power(t2,4) + 
                  44*Power(t2,5) + 8*Power(t2,6)) + 
               Power(s2,2)*t2*
                (1444 - 3994*t2 + 2954*Power(t2,2) - 719*Power(t2,3) + 
                  31*Power(t2,4) + 
                  t1*(44 - 431*t2 + 228*Power(t2,2) - 7*Power(t2,3))) + 
               s2*(356 - 4398*t2 + 8395*Power(t2,2) - 
                  5966*Power(t2,3) + 1227*Power(t2,4) + 3*Power(t2,5) + 
                  30*Power(t2,6) + 
                  2*Power(t1,2)*
                   (-25 + 54*t2 + 98*Power(t2,2) - 86*Power(t2,3) + 
                     10*Power(t2,4)) + 
                  t1*(52 - 874*t2 + 2994*Power(t2,2) - 
                     1992*Power(t2,3) + 538*Power(t2,4) - 72*Power(t2,5)\
))) + s*(-(Power(s2,3)*Power(t2,2)*
                  (-58 + 89*t2 - 25*Power(t2,2) + Power(t2,3))) + 
               Power(t1,3)*(-72 + 195*t2 - 140*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,2)*(99 - 598*t2 + 646*Power(t2,2) - 
                  123*Power(t2,3) - 24*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (10 + 385*t2 - 130*Power(t2,2) - 16*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (45 + 143*t2 - 159*Power(t2,2) - 122*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,2)*t2*
                (56 - 357*t2 + 350*Power(t2,2) - 92*Power(t2,3) + 
                  53*Power(t2,4) - 10*Power(t2,5) + 
                  t1*(-64 + 14*t2 + 153*Power(t2,2) - 86*Power(t2,3) + 
                     4*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (34 + 51*t2 - 253*Power(t2,2) + 163*Power(t2,3) - 
                     16*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (14 - 127*t2 - 23*Power(t2,2) - 113*Power(t2,3) - 
                     10*Power(t2,4) + 6*Power(t2,5)) + 
                  2*t1*(-32 + 109*t2 + 211*Power(t2,2) - 
                     380*Power(t2,3) + 82*Power(t2,4) + 8*Power(t2,5) + 
                     2*Power(t2,6)))) + 
            Power(s,3)*(916 - 2972*t2 + 4946*Power(t2,2) - 
               4775*Power(t2,3) + 2101*Power(t2,4) - 220*Power(t2,5) + 
               2*Power(t2,6) + 2*Power(t2,7) + 
               Power(t1,3)*(-36 + 15*t2 - 8*Power(t2,2)) + 
               Power(s2,3)*Power(t2,2)*
                (-60 + 137*t2 - 53*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(-46 + 24*t2 + 87*Power(t2,2) - 
                  102*Power(t2,3) + 16*Power(t2,4)) + 
               t1*(176 - 1430*t2 + 2596*Power(t2,2) - 
                  1955*Power(t2,3) + 788*Power(t2,4) - 
                  184*Power(t2,5) + 20*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-1018 + 3057*t2 - 3040*Power(t2,2) + 
                  1293*Power(t2,3) - 201*Power(t2,4) + 2*Power(t2,5) + 
                  2*t1*(-83 + 129*t2 - 111*Power(t2,2) + 
                     13*Power(t2,3) + 2*Power(t2,4))) + 
               s2*(-266 + 3874*t2 - 9011*Power(t2,2) + 
                  8652*Power(t2,3) - 3766*Power(t2,4) + 
                  494*Power(t2,5) + 6*Power(t2,6) + 6*Power(t2,7) + 
                  Power(t1,2)*
                   (46 + 88*t2 - 98*Power(t2,2) + 82*Power(t2,3) - 
                     16*Power(t2,4)) - 
                  2*t1*(39 - 526*t2 + 1291*Power(t2,2) - 
                     1024*Power(t2,3) + 314*Power(t2,4) - 
                     64*Power(t2,5) + 6*Power(t2,6)))) - 
            Power(s,2)*(Power(t1,3)*
                (-87 + 140*t2 - 67*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (104 - 69*t2 - 7*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(34 - 418*t2 + 181*Power(t2,2) + 
                  179*Power(t2,3) - 56*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (374 - 227*t2 + 339*Power(t2,2) - 517*Power(t2,3) + 
                  24*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(155 - 354*t2 + 427*Power(t2,2) - 170*Power(t2,3) - 
                  98*Power(t2,4) + 40*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-198 + 437*t2 - 616*Power(t2,2) + 430*Power(t2,3) - 
                  153*Power(t2,4) + 20*Power(t2,5) - 
                  6*t1*(27 - 3*t2 - 12*Power(t2,2) + Power(t2,3))) + 
               s2*(Power(t1,2)*
                   (54 + 141*t2 - 183*Power(t2,2) + 28*Power(t2,3) + 
                     8*Power(t2,4)) - 
                  2*t1*(49 - 306*t2 + 229*Power(t2,2) - 
                     140*Power(t2,3) + 96*Power(t2,4) - 
                     12*Power(t2,5) + 4*Power(t2,6)) - 
                  2*(36 - 543*t2 + 1469*Power(t2,2) - 1619*Power(t2,3) + 
                     963*Power(t2,4) - 343*Power(t2,5) + 
                     35*Power(t2,6) + 2*Power(t2,7))))) - 
         Power(s1,4)*(2*Power(s,9)*(3 - 32*t2 + 51*Power(t2,2)) + 
            Power(s,8)*(-42 + 56*Power(t1,3) + 
               Power(t1,2)*(38 - 14*t2) + 350*t2 - 566*Power(t2,2) + 
               163*Power(t2,3) + 6*Power(s2,2)*(t1 + t2) + 
               2*t1*(7 - 36*t2 + 59*Power(t2,2)) - 
               s2*(20 + 42*Power(t1,2) - 136*t2 + 159*Power(t2,2) + 
                  t1*(6 + 32*t2))) - 
            Power(s,7)*(-102 + Power(t1,3)*(275 - 203*t2) + 732*t2 + 
               4*Power(s2,3)*t2 - 1403*Power(t2,2) + 649*Power(t2,3) + 
               95*Power(t2,4) + 
               Power(t1,2)*(94 - 297*t2 + 216*Power(t2,2)) + 
               t1*(12 - 370*t2 + 893*Power(t2,2) - 469*Power(t2,3)) - 
               2*Power(s2,2)*
                (6 + t2 - 40*Power(t2,2) + t1*(-16 + 17*t2)) + 
               s2*(-112 + 872*t2 - 1312*Power(t2,2) + 441*Power(t2,3) + 
                  Power(t1,2)*(-200 + 129*t2) + 
                  t1*(10 + 14*t2 - 78*Power(t2,2)))) + 
            Power(s,6)*(-50 + 475*t2 - 1274*Power(t2,2) + 
               1344*Power(t2,3) + 42*Power(t2,4) - 326*Power(t2,5) + 
               24*Power(s2,3)*t2*(-1 + 4*t2) + 
               Power(t1,3)*(501 - 805*t2 + 237*Power(t2,2)) + 
               Power(t1,2)*(41 - 866*t2 + 980*Power(t2,2) - 
                  421*Power(t2,3)) + 
               t1*(-80 - 684*t2 + 2179*Power(t2,2) - 1942*Power(t2,3) + 
                  675*Power(t2,4)) - 
               Power(s2,2)*(124 - 334*t2 + 123*Power(t2,2) + 
                  164*Power(t2,3) + t1*(-72 + 133*t2 + 148*Power(t2,2))) \
+ s2*(-115 + 1728*t2 - 3884*Power(t2,2) + 2336*Power(t2,3) - 
                  350*Power(t2,4) + 
                  Power(t1,2)*(-359 + 524*t2 + 46*Power(t2,2)) + 
                  2*t1*(47 + 74*t2 + 6*Power(t2,2) + 43*Power(t2,3)))) - 
            Power(-1 + t2,2)*(-4*Power(-1 + t2,3)*(5 + 11*t2) + 
               6*Power(s2,3)*Power(t2,2)*(1 + 4*t2 + 2*Power(t2,2)) + 
               3*Power(t1,3)*(11 - 35*t2 + 10*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(-31 - 210*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(-22 + 299*t2 - 317*Power(t2,2) + 
                  40*Power(t2,3)) + 
               Power(s2,2)*(-4 + 19*t2 + 121*Power(t2,2) - 
                  68*Power(t2,3) - 68*Power(t2,4) + 
                  t1*(4 + 11*t2 - 91*Power(t2,2) - 62*Power(t2,3) + 
                     12*Power(t2,4))) - 
               s2*(-(Power(-1 + t2,2)*(-3 + 132*t2 + 100*Power(t2,2))) + 
                  Power(t1,2)*
                   (31 - 52*t2 - 145*Power(t2,2) + 40*Power(t2,3)) + 
                  t1*(-34 + 250*t2 + 56*Power(t2,2) - 296*Power(t2,3) + 
                     24*Power(t2,4)))) + 
            Power(s,5)*(-17 - 507*t2 + 310*Power(t2,2) - 
               700*Power(t2,3) + 817*Power(t2,4) + 255*Power(t2,5) - 
               226*Power(t2,6) + 
               Power(s2,3)*t2*(246 - 597*t2 + 239*Power(t2,2)) + 
               Power(t1,3)*(-407 + 995*t2 - 640*Power(t2,2) + 
                  100*Power(t2,3)) + 
               Power(t1,2)*(-73 + 605*t2 - 822*Power(t2,2) + 
                  769*Power(t2,3) - 275*Power(t2,4)) + 
               t1*(375 - 295*t2 - 377*Power(t2,2) + 1559*Power(t2,3) - 
                  1364*Power(t2,4) + 436*Power(t2,5)) - 
               Power(s2,2)*(-462 + 2407*t2 - 3458*Power(t2,2) + 
                  973*Power(t2,3) + 110*Power(t2,4) + 
                  t1*(74 + 98*t2 - 803*Power(t2,2) + 373*Power(t2,3))) + 
               s2*(-604 + 1145*t2 + 934*Power(t2,2) - 3173*Power(t2,3) + 
                  1252*Power(t2,4) + 26*Power(t2,5) + 
                  Power(t1,2)*
                   (294 - 493*t2 - 315*Power(t2,2) + 314*Power(t2,3)) - 
                  2*t1*(120 - 675*t2 + 1476*Power(t2,2) - 
                     737*Power(t2,3) + 112*Power(t2,4)))) + 
            Power(s,4)*(-128 + 2279*t2 - 3566*Power(t2,2) + 
               1440*Power(t2,3) - 621*Power(t2,4) + 595*Power(t2,5) + 
               63*Power(t2,6) - 62*Power(t2,7) + 
               2*Power(s2,3)*t2*
                (-234 + 585*t2 - 492*Power(t2,2) + 100*Power(t2,3)) + 
               Power(t1,3)*(125 - 379*t2 + 381*Power(t2,2) - 
                  127*Power(t2,3) + 10*Power(t2,4)) + 
               Power(t1,2)*(310 + 63*t2 - 711*Power(t2,2) + 
                  591*Power(t2,3) + 13*Power(t2,4) - 56*Power(t2,5)) + 
               t1*(-815 + 2132*t2 - 4314*Power(t2,2) + 
                  4106*Power(t2,3) - 1121*Power(t2,4) - 
                  182*Power(t2,5) + 124*Power(t2,6)) + 
               Power(s2,2)*(-748 + 4807*t2 - 9048*Power(t2,2) + 
                  6648*Power(t2,3) - 1254*Power(t2,4) - 
                  61*Power(t2,5) + 
                  t1*(2 + 583*t2 - 1429*Power(t2,2) + 
                     1231*Power(t2,3) - 237*Power(t2,4))) + 
               s2*(1745 - 7032*t2 + 11817*Power(t2,2) - 
                  7158*Power(t2,3) + 496*Power(t2,4) + 58*Power(t2,5) + 
                  144*Power(t2,6) + 
                  Power(t1,2)*
                   (-85 - 82*t2 + 678*Power(t2,2) - 810*Power(t2,3) + 
                     221*Power(t2,4)) + 
                  t1*(396 - 3834*t2 + 7618*Power(t2,2) - 
                     6606*Power(t2,3) + 2168*Power(t2,4) - 
                     296*Power(t2,5)))) + 
            s*(-1 + t2)*(Power(t1,3)*
                (-99 + 220*t2 - 108*Power(t2,2) + 16*Power(t2,3)) + 
               Power(t1,2)*(33 - 738*t2 + 616*Power(t2,2) + 
                  193*Power(t2,3) - 104*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (-89 + 116*t2 + 291*Power(t2,2) + 80*Power(t2,3) + 
                  2*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (109 + 468*t2 - 2*Power(t2,2) - 24*Power(t2,3) + 
                  8*Power(t2,4)) - 
               Power(s2,3)*t2*
                (-86 + 45*t2 + 28*Power(t2,2) + 33*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s2,2)*(26 - 259*t2 + 56*Power(t2,2) + 
                  11*Power(t2,3) + 45*Power(t2,4) + 135*Power(t2,5) - 
                  14*Power(t2,6) - 
                  t1*(30 + 208*t2 - 139*Power(t2,2) - 228*Power(t2,3) + 
                     38*Power(t2,4) + 4*Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (106 + 57*t2 - 300*Power(t2,2) + 26*Power(t2,3) + 
                     24*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-24 + 7*t2 - 327*Power(t2,2) - 235*Power(t2,3) + 
                     14*Power(t2,4) + 6*Power(t2,5)) + 
                  t1*(-52 + 618*t2 + 286*Power(t2,2) - 856*Power(t2,3) - 
                     48*Power(t2,4) + 56*Power(t2,5) - 4*Power(t2,6)))) + 
            Power(s,3)*(Power(t1,3)*(39 + t2 - 8*Power(t2,2)) + 
               Power(s2,3)*t2*
                (228 - 814*t2 + 982*Power(t2,2) - 547*Power(t2,3) + 
                  72*Power(t2,4)) + 
               Power(t1,2)*(-312 + 207*t2 + 416*Power(t2,2) - 
                  592*Power(t2,3) + 386*Power(t2,4) - 72*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (-78 + 2413*t2 - 2092*Power(t2,2) - 335*Power(t2,3) - 
                  152*Power(t2,4) + 10*Power(t2,5) + 6*Power(t2,6)) + 
               t1*(754 - 2222*t2 + 3819*Power(t2,2) - 
                  4873*Power(t2,3) + 3384*Power(t2,4) - 
                  970*Power(t2,5) + 96*Power(t2,6) + 12*Power(t2,7)) + 
               Power(s2,2)*(528 - 3544*t2 + 7709*Power(t2,2) - 
                  8418*Power(t2,3) + 4318*Power(t2,4) - 
                  523*Power(t2,5) - 37*Power(t2,6) + 
                  t1*(76 - 338*t2 + 912*Power(t2,2) - 806*Power(t2,3) + 
                     386*Power(t2,4) - 40*Power(t2,5))) + 
               s2*(-1628 + 7086*t2 - 15006*Power(t2,2) + 
                  16297*Power(t2,3) - 7540*Power(t2,4) + 
                  750*Power(t2,5) - 15*Power(t2,6) + 56*Power(t2,7) + 
                  Power(t1,2)*
                   (-68 + 13*t2 - 258*Power(t2,2) + 332*Power(t2,3) - 
                     202*Power(t2,4) + 40*Power(t2,5)) - 
                  2*t1*(189 - 1627*t2 + 3001*Power(t2,2) - 
                     2820*Power(t2,3) + 1668*Power(t2,4) - 
                     432*Power(t2,5) + 54*Power(t2,6)))) + 
            Power(s,2)*(2*Power(-1 + t2,3)*
                (-80 - 360*t2 + 575*Power(t2,2) + 154*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,3)*(-105 + 133*t2 - 25*Power(t2,2) - 
                  14*Power(t2,3) + 4*Power(t2,4)) + 
               Power(t1,2)*(101 - 720*t2 + 770*Power(t2,2) - 
                  277*Power(t2,3) + 174*Power(t2,4) - 48*Power(t2,5)) + 
               Power(s2,3)*t2*
                (108 + 20*t2 - 208*Power(t2,2) + 200*Power(t2,3) - 
                  124*Power(t2,4) + 11*Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (-158 + 510*t2 + 137*Power(t2,2) + 130*Power(t2,3) - 
                  60*Power(t2,4) + 20*Power(t2,5)) - 
               Power(s2,2)*(108 - 544*t2 + 1522*Power(t2,2) - 
                  2331*Power(t2,3) + 2287*Power(t2,4) - 
                  1144*Power(t2,5) + 94*Power(t2,6) + 8*Power(t2,7) + 
                  t1*(76 + 223*t2 - 100*Power(t2,2) - 168*Power(t2,3) - 
                     14*Power(t2,4) + 4*Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (135 + 232*t2 - 498*Power(t2,2) + 212*Power(t2,3) - 
                     84*Power(t2,4) + 24*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (483 - 1002*t2 + 1729*Power(t2,2) - 
                     1978*Power(t2,3) + 135*Power(t2,4) + 
                     48*Power(t2,5) + 6*Power(t2,6)) - 
                  2*t1*(-63 + 260*t2 - 696*Power(t2,2) + 
                     453*Power(t2,3) - 12*Power(t2,4) + 88*Power(t2,5) - 
                     36*Power(t2,6) + 6*Power(t2,7))))))*R1q(s1))/
     (s*(-1 + s1)*s1*Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2),2)*(-1 + s2)*(-s + s2 - t1)*Power(-1 + s + t2,3)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(4*Power(s,12)*Power(t2,2) + 
         2*Power(s,11)*(Power(t1,3) + 2*t1*Power(t2,2) + 
            t2*(s1*(4 - 15*t2) + t2*(-16 - 3*s2 + 13*t2))) + 
         Power(s,10)*(Power(t1,3)*(-16 + 17*t2) + 
            Power(t1,2)*(6 + 2*(-2 + s2)*t2 - 3*Power(t2,2)) + 
            Power(t2,2)*(108 + 2*Power(s2,2) + s2*(48 - 39*t2) - 
               206*t2 + 61*Power(t2,2)) + 
            Power(s1,2)*(4 - 52*t2 + 98*Power(t2,2)) + 
            t1*t2*(6 - 10*t2 + 33*Power(t2,2) - s2*(6 + 5*t2)) + 
            s1*(-18*Power(t1,3) + t1*(2 + 6*s2 - 33*t2)*t2 + 
               2*Power(t1,2)*(-3 + 3*s2 + t2) + 
               t2*(-56 - 12*s2 + 254*t2 + 40*s2*t2 - 173*Power(t2,2)))) + 
         Power(-1 + t2,2)*Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,2)*
          Power(s1 + Power(s1,2) - 2*s1*t2 + (-1 + t2)*t2,2)*
          (6 + Power(s1,3)*(2 - 3*s2 + 3*t1 - 2*t2) - 23*t2 + 2*s2*t2 + 
            17*Power(t2,2) - 5*s2*Power(t2,2) + 
            t1*(-6 + 11*t2 - 2*Power(t2,2)) + 
            Power(s1,2)*(1 + s2 + 2*t1 - 3*t2 + 2*s2*t2 - 5*t1*t2 + 
               2*Power(t2,2)) + 
            s1*(11 + t1 - 12*t2 - 6*t1*t2 + Power(t2,2) + 
               2*t1*Power(t2,2) + s2*(-2 + 4*t2 + Power(t2,2)))) - 
         Power(s,9)*(-2*Power(t1,3)*(26 - 63*t2 + 32*Power(t2,2)) + 
            2*Power(s1,3)*(11 - 72*t2 + 91*Power(t2,2)) + 
            Power(t1,2)*(42 + (-69 + 6*s2)*t2 + 
               (7 - 17*s2)*Power(t2,2) + 24*Power(t2,3)) - 
            2*t1*(3 + 5*(-5 + 4*s2)*t2 + (8 - 10*s2)*Power(t2,2) - 
               (29 + 22*s2)*Power(t2,3) + 60*Power(t2,4)) + 
            t2*(-6 + 210*t2 + Power(s2,2)*(30 - 13*t2)*t2 - 
               671*Power(t2,2) + 537*Power(t2,3) - 32*Power(t2,4) + 
               s2*(6 + 121*t2 - 298*Power(t2,2) + 93*Power(t2,3))) + 
            Power(s1,2)*(24 - 72*Power(t1,3) - 336*t2 + 
               852*Power(t2,2) - 490*Power(t2,3) - 
               6*Power(s2,2)*(t1 + t2) + 4*Power(t1,2)*(-11 + 4*t2) + 
               t1*(-4 + 26*t2 - 111*Power(t2,2)) + 
               s2*(6 + 48*Power(t1,2) - 56*t2 + 107*Power(t2,2) + 
                  t1*(6 + 38*t2))) + 
            s1*(3*Power(t1,3)*(-41 + 47*t2) + 
               Power(t1,2)*(10 + s2*(34 - 37*t2) + 55*t2 - 
                  43*Power(t2,2)) + 
               t1*(6 + 2*Power(s2,2)*t2 - 133*Power(t2,2) + 
                  251*Power(t2,3) + s2*(-6 + 4*t2 - 71*Power(t2,2))) + 
               t2*(-148 + 935*t2 - 1259*Power(t2,2) + 347*Power(t2,3) + 
                  Power(s2,2)*(2 + 9*t2) - 
                  2*s2*(51 - 178*t2 + 113*Power(t2,2))))) + 
         Power(s,8)*(2 - 40*t2 + 32*s2*t2 + 475*Power(t2,2) - 
            159*s2*Power(t2,2) + 192*Power(s2,2)*Power(t2,2) - 
            6*Power(s2,3)*Power(t2,2) - 1237*Power(t2,3) - 
            618*s2*Power(t2,3) - 206*Power(s2,2)*Power(t2,3) + 
            1781*Power(t2,4) + 737*s2*Power(t2,4) + 
            32*Power(s2,2)*Power(t2,4) - 674*Power(t2,5) - 
            60*s2*Power(t2,5) - 140*Power(t2,6) + 
            10*Power(s1,4)*(5 - 22*t2 + 21*Power(t2,2)) + 
            Power(t1,3)*(-84 + 368*t2 - 433*Power(t2,2) + 
               140*Power(t2,3)) + 
            Power(t1,2)*(114 - (333 + 14*s2)*t2 + 
               (231 - 45*s2)*Power(t2,2) + (62 + 64*s2)*Power(t2,3) - 
               84*Power(t2,4)) + 
            t1*(-36 + (187 - 86*s2)*t2 + 
               (-228 + 254*s2 - 3*Power(s2,2))*Power(t2,2) + 
               2*(-66 + 23*s2)*Power(t2,3) - 
               (123 + 172*s2)*Power(t2,4) + 252*Power(t2,5)) + 
            Power(s1,3)*(114 + 2*Power(s2,3) - 168*Power(t1,3) - 
               855*t2 + 1576*Power(t2,2) - 763*Power(t2,3) + 
               28*Power(t1,2)*(-5 + 2*t2) - 
               2*Power(s2,2)*(21*t1 + 17*t2) + 
               t1*(-29 + 98*t2 - 191*Power(t2,2)) + 
               2*s2*(11 + 84*Power(t1,2) - 45*t2 + 66*Power(t2,2) + 
                  t1*(20 + 49*t2))) + 
            Power(s1,2)*(50 - 872*t2 - 4*Power(s2,3)*t2 + 
               3096*Power(t2,2) - 3259*Power(t2,3) + 801*Power(t2,4) + 
               Power(t1,3)*(-418 + 516*t2) + 
               Power(t1,2)*(-96 + 471*t2 - 236*Power(t2,2)) + 
               t1*(46 + 70*t2 - 699*Power(t2,2) + 773*Power(t2,3)) + 
               Power(s2,2)*(2 + 8*t2 + 39*Power(t2,2) + 
                  t1*(-26 + 63*t2)) + 
               s2*(42 + Power(t1,2)*(237 - 330*t2) - 495*t2 + 
                  990*Power(t2,2) - 500*Power(t2,3) + 
                  t1*(-23 + 42*t2 - 260*Power(t2,2)))) - 
            s1*(Power(t1,3)*(343 - 876*t2 + 484*Power(t2,2)) + 
               Power(t1,2)*(-191 + 9*t2 + 392*Power(t2,2) - 
                  249*Power(t2,3) + s2*(-74 + 222*t2 - 78*Power(t2,2))) \
+ t2*(214 - 1740*t2 + 4*Power(s2,3)*t2 + 4007*Power(t2,2) - 
                  2586*Power(t2,3) + 105*Power(t2,4) + 
                  17*Power(s2,2)*(2 - 7*t2 + 3*Power(t2,2)) + 
                  s2*(212 - 1346*t2 + 1688*Power(t2,2) - 
                     437*Power(t2,3))) + 
               t1*(14 - 4*t2 + 49*Power(t2,2) - 784*Power(t2,3) + 
                  836*Power(t2,4) + Power(s2,2)*t2*(-26 + 3*t2) + 
                  s2*(26 + 112*t2 + 182*Power(t2,2) - 359*Power(t2,3))))) \
+ Power(s,7)*(-10 + 99*t2 - 50*s2*t2 - 1495*Power(t2,2) + 
            1438*s2*Power(t2,2) - 573*Power(s2,2)*Power(t2,2) + 
            24*Power(s2,3)*Power(t2,2) + 2620*Power(t2,3) - 
            1389*s2*Power(t2,3) + 1264*Power(s2,2)*Power(t2,3) - 
            47*Power(s2,3)*Power(t2,3) - 3178*Power(t2,4) - 
            1178*s2*Power(t2,4) - 602*Power(s2,2)*Power(t2,4) + 
            2620*Power(t2,5) + 847*s2*Power(t2,5) + 
            28*Power(s2,2)*Power(t2,5) - 266*Power(t2,6) + 
            168*s2*Power(t2,6) - 364*Power(t2,7) - 
            2*Power(s1,5)*(30 - 100*t2 + 77*Power(t2,2)) + 
            Power(t1,3)*(56 - 504*t2 + 1097*Power(t2,2) - 
               847*Power(t2,3) + 196*Power(t2,4)) + 
            Power(t1,2)*(-138 + (659 + 98*s2)*t2 - 
               (955 + 91*s2)*Power(t2,2) + (302 - 147*s2)*Power(t2,3) + 
               14*(21 + 10*s2)*Power(t2,4) - 168*Power(t2,5)) + 
            t1*(78 + 8*(-46 + 3*s2)*t2 + 
               (1195 - 809*s2 + 47*Power(s2,2))*Power(t2,2) + 
               (-229 + 701*s2 - 23*Power(s2,2))*Power(t2,3) + 
               (-827 + 378*s2)*Power(t2,4) - 
               7*(11 + 56*s2)*Power(t2,5) + 336*Power(t2,6)) + 
            Power(s1,4)*(-12*Power(s2,3) + 252*Power(t1,3) - 
               28*Power(t1,2)*(-9 + 4*t2) + 
               2*Power(s2,2)*(1 + 63*t1 + 39*t2) + 
               t1*(77 - 158*t2 + 159*Power(t2,2)) + 
               2*(-112 + 599*t2 - 875*Power(t2,2) + 350*Power(t2,3)) - 
               s2*(25 + 336*Power(t1,2) - 18*t2 + 29*Power(t2,2) + 
                  6*t1*(19 + 21*t2))) + 
            Power(s1,3)*(-225 + 2074*t2 - 5308*Power(t2,2) + 
               4576*Power(t2,3) - 931*Power(t2,4) + 
               Power(s2,3)*(-8 + 37*t2) - 
               14*Power(t1,3)*(-59 + 78*t2) + 
               Power(t1,2)*(455 - 1485*t2 + 699*Power(t2,2)) - 
               t1*(44 + 630*t2 - 1829*Power(t2,2) + 1230*Power(t2,3)) - 
               Power(s2,2)*(1 + 41*t2 + 95*Power(t2,2) + 
                  t1*(-163 + 351*t2)) + 
               s2*(-148 + 963*t2 - 1331*Power(t2,2) + 442*Power(t2,3) + 
                  Power(t1,2)*(-723 + 1106*t2) + 
                  t1*(-37 + 208*t2 + 316*Power(t2,2)))) + 
            Power(s1,2)*(-34 + 918*t2 - 5420*Power(t2,2) + 
               9622*Power(t2,3) - 5041*Power(t2,4) + 4*Power(t2,5) + 
               4*Power(s2,3)*t2*(3 + t2) + 
               2*Power(t1,3)*(495 - 1331*t2 + 797*Power(t2,2)) - 
               Power(t1,2)*(178 + 1334*t2 - 2529*Power(t2,2) + 
                  1117*Power(t2,3)) + 
               t1*(-130 + 214*t2 + 1308*Power(t2,2) - 
                  3627*Power(t2,3) + 2333*Power(t2,4)) + 
               2*Power(s2,2)*
                (-14 + 29*t2 - 73*Power(t2,2) + 47*Power(t2,3) + 
                  t1*(24 - 152*t2 + 87*Power(t2,2))) + 
               s2*(-73 + 1460*t2 - 4391*Power(t2,2) + 
                  3679*Power(t2,3) - 668*Power(t2,4) - 
                  2*Power(t1,2)*(227 - 755*t2 + 456*Power(t2,2)) + 
                  t1*(122 + 166*t2 + 602*Power(t2,2) - 840*Power(t2,3)))\
) + s1*(-16 + 635*t2 - 2306*Power(t2,2) + 6349*Power(t2,3) - 
               7997*Power(t2,4) + 2265*Power(t2,5) + 749*Power(t2,6) - 
               3*Power(s2,3)*t2*(4 - 21*t2 + 9*Power(t2,2)) + 
               Power(t1,3)*(485 - 2145*t2 + 2682*Power(t2,2) - 
                  952*Power(t2,3)) + 
               Power(t1,2)*(-550 + 922*t2 + 579*Power(t2,2) - 
                  1549*Power(t2,3) + 707*Power(t2,4)) + 
               t1*(161 - 385*t2 - 266*Power(t2,2) + 97*Power(t2,3) + 
                  2020*Power(t2,4) - 1596*Power(t2,5)) + 
               Power(s2,2)*t2*
                (328 - 1142*t2 + 767*Power(t2,2) - 105*Power(t2,3) + 
                  t1*(-94 + 101*t2 + 37*Power(t2,2))) + 
               s2*(2 - 461*t2 - 955*Power(t2,2) + 4919*Power(t2,3) - 
                  3251*Power(t2,4) + 91*Power(t2,5) + 
                  Power(t1,2)*
                   (-78 + 527*t2 - 570*Power(t2,2) + 14*Power(t2,3)) + 
                  t1*(44 + 408*t2 - 365*Power(t2,2) - 1199*Power(t2,3) + 
                     1029*Power(t2,4))))) + 
         Power(s,6)*(16 - 60*t1 + 30*Power(t1,2) + 28*Power(t1,3) - 
            93*t2 - 8*s2*t2 + 263*t1*t2 + 190*s2*t1*t2 - 
            405*Power(t1,2)*t2 - 210*s2*Power(t1,2)*t2 + 
            210*Power(t1,3)*t2 + 3303*Power(t2,2) - 
            3131*s2*Power(t2,2) + 916*Power(s2,2)*Power(t2,2) - 
            36*Power(s2,3)*Power(t2,2) - 2892*t1*Power(t2,2) + 
            1339*s2*t1*Power(t2,2) - 172*Power(s2,2)*t1*Power(t2,2) + 
            1414*Power(t1,2)*Power(t2,2) + 
            499*s2*Power(t1,2)*Power(t2,2) - 
            1152*Power(t1,3)*Power(t2,2) - 7275*Power(t2,3) + 
            8205*s2*Power(t2,3) - 3389*Power(s2,2)*Power(t2,3) + 
            166*Power(s2,3)*Power(t2,3) + 3779*t1*Power(t2,3) - 
            3190*s2*t1*Power(t2,3) + 311*Power(s2,2)*t1*Power(t2,3) - 
            1403*Power(t1,2)*Power(t2,3) - 
            248*s2*Power(t1,2)*Power(t2,3) + 
            1766*Power(t1,3)*Power(t2,3) + 6425*Power(t2,4) - 
            4968*s2*Power(t2,4) + 3579*Power(s2,2)*Power(t2,4) - 
            161*Power(s2,3)*Power(t2,4) + 602*t1*Power(t2,4) + 
            1116*s2*t1*Power(t2,4) - 77*Power(s2,2)*t1*Power(t2,4) + 
            69*Power(t1,2)*Power(t2,4) - 
            273*s2*Power(t1,2)*Power(t2,4) - 
            1029*Power(t1,3)*Power(t2,4) - 4641*Power(t2,5) - 
            866*s2*Power(t2,5) - 966*Power(s2,2)*Power(t2,5) - 
            2050*t1*Power(t2,5) + 938*s2*t1*Power(t2,5) + 
            574*Power(t1,2)*Power(t2,5) + 
            196*s2*Power(t1,2)*Power(t2,5) + 
            182*Power(t1,3)*Power(t2,5) + 2287*Power(t2,6) + 
            245*s2*Power(t2,6) - 28*Power(s2,2)*Power(t2,6) + 
            133*t1*Power(t2,6) - 574*s2*t1*Power(t2,6) - 
            210*Power(t1,2)*Power(t2,6) + 378*Power(t2,7) + 
            462*s2*Power(t2,7) + 294*t1*Power(t2,7) - 434*Power(t2,8) + 
            2*Power(s1,6)*(20 - 54*t2 + 35*Power(t2,2)) + 
            Power(s1,5)*(238 + 30*Power(s2,3) - 252*Power(t1,3) + 
               140*Power(t1,2)*(-2 + t2) - 1002*t2 + 1182*Power(t2,2) - 
               371*Power(t2,3) - 10*Power(s2,2)*(1 + 21*t1 + 9*t2) + 
               t1*(-92 + 90*t2 - 9*Power(t2,2)) + 
               2*s2*(-3 + 210*Power(t1,2) + 70*t2 - 67*Power(t2,2) + 
                  5*t1*(18 + 7*t2))) + 
            Power(s1,4)*(392 + Power(s2,3)*(43 - 130*t2) - 2522*t2 + 
               5140*Power(t2,2) - 3660*Power(t2,3) + 515*Power(t2,4) + 
               210*Power(t1,3)*(-5 + 7*t2) + 
               Power(t1,2)*(-924 + 2601*t2 - 1268*Power(t2,2)) + 
               t1*(-183 + 1684*t2 - 2647*Power(t2,2) + 
                  1036*Power(t2,3)) + 
               Power(s2,2)*(-42 + 207*t2 + 47*Power(t2,2) + 
                  5*t1*(-88 + 179*t2)) + 
               s2*(223 - 1033*t2 + 851*Power(t2,2) + 104*Power(t2,3) - 
                  5*Power(t1,2)*(-253 + 406*t2) + 
                  t1*(345 - 1176*t2 + 221*Power(t2,2)))) + 
            Power(s1,3)*(51 - 1855*t2 + 8356*Power(t2,2) - 
               11763*Power(t2,3) + 4831*Power(t2,4) + 420*Power(t2,5) + 
               Power(t1,3)*(-1641 + 4628*t2 - 2981*Power(t2,2)) + 
               Power(s2,3)*(10 - 94*t2 + 69*Power(t2,2)) + 
               Power(t1,2)*(-460 + 4569*t2 - 6799*Power(t2,2) + 
                  2811*Power(t2,3)) + 
               t1*(253 + 934*t2 - 5925*Power(t2,2) + 
                  8209*Power(t2,3) - 3378*Power(t2,4)) + 
               Power(s2,2)*(42 + 188*t2 - 225*Power(t2,2) + 
                  43*Power(t2,3) - 
                  2*t1*(124 - 599*t2 + 447*Power(t2,2))) + 
               s2*(400 - 3460*t2 + 6865*Power(t2,2) - 
                  3951*Power(t2,3) + 79*Power(t2,4) + 
                  Power(t1,2)*(1203 - 4124*t2 + 2876*Power(t2,2)) + 
                  t1*(-102 - 1013*t2 + 788*Power(t2,2) + 
                     146*Power(t2,3)))) + 
            Power(s1,2)*(204 - 849*t2 + 4058*Power(t2,2) - 
               13067*Power(t2,3) + 13925*Power(t2,4) - 
               2431*Power(t2,5) - 1705*Power(t2,6) + 
               2*Power(s2,3)*
                (-3 + 41*t2 - 96*Power(t2,2) + 52*Power(t2,3)) + 
               Power(t1,3)*(-1193 + 5376*t2 - 7128*Power(t2,2) + 
                  2760*Power(t2,3)) + 
               Power(t1,2)*(778 + 594*t2 - 6287*Power(t2,2) + 
                  7271*Power(t2,3) - 2642*Power(t2,4)) + 
               t1*(-78 - 749*t2 + 488*Power(t2,2) + 5112*Power(t2,3) - 
                  8675*Power(t2,4) + 3981*Power(t2,5)) + 
               Power(s2,2)*(160 - 1339*t2 + 2295*Power(t2,2) - 
                  999*Power(t2,3) + 83*Power(t2,4) + 
                  t1*(-55 + 532*t2 - 718*Power(t2,2) + 42*Power(t2,3))) \
+ s2*(-246 + 67*t2 + 6886*Power(t2,2) - 12773*Power(t2,3) + 
                  5188*Power(t2,4) + 500*Power(t2,5) + 
                  Power(t1,2)*
                   (405 - 2580*t2 + 3774*Power(t2,2) - 
                     1226*Power(t2,3)) - 
                  t1*(107 + 264*t2 + 570*Power(t2,2) - 
                     2836*Power(t2,3) + 1708*Power(t2,4)))) + 
            s1*(57 + (-2289 + 2488*s2 - 894*Power(s2,2) + 
                  36*Power(s2,3))*t2 + 
               (6103 - 7170*s2 + 4521*Power(s2,2) - 272*Power(s2,3))*
                Power(t2,2) + 
               (-7940 + 583*s2 - 5962*Power(s2,2) + 408*Power(s2,3))*
                Power(t2,3) + 
               (11342 + 7937*s2 + 1990*Power(s2,2) - 77*Power(s2,3))*
                Power(t2,4) - 
               (8569 + 2482*s2 + 63*Power(s2,2))*Power(t2,5) - 
               5*(40 + 203*s2)*Power(t2,6) + 1505*Power(t2,7) - 
               3*Power(t1,3)*
                (105 - 832*t2 + 1853*Power(t2,2) - 1528*Power(t2,3) + 
                  392*Power(t2,4)) + 
               Power(t1,2)*(637 - 2246*t2 + 1144*Power(t2,2) + 
                  2613*Power(t2,3) - 3386*Power(t2,4) + 
                  1169*Power(t2,5) + 
                  s2*(50 - 692*t2 + 1539*Power(t2,2) - 
                     670*Power(t2,3) - 238*Power(t2,4))) + 
               t1*(-333 + 1982*t2 - 1705*Power(t2,2) - 
                  2553*Power(t2,3) + 1439*Power(t2,4) + 
                  2852*Power(t2,5) - 1918*Power(t2,6) + 
                  Power(s2,2)*t2*
                   (220 - 553*t2 + 46*Power(t2,2) + 175*Power(t2,3)) + 
                  s2*(-52 - 1002*t2 + 2680*Power(t2,2) + 
                     566*Power(t2,3) - 3600*Power(t2,4) + 
                     1855*Power(t2,5))))) - 
         Power(s,5)*(4 + 30*t1 - 114*Power(t1,2) + 84*Power(t1,3) + 
            54*t2 - 102*s2*t2 - 332*t1*t2 + 344*s2*t1*t2 + 
            531*Power(t1,2)*t2 - 238*s2*Power(t1,2)*t2 - 
            308*Power(t1,3)*t2 + 4282*Power(t2,2) - 
            3441*s2*Power(t2,2) + 836*Power(s2,2)*Power(t2,2) - 
            24*Power(s2,3)*Power(t2,2) - 3644*t1*Power(t2,2) + 
            1402*s2*t1*Power(t2,2) - 278*Power(s2,2)*t1*Power(t2,2) + 
            199*Power(t1,2)*Power(t2,2) + 
            763*s2*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) - 
            13874*Power(t2,3) + 15002*s2*Power(t2,3) - 
            4689*Power(s2,2)*Power(t2,3) + 218*Power(s2,3)*Power(t2,3) + 
            10604*t1*Power(t2,3) - 6720*s2*t1*Power(t2,3) + 
            966*Power(s2,2)*t1*Power(t2,3) - 
            1783*Power(t1,2)*Power(t2,3) - 
            982*s2*Power(t1,2)*Power(t2,3) + 
            1142*Power(t1,3)*Power(t2,3) + 15696*Power(t2,4) - 
            20415*s2*Power(t2,4) + 8495*Power(s2,2)*Power(t2,4) - 
            492*Power(s2,3)*Power(t2,4) - 7045*t1*Power(t2,4) + 
            6911*s2*t1*Power(t2,4) - 879*Power(s2,2)*t1*Power(t2,4) + 
            1409*Power(t1,2)*Power(t2,4) + 
            361*s2*Power(t1,2)*Power(t2,4) - 
            1615*Power(t1,3)*Power(t2,4) - 8842*Power(t2,5) + 
            9189*s2*Power(t2,5) - 5682*Power(s2,2)*Power(t2,5) + 
            315*Power(s2,3)*Power(t2,5) - 1963*t1*Power(t2,5) - 
            1159*s2*t1*Power(t2,5) + 147*Power(s2,2)*t1*Power(t2,5) + 
            174*Power(t1,2)*Power(t2,5) + 
            315*s2*Power(t1,2)*Power(t2,5) + 
            791*Power(t1,3)*Power(t2,5) + 4130*Power(t2,6) - 
            180*s2*Power(t2,6) + 910*Power(s2,2)*Power(t2,6) + 
            2829*t1*Power(t2,6) - 1274*s2*t1*Power(t2,6) - 
            616*Power(t1,2)*Power(t2,6) - 
            182*s2*Power(t1,2)*Power(t2,6) - 
            112*Power(t1,3)*Power(t2,6) - 1108*Power(t2,7) + 
            497*s2*Power(t2,7) + 98*Power(s2,2)*Power(t2,7) - 
            315*t1*Power(t2,7) + 560*s2*t1*Power(t2,7) + 
            168*Power(t1,2)*Power(t2,7) - 644*Power(t2,8) - 
            546*s2*Power(t2,8) - 168*t1*Power(t2,8) + 304*Power(t2,9) + 
            2*Power(s1,7)*(7 - 16*t2 + 9*Power(t2,2)) + 
            Power(s1,6)*(150 + 40*Power(s2,3) - 168*Power(t1,3) - 
               504*t2 + 464*Power(t2,2) - 98*Power(t2,3) + 
               28*Power(t1,2)*(-7 + 4*t2) - 
               10*Power(s2,2)*(2 + 21*t1 + 5*t2) + 
               t1*(-38 - 58*t2 + 107*Power(t2,2)) + 
               s2*(-44 + 336*Power(t1,2) + 228*t2 - 195*Power(t2,2) - 
                  2*t1*(-85 + 7*t2))) + 
            Power(s1,5)*(306 + Power(s2,3)*(97 - 235*t2) - 1588*t2 + 
               2727*Power(t2,2) - 1531*Power(t2,3) + 41*Power(t2,4) + 
               14*Power(t1,3)*(-64 + 93*t2) + 
               Power(t1,2)*(-1098 + 2861*t2 - 1483*Power(t2,2)) + 
               t1*(-498 + 2195*t2 - 2201*Power(t2,2) + 
                  373*Power(t2,3)) + 
               Power(s2,2)*(-160 + 544*t2 - 203*Power(t2,2) + 
                  5*t1*(-133 + 255*t2)) + 
               s2*(248 - 814*t2 + 140*Power(t2,2) + 556*Power(t2,3) - 
                  9*Power(t1,2)*(-155 + 252*t2) + 
                  t1*(793 - 2332*t2 + 1085*Power(t2,2)))) + 
            Power(s1,4)*(74 - 1915*t2 + 6465*Power(t2,2) - 
               7204*Power(t2,3) + 1891*Power(t2,4) + 740*Power(t2,5) + 
               Power(s2,3)*(41 - 266*t2 + 252*Power(t2,2)) - 
               2*Power(t1,3)*(859 - 2522*t2 + 1726*Power(t2,2)) + 
               Power(t1,2)*(-1332 + 7350*t2 - 10258*Power(t2,2) + 
                  4333*Power(t2,3)) + 
               t1*(-217 + 4063*t2 - 11272*Power(t2,2) + 
                  10372*Power(t2,3) - 2740*Power(t2,4)) + 
               Power(s2,2)*(-65 + 800*t2 - 1287*Power(t2,2) + 
                  648*Power(t2,3) - 
                  2*t1*(268 - 1175*t2 + 978*Power(t2,2))) + 
               s2*(765 - 4245*t2 + 6494*Power(t2,2) - 
                  2360*Power(t2,3) - 845*Power(t2,4) + 
                  Power(t1,2)*(1779 - 6148*t2 + 4611*Power(t2,2)) + 
                  t1*(362 - 3460*t2 + 5288*Power(t2,2) - 
                     2396*Power(t2,3)))) + 
            Power(s1,3)*(-53 + 367*t2 + 3844*Power(t2,2) - 
               13086*Power(t2,3) + 10947*Power(t2,4) + 64*Power(t2,5) - 
               2100*Power(t2,6) + 
               Power(s2,3)*(-39 + 206*t2 - 249*Power(t2,2) + 
                  140*Power(t2,3)) + 
               Power(t1,3)*(-1627 + 7533*t2 - 10537*Power(t2,2) + 
                  4403*Power(t2,3)) + 
               Power(t1,2)*(75 + 4909*t2 - 15531*Power(t2,2) + 
                  15726*Power(t2,3) - 5530*Power(t2,4)) + 
               t1*(444 - 1083*t2 - 6030*Power(t2,2) + 
                  18563*Power(t2,3) - 17057*Power(t2,4) + 
                  5125*Power(t2,5)) + 
               Power(s2,2)*(479 - 2077*t2 + 1930*Power(t2,2) + 
                  58*Power(t2,3) - 436*Power(t2,4) + 
                  6*t1*(-24 + 196*t2 - 338*Power(t2,2) + 
                     103*Power(t2,3))) + 
               s2*(-234 - 2792*t2 + 13739*Power(t2,2) - 
                  16016*Power(t2,3) + 3655*Power(t2,4) + 
                  1615*Power(t2,5) + 
                  Power(t1,2)*
                   (883 - 5495*t2 + 8796*Power(t2,2) - 
                     3641*Power(t2,3)) + 
                  t1*(-314 + 475*t2 + 565*Power(t2,2) - 
                     880*Power(t2,3) + 576*Power(t2,4)))) - 
            Power(s1,2)*(-844 + 3882*t2 - 4759*Power(t2,2) + 
               7378*Power(t2,3) - 15292*Power(t2,4) + 
               9623*Power(t2,5) + 2483*Power(t2,6) - 2502*Power(t2,7) + 
               Power(s2,3)*(12 - 269*t2 + 1065*Power(t2,2) - 
                  1224*Power(t2,3) + 316*Power(t2,4)) + 
               Power(t1,3)*(689 - 5161*t2 + 11808*Power(t2,2) - 
                  10346*Power(t2,3) + 2910*Power(t2,4)) + 
               Power(t1,2)*(-892 + 1348*t2 + 5199*Power(t2,2) - 
                  13768*Power(t2,3) + 11821*Power(t2,4) - 
                  3595*Power(t2,5)) + 
               t1*(777 - 925*t2 - 3917*Power(t2,2) + 
                  3259*Power(t2,3) + 8347*Power(t2,4) - 
                  11615*Power(t2,5) + 4195*Power(t2,6)) + 
               Power(s2,2)*(-313 + 4022*t2 - 11638*Power(t2,2) + 
                  10923*Power(t2,3) - 2536*Power(t2,4) - 
                  48*Power(t2,5) + 
                  t1*(79 - 765*t2 + 1188*Power(t2,2) + 
                     188*Power(t2,3) - 630*Power(t2,4))) + 
               s2*(936 - 7219*t2 + 8471*Power(t2,2) + 
                  10075*Power(t2,3) - 16424*Power(t2,4) + 
                  1481*Power(t2,5) + 2510*Power(t2,6) + 
                  Power(t1,2)*
                   (-144 + 2125*t2 - 5736*Power(t2,2) + 
                     4530*Power(t2,3) - 636*Power(t2,4)) + 
                  t1*(-305 + 1627*t2 + 622*Power(t2,2) - 
                     7136*Power(t2,3) + 7066*Power(t2,4) - 
                     2380*Power(t2,5)))) + 
            s1*(52 - 4433*t2 + 15995*Power(t2,2) - 19520*Power(t2,3) + 
               13543*Power(t2,4) - 11276*Power(t2,5) + 
               4741*Power(t2,6) + 2271*Power(t2,7) - 1407*Power(t2,8) + 
               Power(s2,3)*t2*
                (36 - 448*t2 + 1314*Power(t2,2) - 1125*Power(t2,3) + 
                  119*Power(t2,4)) + 
               Power(t1,3)*(37 + 1013*t2 - 4841*Power(t2,2) + 
                  7619*Power(t2,3) - 4740*Power(t2,4) + 938*Power(t2,5)) \
+ Power(t1,2)*(152 - 1757*t2 + 2909*Power(t2,2) + 552*Power(t2,3) - 
                  4773*Power(t2,4) + 4321*Power(t2,5) - 1197*Power(t2,6)\
) + t1*(-182 + 4132*t2 - 10035*Power(t2,2) + 2872*Power(t2,3) + 
                  8161*Power(t2,4) - 4027*Power(t2,5) - 
                  2342*Power(t2,6) + 1498*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-1112 + 8183*t2 - 17972*Power(t2,2) + 
                  14036*Power(t2,3) - 2749*Power(t2,4) - 
                  105*Power(t2,5) + 
                  t1*(354 - 1596*t2 + 1434*Power(t2,2) + 
                     389*Power(t2,3) - 357*Power(t2,4))) + 
               s2*(20 + 4100*t2 - 21105*Power(t2,2) + 
                  30235*Power(t2,3) - 8692*Power(t2,4) - 
                  5869*Power(t2,5) - 683*Power(t2,6) + 
                  1925*Power(t2,7) + 
                  Power(t1,2)*
                   (46 - 571*t2 + 2114*Power(t2,2) - 2437*Power(t2,3) + 
                     198*Power(t2,4) + 504*Power(t2,5)) - 
                  t1*(78 + 1864*t2 - 8101*Power(t2,2) + 
                     6544*Power(t2,3) + 4021*Power(t2,4) - 
                     6109*Power(t2,5) + 2191*Power(t2,6))))) + 
         s*(s1 - t2)*(-1 + t2)*
          (Power(s1,8)*(s2 - t1)*
             (5*Power(s2,2) + 5*Power(t1,2) - 2*s2*(9 + 5*t1 - 9*t2) - 
               18*t1*(-1 + t2) + 6*Power(-1 + t2,2)) - 
            Power(-1 + t2,2)*
             (10 - 18*(-4 + s2)*t2 + 
               (-491 + 272*s2 - 24*Power(s2,2))*Power(t2,2) + 
               (898 - 904*s2 + 197*Power(s2,2) - 4*Power(s2,3))*
                Power(t2,3) + 
               (-655 + 1077*s2 - 433*Power(s2,2) + 33*Power(s2,3))*
                Power(t2,4) + 
               (164 - 426*s2 + 268*Power(s2,2) - 41*Power(s2,3))*
                Power(t2,5) + (5 - 8*s2 - 5*Power(s2,2))*Power(t2,6) + 
               (-8 + 4*s2 - 3*Power(s2,2))*Power(t2,7) + 
               (5 + 3*s2)*Power(t2,8) + 
               Power(t1,3)*(-10 + 12*t2 - 22*Power(t2,2) + 
                  47*Power(t2,3) - 16*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(30 - 6*(-8 + 3*s2)*t2 + 
                  (-329 + 130*s2)*Power(t2,2) + 
                  (318 - 163*s2)*Power(t2,3) + 
                  2*(-46 + 7*s2)*Power(t2,4) + 26*Power(t2,5) + 
                  (-1 + s2)*Power(t2,6)) + 
               t1*(-30 + 12*(-11 + 3*s2)*t2 + 
                  (842 - 402*s2 + 24*Power(s2,2))*Power(t2,2) + 
                  (-1235 + 967*s2 - 141*Power(s2,2))*Power(t2,3) + 
                  2*(299 - 337*s2 + 85*Power(s2,2))*Power(t2,4) + 
                  (-20 + 71*s2 - 17*Power(s2,2))*Power(t2,5) + 
                  (-26 + 6*s2)*Power(t2,6) + (3 - 4*s2)*Power(t2,7))) + 
            Power(s1,7)*(Power(s2,3)*(-10 + t2) + 8*Power(-1 + t2,3) + 
               Power(t1,3)*(-2 + 11*t2) + 
               2*t1*Power(-1 + t2,2)*(-13 + 16*t2) - 
               3*Power(t1,2)*(7 - 38*t2 + 31*Power(t2,2)) + 
               3*Power(s2,2)*
                (-1 + 26*t2 - 25*Power(t2,2) + 3*t1*(2 + t2)) + 
               s2*(-3*Power(t1,2)*(2 + 7*t2) - 
                  2*Power(-1 + t2,2)*(-11 + 14*t2) + 
                  24*t1*(1 - 8*t2 + 7*Power(t2,2)))) + 
            Power(s1,6)*(-2*Power(-1 + t2,3)*(1 + 18*t2) + 
               Power(s2,3)*(-32 + 97*t2 - 73*Power(t2,2)) - 
               4*Power(t1,3)*(3 - 6*t2 + Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(8 - 126*t2 + 79*Power(t2,2)) + 
               Power(t1,2)*(25 + 70*t2 - 314*Power(t2,2) + 
                  219*Power(t2,3)) + 
               Power(s2,2)*(97 - 186*t2 - 18*Power(t2,2) + 
                  107*Power(t2,3) + t1*(67 - 200*t2 + 157*Power(t2,2))) \
+ s2*(Power(t1,2)*(-23 + 79*t2 - 80*Power(t2,2)) + 
                  Power(-1 + t2,2)*(-34 - 52*t2 + 47*Power(t2,2)) - 
                  2*t1*(62 - 61*t2 - 163*Power(t2,2) + 162*Power(t2,3)))\
) + Power(s1,5)*(Power(-1 + t2,3)*(-22 + 35*t2 + 67*Power(t2,2)) + 
               2*Power(t1,3)*
                (-22 + 65*t2 - 55*Power(t2,2) + Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (81 + 22*t2 - 277*Power(t2,2) + 119*Power(t2,3)) + 
               Power(s2,3)*(-35 + 203*t2 - 325*Power(t2,2) + 
                  179*Power(t2,3)) + 
               Power(t1,2)*(69 - 266*t2 + 7*Power(t2,2) + 
                  490*Power(t2,3) - 300*Power(t2,4)) + 
               Power(s2,2)*(200 - 850*t2 + 1047*Power(t2,2) - 
                  362*Power(t2,3) - 35*Power(t2,4) + 
                  t1*(62 - 435*t2 + 750*Power(t2,2) - 443*Power(t2,3))) \
+ s2*(-(Power(-1 + t2,2)*(110 - 209*t2 + 26*Power(t2,2) + 
                       18*Power(t2,3))) + 
                  3*Power(t1,2)*
                   (7 + 30*t2 - 101*Power(t2,2) + 86*Power(t2,3)) + 
                  2*t1*(-151 + 612*t2 - 590*Power(t2,2) - 
                     34*Power(t2,3) + 163*Power(t2,4)))) - 
            Power(s1,4)*(Power(-1 + t2,3)*
                (11 - 60*t2 + 101*Power(t2,2) + 73*Power(t2,3)) + 
               Power(t1,3)*(39 - 254*t2 + 428*Power(t2,2) - 
                  234*Power(t2,3) + 18*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-253 + 603*t2 - 44*Power(t2,2) - 335*Power(t2,3) + 
                  116*Power(t2,4)) + 
               Power(s2,3)*(21 - 217*t2 + 593*Power(t2,2) - 
                  575*Power(t2,3) + 181*Power(t2,4)) + 
               Power(t1,2)*(104 + 23*t2 - 600*Power(t2,2) + 
                  267*Power(t2,3) + 456*Power(t2,4) - 250*Power(t2,5)) \
+ Power(s2,2)*(-236 + 1505*t2 - 3091*Power(t2,2) + 2470*Power(t2,3) - 
                  705*Power(t2,4) + 57*Power(t2,5) + 
                  t1*(33 + 198*t2 - 1105*Power(t2,2) + 
                     1412*Power(t2,3) - 547*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (282 - 665*t2 + 429*Power(t2,2) - 184*Power(t2,3) + 
                     51*Power(t2,4)) + 
                  Power(t1,2)*
                   (-100 + 324*t2 - 27*Power(t2,2) - 506*Power(t2,3) + 
                     318*Power(t2,4)) + 
                  t1*(147 - 1726*t2 + 4228*Power(t2,2) - 
                     3310*Power(t2,3) + 501*Power(t2,4) + 
                     160*Power(t2,5)))) + 
            Power(s1,3)*(2*Power(-1 + t2,3)*
                (-78 + 69*t2 - 13*Power(t2,2) + 55*Power(t2,3) + 
                  31*Power(t2,4)) + 
               Power(t1,3)*(43 - 17*t2 - 342*Power(t2,2) + 
                  542*Power(t2,3) - 235*Power(t2,4) + 22*Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (43 - 905*t2 + 1439*Power(t2,2) - 246*Power(t2,3) - 
                  210*Power(t2,4) + 70*Power(t2,5)) + 
               Power(s2,3)*(-4 + 104*t2 - 552*Power(t2,2) + 
                  996*Power(t2,3) - 636*Power(t2,4) + 79*Power(t2,5)) + 
               Power(t1,2)*(-214 + 1030*t2 - 1184*Power(t2,2) - 
                  59*Power(t2,3) + 299*Power(t2,4) + 251*Power(t2,5) - 
                  123*Power(t2,6)) + 
               Power(s2,2)*(137 - 1393*t2 + 4542*Power(t2,2) - 
                  6095*Power(t2,3) + 3308*Power(t2,4) - 
                  554*Power(t2,5) + 55*Power(t2,6) - 
                  t1*(81 - 409*t2 + 172*Power(t2,2) + 
                     1193*Power(t2,3) - 1449*Power(t2,4) + 
                     373*Power(t2,5))) + 
               s2*(Power(-1 + t2,2)*
                   (-425 + 1702*t2 - 1719*Power(t2,2) + 
                     365*Power(t2,3) - 202*Power(t2,4) + 88*Power(t2,5)\
) + Power(t1,2)*(14 - 425*t2 + 1086*Power(t2,2) - 551*Power(t2,3) - 
                     370*Power(t2,4) + 207*Power(t2,5)) + 
                  t1*(311 - 523*t2 - 2374*Power(t2,2) + 
                     6250*Power(t2,3) - 4449*Power(t2,4) + 
                     789*Power(t2,5) - 4*Power(t2,6)))) - 
            Power(s1,2)*(Power(s2,3)*t2*
                (-12 + 186*t2 - 704*Power(t2,2) + 980*Power(t2,3) - 
                  463*Power(t2,4) + 7*Power(t2,5)) + 
               Power(-1 + t2,3)*
                (246 - 762*t2 + 403*Power(t2,2) + 42*Power(t2,3) + 
                  44*Power(t2,4) + 46*Power(t2,5)) + 
               Power(t1,3)*(-45 + 281*t2 - 455*Power(t2,2) + 
                  106*Power(t2,3) + 209*Power(t2,4) - 99*Power(t2,5) + 
                  9*Power(t2,6)) + 
               t1*Power(-1 + t2,2)*
                (329 - 687*t2 - 576*Power(t2,2) + 1385*Power(t2,3) - 
                  338*Power(t2,4) - 46*Power(t2,5) + 23*Power(t2,6)) + 
               Power(t1,2)*(-38 - 352*t2 + 1729*Power(t2,2) - 
                  2179*Power(t2,3) + 721*Power(t2,4) + 
                  71*Power(t2,5) + 81*Power(t2,6) - 33*Power(t2,7)) + 
               Power(s2,2)*(-24 + 519*t2 - 2929*Power(t2,2) + 
                  6700*Power(t2,3) - 6806*Power(t2,4) + 
                  2721*Power(t2,5) - 188*Power(t2,6) + 7*Power(t2,7) + 
                  t1*(24 - 351*t2 + 1195*Power(t2,2) - 
                     1188*Power(t2,3) - 272*Power(t2,4) + 
                     753*Power(t2,5) - 143*Power(t2,6))) + 
               s2*(Power(-1 + t2,2)*
                   (218 - 1706*t2 + 3623*Power(t2,2) - 
                     2309*Power(t2,3) + 101*Power(t2,4) - 
                     80*Power(t2,5) + 63*Power(t2,6)) + 
                  Power(t1,2)*
                   (76 - 239*t2 - 242*Power(t2,2) + 1165*Power(t2,3) - 
                     773*Power(t2,4) - 77*Power(t2,5) + 72*Power(t2,6)) \
+ t1*(-294 + 2081*t2 - 4173*Power(t2,2) + 1464*Power(t2,3) + 
                     3306*Power(t2,4) - 2879*Power(t2,5) + 
                     547*Power(t2,6) - 52*Power(t2,7)))) + 
            s1*(-1 + t2)*(Power(s2,3)*Power(t2,2)*
                (12 - 132*t2 + 313*Power(t2,2) - 208*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(t1,3)*(-4 + 85*t2 - 288*Power(t2,2) + 
                  281*Power(t2,3) - 49*Power(t2,4) - 8*Power(t2,5) + 
                  Power(t2,6)) + 
               Power(-1 + t2,2)*
                (-80 + 643*t2 - 1095*Power(t2,2) + 442*Power(t2,3) + 
                  32*Power(t2,4) - Power(t2,5) + 23*Power(t2,6)) + 
               Power(t1,2)*(-72 + 397*t2 - 393*Power(t2,2) - 
                  253*Power(t2,3) + 296*Power(t2,4) + 20*Power(t2,5) + 
                  9*Power(t2,6) - 4*Power(t2,7)) + 
               t1*(156 - 1285*t2 + 3058*Power(t2,2) - 
                  2603*Power(t2,3) + 226*Power(t2,4) + 
                  624*Power(t2,5) - 185*Power(t2,6) + 6*Power(t2,7) + 
                  3*Power(t2,8)) + 
               Power(s2,2)*t2*
                (48 - 579*t2 + 2044*Power(t2,2) - 2750*Power(t2,3) + 
                  1273*Power(t2,4) - 27*Power(t2,5) - 9*Power(t2,6) + 
                  t1*(-48 + 411*t2 - 884*Power(t2,2) + 
                     494*Power(t2,3) + 106*Power(t2,4) - 25*Power(t2,5))\
) + s2*(18 - 508*t2 + 2675*Power(t2,2) - 5465*Power(t2,3) + 
                  4851*Power(t2,4) - 1553*Power(t2,5) - 16*Power(t2,6) - 
                  24*Power(t2,7) + 22*Power(t2,8) + 
                  Power(t1,2)*
                   (18 - 224*t2 + 470*Power(t2,2) - 83*Power(t2,3) - 
                     274*Power(t2,4) + 27*Power(t2,5) + 12*Power(t2,6)) \
- t1*(36 - 732*t2 + 2845*Power(t2,2) - 3668*Power(t2,3) + 
                     1132*Power(t2,4) + 506*Power(t2,5) - 
                     145*Power(t2,6) + 26*Power(t2,7))))) + 
         Power(s,4)*(-14 + 84*t1 - 138*Power(t1,2) + 68*Power(t1,3) + 
            2*Power(s1,8)*Power(-1 + t2,2) + 214*t2 - 112*s2*t2 - 
            847*t1*t2 + 270*s2*t1*t2 + 1121*Power(t1,2)*t2 - 
            154*s2*Power(t1,2)*t2 - 504*Power(t1,3)*t2 + 
            3211*Power(t2,2) - 2137*s2*Power(t2,2) + 
            436*Power(s2,2)*Power(t2,2) - 6*Power(s2,3)*Power(t2,2) - 
            2298*t1*Power(t2,2) + 1048*s2*t1*Power(t2,2) - 
            227*Power(s2,2)*t1*Power(t2,2) - 
            1667*Power(t1,2)*Power(t2,2) + 
            505*s2*Power(t1,2)*Power(t2,2) + 
            1177*Power(t1,3)*Power(t2,2) - 15404*Power(t2,3) + 
            14126*s2*Power(t2,3) - 3580*Power(s2,2)*Power(t2,3) + 
            128*Power(s2,3)*Power(t2,3) + 14199*t1*Power(t2,3) - 
            8222*s2*t1*Power(t2,3) + 1286*Power(s2,2)*t1*Power(t2,3) - 
            373*Power(t1,2)*Power(t2,3) - 
            806*s2*Power(t1,2)*Power(t2,3) - 
            993*Power(t1,3)*Power(t2,3) + 24958*Power(t2,4) - 
            30118*s2*Power(t2,4) + 9916*Power(s2,2)*Power(t2,4) - 
            550*Power(s2,3)*Power(t2,4) - 19709*t1*Power(t2,4) + 
            14368*s2*t1*Power(t2,4) - 2250*Power(s2,2)*t1*Power(t2,4) + 
            2241*Power(t1,2)*Power(t2,4) + 
            855*s2*Power(t1,2)*Power(t2,4) - 
            190*Power(t1,3)*Power(t2,4) - 18537*Power(t2,5) + 
            27498*s2*Power(t2,5) - 11695*Power(s2,2)*Power(t2,5) + 
            810*Power(s2,3)*Power(t2,5) + 8138*t1*Power(t2,5) - 
            9000*s2*t1*Power(t2,5) + 1375*Power(s2,2)*t1*Power(t2,5) - 
            1407*Power(t1,2)*Power(t2,5) - 
            290*s2*Power(t1,2)*Power(t2,5) + 
            772*Power(t1,3)*Power(t2,5) + 7246*Power(t2,6) - 
            9666*s2*Power(t2,6) + 5525*Power(s2,2)*Power(t2,6) - 
            385*Power(s2,3)*Power(t2,6) + 2430*t1*Power(t2,6) + 
            850*s2*t1*Power(t2,6) - 175*Power(s2,2)*t1*Power(t2,6) - 
            71*Power(t1,2)*Power(t2,6) - 
            231*s2*Power(t1,2)*Power(t2,6) - 
            371*Power(t1,3)*Power(t2,6) - 2135*Power(t2,7) + 
            674*s2*Power(t2,7) - 490*Power(s2,2)*Power(t2,7) - 
            2344*t1*Power(t2,7) + 1050*s2*t1*Power(t2,7) + 
            378*Power(t1,2)*Power(t2,7) + 
            112*s2*Power(t1,2)*Power(t2,7) + 
            44*Power(t1,3)*Power(t2,7) + 131*Power(t2,8) - 
            637*s2*Power(t2,8) - 112*Power(s2,2)*Power(t2,8) + 
            287*t1*Power(t2,8) - 364*s2*t1*Power(t2,8) - 
            84*Power(t1,2)*Power(t2,8) + 458*Power(t2,9) + 
            372*s2*Power(t2,9) + 60*t1*Power(t2,9) - 128*Power(t2,10) + 
            Power(s1,7)*(56 + 30*Power(s2,3) - 72*Power(t1,3) - 
               143*t2 + 92*Power(t2,2) - 5*Power(t2,3) + 
               28*Power(t1,2)*(-3 + 2*t2) - 
               2*Power(s2,2)*(10 + 63*t1 + 3*t2) + 
               2*s2*(-25 + 84*Power(t1,2) + t1*(48 - 21*t2) + 87*t2 - 
                  64*Power(t2,2)) + t1*(19 - 114*t2 + 99*Power(t2,2))) - 
            Power(s1,6)*(-74 + Power(t1,3)*(518 - 756*t2) + 394*t2 - 
               592*Power(t2,2) + 179*Power(t2,3) + 93*Power(t2,4) + 
               2*Power(s2,3)*(-59 + 120*t2) + 
               Power(s2,2)*(266 + t1*(610 - 1077*t2) - 758*t2 + 
                  411*Power(t2,2)) + 
               Power(t1,2)*(836 - 2065*t2 + 1128*Power(t2,2)) + 
               t1*(526 - 1581*t2 + 1057*Power(t2,2) + 39*Power(t2,3)) + 
               s2*(-262 + 609*t2 + 76*Power(t2,2) - 464*Power(t2,3) + 
                  Power(t1,2)*(-999 + 1582*t2) - 
                  2*t1*(467 - 1235*t2 + 677*Power(t2,2)))) + 
            Power(s1,5)*(66 - 709*t2 + 1722*Power(t2,2) - 
               1218*Power(t2,3) - 424*Power(t2,4) + 563*Power(t2,5) + 
               Power(t1,3)*(-1166 + 3526*t2 - 2519*Power(t2,2)) + 
               Power(s2,3)*(62 - 366*t2 + 375*Power(t2,2)) + 
               Power(t1,2)*(-1466 + 6833*t2 - 9504*Power(t2,2) + 
                  4189*Power(t2,3)) + 
               t1*(-821 + 5625*t2 - 11142*Power(t2,2) + 
                  7730*Power(t2,3) - 1300*Power(t2,4)) + 
               Power(s2,2)*(-187 + 1331*t2 - 2413*Power(t2,2) + 
                  1337*Power(t2,3) + 
                  t1*(-609 + 2572*t2 - 2250*Power(t2,2))) + 
               s2*(713 - 3361*t2 + 4679*Power(t2,2) - 
                  1226*Power(t2,3) - 897*Power(t2,4) + 
                  Power(t1,2)*(1570 - 5428*t2 + 4233*Power(t2,2)) + 
                  t1*(888 - 5401*t2 + 8734*Power(t2,2) - 
                     4341*Power(t2,3)))) + 
            Power(s1,4)*(-343 + 728*t2 + 1870*Power(t2,2) - 
               5146*Power(t2,3) + 2129*Power(t2,4) + 2222*Power(t2,5) - 
               1460*Power(t2,6) + 
               Power(s2,3)*(-96 + 298*t2 - 193*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,3)*(-1331 + 6374*t2 - 9317*Power(t2,2) + 
                  4135*Power(t2,3)) + 
               Power(t1,2)*(-681 + 7176*t2 - 18798*Power(t2,2) + 
                  18963*Power(t2,3) - 6890*Power(t2,4)) + 
               t1*(458 + 1984*t2 - 15259*Power(t2,2) + 
                  26946*Power(t2,3) - 17977*Power(t2,4) + 
                  3780*Power(t2,5)) + 
               Power(s2,2)*(612 - 1752*t2 + 341*Power(t2,2) + 
                  2081*Power(t2,3) - 1460*Power(t2,4) + 
                  t1*(-83 + 1252*t2 - 2753*Power(t2,2) + 
                     1255*Power(t2,3))) + 
               s2*(231 - 4426*t2 + 12896*Power(t2,2) - 
                  11924*Power(t2,3) + 1531*Power(t2,4) + 
                  1760*Power(t2,5) + 
                  Power(t1,2)*
                   (987 - 6164*t2 + 10326*Power(t2,2) - 
                     4770*Power(t2,3)) + 
                  t1*(-593 + 168*t2 + 5603*Power(t2,2) - 
                     9600*Power(t2,3) + 4830*Power(t2,4)))) + 
            Power(s1,3)*(578 - 308*t2 - 1949*Power(t2,2) - 
               2161*Power(t2,3) + 8561*Power(t2,4) - 2672*Power(t2,5) - 
               4142*Power(t2,6) + 2093*Power(t2,7) - 
               2*Power(s2,3)*
                (34 - 377*t2 + 1029*Power(t2,2) - 968*Power(t2,3) + 
                  290*Power(t2,4)) - 
               2*Power(t1,3)*
                (390 - 2869*t2 + 6733*Power(t2,2) - 6204*Power(t2,3) + 
                  1880*Power(t2,4)) + 
               Power(t1,2)*(224 + 2213*t2 - 12919*Power(t2,2) + 
                  24123*Power(t2,3) - 19691*Power(t2,4) + 
                  6030*Power(t2,5)) + 
               t1*(91 - 3763*t2 + 5916*Power(t2,2) + 
                  9723*Power(t2,3) - 26614*Power(t2,4) + 
                  19284*Power(t2,5) - 4635*Power(t2,6)) + 
               Power(s2,2)*(917 - 6963*t2 + 14972*Power(t2,2) - 
                  10909*Power(t2,3) + 1291*Power(t2,4) + 
                  590*Power(t2,5) + 
                  t1*(-133 + 516*t2 - 170*Power(t2,2) - 
                     1004*Power(t2,3) + 975*Power(t2,4))) + 
               s2*(-1723 + 6607*t2 - 331*Power(t2,2) - 
                  17875*Power(t2,3) + 14906*Power(t2,4) + 
                  1444*Power(t2,5) - 3030*Power(t2,6) + 
                  Power(t1,2)*
                   (260 - 3292*t2 + 9049*Power(t2,2) - 
                     8340*Power(t2,3) + 2015*Power(t2,4)) + 
                  t1*(238 + 2105*t2 - 9340*Power(t2,2) + 
                     9689*Power(t2,3) - 1700*Power(t2,4) - 
                     870*Power(t2,5)))) + 
            Power(s1,2)*(1365 - 9024*t2 + 16745*Power(t2,2) - 
               11257*Power(t2,3) + 6654*Power(t2,4) - 
               8725*Power(t2,5) + 1804*Power(t2,6) + 4143*Power(t2,7) - 
               1705*Power(t2,8) + 
               2*Power(s2,3)*
                (-3 + 129*t2 - 881*Power(t2,2) + 1899*Power(t2,3) - 
                  1410*Power(t2,4) + 220*Power(t2,5)) + 
               Power(t1,3)*(-23 + 1846*t2 - 8267*Power(t2,2) + 
                  13290*Power(t2,3) - 8720*Power(t2,4) + 
                  1892*Power(t2,5)) + 
               Power(t1,2)*(355 - 1062*t2 - 2144*Power(t2,2) + 
                  10358*Power(t2,3) - 15515*Power(t2,4) + 
                  11129*Power(t2,5) - 2936*Power(t2,6)) + 
               t1*(-1396 + 6001*t2 - 1985*Power(t2,2) - 
                  13213*Power(t2,3) + 10865*Power(t2,4) + 
                  6191*Power(t2,5) - 9241*Power(t2,6) + 
                  2791*Power(t2,7)) + 
               Power(s2,2)*(262 - 4848*t2 + 21444*Power(t2,2) - 
                  35425*Power(t2,3) + 22318*Power(t2,4) - 
                  3365*Power(t2,5) - 169*Power(t2,6) + 
                  t1*(-105 + 1240*t2 - 2799*Power(t2,2) + 
                     315*Power(t2,3) + 2890*Power(t2,4) - 
                     1344*Power(t2,5))) + 
               s2*(-1167 + 13286*t2 - 38207*Power(t2,2) + 
                  35286*Power(t2,3) + 258*Power(t2,4) - 
                  8763*Power(t2,5) - 3894*Power(t2,6) + 
                  3188*Power(t2,7) + 
                  Power(t1,2)*
                   (-41 - 784*t2 + 4548*Power(t2,2) - 
                     6488*Power(t2,3) + 2300*Power(t2,4) + 
                     342*Power(t2,5)) + 
                  t1*(778 - 5718*t2 + 7133*Power(t2,2) + 
                     8606*Power(t2,3) - 19097*Power(t2,4) + 
                     10164*Power(t2,5) - 2268*Power(t2,6)))) + 
            s1*(-36 - 4601*t2 + 23192*Power(t2,2) - 40262*Power(t2,3) + 
               30629*Power(t2,4) - 12917*Power(t2,5) + 
               6053*Power(t2,6) - 625*Power(t2,7) - 2166*Power(t2,8) + 
               733*Power(t2,9) + 
               Power(s2,3)*t2*
                (12 - 318*t2 + 1654*Power(t2,2) - 2910*Power(t2,3) + 
                  1710*Power(t2,4) - 105*Power(t2,5)) + 
               Power(t1,3)*(227 - 748*t2 - 305*Power(t2,2) + 
                  4060*Power(t2,3) - 5775*Power(t2,4) + 
                  2988*Power(t2,5) - 476*Power(t2,6)) + 
               Power(t1,2)*(-383 + 297*t2 + 1407*Power(t2,2) - 
                  1205*Power(t2,3) - 1984*Power(t2,4) + 
                  4305*Power(t2,5) - 3260*Power(t2,6) + 763*Power(t2,7)) \
+ t1*(208 + 4328*t2 - 19597*Power(t2,2) + 23775*Power(t2,3) - 
                  1471*Power(t2,4) - 12952*Power(t2,5) + 
                  5375*Power(t2,6) + 1088*Power(t2,7) - 756*Power(t2,8)) \
+ Power(s2,2)*t2*(-700 + 7517*t2 - 25015*Power(t2,2) + 
                  34089*Power(t2,3) - 18340*Power(t2,4) + 
                  2158*Power(t2,5) + 231*Power(t2,6) + 
                  t1*(334 - 2399*t2 + 4622*Power(t2,2) - 
                     2165*Power(t2,3) - 920*Power(t2,4) + 
                     413*Power(t2,5))) + 
               s2*(40 + 3392*t2 - 25575*Power(t2,2) + 
                  61277*Power(t2,3) - 58654*Power(t2,4) + 
                  17480*Power(t2,5) + 1087*Power(t2,6) + 
                  2684*Power(t2,7) - 1729*Power(t2,8) + 
                  Power(t1,2)*
                   (54 - 242*t2 + 1212*Power(t2,2) - 3136*Power(t2,3) + 
                     2365*Power(t2,4) + 366*Power(t2,5) - 
                     518*Power(t2,6)) + 
                  t1*(-98 - 2136*t2 + 13706*Power(t2,2) - 
                     22765*Power(t2,3) + 8550*Power(t2,4) + 
                     7436*Power(t2,5) - 6274*Power(t2,6) + 
                     1701*Power(t2,7))))) + 
         Power(s,2)*(2*Power(s1,9)*(s2 - t1)*
             (Power(s2,2) + t1 + Power(t1,2) - 3*Power(-1 + t2,2) - 
               t1*t2 + s2*(-1 - 2*t1 + t2)) - 
            Power(-1 + t2,2)*
             (4 + (-71 + 8*s2)*t2 + 
               (-439 + 181*s2 - 14*Power(s2,2))*Power(t2,2) + 
               (2748 - 1862*s2 + 285*Power(s2,2) - 2*Power(s2,3))*
                Power(t2,3) + 
               (-4741 + 5099*s2 - 1339*Power(s2,2) + 53*Power(s2,3))*
                Power(t2,4) + 
               (3320 - 5472*s2 + 2199*Power(s2,2) - 180*Power(s2,3))*
                Power(t2,5) + 
               (-855 + 2067*s2 - 1199*Power(s2,2) + 147*Power(s2,3))*
                Power(t2,6) + 
               (33 + 5*s2 + 46*Power(s2,2))*Power(t2,7) + 
               (36 + 7*s2 + 22*Power(s2,2))*Power(t2,8) - 
               (38 + 33*s2)*Power(t2,9) + 3*Power(t2,10) - 
               Power(t1,3)*(4 - 83*t2 + 244*Power(t2,2) - 
                  377*Power(t2,3) + 303*Power(t2,4) - 69*Power(t2,5) - 
                  5*Power(t2,6) + Power(t2,7)) + 
               Power(t1,2)*(12 + (-237 + 8*s2)*t2 + 
                  7*(29 + 9*s2)*Power(t2,2) + 
                  (664 - 354*s2)*Power(t2,3) + 
                  (-962 + 320*s2)*Power(t2,4) + 
                  (461 + 20*s2)*Power(t2,5) + 
                  (-136 + 7*s2)*Power(t2,6) - 
                  2*(4 + 5*s2)*Power(t2,7) + 3*Power(t2,8)) - 
               t1*(12 + (-225 + 16*s2)*t2 - 
                  2*(240 - 122*s2 + 7*Power(s2,2))*Power(t2,2) + 
                  (3737 - 2046*s2 + 215*Power(s2,2))*Power(t2,3) + 
                  (-5582 + 4154*s2 - 675*Power(s2,2))*Power(t2,4) + 
                  (2740 - 2766*s2 + 591*Power(s2,2))*Power(t2,5) + 
                  (5 + 361*s2 - 63*Power(s2,2))*Power(t2,6) + 
                  (-243 + 74*s2)*Power(t2,7) + 
                  (35 - 37*s2)*Power(t2,8) + Power(t2,9))) + 
            Power(s1,8)*(Power(s2,3)*(31 - 42*t2) - 
               4*(-3 + t2)*Power(-1 + t2,3) + 
               t1*Power(-1 + t2,2)*(-73 + 5*t2) + 
               Power(t1,3)*(-46 + 57*t2) + 
               Power(t1,2)*(-126 + 275*t2 - 149*Power(t2,2)) + 
               Power(s2,2)*(-102 + 227*t2 - 125*Power(t2,2) + 
                  3*t1*(-36 + 47*t2)) + 
               s2*(Power(-1 + t2,2)*(65 + 3*t2) - 
                  3*Power(t1,2)*(-41 + 52*t2) + 
                  t1*(228 - 502*t2 + 274*Power(t2,2)))) + 
            Power(s1,7)*(Power(t1,3)*
                (-109 + 352*t2 - 259*Power(t2,2)) + 
               Power(-1 + t2,3)*(45 - 107*t2 + 26*Power(t2,2)) + 
               Power(s2,3)*(-1 - 74*t2 + 91*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(234 - 641*t2 + 158*Power(t2,2)) + 
               Power(t1,2)*(-239 + 1197*t2 - 1775*Power(t2,2) + 
                  817*Power(t2,3)) + 
               Power(s2,2)*(-55 + 573*t2 - 1079*Power(t2,2) + 
                  561*Power(t2,3) + t1*(-80 + 446*t2 - 414*Power(t2,2))\
) + s2*(Power(-1 + t2,2)*(135 - 437*t2 + 53*Power(t2,2)) + 
                  2*Power(t1,2)*(95 - 362*t2 + 291*Power(t2,2)) - 
                  4*t1*(-69 + 429*t2 - 700*Power(t2,2) + 
                     340*Power(t2,3)))) + 
            Power(s1,6)*(-(Power(-1 + t2,3)*
                  (25 + 152*t2 - 342*Power(t2,2) + 79*Power(t2,3))) + 
               Power(s2,3)*(-85 + 272*t2 - 276*Power(t2,2) + 
                  92*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-132 + 1428*t2 - 2263*Power(t2,2) + 571*Power(t2,3)) + 
               Power(t1,3)*(-166 + 835*t2 - 1254*Power(t2,2) + 
                  582*Power(t2,3)) + 
               Power(t1,2)*(-98 + 1388*t2 - 4317*Power(t2,2) + 
                  5029*Power(t2,3) - 2002*Power(t2,4)) + 
               Power(s2,2)*(289 - 700*t2 - 305*Power(t2,2) + 
                  1721*Power(t2,3) - 1005*Power(t2,4) + 
                  3*t1*(46 - 69*t2 - 36*Power(t2,2) + 56*Power(t2,3))) \
- s2*(Power(-1 + t2,2)*(17 + 495*t2 - 964*Power(t2,2) + 
                     56*Power(t2,3)) + 
                  3*Power(t1,2)*
                   (-35 + 292*t2 - 538*Power(t2,2) + 278*Power(t2,3)) + 
                  t1*(330 + 130*t2 - 3782*Power(t2,2) + 
                     6188*Power(t2,3) - 2866*Power(t2,4)))) - 
            Power(s1,5)*(-(Power(-1 + t2,3)*
                  (67 + 104*t2 + 55*Power(t2,2) - 576*Power(t2,3) + 
                    148*Power(t2,4))) + 
               2*Power(t1,3)*
                (98 - 627*t2 + 1422*Power(t2,2) - 1312*Power(t2,3) + 
                  408*Power(t2,4)) + 
               Power(s2,3)*(105 - 746*t2 + 1596*Power(t2,2) - 
                  1448*Power(t2,3) + 515*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-477 + 167*t2 + 3329*Power(t2,2) - 4250*Power(t2,3) + 
                  1051*Power(t2,4)) + 
               Power(t1,2)*(48 - 365*t2 + 3040*Power(t2,2) - 
                  7912*Power(t2,3) + 7962*Power(t2,4) - 
                  2773*Power(t2,5)) + 
               Power(s2,2)*(-638 + 3599*t2 - 6478*Power(t2,2) + 
                  3922*Power(t2,3) + 422*Power(t2,4) - 
                  827*Power(t2,5) + 
                  t1*(-133 + 1200*t2 - 2676*Power(t2,2) + 
                     2518*Power(t2,3) - 975*Power(t2,4))) + 
               s2*(-6*Power(t1,2)*
                   (21 - 108*t2 + 260*Power(t2,2) - 239*Power(t2,3) + 
                     55*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (350 - 473*t2 - 604*Power(t2,2) + 654*Power(t2,3) + 
                     253*Power(t2,4)) + 
                  t1*(800 - 4751*t2 + 7216*Power(t2,2) - 
                     342*Power(t2,3) - 6036*Power(t2,4) + 
                     3113*Power(t2,5)))) + 
            Power(s1,4)*(-(Power(-1 + t2,3)*
                  (176 + 272*t2 - Power(t2,2) - 353*Power(t2,3) - 
                    639*Power(t2,4) + 185*Power(t2,5))) + 
               Power(s2,3)*(-39 + 648*t2 - 2632*Power(t2,2) + 
                  4156*Power(t2,3) - 2800*Power(t2,4) + 678*Power(t2,5)\
) + Power(t1,3)*(-55 + 957*t2 - 3514*Power(t2,2) + 5222*Power(t2,3) - 
                  3373*Power(t2,4) + 752*Power(t2,5)) + 
               t1*Power(-1 + t2,2)*
                (476 - 3381*t2 + 3333*Power(t2,2) + 3194*Power(t2,3) - 
                  4520*Power(t2,4) + 1144*Power(t2,5)) + 
               Power(t1,2)*(-391 + 1596*t2 - 2309*Power(t2,2) + 
                  4116*Power(t2,3) - 8277*Power(t2,4) + 
                  7597*Power(t2,5) - 2332*Power(t2,6)) + 
               Power(s2,2)*(599 - 5482*t2 + 16658*Power(t2,2) - 
                  21760*Power(t2,3) + 12031*Power(t2,4) - 
                  1809*Power(t2,5) - 237*Power(t2,6) + 
                  t1*(-153 + 74*t2 + 2700*Power(t2,2) - 
                     6820*Power(t2,3) + 6065*Power(t2,4) - 
                     1899*Power(t2,5))) + 
               s2*(Power(t1,2)*
                   (125 - 1116*t2 + 2484*Power(t2,2) - 
                     1830*Power(t2,3) - 104*Power(t2,4) + 
                     474*Power(t2,5)) + 
                  Power(-1 + t2,2)*
                   (-1134 + 3980*t2 - 3009*Power(t2,2) - 
                     277*Power(t2,3) - 582*Power(t2,4) + 
                     776*Power(t2,5)) + 
                  t1*(248 + 2876*t2 - 16289*Power(t2,2) + 
                     25944*Power(t2,3) - 13754*Power(t2,4) - 
                     614*Power(t2,5) + 1589*Power(t2,6)))) - 
            Power(s1,3)*(-(Power(-1 + t2,3)*
                  (-825 + 2176*t2 - 242*Power(t2,2) - 
                    379*Power(t2,3) - 543*Power(t2,4) - 
                    563*Power(t2,5) + 154*Power(t2,6))) + 
               Power(s2,3)*(2 - 174*t2 + 1602*Power(t2,2) - 
                  4830*Power(t2,3) + 6071*Power(t2,4) - 
                  3078*Power(t2,5) + 403*Power(t2,6)) + 
               Power(t1,3)*(-122 + 358*t2 + 856*Power(t2,2) - 
                  4062*Power(t2,3) + 5121*Power(t2,4) - 
                  2584*Power(t2,5) + 437*Power(t2,6)) + 
               t1*Power(-1 + t2,2)*
                (798 - 378*t2 - 6341*Power(t2,2) + 8012*Power(t2,3) + 
                  75*Power(t2,4) - 2573*Power(t2,5) + 740*Power(t2,6)) \
+ Power(t1,2)*(97 - 1889*t2 + 6054*Power(t2,2) - 7082*Power(t2,3) + 
                  4882*Power(t2,4) - 5304*Power(t2,5) + 
                  4441*Power(t2,6) - 1199*Power(t2,7)) + 
               Power(s2,2)*(-205 + 3462*t2 - 17514*Power(t2,2) + 
                  38086*Power(t2,3) - 38862*Power(t2,4) + 
                  17233*Power(t2,5) - 2225*Power(t2,6) + 
                  25*Power(t2,7) - 
                  3*t1*(-45 + 440*t2 - 970*Power(t2,2) - 
                     160*Power(t2,3) + 2325*Power(t2,4) - 
                     2122*Power(t2,5) + 528*Power(t2,6))) + 
               s2*(Power(t1,2)*
                   (119 + 128*t2 - 2670*Power(t2,2) + 
                     5076*Power(t2,3) - 2327*Power(t2,4) - 
                     1032*Power(t2,5) + 694*Power(t2,6)) + 
                  Power(-1 + t2,2)*
                   (1015 - 7462*t2 + 14877*Power(t2,2) - 
                     8372*Power(t2,3) - 137*Power(t2,4) - 
                     1227*Power(t2,5) + 973*Power(t2,6)) - 
                  2*t1*(482 - 2738*t2 + 2440*Power(t2,2) + 
                     8163*Power(t2,3) - 17398*Power(t2,4) + 
                     11134*Power(t2,5) - 2106*Power(t2,6) + 
                     23*Power(t2,7)))) - 
            s1*(-1 + t2)*(Power(s2,3)*Power(t2,2)*
                (-6 + 204*t2 - 993*Power(t2,2) + 1591*Power(t2,3) - 
                  821*Power(t2,4) + 7*Power(t2,5)) + 
               Power(t1,3)*(65 - 395*t2 + 1182*Power(t2,2) - 
                  1756*Power(t2,3) + 975*Power(t2,4) + 
                  95*Power(t2,5) - 170*Power(t2,6) + 22*Power(t2,7)) + 
               Power(t1,2)*(-183 + 265*t2 + 911*Power(t2,2) - 
                  1598*Power(t2,3) + 327*Power(t2,4) + 
                  100*Power(t2,5) + 8*Power(t2,6) + 227*Power(t2,7) - 
                  57*Power(t2,8)) - 
               Power(-1 + t2,2)*
                (53 + 939*t2 - 5096*Power(t2,2) + 6777*Power(t2,3) - 
                  2221*Power(t2,4) - 97*Power(t2,5) - 53*Power(t2,6) - 
                  178*Power(t2,7) + 24*Power(t2,8)) + 
               t1*(171 + 963*t2 - 8858*Power(t2,2) + 
                  19628*Power(t2,3) - 16239*Power(t2,4) + 
                  1938*Power(t2,5) + 3751*Power(t2,6) - 
                  1426*Power(t2,7) + 31*Power(t2,8) + 41*Power(t2,9)) + 
               Power(s2,2)*t2*
                (-28 + 803*t2 - 5307*Power(t2,2) + 13481*Power(t2,3) - 
                  14802*Power(t2,4) + 6185*Power(t2,5) - 
                  257*Power(t2,6) - 75*Power(t2,7) + 
                  t1*(28 - 593*t2 + 2659*Power(t2,2) - 
                     4016*Power(t2,3) + 1696*Power(t2,4) + 
                     397*Power(t2,5) - 117*Power(t2,6))) + 
               s2*(8 + 360*t2 - 5075*Power(t2,2) + 20057*Power(t2,3) - 
                  34249*Power(t2,4) + 26738*Power(t2,5) - 
                  7739*Power(t2,6) + 76*Power(t2,7) - 405*Power(t2,8) + 
                  229*Power(t2,9) + 
                  Power(t1,2)*
                   (8 + 124*t2 - 927*Power(t2,2) + 1277*Power(t2,3) + 
                     273*Power(t2,4) - 851*Power(t2,5) - 
                     54*Power(t2,6) + 96*Power(t2,7)) - 
                  t1*(16 + 484*t2 - 5492*Power(t2,2) + 
                     16268*Power(t2,3) - 17948*Power(t2,4) + 
                     5333*Power(t2,5) + 2225*Power(t2,6) - 
                     1125*Power(t2,7) + 239*Power(t2,8)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (6 - 288*t2 + 1968*Power(t2,2) - 4883*Power(t2,3) + 
                  5178*Power(t2,4) - 2088*Power(t2,5) + 104*Power(t2,6)) \
- Power(-1 + t2,3)*(429 - 3685*t2 + 6278*Power(t2,2) - 
                  1847*Power(t2,3) - 430*Power(t2,4) - 
                  296*Power(t2,5) - 396*Power(t2,6) + 81*Power(t2,7)) + 
               Power(t1,3)*(104 - 803*t2 + 2058*Power(t2,2) - 
                  1570*Power(t2,3) - 1198*Power(t2,4) + 
                  2340*Power(t2,5) - 1072*Power(t2,6) + 144*Power(t2,7)) \
+ t1*Power(-1 + t2,2)*(-600 + 4791*t2 - 7194*Power(t2,2) - 
                  1842*Power(t2,3) + 7370*Power(t2,4) - 
                  2095*Power(t2,5) - 571*Power(t2,6) + 263*Power(t2,7)) \
+ Power(t1,2)*(67 - 372*t2 - 1100*Power(t2,2) + 5588*Power(t2,3) - 
                  6815*Power(t2,4) + 3474*Power(t2,5) - 
                  2029*Power(t2,6) + 1549*Power(t2,7) - 362*Power(t2,8)) \
- Power(s2,2)*(-14 + 723*t2 - 7050*Power(t2,2) + 26296*Power(t2,3) - 
                  46085*Power(t2,4) + 39169*Power(t2,5) - 
                  14131*Power(t2,6) + 1041*Power(t2,7) + 
                  51*Power(t2,8) + 
                  t1*(14 - 513*t2 + 3300*Power(t2,2) - 
                     7222*Power(t2,3) + 4950*Power(t2,4) + 
                     1962*Power(t2,5) - 3166*Power(t2,6) + 
                     666*Power(t2,7))) + 
               s2*(Power(t1,2)*
                   (-69 + 698*t2 - 1110*Power(t2,2) - 
                     1650*Power(t2,3) + 4546*Power(t2,4) - 
                     2172*Power(t2,5) - 630*Power(t2,6) + 
                     378*Power(t2,7)) + 
                  Power(-1 + t2,2)*
                   (-187 + 3860*t2 - 16579*Power(t2,2) + 
                     24674*Power(t2,3) - 11591*Power(t2,4) - 
                     199*Power(t2,5) - 752*Power(t2,6) + 652*Power(t2,7)\
) - 2*t1*(-128 + 2211*t2 - 9249*Power(t2,2) + 14026*Power(t2,3) - 
                     4388*Power(t2,4) - 8114*Power(t2,5) + 
                     7338*Power(t2,6) - 1946*Power(t2,7) + 
                     250*Power(t2,8))))) + 
         Power(s,3)*(-(Power(s1,8)*
               (12*Power(s2,3) - 18*Power(t1,3) - 
                 2*Power(s2,2)*(5 + 21*t1 - 3*t2) + 
                 2*Power(-1 + t2,2)*(5 + t2) + 
                 4*Power(t1,2)*(-5 + 4*t2) + 
                 s2*(-27 + 48*Power(t1,2) + t1*(30 - 22*t2) + 70*t2 - 
                    43*Power(t2,2)) + t1*(23 - 62*t2 + 39*Power(t2,2)))) \
+ (-1 + t2)*(-14 + (187 - 50*s2)*t2 + 
               (1526 - 824*s2 + 121*Power(s2,2))*Power(t2,2) + 
               (-8342 + 6661*s2 - 1377*Power(s2,2) + 31*Power(s2,3))*
                Power(t2,3) + 
               (13820 - 15897*s2 + 4717*Power(s2,2) - 241*Power(s2,3))*
                Power(t2,4) + 
               (-9755 + 15674*s2 - 6377*Power(s2,2) + 499*Power(s2,3))*
                Power(t2,5) + 
               (2970 - 5730*s2 + 3178*Power(s2,2) - 301*Power(s2,3))*
                Power(t2,6) + 
               (-490 + 189*s2 - 194*Power(s2,2))*Power(t2,7) - 
               (28 + 173*s2 + 68*Power(s2,2))*Power(t2,8) + 
               6*(26 + 25*s2)*Power(t2,9) - 30*Power(t2,10) + 
               Power(t1,3)*(26 - 286*t2 + 825*Power(t2,2) - 
                  1076*Power(t2,3) + 576*Power(t2,4) + 20*Power(t2,5) - 
                  83*Power(t2,6) + 10*Power(t2,7)) - 
               Power(t1,2)*(66 + (-741 + 54*s2)*t2 + 
                  (1074 - 39*s2)*Power(t2,2) + 
                  (494 - 186*s2)*Power(t2,3) + 
                  4*(-416 + 5*s2)*Power(t2,4) + 
                  (1069 + 170*s2)*Power(t2,5) + 
                  (-224 + 61*s2)*Power(t2,6) - 
                  2*(49 + 22*s2)*Power(t2,7) + 24*Power(t2,8)) + 
               t1*(54 + 2*(-321 + 52*s2)*t2 + 
                  (-1205 + 685*s2 - 91*Power(s2,2))*Power(t2,2) + 
                  (9260 - 5340*s2 + 736*Power(s2,2))*Power(t2,3) + 
                  (-13583 + 9854*s2 - 1628*Power(s2,2))*Power(t2,4) + 
                  2*(3197 - 3179*s2 + 576*Power(s2,2))*Power(t2,5) + 
                  (569 + 833*s2 - 133*Power(s2,2))*Power(t2,6) + 
                  2*(-504 + 187*s2)*Power(t2,7) + 
                  (149 - 152*s2)*Power(t2,8) + 12*Power(t2,9))) + 
            Power(s1,7)*(-6*Power(t1,3)*(-33 + 46*t2) + 
               Power(s2,3)*(-82 + 139*t2) + 
               Power(-1 + t2,2)*(27 - 8*t2 + 39*Power(t2,2)) + 
               Power(s2,2)*(231 + t1*(341 - 533*t2) - 577*t2 + 
                  331*Power(t2,2)) + 
               Power(t1,2)*(415 - 971*t2 + 541*Power(t2,2)) + 
               t1*(282 - 649*t2 + 315*Power(t2,2) + 52*Power(t2,3)) + 
               s2*(-190 + 367*t2 - 27*Power(t2,2) - 150*Power(t2,3) + 
                  Power(t1,2)*(-457 + 670*t2) - 
                  2*t1*(312 - 752*t2 + 425*Power(t2,2)))) + 
            Power(s1,6)*(Power(s2,3)*(-37 + 256*t2 - 276*Power(t2,2)) + 
               2*Power(t1,3)*(247 - 763*t2 + 557*Power(t2,2)) + 
               Power(t1,2)*(839 - 3816*t2 + 5441*Power(t2,2) - 
                  2479*Power(t2,3)) - 
               Power(-1 + t2,2)*
                (-28 + 215*t2 - 253*Power(t2,2) + 200*Power(t2,3)) + 
               t1*(697 - 3657*t2 + 6002*Power(t2,2) - 
                  3483*Power(t2,3) + 441*Power(t2,4)) + 
               Power(s2,2)*(158 - 1211*t2 + 2292*Power(t2,2) - 
                  1254*Power(t2,3) + 
                  2*t1*(181 - 782*t2 + 699*Power(t2,2))) + 
               s2*(-388 + 1941*t2 - 2778*Power(t2,2) + 947*Power(t2,3) + 
                  278*Power(t2,4) + 
                  Power(t1,2)*(-801 + 2798*t2 - 2218*Power(t2,2)) + 
                  t1*(-768 + 4312*t2 - 6990*Power(t2,2) + 
                     3476*Power(t2,3)))) + 
            Power(s1,5)*(Power(t1,3)*
                (648 - 3216*t2 + 4836*Power(t2,2) - 2230*Power(t2,3)) + 
               Power(s2,3)*(121 - 325*t2 + 219*Power(t2,2) - 
                  55*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (101 + 18*t2 + 249*Power(t2,2) - 783*Power(t2,3) + 
                  535*Power(t2,4)) + 
               Power(t1,2)*(530 - 4741*t2 + 12491*Power(t2,2) - 
                  13273*Power(t2,3) + 5059*Power(t2,4)) + 
               t1*(59 - 3620*t2 + 13871*Power(t2,2) - 
                  19172*Power(t2,3) + 10614*Power(t2,4) - 
                  1752*Power(t2,5)) + 
               Power(s2,2)*(-496 + 1153*t2 + 561*Power(t2,2) - 
                  2975*Power(t2,3) + 1823*Power(t2,4) - 
                  t1*(89 + 511*t2 - 1693*Power(t2,2) + 975*Power(t2,3))) \
+ s2*(-234 + 2469*t2 - 6647*Power(t2,2) + 6430*Power(t2,3) - 
                  1323*Power(t2,4) - 695*Power(t2,5) + 
                  Power(t1,2)*
                   (-550 + 3648*t2 - 6330*Power(t2,2) + 
                     3116*Power(t2,3)) + 
                  t1*(566 + 523*t2 - 7593*Power(t2,2) + 
                     12125*Power(t2,3) - 5753*Power(t2,4)))) + 
            Power(s1,4)*(Power(s2,3)*
                (129 - 1035*t2 + 2303*Power(t2,2) - 1991*Power(t2,3) + 
                  660*Power(t2,4)) + 
               Power(t1,3)*(507 - 3639*t2 + 8637*Power(t2,2) - 
                  8195*Power(t2,3) + 2600*Power(t2,4)) + 
               Power(t1,2)*(269 - 2613*t2 + 10623*Power(t2,2) - 
                  20133*Power(t2,3) + 17506*Power(t2,4) - 
                  5670*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (-162 + 1008*t2 + 49*Power(t2,2) - 591*Power(t2,3) - 
                  1282*Power(t2,4) + 880*Power(t2,5)) + 
               t1*(-946 + 3930*t2 + 661*Power(t2,2) - 
                  19355*Power(t2,3) + 28614*Power(t2,4) - 
                  15779*Power(t2,5) + 2875*Power(t2,6)) + 
               Power(s2,2)*(-1067 + 6433*t2 - 11903*Power(t2,2) + 
                  7379*Power(t2,3) + 290*Power(t2,4) - 
                  1150*Power(t2,5) + 
                  t1*(-21 + 751*t2 - 2051*Power(t2,2) + 
                     2149*Power(t2,3) - 1050*Power(t2,4))) - 
               s2*(-1052 + 2572*t2 + 2497*Power(t2,2) - 
                  10483*Power(t2,3) + 6764*Power(t2,4) + 
                  1387*Power(t2,5) - 1685*Power(t2,6) + 
                  Power(t1,2)*
                   (275 - 2455*t2 + 6573*Power(t2,2) - 
                     6449*Power(t2,3) + 1810*Power(t2,4)) + 
                  t1*(-645 + 6045*t2 - 11865*Power(t2,2) + 
                     3759*Power(t2,3) + 6860*Power(t2,4) - 
                     4190*Power(t2,5)))) + 
            Power(s1,3)*(Power(s2,3)*
                (27 - 629*t2 + 3083*Power(t2,2) - 5403*Power(t2,3) + 
                  3654*Power(t2,4) - 723*Power(t2,5)) - 
               2*Power(t1,3)*
                (-29 + 861*t2 - 3659*Power(t2,2) + 5867*Power(t2,3) - 
                  3956*Power(t2,4) + 909*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (-1151 + 1829*t2 + 1547*Power(t2,2) - 240*Power(t2,3) - 
                  1558*Power(t2,4) - 1474*Power(t2,5) + 917*Power(t2,6)) \
+ Power(t1,2)*(110 - 1859*t2 + 6553*Power(t2,2) - 13121*Power(t2,3) + 
                  17904*Power(t2,4) - 13349*Power(t2,5) + 
                  3711*Power(t2,6)) + 
               t1*(687 + 1537*t2 - 13368*Power(t2,2) + 
                  17648*Power(t2,3) + 2597*Power(t2,4) - 
                  18824*Power(t2,5) + 12219*Power(t2,6) - 
                  2496*Power(t2,7)) + 
               Power(s2,2)*(-660 + 7519*t2 - 25871*Power(t2,2) + 
                  36183*Power(t2,3) - 20182*Power(t2,4) + 
                  2679*Power(t2,5) + 281*Power(t2,6) + 
                  t1*(227 - 1151*t2 + 163*Power(t2,2) + 
                     4665*Power(t2,3) - 6041*Power(t2,4) + 
                     2133*Power(t2,5))) + 
               s2*(2026 - 13705*t2 + 28670*Power(t2,2) - 
                  19058*Power(t2,3) - 3434*Power(t2,4) + 
                  3118*Power(t2,5) + 4851*Power(t2,6) - 
                  2468*Power(t2,7) + 
                  Power(t1,2)*
                   (32 + 1194*t2 - 5110*Power(t2,2) + 
                     6572*Power(t2,3) - 2575*Power(t2,4) - 
                     132*Power(t2,5)) + 
                  t1*(-1151 + 2399*t2 + 9663*Power(t2,2) - 
                     29435*Power(t2,3) + 23878*Power(t2,4) - 
                     4880*Power(t2,5) - 372*Power(t2,6)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (-85 + 1143*t2 - 4285*Power(t2,2) + 6182*Power(t2,3) - 
                  3300*Power(t2,4) + 316*Power(t2,5)) + 
               Power(t1,3)*(-211 + 717*t2 + 753*Power(t2,2) - 
                  5695*Power(t2,3) + 7950*Power(t2,4) - 
                  4218*Power(t2,5) + 726*Power(t2,6)) - 
               Power(-1 + t2,2)*
                (1076 - 7377*t2 + 10492*Power(t2,2) - 
                  1949*Power(t2,3) - 51*Power(t2,4) - 
                  1358*Power(t2,5) - 1213*Power(t2,6) + 584*Power(t2,7)) \
- Power(t1,2)*(24 + 37*t2 - 2507*Power(t2,2) + 6851*Power(t2,3) - 
                  8760*Power(t2,4) + 8806*Power(t2,5) - 
                  5891*Power(t2,6) + 1411*Power(t2,7)) + 
               t1*(1239 - 8893*t2 + 15783*Power(t2,2) + 
                  251*Power(t2,3) - 22433*Power(t2,4) + 
                  15901*Power(t2,5) + 1338*Power(t2,6) - 
                  4329*Power(t2,7) + 1143*Power(t2,8)) - 
               Power(s2,2)*(97 - 2744*t2 + 17853*Power(t2,2) - 
                  45491*Power(t2,3) + 51612*Power(t2,4) - 
                  24024*Power(t2,5) + 2522*Power(t2,6) + 
                  146*Power(t2,7) + 
                  t1*(-67 + 1207*t2 - 4651*Power(t2,2) + 
                     4973*Power(t2,3) + 1660*Power(t2,4) - 
                     4504*Power(t2,5) + 1302*Power(t2,6))) + 
               s2*(683 - 10669*t2 + 45248*Power(t2,2) - 
                  79091*Power(t2,3) + 58714*Power(t2,4) - 
                  12447*Power(t2,5) + 197*Power(t2,6) - 
                  4655*Power(t2,7) + 2020*Power(t2,8) + 
                  Power(t1,2)*
                   (96 - 345*t2 - 1557*Power(t2,2) + 5553*Power(t2,3) - 
                     4302*Power(t2,4) - 198*Power(t2,5) + 
                     680*Power(t2,6)) + 
                  t1*(-679 + 7593*t2 - 20163*Power(t2,2) + 
                     11241*Power(t2,3) + 18374*Power(t2,4) - 
                     23526*Power(t2,5) + 8502*Power(t2,6) - 
                     1400*Power(t2,7)))) + 
            s1*(Power(s2,3)*Power(t2,2)*
                (89 - 915*t2 + 2856*Power(t2,2) - 3520*Power(t2,3) + 
                  1545*Power(t2,4) - 49*Power(t2,5)) + 
               Power(t1,3)*(-177 + 1077*t2 - 2502*Power(t2,2) + 
                  2102*Power(t2,3) + 775*Power(t2,4) - 
                  2223*Power(t2,5) + 1086*Power(t2,6) - 144*Power(t2,7)) \
+ Power(-1 + t2,2)*(90 + 2786*t2 - 12796*Power(t2,2) + 
                  15661*Power(t2,3) - 5165*Power(t2,4) + 
                  584*Power(t2,5) - 550*Power(t2,6) - 599*Power(t2,7) + 
                  205*Power(t2,8)) + 
               Power(t1,2)*(426 - 1110*t2 + 127*Power(t2,2) + 
                  1113*Power(t2,3) - 169*Power(t2,4) - 
                  1110*Power(t2,5) + 1821*Power(t2,6) - 
                  1387*Power(t2,7) + 289*Power(t2,8)) + 
               t1*(-339 - 2429*t2 + 18909*Power(t2,2) - 
                  38925*Power(t2,3) + 28850*Power(t2,4) + 
                  1275*Power(t2,5) - 11220*Power(t2,6) + 
                  3871*Power(t2,7) + 244*Power(t2,8) - 236*Power(t2,9)) + 
               Power(s2,2)*t2*
                (218 - 3582*t2 + 17495*Power(t2,2) - 36651*Power(t2,3) + 
                  35576*Power(t2,4) - 14174*Power(t2,5) + 
                  929*Power(t2,6) + 189*Power(t2,7) + 
                  t1*(-158 + 1807*t2 - 5843*Power(t2,2) + 
                     6928*Power(t2,3) - 2090*Power(t2,4) - 
                     949*Power(t2,5) + 287*Power(t2,6))) + 
               s2*(-30 - 1513*t2 + 16172*Power(t2,2) - 
                  55149*Power(t2,3) + 84782*Power(t2,4) - 
                  60640*Power(t2,5) + 16213*Power(t2,6) - 
                  959*Power(t2,7) + 1987*Power(t2,8) - 863*Power(t2,9) + 
                  Power(t1,2)*
                   (-34 - 59*t2 + 504*Power(t2,2) + 436*Power(t2,3) - 
                     2514*Power(t2,4) + 1569*Power(t2,5) + 
                     418*Power(t2,6) - 302*Power(t2,7)) + 
                  t1*(64 + 1372*t2 - 12555*Power(t2,2) + 
                     32305*Power(t2,3) - 31605*Power(t2,4) + 
                     6624*Power(t2,5) + 6853*Power(t2,6) - 
                     3897*Power(t2,7) + 839*Power(t2,8))))))*
       R1q(1 - s + s1 - t2))/
     (s*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(-s + s1 - t2,3)*
       (1 - s + s1 - t2)*Power(-1 + s + t2,3)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) - 
    (8*(-4*Power(s,9)*Power(t2,2) - 
         2*Power(s,8)*(Power(t1,3) + t1*Power(t2,2) + 
            t2*(s1*(4 - 12*t2) + t2*(-7 - 3*s2 + 2*t2))) + 
         Power(s,7)*(Power(t1,3)*(14 - 3*t2) + 
            Power(s1,2)*(-4 + 40*t2 - 60*Power(t2,2)) + 
            t1*t2*(-6 + 6*s2 - 10*t2 + 5*s2*t2 - 5*Power(t2,2)) + 
            Power(t1,2)*(-6 + 4*t2 - 2*s2*t2 + 3*Power(t2,2)) + 
            Power(t2,2)*(58 - 46*s2 - 2*Power(s2,2) - 8*t2 + 5*s2*t2 + 
               5*Power(t2,2)) + 
            s1*(16*Power(t1,3) - 2*Power(t1,2)*(-3 + 3*s2 + t2) + 
               t1*t2*(2 - 6*s2 + 15*t2) + 
               t2*(20 + 12*s2 - 40*t2 - 16*s2*t2 + 19*Power(t2,2)))) + 
         Power(s,6)*(Power(t1,3)*(-38 + 19*t2 + Power(t2,2)) + 
            16*Power(s1,3)*(1 - 5*t2 + 5*Power(t2,2)) + 
            Power(t1,2)*(36 + (-23 + 4*s2)*t2 - 
               (13 + 3*s2)*Power(t2,2) + 3*Power(t2,3)) - 
            t1*(6 + (-44 + 34*s2)*t2 + 
               (-142 + 31*s2 + Power(s2,2))*Power(t2,2) + 
               (31 - 10*s2)*Power(t2,3) + Power(t2,4)) + 
            t2*(-6 - 444*t2 + 34*Power(s2,2)*t2 + 207*Power(t2,2) - 
               41*Power(t2,3) + 5*Power(t2,4) + 
               s2*(6 + 77*t2 - 25*Power(t2,2) - 9*Power(t2,3))) - 
            Power(s1,2)*(-6 + 56*Power(t1,3) + 
               Power(t1,2)*(38 - 14*t2) + 22*t2 + 10*Power(t2,2) + 
               35*Power(t2,3) + 6*Power(s2,2)*(t1 + t2) + 
               t1*(2 + 48*Power(t2,2)) - 
               s2*(6 + 42*Power(t1,2) - 8*t2 - 9*Power(t2,2) + 
                  t1*(6 + 32*t2))) + 
            s1*(3*Power(t1,3)*(-25 + 7*t2) + 
               Power(t1,2)*(10 + 15*t2 - 22*Power(t2,2) + 
                  s2*(28 + 3*t2)) + 
               t1*(6 - 36*t2 + 2*Power(s2,2)*t2 + 62*Power(t2,2) + 
                  33*Power(t2,3) + s2*(-6 + 4*t2 - 27*Power(t2,2))) + 
               t2*(148 + Power(s2,2)*(2 - 11*t2) - 333*t2 + 
                  90*Power(t2,2) - 24*Power(t2,3) + 
                  2*s2*(-49 + 39*t2 + 7*Power(t2,2))))) + 
         Power(s,5)*(-2 + 34*t2 - 26*s2*t2 + 1033*Power(t2,2) + 
            208*s2*Power(t2,2) - 123*Power(s2,2)*Power(t2,2) - 
            6*Power(s2,3)*Power(t2,2) - 782*Power(t2,3) - 
            51*s2*Power(t2,3) + 18*Power(s2,2)*Power(t2,3) - 
            Power(s2,3)*Power(t2,3) + 172*Power(t2,4) + 
            42*s2*Power(t2,4) + 5*Power(s2,2)*Power(t2,4) - 
            20*Power(t2,5) - 8*s2*Power(t2,5) - 
            4*Power(s1,4)*(6 - 20*t2 + 15*Power(t2,2)) + 
            Power(t1,3)*(46 - 43*t2 + 2*Power(t2,3)) + 
            Power(t1,2)*(-78 + 2*(20 + 9*s2)*t2 + 
               (74 - 16*s2)*Power(t2,2) + (-32 + s2)*Power(t2,3)) + 
            t1*(30 + (-101 + 52*s2)*t2 + 
               (-536 + 37*s2 + 22*Power(s2,2))*Power(t2,2) - 
               (-279 + 30*s2 + Power(s2,2))*Power(t2,3) + 
               3*(-8 + s2)*Power(t2,4) + 2*Power(t2,5)) + 
            Power(s1,3)*(8 - 2*Power(s2,3) + 112*Power(t1,3) - 113*t2 + 
               160*Power(t2,2) + 30*Power(t2,3) - 
               6*Power(t1,2)*(-17 + 7*t2) + 
               4*Power(s2,2)*(9*t1 + 7*t2) + 
               5*t1*(3 - 6*t2 + 17*Power(t2,2)) - 
               s2*(-2 + 126*Power(t1,2) + 82*t2 - 75*Power(t2,2) + 
                  t1*(34 + 66*t2))) + 
            Power(s1,2)*(80 + Power(t1,3)*(156 - 63*t2) - 508*t2 + 
               4*Power(s2,3)*t2 + 699*Power(t2,2) - 269*Power(t2,3) + 
               46*Power(t2,4) + 
               Power(s2,2)*(-2 + t1*(20 - 19*t2) - 48*t2 + 
                  55*Power(t2,2)) + 
               Power(t1,2)*(14 - 135*t2 + 69*Power(t2,2)) - 
               2*t1*(28 - 82*t2 + 86*Power(t2,2) + 45*Power(t2,3)) + 
               s2*(-40 + 87*t2 + 195*Power(t2,2) - 110*Power(t2,3) + 
                  Power(t1,2)*(-113 + 24*t2) + t1*(23 + 51*Power(t2,2)))\
) + s1*(-2*Power(t1,3)*(-58 + 40*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-91 - 92*t2 + 90*Power(t2,2) - 
                  15*Power(t2,3) + s2*(-46 + 33*t2 + 17*Power(t2,2))) + 
               t1*(14 + 236*t2 - 26*Power(s2,2)*t2 - 605*Power(t2,2) + 
                  158*Power(t2,3) + 7*Power(t2,4) + 
                  s2*(20 + 42*t2 + 96*Power(t2,2) - 34*Power(t2,3))) + 
               t2*(-792 + 1543*t2 + 7*Power(s2,3)*t2 - 679*Power(t2,2) + 
                  124*Power(t2,3) - 18*Power(t2,4) + 
                  Power(s2,2)*(48 - 6*t2 - 37*Power(t2,2)) + 
                  s2*(116 - 18*t2 - 125*Power(t2,2) + 61*Power(t2,3))))) \
+ Power(s,4)*(8 - 51*t2 + 24*s2*t2 - 1065*Power(t2,2) - 
            856*s2*Power(t2,2) + 153*Power(s2,2)*Power(t2,2) + 
            26*Power(s2,3)*Power(t2,2) + 1202*Power(t2,3) + 
            469*s2*Power(t2,3) - 53*Power(s2,2)*Power(t2,3) + 
            Power(s2,3)*Power(t2,3) - 360*Power(t2,4) - 
            108*s2*Power(t2,4) - 21*Power(s2,2)*Power(t2,4) - 
            Power(s2,3)*Power(t2,4) + 39*Power(t2,5) + 
            26*s2*Power(t2,5) + 3*Power(s2,2)*Power(t2,5) + 
            8*Power(s1,5)*(2 - 5*t2 + 3*Power(t2,2)) - 
            Power(t1,3)*(10 - 35*t2 + 7*Power(t2,2) + 6*Power(t2,3)) + 
            Power(t1,2)*(60 + (29 - 80*s2)*t2 + 
               (-294 + 113*s2)*Power(t2,2) + 
               (151 - 23*s2)*Power(t2,3) + 2*(-6 + s2)*Power(t2,4)) + 
            t1*(-48 + (39 + 28*s2)*t2 + 
               (1055 + 83*s2 - 83*Power(s2,2))*Power(t2,2) + 
               (-747 - 28*s2 + 15*Power(s2,2))*Power(t2,3) + 
               (113 + 5*s2)*Power(t2,4) - 2*(3 + s2)*Power(t2,5)) + 
            Power(s1,4)*(-64 + 10*Power(s2,3) - 140*Power(t1,3) + 
               271*t2 - 230*Power(t2,2) - 10*Power(t2,3) + 
               10*Power(t1,2)*(-15 + 7*t2) - 
               2*Power(s2,2)*(1 + 45*t1 + 25*t2) - 
               10*t1*(4 - 8*t2 + 9*Power(t2,2)) + 
               s2*(-37 + 210*Power(t1,2) + 168*t2 - 100*Power(t2,2) + 
                  20*t1*(4 + 3*t2))) - 
            Power(s1,3)*(147 - 561*t2 + 698*Power(t2,2) - 
               343*Power(t2,3) + 44*Power(t2,4) + 
               Power(s2,3)*(-6 + 19*t2) - 5*Power(t1,3)*(-29 + 21*t2) + 
               2*Power(t1,2)*(16 - 155*t2 + 60*Power(t2,2)) + 
               Power(s2,2)*(19 + t1*(53 - 65*t2) - 165*t2 + 
                  77*Power(t2,2)) - 
               t1*(127 - 310*t2 + 267*Power(t2,2) + 130*Power(t2,3)) + 
               s2*(10 - 259*t2 + 608*Power(t2,2) - 200*Power(t2,3) + 
                  5*Power(t1,2)*(-29 + 19*t2) + 
                  t1*(31 + 162*t2 + 25*Power(t2,2)))) + 
            Power(s1,2)*(-2*Power(s2,3)*t2*(1 + 8*t2) + 
               Power(t1,3)*(-98 + 115*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(19 + 412*t2 - 235*Power(t2,2) + 
                  30*Power(t2,3)) - 
               2*t1*(-65 + 300*t2 - 489*Power(t2,2) + 
                  179*Power(t2,3) + 9*Power(t2,4)) + 
               2*(-168 + 710*t2 - 895*Power(t2,2) + 346*Power(t2,3) - 
                  66*Power(t2,4) + 12*Power(t2,5)) + 
               Power(s2,2)*(32 + 89*t2 - 233*Power(t2,2) + 
                  120*Power(t2,3) + t1*(-29 + 105*t2 + 10*Power(t2,2))) \
+ s2*(27 + 25*t2 - 349*Power(t2,2) + 473*Power(t2,3) - 
                  140*Power(t2,4) + 
                  Power(t1,2)*(79 - 133*t2 - 40*Power(t2,2)) + 
                  t1*(-39 - 262*t2 + 47*Power(t2,2) + 28*Power(t2,3)))) \
+ s1*(14 + 1333*t2 - 2420*Power(t2,2) + 1318*Power(t2,3) - 
               260*Power(t2,4) + 22*Power(t2,5) + 
               Power(s2,3)*t2*(-12 - 13*t2 + 12*Power(t2,2)) - 
               Power(t1,3)*(55 - 81*t2 + 8*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(93 + 371*t2 - 483*Power(t2,2) + 
                  131*Power(t2,3) + 2*Power(t2,4)) - 
               t1*(63 + 875*t2 - 1494*Power(t2,2) + 673*Power(t2,3) - 
                  79*Power(t2,4) + 8*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-198 + 88*t2 + 112*Power(t2,2) - 37*Power(t2,3) + 
                  t1*(106 - 91*t2 - 3*Power(t2,2))) - 
               s2*(2 - 521*t2 + 700*Power(t2,2) - 369*Power(t2,3) + 
                  153*Power(t2,4) - 30*Power(t2,5) + 
                  Power(t1,2)*
                   (-32 + 148*t2 - 99*Power(t2,2) + Power(t2,3)) + 
                  t1*(24 + 98*t2 - 163*Power(t2,2) + 44*Power(t2,3) + 
                     3*Power(t2,4))))) + 
         Power(-1 + s1,4)*(-4 + 25*t2 - 8*s2*t2 - 36*Power(t2,2) + 
            6*s2*Power(t2,2) - 15*Power(s2,2)*Power(t2,2) + 
            2*Power(s2,3)*Power(t2,2) + 14*Power(t2,3) + 
            8*s2*Power(t2,3) + 19*Power(s2,2)*Power(t2,3) - 
            3*Power(s2,3)*Power(t2,3) - 2*s2*Power(t2,4) - 
            7*Power(s2,2)*Power(t2,4) - Power(s2,3)*Power(t2,4) + 
            Power(t2,5) - 4*s2*Power(t2,5) + 3*Power(s2,2)*Power(t2,5) + 
            2*Power(s1,4)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) + 
            Power(t1,3)*(4 - 7*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
            Power(t1,2)*(-12 + (39 - 8*s2)*t2 + 
               (-68 + 19*s2)*Power(t2,2) + (53 - 19*s2)*Power(t2,3) + 
               2*(-6 + s2)*Power(t2,4)) + 
            t1*(12 + (-57 + 16*s2)*t2 + 
               (85 + 9*s2 - 5*Power(s2,2))*Power(t2,2) + 
               (-45 - 40*s2 + 11*Power(s2,2))*Power(t2,3) + 
               (3 + 17*s2)*Power(t2,4) - 2*(-1 + s2)*Power(t2,5)) - 
            Power(s1,3)*(2*Power(-1 + t2,2) + t1*Power(-1 + t2,2) + 
               4*Power(t1,2)*Power(-1 + t2,2) + 
               Power(s2,3)*(1 + 3*t2) - Power(t1,3)*(1 + 3*t2) + 
               Power(s2,2)*(7*Power(-1 + t2,2) - 3*t1*(1 + 3*t2)) + 
               s2*(-5*Power(-1 + t2,2) - 9*t1*Power(-1 + t2,2) + 
                  Power(t1,2)*(3 + 9*t2))) + 
            Power(s1,2)*(-1 + t2)*
             (-1 + Power(t1,2)*(29 - 21*t2) + Power(t1,3)*(-2 + t2) - 
               2*t2 + 3*Power(t2,2) - Power(s2,3)*(2 + t2) + 
               2*t1*(-5 + t2 + 4*Power(t2,2)) + 
               Power(s2,2)*(15 + 5*t1 - 18*t2 + 11*Power(t2,2)) + 
               s2*(Power(t1,2)*(1 - 2*t2) + 
                  t1*(-49 + 41*t2 - 8*Power(t2,2)) - 
                  14*(-1 + Power(t2,2)))) + 
            s1*(-(Power(-1 + t2,2)*(13 + 3*t2 + 2*Power(t2,2))) + 
               Power(s2,3)*t2*(-4 + 5*t2 + 3*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(-21 + 17*t2 + 9*Power(t2,2)) - 
               Power(t1,3)*(5 - 7*t2 + 4*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-3 + 61*t2 - 87*Power(t2,2) + 
                  27*Power(t2,3) + 2*Power(t2,4)) + 
               Power(s2,2)*t2*
                (30 - 45*t2 + 24*Power(t2,2) - 9*Power(t2,3) + 
                  t1*(10 - 19*t2 - 3*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(8 + 24*t2 + 13*Power(t2,2)) + 
                  Power(t1,2)*
                   (8 - 18*t2 + 19*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-16 - 58*t2 + 121*Power(t2,2) - 52*Power(t2,3) + 
                     5*Power(t2,4))))) - 
         s*Power(-1 + s1,2)*(-10 + 42*t1 - 54*Power(t1,2) + 
            22*Power(t1,3) + 94*t2 - 42*s2*t2 - 231*t1*t2 + 
            88*s2*t1*t2 + 168*Power(t1,2)*t2 - 46*s2*Power(t1,2)*t2 - 
            31*Power(t1,3)*t2 - 124*Power(t2,2) - 62*s2*Power(t2,2) - 
            77*Power(s2,2)*Power(t2,2) + 14*Power(s2,3)*Power(t2,2) + 
            412*t1*Power(t2,2) + 85*s2*t1*Power(t2,2) - 
            38*Power(s2,2)*t1*Power(t2,2) - 
            324*Power(t1,2)*Power(t2,2) + 
            104*s2*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) + 
            42*Power(t2,3) + 152*s2*Power(t2,3) + 
            70*Power(s2,2)*Power(t2,3) - 11*Power(s2,3)*Power(t2,3) - 
            273*t1*Power(t2,3) - 178*s2*t1*Power(t2,3) + 
            45*Power(s2,2)*t1*Power(t2,3) + 
            232*Power(t1,2)*Power(t2,3) - 
            77*s2*Power(t1,2)*Power(t2,3) + 6*Power(t1,3)*Power(t2,3) - 
            6*Power(t2,4) - 38*s2*Power(t2,4) - 
            31*Power(s2,2)*Power(t2,4) - 4*Power(s2,3)*Power(t2,4) + 
            44*t1*Power(t2,4) + 65*s2*t1*Power(t2,4) - 
            48*Power(t1,2)*Power(t2,4) + 8*s2*Power(t1,2)*Power(t2,4) + 
            4*Power(t2,5) - 10*s2*Power(t2,5) + 
            12*Power(s2,2)*Power(t2,5) + 6*t1*Power(t2,5) - 
            8*s2*t1*Power(t2,5) + 
            Power(s1,5)*(10*Power(s2,3) - 
               4*Power(s2,2)*(2 + 9*t1 - t2) + 
               s2*(5 + 42*Power(t1,2) + t1*(26 - 18*t2) - 6*t2 + 
                  Power(t2,2)) - 
               t1*(7 + 16*Power(t1,2) - 10*t2 + 3*Power(t2,2) - 
                  2*t1*(-9 + 7*t2))) + 
            Power(s1,4)*(-31 + 62*t2 - 27*Power(t2,2) - 4*Power(t2,3) - 
               Power(s2,3)*(1 + 16*t2) + Power(t1,3)*(8 + 21*t2) + 
               Power(t1,2)*(-14 + 63*t2 - 27*Power(t2,2)) + 
               2*t1*(23 - 34*t2 + 7*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,2)*(-18 + 59*t2 - 19*Power(t2,2) + 
                  t1*(16 + 47*t2)) + 
               s2*(-(Power(t1,2)*(23 + 52*t2)) + 
                  t1*(33 - 120*t2 + 43*Power(t2,2)) + 
                  2*(-7 + 4*t2 + Power(t2,2) + 2*Power(t2,3)))) + 
            Power(s1,3)*(16 + 19*t2 - 94*Power(t2,2) + 55*Power(t2,3) + 
               4*Power(t2,4) + 
               Power(s2,3)*(9 - 19*t2 + 2*Power(t2,2)) + 
               2*Power(t1,3)*(6 - 7*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-107 + 200*t2 - 112*Power(t2,2) + 
                  3*Power(t2,3)) + 
               t1*(-73 + 38*t2 + 68*Power(t2,2) - 30*Power(t2,3) - 
                  3*Power(t2,4)) + 
               Power(s2,2)*(-103 + 183*t2 - 138*Power(t2,2) + 
                  42*Power(t2,3) + 4*t1*(-3 + 7*t2 + Power(t2,2))) - 
               s2*(-38 + 22*t2 - 19*Power(t2,2) + 24*Power(t2,3) + 
                  11*Power(t2,4) + 
                  Power(t1,2)*(6 - 3*t2 + 13*Power(t2,2)) + 
                  t1*(-228 + 432*t2 - 274*Power(t2,2) + 38*Power(t2,3)))\
) + Power(s1,2)*(-38 + 77*t2 - 30*Power(t2,2) + 25*Power(t2,3) - 
               34*Power(t2,4) - 
               2*Power(t1,3)*
                (5 - 7*t2 + 10*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*(10 - 17*t2 + 25*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-174 + 427*t2 - 427*Power(t2,2) + 
                  122*Power(t2,3) + 8*Power(t2,4)) - 
               t1*(-226 + 275*t2 + 2*Power(t2,2) - 55*Power(t2,3) + 
                  2*Power(t2,4) + 2*Power(t2,5)) - 
               Power(s2,2)*(43 - 206*t2 + 301*Power(t2,2) - 
                  133*Power(t2,3) + 39*Power(t2,4) + 
                  t1*(32 - 47*t2 + 78*Power(t2,2) + 15*Power(t2,3))) + 
               s2*(-111 + 13*t2 + 143*Power(t2,2) - 107*Power(t2,3) + 
                  56*Power(t2,4) + 6*Power(t2,5) + 
                  Power(t1,2)*
                   (41 - 80*t2 + 106*Power(t2,2) + 11*Power(t2,3)) + 
                  t1*(129 - 408*t2 + 600*Power(t2,2) - 
                     254*Power(t2,3) + 21*Power(t2,4)))) + 
            s1*(-53 + 88*t2 - 57*Power(t2,2) - 2*Power(t2,3) + 
               20*Power(t2,4) + 4*Power(t2,5) + 
               2*Power(t1,3)*
                (-8 + 5*t2 + 3*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s2,3)*t2*
                (24 - 19*t2 + Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,2)*(-33 + 408*t2 - 550*Power(t2,2) + 
                  283*Power(t2,3) - 40*Power(t2,4)) + 
               t1*(102 - 482*t2 + 503*Power(t2,2) - 48*Power(t2,3) - 
                  87*Power(t2,4) + 12*Power(t2,5)) + 
               Power(s2,2)*t2*
                (120 - 173*t2 + 167*Power(t2,2) - 58*Power(t2,3) + 
                  12*Power(t2,4) + t1*(70 - 80*t2 + 34*Power(t2,2))) + 
               s2*(22 + 237*t2 - 275*Power(t2,2) - 37*Power(t2,3) + 
                  81*Power(t2,4) - 28*Power(t2,5) + 
                  Power(t1,2)*
                   (26 - 81*t2 + 91*Power(t2,2) - 62*Power(t2,3) + 
                     8*Power(t2,4)) - 
                  2*t1*(24 + 171*t2 - 251*Power(t2,2) + 
                     165*Power(t2,3) - 45*Power(t2,4) + 4*Power(t2,5))))) \
+ Power(s,2)*(-4 + 42*t1 - 84*Power(t1,2) + 46*Power(t1,3) + 112*t2 - 
            70*s2*t2 - 346*t1*t2 + 182*s2*t1*t2 + 275*Power(t1,2)*t2 - 
            108*s2*Power(t1,2)*t2 - 47*Power(t1,3)*t2 - 
            157*Power(t2,2) - 505*s2*Power(t2,2) - 
            108*Power(s2,2)*Power(t2,2) + 36*Power(s2,3)*Power(t2,2) + 
            928*t1*Power(t2,2) + 227*s2*t1*Power(t2,2) - 
            103*Power(s2,2)*t1*Power(t2,2) - 
            617*Power(t1,2)*Power(t2,2) + 
            223*s2*Power(t1,2)*Power(t2,2) + 3*Power(t1,3)*Power(t2,2) + 
            129*Power(t2,3) + 628*s2*Power(t2,3) + 
            66*Power(s2,2)*Power(t2,3) - 14*Power(s2,3)*Power(t2,3) - 
            713*t1*Power(t2,3) - 294*s2*t1*Power(t2,3) + 
            70*Power(s2,2)*t1*Power(t2,3) + 
            401*Power(t1,2)*Power(t2,3) - 
            118*s2*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) - 
            83*Power(t2,4) - 145*s2*Power(t2,4) - 
            52*Power(s2,2)*Power(t2,4) - 6*Power(s2,3)*Power(t2,4) + 
            141*t1*Power(t2,4) + 90*s2*t1*Power(t2,4) - 
            72*Power(t1,2)*Power(t2,4) + 12*s2*Power(t1,2)*Power(t2,4) + 
            19*Power(t2,5) + 2*s2*Power(t2,5) + 
            18*Power(s2,2)*Power(t2,5) + 4*t1*Power(t2,5) - 
            12*s2*t1*Power(t2,5) + 
            Power(s1,6)*(-30 + 20*Power(s2,3) - 56*Power(t1,3) + 
               59*t2 - 30*Power(t2,2) + Power(t2,3) - 
               2*Power(s2,2)*(6 + 45*t1 + 5*t2) + 
               6*Power(t1,2)*(-11 + 7*t2) + 
               t1*(-30 + 48*t2 - 20*Power(t2,2)) + 
               s2*(-5 + 126*Power(t1,2) + t1*(70 - 24*t2) + 16*t2 - 
                  9*Power(t2,2))) + 
            Power(s1,5)*(29 + 53*t2 - 115*Power(t2,2) + 
               35*Power(t2,3) - 4*Power(t2,4) - 
               Power(s2,3)*(9 + 34*t2) + Power(t1,3)*(51 + 63*t2) + 
               Power(t1,2)*(34 + 171*t2 - 78*Power(t2,2)) + 
               t1*(156 - 296*t2 + 112*Power(t2,2) + 45*Power(t2,3)) + 
               Power(s2,2)*(-30 + 159*t2 - 25*Power(t2,2) + 
                  2*t1*(41 + 50*t2)) - 
               s2*(89 - 209*t2 + 183*Power(t2,2) - 50*Power(t2,3) + 
                  Power(t1,2)*(130 + 123*t2) + 
                  t1*(20 + 288*t2 - 75*Power(t2,2)))) + 
            Power(s1,4)*(-207 + 228*t2 - 97*Power(t2,2) + 
               80*Power(t2,3) + Power(t2,4) + 3*Power(t2,5) + 
               Power(s2,3)*(4 - 9*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(22 - 55*t2 + 15*Power(t2,2)) + 
               Power(t1,2)*(-174 + 245*t2 - 195*Power(t2,2) + 
                  15*Power(t2,3)) + 
               t1*(-94 + 80*t2 + 274*Power(t2,2) - 251*Power(t2,3) - 
                  13*Power(t2,4)) + 
               Power(s2,2)*(-145 + 332*t2 - 340*Power(t2,2) + 
                  97*Power(t2,3) + t1*(-30 + 14*t2 + 15*Power(t2,2))) + 
               s2*(258 - 419*t2 + 88*Power(t2,2) + 126*Power(t2,3) - 
                  67*Power(t2,4) + 
                  Power(t1,2)*(10 + 42*t2 - 35*Power(t2,2)) + 
                  t1*(330 - 574*t2 + 483*Power(t2,2) - 62*Power(t2,3)))) \
+ Power(s1,3)*(158 + 221*t2 - 535*Power(t2,2) + 176*Power(t2,3) - 
               38*Power(t2,4) - 10*Power(t2,5) - 
               2*Power(t1,3)*
                (9 - 5*t2 + 24*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,3)*(-13 + 32*t2 + 6*Power(t2,2) + 
                  17*Power(t2,3)) + 
               2*Power(t1,2)*
                (-87 + 280*t2 - 357*Power(t2,2) + 109*Power(t2,3) + 
                  6*Power(t2,4)) + 
               t1*(8 - 454*t2 + 166*Power(t2,2) + 96*Power(t2,3) + 
                  66*Power(t2,4) - 8*Power(t2,5)) + 
               Power(s2,2)*(4 + t2 - 321*Power(t2,2) + 
                  234*Power(t2,3) - 78*Power(t2,4) - 
                  2*t1*(17 - 33*t2 + 81*Power(t2,2) + 13*Power(t2,3))) \
+ s2*(-99 + 592*Power(t2,2) - 409*Power(t2,3) + 80*Power(t2,4) + 
                  26*Power(t2,5) + 
                  2*Power(t1,2)*
                   (35 - 68*t2 + 125*Power(t2,2) + 7*Power(t2,3)) + 
                  t1*(222 - 464*t2 + 826*Power(t2,2) - 
                     400*Power(t2,3) + 30*Power(t2,4)))) + 
            Power(s1,2)*(-41 - 329*t2 + 454*Power(t2,2) + 
               40*Power(t2,3) - 98*Power(t2,4) + 30*Power(t2,5) + 
               2*Power(t1,3)*
                (-2 + 3*t2 + 11*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(-316 + 614*t2 - 548*Power(t2,2) + 
                  352*Power(t2,3) - 64*Power(t2,4)) + 
               Power(s2,3)*(14 + 27*t2 - 39*Power(t2,2) + 
                  8*Power(t2,3) - 6*Power(t2,4)) + 
               2*t1*(245 - 23*t2 - 39*Power(t2,2) - 6*Power(t2,3) - 
                  56*Power(t2,4) + 10*Power(t2,5)) + 
               Power(s2,2)*(23 + 56*t2 - 42*Power(t2,2) + 
                  115*Power(t2,3) - 64*Power(t2,4) + 18*Power(t2,5) + 
                  t1*(-56 + 34*t2 - 32*Power(t2,2) + 58*Power(t2,3))) + 
               s2*(-451 + 295*t2 - 152*Power(t2,2) - 228*Power(t2,3) + 
                  200*Power(t2,4) - 50*Power(t2,5) + 
                  2*Power(t1,2)*
                   (28 - 47*t2 + 34*Power(t2,2) - 49*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  2*t1*(-92 + 300*t2 - 333*Power(t2,2) + 
                     214*Power(t2,3) - 63*Power(t2,4) + 6*Power(t2,5)))) \
+ s1*(-33 + 24*t2 + 128*Power(t2,2) - 333*Power(t2,3) + 
               190*Power(t2,4) - 26*Power(t2,5) - 
               Power(s2,3)*t2*
                (48 + 8*t2 - 21*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(-41 + 23*t2 + 8*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(t1,2)*(-20 + 653*t2 - 728*Power(t2,2) + 
                  294*Power(t2,3) - 36*Power(t2,4)) + 
               t1*(100 - 1002*t2 + 602*Power(t2,2) + 259*Power(t2,3) - 
                  178*Power(t2,4) + 16*Power(t2,5)) + 
               2*Power(s2,2)*t2*
                (27 - 14*t2 + 64*Power(t2,2) - 31*Power(t2,3) + 
                  6*Power(t2,4) + t1*(85 - 51*t2 + 13*Power(t2,2))) + 
               s2*(18 + 1089*t2 - 919*Power(t2,2) + 57*Power(t2,3) + 
                  108*Power(t2,4) - 42*Power(t2,5) + 
                  Power(t1,2)*
                   (28 - 93*t2 + 70*Power(t2,2) - 54*Power(t2,3) + 
                     8*Power(t2,4)) - 
                  t1*(50 + 696*t2 - 731*Power(t2,2) + 416*Power(t2,3) - 
                     106*Power(t2,4) + 8*Power(t2,5))))) + 
         Power(s,3)*(-8 + 12*t1 + 30*Power(t1,2) - 38*Power(t1,3) - 
            4*Power(s1,6)*Power(-1 + t2,2) - 20*t2 + 32*s2*t2 + 
            196*t1*t2 - 162*s2*t1*t2 - 196*Power(t1,2)*t2 + 
            130*s2*Power(t1,2)*t2 + 15*Power(t1,3)*t2 + 477*Power(t2,2) + 
            1048*s2*Power(t2,2) - 16*Power(s2,2)*Power(t2,2) - 
            44*Power(s2,3)*Power(t2,2) - 1250*t1*Power(t2,2) - 
            245*s2*t1*Power(t2,2) + 132*Power(s2,2)*t1*Power(t2,2) + 
            591*Power(t1,2)*Power(t2,2) - 
            232*s2*Power(t1,2)*Power(t2,2) + 8*Power(t1,3)*Power(t2,2) - 
            740*Power(t2,3) - 882*s2*Power(t2,3) + 
            20*Power(s2,2)*Power(t2,3) + 6*Power(s2,3)*Power(t2,3) + 
            989*t1*Power(t2,3) + 204*s2*t1*Power(t2,3) - 
            50*Power(s2,2)*t1*Power(t2,3) - 344*Power(t1,2)*Power(t2,3) + 
            82*s2*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            325*Power(t2,4) + 184*s2*Power(t2,4) + 
            44*Power(s2,2)*Power(t2,4) + 4*Power(s2,3)*Power(t2,4) - 
            188*t1*Power(t2,4) - 50*s2*t1*Power(t2,4) + 
            48*Power(t1,2)*Power(t2,4) - 8*s2*Power(t1,2)*Power(t2,4) - 
            40*Power(t2,5) - 26*s2*Power(t2,5) - 
            12*Power(s2,2)*Power(t2,5) + 4*t1*Power(t2,5) + 
            8*s2*t1*Power(t2,5) + 
            Power(s1,5)*(80 - 20*Power(s2,3) + 112*Power(t1,3) + 
               Power(t1,2)*(130 - 70*t2) - 215*t2 + 136*Power(t2,2) - 
               Power(t2,3) + 8*Power(s2,2)*(1 + 15*t1 + 5*t2) + 
               t1*(50 - 90*t2 + 57*Power(t2,2)) - 
               s2*(-39 + 210*Power(t1,2) + 112*t2 - 54*Power(t2,2) + 
                  10*t1*(10 + t2))) + 
            Power(s1,4)*(41 - 196*t2 + 364*Power(t2,2) - 
               195*Power(t2,3) + 21*Power(t2,4) + 
               9*Power(s2,3)*(-1 + 4*t2) - 15*Power(t1,3)*(-2 + 7*t2) + 
               Power(t1,2)*(-2 - 330*t2 + 125*Power(t2,2)) - 
               t1*(172 - 394*t2 + 238*Power(t2,2) + 105*Power(t2,3)) + 
               Power(s2,2)*(48 - 233*t2 + 48*Power(t2,2) - 
                  2*t1*(-6 + 55*t2)) + 
               s2*(110 - 427*t2 + 559*Power(t2,2) - 155*Power(t2,3) + 
                  10*Power(t1,2)*(-1 + 15*t2) + 
                  t1*(22 + 358*t2 - 45*Power(t2,2)))) + 
            Power(s1,3)*(240 - 908*t2 + 777*Power(t2,2) - 
               240*Power(t2,3) + 52*Power(t2,4) - 14*Power(t2,5) - 
               4*Power(t1,3)*(2 + 10*t2 + 5*Power(t2,2)) + 
               Power(s2,3)*(-1 + 13*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(138 - 484*t2 + 300*Power(t2,2) - 
                  30*Power(t2,3)) + 
               t1*(-38 + 444*t2 - 750*Power(t2,2) + 428*Power(t2,3) + 
                  22*Power(t2,4)) + 
               Power(s2,2)*(53 - 389*t2 + 446*Power(t2,2) - 
                  149*Power(t2,3) - 4*t1*(-8 + 32*t2 + 5*Power(t2,2))) + 
               s2*(-20 + 126*t2 + 224*Power(t2,2) - 467*Power(t2,3) + 
                  144*Power(t2,4) + 
                  2*Power(t1,2)*(-4 + 61*t2 + 25*Power(t2,2)) + 
                  4*t1*(-40 + 165*t2 - 105*Power(t2,2) + 7*Power(t2,3)))) \
+ Power(s1,2)*(360 - 877*t2 + 1272*Power(t2,2) - 686*Power(t2,3) + 
               86*Power(t2,4) + 8*Power(t2,5) + 
               2*Power(t1,3)*t2*(-11 + 16*t2 + 10*Power(t2,2)) - 
               Power(s2,3)*(6 + 13*t2 - 25*Power(t2,2) + 
                  23*Power(t2,3)) + 
               Power(t1,2)*(248 - 782*t2 + 920*Power(t2,2) - 
                  228*Power(t2,3) - 8*Power(t2,4)) + 
               2*t1*(-176 + 473*t2 - 528*Power(t2,2) + 
                  226*Power(t2,3) - 54*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*(-81 + 34*t2 + 315*Power(t2,2) - 
                  295*Power(t2,3) + 80*Power(t2,4) + 
                  2*t1*(22 - 89*t2 + 82*Power(t2,2) + 9*Power(t2,3))) - 
               s2*(-277 + 720*t2 - 364*Power(t2,2) + 66*Power(t2,3) - 
                  96*Power(t2,4) + 42*Power(t2,5) + 
                  Power(t1,2)*
                   (42 - 212*t2 + 228*Power(t2,2) + 6*Power(t2,3)) + 
                  2*t1*(31 - 286*t2 + 455*Power(t2,2) - 
                     158*Power(t2,3) + 7*Power(t2,4)))) + 
            s1*(-17 - 804*t2 + 1254*Power(t2,2) - 638*Power(t2,3) + 
               116*Power(t2,4) - 10*Power(t2,5) + 
               4*Power(t1,3)*t2*(-2 + 3*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*t2*
                (40 + 8*t2 - 27*Power(t2,2) + 4*Power(t2,3)) + 
               t1*(-12 + 1486*t2 - 1643*Power(t2,2) + 572*Power(t2,3) - 
                  62*Power(t2,4)) + 
               2*Power(t1,2)*(16 - 389*t2 + 528*Power(t2,2) - 
                  203*Power(t2,3) + 20*Power(t2,4)) - 
               Power(s2,2)*t2*
                (-184 + 77*t2 + 148*Power(t2,2) - 92*Power(t2,3) + 
                  12*Power(t2,4) + 4*t1*(48 - 47*t2 + 8*Power(t2,2))) + 
               s2*(-2 - 1359*t2 + 1499*Power(t2,2) - 530*Power(t2,3) + 
                  64*Power(t2,4) - 12*Power(t2,5) - 
                  2*Power(t1,2)*
                   (9 - 101*t2 + 123*Power(t2,2) - 34*Power(t2,3) + 
                     4*Power(t2,4)) + 
                  t1*(28 + 358*t2 - 716*Power(t2,2) + 412*Power(t2,3) - 
                     80*Power(t2,4) + 8*Power(t2,5))))))*R2q(s))/
     (s*(-1 + s1)*Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + Power(s1,2),
        2)*(-1 + s2)*(-s + s2 - t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),2)) + 
    (8*(Power(-1 + s1,6)*(s1 - t2)*(-1 + t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,12)*(Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
         Power(s,11)*(Power(t1,3)*(-11 - 11*s1 + 3*t2) + 
            t1*t2*(3 + s1*(-1 + 3*s2 - 12*t2) + 4*Power(t2,2) - 
               s2*(3 + t2)) + 
            Power(t1,2)*(3 + (-2 + s2)*t2 + s1*(-3 + 3*s2 + t2)) + 
            Power(t2,2)*(4 + 9*t2 - 5*Power(t2,2) + s2*(-3 + 2*t2) + 
               2*s1*(-5 + 2*s2 + 9*t2))) + 
         Power(s,10)*(Power(t1,3)*
             (52 + 55*Power(s1,2) + s1*(95 - 30*t2) - 31*t2 + 
               3*Power(t2,2)) + 
            Power(t1,2)*(-30 - 6*(-4 + s2)*t2 + 
               (-2 + 3*s2)*Power(t2,2) + Power(t2,3) - 
               2*Power(s1,2)*(-14 + 15*s2 + 5*t2) - 
               s1*(2 + 26*s2 + 7*t2 - 3*Power(t2,2))) + 
            t2*(3 - 33*t2 + Power(s2,2)*t2 + 34*Power(t2,2) + 
               11*Power(t2,3) - 3*Power(t2,4) + 
               Power(s1,2)*(-11 + 3*Power(s2,2) + s2*(2 - 33*t2) + 
                  77*t2 - 72*Power(t2,2)) + 
               s2*(-3 + 29*t2 - 25*Power(t2,2) + 5*Power(t2,3)) + 
               s1*(2 + 21*t2 - 81*Power(t2,2) + 42*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + s2*(3 + 7*t2 + 3*Power(t2,2))\
)) + t1*(3 + (-34 + 29*s2)*t2 - (27 + 10*s2)*Power(t2,2) - 
               (10 + 3*s2)*Power(t2,3) + 5*Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) + 63*Power(t2,2) - 
                  s2*(3 + 25*t2)) + 
               s1*(-3 + 11*t2 - Power(s2,2)*t2 + 4*Power(t2,2) - 
                  42*Power(t2,3) + s2*(3 - 2*t2 + 22*Power(t2,2))))) + 
         Power(s,9)*(1 - 29*t2 + 25*s2*t2 + 117*Power(t2,2) - 
            109*s2*Power(t2,2) - 16*Power(s2,2)*Power(t2,2) - 
            358*Power(t2,3) + 113*s2*Power(t2,3) + 
            8*Power(s2,2)*Power(t2,3) + 137*Power(t2,4) - 
            44*s2*Power(t2,4) - 14*Power(t2,5) + 3*s2*Power(t2,5) + 
            Power(t2,6) + Power(t1,3)*
             (-136 + 134*t2 - 28*Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(126 + 2*(-59 + 2*s2)*t2 + 
               (17 - 18*s2)*Power(t2,2) + 3*(-3 + s2)*Power(t2,3) + 
               2*Power(t2,4)) + 
            t1*(-27 + (158 - 109*s2)*t2 + 
               2*(32 + 64*s2 + Power(s2,2))*Power(t2,2) - 
               4*(5 + 3*s2)*Power(t2,3) - (15 + 2*s2)*Power(t2,4) + 
               2*Power(t2,5)) + 
            Power(s1,3)*(-4 + Power(s2,3) - 165*Power(t1,3) + 75*t2 - 
               259*Power(t2,2) + 168*Power(t2,3) + 
               9*Power(t1,2)*(-13 + 5*t2) - 
               Power(s2,2)*(27*t1 + 23*t2) - 
               12*t1*(1 - 3*t2 + 16*Power(t2,2)) + 
               s2*(1 + 135*Power(t1,2) - 18*t2 + 120*Power(t2,2) + 
                  t1*(26 + 90*t2))) + 
            Power(s1,2)*(1 - 
               2*(-13 + 5*s2 + Power(s2,2) + Power(s2,3))*t2 + 
               4*(-44 + 7*s2 + 11*Power(s2,2))*Power(t2,2) + 
               (302 - 79*s2)*Power(t2,3) - 154*Power(t2,4) + 
               45*Power(t1,3)*(-8 + 3*t2) + 
               Power(t1,2)*(-71 + s2*(196 - 45*t2) + 129*t2 - 
                  27*Power(t2,2)) + 
               t1*(32 - 101*t2 - 37*Power(t2,2) + 192*Power(t2,3) + 
                  Power(s2,2)*(-22 + 17*t2) + 
                  s2*(-7 + 66*t2 - 133*Power(t2,2)))) - 
            s1*(Power(t1,3)*(336 - 238*t2 + 27*Power(t2,2)) + 
               Power(t1,2)*(-118 - 16*t2 + 40*Power(t2,2) + 
                  5*Power(t2,3) + s2*(-94 + 36*t2 + 15*Power(t2,2))) + 
               t1*(4 + 13*t2 + 4*Power(s2,2)*(-4 + t2)*t2 - 
                  199*Power(t2,2) - 77*Power(t2,3) + 47*Power(t2,4) + 
                  s2*(22 + 117*t2 + 16*Power(t2,2) - 50*Power(t2,3))) - 
               t2*(-23 + 177*t2 + Power(s2,3)*t2 - 150*Power(t2,2) - 
                  73*Power(t2,3) + 27*Power(t2,4) + 
                  Power(s2,2)*(22 + 26*t2 - 23*Power(t2,2)) + 
                  s2*(4 - 183*t2 + 62*Power(t2,2) - 5*Power(t2,3))))) + 
         Power(s,8)*(-8 + 110*t2 - 79*s2*t2 - 208*Power(t2,2) + 
            175*s2*Power(t2,2) + 87*Power(s2,2)*Power(t2,2) + 
            1167*Power(t2,3) - 135*s2*Power(t2,3) - 
            83*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) - 
            853*Power(t2,4) + 114*s2*Power(t2,4) + 
            18*Power(s2,2)*Power(t2,4) + 204*Power(t2,5) - 
            16*s2*Power(t2,5) - 24*Power(t2,6) - s2*Power(t2,6) + 
            Power(t2,7) + Power(t1,3)*
             (206 - 309*t2 + 106*Power(t2,2) - 8*Power(t2,3)) + 
            Power(t1,2)*(-282 + (286 + 64*s2)*t2 + 
               (-43 + 18*s2)*Power(t2,2) - 4*(-7 + 4*s2)*Power(t2,3) + 
               (-15 + s2)*Power(t2,4) + Power(t2,5)) + 
            t1*(99 + (-375 + 181*s2)*t2 + 
               (59 - 480*s2 - 16*Power(s2,2))*Power(t2,2) + 
               2*(4 + 96*s2 + 3*Power(s2,2))*Power(t2,3) - 
               3*(-9 + 7*s2)*Power(t2,4) + (-5 + s2)*Power(t2,5)) + 
            Power(s1,4)*(24 - 8*Power(s2,3) + 330*Power(t1,3) - 
               219*t2 + 497*Power(t2,2) - 252*Power(t2,3) - 
               24*Power(t1,2)*(-12 + 5*t2) + 
               Power(s2,2)*(1 + 108*t1 + 76*t2) + 
               14*t1*(4 - 12*t2 + 27*Power(t2,2)) - 
               s2*(9 + 360*Power(t1,2) - 70*t2 + 252*Power(t2,2) + 
                  20*t1*(5 + 9*t2))) + 
            Power(s1,3)*(1 - 101*t2 + 382*Power(t2,2) - 
               617*Power(t2,3) + 322*Power(t2,4) - 
               60*Power(t1,3)*(-13 + 6*t2) + Power(s2,3)*(-7 + 17*t2) + 
               Power(t1,2)*(299 - 540*t2 + 108*Power(t2,2)) - 
               2*t1*(55 - 116*t2 - 74*Power(t2,2) + 252*Power(t2,3)) + 
               Power(s2,2)*(-3 + 56*t2 - 164*Power(t2,2) - 
                  2*t1*(-71 + 50*t2)) + 
               s2*(9 + 18*t2 - 144*Power(t2,2) + 301*Power(t2,3) + 
                  8*Power(t1,2)*(-79 + 30*t2) + 
                  t1*(-23 - 150*t2 + 392*Power(t2,2)))) + 
            Power(s1,2)*(4 + 116*t2 - 578*Power(t2,2) + 
               430*Power(t2,3) + 174*Power(t2,4) - 99*Power(t2,5) - 
               2*Power(s2,3)*t2*(-9 + 5*t2) + 
               Power(t1,3)*(902 - 789*t2 + 108*Power(t2,2)) + 
               Power(t1,2)*(-111 - 621*t2 + 337*Power(t2,2) + 
                  4*Power(t2,3)) + 
               t1*(-103 + 331*t2 - 521*Power(t2,2) - 239*Power(t2,3) + 
                  189*Power(t2,4)) + 
               Power(s2,2)*(t1*(69 - 167*t2 + 36*Power(t2,2)) + 
                  t2*(-47 - 223*t2 + 147*Power(t2,2))) + 
               s2*(-1 - 102*t2 + 461*Power(t2,2) + 75*Power(t2,3) - 
                  74*Power(t2,4) + 
                  Power(t1,2)*(-501 + 392*t2 + 12*Power(t2,2)) + 
                  t1*(66 + 376*t2 + 216*Power(t2,2) - 257*Power(t2,3)))) \
+ s1*(-10 + 112*t2 - 1044*Power(t2,2) + 1719*Power(t2,3) - 
               663*Power(t2,4) + 65*Power(t2,5) + 
               Power(s2,3)*Power(t2,2)*(-13 + 5*t2) + 
               Power(t1,3)*(614 - 734*t2 + 186*Power(t2,2) - 
                  8*Power(t2,3)) + 
               Power(t1,2)*(-444 - 58*t2 + 260*Power(t2,2) + 
                  4*Power(t2,3) - 13*Power(t2,4)) + 
               t1*(103 - 71*t2 - 367*Power(t2,2) + 87*Power(t2,3) + 
                  88*Power(t2,4) - 18*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-69 + 47*t2 + 116*Power(t2,2) - 35*Power(t2,3) + 
                  t1*(-81 + 39*t2 - 8*Power(t2,2))) + 
               s2*(1 - 96*t2 + 570*Power(t2,2) - 442*Power(t2,3) + 
                  69*Power(t2,4) - 
                  2*Power(t1,2)*
                   (92 - 121*t2 - 15*Power(t2,2) + 9*Power(t2,3)) + 
                  t1*(67 + 542*t2 - 536*Power(t2,2) - 65*Power(t2,3) + 
                     43*Power(t2,4))))) + 
         Power(s,7)*(25 - 183*t1 + 336*Power(t1,2) - 154*Power(t1,3) - 
            194*t2 + 103*s2*t2 + 398*t1*t2 - 27*s2*t1*t2 - 
            273*Power(t1,2)*t2 - 266*s2*Power(t1,2)*t2 + 
            384*Power(t1,3)*t2 + 80*Power(t2,2) + 24*s2*Power(t2,2) - 
            238*Power(s2,2)*Power(t2,2) - 324*t1*Power(t2,2) + 
            803*s2*t1*Power(t2,2) + 56*Power(s2,2)*t1*Power(t2,2) - 
            52*Power(t1,2)*Power(t2,2) + 
            129*s2*Power(t1,2)*Power(t2,2) - 
            203*Power(t1,3)*Power(t2,2) - 1815*Power(t2,3) - 
            561*s2*Power(t2,3) + 322*Power(s2,2)*Power(t2,3) + 
            16*Power(s2,3)*Power(t2,3) + 385*t1*Power(t2,3) - 
            644*s2*t1*Power(t2,3) - 50*Power(s2,2)*t1*Power(t2,3) - 
            21*Power(t1,2)*Power(t2,3) + 14*s2*Power(t1,2)*Power(t2,3) + 
            24*Power(t1,3)*Power(t2,3) + 2124*Power(t2,4) + 
            167*s2*Power(t2,4) - 132*Power(s2,2)*Power(t2,4) - 
            4*Power(s2,3)*Power(t2,4) - 196*t1*Power(t2,4) + 
            172*s2*t1*Power(t2,4) + 7*Power(s2,2)*t1*Power(t2,4) + 
            46*Power(t1,2)*Power(t2,4) - 4*s2*Power(t1,2)*Power(t2,4) - 
            770*Power(t2,5) - 9*s2*Power(t2,5) + 
            15*Power(s2,2)*Power(t2,5) + 28*t1*Power(t2,5) - 
            25*s2*t1*Power(t2,5) - 6*Power(t1,2)*Power(t2,5) + 
            116*Power(t2,6) + 13*s2*Power(t2,6) + s2*t1*Power(t2,6) - 
            8*Power(t2,7) - s2*Power(t2,7) + 
            Power(s1,5)*(-60 + 28*Power(s2,3) - 462*Power(t1,3) + 
               355*t2 - 595*Power(t2,2) + 252*Power(t2,3) + 
               42*Power(t1,2)*(-11 + 5*t2) - 
               7*Power(s2,2)*(1 + 36*t1 + 20*t2) - 
               14*t1*(10 - 27*t2 + 36*Power(t2,2)) + 
               s2*(33 + 630*Power(t1,2) - 154*t2 + 336*Power(t2,2) + 
                  14*t1*(16 + 15*t2))) + 
            Power(s1,4)*(11 + Power(s2,3)*(38 - 63*t2) + 23*t2 - 
               290*Power(t2,2) + 762*Power(t2,3) - 420*Power(t2,4) + 
               210*Power(t1,3)*(-5 + 3*t2) - 
               21*Power(t1,2)*(27 - 56*t2 + 12*Power(t2,2)) + 
               t1*(199 - 263*t2 - 336*Power(t2,2) + 840*Power(t2,3)) + 
               Power(s2,2)*(9 - 123*t2 + 336*Power(t2,2) + 
                  14*t1*(-27 + 22*t2)) - 
               s2*(36 + 20*t2 - 263*Power(t2,2) + 567*Power(t2,3) + 
                  70*Power(t1,2)*(-16 + 9*t2) + 
                  2*t1*(-57 + 329*Power(t2,2)))) + 
            Power(s1,3)*(19 - 435*t2 + 1440*Power(t2,2) - 
               1100*Power(t2,3) - 167*Power(t2,4) + 195*Power(t2,5) + 
               Power(s2,3)*(22 - 108*t2 + 41*Power(t2,2)) - 
               3*Power(t1,3)*(425 - 488*t2 + 84*Power(t2,2)) + 
               Power(t1,2)*(-189 + 2055*t2 - 1071*Power(t2,2) + 
                  28*Power(t2,3)) + 
               t1*(252 - 469*t2 + 592*Power(t2,2) + 380*Power(t2,3) - 
                  427*Power(t2,4)) + 
               Power(s2,2)*(3 + 41*t2 + 626*Power(t2,2) - 
                  393*Power(t2,3) + 
                  t1*(-301 + 638*t2 - 140*Power(t2,2))) + 
               s2*(-50 + 294*t2 - 547*Power(t2,2) - 380*Power(t2,3) + 
                  269*Power(t2,4) + 
                  Power(t1,2)*(1052 - 1235*t2 + 84*Power(t2,2)) + 
                  t1*(44 - 1210*t2 - 405*Power(t2,2) + 651*Power(t2,3)))\
) + Power(s1,2)*(-3 - 790*t2 + 3112*Power(t2,2) - 3718*Power(t2,3) + 
               1528*Power(t2,4) - 166*Power(t2,5) - 15*Power(t2,6) + 
               Power(s2,3)*t2*(-70 + 108*t2 - 25*Power(t2,2)) + 
               Power(t1,3)*(-1050 + 1594*t2 - 519*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(352 + 1627*t2 - 1584*Power(t2,2) + 
                  163*Power(t2,3) + 35*Power(t2,4)) + 
               t1*(43 - 121*t2 + 157*Power(t2,2) - 15*Power(t2,3) - 
                  186*Power(t2,4) + 66*Power(t2,5)) + 
               Power(s2,2)*(t1*
                   (-122 + 542*t2 - 315*Power(t2,2) + 48*Power(t2,3)) \
+ t2*(227 + 99*t2 - 665*Power(t2,2) + 178*Power(t2,3))) + 
               s2*(-10 + 457*t2 - 1128*Power(t2,2) + 572*Power(t2,3) + 
                  169*Power(t2,4) - 40*Power(t2,5) + 
                  3*Power(t1,2)*
                   (212 - 370*t2 + 59*Power(t2,2) + 14*Power(t2,3)) + 
                  t1*(-55 - 1840*t2 + 1484*Power(t2,2) + 
                     398*Power(t2,3) - 211*Power(t2,4)))) + 
            s1*(53 - 189*t2 + 2458*Power(t2,2) - 4999*Power(t2,3) + 
               2945*Power(t2,4) - 708*Power(t2,5) + 83*Power(t2,6) - 
               3*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*(52 - 42*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(-585 + 1080*t2 - 466*Power(t2,2) + 
                  44*Power(t2,3)) + 
               Power(t1,2)*(664 + 449*t2 - 1005*Power(t2,2) + 
                  182*Power(t2,3) + 53*Power(t2,4) - 6*Power(t2,5)) - 
               t1*(287 - 111*t2 + 342*Power(t2,2) - 450*Power(t2,3) + 
                  157*Power(t2,4) - 24*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*t2*
                (128 - 540*t2 + 45*Power(t2,2) + 134*Power(t2,3) - 
                  21*Power(t2,4) + 
                  t1*(196 - 147*t2 + 46*Power(t2,2) - 8*Power(t2,3))) - 
               s2*(3 - 226*t2 + 185*Power(t2,2) - 524*Power(t2,3) + 
                  174*Power(t2,4) + 40*Power(t2,5) - 7*Power(t2,6) + 
                  Power(t1,2)*
                   (-216 + 655*t2 - 174*Power(t2,2) - 48*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  t1*(118 + 1083*t2 - 1999*Power(t2,2) + 
                     681*Power(t2,3) + 18*Power(t2,4) - 11*Power(t2,5))))\
) + s*Power(-1 + s1,4)*(Power(s1,7)*Power(s2 - t1,2)*
             (-1 + s2 - t1 + t2) - 
            Power(s1,6)*(s2 - t1)*
             (-Power(-1 + t2,2) + 3*Power(s2,2)*t2 + 3*Power(t1,2)*t2 + 
               t1*(-2 + 5*t2 - 3*Power(t2,2)) + 
               s2*(3 - (7 + 6*t1)*t2 + 4*Power(t2,2))) + 
            Power(s1,5)*(s2 - t1)*(-1 + t2)*
             (2 + t2 - 3*Power(t2,2) + Power(s2,2)*(-5 + 2*t2) + 
               Power(t1,2)*(-7 + 3*t2) + 
               t1*(-13 + 10*t2 - 3*Power(t2,2)) + 
               s2*(4 + t1*(13 - 6*t2) - 4*t2 + 6*Power(t2,2))) - 
            (-1 + t2)*(-4 + (31 - 8*s2)*t2 + 
               (-57 + 40*s2 - 2*Power(s2,2))*Power(t2,2) + 
               (37 - 39*s2 + 10*Power(s2,2) + 2*Power(s2,3))*
                Power(t2,3) + 
               (-7 + 7*s2 - 3*Power(s2,2) + 3*Power(s2,3))*
                Power(t2,4) + Power(t1,3)*(4 - 16*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-12 + (63 - 8*s2)*t2 + 
                  (-56 + 37*s2)*Power(t2,2) + (10 - 6*s2)*Power(t2,3)) \
+ t1*(12 + 2*(-39 + 8*s2)*t2 + 
                  (110 - 77*s2 + 2*Power(s2,2))*Power(t2,2) + 
                  (-51 + 61*s2 - 24*Power(s2,2))*Power(t2,3) + 
                  (7 - 10*s2 + 3*Power(s2,2))*Power(t2,4))) + 
            Power(s1,4)*(-4*Power(-1 + t2,3) - 
               t1*Power(-1 + t2,2)*(22 - 7*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(20 - 22*t2 - 9*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(6 - 27*t2 + 29*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(7 + 33*t2 - 49*Power(t2,2) + 
                  10*Power(t2,3) - Power(t2,4)) - 
               Power(s2,2)*(25 - 79*t2 + 48*Power(t2,2) + 
                  2*Power(t2,3) + 4*Power(t2,4) + 
                  t1*(-10 - 31*t2 + 71*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(8 + 7*t2 + 3*Power(t2,2)) - 
                  3*Power(t1,2)*
                   (12 - 6*t2 - 17*Power(t2,2) + Power(t2,3)) + 
                  t1*(28 - 142*t2 + 127*Power(t2,2) - 18*Power(t2,3) + 
                     5*Power(t2,4)))) + 
            Power(s1,3)*(Power(-1 + t2,3)*(4 + 3*t2) + 
               Power(t1,3)*(-7 - 4*t2 + 26*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (13 + 47*t2 - 10*Power(t2,2) + Power(t2,3)) + 
               Power(t1,2)*(2 - 57*t2 + 26*Power(t2,2) + 
                  30*Power(t2,3) - Power(t2,4)) - 
               Power(s2,3)*(2 + 19*t2 - 54*Power(t2,2) + 
                  45*Power(t2,3) + 3*Power(t2,4)) + 
               Power(s2,2)*(5 + 49*t2 - 181*Power(t2,2) + 
                  115*Power(t2,3) + 11*Power(t2,4) + Power(t2,5) + 
                  t1*(9 - 18*t2 - 17*Power(t2,2) + 68*Power(t2,3) + 
                     3*Power(t2,4))) - 
               s2*(Power(-1 + t2,2)*
                   (-12 + 54*t2 + 8*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-5 - 26*t2 + 48*Power(t2,2) + 28*Power(t2,3)) + 
                  t1*(33 - 71*t2 - 122*Power(t2,2) + 164*Power(t2,3) - 
                     5*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(Power(-1 + t2,3)*(-8 - 7*t2 + Power(t2,2)) + 
               t1*Power(-1 + t2,2)*
                (-31 - 23*t2 - 27*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*(8 + t2 - 6*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,2)*(31 - 33*t2 + 43*Power(t2,2) - 
                  36*Power(t2,3) - 5*Power(t2,4)) + 
               Power(s2,3)*t2*
                (6 + 21*t2 - 50*Power(t2,2) + 31*Power(t2,3) + 
                  Power(t2,4)) + 
               s2*(Power(-1 + t2,2)*
                   (3 + 3*t2 + 71*Power(t2,2) + 3*Power(t2,3)) + 
                  Power(t1,2)*t2*
                   (47 - 48*t2 + 22*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(-3 + 4*t2 - 77*Power(t2,2) + 4*Power(t2,3) + 
                     72*Power(t2,4))) - 
               Power(s2,2)*(2 - 2*t2 + 36*Power(t2,2) - 
                  144*Power(t2,3) + 103*Power(t2,4) + 5*Power(t2,5) + 
                  t1*(-2 + 44*t2 - 33*Power(t2,2) - 10*Power(t2,3) + 
                     27*Power(t2,4) + Power(t2,5)))) - 
            s1*(-(Power(-1 + t2,3)*(14 + 7*t2 + 2*Power(t2,2))) + 
               Power(t1,3)*(1 - 34*t2 + 39*Power(t2,2) - 
                  8*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (27 + 32*t2 - 4*Power(t2,2) + 5*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (6 + 9*t2 - 21*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(12 + 47*t2 - 115*Power(t2,2) + 
                  73*Power(t2,3) - 17*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-4 + 19*t2 - 25*Power(t2,2) + 41*Power(t2,3) - 
                  31*Power(t2,4) + 
                  t1*(4 - 61*t2 + 52*Power(t2,2) + 3*Power(t2,3) - 
                     4*Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-8 + 35*t2 + 8*Power(t2,2) + 25*Power(t2,3)) + 
                  Power(t1,2)*
                   (-8 + 45*t2 + 9*Power(t2,2) - 52*Power(t2,3) + 
                     12*Power(t2,4)) + 
                  t1*(16 - 96*t2 + 109*Power(t2,2) - 49*Power(t2,3) + 
                     11*Power(t2,4) + 9*Power(t2,5))))) - 
         Power(s,6)*(36 - 153*t1 + 126*Power(t1,2) + 28*Power(t1,3) - 
            95*t2 - 13*s2*t2 - 182*t1*t2 + 433*s2*t1*t2 + 
            283*Power(t1,2)*t2 - 532*s2*Power(t1,2)*t2 + 
            168*Power(t1,3)*t2 - 499*Power(t2,2) + 615*s2*Power(t2,2) - 
            377*Power(s2,2)*Power(t2,2) - 32*t1*Power(t2,2) + 
            406*s2*t1*Power(t2,2) + 112*Power(s2,2)*t1*Power(t2,2) - 
            539*Power(t1,2)*Power(t2,2) + 
            525*s2*Power(t1,2)*Power(t2,2) - 
            181*Power(t1,3)*Power(t2,2) - 880*Power(t2,3) - 
            2341*s2*Power(t2,3) + 606*Power(s2,2)*Power(t2,3) + 
            52*Power(s2,3)*Power(t2,3) + 997*t1*Power(t2,3) - 
            856*s2*t1*Power(t2,3) - 171*Power(s2,2)*t1*Power(t2,3) + 
            109*Power(t1,2)*Power(t2,3) - 
            93*s2*Power(t1,2)*Power(t2,3) + 32*Power(t1,3)*Power(t2,3) + 
            2410*Power(t2,4) + 1530*s2*Power(t2,4) - 
            367*Power(s2,2)*Power(t2,4) - 24*Power(s2,3)*Power(t2,4) - 
            713*t1*Power(t2,4) + 412*s2*t1*Power(t2,4) + 
            47*Power(s2,2)*t1*Power(t2,4) + 73*Power(t1,2)*Power(t2,4) + 
            s2*Power(t1,2)*Power(t2,4) - 1366*Power(t2,5) - 
            316*s2*Power(t2,5) + 87*Power(s2,2)*Power(t2,5) + 
            2*Power(s2,3)*Power(t2,5) + 160*t1*Power(t2,5) - 
            106*s2*t1*Power(t2,5) - 3*Power(s2,2)*t1*Power(t2,5) - 
            15*Power(t1,2)*Power(t2,5) + 265*Power(t2,6) + 
            56*s2*Power(t2,6) - 4*Power(s2,2)*Power(t2,6) - 
            10*t1*Power(t2,6) + 9*s2*t1*Power(t2,6) - 19*Power(t2,7) - 
            7*s2*Power(t2,7) + 
            Power(s1,6)*(-80 + 56*Power(s2,3) - 462*Power(t1,3) + 
               252*Power(t1,2)*(-2 + t2) + 345*t2 - 455*Power(t2,2) + 
               168*Power(t2,3) - 7*Power(s2,2)*(3 + 54*t1 + 22*t2) - 
               42*t1*(5 - 12*t2 + 11*Power(t2,2)) + 
               s2*(65 + 756*Power(t1,2) - 210*t2 + 294*Power(t2,2) + 
                  14*t1*(23 + 9*t2))) + 
            Power(s1,5)*(89 + Power(s2,3)*(81 - 133*t2) - 324*t2 + 
               94*Power(t2,2) + 589*Power(t2,3) - 350*Power(t2,4) + 
               126*Power(t1,3)*(-7 + 6*t2) - 
               7*Power(t1,2)*(85 - 222*t2 + 54*Power(t2,2)) + 
               t1*(254 - 220*t2 - 476*Power(t2,2) + 924*Power(t2,3)) + 
               Power(s2,2)*(1 - 52*t2 + 406*Power(t2,2) + 
                  14*t1*(-37 + 41*t2)) + 
               s2*(-74 + 28*t2 + 248*Power(t2,2) - 623*Power(t2,3) - 
                  28*Power(t1,2)*(-41 + 36*t2) + 
                  t1*(187 - 462*t2 - 644*Power(t2,2)))) + 
            Power(s1,4)*(75 - 893*t2 + 2421*Power(t2,2) - 
               1914*Power(t2,3) - 10*Power(t2,4) + 225*Power(t2,5) + 
               4*Power(s2,3)*(19 - 65*t2 + 23*Power(t2,2)) - 
               7*Power(t1,3)*(139 - 234*t2 + 54*Power(t2,2)) + 
               Power(t1,2)*(-591 + 3209*t2 - 1841*Power(t2,2) + 
                  98*Power(t2,3)) + 
               t1*(93 + 60*t2 + 220*Power(t2,2) + 313*Power(t2,3) - 
                  595*Power(t2,4)) + 
               Power(s2,2)*(-41 + 182*t2 + 771*Power(t2,2) - 
                  559*Power(t2,3) + 
                  t1*(-491 + 1220*t2 - 308*Power(t2,2))) + 
               s2*(-62 + 191*t2 - 327*Power(t2,2) - 358*Power(t2,3) + 
                  410*Power(t2,4) + 
                  Power(t1,2)*(1058 - 1953*t2 + 294*Power(t2,2)) + 
                  t1*(463 - 2470*t2 - 50*Power(t2,2) + 931*Power(t2,3)))\
) + Power(s1,3)*(195 - 1757*t2 + 4249*Power(t2,2) - 4508*Power(t2,3) + 
               2196*Power(t2,4) - 285*Power(t2,5) - 40*Power(t2,6) + 
               Power(s2,3)*(42 - 284*t2 + 324*Power(t2,2) - 
                  48*Power(t2,3)) + 
               Power(t1,3)*(-793 + 1646*t2 - 777*Power(t2,2) + 
                  56*Power(t2,3)) + 
               Power(t1,2)*(-478 + 3540*t2 - 3425*Power(t2,2) + 
                  558*Power(t2,3) + 49*Power(t2,4)) + 
               t1*(-126 + 859*t2 - 1530*Power(t2,2) + 
                  487*Power(t2,3) - 138*Power(t2,4) + 130*Power(t2,5)) \
+ Power(s2,2)*(-54 + 513*t2 + 113*Power(t2,2) - 1239*Power(t2,3) + 
                  361*Power(t2,4) + 
                  2*t1*(-168 + 624*t2 - 457*Power(t2,2) + 
                     60*Power(t2,3))) + 
               s2*(-116 + 554*t2 - 427*Power(t2,2) + 5*Power(t2,3) + 
                  405*Power(t2,4) - 95*Power(t2,5) + 
                  2*Power(t1,2)*
                   (376 - 895*t2 + 345*Power(t2,2) + 21*Power(t2,3)) + 
                  t1*(560 - 3879*t2 + 3323*Power(t2,2) + 
                     613*Power(t2,3) - 475*Power(t2,4)))) + 
            Power(s1,2)*(2*Power(s2,3)*t2*
                (-72 + 178*t2 - 103*Power(t2,2) + 12*Power(t2,3)) + 
               3*Power(t1,3)*
                (-175 + 414*t2 - 247*Power(t2,2) + 32*Power(t2,3)) + 
               Power(t1,2)*(-193 + 2964*t2 - 3606*Power(t2,2) + 
                  1019*Power(t2,3) + 43*Power(t2,4) - 15*Power(t2,5)) + 
               t1*(68 + 650*t2 - 2745*Power(t2,2) + 2336*Power(t2,3) - 
                  496*Power(t2,4) + 30*Power(t2,5) - 5*Power(t2,6)) + 
               t2*(-1646 + 6191*t2 - 7809*Power(t2,2) + 
                  3859*Power(t2,3) - 1006*Power(t2,4) + 
                  131*Power(t2,5) - 2*Power(t2,6)) - 
               Power(s2,2)*(2 - 677*t2 + 1188*Power(t2,2) + 
                  359*Power(t2,3) - 589*Power(t2,4) + 84*Power(t2,5) + 
                  t1*(133 - 771*t2 + 836*Power(t2,2) - 
                     307*Power(t2,3) + 37*Power(t2,4))) + 
               s2*(-44 - 20*t2 + 224*Power(t2,2) + 396*Power(t2,3) + 
                  237*Power(t2,4) - 230*Power(t2,5) + 14*Power(t2,6) - 
                  Power(t1,2)*
                   (-438 + 1349*t2 - 759*Power(t2,2) + Power(t2,3) + 
                     15*Power(t2,4)) + 
                  t1*(191 - 3154*t2 + 4757*Power(t2,2) - 
                     1605*Power(t2,3) - 198*Power(t2,4) + 69*Power(t2,5)\
))) + s1*(82 + 145*t2 + 2481*Power(t2,2) - 6926*Power(t2,3) + 
               5272*Power(t2,4) - 1561*Power(t2,5) + 220*Power(t2,6) - 
               17*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*
                (82 - 128*t2 + 43*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,3)*(-233 + 694*t2 - 483*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(t1,2)*(323 + 1494*t2 - 2387*Power(t2,2) + 
                  776*Power(t2,3) + 35*Power(t2,4) - 18*Power(t2,5)) - 
               t1*(216 + 363*t2 + 1341*Power(t2,2) - 2695*Power(t2,3) + 
                  1080*Power(t2,4) - 124*Power(t2,5) + 3*Power(t2,6)) + 
               Power(s2,2)*t2*
                (169 - 1343*t2 + 1133*Power(t2,2) - 32*Power(t2,3) - 
                  56*Power(t2,4) + 4*Power(t2,5) + 
                  t1*(251 - 246*t2 + 64*Power(t2,2) - 26*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(8 + 61*t2 + 2447*Power(t2,2) - 2331*Power(t2,3) + 
                  447*Power(t2,4) - 192*Power(t2,5) + 66*Power(t2,6) - 
                  3*Power(t2,7) - 
                  2*Power(t1,2)*
                   (-88 + 444*t2 - 330*Power(t2,2) + 22*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  t1*(-161 - 1230*t2 + 2950*Power(t2,2) - 
                     2003*Power(t2,3) + 427*Power(t2,4) - 
                     39*Power(t2,5) + Power(t2,6))))) + 
         Power(s,3)*(-37 + 171*t1 - 243*Power(t1,2) + 109*Power(t1,3) + 
            430*t2 - 191*s2*t2 - 1225*t1*t2 + 434*s2*t1*t2 + 
            1079*Power(t1,2)*t2 - 239*s2*Power(t1,2)*t2 - 
            291*Power(t1,3)*t2 - 1142*Power(t2,2) + 745*s2*Power(t2,2) - 
            82*Power(s2,2)*Power(t2,2) + 2036*t1*Power(t2,2) - 
            1150*s2*t1*Power(t2,2) + 56*Power(s2,2)*t1*Power(t2,2) - 
            1372*Power(t1,2)*Power(t2,2) + 
            627*s2*Power(t1,2)*Power(t2,2) + 
            199*Power(t1,3)*Power(t2,2) + 1668*Power(t2,3) - 
            1455*s2*Power(t2,3) + 34*Power(s2,2)*Power(t2,3) + 
            52*Power(s2,3)*Power(t2,3) - 1089*t1*Power(t2,3) + 
            1112*s2*t1*Power(t2,3) - 246*Power(s2,2)*t1*Power(t2,3) + 
            589*Power(t1,2)*Power(t2,3) - 
            414*s2*Power(t1,2)*Power(t2,3) - 
            32*Power(t1,3)*Power(t2,3) - 1477*Power(t2,4) + 
            1505*s2*Power(t2,4) + 14*Power(s2,2)*Power(t2,4) - 
            50*Power(s2,3)*Power(t2,4) - 8*t1*Power(t2,4) - 
            378*s2*t1*Power(t2,4) + 185*Power(s2,2)*t1*Power(t2,4) - 
            58*Power(t1,2)*Power(t2,4) + 64*s2*Power(t1,2)*Power(t2,4) + 
            674*Power(t2,5) - 761*s2*Power(t2,5) + 
            27*Power(s2,2)*Power(t2,5) + 10*Power(s2,3)*Power(t2,5) + 
            178*t1*Power(t2,5) - s2*t1*Power(t2,5) - 
            30*Power(s2,2)*t1*Power(t2,5) - 6*Power(t1,2)*Power(t2,5) - 
            128*Power(t2,6) + 153*s2*Power(t2,6) - 
            20*Power(s2,2)*Power(t2,6) - 52*t1*Power(t2,6) + 
            21*s2*t1*Power(t2,6) + 12*Power(t2,7) - 7*s2*Power(t2,7) + 
            Power(s1,9)*(-4 + 28*Power(s2,3) - 55*Power(t1,3) + 9*t2 - 
               7*Power(t2,2) + 2*Power(t2,3) + 
               Power(s2,2)*(-21 - 108*t1 + 4*t2) + 
               9*Power(t1,2)*(-7 + 5*t2) + 
               t1*(-36 + 63*t2 - 28*Power(t2,2)) + 
               s2*(19 + 135*Power(t1,2) + t1*(80 - 45*t2) - 30*t2 + 
                  12*Power(t2,2))) + 
            Power(s1,8)*(37 - 65*t2 + 18*Power(t2,2) + 17*Power(t2,3) - 
               7*Power(t2,4) + 45*Power(t1,3)*(1 + 3*t2) - 
               Power(s2,3)*(18 + 77*t2) + 
               Power(t1,2)*(16 + 210*t2 - 108*Power(t2,2)) + 
               t1*(105 - 92*t2 - 88*Power(t2,2) + 84*Power(t2,3)) + 
               Power(s2,2)*(-17 + 139*t2 - 16*Power(t2,2) + 
                  t1*(98 + 260*t2)) - 
               s2*(74 - 92*t2 - 4*Power(t2,2) + 31*Power(t2,3) + 
                  Power(t1,2)*(128 + 315*t2) + 
                  t1*(22 + 285*t2 - 83*Power(t2,2)))) + 
            Power(s1,7)*(-1 - 233*t2 + 450*Power(t2,2) - 
               216*Power(t2,3) - 9*Power(t2,4) + 9*Power(t2,5) + 
               Power(t1,3)*(-17 + 24*t2 - 108*Power(t2,2)) + 
               Power(s2,3)*(20 - 18*t2 + 55*Power(t2,2)) + 
               Power(t1,2)*(-259 + 421*t2 - 397*Power(t2,2) + 
                  76*Power(t2,3)) + 
               t1*(-266 + 295*t2 + 66*Power(t2,2) - 16*Power(t2,3) - 
                  89*Power(t2,4)) + 
               Power(s2,2)*(-143 + 181*t2 - 182*Power(t2,2) + 
                  13*Power(t2,3) + t1*(-89 + 82*t2 - 196*Power(t2,2))) \
+ s2*(140 + 10*t2 - 369*Power(t2,2) + 210*Power(t2,3) + 
                  19*Power(t2,4) + 
                  19*Power(t1,2)*(4 - 3*t2 + 12*Power(t2,2)) + 
                  t1*(410 - 526*t2 + 389*Power(t2,2) + 17*Power(t2,3)))) \
+ Power(s1,6)*(-51 + 462*t2 - 402*Power(t2,2) - 292*Power(t2,3) + 
               298*Power(t2,4) - 10*Power(t2,5) - 5*Power(t2,6) + 
               Power(s2,3)*(14 - 72*t2 + 190*Power(t2,2) + 
                  15*Power(t2,3)) + 
               Power(t1,3)*(126 - 386*t2 - 21*Power(t2,2) + 
                  28*Power(t2,3)) + 
               Power(t1,2)*(242 - 215*t2 - 520*Power(t2,2) + 
                  261*Power(t2,3) - 7*Power(t2,4)) + 
               t1*(117 + 605*t2 - 1405*Power(t2,2) + 447*Power(t2,3) + 
                  102*Power(t2,4) + 38*Power(t2,5)) + 
               Power(s2,2)*(-82 + 771*t2 - 1089*Power(t2,2) + 
                  127*Power(t2,3) + 10*Power(t2,4) + 
                  t1*(48 - 96*t2 - 529*Power(t2,2) + 48*Power(t2,3))) - 
               s2*(-102 + 1189*t2 - 1618*Power(t2,2) + 
                  144*Power(t2,3) + 301*Power(t2,4) - 10*Power(t2,5) + 
                  Power(t1,2)*
                   (196 - 618*t2 - 255*Power(t2,2) + 42*Power(t2,3)) + 
                  t1*(95 + 770*t2 - 1710*Power(t2,2) + 
                     230*Power(t2,3) + 113*Power(t2,4)))) + 
            Power(s1,5)*(227 - 874*t2 + 251*Power(t2,2) + 
               921*Power(t2,3) - 457*Power(t2,4) - 80*Power(t2,5) + 
               11*Power(t2,6) + Power(t2,7) - 
               3*Power(t1,3)*
                (27 - 48*t2 - 98*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*(4 + 14*t2 + 8*Power(t2,2) - 
                  236*Power(t2,3) - 27*Power(t2,4)) + 
               Power(t1,2)*(-30 + 25*t2 + 471*Power(t2,2) + 
                  326*Power(t2,3) - 65*Power(t2,4) - 6*Power(t2,5)) + 
               t1*(9 - 393*t2 + 274*Power(t2,2) + 1114*Power(t2,3) - 
                  571*Power(t2,4) - 54*Power(t2,5) - 5*Power(t2,6)) + 
               Power(s2,2)*(91 - 448*t2 - 14*Power(t2,2) + 
                  1295*Power(t2,3) - 122*Power(t2,4) - 
                  15*Power(t2,5) + 
                  t1*(-8 + 60*t2 + 163*Power(t2,2) + 550*Power(t2,3) - 
                     10*Power(t2,4))) - 
               s2*(306 - 1438*t2 + 463*Power(t2,2) + 1946*Power(t2,3) - 
                  816*Power(t2,4) - 100*Power(t2,5) + 13*Power(t2,6) + 
                  Power(t1,2)*
                   (-112 + 275*t2 + 510*Power(t2,2) + 
                     184*Power(t2,3) + 6*Power(t2,4)) + 
                  t1*(166 - 395*t2 + 69*Power(t2,2) + 
                     1881*Power(t2,3) - 144*Power(t2,4) - 69*Power(t2,5)\
))) + s1*(-135 + 283*t2 - 1020*Power(t2,2) + 1501*Power(t2,3) - 
               679*Power(t2,4) + 38*Power(t2,5) + 9*Power(t2,6) + 
               3*Power(t2,7) + 
               2*Power(t1,3)*
                (-92 + 232*t2 - 129*Power(t2,2) + 18*Power(t2,3)) - 
               Power(s2,3)*Power(t2,2)*
                (90 + 36*t2 - 125*Power(t2,2) + 46*Power(t2,3)) + 
               Power(t1,2)*(139 - 862*t2 + 821*Power(t2,2) - 
                  98*Power(t2,3) - 69*Power(t2,4) + 18*Power(t2,5)) + 
               t1*(187 + 646*t2 - 1774*Power(t2,2) + 
                  1354*Power(t2,3) - 407*Power(t2,4) - 
                  58*Power(t2,5) + 43*Power(t2,6)) + 
               Power(s2,2)*t2*
                (84 + 74*t2 + 385*Power(t2,2) - 732*Power(t2,3) + 
                  235*Power(t2,4) + 12*Power(t2,5) + 
                  t1*(-44 + 235*t2 - 2*Power(t2,2) - 80*Power(t2,3) + 
                     20*Power(t2,4))) + 
               s2*(89 - 144*t2 + 1469*Power(t2,2) - 2952*Power(t2,3) + 
                  1756*Power(t2,4) - 124*Power(t2,5) - 95*Power(t2,6) + 
                  10*Power(t2,7) + 
                  Power(t1,2)*
                   (109 + 123*t2 - 774*Power(t2,2) + 472*Power(t2,3) - 
                     70*Power(t2,4)) + 
                  t1*(-202 - 372*t2 + 1025*Power(t2,2) - 
                     487*Power(t2,3) + 16*Power(t2,4) + 
                     33*Power(t2,5) - 20*Power(t2,6)))) - 
            Power(s1,3)*(155 - 507*t2 + 1114*Power(t2,2) - 
               224*Power(t2,3) - 1213*Power(t2,4) + 699*Power(t2,5) + 
               4*Power(t2,6) - 28*Power(t2,7) + 
               Power(t1,3)*(15 - 8*t2 + 88*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(s2,3)*t2*
                (88 - 187*t2 + 208*Power(t2,2) - 90*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(t1,2)*(-309 + 845*t2 - 545*Power(t2,2) + 
                  336*Power(t2,3) - 166*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(334 - 573*t2 - 1006*Power(t2,2) + 
                  1076*Power(t2,3) - 403*Power(t2,4) + 
                  112*Power(t2,5) + 14*Power(t2,6)) + 
               Power(s2,2)*(123 - 727*t2 + 1374*Power(t2,2) - 
                  1379*Power(t2,3) + 326*Power(t2,4) + 
                  80*Power(t2,5) + 4*Power(t2,6) + 
                  t1*(-45 + 34*t2 + 58*Power(t2,2) + 132*Power(t2,3) - 
                     74*Power(t2,4) - 12*Power(t2,5))) + 
               s2*(-702 + 1150*t2 - 1279*Power(t2,2) + 
                  1072*Power(t2,3) + 883*Power(t2,4) - 
                  652*Power(t2,5) - 36*Power(t2,6) + 10*Power(t2,7) + 
                  Power(t1,2)*
                   (-32 - 49*t2 - 96*Power(t2,2) - 32*Power(t2,3) + 
                     36*Power(t2,4)) + 
                  t1*(322 - 180*t2 + 217*Power(t2,2) - 
                     655*Power(t2,3) + 280*Power(t2,4) + 
                     54*Power(t2,5) - 12*Power(t2,6)))) + 
            Power(s1,4)*(-152 + 155*t2 + 1346*Power(t2,2) - 
               1881*Power(t2,3) + 158*Power(t2,4) + 440*Power(t2,5) - 
               64*Power(t2,6) - 2*Power(t2,7) - 
               Power(t1,3)*(6 - 28*t2 + 15*Power(t2,2) + 
                  72*Power(t2,3)) + 
               Power(s2,3)*(16 - 117*t2 + 132*Power(t2,2) - 
                  18*Power(t2,3) + 88*Power(t2,4) + 6*Power(t2,5)) + 
               Power(t1,2)*(53 - 377*t2 + 416*Power(t2,2) - 
                  493*Power(t2,3) - 64*Power(t2,4) + 18*Power(t2,5)) + 
               t1*(164 - 443*t2 + 26*Power(t2,2) - 347*Power(t2,3) - 
                  194*Power(t2,4) + 210*Power(t2,5)) + 
               Power(s2,2)*(-133 + 661*t2 - 544*Power(t2,2) - 
                  420*Power(t2,3) - 314*Power(t2,4) + 63*Power(t2,5) + 
                  4*Power(t2,6) + 
                  t1*(8 - 88*t2 + 226*Power(t2,2) - 142*Power(t2,3) - 
                     197*Power(t2,4) + 6*Power(t2,5))) + 
               s2*(106 - 543*t2 - 913*Power(t2,2) + 2460*Power(t2,3) - 
                  85*Power(t2,4) - 471*Power(t2,5) + 27*Power(t2,6) + 
                  3*Power(t2,7) + 
                  Power(t1,2)*
                   (-16 + 62*t2 - 123*Power(t2,2) + 166*Power(t2,3) + 
                     56*Power(t2,4)) + 
                  t1*(-52 + 585*t2 - 571*Power(t2,2) + 
                     516*Power(t2,3) + 756*Power(t2,4) - 
                     93*Power(t2,5) - 11*Power(t2,6)))) - 
            Power(s1,2)*(49 - 926*t2 + 1580*Power(t2,2) - 
               1256*Power(t2,3) + 640*Power(t2,4) + 52*Power(t2,5) - 
               181*Power(t2,6) + 42*Power(t2,7) - 
               3*Power(t1,3)*
                (26 - 42*t2 - Power(t2,2) + 4*Power(t2,3)) + 
               Power(s2,3)*t2*
                (-38 - 158*t2 + 209*Power(t2,2) - 94*Power(t2,3) + 
                  28*Power(t2,4)) + 
               Power(t1,2)*(164 - 519*t2 - 144*Power(t2,2) + 
                  325*Power(t2,3) - 97*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(117 + 29*t2 + 113*Power(t2,2) + 471*Power(t2,3) - 
                  764*Power(t2,4) + 202*Power(t2,5) - 28*Power(t2,6)) + 
               Power(s2,2)*(20 - 121*t2 + 1253*Power(t2,2) - 
                  1667*Power(t2,3) + 770*Power(t2,4) - 
                  218*Power(t2,5) - 8*Power(t2,6) + 
                  t1*(-6 + 140*t2 - 103*Power(t2,2) + 76*Power(t2,3) - 
                     28*Power(t2,4) + 8*Power(t2,5))) + 
               s2*(74 + 1813*t2 - 3658*Power(t2,2) + 2110*Power(t2,3) - 
                  693*Power(t2,4) + 110*Power(t2,5) + 108*Power(t2,6) - 
                  4*Power(t2,7) + 
                  Power(t1,2)*
                   (124 - 34*t2 - 201*Power(t2,2) + 30*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  t1*(-369 - 404*t2 + 1200*Power(t2,2) - 
                     298*Power(t2,3) + 145*Power(t2,4) - 
                     46*Power(t2,5) + 2*Power(t2,6))))) - 
         Power(s,2)*Power(-1 + s1,2)*
          (-20 + 72*t1 - 84*Power(t1,2) + 32*Power(t1,3) + 186*t2 - 
            62*s2*t2 - 492*t1*t2 + 128*s2*t1*t2 + 417*Power(t1,2)*t2 - 
            66*s2*Power(t1,2)*t2 - 111*Power(t1,3)*t2 - 
            460*Power(t2,2) + 264*s2*Power(t2,2) - 
            18*Power(s2,2)*Power(t2,2) + 917*t1*Power(t2,2) - 
            472*s2*t1*Power(t2,2) + 16*Power(s2,2)*t1*Power(t2,2) - 
            587*Power(t1,2)*Power(t2,2) + 
            234*s2*Power(t1,2)*Power(t2,2) + 
            92*Power(t1,3)*Power(t2,2) + 544*Power(t2,3) - 
            398*s2*Power(t2,3) + 6*Power(s2,2)*Power(t2,3) + 
            16*Power(s2,3)*Power(t2,3) - 675*t1*Power(t2,3) + 
            577*s2*t1*Power(t2,3) - 105*Power(s2,2)*t1*Power(t2,3) + 
            292*Power(t1,2)*Power(t2,3) - 
            191*s2*Power(t1,2)*Power(t2,3) - 
            16*Power(t1,3)*Power(t2,3) - 383*Power(t2,4) + 
            295*s2*Power(t2,4) + 25*Power(s2,2)*Power(t2,4) - 
            14*Power(s2,3)*Power(t2,4) + 178*t1*Power(t2,4) - 
            262*s2*t1*Power(t2,4) + 97*Power(s2,2)*t1*Power(t2,4) - 
            41*Power(t1,2)*Power(t2,4) + 31*s2*Power(t1,2)*Power(t2,4) + 
            171*Power(t2,5) - 128*s2*Power(t2,5) - 
            13*Power(s2,2)*Power(t2,5) + 14*t1*Power(t2,5) + 
            32*s2*t1*Power(t2,5) - 15*Power(s2,2)*t1*Power(t2,5) - 
            Power(t1,2)*Power(t2,5) - 45*Power(t2,6) + 
            30*s2*Power(t2,6) - 4*Power(s2,2)*Power(t2,6) - 
            14*t1*Power(t2,6) + 5*s2*t1*Power(t2,6) + 7*Power(t2,7) - 
            s2*Power(t2,7) + Power(s1,8)*
             (8*Power(s2,3) + Power(s2,2)*(-7 - 27*t1 + 5*t2) + 
               s2*(3 + 30*Power(t1,2) + t1*(19 - 15*t2) - 4*t2 + 
                  Power(t2,2)) - 
               t1*(5 + 11*Power(t1,2) - 8*t2 + 3*Power(t2,2) - 
                  2*t1*(-6 + 5*t2))) + 
            Power(s1,7)*(Power(-1 + t2,2)*(-1 + 2*t2) - 
               Power(s2,3)*(1 + 23*t2) + Power(t1,3)*(3 + 30*t2) + 
               Power(t1,2)*(-11 + 51*t2 - 27*Power(t2,2)) + 
               t1*(9 + 3*t2 - 22*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s2,2)*(-15 + 47*t2 - 19*Power(t2,2) + 
                  t1*(8 + 73*t2)) + 
               s2*(-10 + 7*t2 + 5*Power(t2,2) - 2*Power(t2,3) - 
                  10*Power(t1,2)*(1 + 8*t2) + 
                  t1*(22 - 90*t2 + 42*Power(t2,2)))) + 
            Power(s1,6)*(-7*Power(-1 + t2,3)*(3 + t2) + 
               4*Power(s2,3)*(3 - 6*t2 + 4*Power(t2,2)) - 
               3*Power(t1,3)*(6 - 13*t2 + 9*Power(t2,2)) + 
               Power(t1,2)*(-89 + 167*t2 - 115*Power(t2,2) + 
                  23*Power(t2,3)) + 
               t1*(-37 + 38*t2 - 14*Power(t2,2) + 25*Power(t2,3) - 
                  12*Power(t2,4)) + 
               Power(s2,2)*(-50 + 81*t2 - 72*Power(t2,2) + 
                  27*Power(t2,3) + t1*(-48 + 94*t2 - 60*Power(t2,2))) + 
               s2*(1 + 64*t2 - 95*Power(t2,2) + 31*Power(t2,3) - 
                  Power(t2,4) + 
                  Power(t1,2)*(52 - 105*t2 + 69*Power(t2,2)) - 
                  2*t1*(-64 + 105*t2 - 72*Power(t2,2) + 17*Power(t2,3)))\
) + Power(s1,5)*(Power(t1,3)*
                (66 - 118*t2 - 33*Power(t2,2) + 8*Power(t2,3)) + 
               Power(-1 + t2,2)*
                (-10 - 51*t2 + 30*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s2,3)*(15 - 65*t2 + 98*Power(t2,2) + 
                  10*Power(t2,3)) + 
               Power(t1,2)*(69 + 63*t2 - 262*Power(t2,2) + 
                  84*Power(t2,3) - 5*Power(t2,4)) + 
               t1*(-29 + 298*t2 - 386*Power(t2,2) + 121*Power(t2,3) - 
                  10*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*(-103 + 458*t2 - 440*Power(t2,2) + 
                  51*Power(t2,3) - 17*Power(t2,4) + 
                  t1*(20 + 49*t2 - 270*Power(t2,2) + 8*Power(t2,3))) + 
               s2*(104 - 351*t2 + 208*Power(t2,2) + 105*Power(t2,3) - 
                  71*Power(t2,4) + 5*Power(t2,5) - 
                  2*Power(t1,2)*
                   (55 - 80*t2 - 90*Power(t2,2) + 9*Power(t2,3)) - 
                  t1*(-44 + 537*t2 - 681*Power(t2,2) + 85*Power(t2,3) + 
                     Power(t2,4)))) - 
            Power(s1,4)*(5*Power(t1,3)*(3 + t2 - 22*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-27 - 39*t2 - 9*Power(t2,2) + 16*Power(t2,3) + 
                  5*Power(t2,4)) + 
               2*Power(s2,3)*
                (5 + t2 - 40*Power(t2,2) + 61*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(t1,2)*(-6 + 107*t2 - 111*Power(t2,2) - 
                  158*Power(t2,3) + 17*Power(t2,4) + Power(t2,5)) + 
               t1*(-13 + 178*t2 + 101*Power(t2,2) - 403*Power(t2,3) + 
                  136*Power(t2,4) + Power(t2,6)) - 
               Power(s2,2)*(57 + 26*t2 - 618*Power(t2,2) + 
                  728*Power(t2,3) - 47*Power(t2,4) + 4*Power(t2,5) + 
                  t1*(7 + 23*t2 - 68*Power(t2,2) + 261*Power(t2,3) + 
                     7*Power(t2,4))) + 
               s2*(88 - 8*t2 - 663*Power(t2,2) + 681*Power(t2,3) - 
                  56*Power(t2,4) - 46*Power(t2,5) + 4*Power(t2,6) + 
                  Power(t1,2)*
                   (-14 - 8*t2 + 168*Power(t2,2) + 103*Power(t2,3) + 
                     Power(t2,4)) - 
                  t1*(-39 + 100*t2 + 383*Power(t2,2) - 
                     777*Power(t2,3) + 22*Power(t2,4) + 11*Power(t2,5)))\
) + s1*(Power(s2,3)*Power(t2,2)*
                (-40 + 10*t2 + 35*Power(t2,2) - 19*Power(t2,3)) + 
               Power(t1,3)*(-36 + 158*t2 - 125*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,2)*(-9 - 227*t2 + 324*Power(t2,2) - 
                  96*Power(t2,3) + 3*Power(t2,4) + 4*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (72 - 39*t2 + 188*Power(t2,2) - 40*Power(t2,3) - 
                  28*Power(t2,4) + 13*Power(t2,5)) + 
               t1*(117 - 38*t2 - 169*Power(t2,2) + 113*Power(t2,3) - 
                  32*Power(t2,4) - 8*Power(t2,5) + 17*Power(t2,6)) + 
               Power(s2,2)*t2*
                (28 + 60*t2 - 83*Power(t2,2) - 72*Power(t2,3) + 
                  54*Power(t2,4) + 12*Power(t2,5) + 
                  t1*(-24 + 186*t2 - 138*Power(t2,2) + 
                     20*Power(t2,3) + 5*Power(t2,4))) + 
               s2*(42 - 148*t2 + 294*Power(t2,2) - 500*Power(t2,3) + 
                  347*Power(t2,4) + 18*Power(t2,5) - 56*Power(t2,6) + 
                  3*Power(t2,7) - 
                  2*Power(t1,2)*
                   (-23 + 46*t2 + 72*Power(t2,2) - 85*Power(t2,3) + 
                     18*Power(t2,4)) + 
                  t1*(-88 + 188*t2 - 136*Power(t2,2) + 
                     185*Power(t2,3) - 151*Power(t2,4) + 
                     9*Power(t2,5) - 5*Power(t2,6)))) + 
            Power(s1,3)*(-(Power(t1,3)*
                  (17 + 6*t2 - 46*Power(t2,2) + 32*Power(t2,3))) + 
               Power(-1 + t2,2)*
                (-23 - 124*t2 + 32*Power(t2,2) + 17*Power(t2,3) - 
                  4*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(87 - 351*t2 + 309*Power(t2,2) - 
                  172*Power(t2,3) - 30*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,3)*(-8 + 2*t2 - 6*Power(t2,2) - 
                  32*Power(t2,3) + 51*Power(t2,4) + 5*Power(t2,5)) - 
               t1*(65 - 377*t2 + 191*Power(t2,2) + 76*Power(t2,3) + 
                  86*Power(t2,4) - 42*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*(16 - 3*t2 - 17*Power(t2,2) + 
                  212*Power(t2,3) - 397*Power(t2,4) + 36*Power(t2,5) - 
                  t1*(-32 + 102*t2 - 60*Power(t2,2) - 82*Power(t2,3) + 
                     104*Power(t2,4) + Power(t2,5))) + 
               s2*(130 - 118*t2 - 291*Power(t2,2) - 75*Power(t2,3) + 
                  470*Power(t2,4) - 113*Power(t2,5) - 4*Power(t2,6) + 
                  Power(t2,7) + 
                  2*Power(t1,2)*
                   (5 + 50*t2 - 78*Power(t2,2) + 24*Power(t2,3) + 
                     14*Power(t2,4)) + 
                  t1*(-150 + 227*t2 + 69*Power(t2,2) - 
                     172*Power(t2,3) + 348*Power(t2,4) - 
                     13*Power(t2,5) - 3*Power(t2,6)))) - 
            Power(s1,2)*(Power(t1,3)*
                (4 - 13*t2 + 63*Power(t2,2) - 16*Power(t2,3)) + 
               2*Power(s2,3)*t2*
                (-16 - 6*t2 + 21*Power(t2,2) - 12*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(-1 + t2,2)*
                (-2 + 203*t2 + 32*Power(t2,2) - 72*Power(t2,3) + 
                  12*Power(t2,4) + 5*Power(t2,5)) + 
               Power(t1,2)*(-43 + 23*t2 - 247*Power(t2,2) + 
                  289*Power(t2,3) - 90*Power(t2,4) + 6*Power(t2,5)) + 
               t1*(75 + 16*t2 + 31*Power(t2,2) - 79*Power(t2,3) - 
                  98*Power(t2,4) + 54*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*(10 + 82*t2 - 4*Power(t2,2) - 
                  179*Power(t2,3) + 52*Power(t2,4) - 31*Power(t2,5) + 
                  8*Power(t2,6) + 
                  t1*(-8 + 113*t2 - 136*Power(t2,2) + 108*Power(t2,3) + 
                     20*Power(t2,4) - 11*Power(t2,5))) + 
               s2*(6 + 276*t2 - 711*Power(t2,2) + 240*Power(t2,3) + 
                  216*Power(t2,4) + 4*Power(t2,5) - 34*Power(t2,6) + 
                  3*Power(t2,7) + 
                  Power(t1,2)*
                   (32 - 75*t2 + 15*Power(t2,2) - 94*Power(t2,3) + 
                     22*Power(t2,4)) - 
                  t1*(64 + 209*t2 - 711*Power(t2,2) + 306*Power(t2,3) + 
                     44*Power(t2,4) - 39*Power(t2,5) + 3*Power(t2,6))))) \
+ Power(s,5)*(15 + 27*t1 - 210*Power(t1,2) + 188*Power(t1,3) + 238*t2 - 
            217*s2*t2 - 1164*t1*t2 + 841*s2*t1*t2 + 
            1159*Power(t1,2)*t2 - 644*s2*Power(t1,2)*t2 - 
            210*Power(t1,3)*t2 - 1291*Power(t2,2) + 
            1199*s2*Power(t2,2) - 366*Power(s2,2)*Power(t2,2) + 
            1236*t1*Power(t2,2) - 711*s2*t1*Power(t2,2) + 
            140*Power(s2,2)*t1*Power(t2,2) - 
            1354*Power(t1,2)*Power(t2,2) + 
            945*s2*Power(t1,2)*Power(t2,2) + 
            13*Power(t1,3)*Power(t2,2) + 1394*Power(t2,3) - 
            3748*s2*Power(t2,3) + 584*Power(s2,2)*Power(t2,3) + 
            90*Power(s2,3)*Power(t2,3) + 753*t1*Power(t2,3) - 
            146*s2*t1*Power(t2,3) - 318*Power(s2,2)*t1*Power(t2,3) + 
            399*Power(t1,2)*Power(t2,3) - 
            328*s2*Power(t1,2)*Power(t2,3) + 
            10*Power(t1,3)*Power(t2,3) + 377*Power(t2,4) + 
            3310*s2*Power(t2,4) - 461*Power(s2,2)*Power(t2,4) - 
            59*Power(s2,3)*Power(t2,4) - 1103*t1*Power(t2,4) + 
            359*s2*t1*Power(t2,4) + 133*Power(s2,2)*t1*Power(t2,4) + 
            52*Power(t1,2)*Power(t2,4) + 30*s2*Power(t1,2)*Power(t2,4) - 
            957*Power(t2,5) - 1034*s2*Power(t2,5) + 
            179*Power(s2,2)*Power(t2,5) + 9*Power(s2,3)*Power(t2,5) + 
            375*t1*Power(t2,5) - 176*s2*t1*Power(t2,5) - 
            15*Power(s2,2)*t1*Power(t2,5) - 20*Power(t1,2)*Power(t2,5) + 
            295*Power(t2,6) + 146*s2*Power(t2,6) - 
            20*Power(s2,2)*Power(t2,6) - 44*t1*Power(t2,6) + 
            26*s2*t1*Power(t2,6) - 20*Power(t2,7) - 16*s2*Power(t2,7) + 
            Power(s1,7)*(-60 + 70*Power(s2,3) - 330*Power(t1,3) + 
               201*t2 - 217*Power(t2,2) + 72*Power(t2,3) + 
               42*Power(t1,2)*(-9 + 5*t2) - 
               7*Power(s2,2)*(5 + 54*t1 + 14*t2) - 
               4*t1*(49 - 105*t2 + 72*Power(t2,2)) + 
               s2*(75 + 308*t1 + 630*Power(t1,2) - 182*t2 + 
                  168*Power(t2,2))) + 
            Power(s1,6)*(175 + Power(s2,3)*(80 - 175*t2) - 528*t2 + 
               284*Power(t2,2) + 286*Power(t2,3) - 182*Power(t2,4) + 
               210*Power(t1,3)*(-2 + 3*t2) - 
               7*Power(t1,2)*(49 - 186*t2 + 54*Power(t2,2)) + 
               t1*(273 - 221*t2 - 434*Power(t2,2) + 672*Power(t2,3)) + 
               Power(s2,2)*(-25 + 143*t2 + 280*Power(t2,2) + 
                  14*t1*(-25 + 49*t2)) + 
               s2*(-115 + 132*t2 + 123*Power(t2,2) - 413*Power(t2,3) - 
                  14*Power(t1,2)*(-44 + 75*t2) - 
                  2*t1*(-71 + 399*t2 + 161*Power(t2,2)))) + 
            Power(s1,5)*(70 - 953*t2 + 2493*Power(t2,2) - 
               1910*Power(t2,3) + 89*Power(t2,4) + 153*Power(t2,5) - 
               7*Power(t1,3)*(49 - 156*t2 + 54*Power(t2,2)) + 
               Power(s2,3)*(91 - 311*t2 + 125*Power(t2,2)) + 
               Power(t1,2)*(-715 + 2811*t2 - 1883*Power(t2,2) + 
                  154*Power(t2,3)) + 
               t1*(-284 + 660*t2 - 73*Power(t2,2) + 100*Power(t2,3) - 
                  525*Power(t2,4)) + 
               Power(s2,2)*(-174 + 474*t2 + 324*Power(t2,2) - 
                  435*Power(t2,3) - 
                  2*t1*(179 - 629*t2 + 210*Power(t2,2))) + 
               s2*(104 - 149*t2 - 277*Power(t2,2) + 95*Power(t2,3) + 
                  325*Power(t2,4) + 
                  Power(t1,2)*(468 - 1687*t2 + 462*Power(t2,2)) + 
                  t1*(871 - 2824*t2 + 645*Power(t2,2) + 763*Power(t2,3))\
)) + Power(s1,4)*(281 - 1367*t2 + 2453*Power(t2,2) - 3052*Power(t2,3) + 
               2021*Power(t2,4) - 290*Power(t2,5) - 45*Power(t2,6) + 
               Power(s2,3)*(88 - 409*t2 + 481*Power(t2,2) - 
                  40*Power(t2,3)) + 
               Power(t1,3)*(-176 + 622*t2 - 651*Power(t2,2) + 
                  70*Power(t2,3)) + 
               Power(t1,2)*(-931 + 3123*t2 - 3658*Power(t2,2) + 
                  835*Power(t2,3) + 35*Power(t2,4)) + 
               t1*(-509 + 1931*t2 - 3115*Power(t2,2) + 
                  1059*Power(t2,3) + 70*Power(t2,4) + 150*Power(t2,5)) \
+ Power(s2,2)*(-235 + 937*t2 - 722*Power(t2,2) - 932*Power(t2,3) + 
                  360*Power(t2,4) + 
                  t1*(-308 + 1221*t2 - 1359*Power(t2,2) + 
                     160*Power(t2,3))) + 
               s2*(14 - 440*t2 + 1568*Power(t2,2) - 690*Power(t2,3) + 
                  93*Power(t2,4) - 85*Power(t2,5) + 
                  Power(t1,2)*(296 - 1032*t2 + 1011*Power(t2,2)) + 
                  t1*(1224 - 4415*t2 + 4745*Power(t2,2) + 
                     210*Power(t2,3) - 575*Power(t2,4)))) + 
            Power(s1,3)*(424 - 2610*t2 + 5786*Power(t2,2) - 
               4977*Power(t2,3) + 1901*Power(t2,4) - 736*Power(t2,5) + 
               130*Power(t2,6) + 2*Power(t2,7) + 
               4*Power(t1,3)*
                (-29 + 93*t2 - 104*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,3)*(51 - 377*t2 + 686*Power(t2,2) - 
                  365*Power(t2,3) + 23*Power(t2,4)) - 
               Power(t1,2)*(1065 - 3564*t2 + 4198*Power(t2,2) - 
                  1692*Power(t2,3) + 55*Power(t2,4) + 20*Power(t2,5)) - 
               t1*(361 - 2151*t2 + 4945*Power(t2,2) - 
                  4122*Power(t2,3) + 944*Power(t2,4) + 
                  19*Power(t2,5) + 10*Power(t2,6)) - 
               Power(s2,2)*(213 - 1374*t2 + 1657*Power(t2,2) + 
                  91*Power(t2,3) - 689*Power(t2,4) + 125*Power(t2,5) + 
                  t1*(198 - 910*t2 + 1387*Power(t2,2) - 
                     680*Power(t2,3) + 65*Power(t2,4))) + 
               s2*(67 - 662*t2 + 932*Power(t2,2) - 933*Power(t2,3) + 
                  908*Power(t2,4) - 251*Power(t2,5) + 6*Power(t2,6) - 
                  2*Power(t1,2)*
                   (-124 + 419*t2 - 459*Power(t2,2) + 84*Power(t2,3) + 
                     10*Power(t2,4)) + 
                  t1*(985 - 4299*t2 + 6282*Power(t2,2) - 
                     2924*Power(t2,3) - 207*Power(t2,4) + 
                     145*Power(t2,5)))) - 
            s1*(16 - 866*t2 + 326*Power(t2,2) + 3393*Power(t2,3) - 
               4110*Power(t2,4) + 1487*Power(t2,5) - 172*Power(t2,6) + 
               Power(t1,3)*(19 - 88*t2 + 126*Power(t2,2) - 
                  28*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (-27 + 177*t2 - 123*Power(t2,2) + 14*Power(t2,3)) + 
               Power(t1,2)*(186 - 2119*t2 + 3199*Power(t2,2) - 
                  1362*Power(t2,3) + 105*Power(t2,4) + 12*Power(t2,5)) \
+ t1*(-221 + 1049*t2 + 1054*Power(t2,2) - 3910*Power(t2,3) + 
                  2315*Power(t2,4) - 441*Power(t2,5) + 34*Power(t2,6)) \
+ Power(s2,2)*t2*(-178 + 1469*t2 - 2288*Power(t2,2) + 
                  853*Power(t2,3) - 15*Power(t2,4) - 8*Power(t2,5) + 
                  t1*(-156 + 137*t2 + 16*Power(t2,2) + 9*Power(t2,3) - 
                     4*Power(t2,4))) + 
               s2*(-50 + 339*t2 - 5585*Power(t2,2) + 7594*Power(t2,3) - 
                  2829*Power(t2,4) + 415*Power(t2,5) - 
                  120*Power(t2,6) + 14*Power(t2,7) + 
                  Power(t1,2)*
                   (-146 + 571*t2 - 708*Power(t2,2) + 
                     216*Power(t2,3) - 16*Power(t2,4)) + 
                  t1*(218 + 991*t2 - 2121*Power(t2,2) + 
                     1723*Power(t2,3) - 705*Power(t2,4) + 
                     147*Power(t2,5) - 12*Power(t2,6)))) - 
            Power(s1,2)*(149 + 971*t2 - 5242*Power(t2,2) + 
               7452*Power(t2,3) - 4168*Power(t2,4) + 783*Power(t2,5) - 
               88*Power(t2,6) + 14*Power(t2,7) + 
               Power(t1,3)*(64 - 222*t2 + 234*Power(t2,2) - 
                  48*Power(t2,3)) + 
               Power(s2,3)*t2*
                (148 - 513*t2 + 508*Power(t2,2) - 133*Power(t2,3) + 
                  7*Power(t2,4)) + 
               Power(t1,2)*(1004 - 3440*t2 + 4082*Power(t2,2) - 
                  1702*Power(t2,3) + 151*Power(t2,4) + 12*Power(t2,5)) + 
               t1*(-493 - 728*t2 + 4639*Power(t2,2) - 
                  5448*Power(t2,3) + 2175*Power(t2,4) - 
                  269*Power(t2,5) + 8*Power(t2,6)) + 
               Power(s2,2)*(10 - 1172*t2 + 2974*Power(t2,2) - 
                  1810*Power(t2,3) - 245*Power(t2,4) + 
                  177*Power(t2,5) - 12*Power(t2,6) + 
                  t1*(88 - 441*t2 + 709*Power(t2,2) - 454*Power(t2,3) + 
                     139*Power(t2,4) - 11*Power(t2,5))) + 
               s2*(47 + 2307*t2 - 4966*Power(t2,2) + 2997*Power(t2,3) - 
                  395*Power(t2,4) + 395*Power(t2,5) - 112*Power(t2,6) + 
                  2*Power(t2,7) - 
                  2*Power(t1,2)*
                   (94 - 353*t2 + 378*Power(t2,2) - 92*Power(t2,3) + 
                     3*Power(t2,4)) + 
                  2*t1*(-216 + 1277*t2 - 2372*Power(t2,2) + 
                     1610*Power(t2,3) - 355*Power(t2,4) + 
                     7*Power(t2,5) + 3*Power(t2,6))))) + 
         Power(s,4)*(24 - 183*t1 + 354*Power(t1,2) - 199*Power(t1,3) - 
            501*t2 + 295*s2*t2 + 1652*t1*t2 - 801*s2*t1*t2 - 
            1527*Power(t1,2)*t2 + 496*s2*Power(t1,2)*t2 + 
            396*Power(t1,3)*t2 + 1598*Power(t2,2) - 1226*s2*Power(t2,2) + 
            221*Power(s2,2)*Power(t2,2) - 2339*t1*Power(t2,2) + 
            1442*s2*t1*Power(t2,2) - 112*Power(s2,2)*t1*Power(t2,2) + 
            1793*Power(t1,2)*Power(t2,2) - 
            987*s2*Power(t1,2)*Power(t2,2) - 
            197*Power(t1,3)*Power(t2,2) - 2521*Power(t2,3) + 
            3140*s2*Power(t2,3) - 262*Power(s2,2)*Power(t2,3) - 
            90*Power(s2,3)*Power(t2,3) + 443*t1*Power(t2,3) - 
            904*s2*t1*Power(t2,3) + 355*Power(s2,2)*t1*Power(t2,3) - 
            649*Power(t1,2)*Power(t2,3) + 
            497*s2*Power(t1,2)*Power(t2,3) + 24*Power(t1,3)*Power(t2,3) + 
            1753*Power(t2,4) - 3269*s2*Power(t2,4) + 
            226*Power(s2,2)*Power(t2,4) + 75*Power(s2,3)*Power(t2,4) + 
            700*t1*Power(t2,4) + 85*s2*t1*Power(t2,4) - 
            205*Power(s2,2)*t1*Power(t2,4) + 15*Power(t1,2)*Power(t2,4) - 
            65*s2*Power(t1,2)*Power(t2,4) - 311*Power(t2,5) + 
            1377*s2*Power(t2,5) - 147*Power(s2,2)*Power(t2,5) - 
            15*Power(s2,3)*Power(t2,5) - 397*t1*Power(t2,5) + 
            117*s2*t1*Power(t2,5) + 30*Power(s2,2)*t1*Power(t2,5) + 
            15*Power(t1,2)*Power(t2,5) - 64*Power(t2,6) - 
            225*s2*Power(t2,6) + 32*Power(s2,2)*Power(t2,6) + 
            72*t1*Power(t2,6) - 34*s2*t1*Power(t2,6) + 3*Power(t2,7) + 
            16*s2*Power(t2,7) + 
            Power(s1,8)*(24 - 56*Power(s2,3) + 165*Power(t1,3) - 65*t2 + 
               59*Power(t2,2) - 18*Power(t2,3) + 
               7*Power(s2,2)*(5 + 36*t1 + 4*t2) - 
               24*Power(t1,2)*(-8 + 5*t2) - 
               s2*(51 + 360*Power(t1,2) + t1*(196 - 60*t2) - 98*t2 + 
                  60*Power(t2,2)) + t1*(112 - 216*t2 + 117*Power(t2,2))) \
+ Power(s1,7)*(-137 + 323*t2 - 154*Power(t2,2) - 87*Power(t2,3) + 
               54*Power(t2,4) - 60*Power(t1,3)*(-1 + 6*t2) + 
               Power(s2,3)*(-25 + 147*t2) + 
               Power(s2,2)*(35 + t1*(42 - 532*t2) - 228*t2 - 
                  84*Power(t2,2)) + 
               9*Power(t1,2)*(9 - 76*t2 + 28*Power(t2,2)) + 
               t1*(-222 + 196*t2 + 252*Power(t2,2) - 312*Power(t2,3)) + 
               s2*(125 - 170*t2 - 28*Power(t2,2) + 159*Power(t2,3) + 
                  8*Power(t1,2)*(-7 + 90*t2) + 
                  t1*(-33 + 654*t2 + 8*Power(t2,2)))) + 
            Power(s1,6)*(6 + 568*t2 - 1476*Power(t2,2) + 
               994*Power(t2,3) - 42*Power(t2,4) - 57*Power(t2,5) + 
               Power(s2,3)*(-45 + 175*t2 - 106*Power(t2,2)) + 
               21*Power(t1,3)*(1 - 18*t2 + 12*Power(t2,2)) + 
               Power(t1,2)*(527 - 1431*t2 + 1155*Power(t2,2) - 
                  140*Power(t2,3)) + 
               t1*(429 - 661*t2 + 11*Power(t2,2) + 23*Power(t2,3) + 
                  287*Power(t2,4)) + 
               Power(s2,2)*(244 - 483*t2 + 159*Power(t2,2) + 
                  153*Power(t2,3) + 2*t1*(65 - 323*t2 + 182*Power(t2,2))\
) - s2*(225 - 174*t2 - 455*Power(t2,2) + 357*Power(t2,3) + 
                  130*Power(t2,4) + 
                  Power(t1,2)*(64 - 721*t2 + 420*Power(t2,2)) + 
                  t1*(812 - 1752*t2 + 804*Power(t2,2) + 315*Power(t2,3)))\
) + Power(s1,5)*(-136 + 34*Power(t2,2) + 1097*Power(t2,3) - 
               1099*Power(t2,4) + 139*Power(t2,5) + 24*Power(t2,6) + 
               Power(t1,3)*(-109 + 286*t2 + 273*Power(t2,2) - 
                  56*Power(t2,3)) + 
               Power(s2,3)*(-58 + 246*t2 - 393*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(359 - 1000*t2 + 2013*Power(t2,2) - 
                  656*Power(t2,3) - 7*Power(t2,4)) - 
               t1*(-237 + 1485*t2 - 2785*Power(t2,2) + 
                  991*Power(t2,3) + 180*Power(t2,4) + 102*Power(t2,5)) + 
               Power(s2,2)*(265 - 1089*t2 + 1508*Power(t2,2) + 
                  130*Power(t2,3) - 165*Power(t2,4) + 
                  t1*(97 - 397*t2 + 1128*Power(t2,2) - 120*Power(t2,3))) \
+ s2*(-137 + 1350*t2 - 2530*Power(t2,2) + 624*Power(t2,3) + 
                  347*Power(t2,4) + 22*Power(t2,5) + 
                  Power(t1,2)*
                   (80 - 284*t2 - 732*Power(t2,2) + 42*Power(t2,3)) + 
                  t1*(-747 + 2576*t2 - 3844*Power(t2,2) + 
                     277*Power(t2,3) + 377*Power(t2,4)))) + 
            Power(s1,4)*(-268 + 1601*t2 - 2333*Power(t2,2) + 
               325*Power(t2,3) + 325*Power(t2,4) + 290*Power(t2,5) - 
               74*Power(t2,6) - 3*Power(t2,7) - 
               5*Power(t1,3)*
                (4 - 24*t2 + 29*Power(t2,2) + 8*Power(t2,3)) + 
               Power(s2,3)*(-57 + 273*t2 - 458*Power(t2,2) + 
                  344*Power(t2,3) + 8*Power(t2,4)) + 
               Power(t1,2)*(420 - 1401*t2 + 1551*Power(t2,2) - 
                  1201*Power(t2,3) + 115*Power(t2,4) + 15*Power(t2,5)) + 
               t1*(2 - 1150*t2 + 2812*Power(t2,2) - 3203*Power(t2,3) + 
                  1021*Power(t2,4) + 71*Power(t2,5) + 10*Power(t2,6)) + 
               Power(s2,2)*(299 - 967*t2 + 1159*Power(t2,2) - 
                  1022*Power(t2,3) - 170*Power(t2,4) + 80*Power(t2,5) + 
                  t1*(71 - 354*t2 + 798*Power(t2,2) - 789*Power(t2,3) + 
                     50*Power(t2,4))) + 
               s2*(-128 + 134*t2 - 282*Power(t2,2) + 2084*Power(t2,3) - 
                  1212*Power(t2,4) + 14*Power(t2,5) + 11*Power(t2,6) + 
                  Power(t1,2)*
                   (-40 + 86*t2 - 147*Power(t2,2) + 265*Power(t2,3) + 
                     15*Power(t2,4)) - 
                  t1*(522 - 2189*t2 + 3552*Power(t2,2) - 
                     3182*Power(t2,3) + 57*Power(t2,4) + 145*Power(t2,5))\
)) + s1*(155 - 992*t2 + 2496*Power(t2,2) - 1749*Power(t2,3) - 
               186*Power(t2,4) + 357*Power(t2,5) - 12*Power(t2,7) + 
               Power(t1,3)*(111 - 138*t2 + 5*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (71 + 113*t2 - 168*Power(t2,2) + 40*Power(t2,3)) + 
               Power(t1,2)*(157 - 896*t2 + 1725*Power(t2,2) - 
                  1036*Power(t2,3) + 197*Power(t2,4) - 12*Power(t2,5)) + 
               t1*(-436 + 623*t2 + 741*Power(t2,2) - 2307*Power(t2,3) + 
                  1643*Power(t2,4) - 366*Power(t2,5) + 34*Power(t2,6)) - 
               Power(s2,2)*t2*
                (147 - 622*t2 + 1750*Power(t2,2) - 1348*Power(t2,3) + 
                  218*Power(t2,4) + 4*Power(t2,5) + 
                  t1*(11 + 122*t2 - 76*Power(t2,2) + 13*Power(t2,3) + 
                     2*Power(t2,4))) + 
               s2*(-95 + 396*t2 - 4932*Power(t2,2) + 8128*Power(t2,3) - 
                  4201*Power(t2,4) + 584*Power(t2,5) - 26*Power(t2,6) + 
                  6*Power(t2,7) + 
                  Power(t1,2)*
                   (-144 + 56*t2 + 96*Power(t2,2) + 22*Power(t2,3) - 
                     12*Power(t2,4)) + 
                  t1*(257 + 702*t2 - 1102*Power(t2,2) + 
                     365*Power(t2,3) - 177*Power(t2,4) + 
                     58*Power(t2,5) - 6*Power(t2,6)))) + 
            Power(s1,2)*(284 - 857*t2 + 56*Power(t2,2) + 
               1380*Power(t2,3) - 1582*Power(t2,4) + 592*Power(t2,5) + 
               30*Power(t2,6) - 28*Power(t2,7) + 
               Power(t1,3)*(-15 + 22*t2 - 54*Power(t2,2) + 
                  16*Power(t2,3)) + 
               Power(s2,3)*t2*
                (46 - 360*t2 + 510*Power(t2,2) - 249*Power(t2,3) + 
                  21*Power(t2,4)) + 
               Power(t1,2)*(771 - 1841*t2 + 1605*Power(t2,2) - 
                  706*Power(t2,3) + 110*Power(t2,4) - 6*Power(t2,5)) + 
               t1*(-472 - 177*t2 + 2703*Power(t2,2) - 
                  2871*Power(t2,3) + 1496*Power(t2,4) - 
                  346*Power(t2,5) + 38*Power(t2,6)) + 
               Power(s2,2)*(20 - 912*t2 + 2993*Power(t2,2) - 
                  2993*Power(t2,3) + 1038*Power(t2,4) + 
                  61*Power(t2,5) - 16*Power(t2,6) + 
                  t1*(27 + 32*t2 - 26*Power(t2,2) - 118*Power(t2,3) + 
                     75*Power(t2,4) - 14*Power(t2,5))) - 
               s2*(-30 - 3585*t2 + 7615*Power(t2,2) - 5453*Power(t2,3) + 
                  1259*Power(t2,4) + 185*Power(t2,5) + 62*Power(t2,6) - 
                  12*Power(t2,7) + 
                  Power(t1,2)*
                   (16 - 145*t2 + 102*Power(t2,2) - 94*Power(t2,3) + 
                     22*Power(t2,4)) + 
                  t1*(438 - 592*t2 + 1030*Power(t2,2) - 
                     1029*Power(t2,3) + 492*Power(t2,4) - 
                     108*Power(t2,5) + 6*Power(t2,6)))) + 
            Power(s1,3)*(-128 + 931*t2 - 2680*Power(t2,2) + 
               3587*Power(t2,3) - 1255*Power(t2,4) - 386*Power(t2,5) + 
               84*Power(t2,6) + 8*Power(t2,7) + 
               2*Power(t1,3)*(-7 + 26*t2 - 67*Power(t2,2) + 
                  24*Power(t2,3)) + 
               Power(t1,2)*(339 - 1340*t2 + 1426*Power(t2,2) - 
                  732*Power(t2,3) + 210*Power(t2,4) - 12*Power(t2,5)) + 
               Power(s2,3)*(-31 + 249*t2 - 578*Power(t2,2) + 
                  494*Power(t2,3) - 130*Power(t2,4) + 2*Power(t2,5)) + 
               t1*(309 - 1278*t2 + 2742*Power(t2,2) - 3198*Power(t2,3) + 
                  1657*Power(t2,4) - 332*Power(t2,5) + 6*Power(t2,6)) + 
               Power(s2,2)*(302 - 1658*t2 + 2734*Power(t2,2) - 
                  1456*Power(t2,3) + 11*Power(t2,4) + 80*Power(t2,5) - 
                  12*Power(t2,6) + 
                  t1*(21 - 140*t2 + 274*Power(t2,2) - 428*Power(t2,3) + 
                     221*Power(t2,4) - 14*Power(t2,5))) + 
               s2*(-655 + 2314*t2 - 2822*Power(t2,2) + 481*Power(t2,3) + 
                  76*Power(t2,4) + 476*Power(t2,5) - 82*Power(t2,6) - 
                  2*Power(t2,7) - 
                  4*Power(t1,2)*
                   (10 - 27*t2 + 3*Power(t2,2) - 26*Power(t2,3) + 
                     11*Power(t2,4)) + 
                  t1*(-357 + 1684*t2 - 2350*Power(t2,2) + 
                     2126*Power(t2,3) - 984*Power(t2,4) + 
                     54*Power(t2,5) + 14*Power(t2,6))))))*T2q(s1,s))/
     (Power(s,2)*(-1 + s1)*Power(1 - 2*s + Power(s,2) - 2*s1 - 2*s*s1 + 
         Power(s1,2),2)*(-1 + s2)*(-s + s2 - t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) - 
    (8*((-1 + s1)*(s1 - t2)*Power(-1 + t2,4)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,10)*(Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
         Power(s,9)*(Power(t1,3)*(-9 - 6*s1 + 6*t2) + 
            t1*t2*(3 + s1*(-1 + 3*s2 - 7*t2) + 2*t2 + 5*Power(t2,2) - 
               s2*(3 + t2)) + 
            Power(t1,2)*(3 + (-2 + s2)*t2 + s1*(-3 + 3*s2 + t2)) + 
            Power(t2,2)*(4 + 9*t2 - 13*Power(t2,2) + s2*(-3 + 2*t2) + 
               2*s1*(-5 + 2*s2 + 4*t2))) + 
         Power(s,8)*(Power(t1,3)*
             (33 + 15*Power(s1,2) + s1*(43 - 33*t2) - 49*t2 + 
               15*Power(t2,2)) - 
            Power(t1,2)*(24 + (-29 + 4*s2)*t2 + 
               (8 - 6*s2)*Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(-13 + 15*s2 + 5*t2) + 
               s1*(-7 + s2*(20 - 14*t2) + 24*t2 - 6*Power(t2,2))) + 
            t1*(3 + (-28 + 23*s2)*t2 - 3*(5 + 7*s2)*Power(t2,2) + 
               (21 - 6*s2)*Power(t2,3) + 10*Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) - 5*t2 + 
                  18*Power(t2,2) - s2*(3 + 10*t2)) - 
               s1*(3 - 24*t2 + Power(s2,2)*t2 + 4*Power(t2,2) + 
                  35*Power(t2,3) + s2*(-3 + 11*t2 - 26*Power(t2,2)))) + 
            t2*(3 - 25*t2 + Power(s2,2)*t2 - 4*Power(t2,2) + 
               56*Power(t2,3) - 36*Power(t2,4) + 
               Power(s1,2)*(-11 + 3*Power(s2,2) + s2*(2 - 13*t2) + 
                  27*t2 - 12*Power(t2,2)) + 
               s2*(-3 + 23*t2 - 18*Power(t2,2) + 13*Power(t2,3)) + 
               s1*(2 + 33*t2 - 86*Power(t2,2) + 47*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + 3*s2*(1 + 5*Power(t2,2))))) + 
         Power(s,7)*(1 - 23*t2 + 19*s2*t2 + 72*Power(t2,2) - 
            69*s2*Power(t2,2) - 14*Power(s2,2)*Power(t2,2) - 
            18*Power(t2,3) + 81*s2*Power(t2,3) - 
            Power(s2,2)*Power(t2,3) - 97*Power(t2,4) - 
            58*s2*Power(t2,4) + 143*Power(t2,5) + 36*s2*Power(t2,5) - 
            55*Power(t2,6) + Power(t1,3)*
             (-61 + 156*t2 - 109*Power(t2,2) + 20*Power(t2,3)) - 
            Power(t1,2)*(-75 + (139 + 5*s2)*t2 + 
               (-76 + 21*s2)*Power(t2,2) + (5 - 15*s2)*Power(t2,3) + 
               5*Power(t2,4)) + 
            t1*(-21 - 12*(-9 + 5*s2)*t2 + 
               (-61 + 165*s2 + 2*Power(s2,2))*Power(t2,2) - 
               4*(37 + 14*s2)*Power(t2,3) + (75 - 16*s2)*Power(t2,4) + 
               10*Power(t2,5)) + 
            Power(s1,3)*(Power(s2,3) - 20*Power(t1,3) - 
               4*Power(s2,2)*(3*t1 + 2*t2) + 
               2*Power(t1,2)*(-11 + 5*t2) + 
               t1*(-7 + 21*t2 - 22*Power(t2,2)) + 
               4*(-1 + 5*t2 - 6*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(1 + 30*Power(t1,2) - 8*t2 + 15*Power(t2,2) + 
                  t1*(11 + 10*t2))) + 
            Power(s1,2)*(1 + 
               (26 + 9*s2 - 11*Power(s2,2) - 2*Power(s2,3))*t2 + 
               2*(-67 + 14*s2 + 14*Power(s2,2))*Power(t2,2) + 
               (167 - 51*s2)*Power(t2,3) - 65*Power(t2,4) + 
               Power(t1,3)*(-85 + 75*t2) + 
               Power(t1,2)*(-55 + s2*(81 - 75*t2) + 108*t2 - 
                  27*Power(t2,2)) + 
               t1*(19 - 29*t2 - 58*Power(t2,2) + 80*Power(t2,3) + 
                  Power(s2,2)*(-16 + 21*t2) + 
                  s2*(2 + 27*t2 - 68*Power(t2,2)))) + 
            s1*(Power(t1,3)*(-124 + 209*t2 - 75*Power(t2,2)) + 
               Power(t1,2)*(30 + 87*t2 - 101*Power(t2,2) + 
                  20*Power(t2,3) + s2*(51 - 92*t2 + 24*Power(t2,2))) - 
               t1*(-5 + 98*t2 + 7*Power(s2,2)*(-2 + t2)*t2 - 
                  211*Power(t2,2) + 20*Power(t2,3) + 73*Power(t2,4) + 
                  s2*(16 + 33*t2 + 71*Power(t2,2) - 85*Power(t2,3))) + 
               t2*(-4 - 44*t2 + Power(s2,3)*t2 + 236*Power(t2,2) - 
                  299*Power(t2,3) + 113*Power(t2,4) + 
                  Power(s2,2)*(16 + 12*t2 - 26*Power(t2,2)) + 
                  s2*(-5 - 42*t2 + 15*Power(t2,2) + 9*Power(t2,3))))) + 
         Power(s,6)*(-6 + 64*t2 - 38*s2*t2 - 117*Power(t2,2) + 
            80*s2*Power(t2,2) + 58*Power(s2,2)*Power(t2,2) - 
            46*Power(t2,3) - 159*s2*Power(t2,3) - 
            14*Power(s2,2)*Power(t2,3) - 2*Power(s2,3)*Power(t2,3) + 
            225*Power(t2,4) + 138*s2*Power(t2,4) - 
            17*Power(s2,2)*Power(t2,4) - 263*Power(t2,5) - 
            119*s2*Power(t2,5) + 193*Power(t2,6) + 55*s2*Power(t2,6) - 
            50*Power(t2,7) + Power(t1,3)*
             (51 - 230*t2 + 290*Power(t2,2) - 126*Power(t2,3) + 
               15*Power(t2,4)) + 
            Power(t1,2)*(-108 + (276 + 58*s2)*t2 - 
               3*(81 + 11*s2)*Power(t2,2) + (65 - 43*s2)*Power(t2,3) + 
               (21 + 20*s2)*Power(t2,4) - 10*Power(t2,5)) + 
            t1*(54 + (-203 + 38*s2)*t2 - 
               6*(-57 + 63*s2 + 2*Power(s2,2))*Power(t2,2) + 
               (119 + 406*s2 + 12*Power(s2,2))*Power(t2,3) - 
               (406 + 69*s2)*Power(t2,4) + (132 - 25*s2)*Power(t2,5) + 
               5*Power(t2,6)) + 
            Power(s1,4)*(4 - 3*Power(s2,3) + 15*Power(t1,3) - 9*t2 + 
               7*Power(t2,2) - 2*Power(t2,3) - 
               2*Power(t1,2)*(-9 + 5*t2) + 
               Power(s2,2)*(1 + 18*t1 + 6*t2) - 
               s2*(4 + 15*t1 + 30*Power(t1,2) - 10*t2 + 
                  7*Power(t2,2)) + t1*(11 - 23*t2 + 13*Power(t2,2))) + 
            Power(s1,3)*(2 - 90*Power(t1,3)*(-1 + t2) - 56*t2 + 
               130*Power(t2,2) - 119*Power(t2,3) + 43*Power(t2,4) + 
               5*Power(s2,3)*(-1 + 2*t2) + 
               Power(s2,2)*(-3 + t1*(53 - 66*t2) + 30*t2 - 
                  43*Power(t2,2)) + 
               Power(t1,2)*(95 - 166*t2 + 48*Power(t2,2)) + 
               t1*(4 - 70*t2 + 142*Power(t2,2) - 78*Power(t2,3)) + 
               s2*(11 - 57*Power(t2,2) + 48*Power(t2,3) + 
                  4*Power(t1,2)*(-33 + 35*t2) + 
                  t1*(-36 + 23*t2 + 52*Power(t2,2)))) - 
            Power(s1,2)*(-6 + 44*t2 - 243*Power(t2,2) + 
               514*Power(t2,3) - 443*Power(t2,4) + 134*Power(t2,5) + 
               Power(s2,3)*t2*(-14 + 11*t2) + 
               Power(t1,3)*(-192 + 364*t2 - 153*Power(t2,2)) + 
               Power(t1,2)*(-101 + 450*t2 - 377*Power(t2,2) + 
                  75*Power(t2,3)) + 
               t1*(86 - 239*t2 + 130*Power(t2,2) + 198*Power(t2,3) - 
                  147*Power(t2,4)) + 
               Power(s2,2)*(t2*(7 + 103*t2 - 82*Power(t2,2)) + 
                  t1*(-34 + 117*t2 - 61*Power(t2,2))) + 
               s2*(1 + 30*t2 - 60*Power(t2,2) - 64*Power(t2,3) + 
                  65*Power(t2,4) + 
                  Power(t1,2)*(169 - 360*t2 + 153*Power(t2,2)) + 
                  t1*(-8 - 50*t2 - 179*Power(t2,2) + 162*Power(t2,3)))) \
+ s1*(-5 + 2*t2 + 141*Power(t2,2) - 445*Power(t2,3) + 687*Power(t2,4) - 
               522*Power(t2,5) + 142*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*(-11 + 4*t2) + 
               Power(t1,3)*(183 - 504*t2 + 409*Power(t2,2) - 
                  90*Power(t2,3)) + 
               Power(t1,2)*(-136 - 71*t2 + 446*Power(t2,2) - 
                  261*Power(t2,3) + 41*Power(t2,4)) + 
               t1*(26 + 88*t2 - 579*Power(t2,2) + 579*Power(t2,3) - 
                  53*Power(t2,4) - 83*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-34 + 32*t2 + 91*Power(t2,2) - 57*Power(t2,3) + 
                  t1*(-52 + 80*t2 - 17*Power(t2,2))) + 
               s2*(1 - 29*t2 + 97*Power(t2,2) - 121*Power(t2,3) + 
                  112*Power(t2,4) - 38*Power(t2,5) + 
                  Power(t1,2)*
                   (-62 + 212*t2 - 165*Power(t2,2) + 13*Power(t2,3)) + 
                  t1*(32 + 245*t2 - 293*Power(t2,2) - 181*Power(t2,3) + 
                     146*Power(t2,4))))) + 
         Power(s,5)*(12 - 54*t1 + 45*Power(t1,2) + 9*Power(t1,3) - 
            64*t2 + 8*s2*t2 + 109*t1*t2 + 109*s2*t1*t2 - 
            131*Power(t1,2)*t2 - 145*s2*Power(t1,2)*t2 + 
            104*Power(t1,3)*t2 + 29*Power(t2,2) + 82*s2*Power(t2,2) - 
            108*Power(s2,2)*Power(t2,2) - 386*t1*Power(t2,2) + 
            176*s2*t1*Power(t2,2) + 30*Power(s2,2)*t1*Power(t2,2) + 
            181*Power(t1,2)*Power(t2,2) + 
            273*s2*Power(t1,2)*Power(t2,2) - 
            306*Power(t1,3)*Power(t2,2) + 338*Power(t2,3) + 
            23*s2*Power(t2,3) + 59*Power(s2,2)*Power(t2,3) + 
            12*Power(s2,3)*Power(t2,3) + 188*t1*Power(t2,3) - 
            653*s2*t1*Power(t2,3) - 68*Power(s2,2)*t1*Power(t2,3) - 
            113*Power(t1,2)*Power(t2,3) - 
            93*s2*Power(t1,2)*Power(t2,3) + 
            263*Power(t1,3)*Power(t2,3) - 669*Power(t2,4) - 
            189*s2*Power(t2,4) + 85*Power(s2,2)*Power(t2,4) - 
            10*Power(s2,3)*Power(t2,4) + 560*t1*Power(t2,4) + 
            428*s2*t1*Power(t2,4) + 31*Power(s2,2)*t1*Power(t2,4) - 
            18*Power(t1,2)*Power(t2,4) - 42*s2*Power(t1,2)*Power(t2,4) - 
            79*Power(t1,3)*Power(t2,4) + 549*Power(t2,5) + 
            177*s2*Power(t2,5) - 38*Power(s2,2)*Power(t2,5) - 
            546*t1*Power(t2,5) - 31*s2*t1*Power(t2,5) + 
            44*Power(t1,2)*Power(t2,5) + 15*s2*Power(t1,2)*Power(t2,5) + 
            6*Power(t1,3)*Power(t2,5) - 315*Power(t2,6) - 
            151*s2*Power(t2,6) + 128*t1*Power(t2,6) - 
            25*s2*t1*Power(t2,6) - 10*Power(t1,2)*Power(t2,6) + 
            147*Power(t2,7) + 50*s2*Power(t2,7) + t1*Power(t2,7) - 
            27*Power(t2,8) + Power(s1,5)*
             (3*Power(s2,3) - 2*Power(s2,2)*(1 + 6*t1) + 
               s2*(3 + 15*Power(t1,2) + t1*(9 - 5*t2) - 4*t2 + 
                  Power(t2,2)) - 
               t1*(5 + 6*Power(t1,2) + t1*(7 - 5*t2) - 8*t2 + 
                  3*Power(t2,2))) - 
            Power(s1,4)*(Power(t1,3)*(55 - 60*t2) + 
               Power(s2,3)*(-12 + 17*t2) + 
               Power(s2,2)*(4 + t1*(67 - 82*t2) + 8*t2 - 
                  19*Power(t2,2)) + 
               Power(-1 + t2,2)*(9 - 3*t2 + 14*Power(t2,2)) + 
               Power(t1,2)*(71 - 120*t2 + 42*Power(t2,2)) + 
               t1*(32 - 101*t2 + 98*Power(t2,2) - 29*Power(t2,3)) + 
               s2*(-6 + 31*t2 - 36*Power(t2,2) + 11*Power(t2,3) + 
                  5*Power(t1,2)*(-22 + 25*t2) + 
                  t1*(-59 + 80*t2 - 7*Power(t2,2)))) + 
            Power(s1,3)*(Power(s2,3)*(11 - 46*t2 + 30*Power(t2,2)) - 
               2*Power(t1,3)*(80 - 167*t2 + 81*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (4 + 44*t2 - 111*Power(t2,2) + 75*Power(t2,3)) + 
               Power(t1,2)*(-199 + 591*t2 - 497*Power(t2,2) + 
                  122*Power(t2,3)) + 
               t1*(14 + 72*t2 - 334*Power(t2,2) + 364*Power(t2,3) - 
                  116*Power(t2,4)) - 
               Power(s2,2)*(3 + 24*t2 - 112*Power(t2,2) + 
                  68*Power(t2,3) + t1*(93 - 261*t2 + 146*Power(t2,2))) \
+ s2*(-22 + 63*t2 + 27*Power(t2,2) - 123*Power(t2,3) + 55*Power(t2,4) + 
                  Power(t1,2)*(228 - 521*t2 + 264*Power(t2,2)) + 
                  t1*(81 - 220*t2 + 54*Power(t2,2) + 51*Power(t2,3)))) - 
            Power(s1,2)*(Power(s2,3)*t2*(40 - 72*t2 + 19*Power(t2,2)) + 
               Power(t1,3)*(216 - 657*t2 + 615*Power(t2,2) - 
                  160*Power(t2,3)) + 
               Power(t1,2)*(131 - 919*t2 + 1437*Power(t2,2) - 
                  741*Power(t2,3) + 119*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (27 - 135*t2 + 226*Power(t2,2) - 314*Power(t2,3) + 
                  132*Power(t2,4)) - 
               t1*(127 - 492*t2 + 483*Power(t2,2) + 93*Power(t2,3) - 
                  356*Power(t2,4) + 145*Power(t2,5)) + 
               Power(s2,2)*(t2*
                   (-98 + 9*t2 + 235*Power(t2,2) - 119*Power(t2,3)) + 
                  t1*(38 - 247*t2 + 328*Power(t2,2) - 79*Power(t2,3))) \
+ s2*(7 + 29*t2 - 103*Power(t2,2) + 51*Power(t2,3) + 2*Power(t2,4) + 
                  14*Power(t2,5) + 
                  Power(t1,2)*
                   (-162 + 610*t2 - 639*Power(t2,2) + 150*Power(t2,3)) \
+ t1*(-39 + 419*t2 - 336*Power(t2,2) - 288*Power(t2,3) + 
                     190*Power(t2,4)))) + 
            s1*(Power(s2,3)*Power(t2,2)*(29 - 24*t2 + 7*Power(t2,2)) + 
               Power(t1,3)*(-145 + 579*t2 - 783*Power(t2,2) + 
                  403*Power(t2,3) - 60*Power(t2,4)) + 
               Power(t1,2)*(197 - 88*t2 - 814*Power(t2,2) + 
                  985*Power(t2,3) - 386*Power(t2,4) + 49*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (18 + 30*t2 - 354*Power(t2,2) + 286*Power(t2,3) - 
                  291*Power(t2,4) + 98*Power(t2,5)) + 
               t1*(-75 + 198*t2 + 396*Power(t2,2) - 1128*Power(t2,3) + 
                  724*Power(t2,4) - 58*Power(t2,5) - 57*Power(t2,6)) + 
               Power(s2,2)*t2*
                (44 - 106*t2 - 120*Power(t2,2) + 193*Power(t2,3) - 
                  68*Power(t2,4) + 
                  t1*(78 - 228*t2 + 139*Power(t2,2) - 19*Power(t2,3))) - 
               s2*(1 - 81*t2 + 179*Power(t2,2) - 101*Power(t2,3) + 
                  196*Power(t2,4) - 276*Power(t2,5) + 82*Power(t2,6) + 
                  Power(t1,2)*
                   (-41 + 213*t2 - 321*Power(t2,2) + 112*Power(t2,3) + 
                     13*Power(t2,4)) + 
                  t1*(38 + 526*t2 - 1379*Power(t2,2) + 579*Power(t2,3) + 
                     271*Power(t2,4) - 149*Power(t2,5))))) + 
         s*Power(-1 + t2,3)*(4 - 12*t1 + 12*Power(t1,2) - 
            4*Power(t1,3) - 33*t2 + 8*s2*t2 + 84*t1*t2 - 16*s2*t1*t2 - 
            69*Power(t1,2)*t2 + 8*s2*Power(t1,2)*t2 + 
            18*Power(t1,3)*t2 + 77*Power(t2,2) - 42*s2*Power(t2,2) + 
            2*Power(s2,2)*Power(t2,2) - 161*t1*Power(t2,2) + 
            81*s2*t1*Power(t2,2) - 2*Power(s2,2)*t1*Power(t2,2) + 
            98*Power(t1,2)*Power(t2,2) - 39*s2*Power(t1,2)*Power(t2,2) - 
            14*Power(t1,3)*Power(t2,2) - 73*Power(t2,3) + 
            52*s2*Power(t2,3) - 6*Power(s2,2)*Power(t2,3) - 
            2*Power(s2,3)*Power(t2,3) + 125*t1*Power(t2,3) - 
            96*s2*t1*Power(t2,3) + 20*Power(s2,2)*t1*Power(t2,3) - 
            51*Power(t1,2)*Power(t2,3) + 28*s2*Power(t1,2)*Power(t2,3) + 
            3*Power(t1,3)*Power(t2,3) + 27*Power(t2,4) - 
            10*s2*Power(t2,4) - 8*Power(s2,2)*Power(t2,4) + 
            Power(s2,3)*Power(t2,4) - 43*t1*Power(t2,4) + 
            41*s2*t1*Power(t2,4) - 12*Power(s2,2)*t1*Power(t2,4) + 
            10*Power(t1,2)*Power(t2,4) - 6*s2*Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) - 8*s2*Power(t2,5) + 
            12*Power(s2,2)*Power(t2,5) - 2*Power(s2,3)*Power(t2,5) + 
            7*t1*Power(t2,5) - 10*s2*t1*Power(t2,5) + 
            3*Power(s2,2)*t1*Power(t2,5) + 
            Power(s1,6)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2) - 
            Power(s1,5)*(s2 - t1)*
             (s2*(t1*(8 - 6*t2) + 4*Power(-1 + t2,2)) - 
               Power(-1 + t2,2) - 3*t1*Power(-1 + t2,2) + 
               Power(s2,2)*(-4 + 3*t2) + Power(t1,2)*(-4 + 3*t2)) + 
            Power(s1,4)*(Power(s2,3)*(-2 - 17*t2 + 2*Power(t2,2)) + 
               Power(s2,2)*(-17 + 25*t2 - 14*Power(t2,2) + 
                  6*Power(t2,3) + t1*(12 + 47*t2 - 8*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(1 + 3*t2)) + 
                  Power(t1,2)*(-19 - 41*t2 + 9*Power(t2,2)) + 
                  t1*(42 - 63*t2 + 30*Power(t2,2) - 9*Power(t2,3))) + 
               t1*(Power(-1 + t2,2)*(1 + 3*t2) + 
                  Power(t1,2)*(9 + 11*t2 - 3*Power(t2,2)) + 
                  t1*(-25 + 38*t2 - 16*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(s1,3)*(-4*Power(-1 + t2,3) - 
               t1*Power(-1 + t2,2)*(30 - 10*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(-11 - 21*t2 - 7*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s2,3)*(2 + 5*t2 + 29*Power(t2,2) + 
                  2*Power(t2,3)) + 
               Power(t1,2)*(33 + 14*t2 - 59*Power(t2,2) + 
                  13*Power(t2,3) - Power(t2,4)) - 
               Power(s2,2)*(9 - 83*t2 + 74*Power(t2,2) - 
                  4*Power(t2,3) + 4*Power(t2,4) + 
                  t1*(5 + 48*t2 + 61*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(16 + 4*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (14 + 64*t2 + 39*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(-14 - 127*t2 + 163*Power(t2,2) - 27*Power(t2,3) + 
                     5*Power(t2,4)))) + 
            s1*(Power(-1 + t2,3)*(-16 - 2*t2 + Power(t2,2)) + 
               Power(s2,3)*Power(t2,2)*
                (6 - t2 + 11*Power(t2,2) + Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-33 - 14*t2 - 25*Power(t2,2) + 2*Power(t2,3)) - 
               Power(t1,3)*(1 + 26*t2 - 15*Power(t2,2) + 
                  5*Power(t2,3)) + 
               Power(t1,2)*(18 + 20*t2 - 38*Power(t2,2) + 
                  7*Power(t2,3) - 7*Power(t2,4)) - 
               Power(s2,2)*t2*
                (4 - 3*t2 - 65*Power(t2,2) + 60*Power(t2,3) + 
                  4*Power(t2,4) + 
                  t1*(-4 + 45*t2 + 9*Power(t2,3) + Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-8 + 31*t2 + 45*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(t1,2)*
                   (-8 + 41*t2 + 21*Power(t2,2) - 9*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  t1*(-16 + 88*t2 - 52*Power(t2,2) + 31*Power(t2,3) - 
                     52*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(3*Power(-1 + t2,3)*(1 + t2) + 
               t1*Power(-1 + t2,2)*
                (25 + 51*t2 - 13*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(12 + 15*t2 + 9*Power(t2,2) + Power(t2,3)) - 
               Power(s2,3)*t2*
                (6 + 3*t2 + 25*Power(t2,2) + 3*Power(t2,3)) - 
               2*Power(t1,2)*
                (17 + 5*t2 - 9*Power(t2,2) - 14*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(2 + 12*t2 - 123*Power(t2,2) + 
                  101*Power(t2,3) + 7*Power(t2,4) + Power(t2,5) + 
                  t1*(-2 + 30*t2 + 48*Power(t2,2) + 32*Power(t2,3) + 
                     3*Power(t2,4))) - 
               s2*(Power(-1 + t2,2)*
                   (5 + 53*t2 + 5*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (2 + 63*t2 + 30*Power(t2,2) + 16*Power(t2,3)) + 
                  t1*(-7 - 58*t2 - 75*Power(t2,2) + 149*Power(t2,3) - 
                     10*Power(t2,4) + Power(t2,5))))) + 
         Power(s,2)*Power(-1 + t2,2)*
          (12 - 48*t1 + 60*Power(t1,2) - 24*Power(t1,3) - 119*t2 + 
            46*s2*t2 + 333*t1*t2 - 96*s2*t1*t2 - 300*Power(t1,2)*t2 + 
            50*s2*Power(t1,2)*t2 + 86*Power(t1,3)*t2 + 319*Power(t2,2) - 
            199*s2*Power(t2,2) + 14*Power(s2,2)*Power(t2,2) - 
            646*t1*Power(t2,2) + 352*s2*t1*Power(t2,2) - 
            12*Power(s2,2)*t1*Power(t2,2) + 
            445*Power(t1,2)*Power(t2,2) - 
            179*s2*Power(t1,2)*Power(t2,2) - 
            80*Power(t1,3)*Power(t2,2) - 375*Power(t2,3) + 
            205*s2*Power(t2,3) + 17*Power(s2,2)*Power(t2,3) - 
            12*Power(s2,3)*Power(t2,3) + 548*t1*Power(t2,3) - 
            422*s2*t1*Power(t2,3) + 68*Power(s2,2)*t1*Power(t2,3) - 
            250*Power(t1,2)*Power(t2,3) + 
            159*s2*Power(t1,2)*Power(t2,3) + 
            22*Power(t1,3)*Power(t2,3) + 224*Power(t2,4) + 
            4*s2*Power(t2,4) - 101*Power(s2,2)*Power(t2,4) + 
            19*Power(s2,3)*Power(t2,4) - 263*t1*Power(t2,4) + 
            217*s2*t1*Power(t2,4) - 63*Power(s2,2)*t1*Power(t2,4) + 
            44*Power(t1,2)*Power(t2,4) - 40*s2*Power(t1,2)*Power(t2,4) - 
            Power(t1,3)*Power(t2,4) - 80*Power(t2,5) - 
            60*s2*Power(t2,5) + 73*Power(s2,2)*Power(t2,5) - 
            10*Power(s2,3)*Power(t2,5) + 91*t1*Power(t2,5) - 
            53*s2*t1*Power(t2,5) + 16*Power(s2,2)*t1*Power(t2,5) + 
            s2*Power(t1,2)*Power(t2,5) + 24*Power(t2,6) + 
            8*s2*Power(t2,6) - 3*Power(s2,2)*Power(t2,6) - 
            17*t1*Power(t2,6) + 3*s2*t1*Power(t2,6) + 
            Power(t1,2)*Power(t2,6) - 6*Power(t2,7) - 5*s2*Power(t2,7) + 
            2*t1*Power(t2,7) - s2*t1*Power(t2,7) + Power(t2,8) + 
            s2*Power(t2,8) - Power(s1,6)*(s2 - t1)*
             (3*Power(s2,2) + 3*Power(t1,2) - 7*t1*(-1 + t2) + 
               2*Power(-1 + t2,2) + s2*(-7 - 6*t1 + 7*t2)) + 
            Power(s1,5)*(Power(t1,3)*(10 - 7*t2) - 2*Power(-1 + t2,3) + 
               Power(s2,3)*(-7 + 4*t2) - 
               t1*Power(-1 + t2,2)*(-16 + 7*t2) + 
               Power(t1,2)*(26 - 48*t2 + 22*Power(t2,2)) + 
               s2*(9*Power(t1,2)*(-3 + 2*t2) + 
                  Power(-1 + t2,2)*(-16 + 7*t2) + 
                  t1*(-46 + 84*t2 - 38*Power(t2,2))) + 
               Power(s2,2)*(-3*t1*(-8 + 5*t2) + 
                  4*(5 - 9*t2 + 4*Power(t2,2)))) + 
            Power(s1,4)*(Power(-1 + t2,3)*(-3 + 8*t2) + 
               3*Power(t1,3)*(1 - 6*t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(9 - 2*t2 + 2*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(36 - 55*t2 + 10*Power(t2,2)) - 
               Power(t1,2)*(20 + 43*t2 - 90*Power(t2,2) + 
                  27*Power(t2,3)) + 
               Power(s2,2)*(-30 + t2 + 32*Power(t2,2) - 
                  3*Power(t2,3) - t1*(19 + 6*t2 + 2*Power(t2,2))) + 
               s2*(Power(t1,2)*(7 + 26*t2 - 6*Power(t2,2)) - 
                  Power(-1 + t2,2)*(17 - 34*t2 + 8*Power(t2,2)) + 
                  4*t1*(13 + 9*t2 - 29*Power(t2,2) + 7*Power(t2,3)))) + 
            Power(s1,3)*(4*Power(s2,3)*(1 - 5*t2 + 10*Power(t2,2)) - 
               Power(-1 + t2,3)*(18 - 18*t2 + 13*Power(t2,2)) - 
               Power(t1,3)*(7 + 25*t2 - 11*Power(t2,2) + 
                  3*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (78 + 48*t2 - 61*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,2)*(71 - 10*t2 - 4*Power(t2,2) - 
                  74*Power(t2,3) + 17*Power(t2,4)) + 
               Power(s2,2)*(-17 + 186*t2 - 166*Power(t2,2) + 
                  14*Power(t2,3) - 17*Power(t2,4) + 
                  t1*(-13 + 24*t2 - 93*Power(t2,2) + 10*Power(t2,3))) + 
               s2*(Power(t1,2)*
                   (18 + 15*t2 + 48*Power(t2,2) - 9*Power(t2,3)) + 
                  Power(-1 + t2,2)*
                   (13 + 71*t2 - 12*Power(t2,2) + Power(t2,3)) + 
                  t1*(-3 - 335*t2 + 341*Power(t2,2) - 9*Power(t2,3) + 
                     6*Power(t2,4)))) + 
            Power(s1,2)*(Power(-1 + t2,3)*t2*
                (40 - 23*t2 + 11*Power(t2,2)) - 
               Power(s2,3)*t2*
                (20 - 32*t2 + 56*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,3)*(12 + 18*t2 + 24*Power(t2,2) - 
                  4*Power(t2,3) + Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (46 + 201*t2 - 15*Power(t2,2) - 26*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(t1,2)*(20 + 150*t2 - 140*Power(t2,2) + 
                  5*Power(t2,3) - 41*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*(6 + 91*t2 - 439*Power(t2,2) + 
                  346*Power(t2,3) - 18*Power(t2,4) + 14*Power(t2,5) + 
                  t1*(-4 + 54*t2 + 2*Power(t2,2) + 100*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (13 - 137*t2 - 74*Power(t2,2) - 17*Power(t2,3) + 
                     5*Power(t2,4)) + 
                  Power(t1,2)*
                   (21 - 143*t2 + 41*Power(t2,2) - 81*Power(t2,3) + 
                     9*Power(t2,4)) - 
                  3*t1*(20 - 64*t2 - 117*Power(t2,2) + 
                     185*Power(t2,3) - 29*Power(t2,4) + 5*Power(t2,5)))) \
+ s1*(Power(t1,3)*(3 - 54*t2 + 39*Power(t2,2) - 15*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (28 - 40*t2 + 35*Power(t2,2) + 4*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (39 - 17*t2 + 29*Power(t2,2) - 11*Power(t2,3) + 
                  5*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (84 + 60*t2 + 143*Power(t2,2) - 30*Power(t2,3) - 
                  2*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(42 + 4*t2 - 15*Power(t2,2) - 
                  16*Power(t2,3) - 4*Power(t2,4) - 12*Power(t2,5) + 
                  Power(t2,6)) - 
               Power(s2,2)*t2*
                (20 + 91*t2 - 384*Power(t2,2) + 274*Power(t2,3) - 
                  4*Power(t2,4) + 3*Power(t2,5) + 
                  t1*(-16 + 109*t2 - 56*Power(t2,2) + 41*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (-30 + 88*t2 + 56*Power(t2,2) - 73*Power(t2,3) + 
                     43*Power(t2,4) - 3*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (26 - 64*t2 - 189*Power(t2,2) - 19*Power(t2,3) - 
                     14*Power(t2,4) + 4*Power(t2,5)) + 
                  t1*(56 - 152*t2 + 53*Power(t2,2) - 185*Power(t2,3) + 
                     257*Power(t2,4) - 35*Power(t2,5) + 6*Power(t2,6))))) \
- Power(s,3)*(-1 + t2)*(-9 + 63*t1 - 111*Power(t1,2) + 57*Power(t1,3) + 
            153*t2 - 91*s2*t2 - 517*t1*t2 + 226*s2*t1*t2 + 
            536*Power(t1,2)*t2 - 131*s2*Power(t1,2)*t2 - 
            179*Power(t1,3)*t2 - 524*Power(t2,2) + 417*s2*Power(t2,2) - 
            52*Power(s2,2)*Power(t2,2) + 999*t1*Power(t2,2) - 
            673*s2*t1*Power(t2,2) + 30*Power(s2,2)*t1*Power(t2,2) - 
            820*Power(t1,2)*Power(t2,2) + 
            418*s2*Power(t1,2)*Power(t2,2) + 
            156*Power(t1,3)*Power(t2,2) + 833*Power(t2,3) - 
            443*s2*Power(t2,3) - 26*Power(s2,2)*Power(t2,3) + 
            26*Power(s2,3)*Power(t2,3) - 862*t1*Power(t2,3) + 
            697*s2*t1*Power(t2,3) - 132*Power(s2,2)*t1*Power(t2,3) + 
            486*Power(t1,2)*Power(t2,3) - 
            391*s2*Power(t1,2)*Power(t2,3) - 
            24*Power(t1,3)*Power(t2,3) - 768*Power(t2,4) - 
            7*s2*Power(t2,4) + 237*Power(s2,2)*Power(t2,4) - 
            45*Power(s2,3)*Power(t2,4) + 568*t1*Power(t2,4) - 
            303*s2*t1*Power(t2,4) + 135*Power(s2,2)*t1*Power(t2,4) - 
            97*Power(t1,2)*Power(t2,4) + 
            108*s2*Power(t1,2)*Power(t2,4) - 
            14*Power(t1,3)*Power(t2,4) + 449*Power(t2,5) + 
            150*s2*Power(t2,5) - 176*Power(s2,2)*Power(t2,5) + 
            20*Power(s2,3)*Power(t2,5) - 358*t1*Power(t2,5) + 
            59*s2*t1*Power(t2,5) - 36*Power(s2,2)*t1*Power(t2,5) + 
            15*Power(t1,2)*Power(t2,5) + 3*Power(t1,3)*Power(t2,5) - 
            171*Power(t2,6) - 54*s2*Power(t2,6) + 
            17*Power(s2,2)*Power(t2,6) + 126*t1*Power(t2,6) - 
            12*s2*t1*Power(t2,6) - 10*Power(t1,2)*Power(t2,6) - 
            s2*Power(t1,2)*Power(t2,6) + 48*Power(t2,7) + 
            36*s2*Power(t2,7) - 19*t1*Power(t2,7) + 
            6*s2*t1*Power(t2,7) + Power(t1,2)*Power(t2,7) - 
            12*Power(t2,8) - 8*s2*Power(t2,8) + Power(t2,9) + 
            Power(s1,6)*(s2 - t1)*
             (3*Power(s2,2) + 3*Power(t1,2) - 3*t1*(-1 + t2) - 
               2*Power(-1 + t2,2) + s2*(-3 - 6*t1 + 3*t2)) - 
            Power(s1,5)*(Power(t1,3)*(22 - 27*t2) + 
               2*Power(-1 + t2,4) - t1*Power(-1 + t2,2)*(-22 + 5*t2) + 
               Power(s2,3)*(-13 + 18*t2) + 
               6*Power(t1,2)*(6 - 13*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(22 + t1*(48 - 63*t2) - 50*t2 + 
                  28*Power(t2,2)) + 
               s2*(Power(-1 + t2,2)*(-20 + 3*t2) + 
                  Power(t1,2)*(-57 + 72*t2) - 
                  2*t1*(29 - 64*t2 + 35*Power(t2,2)))) + 
            Power(s1,4)*(Power(-1 + t2,4)*(-11 + 8*t2) + 
               Power(t1,3)*(-50 + 124*t2 - 57*Power(t2,2)) + 
               Power(s2,3)*(14 - 52*t2 + 21*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*(111 - 146*t2 + 29*Power(t2,2)) + 
               Power(t1,2)*(-122 + 349*t2 - 326*Power(t2,2) + 
                  99*Power(t2,3)) + 
               Power(s2,2)*(-45 + 127*t2 - 113*Power(t2,2) + 
                  31*Power(t2,3) + t1*(-67 + 206*t2 - 88*Power(t2,2))) \
+ s2*(Power(-1 + t2,2)*(63 - 86*t2 + 17*Power(t2,2)) + 
                  Power(t1,2)*(103 - 278*t2 + 124*Power(t2,2)) + 
                  t1*(155 - 440*t2 + 403*Power(t2,2) - 118*Power(t2,3)))\
) + Power(s1,3)*(Power(s2,3)*
                (10 - 29*t2 + 42*Power(t2,2) - 7*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (-44 + 68*t2 - 53*Power(t2,2) + 13*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (45 + 306*t2 - 242*Power(t2,2) + 42*Power(t2,3)) + 
               Power(t1,3)*(-45 + 179*t2 - 201*Power(t2,2) + 
                  51*Power(t2,3)) + 
               Power(t1,2)*(-182 + 576*t2 - 783*Power(t2,2) + 
                  482*Power(t2,3) - 93*Power(t2,4)) + 
               2*Power(s2,2)*
                (-21 + 32*t2 - 29*Power(t2,2) + 4*Power(t2,3) + 
                  14*Power(t2,4) + 
                  t1*(-13 + 62*t2 - 88*Power(t2,2) + 15*Power(t2,3))) + 
               s2*(Power(t1,2)*
                   (52 - 247*t2 + 308*Power(t2,2) - 65*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-49 + 266*t2 - 72*Power(t2,2) + 6*Power(t2,3)) + 
                  t1*(110 - 259*t2 + 382*Power(t2,2) - 
                     259*Power(t2,3) + 26*Power(t2,4)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (-8 + 9*t2 + 16*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,3)*(-31 + 124*t2 - 233*Power(t2,2) + 
                  136*Power(t2,3) - 21*Power(t2,4)) + 
               Power(-1 + t2,3)*
                (17 - 176*t2 + 112*Power(t2,2) - 63*Power(t2,3) + 
                  11*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (25 + 278*t2 + 128*Power(t2,2) - 120*Power(t2,3) + 
                  27*Power(t2,4)) + 
               Power(t1,2)*(-68 + 649*t2 - 1072*Power(t2,2) + 
                  778*Power(t2,3) - 329*Power(t2,4) + 42*Power(t2,5)) - 
               Power(s2,2)*(6 + 5*t2 - 305*Power(t2,2) + 
                  356*Power(t2,3) - 114*Power(t2,4) + 52*Power(t2,5) + 
                  t1*(4 - 56*t2 + 128*Power(t2,2) - 2*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (-29 + 43*t2 + 91*Power(t2,2) - 21*Power(t2,3) - 
                     9*Power(t2,4)) - 
                  Power(-1 + t2,2)*
                   (38 - 36*t2 - 315*Power(t2,2) - 50*Power(t2,3) + 
                     25*Power(t2,4)) + 
                  t1*(166 - 685*t2 + 291*Power(t2,2) + 
                     332*Power(t2,3) - 161*Power(t2,4) + 57*Power(t2,5))\
)) - s1*(Power(s2,3)*Power(t2,2)*
                (28 - 51*t2 + 39*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,3)*(14 - 103*t2 + 157*Power(t2,2) - 
                  125*Power(t2,3) + 37*Power(t2,4) - 3*Power(t2,5)) + 
               Power(-1 + t2,3)*
                (-26 + 56*t2 - 197*Power(t2,2) + 66*Power(t2,3) - 
                  36*Power(t2,4) + 5*Power(t2,5)) - 
               t1*Power(-1 + t2,2)*
                (61 + 207*t2 + 259*Power(t2,2) - 120*Power(t2,3) + 
                  10*Power(t2,4) + 7*Power(t2,5)) + 
               Power(t1,2)*(14 - 8*t2 + 285*Power(t2,2) - 
                  482*Power(t2,3) + 285*Power(t2,4) - 104*Power(t2,5) + 
                  10*Power(t2,6)) - 
               Power(s2,2)*t2*
                (40 + 125*t2 - 611*Power(t2,2) + 501*Power(t2,3) - 
                  73*Power(t2,4) + 18*Power(t2,5) + 
                  t1*(-8 + 50*t2 - 14*Power(t2,2) + 36*Power(t2,3) + 
                     5*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (-41 + 93*t2 + 48*Power(t2,2) - 105*Power(t2,3) + 
                     88*Power(t2,4) - 14*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (29 - 40*t2 - 275*Power(t2,2) - 89*Power(t2,3) - 
                     76*Power(t2,4) + 27*Power(t2,5)) + 
                  t1*(74 + 70*t2 - 670*Power(t2,2) + 362*Power(t2,3) + 
                     263*Power(t2,4) - 134*Power(t2,5) + 35*Power(t2,6)))\
)) + Power(s,4)*(-(Power(s1,6)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2)) - 
            (-1 + t2)*(-6 + (-49 + 67*s2)*t2 + 
               (277 - 326*s2 + 103*Power(s2,2))*Power(t2,2) + 
               (-609 + 140*s2 + 35*Power(s2,2) - 26*Power(s2,3))*
                Power(t2,3) + 
               2*(334 + 72*s2 - 87*Power(s2,2) + 10*Power(s2,3))*
                Power(t2,4) + 
               (-374 - 83*s2 + 37*Power(s2,2))*Power(t2,5) + 
               (139 + 85*s2)*Power(t2,6) - 27*(2 + s2)*Power(t2,7) + 
               8*Power(t2,8) - 
               Power(t1,3)*(61 - 83*t2 - 58*Power(t2,2) + 
                  90*Power(t2,3) - 24*Power(t2,4) + Power(t2,5)) + 
               Power(t1,2)*(72 + 2*(-145 + 92*s2)*t2 + 
                  (281 - 371*s2)*Power(t2,2) + 
                  (-65 + 149*s2)*Power(t2,3) + 
                  3*(9 + 4*s2)*Power(t2,4) - (29 + 6*s2)*Power(t2,5) + 
                  5*Power(t2,6)) + 
               t1*(-9 + (270 - 253*s2)*t2 - 
                  4*(50 - 71*s2 + 10*Power(s2,2))*Power(t2,2) + 
                  (130 + 88*s2 + 109*Power(s2,2))*Power(t2,3) - 
                  (447 + 140*s2 + 44*Power(s2,2))*Power(t2,4) + 
                  (325 + 3*s2)*Power(t2,5) + (-69 + 16*s2)*Power(t2,6))) \
+ Power(s1,5)*(Power(t1,3)*(19 - 21*t2) + 2*Power(-1 + t2,3)*(2 + t2) + 
               t1*Power(-1 + t2,2)*(12 + t2) + 
               2*Power(s2,3)*(-5 + 6*t2) + 
               6*Power(t1,2)*(4 - 7*t2 + 3*Power(t2,2)) + 
               Power(s2,2)*(10 + 39*t1 - 14*t2 - 45*t1*t2 + 
                  4*Power(t2,2)) + 
               s2*(-(Power(-1 + t2,2)*(8 + 5*t2)) + 
                  6*Power(t1,2)*(-8 + 9*t2) + 
                  t1*(-34 + 56*t2 - 22*Power(t2,2)))) + 
            Power(s1,4)*(Power(s2,3)*(-19 + 56*t2 - 35*Power(t2,2)) - 
               Power(-1 + t2,3)*(-3 - 9*t2 + 20*Power(t2,2)) + 
               t1*Power(-1 + t2,2)*(45 - 126*t2 + 35*Power(t2,2)) + 
               Power(t1,3)*(78 - 173*t2 + 93*Power(t2,2)) + 
               Power(t1,2)*(123 - 350*t2 + 328*Power(t2,2) - 
                  101*Power(t2,3)) + 
               Power(s2,2)*(14 - 40*t2 + 35*Power(t2,2) - 
                  9*Power(t2,3) + t1*(99 - 251*t2 + 146*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(-3 - 56*t2 + 13*Power(t2,2))) - 
                  2*Power(t1,2)*(79 - 184*t2 + 102*Power(t2,2)) + 
                  t1*(-115 + 324*t2 - 297*Power(t2,2) + 88*Power(t2,3)))) \
+ Power(s1,3)*(Power(t1,3)*(133 - 453*t2 + 469*Power(t2,2) - 
                  139*Power(t2,3)) + 
               Power(s2,3)*(-15 + 88*t2 - 113*Power(t2,2) + 
                  30*Power(t2,3)) + 
               Power(-1 + t2,3)*
                (-24 + 60*t2 - 99*Power(t2,2) + 53*Power(t2,3)) - 
               t1*Power(-1 + t2,2)*
                (19 + 290*t2 - 327*Power(t2,2) + 94*Power(t2,3)) + 
               Power(t1,2)*(247 - 1038*t2 + 1436*Power(t2,2) - 
                  804*Power(t2,3) + 159*Power(t2,4)) + 
               Power(s2,2)*(39 - 162*t2 + 101*Power(t2,2) + 
                  70*Power(t2,3) - 48*Power(t2,4) + 
                  t1*(77 - 390*t2 + 475*Power(t2,2) - 132*Power(t2,3))) \
+ s2*(Power(-1 + t2,2)*(-25 + 165*t2 - 91*Power(t2,2) + 
                     27*Power(t2,3)) + 
                  Power(t1,2)*
                   (-179 + 707*t2 - 783*Power(t2,2) + 225*Power(t2,3)) + 
                  t1*(-137 + 660*t2 - 811*Power(t2,2) + 
                     306*Power(t2,3) - 18*Power(t2,4)))) + 
            Power(s1,2)*(Power(s2,3)*t2*
                (50 - 117*t2 + 80*Power(t2,2) - 14*Power(t2,3)) - 
               Power(-1 + t2,3)*
                (35 - 251*t2 + 206*Power(t2,2) - 185*Power(t2,3) + 
                  62*Power(t2,4)) + 
               t1*Power(-1 + t2,2)*
                (-63 + 306*t2 + 142*Power(t2,2) - 203*Power(t2,3) + 
                  83*Power(t2,4)) + 
               Power(t1,3)*(121 - 528*t2 + 807*Power(t2,2) - 
                  486*Power(t2,3) + 87*Power(t2,4)) + 
               Power(t1,2)*(135 - 1123*t2 + 2337*Power(t2,2) - 
                  2022*Power(t2,3) + 775*Power(t2,4) - 102*Power(t2,5)) \
+ Power(s2,2)*(2 - 148*t2 + 158*Power(t2,2) + 146*Power(t2,3) - 
                  259*Power(t2,4) + 101*Power(t2,5) + 
                  t1*(23 - 246*t2 + 495*Power(t2,2) - 308*Power(t2,3) + 
                     39*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (-50 + 387*t2 - 738*Power(t2,2) + 455*Power(t2,3) - 
                     57*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (31 + 109*t2 - 375*Power(t2,2) - 63*Power(t2,3) + 
                     33*Power(t2,4)) + 
                  t1*(-151 + 934*t2 - 1270*Power(t2,2) + 
                     363*Power(t2,3) + 253*Power(t2,4) - 129*Power(t2,5))\
)) + s1*(Power(s2,3)*Power(t2,2)*
                (-13 + 14*t2 - Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,3)*(60 - 331*t2 + 639*Power(t2,2) - 
                  555*Power(t2,3) + 200*Power(t2,4) - 21*Power(t2,5)) - 
               t1*Power(-1 + t2,2)*
                (-40 + 292*t2 + 331*Power(t2,2) - 338*Power(t2,3) + 
                  67*Power(t2,4) + 25*Power(t2,5)) + 
               Power(-1 + t2,3)*
                (11 + 63*t2 - 406*Power(t2,2) + 179*Power(t2,3) - 
                  134*Power(t2,4) + 35*Power(t2,5)) + 
               Power(t1,2)*(-108 + 125*t2 + 708*Power(t2,2) - 
                  1432*Power(t2,3) + 980*Power(t2,4) - 305*Power(t2,5) + 
                  32*Power(t2,6)) + 
               Power(s2,2)*t2*
                (-47 + 39*t2 + 331*Power(t2,2) - 470*Power(t2,3) + 
                  194*Power(t2,4) - 47*Power(t2,5) + 
                  t1*(-43 + 190*t2 - 231*Power(t2,2) + 71*Power(t2,3) - 
                     11*Power(t2,4))) + 
               s2*(-2*Power(t1,2)*
                   (16 - 62*t2 + 93*Power(t2,2) - 51*Power(t2,3) - 
                     20*Power(t2,4) + 12*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (11 + 48*t2 - 169*Power(t2,2) - 128*Power(t2,3) - 
                     168*Power(t2,4) + 69*Power(t2,5)) + 
                  t1*(53 + 458*t2 - 1878*Power(t2,2) + 1749*Power(t2,3) - 
                     203*Power(t2,4) - 273*Power(t2,5) + 94*Power(t2,6))))\
))*T3q(1 - s + s1 - t2,s1))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(-1 + s + t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) - 
    (8*((-1 + s1)*Power(s1 - t2,4)*(-1 + t2)*
          Power(-1 + s1*(s2 - t1) + t1 + t2 - s2*t2,3) + 
         Power(s,10)*(Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
         Power(s,9)*(Power(t1,3)*(-6 - 9*s1 + 6*t2) + 
            t1*t2*(3 + s1*(-1 + 3*s2 - 10*t2) + 5*t2 + 5*Power(t2,2) - 
               s2*(3 + t2)) + 
            Power(t1,2)*(3 + (-2 + s2)*t2 + s1*(-3 + 3*s2 + t2)) + 
            Power(t2,2)*(4 + 3*t2 - 13*Power(t2,2) + s2*(-3 + 2*t2) + 
               2*s1*(-5 + 2*s2 + 7*t2))) + 
         Power(s,8)*(Power(t1,3)*
             (12 + 36*Power(s1,2) + s1*(43 - 48*t2) - 34*t2 + 
               15*Power(t2,2)) - 
            Power(t1,2)*(15 + (-23 + s2)*t2 + (8 - 6*s2)*Power(t2,2) + 
               Power(t2,3) + Power(s1,2)*(-22 + 24*s2 + 8*t2) + 
               s1*(11 - 11*s2*(-1 + t2) + 15*t2 - 6*Power(t2,2))) + 
            t2*(3 - 13*t2 + Power(s2,2)*t2 + 27*Power(t2,2) + 
               25*Power(t2,3) - 36*Power(t2,4) + 
               Power(s1,2)*(-11 + 3*Power(s2,2) + s2*(2 - 25*t2) + 
                  57*t2 - 42*Power(t2,2)) + 
               s2*(-3 + 14*t2 - 24*Power(t2,2) + 13*Power(t2,3)) + 
               s1*(2 - 9*t2 - 75*Power(t2,2) + 78*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + 
                  3*s2*(1 + 7*t2 + 3*Power(t2,2)))) + 
            t1*(3 + (-19 + 14*s2)*t2 - 3*(1 + 8*s2)*Power(t2,2) + 
               (35 - 6*s2)*Power(t2,3) + 10*Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) - 2*t2 + 42*Power(t2,2) - 
                  s2*(3 + 19*t2)) + 
               s1*(-3 + 12*t2 - Power(s2,2)*t2 - 40*Power(t2,2) - 
                  49*Power(t2,3) + s2*(3 + 7*t2 + 29*Power(t2,2))))) + 
         Power(s,7)*(1 - 14*t2 + 10*s2*t2 + 21*Power(t2,2) - 
            18*s2*Power(t2,2) - 11*Power(s2,2)*Power(t2,2) - 
            90*Power(t2,3) + 104*s2*Power(t2,3) + 
            3*Power(s2,2)*Power(t2,3) + 58*Power(t2,4) - 
            89*s2*Power(t2,4) + 76*Power(t2,5) + 36*s2*Power(t2,5) - 
            55*Power(t2,6) + Power(t1,3)*
             (-6 + 60*t2 - 79*Power(t2,2) + 20*Power(t2,3)) - 
            Power(t1,2)*(-21 + (73 + 11*s2)*t2 + 
               (-58 + 6*s2)*Power(t2,2) + (6 - 15*s2)*Power(t2,3) + 
               5*Power(t2,4)) + 
            t1*(-12 + (42 - 9*s2)*t2 + 
               (-93 + 105*s2 + 2*Power(s2,2))*Power(t2,2) - 
               (73 + 77*s2)*Power(t2,3) + (103 - 16*s2)*Power(t2,4) + 
               10*Power(t2,5)) + 
            Power(s1,3)*(-4 + Power(s2,3) - 84*Power(t1,3) + 53*t2 - 
               135*Power(t2,2) + 70*Power(t2,3) + 
               14*Power(t1,2)*(-5 + 2*t2) - 
               Power(s2,2)*(21*t1 + 17*t2) + 
               t1*(-10 + 33*t2 - 98*Power(t2,2)) + 
               s2*(1 + 84*Power(t1,2) - 14*t2 + 66*Power(t2,2) + 
                  t1*(20 + 49*t2))) + 
            Power(s1,2)*(1 + 
               (-13 + 6*s2 + 7*Power(s2,2) - 2*Power(s2,3))*t2 + 
               (-62 - 56*s2 + 43*Power(s2,2))*Power(t2,2) + 
               (318 - 80*s2)*Power(t2,3) - 195*Power(t2,4) + 
               7*Power(t1,3)*(-19 + 24*t2) + 
               Power(t1,2)*(-1 + s2*(69 - 105*t2) + 141*t2 - 
                  42*Power(t2,2)) + 
               t1*(31 - 98*t2 + 77*Power(t2,2) + 189*Power(t2,3) + 
                  Power(s2,2)*(-7 + 24*t2) - 
                  2*s2*(8 + 3*t2 + 70*Power(t2,2)))) + 
            s1*(Power(t1,3)*(-67 + 212*t2 - 105*Power(t2,2)) + 
               Power(t1,2)*(78 - 30*t2 - 68*Power(t2,2) + 
                  21*Power(t2,3) + s2*(9 - 56*t2 + 9*Power(t2,2))) + 
               t2*(-7 + 82*t2 + Power(s2,3)*t2 - 49*Power(t2,2) - 
                  262*Power(t2,3) + 180*Power(t2,4) + 
                  Power(s2,2)*(7 - 6*t2 - 26*Power(t2,2)) + 
                  s2*(13 - 96*t2 + 161*Power(t2,2) - 22*Power(t2,3))) + 
               t1*(-13 + 25*t2 + Power(s2,2)*(11 - 7*t2)*t2 + 
                  184*Power(t2,2) - 202*Power(t2,3) - 101*Power(t2,4) + 
                  s2*(-7 - 90*t2 + 61*Power(t2,2) + 104*Power(t2,3))))) + 
         Power(s,6)*(-3 + 13*t2 + s2*t2 - 20*Power(t2,2) - 
            10*s2*Power(t2,2) + 22*Power(s2,2)*Power(t2,2) + 
            201*Power(t2,3) - 167*s2*Power(t2,3) - 
            48*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) - 
            222*Power(t2,4) + 317*s2*Power(t2,4) + 
            5*Power(s2,2)*Power(t2,4) + 55*Power(t2,5) - 
            180*s2*Power(t2,5) + 115*Power(t2,6) + 55*s2*Power(t2,6) - 
            50*Power(t2,7) + Power(t1,3)*
             (-9 - 17*t2 + 119*Power(t2,2) - 96*Power(t2,3) + 
               15*Power(t2,4)) + 
            Power(t1,2)*(3 + (49 + 29*s2)*t2 - 
               3*(39 + 20*s2)*Power(t2,2) + (42 - 11*s2)*Power(t2,3) + 
               (17 + 20*s2)*Power(t2,4) - 10*Power(t2,5)) + 
            t1*(9 - 2*(13 + 17*s2)*t2 - 
               2*(-76 + 26*s2 + 3*Power(s2,2))*Power(t2,2) + 
               (-233 + 348*s2 + 4*Power(s2,2))*Power(t2,3) - 
               2*(103 + 67*s2)*Power(t2,4) + 
               (164 - 25*s2)*Power(t2,5) + 5*Power(t2,6)) + 
            Power(s1,4)*(-6*Power(s2,3) + 126*Power(t1,3) - 
               14*Power(t1,2)*(-9 + 4*t2) + 
               Power(s2,2)*(1 + 63*t1 + 39*t2) + 
               5*t1*(7 - 20*t2 + 28*Power(t2,2)) + 
               2*(8 - 51*t2 + 85*Power(t2,2) - 35*Power(t2,3)) - 
               s2*(7 + 168*Power(t1,2) - 40*t2 + 95*Power(t2,2) + 
                  t1*(57 + 63*t2))) + 
            Power(s1,3)*(-13 + Power(t1,3)*(231 - 336*t2) + 19*t2 + 
               255*Power(t2,2) - 584*Power(t2,3) + 260*Power(t2,4) + 
               2*Power(s2,3)*(-1 + 8*t2) + 
               Power(s2,2)*(-3 + t1*(38 - 123*t2) + 3*t2 - 
                  133*Power(t2,2)) + 
               Power(t1,2)*(68 - 419*t2 + 126*Power(t2,2)) + 
               t1*(-92 + 182*t2 + 41*Power(t2,2) - 385*Power(t2,3)) + 
               s2*(14 - 60*t2 + 60*Power(t2,2) + 195*Power(t2,3) + 
                  Power(t1,2)*(-183 + 343*t2) + 
                  t1*(27 + 50*t2 + 287*Power(t2,2)))) - 
            Power(s1,2)*(-9 - 19*t2 + 210*Power(t2,2) + 
               158*Power(t2,3) - 785*Power(t2,4) + 360*Power(t2,5) + 
               2*Power(s2,3)*t2*(-4 + 7*t2) + 
               Power(t1,3)*(-156 + 559*t2 - 315*Power(t2,2)) + 
               Power(t1,2)*(130 + 240*t2 - 497*Power(t2,2) + 
                  105*Power(t2,3)) + 
               t1*(2 - 155*t2 + 667*Power(t2,2) - 333*Power(t2,3) - 
                  355*Power(t2,4)) + 
               Power(s2,2)*(t2*(43 + 25*t2 - 149*Power(t2,2)) + 
                  t1*(-4 + 96*t2 - 79*Power(t2,2))) + 
               s2*(1 + 48*t2 - 387*Power(t2,2) + 432*Power(t2,3) + 
                  50*Power(t2,4) + 
                  Power(t1,2)*(43 - 321*t2 + 180*Power(t2,2)) + 
                  t1*(-17 - 275*t2 + 37*Power(t2,2) + 394*Power(t2,3)))) \
+ s1*(-8 + 44*t2 - 151*Power(t2,2) + 
               4*Power(s2,3)*(-2 + t2)*Power(t2,2) + 397*Power(t2,3) - 
               43*Power(t2,4) - 486*Power(t2,5) + 220*Power(t2,6) + 
               Power(t1,3)*(27 - 276*t2 + 418*Power(t2,2) - 
                  120*Power(t2,3)) + 
               Power(t1,2)*(-97 + 245*t2 + 122*Power(t2,2) - 
                  212*Power(t2,3) + 45*Power(t2,4)) + 
               t1*(59 - 180*t2 + 128*Power(t2,2) + 647*Power(t2,3) - 
                  445*Power(t2,4) - 115*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-4 + 92*t2 + 16*Power(t2,2) - 55*Power(t2,3) + 
                  t1*(-16 + 56*t2 - 19*Power(t2,2))) + 
               s2*(1 - 56*t2 + 179*Power(t2,2) - 638*Power(t2,3) + 
                  512*Power(t2,4) - 105*Power(t2,5) + 
                  Power(t1,2)*
                   (1 + 77*t2 - 111*Power(t2,2) - 15*Power(t2,3)) + 
                  t1*(2 + 128*t2 - 644*Power(t2,2) + 179*Power(t2,3) + 
                     195*Power(t2,4))))) + 
         s*Power(s1 - t2,3)*(Power(s1,6)*Power(s2 - t1,2)*
             (-1 + s2 - t1 + t2) - 
            Power(s1,5)*(s2 - t1)*
             (s2*(t1*(2 - 6*t2) + 4*Power(-1 + t2,2)) - 
               Power(-1 + t2,2) - 3*t1*Power(-1 + t2,2) + 
               Power(s2,2)*(-1 + 3*t2) + Power(t1,2)*(-1 + 3*t2)) + 
            (-1 + t2)*(-7 + (35 - 17*s2)*t2 + 
               (-51 + 43*s2 - 11*Power(s2,2))*Power(t2,2) - 
               (-25 + 18*s2 - 4*Power(s2,2) + Power(s2,3))*
                Power(t2,3) - 
               2*(1 + 4*s2 - 6*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
               Power(t1,3)*(7 - 11*t2 + 3*Power(t2,2)) + 
               Power(t1,2)*(-21 + (57 - 17*s2)*t2 + 
                  (-41 + 22*s2)*Power(t2,2) + (10 - 6*s2)*Power(t2,3)) \
+ t1*(21 + (-81 + 34*s2)*t2 + 
                  (89 - 65*s2 + 11*Power(s2,2))*Power(t2,2) + 
                  (-36 + 31*s2 - 9*Power(s2,2))*Power(t2,3) + 
                  (7 - 10*s2 + 3*Power(s2,2))*Power(t2,4))) + 
            Power(s1,4)*(2*Power(s2,3)*(2 - 4*t2 + Power(t2,2)) + 
               s2*(-(Power(-1 + t2,2)*(1 + 3*t2)) + 
                  Power(t1,2)*(17 - 32*t2 + 9*Power(t2,2)) + 
                  t1*(24 - 45*t2 + 30*Power(t2,2) - 9*Power(t2,3))) + 
               t1*(Power(-1 + t2,2)*(1 + 3*t2) + 
                  Power(t1,2)*(-6 + 11*t2 - 3*Power(t2,2)) + 
                  t1*(-16 + 29*t2 - 16*Power(t2,2) + 3*Power(t2,3))) + 
               Power(s2,2)*(t1*(-15 + 29*t2 - 8*Power(t2,2)) + 
                  2*(-4 + 8*t2 - 7*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(s1,3)*(-4*Power(-1 + t2,3) - 
               t1*Power(-1 + t2,2)*(21 - 10*t2 + 3*Power(t2,2)) + 
               Power(t1,3)*(19 - 21*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*(-1 - 13*t2 + 20*Power(t2,2) + 
                  2*Power(t2,3)) - 
               Power(t1,2)*(3 - 50*t2 + 59*Power(t2,2) - 
                  13*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(-27 + 83*t2 - 56*Power(t2,2) + 
                  4*Power(t2,3) - 4*Power(t2,4) + 
                  t1*(22 + 6*t2 - 52*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(7 + 4*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (-40 + 28*t2 + 39*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(40 - 163*t2 + 145*Power(t2,2) - 27*Power(t2,3) + 
                     5*Power(t2,4)))) + 
            s1*(Power(-1 + t2,3)*(-10 - 2*t2 + Power(t2,2)) + 
               Power(t1,3)*(14 - 26*t2 + 15*Power(t2,2) - 
                  5*Power(t2,3)) + 
               Power(s2,3)*Power(t2,2)*
                (-3 - 7*t2 + 11*Power(t2,2) + Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-6 - 14*t2 - 25*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(-18 + 56*t2 - 38*Power(t2,2) + 
                  7*Power(t2,3) - 7*Power(t2,4)) - 
               Power(s2,2)*t2*
                (22 - 3*t2 - 83*Power(t2,2) + 60*Power(t2,3) + 
                  4*Power(t2,4) + 
                  t1*(-22 + 18*t2 + 9*Power(t2,3) + Power(t2,4))) + 
               s2*(Power(-1 + t2,2)*
                   (-17 + 13*t2 + 45*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(t1,2)*
                   (-17 + 5*t2 + 21*Power(t2,2) - 9*Power(t2,3) + 
                     6*Power(t2,4)) - 
                  t1*(-34 + 52*t2 + 2*Power(t2,2) + 31*Power(t2,3) - 
                     52*Power(t2,4) + Power(t2,5)))) + 
            Power(s1,2)*(3*Power(-1 + t2,3)*t2 + 
               Power(s2,3)*t2*
                (3 + 15*t2 - 22*Power(t2,2) - 3*Power(t2,3)) + 
               t1*Power(-1 + t2,2)*
                (-2 + 51*t2 - 13*Power(t2,2) + Power(t2,3)) + 
               Power(t1,3)*(-18 + 15*t2 + 9*Power(t2,2) + Power(t2,3)) - 
               2*Power(t1,2)*
                (-10 + 32*t2 - 9*Power(t2,2) - 14*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s2,2)*(11 + 39*t2 - 150*Power(t2,2) + 
                  92*Power(t2,3) + 7*Power(t2,4) + Power(t2,5) + 
                  t1*(-11 - 24*t2 + 21*Power(t2,2) + 32*Power(t2,3) + 
                     3*Power(t2,4))) - 
               s2*(Power(-1 + t2,2)*
                   (-13 + 44*t2 + 5*Power(t2,2) + Power(t2,3)) + 
                  Power(t1,2)*
                   (-34 + 9*t2 + 30*Power(t2,2) + 16*Power(t2,3)) + 
                  t1*(47 - 58*t2 - 129*Power(t2,2) + 149*Power(t2,3) - 
                     10*Power(t2,4) + Power(t2,5))))) + 
         Power(s,5)*(Power(t1,3)*
             (12 - 53*t2 - 3*Power(t2,2) + 110*Power(t2,3) - 
               64*Power(t2,4) + 6*Power(t2,5)) - 
            Power(t1,2)*(24 + (-96 + 26*s2)*t2 + 
               (61 - 141*s2)*Power(t2,2) + (22 + 131*s2)*Power(t2,3) + 
               (38 + 4*s2)*Power(t2,4) - (38 + 15*s2)*Power(t2,5) + 
               10*Power(t2,6)) + 
            t1*(12 + 12*(-5 + 4*s2)*t2 + 
               (106 - 199*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (535 - 319*s2 + 2*Power(s2,2))*Power(t2,3) + 
               (-432 + 651*s2 - 7*Power(s2,2))*Power(t2,4) - 
               (254 + 141*s2)*Power(t2,5) + (151 - 25*s2)*Power(t2,6) + 
               Power(t2,7)) + 
            t2*(17 - 47*t2 - 362*Power(t2,2) + 530*Power(t2,3) - 
               265*Power(t2,4) + 22*Power(t2,5) + 95*Power(t2,6) - 
               27*Power(t2,7) + 2*Power(s2,3)*Power(t2,2)*(-3 + 5*t2) + 
               Power(s2,2)*t2*
                (-8 + 85*t2 - 114*Power(t2,2) + 10*Power(t2,3)) + 
               s2*(-22 + 72*t2 + 132*Power(t2,2) - 469*Power(t2,3) + 
                  471*Power(t2,4) - 205*Power(t2,5) + 50*Power(t2,6))) + 
            Power(s1,5)*(-24 + 15*Power(s2,3) - 126*Power(t1,3) + 
               70*Power(t1,2)*(-2 + t2) + 98*t2 - 120*Power(t2,2) + 
               42*Power(t2,3) - 5*Power(s2,2)*(1 + 21*t1 + 9*t2) + 
               t1*(-60 + 145*t2 - 126*Power(t2,2)) + 
               s2*(18 + 210*Power(t1,2) - 60*t2 + 80*Power(t2,2) + 
                  5*t1*(18 + 7*t2))) - 
            Power(s1,4)*(-32 - 5*t2 + 368*Power(t2,2) - 
               551*Power(t2,3) + 195*Power(t2,4) - 
               35*Power(t1,3)*(-7 + 12*t2) + Power(s2,3)*(-9 + 50*t2) + 
               Power(s2,2)*(-8 + t1*(85 - 290*t2) + 11*t2 - 
                  195*Power(t2,2)) + 
               15*Power(t1,2)*(10 - 43*t2 + 14*Power(t2,2)) + 
               t1*(-119 + 91*t2 + 310*Power(t2,2) - 455*Power(t2,3)) + 
               s2*(48 - 110*t2 - 6*Power(t2,2) + 230*Power(t2,3) + 
                  5*Power(t1,2)*(-53 + 119*t2) + 
                  t1*(1 + 195*t2 + 280*Power(t2,2)))) + 
            Power(s1,3)*(-25 - 94*t2 + 268*Power(t2,2) + 
               511*Power(t2,3) - 1046*Power(t2,4) + 360*Power(t2,5) + 
               Power(t1,3)*(-196 + 806*t2 - 525*Power(t2,2)) + 
               Power(s2,3)*(2 - 43*t2 + 60*Power(t2,2)) + 
               Power(t1,2)*(26 + 838*t2 - 1185*Power(t2,2) + 
                  245*Power(t2,3)) + 
               t1*(40 - 383*t2 + 833*Power(t2,2) - 6*Power(t2,3) - 
                  610*Power(t2,4)) + 
               Power(s2,2)*(-2*t1*(9 - 150*t2 + 145*Power(t2,2)) + 
                  3*(-4 + 35*t2 + 35*Power(t2,2) - 105*Power(t2,3))) + 
               s2*(8 + 186*t2 - 815*Power(t2,2) + 576*Power(t2,3) + 
                  160*Power(t2,4) + 
                  Power(t1,2)*(87 - 733*t2 + 555*Power(t2,2)) + 
                  t1*(30 - 549*t2 + 96*Power(t2,2) + 620*Power(t2,3)))) \
- Power(s1,2)*(-15 + 98*t2 - 368*Power(t2,2) + 689*Power(t2,3) + 
               316*Power(t2,4) - 1014*Power(t2,5) + 330*Power(t2,6) + 
               Power(s2,3)*t2*(10 - 69*t2 + 30*Power(t2,2)) + 
               Power(t1,3)*(45 - 501*t2 + 909*Power(t2,2) - 
                  300*Power(t2,3)) + 
               Power(t1,2)*(-137 + 61*t2 + 1230*Power(t2,2) - 
                  988*Power(t2,3) + 150*Power(t2,4)) - 
               t1*(-112 + 229*t2 + 165*Power(t2,2) - 
                  1393*Power(t2,3) + 541*Power(t2,4) + 360*Power(t2,5)) \
+ Power(s2,2)*(t2*(-65 + 348*t2 + 147*Power(t2,2) - 225*Power(t2,3)) + 
                  t1*(2 - 82*t2 + 352*Power(t2,2) - 130*Power(t2,3))) + 
               s2*(13 - 159*t2 + 564*Power(t2,2) - 1704*Power(t2,3) + 
                  1135*Power(t2,4) - 100*Power(t2,5) + 
                  Power(t1,2)*
                   (12 + 181*t2 - 588*Power(t2,2) + 150*Power(t2,3)) + 
                  t1*(-39 + 546*t2 - 1698*Power(t2,2) + 
                     113*Power(t2,3) + 565*Power(t2,4)))) + 
            s1*(12 - 40*t2 + 465*Power(t2,2) - 832*Power(t2,3) + 
               700*Power(t2,4) + 53*Power(t2,5) - 494*Power(t2,6) + 
               150*Power(t2,7) + 
               Power(s2,3)*Power(t2,2)*(14 - 45*t2 + 5*Power(t2,2)) + 
               Power(t1,3)*(27 + 60*t2 - 429*Power(t2,2) + 
                  416*Power(t2,3) - 75*Power(t2,4)) + 
               Power(t1,2)*(-15 - 172*t2 + 65*Power(t2,2) + 
                  576*Power(t2,3) - 351*Power(t2,4) + 55*Power(t2,5)) + 
               t1*(-24 + 162*t2 - 778*Power(t2,2) + 527*Power(t2,3) + 
                  975*Power(t2,4) - 521*Power(t2,5) - 80*Power(t2,6)) + 
               Power(s2,2)*t2*
                (8 - 138*t2 + 349*Power(t2,2) + 48*Power(t2,3) - 
                  60*Power(t2,4) - 
                  t1*(4 + 66*t2 - 144*Power(t2,2) + 25*Power(t2,3))) + 
               s2*(2 - 9*t2 - 299*Power(t2,2) + 885*Power(t2,3) - 
                  1488*Power(t2,4) + 818*Power(t2,5) - 160*Power(t2,6) + 
                  Power(t1,2)*
                   (6 - 79*t2 + 225*Power(t2,2) - 126*Power(t2,3) - 
                     35*Power(t2,4)) + 
                  t1*(-8 + 60*t2 + 835*Power(t2,2) - 1779*Power(t2,3) + 
                     263*Power(t2,4) + 215*Power(t2,5))))) - 
         Power(s,4)*(-4 + 12*t1 - 12*Power(t1,2) + 4*Power(t1,3) + 
            30*t2 - 8*s2*t2 - 111*t1*t2 + 16*s2*t1*t2 + 
            132*Power(t1,2)*t2 - 8*s2*Power(t1,2)*t2 - 
            51*Power(t1,3)*t2 - 137*Power(t2,2) + 99*s2*Power(t2,2) - 
            2*Power(s2,2)*Power(t2,2) + 377*t1*Power(t2,2) - 
            207*s2*t1*Power(t2,2) + 2*Power(s2,2)*t1*Power(t2,2) - 
            359*Power(t1,2)*Power(t2,2) + 
            108*s2*Power(t1,2)*Power(t2,2) + 
            119*Power(t1,3)*Power(t2,2) - 317*Power(t2,3) + 
            66*s2*Power(t2,3) + 13*Power(s2,2)*Power(t2,3) - 
            2*Power(s2,3)*Power(t2,3) + 212*t1*Power(t2,3) + 
            83*s2*t1*Power(t2,3) + 5*Power(s2,2)*t1*Power(t2,3) + 
            206*Power(t1,2)*Power(t2,3) - 
            203*s2*Power(t1,2)*Power(t2,3) - 
            60*Power(t1,3)*Power(t2,3) + 888*Power(t2,4) - 
            433*s2*Power(t2,4) - 158*Power(s2,2)*Power(t2,4) + 
            22*Power(s2,3)*Power(t2,4) - 1127*t1*Power(t2,4) + 
            770*s2*t1*Power(t2,4) - 54*Power(s2,2)*t1*Power(t2,4) - 
            55*Power(t1,2)*Power(t2,4) + 
            137*s2*Power(t1,2)*Power(t2,4) - 
            36*Power(t1,3)*Power(t2,4) - 561*Power(t2,5) + 
            496*s2*Power(t2,5) + 176*Power(s2,2)*Power(t2,5) - 
            20*Power(s2,3)*Power(t2,5) + 532*t1*Power(t2,5) - 
            715*s2*t1*Power(t2,5) + 28*Power(s2,2)*t1*Power(t2,5) + 
            68*Power(t1,2)*Power(t2,5) - 9*s2*Power(t1,2)*Power(t2,5) + 
            22*Power(t1,3)*Power(t2,5) + 150*Power(t2,6) - 
            341*s2*Power(t2,6) - 15*Power(s2,2)*Power(t2,6) + 
            148*t1*Power(t2,6) + 92*s2*t1*Power(t2,6) - 
            30*Power(t1,2)*Power(t2,6) - 6*s2*Power(t1,2)*Power(t2,6) - 
            Power(t1,3)*Power(t2,6) + 5*Power(t2,7) + 
            128*s2*Power(t2,7) - 79*t1*Power(t2,7) + 
            16*s2*t1*Power(t2,7) + 5*Power(t1,2)*Power(t2,7) - 
            43*Power(t2,8) - 27*s2*Power(t2,8) + 8*Power(t2,9) + 
            Power(s1,6)*(-16 + 20*Power(s2,3) - 84*Power(t1,3) + 
               47*t2 - 45*Power(t2,2) + 14*Power(t2,3) + 
               14*Power(t1,2)*(-7 + 4*t2) - 
               5*Power(s2,2)*(2 + 21*t1 + 5*t2) + 
               t1*(-55 + 114*t2 - 70*Power(t2,2)) + 
               s2*(22 + 168*Power(t1,2) + t1*(85 - 7*t2) - 50*t2 + 
                  39*Power(t2,2))) + 
            Power(s1,5)*(28 + Power(s2,3)*(16 - 80*t2) + 29*t2 - 
               245*Power(t2,2) + 267*Power(t2,3) - 78*Power(t2,4) + 
               7*Power(t1,3)*(-23 + 48*t2) + 
               Power(t1,2)*(-157 + 573*t2 - 210*Power(t2,2)) + 
               t1*(69 + 84*t2 - 410*Power(t2,2) + 315*Power(t2,3)) + 
               Power(s2,2)*(2 + 21*t2 + 135*Power(t2,2) + 
                  25*t1*(-4 + 15*t2)) + 
               s2*(-59 + Power(t1,2)*(225 - 609*t2) + 73*t2 + 
                  71*Power(t2,2) - 143*Power(t2,3) + 
                  t1*(51 - 323*t2 - 91*Power(t2,2)))) + 
            Power(s1,4)*(-9 - 173*t2 + 172*Power(t2,2) + 
               512*Power(t2,3) - 673*Power(t2,4) + 180*Power(t2,5) - 
               5*Power(t1,3)*(29 - 137*t2 + 105*Power(t2,2)) + 
               Power(s2,3)*(7 - 89*t2 + 120*Power(t2,2)) + 
               Power(t1,2)*(-163 + 1157*t2 - 1410*Power(t2,2) + 
                  315*Power(t2,3)) + 
               t1*(16 - 256*t2 + 281*Power(t2,2) + 491*Power(t2,3) - 
                  560*Power(t2,4)) + 
               Power(s2,2)*(-44 + 155*t2 + 79*Power(t2,2) - 
                  290*Power(t2,3) + t1*(-36 + 463*t2 - 510*Power(t2,2))\
) + s2*(7 + 330*t2 - 850*Power(t2,2) + 380*Power(t2,3) + 
                  155*Power(t2,4) + 
                  Power(t1,2)*(101 - 880*t2 + 810*Power(t2,2)) + 
                  t1*(146 - 753*t2 + 394*Power(t2,2) + 410*Power(t2,3)))\
) - Power(s1,3)*(-33 + 81*t2 - 490*Power(t2,2) + 591*Power(t2,3) + 
               583*Power(t2,4) - 910*Power(t2,5) + 220*Power(t2,6) + 
               Power(t1,3)*(26 - 444*t2 + 1040*Power(t2,2) - 
                  400*Power(t2,3)) + 
               Power(s2,3)*(-2 + 43*t2 - 191*Power(t2,2) + 
                  80*Power(t2,3)) + 
               Power(t1,2)*(-37 - 631*t2 + 2524*Power(t2,2) - 
                  1712*Power(t2,3) + 250*Power(t2,4)) + 
               t1*(85 - 239*t2 - 100*Power(t2,2) + 944*Power(t2,3) + 
                  32*Power(t2,4) - 490*Power(t2,5)) + 
               2*Power(s2,2)*
                (8 - 145*t2 + 338*Power(t2,2) + 124*Power(t2,3) - 
                  155*Power(t2,4) + 
                  t1*(1 - 81*t2 + 397*Power(t2,2) - 165*Power(t2,3))) + 
               s2*(38 - 107*t2 + 868*Power(t2,2) - 2243*Power(t2,3) + 
                  1194*Power(t2,4) - 30*Power(t2,5) + 
                  Power(t1,2)*
                   (43 + 196*t2 - 1137*Power(t2,2) + 450*Power(t2,3)) + 
                  t1*(-135 + 1147*t2 - 2566*Power(t2,2) + 
                     196*Power(t2,3) + 630*Power(t2,4)))) + 
            Power(s1,2)*(18 - 408*t2 + 936*Power(t2,2) - 
               1105*Power(t2,3) + 760*Power(t2,4) + 380*Power(t2,5) - 
               687*Power(t2,6) + 150*Power(t2,7) + 
               Power(s2,3)*t2*
                (-6 + 87*t2 - 199*Power(t2,2) + 20*Power(t2,3)) + 
               Power(t1,3)*(27 + 66*t2 - 549*Power(t2,2) + 
                  710*Power(t2,3) - 150*Power(t2,4)) + 
               Power(t1,2)*(-20 - 41*t2 - 849*Power(t2,2) + 
                  2264*Power(t2,3) - 1046*Power(t2,4) + 120*Power(t2,5)\
) + t1*(-25 + 506*t2 - 1431*Power(t2,2) + 785*Power(t2,3) + 
                  1172*Power(t2,4) - 418*Power(t2,5) - 210*Power(t2,6)) \
+ Power(s2,2)*(-2 + 45*t2 - 606*Power(t2,2) + 1055*Power(t2,3) + 
                  210*Power(t2,4) - 165*Power(t2,5) + 
                  t1*(2 + 9*t2 - 270*Power(t2,2) + 627*Power(t2,3) - 
                     105*Power(t2,4))) + 
               s2*(2 + 235*t2 - 621*Power(t2,2) + 1435*Power(t2,3) - 
                  2679*Power(t2,4) + 1342*Power(t2,5) - 
                  175*Power(t2,6) + 
                  Power(t1,2)*
                   (11 - 24*t2 + 273*Power(t2,2) - 586*Power(t2,3) + 
                     60*Power(t2,4)) + 
                  t1*(-13 - 373*t2 + 2532*Power(t2,2) - 
                     3706*Power(t2,3) + 169*Power(t2,4) + 
                     445*Power(t2,5)))) + 
            s1*(-13 + 91*t2 + 700*Power(t2,2) - 1726*Power(t2,3) + 
               1320*Power(t2,4) - 504*Power(t2,5) - 116*Power(t2,6) + 
               271*Power(t2,7) - 54*Power(t2,8) + 
               Power(s2,3)*Power(t2,2)*(6 - 73*t2 + 101*Power(t2,2)) + 
               Power(t1,3)*(34 - 118*t2 + 12*Power(t2,2) + 
                  278*Power(t2,3) - 215*Power(t2,4) + 24*Power(t2,5)) + 
               Power(t1,2)*(-81 + 295*t2 - 178*Power(t2,2) + 
                  460*Power(t2,3) - 811*Power(t2,4) + 299*Power(t2,5) - 
                  36*Power(t2,6)) + 
               t1*(60 - 268*t2 - 657*Power(t2,2) + 2279*Power(t2,3) - 
                  1227*Power(t2,4) - 686*Power(t2,5) + 
                  334*Power(t2,6) + 35*Power(t2,7)) + 
               Power(s2,2)*t2*
                (4 - 42*t2 + 518*Power(t2,2) - 712*Power(t2,3) - 
                  37*Power(t2,4) + 35*Power(t2,5) + 
                  t1*(-4 - 12*t2 + 198*Power(t2,2) - 224*Power(t2,3) + 
                     15*Power(t2,4))) + 
               s2*(8 - 101*t2 - 263*Power(t2,2) + 940*Power(t2,3) - 
                  1334*Power(t2,4) + 1532*Power(t2,5) - 
                  677*Power(t2,6) + 121*Power(t2,7) + 
                  Power(t1,2)*
                   (8 - 119*t2 + 270*Power(t2,2) - 315*Power(t2,3) + 
                     113*Power(t2,4) + 27*Power(t2,5)) + 
                  t1*(-16 + 220*t2 + 155*Power(t2,2) - 
                     2301*Power(t2,3) + 2557*Power(t2,4) - 
                     221*Power(t2,5) - 143*Power(t2,6))))) + 
         Power(s,3)*(s1 - t2)*
          (-13 + 39*t1 - 39*Power(t1,2) + 13*Power(t1,3) + 94*t2 - 
            27*s2*t2 - 270*t1*t2 + 54*s2*t1*t2 + 258*Power(t1,2)*t2 - 
            27*s2*Power(t1,2)*t2 - 82*Power(t1,3)*t2 + 103*Power(t2,2) - 
            10*s2*Power(t2,2) + 5*Power(s2,2)*Power(t2,2) + 
            64*t1*Power(t2,2) - 65*s2*t1*Power(t2,2) - 
            5*Power(s2,2)*t1*Power(t2,2) - 302*Power(t1,2)*Power(t2,2) + 
            75*s2*Power(t1,2)*Power(t2,2) + 
            135*Power(t1,3)*Power(t2,2) - 710*Power(t2,3) + 
            317*s2*Power(t2,3) + 5*Power(s2,2)*Power(t2,3) - 
            5*Power(s2,3)*Power(t2,3) + 867*t1*Power(t2,3) - 
            353*s2*t1*Power(t2,3) + 42*Power(s2,2)*t1*Power(t2,3) + 
            17*Power(t1,2)*Power(t2,3) - 71*s2*Power(t1,2)*Power(t2,3) - 
            88*Power(t1,3)*Power(t2,3) + 747*Power(t2,4) - 
            372*s2*Power(t2,4) - 163*Power(s2,2)*Power(t2,4) + 
            29*Power(s2,3)*Power(t2,4) - 1078*t1*Power(t2,4) + 
            787*s2*t1*Power(t2,4) - 90*Power(s2,2)*t1*Power(t2,4) + 
            27*Power(t1,2)*Power(t2,4) + 60*s2*Power(t1,2)*Power(t2,4) + 
            10*Power(t1,3)*Power(t2,4) - 245*Power(t2,5) + 
            173*s2*Power(t2,5) + 159*Power(s2,2)*Power(t2,5) - 
            20*Power(s2,3)*Power(t2,5) + 356*t1*Power(t2,5) - 
            438*s2*t1*Power(t2,5) + 32*Power(s2,2)*t1*Power(t2,5) + 
            27*Power(t1,2)*Power(t2,5) - 10*s2*Power(t1,2)*Power(t2,5) + 
            3*Power(t1,3)*Power(t2,5) + 21*Power(t2,6) - 
            101*s2*Power(t2,6) - 11*Power(s2,2)*Power(t2,6) + 
            32*t1*Power(t2,6) + 35*s2*t1*Power(t2,6) - 
            10*Power(t1,2)*Power(t2,6) - s2*Power(t1,2)*Power(t2,6) + 
            12*Power(t2,7) + 39*s2*Power(t2,7) - 21*t1*Power(t2,7) + 
            6*s2*t1*Power(t2,7) + Power(t1,2)*Power(t2,7) - 
            10*Power(t2,8) - 8*s2*Power(t2,8) + Power(t2,9) + 
            Power(s1,6)*(-4 + 15*Power(s2,3) - 36*Power(t1,3) + 9*t2 - 
               7*Power(t2,2) + 2*Power(t2,3) + 
               14*Power(t1,2)*(-3 + 2*t2) - 
               Power(s2,2)*(10 + 63*t1 + 3*t2) + 
               t1*(-26 + 47*t2 - 22*Power(t2,2)) + 
               s2*(13 + 84*Power(t1,2) + t1*(48 - 21*t2) - 22*t2 + 
                  10*Power(t2,2))) + 
            Power(s1,5)*(7 + Power(s2,3)*(14 - 55*t2) + 16*t2 - 
               61*Power(t2,2) + 49*Power(t2,3) - 11*Power(t2,4) + 
               3*Power(t1,3)*(-21 + 44*t2) + 
               Power(t1,2)*(-89 + 253*t2 - 98*Power(t2,2)) + 
               t1*(11 + 86*t2 - 188*Power(t2,2) + 97*Power(t2,3)) + 
               Power(s2,2)*(-12 + 47*t2 + 22*Power(t2,2) + 
                  t1*(-65 + 213*t2)) + 
               s2*(-29 + Power(t1,2)*(111 - 287*t2) + 19*t2 + 
                  38*Power(t2,2) - 34*Power(t2,3) + 
                  t1*(66 - 224*t2 + 35*Power(t2,2)))) + 
            Power(s1,4)*(21 - 116*t2 + 64*Power(t2,2) + 
               145*Power(t2,3) - 139*Power(t2,4) + 25*Power(t2,5) + 
               Power(t1,3)*(-67 + 281*t2 - 183*Power(t2,2)) + 
               2*Power(s2,3)*(5 - 38*t2 + 35*Power(t2,2)) + 
               Power(t1,2)*(-200 + 721*t2 - 649*Power(t2,2) + 
                  133*Power(t2,3)) + 
               t1*(-32 + 21*t2 - 96*Power(t2,2) + 286*Power(t2,3) - 
                  168*Power(t2,4)) - 
               Power(s2,2)*(60 - 148*t2 + 27*Power(t2,2) + 
                  58*Power(t2,3) + t1*(43 - 322*t2 + 262*Power(t2,2))) \
+ s2*(-7 + 214*t2 - 377*Power(t2,2) + 131*Power(t2,3) + 
                  28*Power(t2,4) + 
                  Power(t1,2)*(79 - 483*t2 + 352*Power(t2,2)) + 
                  t1*(193 - 593*t2 + 341*Power(t2,2) + 51*Power(t2,3)))) \
- Power(s1,3)*(8 + 33*t2 - 255*Power(t2,2) + 188*Power(t2,3) + 
               199*Power(t2,4) - 203*Power(t2,5) + 30*Power(t2,6) + 
               Power(s2,3)*(-5 + 59*t2 - 164*Power(t2,2) + 
                  30*Power(t2,3)) - 
               Power(t1,3)*(16 + 119*t2 - 384*Power(t2,2) + 
                  117*Power(t2,3)) + 
               Power(t1,2)*(51 - 716*t2 + 1564*Power(t2,2) - 
                  769*Power(t2,3) + 92*Power(t2,4)) + 
               t1*(43 - 326*t2 + 365*Power(t2,2) - 9*Power(t2,3) + 
                  151*Power(t2,4) - 142*Power(t2,5)) + 
               Power(s2,2)*(59 - 436*t2 + 583*Power(t2,2) + 
                  90*Power(t2,3) - 72*Power(t2,4) - 
                  2*t1*(6 + 63*t2 - 278*Power(t2,2) + 69*Power(t2,3))) \
+ s2*(-18 + 95*t2 + 427*Power(t2,2) - 998*Power(t2,3) + 
                  440*Power(t2,4) - 28*Power(t2,5) + 
                  Power(t1,2)*
                   (76 + 34*t2 - 615*Power(t2,2) + 173*Power(t2,3)) + 
                  t1*(-165 + 1141*t2 - 1785*Power(t2,2) + 
                     204*Power(t2,3) + 159*Power(t2,4)))) + 
            Power(s1,2)*(112 - 393*t2 + 488*Power(t2,2) - 
               372*Power(t2,3) + 131*Power(t2,4) + 174*Power(t2,5) - 
               160*Power(t2,6) + 20*Power(t2,7) - 
               Power(s2,3)*t2*
                (15 - 117*t2 + 176*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(3 + 14*t2 - 134*Power(t2,2) + 
                  216*Power(t2,3) - 33*Power(t2,4)) + 
               Power(t1,2)*(-29 + 152*t2 - 853*Power(t2,2) + 
                  1339*Power(t2,3) - 433*Power(t2,4) + 38*Power(t2,5)) \
+ t1*(-86 + 485*t2 - 1217*Power(t2,2) + 889*Power(t2,3) + 
                  148*Power(t2,4) - 60*Power(t2,5) - 58*Power(t2,6)) + 
               Power(s2,2)*(5 + 123*t2 - 855*Power(t2,2) + 
                  929*Power(t2,3) + 112*Power(t2,4) - 43*Power(t2,5) + 
                  t1*(-5 + 18*t2 - 213*Power(t2,2) + 438*Power(t2,3) - 
                     27*Power(t2,4))) + 
               s2*(-73 + 236*t2 - 8*Power(t2,2) + 429*Power(t2,3) - 
                  1107*Power(t2,4) + 484*Power(t2,5) - 62*Power(t2,6) + 
                  Power(t1,2)*
                   (12 + 36*t2 + 44*Power(t2,2) - 307*Power(t2,3) + 
                     17*Power(t2,4)) + 
                  t1*(61 - 593*t2 + 2184*Power(t2,2) - 
                     2349*Power(t2,3) + 76*Power(t2,4) + 136*Power(t2,5)\
))) + s1*(-38 - 267*t2 + 1095*Power(t2,2) - 1194*Power(t2,3) + 
               467*Power(t2,4) - 40*Power(t2,5) - 80*Power(t2,6) + 
               64*Power(t2,7) - 7*Power(t2,8) + 
               Power(s2,3)*Power(t2,2)*
                (15 - 97*t2 + 94*Power(t2,2) + 5*Power(t2,3)) + 
               Power(t1,3)*(26 - 86*t2 + 74*Power(t2,2) + 
                  43*Power(t2,3) - 49*Power(t2,4) + 3*Power(t2,5)) + 
               Power(t1,2)*(-90 + 175*t2 - 166*Power(t2,2) + 
                  397*Power(t2,3) - 446*Power(t2,4) + 112*Power(t2,5) - 
                  10*Power(t2,6)) + 
               t1*(102 + 178*t2 - 1261*Power(t2,2) + 1914*Power(t2,3) - 
                  900*Power(t2,4) - 153*Power(t2,5) + 87*Power(t2,6) + 
                  9*Power(t2,7)) + 
               Power(s2,2)*t2*
                (-10 - 69*t2 + 642*Power(t2,2) - 641*Power(t2,3) - 
                  21*Power(t2,4) + 10*Power(t2,5) + 
                  t1*(10 - 72*t2 + 220*Power(t2,2) - 171*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(27 + 83*t2 - 571*Power(t2,2) + 482*Power(t2,3) - 
                  360*Power(t2,4) + 555*Power(t2,5) - 230*Power(t2,6) + 
                  38*Power(t2,7) + 
                  Power(t1,2)*
                   (27 - 87*t2 + 111*Power(t2,2) - 149*Power(t2,3) + 
                     74*Power(t2,4) + 8*Power(t2,5)) + 
                  t1*(-54 + 4*t2 + 781*Power(t2,2) - 2023*Power(t2,3) + 
                     1529*Power(t2,4) - 72*Power(t2,5) - 48*Power(t2,6)))\
)) - Power(s,2)*Power(s1 - t2,2)*
          (-15 + 45*t1 - 45*Power(t1,2) + 15*Power(t1,3) - 22*t2 + 
            7*s2*t2 + 6*t1*t2 - 14*s2*t1*t2 + 54*Power(t1,2)*t2 + 
            7*s2*Power(t1,2)*t2 - 38*Power(t1,3)*t2 + 252*Power(t2,2) - 
            126*s2*Power(t2,2) + 15*Power(s2,2)*Power(t2,2) - 
            381*t1*Power(t2,2) + 169*s2*t1*Power(t2,2) - 
            15*Power(s2,2)*t1*Power(t2,2) + 83*Power(t1,2)*Power(t2,2) - 
            43*s2*Power(t1,2)*Power(t2,2) + 46*Power(t1,3)*Power(t2,2) - 
            418*Power(t2,3) + 232*s2*Power(t2,3) - 
            9*Power(s2,2)*Power(t2,3) - 3*Power(s2,3)*Power(t2,3) + 
            645*t1*Power(t2,3) - 361*s2*t1*Power(t2,3) + 
            48*Power(s2,2)*t1*Power(t2,3) - 151*Power(t1,2)*Power(t2,3) + 
            48*s2*Power(t1,2)*Power(t2,3) - 33*Power(t1,3)*Power(t2,3) + 
            241*Power(t2,4) - 98*s2*Power(t2,4) - 
            79*Power(s2,2)*Power(t2,4) + 15*Power(s2,3)*Power(t2,4) - 
            416*t1*Power(t2,4) + 334*s2*t1*Power(t2,4) - 
            56*Power(s2,2)*t1*Power(t2,4) + 55*Power(t1,2)*Power(t2,4) - 
            s2*Power(t1,2)*Power(t2,4) + 7*Power(t1,3)*Power(t2,4) - 
            33*Power(t2,5) - 14*s2*Power(t2,5) + 
            72*Power(s2,2)*Power(t2,5) - 10*Power(s2,3)*Power(t2,5) + 
            104*t1*Power(t2,5) - 127*s2*t1*Power(t2,5) + 
            16*Power(s2,2)*t1*Power(t2,5) + Power(t1,2)*Power(t2,5) - 
            3*s2*Power(t1,2)*Power(t2,5) - 9*Power(t2,6) - 
            4*s2*Power(t2,6) - 3*Power(s2,2)*Power(t2,6) - 
            t1*Power(t2,6) + 6*s2*t1*Power(t2,6) - 
            Power(t1,2)*Power(t2,6) + 5*Power(t2,7) + 4*s2*Power(t2,7) - 
            2*t1*Power(t2,7) + s2*t1*Power(t2,7) - Power(t2,8) - 
            s2*Power(t2,8) + Power(s1,6)*
             (6*Power(s2,3) + Power(s2,2)*(-5 - 21*t1 + 3*t2) + 
               s2*(3 + 24*Power(t1,2) + t1*(15 - 11*t2) - 4*t2 + 
                  Power(t2,2)) - 
               t1*(5 + 9*Power(t1,2) + t1*(10 - 8*t2) - 8*t2 + 
                  3*Power(t2,2))) - 
            Power(s1,5)*(Power(t1,3)*(13 - 30*t2) - 
               Power(-1 + t2,2)*(-1 + 2*t2) + Power(s2,3)*(-6 + 20*t2) + 
               Power(s2,2)*(13 + t1*(22 - 67*t2) - 33*t2 + 
                  11*Power(t2,2)) + 
               Power(t1,2)*(26 - 61*t2 + 26*Power(t2,2)) + 
               t1*(4 - 28*t2 + 37*Power(t2,2) - 13*Power(t2,3)) + 
               s2*(3 + 4*t2 - 10*Power(t2,2) + 3*Power(t2,3) + 
                  Power(t1,2)*(-29 + 77*t2) + 
                  t1*(-35 + 86*t2 - 33*Power(t2,2)))) + 
            Power(s1,4)*(Power(t1,3)*(-22 + 71*t2 - 36*Power(t2,2)) - 
               Power(-1 + t2,2)*(-14 + 3*t2 + 9*Power(t2,2)) + 
               Power(s2,3)*(8 - 34*t2 + 20*Power(t2,2)) + 
               Power(t1,2)*(-94 + 230*t2 - 161*Power(t2,2) + 
                  31*Power(t2,3)) + 
               t1*(-19 + 40*t2 - 66*Power(t2,2) + 67*Power(t2,3) - 
                  22*Power(t2,4)) + 
               Power(s2,2)*(-36 + 76*t2 - 48*Power(t2,2) + 
                  14*Power(t2,3) + t1*(-33 + 130*t2 - 72*Power(t2,2))) + 
               s2*(-8 + 51*t2 - 56*Power(t2,2) + 12*Power(t2,3) + 
                  Power(t2,4) + 
                  Power(t1,2)*(45 - 163*t2 + 86*Power(t2,2)) + 
                  t1*(111 - 252*t2 + 158*Power(t2,2) - 29*Power(t2,3)))) \
+ Power(s1,3)*(Power(s2,3)*(3 - 39*t2 + 76*Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-5 - 35*t2 + 5*Power(t2,2) + 16*Power(t2,3)) + 
               Power(t1,3)*(33 - 20*t2 - 79*Power(t2,2) + 
                  18*Power(t2,3)) + 
               Power(t1,2)*(-27 + 300*t2 - 477*Power(t2,2) + 
                  173*Power(t2,3) - 17*Power(t2,4)) + 
               t1*(-44 + 221*t2 - 252*Power(t2,2) + 107*Power(t2,3) - 
                  50*Power(t2,4) + 18*Power(t2,5)) + 
               Power(s2,2)*(-67 + 296*t2 - 270*Power(t2,2) - 
                  Power(t2,3) - 6*Power(t2,4) + 
                  t1*(28 + 46*t2 - 226*Power(t2,2) + 24*Power(t2,3))) + 
               s2*(Power(t1,2)*
                   (-75 + 41*t2 + 206*Power(t2,2) - 36*Power(t2,3)) + 
                  t1*(105 - 615*t2 + 727*Power(t2,2) - 
                     119*Power(t2,3) - 2*Power(t2,4)) + 
                  3*(17 - 35*t2 - 21*Power(t2,2) + 57*Power(t2,3) - 
                     20*Power(t2,4) + 2*Power(t2,5)))) - 
            Power(s1,2)*(Power(s2,3)*t2*
                (9 - 69*t2 + 84*Power(t2,2) + 10*Power(t2,3)) + 
               Power(t1,3)*(18 - 9*t2 - 12*Power(t2,2) - 
                  31*Power(t2,3) + 3*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-31 + 15*t2 - 6*Power(t2,2) - 6*Power(t2,3) + 
                  14*Power(t2,4)) + 
               Power(t1,2)*(10 - 18*t2 + 243*Power(t2,2) - 
                  356*Power(t2,3) + 78*Power(t2,4) - 5*Power(t2,5)) + 
               t1*(3 - 179*t2 + 538*Power(t2,2) - 428*Power(t2,3) + 
                  64*Power(t2,4) - 5*Power(t2,5) + 7*Power(t2,6)) + 
               Power(s2,2)*(-15 - 125*t2 + 563*Power(t2,2) - 
                  436*Power(t2,3) - 36*Power(t2,4) + Power(t2,5) + 
                  t1*(15 + 8*t2 + 49*Power(t2,2) - 166*Power(t2,3) - 
                     5*Power(t2,4))) + 
               s2*(51 + 57*t2 - 313*Power(t2,2) + 87*Power(t2,3) + 
                  177*Power(t2,4) - 68*Power(t2,5) + 9*Power(t2,6) + 
                  Power(t1,2)*
                   (-32 - 11*t2 + 41*Power(t2,2) + 100*Power(t2,3) - 
                     2*Power(t2,4)) - 
                  t1*(19 - 197*t2 + 877*Power(t2,2) - 849*Power(t2,3) + 
                     39*Power(t2,4) + 15*Power(t2,5)))) + 
            s1*(Power(s2,3)*Power(t2,2)*
                (9 - 53*t2 + 46*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(14 - 52*t2 + 57*Power(t2,2) - 
                  16*Power(t2,3) - 4*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (46 - 167*t2 + 54*Power(t2,2) + 17*Power(t2,3) - 
                  10*Power(t2,4) + 6*Power(t2,5)) - 
               Power(t1,2)*(-18 + t2 + 38*Power(t2,2) - 99*Power(t2,3) + 
                  102*Power(t2,4) - 16*Power(t2,5) + Power(t2,6)) + 
               t1*(-78 + 312*t2 - 582*Power(t2,2) + 635*Power(t2,3) - 
                  298*Power(t2,4) + Power(t2,5) + 9*Power(t2,6) + 
                  Power(t2,7)) + 
               Power(s2,2)*t2*
                (-30 - 49*t2 + 382*Power(t2,2) - 301*Power(t2,3) - 
                  12*Power(t2,4) + Power(t2,5) + 
                  t1*(30 - 68*t2 + 92*Power(t2,2) - 64*Power(t2,3) - 
                     3*Power(t2,4))) + 
               s2*(-7 + 177*t2 - 226*Power(t2,2) - 102*Power(t2,3) + 
                  116*Power(t2,4) + 67*Power(t2,5) - 30*Power(t2,6) + 
                  5*Power(t2,7) + 
                  Power(t1,2)*
                   (-7 + 11*t2 + 16*Power(t2,2) - 44*Power(t2,3) + 
                     31*Power(t2,4) + Power(t2,5)) + 
                  t1*(14 - 188*t2 + 453*Power(t2,2) - 707*Power(t2,3) + 
                     466*Power(t2,4) - 13*Power(t2,5) - 7*Power(t2,6))))))*
       T4q(-s + s1 - t2))/
     (Power(s,2)*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*Power(-s + s1 - t2,2)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)) + 
    (8*(Power(s,7)*(Power(t1,3) + t1*Power(t2,2) - 2*Power(t2,3)) + 
         Power(s,6)*(Power(t1,3)*(-6 - 6*s1 + 3*t2) + 
            t1*t2*(3 + s1*(-1 + 3*s2 - 7*t2) + 5*t2 + 2*Power(t2,2) - 
               s2*(3 + t2)) + 
            Power(t1,2)*(3 + (-2 + s2)*t2 + s1*(-3 + 3*s2 + t2)) + 
            Power(t2,2)*(4 + 7*t2 - 7*Power(t2,2) + s2*(-3 + 2*t2) + 
               2*s1*(-5 + 2*s2 + 4*t2))) + 
         Power(s,5)*(Power(t1,3)*
             (12 + 15*Power(s1,2) - 16*t2 + 3*Power(t2,2) - 
               5*s1*(-5 + 3*t2)) - 
            Power(t1,2)*(15 + (-14 + s2)*t2 + (2 - 3*s2)*Power(t2,2) + 
               Power(t2,3) + Power(s1,2)*(-13 + 15*s2 + 5*t2) + 
               s1*(2 + s2*(11 - 5*t2) + 12*t2 - 3*Power(t2,2))) + 
            t1*(3 + (-19 + 14*s2)*t2 - 3*(4 + 5*s2)*Power(t2,2) - 
               3*(-10 + s2)*Power(t2,3) + Power(t2,4) + 
               Power(s1,2)*(1 + 3*Power(s2,2) - 5*t2 + 18*Power(t2,2) - 
                  s2*(3 + 10*t2)) - 
               s1*(3 - 21*t2 + Power(s2,2)*t2 + 22*Power(t2,2) + 
                  16*Power(t2,3) + s2*(-3 + 2*t2 - 17*Power(t2,2)))) + 
            t2*(3 - 13*t2 + Power(s2,2)*t2 - 9*Power(t2,2) + 
               22*Power(t2,3) - 9*Power(t2,4) + 
               Power(s1,2)*(-11 + 3*Power(s2,2) + s2*(2 - 13*t2) + 
                  27*t2 - 12*Power(t2,2)) + 
               s2*(-3 + 14*t2 - 15*Power(t2,2) + 7*Power(t2,3)) + 
               s1*(2 + 15*t2 - 48*Power(t2,2) + 21*Power(t2,3) - 
                  Power(s2,2)*(3 + 5*t2) + 3*s2*(1 + 4*t2 + Power(t2,2))))\
) + Power(s,4)*(1 - 14*t2 + 10*s2*t2 + 12*Power(t2,2) - 
            9*s2*Power(t2,2) - 11*Power(s2,2)*Power(t2,2) + 
            27*Power(t2,3) + 62*s2*Power(t2,3) - 49*Power(t2,4) - 
            27*s2*Power(t2,4) + 29*Power(t2,5) + 9*s2*Power(t2,5) - 
            5*Power(t2,6) + Power(t1,3)*
             (-6 + 24*t2 - 13*Power(t2,2) + Power(t2,3)) + 
            Power(t1,2)*(21 - (28 + 11*s2)*t2 + (7 - 3*s2)*Power(t2,2) + 
               (16 + 3*s2)*Power(t2,3) - 2*Power(t2,4)) - 
            t1*(12 + (-33 + 9*s2)*t2 + 
               (36 - 63*s2 - 2*Power(s2,2))*Power(t2,2) + 
               (100 + 33*s2)*Power(t2,3) + 4*(-12 + s2)*Power(t2,4)) + 
            Power(s1,3)*(Power(s2,3) - 20*Power(t1,3) - 
               4*Power(s2,2)*(3*t1 + 2*t2) + 
               2*Power(t1,2)*(-11 + 5*t2) + 
               t1*(-7 + 21*t2 - 22*Power(t2,2)) + 
               4*(-1 + 5*t2 - 6*Power(t2,2) + 2*Power(t2,3)) + 
               s2*(1 + 30*Power(t1,2) - 8*t2 + 15*Power(t2,2) + 
                  t1*(11 + 10*t2))) + 
            Power(s1,2)*(1 + (5 + 15*s2 - 2*Power(s2,2) - 
                  2*Power(s2,3))*t2 + 
               (-56 - 17*s2 + 19*Power(s2,2))*Power(t2,2) + 
               (72 - 14*s2)*Power(t2,3) - 21*Power(t2,4) + 
               10*Power(t1,3)*(-4 + 3*t2) - 
               2*Power(t1,2)*
                (8 - 27*t2 + 6*Power(t2,2) + 3*s2*(-6 + 5*t2)) + 
               t1*(22 - 47*t2 + 5*Power(t2,2) + 36*Power(t2,3) + 
                  Power(s2,2)*(-7 + 12*t2) + 
                  s2*(-7 + 6*t2 - 38*Power(t2,2)))) + 
            s1*(Power(t1,3)*(-31 + 53*t2 - 12*Power(t2,2)) + 
               Power(t1,2)*(33 + s2*(9 - 26*t2) + 36*t2 - 
                  41*Power(t2,2) + 6*Power(t2,3)) - 
               t1*(4 + 23*t2 - 133*Power(t2,2) + 77*Power(t2,3) + 
                  14*Power(t2,4) + Power(s2,2)*t2*(-11 + 4*t2) + 
                  s2*(7 + 57*t2 - 4*Power(t2,2) - 29*Power(t2,3))) + 
               t2*(2 - 23*t2 + Power(s2,3)*t2 + 89*Power(t2,2) - 
                  74*Power(t2,3) + 18*Power(t2,4) + 
                  Power(s2,2)*(7 + 6*t2 - 11*Power(t2,2)) + 
                  s2*(4 - 63*t2 + 52*Power(t2,2) - 10*Power(t2,3))))) - 
         (s1 - t2)*(-1 + t2)*(-1 + Power(s1,4)*Power(s2 - t1,3) + 3*t1 - 
            3*Power(t1,2) + Power(t1,3) + 3*t2 - 3*s2*t2 - 6*t1*t2 + 
            6*s2*t1*t2 + 3*Power(t1,2)*t2 - 3*s2*Power(t1,2)*t2 - 
            Power(t2,2) - 20*s2*Power(t2,2) + 7*Power(s2,2)*Power(t2,2) + 
            25*t1*Power(t2,2) + 4*s2*t1*Power(t2,2) - 
            7*Power(s2,2)*t1*Power(t2,2) - 18*Power(t1,2)*Power(t2,2) + 
            16*s2*Power(t1,2)*Power(t2,2) - 6*Power(t1,3)*Power(t2,2) - 
            5*Power(t2,3) + 49*s2*Power(t2,3) - 
            9*Power(s2,2)*Power(t2,3) + Power(s2,3)*Power(t2,3) - 
            40*t1*Power(t2,3) - 36*s2*t1*Power(t2,3) + 
            8*Power(s2,2)*t1*Power(t2,3) + 42*Power(t1,2)*Power(t2,3) - 
            14*s2*Power(t1,2)*Power(t2,3) + 4*Power(t1,3)*Power(t2,3) + 
            6*Power(t2,4) - 26*s2*Power(t2,4) + 14*t1*Power(t2,4) + 
            30*s2*t1*Power(t2,4) - 4*Power(s2,2)*t1*Power(t2,4) - 
            24*Power(t1,2)*Power(t2,4) + 4*s2*Power(t1,2)*Power(t2,4) - 
            2*Power(t2,5) + 2*Power(s2,2)*Power(t2,5) + 
            4*t1*Power(t2,5) - 4*s2*t1*Power(t2,5) - 
            Power(s1,3)*(s2 - t1)*
             (4*Power(t1,2) - 6*Power(-1 + t2,2) + 
               Power(s2,2)*(1 + 3*t2) + t1*(1 - 5*t2 + 4*Power(t2,2)) + 
               s2*(5 - 7*t2 + 2*Power(t2,2) - t1*(5 + 3*t2))) + 
            Power(s1,2)*(-2*Power(-1 + t2,3) + 
               6*Power(t1,3)*(-2 + t2)*t2 + 3*Power(s2,3)*t2*(1 + t2) + 
               t1*Power(-1 + t2,2)*(25 + 16*t2) - 
               Power(t1,2)*(27 - 55*t2 + 20*Power(t2,2) + 
                  8*Power(t2,3)) + 
               Power(s2,2)*(7 + t2 - 14*Power(t2,2) + 6*Power(t2,3) - 
                  t1*(7 + 4*t2 + 7*Power(t2,2))) + 
               s2*(-(Power(-1 + t2,2)*(29 + 12*t2)) + 
                  Power(t1,2)*(7 + 13*t2 - 2*Power(t2,2)) + 
                  t1*(22 - 62*t2 + 40*Power(t2,2)))) + 
            s1*(-(Power(s2,3)*Power(t2,2)*(3 + t2)) + 
               Power(-1 + t2,3)*(-1 + 4*t2) - 
               2*t1*Power(-1 + t2,2)*(3 + 22*t2 + 7*Power(t2,2)) - 
               4*Power(t1,3)*(1 - 3*t2 + Power(t2,3)) + 
               Power(t1,2)*(9 + 27*t2 - 86*Power(t2,2) + 
                  46*Power(t2,3) + 4*Power(t2,4)) + 
               Power(s2,2)*t2*
                (-14 + 13*t2 + 7*Power(t2,2) - 6*Power(t2,3) + 
                  2*t1*(7 - 5*t2 + 4*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(3 + 55*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (3 - 23*t2 + 10*Power(t2,2) - 2*Power(t2,3)) + 
                  t1*(-6 - 26*t2 + 94*Power(t2,2) - 68*Power(t2,3) + 
                     6*Power(t2,4))))) + 
         Power(s,3)*(-3 + 10*t2 + s2*t2 + 22*Power(t2,2) - 
            40*s2*Power(t2,2) + 22*Power(s2,2)*Power(t2,2) - 
            72*Power(t2,3) - 93*s2*Power(t2,3) - 
            19*Power(s2,2)*Power(t2,3) + 2*Power(s2,3)*Power(t2,3) + 
            82*Power(t2,4) + 113*s2*Power(t2,4) - 
            4*Power(s2,2)*Power(t2,4) - 57*Power(t2,5) - 
            29*s2*Power(t2,5) + 19*Power(t2,6) + 5*s2*Power(t2,6) - 
            Power(t2,7) + Power(t1,3)*
             (-9 + t2 + 11*Power(t2,2) + 3*Power(t2,3)) + 
            Power(t1,2)*(3 + (-14 + 29*s2)*t2 - 
               3*(-4 + 9*s2)*Power(t2,2) - (52 + 7*s2)*Power(t2,3) + 
               (25 + s2)*Power(t2,4) - Power(t2,5)) + 
            t1*(9 + (10 - 34*s2)*t2 + 
               (44 - 25*s2 - 6*Power(s2,2))*Power(t2,2) + 
               (124 + 117*s2 + 2*Power(s2,2))*Power(t2,3) - 
               (179 + 27*s2)*Power(t2,4) + (35 - 3*s2)*Power(t2,5)) + 
            Power(s1,4)*(4 - 3*Power(s2,3) + 15*Power(t1,3) - 9*t2 + 
               7*Power(t2,2) - 2*Power(t2,3) - 
               2*Power(t1,2)*(-9 + 5*t2) + 
               Power(s2,2)*(1 + 18*t1 + 6*t2) - 
               s2*(4 + 15*t1 + 30*Power(t1,2) - 10*t2 + 7*Power(t2,2)) + 
               t1*(11 - 23*t2 + 13*Power(t2,2))) + 
            Power(s1,3)*(-6 - 30*Power(t1,3)*(-1 + t2) - 8*t2 + 
               40*Power(t2,2) - 33*Power(t2,3) + 7*Power(t2,4) + 
               Power(s2,3)*(-2 + 7*t2) + 
               Power(s2,2)*(-3 + t1*(17 - 30*t2) + 6*t2 - 
                  19*Power(t2,2)) + 
               Power(t1,2)*(29 - 70*t2 + 18*Power(t2,2)) + 
               t1*(-17 + 8*t2 + 37*Power(t2,2) - 32*Power(t2,3)) + 
               s2*(14 - 27*t2 + 6*Power(t2,2) + 11*Power(t2,3) + 
                  Power(t1,2)*(-42 + 50*t2) + 
                  t1*(-3 + 20*t2 + 22*Power(t2,2)))) - 
            Power(s1,2)*(-9 + 35*t2 - 99*Power(t2,2) + 125*Power(t2,3) - 
               61*Power(t2,4) + 9*Power(t2,5) + 
               Power(s2,3)*t2*(-8 + 5*t2) + 
               Power(t1,3)*(-27 + 64*t2 - 18*Power(t2,2)) + 
               Power(t1,2)*(-14 + 147*t2 - 104*Power(t2,2) + 
                  12*Power(t2,3)) + 
               t1*(23 - 89*t2 + 139*Power(t2,2) - 28*Power(t2,3) - 
                  25*Power(t2,4)) + 
               Power(s2,2)*(2*t2*(11 + 11*t2 - 10*Power(t2,2)) - 
                  2*t1*(2 - 21*t2 + 8*Power(t2,2))) + 
               s2*(1 + 27*t2 - 99*Power(t2,2) + 55*Power(t2,3) - 
                  4*Power(t2,4) + 
                  2*Power(t1,2)*(8 - 36*t2 + 9*Power(t2,2)) + 
                  t1*(4 - 101*t2 - 8*Power(t2,2) + 40*Power(t2,3)))) + 
            s1*(-5 + 2*t2 + 71*Power(t2,2) + 
               Power(s2,3)*(-8 + t2)*Power(t2,2) - 163*Power(t2,3) + 
               144*Power(t2,4) - 54*Power(t2,5) + 5*Power(t2,6) + 
               Power(t1,3)*(9 - 39*t2 + 31*Power(t2,2) - 3*Power(t2,3)) + 
               Power(t1,2)*(-34 - 28*t2 + 164*Power(t2,2) - 
                  84*Power(t2,3) + 5*Power(t2,4)) - 
               t1*(-23 + 51*t2 + 148*Power(t2,2) - 292*Power(t2,3) + 
                  70*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-4 + 38*t2 + 19*Power(t2,2) - 7*Power(t2,3) + 
                  t1*(-16 + 29*t2 - 4*Power(t2,2))) + 
               s2*(1 - 26*t2 + 122*Power(t2,2) - 192*Power(t2,3) + 
                  68*Power(t2,4) - 13*Power(t2,5) + 
                  Power(t1,2)*
                   (1 + 17*t2 - 15*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(2 + 122*t2 - 239*Power(t2,2) + 25*Power(t2,3) + 
                     21*Power(t2,4))))) + 
         Power(s,2)*(Power(t1,3)*
             (12 - 26*t2 + 12*Power(t2,2) - 5*Power(t2,3) + 
               10*Power(t2,4)) + 
            Power(t1,2)*(-24 + (87 - 26*s2)*t2 + 
               (-82 + 54*s2)*Power(t2,2) + (107 - 33*s2)*Power(t2,3) - 
               3*(31 + s2)*Power(t2,4) + 9*Power(t2,5)) + 
            t1*(12 + (-87 + 48*s2)*t2 + 
               (112 - 97*s2 + 6*Power(s2,2))*Power(t2,2) + 
               (-195 - 77*s2 + 8*Power(s2,2))*Power(t2,3) + 
               (277 + 129*s2 - 7*Power(s2,2))*Power(t2,4) - 
               5*(27 + 2*s2)*Power(t2,5) - (-16 + s2)*Power(t2,6)) + 
            Power(s1,5)*(3*Power(s2,3) - 2*Power(s2,2)*(1 + 6*t1) + 
               s2*(3 + 15*Power(t1,2) + t1*(9 - 5*t2) - 4*t2 + 
                  Power(t2,2)) - 
               t1*(5 + 6*Power(t1,2) + t1*(7 - 5*t2) - 8*t2 + 
                  3*Power(t2,2))) + 
            t2*(2*Power(s2,3)*Power(t2,2)*(-3 + 2*t2) - 
               Power(s2,2)*t2*
                (8 - 31*t2 + 18*Power(t2,2) + Power(t2,3)) + 
               Power(-1 + t2,2)*
                (26 - 28*t2 + 10*Power(t2,2) - 9*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s2*(-22 + 69*t2 + 68*Power(t2,2) - 192*Power(t2,3) + 
                  96*Power(t2,4) - 20*Power(t2,5) + Power(t2,6))) - 
            Power(s1,4)*(-(Power(-1 + t2,2)*(-1 + 2*t2)) - 
               5*Power(t1,3)*(-2 + 3*t2) + Power(s2,3)*(-3 + 8*t2) + 
               Power(t1,2)*(17 - 36*t2 + 12*Power(t2,2)) + 
               t1*(1 - 17*t2 + 26*Power(t2,2) - 10*Power(t2,3)) - 
               Power(s2,2)*(-1 + 7*t2 + Power(t2,2) + 
                  t1*(-13 + 28*t2)) + 
               s2*(6 - 5*t2 - 3*Power(t2,2) + 2*Power(t2,3) + 
                  5*Power(t1,2)*(-4 + 7*t2) - 
                  7*t1*(2 - 5*t2 + Power(t2,2)))) + 
            Power(s1,3)*(-2*Power(t1,3)*(5 - 17*t2 + 6*Power(t2,2)) + 
               Power(s2,3)*(2 - 13*t2 + 6*Power(t2,2)) - 
               Power(-1 + t2,2)*(10 - 24*t2 + 11*Power(t2,2)) + 
               Power(t1,2)*(-46 + 126*t2 - 77*Power(t2,2) + 
                  10*Power(t2,3)) - 
               2*t1*(2 + 3*t2 + 2*Power(t2,2) - 13*Power(t2,3) + 
                  6*Power(t2,4)) + 
               Power(s2,2)*(-12 + 27*t2 + Power(t2,2) - 3*Power(t2,3) + 
                  t1*(-6 + 48*t2 - 20*Power(t2,2))) + 
               s2*(5 + 18*t2 - 36*Power(t2,2) + 14*Power(t2,3) - 
                  Power(t2,4) + 
                  Power(t1,2)*(12 - 65*t2 + 24*Power(t2,2)) + 
                  t1*(39 - 103*t2 + 33*Power(t2,2) + 5*Power(t2,3)))) + 
            Power(s1,2)*(Power(s2,3)*t2*(-10 + 21*t2) + 
               3*Power(t1,3)*t2*(3 - 8*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-3 + 43*t2 - 47*Power(t2,2) + 21*Power(t2,3)) + 
               Power(t1,2)*(-28 + 170*t2 - 264*Power(t2,2) + 
                  91*Power(t2,3) - 4*Power(t2,4)) + 
               t1*(-7 - 89*t2 + 252*Power(t2,2) - 165*Power(t2,3) + 
                  3*Power(t2,4) + 6*Power(t2,5)) + 
               Power(s2,2)*(t2*
                   (53 - 75*t2 - 16*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-2 + 22*t2 - 58*Power(t2,2) + 4*Power(t2,3))) + 
               s2*(-10 + 54*t2 - 147*Power(t2,2) + 139*Power(t2,3) - 
                  41*Power(t2,4) + 5*Power(t2,5) + 
                  Power(t1,2)*
                   (-9 + 5*t2 + 42*Power(t2,2) - 3*Power(t2,3)) + 
                  t1*(45 - 213*t2 + 255*Power(t2,2) - 5*Power(t2,3) - 
                     12*Power(t2,4)))) - 
            s1*(2*Power(t1,3)*Power(t2,2)*(-6 + 7*t2) + 
               Power(s2,3)*Power(t2,2)*(-14 + 15*t2 + Power(t2,2)) + 
               Power(-1 + t2,2)*
                (-3 - 17*t2 + 38*Power(t2,2) - 33*Power(t2,3) + 
                  17*Power(t2,4)) - 
               Power(t1,2)*(-6 + 14*t2 - 181*Power(t2,2) + 
                  232*Power(t2,3) - 47*Power(t2,4) + Power(t2,5)) + 
               t1*(-3 - 51*t2 - 170*Power(t2,2) + 478*Power(t2,3) - 
                  282*Power(t2,4) + 27*Power(t2,5) + Power(t2,6)) + 
               Power(s2,2)*t2*
                (-8 + 72*t2 - 67*Power(t2,2) - 11*Power(t2,3) + 
                  Power(t2,4) + t1*(4 + 24*t2 - 30*Power(t2,2))) + 
               s2*(-2 + 9*t2 + 167*Power(t2,2) - 337*Power(t2,3) + 
                  207*Power(t2,4) - 48*Power(t2,5) + 4*Power(t2,6) + 
                  Power(t1,2)*
                   (-6 - 5*t2 + 24*Power(t2,2) - 16*Power(t2,3) + 
                     Power(t2,4)) + 
                  t1*(8 + 48*t2 - 331*Power(t2,2) + 315*Power(t2,3) - 
                     8*Power(t2,4) - 6*Power(t2,5))))) + 
         s*(-(Power(s1,6)*Power(s2 - t1,2)*(-1 + s2 - t1 + t2)) + 
            Power(s1,5)*(s2 - t1)*
             (s2*(t1*(2 - 6*t2) + 4*Power(-1 + t2,2)) - 
               Power(-1 + t2,2) - 3*t1*Power(-1 + t2,2) + 
               Power(s2,2)*(-1 + 3*t2) + Power(t1,2)*(-1 + 3*t2)) + 
            (-1 + t2)*(-4 + (26 - 8*s2)*t2 + 
               (-42 + 25*s2 - 2*Power(s2,2))*Power(t2,2) - 
               (-16 - 67*s2 + Power(s2,2) + 2*Power(s2,3))*Power(t2,3) + 
               2*(6 - 52*s2 - 3*Power(s2,2) + Power(s2,3))*Power(t2,4) + 
               2*(-5 + 13*s2 + 2*Power(s2,2))*Power(t2,5) + 
               (2 - 6*s2)*Power(t2,6) + 
               Power(t1,3)*(4 - 11*t2 + 3*Power(t2,2) + 4*Power(t2,3) + 
                  4*Power(t2,4)) + 
               Power(t1,2)*(-12 - 8*(-6 + s2)*t2 + 
                  (-41 + 22*s2)*Power(t2,2) + (70 - 28*s2)*Power(t2,3) + 
                  (-70 + 6*s2)*Power(t2,4)) + 
               t1*(12 + (-63 + 16*s2)*t2 + 
                  (80 - 47*s2 + 2*Power(s2,2))*Power(t2,2) + 
                  (-122 - 37*s2 + 13*Power(s2,2))*Power(t2,3) + 
                  (121 + 86*s2 - 11*Power(s2,2))*Power(t2,4) - 
                  8*(4 + s2)*Power(t2,5) + 4*Power(t2,6))) + 
            Power(s1,4)*(Power(s2,3)*(-1 + 5*t2 - 2*Power(t2,2)) + 
               2*Power(s2,2)*(4 - 8*t2 + 7*Power(t2,2) - 3*Power(t2,3) + 
                  t1*(3 - 10*t2 + 4*Power(t2,2))) + 
               t1*(-(Power(-1 + t2,2)*(-3 + 7*t2)) + 
                  Power(t1,2)*(3 - 8*t2 + 3*Power(t2,2)) + 
                  t1*(16 - 29*t2 + 16*Power(t2,2) - 3*Power(t2,3))) + 
               s2*(Power(-1 + t2,2)*(1 + 3*t2) + 
                  Power(t1,2)*(-8 + 23*t2 - 9*Power(t2,2)) + 
                  3*t1*(-8 + 15*t2 - 10*Power(t2,2) + 3*Power(t2,3)))) + 
            Power(s1,3)*(-2*Power(-1 + t2,3)*(2 + t2) + 
               t1*Power(-1 + t2,2)*(-41 + 26*t2 + 11*Power(t2,2)) - 
               Power(t1,3)*(7 - 9*t2 - 7*Power(t2,2) + Power(t2,3)) - 
               Power(s2,3)*(2 - 7*t2 + 11*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(12 - 74*t2 + 80*Power(t2,2) - 
                  19*Power(t2,3) + Power(t2,4)) + 
               Power(s2,2)*(16 - 65*t2 + 53*Power(t2,2) - 
                  8*Power(t2,3) + 4*Power(t2,4) + 
                  t1*(-4 - 6*t2 + 34*Power(t2,2))) + 
               s2*(Power(-1 + t2,2)*(5 - 4*t2 + 3*Power(t2,2)) + 
                  Power(t1,2)*
                   (13 - 10*t2 - 30*Power(t2,2) + 3*Power(t2,3)) + 
                  t1*(-16 + 103*t2 - 97*Power(t2,2) + 15*Power(t2,3) - 
                     5*Power(t2,4)))) + 
            Power(s1,2)*(3*Power(-1 + t2,3)*t2*(-1 + 2*t2) + 
               Power(t1,3)*t2*(21 - 45*t2 + 17*Power(t2,2)) - 
               t1*Power(-1 + t2,2)*
                (-7 - 168*t2 + 79*Power(t2,2) + Power(t2,3)) + 
               Power(s2,3)*t2*
                (6 - 15*t2 + 13*Power(t2,2) + 3*Power(t2,3)) + 
               Power(t1,2)*(-7 - 82*t2 + 237*Power(t2,2) - 
                  152*Power(t2,3) + 4*Power(t2,4)) - 
               Power(s2,2)*(-2 + 33*t2 - 111*Power(t2,2) + 
                  84*Power(t2,3) - 5*Power(t2,4) + Power(t2,5) + 
                  t1*(2 + 3*t2 - 18*Power(t2,2) + 31*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (7 - 39*t2 + 57*Power(t2,2) - 4*Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (-4 + 107*t2 - 25*Power(t2,2) + 17*Power(t2,3)) + 
                  t1*(-11 + 148*t2 - 351*Power(t2,2) + 207*Power(t2,3) + 
                     6*Power(t2,4) + Power(t2,5)))) + 
            s1*(-(Power(s2,3)*Power(t2,2)*
                  (6 - 13*t2 + 8*Power(t2,2) + Power(t2,3))) - 
               Power(-1 + t2,3)*
                (13 - t2 - 13*Power(t2,2) + 6*Power(t2,3)) + 
               Power(t1,3)*(2 - 14*t2 + 9*Power(t2,2) + 25*Power(t2,3) - 
                  20*Power(t2,4)) - 
               t1*Power(-1 + t2,2)*
                (24 - 4*t2 + 217*Power(t2,2) - 78*Power(t2,3) + 
                  8*Power(t2,4)) + 
               Power(t1,2)*(9 + 2*t2 + 115*Power(t2,2) - 
                  295*Power(t2,3) + 165*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s2,2)*t2*
                (-4 + 18*t2 - 59*Power(t2,2) + 53*Power(t2,3) - 
                  8*Power(t2,4) + 
                  t1*(4 + 18*t2 - 42*Power(t2,2) + 25*Power(t2,3) + 
                     Power(t2,4))) + 
               s2*(Power(t1,2)*
                   (-8 + 23*t2 - 24*Power(t2,2) - 5*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-8 + 13*t2 + 186*Power(t2,2) - 42*Power(t2,3) + 
                     18*Power(t2,4)) + 
                  t1*(16 - 52*t2 - 122*Power(t2,2) + 395*Power(t2,3) - 
                     242*Power(t2,4) + 5*Power(t2,5))))))*T5q(s))/
     (s*(-1 + s1)*(-1 + s2)*(-s + s2 - t1)*
       Power(-s1 + t2 - s*t2 + s1*t2 - Power(t2,2),3)));
   return a;
};
