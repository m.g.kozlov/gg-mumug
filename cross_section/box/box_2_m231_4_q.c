#include "../h/nlo_functions.h"
#include "../h/nlo_func_q.h"
#include <quadmath.h>




long double  box_2_m231_4_q(double *vars)
{
    __float128 s=vars[0], s1=vars[1], s2=vars[2], t1=vars[3], t2=vars[4];
    __float128 aG=1/Power(4.*Pi,2);
    __float128 a=aG*((-8*(8*Power(s2,7)*t1*(-1 + t2)*Power(t2,2)*
          (t1 + Power(t1,2) - 3*t1*t2 + t2*(-1 + 2*t2)) + 
         Power(s2,6)*t2*(-2*Power(-1 + t2,2)*Power(t2,2) + 
            Power(t1,4)*(8 + 16*t2 - 24*Power(t2,2)) + 
            Power(t1,3)*(8 + (4 + 16*s + 9*s1)*t2 + 
               (-121 - 16*s + 3*s1)*Power(t2,2) + 97*Power(t2,3)) + 
            Power(t1,2)*t2*(14 - 95*t2 + 230*Power(t2,2) - 
               125*Power(t2,3) + s1*(11 - 40*t2 + 5*Power(t2,2)) + 
               8*s*(1 - 6*t2 + 5*Power(t2,2))) + 
            t1*t2*(-10 - 8*(1 + s)*t2 + (91 + 32*s)*Power(t2,2) - 
               (143 + 24*s)*Power(t2,3) + 58*Power(t2,4) + 
               s1*(6 - 17*t2 + 25*Power(t2,2) - 2*Power(t2,3)))) + 
         Power(s2,5)*t2*(-2*Power(-1 + t2,2)*Power(t2,2)*(1 + 4*t2) + 
            24*Power(t1,5)*(-1 + Power(t2,2)) - 
            Power(t1,4)*(38 - 141*t2 - 78*Power(t2,2) + 
               133*Power(t2,3) + s*(16 + 15*t2 - 43*Power(t2,2)) + 
               2*s1*(5 + 13*t2 + 6*Power(t2,2))) + 
            Power(t1,3)*(-24 + 111*t2 + 8*Power(s,2)*(-1 + t2)*t2 - 
               133*Power(t2,2) - 339*Power(t2,3) + 265*Power(t2,4) + 
               2*Power(s1,2)*(-1 + Power(t2,2)) + 
               s1*(-15 + 32*t2 + 97*Power(t2,2) + 6*Power(t2,3)) - 
               s*(8 + 11*(-1 + 2*s1)*t2 + (-167 + 2*s1)*Power(t2,2) + 
                  158*Power(t2,3))) + 
            t1*t2*(10 - 2*(37 + 2*s)*t2 - 
               (8 + 45*s + 8*Power(s,2))*Power(t2,2) + 
               (280 + 173*s + 8*Power(s,2))*Power(t2,3) - 
               2*(155 + 44*s)*Power(t2,4) + 78*Power(t2,5) - 
               2*Power(s1,2)*(5 - 7*t2 + Power(t2,2) + Power(t2,3)) + 
               s1*(25 + 4*(5 + 3*s)*t2 - 23*(5 + 2*s)*Power(t2,2) + 
                  2*(47 + 5*s)*Power(t2,3))) + 
            Power(t1,2)*(10 + 6*(7 + 2*s)*t2 + 
               (-43 + 50*s + 16*Power(s,2))*Power(t2,2) - 
               (208 + 325*s + 16*Power(s,2))*Power(t2,3) + 
               (517 + 203*s)*Power(t2,4) - 222*Power(t2,5) - 
               2*Power(s1,2)*(1 - 7*Power(t2,2) + 6*Power(t2,3)) + 
               s1*(-7 - (17 + 12*s)*t2 + (69 + 68*s)*Power(t2,2) - 
                  (153 + 8*s)*Power(t2,3) + 12*Power(t2,4)))) + 
         Power(s2,4)*(-8*Power(t1,6)*t2*(-3 + 2*t2 + Power(t2,2)) - 
            4*Power(-1 + t2,2)*Power(t2,3)*(-1 + 2*t2 + 3*Power(t2,2)) + 
            Power(t1,5)*(2 + (7 + 30*s)*t2 - (268 + 33*s)*Power(t2,2) + 
               (111 - 33*s)*Power(t2,3) + 76*Power(t2,4)) + 
            Power(t1,4)*(2 + (13 + 37*s + 8*Power(s,2))*t2 - 
               (254 + 247*s + 6*Power(s,2))*Power(t2,2) + 
               (844 + 18*s - 26*Power(s,2))*Power(t2,3) + 
               2*(-43 + 90*s)*Power(t2,4) - 231*Power(t2,5)) + 
            Power(t1,2)*t2*(-9 + (103 + 6*s)*t2 + 
               (231 + 165*s - 4*Power(s,2))*Power(t2,2) + 
               (-623 - 103*s + 92*Power(s,2))*Power(t2,3) + 
               (50 - 640*s - 88*Power(s,2))*Power(t2,4) + 
               8*(73 + 43*s)*Power(t2,5) - 168*Power(t2,6)) + 
            t1*Power(t2,2)*(11 + (34 - 3*s)*t2 + 
               (-236 - 44*s + 6*Power(s,2))*Power(t2,2) + 
               (117 - 86*s - 49*Power(s,2))*Power(t2,3) + 
               (343 + 324*s + 31*Power(s,2))*Power(t2,4) - 
               (326 + 119*s)*Power(t2,5) + 45*Power(t2,6)) + 
            Power(s1,3)*t1*Power(-1 + t2,2)*
             (3*t1*Power(t2,2) + Power(t1,2)*(1 + t2) + 
               Power(t2,2)*(5 + 2*t2)) + 
            Power(t1,3)*t2*(-33 - 192*t2 + 725*Power(t2,2) - 
               819*Power(t2,3) - 345*Power(t2,4) + 292*Power(t2,5) + 
               Power(s,2)*t2*(-10 - 37*t2 + 83*Power(t2,2)) + 
               s*(-3 - 158*t2 + 406*Power(t2,2) + 331*Power(t2,3) - 
                  372*Power(t2,4))) + 
            s1*t1*(Power(t1,4)*
                (1 + 25*t2 + 31*Power(t2,2) + 15*Power(t2,3)) + 
               Power(t1,3)*(4 + 5*(3 + 4*s)*t2 + 
                  2*(-77 + 17*s)*Power(t2,2) + 
                  (-115 + 18*s)*Power(t2,3) - 38*Power(t2,4)) + 
               Power(t1,2)*(1 + 2*(19 + 6*s)*t2 + 
                  (-120 - 27*s + 13*Power(s,2))*Power(t2,2) - 
                  (-240 + 150*s + Power(s,2))*Power(t2,3) - 
                  27*(-8 + s)*Power(t2,4) - 3*Power(t2,5)) + 
               t1*t2*(-26 - 7*(19 + 4*s)*t2 - 
                  2*(-89 + 11*s + 13*Power(s,2))*Power(t2,2) + 
                  (83 + 242*s + 2*Power(s,2))*Power(t2,3) - 
                  2*(137 + 12*s)*Power(t2,4) + 4*Power(t2,5)) + 
               Power(t2,2)*(-35 + (193 + 16*s)*t2 + 
                  (-62 + 29*s + 13*Power(s,2))*Power(t2,2) - 
                  (290 + 126*s + Power(s,2))*Power(t2,3) + 
                  (190 + 33*s)*Power(t2,4) + 16*Power(t2,5))) - 
            Power(s1,2)*t1*(-1 + t2)*
             (Power(t1,3)*(2 + 6*t2 + 4*Power(t2,2)) + 
               Power(t1,2)*(2 + (9 + 2*s)*t2 + (-19 + 2*s)*Power(t2,2) - 
                  24*Power(t2,3)) + 
               t1*t2*(10 + 26*t2 - (47 + 8*s)*Power(t2,2) + 
                  39*Power(t2,3)) + 
               Power(t2,3)*(-67 + 48*t2 + 11*Power(t2,2) + s*(-2 + 6*t2)))\
) + Power(s2,3)*(8*Power(t1,7)*(-1 + t2)*t2 - 
            2*Power(-1 + t2,2)*Power(t2,3)*
             (-1 - 4*t2 + 6*Power(t2,2) + 4*Power(t2,3)) + 
            Power(t1,6)*(-3 + 92*t2 + 123*Power(t2,2) - 
               148*Power(t2,3) - 16*Power(t2,4) - 
               2*s1*(1 + 10*t2 + 10*Power(t2,2) + 3*Power(t2,3)) + 
               s*(1 - 9*t2 + 38*Power(t2,2) + 6*Power(t2,3))) + 
            Power(t1,3)*(1 - (31 + 3*s)*t2 + 
               (-350 - 142*s + 45*Power(s,2) + 2*Power(s,3))*
                Power(t2,2) - 
               (-21 + 522*s + 138*Power(s,2) + 23*Power(s,3))*
                Power(t2,3) + 
               (1351 + 1630*s + Power(s,2) - 15*Power(s,3))*
                Power(t2,4) + 
               (-1279 + 101*s + 164*Power(s,2))*Power(t2,5) - 
               2*(143 + 172*s)*Power(t2,6) + 141*Power(t2,7) + 
               Power(s1,3)*Power(-1 + t2,2)*(-3 + t2 + 8*Power(t2,2)) + 
               Power(s1,2)*(-1 + t2)*
                (3 + (20 + 11*s)*t2 - (26 + 23*s)*Power(t2,2) + 
                  (58 - 12*s)*Power(t2,3) + 41*Power(t2,4)) + 
               s1*(1 + 10*(12 + s)*t2 + 
                  (150 + 156*s - 19*Power(s,2))*Power(t2,2) + 
                  (-495 - 439*s + 78*Power(s,2))*Power(t2,3) + 
                  (386 - 254*s + 13*Power(s,2))*Power(t2,4) - 
                  7*(-41 + 7*s)*Power(t2,5) - 17*Power(t2,6))) + 
            Power(t1,2)*t2*(-8 - (85 + 3*s)*t2 + 
               (406 + 99*s - 72*Power(s,2) - 4*Power(s,3))*Power(t2,2) + 
               (297 + 398*s + 26*Power(s,2) + 31*Power(s,3))*
                Power(t2,3) + 
               (-1264 - 633*s + 208*Power(s,2) + 9*Power(s,3))*
                Power(t2,4) - 
               2*(-185 + 262*s + 69*Power(s,2))*Power(t2,5) + 
               (382 + 243*s)*Power(t2,6) - 50*Power(t2,7) + 
               Power(s1,3)*Power(-1 + t2,2)*
                (-5 - 23*t2 + 8*Power(t2,2)) - 
               Power(s1,2)*(-1 + t2)*
                (8 + (121 + 19*s)*t2 + (73 - 26*s)*Power(t2,2) - 
                  (201 + 17*s)*Power(t2,3) + 47*Power(t2,4)) + 
               s1*(40 + 8*(-34 + s)*t2 + 
                  (-332 - 219*s + 68*Power(s,2))*Power(t2,2) + 
                  (877 + 334*s - 144*Power(s,2))*Power(t2,3) + 
                  (53 + 189*s + 4*Power(s,2))*Power(t2,4) - 
                  (407 + 24*s)*Power(t2,5) - 7*Power(t2,6))) + 
            t1*Power(t2,2)*(-7 + (65 + 7*s)*t2 + 
               (9 - 32*s + 33*Power(s,2) + 2*Power(s,3))*Power(t2,2) - 
               (376 + 61*s - 28*Power(s,2) + 13*Power(s,3))*
                Power(t2,3) - 
               (-333 + 48*s + 127*Power(s,2) + Power(s,3))*Power(t2,4) + 
               (157 + 239*s + 42*Power(s,2))*Power(t2,5) - 
               3*(63 + 23*s)*Power(t2,6) + 8*Power(t2,7) + 
               2*Power(s1,3)*Power(-1 + t2,2)*
                (-5 + 12*t2 + 2*Power(t2,2)) - 
               Power(s1,2)*(-1 + t2)*
                (25 - (28 + 9*s)*t2 + (-159 + 7*s)*Power(t2,2) + 
                  2*(71 + 5*s)*Power(t2,3) + 16*Power(t2,4)) + 
               s1*(-17 - 6*(14 + 3*s)*t2 + 
                  (442 + 100*s - 39*Power(s,2))*Power(t2,2) + 
                  (-251 - 65*s + 70*Power(s,2))*Power(t2,3) - 
                  (373 + 72*s + 7*Power(s,2))*Power(t2,4) + 
                  (259 + 31*s)*Power(t2,5) + 24*Power(t2,6))) + 
            Power(t1,5)*(-5 + (147 + 41*s - 2*Power(s,2))*t2 + 
               (-219 + 262*s + 36*Power(s,2))*Power(t2,2) + 
               (-821 - 310*s + 14*Power(s,2))*Power(t2,3) + 
               (382 - 77*s)*Power(t2,4) + 84*Power(t2,5) + 
               2*Power(s1,2)*(-2 - t2 + 2*Power(t2,2) + Power(t2,3)) + 
               s1*(-7 + 53*t2 + 216*Power(t2,2) + 131*Power(t2,3) + 
                  39*Power(t2,4) - 
                  2*s*(-1 + 15*t2 + 14*Power(t2,2) + 8*Power(t2,3)))) + 
            Power(t1,4)*(-1 + 152*t2 - 194*Power(t2,2) - 
               453*Power(t2,3) + 1598*Power(t2,4) - 167*Power(t2,5) - 
               167*Power(t2,6) - 
               2*Power(s1,3)*Power(-1 + t2,2)*(1 + t2) + 
               Power(s,3)*Power(t2,2)*(5 + 7*t2) + 
               Power(s1,2)*(-7 + 17*t2 + 47*Power(t2,2) - 
                  53*Power(t2,3) - 4*Power(t2,4)) - 
               s1*(4 - 41*t2 + 50*Power(t2,2) + 574*Power(t2,3) + 
                  142*Power(t2,4) + 39*Power(t2,5)) - 
               2*Power(s,2)*t2*
                (3 - 43*t2 + 59*Power(t2,2) + 41*Power(t2,3) + 
                  s1*(5 + 2*t2 + 5*Power(t2,2))) + 
               s*(-1 + 75*t2 + 143*Power(t2,2) - 1202*Power(t2,3) + 
                  456*Power(t2,4) + 241*Power(t2,5) + 
                  Power(s1,2)*(1 - 5*t2 - Power(t2,2) + 5*Power(t2,3)) + 
                  s1*t2*(-39 + 200*t2 + 165*Power(t2,2) + 58*Power(t2,3)))\
)) + Power(s2,2)*(-2*Power(-1 + t2,2)*Power(t2,3)*
             (1 - 3*t2 - 2*Power(t2,2) + 4*Power(t2,3) + Power(t2,4)) + 
            Power(t1,7)*(1 + s1 - 101*t2 + 5*s1*t2 + 40*Power(t2,2) + 
               6*s1*Power(t2,2) + 48*Power(t2,3) - 
               s*(1 + 5*t2 + 6*Power(t2,2))) - 
            t1*Power(t2,2)*(-1 + (35 - 3*s)*t2 - 
               (143 - 18*s + 5*Power(s,2) + 2*Power(s,3))*Power(t2,2) - 
               (-92 + 7*s + 53*Power(s,2) + 3*Power(s,3))*Power(t2,3) + 
               (251 + 46*s - 49*Power(s,2) + 28*Power(s,3))*
                Power(t2,4) + 
               (-304 - 29*s + 142*Power(s,2) + Power(s,3))*Power(t2,5) - 
               s*(38 + 23*s)*Power(t2,6) + (69 + 13*s)*Power(t2,7) + 
               Power(t2,8) - Power(s1,3)*Power(-1 + t2,2)*t2*
                (-20 + 33*t2 + 2*Power(t2,2)) + 
               Power(s1,2)*(-1 + t2)*t2*
                (44 - 6*(8 + 3*s)*t2 + (-159 + 20*s)*Power(t2,2) + 
                  2*(78 + s)*Power(t2,3) + 7*Power(t2,4)) + 
               s1*(-6 + (52 - 12*s)*t2 + (25 + 64*s)*Power(t2,2) + 
                  (-355 - 172*s + 78*Power(s,2))*Power(t2,3) + 
                  (238 + 173*s - 101*Power(s,2))*Power(t2,4) + 
                  (255 - 50*s + 11*Power(s,2))*Power(t2,5) - 
                  (199 + 3*s)*Power(t2,6) - 10*Power(t2,7))) + 
            Power(t1,3)*(-1 + (59 + 9*s)*t2 + 
               (-148 - 97*s + 77*Power(s,2) + 6*Power(s,3))*
                Power(t2,2) - 
               (877 + 653*s - 94*Power(s,2) + 15*Power(s,3))*
                Power(t2,3) - 
               (-1219 + 97*s + 432*Power(s,2) + 73*Power(s,3))*
                Power(t2,4) + 
               (582 + 1741*s + 182*Power(s,2) - 26*Power(s,3))*
                Power(t2,5) + 
               (-787 + 59*s + 103*Power(s,2))*Power(t2,6) - 
               2*(72 + 67*s)*Power(t2,7) + 25*Power(t2,8) + 
               Power(s1,3)*Power(-1 + t2,2)*
                (2 + 9*t2 - 13*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s1,2)*(-1 + t2)*
                (-1 + 2*(42 + s)*t2 + (245 + 29*s)*Power(t2,2) - 
                  (271 + 82*s)*Power(t2,3) + (29 - 21*s)*Power(t2,4) + 
                  10*Power(t2,5)) + 
               s1*(-2 + 74*t2 + 
                  (780 + 190*s - 60*Power(s,2))*Power(t2,2) + 
                  (-685 + 116*s + 114*Power(s,2))*Power(t2,3) + 
                  (-934 - 699*s + 136*Power(s,2))*Power(t2,4) + 
                  (595 - 174*s + 26*Power(s,2))*Power(t2,5) + 
                  (250 - 9*s)*Power(t2,6) - 6*Power(t2,7))) + 
            Power(t1,2)*t2*(2 - (80 + 13*s)*t2 - 
               2*(48 - 48*s + 23*Power(s,2) + 3*Power(s,3))*
                Power(t2,2) + 
               (769 + 187*s - 143*Power(s,2) + Power(s,3))*Power(t2,3) + 
               (-301 + 290*s + 142*Power(s,2) + 75*Power(s,3))*
                Power(t2,4) + 
               (-805 - 579*s + 219*Power(s,2) + 14*Power(s,3))*
                Power(t2,5) + 
               (362 - 155*s - 76*Power(s,2))*Power(t2,6) + 
               2*(76 + 33*s)*Power(t2,7) - 3*Power(t2,8) + 
               Power(s1,3)*Power(-1 + t2,2)*
                (10 - 42*t2 - 56*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s1,2)*(-1 + t2)*
                (-31 + (68 + 3*s)*t2 + (359 + 47*s)*Power(t2,2) - 
                  2*(61 + 40*s)*Power(t2,3) - 
                  (287 + 10*s)*Power(t2,4) + 25*Power(t2,5)) - 
               s1*(-8 + 3*(-87 + 4*s)*t2 + 
                  (1039 + 48*s - 30*Power(s,2))*Power(t2,2) + 
                  (-198 + 255*s - 61*Power(s,2))*Power(t2,3) + 
                  (-1219 - 456*s + 216*Power(s,2))*Power(t2,4) + 
                  (250 + 61*s - 5*Power(s,2))*Power(t2,5) + 
                  (394 + 8*s)*Power(t2,6) + 3*Power(t2,7))) - 
            Power(t1,6)*(6 + 173*t2 - 624*Power(t2,2) - 
               147*Power(t2,3) + 176*Power(t2,4) + 8*Power(t2,5) + 
               2*Power(s,2)*t2*(5 + 7*t2) + 
               2*Power(s1,2)*(-1 + Power(t2,2)) + 
               s1*(1 + 115*t2 + 171*Power(t2,2) + 109*Power(t2,3) + 
                  12*Power(t2,4)) - 
               s*(-1 - 118*t2 + 92*Power(t2,2) + 219*Power(t2,3) + 
                  12*Power(t2,4) + 2*s1*(-1 + 5*t2 + 8*Power(t2,2)))) + 
            Power(t1,5)*(-8 - 200*t2 + 677*Power(t2,2) - 
               778*Power(t2,3) - 774*Power(t2,4) + 199*Power(t2,5) + 
               32*Power(t2,6) + Power(s1,3)*Power(-1 + t2,2)*(1 + t2) - 
               Power(s,3)*t2*(4 + 7*t2 + Power(t2,2)) + 
               Power(s1,2)*(-3 - 46*t2 + 17*Power(t2,2) + 
                  36*Power(t2,3) - 4*Power(t2,4)) + 
               s1*(-15 - 63*t2 + 410*Power(t2,2) + 355*Power(t2,3) + 
                  122*Power(t2,4) + 43*Power(t2,5)) + 
               Power(s,2)*t2*
                (-33 + 19*t2 + 222*Power(t2,2) + 20*Power(t2,3) + 
                  s1*(2 + 7*t2 + 3*Power(t2,2))) - 
               s*(-1 + 200*t2 - 560*Power(t2,2) - 501*Power(t2,3) + 
                  557*Power(t2,4) + 65*Power(t2,5) + 
                  Power(s1,2)*(1 - 3*t2 - Power(t2,2) + 3*Power(t2,3)) + 
                  2*s1*(1 + 17*t2 + 123*Power(t2,2) + 91*Power(t2,3) + 
                     8*Power(t2,4)))) + 
            Power(t1,4)*(-2 + 125*t2 + 518*Power(t2,2) - 
               1208*Power(t2,3) + 124*Power(t2,4) + 1026*Power(t2,5) - 
               10*Power(t2,6) - 45*Power(t2,7) - 
               Power(s1,3)*Power(-1 + t2,2)*(1 + 13*t2 + 7*Power(t2,2)) + 
               Power(s,3)*t2*(-2 + 15*t2 + 33*Power(t2,2) + 
                  14*Power(t2,3)) + 
               Power(s1,2)*(-4 - 5*t2 + 36*Power(t2,2) + 
                  139*Power(t2,3) - 186*Power(t2,4) + 20*Power(t2,5)) - 
               s1*(7 + 229*t2 - 178*Power(t2,2) - 143*Power(t2,3) + 
                  537*Power(t2,4) + 44*Power(t2,5) + 32*Power(t2,6)) - 
               Power(s,2)*t2*(36 - 29*t2 - 232*Power(t2,2) + 
                  467*Power(t2,3) + 70*Power(t2,4) + 
                  s1*(-30 + 99*t2 + 28*Power(t2,2) + 23*Power(t2,3))) + 
               s*(1 + 18*t2 + 660*Power(t2,2) - 588*Power(t2,3) - 
                  1779*Power(t2,4) + 402*Power(t2,5) + 134*Power(t2,6) + 
                  Power(s1,2)*
                   (-1 + 2*t2 - 25*Power(t2,2) + 8*Power(t2,3) + 
                     16*Power(t2,4)) + 
                  s1*t2*(-76 + 3*t2 + 652*Power(t2,2) + 351*Power(t2,3) + 
                     30*Power(t2,4))))) - 
         t1*(-((-1 + t2)*Power(t2,3)*
               (-4 + (11 - 6*s)*t2 + 
                 (-6 + 9*s - 6*Power(s,2))*Power(t2,2) + 
                 (-6 + 3*s + 3*Power(s,2) - 2*Power(s,3))*Power(t2,3) - 
                 (-6 + 9*s - 6*Power(s,2) + Power(s,3))*Power(t2,4) + 
                 Power(-1 + s,3)*Power(t2,5))) - 
            4*Power(t1,7)*(-1 + 5*t2 + 8*Power(t2,2) + 
               s*(1 + 5*t2 + 6*Power(t2,2)) - 
               s1*(1 + 5*t2 + 6*Power(t2,2))) + 
            t1*Power(t2,2)*(-4 + 4*(-6 + 5*s + 3*Power(s,2))*t2 + 
               (102 - 77*s + 8*Power(s,2) + 6*Power(s,3))*Power(t2,2) + 
               (-95 + 80*s - 16*Power(s,2) + 8*Power(s,3))*Power(t2,3) - 
               (42 + 19*s - 33*Power(s,2) + 23*Power(s,3))*Power(t2,4) + 
               (90 + 7*s - 32*Power(s,2) - 7*Power(s,3))*Power(t2,5) + 
               (-12 - 12*s - 5*Power(s,2) + 4*Power(s,3))*Power(t2,6) + 
               (-15 + s)*Power(t2,7) + 
               2*Power(s1,3)*Power(-1 + t2,2)*t2*(-5 + 7*t2) + 
               Power(s1,2)*Power(-1 + t2,2)*t2*
                (19 - (1 + 9*s)*t2 + 2*(-29 + s)*Power(t2,2)) - 
               s1*(-1 + t2)*(24 + 8*(-8 + 3*s)*t2 + 
                  (9 - 52*s + 6*Power(s,2))*Power(t2,2) + 
                  (88 + 48*s - 42*Power(s,2))*Power(t2,3) - 
                  (-1 + 19*s + Power(s,2))*Power(t2,4) + 
                  (-58 - s + 3*Power(s,2))*Power(t2,5))) + 
            Power(t1,2)*t2*(12 - (43 + 50*s + 24*Power(s,2))*t2 - 
               (91 - 249*s + 174*Power(s,2) + 24*Power(s,3))*
                Power(t2,2) + 
               (416 - 269*s + 41*Power(s,2) + 28*Power(s,3))*
                Power(t2,3) + 
               (-286 + 74*s + 70*Power(s,2) + 73*Power(s,3))*
                Power(t2,4) + 
               (-139 + 3*s + 100*Power(s,2) + 13*Power(s,3))*
                Power(t2,5) - 
               (-101 + 11*s + Power(s,2) + 6*Power(s,3))*Power(t2,6) + 
               (30 + 4*s)*Power(t2,7) + 
               Power(s1,3)*Power(-1 + t2,2)*
                (40 - 50*t2 - 23*Power(t2,2) + 2*Power(t2,3)) - 
               Power(s1,2)*(-1 + t2)*
                (-124 + 3*(61 + 4*s)*t2 + 6*(30 + s)*Power(t2,2) - 
                  (149 + 15*s)*Power(t2,3) - 2*(46 + 5*s)*Power(t2,4) + 
                  (2 + 3*s)*Power(t2,5)) + 
               s1*(32 + 164*t2 + 
                  10*(-67 - 2*s + 12*Power(s,2))*Power(t2,2) + 
                  (349 - 2*s - 26*Power(s,2))*Power(t2,3) + 
                  (379 + 28*s - 94*Power(s,2))*Power(t2,4) + 
                  (-136 + 6*s - 21*Power(s,2))*Power(t2,5) + 
                  (-119 - 12*s + 9*Power(s,2))*Power(t2,6) + Power(t2,7))\
) + Power(t1,3)*(-4 + 4*(15 + 8*s + 3*Power(s,2))*t2 + 
               (-22 - 251*s + 348*Power(s,2) + 30*Power(s,3))*
                Power(t2,2) - 
               (513 - 14*s + 54*Power(s,2) + 84*Power(s,3))*
                Power(t2,3) + 
               (662 + 120*s - 351*Power(s,2) - 117*Power(s,3))*
                Power(t2,4) + 
               (17 + 29*s - 14*Power(s,2) - 13*Power(s,3))*Power(t2,5) + 
               (-184 + 112*s + 11*Power(s,2) + 4*Power(s,3))*
                Power(t2,6) - 4*(4 + 5*s)*Power(t2,7) - 
               Power(s1,3)*Power(-1 + t2,2)*
                (-8 - 60*t2 + 9*Power(t2,2) - 11*Power(t2,3) + 
                  Power(t2,4)) + 
               Power(s1,2)*(-1 + t2)*
                (-4 + 4*(35 + 2*s)*t2 + 3*(68 + 3*s)*Power(t2,2) - 
                  7*(47 + 3*s)*Power(t2,3) + (5 - 26*s)*Power(t2,4) + 
                  6*(-2 + s)*Power(t2,5)) + 
               s1*(-8 - 4*(31 + 6*s)*t2 + 
                  (861 + 152*s - 258*Power(s,2))*Power(t2,2) + 
                  (-741 - 112*s + 192*Power(s,2))*Power(t2,3) + 
                  (-502 + 47*s + 117*Power(s,2))*Power(t2,4) + 
                  6*(77 - 18*s + 5*Power(s,2))*Power(t2,5) + 
                  (47 + 21*s - 9*Power(s,2))*Power(t2,6) + 5*Power(t2,7))\
) + Power(t1,6)*(8 - 32*t2 + 35*Power(t2,2) + 93*Power(t2,3) + 
               4*Power(t2,4) - 8*Power(s,2)*t2*(5 + 7*t2) - 
               8*Power(s1,2)*(-1 + Power(t2,2)) + 
               s1*(12 - 64*t2 + 11*Power(t2,2) - 61*Power(t2,3) - 
                  6*Power(t2,4)) + 
               s*(-4 + 20*t2 - 127*Power(t2,2) + 69*Power(t2,3) + 
                  6*Power(t2,4) + 8*s1*(-1 + 5*t2 + 8*Power(t2,2)))) + 
            Power(t1,5)*(4*Power(s1,3)*Power(-1 + t2,2) - 
               12*Power(s,3)*t2*(1 + 3*t2) + 
               Power(s1,2)*(20 - 68*t2 + 34*Power(t2,2) + 
                  20*Power(t2,3) - 6*Power(t2,4)) - 
               t2*(80 - 170*t2 + 49*Power(t2,2) + 104*Power(t2,3) + 
                  9*Power(t2,4)) + 
               s1*(4 + 88*t2 - 141*Power(t2,2) + 93*Power(t2,3) + 
                  9*Power(t2,4) + 19*Power(t2,5)) + 
               2*Power(s,2)*t2*
                (8 - 10*t2 + 83*Power(t2,2) + 3*Power(t2,3) + 
                  s1*(-2 + 26*t2)) - 
               s*(-4 + 252*t2 - 127*Power(t2,2) - 418*Power(t2,3) + 
                  24*Power(t2,4) + 21*Power(t2,5) + 
                  4*Power(s1,2)*(1 - 6*t2 + 5*Power(t2,2)) + 
                  2*s1*(4 - 64*t2 + 83*Power(t2,2) + 85*Power(t2,3)))) + 
            Power(t1,4)*(-8 + 28*t2 + 264*Power(t2,2) - 488*Power(t2,3) + 
               73*Power(t2,4) + 137*Power(t2,5) + 6*Power(t2,6) + 
               Power(s1,3)*Power(-1 + t2,2)*
                (12 - 16*t2 - 7*Power(t2,2) + Power(t2,3)) + 
               Power(s,3)*t2*(-12 + 62*t2 + 102*Power(t2,2) + 
                  5*Power(t2,3) - Power(t2,4)) + 
               Power(s1,2)*(16 + 116*t2 - 293*Power(t2,2) + 
                  218*Power(t2,3) - 77*Power(t2,4) + 20*Power(t2,5)) + 
               s1*(-12 - 304*t2 + 273*Power(t2,2) + 399*Power(t2,3) - 
                  391*Power(t2,4) + 42*Power(t2,5) - 19*Power(t2,6)) + 
               Power(s,2)*t2*(-176 + 4*t2 + 305*Power(t2,2) - 
                  155*Power(t2,3) - 14*Power(t2,4) + 
                  s1*(132 - 114*t2 - 116*Power(t2,2) - 13*Power(t2,3) + 
                     3*Power(t2,4))) + 
               s*(4 + 60*t2 + 437*Power(t2,2) - 306*Power(t2,3) - 
                  322*Power(t2,4) - 107*Power(t2,5) + 30*Power(t2,6) - 
                  2*s1*t2*(24 + 53*t2 - 59*Power(t2,2) - 
                     95*Power(t2,3) + 5*Power(t2,4)) + 
                  Power(s1,2)*
                   (-4 + 20*t2 - 53*Power(t2,2) + 23*Power(t2,3) + 
                     17*Power(t2,4) - 3*Power(t2,5))))) + 
         s2*(-32*Power(t1,8)*(-1 + t2)*t2 - 
            2*Power(-1 + t2,3)*Power(t2,4)*(-1 + t2 + Power(t2,2)) + 
            Power(t1,7)*(12 + 22*t2 - 318*Power(t2,2) + 60*Power(t2,3) + 
               8*Power(t2,4) - 
               2*s*(2 - 17*t2 + 81*Power(t2,2) + 18*Power(t2,3)) + 
               s1*(8 + 82*t2 + 90*Power(t2,2) + 36*Power(t2,3))) + 
            Power(t1,4)*(-4 - 
               2*(22 - 20*s - 2*Power(s,2) + Power(s,3))*t2 + 
               (499 + 401*s - 262*Power(s,2) - 14*Power(s,3))*
                Power(t2,2) + 
               (-34 + 849*s + 182*Power(s,2) + 128*Power(s,3))*
                Power(t2,3) + 
               (-1114 - 1407*s + 276*Power(s,2) + 113*Power(s,3))*
                Power(t2,4) + 
               (433 - 624*s - 258*Power(s,2) + 3*Power(s,3))*
                Power(t2,5) + (331 + 27*s - 14*Power(s,2))*Power(t2,6) + 
               (-19 + 30*s)*Power(t2,7) - 
               2*Power(s1,3)*Power(-1 + t2,2)*
                (-6 - 6*t2 + 15*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s1,2)*(-1 + t2)*
                (-12 - 4*(35 + 9*s)*t2 + (-69 + 73*s)*Power(t2,2) + 
                  2*(94 + 23*s)*Power(t2,3) + (-67 + 5*s)*Power(t2,4) + 
                  20*Power(t2,5)) - 
               s1*(4 + (392 + 48*s - 6*Power(s,2))*t2 + 
                  (200 + 372*s - 142*Power(s,2))*Power(t2,2) + 
                  (-955 - 419*s + 290*Power(s,2))*Power(t2,3) + 
                  2*(-99 - 146*s + 59*Power(s,2))*Power(t2,4) + 
                  (634 - 199*s + 4*Power(s,2))*Power(t2,5) + 
                  2*(-24 + 5*s)*Power(t2,6) + 19*Power(t2,7))) + 
            Power(t1,6)*(20 + 4*(7 + 26*s + 4*Power(s,2))*t2 - 
               5*(57 + 113*s + 36*Power(s,2))*Power(t2,2) + 
               (684 + 189*s - 76*Power(s,2))*Power(t2,3) + 
               (61 + 134*s)*Power(t2,4) + (-28 + 6*s)*Power(t2,5) - 
               4*Power(s1,2)*(-4 - 5*t2 + 8*Power(t2,2) + Power(t2,3)) + 
               2*s1*(14 - 26*t2 - 137*Power(t2,2) - 36*Power(t2,3) - 
                  52*Power(t2,4) - 3*Power(t2,5) + 
                  s*(-4 + 50*t2 + 82*Power(t2,2) + 40*Power(t2,3)))) + 
            Power(t1,5)*(4 - 196*t2 + 37*Power(t2,2) + 663*Power(t2,3) - 
               621*Power(t2,4) - 235*Power(t2,5) + 36*Power(t2,6) + 
               8*Power(s1,3)*Power(-1 + t2,2)*(1 + t2) - 
               2*Power(s,3)*t2*(-1 + 15*t2 + 22*Power(t2,2)) - 
               2*Power(s1,2)*(-14 + 11*t2 + 88*Power(t2,2) - 
                  88*Power(t2,3) + 3*Power(t2,5)) + 
               s1*(16 + 144*t2 - 167*Power(t2,2) + 105*Power(t2,3) + 
                  154*Power(t2,4) + 41*Power(t2,5) + 19*Power(t2,6)) + 
               2*Power(s,2)*t2*
                (6 - 63*t2 + 140*Power(t2,2) + 118*Power(t2,3) + 
                  3*Power(t2,4) + s1*(17 + 13*t2 + 30*Power(t2,2))) - 
               s*(-4 + 204*t2 + 566*Power(t2,2) - 1461*Power(t2,3) - 
                  360*Power(t2,4) + 158*Power(t2,5) + 21*Power(t2,6) + 
                  4*Power(s1,2)*
                   (1 - 6*t2 - Power(t2,2) + 6*Power(t2,3)) + 
                  2*s1*t2*(-88 + 129*t2 + 231*Power(t2,2) + 
                     100*Power(t2,3)))) + 
            t1*Power(t2,3)*(Power(s,3)*Power(t2,2)*
                (4 - 17*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*(-1 + t2)*t2*
                (-6 - 2*t2 + (-19 + 39*s1)*Power(t2,2) - 
                  5*(11 + s1)*Power(t2,3) + 4*Power(t2,4)) + 
               s*Power(-1 + t2,2)*
                (-6 + 4*(2 + 3*s1)*t2 - 
                  (25 + 22*s1 + 9*Power(s1,2))*Power(t2,2) + 
                  2*(-1 + 16*s1 + Power(s1,2))*Power(t2,3) - 
                  (16 + 5*s1)*Power(t2,4) + Power(t2,5)) - 
               Power(-1 + t2,2)*
                (10 + 31*t2 - 49*Power(t2,2) - 20*Power(t2,3) + 
                  41*Power(t2,4) + 15*Power(t2,5) - 
                  2*Power(s1,3)*t2*(-5 + 7*t2) + 
                  Power(s1,2)*t2*(-19 + t2 + 58*Power(t2,2)) + 
                  s1*(-18 + 31*t2 + 28*Power(t2,2) - 48*Power(t2,3) - 
                     61*Power(t2,4)))) + 
            Power(t1,2)*Power(t2,2)*
             (Power(s,3)*t2*(-2 - 18*t2 + 68*Power(t2,2) + 
                  59*Power(t2,3) + Power(t2,4)) + 
               s*(-1 + t2)*(-12 - 4*(-11 + 16*s1 + 9*Power(s1,2))*t2 + 
                  (-183 + 188*s1 + 23*Power(s1,2))*Power(t2,2) + 
                  (4 - 31*s1 + 20*Power(s1,2))*Power(t2,3) + 
                  (-38 - 45*s1 + Power(s1,2))*Power(t2,4) + 
                  9*Power(t2,5) + 4*Power(t2,6)) - 
               2*Power(s,2)*t2*
                (4 + 115*t2 + 28*Power(t2,2) - 112*Power(t2,3) - 
                  52*Power(t2,4) + 5*Power(t2,5) + 
                  s1*(-3 - 105*t2 + 80*Power(t2,2) + 43*Power(t2,3) - 
                     3*Power(t2,4))) + 
               Power(-1 + t2,2)*
                (34 - 86*t2 - 223*Power(t2,2) + 294*Power(t2,3) + 
                  232*Power(t2,4) + 31*Power(t2,5) + 
                  Power(s1,3)*
                   (60 - 83*t2 - 25*Power(t2,2) + 2*Power(t2,3)) + 
                  Power(s1,2)*
                   (-162 + 54*t2 + 399*Power(t2,2) + 106*Power(t2,3) - 
                     5*Power(t2,4)) + 
                  s1*(72 + 324*t2 - 416*Power(t2,2) - 581*Power(t2,3) - 
                     130*Power(t2,4) + Power(t2,5)))) - 
            Power(t1,3)*t2*(Power(s,3)*t2*
                (-4 - 26*t2 + 166*Power(t2,2) + 111*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s*(6 + 4*(2 + 7*s1 + 17*Power(s1,2))*t2 + 
                  (377 - 502*s1 - 153*Power(s1,2))*Power(t2,2) + 
                  (252 + 568*s1 + 54*Power(s1,2))*Power(t2,3) + 
                  (-472 - 83*s1 + 23*Power(s1,2))*Power(t2,4) + 
                  (-253 + 76*s1 + 8*Power(s1,2))*Power(t2,5) - 
                  (46 + 15*s1)*Power(t2,6) + 20*Power(t2,7)) + 
               Power(s,2)*t2*(2 - 484*t2 + 33*Power(t2,2) + 
                  636*Power(t2,3) - 53*Power(t2,4) - 14*Power(t2,5) + 
                  s1*(12 + 386*t2 - 463*Power(t2,2) - 100*Power(t2,3) - 
                     3*Power(t2,4))) - 
               (-1 + t2)*(-30 - 167*t2 + 266*Power(t2,2) + 
                  796*Power(t2,3) - 513*Power(t2,4) - 357*Power(t2,5) - 
                  13*Power(t2,6) + 
                  Power(s1,3)*
                   (-24 - 91*t2 + 130*Power(t2,2) - 23*Power(t2,3) + 
                     8*Power(t2,4)) + 
                  Power(s1,2)*
                   (30 + 325*t2 + 242*Power(t2,2) - 594*Power(t2,3) + 
                     18*Power(t2,4) - 9*Power(t2,5)) + 
                  s1*(158 - 297*t2 - 1193*Power(t2,2) + 367*Power(t2,3) + 
                     887*Power(t2,4) + 55*Power(t2,5) + 5*Power(t2,6)))))))/
     ((-1 + t1)*t1*(s - s2 + t1)*Power(t1 - t2,2)*(1 - s2 + t1 - t2)*
       Power(-1 + t2,2)*t2*(s - s1 + t2)*(t1 - s2*t2)*
       (-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2))) + 
    (8*(Power(1 + t2,3)*Power(1 + (-1 + s)*t2,3)*
          (2 - 3*t2 + Power(t2,2)) - 
         2*Power(s2,7)*Power(t2,2)*((-5 + t2)*t2 + s1*(1 + t2)) - 
         Power(t1,6)*(-10 - 9*t2 + Power(t2,2) + Power(s,2)*(1 + t2) + 
            Power(s1,2)*(1 + t2) + s1*(-3 + 2*t2 + Power(t2,2)) - 
            s*(-3 + 2*t2 + Power(t2,2) + 2*s1*(1 + t2))) - 
         2*Power(s2,6)*t2*(Power(s1,2)*(1 + Power(t2,2)) + 
            t2*(-1 + (-1 + 6*s)*t2 - 2*(8 + s)*Power(t2,2) + 
               4*Power(t2,3) + t1*(15 + s - 3*t2)*(1 + t2)) - 
            s1*(1 + t2)*((4 + s - 3*t2)*t2 + t1*(2 + 4*t2))) - 
         Power(t1,5)*(-21 + 40*t2 + 24*Power(t2,2) - 5*Power(t2,3) + 
            Power(s,3)*t2*(3 + t2) - 
            Power(s1,3)*(-1 + 2*t2 + Power(t2,2)) + 
            s1*(19 + 8*t2 + Power(t2,2) - 2*Power(t2,3)) - 
            Power(s1,2)*(2 - 5*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s,2)*(s1*(1 - 8*t2 - 3*Power(t2,2)) - 
               2*(-4 + t2 + 4*Power(t2,2) + Power(t2,3))) + 
            s*(-10 - 37*t2 + 6*Power(t2,2) + 7*Power(t2,3) + 
               Power(s1,2)*(-2 + 7*t2 + 3*Power(t2,2)) + 
               s1*(-6 - 3*t2 + 11*Power(t2,2) + 4*Power(t2,3)))) - 
         t1*(1 + t2)*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(5 + 6*t2 - 14*Power(t2,2) - 2*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(-1 + t2,2)*
             (-2 - 16*t2 - 3*Power(t2,2) + 2*Power(t2,3) + 
               s1*(-1 - 5*t2 - Power(t2,2) + 3*Power(t2,3))) - 
            s*(-1 + t2)*(-1 - 11*t2 - 39*Power(t2,2) - 2*Power(t2,3) + 
               9*Power(t2,4) + 
               s1*(6 + 8*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(t1,4)*(-37 - 27*t2 + 57*Power(t2,2) + 34*Power(t2,3) - 
            7*Power(t2,4) + s1*
             (25 - 14*t2 + 19*Power(t2,2) + 5*Power(t2,3) - 
               5*Power(t2,4)) - 
            Power(s1,3)*(-4 + 6*t2 - 2*Power(t2,2) + Power(t2,3) + 
               Power(t2,4)) + 
            Power(s,3)*(2 - 2*t2 + 4*Power(t2,2) + 5*Power(t2,3) + 
               Power(t2,4)) - 
            Power(s1,2)*(27 - 26*t2 - 13*Power(t2,2) + Power(t2,3) + 
               3*Power(t2,4)) - 
            Power(s,2)*(s1*(8 - 2*t2 + 2*Power(t2,2) + 
                  11*Power(t2,3) + 3*Power(t2,4)) + 
               2*(5 - 21*t2 - 16*Power(t2,2) + 10*Power(t2,3) + 
                  5*Power(t2,4))) + 
            s*(62 - 25*t2 - 118*Power(t2,2) + 5*Power(t2,3) + 
               20*Power(t2,4) + 
               Power(s1,2)*(2 + 6*t2 - 4*Power(t2,2) + 7*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(3 - 14*t2 - 51*Power(t2,2) + 11*Power(t2,3) + 
                  13*Power(t2,4)))) + 
         Power(t1,3)*(Power(s,3)*t2*
             (-9 + 7*t2 + 9*Power(t2,2) - 9*Power(t2,3) - 4*Power(t2,4)) \
+ (-1 + t2)*(-9 - 19*t2 - 45*Power(t2,2) - 18*Power(t2,3) + 
               5*Power(t2,4) + Power(s1,3)*(1 + 2*t2 + Power(t2,4)) + 
               2*s1*(19 - 8*t2 - 16*Power(t2,2) - 2*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(s1,2)*(-8 - 5*t2 - 23*Power(t2,2) + 
                  2*Power(t2,3) + 4*Power(t2,4))) + 
            s*(-35 - 138*t2 + 49*Power(t2,2) + 185*Power(t2,3) + 
               9*Power(t2,4) - 28*Power(t2,5) + 
               s1*(66 - 65*t2 - 59*Power(t2,2) + 101*Power(t2,3) + 
                  5*Power(t2,4) - 20*Power(t2,5)) + 
               Power(s1,2)*(-10 + 23*t2 - 30*Power(t2,2) + 
                  16*Power(t2,3) + Power(t2,4) - 6*Power(t2,5))) + 
            Power(s,2)*(20 + 65*t2 - 74*Power(t2,2) - 106*Power(t2,3) + 
               22*Power(t2,4) + 21*Power(t2,5) + 
               s1*(-13 + 17*t2 + 22*Power(t2,2) - 30*Power(t2,3) + 
                  9*Power(t2,4) + 9*Power(t2,5)))) + 
         Power(t1,2)*(Power(s,3)*t2*
             (3 + 15*t2 - 11*Power(t2,2) - 22*Power(t2,3) + 
               7*Power(t2,4) + 6*Power(t2,5)) - 
            Power(-1 + t2,2)*
             (7 - 9*t2 - 21*Power(t2,2) - 4*Power(t2,3) + 
               3*Power(t2,4) + 
               Power(s1,2)*(2 + t2 - 3*Power(t2,2) + Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(-4 - 27*t2 - 40*Power(t2,2) + 5*Power(t2,4))) + 
            s*(-1 + t2)*(11 + 9*t2 - 135*Power(t2,2) - 
               149*Power(t2,3) + 2*Power(t2,4) + 22*Power(t2,5) + 
               Power(s1,2)*(-4 - t2 + 4*Power(t2,2) - 4*Power(t2,3) - 
                  2*Power(t2,4) + 3*Power(t2,5)) + 
               s1*(3 + 66*t2 + 34*Power(t2,2) - 71*Power(t2,3) - 
                  Power(t2,4) + 17*Power(t2,5))) - 
            Power(s,2)*(1 + 26*t2 + 92*Power(t2,2) - 47*Power(t2,3) - 
               127*Power(t2,4) + 8*Power(t2,5) + 23*Power(t2,6) + 
               s1*(-4 - 24*t2 + 22*Power(t2,2) + 34*Power(t2,3) - 
                  32*Power(t2,4) - 3*Power(t2,5) + 9*Power(t2,6)))) + 
         Power(s2,5)*(Power(s1,3)*(-1 + t2) + 
            Power(s1,2)*(t2*(5 + s - 6*t2 - s*t2 + 3*Power(t2,2) - 
                  6*Power(t2,3)) + 
               t1*(2 + 5*t2 + 7*Power(t2,2) + 10*Power(t2,3))) - 
            s1*(2*Power(t1,2)*
                (1 + 9*t2 + 14*Power(t2,2) + 6*Power(t2,3)) + 
               t2*(-4 + (4 + 3*s)*t2 - (25 + 6*s)*Power(t2,2) - 
                  5*(1 + s)*Power(t2,3) + 6*Power(t2,4)) + 
               t1*t2*(16 + 27*t2 - 8*Power(t2,2) - 19*Power(t2,3) + 
                  4*s*(2 + 3*t2 + 3*Power(t2,2)))) + 
            t2*(t1*(-4 + 2*(-6 + 21*s + Power(s,2))*t2 + 
                  2*(-59 + 10*s + Power(s,2))*Power(t2,2) - 
                  (39 + 22*s)*Power(t2,3) + 17*Power(t2,4)) + 
               2*Power(t1,2)*
                (15 + 42*t2 + 6*Power(t2,2) - 3*Power(t2,3) + 
                  s*(2 + 5*t2 + 3*Power(t2,2))) + 
               t2*(-8 - 8*t2 - 2*Power(s,2)*(-3 + t2)*t2 + 
                  22*Power(t2,2) + 46*Power(t2,3) - 12*Power(t2,4) + 
                  s*(2 + 2*t2 - 25*Power(t2,2) + 17*Power(t2,3))))) + 
         Power(s2,4)*(-2*Power(t1,3)*(1 + t2)*
             (5 + s + 39*t2 + 6*s*t2 - 3*Power(t2,2) + 
               3*s*Power(t2,2) - Power(t2,3)) + 
            Power(s1,3)*(3 - 5*t2 + 2*Power(t2,2) + 
               2*(-1 + t1)*Power(t2,3)) + 
            Power(t1,2)*(2 - 6*(-3 + 8*s + Power(s,2))*t2 + 
               (166 - 99*s - 10*Power(s,2))*Power(t2,2) + 
               (201 + 38*s - 8*Power(s,2))*Power(t2,3) + 
               3*(-5 + 11*s)*Power(t2,4) - 10*Power(t2,5)) + 
            t1*t2*(16 + (45 - 3*s - 17*Power(s,2))*t2 - 
               2*(19 - 53*s + Power(s,2))*Power(t2,2) + 
               (-220 - 6*s + 19*Power(s,2))*Power(t2,3) - 
               (17 + 53*s)*Power(t2,4) + 16*Power(t2,5)) - 
            t2*(2 + 2*(-2 + 4*s + Power(s,2))*t2 + 
               (33 - 10*s)*Power(t2,2) + 
               (10 + 31*s - 12*Power(s,2))*Power(t2,3) + 
               2*(-17 + 5*s + 5*Power(s,2))*Power(t2,4) - 
               (35 + 27*s)*Power(t2,5) + 8*Power(t2,6)) - 
            Power(s1,2)*(-3 + 2*t2 - (12 + 5*s)*Power(t2,2) - 
               (-7 + s)*Power(t2,3) - 8*Power(t2,4) + 6*Power(t2,5) + 
               Power(t1,2)*(5 + 7*t2 + 28*Power(t2,2) + 
                  18*Power(t2,3)) + 
               t1*(5 + t2 + 11*Power(t2,2) - 14*Power(t2,3) - 
                  21*Power(t2,4) + s*(4 - 2*t2 + 4*Power(t2,3)))) + 
            s1*(8*Power(t1,3)*(1 + 4*t2 + 4*Power(t2,2) + Power(t2,3)) + 
               t1*(-4 + (-2 + 17*s + 2*Power(s,2))*t2 - 
                  (59 + 16*s)*Power(t2,2) + 
                  (-75 - 18*s + 2*Power(s,2))*Power(t2,3) + 
                  (3 - 33*s)*Power(t2,4) + 15*Power(t2,5)) + 
               Power(t1,2)*(8 + 48*t2 + 23*Power(t2,2) - 
                  34*Power(t2,3) - 21*Power(t2,4) + 
                  s*(6 + 24*t2 + 40*Power(t2,2) + 26*Power(t2,3))) + 
               t2*(-10 + 9*t2 + 4*Power(t2,2) + 36*Power(t2,3) - 
                  7*Power(t2,4) - 2*Power(t2,5) + 
                  Power(s,2)*t2*(-1 - 4*t2 + Power(t2,2)) + 
                  s*(1 - 4*t2 - 11*Power(t2,2) + 9*Power(t2,3) + 
                     3*Power(t2,4))))) + 
         Power(s2,3)*(t1*(2 + (-3 + 5*s + 5*Power(s,2))*t2 - 
               (-82 + 10*s + 11*Power(s,2) + 3*Power(s,3))*
                Power(t2,2) + 
               (95 + 92*s - 58*Power(s,2) - 2*Power(s,3))*Power(t2,3) + 
               (-87 + 138*s + 22*Power(s,2) - 3*Power(s,3))*
                Power(t2,4) + 
               (-177 - 85*s + 46*Power(s,2))*Power(t2,5) + 
               (3 - 48*s)*Power(t2,6) + 5*Power(t2,7)) + 
            2*Power(t1,4)*(15 + 42*t2 + 6*Power(t2,2) - 3*Power(t2,3) + 
               s*(3 + 9*t2 + 7*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*(Power(t1,2)*
                (3 - 5*Power(t2,2) - 6*Power(t2,3)) - 
               2*(1 - t2 - 2*Power(t2,2) + Power(t2,3) + Power(t2,4)) + 
               t1*(1 - 3*t2 + 7*Power(t2,3) + 3*Power(t2,4))) + 
            Power(t1,3)*(-8 - 106*t2 - 285*Power(t2,2) - 
               73*Power(t2,3) + 23*Power(t2,4) + Power(t2,5) + 
               Power(s,2)*(4 + 15*t2 + 23*Power(t2,2) + 
                  10*Power(t2,3)) - 
               2*s*(-9 - 61*t2 - 13*Power(t2,2) + 41*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(t1,2)*(-8 - 66*t2 - 29*Power(t2,2) + 
               349*Power(t2,3) + 181*Power(t2,4) - 31*Power(t2,5) - 
               4*Power(t2,6) + Power(s,3)*t2*(1 + t2 + 2*Power(t2,2)) + 
               Power(s,2)*t2*
                (22 + 27*t2 - 49*Power(t2,2) - 38*Power(t2,3)) + 
               s*(-2 - 6*t2 - 169*Power(t2,2) - 148*Power(t2,3) + 
                  140*Power(t2,4) + 43*Power(t2,5))) + 
            t2*(5 - 3*t2 - 8*Power(t2,2) - 44*Power(t2,3) + 
               20*Power(t2,4) + 21*Power(t2,5) + 11*Power(t2,6) - 
               2*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(2 + t2 + Power(t2,2)) + 
               Power(s,2)*Power(t2,2)*
                (4 + 11*t2 + Power(t2,2) - 18*Power(t2,3)) + 
               s*(-2 - 2*t2 - Power(t2,2) - 67*Power(t2,4) + 
                  11*Power(t2,5) + 19*Power(t2,6))) + 
            Power(s1,2)*(-9 - (-12 + s)*t2 - (19 + 9*s)*Power(t2,2) + 
               8*(1 + 2*s)*Power(t2,3) + 6*Power(t2,4) + 
               4*Power(t2,5) - 2*Power(t2,6) + 
               2*Power(t1,3)*
                (1 + 8*t2 + 20*Power(t2,2) + 7*Power(t2,3)) + 
               Power(t1,2)*(7 + 13*t2 + 15*Power(t2,2) - 
                  41*Power(t2,3) - 26*Power(t2,4) + 
                  s*(-1 + t2 + 12*Power(t2,2) + 14*Power(t2,3))) + 
               t1*(-1 - 18*t2 + 21*Power(t2,2) - 52*Power(t2,3) + 
                  4*Power(t2,4) + 14*Power(t2,5) + 
                  s*(12 - 22*t2 + 3*Power(t2,2) - 18*Power(t2,3) - 
                     7*Power(t2,4)))) + 
            s1*(-3 + t2 - 12*s*t2 + 
               (-14 + 18*s + Power(s,2))*Power(t2,2) + 
               (15 - 35*s - 8*Power(s,2))*Power(t2,3) + 
               (4 - 13*s - 8*Power(s,2))*Power(t2,4) + 
               (8 + 15*s + 3*Power(s,2))*Power(t2,5) - 
               (11 + s)*Power(t2,6) - 
               2*Power(t1,4)*(6 + 14*t2 + 9*Power(t2,2) + Power(t2,3)) - 
               Power(t1,3)*(23 + 46*t2 - 2*Power(t2,2) - 
                  42*Power(t2,3) - 9*Power(t2,4) + 
                  2*s*(7 + 18*t2 + 33*Power(t2,2) + 12*Power(t2,3))) - 
               Power(t1,2)*(-6 - 41*t2 - 175*Power(t2,2) - 
                  72*Power(t2,3) + 35*Power(t2,4) + 11*Power(t2,5) + 
                  Power(s,2)*
                   (5 + t2 + 8*Power(t2,2) + 10*Power(t2,3)) + 
                  s*(14 + 11*t2 - 6*Power(t2,2) - 67*Power(t2,3) - 
                     60*Power(t2,4))) + 
               t1*(10 + 8*t2 - 11*Power(t2,2) - 120*Power(t2,3) - 
                  35*Power(t2,4) + 24*Power(t2,5) + 4*Power(t2,6) + 
                  3*Power(s,2)*t2*
                   (1 + 6*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
                  s*(5 + 15*t2 + 39*Power(t2,2) + 22*Power(t2,3) - 
                     37*Power(t2,5))))) + 
         Power(s2,2)*(1 + 12*s*t2 + 2*Power(t2,2) + 12*s*Power(t2,2) + 
            13*Power(s,2)*Power(t2,2) + 5*Power(t2,3) + 
            47*s*Power(t2,3) + 10*Power(s,2)*Power(t2,3) - 
            16*Power(t2,4) + 45*s*Power(t2,4) + 
            8*Power(s,2)*Power(t2,4) + Power(t2,5) - 76*s*Power(t2,5) + 
            17*Power(s,2)*Power(t2,5) + 3*Power(s,3)*Power(t2,5) + 
            9*Power(t2,6) - 58*s*Power(t2,6) - 
            10*Power(s,2)*Power(t2,6) + 3*Power(s,3)*Power(t2,6) - 
            2*Power(t2,7) + 13*s*Power(t2,7) - 
            14*Power(s,2)*Power(t2,7) + 5*s*Power(t2,8) - 
            2*Power(t1,5)*(1 + t2)*(15 + 3*s - 3*t2 + 2*s*t2) + 
            Power(t1,4)*(26 + 163*t2 + 133*Power(t2,2) - 
               7*Power(t2,3) - 3*Power(t2,4) - 
               Power(s,2)*(7 + 17*t2 + 22*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s*(-47 - 78*t2 + 62*Power(t2,2) + 34*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(s1,3)*t1*(1 - t2 - 6*Power(t2,2) + 5*Power(t2,4) + 
               Power(t2,5) + 
               Power(t1,2)*(-3 - 2*t2 + 11*Power(t2,2) + 
                  6*Power(t2,3)) - 
               t1*(7 - 15*t2 + 4*Power(t2,2) + 9*Power(t2,3) + 
                  7*Power(t2,4))) - 
            Power(t1,3)*(-29 - 84*t2 + 201*Power(t2,2) + 
               303*Power(t2,3) + 10*Power(t2,4) - 13*Power(t2,5) + 
               2*Power(s,3)*(1 + t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s,2)*(11 + 51*t2 - 12*Power(t2,2) - 
                  91*Power(t2,3) - 23*Power(t2,4)) + 
               s*(-7 - 112*t2 - 268*Power(t2,2) + 80*Power(t2,3) + 
                  98*Power(t2,4) + 7*Power(t2,5))) + 
            Power(t1,2)*(-1 - 64*t2 - 207*Power(t2,2) + 
               49*Power(t2,3) + 311*Power(t2,4) + 49*Power(t2,5) - 
               17*Power(t2,6) + 
               Power(s,3)*t2*
                (7 + 4*t2 + 12*Power(t2,2) + 11*Power(t2,3)) + 
               Power(s,2)*t2*
                (33 + 120*t2 + 45*Power(t2,2) - 124*Power(t2,3) - 
                  48*Power(t2,4)) + 
               s*(3 + 15*t2 - 11*Power(t2,2) - 321*Power(t2,3) - 
                  32*Power(t2,4) + 126*Power(t2,5) + 16*Power(t2,6))) - 
            t1*(5 + 7*t2 - 25*Power(t2,2) - 122*Power(t2,3) + 
               29*Power(t2,4) + 78*Power(t2,5) + 35*Power(t2,6) - 
               7*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*
                (5 + 2*t2 + 9*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s,2)*t2*
                (16 + 31*t2 + 55*Power(t2,2) + 54*Power(t2,3) - 
                  65*Power(t2,4) - 43*Power(t2,5)) + 
               s*(1 + 3*t2 + 74*Power(t2,2) + 102*Power(t2,3) - 
                  271*Power(t2,4) - 121*Power(t2,5) + 71*Power(t2,6) + 
                  15*Power(t2,7))) - 
            Power(s1,2)*(Power(t1,4)*
                (-2 + 22*t2 + 24*Power(t2,2) + 4*Power(t2,3)) + 
               Power(t1,3)*(5 + 24*t2 - 6*Power(t2,2) - 
                  38*Power(t2,3) - 13*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (-6 - 5*t2 + 7*Power(t2,2) + 7*Power(t2,3) + 
                  Power(t2,4)) - 
               t1*t2*(40 + 3*t2 - 23*Power(t2,2) - 30*Power(t2,3) + 
                  7*Power(t2,4) + 3*Power(t2,5)) + 
               Power(t1,2)*(3 + 33*t2 - 53*Power(t2,2) - 
                  66*Power(t2,3) + 24*Power(t2,4) + 11*Power(t2,5)) + 
               s*(2*Power(t2,3)*(-1 + Power(t2,2)) + 
                  Power(t1,3)*
                   (-8 + 3*t2 + 31*Power(t2,2) + 16*Power(t2,3)) - 
                  Power(t1,2)*
                   (14 - 28*t2 + 29*Power(t2,2) + 22*Power(t2,3) + 
                     23*Power(t2,4)) + 
                  t1*(14 - 37*t2 + 38*Power(t2,2) - 18*Power(t2,3) + 
                     17*Power(t2,4) + 4*Power(t2,5)))) + 
            s1*(9 + (-9 + 17*s)*t2 + (21 - 10*s)*Power(t2,2) + 
               (-30 - 26*s + Power(s,2))*Power(t2,3) + 
               (-12 - 25*s + 2*Power(s,2))*Power(t2,4) + 
               (18 + 34*s - 8*Power(s,2))*Power(t2,5) + 
               (6 + 11*s + 3*Power(s,2))*Power(t2,6) - 
               (3 + s)*Power(t2,7) + 
               4*Power(t1,5)*(2 + 3*t2 + Power(t2,2)) + 
               Power(t1,4)*(21 + 22*t2 - 18*Power(t2,2) - 
                  20*Power(t2,3) - Power(t2,4) + 
                  s*(8 + 42*t2 + 46*Power(t2,2) + 8*Power(t2,3))) + 
               Power(t1,3)*(-7 - 125*t2 - 156*Power(t2,2) + 
                  7*Power(t2,3) + 27*Power(t2,4) + 2*Power(t2,5) + 
                  2*Power(s,2)*
                   (-1 + 4*t2 + 13*Power(t2,2) + 7*Power(t2,3)) + 
                  s*(21 + 4*t2 + 21*Power(t2,2) - 114*Power(t2,3) - 
                     36*Power(t2,4))) - 
               Power(t1,2)*(17 - 10*t2 - 109*Power(t2,2) - 
                  107*Power(t2,3) + 5*Power(t2,4) + 23*Power(t2,5) + 
                  Power(t2,6) + 
                  Power(s,2)*
                   (-13 + 23*t2 + 23*Power(t2,2) + 22*Power(t2,3) + 
                     27*Power(t2,4)) + 
                  s*(11 + 14*t2 + 113*Power(t2,2) + 106*Power(t2,3) - 
                     74*Power(t2,4) - 50*Power(t2,5))) + 
               t1*(2 + 16*t2 - 35*Power(t2,2) + 64*Power(t2,3) - 
                  60*Power(t2,4) + 13*Power(t2,6) + 
                  Power(s,2)*t2*
                   (-16 + 21*t2 + 16*Power(t2,2) + 7*Power(t2,3) + 
                     10*Power(t2,4)) - 
                  s*(12 + 44*t2 - 110*Power(t2,2) - 69*Power(t2,3) + 
                     Power(t2,4) + 17*Power(t2,5) + 21*Power(t2,6))))) - 
         s2*(-2*Power(t1,6)*(5 + s - t2 + s*t2) + 
            t1*(1 + (-2 + 4*s - 15*Power(s,2))*t2 + 
               (25 + 85*s - 34*Power(s,2) - 13*Power(s,3))*Power(t2,2) - 
               (23 - 180*s + 21*Power(s,2) + 11*Power(s,3))*
                Power(t2,3) + 
               (-25 - 98*s + 100*Power(s,2) + 4*Power(s,3))*
                Power(t2,4) + 
               (20 - 200*s + 72*Power(s,2) + 13*Power(s,3))*
                Power(t2,5) + 
               (7 + 5*s - 40*Power(s,2) + 11*Power(s,3))*Power(t2,6) + 
               (-3 + 24*s - 14*Power(s,2))*Power(t2,7)) + 
            (1 + t2)*(3 + (-5 + 16*s)*t2 + 
               (9 - 10*s + 17*Power(s,2))*Power(t2,2) + 
               (-26 + 6*s + 4*Power(s,3))*Power(t2,3) - 
               (-28 + 50*s - 6*Power(s,2) + Power(s,3))*Power(t2,4) + 
               (-6 + 37*s - 31*Power(s,2) + Power(s,3))*Power(t2,5) + 
               (-4 + 6*s + 4*Power(s,2) - 3*Power(s,3))*Power(t2,6) + 
               (1 - 5*s + 4*Power(s,2))*Power(t2,7)) + 
            Power(t1,5)*(32 + 72*t2 + 15*Power(t2,2) - 3*Power(t2,3) - 
               Power(s,2)*(2 + 13*t2 + 5*Power(t2,2)) + 
               2*s*(-18 + 5*t2 + 10*Power(t2,2) + Power(t2,3))) + 
            Power(t1,4)*(39 - 5*t2 - 191*Power(t2,2) - 49*Power(t2,3) + 
               14*Power(t2,4) - 
               Power(s,3)*(-1 + 4*t2 + 9*Power(t2,2) + 2*Power(t2,3)) + 
               s*(24 + 141*t2 + 44*Power(t2,2) - 61*Power(t2,3) - 
                  14*Power(t2,4)) + 
               Power(s,2)*(-28 - 26*t2 + 55*Power(t2,2) + 
                  31*Power(t2,3) + 2*Power(t2,4))) + 
            Power(s1,3)*Power(t1,2)*
             (Power(t1,2)*(-2 + t2 + 7*Power(t2,2) + 2*Power(t2,3)) + 
               t1*(1 + t2 - 5*Power(t2,3) - 5*Power(t2,4)) + 
               2*(-1 - Power(t2,3) + Power(t2,4) + Power(t2,5))) + 
            Power(t1,3)*(-15 - 159*t2 - 31*Power(t2,2) + 
               226*Power(t2,3) + 78*Power(t2,4) - 19*Power(t2,5) + 
               Power(s,3)*(-2 - 7*t2 + 9*Power(t2,2) + 27*Power(t2,3) + 
                  9*Power(t2,4)) + 
               Power(s,2)*(19 + 70*t2 + 105*Power(t2,2) - 
                  49*Power(t2,3) - 71*Power(t2,4) - 10*Power(t2,5)) + 
               s*(15 + 112*t2 - 218*Power(t2,2) - 208*Power(t2,3) + 
                  83*Power(t2,4) + 36*Power(t2,5))) - 
            Power(t1,2)*(10 - 26*t2 - 88*Power(t2,2) - 17*Power(t2,3) + 
               84*Power(t2,4) + 47*Power(t2,5) - 10*Power(t2,6) + 
               Power(s,3)*t2*
                (-11 - 14*t2 + 9*Power(t2,2) + 29*Power(t2,3) + 
                  15*Power(t2,4)) + 
               Power(s,2)*(1 - 5*t2 + 12*Power(t2,2) + 
                  151*Power(t2,3) + 38*Power(t2,4) - 77*Power(t2,5) - 
                  18*Power(t2,6)) + 
               s*(5 + 119*t2 + 240*Power(t2,2) - 262*Power(t2,3) - 
                  317*Power(t2,4) + 46*Power(t2,5) + 43*Power(t2,6))) - 
            Power(s1,2)*t1*(Power(t1,4)*t2*(11 + 5*t2) + 
               Power(t1,3)*(1 + 12*t2 - 7*Power(t2,2) - 
                  16*Power(t2,3) - 2*Power(t2,4)) + 
               Power(t1,2)*(28 + 27*t2 - 99*Power(t2,2) - 
                  6*Power(t2,3) + 15*Power(t2,4) + 3*Power(t2,5)) + 
               t1*(3 - 55*t2 + 2*Power(t2,2) + 44*Power(t2,3) + 
                  20*Power(t2,4) - 13*Power(t2,5) - Power(t2,6)) + 
               2*(-1 - t2 + 6*Power(t2,2) - 7*Power(t2,4) + 
                  Power(t2,5) + 2*Power(t2,6)) + 
               s*(6 - 7*t2 - 17*Power(t2,2) + 14*Power(t2,3) + 
                  12*Power(t2,4) - 7*Power(t2,5) - Power(t2,6) + 
                  Power(t1,3)*
                   (-5 + 6*t2 + 23*Power(t2,2) + 6*Power(t2,3)) - 
                  Power(t1,2)*
                   (12 + 4*t2 - 15*Power(t2,2) + 28*Power(t2,3) + 
                     19*Power(t2,4)) + 
                  t1*(-22 + 77*t2 - 59*Power(t2,2) + 2*Power(t2,3) + 
                     6*Power(t2,4) + 14*Power(t2,5)))) + 
            s1*(2*Power(t1,6)*(1 + t2) - 
               (-1 + t2)*Power(1 + t2,2)*
                (6 + 2*(-7 + 3*s)*t2 + (8 - 13*s)*Power(t2,2) + 
                  (2 + 6*s - 5*Power(s,2))*Power(t2,3) + 
                  (-2 + s + Power(s,2))*Power(t2,4)) + 
               Power(t1,5)*(9 + 6*t2 - 13*Power(t2,2) - 2*Power(t2,3) + 
                  2*s*(1 + 12*t2 + 5*Power(t2,2))) + 
               Power(t1,4)*(-20 - 106*t2 - 21*Power(t2,2) + 
                  15*Power(t2,3) + 4*Power(t2,4) + 
                  Power(s,2)*
                   (-4 + 9*t2 + 25*Power(t2,2) + 6*Power(t2,3)) - 
                  s*(3 - 61*t2 + 51*Power(t2,2) + 47*Power(t2,3) + 
                     4*Power(t2,4))) + 
               Power(t1,3)*(3 + 50*t2 + 51*Power(t2,2) + 
                  36*Power(t2,3) - 14*Power(t2,4) - 6*Power(t2,5) + 
                  Power(s,2)*
                   (-21 + 12*t2 + 10*Power(t2,2) - 50*Power(t2,3) - 
                     23*Power(t2,4)) + 
                  s*(32 - 79*t2 - 135*Power(t2,2) - 14*Power(t2,3) + 
                     67*Power(t2,4) + 13*Power(t2,5))) + 
               Power(t1,2)*(-7 - 49*t2 + 131*Power(t2,2) - 
                  45*Power(t2,3) - 42*Power(t2,4) + 6*Power(t2,5) + 
                  6*Power(t2,6) + 
                  Power(s,2)*
                   (12 + 18*t2 - 38*Power(t2,2) - 4*Power(t2,3) + 
                     25*Power(t2,4) + 27*Power(t2,5)) + 
                  s*(-32 + 69*t2 + 9*Power(t2,2) + 27*Power(t2,3) + 
                     65*Power(t2,4) - 40*Power(t2,5) - 14*Power(t2,6))) + 
               t1*(-(Power(-1 + t2,2)*
                     (-3 - 55*t2 - 60*Power(t2,2) - 25*Power(t2,3) + 
                       9*Power(t2,4) + 2*Power(t2,5))) - 
                  Power(s,2)*t2*
                   (17 - 13*t2 - 31*Power(t2,2) + 18*Power(t2,3) + 
                     4*Power(t2,4) + 9*Power(t2,5)) + 
                  s*(-13 - t2 + Power(t2,2) - 29*Power(t2,3) + 
                     49*Power(t2,4) - 23*Power(t2,5) + 11*Power(t2,6) + 
                     5*Power(t2,7))))))*B1(1 - s2 + t1 - t2,t2,t1))/
     ((-1 + t1)*(s - s2 + t1)*(1 - s2 + t1 - t2)*(s - s1 + t2)*
       Power(t1 - s2*t2,3)) - (8*
       (Power(s2,10)*(-4*Power(t1,3)*Power(t2,2) + 4*t1*Power(t2,4)) + 
         Power(s2,9)*t2*(Power(t1,3)*(16 + 4*s + 13*s1 - 41*t2)*t2 + 
            2*Power(t2,3) + Power(t1,4)*(8 + 22*t2) + 
            Power(t1,2)*t2*(-6 + s1*(5 - 11*t2) - 21*t2 + Power(t2,2)) + 
            t1*Power(t2,2)*(-8 + s1 - 9*t2 - 4*s*t2 + 4*s1*t2 + 
               24*Power(t2,2))) - 
         Power(s2,8)*(-2*Power(t2,4)*(1 + 6*t2) + 
            Power(t1,5)*(4 + 44*t2 + 44*Power(t2,2)) + 
            Power(t1,4)*t2*(32 + 4*Power(s1,2) - 65*t2 - 
               191*Power(t2,2) + s*(8 + 17*t2) + s1*(26 + 38*t2)) + 
            Power(t1,3)*t2*(-12 + (-34 + s)*t2 - 4*Power(s1,2)*t2 - 
               2*(73 + 21*s)*Power(t2,2) + 207*Power(t2,3) + 
               s1*(10 + (17 + 12*s)*t2 - 104*Power(t2,2))) + 
            t1*Power(t2,3)*(9 + 5*(14 + s)*t2 + 
               8*(8 + 3*s)*Power(t2,2) - 60*Power(t2,3) - 
               s1*(5 + 16*t2 + 24*Power(t2,2))) + 
            Power(t1,2)*Power(t2,2)*
             (-25 - 2*(9 + 7*s)*t2 + (151 + s)*Power(t2,2) - 
               24*Power(t2,3) + 12*Power(s1,2)*(1 + t2) + 
               s1*(-17 - 3*(19 + 4*s)*t2 + 72*Power(t2,2)))) + 
         Power(s2,7)*(2*Power(t2,4)*(-2 + 6*t2 + 15*Power(t2,2)) + 
            Power(t1,6)*(22 + 88*t2 + 38*Power(t2,2)) + 
            Power(t1,5)*(16 - 7*t2 - 513*Power(t2,2) - 
               299*Power(t2,3) + 2*Power(s1,2)*(2 + 7*t2) + 
               s1*(13 + 76*t2 + 6*Power(t2,2)) + 
               s*(4 + (34 - 8*s1)*t2 + 41*Power(t2,2))) + 
            Power(t1,2)*Power(t2,2)*
             (13 + (283 + 8*s)*t2 + 8*(39 + 14*s)*Power(t2,2) - 
               2*(201 + 17*s)*Power(t2,3) + 101*Power(t2,4) + 
               Power(s1,3)*(3 + 2*t2) - 
               Power(s1,2)*t2*(97 + 2*s + 80*t2) + 
               s1*(-41 + (91 + 16*s)*t2 + 12*(18 + 7*s)*Power(t2,2) - 
                  173*Power(t2,3))) + 
            t1*Power(t2,3)*(11 - (78 + s)*t2 - 
               (241 + 28*s)*Power(t2,2) - 15*(13 + 4*s)*Power(t2,3) + 
               80*Power(t2,4) + 
               s1*(-5 + 27*t2 + 71*Power(t2,2) + 60*Power(t2,3))) - 
            Power(t1,4)*(6 + (21 - 2*s)*t2 + 
               2*(213 + 62*s)*Power(t2,2) + 
               (-359 + 188*s)*Power(t2,3) - 691*Power(t2,4) + 
               Power(s1,3)*(2 + t2) + 
               Power(s1,2)*t2*(-6 - 2*s + 55*t2) + 
               s1*(-5 - 3*(25 + 8*s)*t2 + (249 - 52*s)*Power(t2,2) + 
                  150*Power(t2,3))) + 
            Power(t1,3)*t2*(-26 + (22 - 13*s)*t2 + 
               (340 + 6*s)*Power(t2,2) + (483 + 241*s)*Power(t2,3) - 
               575*Power(t2,4) + 5*Power(s1,3)*(1 + t2) + 
               Power(s1,2)*(24 + 57*t2 + 67*Power(t2,2)) + 
               s1*(-37 - 8*(31 + 5*s)*t2 - 2*(33 + 64*s)*Power(t2,2) + 
                  269*Power(t2,3)))) - 
         Power(s2,6)*(4*Power(t1,7)*(11 + 19*t2 + 3*Power(t2,2)) + 
            2*Power(t2,4)*(1 + 8*t2 - 15*Power(t2,2) - 20*Power(t2,3)) + 
            Power(t1,3)*t2*(7 + 4*(103 + s)*t2 + 
               (569 + 157*s)*Power(t2,2) + 
               2*(-617 - 89*s + 16*Power(s,2))*Power(t2,3) - 
               (685 + 756*s)*Power(t2,4) + 889*Power(t2,5) - 
               3*Power(s1,3)*(-7 + 6*t2 + 11*Power(t2,2)) + 
               Power(s1,2)*(15 + (-286 + 6*s)*t2 + 
                  3*(-146 + 5*s)*Power(t2,2) - 269*Power(t2,3)) + 
               s1*(-67 + 3*(107 + 8*s)*t2 + 
                  (1489 + 362*s - 12*Power(s,2))*Power(t2,2) + 
                  (221 + 448*s)*Power(t2,3) - 202*Power(t2,4))) - 
            Power(t1,2)*Power(t2,2)*
             (-31 + 129*t2 + (1133 + 81*s)*Power(t2,2) + 
               (1038 + 293*s + 12*Power(s,2))*Power(t2,3) - 
               3*(172 + 49*s)*Power(t2,4) + 184*Power(t2,5) + 
               Power(s1,3)*(-6 + 15*t2 + 8*Power(t2,2)) + 
               Power(s1,2)*(39 + (17 + s)*t2 - 
                  (317 + 6*s)*Power(t2,2) - 212*Power(t2,3)) + 
               s1*(5 - (257 + 10*s)*t2 + 
                  (149 + 88*s - 4*Power(s,2))*Power(t2,2) + 
                  (458 + 228*s)*Power(t2,3) - 180*Power(t2,4))) + 
            t1*Power(t2,3)*(-13 - (43 + 5*s)*t2 + 
               (226 + 7*s)*Power(t2,2) + (437 + 63*s)*Power(t2,3) + 
               8*(41 + 10*s)*Power(t2,4) - 60*Power(t2,5) + 
               s1*(7 + 34*t2 - 68*Power(t2,2) - 148*Power(t2,3) - 
                  80*Power(t2,4))) + 
            Power(t1,4)*(-9 + 66*t2 + (377 + 30*s)*Power(t2,2) + 
               (2187 + 865*s - 24*Power(s,2))*Power(t2,3) + 
               (-1315 + 852*s)*Power(t2,4) - 1243*Power(t2,5) + 
               Power(s1,3)*(-3 + 32*t2 + 18*Power(t2,2)) + 
               Power(s1,2)*(6 + (83 - 13*s)*t2 + 
                  (83 - 32*s)*Power(t2,2) + 255*Power(t2,3)) + 
               s1*(-19 - (281 + 46*s)*t2 + 
                  2*(-429 - 170*s + 6*Power(s,2))*Power(t2,2) + 
                  (785 - 304*s)*Power(t2,3) + 4*Power(t2,4))) + 
            Power(t1,6)*(17 - 453*t2 + 4*Power(s,2)*t2 - 
               913*Power(t2,2) - 167*Power(t2,3) + 
               2*Power(s1,2)*(7 + 8*t2) + 
               s1*(38 + 12*t2 - 64*Power(t2,2)) + 
               s*(17 + 82*t2 + 69*Power(t2,2) - 4*s1*(2 + 5*t2))) + 
            Power(t1,5)*(-4 - 410*t2 - 4*Power(s,2)*s1*t2 - 
               299*Power(t2,2) + 2257*Power(t2,3) + 729*Power(t2,4) - 
               Power(s1,3)*(5 + 7*t2) - 
               2*Power(s1,2)*(-5 + 36*t2 + 59*Power(t2,2)) + 
               s1*(47 - 158*t2 - 568*Power(t2,2) + 174*Power(t2,3)) + 
               s*(1 - 130*t2 - 539*Power(t2,2) - 392*Power(t2,3) + 
                  Power(s1,2)*(8 + 11*t2) + 
                  2*s1*(6 + 37*t2 + 52*Power(t2,2))))) + 
         Power(s2,5)*(Power(t1,8)*(38 + 24*t2) + 
            2*Power(t2,4)*(1 - 5*t2 - 12*Power(t2,2) + 20*Power(t2,3) + 
               15*Power(t2,4)) + 
            Power(t1,2)*Power(t2,2)*
             (-11 - (271 + 36*s)*t2 + 
               (477 + 40*s - 2*Power(s,2))*Power(t2,2) + 
               (2112 + 342*s + 23*Power(s,2) - 2*Power(s,3))*
                Power(t2,3) + 
               (1677 + 349*s + 56*Power(s,2))*Power(t2,4) - 
               2*(161 + 134*s)*Power(t2,5) + 171*Power(t2,6) + 
               Power(s1,3)*t2*(-20 + 27*t2 + 12*Power(t2,2)) - 
               Power(s1,2)*(18 + (-185 + 6*s)*t2 - 
                  17*(5 + s)*Power(t2,2) + (527 + 4*s)*Power(t2,3) + 
                  288*Power(t2,4)) - 
               s1*(1 + 4*(-20 + 7*s)*t2 + 
                  (627 + 82*s + 7*Power(s,2))*Power(t2,2) + 
                  2*(22 - 76*s + 9*Power(s,2))*Power(t2,3) - 
                  2*(311 + 156*s)*Power(t2,4) + 45*Power(t2,5))) + 
            t1*Power(t2,3)*(-1 + 11*(9 + s)*t2 + 
               (53 + 12*s)*Power(t2,2) - 10*(31 + 2*s)*Power(t2,3) - 
               4*(115 + 18*s)*Power(t2,4) - 
               3*(109 + 20*s)*Power(t2,5) + 24*Power(t2,6) + 
               s1*(6 - 35*t2 - 82*Power(t2,2) + 104*Power(t2,3) + 
                  167*Power(t2,4) + 60*Power(t2,5))) + 
            Power(t1,3)*t2*(21 + (-86 + 22*s)*t2 + 
               (-2051 - 116*s + 6*Power(s,2))*Power(t2,2) + 
               (-2264 - 795*s - 97*Power(s,2) + 4*Power(s,3))*
                Power(t2,3) + 
               (2033 + 770*s - 199*Power(s,2))*Power(t2,4) + 
               (348 + 1265*s)*Power(t2,5) - 756*Power(t2,6) + 
               Power(s1,3)*(22 - 143*t2 + 14*Power(t2,2) + 
                  93*Power(t2,3)) + 
               Power(s1,2)*(-33 + 3*(-50 + 11*s)*t2 + 
                  (1144 - 50*s)*Power(t2,2) + 
                  (1206 - 87*s)*Power(t2,3) + 443*Power(t2,4)) - 
               s1*(-20 - (703 + 90*s)*t2 + 
                  (932 + 144*s - 31*Power(s,2))*Power(t2,2) + 
                  (3596 + 1002*s - 69*Power(s,2))*Power(t2,3) + 
                  8*(85 + 83*s)*Power(t2,4) + 204*Power(t2,5))) + 
            Power(t1,4)*(1 + 269*t2 + 
               (436 + 96*s - 6*Power(s,2))*Power(t2,2) + 
               (-1909 - 152*s + 149*Power(s,2))*Power(t2,3) + 
               3*(-1666 - 982*s + 85*Power(s,2))*Power(t2,4) + 
               (2585 - 1766*s)*Power(t2,5) + 1143*Power(t2,6) + 
               Power(s1,3)*(8 + 22*t2 - 175*Power(t2,2) - 
                  94*Power(t2,3)) - 
               Power(s1,2)*(9 + (223 + 44*s)*t2 + 
                  (878 - 114*s)*Power(t2,2) - 
                  62*(-11 + 3*s)*Power(t2,3) + 429*Power(t2,4)) + 
               s1*(-37 + (318 - 56*s)*t2 + 
                  (2724 + 492*s - 51*Power(s,2))*Power(t2,2) + 
                  (3078 + 1408*s - 99*Power(s,2))*Power(t2,3) + 
                  (-628 + 444*s)*Power(t2,4) + 610*Power(t2,5))) + 
            Power(t1,7)*(-131 - 929*t2 - 563*Power(t2,2) - 
               16*Power(t2,3) + 2*Power(s1,2)*(8 + 3*t2) + 
               Power(s,2)*(4 + 6*t2) + 
               s1*(6 - 128*t2 - 45*Power(t2,2)) + 
               s*(41 + 138*t2 + 45*Power(t2,2) - 4*s1*(5 + 3*t2))) + 
            Power(t1,6)*(-131 - 847*t2 + 2*Power(s,3)*t2 + 
               2325*Power(t2,2) + 2850*Power(t2,3) + 213*Power(t2,4) - 
               Power(s1,3)*(7 + 11*t2) - 
               Power(s1,2)*(17 + 216*t2 + 41*Power(t2,2)) + 
               s1*(-13 - 654*t2 + 217*Power(t2,2) + 338*Power(t2,3)) + 
               Power(s,2)*(-5*s1*(2 + 3*t2) + t2*(16 + 13*t2)) + 
               s*(-48 - 534*t2 - 1035*Power(t2,2) - 362*Power(t2,3) + 
                  Power(s1,2)*(17 + 24*t2) + 
                  2*s1*(11 + 70*t2 + 14*Power(t2,2)))) + 
            Power(t1,5)*(35 + 232*t2 + 3153*Power(t2,2) - 
               4*Power(s,3)*Power(t2,2) + 1004*Power(t2,3) - 
               4745*Power(t2,4) - 773*Power(t2,5) + 
               15*Power(s1,3)*(1 + 3*t2 + 4*Power(t2,2)) + 
               Power(s1,2)*(26 - 52*t2 + 503*Power(t2,2) + 
                  225*Power(t2,3)) - 
               2*s1*(47 + 577*t2 - 175*Power(t2,2) - 362*Power(t2,3) + 
                  366*Power(t2,4)) + 
               Power(s,2)*t2*
                (2 - 95*t2 - 131*Power(t2,2) + s1*(37 + 63*t2)) + 
               s*(3 + 16*t2 + 1118*Power(t2,2) + 2796*Power(t2,3) + 
                  1146*Power(t2,4) + 
                  Power(s1,2)*(17 - 98*t2 - 119*Power(t2,2)) - 
                  6*s1*(1 + 48*t2 + 113*Power(t2,2) + 18*Power(t2,3))))) \
+ Power(s2,4)*(-12*Power(t1,9) + 
            2*Power(t2,5)*(3 - 9*t2 - 8*Power(t2,2) + 15*Power(t2,3) + 
               6*Power(t2,4)) + 
            t1*Power(t2,3)*(-6 + (25 - 6*s + 24*s1)*t2 + 
               (175 + 37*s - 67*s1)*Power(t2,2) + 
               (11 + 6*s - 92*s1)*Power(t2,3) + 
               (-219 - 28*s + 99*s1)*Power(t2,4) + 
               (-286 - 43*s + 104*s1)*Power(t2,5) - 
               24*(8 + s - s1)*Power(t2,6) + 4*Power(t2,7)) + 
            Power(t1,8)*(315 - 6*Power(s,2) - 6*Power(s1,2) + 
               3*s*(-23 + 4*s1 - 30*t2) + 625*t2 + 76*Power(t2,2) + 
               s1*(64 + 90*t2)) + 
            Power(t1,2)*Power(t2,2)*
             (13 + (-180 + 13*s)*t2 + 
               (-602 - 112*s + 25*Power(s,2))*Power(t2,2) + 
               (822 + 277*s - 61*Power(s,2) - 5*Power(s,3))*
                Power(t2,3) + 
               (2077 + 586*s + 71*Power(s,2) - 10*Power(s,3))*
                Power(t2,4) + 
               (1517 + 189*s + 104*Power(s,2))*Power(t2,5) - 
               (66 + 247*s)*Power(t2,6) + 80*Power(t2,7) + 
               Power(s1,3)*Power(t2,2)*(-22 + 21*t2 + 8*Power(t2,2)) + 
               Power(s1,2)*t2*
                (-84 + (361 - 24*s)*t2 + (133 + 45*s)*Power(t2,2) + 
                  (-469 + 4*s)*Power(t2,3) - 212*Power(t2,4)) + 
               s1*(6 + (35 + 24*s)*t2 + 
                  2*(127 - 77*s + 3*Power(s,2))*Power(t2,2) - 
                  (909 + 150*s + 11*Power(s,2))*Power(t2,3) + 
                  (-329 + 86*s - 32*Power(s,2))*Power(t2,4) + 
                  12*(44 + 19*s)*Power(t2,5) + 64*Power(t2,6))) - 
            Power(t1,3)*t2*(6 + (-381 + 5*s)*t2 + 
               (332 + 91*s + 79*Power(s,2))*Power(t2,2) + 
               (4489 + 1079*s - 96*Power(s,2) - 14*Power(s,3))*
                Power(t2,3) + 
               (3384 + 1602*s + 396*Power(s,2) - 33*Power(s,3))*
                Power(t2,4) + 
               (-1535 - 1369*s + 441*Power(s,2))*Power(t2,5) + 
               (98 - 1123*s)*Power(t2,6) + 328*Power(t2,7) + 
               Power(s1,3)*t2*
                (-136 + 337*t2 + 8*Power(t2,2) - 131*Power(t2,3)) + 
               Power(s1,2)*(-6 + (413 + 6*s)*t2 + 
                  (636 - 135*s)*Power(t2,2) + 
                  2*(-1068 + 65*s)*Power(t2,3) + 
                  (-1556 + 169*s)*Power(t2,4) - 325*Power(t2,5)) + 
               s1*(22 + (-94 + 56*s)*t2 + 
                  (-2127 - 620*s + 29*Power(s,2))*Power(t2,2) + 
                  (858 + 358*s - 102*Power(s,2))*Power(t2,3) + 
                  (4364 + 1204*s - 125*Power(s,2))*Power(t2,4) + 
                  (1127 + 394*s)*Power(t2,5) + 421*Power(t2,6))) + 
            Power(t1,4)*(-1 + 3*(10 + s)*t2 + 
               (1828 + 203*s + 87*Power(s,2))*Power(t2,2) + 
               (2953 + 1460*s + 94*Power(s,2) - 8*Power(s,3))*
                Power(t2,3) - 
               (4262 + 211*s - 834*Power(s,2) + 34*Power(s,3))*
                Power(t2,4) + 
               (-5071 - 5112*s + 725*Power(s,2))*Power(t2,5) - 
               3*(-840 + 587*s)*Power(t2,6) + 498*Power(t2,7) + 
               Power(s1,3)*(-12 + 212*t2 + 164*Power(t2,2) - 
                  417*Power(t2,3) - 190*Power(t2,4)) + 
               Power(s1,2)*(24 + (61 + 8*s)*t2 - 
                  2*(675 + 83*s)*Power(t2,2) + 
                  (-2777 + 371*s)*Power(t2,3) + 
                  2*(-751 + 196*s)*Power(t2,4) - 205*Power(t2,5)) + 
               s1*(4 + (-793 + 28*s)*t2 + 
                  (956 - 298*s + 51*Power(s,2))*Power(t2,2) + 
                  (8206 + 1664*s - 255*Power(s,2))*Power(t2,3) + 
                  (5056 + 2334*s - 198*Power(s,2))*Power(t2,4) + 
                  (1000 - 90*s)*Power(t2,5) + 874*Power(t2,6))) + 
            Power(t1,7)*(396 - 643*t2 - 3999*Power(t2,2) - 
               1005*Power(t2,3) + 4*Power(t2,4) - 
               Power(s,3)*(4 + 5*t2) + Power(s1,3)*(7 + 5*t2) + 
               Power(s1,2)*(98 + 100*t2 - 26*Power(t2,2)) + 
               s1*(236 + 76*t2 - 880*Power(t2,2) - 85*Power(t2,3)) + 
               Power(s,2)*(15*s1*(1 + t2) - 
                  2*(8 + 27*t2 + 13*Power(t2,2))) + 
               s*(183 + 906*t2 + 964*Power(t2,2) + 85*Power(t2,3) - 
                  3*Power(s1,2)*(6 + 5*t2) + 
                  s1*(-36 - 46*t2 + 52*Power(t2,2)))) + 
            Power(t1,6)*(-68 - 1813*t2 - 4727*Power(t2,2) + 
               5787*Power(t2,3) + 3500*Power(t2,4) + 77*Power(t2,5) + 
               Power(s,3)*t2*(13 + 12*t2) - 
               Power(s1,3)*(35 + 93*t2 + 50*Power(t2,2)) + 
               Power(s1,2)*(68 - 148*t2 - 743*Power(t2,2) + 
                  103*Power(t2,3)) + 
               s1*(434 + 539*t2 - 1924*Power(t2,2) + 
                  1847*Power(t2,3) + 480*Power(t2,4)) + 
               Power(s,2)*(4 + 135*t2 + 381*Power(t2,2) + 
                  207*Power(t2,3) + s1*(11 - 114*t2 - 74*Power(t2,2))) \
+ s*(9 - 537*t2 - 3262*Power(t2,2) - 3649*Power(t2,3) - 
                  522*Power(t2,4) + 
                  2*Power(s1,2)*(24 + 97*t2 + 56*Power(t2,2)) + 
                  s1*(64 + 442*t2 + 368*Power(t2,2) - 310*Power(t2,3)))) \
+ Power(t1,5)*(-70 - 131*t2 + 1572*Power(t2,2) + 9931*Power(t2,3) + 
               210*Power(t2,4) - 4759*Power(t2,5) - 335*Power(t2,6) + 
               2*Power(s,3)*Power(t2,2)*(-5 + 2*t2) + 
               2*Power(s1,3)*
                (-21 + 106*t2 + 107*Power(t2,2) + 78*Power(t2,3)) + 
               Power(s1,2)*(70 + 765*t2 + 450*Power(t2,2) + 
                  1316*Power(t2,3) - 21*Power(t2,4)) - 
               2*s1*(46 + 976*t2 + 3027*Power(t2,2) - 61*Power(t2,3) + 
                  682*Power(t2,4) + 471*Power(t2,5)) - 
               Power(s,2)*t2*
                (37 + 248*t2 + 830*Power(t2,2) + 569*Power(t2,3) + 
                  s1*(39 - 263*t2 - 164*Power(t2,2))) - 
               s*(5 + 46*t2 + 310*Power(t2,2) - 3680*Power(t2,3) - 
                  6372*Power(t2,4) - 1346*Power(t2,5) + 
                  Power(s1,2)*
                   (2 - 7*t2 + 462*Power(t2,2) + 324*Power(t2,3)) + 
                  s1*(-4 + 232*t2 + 1562*Power(t2,2) + 
                     1550*Power(t2,3) - 514*Power(t2,4))))) + 
         Power(s2,3)*(Power(t1,9)*(-229 + 45*s - 45*s1 - 104*t2) + 
            2*Power(t2,6)*(3 - 7*t2 - 2*Power(t2,2) + 6*Power(t2,3) + 
               Power(t2,4)) - 
            t1*Power(t2,4)*(36 + 3*(-27 + 4*s - 10*s1)*t2 + 
               (-105 - 41*s + 57*s1)*Power(t2,2) + 
               (24 + 4*s + 49*s1)*Power(t2,3) + 
               (76 + 19*s - 53*s1)*Power(t2,4) + 
               3*(33 + 4*s - 11*s1)*Power(t2,5) + 
               (61 + 4*s - 4*s1)*Power(t2,6)) + 
            Power(t1,8)*(-116 + 3*Power(s,3) - 3*Power(s1,3) + 
               2364*t2 + 1739*Power(t2,2) + Power(s1,2)*(-59 + 38*t2) + 
               Power(s,2)*(41 - 9*s1 + 38*t2) + 
               s*(-263 + 9*Power(s1,2) + s1*(18 - 76*t2) - 842*t2 - 
                  290*Power(t2,2)) + 
               s1*(-119 + 746*t2 + 290*Power(t2,2))) + 
            Power(t1,2)*Power(t2,3)*
             (51 - 502*t2 - 408*Power(t2,2) + 589*Power(t2,3) + 
               1127*Power(t2,4) + 763*Power(t2,5) + 21*Power(t2,6) + 
               15*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(6 - 13*t2 - 20*Power(t2,2)) + 
               2*Power(s1,3)*Power(t2,2)*(-4 + 3*t2 + Power(t2,2)) + 
               Power(s1,2)*t2*
                (-150 + 335*t2 + 79*Power(t2,2) - 212*Power(t2,3) - 
                  80*Power(t2,4)) + 
               Power(s,2)*t2*
                (-18 + (25 + 12*s1)*t2 + (-155 + 9*s1)*Power(t2,2) + 
                  (81 - 28*s1)*Power(t2,3) + 96*Power(t2,4)) + 
               s*(-6 + (-29 + 108*s1)*t2 - 
                  2*(119 + 111*s1 + 15*Power(s1,2))*Power(t2,2) + 
                  (525 - 106*s1 + 43*Power(s1,2))*Power(t2,3) + 
                  2*(222 - 7*s1 + 3*Power(s1,2))*Power(t2,4) + 
                  (17 + 84*s1)*Power(t2,5) - 114*Power(t2,6)) + 
               s1*(-12 + 93*t2 + 420*Power(t2,2) - 820*Power(t2,3) - 
                  287*Power(t2,4) + 249*Power(t2,5) + 53*Power(t2,6))) + 
            Power(t1,5)*(-11 - 833*t2 - 2560*Power(t2,2) + 
               4022*Power(t2,3) + 12470*Power(t2,4) - 
               1673*Power(t2,5) - 2002*Power(t2,6) - 36*Power(t2,7) + 
               2*Power(s,3)*Power(t2,2)*(45 + 44*t2 + 43*Power(t2,2)) + 
               2*Power(s1,3)*
                (-32 - 211*t2 + 314*Power(t2,2) + 242*Power(t2,3) + 
                  73*Power(t2,4)) + 
               Power(s1,2)*(72 + 657*t2 + 3374*Power(t2,2) + 
                  1978*Power(t2,3) + 1052*Power(t2,4) - 323*Power(t2,5)\
) - s1*(-284 + 100*t2 + 8427*Power(t2,2) + 10802*Power(t2,3) + 
                  1562*Power(t2,4) + 3048*Power(t2,5) + 414*Power(t2,6)\
) + s*(6 + 14*(-15 + 31*s1 + 22*Power(s1,2))*t2 - 
                  (1423 + 1240*s1 + 280*Power(s1,2))*Power(t2,2) - 
                  4*(565 + 852*s1 + 258*Power(s1,2))*Power(t2,3) + 
                  (6397 - 504*s1 - 260*Power(s1,2))*Power(t2,4) + 
                  2*(3288 + 515*s1)*Power(t2,5) + 617*Power(t2,6)) + 
               Power(s,2)*t2*
                (-3 + 194*t2 - 918*Power(t2,2) - 2228*Power(t2,3) - 
                  801*Power(t2,4) + 
                  2*s1*(9 + 21*t2 + 282*Power(t2,2) + 14*Power(t2,3)))) \
+ Power(t1,4)*t2*(-167 + 40*t2 + 5141*Power(t2,2) + 4261*Power(t2,3) - 
               4212*Power(t2,4) - 2011*Power(t2,5) + 1108*Power(t2,6) + 
               76*Power(t2,7) - 
               Power(s,3)*Power(t2,2)*(68 + 95*t2 + 132*Power(t2,2)) + 
               Power(s1,3)*(-224 + 742*t2 + 328*Power(t2,2) - 
                  499*Power(t2,3) - 161*Power(t2,4)) + 
               Power(s1,2)*(360 + 793*t2 - 3110*Power(t2,2) - 
                  3719*Power(t2,3) - 1232*Power(t2,4) + 110*Power(t2,5)\
) + s1*(-108 - 3398*t2 + 1170*Power(t2,2) + 9766*Power(t2,3) + 
                  4537*Power(t2,4) + 1863*Power(t2,5) + 424*Power(t2,6)\
) - s*(18 + 2*(-50 + 277*s1 + 98*Power(s1,2))*t2 + 
                  2*(-679 + 208*s1 + 58*Power(s1,2))*Power(t2,2) - 
                  (4411 + 2398*s1 + 571*Power(s1,2))*Power(t2,3) - 
                  2*(-339 + 726*s1 + 157*Power(s1,2))*Power(t2,4) + 
                  (4442 + 594*s1)*Power(t2,5) + 794*Power(t2,6)) + 
               Power(s,2)*t2*
                (-33 - 272*t2 + 169*Power(t2,2) + 1680*Power(t2,3) + 
                  845*Power(t2,4) - 
                  3*s1*(6 - 36*t2 + 145*Power(t2,2) + 22*Power(t2,3)))) \
+ Power(t1,7)*(340 + 4063*t2 - 1351*Power(t2,2) - 5813*Power(t2,3) - 
               409*Power(t2,4) + 8*Power(t2,5) + 
               Power(s,3)*(5 + 2*t2 - 3*Power(t2,2)) + 
               Power(s1,3)*(49 + 82*t2 + 3*Power(t2,2)) - 
               2*Power(s1,2)*
                (44 - 351*t2 + 43*Power(t2,2) + 37*Power(t2,3)) - 
               s1*(373 - 1656*t2 + 1042*Power(t2,2) + 
                  1660*Power(t2,3) + 35*Power(t2,4)) + 
               s*(43 + 1534*t2 + 4180*Power(t2,2) + 1832*Power(t2,3) + 
                  35*Power(t2,4) - 
                  9*Power(s1,2)*(13 + 18*t2 + Power(t2,2)) + 
                  4*s1*(-20 - 73*t2 + 134*Power(t2,2) + 37*Power(t2,3))\
) + Power(s,2)*(s1*(63 + 78*t2 + 9*Power(t2,2)) - 
                  2*(32 + 161*t2 + 225*Power(t2,2) + 37*Power(t2,3)))) + 
            Power(t1,3)*Power(t2,2)*
             (67 + 1264*t2 - 1057*Power(t2,2) - 4228*Power(t2,3) - 
               2562*Power(t2,4) + 405*Power(t2,5) - 151*Power(t2,6) - 
               56*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(9 + 54*t2 + 85*Power(t2,2)) + 
               Power(s1,3)*t2*
                (202 - 341*t2 - 11*Power(t2,2) + 90*Power(t2,3)) + 
               Power(s1,2)*(168 - 935*t2 - 982*Power(t2,2) + 
                  2000*Power(t2,3) + 977*Power(t2,4) + 86*Power(t2,5)) \
+ s1*(-116 + 90*t2 + 2485*Power(t2,2) + 313*Power(t2,3) - 
                  2884*Power(t2,4) - 898*Power(t2,5) - 234*Power(t2,6)) \
+ s*(18 + 6*(21 - 7*s1 + 2*Power(s1,2))*t2 + 
                  (-293 + 1168*s1 + 137*Power(s1,2))*Power(t2,2) - 
                  2*(1259 + 214*s1 + 63*Power(s1,2))*Power(t2,3) - 
                  (1365 + 656*s1 + 135*Power(s1,2))*Power(t2,4) - 
                  2*(-592 + 5*s1)*Power(t2,5) + 494*Power(t2,6)) + 
               Power(s,2)*t2*
                (47 + 62*t2 + 320*Power(t2,2) - 610*Power(t2,3) - 
                  449*Power(t2,4) + 
                  s1*(6 - 81*t2 + 110*Power(t2,2) + 75*Power(t2,3)))) - 
            Power(t1,6)*(-16 + 683*t2 + 8167*Power(t2,2) + 
               7813*Power(t2,3) - 6821*Power(t2,4) - 1494*Power(t2,5) + 
               7*Power(t2,6) + 
               Power(s,3)*t2*(42 + 39*t2 + 16*Power(t2,2)) + 
               Power(s1,3)*(30 + 188*t2 + 341*Power(t2,2) + 
                  50*Power(t2,3)) + 
               Power(s1,2)*(221 + 93*t2 + 1017*Power(t2,2) + 
                  471*Power(t2,3) - 275*Power(t2,4)) - 
               s1*(477 + 4470*t2 + 2017*Power(t2,2) - 62*Power(t2,3) + 
                  3207*Power(t2,4) + 202*Power(t2,5)) + 
               Power(s,2)*(-7 - 55*t2 - 865*Power(t2,2) - 
                  1489*Power(t2,3) - 383*Power(t2,4) + 
                  s1*(6 + 144*t2 + 317*Power(t2,2) + 18*Power(t2,3))) + 
               s*(-25 - 512*t2 + 1425*Power(t2,2) + 8162*Power(t2,3) + 
                  4865*Power(t2,4) + 234*Power(t2,5) - 
                  Power(s1,2)*
                   (-124 + 406*t2 + 697*Power(t2,2) + 84*Power(t2,3)) + 
                  2*s1*(-27 - 395*t2 - 909*Power(t2,2) + 
                     369*Power(t2,3) + 329*Power(t2,4))))) + 
         Power(s2,2)*(44*Power(t1,10) + 
            2*Power(t2,7)*(1 - 2*t2 + Power(t2,3)) - 
            t1*Power(t2,5)*(34 + 3*(-21 + 2*s - 4*s1)*t2 + 
               (-16 - 15*s + 18*s1)*Power(t2,2) + 
               (18 + 3*s + 10*s1)*Power(t2,3) + 
               (10 + 5*s - 12*s1)*Power(t2,4) + 
               (15 + s - 4*s1)*Power(t2,5) + 8*Power(t2,6)) + 
            Power(t1,2)*Power(t2,3)*
             (42 - (71 + 12*s + 6*s1)*t2 + 
               (-376 + 12*Power(s,2) + 89*s1 - 84*Power(s1,2) + 
                  3*s*(-37 + 24*s1))*Power(t2,2) + 
               (11 + 16*Power(s,3) + 245*s1 + 120*Power(s1,2) + 
                  Power(s,2)*(-43 + 6*s1) - 
                  2*s*(89 + 35*s1 + 6*Power(s1,2)))*Power(t2,3) + 
               (129 - 9*Power(s,3) - 350*s1 + 14*Power(s1,2) + 
                  Power(s,2)*(-147 + 23*s1) + 
                  s*(365 - 40*s1 + 14*Power(s1,2)))*Power(t2,4) + 
               (311 - 20*Power(s,3) + Power(s,2)*(47 - 12*s1) - 
                  81*s1 - 38*Power(s1,2) + 
                  s*(135 - 22*s1 + 2*Power(s1,2)))*Power(t2,5) + 
               (191 + 44*Power(s,2) + 49*s1 - 12*Power(s1,2) + 
                  4*s*(-7 + 3*s1))*Power(t2,6) + 
               (9 - 21*s + 12*s1)*Power(t2,7)) - 
            Power(t1,9)*(486 + 12*Power(s,2) + 12*Power(s1,2) + 
               1315*t2 + 44*Power(t2,2) + s1*(204 + 325*t2) - 
               s*(240 + 24*s1 + 325*t2)) + 
            Power(t1,5)*(14 + 3*(-7 + 6*Power(s,2))*t2 - 
               (3139 + 712*s - 57*Power(s,2) + 30*Power(s,3))*
                Power(t2,2) + 
               (-4210 - 5615*s + 778*Power(s,2) + 88*Power(s,3))*
                Power(t2,3) + 
               (5360 - 4103*s - 1728*Power(s,2) + 326*Power(s,3))*
                Power(t2,4) + 
               (5148 + 5439*s - 2442*Power(s,2) + 164*Power(s,3))*
                Power(t2,5) + 
               (-1305 + 2685*s - 419*Power(s,2))*Power(t2,6) + 
               (-232 + 66*s)*Power(t2,7) + 
               Power(s1,3)*(96 - 560*t2 - 972*Power(t2,2) + 
                  688*Power(t2,3) + 441*Power(t2,4) + 27*Power(t2,5)) + 
               Power(s1,2)*(-192 + (34 - 64*s)*t2 + 
                  (1985 + 726*s)*Power(t2,2) + 
                  (4590 - 634*s)*Power(t2,3) - 
                  22*(-93 + 43*s)*Power(t2,4) + 
                  (-94 + 49*s)*Power(t2,5) - 189*Power(t2,6)) - 
               s1*(32 + 2*(-1229 + 94*s)*t2 + 
                  6*(-74 - 231*s + 46*Power(s,2))*Power(t2,2) + 
                  (10937 + 2184*s - 490*Power(s,2))*Power(t2,3) + 
                  (6855 + 2636*s - 346*Power(s,2))*Power(t2,4) + 
                  (2569 - 1294*s + 240*Power(s,2))*Power(t2,5) + 
                  (1496 - 558*s)*Power(t2,6) + 24*Power(t2,7))) - 
            Power(t1,3)*Power(t2,2)*
             (86 - (547 + 64*s + 6*Power(s,2))*t2 + 
               (-1228 - 498*s + 77*Power(s,2) + 22*Power(s,3))*
                Power(t2,2) + 
               (1268 + 797*s - 540*Power(s,2) + 7*Power(s,3))*
                Power(t2,3) + 
               (1708 + 2082*s - 404*Power(s,2) - 70*Power(s,3))*
                Power(t2,4) + 
               (1045 + 381*s + 452*Power(s,2) - 97*Power(s,3))*
                Power(t2,5) + 
               (51 - 494*s + 211*Power(s,2))*Power(t2,6) + 
               (37 - 83*s)*Power(t2,7) - 
               2*Power(s1,3)*Power(t2,2)*
                (52 - 63*t2 - Power(t2,2) + 12*Power(t2,3)) + 
               Power(s1,2)*t2*
                (-402 + (867 - 6*s)*t2 + (541 + 19*s)*Power(t2,2) + 
                  (-830 + 32*s)*Power(t2,3) + 
                  (-262 + 36*s)*Power(t2,4) + 2*Power(t2,5)) + 
               s1*(48 + 2*(131 + 90*s)*t2 - 
                  2*(-130 + 191*s + 6*Power(s,2))*Power(t2,2) + 
                  (-1558 - 764*s + 75*Power(s,2))*Power(t2,3) + 
                  (-699 + 232*s - 52*Power(s,2))*Power(t2,4) + 
                  (979 + 126*s + 15*Power(s,2))*Power(t2,5) + 
                  (307 - 62*s)*Power(t2,6) + 41*Power(t2,7))) + 
            Power(t1,4)*t2*(30 - (1065 + 104*s + 18*Power(s,2))*t2 + 
               (246 + 32*s + 95*Power(s,2) + 54*Power(s,3))*
                Power(t2,2) + 
               (5400 + 3872*s - 1218*Power(s,2) - 70*Power(s,3))*
                Power(t2,3) + 
               (2043 + 4835*s + 149*Power(s,2) - 221*Power(s,3))*
                Power(t2,4) - 
               2*(648 + 535*s - 747*Power(s,2) + 91*Power(s,3))*
                Power(t2,5) + 
               (-90 - 1747*s + 415*Power(s,2))*Power(t2,6) - 
               2*(-80 + 59*s)*Power(t2,7) + 
               Power(s1,3)*t2*
                (-520 + 816*t2 + 165*Power(t2,2) - 279*Power(t2,3) - 
                  44*Power(t2,4)) + 
               Power(s1,2)*(-48 + 2*(551 + 24*s)*t2 + 
                  (1797 - 386*s)*Power(t2,2) + 
                  2*(-1445 + 77*s)*Power(t2,3) + 
                  4*(-519 + 98*s)*Power(t2,4) + 
                  (-293 + 42*s)*Power(t2,5) + 92*Power(t2,6)) + 
               s1*(176 + (-462 + 412*s)*t2 + 
                  (-3892 - 2074*s + 68*Power(s,2))*Power(t2,2) + 
                  (97 - 188*s - 20*Power(s,2))*Power(t2,3) + 
                  (4598 + 1492*s - 289*Power(s,2))*Power(t2,4) + 
                  2*(1087 + 14*s + 75*Power(s,2))*Power(t2,5) + 
                  (895 - 350*s)*Power(t2,6) + 52*Power(t2,7))) + 
            Power(t1,8)*(-1080 - 1894*t2 + 4010*Power(t2,2) + 
               795*Power(t2,3) - 20*Power(t2,4) + 
               Power(s,3)*(4 + 7*t2) - Power(s1,3)*(32 + 7*t2) + 
               Power(s1,2)*(-190 - 105*t2 + 202*Power(t2,2)) + 
               s1*(-452 - 501*t2 + 1960*Power(t2,2) + 
                  110*Power(t2,3)) + 
               Power(s,2)*(66 + 363*t2 + 202*Power(t2,2) - 
                  s1*(40 + 21*t2)) + 
               s*(-216 - 2025*t2 - 2242*Power(t2,2) - 110*Power(t2,3) + 
                  Power(s1,2)*(68 + 21*t2) + 
                  s1*(44 - 258*t2 - 404*Power(t2,2)))) + 
            Power(t1,7)*(128 + 2520*t2 + 9279*Power(t2,2) - 
               3057*Power(t2,3) - 2469*Power(t2,4) + 13*Power(t2,5) + 
               Power(s1,3)*(48 + 239*t2 + 146*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(s,3)*(12 - 17*t2 + 44*Power(t2,2) + 
                  11*Power(t2,3)) - 
               2*Power(s1,2)*
                (-4 + 35*t2 - 566*Power(t2,2) + 333*Power(t2,3) + 
                  23*Power(t2,4)) + 
               s1*(-1112 - 2695*t2 + 1504*Power(t2,2) - 
                  3716*Power(t2,3) - 660*Power(t2,4) + 5*Power(t2,5)) + 
               s*(-188 - 291*t2 + 4084*Power(t2,2) + 
                  6180*Power(t2,3) + 776*Power(t2,4) - 5*Power(t2,5) + 
                  Power(s1,2)*
                   (-4 - 551*t2 - 248*Power(t2,2) + 33*Power(t2,3)) + 
                  4*s1*(-26 - 263*t2 - 8*Power(t2,2) + 
                     423*Power(t2,3) + 23*Power(t2,4))) - 
               Power(s,2)*(s1*
                   (104 - 329*t2 - 58*Power(t2,2) + 33*Power(t2,3)) + 
                  2*(-32 + 171*t2 + 748*Power(t2,2) + 513*Power(t2,3) + 
                     23*Power(t2,4)))) + 
            Power(t1,6)*(164 + 1368*t2 - 1307*Power(t2,2) - 
               12639*Power(t2,3) - 2804*Power(t2,4) + 2988*Power(t2,5) + 
               115*Power(t2,6) - 
               7*Power(s,3)*t2*
                (2 + 2*t2 + 31*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s1,3)*(192 - 344*t2 - 418*Power(t2,2) - 
                  413*Power(t2,3) + 10*Power(t2,4)) + 
               Power(s1,2)*(-254 - 2059*t2 - 1673*Power(t2,2) - 
                  1739*Power(t2,3) + 627*Power(t2,4) + 157*Power(t2,5)) \
+ Power(s,2)*(-6 + (-151 + 300*s1)*t2 + (219 - 690*s1)*Power(t2,2) + 
                  (2467 - 169*s1)*Power(t2,3) + 
                  (2177 + 150*s1)*Power(t2,4) + 217*Power(t2,5)) + 
               s1*(-168 + 4215*t2 + 10846*Power(t2,2) + 
                  1677*Power(t2,3) + 3470*Power(t2,4) + 
                  1405*Power(t2,5) - 4*Power(t2,6)) + 
               s*(52 + 487*t2 + 3210*Power(t2,2) - 1311*Power(t2,3) - 
                  8381*Power(t2,4) - 2069*Power(t2,5) - 5*Power(t2,6) + 
                  Power(s1,2)*
                   (16 - 342*t2 + 994*Power(t2,2) + 799*Power(t2,3) - 
                     90*Power(t2,4)) + 
                  s1*(-44 + 338*t2 + 2686*Power(t2,2) + 
                     1682*Power(t2,3) - 2462*Power(t2,4) - 
                     374*Power(t2,5))))) + 
         s2*t1*(-8*Power(t1,9)*(-46 + 15*s - 15*s1 - 9*t2) - 
            4*Power(t2,6)*(1 - 2*t2 + Power(t2,3)) + 
            t1*Power(t2,4)*(64 + 3*(-41 + 18*s - 12*s1)*t2 + 
               (-7 + 42*Power(s,2) + 64*s1 - s*(119 + 12*s1))*
                Power(t2,2) + 
               (35 - 49*Power(s,2) + 14*Power(s,3) + 4*s1 + 
                  s*(-22 + 26*s1))*Power(t2,3) + 
               (Power(s,3) + s*(95 - 12*s1) - 7*(1 + 4*s1) + 
                  Power(s,2)*(-63 + 10*s1))*Power(t2,4) - 
               2*(-13 + 5*Power(s,3) + Power(s,2)*(-10 + s1) + 
                  s*(-1 + s1) + 2*s1)*Power(t2,5) + 
               2*(9 - 5*s + 4*Power(s,2))*Power(t2,6)) + 
            Power(t1,2)*Power(t2,3)*
             (-56 + 609*t2 - 137*Power(t2,2) - 157*Power(t2,3) - 
               271*Power(t2,4) - 180*Power(t2,5) - 26*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*
                (-32 - 45*t2 + 34*Power(t2,2) + 51*Power(t2,3)) + 
               4*Power(s1,2)*t2*
                (48 - 74*t2 + Power(t2,2) + 21*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s,2)*t2*
                (24 + (31 + 54*s1)*t2 + (-554 + 27*s1)*Power(t2,2) - 
                  (190 + 27*s1)*Power(t2,3) + 
                  (173 + 28*s1)*Power(t2,4) + 36*Power(t2,5)) - 
               2*s1*(42 - 28*t2 + 205*Power(t2,2) - 246*Power(t2,3) - 
                  68*Power(t2,4) + 60*Power(t2,5) + 14*Power(t2,6)) + 
               s*(60 - 6*(-3 + 32*s1)*t2 + 
                  (694 + 282*s1 + 36*Power(s1,2))*Power(t2,2) + 
                  (-641 + 54*s1 - 54*Power(s1,2))*Power(t2,3) + 
                  (-542 - 76*s1 + 8*Power(s1,2))*Power(t2,4) + 
                  (61 + 18*s1 + 2*Power(s1,2))*Power(t2,5) + 
                  10*(8 + s1)*Power(t2,6))) + 
            Power(t1,8)*(960 - 716*t2 - 671*Power(t2,2) + 
               16*Power(t2,3) - 88*Power(s1,2)*(-1 + 2*t2) - 
               8*Power(s,2)*(15 + 22*t2) + 
               s1*(428 - 860*t2 - 115*Power(t2,2)) + 
               s*(348 + 1076*t2 + 115*Power(t2,2) + 32*s1*(1 + 11*t2))) \
+ Power(t1,7)*(-72 - 3564*t2 - 1028*Power(t2,2) + 1796*Power(t2,3) - 
               5*Power(t2,4) + 
               Power(s1,3)*(-84 - 140*t2 + 27*Power(t2,2)) - 
               Power(s,3)*(20 + 60*t2 + 27*Power(t2,2)) + 
               Power(s1,2)*(200 - 740*t2 + 463*Power(t2,2) + 
                  98*Power(t2,3)) + 
               s1*(988 - 480*t2 + 1567*Power(t2,2) + 714*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s,2)*(88 + 572*t2 + 859*Power(t2,2) + 
                  98*Power(t2,3) + s1*(-92 - 20*t2 + 81*Power(t2,2))) + 
               s*(220 - 224*t2 - 3137*Power(t2,2) - 850*Power(t2,3) + 
                  10*Power(t2,4) + 
                  Power(s1,2)*(196 + 220*t2 - 81*Power(t2,2)) - 
                  2*s1*(-152 - 196*t2 + 661*Power(t2,2) + 
                     98*Power(t2,3)))) + 
            Power(t1,4)*t2*(344 + 719*t2 - 4159*Power(t2,2) - 
               907*Power(t2,3) + 1598*Power(t2,4) + 313*Power(t2,5) - 
               156*Power(t2,6) + 
               2*Power(s,3)*t2*
                (-18 + 52*t2 + 47*Power(t2,2) + 184*Power(t2,3) + 
                  53*Power(t2,4)) + 
               Power(s1,3)*(544 - 708*t2 - 370*Power(t2,2) + 
                  309*Power(t2,3) + 119*Power(t2,4) - 18*Power(t2,5)) + 
               Power(s1,2)*(-912 - 1168*t2 + 1619*Power(t2,2) + 
                  1976*Power(t2,3) + 482*Power(t2,4) - 
                  215*Power(t2,5) - 8*Power(t2,6)) - 
               s1*(-4 - 3572*t2 + 756*Power(t2,2) + 3131*Power(t2,3) + 
                  1748*Power(t2,4) + 1057*Power(t2,5) + 124*Power(t2,6)\
) + s*(180 + 2*(-19 + 568*s1 + 74*Power(s1,2))*t2 + 
                  2*(-947 + 891*s1 + 58*Power(s1,2))*Power(t2,2) - 
                  (6216 + 2002*s1 + 401*Power(s1,2))*Power(t2,3) - 
                  2*(426 + 320*s1 + 119*Power(s1,2))*Power(t2,4) + 
                  (2033 + 798*s1 + 111*Power(s1,2))*Power(t2,5) + 
                  54*(5 + s1)*Power(t2,6)) + 
               Power(s,2)*t2*
                (24 + 1415*t2 + 1934*Power(t2,2) - 1108*Power(t2,3) - 
                  973*Power(t2,4) - 56*Power(t2,5) + 
                  s1*(180 - 394*t2 + 446*Power(t2,2) - 
                     145*Power(t2,3) - 199*Power(t2,4)))) + 
            Power(t1,3)*Power(t2,2)*
             (-200 - 1533*t2 + 1567*Power(t2,2) + 1262*Power(t2,3) + 
               497*Power(t2,4) + 2*Power(t2,5) + 39*Power(t2,6) + 
               Power(s,3)*t2*
                (12 + 36*t2 + 32*Power(t2,2) - 185*Power(t2,3) - 
                  104*Power(t2,4)) + 
               2*Power(s1,3)*t2*
                (-98 + 131*t2 - 9*Power(t2,2) - 26*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s1,2)*(-384 + 720*t2 + 940*Power(t2,2) - 
                  1031*Power(t2,3) - 367*Power(t2,4) + 
                  36*Power(t2,5) + 2*Power(t2,6)) + 
               s1*(588 - 540*t2 - 933*Power(t2,2) - 788*Power(t2,3) + 
                  957*Power(t2,4) + 442*Power(t2,5) + 98*Power(t2,6)) - 
               s*(180 - 2*(-171 + 8*s1 + 42*Power(s1,2))*t2 + 
                  2*(154 + 799*s1 + 72*Power(s1,2))*Power(t2,2) - 
                  2*(1543 + 248*s1 + 50*Power(s1,2))*Power(t2,3) - 
                  (1281 + 462*s1 + 107*Power(s1,2))*Power(t2,4) + 
                  (640 + 228*s1 + 42*Power(s1,2))*Power(t2,5) + 
                  (220 + 38*s1)*Power(t2,6)) + 
               Power(s,2)*t2*
                (40 - 579*t2 - 1660*Power(t2,2) + 89*Power(t2,3) + 
                  571*Power(t2,4) + 64*Power(t2,5) + 
                  s1*(-60 + 270*t2 - 88*Power(t2,2) - 66*Power(t2,3) + 
                     129*Power(t2,4)))) + 
            Power(t1,5)*(40 + 764*t2 + 2848*Power(t2,2) - 
               2957*Power(t2,3) - 4770*Power(t2,4) + 604*Power(t2,5) + 
               236*Power(t2,6) - 
               Power(s,3)*t2*
                (-36 + 208*t2 + 234*Power(t2,2) + 349*Power(t2,3) + 
                  54*Power(t2,4)) + 
               Power(s1,3)*(128 + 980*t2 - 418*Power(t2,2) - 
                  392*Power(t2,3) - 104*Power(t2,4) + 25*Power(t2,5)) + 
               Power(s1,2)*(-144 - 632*t2 - 2815*Power(t2,2) - 
                  1595*Power(t2,3) - 250*Power(t2,4) + 
                  379*Power(t2,5) + 10*Power(t2,6)) + 
               s1*(-508 - 1752*t2 + 6507*Power(t2,2) + 
                  4982*Power(t2,3) + 1084*Power(t2,4) + 
                  1720*Power(t2,5) + 56*Power(t2,6)) - 
               s*(60 + 12*(-26 + 60*s1 + 35*Power(s1,2))*t2 - 
                  (2943 + 98*s1 + 204*Power(s1,2))*Power(t2,2) - 
                  2*(3150 + 1333*s1 + 471*Power(s1,2))*Power(t2,3) + 
                  (1791 + 360*s1 - 42*Power(s1,2))*Power(t2,4) + 
                  2*(1510 + 585*s1 + 52*Power(s1,2))*Power(t2,5) + 
                  2*(70 + 17*s1)*Power(t2,6)) + 
               Power(s,2)*t2*
                (-72 - 1091*t2 - 527*Power(t2,2) + 2011*Power(t2,3) + 
                  923*Power(t2,4) + 24*Power(t2,5) + 
                  s1*(-180 + 102*t2 - 540*Power(t2,2) + 
                     411*Power(t2,3) + 133*Power(t2,4)))) + 
            Power(t1,6)*(-336 - 132*t2 + 5340*Power(t2,2) + 
               4333*Power(t2,3) - 1962*Power(t2,4) - 122*Power(t2,5) + 
               Power(s1,3)*(20 + 184*t2 + 379*Power(t2,2) - 
                  2*Power(t2,3) - 11*Power(t2,4)) + 
               Power(s,3)*(-12 + 120*t2 + 199*Power(t2,2) + 
                  158*Power(t2,3) + 11*Power(t2,4)) - 
               2*Power(s1,2)*
                (-264 - 590*t2 - 532*Power(t2,2) + 109*Power(t2,3) + 
                  157*Power(t2,4) + 2*Power(t2,5)) + 
               s1*(-868 - 5232*t2 - 1687*Power(t2,2) - 
                  1204*Power(t2,3) - 1580*Power(t2,4) + 8*Power(t2,5)) + 
               s*(-4 - 1536*t2 - 2631*Power(t2,2) + 3990*Power(t2,3) + 
                  2299*Power(t2,4) + 10*Power(t2,5) + 
                  Power(s1,2)*
                   (188 - 408*t2 - 807*Power(t2,2) + 162*Power(t2,3) + 
                     33*Power(t2,4)) + 
                  4*s1*(-60 - 214*t2 - 416*Power(t2,2) + 
                     399*Power(t2,3) + 195*Power(t2,4) + 2*Power(t2,5))) \
+ Power(s,2)*(s1*(60 + 168*t2 + 229*Power(t2,2) - 318*Power(t2,3) - 
                     33*Power(t2,4)) - 
                  2*(-16 - 78*t2 + 352*Power(t2,2) + 901*Power(t2,3) + 
                     233*Power(t2,4) + 2*Power(t2,5))))) + 
         Power(t1,2)*(-32*Power(t1,9) - 
            2*Power(t2,5)*(-3 + (7 - 6*s)*t2 + 
               (-2 + 9*s - 6*Power(s,2))*Power(t2,2) + 
               (-5 + 3*s + 3*Power(s,2) - 2*Power(s,3))*Power(t2,3) - 
               (-4 + s)*Power(-1 + s,2)*Power(t2,4) + 
               Power(-1 + s,3)*Power(t2,5)) + 
            t1*Power(t2,3)*(-48 + (94 - 84*s + 12*s1)*t2 + 
               (7 - 78*Power(s,2) - 16*s1 - 4*s*(-29 + 3*s1))*
                Power(t2,2) + 
               (-61 - 26*Power(s,3) + Power(s,2)*(13 - 12*s1) - 6*s1 + 
                  s*(103 + 10*s1))*Power(t2,3) + 
               (19 - 29*Power(s,3) + Power(s,2)*(123 - 4*s1) + 
                  24*s*(-5 + s1) - 8*s1)*Power(t2,4) + 
               (1 + 4*Power(s,3) + 24*s1 + 2*Power(s,2)*(8 + 7*s1) - 
                  s*(43 + 34*s1))*Power(t2,5) + 
               2*(-7 + 5*Power(s,3) - 3*s1 + 2*s*(7 + 3*s1) - 
                  Power(s,2)*(16 + 3*s1))*Power(t2,6)) + 
            4*Power(t1,8)*(-52 + 12*Power(s,2) + 12*Power(s1,2) + 
               52*t2 - Power(t2,2) + 10*s1*(2 + t2) - 
               2*s*(18 + 12*s1 + 5*t2)) + 
            Power(t1,4)*(-64 + 4*(-110 - 27*s + 36*Power(s,2))*t2 + 
               2*(700 - 62*s - 249*Power(s,2) + 42*Power(s,3))*
                Power(t2,2) + 
               (216 + 2737*s - 1877*Power(s,2) + 302*Power(s,3))*
                Power(t2,3) - 
               (665 - 1033*s + 87*Power(s,2) + 86*Power(s,3))*
                Power(t2,4) - 
               2*(76 + 396*s - 297*Power(s,2) + 68*Power(s,3))*
                Power(t2,5) - 
               2*(-26 + 76*s - 31*Power(s,2) + 5*Power(s,3))*
                Power(t2,6) + 
               Power(s1,3)*(-192 + 192*t2 + 196*Power(t2,2) - 
                  88*Power(t2,3) - 79*Power(t2,4) + 6*Power(t2,5) + 
                  4*Power(t2,6)) + 
               Power(s1,2)*(384 + 64*(1 + 2*s)*t2 - 
                  2*(85 + 146*s)*Power(t2,2) + 
                  (-545 + 54*s)*Power(t2,3) + 
                  7*(-53 + 46*s)*Power(t2,4) + 
                  (123 - 93*s)*Power(t2,5) - 2*(-7 + 9*s)*Power(t2,6)) + 
               s1*(64 - 4*(355 + 32*s)*t2 + 
                  4*(172 - 433*s + 135*Power(s,2))*Power(t2,2) + 
                  (817 + 998*s - 412*Power(s,2))*Power(t2,3) + 
                  (348 + 898*s - 245*Power(s,2))*Power(t2,4) + 
                  (408 - 484*s + 223*Power(s,2))*Power(t2,5) + 
                  6*(12 - 11*s + 4*Power(s,2))*Power(t2,6))) + 
            Power(t1,2)*Power(t2,2)*
             (64 + 2*(-187 + 62*s + 24*Power(s,2))*t2 + 
               2*(75 - 328*s + 109*Power(s,2) + 14*Power(s,3))*
                Power(t2,2) + 
               (132 + 205*s - 419*Power(s,2) + 126*Power(s,3))*
                Power(t2,3) + 
               (27 + 545*s - 417*Power(s,2) + 66*Power(s,3))*
                Power(t2,4) + 
               (50 - 11*s + 105*Power(s,2) - 51*Power(s,3))*
                Power(t2,5) + 
               (19 - 77*s + 72*Power(s,2) - 20*Power(s,3))*Power(t2,6) - 
               2*Power(s1,2)*t2*
                (48 + (-73 + 6*s)*t2 + (3 - 11*s)*Power(t2,2) + 
                  (11 + 8*s)*Power(t2,3) + (14 - 8*s)*Power(t2,4) + 
                  3*(-1 + s)*Power(t2,5)) + 
               s1*(96 + 4*(-37 + 48*s)*t2 + 
                  2*(89 - 118*s + 42*Power(s,2))*Power(t2,2) + 
                  (-99 - 170*s + 40*Power(s,2))*Power(t2,3) + 
                  (-100 + 158*s - 75*Power(s,2))*Power(t2,4) + 
                  (31 + 54*s - 8*Power(s,2))*Power(t2,5) + 
                  (28 - 46*s + 24*Power(s,2))*Power(t2,6))) + 
            Power(t1,3)*t2*(48 + (682 + 100*s - 144*Power(s,2))*t2 - 
               (759 - 848*s + 10*Power(s,2) + 84*Power(s,3))*
                Power(t2,2) - 
               (391 + 1435*s - 1355*Power(s,2) + 266*Power(s,3))*
                Power(t2,3) - 
               (3 + 1031*s - 507*Power(s,2) + 32*Power(s,3))*
                Power(t2,4) + 
               2*(10 + 139*s - 192*Power(s,2) + 62*Power(s,3))*
                Power(t2,5) + 
               (-17 + 143*s - 88*Power(s,2) + 20*Power(s,3))*
                Power(t2,6) - 
               2*Power(s1,3)*t2*
                (-48 + 66*t2 - 7*Power(t2,2) - 10*Power(t2,3) - 
                  2*Power(t2,4) + Power(t2,5)) + 
               Power(s1,2)*(96 - 32*(4 + 3*s)*t2 + 
                  2*(-221 + 66*s)*Power(t2,2) + 
                  (317 + 4*s)*Power(t2,3) + (197 - 101*s)*Power(t2,4) + 
                  2*Power(t2,5) + 2*(-7 + 9*s)*Power(t2,6)) - 
               s1*(352 + 4*(-157 + 80*s)*t2 + 
                  2*(-3 - 614*s + 174*Power(s,2))*Power(t2,2) - 
                  2*(76 - 35*s + 38*Power(s,2))*Power(t2,3) + 
                  (155 + 672*s - 261*Power(s,2))*Power(t2,4) + 
                  (188 - 130*s + 105*Power(s,2))*Power(t2,5) + 
                  (63 - 74*s + 36*Power(s,2))*Power(t2,6))) + 
            Power(t1,7)*(192 - 16*Power(s1,3)*(-4 + t2) + 880*t2 + 
               16*Power(s,3)*t2 - 486*Power(t2,2) - Power(t2,3) - 
               4*Power(s1,2)*(-28 + 18*t2 + 13*Power(t2,2)) + 
               s1*(-208 - 116*t2 - 256*Power(t2,2) + 5*Power(t2,3)) - 
               4*Power(s,2)*(-4 + 54*t2 + 13*Power(t2,2) + 
                  4*s1*(-4 + 3*t2)) + 
               s*(-304 + 476*t2 + 308*Power(t2,2) - 5*Power(t2,3) + 
                  16*Power(s1,2)*(-8 + 3*t2) + 
                  8*s1*(-20 + 36*t2 + 13*Power(t2,2)))) - 
            Power(t1,6)*(-80 + 632*t2 + 1620*Power(t2,2) - 
               478*Power(t2,3) - 43*Power(t2,4) + 
               Power(s1,3)*t2*(116 + 28*t2 - 15*Power(t2,2)) + 
               Power(s,3)*t2*(-44 + 80*t2 + 15*Power(t2,2)) + 
               Power(s1,2)*(336 + 232*t2 + 86*Power(t2,2) - 
                  157*Power(t2,3) - 4*Power(t2,4)) + 
               s1*(-992 - 764*t2 + 92*Power(t2,2) - 589*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s,2)*(-176 - 344*t2 + 426*Power(t2,2) + 
                  249*Power(t2,3) + 4*Power(t2,4) + 
                  s1*(96 - 316*t2 + 132*Power(t2,2) + 45*Power(t2,3))) + 
               s*(-192 - 1404*t2 + 424*Power(t2,2) + 847*Power(t2,3) + 
                  5*Power(t2,4) + 
                  Power(s1,2)*
                   (96 - 388*t2 + 24*Power(t2,2) + 45*Power(t2,3)) + 
                  2*s1*(64 - 360*t2 + 82*Power(t2,2) + 203*Power(t2,3) + 
                     4*Power(t2,4)))) + 
            Power(t1,5)*(32 - 864*t2 + 540*Power(t2,2) + 
               1560*Power(t2,3) - 79*Power(t2,4) - 80*Power(t2,5) + 
               Power(s,3)*t2*(-28 - 180*t2 + 141*Power(t2,2) + 
                  72*Power(t2,3) + 2*Power(t2,4)) + 
               Power(s1,2)*(208 + 496*t2 + 468*Power(t2,2) + 
                  294*Power(t2,3) - 202*Power(t2,4) - 10*Power(t2,5)) + 
               Power(s1,3)*(-288 + 68*t2 + 72*Power(t2,2) + 
                  109*Power(t2,3) - 25*Power(t2,4) - 2*Power(t2,5)) - 
               s1*(-832 + 1764*t2 + 1432*Power(t2,2) - 43*Power(t2,3) + 
                  648*Power(t2,4) + 32*Power(t2,5)) - 
               Power(s,2)*(48 + 4*(-136 + 93*s1)*t2 - 
                  4*(311 + 140*s1)*Power(t2,2) + 
                  21*(18 + s1)*Power(t2,3) + 
                  (516 + 169*s1)*Power(t2,4) + 6*(4 + s1)*Power(t2,5)) + 
               s*(-32 - 388*t2 - 2692*Power(t2,2) - 329*Power(t2,3) + 
                  1129*Power(t2,4) + 74*Power(t2,5) + 
                  Power(s1,2)*
                   (-32 + 268*t2 - 340*Power(t2,2) - 229*Power(t2,3) + 
                     122*Power(t2,4) + 6*Power(t2,5)) + 
                  2*s1*(128 + 440*t2 - 664*Power(t2,2) - 
                     218*Power(t2,3) + 318*Power(t2,4) + 17*Power(t2,5))))\
))*R1(t1))/((-1 + t1)*t1*(s - s2 + t1)*Power(t1 - t2,3)*(1 - s2 + t1 - t2)*
       (s - s1 + t2)*Power(t1 - s2*t2,2)*
       Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - Power(t2,2),2)) - 
    (8*(Power(t1,8)*(1 - 5*t2 + 4*s2*t2 + 13*Power(t2,2) + 
            3*Power(t2,3) - 4*s2*Power(t2,3) + 
            s*(-1 + 3*t2 + 7*Power(t2,2) + 3*Power(t2,3)) - 
            s1*(-1 + 3*t2 + 7*Power(t2,2) + 3*Power(t2,3))) + 
         Power(t1,7)*(2 - 3*s2 - 18*t2 + 31*s2*t2 - 8*Power(s2,2)*t2 + 
            57*Power(t2,2) - 74*s2*Power(t2,2) - 
            16*Power(s2,2)*Power(t2,2) - 80*Power(t2,3) - 
            17*s2*Power(t2,3) + 16*Power(s2,2)*Power(t2,3) + 
            3*Power(t2,4) + 3*s2*Power(t2,4) + 
            8*Power(s2,2)*Power(t2,4) + 
            4*Power(s,2)*t2*(1 + 2*t2 + 3*Power(t2,2)) + 
            Power(s1,2)*(2 - 2*t2 - 6*Power(t2,2) + 6*Power(t2,3)) + 
            s1*(3 - 8*t2 + 17*Power(t2,2) + 14*Power(t2,3) + 
               10*Power(t2,4) + 
               s2*(-2 + 17*t2 + 6*Power(t2,2) + 33*Power(t2,3) + 
                  6*Power(t2,4))) - 
            s*(1 + 3*t2 - 16*Power(t2,2) + Power(t2,3) + 
               11*Power(t2,4) + 
               2*s1*(1 + t2 + Power(t2,2) + 9*Power(t2,3)) + 
               s2*(-1 + 2*t2 - Power(t2,2) + 42*Power(t2,3) + 
                  6*Power(t2,4)))) + 
         Power(t2,4)*(-(Power(s1,3)*Power(s2,2)*
               (s2*(-3 + t2) + 6*Power(-1 + t2,2))*Power(-1 + t2,2)) + 
            2*Power(-1 + t2,2)*Power(1 + (-1 + s)*t2,3)*
             (-2 - t2 + Power(t2,2)) - 
            8*Power(s2,6)*t2*
             (-1 + 3*t2 - 4*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s2,5)*(-12 + (15 - 8*s)*t2 + 
               (46 + 24*s)*Power(t2,2) - (127 + 40*s)*Power(t2,3) + 
               8*(16 + 3*s)*Power(t2,4) - 38*Power(t2,5)) + 
            Power(s2,4)*(3 - 7*(9 + s)*t2 + 118*Power(t2,2) + 
               (25 + 69*s + 8*Power(s,2))*Power(t2,3) - 
               (201 + 158*s + 8*Power(s,2))*Power(t2,4) + 
               2*(73 + 30*s)*Power(t2,5) - 28*Power(t2,6)) - 
            Power(s2,3)*(-16 + (47 + 2*s)*t2 + 
               (42 + 19*s - 2*Power(s,2))*Power(t2,2) + 
               (-179 - 86*s + 15*Power(s,2))*Power(t2,3) + 
               (73 - 27*s - 48*Power(s,2))*Power(t2,4) + 
               (90 + 140*s + 23*Power(s,2))*Power(t2,5) - 
               3*(21 + 16*s)*Power(t2,6) + 6*Power(t2,7)) + 
            Power(s1,2)*Power(s2,2)*(-1 + t2)*
             (2*Power(s2,2)*(3 - 4*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               s2*t2*(25 + (-38 + 4*s)*t2 + 9*Power(t2,2) + 
                  4*Power(t2,3)) + 
               (-1 + t2)*(15 - (16 + 7*s)*t2 + 
                  3*(-11 + 7*s)*Power(t2,2) + 32*Power(t2,3) + 
                  2*Power(t2,4))) - 
            s2*(-1 + t2)*t2*(Power(-1 + t2,3)*
                (-11 - 7*t2 + 2*Power(t2,2)) - 
               Power(s,3)*Power(t2,2)*(-2 + t2 + 3*Power(t2,2)) - 
               s*Power(-1 + t2,2)*
                (-14 - 39*t2 - 11*Power(t2,2) + 10*Power(t2,3)) + 
               Power(s,2)*t2*
                (16 + 11*t2 - 26*Power(t2,2) - 9*Power(t2,3) + 
                  8*Power(t2,4))) + 
            Power(s2,2)*t2*(Power(s,3)*Power(t2,2)*
                (1 + 10*t2 + Power(t2,2)) + 
               Power(-1 + t2,3)*
                (-35 + 25*t2 + 56*Power(t2,2) + 20*Power(t2,3)) + 
               s*Power(-1 + t2,2)*
                (8 - 17*t2 - 32*Power(t2,2) + 33*Power(t2,3) + 
                  12*Power(t2,4)) - 
               Power(s,2)*t2*
                (1 + 39*t2 + 20*Power(t2,2) - 83*Power(t2,3) + 
                  23*Power(t2,4))) + 
            s1*s2*(Power(s2,4)*
                (5 - 14*t2 + 3*Power(t2,2) + 4*Power(t2,3) - 
                  10*Power(t2,4)) + 
               2*Power(-1 + t2,2)*
                (6 + 2*(-7 + 3*s)*t2 + (8 - 13*s)*Power(t2,2) + 
                  (2 + 6*s - 5*Power(s,2))*Power(t2,3) + 
                  (-2 + s + Power(s,2))*Power(t2,4)) + 
               Power(s2,3)*(22 + (-25 + 6*s)*t2 - 
                  6*(7 + s)*Power(t2,2) + (43 + 22*s)*Power(t2,3) + 
                  2*(12 + s)*Power(t2,4) - 22*Power(t2,5)) + 
               Power(s2,2)*(-28 + (139 + 8*s)*t2 - 
                  3*(52 + 8*s + Power(s,2))*Power(t2,2) - 
                  2*(14 - 2*s + 5*Power(s,2))*Power(t2,3) + 
                  (94 + 8*s + Power(s,2))*Power(t2,4) + 
                  (-7 + 4*s)*Power(t2,5) - 14*Power(t2,6)) - 
               s2*(-1 + t2)*(-20 - (1 + 12*s)*t2 + 
                  (119 + 4*s - 2*Power(s,2))*Power(t2,2) + 
                  (-103 - 38*s + 39*Power(s,2))*Power(t2,3) - 
                  3*(15 - 16*s + Power(s,2))*Power(t2,4) - 
                  2*(-24 + s)*Power(t2,5) + 2*Power(t2,6)))) - 
         Power(t1,2)*Power(t2,2)*
          (7 - 81*t2 + 11*s*t2 + 213*Power(t2,2) - 157*s*Power(t2,2) + 
            44*Power(s,2)*Power(t2,2) - 126*Power(t2,3) + 
            330*s*Power(t2,3) - 199*Power(s,2)*Power(t2,3) + 
            42*Power(s,3)*Power(t2,3) + 
            24*Power(s2,6)*(-1 + t2)*Power(t2,3) - 185*Power(t2,4) - 
            94*s*Power(t2,4) + 248*Power(s,2)*Power(t2,4) - 
            72*Power(s,3)*Power(t2,4) + 249*Power(t2,5) - 
            297*s*Power(t2,5) + 75*Power(s,2)*Power(t2,5) - 
            16*Power(s,3)*Power(t2,5) - 67*Power(t2,6) + 
            275*s*Power(t2,6) - 240*Power(s,2)*Power(t2,6) + 
            54*Power(s,3)*Power(t2,6) - 10*Power(t2,7) - 
            68*s*Power(t2,7) + 72*Power(s,2)*Power(t2,7) - 
            20*Power(s,3)*Power(t2,7) + 
            Power(s2,5)*t2*(-20 + 48*t2 + 3*(-9 + 16*s)*Power(t2,2) - 
               2*(83 + 24*s)*Power(t2,3) + 129*Power(t2,4)) + 
            Power(s2,3)*(17 + 2*(53 + 12*s)*t2 + 
               (-200 + 47*s - 6*Power(s,2))*Power(t2,2) + 
               (-323 - 524*s + 21*Power(s,2))*Power(t2,3) + 
               (1012 + 724*s - 132*Power(s,2))*Power(t2,4) + 
               (-893 + 400*s + 153*Power(s,2))*Power(t2,5) - 
               7*(-17 + 41*s)*Power(t2,6) + 42*Power(t2,7)) - 
            Power(s2,2)*(12 - 7*(14 + s)*t2 + 
               (-289 - 102*s + 9*Power(s,2))*Power(t2,2) + 
               (959 + 333*s - 181*Power(s,2) + 12*Power(s,3))*
                Power(t2,3) + 
               (-524 + 777*s + 94*Power(s,2) + 36*Power(s,3))*
                Power(t2,4) + 
               (-344 - 1541*s + 291*Power(s,2) + 24*Power(s,3))*
                Power(t2,5) + 
               (337 + 241*s - 177*Power(s,2))*Power(t2,6) + 
               (-53 + 83*s)*Power(t2,7)) + 
            s2*(-15 - 8*(9 + 5*s)*t2 + 
               (530 + 66*s - 59*Power(s,2))*Power(t2,2) + 
               (-571 + 176*s + 173*Power(s,2) - 20*Power(s,3))*
                Power(t2,3) - 
               (388 + 685*s - 376*Power(s,2) + 62*Power(s,3))*
                Power(t2,4) + 
               (757 + 364*s - 575*Power(s,2) + 20*Power(s,3))*
                Power(t2,5) - 
               (179 - 275*s + 15*Power(s,2) + 34*Power(s,3))*
                Power(t2,6) + 2*(-31 - 78*s + 32*Power(s,2))*Power(t2,7)\
) + Power(s2,4)*t2*(32 - 187*t2 + 435*Power(t2,2) + 
               24*Power(s,2)*(-1 + t2)*Power(t2,2) - 436*Power(t2,3) - 
               177*Power(t2,4) + 153*Power(t2,5) + 
               s*(19 - 44*t2 + 30*Power(t2,2) + 320*Power(t2,3) - 
                  253*Power(t2,4))) + 
            Power(s1,3)*Power(-1 + t2,2)*
             (6*Power(-1 + t2,2) + 
               Power(s2,3)*(-6 - t2 + Power(t2,2)) + 
               Power(s2,2)*(18 - 23*t2 - 17*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s2*(-15 + 15*t2 + 2*Power(t2,2) - 12*Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s1,2)*(-1 + t2)*
             (Power(s2,4)*(2 - 4*t2 + 8*Power(t2,2) + 6*Power(t2,3)) + 
               Power(s2,3)*(16 + (6 + 4*s)*t2 - 
                  5*(9 + 4*s)*Power(t2,2) + 2*(7 + 2*s)*Power(t2,3) + 
                  57*Power(t2,4)) + 
               Power(s2,2)*(-32 + (71 - 28*s)*t2 + 
                  (-47 + 62*s)*Power(t2,2) - 
                  4*(6 + 13*s)*Power(t2,3) + (11 - 18*s)*Power(t2,4) + 
                  45*Power(t2,5)) - 
               (-1 + t2)*(27 - 7*(6 + s)*t2 + 
                  13*(-1 + s)*Power(t2,2) + 2*(8 + 9*s)*Power(t2,3) - 
                  2*(-9 + 8*s)*Power(t2,4) + 6*(-1 + s)*Power(t2,5)) + 
               s2*(-4 + 10*(-12 + s)*t2 + (166 + 21*s)*Power(t2,2) + 
                  (57 - 21*s)*Power(t2,3) - (134 + s)*Power(t2,4) + 
                  (33 - 21*s)*Power(t2,5) + 2*Power(t2,6))) + 
            s1*(Power(s2,5)*t2*
                (-8 + 51*t2 - 42*Power(t2,2) + 35*Power(t2,3)) + 
               Power(s2,4)*(1 + (13 + 6*s)*t2 - 
                  3*(17 + 18*s)*Power(t2,2) + (296 + 6*s)*Power(t2,3) - 
                  2*(116 + 15*s)*Power(t2,4) + 153*Power(t2,5)) + 
               Power(s2,3)*(-4 + (31 + 8*s)*t2 + 
                  (-165 - 22*s + 9*Power(s,2))*Power(t2,2) + 
                  3*(93 - 78*s + 10*Power(s,2))*Power(t2,3) + 
                  (127 + 142*s - 3*Power(s,2))*Power(t2,4) - 
                  2*(141 + 91*s)*Power(t2,5) + 134*Power(t2,6)) + 
               (-1 + t2)*(-20 + (28 - 26*s)*t2 + 
                  (65 + 70*s - 40*Power(s,2))*Power(t2,2) + 
                  (-131 - 54*s + 62*Power(s,2))*Power(t2,3) + 
                  (73 - 108*s + 59*Power(s,2))*Power(t2,4) + 
                  (-33 + 164*s - 71*Power(s,2))*Power(t2,5) + 
                  2*(9 - 23*s + 12*Power(s,2))*Power(t2,6)) + 
               Power(s2,2)*(-35 + 2*(-31 + 5*s)*t2 + 
                  (220 + 126*s + 21*Power(s,2))*Power(t2,2) - 
                  6*(69 + 11*s + 7*Power(s,2))*Power(t2,3) + 
                  (724 - 358*s + 93*Power(s,2))*Power(t2,4) + 
                  (-511 + 312*s + 36*Power(s,2))*Power(t2,5) + 
                  (59 - 168*s)*Power(t2,6) + 19*Power(t2,7)) - 
               s2*(-27 + (127 + 92*s)*t2 + 
                  (31 - 356*s + 40*Power(s,2))*Power(t2,2) + 
                  (-336 + 26*s + 68*Power(s,2))*Power(t2,3) + 
                  (121 + 624*s - 129*Power(s,2))*Power(t2,4) + 
                  (137 - 396*s + 30*Power(s,2))*Power(t2,5) - 
                  (21 + 28*s + 45*Power(s,2))*Power(t2,6) + 
                  (-32 + 38*s)*Power(t2,7)))) + 
         Power(t1,3)*t2*(2 - 63*t2 - 5*s*t2 + 225*Power(t2,2) - 
            80*s*Power(t2,2) + 24*Power(s,2)*Power(t2,2) - 
            110*Power(t2,3) + 251*s*Power(t2,3) - 
            141*Power(s,2)*Power(t2,3) + 46*Power(s,3)*Power(t2,3) + 
            8*Power(s2,6)*(-1 + t2)*Power(t2,3) - 367*Power(t2,4) - 
            195*s*Power(t2,4) + 368*Power(s,2)*Power(t2,4) - 
            110*Power(s,3)*Power(t2,4) + 438*Power(t2,5) - 
            215*s*Power(t2,5) - 105*Power(s,2)*Power(t2,5) - 
            10*Power(s,3)*Power(t2,5) - 96*Power(t2,6) + 
            355*s*Power(t2,6) - 246*Power(s,2)*Power(t2,6) + 
            46*Power(s,3)*Power(t2,6) - 29*Power(t2,7) - 
            111*s*Power(t2,7) + 88*Power(s,2)*Power(t2,7) - 
            20*Power(s,3)*Power(t2,7) + 
            Power(s2,5)*Power(t2,2)*
             (8 + (-65 + 16*s)*t2 - 2*(13 + 8*s)*Power(t2,2) + 
               71*Power(t2,3)) + 
            Power(s2,4)*t2*(-14 - (15 + 4*s)*t2 + 
               (233 + 78*s - 8*Power(s,2))*Power(t2,2) + 
               (-568 + 84*s + 8*Power(s,2))*Power(t2,3) + 
               (63 - 158*s)*Power(t2,4) + 121*Power(t2,5)) - 
            Power(s2,2)*(-1 - (71 + 3*s)*t2 + 
               (-134 - 89*s + 11*Power(s,2))*Power(t2,2) + 
               (811 + 462*s - 111*Power(s,2) + 10*Power(s,3))*
                Power(t2,3) + 
               2*(-554 + 175*s + 65*Power(s,2) + 8*Power(s,3))*
                Power(t2,4) + 
               (496 - 1761*s + 29*Power(s,2) + 22*Power(s,3))*
                Power(t2,5) + 
               (263 + 399*s - 167*Power(s,2))*Power(t2,6) + 
               2*(-68 + 33*s)*Power(t2,7)) + 
            s2*(-3 - (21 + 4*s)*t2 + 
               (391 + 38*s - 31*Power(s,2))*Power(t2,2) + 
               (-525 + 61*s + 165*Power(s,2) - 26*Power(s,3))*
                Power(t2,3) - 
               (323 + 1124*s - 332*Power(s,2) + 84*Power(s,3))*
                Power(t2,4) + 
               (839 + 1079*s - 667*Power(s,2) + 2*Power(s,3))*
                Power(t2,5) + 
               (-393 + 294*s + 109*Power(s,2) - 36*Power(s,3))*
                Power(t2,6) + (35 - 200*s + 56*Power(s,2))*Power(t2,7)) \
+ Power(s1,3)*Power(-1 + t2,2)*
             (-8 + Power(s2,3)*(-3 + t2) + 11*t2 - 3*Power(t2,2) - 
               4*Power(t2,4) + 2*Power(t2,5) + 
               Power(s2,2)*(5 - 29*t2 + 6*Power(t2,3)) + 
               s2*(6 + 22*t2 - 48*Power(t2,2) - 9*Power(t2,3) + 
                  11*Power(t2,4))) + 
            Power(s2,3)*t2*(46 - 174*t2 + 147*Power(t2,2) + 
               405*Power(t2,3) - 1119*Power(t2,4) + 301*Power(t2,5) + 
               34*Power(t2,6) + 
               Power(s,2)*t2*
                (-2 - 9*t2 - 36*Power(t2,2) + 107*Power(t2,3)) + 
               s*(13 - 4*t2 - 240*Power(t2,2) + 760*Power(t2,3) + 
                  7*Power(t2,4) - 248*Power(t2,5))) + 
            Power(s1,2)*(-1 + t2)*
             (-2 - (35 + 9*s)*t2 + (49 + 40*s)*Power(t2,2) + 
               2*(-5 + 4*s)*Power(t2,3) + (29 - 82*s)*Power(t2,4) + 
               (-45 + 57*s)*Power(t2,5) - 2*(-7 + 9*s)*Power(t2,6) + 
               2*Power(s2,4)*t2*(1 - t2 + 2*Power(t2,2)) + 
               Power(s2,3)*(4 - 15*t2 + (38 - 4*s)*Power(t2,2) - 
                  25*Power(t2,3) + 46*Power(t2,4)) + 
               Power(s2,2)*(-1 + (64 - 19*s)*t2 + 
                  (-120 + 7*s)*Power(t2,2) + 3*(11 + s)*Power(t2,3) + 
                  (25 - 27*s)*Power(t2,4) + 71*Power(t2,5)) + 
               s2*(-1 + (-37 + 21*s)*t2 + (67 - 17*s)*Power(t2,2) - 
                  (28 + 29*s)*Power(t2,3) + (-58 + 37*s)*Power(t2,4) + 
                  (65 - 48*s)*Power(t2,5) + 8*Power(t2,6))) + 
            s1*(10 + (-59 + 4*s)*t2 + 
               (20 - 54*s + 48*Power(s,2))*Power(t2,2) + 
               (154 + 258*s - 190*Power(s,2))*Power(t2,3) + 
               (-216 - 200*s + 85*Power(s,2))*Power(t2,4) + 
               (155 - 228*s + 156*Power(s,2))*Power(t2,5) + 
               (-94 + 294*s - 123*Power(s,2))*Power(t2,6) + 
               (30 - 74*s + 36*Power(s,2))*Power(t2,7) + 
               3*Power(s2,5)*Power(t2,2)*(3 - 2*t2 + 3*Power(t2,2)) + 
               Power(s2,4)*t2*
                (-12 + (51 - 12*s)*t2 - 4*(-31 + s)*Power(t2,2) - 
                  (101 + 8*s)*Power(t2,3) + 118*Power(t2,4)) + 
               Power(s2,3)*(-1 + 12*(4 + s)*t2 + 
                  (-204 - 62*s + 3*Power(s,2))*Power(t2,2) + 
                  (483 - 178*s + 10*Power(s,2))*Power(t2,3) - 
                  (38 - 86*s + Power(s,2))*Power(t2,4) - 
                  (95 + 146*s)*Power(t2,5) + 167*Power(t2,6)) + 
               Power(s2,2)*(-1 + 3*(9 + 8*s)*t2 + 
                  (-61 + 54*s + 17*Power(s,2))*Power(t2,2) + 
                  (-411 - 222*s + 40*Power(s,2))*Power(t2,3) + 
                  (1222 - 182*s + 9*Power(s,2))*Power(t2,4) + 
                  2*(-415 + 59*s + 21*Power(s,2))*Power(t2,5) - 
                  4*(-37 + 56*s)*Power(t2,6) + 26*Power(t2,7)) - 
               s2*(8 + (89 + 54*s)*t2 + 
                  (-250 - 288*s + 48*Power(s,2))*Power(t2,2) + 
                  (497 - 88*s + 38*Power(s,2))*Power(t2,3) + 
                  (-581 + 818*s - 185*Power(s,2))*Power(t2,4) + 
                  (96 - 540*s + 64*Power(s,2))*Power(t2,5) + 
                  (199 + 86*s - 73*Power(s,2))*Power(t2,6) + 
                  (-58 + 54*s)*Power(t2,7)))) + 
         t1*Power(t2,3)*(8*Power(s2,6)*t2*
             (-1 + 3*t2 - 6*Power(t2,2) + 4*Power(t2,3)) - 
            (-1 + t2)*(1 + (-1 + s)*t2)*
             (10 + (-35 - 2*s1 + 6*s*(3 + 2*s1))*t2 + 
               (23 + 20*Power(s,2) - 4*s1 - 4*s*(15 + 2*s1))*
                Power(t2,2) + 
               (25 + 6*s - 9*Power(s,2) + 20*s1 - 18*s*s1)*
                Power(t2,3) + 
               (-29 - 19*Power(s,2) - 20*s1 + s*(58 + 20*s1))*
                Power(t2,4) + 
               2*(3 - 11*s + 5*Power(s,2) + 3*s1 - 3*s*s1)*Power(t2,5)) \
+ Power(s2,4)*(27 + (25 + 26*s)*t2 - 4*(57 + 10*s)*Power(t2,2) + 
               (231 - 86*s - 24*Power(s,2))*Power(t2,3) + 
               (75 + 392*s + 24*Power(s,2))*Power(t2,4) - 
               2*(145 + 98*s)*Power(t2,5) + 100*Power(t2,6) + 
               2*Power(s1,2)*
                (-4 + 9*t2 - 6*Power(t2,2) - 5*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s1*(-9 + 20*t2 - (31 + 36*s)*Power(t2,2) - 
                  2*(-83 + 6*s)*Power(t2,3) - 
                  6*(29 + 4*s)*Power(t2,4) + 88*Power(t2,5))) - 
            Power(s2,3)*(11 - (161 + 13*s)*t2 + 
               (121 - 62*s + 6*Power(s,2))*Power(t2,2) + 
               (439 + 383*s - 37*Power(s,2))*Power(t2,3) + 
               (-657 - 208*s + 140*Power(s,2))*Power(t2,4) + 
               (203 - 426*s - 97*Power(s,2))*Power(t2,5) + 
               (69 + 182*s)*Power(t2,6) - 25*Power(t2,7) + 
               Power(s1,3)*Power(-1 + t2,2)*t2*(1 + 5*t2) - 
               Power(s1,2)*(-1 + t2)*
                (-12 + (26 + 4*s)*t2 - (17 + 20*s)*Power(t2,2) + 
                  2*(-5 + 2*s)*Power(t2,3) + 29*Power(t2,4)) + 
               s1*(53 + 12*(-3 + s)*t2 - 
                  (72 + 46*s + 9*Power(s,2))*Power(t2,2) + 
                  (55 + 110*s - 30*Power(s,2))*Power(t2,3) + 
                  (-98 - 58*s + 3*Power(s,2))*Power(t2,4) + 
                  (157 + 78*s)*Power(t2,5) - 59*Power(t2,6))) + 
            s2*(-1 + 12*Power(s1,3)*Power(-1 + t2,4) - (82 + 51*s)*t2 + 
               (295 + 56*s - 51*Power(s,2))*Power(t2,2) + 
               (-240 + 158*s + 68*Power(s,2) - 9*Power(s,3))*
                Power(t2,3) - 
               (183 + 246*s - 196*Power(s,2) + 12*Power(s,3))*
                Power(t2,4) + 
               (290 - 13*s - 200*Power(s,2) + 13*Power(s,3))*
                Power(t2,5) - 
               (47 - 158*s + 49*Power(s,2) + 16*Power(s,3))*
                Power(t2,6) + 
               (-32 - 62*s + 36*Power(s,2))*Power(t2,7) - 
               2*Power(s1,2)*Power(-1 + t2,2)*
                (27 - (44 + s)*t2 + (-17 + 8*s)*Power(t2,2) + 
                  6*(6 + s)*Power(t2,3) + (-2 + s)*Power(t2,4)) - 
               s1*(-1 + t2)*(10 + (75 - 46*s)*t2 - 
                  2*(140 - 65*s + 6*Power(s,2))*Power(t2,2) + 
                  (198 + 20*s - 23*Power(s,2))*Power(t2,3) - 
                  2*(-43 + 54*s + 13*Power(s,2))*Power(t2,4) - 
                  (81 + 6*s + 7*Power(s,2))*Power(t2,5) + 
                  2*(-4 + 5*s)*Power(t2,6))) + 
            Power(s2,2)*(-31 - 3*(-10 + s)*t2 + 
               (357 + 69*s - Power(s,2))*Power(t2,2) - 
               (640 + 83*s - 137*Power(s,2) + 6*Power(s,3))*
                Power(t2,3) + 
               (42 - 469*s + 8*Power(s,2) - 32*Power(s,3))*
                Power(t2,4) + 
               (417 + 617*s - 279*Power(s,2) - 10*Power(s,3))*
                Power(t2,5) + 
               (-148 - 80*s + 99*Power(s,2))*Power(t2,6) - 
               3*(9 + 17*s)*Power(t2,7) - 
               Power(s1,3)*Power(-1 + t2,2)*
                (6 + 5*t2 - 9*Power(t2,2) + 4*Power(t2,3)) + 
               Power(s1,2)*(-1 + t2)*
                (-32 - (8 + 23*s)*t2 + (59 + 79*s)*Power(t2,2) + 
                  (22 - 65*s)*Power(t2,3) - (53 + 3*s)*Power(t2,4) + 
                  12*Power(t2,5)) + 
               s1*(69 + (-309 + 4*s)*t2 + 
                  (249 + 74*s + 11*Power(s,2))*Power(t2,2) + 
                  (180 + 36*s - 96*Power(s,2))*Power(t2,3) + 
                  (-119 - 288*s + 115*Power(s,2))*Power(t2,4) + 
                  (-159 + 224*s + 6*Power(s,2))*Power(t2,5) + 
                  (81 - 50*s)*Power(t2,6) + 8*Power(t2,7))) + 
            Power(s2,5)*(s1*(1 - 6*t2 + 39*Power(t2,2) - 
                  28*Power(t2,3) + 30*Power(t2,4)) + 
               t2*(-5 - 18*t2 + 121*Power(t2,2) - 244*Power(t2,3) + 
                  110*Power(t2,4) - 
                  8*s*(-1 + 3*t2 - 9*Power(t2,2) + 7*Power(t2,3))))) - 
         Power(t1,4)*(1 - 21*t2 + s*t2 + 99*Power(t2,2) - 
            20*s*Power(t2,2) + 8*Power(s,2)*Power(t2,2) + 
            20*Power(t2,3) + 92*s*Power(t2,3) - 
            49*Power(s,2)*Power(t2,3) + 26*Power(s,3)*Power(t2,3) - 
            391*Power(t2,4) - 285*s*Power(t2,4) + 
            290*Power(s,2)*Power(t2,4) - 98*Power(s,3)*Power(t2,4) + 
            438*Power(t2,5) + 6*s*Power(t2,5) - 
            207*Power(s,2)*Power(t2,5) - 4*Power(s,3)*Power(t2,5) - 
            153*Power(t2,6) + 359*s*Power(t2,6) - 
            116*Power(s,2)*Power(t2,6) + 14*Power(s,3)*Power(t2,6) + 
            7*Power(t2,7) - 117*s*Power(t2,7) + 
            62*Power(s,2)*Power(t2,7) - 10*Power(s,3)*Power(t2,7) + 
            20*Power(s2,5)*Power(t2,3)*(-1 + Power(t2,2)) + 
            Power(s2,4)*Power(t2,2)*
             (14 + (-22 + 31*s)*t2 - 2*(106 + s)*Power(t2,2) + 
               (104 - 41*s)*Power(t2,3) + 56*Power(t2,4)) + 
            s2*(-1 + (3 - 2*s)*t2 + 
               (164 + 27*s - 9*Power(s,2))*Power(t2,2) - 
               2*(158 + 24*s - 31*Power(s,2) + 9*Power(s,3))*
                Power(t2,3) + 
               (68 - 900*s + 145*Power(s,2) - 45*Power(s,3))*
                Power(t2,4) + 
               (365 + 1298*s - 296*Power(s,2) - 14*Power(s,3))*
                Power(t2,5) + 
               (-489 + 149*s + 182*Power(s,2) - 19*Power(s,3))*
                Power(t2,6) + 2*(73 - 70*s + 12*Power(s,2))*Power(t2,7)) \
+ Power(s2,3)*Power(t2,2)*(-73 + 290*t2 - 280*Power(t2,2) - 
               578*Power(t2,3) + 265*Power(t2,4) + 16*Power(t2,5) + 
               4*Power(s,2)*t2*(-2 + t2 + 7*Power(t2,2)) + 
               s*(-8 + 2*t2 + 311*Power(t2,2) - 182*Power(t2,3) - 
                  123*Power(t2,4))) + 
            Power(s1,3)*Power(-1 + t2,2)*
             (-2 + 20*t2 - 28*Power(t2,2) + 7*Power(t2,3) - 
               7*Power(t2,4) + 4*Power(t2,5) + 
               Power(s2,2)*(-1 - 5*t2 - 4*Power(t2,2) + 
                  4*Power(t2,3)) + 
               s2*(3 - 7*t2 - 13*Power(t2,2) - 12*Power(t2,3) + 
                  11*Power(t2,4))) + 
            Power(s2,2)*t2*(16 + 21*t2 - 347*Power(t2,2) + 
               987*Power(t2,3) - 1105*Power(t2,4) - 36*Power(t2,5) + 
               104*Power(t2,6) - 
               Power(s,3)*Power(t2,2)*(3 + 2*t2 + 7*Power(t2,2)) + 
               2*Power(s,2)*t2*
                (-2 + 16*t2 - 23*Power(t2,2) + 57*Power(t2,3) + 
                  42*Power(t2,4)) + 
               s*(1 + 19*t2 - 242*Power(t2,2) + 190*Power(t2,3) + 
                  907*Power(t2,4) - 413*Power(t2,5) - 30*Power(t2,6))) + 
            Power(s1,2)*(-1 + t2)*
             (1 - (12 + s)*t2 + 27*(1 + s)*Power(t2,2) - 
               3*(16 + 3*s)*Power(t2,3) + (44 - 53*s)*Power(t2,4) + 
               (-22 + 42*s)*Power(t2,5) - 2*(-7 + 9*s)*Power(t2,6) + 
               2*Power(s2,3)*Power(t2,2)*(5 - 5*t2 + 8*Power(t2,2)) + 
               Power(s2,2)*(2 - (4 + 7*s)*t2 - (7 + 4*s)*Power(t2,2) + 
                  (28 + 11*s)*Power(t2,3) + (1 - 12*s)*Power(t2,4) + 
                  52*Power(t2,5)) + 
               s2*(-3 + (26 + 8*s)*t2 - (31 + 33*s)*Power(t2,2) - 
                  (22 + 7*s)*Power(t2,3) + (-20 + 37*s)*Power(t2,4) + 
                  (88 - 41*s)*Power(t2,5) + 10*Power(t2,6))) + 
            s1*(2 - 10*(4 + s)*t2 + 
               2*(45 + 5*s + 12*Power(s,2))*Power(t2,2) + 
               (-95 + 222*s - 158*Power(s,2))*Power(t2,3) + 
               (-51 - 304*s + 137*Power(s,2))*Power(t2,4) + 
               (260 - 54*s + 68*Power(s,2))*Power(t2,5) + 
               (-205 + 178*s - 59*Power(s,2))*Power(t2,6) + 
               3*(13 - 22*s + 8*Power(s,2))*Power(t2,7) + 
               Power(s2,4)*Power(t2,2)*
                (17 + 13*t2 + 5*Power(t2,2) + 25*Power(t2,3)) + 
               Power(s2,2)*(-1 + (25 + 6*s)*t2 + 
                  (-56 + 10*s + 5*Power(s,2))*Power(t2,2) + 
                  (-127 - 216*s + 27*Power(s,2))*Power(t2,3) + 
                  (798 - 44*s - 11*Power(s,2))*Power(t2,4) + 
                  (-586 - 54*s + 15*Power(s,2))*Power(t2,5) + 
                  (287 - 134*s)*Power(t2,6) + 20*Power(t2,7)) - 
               s2*(1 + t2 - 4*s*t2 + 
                  4*(-27 - 20*s + 6*Power(s,2))*Power(t2,2) + 
                  (623 - 64*s - 34*Power(s,2))*Power(t2,3) + 
                  (-909 + 514*s - 81*Power(s,2))*Power(t2,4) + 
                  (210 - 342*s + 32*Power(s,2))*Power(t2,5) + 
                  (182 + 230*s - 49*Power(s,2))*Power(t2,6) + 
                  (-60 + 34*s)*Power(t2,7)) + 
               Power(s2,3)*Power(t2,2)*
                (-44 + 319*t2 - 121*Power(t2,2) + 109*Power(t2,3) + 
                  97*Power(t2,4) - 
                  2*s*(9 + 25*t2 - 5*Power(t2,2) + 19*Power(t2,3))))) + 
         Power(t1,6)*(-5*s2 + 2*Power(s2,2) - 20*t2 + 61*s2*t2 - 
            30*Power(s2,2)*t2 + 4*Power(s2,3)*t2 + 75*Power(t2,2) - 
            194*s2*Power(t2,2) + 23*Power(s2,2)*Power(t2,2) + 
            32*Power(s2,3)*Power(t2,2) - 162*Power(t2,3) + 
            203*s2*Power(t2,3) + 201*Power(s2,2)*Power(t2,3) + 
            179*Power(t2,4) + 137*s2*Power(t2,4) - 
            61*Power(s2,2)*Power(t2,4) - 32*Power(s2,3)*Power(t2,4) - 
            36*Power(t2,5) - 22*s2*Power(t2,5) - 
            15*Power(s2,2)*Power(t2,5) - 4*Power(s2,3)*Power(t2,5) + 
            2*Power(s,3)*Power(t2,2)*(4 + t2 + Power(t2,2)) + 
            Power(s1,3)*(1 + t2 - 7*Power(t2,2) + 7*Power(t2,3) - 
               2*Power(t2,4)) - 
            Power(s1,2)*(-1 + t2)*
             (5 - 16*t2 + Power(t2,2) + 18*Power(t2,3) + 
               4*Power(t2,4) + 
               2*s2*(-2 + 4*t2 - 3*Power(t2,2) + 9*Power(t2,3))) - 
            2*Power(s,2)*t2*(-3 + 13*t2 + 18*Power(t2,3) + 
               2*Power(t2,4) + 
               s1*(-2 + 8*t2 - 3*Power(t2,2) + 3*Power(t2,3)) + 
               s2*(2 + 5*t2 + 14*Power(t2,2) + 15*Power(t2,3))) - 
            s1*(-1 + 3*t2 - 46*Power(t2,2) + 119*Power(t2,3) - 
               59*Power(t2,4) + 20*Power(t2,5) + 
               Power(s2,2)*(-1 + 21*t2 + 24*Power(t2,2) + 
                  28*Power(t2,3) + 45*Power(t2,4) + 3*Power(t2,5)) + 
               s2*(7 - 49*t2 + 170*Power(t2,2) - 93*Power(t2,3) + 
                  125*Power(t2,4) + 20*Power(t2,5))) + 
            s*(1 - (4 - 12*s2 + Power(s2,2))*t2 + 
               (86 + 7*s2 - 23*Power(s2,2))*Power(t2,2) + 
               2*(-45 - 83*s2 + 15*Power(s2,2))*Power(t2,3) + 
               (-95 + 125*s2 + 63*Power(s2,2))*Power(t2,4) + 
               (30 + 22*s2 + 3*Power(s2,2))*Power(t2,5) + 
               Power(s1,2)*(-1 - 5*t2 + 15*Power(t2,2) - 
                  15*Power(t2,3) + 6*Power(t2,4)) + 
               2*s1*(-1 - 3*t2 + 11*Power(t2,2) + Power(t2,3) + 
                  24*Power(t2,4) + 4*Power(t2,5) + 
                  s2*(1 + 5*t2 + 19*Power(t2,2) - Power(t2,3) + 
                     24*Power(t2,4))))) + 
         Power(t1,5)*(-2 - s2 + 2*Power(s2,2) + 12*t2 + 50*s2*t2 - 
            29*Power(s2,2)*t2 + 4*Power(s2,3)*t2 + 68*Power(t2,2) - 
            192*s2*Power(t2,2) + 15*Power(s2,2)*Power(t2,2) + 
            52*Power(s2,3)*Power(t2,2) - 16*Power(s2,4)*Power(t2,2) - 
            231*Power(t2,3) + 331*s2*Power(t2,3) + 
            302*Power(s2,2)*Power(t2,3) - 217*Power(s2,3)*Power(t2,3) - 
            32*Power(s2,4)*Power(t2,3) + 296*Power(t2,4) - 
            111*s2*Power(t2,4) - 772*Power(s2,2)*Power(t2,4) - 
            89*Power(s2,3)*Power(t2,4) + 32*Power(s2,4)*Power(t2,4) - 
            207*Power(t2,5) - 363*s2*Power(t2,5) + 
            87*Power(s2,2)*Power(t2,5) + 121*Power(s2,3)*Power(t2,5) + 
            16*Power(s2,4)*Power(t2,5) + 52*Power(t2,6) + 
            106*s2*Power(t2,6) + 35*Power(s2,2)*Power(t2,6) + 
            9*Power(s2,3)*Power(t2,6) + 
            Power(s1,3)*Power(-1 + t2,2)*
             (3 - 8*t2 - 2*Power(t2,2) - Power(t2,3) + 2*Power(t2,4) + 
               s2*(-2 - 5*t2 - 3*Power(t2,2) + 4*Power(t2,3))) - 
            Power(s,3)*Power(t2,2)*
             (-6 + 45*t2 + 4*Power(t2,2) + 3*Power(t2,3) + 
               2*Power(t2,4) + 
               s2*(5 + 8*t2 + 7*Power(t2,2) + 4*Power(t2,3))) + 
            Power(s1,2)*(-1 + t2)*
             (-4 + 30*t2 - 65*Power(t2,2) + 30*Power(t2,3) + 
               11*Power(t2,4) + 10*Power(t2,5) + 
               2*Power(s2,2)*
                (-1 + 4*t2 - 2*Power(t2,2) + 5*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s2*(7 - 32*t2 + 43*Power(t2,2) - 44*Power(t2,3) + 
                  70*Power(t2,4) + 4*Power(t2,5))) + 
            s1*(-3 + 32*t2 - 103*Power(t2,2) - 2*Power(t2,3) + 
               250*Power(t2,4) - 196*Power(t2,5) + 34*Power(t2,6) + 
               Power(s2,3)*t2*
                (7 + 42*t2 + 2*Power(t2,2) + 50*Power(t2,3) + 
                  19*Power(t2,4)) + 
               Power(s2,2)*(4 - 37*t2 + 101*Power(t2,2) + 
                  193*Power(t2,3) - 119*Power(t2,4) + 208*Power(t2,5) + 
                  10*Power(t2,6)) + 
               s2*(-4 + 57*t2 - 283*Power(t2,2) + 528*Power(t2,3) - 
                  219*Power(t2,4) + 61*Power(t2,5) + 40*Power(t2,6))) + 
            Power(s,2)*t2*(2 - 16*t2 + 119*Power(t2,2) - 
               102*Power(t2,3) + 9*Power(t2,4) + 24*Power(t2,5) + 
               2*Power(s2,2)*t2*
                (2 + t2 + 24*Power(t2,2) + 9*Power(t2,3)) + 
               s2*(-2 + 11*t2 + 40*Power(t2,2) + 7*Power(t2,3) + 
                  120*Power(t2,4) + 4*Power(t2,5)) + 
               s1*(4 - 54*t2 + 81*Power(t2,2) - 2*Power(t2,3) + 
                  Power(t2,4) + 6*Power(t2,5) + 
                  s2*(-4 + 25*t2 + 6*Power(t2,2) - 3*Power(t2,3) + 
                     12*Power(t2,4)))) - 
            s*(-1 - t2 - 16*Power(t2,2) + 242*Power(t2,3) - 
               133*Power(t2,4) - 261*Power(t2,5) + 74*Power(t2,6) + 
               Power(s2,3)*Power(t2,2)*
                (-15 - 40*t2 + 75*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s2,2)*t2*
                (4 + 39*t2 - 175*Power(t2,2) - 126*Power(t2,3) + 
                  247*Power(t2,4) + 11*Power(t2,5)) + 
               s2*(1 - 2*t2 + 40*Power(t2,2) + 280*Power(t2,3) - 
                  737*Power(t2,4) + 70*Power(t2,5) + 60*Power(t2,6)) + 
               Power(s1,2)*(-1 + t2)*
                (-1 - 6*t2 + 18*Power(t2,2) - 4*Power(t2,3) - 
                  Power(t2,4) + 6*Power(t2,5) + 
                  s2*(1 + 13*t2 - 5*Power(t2,2) - 9*Power(t2,3) + 
                     12*Power(t2,4))) + 
               2*s1*t2*(-2 - 44*t2 + 71*Power(t2,2) - 3*Power(t2,3) - 
                  3*Power(t2,4) + 17*Power(t2,5) + 
                  Power(s2,2)*
                   (4 + 27*t2 + 9*Power(t2,2) + 17*Power(t2,3) + 
                     15*Power(t2,4)) + 
                  s2*(-10 + 13*t2 + 91*Power(t2,2) - 45*Power(t2,3) + 
                     91*Power(t2,4) + 4*Power(t2,5))))))*R1(t2))/
     ((-1 + t1)*(s - s2 + t1)*Power(t1 - t2,3)*(1 - s2 + t1 - t2)*
       Power(-1 + t2,3)*t2*(s - s1 + t2)*Power(t1 - s2*t2,2)) + 
    (8*(-32*Power(t1,7) + 12*Power(s2,9)*Power(t2,2) - 
         2*Power(t2,3)*Power(1 + (-1 + s)*t2,3)*(-2 - t2 + Power(t2,2)) + 
         t1*t2*(1 + (-1 + s)*t2)*
          (-28 + (44 - 56*s)*t2 + 
            (21 - 28*Power(s,2) + s*(34 - 12*s1) + 2*s1)*Power(t2,2) + 
            (-22*Power(s,2) + s*(86 - 4*s1) + 6*(-7 + s1))*Power(t2,3) + 
            (3 + 9*Power(s,2) - 14*s1 + 2*s*(-4 + 7*s1))*Power(t2,4) + 
            2*(1 + 3*Power(s,2) + 3*s1 - s*(7 + 3*s1))*Power(t2,5)) - 
         2*Power(s2,8)*t2*(2*Power(s1,2) - s1*t2 + 
            (8 + 9*s - 35*t2)*t2 + 6*t1*(2 + 3*t2)) + 
         4*Power(t1,6)*(-32 + 12*Power(s,2) + 12*Power(s1,2) + 34*t2 - 
            Power(t2,2) + 2*s1*(6 + 5*t2) - 2*s*(14 + 12*s1 + 5*t2)) + 
         Power(t1,2)*(-32 - 80*t2 + 220*Power(t2,2) + 
            2*Power(s1,3)*(2 - 3*t2)*Power(t2,2) - 34*Power(t2,3) - 
            40*Power(t2,4) - 26*Power(t2,5) + 8*Power(t2,6) + 
            Power(s,3)*Power(t2,2)*
             (40 + 78*t2 + 17*Power(t2,2) - 39*Power(t2,3) - 
               6*Power(t2,4)) + 
            Power(s1,2)*t2*(-36 + 56*t2 - 15*Power(t2,2) + 
               31*Power(t2,3) - 22*Power(t2,4) + 6*Power(t2,5)) + 
            Power(s,2)*t2*(108 + (-8 + 84*s1)*t2 + 
               (-391 + 30*s1)*Power(t2,2) - (193 + 86*s1)*Power(t2,3) + 
               (108 + 23*s1)*Power(t2,4) + 2*(13 + 6*s1)*Power(t2,5)) + 
            s1*(72 - 140*t2 + 42*Power(t2,2) + 85*Power(t2,3) - 
               119*Power(t2,4) + 16*Power(t2,5) + 10*Power(t2,6)) - 
            s*(-72 - 4*(-29 + 30*s1)*t2 + 2*(189 + 80*s1)*Power(t2,2) + 
               (-319 + 146*s1 - 10*Power(s1,2))*Power(t2,3) + 
               (-264 - 222*s1 + 19*Power(s1,2))*Power(t2,4) + 
               (41 + 25*s1 - 16*Power(s1,2))*Power(t2,5) + 
               (34 + 22*s1 + 6*Power(s1,2))*Power(t2,6))) + 
         Power(t1,5)*(48 - 16*Power(s1,3)*(-3 + t2) + 204*t2 - 
            168*Power(t2,2) - 13*Power(t2,3) + 16*Power(s,3)*(1 + t2) - 
            4*Power(s1,2)*(8 - 8*t2 + 13*Power(t2,2)) + 
            s1*(424 + 68*t2 - 150*Power(t2,2) + 5*Power(t2,3)) + 
            s*(-600 + 124*t2 + 202*Power(t2,2) - 5*Power(t2,3) + 
               16*Power(s1,2)*(-5 + 3*t2) + 8*s1*t2*(10 + 13*t2)) - 
            4*Power(s,2)*(4*s1*(-1 + 3*t2) + t2*(28 + 13*t2))) + 
         Power(s2,7)*(-(Power(s1,3)*(2 + t2)) + 
            t1*t2*(32 + 36*s - 204*t2 + 64*s*t2 - 167*Power(t2,2)) + 
            12*Power(t1,2)*(1 + 6*t2 + 3*Power(t2,2)) + 
            Power(t2,2)*(-20 + 6*Power(s,2) - 77*t2 - 107*s*t2 + 
               170*Power(t2,2)) + 
            2*Power(s1,2)*((3 + s - 12*t2)*t2 + t1*(2 + 7*t2)) + 
            s1*t2*(8 + t2 + 5*s*t2 + 15*Power(t2,2) - 
               t1*(4 + 8*s + 39*t2))) + 
         Power(t1,4)*(Power(s,3)*
             (8 + 36*t2 - 62*Power(t2,2) - 15*Power(t2,3)) + 
            Power(s1,3)*(-24 + 12*t2 - 46*Power(t2,2) + 
               15*Power(t2,3)) + 
            Power(s1,2)*(336 - 92*t2 - 28*Power(t2,2) + 
               39*Power(t2,3) + 4*Power(t2,4)) + 
            s1*(-24 - 204*t2 - 394*Power(t2,2) + 159*Power(t2,3) + 
               9*Power(t2,4)) + 
            2*(96 - 92*t2 - 92*Power(t2,2) + 35*Power(t2,3) + 
               16*Power(t2,4)) + 
            Power(s,2)*(-400 - 348*t2 + 172*Power(t2,2) + 
               131*Power(t2,3) + 4*Power(t2,4) + 
               s1*(152 - 172*t2 + 78*Power(t2,2) + 45*Power(t2,3))) + 
            s*(264 + 860*t2 + 186*Power(t2,2) - 283*Power(t2,3) - 
               18*Power(t2,4) + 
               Power(s1,2)*(-136 + 124*t2 + 30*Power(t2,2) - 
                  45*Power(t2,3)) - 
               2*s1*(64 - 196*t2 - 16*Power(t2,2) + 85*Power(t2,3) + 
                  4*Power(t2,4)))) + 
         Power(t1,3)*(-48 - 240*t2 + 48*Power(t2,2) + 188*Power(t2,3) - 
            7*Power(t2,4) - 19*Power(t2,5) + 
            s1*(248 - 308*t2 + 246*Power(t2,2) + 101*Power(t2,3) + 
               2*Power(t2,4) - 26*Power(t2,5)) + 
            Power(s1,3)*(8 - 12*t2 + 18*Power(t2,2) + 3*Power(t2,3) + 
               4*Power(t2,4) - 2*Power(t2,5)) - 
            2*Power(s1,2)*(-8 + 48*t2 + 80*Power(t2,2) - 
               60*Power(t2,3) + 18*Power(t2,4) + Power(t2,5)) + 
            Power(s,3)*(8 - 52*t2 - 98*Power(t2,2) + 45*Power(t2,3) + 
               43*Power(t2,4) + 2*Power(t2,5)) + 
            s*(248 + 36*t2 - 774*Power(t2,2) - 325*Power(t2,3) + 
               157*Power(t2,4) + 45*Power(t2,5) + 
               2*s1*t2*(256 - 204*t2 - 104*Power(t2,2) + 
                  61*Power(t2,3) + 9*Power(t2,4)) + 
               Power(s1,2)*(-72 + 68*t2 + 74*Power(t2,2) - 
                  113*Power(t2,3) + 35*Power(t2,4) + 6*Power(t2,5))) - 
            Power(s,2)*(8*(-2 - 38*t2 - 89*Power(t2,2) + 
                  2*Power(t2,3) + 21*Power(t2,4) + 2*Power(t2,5)) + 
               s1*(72 + 164*t2 - 118*Power(t2,2) - 65*Power(t2,3) + 
                  82*Power(t2,4) + 6*Power(t2,5)))) + 
         Power(s2,6)*(-12*Power(t1,3)*(3 + 6*t2 + Power(t2,2)) + 
            t2*(-4 + (-41 + 45*s + 14*Power(s,2))*t2 + 
               4*(-25 + s + 10*Power(s,2))*Power(t2,2) - 
               24*(6 + 11*s)*Power(t2,3) + 220*Power(t2,4)) + 
            Power(s1,3)*(4 - 14*t2 - 4*Power(t2,2) + t1*(5 + 7*t2)) - 
            Power(t1,2)*(16 - 198*t2 + 4*Power(s,2)*t2 - 
               557*Power(t2,2) - 115*Power(t2,3) + 
               s*(18 + 128*t2 + 87*Power(t2,2))) + 
            t1*t2*(40 + 386*t2 - 592*Power(t2,2) - 308*Power(t2,3) - 
               Power(s,2)*(12 + 37*t2) + 
               s*(8 + 344*t2 + 293*Power(t2,2))) + 
            Power(s1,2)*(6 + (11 + 2*s)*t2 + (39 + 7*s)*Power(t2,2) - 
               62*Power(t2,3) - 2*Power(t1,2)*(7 + 8*t2) + 
               t1*(-6 + 38*t2 + 51*Power(t2,2) - s*(8 + 11*t2))) + 
            s1*(-(t1*(8 + (30 - 4*s - 4*Power(s,2))*t2 + 
                    17*Power(t2,2) + 174*Power(t2,3))) + 
               Power(t1,2)*(2 + 78*t2 + 82*Power(t2,2) + 
                  4*s*(2 + 5*t2)) + 
               t2*(-12 + 26*t2 - Power(s,2)*t2 + 3*Power(t2,2) + 
                  46*Power(t2,3) + s*(2 - 16*t2 + 27*Power(t2,2))))) + 
         Power(s2,5)*(12*Power(t1,4)*(3 + 2*t2) + 
            t1*(4 - 2*(-48 + 52*s + 13*Power(s,2))*t2 + 
               3*(102 - 85*s - 47*Power(s,2) + 3*Power(s,3))*
                Power(t2,2) + 
               (945 + 1021*s - 168*Power(s,2))*Power(t2,3) + 
               (-831 + 532*s)*Power(t2,4) - 282*Power(t2,5)) + 
            Power(s1,3)*(-(Power(t1,2)*(7 + 11*t2)) - 
               2*t2*(-8 + 18*t2 + 3*Power(t2,2)) + 
               t1*(10 + 31*t2 + 28*Power(t2,2))) + 
            Power(t1,3)*(-64 - 613*t2 - 419*Power(t2,2) - 
               16*Power(t2,3) + Power(s,2)*(4 + 6*t2) + 
               s*(64 + 174*t2 + 45*Power(t2,2))) + 
            Power(t1,2)*(-20 - 541*t2 + 2*Power(s,3)*t2 + 
               534*Power(t2,2) + 1291*Power(t2,3) + 129*Power(t2,4) + 
               Power(s,2)*(6 + 82*t2 + 51*Power(t2,2)) - 
               s*(8 + 387*t2 + 887*Power(t2,2) + 279*Power(t2,3))) + 
            t2*(6 + 54*t2 - 135*Power(t2,2) - 200*Power(t2,3) - 
               126*Power(t2,4) + 160*Power(t2,5) - 
               3*Power(s,3)*t2*(2 + t2) + 
               Power(s,2)*t2*(-31 + 55*t2 + 108*Power(t2,2)) + 
               s*(-4 + 90*t2 + 189*Power(t2,2) + 6*Power(t2,3) - 
                  346*Power(t2,4))) + 
            Power(s1,2)*(-12 + 44*t2 + (38 + 8*s)*Power(t2,2) + 
               8*(12 + s)*Power(t2,3) - 88*Power(t2,4) + 
               2*Power(t1,3)*(8 + 3*t2) + 
               Power(t1,2)*(-14 - 114*t2 - 3*Power(t2,2) + 
                  s*(17 + 24*t2)) + 
               t1*(-23 - 129*t2 + 107*Power(t2,2) + 64*Power(t2,3) + 
                  s*(16 - 55*t2 - 42*Power(t2,2)))) + 
            s1*(-6 - (19 + 22*s)*t2 + 
               2*(-50 + 6*s + 5*Power(s,2))*Power(t2,2) - 
               6*(-6 + 11*s + Power(s,2))*Power(t2,3) + 
               (2 + 58*s)*Power(t2,4) + 74*Power(t2,5) - 
               Power(t1,3)*(39 + 164*t2 + 45*Power(t2,2) + 
                  4*s*(5 + 3*t2)) + 
               Power(t1,2)*(29 + 21*t2 + 445*Power(t2,2) + 
                  255*Power(t2,3) - 5*Power(s,2)*(2 + 3*t2) - 
                  s*(9 + 28*t2 + 48*Power(t2,2))) + 
               t1*(12 - 32*t2 + 101*Power(t2,2) - 55*Power(t2,3) - 
                  306*Power(t2,4) + Power(s,2)*t2*(8 + 5*t2) + 
                  2*s*(5 + 38*t2 + 6*Power(t2,2) + 45*Power(t2,3))))) + 
         Power(s2,4)*(2 - 12*Power(t1,5) + 9*t2 + 20*s*t2 + 
            83*Power(t2,2) - 101*s*Power(t2,2) - 
            30*Power(s,2)*Power(t2,2) + 4*Power(s,3)*Power(t2,2) + 
            125*Power(t2,3) + 314*s*Power(t2,3) - 
            126*Power(s,2)*Power(t2,3) - 22*Power(s,3)*Power(t2,3) - 
            168*Power(t2,4) + 312*s*Power(t2,4) + 
            86*Power(s,2)*Power(t2,4) - 14*Power(s,3)*Power(t2,4) - 
            198*Power(t2,5) - 16*s*Power(t2,5) + 
            152*Power(s,2)*Power(t2,5) - 44*Power(t2,6) - 
            254*s*Power(t2,6) + 62*Power(t2,7) + 
            Power(t1,4)*(223 - 6*Power(s,2) + 493*t2 + 76*Power(t2,2) - 
               3*s*(29 + 30*t2)) + 
            Power(s1,3)*(Power(t1,3)*(7 + 5*t2) - 
               4*Power(t2,2)*(-6 + 11*t2 + Power(t2,2)) - 
               Power(t1,2)*(35 + 61*t2 + 29*Power(t2,2)) + 
               t1*(-28 + 54*t2 + 55*Power(t2,2) + 42*Power(t2,3))) + 
            Power(t1,3)*(232 + 28*t2 - 1954*Power(t2,2) - 
               617*Power(t2,3) + 4*Power(t2,4) - 
               Power(s,3)*(4 + 5*t2) - 
               Power(s,2)*(45 + 118*t2 + 26*Power(t2,2)) + 
               s*(150 + 907*t2 + 832*Power(t2,2) + 85*Power(t2,3))) + 
            Power(t1,2)*(-55 - 328*t2 - 2016*Power(t2,2) + 
               756*Power(t2,3) + 1277*Power(t2,4) + 57*Power(t2,5) - 
               Power(s,3)*t2*(14 + 9*t2) + 
               Power(s,2)*(18 + 199*t2 + 483*Power(t2,2) + 
                  193*Power(t2,3)) + 
               s*(59 + 526*t2 - 1206*Power(t2,2) - 1921*Power(t2,3) - 
                  315*Power(t2,4))) + 
            t1*(-6 - 118*t2 + 262*Power(t2,2) + 802*Power(t2,3) + 
               877*Power(t2,4) - 621*Power(t2,5) - 128*Power(t2,6) + 
               2*Power(s,3)*t2*(6 + 4*t2 + 21*Power(t2,2)) - 
               Power(s,2)*t2*
                (-32 + 6*t2 + 451*Power(t2,2) + 302*Power(t2,3)) + 
               s*(-2 - 213*t2 - 692*Power(t2,2) - 740*Power(t2,3) + 
                  1401*Power(t2,4) + 478*Power(t2,5))) + 
            Power(s1,2)*(-6*Power(t1,4) + 
               2*t2*(-24 + 50*t2 + 6*(4 + s)*Power(t2,2) + 
                  (57 + s)*Power(t2,3) - 36*Power(t2,4)) + 
               Power(t1,3)*(63 + 36*t2 - 26*Power(t2,2) - 
                  3*s*(6 + 5*t2)) + 
               Power(t1,2)*(90 + 57*t2 - 245*Power(t2,2) + 
                  89*Power(t2,3) + s*(42 + 103*t2 + 49*Power(t2,2))) - 
               t1*(32 + 165*t2 + 347*Power(t2,2) - 153*Power(t2,3) - 
                  26*Power(t2,4) + 
                  s*(12 - 70*t2 + 79*Power(t2,2) + 58*Power(t2,3)))) + 
            s1*(12 + 2*(-23 + 6*s)*t2 - (5 + 48*s)*Power(t2,2) + 
               4*(-59 + 3*s + 10*Power(s,2))*Power(t2,3) - 
               2*(-18 + 52*s + 7*Power(s,2))*Power(t2,4) + 
               (-2 + 62*s)*Power(t2,5) + 66*Power(t2,6) + 
               2*Power(t1,4)*(41 + 6*s + 45*t2) + 
               Power(t1,3)*(-19 - 380*t2 - 748*Power(t2,2) - 
                  85*Power(t2,3) + 15*Power(s,2)*(1 + t2) + 
                  s*(28 + 82*t2 + 52*Power(t2,2))) - 
               Power(t1,2)*(-6 + 187*t2 + 342*Power(t2,2) - 
                  937*Power(t2,3) - 273*Power(t2,4) + 
                  Power(s,2)*(-17 + 28*t2 + 11*Power(t2,2)) + 
                  s*(72 + 183*t2 + 232*Power(t2,2) + 282*Power(t2,3))) + 
               t1*(31 + 281*t2 - 27*Power(t2,2) + 378*Power(t2,3) - 
                  127*Power(t2,4) - 264*Power(t2,5) + 
                  Power(s,2)*t2*(-42 + 21*t2 - 26*Power(t2,2)) + 
                  2*s*(-7 + 11*t2 + 46*Power(t2,2) - 5*Power(t2,3) + 
                     100*Power(t2,4))))) + 
         Power(s2,2)*(44*Power(t1,6) - 
            t2*(16 - 4*(1 + 6*s + 6*s1)*t2 + 
               (57 - 60*Power(s,2) + 2*s1 + 24*Power(s1,2) + 
                  2*s*(49 + 6*s1))*Power(t2,2) - 
               (149 - 116*Power(s,2) + 24*Power(s,3) + s1 + 
                  30*Power(s1,2) + 4*Power(s1,3) + s*(-137 + 48*s1))*
                Power(t2,3) + 
               (21 + 12*Power(s,3) + Power(s,2)*(166 - 40*s1) + 
                  80*s1 - 5*Power(s1,2) + 6*Power(s1,3) - 
                  2*s*(134 - 15*s1 + Power(s1,2)))*Power(t2,4) + 
               (24*Power(s,3) + Power(s,2)*(-46 + 9*s1) + 
                  s*(-81 + 24*s1 + Power(s1,2)) - 
                  3*(-11 + 6*s1 + 5*Power(s1,2)))*Power(t2,5) + 
               (14 - 48*Power(s,2) + s*(36 - 7*s1) + s1 + 
                  6*Power(s1,2))*Power(t2,6) + 
               2*(-2 + 8*s - 3*s1)*Power(t2,7)) - 
            Power(t1,5)*(296 + 12*Power(s,2) + 12*Power(s1,2) + 
               887*t2 + 44*Power(t2,2) + s1*(238 + 325*t2) - 
               s*(274 + 24*s1 + 325*t2)) + 
            t1*(-12 + (75 + 38*s - 72*Power(s,2))*t2 + 
               (-391 + 525*s + 276*Power(s,2) - 32*Power(s,3))*
                Power(t2,2) + 
               (-391 - 883*s + 655*Power(s,2) + 42*Power(s,3))*
                Power(t2,3) + 
               (287 - 1153*s + 217*Power(s,2) + 67*Power(s,3))*
                Power(t2,4) + 
               (344 - 124*s - 433*Power(s,2) + 72*Power(s,3))*
                Power(t2,5) + 
               (-7 + 347*s - 117*Power(s,2))*Power(t2,6) + 
               (-39 + 37*s)*Power(t2,7) + 
               Power(s1,3)*Power(t2,2)*
                (-32 + 58*t2 + 8*Power(t2,2) + 7*Power(t2,3)) - 
               Power(s1,2)*t2*
                (-132 + 12*(19 + 2*s)*t2 + (135 + 22*s)*Power(t2,2) + 
                  (151 - 35*s)*Power(t2,3) + 3*(-15 + s)*Power(t2,4) + 
                  5*Power(t2,5)) + 
               s1*(-84 + (98 + 36*s)*t2 + 
                  (129 - 4*s - 24*Power(s,2))*Power(t2,2) + 
                  (299 + 218*s - 146*Power(s,2))*Power(t2,3) + 
                  3*(45 - 62*s + 19*Power(s,2))*Power(t2,4) + 
                  (98 + 2*s - 76*Power(s,2))*Power(t2,5) + 
                  12*(-7 + 6*s)*Power(t2,6) - 18*Power(t2,7))) + 
            Power(t1,4)*(-564 - 1422*t2 + 1385*Power(t2,2) + 
               559*Power(t2,3) - 20*Power(t2,4) + 
               Power(s,3)*(-6 + 7*t2) - Power(s1,3)*(22 + 7*t2) + 
               Power(s1,2)*(-68 + 21*t2 + 202*Power(t2,2)) + 
               s1*(-254 + 113*t2 + 1097*Power(t2,2) + 
                  110*Power(t2,3)) + 
               Power(s,2)*(132 + 489*t2 + 202*Power(t2,2) - 
                  s1*(10 + 21*t2)) + 
               s*(30 - 1477*t2 - 1379*Power(t2,2) - 110*Power(t2,3) + 
                  Power(s1,2)*(38 + 21*t2) - 
                  2*s1*(72 + 255*t2 + 202*Power(t2,2)))) + 
            Power(t1,3)*(4 + 372*t2 + 2103*Power(t2,2) - 
               25*Power(t2,3) - 882*Power(t2,4) - 27*Power(t2,5) + 
               Power(s1,3)*(54 + 129*t2 + 69*Power(t2,2) - 
                  11*Power(t2,3)) + 
               Power(s,3)*(10 + 47*t2 + 121*Power(t2,2) + 
                  11*Power(t2,3)) - 
               Power(s1,2)*(228 + 316*t2 - 301*Power(t2,2) + 
                  250*Power(t2,3) + 46*Power(t2,4)) + 
               s1*(170 + 979*t2 + 1781*Power(t2,2) - 942*Power(t2,3) - 
                  368*Power(t2,4) + 5*Power(t2,5)) - 
               Power(s,2)*(-148 + 356*t2 + 1091*Power(t2,2) + 
                  610*Power(t2,3) + 46*Power(t2,4) + 
                  s1*(142 - 91*t2 + 173*Power(t2,2) + 33*Power(t2,3))) \
+ s*(-362 - 2195*t2 - 223*Power(t2,2) + 2252*Power(t2,3) + 
                  484*Power(t2,4) - 5*Power(t2,5) + 
                  Power(s1,2)*
                   (30 - 267*t2 - 17*Power(t2,2) + 33*Power(t2,3)) + 
                  2*s1*(40 + 84*t2 + 197*Power(t2,2) + 
                     430*Power(t2,3) + 46*Power(t2,4)))) - 
            Power(t1,2)*(-120 - 662*t2 + 57*Power(t2,2) + 
               1262*Power(t2,3) + 628*Power(t2,4) - 366*Power(t2,5) - 
               78*Power(t2,6) + 
               Power(s1,3)*(-40 + 54*t2 + 81*Power(t2,2) + 
                  83*Power(t2,3) - Power(t2,4)) + 
               Power(s,3)*(-4 + 2*t2 + 53*Power(t2,2) + 
                  187*Power(t2,3) + 61*Power(t2,4)) + 
               Power(s1,2)*(20 - 499*t2 - 595*Power(t2,2) + 
                  63*Power(t2,3) + 19*Power(t2,4) - 51*Power(t2,5)) - 
               Power(s,2)*(-52 + (-197 + 78*s1)*t2 + 
                  (-821 + 41*s1)*Power(t2,2) + 
                  (705 + 101*s1)*Power(t2,3) + 
                  (787 + 123*s1)*Power(t2,4) + 111*Power(t2,5)) + 
               s1*(138 + 401*t2 + 303*Power(t2,2) + 953*Power(t2,3) + 
                  222*Power(t2,4) - 369*Power(t2,5) - 9*Power(t2,6)) + 
               s*(102 - 477*t2 - 2237*Power(t2,2) - 2159*Power(t2,3) + 
                  1086*Power(t2,4) + 695*Power(t2,5) + 18*Power(t2,6) + 
                  Power(s1,2)*
                   (-84 + 166*t2 + 35*Power(t2,2) - 169*Power(t2,3) + 
                     63*Power(t2,4)) + 
                  2*s1*(-76 + 123*t2 - 249*Power(t2,2) - 
                     52*Power(t2,3) + 213*Power(t2,4) + 81*Power(t2,5))))\
) + Power(s2,3)*(-4 + Power(t1,5)*(-189 + 45*s - 45*s1 - 104*t2) + 
            (16 - 12*s + 48*s1)*t2 + 
            (-59 + 24*Power(s,2) - 70*s1 - 60*Power(s1,2) + 
               2*s*(1 + 6*s1))*Power(t2,2) + 
            (195 - 100*Power(s,2) + 16*Power(s,3) + 31*s1 + 
               92*Power(s1,2) + 16*Power(s1,3) - s*(209 + 4*s1))*
             Power(t2,3) + (63 - 28*Power(s,3) - 224*s1 + 
               26*Power(s1,2) - 26*Power(s1,3) + 
               12*Power(s,2)*(-17 + 5*s1) + 
               8*s*(52 - 2*s1 + Power(s1,2)))*Power(t2,4) - 
            (102 + 26*Power(s,3) - 32*s1 - 66*Power(s1,2) + 
               Power(s1,3) + 2*Power(s,2)*(-37 + 8*s1) + 
               2*s*(-123 + 38*s1 + Power(s1,2)))*Power(t2,5) + 
            (-94 + 118*Power(s,2) - 3*s1 - 32*Power(s1,2) + 
               11*s*(-4 + 3*s1))*Power(t2,6) + 
            (3 - 99*s + 31*s1)*Power(t2,7) + 10*Power(t2,8) + 
            Power(t1,4)*(-140 + 3*Power(s,3) - 3*Power(s1,3) + 
               1267*t2 + 1111*Power(t2,2) + Power(s1,2)*(-33 + 38*t2) + 
               Power(s,2)*(67 - 9*s1 + 38*t2) + 
               s1*(109 + 731*t2 + 290*Power(t2,2)) + 
               s*(-313 + 9*Power(s1,2) - 827*t2 - 290*Power(t2,2) - 
                  2*s1*(17 + 38*t2))) + 
            Power(t1,3)*(122 + 1779*t2 + 689*Power(t2,2) - 
               2108*Power(t2,3) - 297*Power(t2,4) + 8*Power(t2,5) + 
               Power(s,3)*(15 + 29*t2 - 3*Power(t2,2)) + 
               Power(s1,3)*(41 + 55*t2 + 3*Power(t2,2)) - 
               Power(s1,2)*(90 - 243*t2 + 142*Power(t2,2) + 
                  74*Power(t2,3)) + 
               s1*(83 + 605*t2 - 836*Power(t2,2) - 934*Power(t2,3) - 
                  35*Power(t2,4)) - 
               Power(s,2)*(98 + 453*t2 + 506*Power(t2,2) + 
                  74*Power(t2,3) + s1*(-35 + 3*t2 - 9*Power(t2,2))) + 
               s*(-275 + 419*t2 + 2542*Power(t2,2) + 1106*Power(t2,3) + 
                  35*Power(t2,4) - 
                  Power(s1,2)*(91 + 81*t2 + 9*Power(t2,2)) + 
                  2*s1*(66 + 149*t2 + 324*Power(t2,2) + 74*Power(t2,3)))\
) - Power(t1,2)*(-64 + 131*t2 + 974*Power(t2,2) + 2098*Power(t2,3) - 
               726*Power(t2,4) - 549*Power(t2,5) - 7*Power(t2,6) + 
               Power(s1,3)*(-6 + 103*t2 + 119*Power(t2,2) + 
                  21*Power(t2,3)) + 
               Power(s,3)*(6 + 33*t2 + 95*Power(t2,2) + 
                  45*Power(t2,3)) - 
               Power(s1,2)*(151 + 455*t2 + 91*Power(t2,2) - 
                  187*Power(t2,3) + 125*Power(t2,4)) + 
               s1*(181 + 191*t2 + 1013*Power(t2,2) + 640*Power(t2,3) - 
                  895*Power(t2,4) - 109*Power(t2,5)) - 
               Power(s,2)*(-25 - 209*t2 + 607*Power(t2,2) + 
                  941*Power(t2,3) + 233*Power(t2,4) + 
                  s1*(2 + 67*t2 + 17*Power(t2,2) + 69*Power(t2,3))) + 
               s*(-129 - 871*t2 - 2151*Power(t2,2) + 1568*Power(t2,3) + 
                  1775*Power(t2,4) + 141*Power(t2,5) + 
                  Power(s1,2)*
                   (114 - 101*t2 - 197*Power(t2,2) + 3*Power(t2,3)) + 
                  s1*(22 + 46*t2 + 204*Power(t2,2) + 474*Power(t2,3) + 
                     358*Power(t2,4)))) + 
            t1*(-13 - 203*t2 - 487*Power(t2,2) + 355*Power(t2,3) + 
               838*Power(t2,4) + 291*Power(t2,5) - 241*Power(t2,6) - 
               23*Power(t2,7) + 
               Power(s1,3)*t2*
                (-52 + 90*t2 + 37*Power(t2,2) + 28*Power(t2,3)) + 
               Power(s,3)*t2*
                (-8 + 50*t2 + 41*Power(t2,2) + 78*Power(t2,3)) - 
               Power(s1,2)*(-84 + 152*t2 + 299*Power(t2,2) + 
                  353*Power(t2,3) - 121*Power(t2,4) + 6*Power(t2,5)) + 
               s1*(34 + 119*t2 + 513*Power(t2,2) + 111*Power(t2,3) + 
                  342*Power(t2,4) - 153*Power(t2,5) - 111*Power(t2,6)) + 
               Power(s,2)*t2*
                (112 + 295*t2 + 157*Power(t2,2) - 631*Power(t2,3) - 
                  268*Power(t2,4) + 
                  s1*(12 - 134*t2 + 37*Power(t2,2) - 74*Power(t2,3))) - 
               s*(2 - 197*t2 + 689*Power(t2,2) + 1447*Power(t2,3) + 
                  650*Power(t2,4) - 985*Power(t2,5) - 212*Power(t2,6) + 
                  Power(s1,2)*t2*
                   (48 - 62*t2 + 11*Power(t2,2) + 32*Power(t2,3)) - 
                  2*s1*(6 - 16*t2 + 81*Power(t2,2) - 45*Power(t2,3) - 
                     17*Power(t2,4) + 90*Power(t2,5))))) + 
         s2*(-24*Power(t1,6)*(-11 + 5*s - 5*s1 - 3*t2) + 
            Power(t2,2)*(1 + (-1 + s)*t2)*
             (16 + 4*(-7 + 8*s - 3*s1)*t2 + 
               (-13 - 38*s + 16*Power(s,2) + 16*s1)*Power(t2,2) + 
               2*(13 + Power(s,2) + s*(-28 + 5*s1))*Power(t2,3) + 
               (1 - 11*Power(s,2) - 2*s*(-6 + s1) - 4*s1)*Power(t2,4) + 
               (-2 + 8*s)*Power(t2,5)) + 
            t1*(28 + (4 - 60*s - 60*s1)*t2 + 
               (137 - 132*Power(s,2) + s*(214 - 36*s1) + 58*s1 + 
                  72*Power(s1,2))*Power(t2,2) - 
               (345 + 52*Power(s,3) + 19*s1 + 116*Power(s1,2) + 
                  8*Power(s1,3) + 4*Power(s,2)*(-49 + 12*s1) - 
                  s*(431 + 40*s1 + 12*Power(s1,2)))*Power(t2,3) + 
               (43 - 18*Power(s,3) + Power(s,2)*(509 - 58*s1) + 
                  67*s1 + 22*Power(s1,2) + 12*Power(s1,3) + 
                  s*(-575 + 92*s1 - 30*Power(s1,2)))*Power(t2,4) + 
               (73 + 43*Power(s,3) + 41*s1 - 22*Power(s1,2) + 
                  Power(s,2)*(69 + 47*s1) + 
                  s*(-297 - 114*s1 + 14*Power(s1,2)))*Power(t2,5) + 
               (40 + 33*Power(s,3) - 3*s1 + 4*Power(s1,2) - 
                  Power(s,2)*(144 + 35*s1) + 
                  s*(65 + 30*s1 + 2*Power(s1,2)))*Power(t2,6) - 
               2*(6 + 10*Power(s,2) + 8*s1 - 5*s*(5 + s1))*Power(t2,7)) + 
            Power(t1,5)*(588 + Power(s1,2)*(32 - 176*t2) - 144*t2 - 
               455*Power(t2,2) + 16*Power(t2,3) - 
               176*Power(s,2)*(1 + t2) + 
               s1*(92 - 484*t2 - 115*Power(t2,2)) + 
               s*(324 + 700*t2 + 115*Power(t2,2) + 16*s1*(9 + 22*t2))) + 
            Power(t1,4)*(Power(s1,3)*(-76 - 84*t2 + 27*Power(t2,2)) - 
               Power(s,3)*(36 + 116*t2 + 27*Power(t2,2)) + 
               t2*(-804 - 444*t2 + 629*Power(t2,2) + 33*Power(t2,3)) + 
               Power(s1,2)*(148 - 96*t2 + 93*Power(t2,2) + 
                  98*Power(t2,3)) + 
               s1*(-340 - 1512*t2 + 243*Power(t2,2) + 409*Power(t2,3) - 
                  10*Power(t2,4)) + 
               Power(s,2)*(116 + 464*t2 + 489*Power(t2,2) + 
                  98*Power(t2,3) + s1*(-52 + 148*t2 + 81*Power(t2,2))) - 
               s*(-772 - 1336*t2 + 1079*Power(t2,2) + 545*Power(t2,3) - 
                  10*Power(t2,4) + 
                  Power(s1,2)*(-164 - 52*t2 + 81*Power(t2,2)) + 
                  2*s1*(12 + 72*t2 + 291*Power(t2,2) + 98*Power(t2,3)))) \
+ Power(t1,3)*(-296 - 324*t2 + 806*Power(t2,2) + 565*Power(t2,3) - 
               257*Power(t2,4) - 87*Power(t2,5) + 
               Power(s1,3)*(-20 + 80*t2 + 67*Power(t2,2) + 
                  25*Power(t2,3) - 11*Power(t2,4)) + 
               Power(s,3)*(-12 + 80*t2 + 133*Power(t2,2) + 
                  131*Power(t2,3) + 11*Power(t2,4)) + 
               s1*(148 + 192*t2 + 805*Power(t2,2) + 703*Power(t2,3) - 
                  417*Power(t2,4) - 18*Power(t2,5)) - 
               Power(s1,2)*(248 + 620*t2 - 70*Power(t2,2) - 
                  53*Power(t2,3) + 90*Power(t2,4) + 4*Power(t2,5)) + 
               Power(s,2)*(88 + 948*t2 + 62*Power(t2,2) - 
                  691*Power(t2,3) - 242*Power(t2,4) - 4*Power(t2,5) + 
                  s1*(100 - 192*t2 + 49*Power(t2,2) - 237*Power(t2,3) - 
                     33*Power(t2,4))) + 
               s*(-132 - 1360*t2 - 2341*Power(t2,2) + 169*Power(t2,3) + 
                  766*Power(t2,4) + 36*Power(t2,5) + 
                  Power(s1,2)*
                   (92 + 96*t2 - 249*Power(t2,2) + 81*Power(t2,3) + 
                     33*Power(t2,4)) + 
                  s1*(-48 - 248*t2 - 428*Power(t2,2) + 214*Power(t2,3) + 
                     332*Power(t2,4) + 8*Power(t2,5)))) + 
            Power(t1,2)*(-8 + 236*t2 + 564*Power(t2,2) - 
               229*Power(t2,3) - 438*Power(t2,4) + 11*Power(t2,5) + 
               48*Power(t2,6) + 
               Power(s1,3)*t2*
                (4 - 10*t2 - 29*Power(t2,2) - 18*Power(t2,3) + 
                  4*Power(t2,4)) - 
               Power(s,3)*t2*(-4 + 6*t2 + 19*Power(t2,2) + 
                  145*Power(t2,3) + 33*Power(t2,4)) + 
               Power(s1,2)*(-108 + 204*t2 + 181*Power(t2,2) + 
                  173*Power(t2,3) - 73*Power(t2,4) + 29*Power(t2,5) + 
                  2*Power(t2,6)) + 
               Power(s,2)*(36 + 12*(-23 + 5*s1)*t2 + 
                  (-779 + 178*s1)*Power(t2,2) - 
                  (907 + 111*s1)*Power(t2,3) + 
                  (367 + 89*s1)*Power(t2,4) + 
                  7*(39 + 10*s1)*Power(t2,5) + 16*Power(t2,6)) + 
               s1*(44 - 408*t2 + 161*Power(t2,2) - 377*Power(t2,3) - 
                  196*Power(t2,4) + 51*Power(t2,5) + 44*Power(t2,6)) - 
               s*(76 + 504*t2 - 467*Power(t2,2) - 1633*Power(t2,3) - 
                  493*Power(t2,4) + 405*Power(t2,5) + 82*Power(t2,6) + 
                  Power(s1,2)*t2*
                   (-60 + 18*t2 + 65*Power(t2,2) - 74*Power(t2,3) + 
                     41*Power(t2,4)) + 
                  s1*(120 - 8*t2 + 546*Power(t2,2) - 646*Power(t2,3) - 
                     77*Power(t2,4) + 170*Power(t2,5) + 18*Power(t2,6)))))\
)*R2(1 - s2 + t1 - t2))/
     ((-1 + t1)*(s - s2 + t1)*(1 - s2 + t1 - t2)*(s - s1 + t2)*
       Power(t1 - s2*t2,2)*Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - 
         Power(t2,2),2)) - (8*(72*s1*Power(t1,3) + 96*Power(t1,4) + 
         264*s1*Power(t1,4) + 16*Power(s1,2)*Power(t1,4) + 
         8*Power(s1,3)*Power(t1,4) + 16*Power(t1,5) + 
         736*s1*Power(t1,5) + 144*Power(s1,2)*Power(t1,5) - 
         56*Power(s1,3)*Power(t1,5) + 672*Power(t1,6) - 
         224*s1*Power(t1,6) + 464*Power(s1,2)*Power(t1,6) - 
         88*Power(s1,3)*Power(t1,6) - 256*Power(t1,7) - 
         8*s1*Power(t1,7) - 16*Power(s1,2)*Power(t1,7) + 
         56*Power(s1,3)*Power(t1,7) - 512*Power(t1,8) - 
         40*s1*Power(t1,8) + 32*Power(s1,2)*Power(t1,8) + 
         16*Power(s1,3)*Power(t1,8) - 16*Power(t1,9) - 
         32*s1*Power(t1,9) - 60*Power(t1,2)*t2 - 288*Power(t1,3)*t2 - 
         192*s1*Power(t1,3)*t2 - 36*Power(s1,2)*Power(t1,3)*t2 - 
         732*Power(t1,4)*t2 - 472*s1*Power(t1,4)*t2 - 
         72*Power(s1,2)*Power(t1,4)*t2 - 16*Power(s1,3)*Power(t1,4)*t2 - 
         120*Power(t1,5)*t2 - 1144*s1*Power(t1,5)*t2 + 
         264*Power(s1,2)*Power(t1,5)*t2 + 88*Power(s1,3)*Power(t1,5)*t2 + 
         364*Power(t1,6)*t2 + 840*s1*Power(t1,6)*t2 + 
         144*Power(s1,2)*Power(t1,6)*t2 + 64*Power(s1,3)*Power(t1,6)*t2 + 
         1392*Power(t1,7)*t2 + 600*s1*Power(t1,7)*t2 + 
         92*Power(s1,2)*Power(t1,7)*t2 - 8*Power(s1,3)*Power(t1,7)*t2 - 
         20*Power(t1,8)*t2 + 112*s1*Power(t1,8)*t2 + 
         56*Power(s1,2)*Power(t1,8)*t2 - 24*Power(t1,9)*t2 + 
         150*Power(t1,2)*Power(t2,2) + 396*Power(t1,3)*Power(t2,2) + 
         54*s1*Power(t1,3)*Power(t2,2) + 
         74*Power(s1,2)*Power(t1,3)*Power(t2,2) + 
         4*Power(s1,3)*Power(t1,3)*Power(t2,2) + 
         462*Power(t1,4)*Power(t2,2) - 614*s1*Power(t1,4)*Power(t2,2) - 
         124*Power(s1,2)*Power(t1,4)*Power(t2,2) + 
         38*Power(s1,3)*Power(t1,4)*Power(t2,2) - 
         1216*Power(t1,5)*Power(t2,2) - 384*s1*Power(t1,5)*Power(t2,2) - 
         1224*Power(s1,2)*Power(t1,5)*Power(t2,2) + 
         10*Power(s1,3)*Power(t1,5)*Power(t2,2) - 
         1206*Power(t1,6)*Power(t2,2) - 1304*s1*Power(t1,6)*Power(t2,2) - 
         420*Power(s1,2)*Power(t1,6)*Power(t2,2) - 
         86*Power(s1,3)*Power(t1,6)*Power(t2,2) + 
         404*Power(t1,7)*Power(t2,2) - 118*s1*Power(t1,7)*Power(t2,2) - 
         66*Power(s1,2)*Power(t1,7)*Power(t2,2) - 
         46*Power(s1,3)*Power(t1,7)*Power(t2,2) + 
         114*Power(t1,8)*Power(t2,2) + 62*s1*Power(t1,8)*Power(t2,2) - 
         2*Power(s2,12)*(s1 - 3*t2)*Power(t2,2) + 20*t1*Power(t2,3) + 
         60*Power(t1,2)*Power(t2,3) + 10*s1*Power(t1,2)*Power(t2,3) + 
         518*Power(t1,3)*Power(t2,3) + 316*s1*Power(t1,3)*Power(t2,3) - 
         50*Power(s1,2)*Power(t1,3)*Power(t2,3) - 
         8*Power(s1,3)*Power(t1,3)*Power(t2,3) + 
         620*Power(t1,4)*Power(t2,3) + 1786*s1*Power(t1,4)*Power(t2,3) + 
         36*Power(s1,2)*Power(t1,4)*Power(t2,3) - 
         30*Power(s1,3)*Power(t1,4)*Power(t2,3) + 
         564*Power(t1,5)*Power(t2,3) + 734*s1*Power(t1,5)*Power(t2,3) + 
         528*Power(s1,2)*Power(t1,5)*Power(t2,3) - 
         44*Power(s1,3)*Power(t1,5)*Power(t2,3) - 
         984*Power(t1,6)*Power(t2,3) - 236*s1*Power(t1,6)*Power(t2,3) - 
         36*Power(s1,2)*Power(t1,6)*Power(t2,3) + 
         18*Power(s1,3)*Power(t1,6)*Power(t2,3) - 
         222*Power(t1,7)*Power(t2,3) - 178*s1*Power(t1,7)*Power(t2,3) - 
         78*Power(s1,2)*Power(t1,7)*Power(t2,3) - 50*t1*Power(t2,4) - 
         310*Power(t1,2)*Power(t2,4) + 20*s1*Power(t1,2)*Power(t2,4) - 
         782*Power(t1,3)*Power(t2,4) - 6*s1*Power(t1,3)*Power(t2,4) + 
         54*Power(s1,2)*Power(t1,3)*Power(t2,4) + 
         92*Power(t1,4)*Power(t2,4) - 498*s1*Power(t1,4)*Power(t2,4) + 
         488*Power(s1,2)*Power(t1,4)*Power(t2,4) - 
         12*Power(s1,3)*Power(t1,4)*Power(t2,4) + 
         850*Power(t1,5)*Power(t2,4) + 668*s1*Power(t1,5)*Power(t2,4) + 
         246*Power(s1,2)*Power(t1,5)*Power(t2,4) + 
         72*Power(s1,3)*Power(t1,5)*Power(t2,4) + 
         122*Power(t1,6)*Power(t2,4) + 274*s1*Power(t1,6)*Power(t2,4) + 
         116*Power(s1,2)*Power(t1,6)*Power(t2,4) + 
         40*Power(s1,3)*Power(t1,6)*Power(t2,4) - 
         18*Power(t1,7)*Power(t2,4) - 10*s1*Power(t1,7)*Power(t2,4) - 
         2*Power(t2,5) - 6*t1*Power(t2,5) - s1*t1*Power(t2,5) - 
         35*Power(t1,2)*Power(t2,5) - 116*s1*Power(t1,2)*Power(t2,5) + 
         2*Power(s1,2)*Power(t1,2)*Power(t2,5) - 
         107*Power(t1,3)*Power(t2,5) - 704*s1*Power(t1,3)*Power(t2,5) - 
         20*Power(s1,2)*Power(t1,3)*Power(t2,5) + 
         5*Power(s1,3)*Power(t1,3)*Power(t2,5) - 
         431*Power(t1,4)*Power(t2,5) - 565*s1*Power(t1,4)*Power(t2,5) - 
         377*Power(s1,2)*Power(t1,4)*Power(t2,5) + 
         18*Power(s1,3)*Power(t1,4)*Power(t2,5) + 
         117*Power(t1,5)*Power(t2,5) - 163*s1*Power(t1,5)*Power(t2,5) - 
         102*Power(s1,2)*Power(t1,5)*Power(t2,5) - 
         31*Power(s1,3)*Power(t1,5)*Power(t2,5) + 
         64*Power(t1,6)*Power(t2,5) + 29*s1*Power(t1,6)*Power(t2,5) + 
         21*Power(s1,2)*Power(t1,6)*Power(t2,5) + 5*Power(t2,6) + 
         94*t1*Power(t2,6) - 2*s1*t1*Power(t2,6) + 
         338*Power(t1,2)*Power(t2,6) + 63*s1*Power(t1,2)*Power(t2,6) - 
         5*Power(s1,2)*Power(t1,2)*Power(t2,6) + 
         201*Power(t1,3)*Power(t2,6) + 424*s1*Power(t1,3)*Power(t2,6) - 
         91*Power(s1,2)*Power(t1,3)*Power(t2,6) - 
         146*Power(t1,4)*Power(t2,6) - 19*s1*Power(t1,4)*Power(t2,6) + 
         13*Power(s1,2)*Power(t1,4)*Power(t2,6) - 
         20*Power(s1,3)*Power(t1,4)*Power(t2,6) - 
         69*Power(t1,5)*Power(t2,6) - 67*s1*Power(t1,5)*Power(t2,6) - 
         31*Power(s1,2)*Power(t1,5)*Power(t2,6) - 
         11*Power(s1,3)*Power(t1,5)*Power(t2,6) + 
         Power(t1,6)*Power(t2,6) + s1*Power(t1,6)*Power(t2,6) - 
         39*t1*Power(t2,7) + 11*s1*t1*Power(t2,7) - 
         98*Power(t1,2)*Power(t2,7) + 117*s1*Power(t1,2)*Power(t2,7) + 
         2*Power(s1,2)*Power(t1,2)*Power(t2,7) + 
         77*Power(t1,3)*Power(t2,7) + 80*s1*Power(t1,3)*Power(t2,7) + 
         97*Power(s1,2)*Power(t1,3)*Power(t2,7) - 
         2*Power(s1,3)*Power(t1,3)*Power(t2,7) + 
         29*Power(t1,4)*Power(t2,7) + 66*s1*Power(t1,4)*Power(t2,7) + 
         38*Power(s1,2)*Power(t1,4)*Power(t2,7) + 
         10*Power(s1,3)*Power(t1,4)*Power(t2,7) - 
         5*Power(t1,5)*Power(t2,7) - 2*s1*Power(t1,5)*Power(t2,7) - 
         2*Power(s1,2)*Power(t1,5)*Power(t2,7) - 9*Power(t2,8) - 
         55*t1*Power(t2,8) - 8*s1*t1*Power(t2,8) - 
         55*Power(t1,2)*Power(t2,8) - 114*s1*Power(t1,2)*Power(t2,8) + 
         6*Power(s1,2)*Power(t1,2)*Power(t2,8) - 
         10*Power(t1,3)*Power(t2,8) - 38*s1*Power(t1,3)*Power(t2,8) - 
         24*Power(s1,2)*Power(t1,3)*Power(t2,8) + 
         2*Power(s1,3)*Power(t1,3)*Power(t2,8) + 
         7*Power(t1,4)*Power(t2,8) + 5*s1*Power(t1,4)*Power(t2,8) + 
         3*Power(s1,2)*Power(t1,4)*Power(t2,8) + 
         Power(s1,3)*Power(t1,4)*Power(t2,8) + 6*Power(t2,9) + 
         39*t1*Power(t2,9) - 7*s1*t1*Power(t2,9) + 
         7*Power(t1,2)*Power(t2,9) + 15*s1*Power(t1,2)*Power(t2,9) - 
         8*Power(s1,2)*Power(t1,2)*Power(t2,9) - 
         5*Power(t1,3)*Power(t2,9) - 6*s1*Power(t1,3)*Power(t2,9) - 
         4*Power(s1,2)*Power(t1,3)*Power(t2,9) - 
         Power(s1,3)*Power(t1,3)*Power(t2,9) + 3*Power(t2,10) - 
         t1*Power(t2,10) + 10*s1*t1*Power(t2,10) + 
         3*Power(t1,2)*Power(t2,10) + 5*s1*Power(t1,2)*Power(t2,10) + 
         3*Power(s1,2)*Power(t1,2)*Power(t2,10) - 4*Power(t2,11) - 
         2*t1*Power(t2,11) - 3*s1*t1*Power(t2,11) + Power(t2,12) - 
         Power(s,3)*(16*Power(t1,8) + 
            Power(t2,8)*Power(1 + t2,2)*(2 - 3*t2 + Power(t2,2)) - 
            2*Power(t1,7)*(12 + 48*t2 + 23*Power(t2,2)) + 
            2*Power(t1,6)*(28 - 52*t2 + 43*Power(t2,2) + 
               106*Power(t2,3) + 20*Power(t2,4)) - 
            t1*Power(t2,6)*(20 + 15*t2 - 28*Power(t2,2) - 
               22*Power(t2,3) + 8*Power(t2,4) + 4*Power(t2,5)) + 
            Power(t1,5)*(24 - 176*t2 + 278*Power(t2,2) + 
               270*Power(t2,3) - 286*Power(t2,4) - 162*Power(t2,5) - 
               11*Power(t2,6)) + 
            Power(t1,2)*Power(t2,4)*
             (60 + 80*t2 - 67*Power(t2,2) - 148*Power(t2,3) - 
               7*Power(t2,4) + 41*Power(t2,5) + 6*Power(t2,6)) - 
            Power(t1,3)*Power(t2,2)*
             (40 + 170*t2 + 30*Power(t2,2) - 360*Power(t2,3) - 
               209*Power(t2,4) + 122*Power(t2,5) + 65*Power(t2,6) + 
               4*Power(t2,7)) + 
            Power(t1,4)*(-8 + 56*t2 + 186*Power(t2,2) - 
               168*Power(t2,3) - 556*Power(t2,4) + 30*Power(t2,5) + 
               232*Power(t2,6) + 44*Power(t2,7) + Power(t2,8))) - 
         2*Power(s2,11)*t2*(Power(s1,2)*(1 + t2) - 
            s1*((4 + s - 6*t2)*t2 + t1*(2 + 4*t2)) + 
            t2*(-1 + 6*t2 + 4*s*t2 - 25*Power(t2,2) + t1*(9 + s + 13*t2))\
) + Power(s,2)*(8*Power(t1,8)*(16 + 6*s1 + 7*t2) + 
            3*(-2 + t2)*Power(t2,7)*Power(-1 + Power(t2,2),2) - 
            t1*(-1 + t2)*Power(t2,5)*
             (60 + (34 + 6*s1)*t2 + (-107 + 8*s1)*Power(t2,2) - 
               (77 + 5*s1)*Power(t2,3) + (26 - 4*s1)*Power(t2,4) + 
               (13 + 3*s1)*Power(t2,5)) + 
            Power(t1,2)*Power(t2,3)*
             (-180 + (50 - 60*s1)*t2 + (571 - 24*s1)*Power(t2,2) + 
               (153 + 122*s1)*Power(t2,3) + 
               (-593 + 24*s1)*Power(t2,4) - 
               4*(43 + 21*s1)*Power(t2,5) + (115 + 18*s1)*Power(t2,6) + 
               (23 + 9*s1)*Power(t2,7)) + 
            Power(t1,3)*t2*(108 + 2*(49 + 90*s1)*t2 + 
               2*(-459 + 50*s1)*Power(t2,2) - 
               4*(275 + 78*s1)*Power(t2,3) + 
               (1226 - 299*s1)*Power(t2,4) + 
               (1377 + 326*s1)*Power(t2,5) + 
               3*(-53 + 16*s1)*Power(t2,6) - 
               3*(77 + 30*s1)*Power(t2,7) - 3*(7 + 3*s1)*Power(t2,8)) - 
            2*Power(t1,7)*(-72 + 66*t2 + 189*Power(t2,2) + 
               39*Power(t2,3) + s1*(-36 + 100*t2 + 69*Power(t2,2))) + 
            Power(t1,6)*(48 - 1232*t2 - 884*Power(t2,2) + 
               740*Power(t2,3) + 376*Power(t2,4) + 21*Power(t2,5) + 
               2*s1*(108 - 72*t2 - 85*Power(t2,2) + 221*Power(t2,3) + 
                  60*Power(t2,4))) - 
            Power(t1,5)*(s1*(-312 + 520*t2 + 490*Power(t2,2) - 
                  848*Power(t2,3) + 160*Power(t2,4) + 
                  355*Power(t2,5) + 33*Power(t2,6)) + 
               2*(168 + 516*t2 - 948*Power(t2,2) - 1426*Power(t2,3) + 
                  130*Power(t2,4) + 369*Power(t2,5) + 53*Power(t2,6) + 
                  Power(t2,7))) + 
            Power(t1,4)*(s1*(-72 - 288*t2 + 122*Power(t2,2) + 
                  914*Power(t2,3) - 344*Power(t2,4) - 482*Power(t2,5) + 
                  270*Power(t2,6) + 98*Power(t2,7) + 3*Power(t2,8)) + 
               2*(8 + 124*t2 + 850*Power(t2,2) - 80*Power(t2,3) - 
                  1613*Power(t2,4) - 492*Power(t2,5) + 337*Power(t2,6) + 
                  110*Power(t2,7) + 5*Power(t2,8)))) + 
         s*(32*Power(t1,9) - 3*(-2 + t2)*Power(-1 + t2,3)*Power(t2,6)*
             Power(1 + t2,2) + 
            t1*Power(-1 + t2,2)*Power(t2,4)*
             (60 + (29 + 6*s1)*t2 + (-115 + 7*s1)*Power(t2,2) - 
               (79 + 10*s1)*Power(t2,3) - 5*(-5 + s1)*Power(t2,4) + 
               (11 + 6*s1)*Power(t2,5)) - 
            Power(t1,2)*(-1 + t2)*Power(t2,2)*
             (-180 - 20*(-5 + 3*s1)*t2 - 10*(-60 + s1)*Power(t2,2) + 
               (69 + 185*s1 - 4*Power(s1,2))*Power(t2,3) + 
               (-668 + 3*s1 + 3*Power(s1,2))*Power(t2,4) + 
               (-183 - 151*s1 + Power(s1,2))*Power(t2,5) + 
               (90 + 42*s1 - 5*Power(s1,2))*Power(t2,6) + 
               (22 + 17*s1 + 3*Power(s1,2))*Power(t2,7)) + 
            Power(t1,3)*(72 + 72*(-1 + 3*s1)*t2 - 
               6*(167 + 42*s1)*Power(t2,2) + 
               (338 - 616*s1 + 42*Power(s1,2))*Power(t2,3) + 
               (2580 + 254*s1 - 54*Power(s1,2))*Power(t2,4) + 
               (-321 + 1126*s1 - 10*Power(s1,2))*Power(t2,5) + 
               (-1669 - 655*s1 + 77*Power(s1,2))*Power(t2,6) - 
               (222 + 228*s1 + 83*Power(s1,2))*Power(t2,7) + 
               (187 + 145*s1 + 23*Power(s1,2))*Power(t2,8) + 
               (28 + 20*s1 + 6*Power(s1,2))*Power(t2,9)) - 
            2*Power(t1,8)*(-12 + 24*Power(s1,2) + 116*t2 + 
               31*Power(t2,2) + 8*s1*(10 + 7*t2)) + 
            2*Power(t1,7)*(-452 - 616*t2 + 281*Power(t2,2) + 
               164*Power(t2,3) + 5*Power(t2,4) + 
               6*s1*t2*(-10 + 37*t2 + 13*Power(t2,2)) + 
               Power(s1,2)*(-76 + 56*t2 + 69*Power(t2,2))) - 
            Power(t1,6)*(1056 - 2040*t2 - 3812*Power(t2,2) + 
               10*Power(t2,3) + 752*Power(t2,4) + 79*Power(t2,5) + 
               Power(t2,6) + 
               2*Power(s1,2)*
                (-28 + 44*t2 - 171*Power(t2,2) + 124*Power(t2,3) + 
                  60*Power(t2,4)) + 
               2*s1*(224 + 256*t2 - 764*Power(t2,2) + 152*Power(t2,3) + 
                  246*Power(t2,4) + 21*Power(t2,5))) + 
            Power(t1,5)*(800 + 2360*t2 - 776*Power(t2,2) - 
               5058*Power(t2,3) - 1574*Power(t2,4) + 772*Power(t2,5) + 
               219*Power(t2,6) + 7*Power(t2,7) + 
               Power(s1,2)*(280 - 640*t2 + 598*Power(t2,2) - 
                  294*Power(t2,3) - 198*Power(t2,4) + 
                  224*Power(t2,5) + 33*Power(t2,6)) + 
               s1*(-1472 + 1856*t2 + 2096*Power(t2,2) - 
                  2100*Power(t2,3) - 730*Power(t2,4) + 
                  590*Power(t2,5) + 137*Power(t2,6) + 4*Power(t2,7))) - 
            Power(t1,4)*(-264 - 592*t2 + 3122*Power(t2,2) + 
               1926*Power(t2,3) - 3240*Power(t2,4) - 2370*Power(t2,5) + 
               221*Power(t2,6) + 281*Power(t2,7) + 20*Power(t2,8) + 
               Power(s1,2)*(72 - 40*t2 + 38*Power(t2,2) - 
                  196*Power(t2,3) + 320*Power(t2,4) - 274*Power(t2,5) + 
                  18*Power(t2,6) + 64*Power(t2,7) + 3*Power(t2,8)) + 
               s1*(96 - 976*t2 - 760*Power(t2,2) + 2996*Power(t2,3) - 
                  238*Power(t2,4) - 1437*Power(t2,5) + 221*Power(t2,6) + 
                  198*Power(t2,7) + 13*Power(t2,8)))) + 
         Power(s2,10)*(-Power(s1,3) + 
            Power(s1,2)*(t2*(5 + s - 15*t2 - 16*Power(t2,2)) + 
               t1*(2 + 11*t2 + 10*Power(t2,2))) - 
            s1*(2*Power(t1,2)*(1 + 8*t2 + 6*Power(t2,2)) + 
               t2*(-4 + s*(3 - 17*t2)*t2 - 61*Power(t2,2) + 
                  24*Power(t2,3)) + 
               t1*t2*(16 - 17*t2 - 25*Power(t2,2) + 4*s*(2 + 3*t2))) + 
            t2*(t1*(-4 + 2*(14 + 15*s + Power(s,2))*t2 + 
                  28*(-6 + s)*Power(t2,2) - 183*Power(t2,3)) + 
               2*Power(t1,2)*
                (9 + 39*t2 + 21*Power(t2,2) + s*(2 + 3*t2)) + 
               t2*(-8 - 6*t2 + 2*Power(s,2)*t2 - 94*Power(t2,2) + 
                  184*Power(t2,3) + s*(2 + 4*t2 - 69*Power(t2,2))))) + 
         Power(s2,9)*(-2*Power(t1,3)*
             (3 + s + 39*t2 + 6*s*t2 + 63*Power(t2,2) + 
               3*s*Power(t2,2) + 15*Power(t2,3)) + 
            t1*t2*(16 + (37 - 7*s - 5*Power(s,2))*t2 + 
               (451 + 309*s - 5*Power(s,2))*Power(t2,2) + 
               (-657 + 259*s)*Power(t2,3) - 557*Power(t2,4)) + 
            Power(t1,2)*(2 - 2*(10 + 18*s + 3*Power(s,2))*t2 + 
               (216 - 95*s - 8*Power(s,2))*Power(t2,2) + 
               (725 - 47*s)*Power(t2,3) + 240*Power(t2,4)) + 
            t2*(-2 - 2*(-1 + 4*s + Power(s,2))*t2 + 
               (-59 + 40*s + 6*Power(s,2))*Power(t2,2) + 
               (-91 + 37*s + 20*Power(s,2))*Power(t2,3) - 
               (317 + 264*s)*Power(t2,4) + 392*Power(t2,5)) + 
            Power(s1,3)*(3 - 9*t2 - 2*Power(t2,2) + 
               2*t1*(1 + t2 + Power(t2,2))) + 
            Power(s1,2)*(3 + t2 + 4*(11 + 3*s)*Power(t2,2) - 
               39*Power(t2,3) - 56*Power(t2,4) - 
               Power(t1,2)*(9 + 26*t2 + 18*Power(t2,2)) + 
               t1*(-5 + 22*t2 + 92*Power(t2,2) + 57*Power(t2,3) - 
                  4*s*(1 + t2 + Power(t2,2)))) + 
            s1*(8*Power(t1,3)*(1 + 3*t2 + Power(t2,2)) - 
               t1*(4 + (14 - 17*s - 2*Power(s,2))*t2 + 
                  (221 + 73*s - 2*Power(s,2))*Power(t2,2) + 
                  (33 + 59*s)*Power(t2,3) + 38*Power(t2,4)) + 
               Power(t1,2)*(8 + 2*t2 - 75*Power(t2,2) + Power(t2,3) + 
                  s*(6 + 34*t2 + 26*Power(t2,2))) + 
               t2*(-10 + 27*t2 - 15*Power(t2,2) + 194*Power(t2,3) - 
                  Power(s,2)*t2*(1 + t2) + 
                  s*(1 - 3*t2 - 41*Power(t2,2) + 64*Power(t2,3))))) + 
         Power(s2,8)*(t1*(2 + (3 + 5*s + 5*Power(s,2))*t2 - 
               (-205 + 85*s + 26*Power(s,2) + 3*Power(s,3))*
                Power(t2,2) + 
               (511 - 306*s - 101*Power(s,2) + 3*Power(s,3))*
                Power(t2,3) + 
               (2040 + 1284*s - 101*Power(s,2))*Power(t2,4) + 
               (-1445 + 869*s)*Power(t2,5) - 955*Power(t2,6)) + 
            2*Power(t1,4)*(13 + 63*t2 + 45*Power(t2,2) + 
               4*Power(t2,3) + s*(3 + 6*t2 + Power(t2,2))) - 
            Power(s1,3)*(2 - 19*t2 + 30*Power(t2,2) + 12*Power(t2,3) + 
               Power(t1,2)*(1 + 7*t2 + 6*Power(t2,2)) - 
               t1*(7 + 16*t2 + 16*Power(t2,2) + 11*Power(t2,3))) + 
            Power(t1,3)*(4 - 128*t2 - 1085*Power(t2,2) - 
               1008*Power(t2,3) - 131*Power(t2,4) + 
               Power(s,2)*(4 + 19*t2 + 10*Power(t2,2)) + 
               2*s*(7 + 53*t2 + 70*Power(t2,2) + 21*Power(t2,3))) + 
            Power(t1,2)*(-8 - 56*t2 - 789*Power(t2,2) + 
               576*Power(t2,3) + 2576*Power(t2,4) + 574*Power(t2,5) + 
               Power(s,3)*t2*(1 + 2*t2) + 
               Power(s,2)*t2*(10 + 5*t2 + 6*Power(t2,2)) - 
               s*(2 + 8*t2 + 559*Power(t2,2) + 1112*Power(t2,3) + 
                  388*Power(t2,4))) + 
            t2*(5 - 12*t2 + 54*Power(t2,2) - 191*Power(t2,3) - 
               315*Power(t2,4) - 600*Power(t2,5) + 532*Power(t2,6) - 
               Power(s,3)*Power(t2,2)*(2 + t2) + 
               Power(s,2)*Power(t2,2)*(-14 + 39*t2 + 88*Power(t2,2)) - 
               s*(2 + 4*t2 + 11*Power(t2,2) - 235*Power(t2,3) - 
                  135*Power(t2,4) + 588*Power(t2,5))) + 
            Power(s1,2)*(-9 - (-24 + s)*t2 + (3 - 8*s)*Power(t2,2) + 
               (147 + 53*s)*Power(t2,3) - 30*Power(t2,4) - 
               112*Power(t2,5) + 
               2*Power(t1,3)*(8 + 16*t2 + 7*Power(t2,2)) + 
               Power(t1,2)*(-7 - 153*t2 - 217*Power(t2,2) - 
                  60*Power(t2,3) + s*(7 + 16*t2 + 14*Power(t2,2))) + 
               t1*(-7 - 114*t2 + 30*Power(t2,2) + 303*Power(t2,3) + 
                  129*Power(t2,4) - 
                  s*(-12 + 50*t2 + 35*Power(t2,2) + 21*Power(t2,3)))) - 
            s1*(3 + 2*(1 + 6*s)*t2 - 
               (-78 + 13*s + Power(s,2))*Power(t2,2) + 
               2*(-29 + 9*s + Power(s,2))*Power(t2,3) + 
               2*(40 + 97*s + 4*Power(s,2))*Power(t2,4) - 
               2*(167 + 70*s)*Power(t2,5) - 84*Power(t2,6) + 
               2*Power(t1,4)*(6 + 8*t2 + Power(t2,2)) + 
               Power(t1,3)*(7 - 75*t2 - 31*Power(t2,2) + 
                  29*Power(t2,3) + s*(22 + 54*t2 + 24*Power(t2,2))) + 
               Power(t1,2)*(-14 - 271*t2 - 366*Power(t2,2) - 
                  185*Power(t2,3) - 216*Power(t2,4) + 
                  5*Power(s,2)*(1 + 2*t2 + 2*Power(t2,2)) - 
                  s*(-14 + 79*t2 + 245*Power(t2,2) + 58*Power(t2,3))) + 
               t1*(-10 + 38*t2 + 23*Power(t2,2) + 810*Power(t2,3) + 
                  316*Power(t2,4) + 305*Power(t2,5) - 
                  Power(s,2)*t2*(3 + 25*t2 + 8*Power(t2,2)) + 
                  s*(-5 - 18*t2 - 228*Power(t2,2) + 234*Power(t2,3) + 
                     86*Power(t2,4))))) + 
         Power(s2,7)*(1 + t2 + 12*s*t2 + 34*Power(t2,2) + 
            10*s*Power(t2,2) + 13*Power(s,2)*Power(t2,2) + 
            50*Power(t2,3) - 45*s*Power(t2,3) - 
            29*Power(s,2)*Power(t2,3) + 248*Power(t2,4) + 
            93*s*Power(t2,4) - 59*Power(s,2)*Power(t2,4) - 
            14*Power(s,3)*Power(t2,4) - 359*Power(t2,5) + 
            701*s*Power(t2,5) + 109*Power(s,2)*Power(t2,5) - 
            8*Power(s,3)*Power(t2,5) - 558*Power(t2,6) + 
            252*s*Power(t2,6) + 224*Power(s,2)*Power(t2,6) - 
            695*Power(t2,7) - 840*s*Power(t2,7) + 476*Power(t2,8) - 
            2*Power(t1,5)*(21 + 3*s + 45*t2 + 2*s*t2 + 12*Power(t2,2)) - 
            Power(t1,4)*(-30 - 727*t2 - 1586*Power(t2,2) - 
               575*Power(t2,3) - 24*Power(t2,4) + 
               Power(s,2)*(11 + 18*t2 + 4*Power(t2,2)) + 
               s*(39 + 139*t2 + 111*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s1,3)*(Power(t1,3)*(1 + 9*t2 + 6*Power(t2,2)) - 
               10*t2*(1 - 5*t2 + 5*Power(t2,2) + 3*Power(t2,3)) - 
               Power(t1,2)*(25 + 25*t2 + 55*Power(t2,2) + 
                  23*Power(t2,3)) + 
               t1*(-29 + 53*t2 + 66*Power(t2,2) + 46*Power(t2,3) + 
                  24*Power(t2,4))) - 
            Power(t1,3)*(-25 - 601*t2 - 340*Power(t2,2) + 
               4518*Power(t2,3) + 2875*Power(t2,4) + 225*Power(t2,5) + 
               Power(s,3)*(2 + 6*t2 + 4*Power(t2,2)) + 
               Power(s,2)*(7 + 2*t2 + 5*Power(t2,2) + 9*Power(t2,3)) - 
               s*(11 + 469*t2 + 1851*Power(t2,2) + 1593*Power(t2,3) + 
                  259*Power(t2,4))) + 
            Power(t1,2)*(-5 - 239*t2 - 1092*Power(t2,2) - 
               5088*Power(t2,3) + 568*Power(t2,4) + 4784*Power(t2,5) + 
               740*Power(t2,6) - Power(s,3)*t2*(-7 + Power(t2,2)) + 
               Power(s,2)*t2*
                (47 + 255*t2 + 407*Power(t2,2) + 198*Power(t2,3)) + 
               s*(3 + 66*t2 + 691*Power(t2,2) - 2241*Power(t2,3) - 
                  4229*Power(t2,4) - 1041*Power(t2,5))) + 
            t1*(-5 + 16*t2 - 138*Power(t2,2) + 556*Power(t2,3) + 
               1868*Power(t2,4) + 4425*Power(t2,5) - 1997*Power(t2,6) - 
               1005*Power(t2,7) + 
               Power(s,3)*Power(t2,2)*(7 - 4*t2 + 25*Power(t2,2)) - 
               Power(s,2)*t2*
                (16 - 24*t2 + 118*Power(t2,2) + 532*Power(t2,3) + 
                  403*Power(t2,4)) + 
               s*(-1 - 77*Power(t2,2) - 969*Power(t2,3) - 
                  1598*Power(t2,4) + 2953*Power(t2,5) + 1595*Power(t2,6)\
)) - Power(s1,2)*(-6 + 58*t2 + (-77 + 5*s)*Power(t2,2) + 
               2*(5 + 21*s)*Power(t2,3) - 2*(127 + 61*s)*Power(t2,4) - 
               51*Power(t2,5) + 140*Power(t2,6) + 
               2*Power(t1,4)*(7 + 10*t2 + 2*Power(t2,2)) + 
               Power(t1,3)*(-77 - 299*t2 - 201*Power(t2,2) - 
                  13*Power(t2,3) + s*(6 + 27*t2 + 16*Power(t2,2))) - 
               Power(t1,2)*(67 + 119*t2 - 551*Power(t2,2) - 
                  598*Power(t2,3) - 19*Power(t2,4) + 
                  s*(38 + 83*t2 + 121*Power(t2,2) + 47*Power(t2,3))) + 
               t1*(18 + 39*t2 + 596*Power(t2,2) + 146*Power(t2,3) - 
                  528*Power(t2,4) - 137*Power(t2,5) + 
                  s*(14 - 99*t2 + 243*Power(t2,2) + 100*Power(t2,3) + 
                     41*Power(t2,4)))) + 
            s1*(9 + (-21 + 17*s)*t2 - (11 + 75*s)*Power(t2,2) + 
               (-225 + 97*s + 4*Power(s,2))*Power(t2,3) + 
               (20 - 69*s + 14*Power(s,2))*Power(t2,4) - 
               4*(46 + 118*s + 7*Power(s,2))*Power(t2,5) + 
               2*(169 + 98*s)*Power(t2,6) + 168*Power(t2,7) + 
               4*Power(t1,5)*(2 + t2) + 
               Power(t1,4)*(s*(28 + 38*t2 + 8*Power(t2,2)) + 
                  5*(-5 - 13*t2 + 11*Power(t2,2) + 3*Power(t2,3))) + 
               Power(t1,3)*(-111 - 529*t2 - 447*Power(t2,2) - 
                  776*Power(t2,3) - 215*Power(t2,4) + 
                  2*Power(s,2)*(4 + 12*t2 + 7*Power(t2,2)) - 
                  s*(23 + 345*t2 + 220*Power(t2,2) + 4*Power(t2,3))) + 
               Power(t1,2)*(11 + 125*t2 + 1335*Power(t2,2) + 
                  1341*Power(t2,3) + 1476*Power(t2,4) + 
                  646*Power(t2,5) - 
                  Power(s,2)*
                   (-13 + 63*t2 + 69*Power(t2,2) + 23*Power(t2,3)) + 
                  s*(-21 - 375*t2 + 93*Power(t2,2) + 461*Power(t2,3) - 
                     150*Power(t2,4))) + 
               t1*(8 + 202*t2 + 9*Power(t2,2) + 302*Power(t2,3) - 
                  1268*Power(t2,4) - 795*Power(t2,5) - 640*Power(t2,6) + 
                  2*Power(s,2)*t2*(-8 + 2*t2 + 47*Power(t2,2)) + 
                  s*(-12 - 9*t2 + 156*Power(t2,2) + 1008*Power(t2,3) - 
                     371*Power(t2,4) + 56*Power(t2,5))))) + 
         Power(s2,6)*(-3 + 6*t2 - 16*s*t2 + 10*Power(t2,2) + 
            58*s*Power(t2,2) - 17*Power(s,2)*Power(t2,2) + 
            11*Power(t2,3) - 84*s*Power(t2,3) + 
            63*Power(s,2)*Power(t2,3) + 4*Power(s,3)*Power(t2,3) + 
            254*Power(t2,4) - 201*s*Power(t2,4) - 
            147*Power(s,2)*Power(t2,4) + 3*Power(s,3)*Power(t2,4) + 
            488*Power(t2,5) + 391*s*Power(t2,5) - 
            180*Power(s,2)*Power(t2,5) - 41*Power(s,3)*Power(t2,5) - 
            439*Power(t2,6) + 1246*s*Power(t2,6) + 
            174*Power(s,2)*Power(t2,6) - 28*Power(s,3)*Power(t2,6) - 
            577*Power(t2,7) + 245*s*Power(t2,7) + 
            364*Power(s,2)*Power(t2,7) - 502*Power(t2,8) - 
            798*s*Power(t2,8) + 280*Power(t2,9) + 
            2*Power(t1,6)*(15 + s + 12*t2) + 
            t1*(-3 + (-90 - 11*s + 15*Power(s,2))*t2 + 
               (-248 + 135*s + 69*Power(s,2) + 13*Power(s,3))*
                Power(t2,2) + 
               (-1108 - 247*s + 245*Power(s,2) + 49*Power(s,3))*
                Power(t2,3) + 
               (831 - 3683*s - 142*Power(s,2) + 43*Power(s,3))*
                Power(t2,4) + 
               (3394 - 3626*s - 1386*Power(s,2) + 91*Power(s,3))*
                Power(t2,5) + 
               (5355 + 4244*s - 829*Power(s,2))*Power(t2,6) + 
               (-1813 + 1793*s)*Power(t2,7) - 661*Power(t2,8)) + 
            Power(t1,5)*(-184 - 1108*t2 - 939*Power(t2,2) - 
               112*Power(t2,3) + Power(s,2)*(8 + 5*t2) + 
               s*(46 + 96*t2 + 36*Power(t2,2))) + 
            Power(t1,4)*(-169 - 700*t2 + 3700*Power(t2,2) + 
               5688*Power(t2,3) + 1197*Power(t2,4) + 24*Power(t2,5) + 
               Power(s,3)*(3 + 7*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(2 - 27*t2 + 27*Power(t2,2) + 
                  6*Power(t2,3)) - 
               s*(150 + 1392*t2 + 2504*Power(t2,2) + 1019*Power(t2,3) + 
                  61*Power(t2,4))) + 
            Power(s1,3)*(-(Power(t1,4)*(2 + 5*t2 + 2*Power(t2,2))) - 
               5*Power(t2,2)*
                (4 - 14*t2 + 9*Power(t2,2) + 8*Power(t2,3)) + 
               Power(t1,3)*(17 + 67*t2 + 65*Power(t2,2) + 
                  13*Power(t2,3)) - 
               Power(t1,2)*(-2 + 147*t2 + 104*Power(t2,2) + 
                  146*Power(t2,3) + 23*Power(t2,4)) + 
               5*t1*(4 - 25*t2 + 22*Power(t2,2) + 32*Power(t2,3) + 
                  12*Power(t2,4) + 5*Power(t2,5))) - 
            Power(t1,3)*(-93 - 1011*t2 - 6300*Power(t2,2) - 
               4411*Power(t2,3) + 9044*Power(t2,4) + 4009*Power(t2,5) + 
               190*Power(t2,6) + 
               Power(s,3)*(-2 + 7*t2 + 4*Power(t2,2) + 3*Power(t2,3)) + 
               Power(s,2)*(27 + 286*t2 + 690*Power(t2,2) + 
                  753*Power(t2,3) + 169*Power(t2,4)) - 
               s*(-21 - 580*t2 + 1694*Power(t2,2) + 8042*Power(t2,3) + 
                  4951*Power(t2,4) + 503*Power(t2,5))) + 
            Power(t1,2)*(-4 + 97*t2 - 545*Power(t2,2) - 
               4081*Power(t2,3) - 13056*Power(t2,4) + 314*Power(t2,5) + 
               5163*Power(t2,6) + 550*Power(t2,7) - 
               Power(s,3)*t2*
                (23 - 12*t2 + 73*Power(t2,2) + 50*Power(t2,3)) + 
               Power(s,2)*(1 - 44*t2 + 157*Power(t2,2) + 
                  1240*Power(t2,3) + 1899*Power(t2,4) + 638*Power(t2,5)\
) + s*(7 + 221*t2 + 1588*Power(t2,2) + 5107*Power(t2,3) - 
                  4557*Power(t2,4) - 8187*Power(t2,5) - 1450*Power(t2,6)\
)) + Power(s1,2)*(Power(t1,5)*(6 + 5*t2) + 
               t2*(30 - 155*t2 - 2*(-67 + 5*s)*Power(t2,2) - 
                  30*(2 + 3*s)*Power(t2,3) + 
                  15*(17 + 11*s)*Power(t2,4) + 138*Power(t2,5) - 
                  112*Power(t2,6)) + 
               Power(t1,4)*(-139 - 223*t2 - 44*Power(t2,2) + 
                  6*Power(t2,3) + s*(7 + 17*t2 + 6*Power(t2,2))) - 
               Power(t1,3)*(98 - 414*t2 - 1065*Power(t2,2) - 
                  169*Power(t2,3) + 104*Power(t2,4) + 
                  s*(84 + 158*t2 + 152*Power(t2,2) + 29*Power(t2,3))) + 
               Power(t1,2)*(69 + 749*t2 + 902*Power(t2,2) - 
                  913*Power(t2,3) - 709*Power(t2,4) + 
                  176*Power(t2,5) + 
                  s*(-130 + 345*t2 + 280*Power(t2,2) + 
                     301*Power(t2,3) + 11*Power(t2,4))) + 
               t1*(88 - 120*t2 + 22*Power(t2,2) - 1215*Power(t2,3) - 
                  544*Power(t2,4) + 541*Power(t2,5) + 45*Power(t2,6) - 
                  s*(-6 + 73*t2 - 403*Power(t2,2) + 635*Power(t2,3) + 
                     109*Power(t2,4) + 29*Power(t2,5)))) - 
            s1*(6 + 2*Power(t1,6) + (-59 + 6*s)*t2 + 
               (62 - 98*s)*Power(t2,2) + 
               (-69 + 185*s + 5*Power(s,2))*Power(t2,3) + 
               (327 - 329*s - 6*Power(s,2))*Power(t2,4) + 
               (107 + 186*s - 70*Power(s,2))*Power(t2,5) + 
               (243 + 676*s + 56*Power(s,2))*Power(t2,6) - 
               26*(8 + 7*s)*Power(t2,7) - 168*Power(t2,8) + 
               Power(t1,5)*(-33 + 23*t2 + 36*Power(t2,2) + 
                  2*s*(7 + 5*t2)) + 
               Power(t1,4)*(-220 - 475*t2 - 1040*Power(t2,2) - 
                  795*Power(t2,3) - 61*Power(t2,4) + 
                  Power(s,2)*(8 + 19*t2 + 6*Power(t2,2)) + 
                  s*(-159 - 308*t2 - 17*Power(t2,2) + 12*Power(t2,3))) + 
               Power(t1,3)*(87 + 1088*t2 + 2248*Power(t2,2) + 
                  2874*Power(t2,3) + 2871*Power(t2,4) + 
                  412*Power(t2,5) - 
                  Power(s,2)*
                   (51 + 105*t2 + 91*Power(t2,2) + 19*Power(t2,3)) - 
                  s*(176 + 342*t2 - 851*Power(t2,2) + 444*Power(t2,3) + 
                     273*Power(t2,4))) + 
               Power(t1,2)*(121 + 307*t2 + 514*Power(t2,2) - 
                  1868*Power(t2,3) - 1873*Power(t2,4) - 
                  3190*Power(t2,5) - 865*Power(t2,6) + 
                  Power(s,2)*
                   (12 - 79*t2 + 232*Power(t2,2) + 115*Power(t2,3) - 
                     62*Power(t2,4)) + 
                  s*(4 + 276*t2 + 2017*Power(t2,2) + 253*Power(t2,3) + 
                     130*Power(t2,4) + 724*Power(t2,5))) + 
               t1*(-15 - 52*t2 - 891*Power(t2,2) - 48*Power(t2,3) - 
                  1123*Power(t2,4) + 820*Power(t2,5) + 
                  1078*Power(t2,6) + 697*Power(t2,7) + 
                  Power(s,2)*t2*
                   (-17 + 102*t2 + 52*Power(t2,2) - 174*Power(t2,3) + 
                     59*Power(t2,4)) - 
                  s*(13 + 48*t2 - 405*Power(t2,2) + 384*Power(t2,3) + 
                     2236*Power(t2,4) - 334*Power(t2,5) + 350*Power(t2,6)\
)))) - Power(s2,5)*(-2 + 8*Power(t1,7) + (20 - 6*s + 30*s1)*t2 - 
            (15 - 89*s + 6*Power(s,2) + 160*s1 - 30*s*s1 + 
               60*Power(s1,2))*Power(t2,2) + 
            (82 - 191*s + 18*Power(s,2) + 2*Power(s,3) + 160*s1 - 
               157*s*s1 + 198*Power(s1,2) + 20*Power(s1,3))*Power(t2,3) \
+ (68 - 19*Power(s,3) - 290*s1 - 133*Power(s1,2) - 55*Power(s1,3) + 
               Power(s,2)*(-134 + 25*s1) + 
               2*s*(211 + 105*s1 + 5*Power(s1,2)))*Power(t2,4) + 
            (-431 - 18*Power(s,3) + Power(s,2)*(316 - 5*s1) + 268*s1 + 
               115*Power(s1,2) + 21*Power(s1,3) + 
               s*(474 - 585*s1 + 100*Power(s1,2)))*Power(t2,5) + 
            (-472 + 64*Power(s,3) + 191*s1 - 160*Power(s1,2) + 
               30*Power(s1,3) - 7*Power(s,2)*(-53 + 20*s1) + 
               s*(-734 + 315*s1 - 136*Power(s1,2)))*Power(t2,6) + 
            (372 + 56*Power(s,3) + 205*s1 - 135*Power(s1,2) + 
               Power(s,2)*(-183 + 70*s1) + s*(-1389 + 598*s1))*
             Power(t2,7) - 2*
             (-173 + 196*Power(s,2) + 43*s1 - 28*Power(s1,2) + 
               14*s*(3 + 4*s1))*Power(t2,8) + 
            3*(73 + 168*s - 32*s1)*Power(t2,9) - 104*Power(t2,10) + 
            Power(t1,6)*(-290 + Power(s,2) + Power(s1,2) + 
               s1*(3 - 27*t2) - 677*t2 - 192*Power(t2,2) + 
               s*(-2*s1 + 27*(1 + t2))) + 
            Power(t1,5)*(-257 + 1365*t2 + 5563*Power(t2,2) + 
               2547*Power(t2,3) + 144*Power(t2,4) - 
               Power(s1,3)*(1 + t2) + Power(s,3)*(2 + t2) + 
               Power(s1,2)*(-82 - 21*t2 + 17*Power(t2,2)) + 
               s1*(175 + 610*t2 + 1053*Power(t2,2) + 232*Power(t2,3)) + 
               Power(s,2)*(-26 + 20*t2 + 17*Power(t2,2) - 
                  s1*(5 + 3*t2)) + 
               s*(-394 - 1775*t2 - 1511*Power(t2,2) - 232*Power(t2,3) + 
                  Power(s1,2)*(4 + 3*t2) + 
                  s1*(146 + t2 - 34*Power(t2,2)))) - 
            Power(t1,4)*(-339 - 3903*t2 - 8075*Power(t2,2) + 
               7340*Power(t2,3) + 8696*Power(t2,4) + 1086*Power(t2,5) + 
               8*Power(t2,6) + 
               Power(s,3)*(14 + 11*t2 + Power(t2,2) + Power(t2,3)) - 
               Power(s1,3)*(10 + 58*t2 + 19*Power(t2,2) + 
                  Power(t2,3)) + 
               Power(s1,2)*(-113 - 950*t2 - 421*Power(t2,2) + 
                  335*Power(t2,3) + 50*Power(t2,4)) + 
               s1*(369 + 1739*t2 + 2998*Power(t2,2) + 
                  5064*Power(t2,3) + 1844*Power(t2,4) + 74*Power(t2,5)) \
+ Power(s,2)*(106 + 602*t2 + 1036*Power(t2,2) + 623*Power(t2,3) + 
                  50*Power(t2,4) - 
                  s1*(74 + 118*t2 + 21*Power(t2,2) + 3*Power(t2,3))) + 
               s*(158 - 523*t2 - 7478*Power(t2,2) - 9487*Power(t2,3) - 
                  2328*Power(t2,4) - 74*Power(t2,5) + 
                  Power(s1,2)*
                   (58 + 165*t2 + 39*Power(t2,2) + 3*Power(t2,3)) - 
                  s1*(253 - 684*t2 + 326*Power(t2,2) + 
                     958*Power(t2,3) + 100*Power(t2,4)))) + 
            Power(t1,3)*(13 - 222*t2 - 4285*Power(t2,2) - 
               19763*Power(t2,3) - 9626*Power(t2,4) + 
               10063*Power(t2,5) + 2980*Power(t2,6) + 80*Power(t2,7) + 
               Power(s1,3)*(-109 - 101*t2 - 272*Power(t2,2) - 
                  96*Power(t2,3) + 14*Power(t2,4)) - 
               Power(s,3)*(4 - 11*t2 + 97*Power(t2,2) + 
                  173*Power(t2,3) + 41*Power(t2,4)) + 
               Power(s1,2)*(324 + 1293*t2 - 590*Power(t2,2) - 
                  1528*Power(t2,3) + 558*Power(t2,4) + 269*Power(t2,5)) \
+ s1*(-228 - 457*t2 + 1341*Power(t2,2) + 1678*Power(t2,3) + 
                  5892*Power(t2,4) + 4114*Power(t2,5) + 358*Power(t2,6)\
) + Power(s,2)*(-22 + 162*t2 + 1394*Power(t2,2) + 3561*Power(t2,3) + 
                  2758*Power(t2,4) + 396*Power(t2,5) + 
                  s1*(147 - 305*t2 - 267*Power(t2,2) + 
                     171*Power(t2,3) + 96*Power(t2,4))) - 
               s*(-127 - 1292*t2 - 7221*Power(t2,2) + 768*Power(t2,3) + 
                  16251*Power(t2,4) + 7258*Power(t2,5) + 
                  472*Power(t2,6) + 
                  Power(s1,2)*
                   (-86 - 407*t2 - 579*Power(t2,2) - 98*Power(t2,3) + 
                     69*Power(t2,4)) + 
                  s1*(210 + 1753*t2 + 1871*Power(t2,2) - 
                     119*Power(t2,3) + 2862*Power(t2,4) + 
                     665*Power(t2,5)))) + 
            Power(t1,2)*(-55 - 402*t2 - 1908*Power(t2,2) - 
               457*Power(t2,3) + 7544*Power(t2,4) + 16724*Power(t2,5) - 
               528*Power(t2,6) - 3325*Power(t2,7) - 232*Power(t2,8) + 
               3*Power(s,3)*t2*
                (-1 + 35*t2 + 30*Power(t2,2) + 121*Power(t2,3) + 
                  57*Power(t2,4)) + 
               Power(s1,3)*(-80 + 30*t2 + 380*Power(t2,2) + 
                  172*Power(t2,3) + 174*Power(t2,4) - 20*Power(t2,5)) + 
               Power(s1,2)*(32 - 120*t2 - 2044*Power(t2,2) - 
                  2076*Power(t2,3) + 937*Power(t2,4) + 
                  271*Power(t2,5) - 325*Power(t2,6)) + 
               s1*(74 + 1124*t2 + 773*Power(t2,2) + 2329*Power(t2,3) + 
                  277*Power(t2,4) - 1311*Power(t2,5) - 
                  3435*Power(t2,6) - 613*Power(t2,7)) - 
               Power(s,2)*(-1 - 12*t2 - 527*Power(t2,2) - 
                  360*Power(t2,3) + 2834*Power(t2,4) + 
                  4058*Power(t2,5) + 990*Power(t2,6) + 
                  2*s1*(2 + 51*t2 + 116*Power(t2,2) - 
                     197*Power(t2,3) + 44*Power(t2,4) + 157*Power(t2,5)\
)) + s*(-1 + 130*t2 - 429*Power(t2,2) - 7107*Power(t2,3) - 
                  13672*Power(t2,4) + 6017*Power(t2,5) + 
                  9192*Power(t2,6) + 1169*Power(t2,7) + 
                  Power(s1,2)*
                   (-144 + 819*t2 - 1207*Power(t2,2) - 
                     358*Power(t2,3) - 314*Power(t2,4) + 
                     163*Power(t2,5)) + 
                  s1*(-105 - 564*t2 + 587*Power(t2,2) + 
                     4511*Power(t2,3) + 462*Power(t2,4) + 
                     1513*Power(t2,5) + 1160*Power(t2,6)))) - 
            t1*(-4 - 29*t2 - 110*Power(t2,2) - 899*Power(t2,3) - 
               2726*Power(t2,4) + 1019*Power(t2,5) + 3511*Power(t2,6) + 
               3750*Power(t2,7) - 1083*Power(t2,8) - 263*Power(t2,9) + 
               Power(s,3)*Power(t2,2)*
                (-29 + 15*t2 + 130*Power(t2,2) + 178*Power(t2,3) + 
                  189*Power(t2,4)) + 
               2*Power(s1,3)*t2*
                (30 - 100*t2 + 42*Power(t2,2) + 115*Power(t2,3) + 
                  15*Power(t2,4) + 5*Power(t2,5)) - 
               Power(s1,2)*(60 - 390*t2 + 272*Power(t2,2) - 
                  170*Power(t2,3) + 1151*Power(t2,4) + 
                  772*Power(t2,5) - 336*Power(t2,6) + 45*Power(t2,7)) + 
               s1*(-89 + 77*t2 - 231*Power(t2,2) + 1489*Power(t2,3) - 
                  54*Power(t2,4) + 1738*Power(t2,5) + 33*Power(t2,6) - 
                  913*Power(t2,7) - 430*Power(t2,8)) - 
               Power(s,2)*t2*
                (4 + 64*t2 - 389*Power(t2,2) - 1031*Power(t2,3) - 
                  169*Power(t2,4) + 2148*Power(t2,5) + 
                  1033*Power(t2,6) + 
                  s1*(6 - 113*t2 + 167*Power(t2,2) + 238*Power(t2,3) - 
                     202*Power(t2,4) + 158*Power(t2,5))) + 
               s*(1 - 97*t2 + 284*Power(t2,2) + 1007*Power(t2,3) - 
                  767*Power(t2,4) - 7170*Power(t2,5) - 
                  4357*Power(t2,6) + 4015*Power(t2,7) + 
                  1273*Power(t2,8) + 
                  Power(s1,2)*t2*
                   (30 - 175*t2 + 772*Power(t2,2) - 988*Power(t2,3) + 
                     10*Power(t2,4) + 15*Power(t2,5)) + 
                  s1*(-6 - 100*t2 + 318*Power(t2,2) - 1315*Power(t2,3) + 
                     692*Power(t2,4) + 2805*Power(t2,5) - 
                     187*Power(t2,6) + 518*Power(t2,7))))) + 
         Power(s2,4)*(2*Power(t1,7)*(-91 + 3*s - 3*s1 - 72*t2) + 
            t2*(10 + 5*(-11 + 6*s - 12*s1)*t2 + 
               (108 - 79*s - 6*Power(s,2) + 152*s1 - 24*s*s1 + 
                  48*Power(s1,2))*Power(t2,2) - 
               (257 - 75*Power(s,2) + 10*Power(s,3) + 191*s1 + 
                  105*Power(s1,2) + 10*Power(s1,3) - s*(277 + 32*s1))*
                Power(t2,3) + 
               (-73 + 35*Power(s,3) + Power(s,2)*(190 - 50*s1) + 
                  388*s1 + 68*Power(s1,2) + 23*Power(s1,3) - 
                  s*(688 + 90*s1 + 5*Power(s1,2)))*Power(t2,4) + 
               (431 + 45*Power(s,3) + 5*Power(s,2)*(-76 + s1) - 
                  142*s1 - 109*Power(s1,2) - 4*Power(s1,3) + 
                  s*(-632 + 591*s1 - 60*Power(s1,2)))*Power(t2,5) + 
               (188 - 55*Power(s,3) - 148*s1 + 69*Power(s1,2) - 
                  12*Power(s1,3) + 2*Power(s,2)*(-246 + 77*s1) + 
                  s*(832 - 318*s1 + 67*Power(s1,2)))*Power(t2,6) - 
               2*(112 + 35*Power(s,3) + 55*s1 - 33*Power(s1,2) + 
                  Power(s,2)*(-73 + 28*s1) + s*(-480 + 163*s1))*
                Power(t2,7) + 
               (-101 + 280*Power(s,2) + 34*s1 - 16*Power(s1,2) + 
                  s*(-63 + 44*s1))*Power(t2,8) + 
               (-52 - 204*s + 30*s1)*Power(t2,9) + 22*Power(t2,10)) + 
            t1*(30 + 5*(-2 + 33*s)*t2 + 
               (248 - 495*s - 60*Power(s,2) + 12*Power(s,3))*
                Power(t2,2) + 
               (407 + 1264*s - 337*Power(s,2) - 83*Power(s,3))*
                Power(t2,3) + 
               (-1539 + 2954*s + 758*Power(s,2) - 72*Power(s,3))*
                Power(t2,4) + 
               (-2768 - 1871*s + 2180*Power(s,2) + 148*Power(s,3))*
                Power(t2,5) + 
               (1042 - 8096*s + 633*Power(s,2) + 315*Power(s,3))*
                Power(t2,6) + 
               (2069 - 2799*s - 2130*Power(s,2) + 245*Power(s,3))*
                Power(t2,7) + 
               (1454 + 2524*s - 815*Power(s,2))*Power(t2,8) + 
               (-407 + 559*s)*Power(t2,9) - 57*Power(t2,10) + 
               Power(s1,3)*Power(t2,2)*
                (60 - 166*t2 + 17*Power(t2,2) + 192*Power(t2,3) - 
                  8*Power(t2,4) - 3*Power(t2,5)) + 
               Power(s1,2)*t2*
                (-180 + (574 + 60*s)*t2 - 2*(137 + 64*s)*Power(t2,2) + 
                  (223 + 712*s)*Power(t2,3) - 
                  2*(253 + 468*s)*Power(t2,4) + 
                  (-554 + 131*s)*Power(t2,5) + 
                  (117 + 41*s)*Power(t2,6) - 53*Power(t2,7)) + 
               s1*(60 + 15*(-27 + 2*s)*t2 + 
                  (330 - 251*s - 30*Power(s,2))*Power(t2,2) + 
                  (-777 + 398*s + 180*Power(s,2))*Power(t2,3) + 
                  (928 - 2055*s - 67*Power(s,2))*Power(t2,4) + 
                  (-91 + 1168*s - 456*Power(s,2))*Power(t2,5) + 
                  (1585 + 1975*s + 192*Power(s,2))*Power(t2,6) + 
                  (287 - 48*s - 213*Power(s,2))*Power(t2,7) + 
                  (-508 + 406*s)*Power(t2,8) - 143*Power(t2,9))) + 
            Power(t1,6)*(164 + 2692*t2 + 2709*Power(t2,2) + 
               344*Power(t2,3) + Power(s,2)*(2 + 29*t2) + 
               Power(s1,2)*(10 + 29*t2) + 
               s1*(130 + 581*t2 + 315*Power(t2,2)) - 
               s*(2*s1*(6 + 29*t2) + 7*(68 + 143*t2 + 45*Power(t2,2)))) \
+ Power(t1,5)*(968 + 5515*t2 - 1313*Power(t2,2) - 9447*Power(t2,3) - 
               2474*Power(t2,4) - 56*Power(t2,5) + 
               Power(s1,3)*(6 - 11*t2 - 5*Power(t2,2)) + 
               Power(s,3)*(-8 - 2*t2 + 5*Power(t2,2)) + 
               Power(s1,2)*(348 + 472*t2 - 377*Power(t2,2) - 
                  170*Power(t2,3)) - 
               s1*(516 + 1697*t2 + 4515*Power(t2,2) + 
                  3302*Power(t2,3) + 340*Power(t2,4)) - 
               Power(s,2)*(218 + 584*t2 + 860*Power(t2,2) + 
                  170*Power(t2,3) + s1*(-26 + 7*t2 + 15*Power(t2,2))) + 
               s*(50 + 3380*t2 + 9209*Power(t2,2) + 4359*Power(t2,3) + 
                  340*Power(t2,4) + 
                  Power(s1,2)*(-24 + 20*t2 + 15*Power(t2,2)) + 
                  s1*(-202 - 226*t2 + 1237*Power(t2,2) + 
                     340*Power(t2,3)))) + 
            Power(t1,4)*(-42 - 2225*t2 - 16380*Power(t2,2) - 
               20241*Power(t2,3) + 8199*Power(t2,4) + 
               6462*Power(t2,5) + 434*Power(t2,6) + 
               Power(s1,3)*(-92 - 194*t2 - 178*Power(t2,2) + 
                  66*Power(t2,3) + 13*Power(t2,4)) - 
               Power(s,3)*(36 + 54*t2 + 204*Power(t2,2) + 
                  158*Power(t2,3) + 13*Power(t2,4)) + 
               Power(s1,2)*(528 + 75*t2 - 1745*Power(t2,2) + 
                  592*Power(t2,3) + 1001*Power(t2,4) + 76*Power(t2,5)) \
+ s1*(-162 + 655*t2 + 463*Power(t2,2) + 4736*Power(t2,3) + 
                  7665*Power(t2,4) + 1678*Power(t2,5) + 36*Power(t2,6)) \
+ Power(s,2)*(s1*(-120 - 230*t2 + 64*Power(t2,2) + 382*Power(t2,3) + 
                     39*Power(t2,4)) + 
                  2*(57 + 350*t2 + 1709*Power(t2,2) + 
                     2383*Power(t2,3) + 803*Power(t2,4) + 
                     38*Power(t2,5))) - 
               s*(-432 - 4832*t2 - 4357*Power(t2,2) + 
                  15139*Power(t2,3) + 14566*Power(t2,4) + 
                  2272*Power(t2,5) + 36*Power(t2,6) + 
                  Power(s1,2)*
                   (-320 - 418*t2 - 318*Power(t2,2) + 
                     290*Power(t2,3) + 39*Power(t2,4)) + 
                  s1*(550 + 2267*t2 - 493*Power(t2,2) + 
                     4422*Power(t2,3) + 2607*Power(t2,4) + 
                     152*Power(t2,5)))) + 
            Power(t1,3)*(-200 - 1493*t2 - 2813*Power(t2,2) + 
               7379*Power(t2,3) + 27008*Power(t2,4) + 
               7955*Power(t2,5) - 6445*Power(t2,6) - 1162*Power(t2,7) - 
               15*Power(t2,8) + 
               Power(s,3)*t2*
                (156 + 33*t2 + 590*Power(t2,2) + 663*Power(t2,3) + 
                  118*Power(t2,4)) + 
               Power(s1,3)*(-90 + 465*t2 + 236*Power(t2,2) + 
                  412*Power(t2,3) - 30*Power(t2,4) - 67*Power(t2,5)) - 
               Power(s1,2)*(180 + 1530*t2 + 3559*Power(t2,2) - 
                  645*Power(t2,3) - 940*Power(t2,4) + 
                  1188*Power(t2,5) + 266*Power(t2,6)) + 
               s1*(482 + 1446*t2 + 2438*Power(t2,2) + 
                  2342*Power(t2,3) + 2295*Power(t2,4) - 
                  6073*Power(t2,5) - 2843*Power(t2,6) - 149*Power(t2,7)\
) + Power(s,2)*(38 + 664*t2 + 883*Power(t2,2) - 2351*Power(t2,3) - 
                  7621*Power(t2,4) - 4374*Power(t2,5) - 
                  424*Power(t2,6) + 
                  s1*(122 - 615*t2 + 666*Power(t2,2) + 
                     102*Power(t2,3) - 1122*Power(t2,4) - 
                     303*Power(t2,5))) + 
               s*(4 - 477*t2 - 6267*Power(t2,2) - 23340*Power(t2,3) - 
                  2467*Power(t2,4) + 18038*Power(t2,5) + 
                  5682*Power(t2,6) + 232*Power(t2,7) + 
                  Power(s1,2)*
                   (464 - 862*t2 - 713*Power(t2,2) - 841*Power(t2,3) + 
                     489*Power(t2,4) + 252*Power(t2,5)) + 
                  s1*(-394 + 510*t2 + 4565*Power(t2,2) + 
                     2276*Power(t2,3) + 1833*Power(t2,4) + 
                     4766*Power(t2,5) + 690*Power(t2,6)))) + 
            Power(t1,2)*(30 + 
               (199 - 379*s - 125*Power(s,2) + 24*Power(s,3))*t2 - 
               (-1420 + 2143*s + 253*Power(s,2) + 7*Power(s,3))*
                Power(t2,2) - 
               (-5542 + 403*s + 2229*Power(s,2) + 190*Power(s,3))*
                Power(t2,3) + 
               (1241 + 14951*s - 2506*Power(s,2) - 349*Power(s,3))*
                Power(t2,4) + 
               (-8109 + 17848*s + 3761*Power(s,2) - 798*Power(s,3))*
                Power(t2,5) - 
               (11304 + 5789*s - 4894*Power(s,2) + 280*Power(s,3))*
                Power(t2,6) + 
               (782 - 6214*s + 882*Power(s,2))*Power(t2,7) + 
               (1238 - 552*s)*Power(t2,8) + 50*Power(t2,9) + 
               Power(s1,3)*(-60 + 180*t2 + 38*Power(t2,2) - 
                  556*Power(t2,3) - 129*Power(t2,4) - 91*Power(t2,5) + 
                  60*Power(t2,6)) + 
               Power(s1,2)*(-250 + 2*t2 - 75*Power(t2,2) + 
                  2032*Power(t2,3) + 2207*Power(t2,4) - 
                  751*Power(t2,5) + 188*Power(t2,6) + 252*Power(t2,7) + 
                  s*(-60 + 540*t2 - 1931*Power(t2,2) + 
                     2256*Power(t2,3) + 57*Power(t2,4) + 
                     114*Power(t2,5) - 315*Power(t2,6))) + 
               s1*(60 + 140*t2 - 2371*Power(t2,2) + 641*Power(t2,3) - 
                  3476*Power(t2,4) - 2694*Power(t2,5) + 
                  755*Power(t2,6) + 2029*Power(t2,7) + 226*Power(t2,8) + 
                  Power(s,2)*t2*
                   (-180 + 328*t2 + 456*Power(t2,2) - 409*Power(t2,3) + 
                     490*Power(t2,4) + 535*Power(t2,5)) - 
                  s*(130 - 75*t2 - 2226*Power(t2,2) + 1030*Power(t2,3) + 
                     4975*Power(t2,4) - 84*Power(t2,5) + 
                     2220*Power(t2,6) + 974*Power(t2,7))))) + 
         Power(s2,2)*(Power(t1,8)*(306 - 34*s + 34*s1 + 240*t2) + 
            Power(t2,3)*(-20 + (65 - 60*s + 30*s1)*t2 - 
               (6 + 60*Power(s,2) + 79*s1 + 6*Power(s1,2) - 
                  10*s*(17 + 3*s1))*Power(t2,2) + 
               (-116 + 115*Power(s,2) - 20*Power(s,3) + 
                  s*(88 - 82*s1) + 16*s1 + 13*Power(s1,2))*Power(t2,3) \
+ (11 + 10*Power(s,3) + Power(s,2)*(167 - 25*s1) + 81*s1 - 
                  2*Power(s1,2) + 5*s*(-64 + 3*s1))*Power(t2,4) + 
               (140 + 45*Power(s,3) - 37*s1 - 10*Power(s1,2) + 
                  Power(s,2)*(-139 + 4*s1) + 
                  s*(-179 + 107*s1 - 2*Power(s1,2)))*Power(t2,5) + 
               (-62 + Power(s,3) + 34*Power(s,2)*(-6 + s1) - 15*s1 + 
                  4*Power(s1,2) + s*(283 - 54*s1 + 2*Power(s1,2)))*
                Power(t2,6) + 
               (-19 - 28*Power(s,3) + s*(56 - 17*s1) + 
                  Power(s,2)*(54 - 8*s1) + s1 + Power(s1,2))*
                Power(t2,7) + 
               (7 + 34*Power(s,2) + s*(-33 + s1) + 3*s1)*Power(t2,8) - 
               5*s*Power(t2,9)) + 
            t1*t2*(-60 + 12*(5 + 3*s + 3*s1)*t2 + 
               2*(-172 + 72*Power(s,2) + 7*s1 - 24*Power(s1,2) - 
                  s*(173 + 6*s1))*Power(t2,2) + 
               (773 + 60*Power(s,3) + 277*s1 + 102*Power(s1,2) + 
                  12*Power(s1,3) + Power(s,2)*(-446 + 60*s1) + 
                  s*(-836 + 139*s1 - 30*Power(s1,2)))*Power(t2,3) - 
               (-314 + 40*Power(s,3) + Power(s,2)*(989 - 85*s1) + 
                  570*s1 + 82*Power(s1,2) + 25*Power(s1,3) + 
                  s*(-1795 + 36*s1 - 85*Power(s1,2)))*Power(t2,4) + 
               (-1152 - 225*Power(s,3) + Power(s,2)*(451 - 50*s1) + 
                  55*s1 + 46*Power(s1,2) + 2*Power(s1,3) + 
                  s*(2495 - 759*s1 + 49*Power(s1,2)))*Power(t2,5) + 
               (38 - 95*Power(s,3) + Power(s,2)*(1875 - 232*s1) - 
                  92*s1 + 19*Power(s1,2) + 16*Power(s1,3) + 
                  s*(-1837 + 632*s1 - 155*Power(s1,2)))*Power(t2,6) + 
               (327 + 181*Power(s,3) + 345*s1 - 22*Power(s1,2) - 
                  4*Power(s1,3) + 6*Power(s,2)*(50 + 19*s1) + 
                  s*(-1883 - 54*s1 + 45*Power(s1,2)))*Power(t2,7) + 
               (48 + 105*Power(s,3) - 30*s1 - 12*Power(s1,2) - 
                  Power(s1,3) - Power(s,2)*(562 + 85*s1) + 
                  s*(80 + 74*s1 + 9*Power(s1,2)))*Power(t2,8) + 
               (3 - 113*Power(s,2) - 35*s1 - 3*Power(s1,2) + 
                  s*(238 + 46*s1))*Power(t2,9) + 
               (-7 + 15*s)*Power(t2,10)) + 
            Power(t1,2)*(-90 - 12*(-12 + 11*s + 9*Power(s,2))*t2 - 
               6*(98 - 178*s - 73*Power(s,2) + 6*Power(s,3))*
                Power(t2,2) + 
               2*(-860 - 1049*s + 617*Power(s,2) + 36*Power(s,3))*
                Power(t2,3) + 
               (2538 - 8567*s + 315*Power(s,2) + 312*Power(s,3))*
                Power(t2,4) + 
               (2115 + 1521*s - 4762*Power(s,2) + 309*Power(s,3))*
                Power(t2,5) - 
               (769 - 10794*s + 4059*Power(s,2) + 294*Power(s,3))*
                Power(t2,6) + 
               (-1309 + 3645*s + 1786*Power(s,2) - 673*Power(s,3))*
                Power(t2,7) - 
               (482 + 1705*s - 1467*Power(s,2) + 146*Power(s,3))*
                Power(t2,8) + 
               (126 - 523*s + 138*Power(s,2))*Power(t2,9) + 
               (17 - 16*s)*Power(t2,10) + 
               Power(s1,3)*Power(t2,2)*
                (-36 + 100*t2 + 60*Power(t2,2) - 157*Power(t2,3) - 
                  6*Power(t2,4) + 4*Power(t2,5) + 17*Power(t2,6)) + 
               Power(s1,2)*t2*
                (144 - 6*(77 + 6*s)*t2 + (92 - 120*s)*Power(t2,2) + 
                  (99 - 654*s)*Power(t2,3) + 
                  (5 + 1157*s)*Power(t2,4) + 
                  (296 - 258*s)*Power(t2,5) + 
                  25*(-7 + s)*Power(t2,6) - 
                  3*(-37 + 41*s)*Power(t2,7) + 16*Power(t2,8)) + 
               s1*(-180 + 6*(71 + 18*s)*t2 - 
                  12*(28 - 4*s + 3*Power(s,2))*Power(t2,2) + 
                  (368 + 258*s - 124*Power(s,2))*Power(t2,3) + 
                  (77 + 2136*s + 6*Power(s,2))*Power(t2,4) + 
                  (1829 - 2980*s + 653*Power(s,2))*Power(t2,5) + 
                  (-2194 + 293*s - 470*Power(s,2))*Power(t2,6) + 
                  (-664 + 323*s + 401*Power(s,2))*Power(t2,7) + 
                  3*(97 - 226*s + 84*Power(s,2))*Power(t2,8) - 
                  20*(-5 + 6*s)*Power(t2,9) + Power(t2,10))) + 
            2*Power(t1,7)*(298 + 5*Power(s,3) - 5*Power(s1,3) - 
               589*t2 - 816*Power(t2,2) - 88*Power(t2,3) - 
               Power(s1,2)*(21 + 65*t2) - 
               Power(s,2)*(33 + 15*s1 + 65*t2) - 
               s1*(195 + 657*t2 + 272*Power(t2,2)) + 
               s*(449 + 15*Power(s1,2) + 1054*t2 + 272*Power(t2,2) + 
                  2*s1*(27 + 65*t2))) - 
            2*Power(t1,6)*(647 + 4238*t2 + 2069*Power(t2,2) - 
               1878*Power(t2,3) - 502*Power(t2,4) + 
               Power(s1,3)*(15 - 29*t2 - 36*Power(t2,2)) + 
               Power(s,3)*(-1 + 32*t2 + 36*Power(t2,2)) + 
               Power(s1,2)*(166 + 120*t2 - 551*Power(t2,2) - 
                  203*Power(t2,3)) - 
               s1*(38 - 188*t2 + 1444*Power(t2,2) + 1273*Power(t2,3) + 
                  147*Power(t2,4)) - 
               Power(s,2)*(202 + 868*t2 + 1137*Power(t2,2) + 
                  203*Power(t2,3) + 3*s1*(-11 + 31*t2 + 36*Power(t2,2))\
) + s*(-480 - 15*t2 + 3796*Power(t2,2) + 1918*Power(t2,3) + 
                  147*Power(t2,4) + 
                  Power(s1,2)*(-47 + 90*t2 + 108*Power(t2,2)) + 
                  2*s1*(10 + 210*t2 + 844*Power(t2,2) + 203*Power(t2,3))\
)) + Power(t1,5)*(-760 - 748*t2 + 10078*Power(t2,2) + 
               16564*Power(t2,3) - 1478*Power(t2,4) - 
               2296*Power(t2,5) - 99*Power(t2,6) + 
               Power(s1,3)*(242 + 264*t2 + 218*Power(t2,2) - 
                  310*Power(t2,3) - 88*Power(t2,4)) + 
               2*Power(s,3)*(33 + 69*t2 + 345*Power(t2,2) + 
                  316*Power(t2,3) + 44*Power(t2,4)) - 
               Power(s1,2)*(808 + 1060*t2 - 1538*Power(t2,2) + 
                  678*Power(t2,3) + 1266*Power(t2,4) + 169*Power(t2,5)) \
+ s1*(528 + 634*t2 + 5652*Power(t2,2) + 472*Power(t2,3) - 
                  4063*Power(t2,4) - 1005*Power(t2,5) - 24*Power(t2,6)) \
- Power(s,2)*(120 - 536*t2 + 2638*Power(t2,2) + 6458*Power(t2,3) + 
                  2566*Power(t2,4) + 169*Power(t2,5) + 
                  2*s1*(-55 - 122*t2 + 353*Power(t2,2) + 
                     787*Power(t2,3) + 132*Power(t2,4))) + 
               2*s*(-164 - 4711*t2 - 7360*Power(t2,2) + 
                  2314*Power(t2,3) + 4861*Power(t2,4) + 
                  839*Power(t2,5) + 12*Power(t2,6) + 
                  Power(s1,2)*
                   (-289 - 251*t2 - 101*Power(t2,2) + 
                     626*Power(t2,3) + 132*Power(t2,4)) + 
                  s1*(280 + 1350*t2 - 1058*Power(t2,2) + 
                     2694*Power(t2,3) + 1916*Power(t2,4) + 
                     169*Power(t2,5)))) + 
            Power(t1,4)*(438 + 2700*t2 + 7278*Power(t2,2) - 
               4064*Power(t2,3) - 15081*Power(t2,4) - 
               4996*Power(t2,5) + 2000*Power(t2,6) + 336*Power(t2,7) + 
               3*Power(t2,8) + 
               Power(s1,3)*(126 - 478*t2 - 216*Power(t2,2) - 
                  376*Power(t2,3) + 52*Power(t2,4) + 229*Power(t2,5) + 
                  20*Power(t2,6)) - 
               Power(s,3)*(-30 + 356*t2 - 88*Power(t2,2) + 
                  908*Power(t2,3) + 1523*Power(t2,4) + 
                  481*Power(t2,5) + 20*Power(t2,6)) + 
               Power(s1,2)*(220 + 1168*t2 + 3188*Power(t2,2) - 
                  1162*Power(t2,3) - 889*Power(t2,4) + 
                  973*Power(t2,5) + 388*Power(t2,6) + 14*Power(t2,7)) + 
               s1*(-570 - 410*t2 - 56*Power(t2,2) - 2886*Power(t2,3) - 
                  6644*Power(t2,4) + 2439*Power(t2,5) + 
                  1458*Power(t2,6) + 83*Power(t2,7) + Power(t2,8)) + 
               Power(s,2)*(108 - 1180*t2 - 4556*Power(t2,2) - 
                  2956*Power(t2,3) + 6234*Power(t2,4) + 
                  5573*Power(t2,5) + 841*Power(t2,6) + 
                  14*Power(t2,7) + 
                  s1*(-446 + 1322*t2 - 752*Power(t2,2) - 
                     480*Power(t2,3) + 2350*Power(t2,4) + 
                     1191*Power(t2,5) + 60*Power(t2,6))) - 
               s*(574 + 1394*t2 - 4044*Power(t2,2) - 
                  28866*Power(t2,3) - 12626*Power(t2,4) + 
                  9560*Power(t2,5) + 3864*Power(t2,6) + 
                  197*Power(t2,7) + Power(t2,8) + 
                  Power(s1,2)*
                   (542 - 824*t2 - 544*Power(t2,2) - 
                     1148*Power(t2,3) + 879*Power(t2,4) + 
                     939*Power(t2,5) + 60*Power(t2,6)) + 
                  s1*(-1304 + 1156*t2 + 1920*Power(t2,2) - 
                     1182*Power(t2,3) - 449*Power(t2,4) + 
                     5104*Power(t2,5) + 1229*Power(t2,6) + 
                     28*Power(t2,7)))) + 
            Power(t1,3)*(36 + 850*t2 - 1042*Power(t2,2) - 
               5500*Power(t2,3) - 3021*Power(t2,4) + 5349*Power(t2,5) + 
               4930*Power(t2,6) - 61*Power(t2,7) - 374*Power(t2,8) - 
               13*Power(t2,9) + 
               Power(s1,3)*(40 - 68*t2 - 318*Power(t2,2) + 
                  506*Power(t2,3) + 127*Power(t2,4) + 79*Power(t2,5) - 
                  93*Power(t2,6) - 41*Power(t2,7)) + 
               Power(s,3)*(4 + 10*t2 - 150*Power(t2,2) - 
                  92*Power(t2,3) + 90*Power(t2,4) + 1307*Power(t2,5) + 
                  884*Power(t2,6) + 89*Power(t2,7)) + 
               Power(s1,2)*(98 + 262*t2 + 26*Power(t2,2) - 
                  746*Power(t2,3) - 1988*Power(t2,4) + 
                  1368*Power(t2,5) - 249*Power(t2,6) - 
                  281*Power(t2,7) - 28*Power(t2,8)) - 
               Power(s,2)*(70 - 2*(61 + 100*s1)*t2 + 
                  10*(67 + s1)*Power(t2,2) + 
                  (-3558 + 718*s1)*Power(t2,3) - 
                  3*(2885 + 219*s1)*Power(t2,4) + 
                  (-280 + 267*s1)*Power(t2,5) + 
                  3*(1766 + 493*s1)*Power(t2,6) + 
                  (1631 + 219*s1)*Power(t2,7) + 73*Power(t2,8)) + 
               s1*(-330 + 144*t2 + 1688*Power(t2,2) - 
                  4418*Power(t2,3) + 2641*Power(t2,4) + 
                  4042*Power(t2,5) + 586*Power(t2,6) - 
                  1070*Power(t2,7) - 125*Power(t2,8) - 2*Power(t2,9)) + 
               s*(-90 + 522*t2 + 7260*Power(t2,2) + 4626*Power(t2,3) - 
                  17107*Power(t2,4) - 20634*Power(t2,5) + 
                  1064*Power(t2,6) + 4046*Power(t2,7) + 
                  491*Power(t2,8) + 7*Power(t2,9) + 
                  Power(s1,2)*
                   (180 - 574*t2 + 2366*Power(t2,2) - 
                     2784*Power(t2,3) + 260*Power(t2,4) - 
                     450*Power(t2,5) + 688*Power(t2,6) + 171*Power(t2,7)\
) + s1*(468 - 336*t2 - 3276*Power(t2,2) + 4332*Power(t2,3) + 
                     2026*Power(t2,4) - 3038*Power(t2,5) + 
                     1831*Power(t2,6) + 1512*Power(t2,7) + 
                     101*Power(t2,8))))) + 
         Power(s2,3)*(40*Power(t1,8) + 
            Power(t2,2)*(20 - 4*(5 + 3*s + 3*s1)*t2 + 
               (93 - 48*Power(s,2) - 23*s1 + 6*Power(s1,2) + 
                  8*s*(14 + 3*s1))*Power(t2,2) - 
               (241 - 162*Power(s,2) + 20*Power(s,3) + 61*s1 + 
                  2*Power(s1,3) + s*(-182 + 113*s1))*Power(t2,3) + 
               (-28 + 30*Power(s,3) + Power(s,2)*(213 - 50*s1) + 
                  241*s1 + 11*Power(s1,2) + 4*Power(s1,3) - 
                  s*(586 - 13*s1 + Power(s1,2)))*Power(t2,4) + 
               (304 + 60*Power(s,3) - 71*s1 - 52*Power(s1,2) + 
                  3*Power(s,2)*(-95 + 2*s1) + 
                  s*(-471 + 343*s1 - 18*Power(s1,2)))*Power(t2,5) - 
               (22*Power(s,3) + Power(s,2)*(409 - 98*s1) - 
                  3*s*(203 - 61*s1 + 6*Power(s1,2)) + 
                  2*(17 + 31*s1 - 11*Power(s1,2) + Power(s1,3)))*
                Power(t2,6) - 
               (91 + 56*Power(s,3) + 30*s1 - 15*Power(s1,2) + 
                  Power(s,2)*(-99 + 28*s1) + s*(-375 + 104*s1))*
                Power(t2,7) + 
               2*(64*Power(s,2) + 5*s*(-8 + s1) - (-7 + s1)*s1)*
                Power(t2,8) + (-5 - 48*s + 4*s1)*Power(t2,9) + 
               2*Power(t2,10)) + 
            t1*(-20 - 20*(-7 + 3*s - 9*s1)*t2 + 
               2*(-132 + 24*Power(s,2) - 223*s1 - 72*Power(s1,2) + 
                  s*(101 + 6*s1))*Power(t2,2) + 
               2*(355 + 16*Power(s,3) + 247*s1 + 182*Power(s1,2) + 
                  16*Power(s1,3) + 2*Power(s,2)*(-74 + 3*s1) + 
                  s*(-455 - 40*s1 + 6*Power(s1,2)))*Power(t2,3) + 
               (711 - 94*Power(s,3) - 932*s1 - 186*Power(s1,2) - 
                  87*Power(s1,3) + 4*Power(s,2)*(-197 + 32*s1) + 
                  s*(2107 + 164*s1 + 60*Power(s1,2)))*Power(t2,4) + 
               (-1716 - 204*Power(s,3) + 160*s1 + 165*Power(s1,2) - 
                  Power(s1,3) + Power(s,2)*(790 + 8*s1) + 
                  s*(3918 - 1811*s1 + 301*Power(s1,2)))*Power(t2,5) + 
               (-1078 + 27*Power(s,3) + Power(s,2)*(2652 - 456*s1) - 
                  95*s1 - 76*Power(s1,2) + 86*Power(s1,3) + 
                  s*(-2501 + 1236*s1 - 523*Power(s1,2)))*Power(t2,6) + 
               (752 + 312*Power(s,3) + 946*s1 - 194*Power(s1,2) - 
                  14*Power(s1,3) + 2*Power(s,2)*(338 + 85*s1) + 
                  5*s*(-1069 + 126*s1 + 24*Power(s1,2)))*Power(t2,7) + 
               (610 + 203*Power(s,3) + 82*s1 + 8*Power(s1,2) - 
                  4*Power(s1,3) - 172*Power(s,2)*(8 + s1) + 
                  s*(-764 + 51*s1 + 29*Power(s1,2)))*Power(t2,8) + 
               (257 - 401*Power(s,2) - 181*s1 - 21*Power(s1,2) + 
                  s*(1019 + 184*s1))*Power(t2,9) + 
               (-85 + 139*s - 20*s1)*Power(t2,10) - 5*Power(t2,11)) - 
            2*Power(t1,7)*(258 + 9*Power(s,2) + 54*s1 + 9*Power(s1,2) + 
               720*t2 + 89*s1*t2 + 204*Power(t2,2) - 
               s*(125 + 18*s1 + 89*t2)) - 
            2*Power(t1,6)*(683 + 704*t2 - 2585*Power(t2,2) - 
               1411*Power(t2,3) - 72*Power(t2,4) - 
               2*Power(s1,3)*(3 + 5*t2) + Power(s,3)*(3 + 10*t2) + 
               Power(s1,2)*(99 - 94*t2 - 105*Power(t2,2)) - 
               s1*(201 + 1033*t2 + 1476*Power(t2,2) + 
                  309*Power(t2,3)) - 
               Power(s,2)*(53 + 236*t2 + 105*Power(t2,2) + 
                  6*s1*(2 + 5*t2)) + 
               s*(292 + 2265*t2 + 2097*Power(t2,2) + 309*Power(t2,3) + 
                  15*Power(s1,2)*(1 + 2*t2) + 
                  6*s1*(-21 + 55*t2 + 35*Power(t2,2)))) + 
            2*Power(t1,5)*(234 + 3571*t2 + 9349*Power(t2,2) - 
               126*Power(t2,3) - 3498*Power(t2,4) - 468*Power(t2,5) + 
               Power(s1,3)*(21 + 83*t2 - 49*Power(t2,2) - 
                  25*Power(t2,3)) + 
               Power(s,3)*(-6 + 57*t2 + 102*Power(t2,2) + 
                  25*Power(t2,3)) - 
               Power(s1,2)*(69 - 568*t2 + 77*Power(t2,2) + 
                  743*Power(t2,3) + 142*Power(t2,4)) - 
               s1*(121 + 13*t2 + 605*Power(t2,2) + 3438*Power(t2,3) + 
                  1509*Power(t2,4) + 85*Power(t2,5)) - 
               Power(s,2)*(39 + 886*t2 + 2027*Power(t2,2) + 
                  1320*Power(t2,3) + 142*Power(t2,4) + 
                  s1*(-105 - 57*t2 + 253*Power(t2,2) + 75*Power(t2,3))) \
+ s*(-629 - 1945*t2 + 2929*Power(t2,2) + 7373*Power(t2,3) + 
                  2133*Power(t2,4) + 85*Power(t2,5) + 
                  Power(s1,2)*
                   (-96 - 197*t2 + 200*Power(t2,2) + 75*Power(t2,3)) + 
                  s1*(404 - 130*t2 + 1602*Power(t2,2) + 
                     2063*Power(t2,3) + 284*Power(t2,4)))) + 
            Power(t1,4)*(444 + 2642*t2 - 2390*Power(t2,2) - 
               23522*Power(t2,3) - 17480*Power(t2,4) + 
               5611*Power(t2,5) + 2316*Power(t2,6) + 63*Power(t2,7) - 
               2*Power(s,3)*(3 - 22*t2 + 248*Power(t2,2) + 
                  493*Power(t2,3) + 220*Power(t2,4) + 13*Power(t2,5)) + 
               Power(s1,3)*(-200 - 276*t2 - 452*Power(t2,2) - 
                  76*Power(t2,3) + 234*Power(t2,4) + 26*Power(t2,5)) + 
               2*Power(s1,2)*
                (297 + 1395*t2 + 225*Power(t2,2) - 868*Power(t2,3) + 
                  771*Power(t2,4) + 487*Power(t2,5) + 24*Power(t2,6)) + 
               s1*(-702 - 1606*t2 - 2304*Power(t2,2) - 
                  6596*Power(t2,3) + 4021*Power(t2,4) + 
                  5001*Power(t2,5) + 637*Power(t2,6) + 7*Power(t2,7)) + 
               Power(s,2)*(-362 - 502*t2 + 404*Power(t2,2) + 
                  6688*Power(t2,3) + 7619*Power(t2,4) + 
                  1700*Power(t2,5) + 48*Power(t2,6) + 
                  2*s1*(242 - 390*t2 - 126*Power(t2,2) + 
                     718*Power(t2,3) + 557*Power(t2,4) + 39*Power(t2,5)\
)) - s*(-202 - 2506*t2 - 20616*Power(t2,2) - 14504*Power(t2,3) + 
                  16023*Power(t2,4) + 10755*Power(t2,5) + 
                  1023*Power(t2,6) + 7*Power(t2,7) + 
                  2*Power(s1,2)*
                   (27 - 506*t2 - 468*Power(t2,2) + 187*Power(t2,3) + 
                     454*Power(t2,4) + 39*Power(t2,5)) + 
                  s1*(424 + 2192*t2 + 3646*Power(t2,2) - 
                     376*Power(t2,3) + 7508*Power(t2,4) + 
                     2674*Power(t2,5) + 96*Power(t2,6)))) + 
            Power(t1,3)*(-108 - 1226*t2 - 5394*Power(t2,2) - 
               6078*Power(t2,3) + 8747*Power(t2,4) + 
               17661*Power(t2,5) + 2304*Power(t2,6) - 
               2270*Power(t2,7) - 211*Power(t2,8) - Power(t2,9) + 
               Power(s1,3)*(-30 - 138*t2 + 678*Power(t2,2) + 
                  230*Power(t2,3) + 279*Power(t2,4) - 
                  149*Power(t2,5) - 78*Power(t2,6)) + 
               2*Power(s,3)*(-4 - 63*t2 + 136*Power(t2,2) + 
                  111*Power(t2,3) + 636*Power(t2,4) + 
                  533*Power(t2,5) + 71*Power(t2,6)) - 
               Power(s1,2)*(-316 + 300*t2 + 1870*Power(t2,2) + 
                  3530*Power(t2,3) - 1339*Power(t2,4) - 
                  63*Power(t2,5) + 873*Power(t2,6) + 125*Power(t2,7)) + 
               s1*(70 + 1632*t2 - 372*Power(t2,2) + 2314*Power(t2,3) + 
                  5269*Power(t2,4) + 2901*Power(t2,5) - 
                  3471*Power(t2,6) - 936*Power(t2,7) - 27*Power(t2,8)) \
- Power(s,2)*(2*s1*(15 - 33*t2 + 453*Power(t2,2) - 331*Power(t2,3) + 
                     150*Power(t2,4) + 940*Power(t2,5) + 
                     181*Power(t2,6)) + 
                  t2*(160 - 2218*t2 - 5124*Power(t2,2) + 
                     1205*Power(t2,3) + 8548*Power(t2,4) + 
                     3651*Power(t2,5) + 241*Power(t2,6))) + 
               s*(248 + 2004*t2 + 1864*Power(t2,2) - 
                  13660*Power(t2,3) - 32013*Power(t2,4) - 
                  1841*Power(t2,5) + 11495*Power(t2,6) + 
                  2389*Power(t2,7) + 59*Power(t2,8) + 
                  Power(s1,2)*
                   (-460 + 1926*t2 - 2300*Power(t2,2) - 
                     322*Power(t2,3) - 664*Power(t2,4) + 
                     963*Power(t2,5) + 298*Power(t2,6)) + 
                  s1*(-220 - 2100*t2 + 1052*Power(t2,2) + 
                     4610*Power(t2,3) - 929*Power(t2,4) + 
                     2661*Power(t2,5) + 3752*Power(t2,6) + 
                     366*Power(t2,7)))) + 
            Power(t1,2)*(-30 - 218*t2 - 904*Power(t2,2) + 
               1972*Power(t2,3) + 5881*Power(t2,4) + 109*Power(t2,5) - 
               4784*Power(t2,6) - 3808*Power(t2,7) + 494*Power(t2,8) + 
               238*Power(t2,9) + 4*Power(t2,10) + 
               Power(s1,3)*t2*
                (-60 + 168*t2 + 132*Power(t2,2) - 445*Power(t2,3) - 
                  41*Power(t2,4) - 11*Power(t2,5) + 49*Power(t2,6)) - 
               Power(s,3)*t2*
                (12 - 116*t2 - 134*Power(t2,2) + 52*Power(t2,3) + 
                  485*Power(t2,4) + 966*Power(t2,5) + 263*Power(t2,6)) + 
               Power(s1,2)*(180 - 534*t2 + 4*Power(t2,2) - 
                  28*Power(t2,3) + 749*Power(t2,4) + 1177*Power(t2,5) - 
                  465*Power(t2,6) + 244*Power(t2,7) + 95*Power(t2,8)) + 
               s1*(260 - 20*t2 + 596*Power(t2,2) - 1542*Power(t2,3) + 
                  2231*Power(t2,4) - 3383*Power(t2,5) - 
                  2245*Power(t2,6) + 577*Power(t2,7) + 
                  646*Power(t2,8) + 36*Power(t2,9)) + 
               Power(s,2)*t2*
                (250 + 230*t2 - 196*Power(t2,2) - 4376*Power(t2,3) - 
                  4731*Power(t2,4) + 3179*Power(t2,5) + 
                  3515*Power(t2,6) + 466*Power(t2,7) + 
                  s1*(60 - 360*t2 + 124*Power(t2,2) + 693*Power(t2,3) - 
                     427*Power(t2,4) + 635*Power(t2,5) + 485*Power(t2,6)\
)) - s*(10 - 400*t2 + 1242*Power(t2,2) + 6658*Power(t2,3) + 
                  105*Power(t2,4) - 17398*Power(t2,5) - 
                  11951*Power(t2,6) + 3973*Power(t2,7) + 
                  2473*Power(t2,8) + 143*Power(t2,9) + 
                  Power(s1,2)*t2*
                   (180 - 484*t2 + 1846*Power(t2,2) - 
                     2298*Power(t2,3) + 281*Power(t2,4) + 
                     3*Power(t2,5) + 271*Power(t2,6)) + 
                  s1*(-60 + 164*t2 - 6*Power(t2,2) - 3152*Power(t2,3) + 
                     2635*Power(t2,4) + 2357*Power(t2,5) - 
                     565*Power(t2,6) + 1621*Power(t2,7) + 462*Power(t2,8)\
)))) + s2*(-56*Power(t1,9) + Power(t2,4)*(1 + (-1 + s)*t2)*
             (-10 + (-20*s + 6*(3 + s1))*t2 + 
               (17 + 23*s - 10*Power(s,2) - 8*s1)*Power(t2,2) - 
               (33 + Power(s,2) + 6*s1 + s*(-53 + 5*s1))*Power(t2,3) + 
               (-11 + 18*Power(s,2) + s*(-28 + s1) + 10*s1)*
                Power(t2,4) + 
               (22 + 4*Power(s,2) + s*(-43 + 5*s1))*Power(t2,5) - 
               (8*Power(s,2) + s*(-11 + s1) + 2*(1 + s1))*Power(t2,6) + 
               (-1 + 4*s)*Power(t2,7)) + 
            t1*Power(t2,2)*(60 + 60*(-3 + 3*s - s1)*t2 + 
               5*(-4 + 36*Power(s,2) + 27*s1 - 3*s*(29 + 2*s1))*
                Power(t2,2) + 
               (343 + 60*Power(s,3) + 30*Power(s,2)*(-8 + s1) - 
                  15*s1 + 2*Power(s1,2) + 
                  s*(-405 + 92*s1 - 6*Power(s1,2)))*Power(t2,3) + 
               (20 + 15*Power(s,3) - 97*s1 + 
                  Power(s,2)*(-600 + 43*s1) + 
                  13*s*(62 - 6*s1 + Power(s1,2)))*Power(t2,4) + 
               (-417 - 123*Power(s,3) + Power(s,2)*(145 - 57*s1) + 
                  25*s1 - 12*Power(s1,2) + 
                  s*(727 - 57*s1 + 4*Power(s1,2)))*Power(t2,5) + 
               (-84*Power(s,3) + Power(s,2)*(701 - 46*s1) + 
                  s*(-723 + 124*s1 - 18*Power(s1,2)) + 
                  2*(73 - 18*s1 + 6*Power(s1,2)))*Power(t2,6) + 
               (61 + 58*Power(s,3) + 68*s1 + 2*Power(s1,2) + 
                  Power(s,2)*(25 + 44*s1) + 
                  s*(-260 - 92*s1 + 6*Power(s1,2)))*Power(t2,7) + 
               (31*Power(s,3) - Power(s,2)*(131 + 24*s1) + 
                  s*(86 + 36*s1 + Power(s1,2)) - 
                  2*(5 + 9*s1 + 2*Power(s1,2)))*Power(t2,8) + 
               (-3 - 14*Power(s,2) - 2*s1 + s*(24 + 5*s1))*Power(t2,9)) + 
            Power(t1,2)*(60 - 12*(5 + 3*s + 3*s1)*t2 - 
               6*(-79 + 24*Power(s,2) - 16*s1 - 18*Power(s1,2) + 
                  s*(-61 + 18*s1))*Power(t2,2) - 
               2*(429 + 30*Power(s,3) + 186*s1 + 121*Power(s1,2) + 
                  6*Power(s1,3) + 3*Power(s,2)*(-61 + 30*s1) - 
                  2*s*(354 + 49*s1 + 15*Power(s1,2)))*Power(t2,3) + 
               (-739 - 60*Power(s,3) + Power(s,2)*(1619 - 60*s1) + 
                  262*s1 + 100*Power(s1,2) + 24*Power(s1,3) + 
                  s*(-1679 + 273*s1 - 156*Power(s1,2)))*Power(t2,4) + 
               (1550 + 235*Power(s,3) + 34*s1 + 84*Power(s1,2) - 
                  2*Power(s1,3) + Power(s,2)*(544 + 218*s1) + 
                  s*(-4292 + 252*s1 - 43*Power(s1,2)))*Power(t2,5) + 
               (88 + 401*Power(s,3) + 675*s1 - 72*Power(s1,2) - 
                  10*Power(s1,3) + Power(s,2)*(-2725 + 274*s1) + 
                  s*(1633 - 1197*s1 + 213*Power(s1,2)))*Power(t2,6) - 
               (407 + 56*Power(s,3) + 697*s1 - 32*Power(s1,2) + 
                  2*Power(s1,3) + 4*Power(s,2)*(384 + 79*s1) + 
                  s*(-3033 - 723*s1 + 80*Power(s1,2)))*Power(t2,7) + 
               (-130 - 255*Power(s,3) - 25*s1 - 38*Power(s1,2) + 
                  2*Power(s,2)*(322 + 65*s1) + 
                  s*(210 + 6*s1 + 32*Power(s1,2)))*Power(t2,8) + 
               (12 - 45*Power(s,3) + 57*s1 + 27*Power(s1,2) + 
                  2*Power(s1,3) + Power(s,2)*(314 + 72*s1) - 
                  s*(365 + 161*s1 + 29*Power(s1,2)))*Power(t2,9) + 
               (10 + 18*Power(s,2) + 6*s1 + Power(s1,2) - 
                  s*(43 + 14*s1))*Power(t2,10)) + 
            4*Power(t1,8)*(7 + 10*Power(s,2) + 10*Power(s1,2) + 101*t2 + 
               26*Power(t2,2) + s1*(58 + 56*t2) - 
               4*s*(5*s1 + 14*(2 + t2))) + 
            2*Power(t1,7)*(772 + 1304*t2 - 386*Power(t2,2) - 
               268*Power(t2,3) + 4*Power(s,3)*(1 + 7*t2) - 
               4*Power(s1,3)*(2 + 7*t2) + 
               Power(s1,2)*(86 - 190*t2 - 127*Power(t2,2)) - 
               s1*(-96 + 194*t2 + 480*Power(t2,2) + 111*Power(t2,3)) - 
               Power(s,2)*(170 + 486*t2 + 127*Power(t2,2) + 
                  4*s1*(4 + 21*t2)) + 
               s*(-204 + 790*t2 + 801*Power(t2,2) + 111*Power(t2,3) + 
                  4*Power(s1,2)*(5 + 21*t2) + 
                  s1*(-28 + 676*t2 + 254*Power(t2,2)))) + 
            Power(t1,6)*(468 - 1244*t2 - 7622*Power(t2,2) - 
               756*Power(t2,3) + 1132*Power(t2,4) + 69*Power(t2,5) - 
               2*Power(s,3)*t2*(162 + 223*t2 + 54*Power(t2,2)) + 
               4*Power(s1,3)*
                (-30 - 57*t2 + 44*Power(t2,2) + 27*Power(t2,3)) + 
               Power(s1,2)*(408 - 488*t2 - 78*Power(t2,2) + 
                  624*Power(t2,3) + 199*Power(t2,4)) + 
               s1*(72 - 1732*t2 - 1758*Power(t2,2) + 1410*Power(t2,3) + 
                  695*Power(t2,4) + 27*Power(t2,5)) + 
               Power(s,2)*(-264 + 264*t2 + 2370*Power(t2,2) + 
                  1668*Power(t2,3) + 199*Power(t2,4) + 
                  12*s1*(-26 + 11*t2 + 89*Power(t2,2) + 27*Power(t2,3))) \
- s*(-1848 - 6188*t2 - 1988*Power(t2,2) + 4042*Power(t2,3) + 
                  1215*Power(t2,4) + 27*Power(t2,5) + 
                  Power(s1,2)*
                   (-368 - 420*t2 + 798*Power(t2,2) + 324*Power(t2,3)) + 
                  2*s1*(296 - 496*t2 + 698*Power(t2,2) + 
                     1146*Power(t2,3) + 199*Power(t2,4)))) + 
            Power(t1,5)*(-600 - 3688*t2 + 124*Power(t2,2) + 
               6682*Power(t2,3) + 3697*Power(t2,4) - 827*Power(t2,5) - 
               239*Power(t2,6) - 3*Power(t2,7) + 
               Power(s1,3)*(96 + 108*t2 + 194*Power(t2,2) + 
                  138*Power(t2,3) - 199*Power(t2,4) - 55*Power(t2,5)) + 
               Power(s,3)*(8 - 100*t2 + 312*Power(t2,2) + 
                  958*Power(t2,3) + 566*Power(t2,4) + 55*Power(t2,5)) - 
               Power(s1,2)*(704 + 1560*t2 - 326*Power(t2,2) - 
                  1028*Power(t2,3) + 316*Power(t2,4) + 
                  379*Power(t2,5) + 35*Power(t2,6)) - 
               s1*(-296 - 40*t2 + 782*Power(t2,2) - 5022*Power(t2,3) + 
                  331*Power(t2,4) + 1022*Power(t2,5) + 85*Power(t2,6) + 
                  2*Power(t2,7)) - 
               Power(s,2)*(-592 - 1736*t2 - 3290*Power(t2,2) + 
                  768*Power(t2,3) + 3544*Power(t2,4) + 
                  976*Power(t2,5) + 35*Power(t2,6) + 
                  s1*(560 - 596*t2 - 610*Power(t2,2) + 
                     1090*Power(t2,3) + 1331*Power(t2,4) + 
                     165*Power(t2,5))) + 
               s*(216 + 1576*t2 - 12590*Power(t2,2) - 
                  12010*Power(t2,3) + 3016*Power(t2,4) + 
                  2779*Power(t2,5) + 217*Power(t2,6) + 2*Power(t2,7) + 
                  Power(s1,2)*
                   (328 - 540*t2 - 780*Power(t2,2) - 6*Power(t2,3) + 
                     964*Power(t2,4) + 165*Power(t2,5)) + 
                  s1*(688 + 112*t2 + 144*Power(t2,2) - 
                     2756*Power(t2,3) + 2630*Power(t2,4) + 
                     1355*Power(t2,5) + 70*Power(t2,6)))) - 
            Power(t1,4)*(236 - 164*t2 - 2004*Power(t2,2) - 
               3462*Power(t2,3) + 2847*Power(t2,4) + 3207*Power(t2,5) + 
               247*Power(t2,6) - 262*Power(t2,7) - 14*Power(t2,8) + 
               Power(s1,3)*(24 - 212*t2 + 228*Power(t2,2) + 
                  84*Power(t2,3) + 82*Power(t2,4) + 14*Power(t2,5) - 
                  85*Power(t2,6) - 7*Power(t2,7)) + 
               Power(s,3)*(16 - 276*t2 - 58*Power(t2,2) - 
                  300*Power(t2,3) + 550*Power(t2,4) + 981*Power(t2,5) + 
                  237*Power(t2,6) + 7*Power(t2,7)) + 
               s1*(208 + 1272*t2 - 3478*Power(t2,2) + 374*Power(t2,3) + 
                  2829*Power(t2,4) + 1419*Power(t2,5) - 
                  730*Power(t2,6) - 156*Power(t2,7) - 4*Power(t2,8)) + 
               Power(s1,2)*(320 + 40*t2 - 274*Power(t2,2) - 
                  2214*Power(t2,3) + 1537*Power(t2,4) + 
                  98*Power(t2,5) - 273*Power(t2,6) - 59*Power(t2,7) - 
                  2*Power(t2,8)) - 
               Power(s,2)*(64 + 616*t2 - 418*Power(t2,2) - 
                  6654*Power(t2,3) - 4318*Power(t2,4) + 
                  2918*Power(t2,5) + 1888*Power(t2,6) + 
                  179*Power(t2,7) + 2*Power(t2,8) + 
                  s1*(200 - 244*t2 + 456*Power(t2,2) - 
                     204*Power(t2,3) - 778*Power(t2,4) + 
                     1358*Power(t2,5) + 559*Power(t2,6) + 21*Power(t2,7)\
)) + s*(344 + 3912*t2 + 5498*Power(t2,2) - 7210*Power(t2,3) - 
                  16366*Power(t2,4) - 2463*Power(t2,5) + 
                  2904*Power(t2,6) + 567*Power(t2,7) + 14*Power(t2,8) + 
                  Power(s1,2)*
                   (-288 + 1652*t2 - 1794*Power(t2,2) + 
                     396*Power(t2,3) - 790*Power(t2,4) + 
                     363*Power(t2,5) + 407*Power(t2,6) + 21*Power(t2,7)) \
+ s1*(128 - 2496*t2 + 4016*Power(t2,2) + 2632*Power(t2,3) - 
                     4299*Power(t2,4) + 36*Power(t2,5) + 
                     1610*Power(t2,6) + 238*Power(t2,7) + 4*Power(t2,8)))\
) + Power(t1,3)*(72 + 24*t2 + 1724*Power(t2,2) - 1670*Power(t2,3) - 
               1805*Power(t2,4) + 124*Power(t2,5) + 1231*Power(t2,6) + 
               423*Power(t2,7) - 92*Power(t2,8) - 19*Power(t2,9) + 
               Power(s1,3)*t2*
                (-4 + 2*t2 - 86*Power(t2,2) + 69*Power(t2,3) + 
                  49*Power(t2,4) + 4*Power(t2,5) - 12*Power(t2,6) - 
                  10*Power(t2,7)) + 
               Power(s,3)*t2*(-4 - 52*t2 - 162*Power(t2,2) - 
                  484*Power(t2,3) - 295*Power(t2,4) + 645*Power(t2,5) + 
                  375*Power(t2,6) + 29*Power(t2,7)) - 
               Power(s1,2)*(108 - 292*t2 - 200*Power(t2,2) + 
                  280*Power(t2,3) + 46*Power(t2,4) + 473*Power(t2,5) - 
                  518*Power(t2,6) + 142*Power(t2,7) + 42*Power(t2,8) + 
                  3*Power(t2,9)) - 
               s1*(72 + 36*t2 - 438*Power(t2,2) - 324*Power(t2,3) + 
                  3132*Power(t2,4) - 1901*Power(t2,5) - 
                  1023*Power(t2,6) + 106*Power(t2,7) + 146*Power(t2,8) + 
                  6*Power(t2,9)) - 
               Power(s,2)*(-36 + (428 - 36*s1)*t2 - 
                  2*(-502 + 77*s1)*Power(t2,2) + 
                  (2020 + 66*s1)*Power(t2,3) + 
                  (-3104 + 875*s1)*Power(t2,4) - 
                  (6238 + 733*s1)*Power(t2,5) - 
                  (90 + 13*s1)*Power(t2,6) + 
                  3*(575 + 191*s1)*Power(t2,7) + 
                  (346 + 68*s1)*Power(t2,8) + 10*Power(t2,9)) + 
               s*(-96 - 564*t2 + 372*Power(t2,2) + 8208*Power(t2,3) + 
                  1819*Power(t2,4) - 9544*Power(t2,5) - 
                  5113*Power(t2,6) + 1096*Power(t2,7) + 665*Power(t2,8) + 
                  36*Power(t2,9) + 
                  Power(s1,2)*t2*
                   (36 + 40*t2 + 634*Power(t2,2) - 942*Power(t2,3) + 
                     269*Power(t2,4) - 283*Power(t2,5) + 
                     210*Power(t2,6) + 49*Power(t2,7)) + 
                  s1*(-216 - 24*t2 - 828*Power(t2,2) - 1412*Power(t2,3) + 
                     4434*Power(t2,4) - 1003*Power(t2,5) - 
                     1567*Power(t2,6) + 739*Power(t2,7) + 
                     294*Power(t2,8) + 13*Power(t2,9))))))*
       T2(t1,1 - s2 + t1 - t2))/
     ((-1 + t1)*(s - s2 + t1)*Power(1 - s2 + t1 - t2,2)*(s - s1 + t2)*
       Power(t1 - s2*t2,3)*Power(-Power(s2,2) + 4*t1 - 2*s2*t2 - 
         Power(t2,2),2)) + (8*(-(Power(t1,9)*
            (-8 + Power(s,2) + Power(s1,2) + 6*s2 + t2 + 
              s1*(3 + 2*s2 + t2) - s*(3 + 2*s1 + 2*s2 + t2))) - 
         Power(t1,3)*(-2 + 31*t2 + s*t2 - Power(t2,2) + 
            89*s*Power(t2,2) - 19*Power(s,2)*Power(t2,2) - 
            135*Power(t2,3) - 201*s*Power(t2,3) + 
            189*Power(s,2)*Power(t2,3) - 26*Power(s,3)*Power(t2,3) + 
            126*Power(t2,4) - 426*s*Power(t2,4) + 
            142*Power(s,2)*Power(t2,4) - 14*Power(s,3)*Power(t2,4) + 
            50*Power(t2,5) + 495*s*Power(t2,5) - 
            492*Power(s,2)*Power(t2,5) + 98*Power(s,3)*Power(t2,5) - 
            90*Power(t2,6) + 253*s*Power(t2,6) - 
            101*Power(s,2)*Power(t2,6) - Power(s,3)*Power(t2,6) + 
            21*Power(t2,7) - 130*s*Power(t2,7) + 
            132*Power(s,2)*Power(t2,7) - 35*Power(s,3)*Power(t2,7) + 
            6*Power(s2,7)*Power(t2,2)*(2 + 3*t2) + 
            2*Power(s2,6)*t2*
             (-6 - (49 + 6*s)*t2 + (3 - 13*s)*Power(t2,2) + 
               42*Power(t2,3)) + 
            Power(s2,4)*(4 - 2*(-35 + s)*t2 + 
               (492 + 122*s - 4*Power(s,2))*Power(t2,2) + 
               (310 + 401*s + 49*Power(s,2))*Power(t2,3) + 
               (-948 - 265*s + 103*Power(s,2))*Power(t2,4) + 
               (57 - 351*s)*Power(t2,5) + 98*Power(t2,6)) + 
            Power(s2,3)*(-6 - 69*t2 + 
               (89 + 17*s + 21*Power(s,2))*Power(t2,2) + 
               (1217 + 624*s - 14*Power(s,2) - 14*Power(s,3))*
                Power(t2,3) + 
               (101 + 967*s + 285*Power(s,2) - 16*Power(s,3))*
                Power(t2,4) + 
               (-786 - 606*s + 292*Power(s,2))*Power(t2,5) + 
               (166 - 316*s)*Power(t2,6) + 30*Power(t2,7)) - 
            Power(s2,2)*(3 + (74 + 33*s)*t2 + 
               (354 + 79*s + 39*Power(s,2))*Power(t2,2) + 
               (-193 - 200*s + 160*Power(s,2) + 38*Power(s,3))*
                Power(t2,3) + 
               2*(-537 - 785*s + 91*Power(s,2) + 41*Power(s,3))*
                Power(t2,4) + 
               (514 - 880*s - 502*Power(s,2) + 70*Power(s,3))*
                Power(t2,5) + 
               (135 + 603*s - 310*Power(s,2))*Power(t2,6) + 
               5*(-17 + 21*s)*Power(t2,7)) + 
            s2*(7 + 22*(1 + s)*t2 + 
               (-72 - 205*s + 41*Power(s,2))*Power(t2,2) + 
               (-245 - 604*s + 6*Power(s,2) + 78*Power(s,3))*
                Power(t2,3) + 
               (343 + 444*s - 503*Power(s,2) + 7*Power(s,3))*
                Power(t2,4) + 
               (68 + 1593*s - 362*Power(s,2) - 105*Power(s,3))*
                Power(t2,5) + 
               (-194 + 7*s + 320*Power(s,2) - 90*Power(s,3))*
                Power(t2,6) + (59 - 242*s + 110*Power(s,2))*Power(t2,7)) \
+ Power(s2,5)*t2*(32 + 20*t2 - 500*Power(t2,2) + 
               10*Power(s,2)*Power(t2,2) - 40*Power(t2,3) + 
               135*Power(t2,4) + 
               s*(12 + 58*t2 - 74*Power(t2,2) - 165*Power(t2,3))) + 
            Power(s1,3)*(-4 + Power(s2,5) + 8*t2 - 5*Power(t2,3) + 
               2*Power(t2,5) - 2*Power(t2,6) + Power(t2,7) + 
               Power(s2,4)*(-3 + 10*t2 + 8*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s2,3)*(2 + 11*t2 + 57*Power(t2,2) + 
                  43*Power(t2,3) + 25*Power(t2,4)) + 
               Power(s2,2)*(-2 - 87*t2 - 39*Power(t2,2) + 
                  19*Power(t2,3) + 33*Power(t2,4) + 30*Power(t2,5)) + 
               s2*(6 + 58*t2 - 94*Power(t2,2) + 13*Power(t2,3) - 
                  8*Power(t2,4) + 2*Power(t2,5) + 11*Power(t2,6))) + 
            Power(s1,2)*(10 - 2*(-1 + s)*t2 - 
               2*(19 + 14*s)*Power(t2,2) + (-3 + 51*s)*Power(t2,3) + 
               (41 - 23*s)*Power(t2,4) + (5 - 15*s)*Power(t2,5) + 
               (-30 + 31*s)*Power(t2,6) + (13 - 15*s)*Power(t2,7) + 
               2*Power(s2,6)*t2*(1 + t2) + 
               Power(s2,5)*t2*(1 - s + 38*t2 + 34*Power(t2,2)) + 
               Power(s2,4)*(-3 - 4*(13 + 3*s)*t2 - 
                  (166 + 25*s)*Power(t2,2) + (51 - 12*s)*Power(t2,3) + 
                  113*Power(t2,4)) + 
               Power(s2,3)*(13 + (82 + 43*s)*t2 + 
                  (20 - 37*s)*Power(t2,2) - (332 + 95*s)*Power(t2,3) - 
                  57*(-1 + s)*Power(t2,4) + 138*Power(t2,5)) + 
               Power(s2,2)*(-10 + (101 - 40*s)*t2 + 
                  3*(141 + 55*s)*Power(t2,2) + 
                  (76 + 43*s)*Power(t2,3) - 
                  (262 + 109*s)*Power(t2,4) + 
                  (93 - 97*s)*Power(t2,5) + 55*Power(t2,6)) + 
               s2*(-10 + 4*(-34 + 3*s)*t2 - 5*(11 + 17*s)*Power(t2,2) + 
                  (406 + 73*s)*Power(t2,3) + 
                  (-89 + 104*s)*Power(t2,4) - (98 + 27*s)*Power(t2,5) + 
                  (60 - 64*s)*Power(t2,6) + 6*Power(t2,7))) + 
            s1*(-4 - (29 + 24*s)*t2 + 
               (22 + 104*s - 28*Power(s,2))*Power(t2,2) + 
               2*Power(s2,7)*Power(t2,2) + 
               (-16 + 146*s - 41*Power(s,2))*Power(t2,3) + 
               (137 - 433*s + 165*Power(s,2))*Power(t2,4) - 
               7*(7 - 15*s + 9*Power(s,2))*Power(t2,5) + 
               (-91 + 181*s - 57*Power(s,2))*Power(t2,6) + 
               (30 - 89*s + 45*Power(s,2))*Power(t2,7) + 
               2*Power(s2,6)*Power(t2,2)*(-4 - s + 10*t2) + 
               Power(s2,5)*t2*
                (20 - 3*(-38 + s)*t2 + (60 - 27*s)*Power(t2,2) + 
                  97*Power(t2,3)) + 
               Power(s2,4)*(-2 - (56 + 19*s)*t2 + 
                  (-91 - 12*s + 7*Power(s,2))*Power(t2,2) + 
                  (162 - 59*s + 3*Power(s,2))*Power(t2,3) - 
                  2*(-83 + 79*s)*Power(t2,4) + 138*Power(t2,5)) + 
               Power(s2,3)*(-3 + 5*(-34 + 3*s)*t2 + 
                  (-700 - 140*s + 17*Power(s,2))*Power(t2,2) + 
                  (-580 - 29*s + 50*Power(s,2))*Power(t2,3) + 
                  (-157 - 194*s + 39*Power(s,2))*Power(t2,4) + 
                  (208 - 320*s)*Power(t2,5) + 56*Power(t2,6)) + 
               Power(s2,2)*(11 - 13*(-22 + s)*t2 + 
                  (40 - 6*s - 93*Power(s,2))*Power(t2,2) + 
                  (-1024 - 203*s + 40*Power(s,2))*Power(t2,3) + 
                  (-674 + 114*s + 117*Power(s,2))*Power(t2,4) + 
                  (-219 - 260*s + 122*Power(s,2))*Power(t2,5) + 
                  (133 - 248*s)*Power(t2,6) + 5*Power(t2,7)) + 
               s2*(-2 + (-51 + 41*s)*t2 + 
                  (377 + 79*s + 97*Power(s,2))*Power(t2,2) - 
                  2*(-103 + 104*s + 66*Power(s,2))*Power(t2,3) - 
                  (359 + 109*s + 61*Power(s,2))*Power(t2,4) + 
                  (-408 + 248*s + 71*Power(s,2))*Power(t2,5) + 
                  (19 - 151*s + 130*Power(s,2))*Power(t2,6) + 
                  (30 - 70*s)*Power(t2,7)))) + 
         Power(t1,8)*(5 - 54*s2 + 22*Power(s2,2) - 43*t2 + 8*s2*t2 + 
            18*Power(s2,2)*t2 + 8*Power(t2,2) + 3*s2*Power(t2,2) + 
            Power(s1,3)*(1 + t2) - Power(s,3)*(2 + t2) + 
            Power(s1,2)*(-4 + 6*s2 + 4*t2 + 5*s2*t2 + 2*Power(t2,2)) + 
            Power(s,2)*(2 + 8*s2 + 9*t2 + 5*s2*t2 + 2*Power(t2,2) + 
               s1*(5 + 3*t2)) + 
            s1*(-9 + 6*t2 + 5*Power(t2,2) + 4*Power(s2,2)*(2 + t2) + 
               s2*(-3 + 23*t2 + 2*Power(t2,2))) - 
            s*(Power(s1,2)*(4 + 3*t2) + 
               s1*(-2 + 13*t2 + 4*Power(t2,2) + 2*s2*(7 + 5*t2)) + 
               2*(-14 + 5*t2 + 5*Power(t2,2) + Power(s2,2)*(3 + 2*t2) + 
                  s2*(8 + 15*t2 + Power(t2,2))))) + 
         Power(t1,7)*(-17 - 55*s2 + 116*Power(s2,2) - 30*Power(s2,3) - 
            7*t2 + 218*s2*t2 + 71*Power(s2,2)*t2 - 66*Power(s2,3)*t2 + 
            110*Power(t2,2) + 45*s2*Power(t2,2) - 
            70*Power(s2,2)*Power(t2,2) - 18*Power(s2,3)*Power(t2,2) - 
            25*Power(t2,3) - 23*s2*Power(t2,3) - 
            3*Power(s2,2)*Power(t2,3) - 
            Power(s1,3)*(2 + t2)*(1 + s2 + t2 + 2*s2*t2 + Power(t2,2)) + 
            Power(s,3)*(t2*(8 + 7*t2 + Power(t2,2)) + 
               s2*(3 + 7*t2 + 2*Power(t2,2))) - 
            Power(s1,2)*(-17 - 17*t2 + 4*Power(t2,2) + 9*Power(t2,3) + 
               2*Power(s2,2)*(7 + 10*t2 + 2*Power(t2,2)) + 
               s2*(-11 + 13*t2 + 29*Power(t2,2) + 2*Power(t2,3))) - 
            s1*(21 - 28*t2 - 10*Power(t2,2) + 14*Power(t2,3) + 
               2*Power(s2,3)*(6 + 8*t2 + Power(t2,2)) + 
               Power(s2,2)*(-27 + 41*t2 + 37*Power(t2,2) + 
                  Power(t2,3)) + 
               s2*(14 - 49*t2 + 68*Power(t2,2) + 10*Power(t2,3))) - 
            Power(s,2)*(-12 + 3*(-6 + 5*s1)*t2 + 
               (31 + 17*s1)*Power(t2,2) + (16 + 3*s1)*Power(t2,3) + 
               Power(s2,2)*(11 + 18*t2 + 4*Power(t2,2)) + 
               s2*(s1*(8 + 19*t2 + 6*Power(t2,2)) + 
                  2*(-2 + 32*t2 + 22*Power(t2,2) + Power(t2,3)))) + 
            s*(18 - 175*t2 - 3*Power(t2,2) + 44*Power(t2,3) + 
               2*Power(s2,3)*(3 + 6*t2 + Power(t2,2)) + 
               Power(s2,2)*(23 + 95*t2 + 51*Power(t2,2) + Power(t2,3)) + 
               s2*(-80 - 11*t2 + 131*Power(t2,2) + 20*Power(t2,3)) + 
               Power(s1,2)*(2 + 10*t2 + 13*Power(t2,2) + 
                  3*Power(t2,3) + s2*(7 + 17*t2 + 6*Power(t2,2))) + 
               s1*(Power(s2,2)*(28 + 38*t2 + 8*Power(t2,2)) + 
                  s2*(-35 + 66*t2 + 73*Power(t2,2) + 4*Power(t2,3)) + 
                  5*(-1 - 7*t2 + 5*Power(t2,2) + 5*Power(t2,3))))) + 
         t1*Power(t2,2)*(-4*Power(s2,8)*(-1 + t2)*t2 + 
            (1 + (-1 + s)*t2)*
             (6 + (-15 - s1 + s*(11 + 6*s1))*t2 + 
               (-11 + 11*Power(s,2) + 2*s*(-12 + s1) - 3*s1)*
                Power(t2,2) + 
               (37 + 5*Power(s,2) + 8*s1 - s*(40 + 13*s1))*
                Power(t2,3) + 
               (-6 - 21*Power(s,2) + s*(45 + s1))*Power(t2,4) + 
               (-16 - 5*Power(s,2) - 7*s1 + s*(23 + 7*s1))*
                Power(t2,5) + 
               (5 + 7*Power(s,2) + 3*s1 - 3*s*(5 + s1))*Power(t2,6)) - 
            4*Power(s2,7)*t2*
             (s - s*t2 + s1*(2 + t2) + t2*(-12 + 5*t2)) - 
            2*Power(s2,6)*(6 + (46 + 5*s)*t2 + 
               Power(s1,2)*(-3 + t2)*t2 + (40 + 23*s)*Power(t2,2) - 
               (68 + 15*s)*Power(t2,3) + 23*Power(t2,4) + 
               s1*(-3 - 3*(3 + s)*t2 + 12*Power(t2,2) + 13*Power(t2,3))) \
+ Power(s2,5)*(18 + (34 + 6*s)*t2 + 
               4*(-81 - 11*s + Power(s,2))*Power(t2,2) - 
               2*(92 + 65*s + 5*Power(s,2))*Power(t2,3) + 
               (160 + 93*s)*Power(t2,4) - 59*Power(t2,5) - 
               Power(s1,3)*(3 + 8*t2 + 2*Power(t2,2)) + 
               Power(s1,2)*(-12 + (-35 + s)*t2 + 
                  2*(5 + s)*Power(t2,2) - 16*Power(t2,3)) + 
               s1*(18 + 2*(93 + 10*s)*t2 + (172 + 45*s)*Power(t2,2) + 
                  (14 + 13*s)*Power(t2,3) - 45*Power(t2,4))) - 
            Power(s2,4)*(-6 - 2*(52 + 3*s)*t2 + 
               2*(-83 + 3*s + 6*Power(s,2))*Power(t2,2) + 
               (462 + 161*s + 5*Power(s,2))*Power(t2,3) + 
               (86 + 120*s + 53*Power(s,2))*Power(t2,4) - 
               2*(40 + 71*s)*Power(t2,5) + 40*Power(t2,6) + 
               Power(s1,3)*(-15 - 16*t2 + 16*Power(t2,2) + 
                  4*Power(t2,3)) + 
               Power(s1,2)*(-21 + 2*(34 + s)*t2 + 
                  3*(41 + s)*Power(t2,2) + (5 - 4*s)*Power(t2,3) + 
                  31*Power(t2,4)) + 
               s1*(60 + (154 + 5*s)*t2 + 
                  (-433 - 92*s + Power(s,2))*Power(t2,2) - 
                  (354 + 103*s + 3*Power(s,2))*Power(t2,3) - 
                  3*(17 + 12*s)*Power(t2,4) + 27*Power(t2,5))) + 
            Power(s2,3)*(-12 + (-19 + 8*s)*t2 + 
               (169 + 19*s - 15*Power(s,2))*Power(t2,2) + 
               (115 - 174*s - 10*Power(s,2) + 9*Power(s,3))*
                Power(t2,3) + 
               (-278 - 382*s - 43*Power(s,2) + 6*Power(s,3))*
                Power(t2,4) + 
               (43 + 11*s - 102*Power(s,2))*Power(t2,5) + 
               5*(1 + 21*s)*Power(t2,6) - 11*Power(t2,7) + 
               Power(s1,3)*(-24 + 17*t2 + 34*Power(t2,2) - 
                  12*Power(t2,3) - 3*Power(t2,4)) + 
               Power(s1,2)*(3 + (152 - 13*s)*t2 - 
                  (81 + 16*s)*Power(t2,2) + (-126 + s)*Power(t2,3) + 
                  (-12 + 5*s)*Power(t2,4) - 20*Power(t2,5)) + 
               s1*(15 - 7*(24 + 11*s)*t2 + 
                  2*(-173 - 20*s + Power(s,2))*Power(t2,2) + 
                  (404 + 148*s - 20*Power(s,2))*Power(t2,3) + 
                  (286 + 99*s + 5*Power(s,2))*Power(t2,4) + 
                  (1 + 36*s)*Power(t2,5) - 4*Power(t2,6))) + 
            Power(s2,2)*(3 + (-16 + 25*s)*t2 + 
               (-37 + 108*s + 67*Power(s,2))*Power(t2,2) + 
               (55 + 155*s + 72*Power(s,2) + 5*Power(s,3))*
                Power(t2,3) + 
               (3 - 308*s + 99*Power(s,2) + 23*Power(s,3))*
                Power(t2,4) + 
               (-13 - 333*s - 54*Power(s,2) + 19*Power(s,3))*
                Power(t2,5) + (12 + 80*s - 85*Power(s,2))*Power(t2,6) + 
               (-7 + 30*s)*Power(t2,7) - 
               Power(s1,3)*(-12 + 25*t2 - 2*Power(t2,2) - 
                  16*Power(t2,3) + 4*Power(t2,4) + Power(t2,5)) - 
               Power(s1,2)*(12 + (57 - 20*s)*t2 - 
                  3*(44 + 3*s)*Power(t2,2) + (7 + 39*s)*Power(t2,3) + 
                  (46 - 9*s)*Power(t2,4) + (7 - 4*s)*Power(t2,5) + 
                  3*Power(t2,6)) + 
               s1*(39 + (70 + 81*s)*t2 + 
                  (-157 - 153*s + 10*Power(s,2))*Power(t2,2) + 
                  3*(-66 - 27*s + 2*Power(s,2))*Power(t2,3) + 
                  (172 + 123*s - 38*Power(s,2))*Power(t2,4) + 
                  (96 + 42*s - Power(s,2))*Power(t2,5) + 
                  2*(-11 + 9*s)*Power(t2,6))) + 
            s2*(-9 + (6 - 48*s)*t2 + 
               (30 + 20*s - 66*Power(s,2))*Power(t2,2) + 
               (2 + 199*s - 41*Power(s,2) - 25*Power(s,3))*Power(t2,3) + 
               (-63 + 75*s + 92*Power(s,2) - 13*Power(s,3))*
                Power(t2,4) + 
               (18 - 248*s + 145*Power(s,2) + 11*Power(s,3))*
                Power(t2,5) + 
               (22 - 37*s - 38*Power(s,2) + 20*Power(s,3))*Power(t2,6) + 
               (-6 + 39*s - 26*Power(s,2))*Power(t2,7) + 
               s1*(-18 + (57 - 31*s)*t2 + 
                  (2 + 51*s - 17*Power(s,2))*Power(t2,2) + 
                  (-83 - 27*s + 33*Power(s,2))*Power(t2,3) + 
                  (9 - 8*s - 4*Power(s,2))*Power(t2,4) + 
                  (44 + s - 16*Power(s,2))*Power(t2,5) + 
                  (-9 + 9*s - 6*Power(s,2))*Power(t2,6) + 
                  (-2 + 5*s)*Power(t2,7)) + 
               Power(s1,2)*(-1 + t2)*t2*
                (s*(6 - 7*t2 - 11*Power(t2,2) + 7*Power(t2,3) + 
                     Power(t2,4)) - 
                  2*(1 + t2 - 5*Power(t2,2) + Power(t2,3) + 
                     2*Power(t2,4))))) + 
         Power(t1,6)*(-11 + 23*s2 + 105*Power(s2,2) - 104*Power(s2,3) + 
            18*Power(s2,4) + 90*t2 + 247*s2*t2 - 227*Power(s2,2)*t2 - 
            252*Power(s2,3)*t2 + 90*Power(s2,4)*t2 + 24*Power(t2,2) - 
            425*s2*Power(t2,2) - 499*Power(s2,2)*Power(t2,2) + 
            103*Power(s2,3)*Power(t2,2) + 66*Power(s2,4)*Power(t2,2) - 
            172*Power(t2,3) - 184*s2*Power(t2,3) + 
            109*Power(s2,2)*Power(t2,3) + 84*Power(s2,3)*Power(t2,3) + 
            6*Power(s2,4)*Power(t2,3) + 42*Power(t2,4) + 
            70*s2*Power(t2,4) + 22*Power(s2,2)*Power(t2,4) + 
            Power(s2,3)*Power(t2,4) + 
            Power(s1,3)*(1 + 4*t2 + 5*Power(t2,2) + Power(t2,3) + 
               4*Power(t2,4) + Power(s2,2)*(1 + 9*t2 + 6*Power(t2,2)) + 
               s2*(7 + 4*t2 + 17*Power(t2,2) + 11*Power(t2,3))) - 
            Power(s,3)*(Power(s2,2)*(2 + 6*t2 + 4*Power(t2,2)) + 
               t2*(1 + 4*t2 + 20*Power(t2,2) + 7*Power(t2,3)) + 
               s2*(-2 + 14*t2 + 41*Power(t2,2) + 15*Power(t2,3))) + 
            Power(s1,2)*(52 - 44*t2 - 44*Power(t2,2) - 8*Power(t2,3) + 
               19*Power(t2,4) + 
               2*Power(s2,3)*(8 + 16*t2 + 7*Power(t2,2)) + 
               Power(s2,2)*(-15 + 31*t2 + 83*Power(t2,2) + 
                  25*Power(t2,3)) + 
               s2*(-64 - 102*t2 + Power(t2,2) + 69*Power(t2,3) + 
                  9*Power(t2,4))) + 
            s1*(-72 + 49*t2 - 52*Power(t2,2) - 52*Power(t2,3) + 
               28*Power(t2,4) + 
               8*Power(s2,4)*(1 + 3*t2 + Power(t2,2)) + 
               Power(s2,3)*(-33 - 5*t2 + 91*Power(t2,2) + 
                  17*Power(t2,3)) + 
               Power(s2,2)*(59 - 35*t2 + 57*Power(t2,2) + 
                  112*Power(t2,3) + 5*Power(t2,4)) + 
               s2*(9 + 57*t2 - 155*Power(t2,2) + 94*Power(t2,3) + 
                  24*Power(t2,4))) + 
            Power(s,2)*(24 - 47*t2 - 147*Power(t2,2) + 50*Power(t2,3) + 
               57*Power(t2,4) + 
               Power(s2,3)*(4 + 19*t2 + 10*Power(t2,2)) + 
               Power(s2,2)*(-1 + 65*t2 + 132*Power(t2,2) + 
                  35*Power(t2,3)) + 
               s2*(-23 - 71*t2 + 170*Power(t2,2) + 163*Power(t2,3) + 
                  16*Power(t2,4)) + 
               s1*(2*Power(s2,2)*(4 + 12*t2 + 7*Power(t2,2)) + 
                  s2*(-3 + 7*t2 + 90*Power(t2,2) + 41*Power(t2,3)) + 
                  3*(-3 + 8*t2 - Power(t2,2) + 11*Power(t2,3) + 
                     6*Power(t2,4)))) - 
            s*(25 + 63*t2 - 489*Power(t2,2) - 76*Power(t2,3) + 
               110*Power(t2,4) + 
               2*Power(s2,4)*(1 + 6*t2 + 3*Power(t2,2)) + 
               2*Power(s2,3)*
                (7 + 54*t2 + 71*Power(t2,2) + 12*Power(t2,3)) + 
               Power(s2,2)*(-67 - 136*t2 + 286*Power(t2,2) + 
                  226*Power(t2,3) + 10*Power(t2,4)) + 
               s2*(13 - 327*t2 - 356*Power(t2,2) + 280*Power(t2,3) + 
                  84*Power(t2,4)) + 
               Power(s1,2)*(-20 + 23*t2 + 3*Power(t2,2) + 
                  14*Power(t2,3) + 15*Power(t2,4) + 
                  Power(s2,2)*(6 + 27*t2 + 16*Power(t2,2)) + 
                  s2*(14 + t2 + 66*Power(t2,2) + 37*Power(t2,3))) + 
               s1*(34 + 68*t2 - 147*Power(t2,2) - 8*Power(t2,3) + 
                  71*Power(t2,4) + 
                  Power(s2,3)*(22 + 54*t2 + 24*Power(t2,2)) + 
                  Power(s2,2)*
                   (-53 + 57*t2 + 200*Power(t2,2) + 60*Power(t2,3)) + 
                  s2*(10 - 202*t2 + 88*Power(t2,2) + 213*Power(t2,3) + 
                     25*Power(t2,4))))) + 
         Power(t2,3)*(-4*Power(s2,8)*t2 - 
            Power(1 + t2,2)*Power(1 + (-1 + s)*t2,3)*
             (2 - 3*t2 + Power(t2,2)) + 
            2*Power(s2,7)*(2 + (13 + 2*s)*t2 - 8*Power(t2,2) + 
               Power(t2,3) + s1*(-1 + Power(t2,2))) + 
            2*Power(s2,6)*(-3 - (2 + s)*t2 + (47 + 11*s)*Power(t2,2) - 
               2*(7 + s)*Power(t2,3) + 4*Power(t2,4) + 
               Power(s1,2)*(2 + 3*t2 + Power(t2,2)) + 
               s1*(-3 - (20 + 3*s)*t2 - (8 + s)*Power(t2,2) + 
                  3*Power(t2,3))) + 
            s2*(1 + (-1 + s)*t2)*
             (3 + (2 + 13*s)*t2 + 
               (-3 + 13*s + 4*Power(s,2))*Power(t2,2) + 
               (-11 - 8*s + 3*Power(s,2))*Power(t2,3) - 
               (-7 + 23*s + Power(s,2))*Power(t2,4) + 
               (3 + s - 3*Power(s,2))*Power(t2,5) + 
               (-1 + 4*s)*Power(t2,6) - 
               s1*(-1 + Power(t2,2))*
                (6 - 8*t2 - 5*s*Power(t2,2) + (2 + s)*Power(t2,3))) - 
            Power(s2,5)*(2 + Power(s1,3) + 34*t2 + 
               2*(8 + 5*s + Power(s,2))*Power(t2,2) - 
               2*(63 + 21*s + Power(s,2))*Power(t2,3) + 
               (30 + 17*s)*Power(t2,4) - 12*Power(t2,5) - 
               Power(s1,2)*(-4 + (17 + s)*t2 + 19*Power(t2,2) + 
                  6*Power(t2,3)) + 
               s1*(-20 + 2*(-6 + s)*t2 + 3*(34 + 7*s)*Power(t2,2) + 
                  (43 + 5*s)*Power(t2,3) - 6*Power(t2,4))) + 
            Power(s2,4)*(4 + (10 - 6*s)*t2 + 
               8*(-9 + s + Power(s,2))*Power(t2,2) + 
               (-9 + 16*s)*Power(t2,3) + 
               (75 + 37*s + 10*Power(s,2))*Power(t2,4) - 
               (19 + 27*s)*Power(t2,5) + 8*Power(t2,6) + 
               Power(s1,3)*(3 - 4*t2 - 2*Power(t2,2)) + 
               Power(s1,2)*(-13 - 29*t2 + (31 + 7*s)*Power(t2,2) + 
                  26*Power(t2,3) + 6*Power(t2,4)) + 
               s1*(-8 + (66 + 13*s)*t2 - 
                  (-41 + 13*s + Power(s,2))*Power(t2,2) - 
                  (109 + 38*s + Power(s,2))*Power(t2,3) - 
                  3*(13 + s)*Power(t2,4) + 2*Power(t2,5))) + 
            Power(s2,2)*(-1 - (1 + 12*s)*t2 - 
               (9 + 20*s + 13*Power(s,2))*Power(t2,2) + 
               (20 - 49*s - 19*Power(s,2))*Power(t2,3) - 
               (2 - 38*s + 19*Power(s,2) + 4*Power(s,3))*Power(t2,4) + 
               (-9 + 56*s + 4*Power(s,2) - 3*Power(s,3))*Power(t2,5) + 
               2*(1 - 4*s + 7*Power(s,2))*Power(t2,6) - 
               5*s*Power(t2,7) + 
               Power(s1,2)*(-1 + t2)*
                (6 - 7*t2 - 5*Power(t2,2) + (5 + 2*s)*Power(t2,3) + 
                  Power(t2,4)) + 
               s1*(-9 + (6 - 17*s)*t2 + (1 + 15*s)*Power(t2,2) + 
                  (13 + 37*s - Power(s,2))*Power(t2,3) + 
                  (-5 - 24*s + 9*Power(s,2))*Power(t2,4) - 
                  3*(3 + 4*s + Power(s,2))*Power(t2,5) + 
                  (3 + s)*Power(t2,6))) + 
            Power(s2,3)*(-2*Power(s1,3)*(1 - 2*t2 + Power(t2,3)) + 
               Power(s1,2)*(19 - (7 + s)*t2 - 
                  2*(21 + 4*s)*Power(t2,2) + 2*(7 + 4*s)*Power(t2,3) + 
                  14*Power(t2,4) + 2*Power(t2,5)) + 
               s1*(-1 + 6*(-5 + s)*t2 + 
                  (62 + 36*s + Power(s,2))*Power(t2,2) + 
                  (27 - 19*s + 3*Power(s,2))*Power(t2,3) - 
                  (49 + 34*s + 3*Power(s,2))*Power(t2,4) + 
                  (-9 + s)*Power(t2,5)) + 
               t2*(3 + 28*t2 - 38*Power(t2,2) - 6*Power(t2,3) + 
                  16*Power(t2,4) - 5*Power(t2,5) + 2*Power(t2,6) - 
                  Power(s,3)*Power(t2,2)*(2 + t2) + 
                  Power(s,2)*t2*
                   (-4 + 6*t2 + 5*Power(t2,2) + 18*Power(t2,3)) + 
                  s*(6 - 12*t2 + 29*Power(t2,2) + 67*Power(t2,3) + 
                     10*Power(t2,4) - 19*Power(t2,5))))) + 
         Power(t1,2)*t2*(-6 - 3*(-11 - s1 + s*(5 + 6*s1))*t2 + 
            (-17 + 2*Power(s1,2) - Power(s,2)*(29 + 22*s1) + 
               s*(105 + 30*s1 - 4*Power(s1,2)))*Power(t2,2) + 
            4*Power(s2,8)*Power(t2,2) - 
            (92 + 24*Power(s,3) + 50*s1 + 5*Power(s1,2) + 
               7*Power(s,2)*(-15 + 2*s1) + 
               s*(30 - 92*s1 - 7*Power(s1,2)))*Power(t2,3) + 
            (105 - 11*Power(s,3) + 71*s1 + 2*Power(s1,2) + 
               Power(s,2)*(143 + 73*s1) - 
               s*(323 + 160*s1 + 2*Power(s1,2)))*Power(t2,4) + 
            (17 + 62*Power(s,3) + 7*s1 + 6*Power(s1,2) - 
               Power(s,2)*(239 + 17*s1) + 
               s*(177 + 5*s1 - 6*Power(s1,2)))*Power(t2,5) + 
            (-52 + 8*Power(s,3) - 45*s1 - 8*Power(s1,2) - 
               3*Power(s,2)*(28 + 11*s1) + 
               2*s*(75 + 43*s1 + 4*Power(s1,2)))*Power(t2,6) + 
            (12 - 21*Power(s,3) + 14*s1 + 3*Power(s1,2) + 
               Power(s,2)*(71 + 18*s1) - s*(64 + 35*s1 + 3*Power(s1,2))\
)*Power(t2,7) + 2*Power(s2,7)*t2*
             (-6 + (-12 - 2*s + s1)*t2 + 17*Power(t2,2)) + 
            2*Power(s2,6)*t2*
             (12 - 47*t2 - 72*Power(t2,2) + 46*Power(t2,3) + 
               3*Power(s1,2)*(1 + t2) + s*(6 + 6*t2 - 25*Power(t2,2)) + 
               s1*(12 + 18*t2 + 17*Power(t2,2))) + 
            Power(s2,4)*(-18 - 6*(12 + s)*t2 + 
               (346 + 48*s)*Power(t2,2) + 
               (908 + 435*s + 29*Power(s,2))*Power(t2,3) + 
               (-406 + 35*s + 107*Power(s,2))*Power(t2,4) - 
               (89 + 309*s)*Power(t2,5) + 84*Power(t2,6) + 
               Power(s1,3)*(-9 + 42*t2 + 28*Power(t2,2) + 
                  6*Power(t2,3)) + 
               Power(s1,2)*(3 - 6*(-17 + s)*t2 - 
                  (86 + 25*s)*Power(t2,2) - (37 + 12*s)*Power(t2,3) + 
                  85*Power(t2,4)) + 
               s1*(-18 - 3*(88 + 9*s)*t2 + 
                  (-607 - 114*s + 7*Power(s,2))*Power(t2,2) - 
                  (182 + 91*s + Power(s,2))*Power(t2,3) + 
                  (62 - 114*s)*Power(t2,4) + 90*Power(t2,5))) + 
            Power(s2,3)*(-6 - 3*(45 + 4*s)*t2 + 
               (-355 + 3*s + 41*Power(s,2))*Power(t2,2) + 
               (535 + 426*s - 10*Power(s,2) - 16*Power(s,3))*
                Power(t2,3) + 
               (643 + 889*s + 158*Power(s,2) - 14*Power(s,3))*
                Power(t2,4) + 
               (-391 - 222*s + 238*Power(s,2))*Power(t2,5) + 
               (52 - 246*s)*Power(t2,6) + 25*Power(t2,7) + 
               Power(s1,3)*t2*
                (-111 + 19*t2 + 45*Power(t2,2) + 13*Power(t2,3)) + 
               Power(s1,2)*(15 + (48 + 45*s)*t2 + 
                  (358 + 27*s)*Power(t2,2) - 
                  19*(2 + 3*s)*Power(t2,3) - (15 + 29*s)*Power(t2,4) + 
                  76*Power(t2,5)) + 
               s1*(69 + (324 + 87*s)*t2 + 
                  (-388 - 116*s + Power(s,2))*Power(t2,2) + 
                  (-1031 - 227*s + 44*Power(s,2))*Power(t2,3) + 
                  (-398 - 134*s + 13*Power(s,2))*Power(t2,4) + 
                  (83 - 174*s)*Power(t2,5) + 23*Power(t2,6))) + 
            Power(s2,2)*(9 + (30 - 45*s)*t2 - 
               (178 + 171*s + 93*Power(s,2))*Power(t2,2) - 
               (216 + 67*s + 131*Power(s,2) + 22*Power(s,3))*
                Power(t2,3) + 
               (421 + 947*s - 206*Power(s,2) - 58*Power(s,3))*
                Power(t2,4) + 
               (-44 + 785*s + 236*Power(s,2) - 50*Power(s,3))*
                Power(t2,5) + 
               (-78 - 302*s + 219*Power(s,2))*Power(t2,6) + 
               (38 - 76*s)*Power(t2,7) + 
               Power(s1,3)*(18 + 45*t2 - 97*Power(t2,2) - 
                  12*Power(t2,3) + 18*Power(t2,4) + 10*Power(t2,5)) + 
               Power(s1,2)*(-48 - 3*(37 + 16*s)*t2 + 
                  (37 + 71*s)*Power(t2,2) + (227 + 91*s)*Power(t2,3) - 
                  (29 + 52*s)*Power(t2,4) - 5*(-6 + 7*s)*Power(t2,5) + 
                  20*Power(t2,6)) + 
               s1*(-51 - 9*(-18 + 7*s)*t2 + 
                  (620 + 228*s - 55*Power(s,2))*Power(t2,2) + 
                  (-251 - 72*s + 6*Power(s,2))*Power(t2,3) + 
                  (-608 - 133*s + 77*Power(s,2))*Power(t2,4) + 
                  3*(-75 - 34*s + 16*Power(s,2))*Power(t2,5) + 
                  (70 - 110*s)*Power(t2,6) + Power(t2,7))) + 
            Power(s2,5)*(12 + 3*Power(s1,3) + 6*(20 + s)*t2 - 
               2*(-194 - 62*s + Power(s,2))*Power(t2,2) + 
               2*(-111 + 38*s + 8*Power(s,2))*Power(t2,3) - 
               (228 + 185*s)*Power(t2,4) + 121*Power(t2,5) + 
               s1*(-6 - 6*(11 + 3*s)*t2 + (36 - 19*s)*Power(t2,2) + 
                  (82 - 23*s)*Power(t2,3) + 99*Power(t2,4)) + 
               Power(s1,2)*t2*
                (-45 - 10*t2 + 36*Power(t2,2) - s*(3 + 2*t2))) + 
            s2*(9 + 12*(1 + 5*s)*t2 + 
               (2 - 127*s + 83*Power(s,2))*Power(t2,2) + 
               (-92 - 535*s + 51*Power(s,2) + 62*Power(s,3))*
                Power(t2,3) + 
               (99 + 55*s - 292*Power(s,2) + 20*Power(s,3))*
                Power(t2,4) + 
               (23 + 848*s - 336*Power(s,2) - 47*Power(s,3))*
                Power(t2,5) + 
               (-75 + 72*s + 149*Power(s,2) - 57*Power(s,3))*
                Power(t2,6) + 
               2*(11 - 65*s + 36*Power(s,2))*Power(t2,7) + 
               2*Power(s1,3)*
                (-6 + 12*t2 - Power(t2,2) - 5*Power(t2,3) - 
                  Power(t2,4) + Power(t2,6)) + 
               s1*(6 + 3*(-61 + 13*s)*t2 + 
                  (105 + 11*s + 69*Power(s,2))*Power(t2,2) + 
                  (365 + 6*s - 95*Power(s,2))*Power(t2,3) - 
                  2*(53 + 51*s + 5*Power(s,2))*Power(t2,4) + 
                  (-214 + 92*s + 28*Power(s,2))*Power(t2,5) + 
                  (15 - 47*s + 51*Power(s,2))*Power(t2,6) + 
                  (12 - 29*s)*Power(t2,7)) + 
               Power(s1,2)*(30 - 131*Power(t2,2) + 110*Power(t2,3) - 
                  2*Power(t2,4) - 32*Power(t2,5) + 24*Power(t2,6) + 
                  Power(t2,7) + 
                  s*t2*(12 - 77*t2 + 31*Power(t2,2) + 64*Power(t2,3) - 
                     16*Power(t2,4) - 17*Power(t2,5))))) + 
         Power(t1,5)*(-1 + 46*s2 - 27*Power(s2,2) - 64*Power(s2,3) + 
            38*Power(s2,4) - 4*Power(s2,5) + 89*t2 - 79*s2*t2 - 
            378*Power(s2,2)*t2 - 58*Power(s2,3)*t2 + 
            270*Power(s2,4)*t2 - 54*Power(s2,5)*t2 - 150*Power(t2,2) - 
            599*s2*Power(t2,2) - 85*Power(s2,2)*Power(t2,2) + 
            843*Power(s2,3)*Power(t2,2) + 72*Power(s2,4)*Power(t2,2) - 
            90*Power(s2,5)*Power(t2,2) - 81*Power(t2,3) + 
            428*s2*Power(t2,3) + 1048*Power(s2,2)*Power(t2,3) + 
            189*Power(s2,3)*Power(t2,3) - 203*Power(s2,4)*Power(t2,3) - 
            22*Power(s2,5)*Power(t2,3) + 179*Power(t2,4) + 
            325*s2*Power(t2,4) - 57*Power(s2,2)*Power(t2,4) - 
            181*Power(s2,3)*Power(t2,4) - 30*Power(s2,4)*Power(t2,4) - 
            44*Power(t2,5) - 112*s2*Power(t2,5) - 
            65*Power(s2,2)*Power(t2,5) - 7*Power(s2,3)*Power(t2,5) + 
            Power(s,3)*t2*(3 + 5*t2 - 33*Power(t2,2) + 29*Power(t2,3) + 
               21*Power(t2,4) + Power(s2,3)*(1 + 2*t2) + 
               Power(s2,2)*(13 + 31*t2 + 23*Power(t2,2)) + 
               s2*(-17 + 25*t2 + 101*Power(t2,2) + 48*Power(t2,3))) - 
            Power(s1,3)*(-24 + 5*t2 + 4*Power(t2,2) + 7*Power(t2,3) - 
               5*Power(t2,4) + 6*Power(t2,5) + 
               Power(s2,3)*(1 + 7*t2 + 6*Power(t2,2)) + 
               Power(s2,2)*(5 + 3*t2 + 33*Power(t2,2) + 
                  25*Power(t2,3)) + 
               s2*(8 + 29*t2 - 8*Power(t2,2) + 21*Power(t2,3) + 
                  23*Power(t2,4))) - 
            Power(s1,2)*(4 + 115*t2 - 60*Power(t2,3) - 31*Power(t2,4) + 
               26*Power(t2,5) + 
               Power(s2,4)*(9 + 26*t2 + 18*Power(t2,2)) + 
               Power(s2,3)*(-13 + 40*t2 + 113*Power(t2,2) + 
                  66*Power(t2,3)) + 
               Power(s2,2)*(-63 - 253*t2 - 44*Power(t2,2) + 
                  156*Power(t2,3) + 62*Power(t2,4)) + 
               s2*(95 + 50*t2 - 298*Power(t2,2) - 53*Power(t2,3) + 
                  95*Power(t2,4) + 16*Power(t2,5))) - 
            s1*(52 - 113*t2 + 46*Power(t2,2) - 98*Power(t2,3) - 
               96*Power(t2,4) + 40*Power(t2,5) + 
               2*Power(s2,5)*(1 + 8*t2 + 6*Power(t2,2)) + 
               Power(s2,4)*(-12 - 46*t2 + 71*Power(t2,2) + 
                  47*Power(t2,3)) + 
               Power(s2,3)*(40 + 122*t2 - 7*Power(t2,2) + 
                  209*Power(t2,3) + 50*Power(t2,4)) + 
               Power(s2,2)*(-21 + 22*t2 - 17*Power(t2,2) + 
                  2*Power(t2,3) + 176*Power(t2,4) + 10*Power(t2,5)) + 
               s2*(-95 - 233*t2 - 22*Power(t2,2) - 279*Power(t2,3) + 
                  74*Power(t2,4) + 38*Power(t2,5))) - 
            Power(s,2)*(-3 + 105*t2 - 52*Power(t2,2) - 
               405*Power(t2,3) + 24*Power(t2,4) + 118*Power(t2,5) + 
               2*Power(s2,4)*t2*(3 + 4*t2) + 
               2*Power(s2,3)*t2*(10 + 58*t2 + 35*Power(t2,2)) + 
               Power(s2,2)*(-2 - 40*t2 + 106*Power(t2,2) + 
                  386*Power(t2,3) + 129*Power(t2,4)) + 
               s2*(5 - 67*t2 - 277*Power(t2,2) + 155*Power(t2,3) + 
                  334*Power(t2,4) + 54*Power(t2,5)) + 
               s1*(-2 - 37*t2 + 106*Power(t2,2) - 63*Power(t2,3) + 
                  15*Power(t2,4) + 45*Power(t2,5) + 
                  5*Power(s2,3)*(1 + 2*t2 + 2*Power(t2,2)) + 
                  Power(s2,2)*
                   (-13 + 30*t2 + 89*Power(t2,2) + 69*Power(t2,3)) + 
                  s2*(10 - 27*t2 - 43*Power(t2,2) + 166*Power(t2,3) + 
                     114*Power(t2,4)))) + 
            s*(11 + 146*t2 + 146*Power(t2,2) - 790*Power(t2,3) - 
               197*Power(t2,4) + 173*Power(t2,5) + 
               2*Power(s2,5)*t2*(2 + 3*t2) + 
               Power(s2,4)*(4 + 54*t2 + 147*Power(t2,2) + 
                  63*Power(t2,3)) + 
               Power(s2,3)*(-26 - 150*t2 + 131*Power(t2,2) + 
                  483*Power(t2,3) + 105*Power(t2,4)) + 
               Power(s2,2)*(11 - 199*t2 - 789*Power(t2,2) + 
                  226*Power(t2,3) + 522*Power(t2,4) + 40*Power(t2,5)) + 
               s2*(1 + 61*t2 - 627*Power(t2,2) - 1132*Power(t2,3) + 
                  327*Power(t2,4) + 195*Power(t2,5)) + 
               Power(s1,2)*(18 - 77*t2 + 59*Power(t2,2) - 
                  5*Power(t2,3) - 14*Power(t2,4) + 30*Power(t2,5) + 
                  Power(s2,3)*(7 + 16*t2 + 14*Power(t2,2)) + 
                  Power(s2,2)*t2*(26 + 94*t2 + 71*Power(t2,2)) + 
                  s2*(-24 + 29*t2 - 49*Power(t2,2) + 94*Power(t2,3) + 
                     89*Power(t2,4))) + 
               s1*(-65 + 52*t2 + 330*Power(t2,2) - 274*Power(t2,3) - 
                  109*Power(t2,4) + 120*Power(t2,5) + 
                  Power(s2,4)*(6 + 34*t2 + 26*Power(t2,2)) + 
                  Power(s2,3)*
                   (-20 - 13*t2 + 195*Power(t2,2) + 132*Power(t2,3)) + 
                  Power(s2,2)*
                   (1 - 142*t2 - 77*Power(t2,2) + 420*Power(t2,3) + 
                     182*Power(t2,4)) + 
                  s2*(76 + 159*t2 - 362*Power(t2,2) - 51*Power(t2,3) + 
                     327*Power(t2,4) + 65*Power(t2,5))))) + 
         Power(t1,4)*(14 - 11*s2 - 27*Power(s2,2) + 16*Power(s2,3) + 
            12*Power(s2,4) - 4*Power(s2,5) + 13*t2 - 149*s2*t2 - 
            73*Power(s2,2)*t2 + 227*Power(s2,3)*t2 + 130*Power(s2,4)*t2 - 
            108*Power(s2,5)*t2 + 12*Power(s2,6)*t2 - 160*Power(t2,2) - 
            91*s2*Power(t2,2) + 611*Power(s2,2)*Power(t2,2) + 
            749*Power(s2,3)*Power(t2,2) - 383*Power(s2,4)*Power(t2,2) - 
            194*Power(s2,5)*Power(t2,2) + 54*Power(s2,6)*Power(t2,2) + 
            135*Power(t2,3) + 696*s2*Power(t2,3) + 
            848*Power(s2,2)*Power(t2,3) - 819*Power(s2,3)*Power(t2,3) - 
            693*Power(s2,4)*Power(t2,3) + 148*Power(s2,5)*Power(t2,3) + 
            30*Power(s2,6)*Power(t2,3) + 99*Power(t2,4) - 
            136*s2*Power(t2,4) - 1090*Power(s2,2)*Power(t2,4) - 
            680*Power(s2,3)*Power(t2,4) + 222*Power(s2,4)*Power(t2,4) + 
            83*Power(s2,5)*Power(t2,4) - 137*Power(t2,5) - 
            325*s2*Power(t2,5) - 71*Power(s2,2)*Power(t2,5) + 
            229*Power(s2,3)*Power(t2,5) + 70*Power(s2,4)*Power(t2,5) + 
            33*Power(t2,6) + 104*s2*Power(t2,6) + 
            100*Power(s2,2)*Power(t2,6) + 20*Power(s2,3)*Power(t2,6) - 
            Power(s,3)*Power(t2,2)*
             (14 + 11*t2 - 85*Power(t2,2) + 20*Power(t2,3) + 
               35*Power(t2,4) + Power(s2,3)*(6 + 9*t2) + 
               Power(s2,2)*(32 + 68*t2 + 55*Power(t2,2)) + 
               s2*(-52 + 17*t2 + 135*Power(t2,2) + 85*Power(t2,3))) + 
            Power(s1,3)*(22 - 28*t2 - 3*Power(t2,2) + 2*Power(t2,3) + 
               6*Power(t2,4) - 6*Power(t2,5) + 4*Power(t2,6) + 
               2*Power(s2,4)*(1 + t2 + Power(t2,2)) + 
               3*Power(s2,3)*
                (-1 + 3*t2 + 9*Power(t2,2) + 7*Power(t2,3)) + 
               Power(s2,2)*(9 + 45*t2 + 5*Power(t2,2) + 
                  43*Power(t2,3) + 40*Power(t2,4)) + 
               s2*(-30 - 70*t2 + 53*Power(t2,2) - 16*Power(t2,3) + 
                  11*Power(t2,4) + 23*Power(t2,5))) + 
            Power(s1,2)*(-34 - 30*t2 + 69*Power(t2,2) + 66*Power(t2,3) - 
               30*Power(t2,4) - 44*Power(t2,5) + 24*Power(t2,6) + 
               Power(s2,5)*(2 + 11*t2 + 10*Power(t2,2)) + 
               Power(s2,4)*(-5 + 19*t2 + 89*Power(t2,2) + 
                  71*Power(t2,3)) + 
               Power(s2,3)*(-19 - 220*t2 - 162*Power(t2,2) + 
                  151*Power(t2,3) + 132*Power(t2,4)) + 
               Power(s2,2)*(48 + 201*t2 - 356*Power(t2,2) - 
                  252*Power(t2,3) + 162*Power(t2,4) + 79*Power(t2,5)) + 
               s2*(8 + 155*t2 + 408*Power(t2,2) - 306*Power(t2,3) - 
                  115*Power(t2,4) + 90*Power(t2,5) + 14*Power(t2,6))) + 
            s1*(-15 + 80*t2 - 18*Power(t2,2) + 92*Power(t2,3) - 
               114*Power(t2,4) - 113*Power(t2,5) + 41*Power(t2,6) + 
               4*Power(s2,6)*t2*(1 + 2*t2) + 
               Power(s2,5)*t2*(-22 + 7*t2 + 49*Power(t2,2)) + 
               Power(s2,4)*(4 + 122*t2 + 89*Power(t2,2) + 
                  139*Power(t2,3) + 112*Power(t2,4)) + 
               Power(s2,3)*(-8 - 126*t2 + 119*Power(t2,2) + 
                  14*Power(t2,3) + 267*Power(t2,4) + 72*Power(t2,5)) + 
               Power(s2,2)*(-46 - 341*t2 - 591*Power(t2,2) - 
                  242*Power(t2,3) - 103*Power(t2,4) + 179*Power(t2,5) + 
                  10*Power(t2,6)) + 
               s2*(65 + 137*t2 - 342*Power(t2,2) - 317*Power(t2,3) - 
                  406*Power(t2,4) + 38*Power(t2,5) + 42*Power(t2,6))) + 
            Power(s,2)*t2*(-9 + 192*t2 + 2*Power(s2,5)*t2 + 
               36*Power(t2,2) - 586*Power(t2,3) - 49*Power(t2,4) + 
               155*Power(t2,5) + Power(s2,4)*t2*(31 + 47*t2) + 
               Power(s2,3)*(-1 + 16*t2 + 262*Power(t2,2) + 
                  198*Power(t2,3)) + 
               Power(s2,2)*(-2 - 121*t2 - 4*Power(t2,2) + 
                  588*Power(t2,3) + 260*Power(t2,4)) + 
               s2*(12 - 60*t2 - 501*Power(t2,2) - 105*Power(t2,3) + 
                  415*Power(t2,4) + 100*Power(t2,5)) + 
               s1*(-14 - 57*t2 + 187*Power(t2,2) - 97*Power(t2,3) - 
                  35*Power(t2,4) + 60*Power(t2,5) + 
                  2*Power(s2,4)*(1 + t2) + 
                  Power(s2,3)*(18 + 33*t2 + 34*Power(t2,2)) + 
                  Power(s2,2)*
                   (-61 + 51*t2 + 134*Power(t2,2) + 133*Power(t2,3)) + 
                  s2*(55 - 89*t2 - 90*Power(t2,2) + 149*Power(t2,3) + 
                     165*Power(t2,4)))) - 
            s*(-5 - 14*t2 + 274*Power(t2,2) + 2*Power(s2,6)*Power(t2,2) + 
               308*Power(t2,3) - 794*Power(t2,4) - 279*Power(t2,5) + 
               181*Power(t2,6) + 
               2*Power(s2,5)*t2*(6 + 33*t2 + 31*Power(t2,2)) + 
               Power(s2,4)*(-4 - 68*t2 - 59*Power(t2,2) + 
                  352*Power(t2,3) + 214*Power(t2,4)) + 
               Power(s2,3)*(2 - 33*t2 - 507*Power(t2,2) - 
                  364*Power(t2,3) + 758*Power(t2,4) + 239*Power(t2,5)) + 
               Power(s2,2)*(1 + 7*t2 - 293*Power(t2,2) - 
                  1523*Power(t2,3) - 343*Power(t2,4) + 716*Power(t2,5) + 
                  85*Power(t2,6)) + 
               s2*(6 + 101*t2 + 292*Power(t2,2) - 720*Power(t2,3) - 
                  1765*Power(t2,4) + 184*Power(t2,5) + 275*Power(t2,6)) + 
               Power(s1,2)*(2 + 42*t2 - 101*Power(t2,2) + 
                  59*Power(t2,3) + 7*Power(t2,4) - 41*Power(t2,5) + 
                  30*Power(t2,6) + 
                  4*Power(s2,4)*(1 + t2 + Power(t2,2)) + 
                  Power(s2,3)*
                   (-12 + 47*t2 + 63*Power(t2,2) + 47*Power(t2,3)) + 
                  Power(s2,2)*
                   (12 - 85*t2 + 27*Power(t2,2) + 135*Power(t2,3) + 
                     121*Power(t2,4)) + 
                  s2*(-6 - 3*t2 - 23*Power(t2,2) - 101*Power(t2,3) + 
                     62*Power(t2,4) + 106*Power(t2,5))) + 
               s1*(12 - 144*t2 - 54*Power(t2,2) + 552*Power(t2,3) - 
                  254*Power(t2,4) - 199*Power(t2,5) + 130*Power(t2,6) + 
                  4*Power(s2,5)*t2*(2 + 3*t2) + 
                  Power(s2,4)*t2*(-17 + 67*t2 + 103*Power(t2,2)) + 
                  Power(s2,3)*
                   (1 - 104*Power(t2,2) + 266*Power(t2,3) + 
                     291*Power(t2,4)) + 
                  Power(s2,2)*
                   (14 + 97*t2 - 2*Power(t2,2) - 254*Power(t2,3) + 
                     446*Power(t2,4) + 287*Power(t2,5)) + 
                  s2*(-27 + 46*t2 + 336*Power(t2,2) - 182*Power(t2,3) - 
                     248*Power(t2,4) + 289*Power(t2,5) + 90*Power(t2,6))))\
))*T3(t2,t1))/((-1 + t1)*(s - s2 + t1)*Power(t1 - t2,2)*
       Power(1 - s2 + t1 - t2,2)*(s - s1 + t2)*Power(t1 - s2*t2,3)) - 
    (8*(8*Power(s2,8)*(-1 + t2)*Power(t2,3) + 
         (-2 + t2)*Power(-1 + t2,4)*Power(1 + t2,2)*
          Power(1 + (-1 + s)*t2,3) + 
         4*Power(t1,7)*(s + 3*t2 + 2*s*t2 - s1*(1 + 2*t2)) - 
         2*Power(s2,7)*Power(t2,2)*
          (4*t1*(-3 - t2 + 4*Power(t2,2)) + 
            t2*(-15 + 8*s*(-1 + t2) + 34*t2 - 14*Power(t2,2) + 
               Power(t2,3)) + s1*(-1 - 6*Power(t2,2) + Power(t2,3))) - 
         Power(t1,6)*(Power(s,2)*
             (-5 - 17*t2 - 3*Power(t2,2) + Power(t2,3)) + 
            s*(-1 - 40*t2 + 4*Power(t2,2) + 2*Power(t2,3) - 
               Power(t2,4) + 
               s1*(2 + 18*t2 + 6*Power(t2,2) - 2*Power(t2,3))) + 
            (-1 + t2)*(12 + 17*t2 - 12*Power(t2,2) + Power(t2,3) + 
               Power(s1,2)*(-3 - 2*t2 + Power(t2,2)) + 
               s1*(-1 - 5*t2 - Power(t2,2) + Power(t2,3)))) - 
         t1*Power(-1 + t2,3)*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(5 + 2*t2 - 12*Power(t2,2) - 2*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(-1 + t2,2)*
             (-6 - 14*t2 - 3*Power(t2,2) + 2*Power(t2,3) + 
               s1*(-1 - 5*t2 - Power(t2,2) + 3*Power(t2,3))) - 
            s*(-1 + t2)*(-1 - 19*t2 - 35*Power(t2,2) - 2*Power(t2,3) + 
               9*Power(t2,4) + 
               s1*(6 + 8*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4)))) - 
         2*Power(s2,6)*t2*(-12*Power(t1,2)*
             (-1 - 3*t2 + 2*Power(t2,2) + 2*Power(t2,3)) + 
            Power(s1,2)*(-1 + 2*t2 + 2*Power(t2,2) - 4*Power(t2,3) + 
               Power(t2,4)) + 
            t2*(1 + 4*(3 + 6*s + Power(s,2))*t2 - 
               (58 + 65*s + 4*Power(s,2))*Power(t2,2) + 
               (71 + 25*s)*Power(t2,3) - 2*(15 + s)*Power(t2,4) + 
               4*Power(t2,5)) + 
            t1*t2*(s*(23 + 4*t2 - 34*Power(t2,2) + Power(t2,3)) - 
               3*(-15 + 20*t2 + 20*Power(t2,2) - 12*Power(t2,3) + 
                  Power(t2,4))) + 
            s1*(t1*(2 + 7*t2 + 14*Power(t2,2) + 23*Power(t2,3) - 
                  4*Power(t2,4)) + 
               t2*(4 + s - 14*t2 + 5*s*t2 + 22*Power(t2,2) + 
                  7*s*Power(t2,2) - 15*Power(t2,3) - s*Power(t2,3) + 
                  3*Power(t2,4)))) + 
         Power(t1,2)*Power(-1 + t2,3)*
          (Power(s,3)*t2*(3 + 2*t2 - 17*Power(t2,2) + Power(t2,3) + 
               6*Power(t2,4)) - 
            Power(-1 + t2,2)*
             (-5 - 12*t2 - 7*Power(t2,2) + 3*Power(t2,3) + 
               Power(s1,2)*(2 - t2 - 2*Power(t2,2) + 3*Power(t2,3)) + 
               s1*(-6 - 29*t2 - 5*Power(t2,2) + 5*Power(t2,3))) + 
            s*(-1 + t2)*(9 - 48*t2 - 113*Power(t2,2) - 20*Power(t2,3) + 
               22*Power(t2,4) + 
               Power(s1,2)*(-4 + 3*t2 + Power(t2,2) - 5*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(15 + 53*t2 - 41*Power(t2,2) - 18*Power(t2,3) + 
                  17*Power(t2,4))) + 
            Power(s,2)*(-1 - 33*t2 - 17*Power(t2,2) + 92*Power(t2,3) + 
               15*Power(t2,4) - 23*Power(t2,5) + 
               s1*(4 + 8*t2 - 34*Power(t2,2) + 14*Power(t2,3) + 
                  12*Power(t2,4) - 9*Power(t2,5)))) + 
         Power(t1,5)*(Power(s,3)*
             (2 + 7*t2 + 3*Power(t2,2) + Power(t2,3) - Power(t2,4)) + 
            Power(-1 + t2,2)*
             (1 - 10*t2 - 34*Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,3)*(-1 + Power(t2,2)) + 
               s1*(33 + 10*t2 - 5*Power(t2,2) + 2*Power(t2,3)) + 
               Power(s1,2)*(-10 - 3*t2 - Power(t2,2) + 2*Power(t2,3))) - 
            s*(-1 + t2)*(22 + 119*t2 - 11*Power(t2,2) - 
               15*Power(t2,3) + 7*Power(t2,4) + 
               Power(s1,2)*t2*(-5 - 2*t2 + 3*Power(t2,2)) + 
               s1*(-20 - 19*t2 - 12*Power(t2,2) - Power(t2,3) + 
                  4*Power(t2,4))) + 
            Power(s,2)*(s1*(-1 - 4*t2 - 6*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4)) + 
               2*(3 + 9*t2 - Power(t2,2) - 6*Power(t2,3) + Power(t2,5)))) \
+ Power(t1,4)*(Power(s,3)*(8 - 8*t2 - 12*Power(t2,2) + 5*Power(t2,3) - 
               7*Power(t2,4) + Power(t2,5) + Power(t2,6)) - 
            Power(-1 + t2,3)*
             (-13 - 18*t2 - 37*Power(t2,2) + 7*Power(t2,3) + 
               Power(s1,3)*(2 + Power(t2,3)) + 
               Power(s1,2)*(-21 - 9*t2 - 2*Power(t2,2) + 
                  3*Power(t2,3)) + 
               s1*(47 + t2 - 10*Power(t2,2) + 5*Power(t2,3))) + 
            s*Power(-1 + t2,2)*
             (38 + 89*t2 - 72*Power(t2,2) - 35*Power(t2,3) + 
               20*Power(t2,4) + 
               Power(s1,2)*(-16 + 6*t2 - 6*Power(t2,2) + Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(7 + 36*t2 - 29*Power(t2,2) - 15*Power(t2,3) + 
                  13*Power(t2,4))) - 
            Power(s,2)*(-1 + t2)*
             (2*t2*(35 + 28*t2 - 18*Power(t2,2) - 5*Power(t2,3) + 
                  5*Power(t2,4)) + 
               s1*(-18 - 30*t2 + 10*Power(t2,2) - 13*Power(t2,3) + 
                  2*Power(t2,4) + 3*Power(t2,5)))) - 
         Power(t1,3)*(-1 + t2)*
          (Power(s,3)*(4 - 15*t2 - 12*Power(t2,2) + 22*Power(t2,3) - 
               14*Power(t2,4) - 3*Power(t2,5) + 4*Power(t2,6)) - 
            Power(-1 + t2,3)*
             (13 - 12*t2 - 19*Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,2)*t2*(-15 - 2*t2 + 4*Power(t2,2)) + 
               Power(s1,3)*(1 + t2 - Power(t2,2) + Power(t2,3)) + 
               2*s1*(-6 - 9*t2 - 5*Power(t2,2) + 3*Power(t2,3))) + 
            s*Power(-1 + t2,2)*
             (-1 - 11*t2 - 130*Power(t2,2) - 37*Power(t2,3) + 
               28*Power(t2,4) + 
               Power(s1,2)*(-26 + 17*t2 - 3*Power(t2,2) - 
                  7*Power(t2,3) + 6*Power(t2,4)) + 
               s1*(58 + 71*t2 - 54*Power(t2,2) - 25*Power(t2,3) + 
                  20*Power(t2,4))) - 
            Power(s,2)*(-1 + t2)*
             (12 + 87*t2 + 72*Power(t2,2) - 82*Power(t2,3) - 
               20*Power(t2,4) + 21*Power(t2,5) + 
               s1*(-27 - 37*t2 + 42*Power(t2,2) - 18*Power(t2,3) - 
                  9*Power(t2,4) + 9*Power(t2,5)))) + 
         Power(s2,5)*(-(Power(s1,3)*Power(-1 + t2,3)) - 
            8*Power(t1,3)*(-1 - 11*t2 - 6*Power(t2,2) + 
               14*Power(t2,3) + 4*Power(t2,4)) + 
            t1*t2*(4 + 2*(43 + 69*s + 11*Power(s,2))*t2 - 
               2*(171 + 130*s + Power(s,2))*Power(t2,2) + 
               (239 - 216*s - 46*Power(s,2))*Power(t2,3) + 
               (93 + 144*s + 2*Power(s,2))*Power(t2,4) - 
               (97 + 22*s)*Power(t2,5) + 17*Power(t2,6)) + 
            Power(s1,2)*(-1 + t2)*
             (t2*(5 + s - 15*t2 - 2*s*t2 - 3*Power(t2,2) - 
                  3*s*Power(t2,2) + 19*Power(t2,3) - 6*Power(t2,4)) + 
               t1*(2 + 7*t2 - 22*Power(t2,2) - 21*Power(t2,3) + 
                  10*Power(t2,4))) + 
            2*Power(t1,2)*t2*
             (s*(22 + 54*t2 - 66*Power(t2,2) - 49*Power(t2,3) + 
                  3*Power(t2,4)) + 
               3*(15 + 8*t2 - 75*Power(t2,2) + 2*Power(t2,3) + 
                  9*Power(t2,4) - Power(t2,5))) + 
            Power(t2,2)*(-2*Power(s,2)*t2*
                (-7 + 24*t2 - 12*Power(t2,2) + Power(t2,3)) - 
               2*Power(-1 + t2,2)*
                (-4 - 3*t2 + 11*Power(t2,2) - 29*Power(t2,3) + 
                  6*Power(t2,4)) + 
               s*(-2 + 10*t2 - 191*Power(t2,2) + 255*Power(t2,3) - 
                  89*Power(t2,4) + 17*Power(t2,5))) + 
            s1*(Power(t1,2)*(2 + 28*t2 + 54*Power(t2,2) + 
                  118*Power(t2,3) + 62*Power(t2,4) - 12*Power(t2,5)) + 
               t2*(-4 + 3*(4 + s)*t2 + 
                  (-57 + 24*s + 10*Power(s,2))*Power(t2,2) + 
                  (105 - 4*s + 2*Power(s,2))*Power(t2,3) - 
                  (73 + 28*s)*Power(t2,4) + (23 + 5*s)*Power(t2,5) - 
                  6*Power(t2,6)) + 
               t1*t2*(16 - 31*t2 + 8*Power(t2,2) + 68*Power(t2,3) - 
                  80*Power(t2,4) + 19*Power(t2,5) + 
                  s*(8 + 36*t2 + 44*Power(t2,2) + 68*Power(t2,3) - 
                     12*Power(t2,4))))) + 
         Power(s2,4)*(8*Power(t1,4)*
             (-4 - 14*t2 + 6*Power(t2,2) + 11*Power(t2,3) + Power(t2,4)) \
+ Power(t1,2)*(-2 - 2*(50 + 66*s + 9*Power(s,2))*t2 + 
               (288 + 39*s - 34*Power(s,2))*Power(t2,2) + 
               5*(41 + 158*s + 22*Power(s,2))*Power(t2,3) + 
               (-593 - 86*s + 70*Power(s,2))*Power(t2,4) + 
               (191 - 104*s - 8*Power(s,2))*Power(t2,5) + 
               3*(7 + 11*s)*Power(t2,6) - 10*Power(t2,7)) + 
            t1*t2*(-16 - (23 + 17*s + 43*Power(s,2))*t2 + 
               2*(99 + 297*s + 56*Power(s,2) + Power(s,3))*
                Power(t2,2) + 
               (-457 - 473*s + 68*Power(s,2) + 10*Power(s,3))*
                Power(t2,3) + 
               (447 - 211*s - 96*Power(s,2))*Power(t2,4) + 
               (-88 + 160*s + 19*Power(s,2))*Power(t2,5) - 
               (77 + 53*s)*Power(t2,6) + 16*Power(t2,7)) + 
            t2*(2 + 2*(-4 + 4*s + Power(s,2))*t2 + 
               (7 - 2*s + 18*Power(s,2) + 4*Power(s,3))*Power(t2,2) + 
               (-24 + 25*s + 64*Power(s,2) - 14*Power(s,3))*
                Power(t2,3) - 
               (-47 + 130*s + 110*Power(s,2) + 2*Power(s,3))*
                Power(t2,4) + 
               (19 + 180*s + 36*Power(s,2))*Power(t2,5) - 
               2*(46 + 54*s + 5*Power(s,2))*Power(t2,6) + 
               3*(19 + 9*s)*Power(t2,7) - 8*Power(t2,8)) + 
            Power(s1,3)*Power(-1 + t2,3)*
             (3 - 4*t2 - 2*Power(t2,2) + 2*t1*(1 + t2 + Power(t2,2))) - 
            2*Power(t1,3)*(15 + 92*t2 - 175*Power(t2,2) - 
               196*Power(t2,3) + 53*Power(t2,4) + 2*Power(t2,5) - 
               Power(t2,6) + 
               s*(7 + 64*t2 - 9*Power(t2,2) - 129*Power(t2,3) - 
                  26*Power(t2,4) + 3*Power(t2,5))) - 
            Power(s1,2)*(-1 + t2)*
             (Power(t1,2)*(9 - 4*t2 - 61*Power(t2,2) - 
                  22*Power(t2,3) + 18*Power(t2,4)) + 
               (-1 + t2)*(3 - 2*t2 + (18 + 7*s)*Power(t2,2) + 
                  (-7 + 5*s)*Power(t2,3) - 18*Power(t2,4) + 
                  6*Power(t2,5)) + 
               t1*(5 - 2*t2 - 64*Power(t2,2) + 33*Power(t2,3) + 
                  49*Power(t2,4) - 21*Power(t2,5) + 
                  4*s*(1 - t2 - 3*Power(t2,2) - 3*Power(t2,3) + 
                     Power(t2,4)))) + 
            s1*(2*Power(t1,3)*
                (-7 - 36*t2 - 64*Power(t2,2) - 92*Power(t2,3) - 
                  15*Power(t2,4) + 4*Power(t2,5)) + 
               t1*(4 + (2 - 17*s - 2*Power(s,2))*t2 + 
                  (103 - 56*s - 26*Power(s,2))*Power(t2,2) - 
                  (171 + 11*s + 12*Power(s,2))*Power(t2,3) - 
                  s*(15 + 22*s)*Power(t2,4) + 
                  2*(41 + 66*s + Power(s,2))*Power(t2,5) - 
                  (35 + 33*s)*Power(t2,6) + 15*Power(t2,7)) + 
               (-1 + t2)*t2*(Power(s,2)*t2*
                   (-1 + 43*t2 + 3*Power(t2,2) + Power(t2,3)) - 
                  Power(-1 + t2,2)*
                   (10 - 7*t2 + 29*Power(t2,2) + 9*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  s*(1 - 5*t2 - Power(t2,2) + 4*Power(t2,3) - 
                     2*Power(t2,4) + 3*Power(t2,5))) + 
               Power(t1,2)*(-8 - 22*t2 + 89*Power(t2,2) - 
                  154*Power(t2,3) + 58*Power(t2,4) + 58*Power(t2,5) - 
                  21*Power(t2,6) + 
                  2*s*(-3 - 32*t2 - 31*Power(t2,2) - 77*Power(t2,3) - 
                     50*Power(t2,4) + 13*Power(t2,5))))) + 
         s2*(-8*Power(t1,7)*(-1 + t2) + 
            t1*Power(-1 + t2,3)*
             (-3 + (-15 - 26*s + 15*Power(s,2))*t2 + 
               (22 - 127*s + 5*Power(s,2) + 13*Power(s,3))*
                Power(t2,2) + 
               (9 + 3*s - 56*Power(s,2) + 4*Power(s,3))*Power(t2,3) - 
               (6 - 155*s + 70*Power(s,2) + 8*Power(s,3))*Power(t2,4) + 
               (-10 + 19*s + 26*Power(s,2) - 11*Power(s,3))*
                Power(t2,5) + (3 - 24*s + 14*Power(s,2))*Power(t2,6)) - 
            Power(-1 + t2,3)*
             (3 + (-1 + 16*s)*t2 + 
               (-5 + 2*s + 17*Power(s,2))*Power(t2,2) + 
               4*(-2 - 6*s + 3*Power(s,2) + Power(s,3))*Power(t2,3) + 
               (18 - 26*s - 12*Power(s,2) + 3*Power(s,3))*Power(t2,4) - 
               (4 - 31*s + 25*Power(s,2) + Power(s,3))*Power(t2,5) + 
               (-4 + 6*s + 4*Power(s,2) - 3*Power(s,3))*Power(t2,6) + 
               (1 - 5*s + 4*Power(s,2))*Power(t2,7)) + 
            2*Power(t1,6)*(12 - 46*t2 - 7*Power(t2,2) - Power(t2,3) + 
               s*(4 - 26*t2 - 15*Power(t2,2) + Power(t2,3))) - 
            Power(s1,3)*Power(t1,2)*Power(-1 + t2,3)*
             (Power(t1,2)*(2 + 5*t2 + 2*Power(t2,2)) - 
               t1*(7 - 2*t2 + 2*Power(t2,2) + 5*Power(t2,3)) + 
               2*(-1 + t2 - Power(t2,2) + Power(t2,4))) + 
            Power(t1,5)*(t2*(-152 + 175*t2 + Power(t2,2) - 
                  27*Power(t2,3) + 3*Power(t2,4)) + 
               Power(s,2)*(-4 - 45*t2 - 69*Power(t2,2) - 
                  7*Power(t2,3) + 5*Power(t2,4)) - 
               2*s*(-24 + 101*t2 + 52*Power(t2,2) - 28*Power(t2,3) + 
                  6*Power(t2,4) + Power(t2,5))) + 
            Power(t1,4)*(-(Power(-1 + t2,2)*
                  (-31 + 49*t2 - 145*Power(t2,2) - 77*Power(t2,3) + 
                    14*Power(t2,4))) + 
               Power(s,3)*(-5 - 8*t2 - 26*Power(t2,2) - 
                  12*Power(t2,3) + Power(t2,4) + 2*Power(t2,5)) + 
               Power(s,2)*(8 - 46*t2 - 123*Power(t2,2) + 
                  79*Power(t2,3) + 47*Power(t2,4) - 23*Power(t2,5) - 
                  2*Power(t2,6)) + 
               s*(-6 - 385*t2 + 88*Power(t2,2) + 426*Power(t2,3) - 
                  142*Power(t2,4) + 5*Power(t2,5) + 14*Power(t2,6))) + 
            Power(t1,2)*(-1 + t2)*
             (-(Power(-1 + t2,3)*
                  (16 + 18*t2 - 48*Power(t2,2) - 35*Power(t2,3) + 
                    10*Power(t2,4))) + 
               Power(s,3)*t2*
                (1 - 25*t2 + Power(t2,2) - 24*Power(t2,3) - 
                  10*Power(t2,4) + 15*Power(t2,5)) + 
               s*Power(-1 + t2,2)*
                (7 - 38*t2 - 184*Power(t2,2) - 284*Power(t2,3) + 
                  3*Power(t2,4) + 43*Power(t2,5)) + 
               Power(s,2)*(1 + 106*t2 + 137*Power(t2,2) - 
                  275*Power(t2,3) - 79*Power(t2,4) + 151*Power(t2,5) - 
                  23*Power(t2,6) - 18*Power(t2,7))) + 
            Power(t1,3)*(Power(-1 + t2,3)*
                (-11 - 42*t2 - 125*Power(t2,2) - 85*Power(t2,3) + 
                  19*Power(t2,4)) - 
               s*Power(-1 + t2,2)*
                (11 + 276*t2 + 88*Power(t2,2) - 262*Power(t2,3) + 
                  11*Power(t2,4) + 36*Power(t2,5)) + 
               Power(s,3)*(-6 + t2 + 19*Power(t2,2) + 8*Power(t2,3) + 
                  28*Power(t2,4) + 7*Power(t2,5) - 9*Power(t2,6)) + 
               Power(s,2)*(-3 - 88*t2 - 236*Power(t2,2) + 
                  347*Power(t2,3) + 72*Power(t2,4) - 133*Power(t2,5) + 
                  31*Power(t2,6) + 10*Power(t2,7))) - 
            Power(s1,2)*t1*(-1 + t2)*
             (Power(t1,4)*(6 + 19*t2 + 4*Power(t2,2) - 5*Power(t2,3)) - 
               2*Power(-1 + t2,4)*
                (-1 - 2*t2 + 3*Power(t2,2) + 2*Power(t2,3)) + 
               t1*Power(-1 + t2,3)*
                (-1 - 39*t2 - 13*Power(t2,2) + 13*Power(t2,3) + 
                  Power(t2,4)) - 
               Power(t1,2)*Power(-1 + t2,2)*
                (-34 - 83*t2 - 14*Power(t2,2) + 12*Power(t2,3) + 
                  3*Power(t2,4)) + 
               Power(t1,3)*(17 + 27*t2 - 31*Power(t2,2) - 
                  25*Power(t2,3) + 10*Power(t2,4) + 2*Power(t2,5)) + 
               s*(Power(t1,3)*
                   (1 + 9*t2 + 21*Power(t2,2) - 5*Power(t2,3) - 
                     6*Power(t2,4)) + 
                  Power(-1 + t2,3)*
                   (6 - 7*t2 - 11*Power(t2,2) + 7*Power(t2,3) + 
                     Power(t2,4)) - 
                  t1*Power(-1 + t2,2)*
                   (-10 - 31*t2 - 10*Power(t2,2) - 2*Power(t2,3) + 
                     14*Power(t2,4)) + 
                  Power(t1,2)*
                   (26 - 18*t2 + 21*Power(t2,2) - 25*Power(t2,3) - 
                     23*Power(t2,4) + 19*Power(t2,5)))) + 
            s1*(Power(t1,6)*(20 + 36*t2 + 30*Power(t2,2) - 
                  2*Power(t2,3)) + 
               Power(-1 + t2,4)*(1 + t2)*
                (6 + 2*(-7 + 3*s)*t2 + (8 - 13*s)*Power(t2,2) + 
                  (2 + 6*s - 5*Power(s,2))*Power(t2,3) + 
                  (-2 + s + Power(s,2))*Power(t2,4)) + 
               Power(t1,5)*(11 - 14*t2 + 40*Power(t2,2) - 
                  44*Power(t2,3) + 5*Power(t2,4) + 2*Power(t2,5) + 
                  s*(30 + 24*t2 + 84*Power(t2,2) + 16*Power(t2,3) - 
                     10*Power(t2,4))) + 
               t1*Power(-1 + t2,3)*
                (Power(-1 + t2,2)*
                   (-15 - 38*t2 - 26*Power(t2,2) + 7*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  Power(s,2)*t2*
                   (17 - 18*t2 + Power(t2,2) + Power(t2,3) + 
                     9*Power(t2,4)) + 
                  s*(13 - 12*t2 + 27*Power(t2,2) - 34*Power(t2,3) + 
                     17*Power(t2,4) - 6*Power(t2,5) - 5*Power(t2,6))) + 
               Power(t1,4)*(-(Power(-1 + t2,2)*
                     (78 + 118*t2 - 7*Power(t2,2) + 7*Power(t2,3) + 
                       4*Power(t2,4))) + 
                  Power(s,2)*
                   (14 + t2 + 21*Power(t2,2) + 31*Power(t2,3) - 
                     Power(t2,4) - 6*Power(t2,5)) + 
                  s*(83 - 67*t2 + 106*Power(t2,2) - 64*Power(t2,3) - 
                     93*Power(t2,4) + 31*Power(t2,5) + 4*Power(t2,6))) - 
               Power(t1,3)*(-1 + t2)*
                (-(Power(-1 + t2,2)*
                     (67 + 113*t2 - 22*Power(t2,2) + 8*Power(t2,3) + 
                       6*Power(t2,4))) + 
                  Power(s,2)*
                   (41 + 75*t2 + 24*Power(t2,2) + 54*Power(t2,3) + 
                     13*Power(t2,4) - 23*Power(t2,5)) + 
                  s*(-18 - 81*t2 + 136*Power(t2,2) + 49*Power(t2,3) - 
                     127*Power(t2,4) + 28*Power(t2,5) + 13*Power(t2,6))) \
+ Power(t1,2)*Power(-1 + t2,2)*
                (-(Power(-1 + t2,2)*
                     (-21 - 65*t2 - 28*Power(t2,2) + 6*Power(t2,3) + 
                       6*Power(t2,4))) + 
                  Power(s,2)*
                   (12 + 76*t2 + 16*Power(t2,2) + 26*Power(t2,3) + 
                     17*Power(t2,4) - 27*Power(t2,5)) + 
                  s*(-46 - 137*t2 + 183*Power(t2,2) + 59*Power(t2,3) - 
                     85*Power(t2,4) + 12*Power(t2,5) + 14*Power(t2,6))))) \
+ Power(s2,2)*(8*Power(t1,6)*(-4 + t2 + 3*Power(t2,2)) + 
            Power(-1 + t2,3)*
             (1 + t2 + 12*s*t2 + 
               (9 + 20*s + 13*Power(s,2))*Power(t2,2) + 
               (-20 + 49*s + 19*Power(s,2))*Power(t2,3) + 
               (2 - 38*s + 19*Power(s,2) + 4*Power(s,3))*Power(t2,4) + 
               (9 - 56*s - 4*Power(s,2) + 3*Power(s,3))*Power(t2,5) - 
               2*(1 - 4*s + 7*Power(s,2))*Power(t2,6) + 5*s*Power(t2,7)) \
+ Power(s1,3)*t1*Power(-1 + t2,3)*
             (1 - 2*t2 - 4*Power(t2,2) + 4*Power(t2,3) + Power(t2,4) + 
               Power(t1,2)*(1 + 9*t2 + 6*Power(t2,2)) - 
               t1*(5 + 6*Power(t2,2) + 7*Power(t2,3))) + 
            Power(t1,5)*(s*(-42 + 40*t2 + 156*Power(t2,2) + 
                  30*Power(t2,3) - 4*Power(t2,4)) + 
               6*(-13 + 24*t2 + 34*Power(t2,2) - 4*Power(t2,3) + 
                  Power(t2,4))) + 
            Power(t1,4)*(-48 + 257*t2 + 41*Power(t2,2) - 
               392*Power(t2,3) + 140*Power(t2,4) + 5*Power(t2,5) - 
               3*Power(t2,6) + 
               Power(s,2)*(-5 + 13*t2 + 151*Power(t2,2) + 
                  91*Power(t2,3) - 6*Power(t2,4) - 4*Power(t2,5)) + 
               s*(-91 + 112*t2 + 615*Power(t2,2) - 60*Power(t2,3) - 
                  67*Power(t2,4) + 30*Power(t2,5) + Power(t2,6))) - 
            t1*(-1 + t2)*(Power(s,3)*Power(t2,2)*
                (17 - 47*t2 - 13*Power(t2,2) - 9*Power(t2,3) + 
                  10*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (5 + 19*t2 + 2*Power(t2,2) - 49*Power(t2,3) - 
                  23*Power(t2,4) + 7*Power(t2,5)) + 
               Power(s,2)*t2*
                (16 + 91*t2 + 76*Power(t2,2) - 261*Power(t2,3) + 
                  77*Power(t2,4) + 44*Power(t2,5) - 43*Power(t2,6)) + 
               s*Power(-1 + t2,2)*
                (1 - 112*Power(t2,2) - 254*Power(t2,3) - 
                  159*Power(t2,4) + 56*Power(t2,5) + 15*Power(t2,6))) + 
            Power(t1,2)*(Power(s,3)*t2*
                (5 - 10*t2 - 17*Power(t2,2) - 41*Power(t2,3) - 
                  20*Power(t2,4) + 11*Power(t2,5)) - 
               Power(-1 + t2,3)*
                (5 - 33*t2 - 80*Power(t2,2) - 221*Power(t2,3) - 
                  54*Power(t2,4) + 17*Power(t2,5)) + 
               Power(s,2)*t2*
                (43 + 244*t2 + 20*Power(t2,2) - 448*Power(t2,3) + 
                  137*Power(t2,4) + 52*Power(t2,5) - 48*Power(t2,6)) + 
               s*Power(-1 + t2,2)*
                (-3 + 83*t2 + 429*Power(t2,2) - 159*Power(t2,3) - 
                  220*Power(t2,4) + 94*Power(t2,5) + 16*Power(t2,6))) + 
            Power(t1,3)*(Power(s,3)*
                (2 + 6*t2 + 22*Power(t2,2) + 40*Power(t2,3) + 
                  6*Power(t2,4) - 4*Power(t2,5)) + 
               Power(-1 + t2,2)*
                (-43 - 2*t2 + 17*Power(t2,2) - 329*Power(t2,3) - 
                  36*Power(t2,4) + 13*Power(t2,5)) + 
               Power(s,2)*(-9 - 35*t2 + 255*Power(t2,2) + 
                  64*Power(t2,3) - 181*Power(t2,4) + 3*Power(t2,5) + 
                  23*Power(t2,6)) - 
               s*(19 - 268*t2 - 711*Power(t2,2) + 876*Power(t2,3) + 
                  222*Power(t2,4) - 215*Power(t2,5) + 70*Power(t2,6) + 
                  7*Power(t2,7))) - 
            Power(s1,2)*(-1 + t2)*
             (Power(-1 + t2,3)*
                (6 - 7*t2 - 5*Power(t2,2) + (5 + 2*s)*Power(t2,3) + 
                  Power(t2,4)) + 
               2*Power(t1,4)*
                (1 - 22*t2 - 17*Power(t2,2) + 6*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(t1,2)*(-1 + t2)*
                (17 + 95*t2 - 41*Power(t2,2) - 86*Power(t2,3) + 
                  4*Power(t2,4) + 11*Power(t2,5) + 
                  s*(-2 + 46*t2 + 47*Power(t2,2) + 4*Power(t2,3) - 
                     23*Power(t2,4))) - 
               t1*Power(-1 + t2,2)*
                (12 + 6*t2 + Power(t2,2) - 26*Power(t2,3) + 
                  4*Power(t2,4) + 3*Power(t2,5) + 
                  s*(-14 + 39*t2 + 33*Power(t2,2) - 15*Power(t2,3) - 
                     4*Power(t2,4))) + 
               Power(t1,3)*(1 - 87*t2 + 22*Power(t2,2) + 
                  80*Power(t2,3) - 3*Power(t2,4) - 13*Power(t2,5) + 
                  s*(2 - 9*t2 - 44*Power(t2,2) - 5*Power(t2,3) + 
                     16*Power(t2,4)))) + 
            s1*(2*Power(t1,5)*
                (-19 - 38*t2 - 57*Power(t2,2) - 14*Power(t2,3) + 
                  2*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (-9 + (6 - 17*s)*t2 + (1 + 15*s)*Power(t2,2) + 
                  (13 + 37*s - Power(s,2))*Power(t2,3) + 
                  (-5 - 24*s + 9*Power(s,2))*Power(t2,4) - 
                  3*(3 + 4*s + Power(s,2))*Power(t2,5) + 
                  (3 + s)*Power(t2,6)) + 
               Power(t1,4)*(-27 + 50*t2 - 89*Power(t2,2) + 
                  28*Power(t2,3) + 55*Power(t2,4) - 16*Power(t2,5) - 
                  Power(t2,6) + 
                  2*s*(-30 - 21*t2 - 73*Power(t2,2) - 67*Power(t2,3) + 
                     7*Power(t2,4) + 4*Power(t2,5))) - 
               Power(t1,2)*(-1 + t2)*
                (Power(-1 + t2,2)*
                   (29 + 127*t2 + 82*Power(t2,2) - 3*Power(t2,3) + 
                     22*Power(t2,4) + Power(t2,5)) + 
                  Power(s,2)*
                   (-13 - 82*t2 - 92*Power(t2,2) - 85*Power(t2,3) - 
                     31*Power(t2,4) + 27*Power(t2,5)) + 
                  s*(21 + 39*t2 - 13*Power(t2,2) - 145*Power(t2,3) + 
                     90*Power(t2,4) + 58*Power(t2,5) - 50*Power(t2,6))) \
+ t1*Power(-1 + t2,2)*(Power(s,2)*t2*
                   (16 - 127*t2 - 20*Power(t2,2) + Power(t2,3) + 
                     10*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (-8 - 60*t2 - 51*Power(t2,2) + 4*Power(t2,3) + 
                     13*Power(t2,4)) - 
                  3*s*(-4 - 4*t2 - 38*Power(t2,2) + 43*Power(t2,3) + 
                     Power(t2,4) - 5*Power(t2,5) + 7*Power(t2,6))) + 
               Power(t1,3)*(Power(-1 + t2,2)*
                   (55 + 231*t2 + 96*Power(t2,2) - 7*Power(t2,3) + 
                     23*Power(t2,4) + 2*Power(t2,5)) + 
                  2*Power(s,2)*
                   (-9 - 10*t2 - 4*Power(t2,2) - 35*Power(t2,3) - 
                     9*Power(t2,4) + 7*Power(t2,5)) - 
                  s*(77 + 26*t2 + 18*Power(t2,2) + 108*Power(t2,3) - 
                     243*Power(t2,4) - 22*Power(t2,5) + 36*Power(t2,6))))\
) + Power(s2,3)*(-24*Power(t1,5)*
             (-2 - 2*t2 + 3*Power(t2,2) + Power(t2,3)) + 
            t1*(-2 + (3 - 5*s - 5*Power(s,2))*t2 - 
               (-30 + 76*s + 49*Power(s,2) + 9*Power(s,3))*Power(t2,2) + 
               (-6 - 69*s - 223*Power(s,2) + 28*Power(s,3))*
                Power(t2,3) + 
               (-15 + 598*s + 241*Power(s,2) + 12*Power(s,3))*
                Power(t2,4) + 
               (-224 - 657*s + 94*Power(s,2) + 20*Power(s,3))*
                Power(t2,5) + 
               (360 + 158*s - 104*Power(s,2) - 3*Power(s,3))*
                Power(t2,6) + 
               (-138 + 99*s + 46*Power(s,2))*Power(t2,7) - 
               (13 + 48*s)*Power(t2,8) + 5*Power(t2,9)) - 
            Power(s1,3)*Power(-1 + t2,3)*
             (Power(t1,2)*(1 + 7*t2 + 6*Power(t2,2)) + 
               2*(1 - 2*t2 + Power(t2,3)) - 
               3*t1*(-1 + 2*t2 + 2*Power(t2,2) + Power(t2,3))) + 
            2*Power(t1,4)*(-3*
                (-14 - 5*t2 + 83*Power(t2,2) + 11*Power(t2,3) - 
                  6*Power(t2,4) + Power(t2,5)) + 
               s*(22 + 44*t2 - 103*Power(t2,2) - 83*Power(t2,3) - 
                  Power(t2,4) + Power(t2,5))) + 
            (-1 + t2)*t2*(Power(s,3)*Power(t2,2)*
                (6 - 19*t2 - 2*Power(t2,2) + Power(t2,3)) + 
               Power(s,2)*Power(t2,2)*
                (30 + 23*t2 - 64*Power(t2,2) + 29*Power(t2,3) - 
                  18*Power(t2,4)) - 
               Power(-1 + t2,3)*
                (5 + 3*t2 - 3*Power(t2,2) - 13*Power(t2,3) - 
                  7*Power(t2,4) + 2*Power(t2,5)) + 
               s*Power(-1 + t2,2)*
                (-2 - 4*t2 - 69*Power(t2,2) - 87*Power(t2,3) - 
                  8*Power(t2,4) + 19*Power(t2,5))) + 
            Power(t1,3)*(38 - 14*t2 - 559*Power(t2,2) + 
               539*Power(t2,3) + 126*Power(t2,4) - 150*Power(t2,5) + 
               19*Power(t2,6) + Power(t2,7) + 
               Power(s,2)*(4 + 33*t2 - 73*Power(t2,2) - 
                  183*Power(t2,3) - 31*Power(t2,4) + 10*Power(t2,5)) - 
               2*s*(-21 - 91*t2 + 342*Power(t2,2) + 238*Power(t2,3) - 
                  125*Power(t2,4) + 9*Power(t2,5) + 8*Power(t2,6))) + 
            Power(t1,2)*(Power(s,3)*t2*
                (-1 - 5*t2 - 27*Power(t2,2) - 17*Power(t2,3) + 
                  2*Power(t2,4)) + 
               Power(s,2)*t2*
                (38 - 37*t2 - 307*Power(t2,2) + 139*Power(t2,3) + 
                  85*Power(t2,4) - 38*Power(t2,5)) - 
               Power(-1 + t2,2)*
                (-8 - 92*t2 + 111*Power(t2,2) - 167*Power(t2,3) - 
                  251*Power(t2,4) + 23*Power(t2,5) + 4*Power(t2,6)) + 
               s*(2 + 26*t2 - 665*Power(t2,2) - 130*Power(t2,3) + 
                  991*Power(t2,4) - 251*Power(t2,5) - 16*Power(t2,6) + 
                  43*Power(t2,7))) - 
            Power(s1,2)*(-1 + t2)*
             (-2*Power(t1,3)*
                (6 - 18*t2 - 35*Power(t2,2) + 7*Power(t2,4)) + 
               Power(-1 + t2,2)*
                (9 + (-9 + s)*t2 + (2 + 8*s)*Power(t2,2) + 
                  (2 + 4*s)*Power(t2,3) - 6*Power(t2,4) + 2*Power(t2,5)) \
+ Power(t1,2)*(-13 + 60*t2 + 56*Power(t2,2) - 94*Power(t2,3) - 
                  35*Power(t2,4) + 26*Power(t2,5) + 
                  s*(-7 + 10*t2 + 35*Power(t2,2) + 16*Power(t2,3) - 
                     14*Power(t2,4))) - 
               t1*(-1 + t2)*(7 + 32*t2 + 39*Power(t2,2) - 
                  72*Power(t2,3) - 20*Power(t2,4) + 14*Power(t2,5) + 
                  s*(-12 + 32*t2 + 31*Power(t2,2) + 4*Power(t2,3) - 
                     7*Power(t2,4)))) + 
            s1*(-2*Power(t1,4)*
                (-17 - 48*t2 - 85*Power(t2,2) - 62*Power(t2,3) + 
                  Power(t2,4) + Power(t2,5)) - 
               Power(-1 + t2,2)*
                (-3 + t2 - 12*s*t2 + 
                  (-26 + 20*s + Power(s,2))*Power(t2,2) + 
                  (57 + 47*s - 42*Power(s,2))*Power(t2,3) + 
                  (-14 - 35*s + 4*Power(s,2))*Power(t2,4) - 
                  (26 + 21*s + 3*Power(s,2))*Power(t2,5) + 
                  (11 + s)*Power(t2,6)) + 
               Power(t1,3)*(25 - 26*t2 - 5*Power(t2,2) + 
                  132*Power(t2,3) - 141*Power(t2,4) + 6*Power(t2,5) + 
                  9*Power(t2,6) + 
                  s*(38 + 92*t2 + 96*Power(t2,2) + 236*Power(t2,3) + 
                     42*Power(t2,4) - 24*Power(t2,5))) + 
               t1*(-1 + t2)*(Power(s,2)*t2*
                   (3 - 117*t2 - 47*Power(t2,2) - 29*Power(t2,3) + 
                     6*Power(t2,4)) + 
                  Power(-1 + t2,2)*
                   (10 + 22*t2 + 89*Power(t2,2) + 25*Power(t2,3) + 
                     22*Power(t2,4) + 4*Power(t2,5)) + 
                  s*(5 + 8*t2 + 28*Power(t2,2) - 61*Power(t2,3) - 
                     8*Power(t2,4) + 65*Power(t2,5) - 37*Power(t2,6))) + 
               Power(t1,2)*(Power(s,2)*
                   (5 + 25*t2 + 19*Power(t2,2) + 45*Power(t2,3) + 
                     36*Power(t2,4) - 10*Power(t2,5)) - 
                  Power(-1 + t2,2)*
                   (14 + 129*t2 + 221*Power(t2,2) + 25*Power(t2,4) + 
                     11*Power(t2,5)) + 
                  s*(14 + 109*t2 - 42*Power(t2,2) + 148*Power(t2,3) - 
                     136*Power(t2,4) - 153*Power(t2,5) + 60*Power(t2,6))))\
))*T4(-1 + t2))/
     ((-1 + t1)*(s - s2 + t1)*Power(1 - s2 + t1 - t2,2)*Power(-1 + t2,2)*
       (s - s1 + t2)*Power(t1 - s2*t2,3)) - 
    (8*(-2*Power(s2,7)*Power(t2,2)*(s1 + t2) + 
         Power(1 + t2,2)*Power(1 + (-1 + s)*t2,3)*
          (2 - 3*t2 + Power(t2,2)) - 
         Power(t1,6)*(-6 + Power(s,2) + s1 + Power(s1,2) + t2 + s1*t2 - 
            s*(1 + 2*s1 + t2)) + 
         2*Power(s2,6)*t2*(-(Power(s1,2)*(1 + t2)) + 
            t2*(1 + 2*(3 + s)*t2 - 4*Power(t2,2) + t1*(3 - s + 3*t2)) + 
            s1*((4 + s - 3*t2)*t2 + t1*(2 + 4*t2))) + 
         Power(t1,5)*(-1 - 17*t2 + 5*Power(t2,2) + Power(s1,3)*(1 + t2) - 
            Power(s,3)*(2 + t2) + s1*(-7 - 3*t2 + 2*Power(t2,2)) + 
            Power(s1,2)*(-2 + t2 + 2*Power(t2,2)) + 
            Power(s,2)*(2*t2*(3 + t2) + s1*(5 + 3*t2)) + 
            s*(22 + t2 - 7*Power(t2,2) - Power(s1,2)*(4 + 3*t2) + 
               s1*(2 - 7*t2 - 4*Power(t2,2)))) + 
         Power(t1,4)*(-1 + 2*t2 + 29*Power(t2,2) - 7*Power(t2,3) + 
            Power(s,3)*t2*(2 + 4*t2 + Power(t2,2)) + 
            s1*(-41 + 5*t2 + 10*Power(t2,2) - 5*Power(t2,3)) + 
            Power(s1,2)*(15 + 9*t2 + 2*Power(t2,2) - 3*Power(t2,3)) - 
            Power(s1,3)*(2 + Power(t2,3)) - 
            Power(s,2)*(s1*(2 + 8*Power(t2,2) + 3*Power(t2,3)) + 
               2*(-4 - 13*t2 + 5*Power(t2,2) + 5*Power(t2,3))) + 
            s*(4 - 79*t2 - 15*Power(t2,2) + 20*Power(t2,3) + 
               Power(s1,2)*(4 - 2*t2 + 4*Power(t2,2) + 3*Power(t2,3)) + 
               s1*(-5 - 31*t2 - 2*Power(t2,2) + 13*Power(t2,3)))) - 
         t1*(1 + (-1 + s)*t2)*
          (Power(s,2)*t2*(5 + 2*t2 - 12*Power(t2,2) - 2*Power(t2,3) + 
               4*Power(t2,4)) + 
            Power(-1 + t2,2)*(-6 - 14*t2 - 3*Power(t2,2) + 
               2*Power(t2,3) + 
               s1*(-1 - 5*t2 - Power(t2,2) + 3*Power(t2,3))) - 
            s*(-1 + t2)*(-1 - 19*t2 - 35*Power(t2,2) - 2*Power(t2,3) + 
               9*Power(t2,4) + 
               s1*(6 + 8*t2 - 5*Power(t2,2) - 4*Power(t2,3) + 
                  3*Power(t2,4)))) - 
         Power(t1,3)*(Power(s,3)*t2*
             (1 - 8*t2 + 5*Power(t2,2) + 4*Power(t2,3)) - 
            (-1 + t2)*(17 - 6*t2 - 19*Power(t2,2) + 5*Power(t2,3) + 
               Power(s1,2)*t2*(-15 - 2*t2 + 4*Power(t2,2)) + 
               Power(s1,3)*(1 + t2 - Power(t2,2) + Power(t2,3)) + 
               2*s1*(-5 - 9*t2 - 5*Power(t2,2) + 3*Power(t2,3))) - 
            Power(s,2)*(24 - 9*t2 - 81*Power(t2,2) + Power(t2,3) + 
               21*Power(t2,4) + 
               s1*(-13 + 24*t2 - 18*Power(t2,2) + 9*Power(t2,4))) + 
            s*(19 + t2 - 124*Power(t2,2) - 37*Power(t2,3) + 
               28*Power(t2,4) + 
               Power(s1,2)*(-22 + 17*t2 - 3*Power(t2,2) - 
                  7*Power(t2,3) + 6*Power(t2,4)) + 
               s1*(42 + 69*t2 - 54*Power(t2,2) - 25*Power(t2,3) + 
                  20*Power(t2,4)))) + 
         Power(t1,2)*(Power(s,3)*t2*
             (3 + 2*t2 - 17*Power(t2,2) + Power(t2,3) + 6*Power(t2,4)) - 
            Power(-1 + t2,2)*(-5 - 12*t2 - 7*Power(t2,2) + 
               3*Power(t2,3) + 
               Power(s1,2)*(2 - t2 - 2*Power(t2,2) + 3*Power(t2,3)) + 
               s1*(-6 - 29*t2 - 5*Power(t2,2) + 5*Power(t2,3))) + 
            s*(-1 + t2)*(9 - 48*t2 - 113*Power(t2,2) - 20*Power(t2,3) + 
               22*Power(t2,4) + 
               Power(s1,2)*(-4 + 3*t2 + Power(t2,2) - 5*Power(t2,3) + 
                  3*Power(t2,4)) + 
               s1*(15 + 53*t2 - 41*Power(t2,2) - 18*Power(t2,3) + 
                  17*Power(t2,4))) + 
            Power(s,2)*(-1 - 33*t2 - 17*Power(t2,2) + 92*Power(t2,3) + 
               15*Power(t2,4) - 23*Power(t2,5) + 
               s1*(4 + 8*t2 - 34*Power(t2,2) + 14*Power(t2,3) + 
                  12*Power(t2,4) - 9*Power(t2,5)))) - 
         Power(s2,5)*(Power(s1,3) - 
            Power(s1,2)*(t2*(5 + s - 5*t2 - 6*Power(t2,2)) + 
               t1*(2 + 11*t2 + 10*Power(t2,2))) + 
            s1*(2*Power(t1,2)*(1 + 8*t2 + 6*Power(t2,2)) + 
               t2*(-4 + s*(3 - 5*t2)*t2 - 15*Power(t2,2) + 
                  6*Power(t2,3)) + 
               t1*t2*(16 + 11*t2 - 19*Power(t2,2) + 4*s*(2 + 3*t2))) + 
            t2*(t1*(4 + (44 + 6*s - 2*Power(s,2))*t2 + 
                  (2 + 22*s)*Power(t2,2) - 17*Power(t2,3)) + 
               Power(t1,2)*(-2*s*(2 + 3*t2) + 
                  6*(1 + 3*t2 + Power(t2,2))) + 
               t2*(2*Power(s,2)*t2 + s*(-2 + 8*t2 - 17*Power(t2,2)) + 
                  4*(2 - t2 - 7*Power(t2,2) + 3*Power(t2,3))))) + 
         Power(s2,4)*(-2*Power(t1,3)*
             (-1 + s - 9*t2 + 6*s*t2 - 9*Power(t2,2) + 3*s*Power(t2,2) - 
               Power(t2,3)) + 
            Power(t1,2)*(2 + (52 - 6*Power(s,2))*t2 + 
               (66 + 45*s - 8*Power(s,2))*Power(t2,2) + 
               33*(-1 + s)*Power(t2,3) - 10*Power(t2,4)) + 
            t1*t2*(16 + (17 + 29*s + 7*Power(s,2))*t2 + 
               (-111 - 19*s + 19*Power(s,2))*Power(t2,2) + 
               (1 - 53*s)*Power(t2,3) + 16*Power(t2,4)) - 
            t2*(2 + 2*(-1 + 4*s + Power(s,2))*t2 + 
               (5 + 16*s + 2*Power(s,2))*Power(t2,2) + 
               (-9 + 9*s + 10*Power(s,2))*Power(t2,3) - 
               27*(1 + s)*Power(t2,4) + 8*Power(t2,5)) + 
            Power(s1,3)*(3 - 4*t2 - 2*Power(t2,2) + 
               2*t1*(1 + t2 + Power(t2,2))) + 
            Power(s1,2)*(3 + t2 + (19 + 7*s)*Power(t2,2) + 
               6*Power(t2,3) - 6*Power(t2,4) - 
               Power(t1,2)*(9 + 26*t2 + 18*Power(t2,2)) - 
               t1*(5 + 8*t2 - 17*Power(t2,2) - 21*Power(t2,3) + 
                  4*s*(1 + t2 + Power(t2,2)))) + 
            s1*(8*Power(t1,3)*(1 + 3*t2 + Power(t2,2)) + 
               t1*(-4 + (-14 + 17*s + 2*Power(s,2))*t2 + 
                  (-43 - 7*s + 2*Power(s,2))*Power(t2,2) - 
                  (14 + 33*s)*Power(t2,3) + 15*Power(t2,4)) + 
               Power(t1,2)*(8 + 40*t2 - 17*Power(t2,2) - 
                  21*Power(t2,3) + s*(6 + 34*t2 + 26*Power(t2,2))) + 
               t2*(-10 + 7*t2 + Power(s,2)*(-1 + t2)*t2 - 
                  21*Power(t2,2) - 5*Power(t2,3) - 2*Power(t2,4) + 
                  s*(1 - 3*t2 - 4*Power(t2,2) + 3*Power(t2,3))))) - 
         Power(s2,3)*(-(t1*(2 + (3 + 5*s + 5*Power(s,2))*t2 + 
                 (3 + 73*s - 2*Power(s,2) - 3*Power(s,3))*Power(t2,2) + 
                 (-2 + 111*s + 32*Power(s,2) - 3*Power(s,3))*
                  Power(t2,3) + 
                 (-113 - 53*s + 46*Power(s,2))*Power(t2,4) + 
                 (10 - 48*s)*Power(t2,5) + 5*Power(t2,6))) - 
            2*Power(t1,4)*(-3*(1 + 3*t2 + Power(t2,2)) + 
               s*(3 + 6*t2 + Power(t2,2))) + 
            Power(s1,3)*(Power(t1,2)*(1 + 7*t2 + 6*Power(t2,2)) + 
               2*(1 - 2*t2 + Power(t2,3)) - 
               3*t1*(-1 + 2*t2 + 2*Power(t2,2) + Power(t2,3))) - 
            Power(t1,3)*(-20 - 94*t2 - 11*Power(t2,2) + 26*Power(t2,3) + 
               Power(t2,4) + Power(s,2)*(4 + 19*t2 + 10*Power(t2,2)) - 
               2*s*(-1 + 12*t2 + 35*Power(t2,2) + 8*Power(t2,3))) + 
            Power(t1,2)*(8 + 46*t2 - 125*Power(t2,2) - 96*Power(t2,3) + 
               39*Power(t2,4) + 4*Power(t2,5) - 
               Power(s,3)*t2*(1 + 2*t2) + 
               Power(s,2)*t2*(2 + 47*t2 + 38*Power(t2,2)) + 
               s*(2 + 44*t2 + 61*Power(t2,2) - 111*Power(t2,3) - 
                  43*Power(t2,4))) + 
            t2*(-5 + 2*t2 + 2*Power(t2,2) + 8*Power(t2,3) - 
               9*Power(t2,5) + 2*Power(t2,6) - 
               Power(s,3)*Power(t2,2)*(2 + t2) + 
               Power(s,2)*Power(t2,2)*(6 + 7*t2 + 18*Power(t2,2)) + 
               s*(2 + 4*t2 + 49*Power(t2,2) + 75*Power(t2,3) + 
                  2*Power(t2,4) - 19*Power(t2,5))) + 
            Power(s1,2)*(9 + (-9 + s)*t2 + (2 + 8*s)*Power(t2,2) + 
               2*Power(t2,3) - 6*Power(t2,4) + 2*Power(t2,5) - 
               2*Power(t1,3)*(8 + 16*t2 + 7*Power(t2,2)) - 
               Power(t1,2)*(13 + 2*t2 - 29*Power(t2,2) - 
                  26*Power(t2,3) + s*(7 + 16*t2 + 14*Power(t2,2))) + 
               t1*(7 + 39*t2 + 60*Power(t2,2) - 14*Power(t2,4) + 
                  s*(-12 + 20*t2 + 15*Power(t2,2) + 7*Power(t2,3)))) + 
            s1*(3 + 2*(1 + 6*s)*t2 - 
               (-28 + 8*s + Power(s,2))*Power(t2,2) + 
               (-31 - 39*s + Power(s,2))*Power(t2,3) - 
               (13 + 18*s + 3*Power(s,2))*Power(t2,4) + 
               (11 + s)*Power(t2,5) + 
               2*Power(t1,4)*(6 + 8*t2 + Power(t2,2)) + 
               Power(t1,3)*(23 + 23*t2 - 37*Power(t2,2) - 
                  9*Power(t2,3) + s*(22 + 54*t2 + 24*Power(t2,2))) + 
               Power(t1,2)*(-14 - 53*t2 - 76*Power(t2,2) + 
                  22*Power(t2,3) + 11*Power(t2,4) + 
                  5*Power(s,2)*(1 + 2*t2 + 2*Power(t2,2)) + 
                  s*(14 + 37*t2 - 57*Power(t2,2) - 60*Power(t2,3))) - 
               t1*(10 + 22*t2 + 65*Power(t2,2) + 7*Power(t2,3) + 
                  16*Power(t2,4) + 4*Power(t2,5) + 
                  3*Power(s,2)*t2*(1 + 3*t2 + 2*Power(t2,2)) + 
                  s*(5 + 18*t2 + 47*Power(t2,2) + Power(t2,3) - 
                     37*Power(t2,4))))) + 
         Power(s2,2)*(1 + t2 + 12*s*t2 + 9*Power(t2,2) + 
            20*s*Power(t2,2) + 13*Power(s,2)*Power(t2,2) - 
            20*Power(t2,3) + 49*s*Power(t2,3) + 
            19*Power(s,2)*Power(t2,3) + 2*Power(t2,4) - 
            38*s*Power(t2,4) + 19*Power(s,2)*Power(t2,4) + 
            4*Power(s,3)*Power(t2,4) + 9*Power(t2,5) - 56*s*Power(t2,5) - 
            4*Power(s,2)*Power(t2,5) + 3*Power(s,3)*Power(t2,5) - 
            2*Power(t2,6) + 8*s*Power(t2,6) - 14*Power(s,2)*Power(t2,6) + 
            5*s*Power(t2,7) + Power(t1,5)*(6 - 6*s + 6*t2 - 4*s*t2) + 
            Power(t1,4)*(38 + 53*t2 - 16*Power(t2,2) - 3*Power(t2,3) - 
               Power(s,2)*(11 + 18*t2 + 4*Power(t2,2)) + 
               s*(1 + 41*t2 + 33*Power(t2,2) + Power(t2,3))) + 
            Power(s1,3)*t1*(1 - 2*t2 - 4*Power(t2,2) + 4*Power(t2,3) + 
               Power(t2,4) + Power(t1,2)*(1 + 9*t2 + 6*Power(t2,2)) - 
               t1*(5 + 6*Power(t2,2) + 7*Power(t2,3))) + 
            Power(t1,3)*(25 - 29*t2 - 148*Power(t2,2) + 13*Power(t2,3) + 
               13*Power(t2,4) - 
               2*Power(s,3)*(1 + 3*t2 + 2*Power(t2,2)) + 
               Power(s,2)*(-3 + 20*t2 + 72*Power(t2,2) + 
                  23*Power(t2,3)) + 
               s*(23 + 113*t2 - 31*Power(t2,2) - 91*Power(t2,3) - 
                  7*Power(t2,4))) + 
            Power(t1,2)*(-5 + 3*t2 - 28*Power(t2,2) + 155*Power(t2,3) + 
               30*Power(t2,4) - 17*Power(t2,5) + 
               Power(s,3)*t2*(7 + 13*t2 + 11*Power(t2,2)) + 
               Power(s,2)*t2*
                (23 + 11*t2 - 92*Power(t2,2) - 48*Power(t2,3)) + 
               s*(3 - 62*t2 - 209*Power(t2,2) - 86*Power(t2,3) + 
                  110*Power(t2,4) + 16*Power(t2,5))) - 
            t1*(5 + 14*t2 - 5*Power(t2,2) - 45*Power(t2,3) + 
               8*Power(t2,4) + 30*Power(t2,5) - 7*Power(t2,6) + 
               Power(s,3)*Power(t2,2)*(5 + 11*t2 + 10*Power(t2,2)) + 
               Power(s,2)*t2*
                (16 + 15*t2 + 36*Power(t2,2) - 42*Power(t2,3) - 
                  43*Power(t2,4)) + 
               s*(1 - 52*Power(t2,2) - 218*Power(t2,3) - 
                  141*Power(t2,4) + 56*Power(t2,5) + 15*Power(t2,6))) + 
            Power(s1,2)*(-2*Power(t1,4)*(7 + 10*t2 + 2*Power(t2,2)) - 
               (-1 + t2)*(6 - 7*t2 - 5*Power(t2,2) + 
                  (5 + 2*s)*Power(t2,3) + Power(t2,4)) + 
               Power(t1,3)*(-13 + t2 + 29*Power(t2,2) + 
                  13*Power(t2,3) - s*(6 + 27*t2 + 16*Power(t2,2))) + 
               Power(t1,2)*(17 + 94*t2 + 53*Power(t2,2) - 
                  15*Power(t2,3) - 11*Power(t2,4) + 
                  s*(-2 + 8*t2 + 19*Power(t2,2) + 23*Power(t2,3))) + 
               t1*(12 + 6*t2 + Power(t2,2) - 26*Power(t2,3) + 
                  4*Power(t2,4) + 3*Power(t2,5) + 
                  s*(-14 + 39*t2 + 21*Power(t2,2) - 15*Power(t2,3) - 
                     4*Power(t2,4)))) + 
            s1*(9 + (-6 + 17*s)*t2 - (1 + 15*s)*Power(t2,2) + 
               (-13 - 37*s + Power(s,2))*Power(t2,3) + 
               (5 + 24*s - 9*Power(s,2))*Power(t2,4) + 
               3*(3 + 4*s + Power(s,2))*Power(t2,5) - 
               (3 + s)*Power(t2,6) + 4*Power(t1,5)*(2 + t2) + 
               Power(t1,4)*(21 - 11*t2 - 19*Power(t2,2) - Power(t2,3) + 
                  s*(28 + 38*t2 + 8*Power(t2,2))) + 
               Power(t1,3)*(-25 - 78*t2 - 24*Power(t2,2) + 
                  25*Power(t2,3) + 2*Power(t2,4) + 
                  2*Power(s,2)*(4 + 12*t2 + 7*Power(t2,2)) + 
                  s*(39 + 3*t2 - 86*Power(t2,2) - 36*Power(t2,3))) - 
               Power(t1,2)*(29 + 103*t2 + 52*Power(t2,2) - 
                  21*Power(t2,3) + 22*Power(t2,4) + Power(t2,5) + 
                  Power(s,2)*
                   (-13 + 12*t2 + 23*Power(t2,2) + 27*Power(t2,3)) + 
                  s*(21 + 69*t2 + 86*Power(t2,2) - 42*Power(t2,3) - 
                     50*Power(t2,4))) + 
               t1*(8 + 52*t2 - 15*Power(t2,2) - 49*Power(t2,3) - 
                  9*Power(t2,4) + 13*Power(t2,5) + 
                  Power(s,2)*t2*
                   (-16 - 9*t2 + 11*Power(t2,2) + 10*Power(t2,3)) - 
                  3*s*(4 + 8*t2 + 30*Power(t2,2) + Power(t2,3) + 
                     2*Power(t2,4) + 7*Power(t2,5))))) - 
         s2*(3 - 2*(-1 + s)*Power(t1,6) - t2 + 16*s*t2 - 5*Power(t2,2) + 
            2*s*Power(t2,2) + 17*Power(s,2)*Power(t2,2) - 8*Power(t2,3) - 
            24*s*Power(t2,3) + 12*Power(s,2)*Power(t2,3) + 
            4*Power(s,3)*Power(t2,3) + 18*Power(t2,4) - 26*s*Power(t2,4) - 
            12*Power(s,2)*Power(t2,4) + 3*Power(s,3)*Power(t2,4) - 
            4*Power(t2,5) + 31*s*Power(t2,5) - 25*Power(s,2)*Power(t2,5) - 
            Power(s,3)*Power(t2,5) - 4*Power(t2,6) + 6*s*Power(t2,6) + 
            4*Power(s,2)*Power(t2,6) - 3*Power(s,3)*Power(t2,6) + 
            Power(t2,7) - 5*s*Power(t2,7) + 4*Power(s,2)*Power(t2,7) + 
            t1*(3 + (15 + 26*s - 15*Power(s,2))*t2 - 
               (22 - 127*s + 5*Power(s,2) + 13*Power(s,3))*Power(t2,2) - 
               (9 + 3*s - 56*Power(s,2) + 4*Power(s,3))*Power(t2,3) + 
               (6 - 155*s + 70*Power(s,2) + 8*Power(s,3))*Power(t2,4) + 
               (10 - 19*s - 26*Power(s,2) + 11*Power(s,3))*Power(t2,5) + 
               (-3 + 24*s - 14*Power(s,2))*Power(t2,6)) + 
            Power(t1,5)*(26 + 6*t2 - 3*Power(t2,2) - 
               Power(s,2)*(8 + 5*t2) + 2*s*(2 + 9*t2 + Power(t2,2))) + 
            Power(t1,4)*(13 - 64*t2 - 27*Power(t2,2) + 14*Power(t2,3) - 
               Power(s,3)*(3 + 7*t2 + 2*Power(t2,2)) + 
               s*(50 + 49*t2 - 47*Power(t2,2) - 14*Power(t2,3)) + 
               Power(s,2)*(-8 + 34*t2 + 29*Power(t2,2) + 2*Power(t2,3))) + 
            Power(s1,3)*Power(t1,2)*
             (Power(t1,2)*(2 + 5*t2 + 2*Power(t2,2)) - 
               t1*(7 - 2*t2 + 2*Power(t2,2) + 5*Power(t2,3)) + 
               2*(-1 + t2 - Power(t2,2) + Power(t2,4))) + 
            Power(t1,3)*(1 - 22*t2 + 71*Power(t2,2) + 61*Power(t2,3) - 
               19*Power(t2,4) + 
               Power(s,3)*(-2 + 5*t2 + 20*Power(t2,2) + 9*Power(t2,3)) + 
               Power(s,2)*(19 + 47*t2 - 20*Power(t2,2) - 
                  61*Power(t2,3) - 10*Power(t2,4)) + 
               s*(-5 - 103*t2 - 191*Power(t2,2) + 47*Power(t2,3) + 
                  36*Power(t2,4))) - 
            Power(t1,2)*(16 + 14*t2 - 60*Power(t2,2) - 5*Power(t2,3) + 
               45*Power(t2,4) - 10*Power(t2,5) + 
               Power(s,3)*t2*
                (-11 + t2 + 20*Power(t2,2) + 15*Power(t2,3)) + 
               Power(s,2)*(1 + 82*Power(t2,2) + 51*Power(t2,3) - 
                  59*Power(t2,4) - 18*Power(t2,5)) + 
               s*(7 + 22*t2 - 148*Power(t2,2) - 266*Power(t2,3) + 
                  3*Power(t2,4) + 43*Power(t2,5))) + 
            Power(s1,2)*t1*(-(Power(t1,4)*(6 + 5*t2)) + 
               Power(t1,3)*(-7 + t2 + 14*Power(t2,2) + 2*Power(t2,3)) + 
               Power(t1,2)*(28 + 65*t2 + 14*Power(t2,2) - 
                  12*Power(t2,3) - 3*Power(t2,4)) + 
               2*(1 - 6*Power(t2,2) + 6*Power(t2,3) + Power(t2,4) - 
                  2*Power(t2,5)) + 
               t1*(1 + 38*t2 - 26*Power(t2,2) - 26*Power(t2,3) + 
                  12*Power(t2,4) + Power(t2,5)) + 
               s*(-6 + 13*t2 + 4*Power(t2,2) - 18*Power(t2,3) + 
                  6*Power(t2,4) + Power(t2,5) - 
                  Power(t1,3)*(7 + 17*t2 + 6*Power(t2,2)) + 
                  Power(t1,2)*
                   (14 - 14*t2 + 15*Power(t2,2) + 19*Power(t2,3)) + 
                  t1*(10 + 19*t2 + 10*Power(t2,2) + 2*Power(t2,3) - 
                     14*Power(t2,4)))) + 
            s1*(2*Power(t1,6) - 
               (-1 + Power(t2,2))*
                (6 + 2*(-7 + 3*s)*t2 + (8 - 13*s)*Power(t2,2) + 
                  (2 + 6*s - 5*Power(s,2))*Power(t2,3) + 
                  (-2 + s + Power(s,2))*Power(t2,4)) + 
               Power(t1,5)*(5 - 11*t2 - 2*Power(t2,2) + 2*s*(7 + 5*t2)) + 
               Power(t1,4)*(-22 - 38*t2 + 11*Power(t2,2) + 
                  4*Power(t2,3) + 
                  Power(s,2)*(8 + 19*t2 + 6*Power(t2,2)) - 
                  s*(-27 + 24*t2 + 43*Power(t2,2) + 4*Power(t2,3))) - 
               Power(t1,3)*(59 + 91*t2 - 40*Power(t2,2) + 8*Power(t2,3) + 
                  6*Power(t2,4) + 
                  Power(s,2)*(-1 - 11*t2 + 33*Power(t2,2) + 
                     23*Power(t2,3)) + 
                  s*(14 + 99*t2 + 32*Power(t2,2) - 54*Power(t2,3) - 
                     13*Power(t2,4))) + 
               Power(t1,2)*(21 + 38*t2 - 31*Power(t2,2) - 
                  34*Power(t2,3) + 6*Power(t2,5) + 
                  Power(s,2)*(12 - 32*t2 - 16*Power(t2,2) + 
                     10*Power(t2,3) + 27*Power(t2,4)) - 
                  s*(46 + 135*t2 - 6*Power(t2,2) - 59*Power(t2,3) + 
                     26*Power(t2,4) + 14*Power(t2,5))) - 
               t1*(Power(-1 + t2,2)*
                   (-15 - 38*t2 - 26*Power(t2,2) + 7*Power(t2,3) + 
                     2*Power(t2,4)) + 
                  Power(s,2)*t2*
                   (17 - 18*t2 + Power(t2,2) + Power(t2,3) + 
                     9*Power(t2,4)) + 
                  s*(13 - 12*t2 + 27*Power(t2,2) - 34*Power(t2,3) + 
                     17*Power(t2,4) - 6*Power(t2,5) - 5*Power(t2,6))))))*
       T5(1 - s2 + t1 - t2))/
     ((-1 + t1)*(s - s2 + t1)*(1 - s2 + t1 - t2)*(s - s1 + t2)*
       Power(t1 - s2*t2,3)));
   return a;
};
